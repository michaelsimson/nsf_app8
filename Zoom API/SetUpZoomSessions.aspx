﻿<%--<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="SetUpZoomSessions.aspx.cs" Inherits="Zoom_API_SetUpZoomSessions" %>--%>

<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="SetUpZoomSessions.aspx.cs" Inherits="Zoom_API_SetUpZoomSessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <%--<script src="../js/jquery-1.7.2.min.js"></script>
        <script src="js/jquery-1.7.2.min.js"></script>--%>
        <link href="../css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="../css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="../js/jquery.qtip.min.js"></script>

        <script src="../js/ezmodal.js"></script>

        <script src="../js/Hover.js"></script>
        <style type="text/css">
            .tblCell {
                border: 1px solid #999999;
                border-collapse: collapse;
            }
        </style>
        <script type="text/javascript">
            function StartMeeting(jsonData, type) {
                if (type == "1") {

                    var sessionKey = jsonData.id;
                    document.getElementById("<%=hdnSessionKey.ClientID%>").value = sessionKey;
                    var hostlink = jsonData.start_url
                    document.getElementById("<%=hdnHostURL.ClientID%>").value = hostlink;

                    var joinLink = jsonData.join_url;
                    document.getElementById("<%=hdnJoinMeetingUrl.ClientID%>").value = joinLink;

                    document.getElementById("<%=btnCreateZoomSession.ClientID%>").click();
                    $("#spnStatus").text("Meeting created successfully");
                }
                if (type == "3") {
                    $("#spnStatus").text("Meeting deleted successfully");

                }
                if (type == "4") {
                    $("#spnStatus").text("Meeting updated successfully");
                }
                bindMeetingList(jsonData);

            }

            function bindMeetingList(jsonData) {
                var tblHtml = "";

                tblHtml += "<tr style='background-color:#FFFFCC;'>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Ser#";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Action";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Title";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Key";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Meeting Type";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Star Date/Time";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Duration";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Host URL";
                tblHtml += "</td>";
                tblHtml += "<td class='tblCell' style='font-weight:bold; color:green;'>Join URL";
                tblHtml += "</td>";


                tblHtml += "</tr>";
                if (jsonData.meetings != "") {

                    $.each(jsonData.meetings, function (index, value) {

                        var meetingType = "";
                        if (value.type == "1") {
                            meetingType = "Instant Meeting";
                        } else if (value.type == "2") {
                            meetingType = "Scheduled Meeting";
                        } else if (value.type == "3") {
                            meetingType = "Recurring Meeting";
                        }
                        // $.each(value, function (meetIndex, meetVal) {

                        tblHtml += "<tr style='height:25px;'>";
                        tblHtml += "<td class='tblCell'>" + (index + 1) + "";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'><input type='button' attr-meetingID=" + value.id + " class='btnUpdate' value='Update'/><input type='button' attr-meetingID=" + value.id + " class='btnDelete' value='Delete'/>";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'>" + value.topic + "";
                        tblHtml += "</td>";

                        tblHtml += "<td class='tblCell'>" + value.id + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + meetingType + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + value.start_time + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'>" + value.duration + "";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'><a href=" + value.start_url + ">" + (value.start_url).substring(0, 20) + "...</a>";
                        tblHtml += "</td>";
                        tblHtml += "<td class='tblCell'><a href=" + value.join_url + ">" + (value.join_url).substring(0, 20) + "...</a>";
                        tblHtml += "</td>";
                        tblHtml += "</tr>";
                        //});

                    });
                } else {
                    tblHtml += "<tr style='height:25px;'>";
                    tblHtml += "<td colspan='9' align='center'><span style='font-weight:bold; color:red;'>No meeting found</span>";
                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                }

                $("#tblMeetingList").html(tblHtml);

            }

            function deleteMeeting() {


                if (confirm("Are you sure want to delete meeting?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }
            }

            $(document).on("click", ".btnDelete", function (e) {
                var meetingID = $(this).attr("attr-meetingID");
                document.getElementById("<%=hdnMeetingKey.ClientID%>").value = meetingID;
                if (confirm("Are you sure want to delete meeting?")) {
                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }

            });

            $(document).on("click", ".btnUpdate", function (e) {
                var meetingID = $(this).attr("attr-meetingID");
                document.getElementById("<%=hdnMeetingKey.ClientID%>").value = meetingID;
                var topic = $(this).parent().next().html();
                var startTime = $(this).parent().next().next().next().next().html();
                var duration = $(this).parent().next().next().next().next().next().html();
                document.getElementById("<%=txtTopic.ClientID%>").value = topic;

                document.getElementById("<%=txtDuration.ClientID%>").value = duration;
                document.getElementById("<%=txtMeetingPwd.ClientID%>").value = "training";
                document.getElementById("<%=btnCreateMeeting.ClientID%>").value = "Update Session";
                document.getElementById("<%=hdnIsUpdate.ClientID%>").value = "1";
            });

            function JoinMeeting() {

                var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

                window.open(url, '_blank');

            }

            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }
            function showAlert() {
                alert("Meeting attendees can only join their class up to 30 minutes before class time");
            }

            function showPwdAlert() {
                if (confirm("If you choose to use the password, student or attendee will need the password to join the session.  Press Cancel to set up the meeting without password.  If you choose OK, please communicate the password with the attendees.")) {
                    document.getElementById("<%=btnScheduleMeeting.ClientID%>").click();
                } else {
                    document.getElementById("<%=btnScheduleMeeting.ClientID%>").click();
                }
            }

            $(function (e) {
                roleID = document.getElementById("<%=hdnRoleID.ClientID%>").click();
                accepted = "Y";
            });
            //$(function (e) {


            //    $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
            //        content: {

            //            text: function (event, api) {
            //                return '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx" target="_blank">Calendar signup of that coach (all signups)</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachClassCalendar.aspx?coachID=' + $(this).attr("attr-coachid") + '"" target="_blank">Set up Class Calendar</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=1" target="_blank">Coach Contact Information</a> </div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '" target="_blank" >Contact List of Students</a> </div>';
            //            },

            //            title: 'Online Coaching!',
            //            button: 'Close'
            //        },
            //        hide: {
            //            event: false
            //        },
            //        style: {
            //            classes: 'qtip-green qtip-shadow'
            //        },
            //        show: {
            //            solo: true
            //        }
            //    })

            //    $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip to
            //        content: {

            //            text: function (event, api) {
            //                return '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + $(this).attr("attr-parentId") + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx" target="_blank">Change Coach</a> </div>';
            //            },

            //            title: 'Online Coaching!',
            //            button: 'Close'
            //        },
            //        hide: {
            //            event: false
            //        },
            //        style: {
            //            classes: 'qtip-green qtip-shadow'
            //        },
            //        show: {
            //            solo: true
            //        }
            //    })


            //});

            //$(document).on("mouseover", ".ancParentInfo", function (e) {
            //    getParentInfo($(this).attr("attr-parentID"));
            //});

            //$(document).on("click", ".ancParentInfo", function (e) {
            //    $("#btnPopUP").trigger("click");
            //});

            //function getParentInfo(pMemberID) {

            //    var jsonData = { PMemberID: pMemberID };
            //    $.ajax({
            //        type: "POST",
            //        url: "SetupZoomSessions.aspx/ListParentInfo",
            //        contentType: "application/json; charset=utf-8",
            //        dataType: "json",
            //        data: JSON.stringify(jsonData),
            //        async: true,
            //        cache: false,
            //        success: function (data) {

            //            var tblHtml = "";
            //            tblHtml += " <thead>";
            //            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent1 Name</td>";
            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Parent2 Name</td>";

            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Parent1 Email</td>";
            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent2 Email</td>";

            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child Name</td>";


            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child EMail</td>";
            //            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
            //            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


            //            tblHtml += "</tr>";
            //            tblHtml += " </thead>";

            //            $.each(data.d, function (index, value) {
            //                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndFirstName + " " + value.IndLastName + "";
            //                tblHtml += "</td>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseFirstName + " " + value.SpouseLastName + "";
            //                tblHtml += "</td>";


            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndEMail + "";
            //                tblHtml += "</td>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseEmail + "";
            //                tblHtml += "</td>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildFirstName + " " + value.ChildLastName + "";
            //                tblHtml += "</td>";



            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildEmail + "";
            //                tblHtml += "</td>";

            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
            //                tblHtml += "</td>";
            //                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
            //                tblHtml += "</td>";


            //                tblHtml += "</tr>";
            //            });

            //            $(".tblParentInfo").html(tblHtml);


            //        }
            //    })
            //}


        </script>
    </div>



    <div style="width: 1100px; margin-left: auto; margin-right: auto; min-height: 400px;">
        <div>
            <asp:Button ID="btnDeleteMeeting" runat="server" Text="Delete Meeting" Style="display: none;" OnClick="btnDeleteMeeting_Click" />
            <asp:Button ID="btnUpdateMeeting" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnCreateZoomSession_Click" />
            <asp:Button ID="btnCreateZoomSession" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnCreateZoomSession_Click" />
            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            <asp:Button ID="btnScheduleMeeting" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnScheduleMeeting_Click" />

        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <h1 style="color: #46A71C;">Set up Zoom Sessions</h1>
        </div>
        <div style="clear: both; margin-bottom: 30px;"></div>

        <div id="dvManipulateZoom" style="width: 1100px;">
            <div id="dvFrstRow" style="padding: 10px;">
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Event year</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlYear" Style="width: 140px;" OnSelectedIndexChanged="ddYear_SelectedIndexChanged" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Semester</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlPhase" Style="width: 140px;" runat="server" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Coach</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlCoach" Style="width: 140px;" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Product Group</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlProductGroup" Style="width: 140px;" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="dvSecondRow" style="padding: 10px;">
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Product</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlProduct" Style="width: 140px;" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Level</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlLevel" Style="width: 140px;" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Session#</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlSessionNo" Style="width: 140px;" runat="server">
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Meeting Type</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlMeetingType" Style="width: 140px;" runat="server">
                            <asp:ListItem Value="3" Selected="True">Recurring Meeting</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="dvThirdRow" style="padding: 10px;">
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Topic</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:TextBox ID="txtTopic" Style="width: 135px;" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Meeting Password</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:TextBox ID="txtMeetingPwd" Style="width: 135px;" TextMode="Password" runat="server"></asp:TextBox>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Start Type</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlStartType" Style="width: 140px;" runat="server">

                            <asp:ListItem Value="Video">Video</asp:ListItem>
                            <asp:ListItem Value="Scree_Share" Selected="True">Scree_Share</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Join Before Host</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlJoinBH" Style="width: 140px;" runat="server">

                            <asp:ListItem Value="true">True</asp:ListItem>
                            <asp:ListItem Value="false">False</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div id="dvForthRow" style="padding: 10px;">
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Audio</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlAudio" Style="width: 140px;" runat="server">
                            <asp:ListItem Value="both">Both</asp:ListItem>
                            <asp:ListItem Value="Voip">Voip</asp:ListItem>
                            <asp:ListItem Value="telephony">Telephony</asp:ListItem>


                        </asp:DropDownList>
                    </div>
                </div>
                <div style="width: 250px; float: left;">
                    <div style="width: 90px; float: left;">
                        <span style="font-weight: bold;">Host Video</span>
                    </div>
                    <div style="width: 150px; float: left;">
                        <asp:DropDownList ID="ddlHostVideo" Style="width: 140px;" runat="server">

                            <asp:ListItem Value="true">True</asp:ListItem>
                            <asp:ListItem Value="false" Selected="True">False</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>

                <div style="width: 250px; float: left;">
                    <div style="float: left;">
                        <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Session" OnClick="btnCreateMeeting_Click" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />
                    </div>
                </div>


            </div>
        </div>

        <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; padding: 10px; display: none;">
            <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
                <td align="left">Event year <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Event <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Chapter <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Semester <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Coach <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Product Group <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">product <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Level <span style="color: red; font-size: 11px;">*</span></td>
                <td align="left">Session No <span style="color: red; font-size: 11px;">*</span></td>
            </tr>

            <tr class="ContentSubTitle" align="center">
                <td align="left"></td>
                <td align="left">
                    <asp:DropDownList ID="ddlEvent" Style="width: 103px;" runat="server">
                    </asp:DropDownList></td>
                <td align="left">
                    <asp:DropDownList ID="ddlChapter" runat="server">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                    </asp:DropDownList></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left"></td>
            </tr>

            <tr>
                <td align="center" colspan="9">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />

                </td>
            </tr>
        </table>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>

        <div style="margin-left: auto; margin-right: auto; padding: 10px; width: 80%; border: 1px solid #c6cccd; display: none;">
            <center>
                <h2>Create Zoom Session(s)</h2>
            </center>

            <table id="tblAddNewMeeting" runat="server" visible="true" align="center" style="width: 80%;">

                <tr class="ContentSubTitle" align="center">
                    <td align="left">Meeting Type
                    </td>
                    <td align="left"></td>
                    <td align="left">Topic
                    </td>
                    <td align="left">



                        <span style="color: red; font-size: 11px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>
                </tr>


                <tr class="ContentSubTitle">
                    <td align="left">Meeting Password
                    </td>
                    <td align="left">

                        <div style="margin-bottom: 10px;"></div>
                    </td>

                    <td align="left">Time Zone
                    </td>
                    <td align="left">
                        <asp:TextBox ID="txtDuration" runat="server" Visible="false"></asp:TextBox>
                        <asp:DropDownList ID="ddlTimeZone" Style="width: 206px;" runat="server">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                            <asp:ListItem Value="11" Selected="true">EST/EDT (Eastern or New York Time)</asp:ListItem>
                            <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                            <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                            <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>

                        </asp:DropDownList>
                        <span style="color: red; position: relative; left: 5px;">*</span>
                        <div style="margin-bottom: 5px;"></div>
                    </td>

                </tr>

                <tr class="ContentSubTitle">
                    <td align="left">Start Type
                    </td>
                    <td align="left">

                        <div style="margin-bottom: 5px;"></div>
                    </td>

                    <td align="left">Join Before Host
                    </td>
                    <td align="left">

                        <div style="margin-bottom: 5px;"></div>
                    </td>

                </tr>

                <tr class="ContentSubTitle">
                    <td align="left">Audio
                    </td>
                    <td align="left"></td>

                    <td align="left">Host Video
                    </td>
                    <td align="left">

                        <div style="margin-bottom: 5px;"></div>
                    </td>

                </tr>

                <tr class="ContentSubTitle" align="center">
                    <td align="left" colspan="4">

                        <div style="clear: both; margin-bottom: 5px;"></div>
                        <span style="color: red; font-size: 11px;">* mandatory fields</span>
                    </td>
                    <td align="left"></td>
                </tr>



            </table>
        </div>


        <div style="clear: both;"></div>
        <div align="center"><span id="spnError" runat="server" style="color: Red; font-weight: bold;"></span></div>
        <div style="clear: both;"></div>
        <div align="center"><span id="spnStatus" runat="server" style="color: green; font-weight: bold;"></span></div>
        <div style="clear: both; margin-bottom: 30px;"></div>
        <div style="float: left;" id="dvFilterSection" runat="server">
            <center><span style="font-weight: bold; font-size: 18px;">Search Recurring Zoom Sessions</span></center>
            <div style="clear: both; margin-bottom: 20px;"></div>
            <div style="float: left; margin-left: 10px; width: 125px;" id="dvYearSection" runat="server">
                <div style="float: left; width: 50px;">
                    <span style="font-weight: bold;">Year</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="ddlYearFilter" runat="server" OnSelectedIndexChanged="ddlYearFilter_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; width: 160px; margin-left: 10px;" id="dvPhaseFilter" runat="server">
                <div style="float: left; width: 75px;">
                    <span style="font-weight: bold;">Semester</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="ddlPhaseFilter" runat="server" OnSelectedIndexChanged="ddlPhaseFilter_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="Fall">Fall</asp:ListItem>
                        <asp:ListItem Value="Spring">Spring</asp:ListItem>
                        <asp:ListItem Value="Summer">Summer</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; margin-left: 10px; width: 170px;" id="dvFilterPrdGroupSection" runat="server">
                <div style="float: left; width: 100px;">
                    <span style="font-weight: bold;">Product Group</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="ddlProductGroupFilter" runat="server" OnSelectedIndexChanged="ddlProductGroupFilter_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; width: 160px; margin-left: 10px;" id="dvProductFilter" runat="server">
                <div style="float: left; width: 75px;">
                    <span style="font-weight: bold;">Product</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="ddlProductFilter" runat="server" OnSelectedIndexChanged="ddlProductFilter_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
            <div style="float: left; width: 200px; margin-left: 10px;" id="Div1" runat="server">
                <div style="float: left; width: 75px;">
                    <span style="font-weight: bold;">Level</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="DDlLevelFilter" Style="width: 110px;" runat="server" OnSelectedIndexChanged="DDlLevelFilter_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>

            <div style="float: left; margin-left: 10px;" id="dvCoachFilter" runat="server">
                <div style="float: left; width: 60px;">
                    <span style="font-weight: bold;">Coach</span>
                </div>
                <div style="float: left;">
                    <asp:DropDownList ID="ddlCoachFilter" Style="width: 110px;" runat="server" OnSelectedIndexChanged="ddlCoachFilter_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
              <div style="float: left; margin-left:10px;">
                    <asp:Button ID="btnClearFilter" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                </div>
            <div style="float: left; width: 200px;" id="dvButton" runat="server" visible="false">

                <div style="float: left;">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
                </div>
               
            </div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <span style="font-weight: bold; font-size: 13px; color: green;">Table 1: Meeting List</span>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <center><span id="spntable" runat="server" style="font-weight: bold; color: red;"></span></center>
        <table id="tblMeetingList" align="center" style="border: 1px solid #999999; border-width: 3px; border-collapse: collapse; width: 100%;">
        </table>

        <div style="clear: both;"></div>
        <div style="float: left; width: 50px; display: none;">
            <span style="font-weight: bold;">Filter :</span>
        </div>
        <div style="float: left; display: none;">
            <asp:DropDownList ID="ddlFilterBy" runat="server" OnSelectedIndexChanged="ddlFilterBy_SelectedIndexChanged" AutoPostBack="true">
                <asp:ListItem Value="None">None</asp:ListItem>
                <asp:ListItem Value="Year">By Year</asp:ListItem>
                <asp:ListItem Value="ProductGroup">By ProductGroup</asp:ListItem>
                <asp:ListItem Value="Product">By Product</asp:ListItem>
                <asp:ListItem Value="Phase">By Semester</asp:ListItem>
                <asp:ListItem Value="Coach">By Coach</asp:ListItem>
            </asp:DropDownList>

        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="width: 1100px; overflow-x: scroll;">
            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" OnRowCommand="GrdMeeting_RowCommand" Style="width: 1100px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" PageSize="500" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                        <ItemTemplate>
                            <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                                <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                                <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                                <asp:Label ID="lblOrgTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'>'></asp:Label>
                                <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>
                                <asp:Label ID="lblHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'>'></asp:Label>
                            </div>
                            <asp:Button ID="btnModifyMeeting" runat="server" Text="Update" CommandName="Modify" />
                            <asp:Button ID="btnCancelMeeting" runat="server" Text="Delete" CommandName="DeleteMeeting" />
                        </ItemTemplate>

                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="SessionNo" HeaderText="Session"></asp:BoundField>
                    <asp:TemplateField HeaderText="Coach">

                        <ItemTemplate>

                            <asp:LinkButton runat="server" ID="lnkCoach" attr-coachId='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>' CssClass="lnkCoachName" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                    <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                    <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                    <asp:BoundField DataField="StartDate" HeaderText="Start Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                    <asp:TemplateField HeaderText="Begin Time">

                        <ItemTemplate>
                            <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="End Time">

                        <ItemTemplate>
                            <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                        </ItemTemplate>
                    </asp:TemplateField>
                    <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                    <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                    <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>
                    <asp:BoundField DataField="SessionType" HeaderText="Meeting Type"></asp:BoundField>
                    <asp:BoundField DataField="VRoom" HeaderText="VRoom"></asp:BoundField>
                    <asp:BoundField DataField="UserID" HeaderText="UserID" Visible="false"></asp:BoundField>
                    <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>

                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>

                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                                <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                            <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>

                <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

                <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
            </asp:GridView>
        </div>

        <div style="clear: both; margin-bottom: 20px;"></div>

        <div style="width: 1100px; overflow-x: scroll;" id="dvStudentList" runat="server">
            <div align="center">
                <span style="font-weight: bold; font-size: 13px;">Table 2: Students List</span>
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1100px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdStudentsList_RowCommand">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" Visible="false" HeaderText="Action">

                        <ItemTemplate>


                            <%--  <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                            <asp:Button ID="BtnReRegister" runat="server" Text="Update Registration" CommandName="ReRegister" />--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>

                            <div style="display: none;">
                                <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeJoinURL") %>'>'></asp:Label>
                                <asp:Label ID="lblRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RegisteredID") %>'>'></asp:Label>
                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>

                    <asp:TemplateField HeaderText="Parent">

                        <ItemTemplate>
                            <asp:LinkButton runat="server" ID="lnkparent" attr-parentId='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>' CssClass="lnkParentName" Text='<%# Bind("ParentName")%>'></asp:LinkButton>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                    <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                    <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="AttendeeID" Visible="false" HeaderText="Attendee ID"></asp:BoundField>
                    <asp:BoundField DataField="RegisteredID" Visible="false" HeaderText="Registered ID"></asp:BoundField>
                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString()+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>
                            </div>

                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
            <div style="clear: both;"></div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <asp:Button ID="btnCloseTable2" runat="server" Visible="false" Text="Close Table 2" OnClick="btnCloseTable2_Click" />
        </div>
    </div>


    <button type="button" class="btnPopUP" style="display: none;" ezmodal-target="#demo">Open</button>

    <div id="demo" class="ezmodal" style="display: none;">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                Parent Information
            </div>

            <div class="ezmodal-content">


                <div>
                    <table align="center" class="tblParentInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                    </table>
                </div>

            </div>



            <div class="ezmodal-footer">

                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>



    <input type="hidden" id="hdnMeetingKey" value="" runat="server" />
    <input type="hidden" id="hdnIsUpdate" value="" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="" runat="server" />
    <input type="hidden" id="hdnHostURL" value="" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="" runat="server" />
    <input type="hidden" id="hdnJoinMeetingUrl" value="" runat="server" />
    <input type="hidden" id="hdnZoomURL" value="" runat="server" />
    <input type="hidden" id="hdnStartTime" value="" runat="server" />
    <input type="hidden" id="hdnDay" value="" runat="server" />
    <input type="hidden" id="hdnhostID" value="" runat="server" />
    <input type="hidden" id="hdnCoachID" value="" runat="server" />

    <input type="hidden" id="hdnRoleID" value="0" runat="server" />

</asp:Content>
