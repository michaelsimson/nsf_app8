<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingSelectionSum.aspx.vb" Inherits="CoachingSelectionSum" title="Coaching Selection Summary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table id="Table1">
				<tr>
				<td colspan ="2">
				<table id="Table2" width="100%">
				<tr>
				<td class="ContentSubTitle" align="left" width="25%"   >
						<asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02"  runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
					<td class="Heading" style ="padding-left:200px"  align="left" >
                        Coaching Summary
					</td>	
				</tr>
				</table> 
				</td>
				
					
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
<%--							<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>--%>
							<tr>
								<td><asp:label id="lblStateZip" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td><asp:label ID="lblNote" Visible="true" runat="server" Font-Bold="true" Text="" ForeColor="Red" Font-Size="Small"></asp:label></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">Child/Children Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
					<asp:datagrid id="dgselected" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildName") %>' CssClass="SmallFont"></asp:Label>
								        <asp:Label id="lblChildNumber" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildNumber") %>' CssClass="SmallFont"/>
									    <asp:Label id="lblProductCode" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>' CssClass="SmallFont"/>
									    <asp:Label id="lblChapterID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterID") %>' CssClass="SmallFont"/>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Grade" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblGrade" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Grade") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Product" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblProductName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Coach Name" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblCoachName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CoachName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Level" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblLevel" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Level") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>									
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="capacity" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblcapacity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MaxCapacity") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="StartDate" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblStartDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="CoachingDay" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lblCoachingDay" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Day") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Eastern Time Zone" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" >
									<ItemTemplate>
										<asp:Label id="lbltime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Duration(in hours)" HeaderStyle-Font-Bold="true"   HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblDuration" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Duration") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<%--<asp:TemplateColumn HeaderText="Start Date" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblSdate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.StartDate", "{0:d}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>--%>
								<asp:TemplateColumn HeaderText="End Date" Visible="false" HeaderStyle-Font-Bold="true"   HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblEDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EndDate", "{0:d}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"   HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblRegFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegFee", "{0:c}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							    <asp:TemplateColumn HeaderText="LateFee" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblLateFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LateFee", "{0:c}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="City" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000" Visible="false">
									<ItemTemplate>
										<asp:Label id="lblCity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' CssClass="SmallFont">
										</asp:Label>
								    </ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="State" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000" Visible="false">
									<ItemTemplate>
									    <asp:Label id="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>' CssClass="SmallFont">
										</asp:Label>										
									</ItemTemplate>
								</asp:TemplateColumn>	
								<asp:TemplateColumn HeaderText="ProductLevelPricing"  ItemStyle-HorizontalAlign="Center" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblProductLevelPricing" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductLevelPricing") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>	
								<asp:TemplateColumn HeaderText="ProductGroupId" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" Visible="false">
									<ItemTemplate>
										<asp:Label id="lblProductGroupID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductGroupID") %>' CssClass="SmallFont"/></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ProductID" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" Visible="false">
									<ItemTemplate>
										<asp:Label id="lblProductID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductID") %>' CssClass="SmallFont"/></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ProductGroupCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" Visible="false">
									<ItemTemplate>
										<asp:Label id="lblProductGroupCode" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductGroupCode") %>' CssClass="SmallFont"/></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>			
							</Columns>
						</asp:datagrid>
					</td>
				</tr>
				           <tr><td align="right"><asp:Label id="lblDiscount" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                             <asp:Label ID="lblTotalFee" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label></td></tr>
						   <tr><td class="SmallFont">Your registration is 2/3rd tax-deductible and helps a poor child go to college in India.</td></tr>
							
				<tr>
				    <td>
				    <table align="center">
				    <tr><td>
				    	<asp:CheckBox ID="ChkEmail" runat="server" Visible="true" /> 
                        <%--<asp:RequiredFieldValidator ID="RFVChkEmail"   runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>--%> You will receive all communications to  
				        <asp:Label ID="lblCommuEmail" runat="Server" Text="" Font-Bold="True" ForeColor="Red"></asp:Label>. 
				         <asp:LinkButton  class="btn_02" ID="hlinkEmail" runat="server">Change Email</asp:LinkButton>.(This will change your login too)
				        </td></tr>
				        <tr><td id="tdclasstime" runat="server" visible="false" ><asp:CheckBox ID="ChkClassTime" runat="server" Visible="true"/>
				       <%-- <asp:RequiredFieldValidator ID="RFVChkClassTime" ControlToValidate="ChkClassTime" runat="server" ErrorMessage="*"/>--%>
				        I have reviewed the 
				        <% If Session("ProductCodeCoach") = "MB2" Then %>
				        <a href ="~/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc" class ="btn_02" target ="_parent">level determination</a>
				        <% ElseIf Session("ProductCodeCoach") = "SATE" Or Session("ProductCodeCoach") = "SATM" Then%>
				        <a href ="#" class ="btn_02" target ="_parent">level determination</a>
				         <% Else %>
				        <a href ="~/BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc" class ="btn_02" target ="_parent">level determination</a>
				         <% End If %>
				         document and picked appropriate level as this cannot be changed.</td></tr>
				        <tr><td><asp:CheckBox ID="ChkDetails" runat="server" Visible="true"/>
				        <%--<asp:RequiredFieldValidator ID="RFVChkDetails" ControlToValidate="ChkDetails" runat="server" ErrorMessage="*"/>--%>
				        Class timing and level cannot be changed later. Unless paid, seat is not guaranteed. Once paid, fee is not refundable.</td></tr>
				     </table> 
				    </td>
				</tr> 
				
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Text ="" Visible="True" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblEmailInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="True" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
			</table>
			
</asp:Content>

