if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_Bee_Contest]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[Bee] DROP CONSTRAINT FK_Bee_Contest
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[FK_TechTeam_Contest]') and OBJECTPROPERTY(id, N'IsForeignKey') = 1)
ALTER TABLE [dbo].[TechTeam] DROP CONSTRAINT FK_TechTeam_Contest
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Contest]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Contest]
GO

CREATE TABLE [dbo].[Contest] (
	[ContestID] [int] IDENTITY (1, 1) NOT NULL ,
	[ContestTypeID] [int] NOT NULL ,
	[NSFChapterID] [int] NOT NULL ,
	[ContestCategoryID] [int] NULL ,
	[Phase] [int] NULL ,
	[GroupNo] [int] NULL ,
	[BadgeNoBeg] [int] NULL ,
	[BadgeNoEnd] [int] NULL ,
	[ContestDate] [smalldatetime] NULL ,
	[StartTime] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[EndTime] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Building] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Contest_Year] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Room] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Contest] ADD 
	CONSTRAINT [PK_Contest] PRIMARY KEY  CLUSTERED 
	(
		[ContestID]
	)  ON [PRIMARY] 
GO

ALTER TABLE [dbo].[Contest] ADD 
	CONSTRAINT [FK_Contest_ContestTypeLookup] FOREIGN KEY 
	(
		[ContestTypeID]
	) REFERENCES [dbo].[ContestTypeLookup] (
		[ContestTypeID]
	)
GO

