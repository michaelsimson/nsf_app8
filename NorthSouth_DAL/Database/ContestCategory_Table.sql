if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[ContestCategory]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[ContestCategory]
GO

CREATE TABLE [dbo].[ContestCategory] (
	[ContestCategoryID] [int] NOT NULL ,
	[ContestCode] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NOT NULL ,
	[ContestDesc] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[GradeBased] [bit] NULL ,
	[AgeFrom] [float] NULL ,
	[AgeTo] [float] NULL ,
	[GradeFrom] [int] NULL ,
	[GradeTo] [int] NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Fee] [money] NULL ,
	[DownloadLink] [varchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContestYear] [int] NULL 
) ON [PRIMARY]
GO

