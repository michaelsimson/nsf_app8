if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[IndSpouse]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[IndSpouse]
GO

CREATE TABLE [dbo].[IndSpouse] (
	[AutoMemberID] [int] IDENTITY (1, 1) NOT NULL ,
	[Relationship] [int] NULL ,
	[DeleteReason] [nvarchar] (255) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DeletedFlag] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CreateDate] [smalldatetime] NULL ,
	[MemberSince] [smalldatetime] NULL ,
	[ModifyDate] [smalldatetime] NULL ,
	[MailingLabel] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SendReceipt] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LiasonPerson] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ReferredBy] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Chapter] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[NewsLetter] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Group2] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Group3] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PotentialDonor] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VolunteerFlag] [nvarchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VolunteerRole1] [int] NULL ,
	[Group1] [nvarchar] (60) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Career] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Employer] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CountryOfOrigin] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Email] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SecondaryEmail] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Education] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Fax] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WPhone] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[WFax] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Gender] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HPhone] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CPhone] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Zip] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Country] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[FirstName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MiddleInitial] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[LastName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MemberID] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DonorType] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Title] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[StateOfOrigin] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PrimaryEmail_Old] [nvarchar] (100) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[HomePhone_Old] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CellPhone_Old] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address1_Old] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Address2_Old] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[City_old] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[State_Old] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ZipCode_Old] [nvarchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PrimaryIndSpouseID] [int] NULL ,
	[PrimaryContact] [nvarchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[VolunteerRole2] [int] NULL ,
	[VolunteerRole3] [int] NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[IndSpouse] ADD 
	CONSTRAINT [PK_IndSpouse] PRIMARY KEY  CLUSTERED 
	(
		[AutoMemberID]
	)  ON [PRIMARY] 
GO

