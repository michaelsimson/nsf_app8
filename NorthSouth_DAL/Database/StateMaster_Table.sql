if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[StateMaster]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[StateMaster]
GO

CREATE TABLE [dbo].[StateMaster] (
	[StateMasterID] [int] NULL ,
	[StateCode] [char] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Name] [char] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

