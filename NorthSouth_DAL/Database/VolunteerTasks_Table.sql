if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[VolunteerTasks]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[VolunteerTasks]
GO

CREATE TABLE [dbo].[VolunteerTasks] (
	[VolunteerTaskID] [int] IDENTITY (1, 1) NOT NULL ,
	[LevelCode] [char] (1) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[TaskDescription] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[VolunteerTasks] ADD 
	CONSTRAINT [PK_VolunteerTasks] PRIMARY KEY  CLUSTERED 
	(
		[VolunteerTaskID]
	)  ON [PRIMARY] 
GO

