/*
 * VSS Header, do not delete!
 *
 * $Revision: 1.1 $
 * $Date: 2003/10/05 22:16:53 $
 * $Author: Administrator $
 *
 */

USE NSF_Registration
GO

/*
 * Purpose:
 *
 * Change History:
 *
 */

--VSS $Revision: 1.1 $
TRUNCATE TABLE [dbo].[ContestCategory]

DBCC CHECKIDENT ('[dbo].[ContestCategory]', RESEED, 1)

SET IDENTITY_INSERT [dbo].[ContestCategory] ON

INSERT INTO [dbo].[ContestCategory] (
	[ContestCategoryID],
	[ContestCode],
	[ContestDesc],
	[GradeBased],
	[AgeFrom],
	[AgeTo],
	[GradeFrom],
	[GradeTo],
	[Status],
	[Fee],
	[DownloadLink],
	[ContestYear]
)
	SELECT 1, 'JE', 'Junior Essay Writing Bee', True, NULL, NULL, 3, 5, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 2, 'JGB', 'Junior Geography Bee', True, NULL, NULL, 1, 3, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 3, 'JSB', 'Junior Spelling Bee', False, 5, 8, NULL, NULL, 'Active', 25.0000, '3CA7F1EC-0A7F-408F-9159.pdf', 2006 UNION
	SELECT 4, 'JSCB', 'Junior SpeechCraft Bee', True, NULL, NULL, 6, 8, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 5, 'JVB', 'Junior Vocabulary Bee', False, 9, 12, NULL, NULL, 'Active', 25.0000, '41EFDDAF-403-05398.pdf', 2006 UNION
	SELECT 6, 'MB-I', 'Math Bee Level I', True, NULL, NULL, 1, 2, 'Active', 20.0000, NULL, 2006 UNION
	SELECT 7, 'MB-II', 'Math Bee Level II', True, NULL, NULL, 3, 5, 'Active', 20.0000, NULL, 2006 UNION
	SELECT 8, 'MB-III', 'Math Bee Level III', True, NULL, NULL, 6, 8, 'Active', 20.0000, NULL, 2006 UNION
	SELECT 9, 'MB-IV', 'Math Bee Level IV', True, NULL, NULL, 9, 10, 'Active', 20.0000, NULL, 2006 UNION
	SELECT 10, 'ME', 'Intermediate Essay Writing Bee', True, NULL, NULL, 6, 8, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 11, 'SE', 'Senior Essay Writing  Bee', True, NULL, NULL, 9, 12, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 12, 'SGB', 'Senior Geography Bee', True, NULL, NULL, 4, 8, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 13, 'SSB', 'Senior Spelling Bee', False, 9, 14, NULL, NULL, 'Active', 25.0000, '44026C53-41E20CEC-362-0B8FE.pdf', 2006 UNION
	SELECT 14, 'SSCB', 'Senior SpeechCraft Bee', True, NULL, NULL, 9, 12, 'Active', 15.0000, NULL, 2006 UNION
	SELECT 15, 'SVB', 'Senior Vocabulary Bee', False, 13, 16, NULL, NULL, 'Active', 25.0000, '41EFC4C7-340-01220.pdf', 2006

SET IDENTITY_INSERT [dbo].[ContestCategory] OFF
