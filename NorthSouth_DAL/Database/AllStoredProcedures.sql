if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Split]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[Split]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_DeleteChapter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_DeleteChapter]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_DeleteIndSpouse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_DeleteIndSpouse]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetChapterByID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetChapterByID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetChapters]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetChapters]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetChaptersAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetChaptersAll]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetEvent]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetEvent]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetEventsList]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetEventsList]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetIndSpouseAll]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetIndSpouseAll]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetIndSpouseByID]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetIndSpouseByID]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetStates]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetStates]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetVolunteerAcivities]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetVolunteerAcivities]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_GetVolunteerTasks]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_GetVolunteerTasks]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertChapter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertChapter]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertChapterLevelContest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertChapterLevelContest]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertContest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertContest]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertIndSpouse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertIndSpouse]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_InsertNewEvent]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_InsertNewEvent]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_SelectContestCategoryByYear]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_SelectContestCategoryByYear]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_SelectWhereChapter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_SelectWhereChapter]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_SelectWhereIndSpouse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_SelectWhereIndSpouse]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateChapter]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateChapter]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateChapterLevelContest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateChapterLevelContest]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateContest]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateContest]
GO

if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[usp_UpdateIndSpouse]') and OBJECTPROPERTY(id, N'IsProcedure') = 1)
drop procedure [dbo].[usp_UpdateIndSpouse]
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE dbo.Split
	@TableName varchar(50),
	@ColName varchar(50),
	@UserVal varchar(4000)
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\Split
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Split the string by a specified delimiter and inserts the items to a Temp Table
	--
	--	Init	Date		Description                    
	--   	------	-------------------	
----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************
AS

	Declare @SQL as varchar(100)
	Declare @separator char(1)
	Declare @separator_position int
	Declare @array_value varchar(1000) 

	-- For my loop to work I need an extra separator at the end.  I always look to the
	-- left of the separator character for each array value
	set @separator = '~'
	--set @UserVal = @UserVal + @separator

	-- Loop through the string searching for separtor characters
	while patindex('%' + @separator + '%' , @UserVal) <> 0 
	begin
		-- patindex matches the a pattern against a string
		select @separator_position =  patindex('%' + @separator + '%' , 
@UserVal)
		select @array_value = left(@UserVal, @separator_position - 1)
		
		-- This is where you process the values passed.
		-- Replace this select statement with your processing
		-- @array_value holds the value of this element of the array
		--select Array_Value = @array_value
		
		set @SQL = 'Insert into ' + @TableName + ' (' + @ColName + ') values (''' + @array_value + ''')'

		Exec (@SQL)		

		-- This replaces what we just processed with and empty string
		select @UserVal = stuff(@UserVal, 1, @separator_position, '')
	end
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_DeleteChapter
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE dbo.usp_DeleteChapter
	@ChapterID int
AS

	Delete From dbo.Chapter
	Where
	ChapterID = @ChapterID

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO



------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Wednesday, October 26, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       StoredProcedures.cst
-- Procedure Name: dbo.usp_DeleteIndSpouse
-- Date Generated: Wednesday, October 26, 2005
-- Author:         Venkat Ambati
-- Contact:        TAG Workflow Team
-- Organization:   ISD Loans
-- Date Generated: Wednesday, October 26, 2005

------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE dbo.usp_DeleteIndSpouse
	@AutoMemberID int
AS

	Delete From dbo.IndSpouse
	Where
	AutoMemberID = @AutoMemberID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_GetChapterByID
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_GetChapterByID
	@ChapterID int
AS

SET TRANSACTION ISOLATION LEVEL READ COMMITTED

Select
	ChapterID,
	ChapterName,
	ChapterStateAbbr,
	CoordinatorName,
	Year,
	ContestDate,
	ContestNotes,
	CenterName,
	ChapterAddress1,
	ChapterAddress2,
	ChapterCity,
	ChapterState,
	ChapterZip,
	ChapterPhone,
	ContactEmail,
	RegistrationInstructions,
	SponsorName,
	ChapterArea,
	PaymentInfavor,
	PaymentAddress,
	PaymentCity,
	PaymentZip,
	PaymentAddressName,
	PaymentState,
	Status,
	PaymentEmail,
	MapFlag,
	DeadlineDate,
	RegfeePenalty,
	LatefeeStartdate,
	MathbeeFlag,
	ScoresStatus,
	CenterZone
From
	dbo.Chapter
Where
	ChapterID = @ChapterID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.usp_GetChapters 
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetChapters
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Retrieves all NSFChapters
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************
AS
	SELECT     ChapterID, ChapterName + ', ' + ChapterStateAbbr AS ChapterLocation
	FROM         dbo.ChapterDetails
	ORDER BY	ChapterStateAbbr
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_GetChaptersAll
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_GetChaptersAll
AS

SET TRANSACTION ISOLATION LEVEL READ COMMITTED

Select
	ChapterID,
	ChapterName,
	ChapterStateAbbr,
	CoordinatorName,
	Year,
	ContestDate,
	ContestNotes,
	CenterName,
	ChapterAddress1,
	ChapterAddress2,
	ChapterCity,
	ChapterState,
	ChapterZip,
	ChapterPhone,
	ContactEmail,
	RegistrationInstructions,
	SponsorName,
	ChapterArea,
	PaymentInfavor,
	PaymentAddress,
	PaymentCity,
	PaymentZip,
	PaymentAddressName,
	PaymentState,
	Status,
	PaymentEmail,
	MapFlag,
	DeadlineDate,
	RegfeePenalty,
	LatefeeStartdate,
	MathbeeFlag,
	ScoresStatus,
	CenterZone,
	ChapterName + ', ' + ChapterStateAbbr AS ChapterLocation
From
	dbo.Chapter
Order By	ChapterStateAbbr
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE	dbo.usp_GetEvent
@ContestID int
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetEvent
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Retrieves a Contest Inforamtion
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************
AS
Select	Contest.ContestID,  ContestCategory.ContestDesc as ContestDescription, Contest.Contest_Year, ContestTypeLookUp.ContestTypeDescription as ContestTypeName, Contest.ContestDate as  ContestDate, ChapterDetails.ChapterCity + ', ' +  ChapterDetails.ChapterState  as ChapterName, Contest.ContestTypeID as Contest_Type, Contest.Phase, Contest.StartTime, Contest.EndTime,Contest.Building, Contest.Room
	From	dbo.tmp_Contest Contest INNER JOIN
		dbo.ContestTypeLookUp on ContestTypeLookUp.ContestTypeID = Contest.ContestTypeID	INNER JOIN
		dbo.ChapterDetails 	on ChapterDetails.ChapterID = Contest.NSFChapterID	INNER JOIN
		dbo.tmp_ContestCategory	ContestCategory on ContestCategory.ContestCategoryID = Contest.ContestCategoryID
	Where Contest.ContestID = @ContestID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE	dbo.usp_GetEventsList
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetEventsList
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Retrieves a List of Contest for a give year
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************
@ContestYear char(10) 
AS
	Select	Contest.ContestID,  ContestCategory.ContestDesc as ContestDescription, Contest.Contest_Year, ContestTypeLookUp.ContestTypeDescription as ContestTypeName, Contest.ContestDate as  ContestDate, ChapterDetails.ChapterCity + ', ' + ChapterDetails.ChapterState as ChapterName
	From	dbo.tmp_Contest Contest INNER JOIN
		dbo.ContestTypeLookUp on ContestTypeLookUp.ContestTypeID = Contest.ContestTypeID	INNER JOIN
		dbo.ChapterDetails 	on ChapterDetails.ChapterID = Contest.NSFChapterID INNER JOIN
		dbo.tmp_ContestCategory ContestCategory	on ContestCategory.ContestCategoryID = Contest.ContestCategoryID
	Where Contest.Contest_Year = @ContestYear
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Wednesday, October 26, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       StoredProcedures.cst
-- Procedure Name: dbo.usp_GetIndSpouseAll
-- Date Generated: Wednesday, October 26, 2005
-- Author:         Venkat Ambati
-- Contact:        TAG Workflow Team
-- Organization:   ISD Loans
-- Date Generated: Wednesday, October 26, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_GetIndSpouseAll
AS

SET TRANSACTION ISOLATION LEVEL READ COMMITTED

Select
	AutoMemberID,
	Relationship,
	DeleteReason,
	DeletedFlag,
	CreateDate,
	MemberSince,
	ModifyDate,
	MailingLabel,
	SendReceipt,
	LiasonPerson,
	ReferredBy,
	Chapter,
	NewsLetter,
	Group2,
	Group3,
	PotentialDonor,
	VolunteerFlag,
	VolunteerRole1,
	Group1,
	Career,
	Employer,
	CountryOfOrigin,
	Email,
	SecondaryEmail,
	Education,
	Fax,
	WPhone,
	WFax,
	Gender,
	HPhone,
	CPhone,
	State,
	Zip,
	Country,
	Address1,
	Address2,
	City,
	FirstName,
	MiddleInitial,
	LastName,
	MemberID,
	DonorType,
	Title,
	StateOfOrigin,
	PrimaryEmail_Old,
	HomePhone_Old,
	CellPhone_Old,
	Address1_Old,
	Address2_Old,
	City_old,
	State_Old,
	ZipCode_Old,
	PrimaryIndSpouseID,
	PrimaryContact,
	VolunteerRole2,
	VolunteerRole3
From
	dbo.tmp_IndSpouse
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Wednesday, October 26, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       StoredProcedures.cst
-- Procedure Name: dbo.usp_GetIndSpouseByID
-- Date Generated: Wednesday, October 26, 2005
-- Author:         Venkat Ambati
-- Contact:        TAG Workflow Team
-- Organization:   ISD Loans
-- Date Generated: Wednesday, October 26, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_GetIndSpouseByID
	@AutoMemberID int
AS

SET TRANSACTION ISOLATION LEVEL READ COMMITTED

Select
	AutoMemberID,
	Relationship,
	DeleteReason,
	DeletedFlag,
	CreateDate,
	MemberSince,
	ModifyDate,
	MailingLabel,
	SendReceipt,
	LiasonPerson,
	ReferredBy,
	Chapter,
	NewsLetter,
	Group2,
	Group3,
	PotentialDonor,
	VolunteerFlag,
	VolunteerRole1,
	Group1,
	Career,
	Employer,
	CountryOfOrigin,
	Email,
	SecondaryEmail,
	Education,
	Fax,
	WPhone,
	WFax,
	Gender,
	HPhone,
	CPhone,
	State,
	Zip,
	Country,
	Address1,
	Address2,
	City,
	FirstName,
	MiddleInitial,
	LastName,
	MemberID,
	DonorType,
	Title,
	StateOfOrigin,
	PrimaryEmail_Old,
	HomePhone_Old,
	CellPhone_Old,
	Address1_Old,
	Address2_Old,
	City_old,
	State_Old,
	ZipCode_Old,
	PrimaryIndSpouseID,
	PrimaryContact,
	VolunteerRole2,
	VolunteerRole3
From
	dbo.tmp_IndSpouse
Where
	AutoMemberID = @AutoMemberID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.usp_GetStates 
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetStates
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Retrieves all NSFChapters
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	10/15/2005	Initial Development
	--
	
--***************************************************************************************************************
AS
	SELECT	StateMasterID,
			StateCode,
			[Name]

	FROM	dbo.StateMaster
	ORDER BY	StateCode
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.usp_GetVolunteerAcivities 

--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetVolunteerAcivities
	--
	--   	Desc:	Retrieves List of Activities for the Volunteers used in Volunteer SignUpt
	--
	--	Init	Date		Description                    
	--   	------	-------------------	
----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************

AS
	Select	*
	From VolunteerTasks
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE dbo.usp_GetVolunteerTasks 
--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_GetVolunteerTasks
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Retrieves all Volunteer Tasks
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	10/25/2005	Initial Development
	--
	
--***************************************************************************************************************
AS
	SELECT    VolunteerTaskID, TaskDescription
	FROM         dbo.VolunteerTasks
	ORDER BY	TaskDescription
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_InsertChapter
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_InsertChapter
	@ChapterName nvarchar(50),
	@ChapterStateAbbr nvarchar(2),
	@CoordinatorName nvarchar(50),
	@Year int,
	@ContestDate smalldatetime,
	@ContestNotes nvarchar(50),
	@CenterName nvarchar(50),
	@ChapterAddress1 nvarchar(50),
	@ChapterAddress2 nvarchar(50),
	@ChapterCity nvarchar(50),
	@ChapterState nvarchar(50),
	@ChapterZip nvarchar(50),
	@ChapterPhone nvarchar(50),
	@ContactEmail nvarchar(50),
	@RegistrationInstructions nvarchar(200),
	@SponsorName varchar(400),
	@ChapterArea varchar(30),
	@PaymentInfavor varchar(50),
	@PaymentAddress varchar(50),
	@PaymentCity varchar(30),
	@PaymentZip varchar(10),
	@PaymentAddressName varchar(50),
	@PaymentState varchar(10),
	@Status varchar(10),
	@PaymentEmail varchar(50),
	@MapFlag char(5),
	@DeadlineDate smalldatetime,
	@RegfeePenalty float,
	@LatefeeStartdate smalldatetime,
	@MathbeeFlag char(10),
	@ScoresStatus varchar(20),
	@CenterZone varchar(10),
	@ChapterID int OUTPUT
AS

Insert Into dbo.Chapter (
	ChapterName,
	ChapterStateAbbr,
	CoordinatorName,
	Year,
	ContestDate,
	ContestNotes,
	CenterName,
	ChapterAddress1,
	ChapterAddress2,
	ChapterCity,
	ChapterState,
	ChapterZip,
	ChapterPhone,
	ContactEmail,
	RegistrationInstructions,
	SponsorName,
	ChapterArea,
	PaymentInfavor,
	PaymentAddress,
	PaymentCity,
	PaymentZip,
	PaymentAddressName,
	PaymentState,
	Status,
	PaymentEmail,
	MapFlag,
	DeadlineDate,
	RegfeePenalty,
	LatefeeStartdate,
	MathbeeFlag,
	ScoresStatus,
	CenterZone
) Values (
	@ChapterName,
	@ChapterStateAbbr,
	@CoordinatorName,
	@Year,
	@ContestDate,
	@ContestNotes,
	@CenterName,
	@ChapterAddress1,
	@ChapterAddress2,
	@ChapterCity,
	@ChapterState,
	@ChapterZip,
	@ChapterPhone,
	@ContactEmail,
	@RegistrationInstructions,
	@SponsorName,
	@ChapterArea,
	@PaymentInfavor,
	@PaymentAddress,
	@PaymentCity,
	@PaymentZip,
	@PaymentAddressName,
	@PaymentState,
	@Status,
	@PaymentEmail,
	@MapFlag,
	@DeadlineDate,
	@RegfeePenalty,
	@LatefeeStartdate,
	@MathbeeFlag,
	@ScoresStatus,
	@CenterZone
)

SET @ChapterID = @@IDENTITY


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE   PROCEDURE usp_InsertChapterLevelContest
	(@ContestTypeID	int,
	 @NSFChapterID	int,
	 @ContestYear	char(10),
	 @ContestDate	smalldatetime,
	 @ContestCategoryID  int)

--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_InsertChapterLevelContest
	--   Author: Venkat Ambati
	--   Contact: tejasreeja@yahoo.com
	--
	--   	Desc:	Creates a Contest for a given ChapterID
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************
AS 
INSERT INTO dbo.Contest 
	(ContestTypeID,
	NSFChapterID,
	Contest_Year,
	ContestDate,
	ContestCategoryID) 
 
VALUES 
	(@ContestTypeID,
	@NSFChapterID,
	@ContestYear,
	@ContestDate,
	@ContestCategoryID)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE usp_InsertContest
	(@ContestID_1 	[int],
	 @ContestTypeID_2 	[int],
	 @NSFChapterID_3 	[int],
	 @ContestDate_4 	[smalldatetime])

AS INSERT INTO [NSF_Registration].[dbo].[Contest] 
	 ( [ContestID],
	 [ContestTypeID],
	 [NSFChapterID],
	 [ContestDate]) 
 
VALUES 
	( @ContestID_1,
	 @ContestTypeID_2,
	 @NSFChapterID_3,
	 @ContestDate_4)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Wednesday, October 26, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       StoredProcedures.cst
-- Procedure Name: dbo.usp_InsertIndSpouse
-- Date Generated: Wednesday, October 26, 2005
-- Author:         Venkat Ambati
-- Contact:        TAG Workflow Team
-- Organization:   ISD Loans
-- Date Generated: Wednesday, October 26, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_InsertIndSpouse
	@Relationship int,
	@DeleteReason nvarchar(255),
	@DeletedFlag nvarchar(20),
	@CreateDate	smalldatetime,
	@MemberSince smalldatetime,
	@ModifyDate smalldatetime,
	@MailingLabel nvarchar(60),
	@SendReceipt nvarchar(20),
	@LiasonPerson nvarchar(60),
	@ReferredBy nvarchar(60),
	@Chapter nvarchar(60),
	@NewsLetter nvarchar(20),
	@Group2 nvarchar(60),
	@Group3 nvarchar(60),
	@PotentialDonor nvarchar(20),
	@VolunteerFlag nvarchar(20),
	@VolunteerRole1 int,
	@Group1 nvarchar(60),
	@Career nvarchar(100),
	@Employer nvarchar(100),
	@CountryOfOrigin nvarchar(50),
	@Email nvarchar(100),
	@SecondaryEmail nvarchar(100),
	@Education nvarchar(100),
	@Fax nvarchar(30),
	@WPhone nvarchar(30),
	@WFax nvarchar(30),
	@Gender nvarchar(10),
	@HPhone nvarchar(30),
	@CPhone nvarchar(30),
	@State nvarchar(50),
	@Zip nvarchar(10),
	@Country nvarchar(50),
	@Address1 nvarchar(50),
	@Address2 nvarchar(50),
	@City nvarchar(50),
	@FirstName nvarchar(50),
	@MiddleInitial nvarchar(10),
	@LastName nvarchar(50),
	@MemberID nvarchar(50),
	@DonorType nvarchar(30),
	@Title nvarchar(30),
	@StateOfOrigin varchar(50),
	@PrimaryEmail_Old nvarchar(100),
	@HomePhone_Old nvarchar(30),
	@CellPhone_Old nvarchar(30),
	@Address1_Old nvarchar(50),
	@Address2_Old nvarchar(50),
	@City_old nvarchar(50),
	@State_Old nvarchar(50),
	@ZipCode_Old nvarchar(10),
	@PrimaryIndSpouseID int,
	@PrimaryContact nvarchar(30),
	@VolunteerRole2	int,
	@VolunteerRole3	int
	--@AutoMemberID int OUTPUT
AS

Insert Into dbo.tmp_IndSpouse (
	Relationship,
	DeleteReason,
	DeletedFlag,
	CreateDate,
	MemberSince,
	ModifyDate,
	MailingLabel,
	SendReceipt,
	LiasonPerson,
	ReferredBy,
	Chapter,
	NewsLetter,
	Group2,
	Group3,
	PotentialDonor,
	VolunteerFlag,
	VolunteerRole1,
	Group1,
	Career,
	Employer,
	CountryOfOrigin,
	Email,
	SecondaryEmail,
	Education,
	Fax,
	WPhone,
	WFax,
	Gender,
	HPhone,
	CPhone,
	State,
	Zip,
	Country,
	Address1,
	Address2,
	City,
	FirstName,
	MiddleInitial,
	LastName,
	MemberID,
	DonorType,
	Title,
	StateOfOrigin,
	PrimaryEmail_Old,
	HomePhone_Old,
	CellPhone_Old,
	Address1_Old,
	Address2_Old,
	City_old,
	State_Old,
	ZipCode_Old,
	PrimaryIndSpouseID,
	PrimaryContact,
	VolunteerRole2,
	VolunteerRole3
) Values (
	@Relationship,
	@DeleteReason,
	@DeletedFlag,
	@CreateDate,
	@MemberSince,
	@ModifyDate,
	@MailingLabel,
	@SendReceipt,
	@LiasonPerson,
	@ReferredBy,
	@Chapter,
	@NewsLetter,
	@Group2,
	@Group3,
	@PotentialDonor,
	@VolunteerFlag,
	@VolunteerRole1,
	@Group1,
	@Career,
	@Employer,
	@CountryOfOrigin,
	@Email,
	@SecondaryEmail,
	@Education,
	@Fax,
	@WPhone,
	@WFax,
	@Gender,
	@HPhone,
	@CPhone,
	@State,
	@Zip,
	@Country,
	@Address1,
	@Address2,
	@City,
	@FirstName,
	@MiddleInitial,
	@LastName,
	@MemberID,
	@DonorType,
	@Title,
	@StateOfOrigin,
	@PrimaryEmail_Old,
	@HomePhone_Old,
	@CellPhone_Old,
	@Address1_Old,
	@Address2_Old,
	@City_old,
	@State_Old,
	@ZipCode_Old,
	@PrimaryIndSpouseID,
	@PrimaryContact,
	@VolunteerRole2,
	@VolunteerRole3

)

	return @@IDENTITY
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO







CREATE       PROCEDURE	dbo.usp_InsertNewEvent
	(@Contest_Type	int,
	 @Contest_Year 	char(10),
	 @Contest_Date	smalldatetime,
	 @ContestCategoryID	int,
	 @Chapters	varchar(1000))

--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_InsertNewEvent
	--
	--   	Desc:	Insers New Events for the Selected Chapters
	--
	--	Init	Date		Description                    
	--   	------	-------------------	
----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************

AS 
	Create Table	#SelectedChapters (ChapterID int)
	Declare	@ChapterID int
	If (@Chapters <> '')
		Exec Split  '#SelectedChapters', 'ChapterID', @Chapters
	Select ChapterID FROM #SelectedChapters

	Declare chapter_Cursor Cursor FOR  Select ChapterID FROM #SelectedChapters
	Open chapter_Cursor
	Fetch Next From	chapter_Cursor Into 	@ChapterID
	WHILE @@Fetch_Status = 0
		BEGIN
			Exec usp_InsertChapterLevelContest @Contest_Type, @ChapterID, @Contest_Year, @Contest_Date, @ContestCategoryID
			Fetch Next From chapter_Cursor	Into 	@ChapterID
		END
	Close chapter_Cursor
	Deallocate chapter_Cursor 
	Drop Table #SelectedChapters
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS OFF 
GO

CREATE PROCEDURE [dbo].[usp_SelectContestCategoryByYear]
	@ContestYear int  = 0
------------------------------------------------------------------------------------------------------------------------
-- Generated By:   CodeSmith 3.1.2.437
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: [dbo].[usp_SelectIndSpousesAll]
-- Date Generated: Sunday, October 16, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   North South Foundation
-- Date Generated: Sunday, October 16, 2005
------------------------------------------------------------------------------------------------------------------------
AS
IF (@ContestYear = 0)
	BEGIN
		SELECT
			ContestCategoryID,
			ContestCode,
			ContestDesc,
			GradeBased,
			AgeFrom,
			AgeTo,
			GradeFrom,
			GradeTo,
			Status,
			Fee,
			DownloadLink,
			ContestYear
		
		FROM
			[dbo].ContestCategory
		WHERE ContestYear = Year(GetDate()) + 1
	END
ELSE
	BEGIN
		SELECT
			ContestCategoryID,
			ContestCode,
			ContestDesc,
			GradeBased,
			AgeFrom,
			AgeTo,
			GradeFrom,
			GradeTo,
			Status,
			Fee,
			DownloadLink,
			ContestYear
		
		FROM
			[dbo].ContestCategory
		WHERE ContestYear = @ContestYear
	END


--endregion
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_UpdateChapter
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_SelectWhereChapter
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = '
Select	*
From dbo.Chapter
Where	' + @WhereCondition

EXEC sp_executesql @SQL

GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Generated By:   CodeSmith 3.1.2.437
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: [dbo].[usp_SelectWhereIndSpouse]
-- Date Generated: Sunday, October 16, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   North South Foundation
-- Date Generated: Sunday, October 16, 2005
------------------------------------------------------------------------------------------------------------------------


CREATE PROCEDURE dbo.usp_SelectWhereIndSpouse
	@WhereCondition nvarchar(500)
AS

SET NOCOUNT ON

DECLARE @SQL nvarchar(3250)

SET @SQL = '
Select	*
From dbo.tmp_IndSpouse
Where	' + @WhereCondition

EXEC sp_executesql @SQL
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO

------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Saturday, November 05, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       NSF_StoredProcs.cst
-- Procedure Name: dbo.usp_UpdateChapter
-- Date Generated: Saturday, November 05, 2005
-- Author:         Venkat Ambati
-- Contact:        tejasreeja@gmail.com
-- Organization:   NortSouth.org
-- Date Generated: Saturday, November 05, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_UpdateChapter
	@ChapterID int, 
	@ChapterName nvarchar(50), 
	@ChapterStateAbbr nvarchar(2), 
	@CoordinatorName nvarchar(50), 
	@Year int, 
	@ContestDate smalldatetime, 
	@ContestNotes nvarchar(50), 
	@CenterName nvarchar(50), 
	@ChapterAddress1 nvarchar(50), 
	@ChapterAddress2 nvarchar(50), 
	@ChapterCity nvarchar(50), 
	@ChapterState nvarchar(50), 
	@ChapterZip nvarchar(50), 
	@ChapterPhone nvarchar(50), 
	@ContactEmail nvarchar(50), 
	@RegistrationInstructions nvarchar(200), 
	@SponsorName varchar(400), 
	@ChapterArea varchar(30), 
	@PaymentInfavor varchar(50), 
	@PaymentAddress varchar(50), 
	@PaymentCity varchar(30), 
	@PaymentZip varchar(10), 
	@PaymentAddressName varchar(50), 
	@PaymentState varchar(10), 
	@Status varchar(10), 
	@PaymentEmail varchar(50), 
	@MapFlag char(5), 
	@DeadlineDate smalldatetime, 
	@RegfeePenalty float, 
	@LatefeeStartdate smalldatetime, 
	@MathbeeFlag char(10), 
	@ScoresStatus varchar(20), 
	@CenterZone varchar(10) 
AS

Update dbo.Chapter SET
	ChapterName = @ChapterName,
	ChapterStateAbbr = @ChapterStateAbbr,
	CoordinatorName = @CoordinatorName,
	Year = @Year,
	ContestDate = @ContestDate,
	ContestNotes = @ContestNotes,
	CenterName = @CenterName,
	ChapterAddress1 = @ChapterAddress1,
	ChapterAddress2 = @ChapterAddress2,
	ChapterCity = @ChapterCity,
	ChapterState = @ChapterState,
	ChapterZip = @ChapterZip,
	ChapterPhone = @ChapterPhone,
	ContactEmail = @ContactEmail,
	RegistrationInstructions = @RegistrationInstructions,
	SponsorName = @SponsorName,
	ChapterArea = @ChapterArea,
	PaymentInfavor = @PaymentInfavor,
	PaymentAddress = @PaymentAddress,
	PaymentCity = @PaymentCity,
	PaymentZip = @PaymentZip,
	PaymentAddressName = @PaymentAddressName,
	PaymentState = @PaymentState,
	Status = @Status,
	PaymentEmail = @PaymentEmail,
	MapFlag = @MapFlag,
	DeadlineDate = @DeadlineDate,
	RegfeePenalty = @RegfeePenalty,
	LatefeeStartdate = @LatefeeStartdate,
	MathbeeFlag = @MathbeeFlag,
	ScoresStatus = @ScoresStatus,
	CenterZone = @CenterZone
Where
	ChapterID = @ChapterID


GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS ON 
GO


CREATE  PROCEDURE	dbo.usp_UpdateChapterLevelContest
	(@ContestID 	int,
	 @ContestDate 	smalldatetime,
	 @Phase 	int,
	 @StartTime 	nvarchar(50),
	 @EndTime 	nvarchar(50),
	 @Building 	varchar(20),
	 @Room 	varchar(20))

--***************************************************************************************************************
--*                                                        REVISION HISTORY                                                       *
	
--***************************************************************************************************************
	
--***************************************************************************************************************
	--   Name: \NSF\usp_UpdateChapterLevelContest
	--
	--   	Desc:	Updates Chapter Level Event information  to the Contest Table
	--
	--	Init	Date		Description                    
	--   	------	-------------------	----------------------------------------------- 
	--	VA	07/15/2005	Initial Development
	--
	
--***************************************************************************************************************

AS 
	UPDATE	dbo.tmp_Contest 
	SET	ContestDate	 = @ContestDate,
		Phase	 = @Phase,
		StartTime	 = @StartTime,
		EndTime	 = @EndTime,
		Building	 = @Building,
		Room	 = @Room 

	WHERE	(ContestID = @ContestID)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

CREATE PROCEDURE [usp_UpdateContest]
	(@NSFChapterID_1 	[int],
	 @Contest_Year_2 	[char],
	 @ContestID_3 	[int],
	 @ContestTypeID_4 	[int],
	 @NSFChapterID_5 	[int],
	 @Phase_6 	[int],
	 @GroupNo_7 	[int],
	 @BadgeNoBeg_8 	[int],
	 @BadgeNoEnd_9 	[int],
	 @ContestDate_10 	[smalldatetime],
	 @StartTime_11 	[datetime],
	 @EndTime_12 	[datetime],
	 @Building_13 	[varchar](20),
	 @Contest_Year_14 	[char](10),
	 @Room_15 	[varchar](20))

AS UPDATE [NSF_Registration].[dbo].[Contest] 

SET  [ContestID]	 = @ContestID_3,
	 [ContestTypeID]	 = @ContestTypeID_4,
	 [NSFChapterID]	 = @NSFChapterID_5,
	 [Phase]	 = @Phase_6,
	 [GroupNo]	 = @GroupNo_7,
	 [BadgeNoBeg]	 = @BadgeNoBeg_8,
	 [BadgeNoEnd]	 = @BadgeNoEnd_9,
	 [ContestDate]	 = @ContestDate_10,
	 [StartTime]	 = @StartTime_11,
	 [EndTime]	 = @EndTime_12,
	 [Building]	 = @Building_13,
	 [Contest_Year]	 = @Contest_Year_14,
	 [Room]	 = @Room_15 

WHERE 
	( [NSFChapterID]	 = @NSFChapterID_1 AND
	 [Contest_Year]	 = @Contest_Year_2)
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

SET QUOTED_IDENTIFIER ON 
GO
SET ANSI_NULLS OFF 
GO


------------------------------------------------------------------------------------------------------------------------
-- Date Created:   Wednesday, October 26, 2005
-- Generated By:   CodeSmith  2.6.0.117
-- Template:       StoredProcedures.cst
-- Procedure Name: dbo.usp_UpdateIndSpouse
-- Date Generated: Wednesday, October 26, 2005
-- Author:         Venkat Ambati
-- Contact:        TAG Workflow Team
-- Organization:   ISD Loans
-- Date Generated: Wednesday, October 26, 2005

------------------------------------------------------------------------------------------------------------------------

CREATE PROCEDURE dbo.usp_UpdateIndSpouse
	@AutoMemberID int, 
	@Relationship int, 
	@DeleteReason nvarchar(255), 
	@DeletedFlag nvarchar(20), 
	@CreateDate smalldatetime, 
	@MemberSince smalldatetime, 
	@ModifyDate smalldatetime, 
	@MailingLabel nvarchar(60), 
	@SendReceipt nvarchar(20), 
	@LiasonPerson nvarchar(60), 
	@ReferredBy nvarchar(60), 
	@Chapter nvarchar(60), 
	@NewsLetter nvarchar(20), 
	@Group2 nvarchar(60), 
	@Group3 nvarchar(60), 
	@PotentialDonor nvarchar(20), 
	@VolunteerFlag nvarchar(20), 
	@VolunteerRole1 int, 
	@Group1 nvarchar(60), 
	@Career nvarchar(100), 
	@Employer nvarchar(100), 
	@CountryOfOrigin nvarchar(50), 
	@Email nvarchar(100), 
	@SecondaryEmail nvarchar(100), 
	@Education nvarchar(100), 
	@Fax nvarchar(30), 
	@WPhone nvarchar(30), 
	@WFax nvarchar(30), 
	@Gender nvarchar(10), 
	@HPhone nvarchar(30), 
	@CPhone nvarchar(30), 
	@State nvarchar(50), 
	@Zip nvarchar(10), 
	@Country nvarchar(50), 
	@Address1 nvarchar(50), 
	@Address2 nvarchar(50), 
	@City nvarchar(50), 
	@FirstName nvarchar(50), 
	@MiddleInitial nvarchar(10), 
	@LastName nvarchar(50), 
	@MemberID nvarchar(50), 
	@DonorType nvarchar(30), 
	@Title nvarchar(30), 
	@StateOfOrigin varchar(50), 
	@PrimaryEmail_Old nvarchar(100), 
	@HomePhone_Old nvarchar(30), 
	@CellPhone_Old nvarchar(30), 
	@Address1_Old nvarchar(50), 
	@Address2_Old nvarchar(50), 
	@City_old nvarchar(50), 
	@State_Old nvarchar(50), 
	@ZipCode_Old nvarchar(10), 
	@PrimaryIndSpouseID int, 
	@PrimaryContact nvarchar(30),
	@VolunteerRole2 int,
	@VolunteerRole3 int
AS

Update dbo.IndSpouse SET
	Relationship = @Relationship,
	DeleteReason = @DeleteReason,
	DeletedFlag = @DeletedFlag,
	CreateDate = @CreateDate,
	MemberSince = @MemberSince,
	ModifyDate = @ModifyDate,
	MailingLabel = @MailingLabel,
	SendReceipt = @SendReceipt,
	LiasonPerson = @LiasonPerson,
	ReferredBy = @ReferredBy,
	Chapter = @Chapter,
	NewsLetter = @NewsLetter,
	Group2 = @Group2,
	Group3 = @Group3,
	PotentialDonor = @PotentialDonor,
	VolunteerFlag = @VolunteerFlag,
	VolunteerRole1 = @VolunteerRole1,
	Group1 = @Group1,
	Career = @Career,
	Employer = @Employer,
	CountryOfOrigin = @CountryOfOrigin,
	Email = @Email,
	SecondaryEmail = @SecondaryEmail,
	Education = @Education,
	Fax = @Fax,
	WPhone = @WPhone,
	WFax = @WFax,
	Gender = @Gender,
	HPhone = @HPhone,
	CPhone = @CPhone,
	State = @State,
	Zip = @Zip,
	Country = @Country,
	Address1 = @Address1,
	Address2 = @Address2,
	City = @City,
	FirstName = @FirstName,
	MiddleInitial = @MiddleInitial,
	LastName = @LastName,
	MemberID = @MemberID,
	DonorType = @DonorType,
	Title = @Title,
	StateOfOrigin = @StateOfOrigin,
	PrimaryEmail_Old = @PrimaryEmail_Old,
	HomePhone_Old = @HomePhone_Old,
	CellPhone_Old = @CellPhone_Old,
	Address1_Old = @Address1_Old,
	Address2_Old = @Address2_Old,
	City_old = @City_old,
	State_Old = @State_Old,
	ZipCode_Old = @ZipCode_Old,
	PrimaryIndSpouseID = @PrimaryIndSpouseID,
	PrimaryContact = @PrimaryContact,
	VolunteerRole2 = @VolunteerRole2,
	VolunteerRole3 = @VolunteerRole3

Where
	AutoMemberID = @AutoMemberID
GO
SET QUOTED_IDENTIFIER OFF 
GO
SET ANSI_NULLS ON 
GO

