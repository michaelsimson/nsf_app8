if exists (select * from dbo.sysobjects where id = object_id(N'[dbo].[Chapter]') and OBJECTPROPERTY(id, N'IsUserTable') = 1)
drop table [dbo].[Chapter]
GO

CREATE TABLE [dbo].[Chapter] (
	[ChapterID] [int] IDENTITY (1, 1) NOT NULL ,
	[ChapterName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterStateAbbr] [nvarchar] (2) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CoordinatorName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Year] [int] NULL ,
	[ContestDate] [smalldatetime] NULL ,
	[ContestNotes] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CenterName] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterAddress1] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterAddress2] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterCity] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterState] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterZip] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterPhone] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ContactEmail] [nvarchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[RegistrationInstructions] [nvarchar] (200) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[SponsorName] [varchar] (400) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ChapterArea] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentInfavor] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentAddress] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentCity] [varchar] (30) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentZip] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentAddressName] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentState] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[Status] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[PaymentEmail] [varchar] (50) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[MapFlag] [char] (5) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[DeadlineDate] [smalldatetime] NULL ,
	[RegfeePenalty] [float] NULL ,
	[LatefeeStartdate] [smalldatetime] NULL ,
	[MathbeeFlag] [char] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[ScoresStatus] [varchar] (20) COLLATE SQL_Latin1_General_CP1_CI_AS NULL ,
	[CenterZone] [varchar] (10) COLLATE SQL_Latin1_General_CP1_CI_AS NULL 
) ON [PRIMARY]
GO

ALTER TABLE [dbo].[Chapter] ADD 
	CONSTRAINT [PK_ChapterDetails] PRIMARY KEY  CLUSTERED 
	(
		[ChapterID]
	)  ON [PRIMARY] 
GO

