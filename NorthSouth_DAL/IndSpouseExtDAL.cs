using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using Microsoft.ApplicationBlocks.Data;

namespace NorthSouth.DAL
{
	/// <summary>
	/// Summary description for IndSpuseExtDAL.
	/// </summary>
	public class IndSpouseExtDAL:IndSpouseDAL
	{
		public IndSpouseExtDAL(string ConnectionString): base(ConnectionString)
		{

		}

		/// <summary>
		/// Get info based on email.
		/// </summary>
		/// <param name="_email"></param>
		/// <returns></returns>
		public SqlDataReader GetIndSpouseByEmail(string _email)
		{
		
			// SqlDataReader that will hold the returned results		
			SqlDataReader dr = null;
		
			// Call ExecuteReader static method of SqlHelper class that returns a SqlDataReader
			SqlParameter param = new SqlParameter("@Email", SqlDbType.VarChar,100);
			param.Value = _email;

			

			dr = SqlHelper.ExecuteReader(base.ConnectionString, "usp_GetIndSpouseByEmail",param);
				
			return dr;
							
		}

		/// <summary>
		/// Check for duplicates.
		/// </summary>
		/// <param name="_email"></param>
		/// <returns></returns>
		public SqlDataReader CheckIndSpouseDuplicateEmail(string _email, string _lastName, string _address1, string _phone)
		{
		
			// SqlDataReader that will hold the returned results		
			SqlDataReader dr = null;
		
			// Call ExecuteReader static method of SqlHelper class that returns a SqlDataReader
			SqlParameter [] param = new SqlParameter[4];

			param[0] = new SqlParameter("@Email", SqlDbType.Int);
			param[0].Value = _email;
			param[0].Direction = ParameterDirection.Input;

			param[1] = new SqlParameter("@LastName", SqlDbType.NVarChar);
			param[1].Value = _lastName;
			param[1].Direction = ParameterDirection.Input;

			param[2] = new SqlParameter("@Address1", SqlDbType.NVarChar);
			param[2].Value = _address1;
			param[2].Direction = ParameterDirection.Input;

            param[3] = new SqlParameter("@Phone", SqlDbType.NVarChar);
            param[3].Value = _phone;
            param[3].Direction = ParameterDirection.Input;

			dr = SqlHelper.ExecuteReader(base.ConnectionString, "usp_CheckIndSpouseDuplicateEmail",param);
				
			return dr;
							
		}
	}
}
