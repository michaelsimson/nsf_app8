//------------------------------------------------------------------------------
// <autogenerated>
//     This code was generated by CodeSmith.
//
//     Date:    12/17/2005
//     Time:    12:25 AM
//     Version: 2.6.0.117
//     Author:  Venkat Ambati
//     Contact: tejasreeja@gmail.com
//     Organization: North South Foundation
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </autogenerated>
//------------------------------------------------------------------------------

using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;

using Microsoft.ApplicationBlocks.Data;

namespace NorthSouth.DAL
{
	/// <summary>
	/// Summary description for BlogMessage.
	/// </summary>
	public class ContestDAL
	{
		/// <summary>
		/// Database Connection String Property
		/// </summary>
		private string _connectionString;

		/// <summary>
		/// Database Connection String Property
		/// </summary>
		public string ConnectionString
		{
			get {return _connectionString;}
			set {_connectionString = value;}
		}
		/// <summary>
		/// BlogMessageDAL Constructor
		/// </summary>
		/// <param name="ConnectionString"></param>
		public ContestDAL(string ConnectionString)
		{
			this.ConnectionString = ConnectionString;
		}

		/// <summary>
		/// GetConnection Method to Get the Connection String 
		/// </summary>
		/// <returns></returns>
		private SqlConnection GetConnection()
		{
			return new SqlConnection(this.ConnectionString);
		}
		/// <summary>
		/// GetContestByID method returns the Contest Details for a givem _id
		/// </summary>
		/// <param name="MessageID"></param>
		/// <returns></returns>
		public SqlDataReader GetContestByID(int _id)
		{
		
			// SqlDataReader that will hold the returned results		
			SqlDataReader dr = null;
		
			// Call ExecuteReader static method of SqlHelper class that returns a SqlDataReader
			SqlParameter param = new SqlParameter("@ContestID", SqlDbType.Int);
			param.Value = _id;

			dr = SqlHelper.ExecuteReader(this.GetConnection(), "usp_GetContestByID",param);

				
			return dr;
							
		}
		/// <summary>
		/// Delete Contest Method
		/// </summary>
		/// <param name="MessageID"></param>
		public  void DeleteContest(int _id)
		{
			SqlParameter param = new SqlParameter("@ContestID", SqlDbType.Int);
			param.Value = _id;

			SqlHelper.ExecuteNonQuery(this.GetConnection(), "usp_DeleteContest",param);
		}
		/// <summary>
		/// Adds New Contest to Contest Table
		/// </summary>
		/// <returns></returns>
		public int AddContest(int ContestTypeID, int NSFChapterID, int ContestCategoryID, int Phase, int GroupNo, int BadgeNoBeg, int BadgeNoEnd, DateTime ContestDate, string StartTime, string EndTime, string Building, string Contest_Year, string Room, DateTime RegistrationDeadline, string CheckinTime )
		{
			SqlParameter [] param = new SqlParameter[15];
			param[0] = new SqlParameter("@ContestTypeID", SqlDbType.Int);
			param[0].Value = ContestTypeID;
			param[0].Direction = ParameterDirection.Input;

			param[1] = new SqlParameter("@NSFChapterID", SqlDbType.Int);
			param[1].Value = NSFChapterID;
			param[1].Direction = ParameterDirection.Input;

			param[2] = new SqlParameter("@ContestCategoryID", SqlDbType.Int);
			param[2].Value = ContestCategoryID;
			param[2].Direction = ParameterDirection.Input;

			param[3] = new SqlParameter("@Phase", SqlDbType.Int);
			param[3].Value = Phase;
			param[3].Direction = ParameterDirection.Input;

			param[4] = new SqlParameter("@GroupNo", SqlDbType.Int);
			param[4].Value = GroupNo;
			param[4].Direction = ParameterDirection.Input;

			param[5] = new SqlParameter("@BadgeNoBeg", SqlDbType.Int);
			param[5].Value = BadgeNoBeg;
			param[5].Direction = ParameterDirection.Input;

			param[6] = new SqlParameter("@BadgeNoEnd", SqlDbType.Int);
			param[6].Value = BadgeNoEnd;
			param[6].Direction = ParameterDirection.Input;

			param[7] = new SqlParameter("@ContestDate", SqlDbType.DateTime);
			param[7].Value = ContestDate;
			param[7].Direction = ParameterDirection.Input;

			param[8] = new SqlParameter("@StartTime", SqlDbType.NVarChar);
			param[8].Value = StartTime;
			param[8].Direction = ParameterDirection.Input;

			param[9] = new SqlParameter("@EndTime", SqlDbType.NVarChar);
			param[9].Value = EndTime;
			param[9].Direction = ParameterDirection.Input;

			param[10] = new SqlParameter("@Building", SqlDbType.VarChar);
			param[10].Value = Building;
			param[10].Direction = ParameterDirection.Input;

			param[11] = new SqlParameter("@Contest_Year", SqlDbType.Char);
			param[11].Value = Contest_Year;
			param[11].Direction = ParameterDirection.Input;

			param[12] = new SqlParameter("@Room", SqlDbType.VarChar);
			param[12].Value = Room;
			param[12].Direction = ParameterDirection.Input;

			param[13] = new SqlParameter("@RegistrationDeadline", SqlDbType.DateTime);
			param[13].Value = RegistrationDeadline;
			param[13].Direction = ParameterDirection.Input;

			param[14] = new SqlParameter("@CheckinTime", SqlDbType.NVarChar);
			param[14].Value = CheckinTime;
			param[14].Direction = ParameterDirection.Input;


			return Convert.ToInt32(SqlHelper.ExecuteScalar(this.GetConnection(),CommandType.StoredProcedure, "usp_InsertContest",param));
		}
		/// <summary>
		/// Updates the Contest info for a give id
		/// </summary>
		/// <returns></returns>
		public int UpdateContest(int ContestID, int ContestTypeID, int NSFChapterID, int ContestCategoryID, int Phase, int GroupNo, int BadgeNoBeg, int BadgeNoEnd, DateTime ContestDate, string StartTime, string EndTime, string Building, string Contest_Year, string Room, DateTime RegistrationDeadline, string CheckinTime )
		{

			SqlParameter [] param = new SqlParameter[16];
			param[0] = new SqlParameter("@ContestID", SqlDbType.Int);
			param[0].Value = ContestID;
			param[0].Direction = ParameterDirection.Input;

			param[1] = new SqlParameter("@ContestTypeID", SqlDbType.Int);
			param[1].Value = ContestTypeID;
			param[1].Direction = ParameterDirection.Input;

			param[2] = new SqlParameter("@NSFChapterID", SqlDbType.Int);
			param[2].Value = NSFChapterID;
			param[2].Direction = ParameterDirection.Input;

			param[3] = new SqlParameter("@ContestCategoryID", SqlDbType.Int);
			param[3].Value = ContestCategoryID;
			param[3].Direction = ParameterDirection.Input;

			param[4] = new SqlParameter("@Phase", SqlDbType.Int);
			param[4].Value = Phase;
			param[4].Direction = ParameterDirection.Input;

			param[5] = new SqlParameter("@GroupNo", SqlDbType.Int);
			param[5].Value = GroupNo;
			param[5].Direction = ParameterDirection.Input;

			param[6] = new SqlParameter("@BadgeNoBeg", SqlDbType.Int);
			param[6].Value = BadgeNoBeg;
			param[6].Direction = ParameterDirection.Input;

			param[7] = new SqlParameter("@BadgeNoEnd", SqlDbType.Int);
			param[7].Value = BadgeNoEnd;
			param[7].Direction = ParameterDirection.Input;

			param[8] = new SqlParameter("@ContestDate", SqlDbType.DateTime);
			param[8].Value = ContestDate;	
			param[8].Direction = ParameterDirection.Input;

			param[9] = new SqlParameter("@StartTime", SqlDbType.NVarChar);
			param[9].Value = StartTime;
			param[9].Direction = ParameterDirection.Input;

			param[10] = new SqlParameter("@EndTime", SqlDbType.NVarChar);
			param[10].Value = EndTime;
			param[10].Direction = ParameterDirection.Input;

			param[11] = new SqlParameter("@Building", SqlDbType.VarChar);
			param[11].Value = Building;
			param[11].Direction = ParameterDirection.Input;

			param[12] = new SqlParameter("@Contest_Year", SqlDbType.Char);
			param[12].Value = Contest_Year;
			param[12].Direction = ParameterDirection.Input;

			param[13] = new SqlParameter("@Room", SqlDbType.VarChar);
			param[13].Value = Room;
			param[13].Direction = ParameterDirection.Input;

			param[14] = new SqlParameter("@RegistrationDeadline", SqlDbType.DateTime);
			param[14].Value = RegistrationDeadline;
			param[14].Direction = ParameterDirection.Input;

			param[15] = new SqlParameter("@CheckinTime", SqlDbType.NVarChar);
			param[15].Value = CheckinTime;
			param[15].Direction = ParameterDirection.Input;


			return Convert.ToInt32(SqlHelper.ExecuteScalar(this.GetConnection(),CommandType.StoredProcedure, "usp_UpdateContest",param ));

		}
		/// <summary>
		/// Get All the Contest  from table Contest
		/// </summary>
		/// <param name="DataSet"></param>
		/// <returns></returns>
		public DataSet GetAllContests(DataSet ds)
		{
			string [] tableName = new string[1];
			tableName[0] = "Contest";
			SqlHelper.FillDataset(this.GetConnection(),CommandType.StoredProcedure,"usp_GetContestAll",ds,tableName);
			return ds;
		}

		/// <summary>
		/// Get All Contest from table Contest with Where Condition 
		/// </summary>
		/// <param name="DataSet"></param>
		/// <returns></returns>
		public DataSet SearchContestWhere(DataSet ds, string whereCondition)
		{
			string [] tableName = new string[1];
			tableName[0] = "Contest";
			SqlParameter [] param = new SqlParameter[1];
			param[0] = new SqlParameter("@WhereCondition", SqlDbType.NVarChar);
			param[0].Value = whereCondition;
			param[0].Direction = ParameterDirection.Input;
			SqlHelper.FillDataset(this.GetConnection(),"usp_SelectWhereContest",ds,tableName,param);
			return ds;
		}
		
		
	}
}
