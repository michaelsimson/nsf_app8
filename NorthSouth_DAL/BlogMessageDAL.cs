using System;
using System.Data;
using System.Data.SqlClient;

using Microsoft.ApplicationBlocks.Data;

namespace NorthSouth.DAL
{
		/// <summary>
		/// Summary description for BlogMessage.
		/// </summary>
		public class BlogMessageDAL
		{
			/// <summary>
			/// <Blog_Messages MessageID="1" Title="ASPNET" Message="Some Test" AddedDate="2005-01-01T00:00:00"/>
			/// </summary>
			private string _connectionString;

			/// <summary>
			/// Database Connection String Property
			/// </summary>
			public string ConnectionString
			{
				get {return _connectionString;}
				set {_connectionString = value;}
			}
			/// <summary>
			/// BlogMessageDAL Constructor
			/// </summary>
			/// <param name="ConnectionString"></param>
			public BlogMessageDAL(string ConnectionString)
			{
				this.ConnectionString = ConnectionString;
			}
			
			/// <summary>
			/// GetConnection Method to Get the Connection String 
			/// </summary>
			/// <returns></returns>
			private SqlConnection GetConnection()
			{
				return new SqlConnection(this.ConnectionString);
			}
			
			/// <summary>
			/// GetBlogMessageByID method returns the Message Details for a givem MessageID
			/// </summary>
			/// <param name="MessageID"></param>
			/// <returns></returns>
			public SqlDataReader GetBlogMessageByID(int MessageID)
			{
			
				// SqlDataReader that will hold the returned results		
				SqlDataReader drBlogMesage = null;
			
				// Call ExecuteReader static method of SqlHelper class that returns a SqlDataReader
				SqlParameter paramMessageID = new SqlParameter("@MessageID", SqlDbType.Int);
				paramMessageID.Value = MessageID;

				drBlogMesage = SqlHelper.ExecuteReader(this.GetConnection(), "usp_GetBlogMessageByID",paramMessageID);

    					
				return drBlogMesage;
								
			}	
			/// <summary>
			/// Delete BlogMessage Method
			/// </summary>
			/// <param name="MessageID"></param>
			public  void DeleteBlogMessage(int MessageID)
			{
				SqlParameter paramMessageID = new SqlParameter("@MessageID", SqlDbType.Int);
				paramMessageID.Value = MessageID;

				SqlHelper.ExecuteNonQuery(this.GetConnection(), "usp_DeleteBlogMessage",paramMessageID);
			
			}
			/// <summary>
			/// Adds New Blog Message to Blog_Message Table
			/// </summary>
			/// <param name="Title"></param>
			/// <param name="Message"></param>
			/// <param name="AddedDate"></param>
			/// <returns></returns>
			public int AddBlogMessage(string Title, string Message, DateTime AddedDate)
			{
				SqlParameter [] param = new SqlParameter[3];
	
				param[0] = new SqlParameter("@Title", SqlDbType.VarChar);
				param[0].Value = Title;
				param[0].Direction = ParameterDirection.Input;
	
				param[1] = new SqlParameter("@Message", SqlDbType.VarChar);
				param[1].Value = Message;
				param[1].Direction = ParameterDirection.Input;
			
				param[2] = new SqlParameter("@AddedDate", SqlDbType.SmallDateTime);
				param[2].Value = AddedDate;
				param[2].Direction = ParameterDirection.Input;

				return Convert.ToInt32(SqlHelper.ExecuteScalar(this.GetConnection(),CommandType.StoredProcedure, "usp_InsertBlogMessage",param));
				//return  Convert.ToInt32(param[3].Value);
			}
			/// <summary>
			/// Updates the Blog Message info for a give id
			/// </summary>
			/// <param name="MessageID"></param>
			/// <param name="Title"></param>
			/// <param name="Message"></param>
			/// <param name="AddedDate"></param>
			/// <returns></returns>
			public int UpdateBlogMessage(int MessageID,string Title, string Message, DateTime AddedDate)
			{

				SqlParameter [] param = new SqlParameter[4];
	
				param[0] = new SqlParameter("@MessageID", SqlDbType.Int);
				param[0].Value = MessageID;
				param[0].Direction = ParameterDirection.Input;

				param[1] = new SqlParameter("@Title", SqlDbType.VarChar);
				param[1].Value = Title;
				param[1].Direction = ParameterDirection.Input;
	
				param[2] = new SqlParameter("@Message", SqlDbType.VarChar);
				param[2].Value = Message;
				param[2].Direction = ParameterDirection.Input;
			
				param[3] = new SqlParameter("@AddedDate", SqlDbType.SmallDateTime);
				param[3].Value = AddedDate;
				param[3].Direction = ParameterDirection.Input;

				return Convert.ToInt32(SqlHelper.ExecuteScalar(this.GetConnection(),CommandType.StoredProcedure, "usp_UpdateBlogMessage",param ));

			}
			/// <summary>
			/// Get All the BlogMessages in the Table
			/// </summary>
			/// <param name="dsBlogMessages"></param>
			/// <returns></returns>
			public DataSet GetAllBlogMessages(DataSet dsBlogMessages)
			{
				string [] tableName = new string[1];
				tableName[0] = "BlogMessages";
				SqlHelper.FillDataset(this.GetConnection(),CommandType.StoredProcedure,"usp_GetAllBlogMessages",dsBlogMessages,tableName);
				return dsBlogMessages;
			}


		}
	}



