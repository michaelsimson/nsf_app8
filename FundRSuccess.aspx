﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRSuccess.aspx.vb" Inherits="VRegistration.FundRSuccess" title="Fundraising Success" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table cellSpacing="0" cellPadding="0" border="0">
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><asp:label id="lblDonationMessage" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><asp:label id="lblMessage" runat="server"></asp:label></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
    
				<TR>
					<TD class="largewordingbold"><asp:label id="lblEmailStatus" runat="server"></asp:label></TD>
				</TR>

 <tr>
            <td><b>Adults attending: </b><asp:Label ID="lblAdult" runat="server" Text="0"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Children attending:</b> <asp:Label ID="lblChildren" runat="server" Text="0"></asp:Label>
</td>
        </tr>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"><FONT color="#ff0000" size="4">Please Do Not use Back 
							Button on Browser to avoid Duplicate Charges to your Credit Card.</FONT></TD>
				</TR>
				<TR>
					<TD class="largewordingbold"></TD>
				</TR>
				<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"></td>
				</tr>
			</table>
			
			<table width="100%">
				<tr>
					<TD class="largewordingbold" align="center"><FONT color="#cc3300" size="4"><STRONG></STRONG></FONT></TD>
					<td class="Content">
					</td>
				</tr>
				<tr>
					<td>
						<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="FundRstatus.aspx">View Fundraising Status</asp:hyperlink>
					</td>
				</tr>
			</table>
</asp:Content>

