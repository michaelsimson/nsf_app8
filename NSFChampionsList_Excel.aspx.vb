﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Reflection
Imports NativeExcel
Imports iTextSharp

Partial Class NSFChampionsList
    Inherits System.Web.UI.Page

    Protected Sub btn_Click(sender As Object, e As EventArgs) Handles btn.Click
        Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim i As Integer
        Dim j As Integer = 0
        Dim ds As DataSet
        Dim sheet As IWorksheet = book.Worksheets.Add()
        sheet.Cells("A2").Value = "JSB_REG_WORDS"
        sheet.Cells("B2").Value = "Level"
        sheet.Cells("C2").Value = "SubLevel"
        sheet.Name = "JSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from USSAward")

        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet1 As IWorksheet = book.Worksheets.Add()
        sheet1.Cells("A2").Value = "JSB_NAT_WORDS"
        sheet1.Cells("B2").Value = "Level"
        sheet1.Cells("C2").Value = "SubLevel"
        sheet1.Name = "JSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from USSAward")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet1.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet1.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet1.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet2 As IWorksheet = book.Worksheets.Add()
        sheet2.Cells("A2").Value = "SSB_REG_WORDS"
        sheet2.Cells("B2").Value = "Level"
        sheet2.Cells("C2").Value = "SubLevel"
        sheet2.Name = "SSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from USSAward ")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet2.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet2.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet2.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet3 As IWorksheet = book.Worksheets.Add()
        sheet3.Cells("A2").Value = "SSB_NAT_WORDS"
        sheet3.Cells("B2").Value = "Level"
        sheet3.Cells("C2").Value = "SubLevel"
        sheet3.Name = "SSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from USSAward")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet3.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet3.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet3.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]

        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]

        'Stream workbook  
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=SBPubWords_.xls")
        book.SaveAs(Response.OutputStream)
        Response.End()
    End Sub
End Class
