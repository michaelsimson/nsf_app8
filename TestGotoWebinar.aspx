﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestGotoWebinar.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="TestGotoWebinar" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link href="css/ClassCal.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="css/HtmlGridTable.css" rel="stylesheet" />
    <link href="css/GridTable.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>

    <%-- <link href="css/BeatPicker.min.css" rel="stylesheet" />
    <script src="js/BeatPicker.min.js"></script>--%>

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/Loader.css" rel="stylesheet" />
    <script src="js/jquery.toast.js"></script>
    <link href="css/jquery.toast.css" rel="stylesheet" />

    <%--  <link href="css/Loader.css" rel="stylesheet" />--%>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script src="js/ezmodal.js"></script>

    <div>
        <%-- <link href="css/dcalendar.picker.css" rel="stylesheet" />
        <script src="js/dcalendar.picker.js"></script>--%>
        <style type="text/css">
            @-webkit-keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @-moz-keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @-o-keyframes invalid {
                from; {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            @keyframes invalid {
                from {
                    background-color: red;
                }

                to {
                    background-color: inherit;
                }
            }

            .invalid {
                -webkit-animation: invalid 1s infinite; /* Safari 4+ */
                -moz-animation: invalid 1s infinite; /* Fx 5+ */
                -o-animation: invalid 1s infinite; /* Opera 12+ */
                animation: invalid 1s infinite; /* IE 10+ */
            }

            .clear {
                clear: both;
                margin-bottom: 10px;
            }

            label {
                font-weight: bold;
            }
        </style>

        <style type="text/css">
            .custom_modal {
                width: auto !important;
            }

            .ac-wrapper {
                position: fixed;
                top: 0;
                left: 0;
                width: 100%;
                height: 100%;
                background: rgba(255,255,255,.6);
                z-index: 1001;
            }

            .popup {
                width: 480px;
                height: 250px;
                background: #FFFFFF;
                border: 2px solid #8CC403;
                border-radius: 15px;
                -moz-border-radius: 15px;
                -webkit-border-radius: 15px;
                box-shadow: #8CC403 0px 0px 3px 3px;
                -moz-box-shadow: #8CC403 0px 0px 3px 3px;
                -webkit-box-shadow: #8CC403 0px 0px 3px 3px;
                position: relative;
                top: 150px;
                left: 450px;
            }

            .Seacrh {
                color: grey;
            }

                .Seacrh:hover {
                    color: blue;
                }

            /*.qTipWidth {
            max-width: 900px !important;
        }*/
        </style>
        <script type="text/javascript">

            var arrJoinURl = "";
            var arrRegKey = "";
            var arrOrganizer = [];
            var accessToken;
            var orgAnizerKey;
            var option = "Organizer";
            $(function (e) {

                $('#dvStartDate').datepicker({
                    format: 'mm-dd-yyyy',
                    autoclose: true,
                });

                $('#dvEndDate').datepicker({
                    format: 'mm-dd-yyyy',
                    autoclose: true,
                });
                accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
                orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;
                listEvents();

                listWebinars();
                getWebinars();
                // getWebinarBasedOnIDFromNSF();

               //registerAttendees("6056502962207313675");

                var onlineWsCalId = getParameterByName("coachID");
            });

            function getWebinars() {
                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'GET',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    success: function (data) {

                        $.each(data, function (index, value) {


                        })
                    },
                    error: function () {
                        //  alert("Cannot get data");
                    }
                });
            }

            function createRegistrants(webinarkey, firstName, lastName, email, source, address, city, state, zipCode, country, phone) {
                var jsonObject = { "firstName": firstName, "lastName": lastName, "email": email, "source": source, "address": address, "city": city, "state": state, "zipCode": zipCode, "Country": country, "phone": phone };
                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarkey + "/registrants",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    data: JSON.stringify(jsonObject),
                    success: function (data) {
                        //alert(JSON.stringify(data));
                        $.each(data, function (index, value) {
                            //alert(value.JoinURL);
                            //updateJoinURLToAttendees();
                            //$("#hdnJoinURL").val(value.JoinURL);
                            //$("#hdnRegistrantKey").val(value.RegistrantKey);
                        });

                    },
                    error: function () {
                        //  alert("Cannot get data");
                    }
                });
            }

            function createWebinar() {
                var webinarKey;
                var title = $("#txtWebinarTitle").val();

                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();
                // var endDate = $("#txtEndDate").val();
                // var endTime = $("#selEndTime").val();
                var sessionType = $("#selSessionType").val();
                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();

                var startUnivTime = $("#hdnUTCStartTime").val();

                var endUnivTime = $("#hdnUTCEndTime").val();

                var jsonObject = {
                    "subject": title, "description": title, "times": [{ "startTime": startUnivTime, "endTime": endUnivTime }], "timeZone": "America/New_York", "type": "single_session",
                    "isPasswordProtected": false
                };

                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    data: JSON.stringify(jsonObject),
                    success: function (data) {

                        $.each(data, function (index, value) {

                            webinarKey = value;

                        });
                        postWebinarToNSF(webinarKey, "1")
                    },
                    error: function () {
                        // alert("Cannot get data");
                    }
                });


            }
            $(document).on("click", "#btnScheduleWebinar", function (e) {
                var webinarkey = $("#hdnWebinarkey").val();


                if ($("#btnScheduleWebinar").val() == "Update Webinar") {
                    if (validateWebinar() > 0) {

                        checkTimeConfictsOfWebinar();
                    }

                } else {
                    if (validateWebinar() > 0) {
                        checkTimeConfictsOfWebinar();

                    }
                }
                //createWebinar();

            });
            function postWebinarToNSF(webinarKey, mode) {

                var title = $("#txtWebinarTitle").val();

                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();
                //  var endDate = $("#txtEndDate").val();
                //   var endTime = $("#selEndTime").val();
                var sessionType = $("#selSessionType").val();
                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();
                var duration = $("#selDuration").val();
                var eventid = $("#selEvent").val();



                var jsonData = JSON.stringify({ ObjWebinar: { WebinarKey: webinarKey, EventYear: eventYear, ProductGroupId: pgId, ProductId: prdId, TeacherId: teacherId, Title: title, StartDate: startDate, StartTime: startTime, SessionType: sessionType, Mode: mode, EventId: eventid, Duration: duration } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/PostNewWebinar",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) > 0) {
                            registerAttendees(webinarKey);
                            if (mode == "1") {
                                statusMessage("Webinar created successfully!", "success");
                                $("#hdnWebinarkey").val(webinarKey);
                                arrOrganizer.push(teacherId);
                                assignCoOrganizer();
                                listWebinars();
                                clear();
                            } else if (mode == "2") {
                                updateWebinar(webinarKey);
                                statusMessage("Webinar updated successfully", "success");
                                $("#btnScheduleWebinar").val("Create Webinar");
                                clear();
                                $("#btnCancelWebinar").val("Reset");
                                listWebinars();
                            }
                        }
                    }, failure: function (e) {

                    }
                });

            }

            function registerAttendees(webinarKey) {


                var title = $("#txtWebinarTitle").val();

                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();
                // var endDate = $("#txtEndDate").val();
                // var endTime = $("#selEndTime").val();
                var sessionType = $("#selSessionType").val();
                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();

                //teacherId = 82296;
                //eventYear = 2018;
                //pgId = 55;
                //prdId = 152;

                var jsonData = JSON.stringify({ objWeniar: { TeacherId: teacherId, EventYear: eventYear, ProductGroupId: pgId, ProductId: prdId, WebinarKey: webinarKey, Source: "" } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListAttendees",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var onlineWsCalId;
                        var previousemail = "";
                        $.each(data.d, function (index, value) {

                            var firstName = value.FirstName
                            var lastName = value.LastName;
                            var email = value.Email;
                            var source = "NSF";
                            var address = value.Address1;
                            var city = value.City;
                            var state = value.State;
                            var zipCode = value.Zip;
                            var country = value.Country;
                            var phone = value.Phone;
                            onlineWsCalId = value.OnlineWsCalID;
                            var spouseEmail = value.SpouseEmail;
                            var secondaryEmail = value.SecondaryEmail;

                            var regId = value.RegId;
                            $("#hdnOnlineWsCalid").val(onlineWsCalId);
                            if (email == previousemail) {
                                email = secondaryEmail;
                                if (email == secondaryEmail) {
                                    email = spouseEmail;
                                }
                            }
                            previousemail = value.Email;

                            var jsonObject = { "firstName": firstName, "lastName": lastName, "email": email, "source": source, "address": address, "city": city, "state": state, "zipCode": zipCode, "Country": country, "phone": phone };
                            $.ajax({

                                url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/registrants",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization", accessToken);
                                },
                                type: 'POST',
                                crossDomain: true,
                                dataType: 'json',
                                contentType: 'application/json',
                                processData: false,
                                data: JSON.stringify(jsonObject),
                                success: function (data) {

                                    var obj = $.parseJSON(JSON.stringify(data));
                                    arrJoinURl += obj.joinUrl + ",";
                                    arrRegKey += obj.registrantKey + ",";

                                    updateJoinURLToAttendees(obj.joinUrl, obj.registrantKey, regId)

                                },
                                error: function (data) {
                                    var obj = $.parseJSON(JSON.stringify(data.responseJSON));

                                    updateJoinURLToAttendees(obj.joinUrl, obj.registrantKey, regId);
                                    // alert("Cannot get data");
                                }
                            });

                            // createRegistrants(webinarKey, firstName, lastName, email, source, address, city, state, zipCode, country, phone);
                        });
                        listWebinars();
                    }, failure: function (e) {

                    }
                });
            }

            function listWebinars() {


                var eventyear = $("#selEventyear").val();

                var jsonData = JSON.stringify({ objWeniar: { EventYear: eventyear } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListWebinarSessions",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Organizer</th>";
                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Event Year</th>";
                        tblHtml += "<th>Event</th>";
                        tblHtml += "<th>Product group</th>";
                        tblHtml += " <th>Product</th>";
                        tblHtml += "<th>Title</th>";

                        tblHtml += "<th>Co-Organizer</th>";
                        tblHtml += "<th>Start Date</th>";
                        tblHtml += "<th>Start Time (EST)</th>";
                        tblHtml += "<th>Duration (Hrs)</th>";

                        //  tblHtml += "<th>Session Type</th>";
                        tblHtml += "</tr>";
                        tblHtml += "</thead>";


                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {


                                tblHtml += "<tr>";
                                var deleteClass = "delete";
                                var deleteColor = "red";
                                var deleteCurso = "pointer";
                                var deleteTitle = "Delete";




                                tblHtml += '<td><div style="float:left;"><a class="assign" attr-webinarId=' + value.WebinarKey + '  title="Assign Organizer" style="cursor:pointer;"><i style="color:blue;" class="fa fa-user-plus fa-2x" aria-hidden="true"></i></a> </div> <div style="float:left; position:relative; left: 5px;"><a class="viewOrg" attr-webinarId=' + value.WebinarKey + ' title="View Organizer" style="cursor:pointer;"><i style="color:blue;" class="fa fa-eye fa-2x" aria-hidden="true"></i></a> </div></td>';

                                tblHtml += '<td><div style="float:left;"><a class="editWebinar" attr-webinarId=' + value.WebinarKey + '  title="Edit Webinar" style="cursor:pointer;"><i style="color:blue;" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div> <div style="float:left; position:relative; left: 5px;"><a class="deleteWebinar" attr-webinarId=' + value.WebinarKey + ' title="Delete Webinar" style="cursor:pointer;"><i style="color:blue;" class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a> </div></td>';

                                tblHtml += "<td>" + value.EventYear + "</td>";
                                tblHtml += "<td>" + value.EventId + "</td>";
                                tblHtml += "<td>" + value.ProductGroupCode + "</td>";
                                tblHtml += "<td>" + value.ProductCode + "</td>";
                                tblHtml += "<td>" + value.Title + "</td>";

                                tblHtml += "<td>" + value.TeacherName + "</td>";
                                tblHtml += "<td>" + value.StartDate + "</td>";
                                tblHtml += "<td>" + value.StartTime + "</td>";
                                tblHtml += "<td>" + value.Duration + "</td>";
                                //  tblHtml += "<td>" + value.EndTime + "</td>";
                                //   tblHtml += "<td>" + value.SessionType + "</td>";
                                tblHtml += "</tr>";


                            });

                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='12' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";



                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);

                    },
                    failure: function (response) {

                    }
                });
            }

            $(document).on("click", ".assign", function (e) {
                option = "Organizer";
                var webinarKey = $(this).attr("attr-webinarId");
                $("#hdnWebinarkey").val(webinarKey);
                $("#dvSearchNSFMember").css("display", "block");
            });

            $(document).on("click", ".SearchTeacher", function (e) {
                option = "Teacher";
                $("#dvSearchNSFMember").css("display", "block");
            });

            function updateJoinURLToAttendees(joinURL, registrantKey, regId) {

                //var pgId = $("#selProductGroup").val();
                //var prdId = $("#selProduct").val();
                //var eventyear = $("#selEventyear").val();
                var jsonData = JSON.stringify({ ObjWebinar: { JoinURL: joinURL, RegistrantKey: registrantKey, RegId: regId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/UpdateJoinURLToAttendees",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) > 0) {
                            listWebinars();
                        }
                    }, failure: function (e) {

                    }
                });
            }

            function listProductGroups(eventId) {
                var eventyear = $("#selEventyear").val();

                $("#selProductGroup").empty();

                var jsonData = JSON.stringify({ objWeniar: { EventYear: eventyear, EventId: eventId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListProductGroup",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var productGroupID;
                        if (JSON.stringify(data.d.length) > 0) {

                            $("#selProductGroup").append($("<option></option>").val
                                ("0").html("Select"));

                            $.each(data.d, function (index, value) {
                                $("#selProductGroup").append($("<option></option>").val
                                    (value.ProductGroupId).html(value.ProductGroupCode));
                                productGroupID = value.ProductGroupId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProductGroup").removeAttr("disabled");
                                if ($("#hdnPgId").val() != "") {
                                    $("#selProductGroup").val($("#hdnPgId").val());
                                    listProducts();
                                }

                            } else {
                                $("#selProductGroup").val(productGroupID);
                                $("#selProductGroup").attr("disabled", "disabled");
                                listProducts();
                            }
                        }
                    }, failure: function (e) {

                    }
                });
            }

            function listProducts() {

                var eventyear = $("#selEventyear").val();
                var productgroupId = $("#selProductGroup").val();
                $("#selProduct").empty();

                var jsonData = JSON.stringify({ objWeniar: { EventYear: eventyear, ProductGroupId: productgroupId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListProduct",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var productId;
                        if (JSON.stringify(data.d.length) > 0) {

                            $("#selProduct").append($("<option></option>").val
                                ("0").html("Select"));

                            $.each(data.d, function (index, value) {
                                $("#selProduct").append($("<option></option>").val
                                    (value.ProductId).html(value.ProductCode));
                                productId = value.ProductId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProduct").removeAttr("disabled");

                                if ($("#hdnPrdid").val() != "") {
                                    $("#selProduct").val($("#hdnPrdid").val());
                                    listTeachers();
                                }

                                getWebinarTitle();
                                getWebinarStartDateAndEndDate();

                            } else {
                                $("#selProduct").val(productId);
                                $("#selProduct").attr("disabled", "disabled");
                                listTeachers();
                            }
                        }

                    }, failure: function (e) {

                    }
                });
            }

            function listTeachers() {
                var eventyear = $("#selEventyear").val();
                var productId = $("#selProduct").val();
                var productgroupid = $("#selProductGroup").val();
                var eventid = $("#selEvent").val();
                $("#selCoach").empty();

                var jsonData = JSON.stringify({ objWeniar: { EventYear: eventyear, ProductId: productId, ProductGroupId: productgroupid, EventId: eventid } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListTeacher",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            $("#selCoach").append($("<option></option>").val
                                ("0").html("Select"));

                            $.each(data.d, function (index, value) {
                                $("#selCoach").append($("<option></option>").val
                                    (value.TeacherId).html(value.TeacherName));
                                teacherid = value.TeacherId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selCoach").removeAttr("disabled");

                                if ($("#hdnteacherid").val() != "") {
                                    $("#selCoach").val($("#hdnteacherid").val());
                                }


                            } else {
                                $("#selCoach").val(teacherid);
                                $("#selCoach").attr("disabled", "disabled");

                            }
                        }
                    }, failure: function (e) {

                    }
                });

            }
            $(document).on("change", "#selProductGroup", function (e) {
                if (parseInt($(this).val()) > 0) {
                    listProducts();
                    listTeachers();
                    GetWebinarTitle();
                }
            });
            $(document).on("change", "#selProduct", function (e) {
                if (parseInt($(this).val()) > 0) {
                    listTeachers();
                    getWebinarTitle();
                    getWebinarStartDateAndEndDate();
                }
            });
            function statusMessage(message, type) {

                $.toast({
                    // heading: 'Can I add <em>icons</em>?',
                    text: '<b>' + message + '</b>',
                    icon: type,
                    position: 'top-center',
                    hideAfter: 12000,
                    stack: 1

                })


            }


            function ListNSFMembers() {


                var firstName = $("#txtFirstName").val();
                var lastName = $("#txtLastName").val();
                var email = $("#txtEmail").val();

                var jsonData = JSON.stringify({ ObjAttendees: { FirstName: firstName, LastName: lastName, Email: email } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListNSFMembers",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Name</th>";
                        tblHtml += "<th>Email</th>";
                        tblHtml += " <th>Login</th>";
                        tblHtml += "<th>DonorType</th>";
                        tblHtml += "<th>Cphone</th>";
                        tblHtml += "<th>Hphone</th>";
                        tblHtml += "<th>Address</th>";
                        tblHtml += "<th>City</th>";
                        tblHtml += "<th>State</th>";
                        tblHtml += "<th>Chapter</th>";
                        tblHtml += "</tr>";
                        tblHtml += "</thead>";


                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {


                                tblHtml += "<tr>";

                                if (option == "Organizer") {

                                    tblHtml += '<td><div style="float:left;"> <input type="checkbox" class="chkOrgainzer"  attr-memberId=' + value.AutoMemberId + ' /></div></td>';
                                } else if (option == "Teacher") {

                                    tblHtml += '<td><div style="float:left;"> <input type="button" class="btnTeacher"  attr-memberId=' + value.AutoMemberId + ' value="Select" /></div></td>';
                                }

                                tblHtml += "<td>" + value.Name + "</td>";
                                tblHtml += "<td>" + value.Email + "</td>";
                                tblHtml += "<td>" + value.Login + "</td>";
                                tblHtml += "<td>" + value.DonorType + "</td>";
                                tblHtml += "<td>" + value.CPhone + "</td>";
                                tblHtml += "<td>" + value.HPhone + "</td>";
                                tblHtml += "<td>" + value.Address + "</td>";
                                tblHtml += "<td>" + value.City + "</td>";
                                tblHtml += "<td>" + value.State + "</td>";
                                tblHtml += "<td>" + value.Chapter + "</td>";

                                tblHtml += "</tr>";


                            });

                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";



                        }
                        tblHtml += "</tbody>";
                        $("#tblNSFMember").html(tblHtml);

                    },
                    failure: function (response) {

                    }
                });
            }

            $(document).on("click", "#btnSearch", function (e) {
                var firstName = $("#txtFirstName").val();
                var lastName = $("#txtLastName").val();
                var email = $("#txtEmail").val();
                if (firstName != "" || lastName != "" || email != "") {
                    $("#dvNSftable").css("display", "block");
                    if (option == "Teacher") {
                        $("#btnAssignCoORg").css("display", "none");
                    } else if (option == "Organizer") {
                        $("#btnAssignCoORg").css("display", "block");
                    }
                    ListNSFMembers();
                } else {
                    statusMessage("Please Enter Email (or) First Name (or) Last Name", "error");
                }
            });

            $(document).on("click", ".chkOrgainzer", function (e) {
                var memberId;


                if ($(this).prop("checked") == true) {

                    var webinarKey = $("#hdnWebinarkey").val();
                    memberId = $(this).attr("attr-memberId");
                    checkDuplicateOrganizers(webinarKey, memberId);




                } else {

                    arrOrganizer.splice($.inArray(memberId, arrOrganizer), 1);

                }
            });

            $(document).on("click", "#btnAssignCoORg", function (e) {
                if (arrOrganizer.length > 0) {
                    assignCoOrganizer();
                    statusMessage("Co-organizer created successfully!", "success");
                    $("#dvNSftable").css("display", "none");
                    $("#dvSearchNSFMember").css("display", "none");
                } else {
                    statusMessage("Please select co-orgainzer(s)", "error");
                }
            });



            function assignCoOrganizer() {


                var webinarKey = $("#hdnWebinarkey").val();
                var memberids = "";

                for (var i = 0; i < arrOrganizer.length; i++) {
                    memberids += arrOrganizer[i] + ",";
                }

                var jsonData = JSON.stringify({ ObjAttendees: { AutoMemberId: memberids, Webinarkey: webinarKey } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListOrganizer",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        $.each(data.d, function (index, value) {
                            var name = value.Name;
                            var email = value.Email;
                            var webinarKey = value.Webinarkey;
                            var memberId = value.AutoMemberId;

                            var jsonObject = [{
                                "external": true, "organizerKey": memberId, "givenName": name, "email": email
                            }];

                            $.ajax({
                                url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/coorganizers",
                                beforeSend: function (xhr) {
                                    xhr.setRequestHeader("Authorization", accessToken);
                                },
                                type: 'POST',
                                crossDomain: true,
                                dataType: 'json',
                                contentType: 'application/json',
                                processData: false,
                                data: JSON.stringify(jsonObject),
                                success: function (data) {

                                    var obj = $.parseJSON(JSON.stringify(data));
                                    var joinLink = obj[0].joinLink;
                                    var memberKey = obj[0].memberKey
                                    var loginID = document.getElementById("<%=hdnLoginId.ClientID%>").value;
                                    inserCoorganizer(webinarKey, memberKey, joinLink, loginID, memberId);
                                },
                                error: function () {
                                    //  alert("Cannot get data");
                                }
                            });

                        });

                    },
                    failure: function (response) {

                    }
                });
            }

            function assignCoorganiserToWebinar(name, email, webinarKey, memeberId) {



                var jsonObject = {
                    "external": true, "organizerKey": memeberId, "givenName": name, "email": email
                };

                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/coorganizers",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'POST',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    data: JSON.stringify(jsonObject),
                    success: function (data) {


                    },
                    error: function () {
                        //  alert("Cannot get data");
                    }
                });


            }

            $(document).on("click", "#btnClose", function (e) {
                $("#dvNSftable").css("display", "none");
                $("#dvSearchNSFMember").css("display", "none");
            });

            function inserCoorganizer(webinarKey, memberKey, joinUrl, loginId, memberId) {

                var jsonData = JSON.stringify({ ObjAttendee: { Webinarkey: webinarKey, JoinURL: joinUrl, RegistrantKey: memberKey, LoginId: loginId, AutoMemberId: memberId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/InsertCoOrganizers",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) > 0) {

                        }
                    }, failure: function (e) {

                    }
                });
            }

            $(document).on("click", ".viewOrg", function (e) {
                $("#hdnWebinarkey").val($(this).attr("attr-webinarid"));
                listOrganizersFromNSF($(this).attr("attr-webinarid"));
                $(".btnPopUP").trigger("click");
                $(".ezmodal-container").css("top", "30%");
            });



            function listOrganizersFromNSF(webinarKey) {



                var jsonData = JSON.stringify({ ObjAttendees: { Webinarkey: webinarKey } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListOrganizersFromNSF",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Name</th>";
                        tblHtml += "<th>Email</th>";
                        tblHtml += " <th>JoinURL</th>";

                        tblHtml += "</tr>";
                        tblHtml += "</thead>";


                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {

                                var name = value.Name;
                                var email = value.Email;
                                var joinURL = value.JoinURL;

                                tblHtml += "<tr>";


                                tblHtml += '<td><div style="float:left;"><a class="deleteOrg" attr-webinarId=' + webinarKey + ' attr-memberid=' + value.AutoMemberId + '  title="Delete Organizer" style="cursor:pointer;"><i style="color:blue;" class="fa fa-trash-o fa-2x" aria-hidden="true"></i></a> </div> </td>';

                                tblHtml += "<td>" + name + "</td>";
                                tblHtml += "<td>" + email + "</td>";
                                tblHtml += "<td><input type='button'class ='joinUrl' value='Join' /></td>";


                                tblHtml += "</tr>";


                            });

                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='4' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";



                        }
                        tblHtml += "</tbody>";
                        $("#tblOrganizers").html(tblHtml);


                    },
                    failure: function (response) {

                    }
                });
            }

            function getParameterByName(name, url) {
                if (!url) url = window.location.href;
                name = name.replace(/[\[\]]/g, "\\$&");
                var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
                    results = regex.exec(url);
                if (!results) return null;
                if (!results[2]) return '';
                return decodeURIComponent(results[2].replace(/\+/g, " "));
            }

            function getWebinarBasedOnIDFromNSF() {
                var onlineWsCalId = getParameterByName("Id");

                var jsonData = JSON.stringify({ OnlineWsCalID: onlineWsCalId });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/GetWebinarDetails",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            var obj = $.parseJSON(JSON.stringify(data.d));

                            var pgId = obj[0].ProductGroupId;

                            var prdId = obj[0].ProductId;
                            var teacherId = obj[0].TeacherId;
                            var eventyear = obj[0].EventYear;
                            $("#selEventyear").val(eventyear);


                            $('#selProductGroup').val(pgId)

                            $("#selProduct").val(prdId);
                            $("#selCoach").val(teacherId);
                            // listProducts();
                            $('#selProductGroup').attr("disabled", "disabled");
                            $("#selEventyear").attr("disabled", "disabled");
                            $("#txtHwRelDate").val(obj[0].StartDate);
                            $("#selStartTime").val(obj[0].StartTime);
                            $("#selDuration").val(obj[0].Duration);
                            // $("#txtEndDate").val(obj[0].EndDate);
                            // $("#selEndTime").val(obj[0].EndTime);

                            //  $("#hdnUTCStartTime").val(obj[0].UTCStartTime);

                            //  $("#hdnUTCEndTime").val(obj[0].UTCEndTime);

                        } else {

                        }
                    }, error: function () {
                        // alert("Cannot get data");
                    }
                });
            }

            function checkDuplicateSeminar() {

                var retVal = 1;

                var startDate = $("#txtHwRelDate").val();

                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();

                var jsonData = JSON.stringify({ ObjWebianr: { EventYear: eventYear, ProductGroupId: pgId, ProductId: prdId, TeacherId: teacherId, StartDate: startDate } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/CheckDuplicateWebinar",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d) > 0) {
                            statusMessage("There is a Webinar already exists at the same date!", "error");
                        } else {
                            createWebinar();
                        }
                    }, failure: function (e) {

                    }
                });
            }

            function checkDuplicateOrganizers(webinarkey, memberId) {

                var retVal = 1;


                var jsonData = JSON.stringify({ ObjWebianr: { WebinarKey: webinarkey, TeacherId: memberId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/CheckDuplicateOrganizer",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        retVal = JSON.stringify(data.d);

                        if (parseInt(retVal) > 0) {

                            statusMessage("The selected NSF member is already an organizer for the webinar", "error");
                            $(this).prop("checked", false);
                        } else {

                            arrOrganizer.push(memberId);
                        }
                    }, failure: function (e) {

                    }
                });

            }

            function deleteOrganizer(webinarkey, memberId) {

                var retVal = 1;


                var jsonData = JSON.stringify({ ObjWebianr: { WebinarKey: webinarkey, TeacherId: memberId } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/DeleteOrganizer",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        retVal = JSON.stringify(data.d);
                        if (parseInt(retVal)) {
                            //  statusMessage("Deleted successfully", "error");
                            listOrganizersFromNSF(webinarkey);
                        }
                    }, failure: function (e) {

                    }
                });
                return retVal;
            }

            $(document).on("click", ".deleteOrg", function (e) {
                var memberid = $(this).attr("attr-memberid");
                var webinarkey = $(this).attr("attr-webinarId");
                if (confirm("Are you sure want to delete?")) {
                    deleteOrganizer(webinarkey, memberid);
                }
            });

            function getWebinarBasedOnId(webinarKey) {



                var jsonData = JSON.stringify({ Webinarkey: webinarKey });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/GetWebinarDetailsFromNSF",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            // listEvents();

                            var obj = $.parseJSON(JSON.stringify(data.d));

                            var pgId = obj[0].ProductGroupId;

                            var prdId = obj[0].ProductId;
                            var teacherId = obj[0].TeacherId;
                            var eventyear = obj[0].EventYear;
                            var eventId = obj[0].EventId;

                            $("#hdnPgId").val(pgId);
                            $("#hdnPrdid").val(prdId);

                            $("#hdnteacherid").val(teacherId);

                            $("#selEventyear").val(eventyear);

                            $("#selEvent").val(eventId);
                            if (eventId != "") {

                                listProductGroups(eventId);
                                $(".SearchTeacher").hide();
                                $("#dvStartDateparent").css("margin-left", "20px");
                                $("#dvStartDateLbl").css("width", "100px")
                            } else {
                                $("#selEvent").val("0");
                                $(".SearchTeacher").show();
                                $("#dvStartDateparent").css("margin-left", "10px");
                                $("#dvStartDateLbl").css("width", "85px")
                            }
                            if (pgId != "") {
                                $('#selProductGroup').val(pgId);

                            }

                            if (prdId != "" || pgId != "" || eventId != "") {
                                if (prdId != "") {
                                    $("#selProduct").val(prdId);
                                }

                            }
                            if (eventId != "") {
                                listTeachers();
                                //if (teacherId != "") {
                                $("#selCoach").val(teacherId);
                            } else {
                                $("#selCoach").html("<option value=" + obj[0].TeacherId + ">" + obj[0].TeacherName + "</option>");
                            }
                            // }
                            // listProducts();
                            //    $('#selProductGroup').attr("disabled", "disabled");
                            // $("#selEventyear").attr("disabled", "disabled");
                            $("#txtHwRelDate").val(obj[0].StartDate);
                            $("#selStartTime").val(obj[0].StartTime);
                            $("#selDuration").val(obj[0].Duration);
                            // $("#txtEndDate").val(obj[0].EndDate);
                            //  $("#selEndTime").val(obj[0].EndTime);

                            $("#txtWebinarTitle").val(obj[0].Title);
                            $("#btnScheduleWebinar").val("Update Webinar");
                            $("#hdnWebinarkey").val(obj[0].WebinarKey);
                            //   $("#selProduct").attr("disabled", "disabled");
                            //  $("#selCoach").attr("disabled", "disabled");

                            $("#btnCancelWebinar").val("Cancel");

                        } else {

                        }
                    }, error: function () {
                        //  alert("Cannot get data");
                    }
                });
            }

            $(document).on("click", ".editWebinar", function (e) {

                var webinarkey = $(this).attr("attr-webinarId");
                getWebinarBasedOnId(webinarkey);
            });

            function updateWebinar(webinarKey) {

                var title = $("#txtWebinarTitle").val();

                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();
                //    var endDate = $("#txtEndDate").val();
                //   var endTime = $("#selEndTime").val();
                var sessionType = $("#selSessionType").val();
                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();

                var startUnivTime = $("#hdnUTCStartTime").val();
                var endUnivTime = $("#hdnUTCEndTime").val();

                var jsonObject = {
                    "subject": title, "description": title, "times": [{ "startTime": startUnivTime, "endTime": endUnivTime }], "timeZone": "America/New_York", "type": "single_session",
                    "isPasswordProtected": false
                };

                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'PUT',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    data: JSON.stringify(jsonObject),
                    success: function (data) {

                        $.each(data, function (index, value) {

                            webinarKey = value;

                        });
                        statusMessage("Webinar updated successfully", "success");
                        // postWebinarToNSF(webinarKey, "1")
                    },
                    error: function () {
                        // alert("Cannot get data");
                    }
                });
            }

            function deleteWebinar(webinarkey) {

                var retVal = 1;


                var jsonData = JSON.stringify({ ObjWebianr: { WebinarKey: webinarkey } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/DeleteWebinar",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        retVal = JSON.stringify(data.d);
                        if (parseInt(retVal)) {
                            cancelWebinarInGotoWebinar(webinarkey);
                            listWebinars();
                            statusMessage("Webinar deleted successfully", "success");
                        }
                    }, failure: function (e) {

                    }
                });
                return retVal;
            }

            $(document).on("click", ".deleteWebinar", function (e) {
                var webinarkey = $(this).attr("attr-webinarId");
                if (confirm("Are sure want to delete?")) {
                    deleteWebinar(webinarkey);
                }
            });


            function listEvents() {


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ListEvents",
                    // data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        $.each(data.d, function (index, value) {
                            $("#selEvent").append($("<option></option>").val
                                (value.EventId).html(value.EventName));
                        });


                    }, failure: function (e) {

                    }
                });

            }

            $(document).on("change", "#selEvent", function (e) {
                var eventid = $(this).val();
                if (eventid == "0") {
                    $("#selProductGroup").html("<option value='0'>Select</option>");
                    $("#selProduct").html("<option value='0'>Select</option>");
                    $("#selCoach").html("<option value='0'>Select</option>");
                    $(".SearchTeacher").show();
                    $("#dvStartDateparent").css("margin-left", "10px");
                    $("#dvStartDateLbl").css("width", "85px")
                } else {

                    listTeachers();
                    listProductGroups(eventid);
                    $(".SearchTeacher").hide();
                    $("#dvStartDateparent").css("margin-left", "20px");
                    $("#dvStartDateLbl").css("width", "100px")
                }

            });

            function getWebinarStartDateAndEndDate() {

                var productId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();
                var eventYear = $("#selEventyear").val();
                var jsonData = JSON.stringify({ ObjWebinarIn: { ProductId: productId, TeacherId: teacherId, EventYear: eventYear } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/GetWebinarStartDateAndEndDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            var obj = $.parseJSON(JSON.stringify(data.d));

                            var pgId = obj[0].ProductGroupId;

                            var prdId = obj[0].ProductId;
                            var teacherId = obj[0].TeacherId;
                            var eventyear = obj[0].EventYear;

                            $("#txtHwRelDate").val(obj[0].StartDate);
                            $("#selStartTime").val(obj[0].StartTime);
                            $("#selDuration").val(obj[0].Duration);


                        } else {

                        }
                    }, error: function () {
                        // alert("Cannot get data");
                    }
                });
            }
            function getWebinarTitle() {

                var productId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();
                var eventYear = $("#selEventyear").val();
                var productGroupIdId = $("#selProductGroup").val();
                var jsonData = JSON.stringify({ ObjWebinarIn: { ProductId: productId, EventYear: eventYear, ProductGroupId: productGroupIdId } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/GetWebinarTitle",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        var webinarTitle = $.parseJSON(JSON.stringify(data.d));
                        $("#txtWebinarTitle").val(webinarTitle);
                    }, error: function () {
                        // alert("Cannot get data");
                    }
                });
            }

            function validateWebinar() {
                var retVal = 1;
                var title = $("#txtWebinarTitle").val();
                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();
                // var endDate = $("#txtEndDate").val();
                //  var endTime = $("#selEndTime").val();
                var sessionType = $("#selSessionType").val();
                var eventYear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var prdId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();
                var eventid = $("#selEvent").val();
                var duration = $("#selDuration").val();

                if (eventYear == "0") {
                    retVal = -1;
                    statusMessage("Please select Event Year.", "error");
                } else if (teacherId == "0") {
                    statusMessage("Please select Teacher.", "error");
                } else if (eventid == "0" && title == "") {
                    retVal = -1;
                    statusMessage("Please enter Webinar Title.", "error");

                } else if (eventid != "0" && pgId == "0" && prdId == "0" && title == "") {
                    retVal = -1;
                    statusMessage("Please enter Webinar Title.", "error");

                } else if (startDate == "") {
                    retVal = -1;
                    statusMessage("Please enter Start Date.", "error");

                } else if (startTime == "") {
                    retVal = -1;
                    statusMessage("Please enter Start Time.", "error");

                } else if (duration == "") {
                    retVal = -1;
                    statusMessage("Please enter Duration.", "error");

                }
                return retVal;
            }

            function checkTimeConfictsOfWebinar() {

                var retVal = -1;
                var productId = $("#selProduct").val();
                var teacherId = $("#selCoach").val();
                var eventYear = $("#selEventyear").val();
                var duration = $("#selDuration").val();
                var startDate = $("#txtHwRelDate").val();
                var startTime = $("#selStartTime").val();

                var jsonData = JSON.stringify({ ObjWebinar: { ProductId: productId, EventYear: eventYear, StartDate: startDate, StartTime: startTime, Duration: duration } });



                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/ValidateWebinarDateTimeConflicts",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {


                        if (JSON.stringify(data.d.length) > 0) {

                            var obj = $.parseJSON(JSON.stringify(data.d));

                            if (obj[0].IsValidation == "true") {

                                $("#hdnUTCStartTime").val(obj[0].UTCStartTime);


                                $("#hdnUTCEndTime").val(obj[0].UTCEndTime);

                                //  $("#txtEndDate").val(obj[0].EndDate);
                                //  $("#selEndTime").val(obj[0].EndTime);

                                if ($("#btnScheduleWebinar").val() == "Update Webinar") {

                                    var webinarkey = $("#hdnWebinarkey").val();
                                    postWebinarToNSF(webinarkey, "2");


                                } else {

                                    checkDuplicateSeminar();
                                }


                            } else {
                                var obj = $.parseJSON(JSON.stringify(data.d));
                                var startTime = obj[0].StartTime;
                                var endTime = obj[0].EndTime;
                                statusMessage("There is a time conflict, as there is another Webinar scheduled already. Start time:" + startTime + ", End time: " + endTime + "", "error");
                            }
                        }
                    }, error: function () {
                        retVal = -1;
                        // alert("Cannot get data");
                    }
                });


            }
            $(document).on("click", ".btnTeacher", function (e) {
                var teacherid = $(this).attr("attr-memberId");
                getTeacherDetails(teacherid);
                $("#dvNSftable").css("display", "none");
                $("#dvSearchNSFMember").css("display", "none");
            });
            function getTeacherDetails(teacherid) {
                var jsonData = JSON.stringify({ ObjWebinarIn: { TeacherId: teacherid } });


                $.ajax({
                    type: "POST",
                    url: "TestGotoWebinar.aspx/GetTeacherDetails",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            $("#selCoach").append($("<option></option>").val
                                ("0").html("Select"));

                            $.each(data.d, function (index, value) {
                                $("#selCoach").append($("<option></option>").val
                                    (value.TeacherId).html(value.TeacherName));
                                teacherid = value.TeacherId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selCoach").removeAttr("disabled");




                            } else {
                                $("#selCoach").val(teacherid);
                                $("#selCoach").attr("disabled", "disabled");

                            }
                        }
                    }, failure: function (e) {

                    }
                });
            }

            $(document).on("click", "#btnCancelWebinar", function (e) {
                location.href = "TestGoToWebinar.aspx";
            });
            function cancelWebinarInGotoWebinar(webinarKey) {



                $.ajax({
                    url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "",
                    beforeSend: function (xhr) {
                        xhr.setRequestHeader("Authorization", accessToken);
                    },
                    type: 'DELETE',
                    crossDomain: true,
                    dataType: 'json',
                    contentType: 'application/json',
                    processData: false,
                    success: function (data) {

                        listWebinars();
                    },
                    error: function () {
                        //  alert("Cannot get data");
                    }
                });



            }

            function clear() {
                $("#selEvent").val("0");
                $("#selProductGroup").val("0");
                $("#selProduct").val("0");
                $("#selCoach").val("0");
                $("#selCoach").removeAttr("disabled");
                $("#selStartTime").val("0");
                $("#selDuration").val("0");
                $("#txtHwRelDate").val("");
                $("#txtWebinarTitle").val("");
            }
        </script>

        <div style="float: left;">

            <a href="VolunteerFunctions.aspx">Back to Volunteer Functions</a>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div>
            <center>
                <h1 style="color: #00a0b0;">Setup Webinar</h1>
            </center>

        </div>
        <div class="clear"></div>
        <div style="width: 1000px; margin-left: auto; margin-right: auto;">

            <fieldset style="padding: 0px;">
                <div style="float: left; width: 100px;">
                    <label for="name">Event Year:</label>
                </div>
                <div style="float: left; width: 159px;">

                    <select id="selEventyear">
                        <option value="0">Select</option>
                        <option value="2018" selected>2018</option>
                        <option value="2019">2019</option>
                    </select>
                </div>

                <div style="float: left; width: 100px; margin-left: 20px;">
                    <label for="name">Event</label>
                </div>
                <div style="float: left; width: 159px;">

                    <select id="selEvent">
                        <option value="0">Select</option>

                    </select>
                </div>


                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 100px;">
                        <label for="selPgGroup">Product Group:</label>
                    </div>

                    <select id="selProductGroup" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 100px;">
                        <label for="password">Product:</label>
                    </div>

                    <select id="selProduct" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>


                <div class="clear" style="margin-bottom: 5px;"></div>
                <div style="float: left;">
                    <div style="float: left; width: 100px;">
                        <label for="">Co-Organizer:</label>
                    </div>

                    <select id="selCoach" style="width: 160px;">

                        <option value="0">Select</option>

                    </select>
                    <a class="SearchTeacher" title="Search Teacher" style="cursor: pointer;"><i style="" class="fa fa-search fa-2x Seacrh" aria-hidden="true"></i></a>
                </div>


                <div id="dvStartDateparent" style="float: left; margin-left: 10px;">
                    <div id="dvStartDateLbl" style="float: left; width: 85px;">
                        <label for="">Start Date:</label>
                    </div>
                    <div style="float: left;" id="dvStartDate" class="input-append date">

                        <input type="text" style="padding: 6px; width: 158px;" id="txtHwRelDate" placeholder="mm/dd/yyyy" />

                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
                <div style="float: left; margin-left: 18px;">
                    <div style="float: left; width: 102px;">
                        <label for="">Start Time (EST):</label>
                    </div>
                    <div style="float: left;">

                        <select id="selStartTime" style="width: 100px;">
                            <option value="0">Select</option>
                            <option value="08:00:00">8:00 AM</option>
                            <option value="08:30:00">8:30 AM</option>
                            <option value="09:00:00">9:00 AM</option>
                            <option value="09:30:00">9:30 AM</option>
                            <option value="10:00:00">10:00 AM</option>
                            <option value="10:30:00">10:30 AM</option>
                            <option value="11:00:00">11:00 AM</option>
                            <option value="11:30:00">11:30 AM</option>
                            <option value="12:00:00">12:00 PM</option>
                            <option value="12:30:00">12:30 PM</option>
                            <option value="13:00:00">01:00 PM</option>
                            <option value="13:30:00">01:30 PM</option>
                            <option value="14:00:00">02:00 PM</option>
                            <option value="14:30:00">02:30 PM</option>
                            <option value="15:00:00">03:00 PM</option>
                            <option value="15:30:00">03:30 PM</option>
                            <option value="16:00:00">04:00 PM</option>
                            <option value="16:30:00">04:30 PM</option>
                            <option value="17:00:00">05:00 PM</option>
                            <option value="17:30:00">05:30 PM</option>
                            <option value="18:00:00">06:00 PM</option>
                            <option value="18:30:00">06:30 PM</option>
                            <option value="19:00:00">07:00 PM</option>
                            <option value="19:30:00">07:30 PM</option>
                            <option value="20:00:00">08:00 PM</option>
                            <option value="20:30:00">08:30 PM</option>
                            <option value="21:00:00">09:00 PM</option>
                            <option value="21:30:00">09:30 PM</option>
                            <option value="22:00:00">10:00 PM</option>
                            <option value="22:30:00">10:30 PM</option>
                            <option value="23:00:00">11:00 PM</option>
                            <option value="23:30:00">11:30 PM</option>

                        </select>
                    </div>
                </div>


                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 100px;">
                        <label for="">Duration (Hrs):</label>
                    </div>
                    <div style="float: left;" id="dvDuration" class="input-append date">

                        <select id="selDuration" style="width: 100px;">
                            <option value="0">Select</option>
                            <option value="0.5">0.5</option>
                            <option value="1">1</option>
                            <option value="1.5">1.5</option>
                            <option value="2">2</option>
                            <option value="2.5">2.5</option>
                            <option value="3">3</option>
                            <option value="3.5">3.5</option>
                            <option value="4">4</option>
                            <option value="4.5">4.5</option>
                            <option value="5">5</option>
                            <option value="5.5">5.5</option>
                            <option value="6">6</option>
                        </select>



                    </div>
                </div>


                <div class="clear" style="margin-bottom: 5px;"></div>

                <div style="float: left; margin-left: 20px; display: none;">
                    <div style="float: left; width: 100px;">
                        <label for="">End Date:</label>
                    </div>
                    <div style="float: left;" id="dvEndDate" class="input-append date">

                        <input type="text" style="padding: 6px;" id="txtEndDate" placeholder="mm/dd/yyyy" />

                        <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px; display: none;">
                    <div style="float: left; width: 100px;">
                        <label for="">End Time:</label>
                    </div>
                    <div style="float: left;">


                        <select id="selEndTime" style="width: 100px;">
                            <option value="0">Select</option>
                            <option value="08:00:00">8:00 AM</option>
                            <option value="09:00:00">9:00 AM</option>
                            <option value="10:00:00">10:00 AM</option>
                            <option value="11:00:00">11:00 AM</option>
                            <option value="12:00:00">12:00 PM</option>
                            <option value="13:00:00">01:00 PM</option>
                            <option value="14:00:00">02:00 PM</option>
                            <option value="15:00:00">03:00 PM</option>
                            <option value="16:00:00">04:00 PM</option>
                            <option value="17:00:00">05:00 PM</option>
                            <option value="18:00:00">06:00 PM</option>
                            <option value="19:00:00">07:00 PM</option>
                            <option value="20:00:00">08:00 PM</option>
                            <option value="21:00:00">09:00 PM</option>
                            <option value="22:00:00">10:00 PM</option>
                            <option value="23:00:00">11:00 PM</option>

                        </select>


                    </div>
                </div>
                <div class="clear" style="margin-bottom: 5px;"></div>
                <div>
                    <div style="float: left; width: 100px;">
                        <label for="">Webinar Title:</label>
                    </div>
                    <div style="float: left;">

                        <input type="text" style="padding: 6px; width: 285px;" id="txtWebinarTitle" />
                    </div>
                </div>
                <div style="display: none;">
                    <div style="float: left; width: 100px;">
                        <label for="">Session Type:</label>
                    </div>
                    <div style="float: left;">

                        <select id="selSessionType">

                            <option value="single_session" selected>Single</option>
                            <option value="Sequence">Recurring</option>

                        </select>
                    </div>
                </div>
                <%--   <div class="clear"></div>--%>
                <div style="float: left; margin-left: 20px;">
                    <input type="button" class="btnClass" id="btnScheduleWebinar" value="Create Webinar" style="cursor: pointer;" />
                    <input type="button" class="btnClass" id="btnCancelWebinar" value="Reset" style="cursor: pointer;" />
                </div>
            </fieldset>

        </div>

        <div class="clear"></div>

        <div id="dvSearchNSFMember" style="width: 1000px; margin-left: auto; margin-right: auto; padding: 10px; border: 1px solid #c6cccd; display: none;">
            <center>
                <h2>Search NSF Member</h2>
            </center>
            <fieldset style="padding: 0px;">
                <div style="float: left; width: 80px;">
                    <label for="name">First Name:</label>
                </div>
                <div style="float: left;">
                    <input type="text" style="padding: 6px; width: 125px;" id="txtFirstName" />
                </div>

                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 80px;">
                        <label for="selPgGroup">Last Name:</label>
                    </div>

                    <input type="text" style="padding: 6px; width: 125px;" id="txtLastName" />
                </div>

                <div style="float: left; margin-left: 20px;">
                    <div style="float: left; width: 80px;">
                        <label for="Email">Email:</label>
                    </div>
                    <input type="text" style="padding: 6px; width: 125px;" id="txtEmail" />
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <input type="button" class="btnClass" id="btnSearch" value="Seacrh" style="cursor: pointer;" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <input type="button" class="btnClass" id="btnClose" value="Close" style="cursor: pointer;" />
                    </div>
                </div>

            </fieldset>
        </div>
        <div class="clear"></div>
        <div id="dvNSftable" style="display: none;">
            <center>
                <span id="Span1" style="font-weight: bold;">Table 2: NSF Member</span>
            </center>
            <div style="clear: both;"></div>
            <div style="width: 1100px; overflow-y: scroll;">
                <input type="button" class="btnClass" id="btnAssignCoORg" value="Assign Co-organizer" style="cursor: pointer;" />
                <div style="clear: both;"></div>
                <table id="tblNSFMember" class="table">
                </table>
            </div>
        </div>
        <div class="clear"></div>
        <center>
            <span id="spnGridTitle" style="font-weight: bold;">Table 1: Webinar Sessions</span>
        </center>
        <div class="clear"></div>
        <table id="table" style="width: 1200px;">
        </table>

        <div style="clear: both; margin-bottom: 10px;"></div>

        <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>
        <div id="demo" class="ezmodal">

            <div class="ezmodal-container" style="width: 550px !important;">
                <div class="ezmodal-header">
                    <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                    <span class="spnMemberTitle">Co-Organizers</span>
                </div>

                <div class="ezmodal-content">



                    <div class="dvgridContent">
                        <div>
                            <table align="center" id="tblOrganizers" class="table" style="width: 450px;">
                            </table>
                        </div>

                    </div>
                </div>



                <div class="ezmodal-footer">

                    <button type="button" class="btn" data-dismiss="ezmodal" style="cursor: pointer;">Close</button>

                </div>

            </div>

        </div>
        <input type="hidden" id="hdnPgId" value="" />
        <input type="hidden" id="hdnPrdid" value="" />
        <input type="hidden" id="hdnteacherid" value="" />
        <input type="hidden" id="hdnJoinURL" value="" />
        <input type="hidden" id="hdnRegistrantKey" value="" />
        <input type="hidden" id="hdnOnlineWsCalid" value="" />
        <input type="hidden" id="hdnWebinarkey" value="" />
        <input type="hidden" id="hdnLoginId" value="" runat="server" />
        <input type="hidden" id="hdnAccessToken" value="" runat="server" />
        <input type="hidden" id="hdnOrganizerKey" value="" runat="server" />
        <input type="hidden" id="hdnUTCStartTime" value="" />
        <input type="hidden" id="hdnUTCEndTime" value="" />
</asp:Content>
