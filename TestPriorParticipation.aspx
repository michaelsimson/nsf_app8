﻿<%@ Page Language="C#" Debug="true"  AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="TestPriorParticipation.aspx.cs" Inherits="TestPriorParticipation" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     
    


    <table align="center" width="100%">
         <tr>
            <td align="left" colspan="3">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>

        </tr>
        <tr>

            <td colspan="3">
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Prior Participation
</strong> </div>
            </td>
            
        </tr>
         <tr>

            <td align="right" colspan="3">
                
            </td>
        </tr>
        <tr><td colspan="3"><div align="center"><b>Table 1: Name of Children from Fundraiser Registration </b></div></td></tr>
     
       <tr>
           <td align="left">
               
               <asp:Button ID="Button3" runat="server" Text="Insert Prior Participation Flag" OnClick="Button3_Click" />
               </td>
               <td align="left">
               <asp:Label ID="Label156" runat="server" Visible="false" ForeColor="Blue" Font-Bold="true" Text="Label"></asp:Label>
                  </td>
               <td align="center">
                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
               <asp:Button ID="Button2" runat="server" Text="Export to Excel" OnClick="Button2_Click" />
     
        </td>
   
         
         

     </tr>
     <tr align="center">

         <td colspan="3"><div style=" overflow:auto;">
             
             
             <asp:GridView ID="gvFundRaiserReg" runat="server" AllowPaging="True" PageSize="10" OnRowCommand="Function_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True" >
                 <Columns>
                     <asp:TemplateField HeaderText="Action">
                                       
                                        <ItemTemplate>
                                            

                                            <asp:Button ID="btnAction" Text="Select" runat="server" OnClientClick="return ConfirmToUpdate(this)" CommandName="Select" />
                                        </ItemTemplate>
                                    </asp:TemplateField>
                     <asp:TemplateField HeaderText="Ser#">
                         <ItemTemplate>

                                            <asp:Label ID="Label0" runat="server" Text='<%#Eval("Ser#") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                         <asp:TemplateField HeaderText="Child Number">
                         <ItemTemplate>

                                            <asp:Label ID="Label123" runat="server" Text='<%#Eval("ChildNumber") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                      <asp:TemplateField HeaderText="Prior">
                         <ItemTemplate>

                                            <asp:Label ID="Label155" runat="server" Text='<%#Eval("Prior") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="First Name">
                         <ItemTemplate>

                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("First_Name") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Last Name">
                         <ItemTemplate>

                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("Last_Name") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Father FirstName">
                         <ItemTemplate>

                                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("FFFName") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Father LastName">
                         <ItemTemplate>

                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("FFLName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="MemberID">
                          <ItemTemplate>

                                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("AutoMemberID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Email">
                          <ItemTemplate>

                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="HPhone">
                          <ItemTemplate>

                                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("HPhone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                   
                     <asp:TemplateField HeaderText="CPhone">
                          <ItemTemplate>

                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("CPhone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother FirstName">
                          <ItemTemplate>

                                            <asp:Label ID="Label9" runat="server" Text='<%#Eval("MFFName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother LastName">
                          <ItemTemplate>

                                            <asp:Label ID="Label10" runat="server" Text='<%#Eval("MFLName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="RelationshipID">
                          <ItemTemplate>

                                            <asp:Label ID="Label11" runat="server" Text='<%#Eval("Relationship") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother Email">
                          <ItemTemplate>

                                            <asp:Label ID="Label12" runat="server" Text='<%#Eval("MFEmail") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Address1">
                          <ItemTemplate>

                                            <asp:Label ID="Label13" runat="server" Text='<%#Eval("Address1") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="City">
                          <ItemTemplate>

                                            <asp:Label ID="Label14" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="State">
                          <ItemTemplate>

                                            <asp:Label ID="Label15" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Zip">
                          <ItemTemplate>

                                            <asp:Label ID="Label16" runat="server" Text='<%#Eval("Zip") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
              <asp:Repeater ID="rptPager" runat="server">
        <ItemTemplate>
            <asp:LinkButton ID="lnkPage" runat="server" Text = '<%#Eval("Text") %>' CommandArgument = '<%# Eval("Value") %>' Enabled = '<%# Eval("Enabled") %>' OnClick = "Page_Changed"></asp:LinkButton>
        </ItemTemplate>
        </asp:Repeater>
             </div>
         </td>

     </tr>
        <tr><td colspan="3">
            <br />
            <asp:Button ID="Button1" runat="server" Text="Insert Prior Participation Flag" OnClick="Button1_Click" Visible="false" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Label ID="Label124" runat="server" ForeColor="Blue" Text="" Visible="false"></asp:Label>
            </td></tr>
<tr><td colspan="3"><div id="divTable2" runat="server" visible="false" align="center"><b>Table 2: Prior Participation for a Given Child</b></div></td></tr>

        <tr align="center">

         <td colspan="3"><div style=" overflow:auto;">
             
             
             <asp:GridView ID="gvTable2" runat="server" AllowPaging="True" PageSize="10"  AutoGenerateColumns="False" OnPageIndexChanging="gvTable2_PageIndexChanging" OnRowDataBound="gvTable2_RowDataBound">
                 <Columns>
                     <%--<asp:TemplateField HeaderText="Action">
                                       
                                        <ItemTemplate>
                                            <asp:HiddenField ID="FundRContRegID" Value='<%# Bind("FundRContRegID")%>' runat="server" />

                                            <asp:Button ID="btnAction" Text="Select" runat="server" OnClientClick="return ConfirmToUpdate(this)" CommandName="Select" />
                                        </ItemTemplate>
                                    </asp:TemplateField>--%>
                     <asp:TemplateField HeaderText="Ser#">
                         <ItemTemplate>

                                            <asp:Label ID="Label0" runat="server" Text='<%#Eval("Ser#") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Child Number">
                         <ItemTemplate>

                                            <asp:Label ID="Label123" runat="server" Text='<%#Eval("ChildNumber") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="First Name">
                         <ItemTemplate>

                                            <asp:Label ID="Label1" runat="server" Text='<%#Eval("First_Name") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Last Name">
                         <ItemTemplate>

                                            <asp:Label ID="Label2" runat="server" Text='<%#Eval("Last_Name") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Prior">
                         <ItemTemplate>

                                            <asp:Label ID="Label122" runat="server" Text='<%#Eval("Prior") %>'></asp:Label>
                                        </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Father FirstName">
                         <ItemTemplate>
                                            <asp:Label ID="Label3" runat="server" Text='<%#Eval("FFFName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Father Last Name">
                         <ItemTemplate>

                                            <asp:Label ID="Label4" runat="server" Text='<%#Eval("FFLName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Member ID">
                          <ItemTemplate>

                                            <asp:Label ID="Label5" runat="server" Text='<%#Eval("AutoMemberID") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Email">
                          <ItemTemplate>

                                            <asp:Label ID="Label6" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="HPhone">
                          <ItemTemplate>

                                            <asp:Label ID="Label7" runat="server" Text='<%#Eval("HPhone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                   
                     <asp:TemplateField HeaderText="CPhone">
                          <ItemTemplate>

                                            <asp:Label ID="Label8" runat="server" Text='<%#Eval("CPhone") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother FirstName">
                          <ItemTemplate>

                                            <asp:Label ID="Label9" runat="server" Text='<%#Eval("MFFName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother LastName">
                          <ItemTemplate>

                                            <asp:Label ID="Label10" runat="server" Text='<%#Eval("MFLName") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Relationship">
                          <ItemTemplate>

                                            <asp:Label ID="Label11" runat="server" Text='<%#Eval("Relationship") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Mother Email">
                          <ItemTemplate>

                                            <asp:Label ID="Label12" runat="server" Text='<%#Eval("MFEmail") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Address1">
                          <ItemTemplate>

                                            <asp:Label ID="Label13" runat="server" Text='<%#Eval("Address1") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="City">
                          <ItemTemplate>

                                            <asp:Label ID="Label14" runat="server" Text='<%#Eval("City") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="State">
                          <ItemTemplate>

                                            <asp:Label ID="Label15" runat="server" Text='<%#Eval("State") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                     <asp:TemplateField HeaderText="Zip">
                          <ItemTemplate>

                                            <asp:Label ID="Label16" runat="server" Text='<%#Eval("Zip") %>'></asp:Label>
                         </ItemTemplate>
                     </asp:TemplateField>
                 </Columns>
             </asp:GridView>
             </div>
         </td>

     </tr>
  
       
                </table><div align="center">
    <asp:Button ID="btnClose" runat="server" Text="Close Table 2" Visible="false" OnClick="btnClose_Click" />
                </div>
       </asp:Content>
    