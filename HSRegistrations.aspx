﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="HSRegistrations.aspx.cs" Inherits="HSRegistrations" MasterPageFile="~/HumStudyMaster.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <%-- <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">--%>

    <!-- DataTables CSS -->
    <link href="css/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="css/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="css/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="css/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="css/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="css/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script src="js/ezmodal.js"></script>
    <link href="css/Loader.css" rel="stylesheet" />

    <link href="css/jquery.toast.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <script src="js/jquery.toast.js"></script>
    <script type="text/javascript">

        function manageSideBar() {
            //if it is school
            if ($("#ulSideMenuSchoolAdmin").css('display') == 'block') {
                $("#topmenu_2").trigger("click");
                $("#sm-result").removeClass("submenu_active");
                $("#sm-reg").addClass("submenu_active");
                $("#sm-rep").removeClass("submenu_active");
            }
        }
        $(function (e) {

          
            manageSideBar();

            $('#dateRangePicker').datepicker({
                format: 'mm/dd/yyyy',
                autoclose: true,
            });

            fillStudentDetails();
            if (document.getElementById('<%= hdnIsSuccess.ClientID%>').value == "1") {
                document.getElementById('<%= hdnIsSuccess.ClientID%>').value = "0";
                statusMessage("Saved Successfully", "", "success");
            }
        });
        $(document).on("click", ".close-jq-toast-single", function (e) {
            document.getElementById('<%= hdnIsSuccess.ClientID%>').value = "0"; 
        }); 

        function fillStudentDetails() {
            showLoader(); 
            var cyear = $("#selAcademicYr").val();
            document.getElementById('<%= hdnCYear.ClientID%>').value = cyear;
            var schoolId = document.getElementById('<%= hdnSchoolId.ClientID%>').value; 
            var strclass = $("#selViewClass").val(); 
            var sect = $("#selViewSection").val(); 
            var source = "CBSE"; 
            var jsonData = JSON.stringify({ Year: cyear, SchoolID: schoolId, Source: source, Class: strclass, Section: sect, Grade: strclass, StudentId: "0" });
            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/ListHumStudyStudents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    //  hideLoader();
                    var tblHtml = "";
                    tblHtml += "<thead>";
                    tblHtml += "<tr>";
                    tblHtml += "<th>Action</th>";
                    tblHtml += "<th>Id</th>";
                    tblHtml += "<th style='width:10%'>Student Name</th>";
                    tblHtml += "<th>Grade</th>";
                    tblHtml += "<th>DOB</th>";
                    tblHtml += "<th>Email</th>";
                    tblHtml += "<th>Phone</th>";
                    tblHtml += "<th>Gender</th>";
                    tblHtml += "<th>Image</th>";
                    tblHtml += "<th>Class</th>"; 
                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    tblHtml += "<tbody>";
                    $.each(data.d, function (index, value) {
                        tblHtml += "<tr>";
                        tblHtml += '<td><a id="btnEdit' + value.StudentId + '" class="modify" attr-studentid=' + value.StudentId + ' title="Modify"  style="cursor: pointer;" data-toggle="modal" data-target="#AddNewModel"> <i class="fa  fa-pencil-square-o fa-1x"></i></a></td>';
                        tblHtml += "<td>" + value.StudentId + "</td>";
                        tblHtml += "<td>" + value.StudentName + "</td>";
                        tblHtml += "<td>" + value.Grade + "</td>";
                        tblHtml += "<td>" + value.DOB + "</td>";
                        tblHtml += "<td>" + value.EMail + "</td>";
                        tblHtml += "<td>" + value.Phone + "</td>";
                        tblHtml += "<td>" + value.Gender + "</td>";
                        if (value.PhotoPath == "") {
                            tblHtml += '<td><span  style="width:35px; height:30px; position:relative; left:25px;"><i class="fa fa-user" aria-hidden="true"></i></span></td>';
                        } else {
                            tblHtml += "<td><img src=" + value.PhotoPath + " style='width:35px; height:30px; position:relative; left:17px;' /></td>";
                        }
                        tblHtml += "<td>" + value.Class + " </td>";

                        tblHtml += "</tr>";
                    });
                    tblHtml += "</tbody>";
                    $('#tblStudents').html(tblHtml);
                    /**/ $('#tblStudents').DataTable({
                        responsive: true,
                        destroy: true
                    });

                    getAvailableSubjects();
                },
                failure: function (response) {
                    //  alert(response.d);
                }
            });
            hideLoader();
        }

        function postNewStudent() {
            showLoader();
            var schoolId = document.getElementById('<%= hdnSchoolId.ClientID%>').value;
            var stateid = document.getElementById('<%= hdnStateId.ClientID%>').value;
            var syetemId = document.getElementById('<%= hdnSystemId.ClientID%>').value;
            var regionId = document.getElementById('<%= hdnRegionId.ClientID%>').value;

            var loginId = document.getElementById('<%= hdnLoginId.ClientID%>').value;

            var studentname = $("#txtStudName").val();
            var language = $("#selLanguage").val();
            var grade = $("#selGrade").val();
            var dob = $("#txtDOB").val();
            var gender = $("#selGender").val();
            var mailId = $("#txtMailId").val();
            var phoneNo = $("#txtPhoneNo").val();
            var parentname = $("#txtParentName").val();
            var year = $("#selAcademicYr").val();

            var jsonData = JSON.stringify({ ObjStudent: { SystemId: syetemId, RegionID: regionId, StateID: stateid, SchoolId: schoolId, StudentName: studentname, Grade: grade, DOB: dob, Gender: gender, Language: language, EMail: mailId, Phone: phoneNo, ParentName: parentname, LoginId: loginId, CYear: year } });

            // var jsonData = JSON.stringify({ });
            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/PostNewStudents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    var studentId = JSON.stringify(data.d);
                    document.getElementById('<%= hdnStudentId.ClientID%>').value = studentId;

                    if (parseInt(studentId) > 0) {
                        $("#hdnStudId").val("0");
                        statusMessage("Saved successfully", "", "success");
                        reset();
                        fillStudentDetails();
                        document.getElementById('<%= BtnFileUpload.ClientID%>').click();
                    }
                }
            });
            hideLoader();
        }

        function validateStudent() {
            var retVal = 1;
            var studentname = $("#txtStudName").val();
            var language = $("#selLanguage").val();
            var grade = $("#selGrade").val();
            var dob = $("#txtDOB").val();
            var gender = $("#selGender").val();
            var mailId = $("#txtMailId").val();
            var phoneNo = $("#txtPhoneNo").val();
            var parentname = $("#txtParentName").val();



            if (studentname == "") {
                retval = -1;
                statusMessage("Please enter Student Name", "", "error");
            } else if (language == "0") {
                retVal = -1;
                statusMessage("Please select Language", "", "error");
            } else if (grade == "0") {
                retVal = -1;
                statusMessage("Please select Grade", "", "error");
            } else if (dob == "") {
                retVal = -1;
                statusMessage("Please enter Date Of Birth", "", "error");
            } else if (gender == "0") {
                retVal = -1;
                statusMessage("Please select Gender", "", "error");
            } else if (phoneNo == "") {
                retVal = -1;
                statusMessage("Please enter Contact Pnone Number", "", "error");
            } else if (mailId == "") {
                retVal = -1;
                statusMessage("Please enter Contact MailId", "", "error");
            } else if (!ValidateEmail(mailId)) {

                retVal = -1;
                statusMessage("Please enter valid Email Address", "middle-center", "error");

            } else if (phoneNo.length < 10 || phoneNo.length > 10) {
                retVal = -1;
                statusMessage("Please enter valid Phone number", "middle-center", "error");
            } else if (parentname == "") {
                retVal = -1;
                statusMessage("Please enter Parent Name", "middle-center", "error");
            }

            return retVal;
        }


        function statusMessage(message, position, type) {

            $.toast({
                // heading: 'Can I add <em>icons</em>?',
                text: '<b>' + message + '</b>',
                icon: type,
                position: 'mid-center',
                loader: true,        // Change it to false to disable loader
                loaderBg: '#9EC600',  // To change the background
                stack: 1

            })
        }
        function reset() {
            $("#txtStudName").val("");
            $("#selLanguage").val("0");
            $("#selGrade").val("0");
            $("#txtDOB").val("");
            $("#selGender").val("0");
            $("#txtMailId").val("");
            $("#txtPhoneNo").val("");
            $("#txtParentName").val("");
        }
        $(document).on("click", "#btnSubmit", function (e) {

            if (validateStudent() == 1) {
                if ($("#hdnStudId").val() == "0") {
                    postNewStudent();
                } else {
                    updateStudent();
                }
            }
        });

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };
        $(document).on("change", "#selViewClass", function (e) {
            fillStudentDetails();
        });
        $(document).on("change", "#selViewSection", function (e) {
            fillStudentDetails();
        });

        function getHumStudyStudentdetails(studentId) {
            showLoader();
            //var department = $(this).val();
            var cyear = $("#selAcademicYr").val();
            document.getElementById('<%= hdnCYear.ClientID%>').value = cyear;
            var schoolId = document.getElementById('<%= hdnSchoolId.ClientID%>').value;

            var strclass = $("#selViewClass").val();
            // alert(strclass);
            var sect = $("#selViewSection").val();

            var source = "CBSE";

            var jsonData = JSON.stringify({ Year: cyear, SchoolID: schoolId, Source: source, Class: strclass, Section: sect, Grade: strclass, StudentId: studentId });

            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/ListHumStudyStudents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    //  hideLoader();

                    $.each(data.d, function (index, value) {

                        $("#txtStudName").val(value.StudentName);
                        $("#selLanguage").val(value.Language);
                        $("#selGrade").val(value.Grade);
                        $("#selGrade").attr("disabled", "disabled");
                        $("#txtDOB").val(value.DOB);
                        $("#selGender").val(value.Gender);
                        $("#txtMailId").val(value.EMail);
                        $("#txtPhoneNo").val(value.Phone);
                        $("#txtParentName").val(value.ParentName);
                        $("#hdnStudId").val(value.StudentId)

                    });

                },
                failure: function (response) {
                    //  alert(response.d);
                }
            });
            hideLoader();
        }

        $(document).on("click", ".modify", function (e) {
            var studentId = $(this).attr("attr-studentid");
            getHumStudyStudentdetails(studentId);
        });

        function getAvailableSubjects() {
            showLoader();

            var cyear = $("#selAcademicYr").val();
            var regionId = document.getElementById('<%= hdnRegionId.ClientID%>').value;

            var strclass = $("#selViewClass").val();


            var jsonData = JSON.stringify({ Year: cyear, RegionId: regionId, Grade: strclass });

            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/GetAvailableSubjects",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    //  hideLoader();
                    var subjectCodes = "";
                    $.each(data.d, function (index, value) {

                        subjectCodes += value.Subjectcode + ",";

                    });
                    if (subjectCodes == "") {
                    } else {
                        $("#spnSubjects").text("Available Subjects: " + subjectCodes + "");
                    }

                },
                failure: function (response) {
                    //  alert(response.d);
                }
            });
            hideLoader();
        }

        function updateStudent() {
            showLoader();
            var schoolId = document.getElementById('<%= hdnSchoolId.ClientID%>').value;
            var stateid = document.getElementById('<%= hdnStateId.ClientID%>').value;
            var syetemId = document.getElementById('<%= hdnSystemId.ClientID%>').value;
            var regionId = document.getElementById('<%= hdnRegionId.ClientID%>').value;

            var loginId = document.getElementById('<%= hdnLoginId.ClientID%>').value;

            var studentname = $("#txtStudName").val();
            var language = $("#selLanguage").val();
            var grade = $("#selGrade").val();
            var dob = $("#txtDOB").val();
            var gender = $("#selGender").val();
            var mailId = $("#txtMailId").val();
            var phoneNo = $("#txtPhoneNo").val();
            var parentname = $("#txtParentName").val();
            var year = $("#selAcademicYr").val();
            var studentId = $("#hdnStudId").val();

            var jsonData = JSON.stringify({ ObjStudent: { StudentName: studentname, Grade: grade, DOB: dob, Gender: gender, Language: language, EMail: mailId, Phone: phoneNo, ParentName: parentname, LoginId: loginId, CYear: year, StudentId: studentId } });

            // var jsonData = JSON.stringify({ });
            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/UpdateStudents",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    if (JSON.stringify(data.d) == 1) {
                        $("#selGrade").removeAttr("disabled");
                        $("#hdnStudId").val("0");
                        statusMessage("Updated successfully", "", "success");
                        reset();
                        fillStudentDetails();
                    } else if (JSON.stringify(data.d) == -2) {
                        statusMessage("Duplicate exists. Please contact admin!!!", "", "error");
                    }
                }
            });
            hideLoader();
        }

    </script>

    <asp:Button ID="BtnFileUpload" Style="display: none;" runat="server" OnClick="BtnFileUpload_Click" />
    <div class="row" style="padding-top: 10px">
        <div class="col-lg-8">
            <div class="panel panel-default">

                <div class="panel-heading">
                    <i class="fa fa-user fa-fw"></i>Add Student
                </div>

                <div class="panel-body">

                    <div class="form-group col-md-4">
                        <label>Name of the Student  </label>
                        <input class="form-control" placeholder="Student Name" id="txtStudName">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Language</label>
                        <select class="form-control" id="selLanguage">
                            <option value="0">Select</option>
                            <option>Telegu</option>
                            <option>English</option>
                            <option>Hindi</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Grade</label>
                        <select class="form-control" id="selGrade">
                            <option value="0">Select</option>
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                            <option>4</option>
                            <option>5</option>
                            <option>6</option>
                            <option>7</option>
                            <option>8</option>
                            <option>9</option>
                            <option>10</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>DOB  </label>
                        <div class="col-md-10 input-group input-append date" id="dateRangePicker">
                            <input type="text" class="form-control" name="date" id="txtDOB" placeholder="dd/mm/yyyy">
                            <span class="input-group-addon add-on"><span class="glyphicon glyphicon-calendar"></span></span>
                        </div>

                    </div>
                    <div class="form-group col-md-4">
                        <label>Gender</label>
                        <select class="form-control" id="selGender">
                            <option value="0">Select</option>
                            <option value="M">Male</option>
                            <option value="F">Female</option>
                        </select>
                    </div>
                    <div class="form-group col-md-4">
                        <label>Contact Mail ID  </label>
                        <input class="form-control" type="" placeholder="Contact Mail ID" id="txtMailId">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Contact Phone Number  </label>
                        <input class="form-control" type="" placeholder="Contact Phone Number" id="txtPhoneNo">
                    </div>
                    <div class="form-group col-md-4">
                        <label>Student Photo</label>
                        <asp:FileUpload ID="FStudentPhoto" runat="server" />
                    </div>
                    <div class="form-group col-md-4">
                        <label>Parent Name</label>
                        <input class="form-control" type="" placeholder="Parent Name" id="txtParentName">
                    </div>
                    <div class="form-group col-md-4">
                        <input type="button" id="btnCancel" value="Cancel" class="btn btn-warning" />
                        <input type="button" id="btnSubmit" value="submit" class="btn btn-success" />
                    </div>


                </div>
            </div>
        </div>


        <div class="col-lg-4">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list fa-fw"></i>Upload Students 
                </div>
                <div class="panel-body">
                    <div class="form-group col-md-12">
                        <label>Select File</label>
                        <asp:FileUpload ID="bulkFUpload" runat="server" />
                        <br />
                        <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                        <br />

                        <asp:Button ID="btnBulkUpload" runat="server" Text="Upload" OnClick="btnBulkUpload_Click" CssClass="btn btn-success" />

                    </div>

                    <%-- <center>
                    <button type="reset" class="btn btn-warning" id="btnBulkCancel">Cancel</button>
                    <button type="submit" class="btn btn-success" id="btnBulkSubmit">Submit</button></center>--%>
                </div>
            </div>

        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list fa-fw"></i>Students Details
                  
                        <span id="spnSubjects" style="position: relative; left: 30px;"></span>

                    <div class="pull-right">

                        <div class="btn-group">


                            <select style="Color: black" id="selViewClass">
                                <option value="-1">Select Class</option>
                                <option value="9" selected="">Class 9</option>
                                <option value="8">Class 8</option>
                                <option value="7">Class 7 </option>
                                <option value="6">Class 6</option>
                                <option value="5">Class 5</option>
                            </select>
                            <select style="Color: black" id="selViewSection">
                                <option value="-1">Select Section</option>
                                <option value="A" selected="">Section : A</option>
                                <option value="B">Section : B</option>
                                <option value="C">Section : C</option>
                                <option value="D">Section : D</option>
                            </select>
                        </div>

                    </div>
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="divtblStudents">
                        <table id="tblStudents" style="width: 100%;" class="table table-striped table-bordered table-hover">
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>
      <a id="ashowpopup" data-toggle="modal" data-target="#divDuplicate">click</a>
    <div id="divDuplicate" class="modal" style="display: none;"><div class="row">
        <div class="col-lg-8">
            <div class="panel panel-default" style="margin-left: 215px;margin-right: -250px;">
                <div class="panel-heading">
                      <asp:Label ID="lblSuccess" runat="server" ForeColor="Blue"></asp:Label><br />
                    <i class="fa fa-user fa-fw" style="color: #47a81d;"></i><span style="color: #47a81d;">Duplicate Student Name List</span><span class="Addclose" data-dismiss="modal">×</span>
                    <asp:Label ID="lblDuplicateError" runat="server" ForeColor="Red"></asp:Label>
                </div>

                <div class="panel-body" style="overflow: scroll;">
        <table  id="duplicateList" style="width: 100%;"  class="table table-striped table-bordered table-hover" >
                        
                    </table>  
                    </div></div></div></div>
        </div>

    <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
    <div id="overlay"></div>
    <input type="hidden" id="hdnSchoolId" value="0" runat="server" />
    <input type="hidden" id="hdnTeacherId" value="0" runat="server" />
    <input type="hidden" id="hdnStudentId" value="0" runat="server" />
    <input type="hidden" id="hdnCYear" value="0" runat="server" />
    <input type="hidden" id="hdnLoginId" value="0" runat="server" />
    <input type="hidden" id="hdnSystemId" value="0" runat="server" />
    <input type="hidden" id="hdnRegionId" value="0" runat="server" />
    <input type="hidden" id="hdnStateId" value="0" runat="server" />
    <input type="hidden" id="hdnStudId" value="0" />
    <input type="hidden" id="hdnIsSuccess" value="0" runat="server" />

</asp:Content>
