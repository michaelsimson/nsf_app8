﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Partial Class ExceptContestant
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
           

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Server.Transfer("login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Then
            Else
                Server.Transfer("maintest.aspx")
            End If

            If Not Page.IsPostBack Then
                LoadChapter(Session("LoginChapterID"))
                If Session("LoginChapterID") = 1 Then
                    Session("EventID") = 1
                Else
                    Session("EventID") = 2
                End If
                LoadEvent(Session("EventID"))
                If Convert.ToInt32(Now.Month) > 11 Then
                    ddlEventYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
                Else
                    ddlEventYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year))
                End If
                ddlEventYear.Items(0).Selected = True
                ddlEventYear.Enabled = False
                GetRecords()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from Product where ProductId=" & ProductID & "")
    End Function

    Function getProductGroupID(ByVal ProductID As Integer) As Integer
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupId from Product where ProductId=" & ProductID & "")
    End Function

    Private Sub LoadProductID(ByVal status As String)
        ' will load depending on Selected item in Productgroup
        lblErr.Text = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        Select Case ddlEvent.SelectedValue
            Case 1
                'national
                strSql = " SELECT b.ProductID, b.ProductCode FROM Contestant a INNER JOIN ContestCategory c  ON a.ContestCategoryID = c.ContestCategoryID INNER JOIN Contest B on B.ContestCategoryID =  c.ContestCategoryID"
                strSql = strSql & " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber  AND a.ParentID = e.MEMBERID  WHERE a.ParentID = " & HlblParentID.Text & "  and a.ContestYear = " & ddlEventYear.SelectedValue & " and a.NationalInvitee = 1 and a.ChildNumber = " & ddlChild.SelectedValue & " and b.NSFChapterID = 1 " 'DATEDIFF (d,getdate(),B.registrationdeadline)>0 And  and a.contestant_id is null"
                'second Query with Grade
                strSql = strSql & " UNION Select b.ProductID, b.ProductCode from Contest b INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID and b.Contest_Year = " & ddlEventYear.SelectedValue & " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' "
                strSql = strSql & " and a.GradeBased=1   and (Select Grade from Child  where childnumber = " & ddlChild.SelectedValue & " ) between GradeFrom and GradeTo  " 'and DATEDIFF (d,getdate(),b.registrationdeadline)>0 
                strSql = strSql & " Left join Contestant c on c.ContestCategoryID = a.ContestCategoryID and b.contestId=c.contestID and c.childnumber=" & ddlChild.SelectedValue & " and c.PaymentReference is not null Where c.contestant_id is null " '20522 
                strSql = strSql & " UNION  "
                'Third Query with Age
                strSql = strSql & " Select b.ProductID, b.ProductCode from Contest b INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID and b.Contest_Year = " & ddlEventYear.SelectedValue & " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' "
                strSql = strSql & " and a.GradeBased=0   and (select DATEDIFF(YY,Date_OF_Birth,GEtdate()) from Child where childnumber = " & ddlChild.SelectedValue & ") between AgeFrom and AgeTo  " 'and DATEDIFF (d,b.registrationdeadline,getdate())>0
                strSql = strSql & " Left join Contestant c on c.ContestCategoryID = a.ContestCategoryID and b.contestId=c.contestID and c.childnumber=" & ddlChild.SelectedValue & "" '20522 "
                strSql = strSql & " and c.PaymentReference is not null Where c.contestant_id is null   ORDER BY b.ProductID"
                'Response.Write(strSql)
            Case 2
                'regional
                strSql = "Select b.ProductID, b.ProductCode from Contest b INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID and b.Contest_Year = " & ddlEventYear.SelectedValue & " and b.nsfchapterid = " & ddlChapter.SelectedValue & " and a.RegionalStatus = 'Active' "
                strSql = strSql & " and a.GradeBased=1   and (Select Grade from Child  where childnumber = " & ddlChild.SelectedValue & " ) between GradeFrom and GradeTo  UNION  "
                strSql = strSql & " Select b.ProductID, b.ProductCode from Contest b INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID and b.Contest_Year = " & ddlEventYear.SelectedValue & " and b.nsfchapterid = " & ddlChapter.SelectedValue & " and a.RegionalStatus = 'Active' "
                strSql = strSql & " and a.GradeBased=0   and (select DATEDIFF(YY,Date_OF_Birth,GEtdate()) from Child where childnumber = " & ddlChild.SelectedValue & ") between AgeFrom and AgeTo  ORDER BY b.ProductID"
            Case Else
                Exit Sub
        End Select
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlProduct.Items.Clear()
        ddlProduct.DataSource = drproductid
        ddlProduct.DataBind()
        If ddlProduct.Items.Count > 0 Then
            If status = "Y" Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
            End If
            ddlProduct.Enabled = True
        Else
            lblErr.ForeColor = Color.Red
            lblErr.Text = "No Eligible Contests Available"
        End If
    End Sub
    Private Sub LoadChapter(ByVal loginchapterID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "Select ChapterID, ChapterCode from chapter where Status='A' Order by State,Chaptercode"
        Dim drChapter As SqlDataReader
        drChapter = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlChapter.DataSource = drChapter
        ddlChapter.DataBind()
        ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(LoginChapterID))
        ddlChapter.Enabled = False
    End Sub

    Private Sub LoadEvent(ByVal EventID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "select EventID,Name from event where EventYear>=YEAR(Getdate())"
        Dim drEvent As SqlDataReader
        drEvent = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlEvent.DataSource = drEvent
        ddlEvent.DataBind()
        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(EventID))
        ddlEvent.Enabled = False
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer, ByVal status As String)
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            'Dim strSql As String = "Select First_name+' '+Last_name as Name, ChildNumber From Child where (memberid=" & ParentID & " Or SpouseID=" & ParentID & " Or MemberID =(Select Relationship from IndSpouse where AutoMemberID =" & ParentID & ")) and MemberID >0"
            Dim strSql As String = "Select First_name+' '+Last_name as Name, ChildNumber From Child where (memberid=" & ParentID & ")" ' Or SpouseID=" & ParentID & " Or MemberID =(Select Relationship from IndSpouse where AutoMemberID =" & ParentID & ")) and MemberID >0"

            Dim drChild As SqlDataReader
            drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlChild.DataSource = drChild
            ddlChild.DataBind()
            ddlChild.Enabled = True
            If status = "Y" Then
                ddlChild.Items.Insert(0, "Select Child")
                ddlChild.Items(0).Selected = True
            End If
        Catch ex As Exception
            'Response.Write(ParentID & ex.ToString())
        End Try
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  ( I.firstName like '%" + firstName + "%'  OR I.firstName like '%" + firstName + "%' ) ")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and (I.lastName like '%" + lastName + "%' OR I1.lastName like '%" + lastName + "%' )")
            Else
                strSql.Append("  (I.lastName like '%" + lastName + "%' OR I1.lastName like '%" + lastName + "%')")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and (I.Email like '%" + email + "%' OR I1.Email like '%" + email + "%')")
            Else
                strSql.Append("  (I.Email like '%" + email + "%' OR I1.Email like '%" + email + "%' )")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            'strSql.Append(" and (DonorType='IND' OR DonorType='SPOUSE') order by lastname,firstname")
            strSql.Append(" and  I.DonorType='IND' AND I1.DonorType='SPOUSE'  order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        'Response.Write(sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse_Exp", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Search.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        txtParent.Text = row.Cells(1).Text + " " + row.Cells(2).Text
        HlblParentID.Text = GridMemberDt.DataKeys(index).Value
        Loadchild(HlblParentID.Text, "Y")
        pIndSearch.Visible = False
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlChild.Items(0).Selected = True Then
            LoadProductID("Y")
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try

            Dim strSQL As String = ""
            If btnSubmit.Text = "Add" Then
                If HlblParentID.Text = "" Then
                    lblErr.Text = "Please search for Parent Using Search Button"
                ElseIf ddlChild.Items(0).Selected = True Or ddlChild.Items.Count < 1 Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlProduct.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Contests Available"
                ElseIf ddlProduct.Items(0).Selected = True Then
                    lblErr.Text = "Please select a Product"
                ElseIf txtDeadline.Text = "" Or IsDate(txtDeadline.Text) <> True Then
                    lblErr.Text = "Please enter new deadline date in mm/dd/yyyy format"
                ElseIf Date.Parse(txtDeadline.Text) < Now.Date Then
                    lblErr.Text = "Deadline date Entered was Passed"
                Else
                    ' BadgeNumber is tested to be Null--This should be done for all children in the contest --21-04-2014
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from contestant where ChapterID =" & ddlChapter.SelectedValue & " AND ContestYear =" & ddlEventYear.SelectedValue & " AND EventID =" & ddlEvent.SelectedValue & " AND ProductID= " & ddlProduct.SelectedValue & " and badgenumber is not null") = 0 Then
                        'Added if there is no record already existing in ExCotnestant table
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from Excontestant where ChildNumber=" & ddlChild.SelectedValue & " AND ChapterID =" & ddlChapter.SelectedValue & " AND ContestYear =" & ddlEventYear.SelectedValue & " AND EventID =" & ddlEvent.SelectedValue & " AND ProductID= " & ddlProduct.SelectedValue & "") < 1 Then
                            'Added on 04-04-2014 
                            '/*******A Duplicate[record to ExContestant table if Contestant table has the same record] is allowed after the contest in the contestant table is over and there are no scores in the table (that is, the child did not participate in the contest)..****************'
                            Dim ContestantCount As Integer = 0
                            Dim ContRegCount As Integer = 0
                            Dim ContestDate As Boolean = False
                            ''Count - Parent had registered with Payment but not taken the Contest --reversed
                            ''16-04-2014  Allowing pending transactions to be processed on an exception basis...

                            ContestantCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null and  GETDATE()< Cn.Contestdate and convert(varchar(10),'" & txtDeadline.Text & "',101)>= GETDATE()") 'C.PaymentReference is Not Null
                            ''Count-No Registration made anywhere
                            ContRegCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select COUNT(*) from Contestant where ChildNumber=" & ddlChild.SelectedValue & " AND ChapterID =" & ddlChapter.SelectedValue & " AND ContestYear =" & ddlEventYear.SelectedValue & " AND EventID =" & ddlEvent.SelectedValue & " AND ProductID= " & ddlProduct.SelectedValue)
                            ContestDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select top 1 Case when  GETDATE()< Contestdate  then 'True' else 'false' end from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and convert(varchar(10),'" & txtDeadline.Text & "',101)>= GETDATE()")

                            Dim regDeadLine As Date
                            regDeadLine = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select RegistrationDeadLine from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue)
                            Dim strMsg As String = String.Empty

                            If ContestantCount > 0 Or (ContRegCount = 0 And ContestDate = True) Then 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null and Cn.Contestdate < GETDATE()") > 0 Then
                                Dim PaidRegistrations As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and  GETDATE()< Cn.Contestdate"))
                                Dim Capacity As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and GETDATE()< Contestdate "))
                                If Capacity > 0 Then
                                    If PaidRegistrations >= Capacity Then
                                        'msg to 
                                        strMsg = "Capacity has reached"
                                    End If
                                    If PaidRegistrations >= Capacity And regDeadLine < Now.Date Then
                                        'msg
                                        strMsg = strMsg & " and "
                                    End If
                                    If regDeadLine < Now.Date Then
                                        strMsg = strMsg & " Contest deadline passed"
                                    End If
                                    If strMsg <> String.Empty Then
                                        strMsg = strMsg & ". Do you still want to add?"
                                        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToSave('" & strMsg & "');", True)
                                        Exit Sub
                                    End If
                                ElseIf regDeadLine < Now.Date Then
                                    strMsg = "Contest deadline passed. Do you still want to add?"
                                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToSave('" & strMsg & "');", True)
                                    Exit Sub
                                End If
                                btnSave_Click(btnSave, New EventArgs)
                            Else
                                'Updated on 18-04-2014 
                                '1) If badge numbers were generated, No registration will be allowed
                                '2) If todate >= the contest date,No registration will be allowed

                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and Contestdate < GETDATE() ") > 0 Then 'Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null and GETDATE() > Cn.ContestDate ") > 0 Then ' C.PaymentReference is Not Null 'and C.PaymentReference is Null 
                                    lblErr.Text = " Contest Date is over."
                                ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & " and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and (C.Score1 is not null or C.score2 is not null or C.Score3 is not null) and C.BadgeNumber is not null") > 0 Then ' C.PaymentReference is Not Null
                                    lblErr.Text = "Duplicates cannot be created as the child has participated in the Contest."
                                ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & " and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.BadgeNumber is not null") > 0 Then ' C.PaymentReference is Not Null
                                    lblErr.Text = "BadgeNumbers were generated / Contestant Record [with Payment] exists for the selections made."
                                Else
                                    lblErr.Text = "Records cannot be added."
                                End If

                            End If
                        Else
                            lblErr.Text = "Data already Exist"
                        End If
                    End If
                End If
            Else
                If HlblParentID.Text = "" And hlblExContestantID.Text = "" Then
                    lblErr.Text = "Please select once again"
                ElseIf ddlChild.Items.Count < 1 Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlProduct.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Contests Available"
                ElseIf txtDeadline.Text = "" Or IsDate(txtDeadline.Text) <> True Then
                    lblErr.Text = "Please enter new deadline date in mm/dd/yyyy format"
                ElseIf Date.Parse(txtDeadline.Text) < Now.Date Then
                    lblErr.Text = "Deadline date Entered was Passed"
                Else
                    'hlblExContestantID.Text is the ExceontestID to be updated

                    'Added on 04-04-2014 
                    '/*******A Duplicate[record to ExContestant table if Contestant table has the same record] is allowed after the contest in the contestant table is over and there are no scores in the table (that is, the child did not participate in the contest)..****************'
                    Dim ContestantCount As Integer = 0
                    Dim ContRegCount As Integer = 0
                    Dim ContestDate As Boolean = False
                    ''Count -Parent had registered with Payment but not taken the Contest --reversed
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from contestant where ChapterID =" & ddlChapter.SelectedValue & " AND ContestYear =" & ddlEventYear.SelectedValue & " AND EventID =" & ddlEvent.SelectedValue & " AND ProductID= " & ddlProduct.SelectedValue & " and badgenumber is not null") = 0 Then

                        ''16-04-2014  Allowing pending transactions to be processed on an exception basis...
                        ContestantCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null and GETDATE() < Cn.Contestdate and convert(varchar(10),'" & txtDeadline.Text & "',101)>= GETDATE()") 'C.PaymentReference is Not Null
                        ''Count-No Registration made anywhere
                        ContRegCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select COUNT(*) from Contestant where ChildNumber=" & ddlChild.SelectedValue & " AND ChapterID =" & ddlChapter.SelectedValue & " AND ContestYear =" & ddlEventYear.SelectedValue & " AND EventID =" & ddlEvent.SelectedValue & " AND ProductID= " & ddlProduct.SelectedValue & "")
                        ContestDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select top 1 Case when  GETDATE()< Contestdate  then 'True' else 'false' end from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and convert(varchar(10),'" & txtDeadline.Text & "',101)>= GETDATE()")

                        If ContestantCount > 0 Or (ContRegCount = 0 And ContestDate = True) Then
                            Dim regDeadLine As Date
                            regDeadLine = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select RegistrationDeadLine from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue)
                            Dim strMsg As String = String.Empty

                            Dim PaidRegistrations As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and  GETDATE()< Cn.Contestdate"))
                            Dim Capacity As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and GETDATE()< Contestdate "))
                            If Capacity > 0 Then
                                If PaidRegistrations >= Capacity Then
                                    'msg to 
                                    strMsg = "Capacity has reached"
                                End If
                                If PaidRegistrations >= Capacity And regDeadLine < Now.Date Then
                                    'msg
                                    strMsg = strMsg & " and "
                                End If
                                If regDeadLine < Now.Date Then
                                    strMsg = strMsg & " Contest deadline passed"
                                End If
                                If strMsg <> String.Empty Then
                                    strMsg = strMsg & ". Do you still want to add?"
                                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToUpdate('" & strMsg & "');", True)
                                    Exit Sub
                                End If
                            ElseIf regDeadLine < Now.Date Then
                                strMsg = "Contest deadline passed. Do you still want to add?"
                                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfirmToUpdate('" & strMsg & "');", True)
                                Exit Sub
                            End If
                            btnUpdate_Click(btnUpdate, New EventArgs)
                            'strSQL = "Update ExContestant set MemberID=" & HlblParentID.Text & ", ChildNumber=" & ddlChild.SelectedValue & ", ProductGroupID=" & getProductGroupID(ddlProduct.SelectedValue) & ", ProductGroupCode='" & getProductGroupcode(ddlProduct.SelectedValue) & "', ProductID=" & ddlProduct.SelectedValue & ", ProductCode='" & ddlProduct.SelectedItem.Text & "', NewDeadline='" & txtDeadline.Text & "', ModifyDate=Getdate(), ModifiedBy=" & Session("LoginID") & " WHERE ExcontestID= " & hlblExContestantID.Text
                            'If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQL) > 0 Then
                            '    clear()
                            '    lblErr.Text = "Updated Successfully"
                            'Else
                            '    lblErr.Text = "Error in Updating records."
                            'End If
                            'GetRecords()
                            'btnSubmit.Text = "Add"
                        Else

                            'Updated on 18-04-2014 
                            '1) If badge numbers were generated, No registration will be allowed
                            '2) If todate >= the contest date,No registration will be allowed

                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Contest where EventID = " & ddlEvent.SelectedValue & " And NsfChapterID =" & ddlChapter.SelectedValue & " And Contest_Year =" & ddlEventYear.SelectedValue & " And  ProductID = " & ddlProduct.SelectedValue & " and Contestdate < GETDATE() ") > 0 Then 'Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null and GETDATE() > Cn.ContestDate ") > 0 Then ' C.PaymentReference is Not Null 'and C.PaymentReference is Null 
                                lblErr.Text = " Contest Date is over."
                            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & " and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and (C.Score1 is not null or C.score2 is not null or C.Score3 is not null) and C.BadgeNumber is not null") > 0 Then ' C.PaymentReference is Not Null
                                lblErr.Text = "Duplicates cannot be created as the child has participated in the Contest."
                            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & " and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.BadgeNumber is not null") > 0 Then ' C.PaymentReference is Not Null
                                lblErr.Text = " BadgeNumbers were generated/Contestant Record [with Payment] exists for the selections made."
                            Else
                                lblErr.Text = "Records cannot be updated."
                            End If

                            'If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and C.Score1 is null and C.score2 is null and C.Score3 is null") > 0 Then
                            '    lblErr.Text = " Contest is not yet over."
                            'ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant C Inner Join Contest Cn on C.ContestID =Cn.ContestID and C.ContestYear=Cn.Contest_Year where C.ChildNumber =" & ddlChild.SelectedValue & " And C.ChapterID = " & ddlChapter.SelectedValue & " AND C.ContestYear =" & ddlEventYear.SelectedValue & "  AND C.EventID = " & ddlEvent.SelectedValue & "  and C.PaymentReference is Not Null AND C.ProductID = " & ddlProduct.SelectedValue & " and (C.Score1 is not null or C.score2 is not null or C.Score3 is null)") > 0 Then
                            '    lblErr.Text = "Duplicates cannot be created as the child has participated in the Contest."
                            'Else
                            '    lblErr.Text = "Contestant Record [with Payment] exists for selections made."
                            'End If
                        End If
                    End If
                End If
            End If

        Catch ex As Exception
            lblErr.Text = lblErr.Text & " <br> " & ex.ToString()
        End Try
    End Sub


    Private Sub GetRecords()
        Try
            'Pnl3Msg.Text = ""
            'lblErr.Text = ""
            Dim dsRecords As New DataSet

            Dim strsql As String = "SELECT Ex.ExContestID, Ex.ChapterID, Ex.ContestYear, Ex.EventID, Ex.MemberID, Ex.ChildNumber, Ex.ProductGroupID, Ex.ProductGroupCode, Ex.ProductID, Ex.ProductCode, Ex.NewDeadline, Ex.CreateDate, Ex.CreatedBy, Ex.ModifyDate, Ex.ModifiedBy,I.FirstName + ' ' + I.LastName as ParentName,I.Email, C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName, CASE WHEN CNST.contestant_id IS NULL then 'N' ELSE 'Y' END as Paid,ISNULL(EXE.cnt,0) as cnt FROM ExContestant Ex INNER JOIN IndSpouse I ON I.AutoMemberID=Ex.MemberID INNER JOIN Child C ON C.ChildNumber = Ex.ChildNumber Left Join Contestant Cnst ON Cnst.ChildNumber=C.ChildNumber AND Cnst.ProductId=Ex.ProductId AND Cnst.PaymentReference is Not Null AND Cnst.EventId=Ex.EventID AND Cnst.ContestYear=Ex.ContestYear Left Join (select COUNT(Ex1.ExContestID) as cnt,Ex1.ExContestID from ExContestant Ex1 Inner Join Contestant C1 on C1.ChildNumber = ex1.ChildNumber and C1.PaymentReference is Not Null and c1.ParentID = ex1.MemberID and c1.ContestYear = ex1.ContestYear and c1.EventId = ex1.EventID and c1.ProductId = ex1.ProductID and Ex1.EventID=" & ddlEvent.SelectedValue & " and Ex1.ContestYear=" & ddlEventYear.SelectedValue & " and Ex1.ChapterID=" & ddlChapter.SelectedValue & " Group By Ex1.ExContestID) EXE On EXE.ExContestID = Ex.ExContestID WHERE Ex.EventID=" & ddlEvent.SelectedValue & " and Ex.ContestYear=" & ddlEventYear.SelectedValue & " and Ex.ChapterID=" & ddlChapter.SelectedValue & ""
            'Response.Write(strsql)
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridView1.DataSource = dt
            GridView1.DataBind()
            GridView1.Columns(1).Visible = True
            panel3.Visible = True
            If (dt.Rows.Count = 0) Then
                Pnl3Msg.Text = "No Records to Display"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        clear()
        hlblExContestantID.Text = transID
        Dim strsql As String = "SELECT Ex.ExContestID, Ex.ChapterID, Ex.ContestYear, Ex.EventID, Ex.MemberID, Ex.ChildNumber, Ex.ProductGroupID, Ex.ProductGroupCode, Ex.ProductID, Ex.ProductCode, Ex.NewDeadline, Ex.CreateDate, Ex.CreatedBy, Ex.ModifyDate, Ex.ModifiedBy,I.FirstName + ' ' + I.LastName as ParentName, C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName FROM ExContestant Ex INNER JOIN IndSpouse I ON I.AutoMemberID=Ex.MemberID INNER JOIN Child C ON C.ChildNumber = Ex.ChildNumber WHERE ExcontestID=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        If dsRecords.Tables(0).Rows.Count > 0 Then
            HlblParentID.Text = dsRecords.Tables(0).Rows(0)("Memberid").ToString()
            txtParent.Text = dsRecords.Tables(0).Rows(0)("ParentName").ToString() 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT     FirstName + ' ' + LastName as name FROM IndSpouse WHERE AutoMemberID = " & HlblParentID.Text & "")
            Loadchild(HlblParentID.Text, "N")
            ddlChild.SelectedIndex = ddlChild.Items.IndexOf(ddlChild.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ChildNumber").ToString()))
            'ddlChild.Enabled = False
            LoadProductID("N")
            ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ProductID").ToString()))
            'ddlProduct.Enabled = False
            txtDeadline.Text = Date.Parse(dsRecords.Tables(0).Rows(0)("NewDeadline").ToString())
        End If
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
        btnSubmit.Text = "Add"
    End Sub
    Private Sub clear()
        lblErr.Text = ""
        Pnl3Msg.Text = ""
        HlblParentID.Text = 0
        hlblExContestantID.Text = 0
        txtParent.Text = String.Empty
        txtDeadline.Text = String.Empty
        ddlChild.Items.Clear()
        ddlProduct.Items.Clear()
    End Sub

    Protected Sub GridView1_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles GridView1.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim TransID As Integer = e.Item.Cells(2).Text
        If (cmdName = "Delete") Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from ExContestant where ExContestID=" & TransID & "")
            lblErr.Text = "Item Deleted"
            clear()
            GetRecords()
        Else
            btnSubmit.Text = "Update"
            If (TransID) And TransID > 0 Then
                GetSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End If
    End Sub

    
    Protected Sub GridView1_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles GridView1.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            Dim btn1 As Button = Nothing
            btn1 = CType(e.Item.Cells(0).Controls(0), Button)
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            'm sqlstr As String = "select COUNT(Ex.ExContestID) from ExContestant Ex Inner Join Contestant C on C.ChildNumber = ex.ChildNumber and C.PaymentReference is Not Null and c.ParentID = ex.MemberID and c.ContestYear = ex.ContestYear and c.EventId = ex.EventID and c.ProductId = ex.ProductID and Ex.ExContestID=" & e.Item.Cells(2).Text
            'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlstr)
            If DataBinder.Eval(e.Item.DataItem, "cnt") > 0 Then
                'btn.Enabled = False
                'btn1.Enabled = False
            Else
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            End If
        End If
    End Sub
    Protected Sub btnSave_Click(sender As Object, e As EventArgs) Handles btnSave.Click
        Dim strSQL As String
        strSQL = "Insert into ExContestant(ChapterID, ContestYear, EventID, MemberID, ChildNumber, ProductGroupID, ProductGroupCode, ProductID, ProductCode, NewDeadline, CreateDate, CreatedBy) Values ("
        strSQL = strSQL & ddlChapter.SelectedValue & "," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & "," & HlblParentID.Text & "," & ddlChild.SelectedValue & "," & getProductGroupID(ddlProduct.SelectedValue) & ",'" & getProductGroupcode(ddlProduct.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "','" & txtDeadline.Text & "',GetDate()," & Session("LoginID") & ")"
        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQL) > 0 Then
            lblErr.Text = "Inserted Successfully"
        Else
            lblErr.Text = "Error in Inserting records."
        End If
        GetRecords()
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Dim strSQL As String
        strSQL = "Update ExContestant set MemberID=" & HlblParentID.Text & ", ChildNumber=" & ddlChild.SelectedValue & ", ProductGroupID=" & getProductGroupID(ddlProduct.SelectedValue) & ", ProductGroupCode='" & getProductGroupcode(ddlProduct.SelectedValue) & "', ProductID=" & ddlProduct.SelectedValue & ", ProductCode='" & ddlProduct.SelectedItem.Text & "', NewDeadline='" & txtDeadline.Text & "', ModifyDate=Getdate(), ModifiedBy=" & Session("LoginID") & " WHERE ExcontestID= " & hlblExContestantID.Text
        If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQL) > 0 Then
            clear()
            lblErr.Text = "Updated Successfully"
        Else
            lblErr.Text = "Error in Updating records."
        End If
        GetRecords()
        btnSubmit.Text = "Add"
    End Sub
End Class
