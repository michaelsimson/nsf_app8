<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="DownloadCoachPapers.aspx.cs" Inherits="DownloadCoachPapers" Title="Download Coach Papers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left" >
     	<asp:LinkButton ID="lnkredirect" runat="server" CssClass="btn_02" 
            onclick="lnkredirect_Click">Back to Parent Functions Page</asp:LinkButton>
        <br />
          </div>
        <asp:Panel ID="Panel3" runat="server" 
        style="text-align: center; font-size: x-large;" Font-Bold="True" 
        ForeColor="#009933">
            <asp:Label ID="lbltitle" runat="server" Font-Size="Large" 
                Text="Download Papers - Online Coaching"></asp:Label>
            <br />
        </asp:Panel>
        <asp:Panel ID="Panel1" runat="server" 
        style="text-align: center">
            <br />
            <asp:Label ID="lblMainError" runat="server" Font-Bold="True" 
                ForeColor="#FF3300" Visible="False"></asp:Label>
        </asp:Panel>
        <asp:Panel ID="Panel2" runat="server">
       <center >
            <table style="width: 100%; margin-top: 0px;">
                <tr>
                    <td style="width: 140px; height: 24px;">
                        &nbsp;</td>
                    <td style="height: 24px; text-align: center;" colspan="6">
                    </td>
                    <td style="height: 24px;">
                        </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        &nbsp;</td>
                    <td style="width: 61px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                    <td style="width: 121px">
                        &nbsp;</td>
                    <td colspan="2">
                        &nbsp;</td>
                    <td style="width: 118px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        </td>
                   <td bgcolor="Honeydew" style="width: 94px; text-align: left;">
                        <b>Event Year</b></td>
                     <td bgcolor="Honeydew" style="width: 94px; text-align: left;">
                        <b>Semester</b></td>
                    <td bgcolor="Honeydew" style="width: 94px; text-align: left;">
                        <b>Student Name</b></td>
                    <td bgcolor="Honeydew" style="width: 121px; text-align: left;">
                        <b>Product Group Code</b></td>
                    <td style="font-weight: 700; width: 78px;" bgcolor="Honeydew">
                        Product Code</td>
                    <td style="font-weight: 700; width: 141px;" bgcolor="Honeydew">
                        Level</td>
                    <td style="width: 118px; ">
                    </td>
                    <td>
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px; height: 9px">
                        </td>
                    <td style="width: 61px; height: 9px">
                        <asp:DropDownList ID="ddlEventYear" runat="server" AutoPostBack="True" 
                            Height="22px" onselectedindexchanged="ddlEventYear_SelectedIndexChanged" 
                            Width="85px">
                        </asp:DropDownList>
                        </td>
                      <td style="width: 61px; height: 9px">
                        <asp:DropDownList ID="ddlSemester" runat="server" AutoPostBack="True" 
                            Height="22px" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" 
                            Width="85px">
                        </asp:DropDownList>
                        </td>
                    <td style="width: 94px; height: 9px">
                        <asp:DropDownList ID="ddlChildName" runat="server" AutoPostBack="True" 
                            Height="22px" onselectedindexchanged="ddlChildName_SelectedIndexChanged" 
                            Width="269px">
                        </asp:DropDownList>
                    </td>
                    <td style="width: 121px; height: 9px">
                        <asp:DropDownList ID="ddlproductgroup" runat="server" AutoPostBack="true" 
                            Height="22px" OnSelectedIndexChanged="ddlFlrvProductGroup_SelectedIndexChanged" 
                            Width="148px">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 9px; width: 78px;">
                        <asp:DropDownList ID="ddlproduct" runat="server" Height="22px" Width="148px" 
                            AutoPostBack="True" onselectedindexchanged="ddlproduct_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td style="height: 9px; width: 141px;">
                        <asp:DropDownList ID="ddlLevel" runat="server" Width="140px" Height="22px">               
                        </asp:DropDownList>
                    </td>
                    <td style="width: 118px; height: 9px">
                        </td>
                    <td style="height: 9px">
                    </td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        &nbsp;</td>
                    <td style="width: 61px">
                        &nbsp;</td>
                    <td colspan="5">
                        <asp:Button ID="btnSearch" runat="server" onclick="btnSearch_Click" 
                            Text="Search" Width="91px" style="text-align: left" />
                        <asp:Label ID="Label1" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td>
                        &nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 140px">
                        &nbsp;</td>
                    <td colspan="2">
                        &nbsp;</td>
                    <td style="width: 121px; text-align: left;">
                        &nbsp;</td>
                    <td colspan="2">
                        &nbsp;</td>
                    <td style="width: 118px">
                        &nbsp;</td>
                    <td>
                        &nbsp;</td>
                </tr>
            </table>
        </center>
        </asp:Panel>
        
        <asp:HiddenField ID="hdnPgroup" runat="server" />
        
        <asp:GridView ID="gvTestPapers" runat="server" AutoGenerateColumns="False" 
                    Height="80px" o="gvTestPapers_RowDataBound" 
                    OnRowCommand="gvTestPapers_RowCommand" 
                    OnSelectedIndexChanged="gvTestPapers_SelectedIndexChanged" 
                    style="text-align: center" Width="907px">
                    <Columns>
                        <asp:ButtonField ButtonType="Image" CommandName="Download" 
                            HeaderText="Download" ShowHeader="True" Text="Download" 
                            ImageUrl="~/Images/download.png">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" HorizontalAlign="Center" 
                                BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" Wrap="True" />
                        </asp:ButtonField>
                        <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId" 
                            SortExpression="CoachPaperId">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" 
                                BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="QPaperId" HeaderText="QPaperId" 
                            SortExpression="QPaperId">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventYear" HeaderText="EventYear" 
                            SortExpression="EventYear">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductId" HeaderText="ProductId" 
                            SortExpression="ProductId">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="EventId" HeaderText="EventId" 
                            SortExpression="EventId">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="PaperType" HeaderText="PaperType" 
                            SortExpression="PaperType">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="DocType" HeaderText="DocType" 
                            SortExpression="DocType">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" 
                            SortExpression="ProductGroupCode">
                            <ControlStyle BackColor="#00CC00" />
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" 
                            SortExpression="ProductCode">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Session" HeaderText="Session" 
                            SortExpression="Session">
                            <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Sections" HeaderText="Sec.Num" 
                            SortExpression="Sections">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                        </asp:BoundField>
                        <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Center" />
                        </asp:BoundField>
                        <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" 
                            SortExpression="TestFileName">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Description" HeaderText="Description" 
                            SortExpression="Description">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                        <asp:BoundField DataField="Password" HeaderText="Password" 
                            SortExpression="Password">
                            <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                            <ItemStyle HorizontalAlign="Left" />
                        </asp:BoundField>
                    </Columns>
                </asp:GridView>
            
 
    </asp:Content>

