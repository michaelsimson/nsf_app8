﻿<%@ Page Language="C#" AutoEventWireup="true"  MasterPageFile="~/NSFMasterPage.master" CodeFile="RemoveFromSuppressionList.aspx.cs" Inherits="RemoveFromSuppressionList" %>
 <asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server"> 
      <div>
          <asp:HyperLink runat="server" ID="HyperLink1" CssClass="btn_02" Text="Back to Main Page" NavigateUrl="~/Maintest.aspx"></asp:HyperLink>&nbsp;&nbsp;&nbsp;					    
            <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                runat="server">
                <strong>Remove My Email from Suppression List


                <br />
                <br />
                <br />


                </strong>
                        </div>
          <div id="gvView" runat="server">
          <center>
              <asp:GridView ID="gvEmailRemovalList" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" OnRowDataBound="gvEmailRemovalList_RowDataBound">
              <Columns>
                  <asp:TemplateField HeaderText="FirstName">
                      <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%#Eval("FirstName") %>'></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="LastName">
                      <ItemTemplate>
                    <asp:Label ID="Label43" runat="server" Text='<%#Eval("LastName") %>'></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateField>
                 <%-- <asp:TemplateField HeaderText="Email">
                      <ItemTemplate>
                    <asp:Label ID="Label44" runat="server" Text='<%#Eval("Email") %>'></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateField>--%>
                  <asp:BoundField DataField="Email" HeaderText="Email" ItemStyle-Width="70" />
                  <asp:TemplateField HeaderText=" Flag in Profile">
                        <ItemTemplate>
                        <asp:Label ID="NewsLetter" runat="server" Text='<%#Eval("NewsLetter") %>'></asp:Label>
                        </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Flag in Mail Server">
                      <ItemTemplate>
                    <asp:Label ID="ComplaintType" runat="server" Text='<%#Eval("ComplaintType") %>'></asp:Label>
                    </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Date">
                      <ItemTemplate>
                    <asp:Label ID="Label47" runat="server" Text='<%#Eval("Date") %>'></asp:Label>
                </ItemTemplate>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Request to Remove">
                      <ItemTemplate>
                          <asp:CheckBox ID="ckallowRemove" runat="server" te/>Yes
                </ItemTemplate>
                  </asp:TemplateField>
              </Columns>
              </asp:GridView>
              <asp:Label ID="lblAbuseAlert" runat="server" Text="Label" Visible="false"></asp:Label>
              <br />
              <asp:Button ID="btnSubmit" runat="server" Text="Submit" Enabled="false" OnClick="btnSubmit_Click" />
              <br />
              </center></div>
          

            <%--<table>
                <tr><td></td>



                </tr>
                


            </table>--%>


        </div>
</asp:Content>