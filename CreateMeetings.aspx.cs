﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;



public partial class CreateMeetings : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        lblSuccess.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblerr.Text = "";
            if (!IsPostBack)
            {

                LoadEvent(ddEvent);

                // TestCreateTrainingSession();
            }
        }
    }
    public void GetJoinUrlMeetingTest(string Name, string email, string RegsiterID)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "<attendeeName>" + Name + "</attendeeName>";
        strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
        strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
        strXML += "<RegID>" + RegsiterID + "</RegID>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }


    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {

        if (RbtnNonCoachRegistration.Checked == true)
        {
            if (validatemeeting() == "1")
            {
                CretaeMeetingsRecurringDynamic(txtUserID.Text, txtPWD.Text, "Term", 25, txtDate.Text, txtTime.Text);
            }
            else
            {
                validatemeeting();
            }
        }
        else
        {
            if (btnCreateMeeting.Text == "Update Meeting")
            {



                // UpdateTrainingSession(hdnWebExID.Value, hdnWebExPwd.Value, "Term", 20, txtDate.Text, txtTime.Text, ddlDay.SelectedValue, txtTime.Text, txtEndTime.Text);
                string strBeginTime = string.Empty;
                string strEndTime = string.Empty;
                strBeginTime = txtTime.Text;
                strEndTime = txtEndTime.Text;

                string year = ddYear.SelectedValue;
                TimeSpan EndSessTime = GetTimeFromString(strEndTime, 30);

                string strEndSessTime = EndSessTime.ToString();
                TimeSpan TsStartSessTime = GetTimeFromStringSubtract(strBeginTime, 0);
                string strStartTime = TsStartSessTime.ToString();



                string cmdText = string.Empty;
                cmdText = "select distinct UserID,Pwd,VRoom from CalSignUp where EventYear=2015 and Accepted='Y' and [Begin] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and [End] not between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and UserID not in (select distinct UserID from CalSignUp where EventYear=2015 and Accepted='Y' and ([Begin] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' or [End] between '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "') and UserID is not null and day='" + ddlDay.SelectedValue + "') order by Vroom ASC";
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                DataSet ds1 = new DataSet();
                cmdText = "select WC.UserID,WC.PWD from WebConfLog WC   where  WC.[BeginTime] between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' or wc.EndTime between  '" + strStartTime.Substring(0, 5) + "' and '" + strEndSessTime.Substring(0, 5) + "' and  WC.eventyear = '" + year + "' and (SessionType='Substitute' or SessionType='Makeup') and WC.MemberID in(select CMemberID from CoachReg where EventYear='" + year + "' and Approved='Y') order by PWD ASC";

                ds1 = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                string WebExUserID = string.Empty;
                string WebExUserPwd = string.Empty;
                string Vroom = string.Empty;
                string Uid = string.Empty;

                if (ds.Tables[0].Rows.Count > 0)
                {
                    //ddlVRoom.Items.Clear()
                    if (ds1.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr1 in ds1.Tables[0].Rows)
                        {
                            int count = ds.Tables[0].Rows.Count;
                            for (int i = 0; i < count; i++)
                            {
                                if (ds.Tables[0].Rows[i]["UserID"].ToString() != dr1["UserID"].ToString())
                                {
                                    WebExUserID = ds.Tables[0].Rows[i]["UserID"].ToString();
                                    WebExUserPwd = ds.Tables[0].Rows[i]["Pwd"].ToString();

                                    Vroom = ds.Tables[0].Rows[i]["Vroom"].ToString();
                                }
                                else
                                {
                                    ds.Tables[0].Rows[i].Delete();
                                    ds.Tables[0].AcceptChanges();
                                    count = count - 1;
                                }

                            }
                            //foreach (DataRow dr2 in ds.Tables[0].Rows)
                            //{


                            //}
                        }
                    }
                    else
                    {
                        foreach (DataRow dr1 in ds.Tables[0].Rows)
                        {
                            WebExUserID = dr1["UserID"].ToString();
                            WebExUserPwd = dr1["Pwd"].ToString();
                            Vroom = dr1["Vroom"].ToString();
                        }
                    }
                    //foreach (DataRow dr1 in ds.Tables[0].Rows)
                    //{
                    //    if (ds1.Tables[0].Rows.Count > 0)
                    //    {
                    //        foreach (DataRow dr2 in ds1.Tables[0].Rows)
                    //        {

                    //            if (dr1["UserID"].ToString() != dr2["UserID"].ToString())
                    //            {
                    //                WebExUserID = dr1["UserID"].ToString();
                    //                WebExUserPwd = dr1["Pwd"].ToString();

                    //                Vroom = dr1["Vroom"].ToString();
                    //            }
                    //        }
                    //    }
                    //    else
                    //    {
                    //        WebExUserID = dr1["UserID"].ToString();
                    //        WebExUserPwd = dr1["Pwd"].ToString();
                    //        Vroom = dr1["Vroom"].ToString();
                    //    }
                    //}
                }
                if (WebExUserID != "")
                {
                    cmdText = "update CalSignup set Time='" + txtOriginalTime.Text + "', [Begin]='" + txtTime.Text + "', [End]='" + txtEndTime.Text + "', Vroom='" + Vroom + "', UserID='" + WebExUserID + "', Pwd='" + WebExUserPwd + "', Day='" + ddlDay.SelectedValue + "' where MeetingKey='" + hdnSessionKey.Value + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


                    WebRequest request = WebRequest.Create(strXMLServer);
                    // Set the Method property of the request to POST.
                    request.Method = "POST";
                    // Set the ContentType property of the WebRequest.
                    request.ContentType = "application/x-www-form-urlencoded";

                    // Create POST data and convert it to a byte array.
                    string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


                    strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
                    strXML += "<header>\r\n";
                    strXML += "<securityContext>\r\n";
                    strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
                    strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";
                    strXML += "<siteName>northsouth</siteName>\r\n";
                    strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
                    strXML += "</securityContext>\r\n";
                    strXML += "</header>\r\n";
                    strXML += "<body>\r\n";
                    strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.DelTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

                    strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";

                    strXML += "</bodyContent>\r\n";
                    strXML += "</body>\r\n";
                    strXML += "</serv:message>\r\n";
                    byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

                    // Set the ContentLength property of the WebRequest.
                    request.ContentLength = byteArray.Length;

                    // Get the request stream.
                    Stream dataStream = request.GetRequestStream();
                    // Write the data to the request stream.
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    // Close the Stream object.
                    dataStream.Close();
                    // Get the response.
                    WebResponse response = request.GetResponse();

                    // Get the stream containing content returned by the server.
                    dataStream = response.GetResponseStream();
                    XmlDocument xmlReply = null;
                    if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
                    {

                        xmlReply = new XmlDocument();
                        xmlReply.Load(dataStream);

                    }
                    //string result = ProcessMeetingHostJoinURLResponse(xmlReply);
                    //lblMsg3.Text = result;
                    //lblSuccess.Text = "Meeting Cancelled Successfully";

                    cmdText = "delete from WebConfLog where SessionKey='" + hdnSessionKey.Value + "'";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);


                    cmdText = "update Calsignup set Meetingkey=null where MeetingKey='" + hdnSessionKey.Value + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    ScheduleTrainingCenterAll();
                    btnCreateMeeting.Text = "Create Meeting";
                }
                else
                {
                    lblerr.Text = "No virtual rooms are available at this time.";
                }
            }
            else if (btnCreateMeeting.Text == "Create Meeting")
            {
                if (validatemeeting() == "1")
                {
                    ScheduleTrainingCenterAll();
                }
                else
                {
                    validatemeeting();

                }
            }
        }



    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }

    private string ProcessMeetingResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                string sessionKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:sessionkey", manager).InnerText;
                hdnSessionKey.Value = sessionKey;
                //string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:meetingkey", manager).InnerText;
                //hdnSessionKey.Value = sessionKey;
                //sb.Append("Meeting Key:" + meetingKey + "<br/>");

                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                ////hostCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                //string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                ////attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                hdnException.Value = error;
            }
            else
            {
                // lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    private bool SendEmail(string sMailTo, MailMessage mmMailMsg)
    {
        // ' ''(1) Rename the mail to
        //  ''If mmMailMsg.To.Count > 0 Then mmMailMsg.To.RemoveAt(0)
        //  ''mmMailMsg.To.Add(sMailTo)
        // (3) Create the SmtpClient object
        SmtpClient smtp = new SmtpClient();
        // (4) Send the MailMessage (will use the Web.config settings)
        smtp.Host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost");
        // "espresso.extremezone.com"
        try
        {
            smtp.Send(mmMailMsg);
        }
        catch (Exception ex)
        {
            return false;
        }
        return true;
    }


    public void registerMeetings()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.RegisterMeetingAttendee\">\r\n";//
        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>Michael</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>sz</city>";
        strXML += "<country>china</country>";
        strXML += "</address>";
        //strXML += "<phones>0</phones>";
        strXML += "<email>michael.simson@capestart.com</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        //strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>649484914</sessionKey>";
        strXML += "</attendees>";
        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>dino</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>sz</city>";
        strXML += "<country>china</country>";
        strXML += "</address>";
        // strXML += "<phones>0</phones>";
        strXML += "<email>dinold.jeeva@capestart.com</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        //strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>649484914</sessionKey>";
        strXML += "</attendees>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingAttendeeResponse(xmlReply);
        ///lblMsg1.Text = result;
    }



    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
        }
        else if (Session["RoleID"].ToString() == "88")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y')";
        }


        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + ")";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 1;
                ddlProduct.Enabled = false;
                if (Session["RoleID"].ToString() == "88")
                {
                    //fillCoach();
                }

            }
            else
            {

                ddlProduct.Enabled = true;
            }

        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();

    }

    public void fillCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Accepted='Y' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlCoach.Items.Insert(1, new ListItem("All", "All"));
            }
            else if (ds.Tables[0].Rows.Count == 1)
            {
                ddlCoach.SelectedValue = Session["LoginId"].ToString();
            }

        }

        //if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
        //{
        //    ddlCoach.Items.Insert(0, new ListItem("All", "All"));
        //    ddlCoach.SelectedValue = "All";
        //    ddlCoach.Enabled = false;
        //}
        //else if (ddlProductGroup.SelectedValue == "All" || ddlProduct.SelectedValue == "All")
        //{
        //    ddlCoach.Items.Insert(0, new ListItem("All", "All"));
        //    ddlCoach.SelectedValue = "All";
        //    ddlCoach.Enabled = false;
        //}
        //else
        //{
        //    cmdText = "select (ID.FirstName+' '+ ID.LastName) as Name,ID.AutoMemberID from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.ProductID=" + ddlProduct.SelectedValue + "  and V.Accepted='Y' order by ID.Lastname ASC";

        //    //ddlCoach.Enabled = true;
        //}
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    public void loadLevel()
    {


        if (ddlProductGroup.SelectedValue == "41")
        {
            ddlLevel.Items.Clear();
            ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
            ddlLevel.Items.Insert(2, new ListItem("Senior", "Senior"));
        }
        else if (ddlProductGroup.SelectedValue == "42")
        {
            //ddlObject.Enabled = False
            ddlLevel.Items.Clear();
            ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(1, new ListItem("Junior", "Junior"));
            ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
            ddlLevel.Items.Insert(3, new ListItem("Senior", "Senior"));
        }
        else if (ddlProductGroup.SelectedValue == "33")
        {
            ddlLevel.Items.Clear();
            ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(1, new ListItem("One level only", "One level only"));

        }

        else
        {
            ddlLevel.Items.Clear();
            ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
            ddlLevel.Items.Insert(1, new ListItem("Beginner", "Beginner"));
            ddlLevel.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
            ddlLevel.Items.Insert(3, new ListItem("Advanced", "Advanced"));
        }
    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 1))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
                // *********Added on 26-11-2013 to disable Prepbclub for the current year
                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;
                // fillProductGroup();
                fillCoach();
                ddchapter.SelectedValue = "112";
                // **************************************************************************'
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }
            // ddlObject.SelectedIndex = 0
            // btnCreateAllSessions.Visible = true;
        }
        else
        {
            btnCreateAllSessions.Visible = false;
            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
    {
        //fillCoach();
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        // fillCoach();
    }

    private string ProcessMeetingAttendeeResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Attendee Information</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText;

                string attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText;
                //xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).RemoveAll();

                //string meetingKey1 = (xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent", manager).LastChild).InnerText;
                //hdnSessionKey.Value = meetingKey;
                sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey + "<br/>");
                hdsnRegistrationKey.Value = meetingKey;
                hdnMeetingAttendeeID.Value = attendeeID;
                //hdsnRegistrationKey1.Value = meetingKey1;
                //sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey1 + "<br/>");

                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                //hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                lblerr.Text = error;
            }
            else
            {
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }
    public void listMeeting()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.LstMeetingAttendee\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg.Text = result;
        //}
    }

    //public void GetJoinUrlMeeting(string Name, string email, string RegsiterID)
    //{
    //    string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


    //    WebRequest request = WebRequest.Create(strXMLServer);
    //    // Set the Method property of the request to POST.
    //    request.Method = "POST";
    //    // Set the ContentType property of the WebRequest.
    //    request.ContentType = "application/x-www-form-urlencoded";

    //    // Create POST data and convert it to a byte array.
    //    string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


    //    strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
    //    strXML += "<header>\r\n";
    //    strXML += "<securityContext>\r\n";
    //    strXML += "<webExID>Nsfadm</webExID>\r\n";
    //    strXML += "<password>Trainer!Adm2</password>\r\n";

    //    //strXML += "<siteID>690319</siteID>";
    //    //strXML += "<partnerID>g0webx!</partnerID>\r\n";
    //    strXML += "<siteName>northsouth</siteName>\r\n";
    //    strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
    //    strXML += "</securityContext>\r\n";
    //    strXML += "</header>\r\n";
    //    strXML += "<body>\r\n";
    //    strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

    //    strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
    //    strXML += "<attendeeName>" + Name + "</attendeeName>";
    //    strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
    //    strXML += "<meetingPW>" + hdnMeetingPwd + "</meetingPW>";
    //    strXML += "<RegID>" + RegsiterID + "</RegID>";

    //    strXML += "</bodyContent>\r\n";
    //    strXML += "</body>\r\n";
    //    strXML += "</serv:message>\r\n";
    //    byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

    //    // Set the ContentLength property of the WebRequest.
    //    request.ContentLength = byteArray.Length;

    //    // Get the request stream.
    //    Stream dataStream = request.GetRequestStream();
    //    // Write the data to the request stream.
    //    dataStream.Write(byteArray, 0, byteArray.Length);
    //    // Close the Stream object.
    //    dataStream.Close();
    //    // Get the response.
    //    WebResponse response = request.GetResponse();

    //    // Get the stream containing content returned by the server.
    //    dataStream = response.GetResponseStream();
    //    XmlDocument xmlReply = null;
    //    if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
    //    {

    //        xmlReply = new XmlDocument();
    //        xmlReply.Load(dataStream);

    //    }
    //    string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
    //    //string result = this.ProcessMeetingResponse(xmlReply);
    //    //if (!string.IsNullOrEmpty(result))
    //    //{
    //    lblMsg2.Text += result;
    //    //}
    //}
    public void GetJoinUrlMeeting(string Name, string email, string RegsiterID)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value.Trim() + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value.Trim() + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value.Trim() + "</sessionKey>\r\n";
        strXML += "<attendeeName>" + Name.Trim() + "</attendeeName>\r\n";
        strXML += "<attendeeEmail>" + email.Trim() + "</attendeeEmail>\r\n";
        strXML += "<meetingPW>" + hdnMeetingPwd.Value.Trim() + "</meetingPW>\r\n";
        strXML += "<RegID>" + RegsiterID.Trim() + "</RegID>\r\n";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }

        string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }

    public void GetHostUrlMeeting(string WebExID, string Pwd)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>webex.nsf.adm@gmail.com</email>";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        //strXML += "<meetingPWD>" + hdnMeetingPwd.Value + "</meetingPWD>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingHostJoinURLResponse(xmlReply);
        //lblMsg3.Text = result;

    }
    private string ProcessMeetingAttendeeJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                //string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:registerMeetingURL", manager).InnerXml;

                //string meetingKey1 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:inviteMeetingURL", manager).InnerXml;

                string meetingKey2 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml;
                string URL = meetingKey2.Replace("&amp;", "&");



                hdnMeetingUrl.Value = URL;
                //hdnSessionKey.Value = meetingKey;
                //sb.Append("<a href=" + meetingKey + " target='blank'>" + meetingKey + "</a></br>");
                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");

                //sb.Append("<a href=" + meetingKey2 + " target='blank'>" + meetingKey2 + "</a></br>");
                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {
                //lblMsg2.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    private string ProcessMeetingHostJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = status;
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml;


                //hdnHostURL.Value = meetingKey;
                string URL = meetingKey.Replace("&amp;", "&");
                hdnHostURL.Value = URL;

                //hdnSessionKey.Value = meetingKey;
                //sb.Append("<a href=" + meetingKey + " target='blank'>" + meetingKey + "</a></br>");
                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");

                //sb.Append("<a href=" + meetingKey2 + " target='blank'>" + meetingKey2 + "</a></br>");
                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {
                lblMsg3.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }
    public void saveMeeting()
    {
        string year = ddYear.SelectedValue;
        string eventID = ddEvent.SelectedValue;
        string chapterId = ddchapter.SelectedValue;
        string ProductGroupID = ddlProductGroup.SelectedValue;
        string ProductGroupCode = ddlProductGroup.SelectedItem.Text;
        string ProductID = ddlProduct.SelectedValue;
        string ProductCode = ddlProduct.SelectedItem.Text;
        string Phase = ddlPhase.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string Sessionno = ddlSession.SelectedValue;
        string CoachID = ddlCoach.SelectedValue;
        string MeetingPwd = txtMeetingPwd.Text;
        string Date = txtDate.Text;
        string Time = txtTime.Text;
        int Duration = Convert.ToInt32(txtDuration.Text);
        string timeZoneID = ddlTimeZone.SelectedValue;
        string TimeZone = ddlTimeZone.SelectedItem.Text;
        string userID = Session["LoginID"].ToString();

        string cmdText = "";
        cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,Date,Time,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Status)values(" + year + "," + eventID + "," + chapterId + "," + ProductGroupID + ",'" + ProductGroupCode + "'," + ProductID + ",'" + ProductCode + "'," + CoachID + ",'" + hdnSessionKey.Value + "','" + txtDate.Text + "','" + txtTime.Text + "'," + Phase + ",'" + Level + "'," + Sessionno + ",'" + MeetingPwd + "'," + Duration + "," + timeZoneID + ",'" + TimeZone + "',getDate()," + userID + ",'Active')";
        if (btnCreateMeeting.Text == "Update Meeting")
        {

            //Updatemeeting(hdnSessionKey.Value);
            UpdateTrainingSession(hdnWebExID.Value, hdnWebExPwd.Value, "Term", 20, txtDate.Text, txtTime.Text, ddlDay.SelectedValue, txtTime.Text, txtEndTime.Text);
        }
        else
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        }
        if (btnCreateMeeting.Text == "Update Meeting")
        {
            lblSuccess.Text = "meeting updated successfully";
        }
        else
        {
            lblSuccess.Text = "meeting saved successfully";
        }
        tblAddNewMeeting.Visible = false;
        fillMeetingGrid();

        //lblSuccess.ForeColor = System.Drawing.Color.Blue;
    }

    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }

        else if (tblAddNewMeetingInsert.Visible == true)
        {
            if (txtMeetingPwd.Text == "")
            {
                lblerr.Text = "Please fill Meeting Password";
                retVal = "-1";
            }

            else if (ddlTimeZone.SelectedValue == "-1")
            {
                lblerr.Text = "Please select Time Zone";
                retVal = "-1";
            }
        }

        if (RbtnNonCoachRegistration.Checked == true)
        {
            if (txtUserID.Text == "")
            {
                lblerr.Text = "Please fill WebEx UserID";
                retVal = "-1";
            }
            else if (txtPWD.Text == "")
            {
                lblerr.Text = "Please fill PWD";
                retVal = "-1";
            }
            else if (ddlPhase.SelectedValue == "-1")
            {
                lblerr.Text = "Please select Phase";
                retVal = "-1";
            }
            else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "")
            {
                lblerr.Text = "Please select Level";
                retVal = "-1";
            }
            else if (ddlSession.SelectedValue == "-1")
            {
                lblerr.Text = "Please select Session";
                retVal = "-1";
            }
            else if (ddlCoach.SelectedValue == "-1")
            {
                lblerr.Text = "Please select Coach";
                retVal = "-1";
            }
            else if (txtDate.Text == "")
            {
                lblerr.Text = "Please fill Date";
                retVal = "-1";
            }
            else if (txtTime.Text == "")
            {
                lblerr.Text = "Please fill Time";
                retVal = "-1";
            }
            else if (txtDuration.Text == "")
            {
                lblerr.Text = "Please fill Duration";
                retVal = "-1";
            }
        }

        return retVal;
    }

    public void fillMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD, case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.SessionType is null order by IP.LastName, IP.FirstName Asc";
        }
        else if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.SessionType is null  order by IP.LastName, IP.FirstName Asc";
        }
        else if (ddlProduct.SelectedValue == "All" && ddlCoach.SelectedValue == "All")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.SessionType is null order by IP.LastName, IP.FirstName Asc";
        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " AND vc.MemberID=" + ddlCoach.SelectedValue + " and VC.SessionType is null  order by IP.LastName, IP.FirstName Asc";
        }
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    trChildList.Visible = false;
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    trChildList.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
        tblAddNewMeeting.Visible = false;
        if (ddlProductGroup.SelectedValue == "All" && ddlCoach.SelectedValue == "All")
        {
            btnAddNewMeeting.Text = "Create All Sessions";
        }
        else
        {
            if (RbtnNonCoachRegistration.Checked == true)
            {
                btnAddNewMeeting.Text = "Create New Session";
            }
            else
            {
                btnAddNewMeeting.Text = "Create New Sessions";
            }
        }
        if (validateSearchmeeting() == "1")
        {
            if (RbtnCoachRegistration.Checked == true)
            {
                fillMeetingGrid();
            }
            else
            {
                fillNonCoachMeetingGrid();
            }
        }
        else
        {
            validatemeeting();
        }



    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {
        //tblAddNewMeeting.Visible = true;
        tblAddNewMeetingInsert.Visible = true;
        btnCreateMeeting.Text = "Create Meeting";
        if (RbtnNonCoachRegistration.Checked == true)
        {
            TdUserIDTitle.Visible = true;
            TdUserID.Visible = true;
            TdPWDTitle.Visible = true;
            TdPWD.Visible = true;
        }
        else
        {
            TdUserIDTitle.Visible = false;
            TdUserID.Visible = false;
            TdPWDTitle.Visible = false;
            TdPWD.Visible = false;
        }
    }
    public string validateSearchmeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }
        return retVal;
    }
    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string Time = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblOrgTime") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;

                ddlCoach.SelectedValue = MemberID;
                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;
                txtDate.Text = GrdMeeting.Rows[selIndex].Cells[15].Text;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;
                txtTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                txtOriginalTime.Text = Time;
                txtEndTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                tblAddNewMeeting.Visible = true;
                tblAddNewMeetingInsert.Visible = false;
                btnCreateMeeting.Text = "Update Meeting";
                hdnSessionKey.Value = sessionKey;
                txtEndDate.Text = GrdMeeting.Rows[selIndex].Cells[16].Text;

                ddlDay.SelectedValue = (GrdMeeting.Rows[selIndex].Cells[14].Text).ToUpper();
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnCoachname.Value = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnCoachname.Value = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                string SessionNo = GrdMeeting.Rows[selIndex].Cells[10].Text.Trim();
                hdnSessionNo.Value = SessionNo;
                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                GetHostUrlMeeting(WebExID, WebExPwd);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    lblerr.Text = hdnMeetingStatus.Value;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {

            GrdMeeting.EditIndex = e.RowIndex;
            int rowIndex = e.RowIndex;
            GrdMeeting.EditIndex = -1;
            string sessionKey = string.Empty;
            sessionKey = GrdMeeting.Rows[rowIndex].Cells[12].Text;
            string WebExID = string.Empty;
            string WebExPwd = string.Empty;
            string CoachID = string.Empty;
            WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
            WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
            CoachID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblMemberID") as Label).Text;
            hdnCoachID.Value = CoachID;
            hdnSessionKey.Value = sessionKey;
            hdnWebExID.Value = WebExID;
            hdnWebExPwd.Value = WebExPwd;
            string Level = GrdMeeting.Rows[rowIndex].Cells[9].Text;
            HdnLevel.Value = Level.Trim();
            string ProductGroupID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblProductGroupID") as Label).Text;
            string ProductID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblProductID") as Label).Text;
            hdnProductID.Value = ProductID;
            hdnProductGroupID.Value = ProductGroupID;
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);

        }
        catch (Exception ex)
        {
        }
    }

    public void CancelMeeting(string sessionKey, string WebExID, string Pwd)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.DelTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + sessionKey + "</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        //string result = ProcessMeetingHostJoinURLResponse(xmlReply);
        //lblMsg3.Text = result;
        lblSuccess.Text = "Meeting Cancelled Successfully";

        string cmdText = "delete from WebConfLog where SessionKey='" + sessionKey + "'";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        cmdText = "update CalSignUp set status=null, HostJoinURL=null,MeetingKey=null,MeetingPwd=null,UserID=null, PWD=null,VRoom=null where MeetingKey='" + sessionKey + "'";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        cmdText = "update CoachReg set status=null, AttendeeJoinURL=null,AttendeeID=null,RegisteredID=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y'";

        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

        fillMeetingGrid();
    }

    public void UpdateTrainingSession(string WebEXID, string PWD, string MeetingTitle, int Capacity, string StartDate, string Time, string Day, string BeginTime, string EndTime)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";

        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.SetTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>training</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        //strXML += "<schedule>\r\n";
        //strXML += "     <startDate>" + StartDate.Replace("-", "/") + " " + Time + "</startDate>\r\n";

        ////strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        //strXML += "     <duration>" + txtDuration.Text + "</duration>\r\n";
        //strXML += "     <timeZoneID>11</timeZoneID>\r\n";
        //strXML += "     <openTime>20</openTime>\r\n";
        //strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        //strXML += "     <confName>" + MeetingTitle + "</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";


        //strXML += "<presenters>\r\n";
        //strXML += " <participants>\r\n";
        //strXML += "     <participant>\r\n";
        //strXML += "         <person>\r\n";
        //strXML += "             <name>alternatehost1</name>\r\n";
        //strXML += "             <email>host1@test.com</email>\r\n";
        //strXML += "             <type>MEMBER</type>";
        //strXML += "         </person>\r\n";
        ////strXML += "             <role>HOST</role>\r\n";
        //strXML += "     </participant>\r\n";
        //strXML += " </participants>\r\n";
        //strXML += "</presenters>\r\n";

        strXML += "<repeat>";
        strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        strXML += "     <dayInWeek>";
        strXML += "         <day>" + ddlDay.SelectedValue.ToUpper() + "</day>";
        strXML += "     </dayInWeek>";
        strXML += "     <occurenceType>WEEKLY</occurenceType>";
        strXML += "     <expirationDate>" + (txtEndDate.Text).Replace("-", "/") + " " + txtEndTime.Text + "</expirationDate>";
        strXML += "</repeat>";
        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>training</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>10/30/2015 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += " <enableOptions>";
        strXML += "     <autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
        strXML += "     <supportBreakoutSessions>true</supportBreakoutSessions>";
        strXML += "     <presenterBreakoutSession>true</presenterBreakoutSession>";
        // strXML += "     <attendeeBreakoutSession>true</attendeeBreakoutSession>";
        strXML += " </enableOptions>";

        strXML += " <telephony>";
        strXML += "<telephonySupport>CALLIN</telephonySupport>";
        strXML += " <numPhoneLines>1</numPhoneLines>";
        // strXML += "<extTelephonyURL>String</extTelephonyURL>";
        //<extTelephonyDescription>String</extTelephonyDescription>
        //<enableTSP>false</enableTSP>
        //<tspAccountIndex>1</tspAccountIndex>
        strXML += " </telephony>";


        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        //strXML += "<status>NOT_INPROGRESS</status>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //    //lblMsg.Text = result;
        //}
        if (!string.IsNullOrEmpty(result))
        {

            //lblMsg.Text = result;

            //GetJoinUrlMeeting("Michael", "michael.simson@capestart.com", hdsnRegistrationKey.Value);
            string cmdtext = "";
            cmdtext = "Update WebConfLog set EventYear=" + ddYear.SelectedValue + ",EventID=" + ddEvent.SelectedValue + ", ChapterID=" + ddchapter.SelectedValue + ",ProductGroupID=" + ddlProductGroup.SelectedValue + ",ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "',ProductID=" + ddlProduct.SelectedValue + ",ProductCode='" + ddlProduct.SelectedItem.Text + "',MemberID=" + ddlCoach.SelectedValue + ",StartDate='" + txtDate.Text + "',BeginTime ='" + txtTime.Text + "',Duration=" + txtDuration.Text + ", day='" + ddlDay.SelectedValue + "' where SessionKey='" + hdnSessionKey.Value + "'";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
                lblSuccess.Text = "Meeting Updated Successfully";
                fillMeetingGrid();
                tblAddNewMeeting.Visible = false;
            }
            catch (Exception ex)
            {
            }
        }
    }
    public void Updatemeeting(string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";
        //string strXMLServer = "https://go.webex.com/WBXService/XMLService";
        //string strXMLServer = "https://apidemoeu.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        //strXML += "<webExID>nsf1234</webExID>\r\n";
        //strXML += "<password>nsf1234</password>\r\n";//243585    g0webx!
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>michael.simson@capestart.com</email>";

        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.SetMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting
        strXML += "<accessControl>\r\n";
        strXML += "<meetingPassword>meetme</meetingPassword>\r\n";
        strXML += "</accessControl>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "<confName>Sample Meeting</confName>\r\n";

        strXML += "<agenda>Test</agenda>\r\n";
        strXML += "</metaData>\r\n";

        strXML += "<participants>\r\n";
        strXML += "<maxUserNumber>4</maxUserNumber>\r\n";
        strXML += "<attendees>\r\n";

        strXML += "</attendees>\r\n";

        strXML += "</participants>\r\n";

        strXML += "<attendeeOptions>";
        strXML += "<registration>TRUE</registration>";
        strXML += "</attendeeOptions>";

        strXML += "<enableOptions>\r\n";
        strXML += "<chat>true</chat>\r\n";
        strXML += "<poll>true</poll>\r\n";
        strXML += "<audioVideo>true</audioVideo>\r\n";
        strXML += "<attendeeList>TRUE</attendeeList>";
        strXML += "</enableOptions>\r\n";

        strXML += "<schedule>\r\n";
        strXML += "<startDate>" + (txtDate.Text).Replace("-", "/") + " " + txtTime.Text + "</startDate>\r\n";
        strXML += "<duration>" + txtDuration.Text + "</duration>\r\n";
        strXML += "<timeZoneID>" + ddlTimeZone.SelectedValue + "</timeZoneID>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<meetingkey>" + SessionKey + "</meetingkey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingResponse(xmlReply);
        if (!string.IsNullOrEmpty(result))
        {
            lblSuccess.Text = "Meeting Updated Successfully";
            //lblMsg.Text = result;
        }
        //GetJoinUrlMeeting("Michael", "michael.simson@capestart.com", hdsnRegistrationKey.Value);
        string cmdtext = "";
        cmdtext = "Update WebConfLog set EventYear=" + ddYear.SelectedValue + ",EventID=" + ddEvent.SelectedValue + ", ChapterID=" + ddchapter.SelectedValue + ",ProductGroupID=" + ddlProductGroup.SelectedValue + ",ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "',ProductID=" + ddlProduct.SelectedValue + ",ProductCode='" + ddlProduct.SelectedItem.Text + "',MemberID=" + ddlCoach.SelectedValue + ",StartDate='" + txtDate.Text + "',BeginTime ='" + txtTime.Text + "',Duration=" + txtDuration.Text + ",MeetingPwd='" + txtMeetingPwd.Text + "',TimeZoneID=" + ddlTimeZone.SelectedValue + ",MeetingUrl='" + hdnMeetingUrl.Value + "',TimeZone='" + ddlTimeZone.SelectedItem.Text + "' where SessionKey='" + SessionKey + "'";

        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);

        fillMeetingGrid();
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        CancelMeeting(hdnSessionKey.Value, hdnWebExID.Value, hdnWebExPwd.Value);
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = false;
        tblAddNewMeetingInsert.Visible = false;

        //fillMeetingGrid();
    }

    public void CretaeMeetingsRecurringDynamic(string WebEXID, string PWD, string MeetingTitle, int Capacity, string StartDate, string Time)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>" + txtMeetingPwd.Text + "</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        strXML += "<schedule>\r\n";
        strXML += "     <startDate>" + StartDate + " " + Time + "</startDate>\r\n";

        //strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        strXML += "     <duration>" + txtDuration.Text + "</duration>\r\n";
        //strXML += "     <duration>240</duration>\r\n";
        strXML += "     <timeZoneID>" + ddlTimeZone.SelectedValue + "</timeZoneID>\r\n";
        strXML += "     <openTime>20</openTime>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "     <confName>" + MeetingTitle + "</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";


        //strXML += "<repeat>";
        //strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        //strXML += "     <dayInWeek>";
        //strXML += "         <day>THURSDAY</day>";
        //strXML += "     </dayInWeek>";
        //strXML += "     <occurenceType>WEEKLY</occurenceType>";
        //strXML += "     <expirationDate>" + EndDate + " " + EndTime + "</expirationDate>";
        //strXML += "</repeat>";

        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>" + txtMeetingPwd.Text + "</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>09/20/2015 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingResponse(xmlReply);

        if (!string.IsNullOrEmpty(result))
        {
            TestGetHostUrlMeeting(WebEXID, PWD, hdnSessionKey.Value);
            string cmdText = "";
            string meetingURL = hdnHostURL.Value;
            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Phase,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Status,Meetingurl,Day,UserID,PWD)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + Session["LoginID"].ToString() + ",'" + hdnSessionKey.Value + "','" + txtDate.Text + "','" + txtEndDate.Text + "','" + txtTime.Text + "','" + txtEndTime.Text + "'," + ddlPhase.SelectedValue + ",'" + ddlLevel.SelectedValue + "'," + ddlSession.SelectedValue + ",'" + txtMeetingPwd.Text + "'," + txtDuration.Text + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate(),4240,'Active','" + meetingURL + "','" + ddlDay.SelectedItem.Text + "','" + txtUserID.Text + "','" + txtPWD.Text + "')";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                fillNonCoachMeetingGrid();
            }
            catch (Exception ex)
            {
            }
            lblSuccess.Text = "Training session Created successfully";
        }

    }
    public void ScheduleTrainingCenterAll()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;
            string SessionCreationDate = string.Empty;
            string CoachChange = string.Empty;
            ////local
            string CurrDate = DateTime.Today.ToString("dd/MM/yyyy");
            //Server
            //String CurrDate = DateTime.Today.ToString("MM/dd/yyyy");
            DateTime dt = Convert.ToDateTime(CurrDate);
            CmdText = "select SessionCreationDate,CoachChange from WebConfControl where EventYear=" + ddYear.SelectedValue + "";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "All")
            {
                CmdText += " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    //local
                    SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("dd/MM/yyyy");
                    //Server
                    //SessionCreationDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["SessionCreationDate"].ToString()).ToString("MM/dd/yyyy");
                    CoachChange = ds.Tables[0].Rows[0]["CoachChange"].ToString();
                }
            }
            if (SessionCreationDate != "")
            {
                DateTime dtSession = Convert.ToDateTime(SessionCreationDate);
                if (dt >= dtSession && CoachChange.Trim() == "N")
                {
                    if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                    {
                        CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
                    }
                    else if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                    {
                        CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
                    }
                    else if (ddlCoach.SelectedValue == "All" && ddlProduct.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All")
                    {
                        CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
                    }
                    else
                    {
                        CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Phase,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.UserID is not null";
                    }



                    ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

                    if (ds.Tables[0] != null)
                    {
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            foreach (DataRow dr in ds.Tables[0].Rows)
                            {
                                count++;

                                string WebExID = dr["UserID"].ToString();
                                string Pwd = dr["PWD"].ToString();
                                int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                                string ScheduleType = dr["ScheduleType"].ToString();

                                string year = dr["EventYear"].ToString();
                                string eventID = dr["EventID"].ToString();
                                string chapterId = ddchapter.SelectedValue;
                                string ProductGroupID = dr["ProductGroupID"].ToString();
                                string ProductGroupCode = dr["ProductGroupCode"].ToString();
                                string ProductID = dr["ProductID"].ToString();
                                string ProductCode = dr["ProductCode"].ToString();
                                string Phase = dr["Phase"].ToString();
                                string Level = dr["Level"].ToString();
                                string Sessionno = dr["SessionNo"].ToString();
                                string CoachID = dr["MemberID"].ToString();
                                string CoachName = dr["CoachName"].ToString();

                                string MeetingPwd = "training";

                                string Date = txtDate.Text;
                                string Time = dr["Time"].ToString();
                                string Day = dr["Day"].ToString();
                                string BeginTime = dr["Begin"].ToString();
                                string EndTime = dr["End"].ToString();
                                string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                                if (BeginTime == "")
                                {
                                    BeginTime = "20:00:00";
                                }
                                if (EndTime == "")
                                {
                                    EndTime = "21:00:00";
                                }

                                if (txtDuration.Text != "")
                                {
                                    int Duration = Convert.ToInt32(txtDuration.Text);
                                }
                                string timeZoneID = string.Empty;
                                if (tblAddNewMeetingInsert.Visible == true)
                                {
                                    timeZoneID = DDLTimeZoneList.SelectedValue;
                                }
                                else
                                {
                                    timeZoneID = ddlTimeZone.SelectedValue;
                                }
                                string TimeZone = ddlTimeZone.SelectedItem.Text;
                                string SignUpId = dr["SignupID"].ToString();
                                double Mins = 0.0;
                                DateTime dFrom;
                                DateTime dTo;
                                string sDateFrom = BeginTime;
                                string sDateTo = EndTime;
                                if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                                {
                                    TimeSpan TS = dTo - dFrom;
                                    Mins = TS.TotalMinutes;

                                }
                                string durationHrs = Mins.ToString("0");
                                if (durationHrs.IndexOf("-") > -1)
                                {
                                    durationHrs = "188";
                                }

                                if (timeZoneID == "4")
                                {
                                    TimeZone = "EST/EDT – Eastern";
                                }
                                else if (timeZoneID == "7")
                                {
                                    TimeZone = "CST/CDT - Central";
                                }
                                else if (timeZoneID == "6")
                                {
                                    TimeZone = "MST/MDT - Mountain";
                                }
                                string userID = Session["LoginID"].ToString();


                                if (dr["MeetingKey"].ToString() == "")
                                {



                                    CreateTrainingSession(WebExID, Pwd, ScheduleType, Capacity, startDate.Replace("-", "/"), Time, Day, BeginTime, EndTime, EndDate.Replace("-", "/"), durationHrs, MeetingPwd, CoachName, ProductCode);
                                    GetHostUrlMeeting(WebExID, Pwd);
                                    if (hdnMeetingStatus.Value == "SUCCESS")
                                    {
                                        string meetingURL = (hdnHostURL.Value == "0" ? "" : hdnHostURL.Value);

                                        string cmdText = "";

                                        cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," + year + "," + eventID + "," + chapterId + "," + ProductGroupID + ",'" + ProductGroupCode + "'," + ProductID + ",'" + ProductCode + "','" + startDate + "','" + EndDate + "','" + BeginTime + "','" + EndTime + "'," + durationHrs + "," + timeZoneID + ",'" + TimeZone + "','" + SignUpId + "'," + CoachID + ",'" + Phase + "','" + Level + "','" + Sessionno + "','" + meetingURL + "','" + hdnSessionKey.Value + "','" + MeetingPwd + "','Active','" + Day + "','" + userID + "','" + WebExID + "','" + Pwd + "'";

                                        DataSet objDs = new DataSet();
                                        objDs = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                                        if (null != objDs && objDs.Tables.Count > 0)
                                        {
                                            if (objDs.Tables[0].Rows.Count > 0)
                                            {
                                                if (Convert.ToInt32(objDs.Tables[0].Rows[0]["Retval"].ToString()) > 0)
                                                {
                                                    lblSuccess.Text = "Training session Created successfully";
                                                    string ChidName = string.Empty;
                                                    string Email = string.Empty;
                                                    string City = string.Empty;
                                                    string Country = string.Empty;
                                                    string ChildNumber = string.Empty;
                                                    string CoachRegID = string.Empty;
                                                    hdnWebExID.Value = WebExID;
                                                    hdnWebExPwd.Value = Pwd;

                                                    DataSet dsChild = new DataSet();

                                                    string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + CoachID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + CoachID + " and Approved='Y' and Level='" + Level + "') and CR.Level='" + Level + "'";



                                                    dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                                                    if (null != dsChild && dsChild.Tables.Count > 0)
                                                    {
                                                        if (dsChild.Tables[0].Rows.Count > 0)
                                                        {
                                                            foreach (DataRow drChild in dsChild.Tables[0].Rows)
                                                            {
                                                                ChidName = drChild["Name"].ToString();
                                                                Email = drChild["WebExEmail"].ToString();
                                                                if (Email.IndexOf(';') > 0)
                                                                {
                                                                    Email = Email.Substring(0, Email.IndexOf(';'));
                                                                }
                                                                City = drChild["City"].ToString();
                                                                Country = drChild["Country"].ToString();
                                                                ChildNumber = drChild["ChildNumber"].ToString();
                                                                CoachRegID = drChild["CoachRegID"].ToString();
                                                                registerMeetingsAttendees(ChidName, Email, City, Country);
                                                                if (hdnMeetingStatus.Value == "SUCCESS")
                                                                {
                                                                    GetJoinUrlMeeting(ChidName, Email.Trim(), hdsnRegistrationKey.Value);

                                                                    string CmdChildUpdateText = "update CoachReg set AttendeeJoinURL='" + hdnMeetingUrl.Value + "', RegisteredID=" + hdsnRegistrationKey.Value + ", AttendeeID=" + hdnMeetingAttendeeID.Value + ", Status='Active',ModifyDate=GetDate(), ModifiedBy='" + userID + "' where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + CoachID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and Approved='Y' and CoachRegID='" + CoachRegID + "'";

                                                                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                                                                }
                                                            }
                                                        }
                                                    }

                                                }
                                                else
                                                {
                                                    lblSuccess.Text = objDs.Tables[0].Rows[0]["Retval"].ToString();
                                                }
                                            }
                                        }


                                        lblSuccess.Text = "Training session Created successfully";
                                    }
                                    else
                                    {
                                        lblerr.Text = hdnException.Value;
                                    }
                                }
                                else
                                {
                                    lblerr.Text = "Duplicate exists..!";
                                }
                            }
                            fillMeetingGrid();
                        }
                        else
                        {
                            lblerr.Text = "No child is assigned for the selected Coach..";
                        }
                    }
                }
                else
                {
                    if (dt < dtSession)
                    {
                        lblerr.Text = "This application can only be run on " + SessionCreationDate + "";
                    }
                    else if (CoachChange.Trim() == "Y")
                    {
                        lblerr.Text = "This application cannot be run since the coach change flag is not set to N";
                    }

                }
            }
            else
            {
                lblerr.Text = "This application can only be run on.....";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    private TimeSpan GetTimeFromString1(string timeString)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.TimeOfDay;
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    public void CreateTrainingSession(string WebEXID, string PWD, string MeetingTitle, int Capacity, string StartDate, string Time, string Day, string BeginTime, string EndTime, string EndDate, string Duration, string MeetingPwd, string CoachName, string ProductCode)
    {
        string MTitle = CoachName + " - " + ProductCode;
        //string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        //WebRequest request = WebRequest.Create(strXMLServer);

        //request.Method = "POST";

        //request.ContentType = "application/x-www-form-urlencoded";


        //string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        //strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        //strXML += "<header>\r\n";
        //strXML += "<securityContext>\r\n";
        //strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        //strXML += "<password>" + PWD + "</password>\r\n";
        //strXML += "<siteName>northsouth</siteName>\r\n";
        //strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "</securityContext>\r\n";
        //strXML += "</header>\r\n";
        //strXML += "<body>\r\n";
        //strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        //strXML += "<accessControl>\r\n";
        ////strXML += "  <listing>PUBLIC</listing>\r\n";
        //strXML += "  <sessionPassword>" + txtMeetingPwd.Text + "</sessionPassword>\r\n";
        //strXML += "</accessControl>\r\n";

        //strXML += "<schedule>\r\n";
        //strXML += "     <startDate>" + StartDate + " " + Time + "</startDate>\r\n";


        //strXML += "     <duration>" + Duration + "</duration>\r\n";

        //strXML += "     <timeZoneID>" + ddlTimeZone.SelectedValue + "</timeZoneID>\r\n";
        //strXML += "     <openTime>20</openTime>\r\n";
        //strXML += "</schedule>\r\n";

        //strXML += "<metaData>\r\n";
        //strXML += "     <confName>" + MeetingTitle + "</confName>\r\n";
        //strXML += "     <agenda>agenda 1</agenda>\r\n";
        //strXML += "     <description>Training</description>\r\n";
        //strXML += "     <greeting>greeting</greeting>\r\n";
        //strXML += "     <location>location</location>\r\n";
        //strXML += "     <invitation>invitation</invitation>\r\n";
        //strXML += "</metaData>\r\n";

        //strXML += "<enableOptions>\r\n";
        //strXML += "<chat>true</chat>\r\n";
        //strXML += "<poll>true</poll>\r\n";
        //strXML += "<audioVideo>true</audioVideo>\r\n";
        //strXML += "<attendeeList>TRUE</attendeeList>";
        //strXML += "</enableOptions>\r\n";
        ////strXML += "<presenters>\r\n";
        ////strXML += " <participants>\r\n";
        ////strXML += "     <participant>\r\n";
        ////strXML += "         <person>\r\n";
        ////strXML += "             <name>alternatehost1</name>\r\n";
        ////strXML += "             <email>host1@test.com</email>\r\n";
        ////strXML += "             <type>MEMBER</type>";
        ////strXML += "         </person>\r\n";
        //////strXML += "             <role>HOST</role>\r\n";
        ////strXML += "     </participant>\r\n";
        ////strXML += " </participants>\r\n";
        ////strXML += "</presenters>\r\n";

        ////strXML += "<repeat>";
        ////strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        ////strXML += "     <dayInWeek>";
        ////strXML += "         <day>THURSDAY</day>";
        ////strXML += "     </dayInWeek>";
        ////strXML += "     <occurenceType>WEEKLY</occurenceType>";
        ////strXML += "     <expirationDate>" + EndDate + " " + EndTime + "</expirationDate>";
        ////strXML += "</repeat>";

        //strXML += "<attendeeOptions>\r\n";
        //strXML += "      <request>true</request>\r\n";
        //strXML += "      <registration>true</registration>\r\n";
        //strXML += "      <auto>true</auto>\r\n";
        //strXML += "      <registrationPWD>" + txtMeetingPwd.Text + "</registrationPWD>\r\n";
        //strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        //strXML += "      <registrationCloseDate>10/30/2015 12:00:00";
        //strXML += "      </registrationCloseDate>\r\n";
        //strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        //strXML += "</attendeeOptions>";

        //strXML += "</bodyContent>\r\n";
        //strXML += "</body>\r\n";
        //strXML += "</serv:message>\r\n";


        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>training</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        DateTime dt = DateTime.Now;
        string strDate = dt.Month + "/" + dt.Day + "/" + dt.Year + " " + dt.Hour + ":" + dt.Minute + ":" + dt.Second;
        DateTime dtG2 = Convert.ToDateTime(dt);
        string strDateG2 = dt.Month + "/" + dt.Day + "/" + dt.Year + " " + Time;
        DateTime dtG3 = Convert.ToDateTime(strDateG2);
        if (dtG3 > dtG2)
        {
            strDate = dt.Month + "/" + dt.Day + "/" + dt.Year;

        }
        else
        {
            dt = DateTime.Now.AddDays(1);
            strDate = dt.Month + "/" + dt.Day + "/" + dt.Year;
        }

        // StartDate = "11/29/2015";
        //Time = "03:10:10";
        EndDate = "04/10/2016";
        strXML += "<schedule>\r\n";
        //strXML += "     <startDate>" + StartDate + " " + Time + "</startDate>\r\n";
        strXML += "     <startDate>" + strDate + " " + Time + "</startDate>\r\n";

        //strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        strXML += "     <duration>" + Duration + "</duration>\r\n";
        //strXML += "     <duration>240</duration>\r\n";
        strXML += "     <timeZoneID>11</timeZoneID>\r\n";
        strXML += "     <openTime>11</openTime>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "     <confName>" + MTitle + "</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";


        //strXML += "<presenters>\r\n";
        //strXML += " <participants>\r\n";
        //strXML += "     <participant>\r\n";
        //strXML += "         <person>\r\n";
        //strXML += "             <name>alternatehost1</name>\r\n";
        //strXML += "             <email>host1@test.com</email>\r\n";
        //strXML += "             <type>MEMBER</type>";
        //strXML += "         </person>\r\n";
        ////strXML += "             <role>HOST</role>\r\n";
        //strXML += "     </participant>\r\n";
        //strXML += " </participants>\r\n";
        //strXML += "</presenters>\r\n";

        ////strXML += "<repeat>";
        ////strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        ////strXML += "     <dayInWeek>";
        ////strXML += "         <day>THURSDAY</day>";
        ////strXML += "     </dayInWeek>";
        ////strXML += "     <occurenceType>WEEKLY</occurenceType>";
        ////strXML += "     <expirationDate>" + EndDate + " " + EndTime + "</expirationDate>";
        ////strXML += "</repeat>";

        strXML += "<repeat>";
        strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        strXML += "     <dayInWeek>";
        strXML += "         <day>" + Day.ToUpper() + "</day>";
        strXML += "     </dayInWeek>";
        strXML += "     <occurenceType>WEEKLY</occurenceType>";
        strXML += "     <expirationDate>" + EndDate + " " + EndTime + "</expirationDate>";
        strXML += "</repeat>";

        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>training</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>04/30/2016 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += " <enableOptions>";
        strXML += "     <autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
        strXML += "     <supportBreakoutSessions>true</supportBreakoutSessions>";
        strXML += "     <presenterBreakoutSession>true</presenterBreakoutSession>";
        // strXML += "     <attendeeBreakoutSession>true</attendeeBreakoutSession>";
        //strXML="  <trainingSessionRecord>false</trainingSessionRecord>";
        strXML += " </enableOptions>";

        strXML += " <telephony>";
        strXML += "<telephonySupport>CALLIN</telephonySupport>";
        strXML += " <numPhoneLines>1</numPhoneLines>";
        // strXML += "<extTelephonyURL>String</extTelephonyURL>";
        //<extTelephonyDescription>String</extTelephonyDescription>
        //<enableTSP>false</enableTSP>
        //<tspAccountIndex>1</tspAccountIndex>
        strXML += " </telephony>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }



        string result = this.ProcessMeetingResponse(xmlReply);
        if (!string.IsNullOrEmpty(result))
        {
            if (result == "FAILURE")
            {
                lblerr.Text = result;
                hdnException.Value = result;
            }
            //lblMsg.Text = result;
        }
    }

    public void CreateMeetingAttendee()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.DelMeetingAttendee\">\r\n";// Delete Attendee
        strXML += "<attendeeID>13118243</attendeeID>";
        strXML += "<sessionKey>642005722</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }

    }

    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string SessionNo = string.Empty;
                SessionNo = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblSessionNo") as Label).Text;
                hdnSessionNo.Value = SessionNo;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }

                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {

                    GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

                    string CmdText = "update CoachReg set AttendeeJoinURL='" + hdnMeetingUrl.Value + "', RegisteredID=" + hdsnRegistrationKey.Value + ", AttendeeID=" + hdnMeetingAttendeeID.Value + ", Status='Active' where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {

                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;

                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                //if (Session["LoginID"] != null)
                //{
                //    LoginSessionID = Session["LoginID"].ToString();
                //}
                //if (Session["StudentID"] != null)
                //{
                //    LoginSessionChildID = Session["StudentID"].ToString();
                //}
                //if (LoginSessionID == PMemberID)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                //else if (LoginSessionChildID == ChildNumber)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                if (countEmail > 0)
                {
                    string status = GetTrainingSessions(hdnWebExID.Value, hdnWebExPwd.Value, hdnSessionKey.Value);
                    if (status == "INPROGRESS")
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                    else
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" + ProductCode + "','" + hdnCoachname.Value + "');", true);
                        //lblerr.Text = "Meeting Not In-Progress";

                        // System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                    }
                }
                else
                {
                    lblerr.Text = "Only authorized child can join into the training session.";
                }
            }
            else if (e.CommandName == "ReRegister")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                GrdStudentsList.Rows[selIndex].BackColor = System.Drawing.Color.Gray;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string RegID = string.Empty;
                RegID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblRegID") as Label).Text;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                string ProductGroupCode = string.Empty;
                string ProductCode = string.Empty;
                ProductGroupCode = GrdStudentsList.Rows[selIndex].Cells[9].Text;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                // DelMeetingAttendee(hdnSessionKey.Value, hdnWebExID.Value, hdnWebExPwd.Value, Email);
                // registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
                //if (hdnMeetingStatus.Value == "SUCCESS")
                //{
                hdsnRegistrationKey.Value = RegID;
                GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

                // string CmdText = "update CoachReg set AttendeeJoinURL='" + hdnMeetingUrl.Value + "', RegisteredID=" + hdsnRegistrationKey.Value + ", AttendeeID=" + hdnMeetingAttendeeID.Value + ", Status='Active' where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y'";

                // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                // lblSuccess.Text = "Selected child '" + ChidName + "' Re-registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                // fillStudentList(MemberID, ProductGroupCode.Trim(), ProductCode.Trim());
                // }

            }
        }
        catch (Exception ex)
        {
        }
    }
    public void registerMeetingsAttendees(string Name, string Email, string City, string Country)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.RegisterMeetingAttendee\">\r\n";//

        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>" + Name + "</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>" + City + "</city>";
        strXML += "<country>" + Country + "</country>";
        strXML += "</address>";
        //strXML += "<phones>0</phones>";
        strXML += "<email>" + Email + "</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        // strXML += "<sessionNum>1</sessionNum>";
        // strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "</attendees>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingAttendeeResponse(xmlReply);
        ///lblMsg1.Text = result;
    }

    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {

        trChildList.Visible = true;
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level, CR.SessionNo from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + HdnLevel.Value + "' and CR.SessionNo=" + hdnSessionNo.Value + " order by C1.Last_Name, C1.First_Name Asc";


        try
        {


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "";
                    btnClosetable2.Visible = true;
                    btnSwitchStudent.Visible = true;
                    for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                    {
                        if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                        }
                    }
                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "No record found";
                    btnClosetable2.Visible = false;
                    btnSwitchStudent.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }
    protected void btnSwitchStudent_Click(object sender, EventArgs e)
    {


        fillChild();
    }
    public void fillChild()
    {
        string cmdText = "";
        cmdText = "select CH.FIRST_NAME +' '+CH.LAST_NAME as Name,C.ChildNumber from CoachReg C inner join child CH on(C.ChildNumber=CH.ChildNumber) where C.EventYear=" + ddYear.SelectedValue + " and C.EventID=" + ddEvent.SelectedValue + " and C.ProductGroupID=" + ddlProductGroup.SelectedValue + " and C.ProductID=" + ddlProduct.SelectedValue + " and C.Approved='Y'  and C.CMemberID in (select ID.AutoMemberID from CalSignUP V left join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.ProductID=" + ddlProduct.SelectedValue + "  and V.Accepted='Y')";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlChildrenList.DataSource = ds;
            ddlChildrenList.DataTextField = "Name";
            ddlChildrenList.DataValueField = "ChildNumber";
            ddlChildrenList.DataBind();
            ddlChildrenList.Items.Insert(0, new ListItem("Select", "0"));
            ddlChildrenList.Items.Insert(1, new ListItem("All", "All"));
        }
    }

    protected void ddlChildrenList_SelectedIndexChanged(object sender, EventArgs e)
    {
        string cmdText = "";
        cmdText = "select C.CMemberID from CoachReg C  where C.EventYear=2014 and C.EventID=13 and C.ProductGroupID=31 and C.ProductID=65 and C.ChildNumber=" + ddlChildrenList.SelectedValue + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            string MemberID = string.Empty;
            if (ds.Tables[0].Rows.Count > 0)
            {
                MemberID = ds.Tables[0].Rows[0]["CMemberID"].ToString();
                cmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber=" + ddlChildrenList.SelectedValue + " and C1.Approved='Y'";
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GrdStudentsList.DataSource = ds;
                        GrdStudentsList.DataBind();
                        lblChildMsg.Text = "";
                        btnClosetable2.Visible = true;
                        btnSwitchStudent.Visible = true;
                    }
                    else
                    {
                        GrdStudentsList.DataSource = ds;
                        GrdStudentsList.DataBind();
                        lblChildMsg.Text = "No record found";
                        btnClosetable2.Visible = false;
                        btnSwitchStudent.Visible = false;
                    }
                }
            }
        }
    }
    public void DelMeetingAttendee(string sessionKey, string WebExID, string Pwd, string email)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.DelMeetingAttendee\">\r\n";

        strXML += "<attendeeEmail>\r\n";
        strXML += "<email>" + email + "</email>\r\n";
        strXML += "<sessionKey>" + sessionKey + "</sessionKey>\r\n";
        strXML += "</attendeeEmail>\r\n";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        request.ContentLength = byteArray.Length;


        Stream dataStream = request.GetRequestStream();

        dataStream.Write(byteArray, 0, byteArray.Length);

        dataStream.Close();

        WebResponse response = request.GetResponse();


        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }

    }
    protected void btnCreateAllSessions_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = false;
        tblAddNewMeetingInsert.Visible = true;
        btnCreateMeeting.Text = "Create All Meeting";
    }

    public void GetTrainingSession()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>NSFSix</webExID>\r\n";
        strXML += "<password>Trainer!6</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>640445016</sessionKey>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        request.ContentLength = byteArray.Length;


        Stream dataStream = request.GetRequestStream();

        dataStream.Write(byteArray, 0, byteArray.Length);

        dataStream.Close();

        WebResponse response = request.GetResponse();


        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }

    }


    public void TestCreateTrainingSession()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";

        strXML += "<webExID>NSFSeven</webExID>\r\n";
        strXML += "<password>Trainer!7</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.CreateTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<accessControl>\r\n";
        //strXML += "  <listing>PUBLIC</listing>\r\n";
        strXML += "  <sessionPassword>meetme</sessionPassword>\r\n";
        strXML += "</accessControl>\r\n";

        strXML += "<schedule>\r\n";
        strXML += "     <startDate>10/03/2015 05:00:10</startDate>\r\n";

        //strXML += "     <timeZone>GMT-12:00, Dateline (Eniwetok)</timeZone>\r\n";
        strXML += "     <duration>10</duration>\r\n";
        //strXML += "     <duration>240</duration>\r\n";
        strXML += "     <timeZoneID>11</timeZoneID>\r\n";
        strXML += "     <openTime>20</openTime>\r\n";
        strXML += "</schedule>\r\n";

        strXML += "<metaData>\r\n";
        strXML += "     <confName>term</confName>\r\n";
        strXML += "     <agenda>agenda 1</agenda>\r\n";
        strXML += "     <description>Training</description>\r\n";
        strXML += "     <greeting>greeting</greeting>\r\n";
        strXML += "     <location>location</location>\r\n";
        strXML += "     <invitation>invitation</invitation>\r\n";
        strXML += "</metaData>\r\n";


        //strXML += "<presenters>\r\n";
        //strXML += " <participants>\r\n";
        //strXML += "     <participant>\r\n";
        //strXML += "         <person>\r\n";
        //strXML += "             <name>alternatehost1</name>\r\n";
        //strXML += "             <email>host1@test.com</email>\r\n";
        //strXML += "             <type>MEMBER</type>";
        //strXML += "         </person>\r\n";
        ////strXML += "             <role>HOST</role>\r\n";
        //strXML += "     </participant>\r\n";
        //strXML += " </participants>\r\n";
        //strXML += "</presenters>\r\n";

        //strXML += "<repeat>";
        //strXML += "     <repeatType>MULTIPLE_SESSION</repeatType>";
        //strXML += "     <dayInWeek>";
        //strXML += "         <day>THURSDAY</day>";
        //strXML += "     </dayInWeek>";
        //strXML += "     <occurenceType>WEEKLY</occurenceType>";
        //strXML += "     <expirationDate>" + EndDate + " " + EndTime + "</expirationDate>";
        //strXML += "</repeat>";

        strXML += "<attendeeOptions>\r\n";
        strXML += "      <request>true</request>\r\n";
        strXML += "      <registration>true</registration>\r\n";
        strXML += "      <auto>true</auto>\r\n";
        strXML += "      <registrationPWD>meetme</registrationPWD>\r\n";
        strXML += "      <maxRegistrations>200</maxRegistrations>\r\n";
        strXML += "      <registrationCloseDate>09/30/2015 12:00:00";
        strXML += "      </registrationCloseDate>\r\n";
        strXML += "      <emailInvitations>true</emailInvitations>\r\n";
        strXML += "</attendeeOptions>";

        strXML += " <enableOptions>";
        strXML += "     <autoDeleteAfterMeetingEnd>true</autoDeleteAfterMeetingEnd>";
        strXML += "     <supportBreakoutSessions>true</supportBreakoutSessions>";
        strXML += "     <presenterBreakoutSession>true</presenterBreakoutSession>";
        // strXML += "     <attendeeBreakoutSession>true</attendeeBreakoutSession>";
        strXML += " </enableOptions>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }

    }


    public void TestGetHostUrlMeeting(string WebExID, string Pwd, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        //strXML += "<email>webex.nsf.adm@gmail.com</email>";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GethosturlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        //strXML += "<meetingPW>meetme</meetingPW>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string Result = ProcessMeetingHostJoinURLResponse(xmlReply);
    }

    public void GetURlAPI()
    {
        string text = "https://northsouth.webex.com/nothsouth/w.php?AT=KM&MK=313856883&PID=mY7ck6lr82MeCSnQ2Mi6Ig&WID=Nsfadm&PW=Trainer!Adm2";
    }

    public void GetAPIVersion()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.ep.GetAPIVersion\">\r\n";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
    }
    public void GetTrainingSessions()
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>Nsfadm</webExID>\r\n";
        strXML += "<password>Trainer!Adm2</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>649989584</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingTestResponse(xmlReply);
        }
        if (result == "INPROGRESS")
        {

        }
    }

    private string ProcessTrainingTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                string sessionKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;
                hdnSessionKey.Value = sessionKey;

            }

        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }
    protected void RbtnNonCoachRegistration_CheckedChanged(object sender, EventArgs e)
    {
        if (RbtnNonCoachRegistration.Checked == true)
        {
            tdCoach.Visible = true;
            tdCoachTitle.Visible = true;

            fillProductGroup();
            ddlProduct.Items.Clear();
            spnTable1Title.InnerText = "Table 1: Non-Coach Meeeting List";
            tblAddNewMeeting.Visible = false;
            GrdMeeting.Visible = false;
            spnTable1Title.Visible = false;
            btnAddNewMeeting.Visible = false;

        }
    }
    protected void RbtnCoachRegistration_CheckedChanged(object sender, EventArgs e)
    {
        if (RbtnCoachRegistration.Checked == true)
        {
            tdCoach.Visible = true;
            tdCoachTitle.Visible = true;
            TdUserIDTitle.Visible = false;
            TdUserID.Visible = false;
            TdPWDTitle.Visible = false;
            TdPWD.Visible = false;
            fillProductGroup();
            ddlProduct.Items.Clear();
            spnTable1Title.InnerText = "Table 1: Coach Meeeting List";
            tblAddNewMeeting.Visible = false;
            GrdMeeting.Visible = false;
            spnTable1Title.Visible = false;
            btnAddNewMeeting.Visible = false;

        }
    }

    public void fillNonCoachMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.MemberID=" + ddlCoach.SelectedValue + "  order by VC.ProductGroupCode";
        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=" + ddEvent.SelectedValue + " and VC.ChapterID=" + ddchapter.SelectedValue + " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + " and VC.ProductID=" + ddlProduct.SelectedValue + " and VC.MemberID not in(select MemberID from CalSignup)  order by VC.ProductGroupCode";
        }

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    trChildList.Visible = false;
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    trChildList.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void FillNonCoach()
    {
        string cmdText = string.Empty;
        cmdText = "select distinct V.MemberID,IP.FirstName+' '+ IP.LastName as Name from Volunteer V inner join IndSpouse IP on (V.MemberId=IP.AutoMemberID) where V.MemberID not in(select MemberID from CalSignup)";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
        ddlCoach.DataValueField = "MemberID";
        ddlCoach.DataTextField = "Name";
        ddlCoach.DataSource = ds;
        ddlCoach.DataBind();
        ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
    }
    public string GetTrainingSessions(string WebExID, string PWD, string SessionKey)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";

        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + PWD + "</password>\r\n";

        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.GetTrainingSession\">\r\n";
        strXML += "<sessionKey>" + SessionKey + "</sessionKey>";
        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        string result = string.Empty;

        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);
            result = ProcessTrainingStatusTestResponse(xmlReply);
        }

        if (result == "INPROGRESS")
        {
            //string cmdUpdateText = "update WebConfLog set Status='InProgress' where SessionKey='" + SessionKey + "'";
            //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

        }
        else
        {
            // lblerr.Text = "Meeting Not In-Progress";

        }
        return result;

    }
    private string ProcessTrainingStatusTestResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        string MeetingStatus = string.Empty;
        string startTime = string.Empty;
        string day = string.Empty;
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession");
            manager.AddNamespace("sess", "http://www.webex.com/schemas/2002/06/service/session");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
            string ConfName = string.Empty;
            if (status == "SUCCESS")
            {

                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Information</b>:</br>");

                MeetingStatus = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:status", manager).InnerText;

                startTime = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/sess:schedule/sess:startDate", manager).InnerText;
                day = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:repeat/train:dayInWeek/train:day", manager).InnerText;
                startTime = startTime.Substring(10, 9);
                double Mins = 0.0;
                DateTime dFrom = DateTime.Now;
                DateTime dTo = DateTime.Now;
                string sDateFrom = startTime;

                string today = DateTime.Today.DayOfWeek.ToString();
                if (today.ToLower() == day.ToLower())
                {
                    string sDateTo = DateTime.Now.ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);

                    //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                    //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);

                    DateTime easternTime = DateTime.Now;
                    string strEasternTime = easternTime.ToShortDateString();
                    strEasternTime = strEasternTime + " " + sDateFrom;
                    DateTime dtEasternTime = Convert.ToDateTime(strEasternTime);
                    DateTime easternTimeNow = DateTime.Now.AddHours(1);

                    TimeSpan TS = dtEasternTime - easternTimeNow;
                    Mins = TS.TotalMinutes;


                }
                else
                {
                    for (int i = 1; i <= 7; i++)
                    {
                        today = DateTime.UtcNow.AddDays(i).DayOfWeek.ToString();
                        if (today.ToLower() == day.ToLower())
                        {
                            dTo = DateTime.UtcNow.AddDays(i);
                            //TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
                            //DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(dTo, easternZone);
                            DateTime easternTime = DateTime.Now.AddDays(i);
                            string sDateTo = DateTime.Now.AddDays(i).ToString("HH:mm:ss", System.Globalization.DateTimeFormatInfo.InvariantInfo);
                            string targetDateTime = easternTime.ToShortDateString() + " " + sDateFrom;
                            //DateTime easternTimeNow = TimeZoneInfo.ConvertTimeFromUtc(dFrom, easternZone);
                            DateTime easternTimeNow = DateTime.Now.AddHours(1);

                            DateTime dtTargetTime = Convert.ToDateTime(targetDateTime);
                            TimeSpan TS = dtTargetTime - easternTimeNow;
                            Mins = TS.TotalMinutes;


                        }
                    }
                }
                string DueMins = Mins.ToString().Substring(0, Mins.ToString().IndexOf("."));
                hdnSessionStartTime.Value = startTime;
                hdnStartMins.Value = Convert.ToString(DueMins);
            }

        }
        catch (Exception e)
        {
            // sb.Append("Error: " + e.Message);
        }

        return MeetingStatus;
    }

    //public void tempUpdateSession(string WebEXID, string PWD, string MeetingKey)
    //{
    //    string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";

    //    WebRequest request = WebRequest.Create(strXMLServer);

    //    request.Method = "POST";

    //    request.ContentType = "application/x-www-form-urlencoded";


    //    string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


    //    strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
    //    strXML += "<header>\r\n";
    //    strXML += "<securityContext>\r\n";

    //    strXML += "<webExID>" + WebEXID + "</webExID>\r\n";
    //    strXML += "<password>" + PWD + "</password>\r\n";
    //    strXML += "<siteName>northsouth</siteName>\r\n";
    //    strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
    //    strXML += "</securityContext>\r\n";
    //    strXML += "</header>\r\n";
    //    strXML += "<body>\r\n";
    //    strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.training.SetTrainingSession\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting



    //    strXML += " <enableOptions>";

    //    strXML += "     <trainingSessionRecord>false</trainingSessionRecord>";
    //    strXML += " </enableOptions>";


    //    strXML += "<sessionKey>" + MeetingKey + "</sessionKey>";
    //    //strXML += "<status>NOT_INPROGRESS</status>";
    //    strXML += "</bodyContent>\r\n";
    //    strXML += "</body>\r\n";
    //    strXML += "</serv:message>\r\n";

    //    byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

    //    // Set the ContentLength property of the WebRequest.
    //    request.ContentLength = byteArray.Length;

    //    // Get the request stream.
    //    Stream dataStream = request.GetRequestStream();
    //    // Write the data to the request stream.
    //    dataStream.Write(byteArray, 0, byteArray.Length);
    //    // Close the Stream object.
    //    dataStream.Close();
    //    // Get the response.
    //    WebResponse response = request.GetResponse();

    //    // Get the stream containing content returned by the server.
    //    dataStream = response.GetResponseStream();
    //    XmlDocument xmlReply = null;
    //    if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
    //    {

    //        xmlReply = new XmlDocument();
    //        xmlReply.Load(dataStream);

    //    }
    //}

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCoach.SelectedValue == "All")
        {

            ddlProductGroup.Items.Insert(0, new ListItem("All", "All"));
            ddlProductGroup.SelectedValue = "All";
            ddlProductGroup.Enabled = false;

            ddlProduct.Items.Insert(0, new ListItem("All", "All"));
            ddlProduct.SelectedValue = "All";
            ddlProduct.Enabled = false;
        }
        else if (ddlCoach.SelectedValue != "All")
        {
            fillProductGroup();
        }

    }
}