﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShoppingCatalog.aspx.vb" Inherits="ShoppingCatalog" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <LINK href="Styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
    
-->
</style>
    <title>Shopping Catalog</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <table border="0" cellpadding ="2" cellspacing = "0" width ="1000">
    <tr><td align="center">
   <img src="images/trilogo.gif"  alt ="" />
	</td></tr>
	<tr>
  <td align="left"> &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSrchParent" OnClick="btnSrchParent_Click" runat="server" Text="Search Parent" Visible = "false"  />
  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="ltl1" runat="server"></asp:Literal>
  </td> </tr>
  
  <tr><td align = "center" >
    <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF Parent</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="right">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    </td>
    	    <td  align="left">					
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
  </td></tr>
  <tr><td align="center">
   <table id="trH1" runat = "server"  border="0" cellpadding ="3" cellspacing = "0" >
    <tr><td align="center">
 Select Items for the Shopping Cart</h5>
     <table border = "0" cellpadding = "3" cellspacing = "0" >
    <tr><td align ="left" >Chapter </td><td align ="left" >
        <asp:DropDownList ID="ddlChapter" Width="150px" runat="server"></asp:DropDownList> </td><td align ="left" > </td><td align ="lEvent</td><td align ="left" >
            <asp:DropDownList Width="175px" ID="ddlEvent" runat="server">
            <asp:ListItem Value="0">Select Event</asp:ListItem>
            <asp:ListItem Value="1">National Finals</asp:ListItem>
            <asp:ListItem Value="2">Chapter Contests</asp:ListItem>
            <asp:ListItem Value="3">Workshop</asp:ListItem> 
            <asp:ListItem Value="5">Walk-a-thon</asp:ListItem> 
            <asp:ListItem Value="12">Marathon</asp:ListItem> 
            <asp:ListItem Value="9">Entertainment Program</asp:ListItem> 
            <asp:ListItem Value="10">Shop</asp:ListItem>             
            </asp:DropDownList> </td>
            <td>
                <asp:DropDownList ID="ddlEventYear" runat="server">
                </asp:DropDownList>
            </td></tr>
    </table>
    </td> </tr> 
    <tr><td align="center" >
    <asp:datagrid id="dgCatalog" OnItemDataBound="dgCatalog_ItemDataBound" runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" CellPadding="3" 
          DataKeyField="CatID" BackColor="White" BorderColor="#E7E7FF" 
          BorderStyle="None" GridLines="Horizontal">
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
			<asp:TemplateColumn HeaderText="Select"  HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
            <ItemTemplate>
  <asp:CheckBox ID="chkSelect" OnCheckedChanged="chkSelect_CheckedChanged" AutoPostBack="true" runat="server"  />        </ItemTemplate>	
                <HeaderStyle ForeColor="White" Font-Bold="true" Wrap="False"></HeaderStyle>
            <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
            </asp:TemplateColumn> 
			<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
            <asp:TextBox Width="50px" AutoPostBack="true" OnTextChanged="txtQuantity_TextChanged" ID="txtQuantity" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:TextBox>
            <asp:RangeValidator ID="RangeValidator1"  runat="server" ControlToValidate="txtQuantity"  MaximumValue="100" MinimumValue ="0" Type="Integer" ErrorMessage="*"></asp:RangeValidator>
      

        </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="CatID" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
           <asp:Label ID="lblCatID" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "CatID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Category" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblCategory" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Category") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="ShortName" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label ID="lblShortName" Runat=server CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ShortName")  %>'>
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Description" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblDescription" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Description") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblUnitPrice" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"UnitPrice","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Amount" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblAmount" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Status") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
		</td></tr>
		<tr><td align = "right" >
		  <asp:Label ID="lblTotal" runat="server"></asp:Label>	&nbsp;&nbsp;&nbsp;	
		</td> </tr>
		<tr id="trH3" runat = "server" ><td align = "center" >
		    <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick="btnClear_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" />
		</td> </tr> 
		 </table> 
     </td> </tr>
     <tr>
  <td align="center">
      <asp:Label ID="lblerr" ForeColor = "Red" runat="server"></asp:Label>
      <asp:Label ID="lblCustIndID" ForeColor="White" runat="server"></asp:Label>
            </td> </tr>
             <tr  id="trH2" runat = "server">
  <td align="center">
  <h5>Past Purchase(s)</h5>
  <asp:datagrid id="dgPaidProducts"  runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="2px" CellPadding="3" 
          DataKeyField="SaleTranID" BackColor="White" BorderColor="White" 
          BorderStyle="Ridge" GridLines="None" CellSpacing="1">
			<FooterStyle CssClass="GridFooter" BackColor="#C6C3C6" ForeColor="Black"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#9471DE" Font-Bold="True" 
                ForeColor="White"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#DEDFDE" 
                ForeColor="Black"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" 
                ForeColor="#E7E7FF" ></HeaderStyle>
			<Columns>
			<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
            <asp:Label ID="lblQuantity" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Quantity") %>'></asp:label>
       </ItemTemplate>	           											
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="CatID" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
           <asp:Label ID="lblCatID" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "CatID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Category" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblCategory" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Category") %>'>													
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="ShortName" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label ID="lblShortName" Runat=server CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ShortName")  %>'>
        </asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Description" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblDescription" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Description") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Unit Price" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblUnitPrice" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"UnitPrice","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Amount" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblAmount" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Amount","{0:c}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblChapterCode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterCode") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        
 <asp:TemplateColumn HeaderText="Event" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblEvent" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"EventName") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="SaleTranID" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblSaleTranID" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"SaleTranID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="PaymentDate" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblPaymentDate" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentDate","{0:d}") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="PaymentReference" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblPaymentReference" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentReference") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#C6C3C6"></PagerStyle>
		</asp:datagrid>
		<br />
      <asp:Label ID="lblerror" runat="server" ForeColor = "Red" ></asp:Label>
  </td> </tr> 
     </table> 
     <div align="left" >
   <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr><td width="10px" >&nbsp;
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table> 
        </div>      
       
    </div>
    </form>
</body>
</html>
