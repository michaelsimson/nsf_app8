

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CorpMatchGift.aspx.vb" Inherits="CorpMatchGift" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <div><script language="javascript" type="text/javascript">
    function PopupPicker(ctl, w, h) {
        var PopupWindow = null;
        settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
        PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
        PopupWindow.focus();
    }
		</script></div>
   
   <div style="text-align:left">
      <br />
      <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl = "VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
      &nbsp;&nbsp;&nbsp;<br />
   </div>
   <div align="center">
      <table border = "0" cellpadding = "4" cellspacing = "0" >
         <tr ID="TrMatGiftDet" runat="server" visible="false" style="width:50%">
            <td align="right" colspan="6">
               <table>
                  <tr>
                     <td align="left" colspan="2">Matching_Gift_URL:</td>
                     <td style=" width:auto">
                     <asp:Label ID="lblMatGiftURL" runat="server" Enabled="false"></asp:Label>
                       <%-- <asp:TextBox ID="txtMatGiftURL" runat="server" Enabled="false"></asp:TextBox>--%>
                     </td>
                  </tr>
                  <tr>
                     <td align="left" colspan="2">Matching_Gift_Acc_No:</td>
                     <td colspan="2">
                     <asp:Label ID="lblMatGiftAccNo" runat="server" Enabled="false"></asp:Label>
                    
                     <%--   <asp:TextBox ID="txtMatGiftAccNo" runat="server" Enabled="false"></asp:TextBox>--%>
                     </td>
                  </tr>
                  <tr>
                     <td align="left" colspan="2">Matching_Gift_User_Id:</td>
                     <td colspan="2">
                      <asp:Label ID="lblMatGiftUserId" runat="server" Enabled="false"></asp:Label>
                    
                     <%--   <asp:TextBox ID="txtMatGiftUserId" runat="server" Enabled="false"></asp:TextBox>--%>
                     </td>
                  </tr>
                  <tr>
                     <td align="left" colspan="2">Matching_Gift_Password:</td>
                     <td colspan="2">
                      <asp:Label ID="lblMatGiftPwd" runat="server" Enabled="false"></asp:Label>
                    
                       <%-- <asp:TextBox ID="txtMatGiftPwd" runat="server" Enabled="false"></asp:TextBox>--%>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <tr>
            <td align="center" colspan = "7">
               <h3>United Way, Corp and Other Contributions</h3>
            </td>
         </tr>
         <tr>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
            <td align="left"></td>
         </tr>
         <tr>
            <td align="left">Organization  </td>
            <td align="left">
               <asp:DropDownList ID="DDMatchURL" runat="server" AutoPostBack="True" Width="150px">
                  <asp:ListItem>Organization Name</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td id="Td1" align="left" runat="server">
               <asp:TextBox ID="txtOrganization" Enabled ="false" Width="150px" runat="server"  Visible="false"></asp:TextBox>
            </td>
            <td align="left">Org. Check Number</td>
            <td align="left">
               <asp:TextBox ID="txtChckNo" Enabled = "true" Width ="100px" runat="server" AutoPostBack="true"></asp:TextBox>
            </td>
            <td></td><td></td>
         </tr>
         <tr id="tremployer" runat ="server" visible ="false" >
            <td align="left">Emp / Foundation</td>
            <td align="left">
               <asp:TextBox ID="txtEmployer" Enabled ="false" Width="150px" runat="server"></asp:TextBox>
            </td>
            <td align="left">
               <asp:Button ID="btnEmployer" OnClick="btnEmployer_Click" runat="server" Text="Search" />
            </td>
            <td align="left"></td>
            <td align="left"></td>
                        <td></td><td></td>

         </tr>
         <tr>
            <td align="left">Employee Name </td>
            <td align="left">
               <asp:TextBox ID="txtEmployee" Enabled ="false" Width="150px" runat="server"></asp:TextBox>
            </td>
            <td align="left">
               <asp:Button ID="btnEmployee" onclick= "btnEmployee_Click" runat="server" Text="Search" />
               &nbsp;
               <asp:DropDownList ID="ddlEmployee" runat="server"></asp:DropDownList>
            </td>
            <td align="left">Check Date</td>
            <td align="left">
               <asp:TextBox ID="txtChkDate" Enabled="true" Width="100px" runat="server"></asp:TextBox>
                <a href="javascript:PopupPicker('<%=txtChkDate.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
    <asp:RequiredFieldValidator ControlToValidate="txtChkDate" ID="RequiredFieldValidator9" runat="server" ErrorMessage="*" ></asp:RequiredFieldValidator>

            </td>
                        <td>
                        
                        </td><td></td>

         </tr>
         <tr>
            <td align="left">Program Type  </td>
            <td align="left">
               <asp:DropDownList ID="ddlPrgmType" runat="server">
                  <asp:ListItem Value="0" Selected="True">Select Program Type</asp:ListItem>
                  <asp:ListItem>Matching Gift</asp:ListItem>
                  <asp:ListItem>Volunteering</asp:ListItem>
                  <asp:ListItem>Employee Donations</asp:ListItem>
               </asp:DropDownList>
            </td>
            <td align="left">
               <asp:Label ID="lblCorpEmpID"  ForeColor="White" runat="server"></asp:Label>
            </td>
            <td align="left">PaymentID</td>
            <td align="left">
               <%--    <asp:Button ID="btnSearchDon" OnClick="btnSearchDon_Click" runat="server" Text="Search Donations" Visible="false" />
                  --%>   
               <asp:TextBox ID="txtPaymentID" Enabled="true" Width="100px" runat="server"></asp:TextBox>
            </td>
                        <td></td><td></td>

         </tr>
         <tr>
            <td align="left">Employee Amount        </td>
            <td align="left">
               <asp:TextBox ID="txtEmpAmt" Width="150px" runat="server"></asp:TextBox>
            
<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="txtEmpAmt" Display="Dynamic" ErrorMessage="Only Numeric Values"
Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
            </td>
            <td align="left">
               <asp:Literal ID="lblEmpAmt" runat="server"></asp:Literal>
            </td>
            <td align="left">Gift Date</td>
            <td align="left">
               <asp:TextBox ID="txtGiftDate" Enabled="true" Width="100px" runat="server"></asp:TextBox>
                           <a href="javascript:PopupPicker('<%=txtGiftDate.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>


            </td>
                        <td align="left">Hours </td><td align="left"><asp:TextBox ID="txtHours" Width="100px" runat="server"></asp:TextBox>
           </td>

         </tr>
         <tr>
            <td align="left">Matching Amount        </td>
            <td align="left">
               <asp:TextBox ID="txtMatchAmt" Width="150px" runat="server"></asp:TextBox>
            </td>
            <td align="left">
               <asp:Literal ID="lblMatchAmt" runat="server"></asp:Literal>
            </td>
            <td align="left">Pay Year</td>
            <td align="left">
               <asp:DropDownList ID="ddlPayYear" runat="server"></asp:DropDownList>
            </td>
                        <td align="left">Email</td><td align="left"> <asp:TextBox ID="txtMemEmail" Width="150px" runat="server" Text=""></asp:TextBox>
                          <asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="txtMemEmail" Display="Dynamic"
										ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator><%--<br />--%>
                                
                        </td>

         </tr>
         <tr>
            <td align="left">Total Amount        </td>
            <td align="left">
               <asp:TextBox ID="txtAmount" Width="150px" runat="server"></asp:TextBox>
               <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="txtAmount" Display="Dynamic" ErrorMessage="Only Numeric Values" Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>

            </td>
            <td align="left">
               <asp:Literal ID="ltltotal" runat="server"></asp:Literal>
            </td>
            <td align="left">Pay Method</td>
            <td align="left">
               <asp:DropDownList ID="ddlPayMethod" runat="server">
                  <asp:ListItem Value="0" Selected="True">Select Pay Method</asp:ListItem>
                  <asp:ListItem>Payroll</asp:ListItem>
                  <asp:ListItem>One time</asp:ListItem>
                  <asp:ListItem>Volunteer</asp:ListItem>
               </asp:DropDownList>
            </td>
                        <td></td><td></td>

         </tr>
          <tr>
            <td align="left">   </td>
            <td align="left">
            </td>
            <td align="left">
                <br />
            </td>
            <td align="left">Employer's EmployeeID </td>
            <td align="left">
               <asp:TextBox ID="txtEmprMemID" runat="server" Text="" Enabled="false"></asp:TextBox>
            </td>
                        <td align="left">&nbsp;</td><td align="left"> <%--<br />--%>
                                
                        </td>

         </tr>
         <tr>
            <td align="left"></td>
            <td align="center" colspan ="3">
               <asp:Button ID="btnSubmit" OnClick="btnSubmit_Click" runat="server" Text="Add" />
               &nbsp; &nbsp;&nbsp; &nbsp;
               <asp:Button ID="btnClear" runat="server" Text="Clear" OnClick ="btnClear_Click" />
            </td>
            <td align="left">
               <asp:Label ID="lblDonationID" runat="server" ForeColor="White"></asp:Label>
            </td>
            
         </tr>
         <tr>
            <td align="left">&nbsp;</td>
            <td align="center" colspan ="3">
               <asp:Label ID="lblerr" runat="server" ForeColor = "Red" ></asp:Label>
            </td>
            <td align="left">&nbsp;</td>
         </tr>
         <tr id="TrPaymentIDErr" runat="server" visible="false"><td colspan="5" runat="server" align="center">
         <table>
         <tr><td align="center"><asp:Label ID="lblPayIDErr" runat="server" Text="Is PaymentID Available" ForeColor="Red"></asp:Label></td></tr>
         <tr><td align="center">
         <asp:Button ID="BtnYes" runat="server" Text="Yes" /> &nbsp;
         <asp:Button ID="BtnNo" runat="server" Text="No" />
         </td></tr>
         </table>
         
         </td></tr>
      </table>
   </div>
   <table border="0" width="900px" cellpadding ="0" cellspacing ="0">
      <tr>
         <td align="center">
            <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
               <b> Search NSF member</b>   
               <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
                     <td align="left" >
                        <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
                     <td  align ="left" >
                        <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
                     <td align="left">
                        <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
                     <td align="left" >
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" colspan = "2">
                        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>
                     </td>
                  </tr>
               </table>
               <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
               <br />
               <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
                  <b> Search Result</b>
                  <asp:GridView  HorizontalAlign="Center"   RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                     <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                        <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                        <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                        <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                        <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                        <asp:BoundField DataField="Employer" ItemStyle-Width="200px" headerText="Employer" ></asp:BoundField>
                        <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                        <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                        <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                        <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                        <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                     </Columns>
                  </asp:GridView>
               </asp:Panel>
            </asp:Panel>
         </td>
      </tr>
      <tr>
         <td align="center">
            <asp:Panel ID="pORGSearch" runat="server" Width="950px"  Visible="False">
               <b> Search Organization</b>   
               <table  border="1" runat="server" id="tblORGSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">Organization </td>
                     <td align="left" >
                        <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                     </td>
                     <td class="ItemLabel" vAlign="top" align="left">State </td>
                     <td align="left" >
                        <asp:DropDownList ID="ddlOrgState" runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" align="left">City </td>
                     <td align="left">
                        <asp:TextBox ID="txtCity" runat="server"></asp:TextBox>
                     </td>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;NSF Chapter</td>
                     <td align="left">
                        <asp:DropDownList ID="ddlNSFChapter" runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" colspan="4">
                        <asp:Button ID="btnORGSearch" runat="server" OnClick="btnORGSearch_onClick" Text="Find" CausesValidation="False" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;					
                        <asp:Button ID="btnORGClose" runat="server"  Text="Close" onclick="btnORGClose_onclick" CausesValidation="False"/>
                     </td>
                  </tr>
               </table>
               <asp:Label ID="lblORGSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
               <br />
               <asp:Panel ID="Panel5" runat="server"  Visible="False" HorizontalAlign="Center">
                  <b> Search Result</b>
                  <asp:GridView  HorizontalAlign="Center"   RowStyle-HorizontalAlign="Left"  ID="GridORG" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridORG_RowCommand" RowStyle-CssClass="SmallFont">
                     <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                        <asp:BoundField DataField="Organization_name" headerText="Organization Name" ></asp:BoundField>
                        <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                        <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                        <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                        <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                        <asp:BoundField DataField="ChapterCode" headerText="Chapter" ></asp:BoundField>
                     </Columns>
                  </asp:GridView>
               </asp:Panel>
            </asp:Panel>
         </td>
      </tr>
      <tr>
         <td align="center">
            <asp:Panel ID="pEmployerSearch" runat="server" Width="950px"  Visible="False">
               <b> Search Employer</b>   
               <table  border="1" runat="server" id="tblEmployerSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >
                  <tr>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">Employer </td>
                     <td align="left" >
                        <asp:TextBox ID="txtEmployerName" runat="server"></asp:TextBox>
                     </td>
                     <td class="ItemLabel" vAlign="top" align="left">State </td>
                     <td align="left" >
                        <asp:DropDownList ID="ddlEmployerState" runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td class="ItemLabel" vAlign="top" align="left">City </td>
                     <td align="left">
                        <asp:TextBox ID="txtEmployerCity" runat="server"></asp:TextBox>
                     </td>
                     <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;NSF Chapter</td>
                     <td align="left">
                        <asp:DropDownList ID="ddlEmployerNSFChapter" runat="server">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr>
                     <td align="Center" colspan="4">
                        <asp:Button ID="btnEmployerSearch" runat="server" OnClick="btnEmployerSearch_onClick" Text="Find" CausesValidation="False" />
                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;				
                        <asp:Button ID="btnEmployerClose" runat="server"  Text="Close" onclick="btnEmployerClose_onclick" CausesValidation="False"/>
                     </td>
                  </tr>
               </table>
               <asp:Label ID="lblEmployerSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
               <br />
               <asp:Panel ID="PanelEmployer" runat="server"  Visible="False" HorizontalAlign="Center">
                  <b> Search Result</b>  
                  <asp:GridView  HorizontalAlign="Center"   RowStyle-HorizontalAlign="Left"  ID="GridEmployer" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridEmployer_RowCommand" RowStyle-CssClass="SmallFont">
                     <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                        <asp:BoundField DataField="Organization_name" headerText="Organization Name" ></asp:BoundField>
                        <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                        <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                        <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                        <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                        <asp:BoundField DataField="ChapterCode" headerText="Chapter" ></asp:BoundField>
                     </Columns>
                  </asp:GridView>
               </asp:Panel>
            </asp:Panel>
         </td>
      </tr>
      <tr>
         <td align="center">
            <br /><br />
            <asp:Panel ID="PDonations" runat="server" Width="950px"  Visible="False">
               <b>Table 1:  Organization Remittances</b><br /> 
               <asp:GridView  HorizontalAlign="Center"   RowStyle-HorizontalAlign="Left"  ID="GridDonations" DataKeyNames="DonationId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridDonations_RowCommand" RowStyle-CssClass="SmallFont">
                  <Columns>
                     <asp:ButtonField Text="Select" />
                     <asp:BoundField DataField="DonationId" headerText="Donation Id" ></asp:BoundField>
                     <asp:BoundField DataField="Organization_name" headerText="Organization Name" ></asp:BoundField>
                     <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
                     <asp:BoundField DataField="Amount" headerText="Amount" DataFormatString="{0:c}" ></asp:BoundField>
                     <asp:BoundField DataField="DonationDate" headerText="Check Date" DataFormatString="{0:d}"  ></asp:BoundField>
                     <asp:BoundField DataField="Transaction_number" headerText="Check Number" ></asp:BoundField>
                     <asp:BoundField DataField="City" headerText="City" ></asp:BoundField>
                     <asp:BoundField DataField="State" headerText="State" ></asp:BoundField>
                  </Columns>
               </asp:GridView>
               <br /> 
               <asp:Button ID="btnCloseOrgRem" runat="server" Text="Close Table 1" />
            </asp:Panel>
         </td>
      </tr>
      <tr>
         <td align="center">
         <asp:Panel ID="PnlCorpEmp" runat="server" Width="950px"  Visible="False">
<br />
            <asp:Label ID="lblHead" runat="server" Visible = "false" ><b>Table 2: Employees behind the Payment</b></asp:Label>
            <br /> 
            <asp:Label ID="lblCorp" runat="server"></asp:Label>
            <asp:GridView  HorizontalAlign="Center"   RowStyle-HorizontalAlign="Left"  ID="GridCorp" DataKeyNames="CorpEmpID" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridCorp_RowCommand" RowStyle-CssClass="SmallFont">
               <Columns>
                  <asp:ButtonField Text="Modify" CommandName="Modify" />
                  <asp:ButtonField Text="Delete" CommandName="Delete1" />
                  <asp:BoundField DataField="CorpEmpID" headerText="CorpEmp ID" ></asp:BoundField>
                  <asp:BoundField DataField="OrgMemberID" headerText="OrgMemberID" Visible="true"  ></asp:BoundField>
                  <asp:BoundField DataField="OrgName" headerText="Organization Name" ></asp:BoundField>
                  <asp:BoundField DataField="EmployerMembID" headerText="EmployerMembID" Visible="true" ></asp:BoundField>
                  <asp:BoundField DataField="EmpName" headerText="EmpName" ></asp:BoundField>
                  <asp:BoundField DataField="MemberID" headerText="Member ID" Visible="true" ></asp:BoundField>
                  <asp:BoundField DataField="EmployeeName" headerText="EmployeeName" ></asp:BoundField>
                  <asp:BoundField DataField="ProgramType" headerText="Program Type" ></asp:BoundField>
                  <asp:BoundField DataField="OrgCheckNo" headerText="OrgCheckNo" ></asp:BoundField>
                  <asp:BoundField DataField="CheckDate" headerText="Check Date"  DataFormatString="{0:d}"   ></asp:BoundField>
                  <asp:BoundField DataField="EmpFunds" headerText="Emp Amount" DataFormatString="{0:c}"></asp:BoundField>
                  <asp:BoundField DataField="CompFunds" headerText="Match Amount" DataFormatString="{0:c}" ></asp:BoundField>
                  <asp:BoundField DataField="TotAmount" headerText="Total"  DataFormatString="{0:c}"></asp:BoundField>
                  <asp:BoundField DataField="PayYear" headerText="PayYear" ></asp:BoundField>
                  <asp:BoundField DataField="GiftDate" headerText="GiftDate"  DataFormatString="{0:d}"   ></asp:BoundField>
                  <asp:BoundField DataField="PaymentID" headerText="PaymentID" ></asp:BoundField>
                  <asp:BoundField DataField="PayMethod" headerText="PayMethod" ></asp:BoundField>
                  <asp:BoundField DataField="Hours" headerText="Hours" ></asp:BoundField>
                  <asp:BoundField DataField="Email" headerText="Email" ></asp:BoundField>
                  
               </Columns>
            </asp:GridView>
            </asp:Panel>
         </td>
      </tr>
   </table>
   <asp:Label ID="lblOrgID" ForeColor="White" runat="server"></asp:Label>
   <asp:Label ID="lblEmployerID" ForeColor="White"  runat="server"></asp:Label>
   <asp:Label ID="lblFlag"  ForeColor="White"    runat="server" ></asp:Label>
   <asp:Label ID="lblEmployeeID"  ForeColor="White"  runat="server"></asp:Label>
</asp:Content>

