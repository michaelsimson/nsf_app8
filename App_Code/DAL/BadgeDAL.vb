Imports Microsoft.VisualBasic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Namespace NorthSouth.DAL
    Public Class BadgeDAL
        Private _connectionString As String
        Private strSql As String
        ''' <summary>
        ''' Database Connection String Property
        ''' </summary>
        Public Property ConnectionString() As String
            Get
                Return _connectionString
            End Get
            Set(ByVal value As String)
                _connectionString = value
            End Set
        End Property
        ''' <summary>
        ''' BlogMessageDAL Constructor
        ''' </summary>
        ''' <param name="ConnectionString"></param>
        Public Sub New(ByVal ConnectionString As String)
            Me.ConnectionString = ConnectionString
        End Sub

        ''' <summary>
        ''' GetConnection Method to Get the Connection String 
        ''' </summary>
        ''' <returns></returns>
        Private Function GetConnection() As SqlConnection
            Return New SqlConnection(Me.ConnectionString)
        End Function

        Public Function GetContestDates(ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            strSql = "SELECT DISTINCT(CONVERT(VARCHAR,ContestDate,101)) contestdate FROM Contest "
            strSql = strSql & " WHERE Contest_year =" & ContestYear & ""
            strSql = strSql & " and nsfchapterid = " & ChapterID & " ORDER BY ContestDate"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetContestDates_Score(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestID As Integer) As DataSet
            strSql = "SELECT DISTINCT(CONVERT(VARCHAR,ContestDate,101)) contestdate FROM Contest "
            strSql = strSql & " WHERE Contest_year =" & ContestYear & ""
            strSql = strSql & " and nsfchapterid = " & ChapterID & "  and ContestID =" & ContestID & "ORDER BY ContestDate"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function

        Public Function GetChildsHavingBadges(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            'strSql = "SELECT b.first_name, b.last_name, a.badgenumber, d.chaptercode, "
            'strSql = strSql & " c.contestdate, a.ProductGroupCode, a.chapterid, "
            'strSql = strSql & " a.childnumber "
            'strSql = strSql & " from contestant a, child b, contest c, chapter d  "
            'strSql = strSql & " WHERE a.childnumber = b.childnumber and "
            'strSql = strSql & "  a.contestid = c.contestid and d.chapterid = a.chapterid and "
            'strSql = strSql & " contestdate in (" & ContestDates & ") and a.chapterid = " & ChapterID & ""
            'strSql = strSql & "  and a.contestyear = " & ContestYear & " ORDER by b.last_name, b.first_name, a.productid"

            'strSql = "SELECT DISTINCT(A.CHILDNUMBER), b.last_name,"
            'strSql = strSql & " b.first_name,d.chaptercode "
            'strSql = strSql & " from contestant a, child b, contest c,chapter d"
            'strSql = strSql & " WHERE a.badgenumber is not null and a.childnumber = b.childnumber and "
            'strSql = strSql & " a.contestid = c.contestid and "
            'strSql = strSql & " d.chapterid = a.chapterid and "
            'strSql = strSql & " contestdate in (" & ContestDates & ") and a.chapterid = " & ChapterID
            'strSql = strSql & " and a.contestyear = " & ContestYear
            'strSql = strSql & " ORDER by b.last_name,"
            'strSql = strSql & " b.first_name "
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetChildHavingBadges", prmArray)

        End Function

        Public Function GetChildsHavingBadgesFinals(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal FromRowID As String, ByVal ToRowID As String) As DataSet
            Dim prmArray(5) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter("@FromRowID", SqlDbType.VarChar, 10)
            prmArray(3).Value = FromRowID
            prmArray(3).Direction = ParameterDirection.Input

            prmArray(4) = New SqlParameter("@TORowID", SqlDbType.VarChar, 10)
            prmArray(4).Value = ToRowID
            prmArray(4).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetChildHavingBadgesFinals", prmArray)
        End Function

        Public Function ShowBadge(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal ChildNumber As Double) As DataSet
            strSql = "SELECT a.badgenumber, d.chaptercode, "
            strSql = strSql & " c.contestdate, a.ProductGroupCode, a.chapterid, "
            strSql = strSql & " a.childnumber "
            strSql = strSql & " from contestant a, child b, contest c, chapter d  "
            strSql = strSql & " WHERE a.childnumber = b.childnumber and "
            strSql = strSql & "  a.contestid = c.contestid and d.chapterid = a.chapterid and "
            strSql = strSql & " contestdate in (" & ContestDates & ") and a.chapterid = " & ChapterID & ""
            strSql = strSql & " AND A.ChildNumber=" & ChildNumber
            strSql = strSql & "  and a.contestyear = " & ContestYear
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function ShowParticipantCertificate(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            'strSql = " SELECT b.first_name,"
            'strSql = strSql & " b.last_name, a.badgenumber,contestyear, "
            'strSql = strSql & " d.chaptercode, c.contestdate,"
            'strSql = strSql & " a.productcode, e.name ProductName,a.chapterid,ContestYear,"
            'strSql = strSql & " a.childnumber from contestant a, "
            'strSql = strSql & " child b, contest c, chapter d, product e "
            'strSql = strSql & " where a.badgenumber IS NOT NULL and a.childnumber = b.childnumber "
            'strSql = strSql & " and a.contestid = c.contestid and "
            'strSql = strSql & " d.chapterid = a.chapterid and "
            'strSql = strSql & " e.productid = a.productid and "
            'strSql = strSql & " contestdate in (" & ContestDates & ") and"
            'strSql = strSql & " a.chapterid = " & ChapterID & " And a.contestyear = " & ContestYear
            'strSql = strSql & " order by b.last_name, b.first_name"
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_ShowParticipantCertificate", prmArray)


        End Function


        Public Function GetParticipantsHavingCertificates(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            'strSql = " SELECT distinct  b.first_name,"
            'strSql = strSql & " b.last_name, contestyear,  "
            'strSql = strSql & " d.chaptercode,"
            'strSql = strSql & " a.chapterid,"
            'strSql = strSql & " a.childnumber"
            'strSql = strSql & " from contestant a,  child b, contest c, chapter d, product e "
            'strSql = strSql & " where a.badgenumber IS NOT NULL and "
            'strSql = strSql & " a.childnumber = b.childnumber  and a.contestid = c.contestid and  "
            'strSql = strSql & " d.chapterid = a.chapterid and  e.productid = a.productid and"
            'strSql = strSql & " contestdate in (" & ContestDates & ") and a.chapterid = " & ChapterID & " And"
            'strSql = strSql & " a.contestyear = " & ContestYear & " order by b.last_name, b.first_name"
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetParticipantsHavingCertificates", prmArray)

        End Function

        Public Function GetParticipantsHavingCertificatesFinals(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal FromRowID As String, ByVal ToRowID As String) As DataSet
            Dim prmArray(4) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter("@FromRowID", SqlDbType.VarChar, 10)
            prmArray(3).Value = FromRowID
            prmArray(3).Direction = ParameterDirection.Input

            prmArray(4) = New SqlParameter("@TORowID", SqlDbType.VarChar, 10)
            prmArray(4).Value = ToRowID
            prmArray(4).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetParticipantsHavingCertificatesFinals", prmArray)

        End Function

        Public Function GetProductNames(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDate As String, ByVal ChildNumber As Double) As DataSet
            strSql = " Select  e.name"
            strSql = strSql & " from contestant a,  child b, contest c, chapter d, product e "
            strSql = strSql & " where a.badgenumber IS NOT NULL and "
            strSql = strSql & " a.childnumber = b.childnumber  and a.contestid = c.contestid and  "
            strSql = strSql & " d.chapterid = a.chapterid and  e.productid = a.productid and"
            strSql = strSql & " contestdate in (" & ContestDate & ") and a.chapterid = " & ChapterID
            strSql = strSql & " and a.childnumber=" & ChildNumber & " and "
            strSql = strSql & " a.contestyear = " & ContestYear
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetLeftSignatures(ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            strSql = " SELECT a.productcode, d.name, b.firstname, b.lastname, "
            strSql = strSql & " a.lefttitle, CONVERT(VARCHAR,a.contestdate,101) contestdate, c.chaptercode, "
            strSql = strSql & " a.nsfchapterid FROM Contest a, "
            strSql = strSql & " Indspouse b, chapter c, product d "
            strSql = strSql & " WHERE b.automemberid = a.LeftSignID and "
            strSql = strSql & "  c.chapterid = a.nsfchapterid and "
            strSql = strSql & " d.productid = a.productid and "
            strSql = strSql & " a.contest_year = " & ContestYear & " and "
            strSql = strSql & " a.nsfchapterid = " & ChapterID & " order by a.productid"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetLeftSignaturesList(ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            strSql = " SELECT DISTINCT b.firstname || b.lastname NAME, "
            strSql = strSql & " a.LEFTSIGNID "
            strSql = strSql & " FROM Contest a, "
            strSql = strSql & " Indspouse b, chapter c, product d "
            strSql = strSql & " WHERE b.automemberid = a.LeftSignID and "
            strSql = strSql & "  c.chapterid = a.nsfchapterid and "
            strSql = strSql & " d.productid = a.productid and "
            strSql = strSql & " a.contest_year = " & ContestYear & " and "
            strSql = strSql & " a.nsfchapterid = " & ChapterID & " order by a.productid"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetRightSignatures(ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            strSql = " SELECT a.productcode, d.name, b.firstname, b.lastname, "
            strSql = strSql & " a.righttitle,CONVERT(VARCHAR,a.contestdate,101) contestdate, c.chaptercode, "
            strSql = strSql & " a.nsfchapterid FROM Contest a, "
            strSql = strSql & " Indspouse b, chapter c, product d "
            strSql = strSql & " WHERE b.automemberid = a.RightSignID  and "
            strSql = strSql & "  c.chapterid = a.nsfchapterid and "
            strSql = strSql & " d.productid = a.productid and "
            strSql = strSql & " a.contest_year = " & ContestYear & " and "
            strSql = strSql & " a.nsfchapterid = " & ChapterID & " order by a.productid"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetAllSignatureList(ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            strSql = "SELECT DISTINCT a.contestid, a.contest_year, "
            strSql = strSql & " a.productcode, d.chaptercode, c.name, "
            strSql = strSql & " a.leftsignid, b.firstname, b.lastname, "
            strSql = strSql & " a.lefttitle, a.rightsignid, e.firstname, "
            strSql = strSql & " e.lastname, a.righttitle,b.title,e.title, c.ProductID, "
            strSql = strSql & "  isnull(( select SignFileName from Certificatesign where MemberId = a.leftsignid),'''') as LeftSignImage ,"
            strSql = strSql & "  isnull(( select SignFileName from Certificatesign where MemberId = a.Rightsignid),'''') as RightSignImage "
            strSql = strSql & " from contest a, indspouse b, product c, "
            strSql = strSql & " chapter d, indspouse e where "
            strSql = strSql & " b.automemberid = a.leftsignid and "
            strSql = strSql & " e.automemberid = a.rightsignid and "
            strSql = strSql & " c.productcode = a.productcode and "
            strSql = strSql & " c.productid = a.productid and  "
            strSql = strSql & " d.chapterid = a.nsfchapterid and "
            strSql = strSql & " a.contest_year = " & ContestYear
            strSql = strSql & " and a.nsfchapterid = " & ChapterID

            strSql = strSql & " ORDER BY c.ProductID "
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetChapterName(ByVal ChapterID As Integer) As String
            strSql = " SELECT ChapterCode FROM Chapter WHERE chapterid=" & ChapterID
            Return Convert.ToString(SqlHelper.ExecuteScalar(Me.GetConnection(), CommandType.Text, strSql))
        End Function
        Public Function GetVolunteerBadges(ByVal ChapterID As Integer) As DataSet
            strSql = " SELECT DISTINCT b.firstname, b.lastname  "
            strSql = strSql & " FROM Volunteer a, IndSpouse b, Role c, Chapter d  "
            strSql = strSql & " where a.memberid = b.automemberid And "
            strSql = strSql & "  a.Roleid = c.Roleid and d.chapterid = a.chapterid and "
            strSql = strSql & " a.chapterid =" & ChapterID & " AND EventYear=Year(GetDate())  order by b.lastname, b.firstname"
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetVolunteerCertificates(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            'strSql = " SELECT DISTINCT b.firstname, b.lastname, d.chaptercode , " & System.Configuration.ConfigurationSettings.AppSettings("Contest_Year") & " ContestYear"
            'strSql = strSql & " FROM Volunteer a, IndSpouse b, Role c, Chapter d  "
            'strSql = strSql & " where a.memberid = b.automemberid And "
            'strSql = strSql & "  a.Roleid = c.Roleid and d.chapterid = a.chapterid and "
            'strSql = strSql & " a.chapterid =" & ChapterID & "  order by b.lastname, b.firstname"
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            'ContestYear = Now.Year - 1

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetVolunteerCertificates", prmArray)

        End Function
        Public Function GetVolunteerCertificatesBlank(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            'strSql = " SELECT DISTINCT b.firstname, b.lastname, d.chaptercode , " & System.Configuration.ConfigurationSettings.AppSettings("Contest_Year") & " ContestYear"
            'strSql = strSql & " FROM Volunteer a, IndSpouse b, Role c, Chapter d  "
            'strSql = strSql & " where a.memberid = b.automemberid And "
            'strSql = strSql & "  a.Roleid = c.Roleid and d.chapterid = a.chapterid and "
            'strSql = strSql & " a.chapterid =" & ChapterID & "  order by b.lastname, b.firstname"
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            'ContestYear = Now.Year - 1

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetVolunteerCertificatesBlank", prmArray)

        End Function
        Public Function GetRankCertificates(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet

            'strSql = " select c.productcode, p.name,c.contest_year,a.chaptercode "
            'strSql = strSql & " from contest c, product p,Chapter A "
            'strSql = strSql & "  where c.productid = p.productid and "
            'strSql = strSql & " c.contest_year = " & ContestYear & " and "
            'strSql = strSql & " c.nsfchapterid= a.ChapterID AND "
            'strSql = strSql & " c.nsfchapterid = " & ChapterID & " order by c.productid "
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            Dim prmArray(2) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetRankCertificates", prmArray)

        End Function
        Public Function GetRankCertificatesPhase3(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal ContestID As Integer, ByVal TopRank As Integer) As DataSet

            'strSql = " select c.productcode, p.name,c.contest_year,a.chaptercode "
            'strSql = strSql & " from contest c, product p,Chapter A "
            'strSql = strSql & "  where c.productid = p.productid and "
            'strSql = strSql & " c.contest_year = " & ContestYear & " and "
            'strSql = strSql & " c.nsfchapterid= a.ChapterID AND "
            'strSql = strSql & " c.nsfchapterid = " & ChapterID & " order by c.productid "
            'Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)

            Dim prmArray(4) As SqlParameter
            prmArray(0) = New SqlParameter("@ContestDates", SqlDbType.VarChar, 500)
            prmArray(0).Value = ContestDates
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter("@ChapterID", SqlDbType.Int)
            prmArray(1).Value = ChapterID
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter("@ContestYear", SqlDbType.VarChar, 10)
            prmArray(2).Value = ContestYear
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter("@ContestID", SqlDbType.Int)
            prmArray(3).Value = ContestID
            prmArray(3).Direction = ParameterDirection.Input

            prmArray(4) = New SqlParameter("@TopRank", SqlDbType.Int)
            prmArray(4).Value = TopRank
            prmArray(4).Direction = ParameterDirection.Input 
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.StoredProcedure, "usp_GetRankCertificatesPhase3", prmArray)
        End Function

        Public Function SignatureNameByProductCode(ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ProductCode As String) As DataSet
            strSql = "SELECT DISTINCT  "
            strSql = strSql & "  b.firstname, b.lastname, "
            strSql = strSql & " a.lefttitle, e.firstname, "
            strSql = strSql & " e.lastname, a.righttitle,b.title ltitle,e.title rtitle,'http://www.northsouth.org/signs/'+ isnull(left_sign.SignFileName,'spacer.gif')  as LeftSignatureImage,'http://www.northsouth.org/signs/'+isnull(right_sign.SignFileName,'spacer.gif') as RightSignatureImage"
            strSql = strSql & " from contest a inner join indspouse b on b.automemberid = a.leftsignid inner join product c on c.productcode = a. productcode  "
            strSql = strSql & " inner join chapter d on d.chapterid = a.nsfchapterid inner join indspouse e on e.automemberid = a.rightsignid  "
            strSql = strSql & " Left join certificatesign left_sign on left_sign.Memberid = a.LeftSignID Left Join certificatesign right_sign on right_sign.memberid = a.RightSignID where"
            strSql = strSql & " c.ProductCode ='" & ProductCode & "' AND "
            strSql = strSql & " a.contest_year = " & ContestYear
            strSql = strSql & " and a.nsfchapterid = " & ChapterID
            Return SqlHelper.ExecuteDataset(Me.GetConnection(), CommandType.Text, strSql)
        End Function
    End Class
End Namespace