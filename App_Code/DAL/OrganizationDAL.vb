Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Namespace NorthSouth.DAL
    Public Class OrganizationDAL
        Private _connectionString As String
        Private strSql As String
        ''' <summary>
        ''' Database Connection String Property
        ''' </summary>
        Public Property ConnectionString() As String
            Get
                Return _connectionString
            End Get
            Set(ByVal value As String)
                _connectionString = value
            End Set
        End Property
        Public Sub New(ByVal ConnectionString As String)
            Me.ConnectionString = ConnectionString
        End Sub

        ''' <summary>
        ''' GetConnection Method to Get the Connection String 
        ''' </summary>
        ''' <returns></returns>
        Private Function GetConnection() As SqlConnection
            Return New SqlConnection(Me.ConnectionString)
        End Function
        ''' <summary>
        ''' Adds New Donation to Donation Table
        ''' </summary>
        ''' <returns></returns>
        Public Function AddOrganization(ByVal organization As Organization) As Integer
            strSql = " INSERT INTO organizationInfo"
            strSql = strSql & " (MEMBERID,ORGANIZATION_NAME,TITLE,"
            strSql = strSql & " FIRST_NAME,MIDDLE_INITIAL,LAST_NAME,"
            strSql = strSql & "  ADDRESS1,ADDRESS2,CITY,STATE,ZIP,"
            strSql = strSql & " COUNTRY,PHONE,FAX,	EMAIL,SecondaryEMAIL,"
            strSql = strSql & "  MATCHING_GIFT,SEND_EMAIL,REFERRED_BY,ChapterID,"
            strSql = strSql & " NSF_Chapter,SEND_NEWSLETTER,MAILING_LABEL,SEND_RECEIPT,"
            strSql = strSql & "  LIAISONPERSON,CreateDate,MemberSince,ModifyDate,"
            strSql = strSql & "  DeleteReason,DeletedFlag, irscat, bustype, sponsor, "
            strSql = strSql & "  venue, website,.Matching_Gift_URL ,Matching_Gift_Account_Number,Matching_Gift_User_Id,Matching_Gift_Password,createdBy) VALUES('" & organization.MemberID & "','"
            strSql = strSql & organization.OrganizationName & "','" & organization.Title & "','" & organization.FirstName & "',"
            strSql = strSql & " '" & organization.Initial & "','" & organization.LastName & "',"
            strSql = strSql & " '" & organization.Address1 & "','" & organization.Address2 & "',"
            strSql = strSql & " '" & organization.City & "','" & organization.State & "',"
            strSql = strSql & " '" & organization.Zip & "','" & organization.Country & "',"
            strSql = strSql & " '" & organization.Phone & "','" & organization.Fax & "',"
            strSql = strSql & " '" & organization.Email & "','" & organization.Email2 & "',"
            strSql = strSql & " '" & organization.MatchingGift & "','" & organization.SendMail & "',"
            strSql = strSql & " '" & organization.ReferredBy & "'," & organization.ChapterID & ","
            strSql = strSql & " '" & organization.NSFChapter & "','" & organization.SendNewsLetter & "',"
            strSql = strSql & " '" & organization.MailingLabel & "','" & organization.SendReceipt & "',"
            strSql = strSql & " '" & organization.LiasonPerson & "','" & organization.CreatedDate & "','" & organization.MemberSince & "',"
            strSql = strSql & " null,'','" & organization.DeletedFlag & "',"
            strSql = strSql & " '" & organization.IRSCat & "','" & organization.BusType & "',"
            strSql = strSql & " '" & organization.Sponsor & "','" & organization.Venue & "',"
            strSql = strSql & " '" & organization.WebSite & "','" & organization.Matching_Gift_URL & "','" & organization.Matching_Gift_Account_Number & "','" & organization.Matching_Gift_User_Id & "','" & organization.Matching_Gift_Password & "'," & organization.CreatedBy & " )"
            Return SqlHelper.ExecuteNonQuery(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function UpdateOrganization(ByVal organization As Organization) As Integer
            Try
                strSql = " UPDATE organizationInfo SET "
                strSql = strSql & " ORGANIZATION_NAME='" & organization.OrganizationName & "', "
                strSql = strSql & " TITLE='" & organization.Title & "', "
                strSql = strSql & " FIRST_NAME='" & organization.FirstName & "',"
                strSql = strSql & " MIDDLE_INITIAL='" & organization.Initial & "',"
                strSql = strSql & "  LAST_NAME='" & organization.LastName & "', "
                strSql = strSql & "ADDRESS1='" & organization.Address1 & "', "
                strSql = strSql & " ADDRESS2='" & organization.Address2 & "', "
                strSql = strSql & " CITY='" & organization.City & "', "
                strSql = strSql & " STATE='" & organization.State & "', "
                strSql = strSql & " ZIP='" & organization.Zip & "', "
                strSql = strSql & " COUNTRY='" & organization.Country & "', "
                strSql = strSql & " PHONE='" & organization.Phone & "', "
                strSql = strSql & " FAX='" & organization.Fax & "', "
                strSql = strSql & " DeletedFlag='No',"
                strSql = strSql & " EMAIL='" & organization.Email & "', "
                strSql = strSql & " SecondaryEMAIL='" & organization.Email2 & "', "
                strSql = strSql & " MATCHING_GIFT='" & organization.MatchingGift & "', "
                strSql = strSql & " SEND_EMAIL='" & organization.SendMail & "', "
                strSql = strSql & " REFERRED_BY='" & organization.ReferredBy & "', "
                strSql = strSql & " NSF_CHAPTER='" & organization.NSFChapter & "', "
                strSql = strSql & " ChapterID = " & organization.ChapterID & ","
                strSql = strSql & " SEND_NEWSLETTER='" & organization.SendNewsLetter & "', "
                strSql = strSql & " MAILING_LABEL='" & organization.MailingLabel & "', "
                strSql = strSql & " SEND_RECEIPT='" & organization.SendReceipt & "', "
                strSql = strSql & " LIAISONPERSON='" & organization.LiasonPerson & "', "
                strSql = strSql & " ModifyDate='" & organization.ModifyDate & "', "
                strSql = strSql & " ModifiedBy=" & organization.ModifiedBy & ", "
                strSql = strSql & " IRSCat='" & organization.IRSCat & "' ,"
                strSql = strSql & " BusType='" & organization.BusType & "' , "
                strSql = strSql & " Sponsor='" & organization.Sponsor & "' , "
                strSql = strSql & " Venue='" & organization.Venue & "' , "
                strSql = strSql & " Website='" & organization.WebSite & "', "
                strSql = strSql & " Matching_Gift_URL='" & organization.Matching_Gift_URL & "',"
                strSql = strSql & " Matching_Gift_Account_Number='" & organization.Matching_Gift_Account_Number & "',"
                strSql = strSql & " Matching_Gift_User_Id='" & organization.Matching_Gift_User_Id & "', "
                strSql = strSql & " Matching_Gift_Password='" & organization.Matching_Gift_Password & "' "
                strSql = strSql & " WHERE AutoMEMBERID = " & organization.MemberID
                Return SqlHelper.ExecuteNonQuery(Me.GetConnection(), CommandType.Text, strSql)
            Catch ex As Exception

            End Try
        End Function
        Public Function GetMaxOrganizationMemberID() As Integer
            strSql = "SELECT MAX(AutoMemberID) FROM OrganizationInfo "
            Return Convert.ToInt32(SqlHelper.ExecuteScalar(Me.GetConnection(), CommandType.Text, strSql))
        End Function
        Public Function GetOrganizations(ByVal sortField As String, ByVal chapterID As String) As DataSet
            strSql = "SELECT * FROM OrganizationInfo "
            If chapterID <> "0" Then
                strSql = strSql & " WHERE ChapterID in ( " & chapterID & ")"
            End If
            strSql = strSql & " ORDER BY "
            If sortField.ToUpper <> "ORGANIZATION_NAME" Then
                strSql = strSql & sortField & ",ORGANIZATION_NAME ASC "
            Else
                strSql = strSql & sortField
            End If
            Dim dsOrganizations As New DataSet
            Dim tableName As String() = New String(0) {}
            tableName(0) = "OrganizationInfo"
            SqlHelper.FillDataset(Me.GetConnection(), CommandType.Text, strSql, dsOrganizations, tableName)
            Return dsOrganizations
        End Function
        Public Function GetOrganizationByMemberID(ByVal MemberID As String) As SqlDataReader
            strSql = "SELECT * FROM OrganizationInfo WHERE AutoMemberID=" & MemberID
            Return SqlHelper.ExecuteReader(Me.GetConnection(), CommandType.Text, strSql)
        End Function
    End Class
End Namespace
