﻿'***************************** Module Header ******************************\
' Module Name:  InsertImage.vb
' Project:      VBOpenXmlInsertImageToPPT
' Copyright(c)  Microsoft Corporation.
' 
' The Class is used to Insert Image into PowerPoint using Open XML SDK.
' 
' This source is subject to the Microsoft Public License.
' See http://www.microsoft.com/en-us/openness/licenses.aspx.
' All other rights reserved.
' 
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
' EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
' WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
'**************************************************************************/

Imports System.IO
Imports System.Linq
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Drawing
Imports DocumentFormat.OpenXml.Office2010.Drawing
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Presentation
'Imports A = DocumentFormat.OpenXml.Drawing
Imports P = DocumentFormat.OpenXml.Presentation
Imports D = DocumentFormat.OpenXml.Drawing
Public Class InsertImageTest

    Dim p As PresentationDocument


    ''' <summary>
    ''' Insert a new Slide into PowerPoint
    ''' </summary>
    ''' <param name="presentationPart">Presentation Part</param>
    ''' <param name="layoutName">Layout of the new Slide</param>
    ''' <returns>Slide Instance</returns>
    ''' 
    Public Function InsertSlide(presentationPart As PresentationPart, layoutName As String) As Slide



        Dim slideId As UInt32 = 256UI

        ' Get the Slide Id collection of the presentation document
        Dim slideIdList = presentationPart.Presentation.SlideIdList

        If slideIdList Is Nothing Then
            Throw New NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again")
        End If

        slideId += Convert.ToUInt32(slideIdList.Count())

        ' Creates a Slide instance and adds its children.
        Dim slide As New Slide(New CommonSlideData(New ShapeTree()))

        Dim slidePart As SlidePart = presentationPart.AddNewPart(Of SlidePart)()


        ''////////

        '  Dim f As FooterPart = presentationPart.AddNewPart(Of FooterPart)()

        ' f.Footer.Slide.CommonSlideData = DirectCast(slideMasterPart.SlideLayoutParts.SingleOrDefault(Function(sl) sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName)).SlideLayout.CommonSlideData.Clone(), CommonSlideData)

        '  slidePart.AddPart(f)        
        '     (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = footerText})))

        '////////////
        'Dim doc As MainDocumentPart = presentationPart.SlideMasterParts
        'presentationPart.DeleteParts(doc.HeaderParts)

        'Dim hf As HeaderFooter = slide.Descendants(Of HeaderFoote).FirstOrDefault()

        'If hf Is Nothing Then
        '    hf = New HeaderFooter
        '    slide.AppendChild(Of HeaderFooter)(hf)
        '    hf.Footer = True ' New Drawing.Charts.FirstFooter
        '    hf.Footer.InnerText = "TEST"
        '    ' Response.Write(hf.Footer.HasValue)
        '    hf.SlideNumber = True
        '    hf.SlideNumber.InnerText = "sfdfs"
        '    hf.DateTime = True
        '    hf.DateTime.InnerText = "dfsddfs"
        '    hf.Footer.Value = True
        'End If
        '/////// 

        slide.Save(slidePart)

        ' Get SlideMasterPart and SlideLayoutPart from the existing Presentation Part
        Dim slideMasterPart As SlideMasterPart = presentationPart.SlideMasterParts.First()
        Dim slideLayoutPart As SlideLayoutPart = slideMasterPart.SlideLayoutParts.SingleOrDefault(Function(sl) sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName, StringComparison.OrdinalIgnoreCase))
        If slideLayoutPart Is Nothing Then
            Throw New Exception("The slide layout " & layoutName & " is not found")
        End If

        slidePart.AddPart(Of SlideLayoutPart)(slideLayoutPart)

        slidePart.Slide.CommonSlideData = DirectCast(slideMasterPart.SlideLayoutParts.SingleOrDefault(Function(sl) sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName)).SlideLayout.CommonSlideData.Clone(), CommonSlideData)

        ' Create SlideId instance and Set property
        Dim newSlideId As SlideId = presentationPart.Presentation.SlideIdList.AppendChild(Of SlideId)(New SlideId())
        newSlideId.Id = slideId
        newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart)

        Return GetSlideByRelationShipId(presentationPart, newSlideId.RelationshipId)
    End Function



    ''' <summary>
    ''' Get Slide By RelationShip ID
    ''' </summary>
    ''' <param name="presentationPart">Presentation Part</param>
    ''' <param name="relationshipId">Relationship ID</param>
    ''' <returns>Slide Object</returns>
    Private Shared Function GetSlideByRelationShipId(presentationPart As PresentationPart, relationshipId As StringValue) As Slide
        ' Get Slide object by Relationship ID
        Dim slidePart As SlidePart = TryCast(presentationPart.GetPartById(relationshipId), SlidePart)
        '//////
        ' Dim hf As FooterPart = slidePart.AddNewPart(Of FooterPart)()


        '///////
        If slidePart IsNot Nothing Then
            Return slidePart.Slide
        Else
            Return Nothing
        End If
    End Function

    Public Sub AddFooter(slide As Slide, footerText As String)


        'Dim shapeProperties1 As New P.ShapeProperties()
        'Dim transform2D1 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        'Dim offset1 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
        '  .X = 807200L, _
        '  .Y = 5524000L _
        '}
        'Dim extents1 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
        '  .Cx = 10029600L, _
        '  .Cy = 429200L _
        '}
        ''48229600L, _
        ''  .Cy = 45029200L _
        'transform2D1.Append(offset1)
        'transform2D1.Append(extents1)
        'shapeProperties1.Append(transform2D1)
        '' shapeProperties2.Append(transform2D1.Clone)
        'Dim drawingObjectId As UInt16 = 1
        '' Declare and instantiate the title shape of the new slide.
        'Dim titleShape1 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
        '    (New DocumentFormat.OpenXml.Presentation.Shape())
        'titleShape1.Append(shapeProperties1)
        'drawingObjectId = (drawingObjectId + 1)

        '' Specify the required shape properties for the title shape. 
        'titleShape1.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
        '    DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
        '    New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
        '    (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
        '    New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Footer}))

        'titleShape1.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
        '     New Drawing.ListStyle, New Drawing.Paragraph _
        '     (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = footerText})))
    End Sub

    Public Sub CreateParagraph(slide As Slide, p1 As String, p2 As String, p3 As String, p4 As String)
        '' Creates a ShapeProperties instance and adds its children.
        Dim shapeProperties1 As New P.ShapeProperties()
        Dim shapeProperties2 As New P.ShapeProperties()
        Dim shapeProperties3 As New P.ShapeProperties()
        Dim shapeProperties4 As New P.ShapeProperties()
        ''.X = 457200L, _
        '' .Y = 1524000L _
        Dim transform2D1 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset1 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 507200L, _
          .Y = 2524000L _
        }
        Dim extents1 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 8029600L, _
          .Cy = 529200L _
        }
        '48229600L, _
        '  .Cy = 45029200L _
        transform2D1.Append(offset1)
        transform2D1.Append(extents1)

        Dim transform2D2 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset2 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 507200L, _
          .Y = 3024000L _
        }
        Dim extents2 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 8029600L, _
          .Cy = 529200L _
        }
        transform2D2.Append(offset2)
        transform2D2.Append(extents2)

        Dim transform2D3 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset3 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 507200L, _
          .Y = 4024000L _
        }
        Dim extents3 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 8029600L, _
          .Cy = 529200L _
        }
        transform2D3.Append(offset3)
        transform2D3.Append(extents3)
        Dim transform2D4 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset4 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 507200L, _
          .Y = 5024000L _
        }
        Dim extents4 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 8029600L, _
          .Cy = 529200L _
        }
        transform2D4.Append(offset4)
        transform2D4.Append(extents4)


        '//1- point
        shapeProperties1.Append(transform2D1)
        shapeProperties2.Append(transform2D2)
        shapeProperties3.Append(transform2D3)
        shapeProperties4.Append(transform2D4)

        ' shapeProperties2.Append(transform2D1.Clone)
        Dim drawingObjectId As UInt16 = 1
        ' Declare and instantiate the title shape of the new slide.
        Dim titleShape1 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        titleShape1.Append(shapeProperties1)
        drawingObjectId = (drawingObjectId + 1)

        ' Specify the required shape properties for the title shape. 
        titleShape1.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Body}))

        titleShape1.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
             New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p1})))


        ' Declare and instantiate the title shape of the new slide.
        Dim titleShape2 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        titleShape2.Append(shapeProperties2)
        drawingObjectId = (drawingObjectId + 1)

        ' Specify the required shape properties for the title shape. 
        titleShape2.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Body}))

        titleShape2.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
             New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p2})))


        ' Declare and instantiate the title shape of the new slide.
        Dim titleShape3 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        titleShape3.Append(shapeProperties3)
        drawingObjectId = (drawingObjectId + 1)

        ' Specify the required shape properties for the title shape. 
        titleShape3.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Body}))

        titleShape3.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
             New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p3})))

        ' Declare and instantiate the title shape of the new slide.
        Dim titleShape4 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        titleShape4.Append(shapeProperties4)
        drawingObjectId = (drawingObjectId + 1)

        ' Specify the required shape properties for the title shape. 
        titleShape4.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Body}))

        titleShape4.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
             New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p4})))



    End Sub

    ''' <summary>
    ''' Insert Image into Slide
    ''' </summary>
    ''' <param name="imagePath">Image Path</param>
    ''' <param name="imageExt">Image Extension</param>
    Public Sub InsertImageInLastSlide(slide As Slide, imagePath As String, imageExt As String, p1 As String, p2 As String, p3 As String, p4 As String, sFooterTitle As String)
        If p1.Length > 0 Then
            CreateParagraph(slide, p1, p2, p3, p4)
        End If
        AddFooter(slide, "TEST")
        ' Creates a Picture instance and adds its children.
        Dim picture As New P.Picture()
        Dim embedId As String = String.Empty
        embedId = "rId" & (slide.Elements(Of P.Picture)().Count() + 915).ToString()
        Dim nonVisualPictureProperties As New P.NonVisualPictureProperties(New P.NonVisualDrawingProperties() With { _
         .Id = 4, _
         .Name = "Picture 5" _
        }, New P.NonVisualPictureDrawingProperties(New PictureLocks() With { _
         .NoChangeAspect = True _
        }), New ApplicationNonVisualDrawingProperties())

        Dim blipFill As New P.BlipFill()
        Dim blip As New Blip() With { _
         .Embed = embedId _
        }

        ' Creates a BlipExtensionList instance and adds its children
        Dim blipExtensionList As New BlipExtensionList()
        Dim blipExtension As New BlipExtension() With { _
         .Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" _
        }

        Dim useLocalDpi As New UseLocalDpi() With { _
         .Val = False _
        }
        useLocalDpi.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main")

        blipExtension.Append(useLocalDpi)
        blipExtensionList.Append(blipExtension)
        blip.Append(blipExtensionList)

        Dim stretch As New Stretch()
        Dim fillRectangle As New FillRectangle()
        stretch.Append(fillRectangle)

        blipFill.Append(blip)
        blipFill.Append(stretch)

        ' Creates a ShapeProperties instance and adds its children.
        Dim shapeProperties As New P.ShapeProperties()
        '.X = 457200L, _
        ' .Y = 1524000L _
        Dim transform2D As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset As DocumentFormat.OpenXml.Drawing.Offset
        If p1.Length > 0 Then
            offset = New DocumentFormat.OpenXml.Drawing.Offset() With {.X = 457200L, .Y = 724000L}
        Else
            offset = New DocumentFormat.OpenXml.Drawing.Offset() With {.X = 457200L, .Y = 724000L}
        End If
        Dim extents As DocumentFormat.OpenXml.Drawing.Extents
        If p1.Length > 0 Then
            extents = New DocumentFormat.OpenXml.Drawing.Extents() With {.Cx = 8229600L, .Cy = 1829200L}
        Else
            extents = New DocumentFormat.OpenXml.Drawing.Extents() With {.Cx = 8229600L, .Cy = 5029200L}
        End If
        '.Cx = 8229600L, _
        '.Cy = 5029200L _
        transform2D.Append(offset)
        transform2D.Append(extents)

        Dim presetGeometry As New DocumentFormat.OpenXml.Drawing.PresetGeometry() With { _
         .Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle _
        }
        Dim adjustValueList As New DocumentFormat.OpenXml.Drawing.AdjustValueList()

        presetGeometry.Append(adjustValueList)

        shapeProperties.Append(transform2D)
        shapeProperties.Append(presetGeometry)

        picture.Append(nonVisualPictureProperties)
        picture.Append(blipFill)
        picture.Append(shapeProperties)

        slide.CommonSlideData.ShapeTree.AppendChild(picture)

        ' Generates content of imagePart.
        Dim imagePart As ImagePart = slide.SlidePart.AddNewPart(Of ImagePart)(imageExt, embedId)
        Dim fileStream As New FileStream(imagePath, FileMode.Open)
        imagePart.FeedData(fileStream)
        fileStream.Close()

        ' Add footer title 
        Dim footerTitleShapes = slide.CommonSlideData.ShapeTree.ChildElements.OfType(Of DocumentFormat.OpenXml.Presentation.Shape)().Where(Function(s) s.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value = "Footer Placeholder 2")
        For Each fdt In footerTitleShapes
            fdt.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
                New Drawing.ListStyle, New Drawing.Paragraph _
                (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = sFooterTitle})))
        Next
    End Sub



    Public Function InsertNewValue(shape As DocumentFormat.OpenXml.Presentation.Shape, value As String)



        Try

            shape.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
                        New Drawing.ListStyle, New Drawing.Paragraph _
                        (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = "footerText"})))

            'Dim para As Paragraph = shape.TextBody.ChildElements.OfType(Of Paragraph)().ElementAt(0)
            'Dim run = para.ChildElements.OfType(Of Run)()
            'For Each r In run
            '    r.Text.Text = "SDFS"
            'Next

        Catch ex As Exception

        End Try


    End Function


    Public Function GetAllTextInSlide(ByVal slidePart As SlidePart) As String()
        ' Verify that the slide part exists.
        If slidePart Is Nothing Then
            Throw New ArgumentNullException("slidePart")
        End If

        ' Create a new linked list of strings.
        Dim texts As New LinkedList(Of String)()

        ' If the slide exists...
        If slidePart.Slide IsNot Nothing Then

            Dim sld As DocumentFormat.OpenXml.Presentation.Slide = slidePart.Slide

            Dim datePlaceholders = sld.CommonSlideData.ShapeTree.ChildElements.OfType(Of DocumentFormat.OpenXml.Presentation.Shape)().Where(Function(s) s.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value = "Date Placeholder 1")

            For Each dp In datePlaceholders.ToArray
                InsertNewValue(dp, "SFS")
            Next

            p.PresentationPart.Presentation.Save()
            'If (Not datePlaceholders Is Nothing) Then
            '    Dim para As Paragraph = datePlaceholders.TextBody.ChildElements.OfType(Of Paragraph)().ElementAt(0)

            '    Dim run As Run = para.ChildElements.OfType(Of Run).ElementAt(0)

            '    run.Text.Text = "ghfg"

            'End If




            ' Iterate through all the paragraphs in the slide.
            For Each paragraph In slidePart.Slide.Descendants(Of DocumentFormat.OpenXml.Drawing.Paragraph)()

                'Dim t As Drawing.Text = paragraph.ChildElements.OfType(Of Run).ElementAt(0).Text
                't.Text = "SFSD"
                'p.PresentationPart.Presentation.Save()




                '  Dim para As Paragraph = paragraph.Elements(0) 'shape.TextBody.ChildElements.OfType<Paragraph>().ElementAt(0);
                '  Dim run As Run = para.ChildElements.OfType(Of Run).ElementAt(0)






                ' Create a new string builder.                    
                Dim paragraphText As New StringBuilder()

                ' Iterate through the lines of the paragraph.
                For Each text In paragraph.Descendants(Of DocumentFormat.OpenXml.Drawing.Text)()
                    ' Append each line to the previous lines.
                    ' text.Text = "SFSDF"
                    '   p.PresentationPart.Presentation.Save()
                    paragraphText.Append(text.Text)
                Next text

                If paragraphText.Length > 0 Then
                    ' Add each paragraph to the linked list.
                    texts.AddLast(paragraphText.ToString())
                End If


            Next paragraph
        End If

        If texts.Count > 0 Then
            ' Return an array of strings.
            Return texts.ToArray()
        Else
            Return Nothing
        End If
    End Function


    Public Function GetAllTextInSlide(ByVal presentationDocument As PresentationDocument, ByVal slideIndex As Integer) As String()
        ' Verify that the presentation document exists.
        If presentationDocument Is Nothing Then
            Throw New ArgumentNullException("presentationDocument")
        End If

        ' Verify that the slide index is not out of range.
        If slideIndex < 0 Then
            Throw New ArgumentOutOfRangeException("slideIndex")
        End If

        ' Get the presentation part of the presentation document.
        Dim presentationPart As PresentationPart = presentationDocument.PresentationPart

        ' Verify that the presentation part and presentation exist.
        If presentationPart IsNot Nothing AndAlso presentationPart.Presentation IsNot Nothing Then
            ' Get the Presentation object from the presentation part.
            Dim presentation As Presentation = presentationPart.Presentation

            ' Verify that the slide ID list exists.
            If presentation.SlideIdList IsNot Nothing Then
                ' Get the collection of slide IDs from the slide ID list.
                Dim slideIds = presentation.SlideIdList.ChildElements


                ' If the slide ID is in range...
                If slideIndex < slideIds.Count Then
                    ' Get the relationship ID of the slide.
                    Dim slidePartRelationshipId As String = (TryCast(slideIds(slideIndex), SlideId)).RelationshipId

                    ' Get the specified slide part from the relationship ID.
                    Dim slidePart As SlidePart = CType(presentationPart.GetPartById(slidePartRelationshipId), SlidePart)


                    ' Pass the slide part to the next method, and
                    ' then return the array of strings that method
                    ' returns to the previous method.
                    Return GetAllTextInSlide(slidePart)
                End If
            End If
        End If

        ' Else, return null.
        Return Nothing
    End Function
    ' Get all the text in a slide.
    Public Function GetAllTextInSlide(ByVal presentationFile As String, ByVal slideIndex As Integer) As String()
        ' Open the presentation as read-only.
        Using presentationDocument As PresentationDocument = presentationDocument.Open(presentationFile, True)
            ' Pass the presentation and the slide index
            ' to the next GetAllTextInSlide method, and
            ' then return the array of strings it returns. 
            p = presentationDocument
            Return GetAllTextInSlide(presentationDocument, slideIndex)
        End Using
    End Function

    Public Sub ReplaceText(presentationFile As String)
        Using pdoc As PresentationDocument = PresentationDocument.Open(presentationFile, True)
            For Each spart As SlidePart In pdoc.PresentationPart.SlideParts
                Dim sld = spart.Slide

                Dim footerDateShape = sld.CommonSlideData.ShapeTree.ChildElements.OfType(Of DocumentFormat.OpenXml.Presentation.Shape)().Where(Function(s) s.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value = "Date Placeholder 1")

                For Each fdt In footerDateShape
                    fdt.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
                        New Drawing.ListStyle, New Drawing.Paragraph _
                        (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = "footerText"})))
                Next
            Next
            pdoc.PresentationPart.Presentation.Save()
        End Using
    End Sub
End Class
