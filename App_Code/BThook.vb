﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Braintree
'Imports System.Web.Mvc

Imports System.Net.Mail

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class BThook
    Inherits System.Web.Services.WebService


    <WebMethod(True)> _
    Public Function WebHook() As String
        Static Gateway As New BraintreeGateway(Braintree.Environment.DEVELOPMENT, "jcc9t485jznz4kcm", "mknrwcsqs6xvbzs3", "2897e9457f385c31fc9aa0f302063935")
        Try
            'If Context.Request.HttpMethod = "POST" Then
            '    Dim WebhookNotification As WebhookNotification = Gateway.WebhookNotification.Parse(Context.Request.Params("bt_signature"), Context.Request.Params("bt_payload"))
            '    Dim message As String = String.Format("[Webhook Received {0}] | Kind: {1} | Subscription: {2}", WebhookNotification.Timestamp.Value, WebhookNotification.Kind, WebhookNotification.Subscription.Id)
            '    '  System.Console.WriteLine(message)
            '    ' Context.Response.StatusCode = "200"
            '    'Return Context.Response.StatusCode
            '    Return New Net.HttpStatusCode(200).ToString
            '    ' Return New HttpStatusCodeResult(200).ToString

            'Else
            If Context.Request.HttpMethod = "GET" Then
                Dim chl_resp As String
                chl_resp = Gateway.WebhookNotification.Verify(Context.Request.QueryString("bt_challenge"))
                SendMessage("Brain Tree Webhook", "Before : " + chl_resp + " Bt_challenge :" + Context.Request.QueryString("bt_challenge").ToString, "")
                Return chl_resp.ToString()
            End If

        Catch ex As Exception
            SendMessage("Brain Tree Webhook", ex.ToString(), "")
        End Try

        Return ""
    End Function
    Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@northsouth.org")
        email.To.Add("bindhu.rajalakshmi@capestart.com")
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        '  Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(Server.MapPath(strLogFile))
        '  email.Attachments.Add(oAttch)

        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Try
            client.Send(email)
        Catch ex As Exception
            'lblMessage.Text = e.Message.ToString
            ok = False
        End Try

    End Sub
End Class