Imports Microsoft.VisualBasic
Namespace NorthSouth.BAL
    Public Class Globals
        Private Shared _sortExpression As String
        Public Shared Property SortExpression() As String
            Get
                Return _sortExpression
            End Get
            Set(ByVal value As String)
                _sortExpression = value
            End Set
        End Property
    End Class
End Namespace
