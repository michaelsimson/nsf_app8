﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Microsoft.ApplicationBlocks.Data

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
Public Class NFGDetails
    Inherits System.Web.Services.WebService
    Public Class Survey
        Dim UniqueId As String

    End Class



    <WebMethod(True)> _
    Public Function NFGData(iUniqueID As Integer) As String

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from NFG_Transactions where unique_id=" & iUniqueID)
        Dim dt As DataTable = ds.Tables(0)
        Dim str As String = ""
        If dt.Rows.Count > 0 Then
            Dim dr As DataRow = dt.Rows(0)

            str = str & "<table cellpadding=5 cellspacing=0 border=1><tr style='background:#CFCACA'><th>Unique_id</th><th>MemberId</th><th>EventYear</th><th>Event_for</th><th>Payment Date</th><th>PaymentNotes</th><th>Fee</th><th>MealsAmount</th><th>TotalPayment</th><th>Discount</th></tr>"
            str = str & "<tr><td>" & dr("Unique_Id") & "</td><td>" & dr("MemberId") & "</td><td>" & dr("EventYear") & "</td><td>" & dr("Event_for") & "</td><td>" & dr("Payment Date") & "</td><td>" & dr("PaymentNotes") & "</td><td>" & dr("Fee") & "</td><td>" & dr("MealsAmount") & "</td><td>" & dr("TotalPayment") & "</td><td>" & dr("Discount") & "</td></tr>"
            str = str & "</table>"
        End If

        Return "str"
    End Function

End Class