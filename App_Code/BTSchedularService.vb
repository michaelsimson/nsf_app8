﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Braintree
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Web.Script.Services
Imports System
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Data

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
Public Class BTSchedularService
    Inherits System.Web.Services.WebService

    Dim strLogFile As String '= System.Configuration.ConfigurationManager.AppSettings("logFilePath").ToString()

    <WebMethod(True)> _
    Public Function DisbursementDetails() As Boolean

        Dim strLogMessage As String = ""
        Try
            strLogMessage = "================= Log Details ===================\n"

            Dim cmdText As String, BT_PaymentRef As String, BT_TransId As String, BT_RefundRef As String, BT_ETG_Response As String
            Dim BT_Token As String, BT_Brand As String, ccF, ccM, ccL As String, MS_TransDate As String, BT_DisbDate As String
            Dim EMail, CardHName As String, TransType, CreatedBy As String, CreateDate As String, BT_CustomerId As String
            Dim MS_Amount As Decimal

            ' Brain Tree Gateway
            Dim BT As NameValueCollection = CType(ConfigurationManager.GetSection("BT/crd"), NameValueCollection)
            Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, BT("MerchantId"), BT("PublicKey"), BT("PrivateKey"))
            ' Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
            Dim ccReq As CreditCardRequest = New CreditCardRequest()
            Dim ccAddrReq As CreditCardAddressRequest = New CreditCardAddressRequest()
            Dim tReq As TransactionRequest = New TransactionRequest()
            Dim search As New TransactionSearchRequest()
            Dim FromDate As Date, ToDate As Date, strLastDate As String
            ToDate = DateTime.Now.AddDays(-3)
            strLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull(max(DisbDate),0) from MSChargeTemp")
            strLogMessage = strLogMessage & ":" & strLastDate
            If Convert.ToDateTime(strLastDate).ToShortDateString = "1/1/1900" Then
                FromDate = Convert.ToDateTime("03/15/2015")
            Else
                FromDate = Convert.ToDateTime(strLastDate).AddDays(1)
            End If
            strLogMessage = strLogMessage & ":" & FromDate.ToString & ":" & ToDate.ToString
            search.DisbursementDate.Between(FromDate.ToShortDateString, ToDate.ToShortDateString)
            Dim coll As ResourceCollection(Of Transaction) = gateway.Transaction.Search(search)
            Dim t As Transaction
            Dim isValid As Boolean = False
            cmdText = "  if (exists(SELECT * FROM sys.objects  where type='U' and object_id = OBJECT_ID(N'[dbo].TempTableMSCharge')))BEGIN Drop Table TempTableMSCharge ; END"
            cmdText = cmdText & " CREATE TABLE TempTableMSCharge ([MS_TransDate] [smalldatetime] NULL,[DisbDate] [smalldatetime] NULL,[ID] [varchar](50) NULL,[CustomerId] [nvarchar](50) NULL,	[PaymentReference] [nvarchar](50) NULL,"
            cmdText = cmdText & " [RefundReference] [nvarchar](50) NULL,[Token] [nvarchar](50) NULL,[Brand] [nvarchar](50) NULL,[TransType] [varchar](50) NULL,[Email] [nvarchar](50) NULL,[CHName] [varchar](100) NULL, "
            cmdText = cmdText & " [MS_Amount] [money] NULL,[MS_CN1] [nchar](10) NULL,[MS_CN2] [nchar](10) NULL,[MS_CN3] [nchar](10) NULL,[PP_Match] [varchar](10) NULL,[PPChargeID] [int] NULL,"
            cmdText = cmdText & " [ChargeRecID] [int] NULL,[CreateDate] [smalldatetime] NULL,[CreatedBy] [int] NULL,[ModifiedDate] [smalldatetime] NULL,[ModifiedBy] [int] NULL,[PP_Date] [smalldatetime] NULL,"
            cmdText = cmdText & " [CardType] [nvarchar](255) NULL,[AuthCode] [nvarchar](255) NULL) "
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
            strLogMessage = strLogMessage & (" \n Insert into temp table")
            Dim jCnt As Integer = 0
            cmdText = ""
            For Each t In coll

                isValid = True
                MS_Amount = Nothing
                MS_TransDate = ""
                ccF = ""
                ccM = ""
                ccL = ""
                BT_Brand = ""
                BT_ETG_Response = ""
                BT_PaymentRef = ""
                BT_TransId = ""
                BT_CustomerId = ""
                BT_RefundRef = ""
                BT_Token = ""
                EMail = ""
                CardHName = ""
                BT_RefundRef = ""
                TransType = ""
                CreateDate = ""
                BT_DisbDate = ""
                CreatedBy = ""
                BT_TransId = t.Id
                BT_CustomerId = t.Customer.Id
                BT_PaymentRef = t.Id + "-" + t.Customer.Id
                If Not t.RefundedTransactionId Is Nothing Then
                    BT_RefundRef = t.RefundedTransactionId + "-" + t.Customer.Id
                End If
                BT_Brand = t.CreditCard.CardType.ToString
                BT_Token = t.CreditCard.Token
                Try
                    MS_Amount = Convert.ToDecimal(t.Amount)
                    If Not t.RefundedTransactionId Is Nothing Then
                        MS_Amount = "-" & MS_Amount
                    End If
                    ccF = Left(t.CreditCard.MaskedNumber.ToString, 4)
                    ccM = Right(Left(t.CreditCard.MaskedNumber.ToString, 6), 2)
                    ccL = Right(t.CreditCard.MaskedNumber.ToString, 4)
                    If t.Customer.Email Is Nothing Then
                        EMail = ""
                    Else
                        EMail = t.Customer.Email.ToString
                    End If
                    If Not t.CreditCard.CardholderName Is Nothing Then
                        CardHName = t.CreditCard.CardholderName.ToString
                    Else
                        CardHName = ""
                    End If
                    MS_TransDate = t.CreatedAt
                    BT_ETG_Response = t.ProcessorAuthorizationCode
                    If t.DisbursementDetails.DisbursementDate.ToString Is Nothing Then
                        BT_DisbDate = ""
                    Else
                        BT_DisbDate = t.DisbursementDetails.DisbursementDate.ToString
                    End If
                Catch ex As Exception
                    strLogMessage = strLogMessage & (" \n 0. Error on Brain Tree \n\r\r" & ex.ToString)
                End Try
                cmdText = cmdText & " Insert into TempTableMSCharge(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,Email,CHName, MS_CN1, MS_CN2, MS_CN3,TransType,CreateDate,CreatedBy,AuthCode) "
                cmdText = cmdText & " VALUES (CONVERT(DATETIME,'" & MS_TransDate & "',101) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",101),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',"
                cmdText = cmdText & " (Select ISNULL(TransType,'') from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), (select CreateDate from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), (select CreatedBy from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & " ) "
                jCnt = jCnt + 1
                If jCnt = 80 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    jCnt = 0
                    cmdText = ""
                End If
            Next
            If jCnt <> 0 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                cmdText = ""
                jCnt = 0
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from TempTableMSCharge Order by DisbDate asc")
            Dim dt As DataTable = ds.Tables(0)
            Dim i As Integer
            cmdText = ""
            Dim j As Integer = 0
            For i = 0 To dt.Rows.Count - 1
                BT_PaymentRef = dt.Rows(i).Item("PaymentReference").ToString
                cmdText = cmdText & " IF( exists(Select * from MSChargeTemp where PaymentReference='" & BT_PaymentRef & "')) BEGIN "
                cmdText = cmdText & " update MSChargeTemp set MS_TransDate = t.MS_TransDate,DisbDate=t.DisbDate,ID=t.ID,CustomerId=t.CustomerId,PaymentReference=t.PaymentReference,RefundReference=t.RefundReference, "
                cmdText = cmdText & " MS_Amount=t.MS_Amount, Brand=t.Brand, Token=t.Token,TransType=t.TransType,Email=t.Email,CHName=t.CHName, MS_CN1=t.MS_CN1, MS_CN2=t.MS_CN2, MS_CN3=t.MS_CN3,CreateDate=t.CreateDate,CreatedBy=t.CreatedBy,AuthCode=t.AuthCode,DLDate=CONVERT(varchar(10),getdate(),101) FROM MSChargeTemp ms INNER JOIN TempTableMSCharge t on ms.PaymentReference = t.PaymentReference WHERE t.PaymentReference =  '" & BT_PaymentRef & "' ; "
                cmdText = cmdText & " END ELSE BEGIN "
                cmdText = cmdText & " insert into MSChargeTemp(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,DLDate) SELECT MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,CONVERT(varchar(10),getdate(),101) DLDate FROM TempTableMSCharge WHERE PaymentReference =  '" & BT_PaymentRef & "'; "
                cmdText = cmdText & " END  "
                j = j + 1
                If j = 80 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    cmdText = ""
                    j = 0
                End If
            Next
            If j <> 0 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                cmdText = ""
                j = 0
            End If
            strLogMessage = strLogMessage & "\n 4. Records exists in Brain Tree : " & isValid
        Catch ex1 As Exception
            strLogMessage = strLogMessage & (" \n 5. Error on Brain Tree Web Service \n\r\r" & ex1.ToString)
        End Try
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " if (exists(SELECT * FROM sys.objects  where type='U' and object_id = OBJECT_ID(N'[dbo].TempTableMSCharge')))BEGIN Drop Table TempTableMSCharge END")
        writeToLogFile(strLogMessage)
        SendMessage("Brain Tree Schedular", "Disbursement Details", "")
        Return True
    End Function

    Public Sub writeToLogFile(logMessage As String)
        Dim strPath As String '= Server.MapPath(strLogFile)
        Dim strLogMessage As String = ""
        Try
            strLogFile = "~\BTScheduleLog.txt"
            strPath = Server.MapPath(strLogFile)

            Dim swLog As StreamWriter
            strLogMessage = String.Format("{0}: {1}", Date.Now, logMessage)
            If (Not File.Exists(strLogFile)) Then
                swLog = New StreamWriter(Server.MapPath(strLogFile))
            Else
                swLog = File.AppendText(strLogFile)
            End If
            swLog.WriteLine(strLogMessage)
            swLog.WriteLine()
            swLog.Close()
        Catch ex As Exception
        End Try
    End Sub

    Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@northsouth.org")
        email.To.Add("bindhu.rajalakshmi@capestart.com")
        email.Subject = sSubject & Date.Today.ToString
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(Server.MapPath(strLogFile))
        email.Attachments.Add(oAttch)

        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Try
            client.Send(email)
        Catch ex As Exception
            'lblMessage.Text = e.Message.ToString
            ok = False
        End Try

    End Sub



    <WebMethod(True)> _
    Public Function DisputesDetails() As Boolean

        Dim strLogMessage As String = ""
        Try

      
        strLogMessage = "================= Log Details ===================\n"

        Dim cmdText As String, BT_PaymentRef As String, BT_TransId As String, BT_RefundRef As String, BT_ETG_Response As String
        Dim BT_Token As String, BT_Brand As String, ccF, ccM, ccL As String, MS_TransDate As String, BT_DisbDate As String
        Dim EMail, CardHName As String, TransType, CreatedBy As String, CreateDate As String, BT_CustomerId As String
        Dim MS_Amount As Decimal

        ' Brain Tree Gateway
        Dim BT As NameValueCollection = CType(ConfigurationManager.GetSection("BT/crd"), NameValueCollection)
        Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, BT("MerchantId"), BT("PublicKey"), BT("PrivateKey"))
        ' Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
        Dim ccReq As CreditCardRequest = New CreditCardRequest()
        Dim ccAddrReq As CreditCardAddressRequest = New CreditCardAddressRequest()
        Dim tReq As TransactionRequest = New TransactionRequest()
        Dim search As New TransactionSearchRequest()
            strLogMessage = strLogMessage & (" Con :" & Application("ConnectionString").ToString)
        Dim FromDate As Date, ToDate As Date, strLastDate As String
            FromDate = Convert.ToDateTime("01/15/2015")
        ToDate = Convert.ToDateTime("03/17/2016")
        search.DisputeDate.Between(FromDate.ToShortDateString, ToDate.ToShortDateString)
        Dim coll As ResourceCollection(Of Transaction) = gateway.Transaction.Search(search)
            Dim t As Transaction
            strLogMessage = strLogMessage & (" \n coll.MaximumCount : " & coll.MaximumCount)
        Dim d As Dispute
        For Each t In coll
            For Each d In t.Disputes
                If t.Customer.Email Is Nothing Then
                    EMail = ""
                Else
                    EMail = t.Customer.Email.ToString
                End If
                If Not t.CreditCard.CardholderName Is Nothing Then
                    CardHName = t.CreditCard.CardholderName.ToString
                Else
                    CardHName = ""
                End If
                strLogMessage = strLogMessage & (" \n Insert into ChargeBack table")
                    cmdText = "Insert into ChargeBack (TransID,Email,CCName) values ('" & t.Id & "','" & EMail & "','" & CardHName & "')"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
            Next
            Next
        Catch ex As Exception
            strLogMessage = strLogMessage & (" \n Insert into ChargeBack table - Error :" & ex.ToString())
        End Try
        writeToLogFile(strLogMessage)
        SendMessage("Brain Tree Schedular Disputes", "Dispute Details", "")


        '    Dim FromDate As Date, ToDate As Date, strLastDate As String
        '    ToDate = DateTime.Now.AddDays(-3)
        '    strLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull(max(DisbDate),0) from MSChargeTemp")
        '    strLogMessage = strLogMessage & ":" & strLastDate
        '    If Convert.ToDateTime(strLastDate).ToShortDateString = "1/1/1900" Then
        '        FromDate = Convert.ToDateTime("03/15/2015")
        '    Else
        '        FromDate = Convert.ToDateTime(strLastDate).AddDays(1)
        '    End If
        '    strLogMessage = strLogMessage & ":" & FromDate.ToString & ":" & ToDate.ToString
        '    search.DisbursementDate.Between(FromDate.ToShortDateString, ToDate.ToShortDateString)
        '    Dim coll As ResourceCollection(Of Transaction) = gateway.Transaction.Search(search)
        '    Dim t As Transaction
        '    Dim isValid As Boolean = False
        '    cmdText = "  if (exists(SELECT * FROM sys.objects  where type='U' and object_id = OBJECT_ID(N'[dbo].TempTableMSCharge')))BEGIN Drop Table TempTableMSCharge ; END"
        '    cmdText = cmdText & " CREATE TABLE TempTableMSCharge ([MS_TransDate] [smalldatetime] NULL,[DisbDate] [smalldatetime] NULL,[ID] [varchar](50) NULL,[CustomerId] [nvarchar](50) NULL,	[PaymentReference] [nvarchar](50) NULL,"
        '    cmdText = cmdText & " [RefundReference] [nvarchar](50) NULL,[Token] [nvarchar](50) NULL,[Brand] [nvarchar](50) NULL,[TransType] [varchar](50) NULL,[Email] [nvarchar](50) NULL,[CHName] [varchar](100) NULL, "
        '    cmdText = cmdText & " [MS_Amount] [money] NULL,[MS_CN1] [nchar](10) NULL,[MS_CN2] [nchar](10) NULL,[MS_CN3] [nchar](10) NULL,[PP_Match] [varchar](10) NULL,[PPChargeID] [int] NULL,"
        '    cmdText = cmdText & " [ChargeRecID] [int] NULL,[CreateDate] [smalldatetime] NULL,[CreatedBy] [int] NULL,[ModifiedDate] [smalldatetime] NULL,[ModifiedBy] [int] NULL,[PP_Date] [smalldatetime] NULL,"
        '    cmdText = cmdText & " [CardType] [nvarchar](255) NULL,[AuthCode] [nvarchar](255) NULL) "
        '    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
        '    strLogMessage = strLogMessage & (" \n Insert into temp table")
        '    Dim jCnt As Integer = 0
        '    cmdText = ""
        '    For Each t In coll

        '        isValid = True
        '        MS_Amount = Nothing
        '        MS_TransDate = ""
        '        ccF = ""
        '        ccM = ""
        '        ccL = ""
        '        BT_Brand = ""
        '        BT_ETG_Response = ""
        '        BT_PaymentRef = ""
        '        BT_TransId = ""
        '        BT_CustomerId = ""
        '        BT_RefundRef = ""
        '        BT_Token = ""
        '        EMail = ""
        '        CardHName = ""
        '        BT_RefundRef = ""
        '        TransType = ""
        '        CreateDate = ""
        '        BT_DisbDate = ""
        '        CreatedBy = ""
        '        BT_TransId = t.Id
        '        BT_CustomerId = t.Customer.Id
        '        BT_PaymentRef = t.Id + "-" + t.Customer.Id
        '        If Not t.RefundedTransactionId Is Nothing Then
        '            BT_RefundRef = t.RefundedTransactionId + "-" + t.Customer.Id
        '        End If
        '        BT_Brand = t.CreditCard.CardType.ToString
        '        BT_Token = t.CreditCard.Token
        '        Try
        '            MS_Amount = Convert.ToDecimal(t.Amount)
        '            If Not t.RefundedTransactionId Is Nothing Then
        '                MS_Amount = "-" & MS_Amount
        '            End If
        '            ccF = Left(t.CreditCard.MaskedNumber.ToString, 4)
        '            ccM = Right(Left(t.CreditCard.MaskedNumber.ToString, 6), 2)
        '            ccL = Right(t.CreditCard.MaskedNumber.ToString, 4)
        '            If t.Customer.Email Is Nothing Then
        '                EMail = ""
        '            Else
        '                EMail = t.Customer.Email.ToString
        '            End If
        '            If Not t.CreditCard.CardholderName Is Nothing Then
        '                CardHName = t.CreditCard.CardholderName.ToString
        '            Else
        '                CardHName = ""
        '            End If
        '            MS_TransDate = t.CreatedAt
        '            BT_ETG_Response = t.ProcessorAuthorizationCode
        '            If t.DisbursementDetails.DisbursementDate.ToString Is Nothing Then
        '                BT_DisbDate = ""
        '            Else
        '                BT_DisbDate = t.DisbursementDetails.DisbursementDate.ToString
        '            End If
        '        Catch ex As Exception
        '            strLogMessage = strLogMessage & (" \n 0. Error on Brain Tree \n\r\r" & ex.ToString)
        '        End Try
        '        cmdText = cmdText & " Insert into TempTableMSCharge(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,Email,CHName, MS_CN1, MS_CN2, MS_CN3,TransType,CreateDate,CreatedBy,AuthCode) "
        '        cmdText = cmdText & " VALUES (CONVERT(DATETIME,'" & MS_TransDate & "',101) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",101),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',"
        '        cmdText = cmdText & " (Select ISNULL(TransType,'') from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), (select CreateDate from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), (select CreatedBy from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'), " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & " ) "
        '        jCnt = jCnt + 1
        '        If jCnt = 80 Then
        '            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
        '            jCnt = 0
        '            cmdText = ""
        '        End If
        '    Next
        '    If jCnt <> 0 Then
        '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
        '        cmdText = ""
        '        jCnt = 0
        '    End If
        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from TempTableMSCharge Order by DisbDate asc")
        '    Dim dt As DataTable = ds.Tables(0)
        '    Dim i As Integer
        '    cmdText = ""
        '    Dim j As Integer = 0
        '    For i = 0 To dt.Rows.Count - 1
        '        BT_PaymentRef = dt.Rows(i).Item("PaymentReference").ToString
        '        cmdText = cmdText & " IF( exists(Select * from MSChargeTemp where PaymentReference='" & BT_PaymentRef & "')) BEGIN "
        '        cmdText = cmdText & " update MSChargeTemp set MS_TransDate = t.MS_TransDate,DisbDate=t.DisbDate,ID=t.ID,CustomerId=t.CustomerId,PaymentReference=t.PaymentReference,RefundReference=t.RefundReference, "
        '        cmdText = cmdText & " MS_Amount=t.MS_Amount, Brand=t.Brand, Token=t.Token,TransType=t.TransType,Email=t.Email,CHName=t.CHName, MS_CN1=t.MS_CN1, MS_CN2=t.MS_CN2, MS_CN3=t.MS_CN3,CreateDate=t.CreateDate,CreatedBy=t.CreatedBy,AuthCode=t.AuthCode,DLDate=CONVERT(varchar(10),getdate(),101) FROM MSChargeTemp ms INNER JOIN TempTableMSCharge t on ms.PaymentReference = t.PaymentReference WHERE t.PaymentReference =  '" & BT_PaymentRef & "' ; "
        '        cmdText = cmdText & " END ELSE BEGIN "
        '        cmdText = cmdText & " insert into MSChargeTemp(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,DLDate) SELECT MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,CONVERT(varchar(10),getdate(),101) DLDate FROM TempTableMSCharge WHERE PaymentReference =  '" & BT_PaymentRef & "'; "
        '        cmdText = cmdText & " END  "
        '        j = j + 1
        '        If j = 80 Then
        '            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
        '            cmdText = ""
        '            j = 0
        '        End If
        '    Next
        '    If j <> 0 Then
        '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
        '        cmdText = ""
        '        j = 0
        '    End If
        '    strLogMessage = strLogMessage & "\n 4. Records exists in Brain Tree : " & isValid
        'Catch ex1 As Exception
        '    strLogMessage = strLogMessage & (" \n 5. Error on Brain Tree Web Service \n\r\r" & ex1.ToString)
        'End Try
        'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, " if (exists(SELECT * FROM sys.objects  where type='U' and object_id = OBJECT_ID(N'[dbo].TempTableMSCharge')))BEGIN Drop Table TempTableMSCharge END")
        ' writeToLogFile(strLogMessage)
        '   SendMessage("Brain Tree Schedular", "Disbursement Details", "")
        Return True
    End Function


End Class