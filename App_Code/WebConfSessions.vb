﻿
Imports System.Collections.Generic
Imports System.Linq
Imports System.Web
Imports System.Xml
Imports System.IO
Imports System.Net

''' <summary>
''' Summary description for WebConfSessions
''' </summary>
Public Class WebConfSessions
    Public Property WebExID() As String
        Get
            Return m_WebExID
        End Get
        Set(value As String)
            m_WebExID = value
        End Set
    End Property
    Private m_WebExID As String
    Public Property Password() As String
        Get
            Return m_Password
        End Get
        Set(value As String)
            m_Password = value
        End Set
    End Property
    Private m_Password As String
    Public Property WebSiteID() As String
        Get
            Return m_WebSiteID
        End Get
        Set(value As String)
            m_WebSiteID = value
        End Set
    End Property
    Private m_WebSiteID As String
    Public Property PartnerID() As String
        Get
            Return m_PartnerID
        End Get
        Set(value As String)
            m_PartnerID = value
        End Set
    End Property
    Private m_PartnerID As String

    Public Property SessionKey() As String
        Get
            Return m_SessionKey
        End Get
        Set(value As String)
            m_SessionKey = value
        End Set
    End Property
    Private m_SessionKey As String
    Public Property AttendeeName() As String
        Get
            Return m_AttendeeName
        End Get
        Set(value As String)
            m_AttendeeName = value
        End Set
    End Property
    Private m_AttendeeName As String
    Public Property AttendeeType() As String
        Get
            Return m_AttendeeType
        End Get
        Set(value As String)
            m_AttendeeType = value
        End Set
    End Property
    Private m_AttendeeType As String
    Public Property MaxCapacity() As Integer
        Get
            Return m_MaxCapacity
        End Get
        Set(value As Integer)
            m_MaxCapacity = value
        End Set
    End Property
    Private m_MaxCapacity As Integer
    Public Property StartTime() As String
        Get
            Return m_StartTime
        End Get
        Set(value As String)
            m_StartTime = value
        End Set
    End Property
    Private m_StartTime As String
    Public Property EndDate() As String
        Get
            Return m_EndDate
        End Get
        Set(value As String)
            m_EndDate = value
        End Set
    End Property
    Private m_EndDate As String
    Public Property BeginTime() As String
        Get
            Return m_BeginTime
        End Get
        Set(value As String)
            m_BeginTime = value
        End Set
    End Property
    Private m_BeginTime As String
    Public Property EndTime() As String
        Get
            Return m_EndTime
        End Get
        Set(value As String)
            m_EndTime = value
        End Set
    End Property
    Private m_EndTime As String
    Public Property AttendeeEmail() As String
        Get
            Return m_AttendeeEmail
        End Get
        Set(value As String)
            m_AttendeeEmail = value
        End Set
    End Property
    Private m_AttendeeEmail As String
    Public Property City() As String
        Get
            Return m_City
        End Get
        Set(value As String)
            m_City = value
        End Set
    End Property
    Private m_City As String
    Public Property State() As String
        Get
            Return m_State
        End Get
        Set(value As String)
            m_State = value
        End Set
    End Property
    Private m_State As String
    Public Property Country() As String
        Get
            Return m_Country
        End Get
        Set(value As String)
            m_Country = value
        End Set
    End Property
    Private m_Country As String
    Public Property HostJoinURL() As String
        Get
            Return m_HostJoinURL
        End Get
        Set(value As String)
            m_HostJoinURL = value
        End Set
    End Property
    Private m_HostJoinURL As String
    Public Property AttendeeURL() As String
        Get
            Return m_AttendeeURL
        End Get
        Set(value As String)
            m_AttendeeURL = value
        End Set
    End Property
    Private m_AttendeeURL As String
    Public Property MeetingPassword() As String
        Get
            Return m_MeetingPassword
        End Get
        Set(value As String)
            m_MeetingPassword = value
        End Set
    End Property
    Private m_MeetingPassword As String
    Public Sub DelMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.DelMeetingAttendee"">" & vbCr & vbLf
        strXML += "<attendeeID>" & AttendeeID & "</attendeeID>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = ProcessMeetingAttendeeResponse(xmlReply)
    End Sub
    Public Sub RegisterMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, City As String, Email As String, Country As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.RegisterMeetingAttendee"">" & vbCr & vbLf

        strXML += "<attendees>" & vbCr & vbLf
        strXML += "<person>" & vbCr & vbLf
        strXML += "<name>" & Name & "</name>" & vbCr & vbLf
        strXML += "<title>title</title>" & vbCr & vbLf
        strXML += "<company>microsoft</company>" & vbCr & vbLf
        strXML += "<address>" & vbCr & vbLf
        strXML += "<addressType>PERSONAL</addressType>" & vbCr & vbLf
        strXML += "<city>" & City & "</city>" & vbCr & vbLf
        strXML += "<country>US</country>" & vbCr & vbLf
        strXML += "</address>" & vbCr & vbLf

        strXML += "<email>" & Email & "</email>" & vbCr & vbLf
        strXML += "<notes>notes</notes>" & vbCr & vbLf
        strXML += "<url>https://</url>" & vbCr & vbLf
        strXML += "<type>VISITOR</type>" & vbCr & vbLf
        strXML += "</person>" & vbCr & vbLf
        strXML += "<joinStatus>ACCEPT</joinStatus>" & vbCr & vbLf
        strXML += "<role>ATTENDEE</role>" & vbCr & vbLf

        strXML += "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML += "</attendees>" & vbCr & vbLf


        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = RegisterMeetingAttendeeResponse(xmlReply)
    End Sub
    Public Sub GetJoinMeetingURL(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, Email As String, MeetingPassword As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML += "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML += "<header>" & vbCr & vbLf
        strXML += "<securityContext>" & vbCr & vbLf
        strXML += "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML += "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML += "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML += "</securityContext>" & vbCr & vbLf
        strXML += "</header>" & vbCr & vbLf
        strXML += "<body>" & vbCr & vbLf
        strXML += "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GetjoinurlMeeting"">" & vbCr & vbLf

        strXML += "<sessionKey>" + sessionKey + "</sessionKey>" & vbCr & vbLf
        strXML += "<attendeeName>" + Name + "</attendeeName>" & vbCr & vbLf
        strXML += "<attendeeEmail>" + Email + "</attendeeEmail>" & vbCr & vbLf
        strXML += "<meetingPW>" + MeetingPassword + "</meetingPW>" & vbCr & vbLf
        'strXML += "<RegID>" + hdnAttendeeRegisteredID.Value + "</RegID>" & vbCr & vbLf

        strXML += "</bodyContent>" & vbCr & vbLf
        strXML += "</body>" & vbCr & vbLf
        strXML += "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingAttendeeURLResponse(xmlReply)
    End Sub
    Private Function ProcessMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then


            ElseIf status = "FAILURE" Then


            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
    Private Function RegisterMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText
                Dim attendeeID As String

                attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText



            ElseIf status = "FAILURE" Then

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function

    Private Function MeetingAttendeeURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml



            ElseIf status = "FAILURE" Then


            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " + e.Message)
        End Try

        Return sb.ToString()
    End Function
End Class