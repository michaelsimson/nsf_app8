﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports Braintree
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Web.Script.Services
Imports System
Imports System.Collections.Specialized
Imports System.Configuration
Imports System.Data

<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<System.Web.Script.Services.ScriptService()> _
Public Class ChildInfo
    Inherits System.Web.Services.WebService


    <WebMethod(True)> _
    Public Function IsDupliateChild(MemberId As String, FName As String, LName As String, DOB As String, Gender As String, Grade As Integer) As Boolean
        Dim strCmd As String = " Select count(*) from Child Where FIRST_NAME='" & FName & "' and MIDDLE_INITIAL ='' and LAST_NAME='" & LName & "' "
        Dim iCnt As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd)
        If iCnt <> 0 Then
            Return True
        End If
        Return False
    End Function

    <WebMethod(True)> _
    Public Function AddChildDetails(MemberId As String, FName As String, MName As String, LName As String, DOB As String, Gender As String, Grade As Integer) As Boolean

        Dim strCmd As String = " Select count(*) from Child Where FIRST_NAME='" & FName & "' and MIDDLE_INITIAL ='" & MName & "' and LAST_NAME='" & LName & "' "
        Dim iCnt As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd)
        If iCnt = 0 Then
            strCmd = "INSERT INTO Child (MEMBERID,FIRST_NAME,MIDDLE_INITIAL,LAST_NAME,DATE_OF_BIRTH,Gender,GRADE) Values(" + MemberId + ","
            strCmd = strCmd & "'" & FName & "','" + MName & "','" & LName & "','" & DOB & "','" & Gender & "'," & Grade & ")"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strCmd)
        Else
            Return False
        End If
        Return True
    End Function
   

End Class