﻿Imports System.Collections.Generic
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.Web
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Xml
Imports Microsoft.ApplicationBlocks.Data
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization

Public Class CoachingExceptionLog
    Public Shared Sub createExceptionLog(ByVal coachName As String, ByVal status As String, ByVal Exception As String, ByVal appName As String)

        Dim filepath As String = String.Empty
        Try

            ' string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));

            Dim pageName As String = Path.GetFileName(System.Web.HttpContext.Current.Request.Path)
            Dim filename As String = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace("/"c, "-"c) + ".txt"

            Dim folderPath As String = System.Web.HttpContext.Current.Server.MapPath("~/CoachingLog")
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If

            filepath = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString("~/CoachingLog/") & filename)

            If File.Exists(filepath) Then
                Using stwriter As New StreamWriter(filepath, True)
                    Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                    message += Environment.NewLine
                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine
                    message += String.Format("Date & Time: {0}", DateTime.Now)
                    message += Environment.NewLine
                    message += String.Format("Coach Name: {0}", coachName)
                    message += Environment.NewLine
                    message += String.Format("Validation Status: {0}", status)
                    message += Environment.NewLine

                    message += String.Format("Source: {0}", appName)
                    message += Environment.NewLine

                    message += Environment.NewLine
                    message += String.Format("Exception: {0}", Exception)
                    message += Environment.NewLine

                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine

                    stwriter.WriteLine(message)
                End Using
            Else
                Dim stwriter As StreamWriter = File.CreateText(filepath)
                Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                message += Environment.NewLine
                message += "-----------------------------------------------------------"
                message += Environment.NewLine
                message += String.Format("Date & Time: {0}", DateTime.Now)
                message += Environment.NewLine
                message += String.Format("Coach Name: {0}", coachName)
                message += Environment.NewLine
                message += String.Format("Validation Status: {0}", status)
                message += Environment.NewLine

                message += String.Format("Source: {0}", appName)
                message += Environment.NewLine

                message += Environment.NewLine
                message += String.Format("Exception: {0}", Exception)
                message += Environment.NewLine

                message += "-----------------------------------------------------------"
                message += Environment.NewLine

                stwriter.WriteLine(message)
                stwriter.Close()
            End If
            ' lblerr.Text = ex.Message;
        Catch ex As Exception
        End Try

    End Sub
    Public Shared Sub createWebinarExceptionLog(ByVal coachName As String, ByVal status As String, ByVal Exception As String, ByVal appName As String)

        Dim filepath As String = String.Empty
        Try

            ' string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));

            Dim pageName As String = Path.GetFileName(System.Web.HttpContext.Current.Request.Path)
            Dim filename As String = "WebinarLog_" + DateTime.Now.ToShortDateString().Replace("/"c, "-"c) + ".txt"

            Dim folderPath As String = System.Web.HttpContext.Current.Server.MapPath("~/CoachingLog")
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If

            filepath = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString("~/CoachingLog/") & filename)

            If File.Exists(filepath) Then
                Using stwriter As New StreamWriter(filepath, True)
                    Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                    message += Environment.NewLine
                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine
                    message += String.Format("Date & Time: {0}", DateTime.Now)
                    message += Environment.NewLine
                    message += String.Format("Coach Name: {0}", coachName)
                    message += Environment.NewLine
                    message += String.Format("Validation Status: {0}", status)
                    message += Environment.NewLine

                    message += String.Format("Source: {0}", appName)
                    message += Environment.NewLine

                    message += Environment.NewLine
                    message += String.Format("Exception: {0}", Exception)
                    message += Environment.NewLine

                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine

                    stwriter.WriteLine(message)
                End Using
            Else
                Dim stwriter As StreamWriter = File.CreateText(filepath)
                Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                message += Environment.NewLine
                message += "-----------------------------------------------------------"
                message += Environment.NewLine
                message += String.Format("Date & Time: {0}", DateTime.Now)
                message += Environment.NewLine
                message += String.Format("Coach Name: {0}", coachName)
                message += Environment.NewLine
                message += String.Format("Validation Status: {0}", status)
                message += Environment.NewLine

                message += String.Format("Source: {0}", appName)
                message += Environment.NewLine

                message += Environment.NewLine
                message += String.Format("Exception: {0}", Exception)
                message += Environment.NewLine

                message += "-----------------------------------------------------------"
                message += Environment.NewLine

                stwriter.WriteLine(message)
                stwriter.Close()
            End If
            ' lblerr.Text = ex.Message;
        Catch ex As Exception
        End Try

    End Sub

    Public Shared Sub FileCalendarSignup(ByVal coachName As String, ByVal Productgroup As String, ByVal product As String)

        Dim filepath As String = String.Empty
        Try

            ' string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));

            Dim pageName As String = Path.GetFileName(System.Web.HttpContext.Current.Request.Path)
            Dim filename As String = "CalSignupLog_" + DateTime.Now.ToShortDateString().Replace("/"c, "-"c) + ".txt"

            Dim folderPath As String = System.Web.HttpContext.Current.Server.MapPath("~/CoachingLog")
            If Not Directory.Exists(folderPath) Then
                Directory.CreateDirectory(folderPath)
            End If

            filepath = System.Web.HttpContext.Current.Server.MapPath(Convert.ToString("~/CoachingLog/") & filename)

            If File.Exists(filepath) Then
                Using stwriter As New StreamWriter(filepath, True)
                    Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                    message += Environment.NewLine
                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine
                    message += String.Format("Date & Time: {0}", DateTime.Now)
                    message += Environment.NewLine
                    message += String.Format("Coach Name: {0}", coachName)
                    message += Environment.NewLine
                    message += String.Format("CoachName Status: {0}", coachName)
                    message += Environment.NewLine

                    message += String.Format("ProductGroup: {0}", Productgroup)
                    message += Environment.NewLine

                    message += Environment.NewLine
                    message += String.Format("product: {0}", product)
                    message += Environment.NewLine

                    message += "-----------------------------------------------------------"
                    message += Environment.NewLine

                    stwriter.WriteLine(message)
                End Using
            Else
                Dim stwriter As StreamWriter = File.CreateText(filepath)
                Dim message As String = String.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"))
                message += Environment.NewLine
                message += "-----------------------------------------------------------"
                message += Environment.NewLine
                message += String.Format("Date & Time: {0}", DateTime.Now)
                message += Environment.NewLine
                message += String.Format("Coach Name: {0}", coachName)
                message += Environment.NewLine
                message += String.Format("CoachName Status: {0}", coachName)
                message += Environment.NewLine

                message += String.Format("ProductGroup: {0}", Productgroup)
                message += Environment.NewLine

                message += Environment.NewLine
                message += String.Format("product: {0}", product)
                message += Environment.NewLine

                message += "-----------------------------------------------------------"
                message += Environment.NewLine

                stwriter.WriteLine(message)
                stwriter.Close()
            End If
            ' lblerr.Text = ex.Message;
        Catch ex As Exception
        End Try

    End Sub
End Class
