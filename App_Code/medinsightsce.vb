﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Security.Cryptography

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _ 
Public Class medinsightsce
    Inherits System.Web.Services.WebService

    <WebMethod(True)>
    Public Function Encrypt(ByVal stringToEncrypt As String, ByVal key As String) As String
        Dim DES As New TripleDESCryptoServiceProvider
        Dim MD5 As New MD5CryptoServiceProvider

        DES.Key = MD5.ComputeHash(ASCIIEncoding.ASCII.GetBytes(key))
        DES.Mode = CipherMode.ECB
        Dim Buffer As Byte() = ASCIIEncoding.Unicode.GetBytes(stringToEncrypt)

        Return Convert.ToBase64String(DES.CreateEncryptor().TransformFinalBlock(Buffer, 0, Buffer.Length))

    End Function
    
 
End Class