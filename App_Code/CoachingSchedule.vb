﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols
Imports System.Net.Mail

' To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line.
' <System.Web.Script.Services.ScriptService()> _
<WebService(Namespace:="http://tempuri.org/")> _
<WebServiceBinding(ConformsTo:=WsiProfiles.BasicProfile1_1)> _
<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Public Class CoachingSchedule
    Inherits System.Web.Services.WebService

    <WebMethod(True)> _
    Public Function getCoachingEmails() As String
        Dim tblHtml As String = String.Empty
        tblHtml += "<table>"
        tblHtml += "<tr>"
        tblHtml += "<td>"
        tblHtml += "<span>Hello All, </span> </br>"
        tblHtml += "</td>"
        tblHtml += "</tr>"
        tblHtml += "<tr>"
        tblHtml += "<tr>"
        tblHtml += "<td>"

        tblHtml += "</td>"
        tblHtml += "</tr>"
        tblHtml += "<tr>"
        tblHtml += "<td>"
        tblHtml += "<span>Gentle reminder for your tomorrow's meeting.</span> </br>"
        tblHtml += "</td>"
        tblHtml += "</tr>"
        tblHtml += "<tr>"
        tblHtml += "<tr>"
        tblHtml += "<td>"

        tblHtml += "</td>"
        tblHtml += "</tr>"
        tblHtml += "<tr>"
        tblHtml += "<td>"
        tblHtml += "<span>If you have any questions, please send an email to <b>nsfprogramleads@northsouth.org</b></span> </br>"
        tblHtml += "</td>"
        tblHtml += "</tr>"
        tblHtml += "<tr>"
        tblHtml += "<tr>"
        tblHtml += "<td>"

        tblHtml += "</td>"
        tblHtml += "</tr>"

        tblHtml += "</table>"

        Dim subject As String = String.Empty
        subject = "REMINDER: Zoom Session"

        SendMessage(subject, tblHtml, "")

        Return "Success"
    End Function


    Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@northsouth.org")
        email.To.Add("bindhu.rajalakshmi@capestart.com")
        email.Subject = sSubject & Date.Today.ToString
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        'Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(Server.MapPath(strLogFile))
        'email.Attachments.Add(oAttch)

        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Try
            client.Send(email)
        Catch ex As Exception
            'lblMessage.Text = e.Message.ToString
            ok = False
        End Try

    End Sub

End Class