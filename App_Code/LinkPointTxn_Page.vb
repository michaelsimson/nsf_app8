'////////////////////////////////////////////////////////////////////////////
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
' 
' This software is the proprietary information of LinkPoint International, Inc.  
' Use is subject to license terms.
'////////////////////////////////////////////////////////////////////////////
Imports System
Imports System.Text
Imports System.Collections
Imports System.Configuration
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections.Specialized
Imports LinkPointTransaction
Imports NorthSouth.BAL

Namespace VRegistration.LinkPointAPI_cs

    ' <summary>
' Summary description for LinkPointTxn_Page.
' </summary>
    Public Class LinkPointTxn_Page
        Inherits System.Web.UI.Page

        Public Sub New()
            MyBase.New()

        End Sub

        Public Sub WriteYearOpts()
            Dim year As Integer = DateTime.Now.Year
            Dim pad As String = ""
            Dim sel As String = ""
            Try
                expyearVal = Int32.Parse(expyear)
                If (expyearVal < 10) Then
                    expyearVal = (expyearVal + 2000)
                End If
            Catch e As Exception
                expyearVal = year
            End Try
            Dim yr As Integer = year
            Do While (yr _
                        <= (year + 5))
                sel = (expyearVal = yr)
                'TODO: Warning!!!, inline IF is not supported ?
                pad = ((yr - 2000) _
                            < 10)
                'TODO: Warning!!!, inline IF is not supported ?
                Response.Write(("<option value=" _
                                + (pad _
                                + ((yr - 2000) _
                                + (sel + (">" _
                                + (yr + "</option>")))))))
                yr = (yr + 1)
            Loop
        End Sub

        Public Sub WriteMonthOpts()
            Dim mn As Integer
            Dim cur As Integer = Int32.Parse(expmonth)
            Dim pad As String = ""
            mn = 1
            Do While (mn <= 12)
                pad = (mn < 10)
                'TODO: Warning!!!, inline IF is not supported ?
                If (mn = cur) Then
                    Response.Write(("<option value=" _
                                    + (pad _
                                    + (mn + (" selected>" _
                                    + (mn + "</option>"))))))
                Else
                    Response.Write(("<option value=" _
                                    + (pad _
                                    + (mn + (">" _
                                    + (mn + "</option>"))))))
                End If
                mn = (mn + 1)
            Loop
        End Sub

        Protected Sub ParseFormData()
            '            bname = Request.Form["bname"];
            '            bcompany = Request.Form["bcompany"];
            '            baddr1 = Request.Form["baddr1"];
            '            baddr2 = Request.Form["baddr2"];
            '            bstate = Request.Form["bstate"];
            '            bcity = Request.Form["bcity"];
            '            bcountry = Request.Form["bcountry"];
            '            bzip = Request.Form["bzip"];
            '            bemail = Request.Form["bemail"];
            '            bfax = Request.Form["bfax"];
            '            bphone = Request.Form["bphone"];
            '            baddrnum = Request.Form["baddrnum"];
            '
            '            sname = Request.Form["sname"];
            '        
            '            saddr1 = Request.Form["saddr1"];
            '            saddr2 = Request.Form["saddr2"];
            '            scity = Request.Form["scity"];
            '            sstate = Request.Form["sstate"];
            '            scountry = Request.Form["scountry"];
            '            szip = Request.Form["szip"];
            '            scarrier = Request.Form["scarrier"];
            '            sweight = Request.Form["sweight"];
            '            sitems = Request.Form["sitems"];
            '            stotal = Request.Form["stotal"];   
            ' 
            '            //phonenumber = Request.Form["phone"];
            '            //faxnumber = Request.Form["fax"];
            '            //emailaddress = Request.Form["emailaddress"];
            '            comments = Request.Form["comments"];
            '
            '            cardnumber = Request.Form["cardnumber"];
            '            expmonth = Request.Form["expmonth"];
            '            expyear = Request.Form["expyear"];
            '            //cvmindicator = Request.Form["cvmindicator"];
            '            cvmvalue = Request.Form["cvmvalue"];
            '
            '            subtotal = Request.Form["subtotal"];
            '            if ( (subtotal == null) || subtotal.Length < 1 ){ subtotal = "0.0"; }
            '            tax = Request.Form["tax"];
            '            if ( (tax==null) || tax.Length < 1 ){ tax = "0.0"; }
            '            shipping = Request.Form["shipping"];
            '            if ( (shipping==null) || shipping.Length < 1 ){ shipping = "0.0"; }
            '            total = Request.Form["total"];
            '            if ( (total==null) || total.Length < 1 ){ total = "0.0"; }
            '            pbthreshold = Request.Form["pbthreshold"];
            '            startdate = Request.Form["startdate"];
            '            periodicity = Request.Form["periodicity"];
            '            installments = Request.Form["installments"];
            '            pbcomments = Request.Form["pbcomments"];
            '
            '            accttype = Request.Form["accttype"];
            '            micr = Request.Form["micr"];
            '
            '            accountNbr = Request.Form["accountNbr"];
            '            routeNbr = Request.Form["routeNbr"];
            '            bankName = Request.Form["bankName"];
            '            bankState = Request.Form["bankState"];
            '            dl = Request.Form["dl"];
            '            dlState = Request.Form["dlState"];
            '            ssn = Request.Form["ssn"];
            '            checkNbr = Request.Form["checkNbr"];
            '
            '            refnumber = Request.Form["refnumber"]; 
            '            tax   = Request.Form["tax"]; 
            '            taxexempt = Request.Form["taxexempt"]; 
            '            oid  = Request.Form["oid"]; 
            '
            '            comments = Request.Form["comments"]; 
            '            referred  = Request.Form["referred"]; 
            '            result = Request.Form["result"]; 
            '            origin  = Request.Form["origin"]; 
            GetConfigParams()
        End Sub

        Protected Sub ParseResponse(ByVal rsp As String)
            Dim objParse As New NorthSouth.BAL.PraseCreditString
            objParse.ParseResponse(rsp)
            With objParse
                R_Approved = .R_Approved
                R_Authresr = .R_Authresr
                R_AVS = .R_AVS
                R_Code = .R_Code
                R_Error = .R_Error
                R_ESD = .R_ESD
                R_FraudCode = .R_FraudCode
                R_Message = .R_Message
                R_OrderNum = .R_OrderNum
                R_Ref = .R_Ref
                R_Score = .R_Score
                R_Shipping = .R_Shipping
                R_Tax = .R_Tax
                R_TDate = .R_TDate
                R_Time = .R_Time
            End With
            'R_Time = ParseTag("r_time", rsp)
            'R_Ref = ParseTag("r_ref", rsp)
            'R_Approved = ParseTag("r_approved", rsp)
            'R_Code = ParseTag("r_code", rsp)
            'R_Authresr = ParseTag("r_authresronse", rsp)
            'R_Error = ParseTag("r_error", rsp)
            'R_OrderNum = ParseTag("r_ordernum", rsp)
            'R_Message = ParseTag("r_message", rsp)
            'R_Score = ParseTag("r_score", rsp)
            'R_TDate = ParseTag("r_tdate", rsp)
            'R_AVS = ParseTag("r_avs", rsp)
            'R_Tax = ParseTag("r_tax", rsp)
            'R_Shipping = ParseTag("r_shipping", rsp)
            'R_FraudCode = ParseTag("r_fraudCode", rsp)
            'R_ESD = ParseTag("esd", rsp)
        End Sub

        Protected Function ParseTag(ByVal tag As String, ByVal rsp As String) As String
            Dim retValue As String
            Dim sb As StringBuilder = New StringBuilder(256)
            sb.AppendFormat("<{0}>", tag)
            Dim len As Integer = sb.Length
            Dim idxEnd As Integer = -1
            Dim idxSt As Integer = -1

            If Not (-1 = (idxSt = rsp.IndexOf(sb.ToString()))) Then
                idxSt += len
                sb.Remove(0, len)
                sb.AppendFormat("</{0}>", tag)
                If Not (-1 = idxEnd = rsp.IndexOf(sb.ToString(), idxSt)) Then
                    retValue = rsp.Substring(idxSt, idxEnd - idxSt)
                Else
                    retValue = ""
                End If
            Else
                retValue = ""
            End If
            Return retValue
        End Function
        Protected bname As String = "Joe Customer"
        Protected bcompany As String = "SomeWhere, Inc."
        Protected baddr1 As String = "123 Broadway"
        Protected baddr2 As String = "Suite 23"
        Protected bstate As String = "CA"
        Protected bcity As String = "Moorpark"
        Protected bcountry As String = "US"
        Protected bphone As String = "8051234567"
        Protected bfax As String = "8059876543"
        Protected bemail As String = "joe.customer@somewhere.com"
        Protected baddrnum As String = "123"
        Protected bzip As String = "87123"
        Protected sname As String = "Joe Customer"
        Protected saddr1 As String = "123 Broadway"
        Protected saddr2 As String = "Suite 23"
        Protected scity As String = "Moorpark"
        Protected sstate As String = "CA"
        Protected scountry As String = "US"
        Protected szip As String = "12345"
        Protected semail As String = "joe.customer@somewhere.com"
        Protected sfax As String = "8059876543"
        Protected saddrnum As String = "123"
        Protected sweight As String = "1.2"
        Protected sitems As String = "1"
        Protected scarrier As String = "2"
        Protected stotal As String = "12.99"
        Protected comments As String = "Repeat customer. Ship immediately."
        Protected referred As String = "Saw ad on Web site."
        Protected cardnumber As String = "4111-1111-1111-1111"
        Protected expmonth As String = "03"
        Protected expyear As String = "05"
        Protected cvmvalue As String = "123"
        Protected cvmindicator As String = "provided"
        Protected expyearVal As Integer = 2005
        Protected subtotal As String = "12.99"
        Protected tax As String = "0.34"
        Protected shipping As String = "1.45"
        Protected total As String = "14.78"
        Protected vattax As String = "0.0"
        Protected refnumber As String = "NEW987654"
        Protected ponumber As String = "1203A-G4567"
        Protected taxexempt As String = "N"
        Protected oid As String = "9090018-A345"
        Protected startdate As String = "immediate"
        Protected periodicity As String = "monthly"
        Protected installments As String = "3"
        Protected pbthreshold As String = "3"
        Protected pbcomments As String = ""
        Protected accttype As String = "pc"
        Protected micr As String = "%B4111111111111111^CUSTOMER/JOE           ^0503000500000?"
        Protected accountNbr As String = "2139842610"
        Protected routeNbr As String = "123456789"
        Protected bankName As String = ""
        Protected bankState As String = "CA"
        Protected dl As String = "120381698"
        Protected dlState As String = "CA"
        Protected ssn As String = ""
        Protected checkNbr As String = ""
        'protected string result="LIVE";   
        Protected result As String = "GOOD"
        Protected origin As String = "MOTO"
        ' transaction object
        Protected LPTxn As LinkPointTxn
        ' config params
        Protected configfile As String
        Protected keyfile As String
        Protected host As String
        Protected port As Integer
        ' response holders
        Protected R_Time As String
        Protected R_Ref As String
        Protected R_Approved As String
        Protected R_Code As String
        Protected R_Authresr As String
        Protected R_Error As String
        Protected R_OrderNum As String
        Protected R_Message As String
        Protected R_Score As String
        Protected R_TDate As String
        Protected R_AVS As String
        Protected R_FraudCode As String
        Protected R_ESD As String
        Protected R_Tax As String
        Protected R_Shipping As String
        Protected Shared dbg As Boolean = False

        Protected Sub GetConfigParams()
            ' get some stuff from app config
            Dim config As NameValueCollection = CType(ConfigurationManager.GetSection("LPAPP_Config/client"), NameValueCollection)
            configfile = config("Configfile")
            keyfile = config("Keyfile")
            host = config("Host")
            port = Int32.Parse(config("Port"))
            result = config("result")
            config = CType(ConfigurationManager.GetSection("LPAPP_Config/opts"), NameValueCollection)
            ' not in use anymore
            Try
                dbg = Boolean.Parse(config("dbg"))
            Catch e As Exception

            End Try
        End Sub

    End Class
End Namespace
