﻿Imports System.Web
Imports System.Web.Services
Imports System.Web.Services.Protocols

Imports Microsoft.VisualBasic
Imports Microsoft.ApplicationBlocks.Data

Public Class GlobalVariable

    Public Function GetYear(con As String, EventId As Integer) As Integer
        Dim iYear As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(con, CommandType.Text, "select isnull(EventYear,year(GETDATE())) from Event where EventId=" & EventId))

        Return iYear

    End Function

    Public Function EmailAddress(choice As String) As String
        Select Case choice
            Case "coaching"
                Return "coaching@northsouth.org"
            Case "workshop"
                Return "workshops@northsouth.org"
        End Select
        'Case "support"
        Return "support@northsouth.org"
    End Function
    Public Sub clrSessionBeginRegistration()
        HttpContext.Current.Session.Remove("RegFee")
        HttpContext.Current.Session.Remove("MealCharges")
        HttpContext.Current.Session.Remove("LATEFEE")
        HttpContext.Current.Session.Remove("SaleAmt")
        HttpContext.Current.Session.Remove("FundRAmt") 
        HttpContext.Current.Session.Remove("SaleItems")
        HttpContext.Current.Session.Remove("Donation")
        HttpContext.Current.Session.Remove("Discount")
    End Sub
    Public Function GetSBCredentials() As SBCredentials
        Dim obj As SBCredentials = New SBCredentials With {.UserName = "mohan@gmail.com",
                                                    .Password = "password"}
        Return obj
    End Function

    Public Function GetAllSupportMail() As SupportMails
        Dim obj As SupportMails = New SupportMails With {.Support = EmailAddress("support"),
                                                         .Coaching = EmailAddress("coaching"),
                                                         .Workshop = EmailAddress("workshop")}
        Return obj
    End Function
End Class
Public Class SBCredentials
    Public UserName As String
    Public Password As String

End Class

Public Class SupportMails
    Public Support As String
    Public Coaching As String
    Public Workshop As String

End Class
