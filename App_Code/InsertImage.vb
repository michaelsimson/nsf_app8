﻿'***************************** Module Header ******************************\
' Module Name:  InsertImage.vb
' Project:      VBOpenXmlInsertImageToPPT
' Copyright(c)  Microsoft Corporation.
' 
' The Class is used to Insert Image into PowerPoint using Open XML SDK.
' 
' This source is subject to the Microsoft Public License.
' See http://www.microsoft.com/en-us/openness/licenses.aspx.
' All other rights reserved.
' 
' THIS CODE AND INFORMATION IS PROVIDED "AS IS" WITHOUT WARRANTY OF ANY KIND, 
' EITHER EXPRESSED OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED 
' WARRANTIES OF MERCHANTABILITY AND/OR FITNESS FOR A PARTICULAR PURPOSE.
'**************************************************************************/

Imports System.IO
Imports System.Linq
Imports DocumentFormat.OpenXml
Imports DocumentFormat.OpenXml.Drawing
Imports DocumentFormat.OpenXml.Office2010.Drawing
Imports DocumentFormat.OpenXml.Packaging
Imports DocumentFormat.OpenXml.Presentation
'Imports A = DocumentFormat.OpenXml.Drawing
Imports P = DocumentFormat.OpenXml.Presentation
Imports D = DocumentFormat.OpenXml.Drawing
Public Class InsertImage
    ''' <summary>
    ''' Insert a new Slide into PowerPoint
    ''' </summary>
    ''' <param name="presentationPart">Presentation Part</param>
    ''' <param name="layoutName">Layout of the new Slide</param>
    ''' <returns>Slide Instance</returns>
    ''' 
    Public Function InsertSlide(presentationPart As PresentationPart, layoutName As String) As Slide
        Dim slideId As UInt32 = 256UI

        ' Get the Slide Id collection of the presentation document
        Dim slideIdList = presentationPart.Presentation.SlideIdList

        If slideIdList Is Nothing Then
            Throw New NullReferenceException("The number of slide is empty, please select a ppt with a slide at least again")
        End If

        slideId += Convert.ToUInt32(slideIdList.Count())

        ' Creates a Slide instance and adds its children.
        Dim slide As New Slide(New CommonSlideData(New ShapeTree()))

        Dim slidePart As SlidePart = presentationPart.AddNewPart(Of SlidePart)()
        slide.Save(slidePart)

        ' Get SlideMasterPart and SlideLayoutPart from the existing Presentation Part
        Dim slideMasterPart As SlideMasterPart = presentationPart.SlideMasterParts.First()
        Dim slideLayoutPart As SlideLayoutPart = slideMasterPart.SlideLayoutParts.SingleOrDefault(Function(sl) sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName, StringComparison.OrdinalIgnoreCase))
        If slideLayoutPart Is Nothing Then
            Throw New Exception("The slide layout " & layoutName & " is not found")
        End If

        slidePart.AddPart(Of SlideLayoutPart)(slideLayoutPart)

        slidePart.Slide.CommonSlideData = DirectCast(slideMasterPart.SlideLayoutParts.SingleOrDefault(Function(sl) sl.SlideLayout.CommonSlideData.Name.Value.Equals(layoutName)).SlideLayout.CommonSlideData.Clone(), CommonSlideData)

        ' Create SlideId instance and Set property
        Dim newSlideId As SlideId = presentationPart.Presentation.SlideIdList.AppendChild(Of SlideId)(New SlideId())
        newSlideId.Id = slideId
        newSlideId.RelationshipId = presentationPart.GetIdOfPart(slidePart)

        Return GetSlideByRelationShipId(presentationPart, newSlideId.RelationshipId)
    End Function

    ''' <summary>
    ''' Get Slide By RelationShip ID
    ''' </summary>
    ''' <param name="presentationPart">Presentation Part</param>
    ''' <param name="relationshipId">Relationship ID</param>
    ''' <returns>Slide Object</returns>
    Private Shared Function GetSlideByRelationShipId(presentationPart As PresentationPart, relationshipId As StringValue) As Slide
        ' Get Slide object by Relationship ID
        Dim slidePart As SlidePart = TryCast(presentationPart.GetPartById(relationshipId), SlidePart)
        If slidePart IsNot Nothing Then
            Return slidePart.Slide
        Else
            Return Nothing
        End If
    End Function

    Public Sub SetTitle(slide As Slide, Title As String)
        Dim shapeTextBox As New P.ShapeProperties()
        Dim transformTB As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offsetTB As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 1407200L, _
          .Y = 124000L _
        }
        Dim extentsTB As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 6529600L, _
          .Cy = 429200L _
        }
        transformTB.Append(offsetTB)
        transformTB.Append(extentsTB)
        Dim shapeType As D.ShapeTypeValues = ShapeTypeValues.Rectangle '// Any of the built-in shapes (ellipse, rectangle, etc)
        Dim rgbColorHex As String = "FF0000"
        Dim presetGeometry1 As D.PresetGeometry = New D.PresetGeometry() With {.Preset = shapeType}
        Dim adjustValueList1 As D.AdjustValueList = New D.AdjustValueList()

        presetGeometry1.Append(adjustValueList1)

        Dim solidFill1 As D.SolidFill = New D.SolidFill()
        Dim rgbColorModelHex1 As D.RgbColorModelHex = New D.RgbColorModelHex() With {.Val = rgbColorHex}

        solidFill1.Append(rgbColorModelHex1)
        Dim outline1 As D.Outline = New D.Outline() With {.Width = 12700}

        Dim solidFill2 As D.SolidFill = New D.SolidFill()
        Dim schemeColor1 As D.SchemeColor = New D.SchemeColor() With {.Val = D.SchemeColorValues.Dark1}
        solidFill2.Append(schemeColor1)

        outline1.Append(solidFill2)

        shapeTextBox.Append(transformTB)
        shapeTextBox.Append(presetGeometry1)
        shapeTextBox.Append(solidFill1)
        Dim drawingObjectIdTB As UInt16 = 1
        ' Declare and instantiate the title shape of the new slide.
        Dim shapeTB As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        shapeTB.Append(shapeTextBox)

        ' Specify the required shape properties for the title shape. 
        shapeTB.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectIdTB, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Title}))
        shapeTB.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties(New SolidFill(New RgbColorModelHex() _
             With {.Val = "FFFFFF"})) With {.FontSize = 1700}, New Drawing.Text() With {.Text = Title})))
         
    End Sub

    Public Sub CreateParagraph(slide As Slide, p1 As String, p2 As String, p3 As String, p4 As String)
        '' Creates a ShapeProperties instance and adds its children.
        Dim shapeProperties1 As New P.ShapeProperties()
     
        Dim transform2D1 As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset1 As New DocumentFormat.OpenXml.Drawing.Offset() With { _
          .X = 507200L, _
          .Y = 2524000L _
        }
        Dim extents1 As New DocumentFormat.OpenXml.Drawing.Extents() With { _
          .Cx = 8029600L, _
          .Cy = 3029200L _
        }
        '.Cx = 8029600L, _
        '.Cy = 529200L _
        transform2D1.Append(offset1)
        transform2D1.Append(extents1)
        '//1- point
        shapeProperties1.Append(transform2D1)
        ' shapeProperties2.Append(transform2D1.Clone)
        Dim drawingObjectId As UInt16 = 1
        ' Declare and instantiate the title shape of the new slide.
        Dim titleShape1 As DocumentFormat.OpenXml.Presentation.Shape = slide.CommonSlideData.ShapeTree.AppendChild _
            (New DocumentFormat.OpenXml.Presentation.Shape())
        titleShape1.Append(shapeProperties1)
        drawingObjectId = (drawingObjectId + 1)

        ' Specify the required shape properties for the title shape. 
        titleShape1.NonVisualShapeProperties = New DocumentFormat.OpenXml.Presentation.NonVisualShapeProperties(New  _
            DocumentFormat.OpenXml.Presentation.NonVisualDrawingProperties() With {.Id = drawingObjectId, .Name = "Title"}, _
            New DocumentFormat.OpenXml.Presentation.NonVisualShapeDrawingProperties _
            (New Drawing.ShapeLocks() With {.NoGrouping = True}), _
            New ApplicationNonVisualDrawingProperties(New PlaceholderShape() With {.Type = PlaceholderValues.Body}))

        titleShape1.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
             New Drawing.ListStyle, New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p1})), _
         New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p2})), _
         New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p3})), _
         New Drawing.Paragraph _
             (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = p4})))
    End Sub

    ''' <summary>
    ''' Insert Image into Slide
    ''' </summary>
    ''' <param name="imagePath">Image Path</param>
    ''' <param name="imageExt">Image Extension</param>
    Public Sub InsertImageInLastSlide(slide As Slide, imagePath As String, imageExt As String, p1 As String, p2 As String, p3 As String, p4 As String, FooterTitle As String, SlideTitle As String)
        If p1.Length > 0 Then
            CreateParagraph(slide, p1, p2, p3, p4)
        End If
        If SlideTitle.Length > 0 Then
            SetTitle(slide, SlideTitle)
        End If
        ' Creates a Picture instance and adds its children.
        Dim picture As New P.Picture()
        Dim embedId As String = String.Empty
        embedId = "rId" & (slide.Elements(Of P.Picture)().Count() + 915).ToString()
        Dim nonVisualPictureProperties As New P.NonVisualPictureProperties(New P.NonVisualDrawingProperties() With { _
         .Id = 4, _
         .Name = "Picture 5" _
        }, New P.NonVisualPictureDrawingProperties(New PictureLocks() With { _
         .NoChangeAspect = True _
        }), New ApplicationNonVisualDrawingProperties())

        Dim blipFill As New P.BlipFill()
        Dim blip As New Blip() With { _
         .Embed = embedId _
        }

        ' Creates a BlipExtensionList instance and adds its children
        Dim blipExtensionList As New BlipExtensionList()
        Dim blipExtension As New BlipExtension() With { _
         .Uri = "{28A0092B-C50C-407E-A947-70E740481C1C}" _
        }

        Dim useLocalDpi As New UseLocalDpi() With { _
         .Val = False _
        }
        useLocalDpi.AddNamespaceDeclaration("a14", "http://schemas.microsoft.com/office/drawing/2010/main")

        blipExtension.Append(useLocalDpi)
        blipExtensionList.Append(blipExtension)
        blip.Append(blipExtensionList)

        Dim stretch As New Stretch()
        Dim fillRectangle As New FillRectangle()
        stretch.Append(fillRectangle)

        blipFill.Append(blip)
        blipFill.Append(stretch)

        ' Creates a ShapeProperties instance and adds its children.
        Dim shapeProperties As New P.ShapeProperties()
        '.X = 457200L, _
        ' .Y = 1524000L _
        Dim transform2D As New DocumentFormat.OpenXml.Drawing.Transform2D()
        Dim offset As DocumentFormat.OpenXml.Drawing.Offset
        If p1.Length > 0 Then
            offset = New DocumentFormat.OpenXml.Drawing.Offset() With {.X = 457200L, .Y = 724000L}
        Else
            offset = New DocumentFormat.OpenXml.Drawing.Offset() With {.X = 457200L, .Y = 724000L}
        End If
        Dim extents As DocumentFormat.OpenXml.Drawing.Extents
        If p1.Length > 0 Then
            extents = New DocumentFormat.OpenXml.Drawing.Extents() With {.Cx = 8229600L, .Cy = 1829200L}
        Else
            extents = New DocumentFormat.OpenXml.Drawing.Extents() With {.Cx = 8229600L, .Cy = 5029200L}
        End If
        '.Cx = 8229600L, _
        '.Cy = 5029200L _
        transform2D.Append(offset)
        transform2D.Append(extents)

        Dim presetGeometry As New DocumentFormat.OpenXml.Drawing.PresetGeometry() With { _
         .Preset = DocumentFormat.OpenXml.Drawing.ShapeTypeValues.Rectangle _
        }
        Dim adjustValueList As New DocumentFormat.OpenXml.Drawing.AdjustValueList()

        presetGeometry.Append(adjustValueList)

        shapeProperties.Append(transform2D)
        shapeProperties.Append(presetGeometry)

        picture.Append(nonVisualPictureProperties)
        picture.Append(blipFill)
        picture.Append(shapeProperties)

        slide.CommonSlideData.ShapeTree.AppendChild(picture)

        ' Generates content of imagePart.
        Dim imagePart As ImagePart = slide.SlidePart.AddNewPart(Of ImagePart)(imageExt, embedId)
        Dim fileStream As New FileStream(imagePath, FileMode.Open)
        imagePart.FeedData(fileStream)
        fileStream.Close()

        ' Add footer title 
        Dim footerTitleShapes = slide.CommonSlideData.ShapeTree.ChildElements.OfType(Of DocumentFormat.OpenXml.Presentation.Shape)().Where(Function(s) s.NonVisualShapeProperties.NonVisualDrawingProperties.Name.Value = "Footer Placeholder 2")
        For Each fdt In footerTitleShapes
            fdt.TextBody = New DocumentFormat.OpenXml.Presentation.TextBody(New Drawing.BodyProperties, _
                New Drawing.ListStyle, New Drawing.Paragraph _
                (New Drawing.Run(New Drawing.RunProperties() With {.FontSize = 1500}, New Drawing.Text() With {.Text = FooterTitle})))
        Next


    End Sub
     

End Class
