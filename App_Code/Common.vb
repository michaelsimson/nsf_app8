﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml
Imports NativeExcel
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Namespace VRegistration
    Public Class Common

        Private Function IsUserAuthenthicated(ByVal userName As String, ByVal password As String) As Boolean
            If String.IsNullOrEmpty(userName) <> True And String.IsNullOrEmpty(password) Then
                Return True
            Else
                Return False
            End If
        End Function

        Public Fall() As String = {"Aug", "Sep", "Oct", "Nov", "Dec"}
        Public Spring() As String = {"Feb", "Mar", "Apr"}
        Public Summer() As String = {"Jun", "Jul"}
        Public Function GetSemesterNameByMonth(val As String) As String
            If Fall.Contains(val) Then
                Return "Fall"
            ElseIf Spring.Contains(val) Then
                Return "Spring"
            ElseIf Summer.Contains(val) Then
                Return "Summer"
            End If
            Return ""
        End Function
        Public Function GetSemesters() As ArrayList
            Dim arList As New ArrayList
            arList.Add("Fall")
            arList.Add("Spring")
            arList.Add("Summer")

            Return arList

        End Function

        Public Function GetDefaultSemester(ByVal year As String) As String
            Dim Semester As String = String.Empty
            Try
                Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("DBConnection"))
                cn.Open()

                Dim cmdtext As String
                cmdtext = "select Semester from Event where EventId=13 and Eventyear=" & year & ""
                Dim cmd As SqlCommand = New SqlCommand(cmdtext, cn)
                Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
                Dim ds As DataSet = New DataSet()

                da.Fill(ds)

                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        Semester = ds.Tables(0).Rows(0)("Semester").ToString()
                    Else
                        Semester = "Fall"
                    End If
                End If

            Catch ex As Exception

            End Try
            Return Semester
        End Function
    End Class
End Namespace

