'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/VolunteerSignUp.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page enables the users(Volunteers) to SignUp for Events.
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetVolunteerAcivities
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data.SqlClient


Namespace VRegistration

Partial Class VolunteerSignUp
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'If Session("LoggedIn") <> "LoggedIn" Then
            '    Server.Transfer("login.aspx")
            'End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        Dim da As New SqlDataAdapter
        Dim ds As New DataSet

        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_GetVolunteerAcivities"
        'cmd.Parameters.Add("@NSFChapter", DbType.String).Value = ddlNSFChapter.SelectedValue
        da.SelectCommand = cmd
        da.Fill(ds)
        If ds.Tables.Count > 0 Then
            dgSigningAuth.DataSource = ds.Tables(0)
            dgSigningAuth.DataBind()
        End If

        End Sub
        'TODO : Need to Fix for 2.0 Commented by Venkat
        'Public Sub chkEvent_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles chkEvent_CheckedChanged
        '    Dim chkTemp As CheckBox = CType(sender, CheckBox)
        '    Dim dgi As DataGridItem

        '    dgi = CType(chkTemp.Parent.Parent, DataGridItem)
        '    If (chkTemp.Checked) Then
        '        dgi.BackColor = dgSigningAuth.SelectedItemStyle.BackColor
        '        dgi.ForeColor = dgSigningAuth.SelectedItemStyle.ForeColor
        '    Else
        '        dgi.BackColor = dgSigningAuth.ItemStyle.BackColor
        '        dgi.ForeColor = dgSigningAuth.ItemStyle.ForeColor
        '    End If
        'End Sub
    End Class

End Namespace

