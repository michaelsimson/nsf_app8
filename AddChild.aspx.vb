Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL


Namespace VRegistration

Partial Class AddChild
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Requiredfieldvalidator1 As System.Web.UI.WebControls.RequiredFieldValidator
    Protected WithEvents cuvDOB As System.Web.UI.WebControls.CustomValidator
    Protected WithEvents revDOB As System.Web.UI.WebControls.RegularExpressionValidator
    Protected WithEvents Label2 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            ''Debug 
            'Session("CustIndID") = 4
            'Session("LoggedIn") = "True"
            'Session("CustSpouseID") = 5

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Session("entryToken").ToString.ToUpper = "DONOR" Then
                hlinkParentRegistration.NavigateUrl = "~/DonorFunctions.aspx"
            ElseIf Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                hlinkParentRegistration.NavigateUrl = "~/VolunteerFunctions.aspx"
                hlinkParentRegistration.Text = "Back to Main Page"
            Else
                hlinkParentRegistration.NavigateUrl = "~/UserFunctions.aspx"
                hlinkParentRegistration.Text = "Back to Main Page"
            End If
            If Not Request.QueryString("Id") Is Nothing Then
                hlnkSearch.Visible = True
            End If

        End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            'sandhya - to take care of session time out issues

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                Exit Sub
            End If


            lblDuplicateMessage.Visible = False
            If Not IsDate(Server.HtmlEncode(txtDOB.Text)) Then
                CustDOB.Enabled = True
                CustDOB.IsValid = False
                CustDOB.ErrorMessage = "Date of Birth Should be Valid Date in MM/DD/YYYY form"
                Exit Sub
            End If
            Dim bIsValidate As Boolean
            If Convert.ToDateTime(Server.HtmlEncode(txtDOB.Text)) > DateAdd(DateInterval.Year, -3, Now.Date()) Then
                CustDOB.IsValid = False
                CustDOB.ErrorMessage = "Child Should be atleast 3 Years old"
                Exit Sub
            End If

            If ddlGrade.SelectedValue = "0" Then
                rfvGrade.IsValid = False
            End If
            If ddlCountryofBirth.SelectedValue = "-1" Then
                rfvCountryofBirth.IsValid = False
            End If
            If ddlGender.SelectedValue = "0" Then
                rfvGender.IsValid = False
            End If
            Page.Validate()
            If Not Page.IsValid Then
                Exit Sub
            End If
            Try
                Dim myDateTimeUS As System.DateTime
                Dim format As New System.Globalization.CultureInfo("en-US", True)
                myDateTimeUS = System.DateTime.Parse(txtDOB.Text, format)
                If Convert.ToDateTime(Server.HtmlEncode(txtDOB.Text)) > DateAdd(DateInterval.Year, -3, Now.Date()) Then
                    CustDOB.IsValid = False
                    CustDOB.ErrorMessage = "Child Should be atleast 3 Years old"
                    Exit Sub
                End If

                bIsValidate = True
                Dim iChildAge As Integer
                iChildAge = DateDiff(DateInterval.Year, CDate(txtDOB.Text), CDate(Date.Now))
                If ((iChildAge < Convert.ToInt32(ddlGrade.SelectedValue) + 4) Or (iChildAge > Convert.ToInt32(ddlGrade.SelectedValue) + 6)) Then
                    pnlMessage.Visible = True
                    bIsValidate = False
                    If rblOptions.SelectedValue = "Correct" Then
                        bIsValidate = True
                    Else
                        bIsValidate = False
                    End If
                End If
                If bIsValidate = True Then
                    UpdateChildInfo()
                End If
            Catch ex As Exception
                Response.Write("Not Valid Date of Birth, format should be in MM/DD/yyyy")
            End Try
            If lblDuplicateMessage.Visible = False Then
                If bIsValidate = True Then
                    If Session("entryToken").ToString.ToUpper() = "DONOR" Then
                        Response.Redirect("MainChild.aspx")
                    ElseIf Session("entryToken").ToString.ToUpper() = "PARENT" Then
                        'Response.Redirect("UserFunctions.aspx")
                        Response.Redirect("MainChild.aspx")
                    Else
                        Response.Redirect("VolunteerFunctions.aspx")
                    End If
                End If
            End If
        End Sub
    Private Sub UpdateChildInfo()
        Dim objChild As New Child
            Dim intChildNumber As Integer
            Dim strSql As String
            Dim iMemberID As Double
            If Request.QueryString("Id") Is Nothing Then
                iMemberID = Session("CustIndID")
            Else
                iMemberID = Request.QueryString("Id")
            End If
            Dim bIsDuplicate As Boolean
            strSql = "SELECT * FROM CHILD WHERE memberid = " & iMemberID & " AND UPPER(FIRST_NAME) = '" & txtFirstName.Text.ToUpper() & "' "
            Dim drChildInfo As SqlDataReader
            drChildInfo = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If drChildInfo.Read() Then
                lblDuplicateMessage.Visible = True
                lblDuplicateMessage.Text = "Name already Exists."
                bIsDuplicate = True
            End If
            If bIsDuplicate = False Then
                With objChild
                    If Request.QueryString("Id") Is Nothing Then
                        .MEMBERID = Session("CustIndID")
                    Else
                        .MEMBERID = Request.QueryString("Id")
                    End If
                    .SPOUSEID = Session("CustSpouseID")
                    .FIRST_NAME = StrConv(Server.HtmlEncode(txtFirstName.Text), vbProperCase)
                    .LAST_NAME = StrConv(Server.HtmlEncode(txtLastName.Text), vbProperCase)
                    .MIDDLE_INITIAL = StrConv(Server.HtmlEncode(txtMiddleName.Text), vbProperCase)
                    .GENDER = ddlGender.SelectedValue
                    .DATE_OF_BIRTH = Convert.ToDateTime(Server.HtmlEncode(txtDOB.Text)).ToShortDateString
                    .GRADE = ddlGrade.SelectedValue
                    .SchoolName = txtSchoolName.Text
                    .CountryOfBirth = ddlCountryofBirth.SelectedValue
                    .Acheivements = txtAchievements.Text
                    .Hobbies = txtHobbies.Text
                    .CreateDate = Convert.ToDateTime(Now).ToShortDateString
                    .ModifyDate = Convert.ToDateTime(Now).ToShortDateString

                End With
                intChildNumber = objChild.AddChild(Application("ConnectionString"))
            End If
        End Sub
End Class

End Namespace

