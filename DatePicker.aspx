<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.DatePicker" CodeFile="DatePicker.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>DatePicker</title>
		<LINK href="Styles.css" rel="stylesheet">
			<script language="Javascript">
			function SetDate(dateValue)
			{
				// retrieve from the querystring the value of the Ctl param,
				// that is the name of the input control on the parent form
			    // that the user want to set with the clicked date
				ctl = window.location.search.substr(5);
				// set the value of that control with the passed date
				thisForm = window.opener.document.forms[0].elements[ctl].value = dateValue;
				// close this popup
				self.close();
				
			}
			</script>
	</HEAD>
	<body leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
		<form id="Form1" method="post" runat="server">
			<asp:calendar id="Picker" runat="server" Height="180px" Width="200px" BackColor="White" BorderColor="#999999"
				ForeColor="Black" DayNameFormat="FirstLetter" Font-Size="8pt" Font-Names="Verdana" CellPadding="4">
				<TodayDayStyle ForeColor="Black" BackColor="#CCCCCC"></TodayDayStyle>
				<SelectorStyle BackColor="#CCCCCC"></SelectorStyle>
				<NextPrevStyle VerticalAlign="Bottom"></NextPrevStyle>
				<DayHeaderStyle Font-Size="7pt" Font-Bold="True" BackColor="#CCCCCC"></DayHeaderStyle>
				<SelectedDayStyle Font-Bold="True" ForeColor="White" BackColor="#666666"></SelectedDayStyle>
				<TitleStyle Font-Bold="True" BorderColor="Black" BackColor="#999999"></TitleStyle>
				<WeekendDayStyle BackColor="#FFFFCC"></WeekendDayStyle>
				<OtherMonthDayStyle ForeColor="#808080"></OtherMonthDayStyle>
			</asp:calendar>
		</form>
	</body>
</HTML>

 

 
 
 