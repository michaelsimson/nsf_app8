﻿
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Partial Class CoachingSelectionSum
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            Session("ChildNumbers") = Nothing
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadData()
            lblNote.Text = "<br /> Note:Product Level Pricing flag 'N' Calculates the Reg fee only once for the Product group as a whole. <br />"
            Dim discount As Decimal = 0
            If Not Session("Discount") Is Nothing And Session("Discount") > 0 Then
                discount = Session("Discount")
            End If


            lblTotalFee.Text = " Total Reg Fee: " & Format$(Session("TotalFee") - discount, "Currency") ' Session("RegFee") + Session("LateFee")
        End If

    End Sub

    Private Sub LoadData()
        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
        conn.Open()
        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                ' lblCity.Text = drIndSpouse.Item("City")
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If
        conn.Close()
        LoadSelectedCoaching()
        'Dim objChild As New Child
        'Dim dsChild As New DataSet

        'objChild.SearchChildWhere(Application("ConnectionString"), dsChild, "MemberId='" & intIndID & "'")

        'If dsChild.Tables.Count > 0 Then
        '    dgChildList.DataSource = dsChild.Tables(0)
        '    dgChildList.DataBind()
        '    Session("ChildCount") = dsChild.Tables(0).Rows.Count
        '    ViewState("ChildInfo") = dsChild
        'End If

        '********************************************************
        '*** Show Selected Contests
        '********************************************************

    End Sub

    Private Sub LoadSelectedCoaching()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "select CR.CoachRegID, CR.ChapterID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,P.ProductID,P.ProductGroupID,P.ProductGroupCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level" 'Case When Ch.Grade In (6,7,8) and P.ProductGroupCode in ('SAT') then 'Junior' else case when Ch.GRADE in(9,10,11,12) and P.ProductGroupCode in ('SAT') then 'Senior' else C.Level End End as Level"
        SQLStr = SQLStr & ",Ch.Grade,C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) + ' EST' as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Y' Else 'N' End as Status,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID "
        SQLStr = SQLStr & ",EF.RegFee,Case When GETDATE()> DateAdd(dd,1,EF.DeadlineDate) Then EF.LateFee Else '' End as LateFee,EF.ProductLevelPricing"
        SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END + ' hr(s)'as Duration "
        SQLStr = SQLStr & " from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.PaymentReference is Null and CR.Approved='N' AND CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & "" '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) 'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
        'SQLStr = SQLStr & " and C.Enddate>Getdate() and CR.EventYear=" & Now.Year & " ORDER BY ChildName,P.ProductID,LevelID "
        SQLStr = SQLStr & " and CR.EventYear=" & Now.Year & " and C.Accepted='Y' ORDER BY ChildName,P.ProductID,LevelID,C.Level,CASE C.Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end, CAST(C.TIME AS TIME)"

        'Response.Write(SQLStr)
        conn.Open()
        Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
        dgselected.DataSource = drCoaching
        dgselected.DataBind()
        conn.Close()
        If dgselected.Items.Count > 0 Then
            dgselected.Visible = True

            Dim dgItem As DataGridItem
            Dim strPaymentNotes As String
            Dim Lbl1 As Label
            Dim Lbl2 As Label
            Dim Lbl3 As Label
            Dim Lbl4 As Label
            Dim Lbl5 As Label
            Dim LblProductGroup As Label
            Dim ProdGroupCount As Double = 0.0
            Dim Products As String = ""
            Dim ProductCount As Integer = 0
            strPaymentNotes = ""
            For Each dgItem In dgselected.Items
                Lbl1 = dgItem.FindControl("lblRegFee")
                Lbl2 = dgItem.FindControl("lblProductCode")
                strPaymentNotes = strPaymentNotes + Lbl2.Text
                Lbl3 = dgItem.FindControl("lblChildNumber")
                strPaymentNotes = strPaymentNotes + "(" + Lbl3.Text + ")"
                Lbl4 = dgItem.FindControl("lblChapterID")
                strPaymentNotes = strPaymentNotes + "(" + Lbl4.Text + ")"
                LblProductGroup = dgItem.FindControl("lblProductGroupID")
                ProdGroupCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From EventFees Where EventID=" & Session("EventID") & " and EventYear=" & Now.Year() & " and ProductGroupID=" & LblProductGroup.Text & " and ProductLevelPricing ='N'")
                If ProdGroupCount > 0 Then
                    Lbl1.Text = Format$((Lbl1.Text / ProdGroupCount), "Currency") 'Lbl1.Text / ProdGroupCount
                End If
                strPaymentNotes = strPaymentNotes + "(" + Lbl1.Text.Replace("$", "") + ")"
                Lbl5 = dgItem.FindControl("lblProductGroupCode")
                If Lbl5.Text.Contains("SAT") Then
                    ProductCount = ProductCount + 1
                End If
            Next

            Session("ContestsSelected") = strPaymentNotes
            If ProductCount = dgselected.Items.Count Then
                tdclasstime.Visible = False
                Session("ProductType") = "SAT"
            Else
                tdclasstime.Visible = True
            End If
            ' lblSelected.Text = ""
        Else
            dgselected.Visible = False
            ' lblSelected.ForeColor = Color.Red
            'lblSelected.Text = "No Coaching was selected"
        End If
        lblCommuEmail.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSpouse where AutoMemberID=" & Session("CustIndID") & "")
        'conn.Close()
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        lblContestInfo.Text = ""
        lblEmailInfo.Text = ""
        If ChkEmail.Checked = False Then
            lblContestInfo.Text = "Please check that you will use this email for all communications with NSF."
            Exit Sub
        ElseIf tdclasstime.Visible = True And ChkClassTime.Checked = False Then
            lblContestInfo.Text = "Please agree that you have reviewed the level determination document."
            Exit Sub
        ElseIf ChkDetails.Checked = False Then
            lblContestInfo.Text = "Please check that Class timing and level cannot be changed later and that seat is not guaranteed until paid."
            Exit Sub
        Else
            lblContestInfo.Text = ""
            Response.Redirect("ChildAccess.aspx?id=1")
        End If
    End Sub

    Protected Sub hlinkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkEmail.Click
        lblEmailInfo.Text = "Please Refresh the Page after Email Change and Proceed"
        Response.Redirect("ChangeEmail.aspx?ID=2")
        'Response.Write("<script language='javascript'>window.open('ChangeEmail.aspx','_blank','left=150,top=0,width=800,height=500,toolbar=0,location=0,scrollbars=1');</script> ")
    End Sub
    Dim str As String = ""
    Protected Sub dgselected_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgselected.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            Dim lblChildNumber As Label = CType(e.Item.FindControl("lblChildNumber"), Label)
            If Not Session("ChildNumbers") Is Nothing Then
                str = Session("ChildNumbers")
                If str.Length > 0 Then
                    str = str & ","
                End If
                str = str & lblChildNumber.Text
            Else
                str = lblChildNumber.Text
            End If
            Session("ChildNumbers") = str
        End If
    End Sub
End Class
