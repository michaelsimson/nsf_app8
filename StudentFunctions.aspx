﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="StudentFunctions.aspx.cs" Inherits="VRegistration.StudentFunctions" Title="North South Foundation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div class="title02" align="center">
	Student Functions
  </div>
  
  	<table id="Table1" class="tableclass"  borderColor="#cc6600" cellSpacing="1" 
        borderColorDark="#ffffff" cellPadding="3"
				 bgColor="#ff9966" borderColorLight="#ffff66" border="2" align="center">
				<tr id="trMessage" runat="server">
				    <td colspan="4" class="title04"  borderColor="#ffff99" bgColor="#ffffcc">
				            <asp:Label CssClass="link" runat="server" ID="lblMessage"></asp:Label>				    
					</td>
				</tr>
					<tr>
					    <th borderColor="#ffff99" bgColor="#ffffcc" width="20%" align="center">Application</th>
						<th borderColor="#ffff99" bgColor="#ffffcc" width="30%" align="center">Main Option</th>
						<th borderColor="#ffff99" bgColor="#ffffcc" width="25%" align="center">Reports</th>	
						<th borderColor="#ffff99" bgColor="#ffffcc" width="25%" align="center">Bulletin Board</th>						
					</tr>
					<tr id="tr1" runat="server">
					<td borderColor="#ffff99" bgColor="#ffffcc" class="title04">Coaching</td><td borderColor="#ffff99" bgColor="#ffffcc">
				
					<p><asp:LinkButton  CssClass="btn_02" id="lbtnEnterAnswers" runat="server" 
                            onclick="lbtnEnterAnswers_Click">Enter Answers</asp:LinkButton></p>
                    	</td><td borderColor="#ffff99" bgColor="#ffffcc">
					<p><asp:hyperlink  CssClass="btn_02" id="Hyperlink8" runat="server" NavigateUrl="CoachingStatus.aspx">View Status for Coaching</asp:hyperlink></p>
                            <p><asp:hyperlink  CssClass="btn_02" id="Hyperlink1" runat="server" NavigateUrl="~/Reports/CoachClassCalStudentsRpt.aspx">Class Calendar</asp:hyperlink></p>
					<p><asp:hyperlink CssClass="btn_02" id="Hyperlink16" runat="server" NavigateUrl="ViewSATTestScores.aspx"> View Scores</asp:hyperlink></p>					
                    <p><asp:LinkButton ID="bttnDownloadPapers" runat="server" CssClass="btn_02" 
                            onclick="bttnDownloadPapers_Click">Download Papers</asp:LinkButton></p>
					</td><td borderColor="#ffff99" bgColor="#ffffcc">
					<p><asp:hyperlink CssClass="btn_02" id="Hyperlink11" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc">Mathcounts - Level Determination</asp:hyperlink></p>
					<p><asp:hyperlink CssClass="btn_02" id="Hyperlink12" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc">Pre-Mathcounts Level Determination</asp:hyperlink></p>
					<p><asp:hyperlink CssClass="btn_02" id="Hyperlink13" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/FAQ - MathCounts.docx">Mathcounts FAQ</asp:hyperlink></p>
					<p><asp:hyperlink CssClass="btn_02" id="Hyperlink14" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/FAQ - Pre-MathCounts.docx">Pre-Mathcounts FAQ</asp:hyperlink></p>
					
					</td></tr>
					
					<tr><td borderColor="#ffff99" bgColor="#ffffcc" class="title04">
					Games</td><td borderColor="#ffff99" bgColor="#ffffcc">
					<p><asp:LinkButton  CssClass="btn_02" id="hlinkRegisterForGame" runat="server" >Play Game</asp:LinkButton></p>
					</td><td borderColor="#ffff99" bgColor="#ffffcc">
					</td><td borderColor="#ffff99" bgColor="#ffffcc"></td></tr>
					</table> 
</asp:Content>

