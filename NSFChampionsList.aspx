﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" EnableEventValidation="false"
    AutoEventWireup="true" CodeFile="NSFChampionsList.aspx.cs" Inherits="NSFChampionsList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        Champions List
    </div>
    <br />
    <style>
        div.ex
        {
            width: 500px;
            padding: 10px;
            border: 5px solid black;
            margin: 1px;
        }
    </style>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Label ID="lblFrmYear" runat="server" Text="From Year"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="FromYear" runat="server" Width="150px">
    </asp:DropDownList>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
                 <asp:Label ID="LbToyear" runat="server" Text="To Year"></asp:Label>
    &nbsp;&nbsp;&nbsp;&nbsp;
    <asp:DropDownList ID="ToYear" runat="server" Width="150px">
    </asp:DropDownList>


    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnsearchChild0" runat="server" Text="Generate Report" OnClick="btnsearchChild0_Click" />
<%--    <div style=" border: 3px solid black; padding:5px;">--%>
      <%--      <table id="Tblborder" style=" border: 3px solid black; padding:5px;"><tr><td></td></tr>--%>
          <%--  <tr>--%>
       
    <asp:DataList runat="server" ID="DataLists"
        OnItemDataBound="DataLists_ItemDataBound" Width="620px">
        <ItemTemplate>
         
            <table align="center" id="Head" runat="server">
                <tr>

                    <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Label runat="server" Font-Size="Large" Font-Bold="true" ID="lblname" Text='<%# Eval("Name") %>'></asp:Label>
                      
                        <asp:HiddenField ID="hdn" runat="server" Value='<%# Eval("ProductGroupCode") %>'>
                        </asp:HiddenField>
                    </td>

                </tr>
               
            </table>
              
          &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <asp:DataList
                ID="DataList1" RepeatColumns="2" RepeatDirection="Horizontal"
                runat="server" OnItemDataBound="DataList1_ItemDataBound" BorderColor="#0000"
                BorderStyle="None" BorderWidth="1px" CellPadding="3"
                GridLines="Horizontal">

                <ItemTemplate>
                  
                    
         
                           <table align="center">
                               <tr>
                                   <td>
                                       <asp:Label runat="server" ID="lblyear" Font-Bold="true" Text='<%# Eval("ContestYear") %>'></asp:Label>
                                   </td>
                                   <td style="width: 957px">
                                       <asp:Label runat="server" ID="lblProductGroupCode" Font-Bold="true" Text='<%# Eval("Name") %>'></asp:Label>
                                       <asp:Label runat="server" ID="lblprod" Visible="false" Text='<%# Eval("ProductCode") %>'></asp:Label>
                                   </td>


                                   <asp:Repeater ID="rp_Postings" runat="server">
                                       <ItemTemplate>
                                           <tr>
                                               <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label runat="server" ID="Label2" Text='<%# Eval("Rank") %>'></asp:Label>
                                                   <%--  <asp:HiddenField ID="hdnpostChargeid" runat="server" Value='<%# Eval("PostChargeID") %>' />--%>
                                               </td>
                                               <td td style="width: 957px>
                                                   <asp:Label runat="server" ID="lblname" Text='<%# Eval("FIRST_NAME") %>'></asp:Label>
                                                   <asp:Label runat="server" ID="Label1" Text='<%# Eval("LAST_NAME") %>'></asp:Label>,
                                                   <asp:Label runat="server" ID="Label3" Text='<%# Eval("City") %>'></asp:Label>,
                                              <asp:Label runat="server" ID="Label4" Text='<%# Eval("State") %>'></asp:Label>

                                               </td>

                                           </tr>
                                       </ItemTemplate>
                                   </asp:Repeater>


                                   </td>
                               </tr>


                           </table>
                            
                           

                </ItemTemplate>

            </asp:DataList>
        </ItemTemplate>
    </asp:DataList>

    <%--</tr>--%>
  <%--     </table>--%>
    <br />
    <br />
    <br />

    

    </div>
</asp:Content>

