﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using NativeExcel;
using System.Collections;
using VRegistration;
public partial class StudentRoster : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                loadyear();
                loadPhase();
                fillStudents();
            }
        }
    }

    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    public void fillStudents()
    {
        string cmdText = "";
        cmdText = "select distinct case when CR.AdultId is null then CR.ChildNumber else CR.AdultId end as ChildNumber , case when CR.AdultId is null then C.First_Name+' '+C.Last_name +'-'+'Child' else IP.FirstName +' '+ IP.LastName+'-'+'Adult' end as Name, C.First_Name, C.Last_name from CoachReg CR  left join Child C on (C.ChildnUmber=CR.ChildNumber) left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) where CR.EvenTYear=" + ddYear.SelectedValue + " and CR.Approved='Y' and CR.Semester='" + DDlSemester.SelectedValue + "' order by C.First_Name, C.Last_name ASC";
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlChild.DataSource = ds;
                ddlChild.DataTextField = "Name";
                ddlChild.DataValueField = "ChildNumber";
                ddlChild.DataBind();
                ddlChild.Items.Insert(0, new ListItem("Select", "0"));


            }
        }
        catch
        {
        }

    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillStudents();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validation() == 1)
        {
            fillStudentRoster();

        }
    }

    public void fillStudentRoster()
    {
        string cmdText = string.Empty;
        cmdText = "select distinct C.ChildNumber, C.First_Name+' '+C.Last_Name as ChildName, C.Email,C.PwD,Cs.Meetingkey,cs.Time, IP.FirstName+' '+IP.LastName as Parent1Name, IP.Email as Parent1Email,LM.User_Pwd as Parent1Pwd,IP.CPhone as Parent1Phone, IP1.FirstName+' '+IP1.LastName as Parent2Name, IP1.Email as Parent2Email,LM1.User_Pwd as Parent2Pwd,IP1.CPhone as Parent2Phone, IP2.FirstName+' '+IP2.LastName as CoachName, IP2.Email as CoachEmail,LM2.User_Pwd as CoachPwd,IP2.CPhone as CoachPhone,CS.ProductCode, CS.Day,CS.Vroom,cs.ProductGroupCode,cs.Level,IP2.HPhone,IP2.State,cs.SessionNO, CS.Semester from Indspouse IP inner join Child C on (IP.AutoMemberID=C.MemberID) left join Indspouse IP1 on (IP1.Relationship=IP.AutoMemberID) inner join Login_Master LM on (LM.User_Email=IP.Email) left join Login_Master LM1 on (LM1.User_EMail=IP1.EMail) inner join CoachReg CR on (CR.Childnumber=C.ChildNumber) inner join IndSpouse IP2 on (IP2.AutoMemberID=CR.CMemberID) left join Login_Master LM2 on (LM2.User_Email=IP2.Email) inner join CalSignUp cs on (Cs.MemberID=CR.CMemberID and CS.ProductID=CR.ProductID and CS.Level=CR.Level and CS.SessionNo=CR.SessionNo  and CR.Approved='Y' and CS.Accepted='Y' and CS.Semester='" + DDlSemester.SelectedValue + "' and CR.Eventyear=" + ddYear.SelectedValue + " and CS.Eventyear=" + ddYear.SelectedValue + ")  where C.ChildNumber=" + ddlChild.SelectedValue + " and CR.Approved='Y' and CR.Semester='" + DDlSemester.SelectedValue + "' and CR.Eventyear=" + ddYear.SelectedValue + " order by ChildName ASC";
        if (ddlChild.SelectedItem.Text.Split('-')[1].Trim() == "Adult")
        {
            cmdText = "select distinct CR.AdultId, IP3.FirstName+' '+IP3.LastName as ChildName, IP3.Email,LM2.User_pwd as pwd,Cs.Meetingkey,cs.Time, IP.FirstName+' '+IP.LastName as Parent1Name, IP.Email as Parent1Email,LM.User_Pwd as Parent1Pwd,IP.CPhone as Parent1Phone, IP1.FirstName+' '+IP1.LastName as Parent2Name, IP1.Email as Parent2Email,LM1.User_Pwd as Parent2Pwd,IP1.CPhone as Parent2Phone, IP2.FirstName+' '+IP2.LastName as CoachName, IP2.Email as CoachEmail,LM2.User_Pwd as CoachPwd,IP2.CPhone as CoachPhone,CS.ProductCode, CS.Day,CS.Vroom,cs.ProductGroupCode,cs.Level,IP2.HPhone,IP2.State,cs.SessionNO, CS.Semester from CoachReg CR inner join Indspouse IP3 on (CR.AdultId=IP3.AutoMemberId) inner join Indspouse IP on (IP.AutoMemberID=CR.PMemberID)left join Indspouse IP1 on (IP1.Relationship=IP.AutoMemberID)inner join Login_Master LM on (LM.User_Email=IP.Email) left join Login_Master LM1 on (LM1.User_EMail=IP1.EMail) inner join IndSpouse IP2 on (IP2.AutoMemberID=CR.CMemberID)left join Login_Master LM2 on (LM2.User_Email=IP2.Email) inner join CalSignUp cs on (Cs.MemberID=CR.CMemberID and CS.ProductID=CR.ProductID and CS.Level=CR.Level and CS.SessionNo=CR.SessionNo  and CR.Approved='Y' and CS.Accepted='Y' and CS.Semester='" + DDlSemester.SelectedValue + "' and CR.Eventyear=" + ddYear.SelectedItem.Value + " and CS.Eventyear=" + ddYear.SelectedValue + ") inner join Login_Master LM3 on (IP3.Email=LM3.User_Email)  where CR.AdultId=" + ddlChild.SelectedValue + " and CR.Approved='Y' and CR.Semester='" + DDlSemester.SelectedValue + "' and CR.Eventyear=" + ddYear.SelectedValue + " order by ChildName ASC";
        }

        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                grdStudentRoster.DataSource = ds;
                grdStudentRoster.DataBind();

            }
        }
        catch
        {
        }
    }

    public void exportExcel(string exportType)
    {
        string fileName = string.Empty;
        string cmdText = string.Empty;
        cmdText = "select distinct C.ChildNumber, C.First_Name+' '+C.Last_Name as ChildName, C.Email,C.PwD,Cs.Meetingkey,cs.Time, IP.FirstName+' '+IP.LastName as Parent1Name, IP.Email as Parent1Email,LM.User_Pwd as Parent1Pwd,IP.CPhone as Parent1Phone, IP1.FirstName+' '+IP1.LastName as Parent2Name, IP1.Email as Parent2Email,LM1.User_Pwd as Parent2Pwd,IP1.CPhone as Parent2Phone, IP2.FirstName+' '+IP2.LastName as CoachName, IP2.Email as CoachEmail,LM2.User_Pwd as CoachPwd,IP2.CPhone as CoachPhone,CS.ProductCode, CS.Day,CS.Vroom,cs.ProductGroupCode,cs.Level,IP2.HPhone,IP2.State,cs.SessionNO, CS.Semester from Indspouse IP left join Child C on (IP.AutoMemberID=C.MemberID) left join Indspouse IP1 on (IP1.Relationship=IP.AutoMemberID) left join Login_Master LM on (LM.User_Email=IP.Email) left join Login_Master LM1 on (LM1.User_EMail=IP1.EMail) left join CoachReg CR on (CR.Childnumber=C.ChildNumber) left join IndSpouse IP2 on (IP2.AutoMemberID=CR.CMemberID) left join Login_Master LM2 on (LM2.User_Email=IP2.Email) left join CalSignUp cs on (Cs.MemberID=CR.CMemberID and CS.ProductID=CR.ProductID and CS.Level=CR.Level and CS.SessionNo=CR.SessionNo and CR.Approved='Y' and CS.Accepted='Y' and CS.Semester='" + DDlSemester.SelectedValue + "' and CR.EventYear=" + ddYear.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + ") ";
        if (exportType == "1")
        {
            cmdText += " where CR.EventYear=" + ddYear.SelectedValue + " and CR.Approved='Y' ";
            fileName = "StudentRosterAll_";
        }
        else
        {
            fileName = "StudentRoster_";
            cmdText += " where C.ChildNumber=" + ddlChild.SelectedValue + " and CR.Approved='Y' and CR.EventYear=" + ddYear.SelectedValue + " ";
        }

        cmdText += " order by ChildName ASC";
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Student Roster";
                    oSheet.Range["A1:AA1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Student Roster";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "ChildNumber";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "ChildName";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Email";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "PwD";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "Meetingkey";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Time";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Parent1Name";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Parent1Email";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Parent1Pwd";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Parent1Phone";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "Parent2Name";
                    oSheet.Range["L3"].Font.Bold = true;

                    oSheet.Range["M3"].Value = "Parent2Email";
                    oSheet.Range["M3"].Font.Bold = true;

                    oSheet.Range["N3"].Value = "Parent2Pwd";
                    oSheet.Range["N3"].Font.Bold = true;

                    oSheet.Range["O3"].Value = "Parent2Phone";
                    oSheet.Range["O3"].Font.Bold = true;

                    oSheet.Range["P3"].Value = "CoachName";
                    oSheet.Range["P3"].Font.Bold = true;

                    oSheet.Range["Q3"].Value = "CoachEmail";
                    oSheet.Range["Q3"].Font.Bold = true;

                    oSheet.Range["R3"].Value = "CoachPwd";
                    oSheet.Range["R3"].Font.Bold = true;

                    oSheet.Range["S3"].Value = "CoachPhone";
                    oSheet.Range["S3"].Font.Bold = true;

                    oSheet.Range["T3"].Value = "HPhone";
                    oSheet.Range["T3"].Font.Bold = true;

                    oSheet.Range["U3"].Value = "ProductGroupCode";
                    oSheet.Range["U3"].Font.Bold = true;

                    oSheet.Range["V3"].Value = "ProductCode";
                    oSheet.Range["V3"].Font.Bold = true;

                    oSheet.Range["W3"].Value = "Day";
                    oSheet.Range["W3"].Font.Bold = true;

                    oSheet.Range["X3"].Value = "Vroom";
                    oSheet.Range["X3"].Font.Bold = true;

                    oSheet.Range["Y3"].Value = "Level";
                    oSheet.Range["Y3"].Font.Bold = true;

                    oSheet.Range["Z3"].Value = "SessionNo";
                    oSheet.Range["Z3"].Font.Bold = true;

                    oSheet.Range["AA3"].Value = "State";
                    oSheet.Range["AA3"].Font.Bold = true;

                    oSheet.Range["BB3"].Value = "State";
                    oSheet.Range["BB3"].Font.Bold = true;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["ChildNumber"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["ChildName"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["Pwd"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["MeetingKey"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["Time"].ToString();

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent1Name"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent1Email"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent1Pwd"].ToString();

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent1Phone"].ToString();

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent2Name"].ToString();

                        CRange = oSheet.Range["M" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent2Email"].ToString();

                        CRange = oSheet.Range["N" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent2Pwd"].ToString();

                        CRange = oSheet.Range["O" + iRowIndex.ToString()];
                        CRange.Value = dr["Parent2Phone"].ToString();

                        CRange = oSheet.Range["P" + iRowIndex.ToString()];
                        CRange.Value = dr["CoachName"].ToString();

                        CRange = oSheet.Range["Q" + iRowIndex.ToString()];
                        CRange.Value = dr["CoachEmail"].ToString();

                        CRange = oSheet.Range["R" + iRowIndex.ToString()];
                        CRange.Value = dr["CoachPwd"].ToString();

                        CRange = oSheet.Range["S" + iRowIndex.ToString()];
                        CRange.Value = dr["CoachPhone"].ToString();

                        CRange = oSheet.Range["T" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"].ToString();

                        CRange = oSheet.Range["U" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductGroupCode"].ToString();

                        CRange = oSheet.Range["V" + iRowIndex.ToString()];
                        CRange.Value = dr["ProductCode"].ToString();

                        CRange = oSheet.Range["W" + iRowIndex.ToString()];
                        CRange.Value = dr["Day"].ToString();

                        CRange = oSheet.Range["X" + iRowIndex.ToString()];
                        CRange.Value = dr["Vroom"].ToString();

                        CRange = oSheet.Range["Y" + iRowIndex.ToString()];
                        CRange.Value = dr["Level"].ToString();

                        CRange = oSheet.Range["Z" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionNo"].ToString();

                        CRange = oSheet.Range["AA" + iRowIndex.ToString()];
                        CRange.Value = dr["State"].ToString();

                        CRange = oSheet.Range["BB" + iRowIndex.ToString()];
                        CRange.Value = dr["Semester"].ToString();

                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = fileName + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();

                }

            }
        }
        catch
        {
        }
    }

    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        if (validation() == 1)
        {
            exportExcel("2");
        }

    }
    protected void btnExportToExcelAll_Click(object sender, EventArgs e)
    {
        exportExcel("1");
    }

    public int validation()
    {
        int retVal = 1;
        if (ddYear.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select Year";
            retVal = -1;
        }
        else if (ddlChild.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select Student";
            retVal = -1;
        }
        return retVal;
    }
    protected void DDlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillStudents();
    }

    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            DDlSemester.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        DDlSemester.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
    protected void ddlChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (validation() == 1)
        {
            fillStudentRoster();

        }
    }
}