﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SendEmail_HR.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="SendEmail_HR" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="width: 1100px; margin-left: auto; margin-right: auto; min-height: 400px;">
        <div style="float: left">
            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
        </div>
        <div style="clear: both; margin-bottom: 1px;"></div>
        <div align="center">

            <h1 style="color: #46A71C;">Send Email to Volunteers</h1>
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <asp:Label ID="lblErrMsg" runat="server" Font-Bold="true"></asp:Label></center>
        <div style="clear: both; margin-bottom: 10px;"></div>

        <div style="width: 850px; margin-left: auto; margin-right: auto;">
            <div style="float: left;">
                <div style="text-align: center;"><span style="font-weight: bold; color: green;">Table 1: Roles</span></div>
                <div style="clear: both;"></div>
                <div>
                    <asp:CheckBox ID="chkSelectAll" runat="server" Font-Bold="true" Text="Select All" OnCheckedChanged="chkSelectAll_CheckedChanged" AutoPostBack="true" />
                    <asp:Button ID="BtnSendEmail" runat="server" Text="Send Email" OnClick="BtnSendEmail_Click" Style="float: right;" />
                </div>
                <div style="clear: both;"></div>
                <div>
                    <center>
                        <asp:Label ID="lblNoRecord" runat="server" Text="No record exists" ForeColor="Red" Visible="false"></asp:Label></center>
                </div>
                <div style="clear: both;"></div>
                <asp:GridView ID="GrdRoles" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="400px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc" RowStyle-CssClass="SmallFont">
                    <Columns>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>

                                <asp:CheckBox ID="chkRole" runat="server" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="RoleId" ItemStyle-HorizontalAlign="Center">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRoleId" Text='<%#DataBinder.Eval(Container.DataItem,"RoleId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Name">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRoleName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>

        </div>


        <div id="dvManipulateRoles" style="width: 300px; margin-left: 25px; margin-right: auto; float: left;">
            <div align="center">

                <h2 style="color: #000;">Add Role</h2>
            </div>
            <div style="width: 200px; float: left;">
                <div style="width: 50px; float: left;">
                    <span style="font-weight: bold;">Role: </span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlRole" Style="width: 140px;" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 100px; float: left;">
                <asp:Button runat="server" ID="BtnAddNew" Text="Add New" OnClick="BtnAddNew_Click" />
            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;" runat="server" id="dvAdditional">
                <div style="text-align: center;"><span style="font-weight: bold; color: green;">Table 2: Additional Role(s)</span></div>
                <div style="clear: both;"></div>
                <div>
                    <asp:CheckBox ID="chkAddAllRole" runat="server" Font-Bold="true" Text="Select All" OnCheckedChanged="chkAddAllRole_CheckedChanged" Checked="true" AutoPostBack="true" Visible="false" />

                </div>
                <div style="clear: both;"></div>
                <div>
                    <center>
                        <asp:Label ID="Label1" runat="server" Text="No record exists" ForeColor="Red" Visible="false"></asp:Label></center>
                </div>
                <div style="clear: both;"></div>
                <asp:GridView ID="GrdAdditionalRole" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="400px" Style="margin-left: auto; margin-right: auto;" HeaderStyle-BackColor="#ffffcc" RowStyle-CssClass="SmallFont">
                    <Columns>

                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Select" ItemStyle-HorizontalAlign="Center">
                            <ItemTemplate>

                                <asp:CheckBox ID="chkRole" runat="server" Checked="true" />
                            </ItemTemplate>
                        </asp:TemplateField>



                        <asp:TemplateField HeaderText="RoleId" ItemStyle-HorizontalAlign="Center">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRoleId" Text='<%#DataBinder.Eval(Container.DataItem,"RoleId") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Name">

                            <ItemTemplate>
                                <asp:Label runat="server" ID="lblRoleName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                            </ItemTemplate>

                        </asp:TemplateField>

                    </Columns>
                </asp:GridView>

            </div>
        </div>
    </div>
</asp:Content>
