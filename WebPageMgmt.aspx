<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="WebPageMgmt.aspx.vb" Inherits="WebPageMgmt" title="Web Page Management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<script language="javascript" type="text/javascript"> 
function fnOpen(fl)
{
    window.open('download.aspx?file=' + fl);
}
 </script>
    <%--<script language="javascript" type="text/javascript">    
    function CheckFile() {
        var fu = document.getElementById("<%=FileUpLoad1.ClientID%>");
        var btn = document.getElementById("<%=hBtn.ClientID%>");
        if(fu.value=='')
        {
            alert('Please select a file.');            
            fu.focus();
        }
        else
        {
            //fire request to server
            CallServer(fu.value, "filename");
            btn.disabled=true;
        }        
    }    
    
    function ReceiveServerData(rValue)
    {   
        var fu= document.getElementById("FileUpload1");
        var btnServer = document.getElementById("btnSave");
        var btn = document.getElementById("hBtn");
   
        //disable the button for further clicking
        btn.disabled=false;
        
        if(rValue=='true') // file exists
        {
            if(confirm('Are you sure you want to replace the file ?'))
            {      
                document.getElementById("hdnIsOverwrite").value="true";
                //save file , you can also use __doPostback()             
                btnServer.click();
            }            
        }
        else
        {
            document.getElementById("hdnIsOverwrite").value="false";
            //save file , you can also use __doPostback()             
            btnServer.click();
        }
    }
    function processError(rValue)
    {
        alert(rValue);
    }
    </script>--%>
<div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    <asp:LinkButton ID="LinkButton1" PostBackUrl="WebPageMgmtMain.aspx" runat="server">Back</asp:LinkButton>
</div>
<div runat ="server" id="divMulti" visible = "false" style ="width:800px; text-align : center ">
<table border="0px" width="100%" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align = "center" colspan="2" >Select Folder : 
    <asp:DropDownList ID="ddlselectFolder" AutoPostBack="true" OnSelectedIndexChanged=" ddlselectFolder_SelectedIndexChanged" DataValueField="FolderListID" DataTextField="DisplayName" runat="server">
    </asp:DropDownList> <br/>
    <asp:Label ID="lblVErr" runat="server" ></asp:Label>
    </td> </tr> </table></div>
<div  runat ="server" id="divAll" visible = "true" style ="width:800px; text-align : center ">
<table border="0px" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align="center" width="800px">
<div align="center">
<table border="0" cellpadding ="3" cellspacing ="0"> 
 <tr><td align="center" colspan="3"><h5>Download/Upload Web Pages</h5></td></tr>
<tr><td align="center">
<table runat = "server"  id="tblddls" border="0" cellpadding = "3" cellspacing = "0">
<tr><td align = "left" width="125px" > Level 1 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel1"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged" Width="150px" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 2 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel2"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 3 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel3"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 4 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel4"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 5 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel5"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel5_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 6 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel6"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel6_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 7 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel7"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel7_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 8 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel8"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel8_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 9 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel9"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel9_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 10 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel10"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel10_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 11 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel11"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel11_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 12 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel12"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel12_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>


<tr><td align = "center" colspan="2"><asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>&nbsp;</td></tr>


<tr><td align = "center" colspan="2" id="trfilelist1" runat = "server" > 
    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick ="btnContinue_Click"/>
</td></tr>
<tr id="trfilelist" runat = "server"><td align = "left" >List of Files </td><td align = "left" >
    <asp:DropDownList ID="ddlFiles" DataTextField = "FileName" DataValueField = "FileListId" Width="250px" runat="server">
    </asp:DropDownList>
</td></tr>
<tr id="trfileUpload"  visible="false" runat = "server"><td align = "center" colspan ="2" >
    <asp:Label ID="lblupload" runat="server" Text="select File :"></asp:Label> 
    <asp:FileUpLoad id="FileUpLoad1"   runat="server" /><br />
   * The file name should not contain spaces. Space will be eliminated if exist.
    <br />
    <asp:Button ID="btnFileUpload" runat="server" Text="Upload File" OnClick ="btnFileUpload_Click" />
     <asp:Button ID="btnConfirmUpload" runat="server" Visible="false"  
        Text="Confirm Replace" OnClick ="btnConfirmUpload_Click" BackColor="Black" 
        BorderColor="Red" BorderStyle="Solid" BorderWidth="2px" Font-Bold="True" 
        ForeColor="White" />
    &nbsp;&nbsp; 
    <asp:Button ID="btnFileUploadCancel" runat="server" Text="Cancel" OnClick="btnFileUploadCancel_Click" />
</td></tr>
<tr id="trRename"  visible="false" runat = "server"><td align = "center" colspan ="2" >New File Name : 
    <asp:TextBox ID="txtRenameFile" runat="server" Width="250px"></asp:TextBox>
   <br />
   * Please enter name alone without extension.
   <br />
    <asp:Button ID="btnRenameFile" runat="server" Text="Rename File" OnClick ="btnRenameFile_Click" />
    &nbsp;&nbsp; 
    <asp:Button ID="btnRenameCancel" runat="server" Text="Cancel" OnClick="btnRenameCancel_Click" />
</td></tr>
<tr id="trDownload"  visible="false" runat = "server"><td align = "center" colspan ="2" >
    <asp:ListBox ID="lstFiles" SelectionMode="Multiple" DataTextField="FileName" DataValueField ="FileListID" runat="server"></asp:ListBox>
    <br />
    <asp:Label ID="lblDownloaderr" runat="server" ForeColor = "Red" ></asp:Label>
    <br />
    <asp:Button ID="btnDownloadfiles" OnClick="btnDownloadfiles_Click" runat="server" Text="Download Files" />
    &nbsp;&nbsp; 
    <asp:Button ID="btnDownloadCancel" runat="server" Text="Cancel" />
    
</td></tr> 
</table> 
</td></tr>
<tr><td align="center"> 
    <asp:Label ID="lblURL" runat="server" ></asp:Label></td></tr>
    <tr id="trbuttons"  runat ="server"><td align = "center"  > 
    <asp:Button ID="btnDownload" runat="server" Text="Download" Enabled = "false" OnClick ="btnDownload_Click"/>&nbsp;&nbsp;
    &nbsp;&nbsp;
    <asp:Button ID="BtnUpload" runat="server" Text="Upload" Enabled = "false" OnClick ="BtnUpload_Click" />
    &nbsp;&nbsp;
    <asp:Button ID="BtnRename" runat="server" Text="Rename" Enabled = "false" OnClick ="BtnRename_Click" />
    &nbsp;&nbsp;
    <asp:Button ID="BtnDelete" runat="server" Text="Delete" Enabled = "false" OnClick ="Btndelete_Click" />
    &nbsp;&nbsp;
    <asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick ="BtnClear_Click" />
<%--     <asp:Button ID="hBtn" runat="server" Text="test" OnClientClick ="CheckFile()"  />--%>
 
</td></tr>


<tr id="Tr1" runat="server" visible = "false"><td align = "center"  >
    <asp:Label ID="lblLevel" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblParentFID" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblPath" runat="server" visible = "false"></asp:Label>
   
        
<%--<asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
<asp:HiddenField ID="hdnIsOverwrite" runat="server" />--%>
  
    </td></tr>
</table>
  <asp:Label ID="lbltempfilename"  runat="server" ></asp:Label>
    <asp:Label ID="lblfiletemp" runat="server" ></asp:Label>
</div>
</td> </tr> </table> </div> 
<center>
    <asp:Label ID="lblNoAccess" runat="server"></asp:Label></center>
</asp:Content>



