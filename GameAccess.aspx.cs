﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Net;
using System.Data.SqlClient;
using System.Drawing;



public partial class GameAccess : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
          
            hdn_userID.Value = Session["CustIndID"].ToString();
            PopulateData();
        }
        
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
       
    }
    protected void gvChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblResult.Text = "";
        GridViewRow row = gvChild.SelectedRow;
        txtEmailID.Text = row.Cells[6].Text;
        txtPassword.Text = row.Cells[7].Text;
        hdn_childID.Value = row.Cells[0].Text.Trim();
        txtEmailID.Enabled = true;
        txtPassword.Enabled =true;
        if (row.Cells[6].Text == string.Empty  || row.Cells[7].Text == string.Empty )
        {
            btnUpdate.Enabled = false;
            btnAdd.Enabled = true;
        }
        else
        {
            btnAdd.Enabled = false;
            btnUpdate.Enabled = true;
        }           
    }   
    protected void PopulateData()
    {
        lblStatus.Text = "";
        DataSet ds;
        try
        {
            string SqlString = "With TBL AS ( SELECT   CH.ChildNumber , CH.FIRST_NAME +' ' + CH.LAST_NAME AS ChildName,CH.GRADE AS Grade,GA.ChildLoginID AS LoginID, GA.ChildPWD as 'Password' FROM Child CH INNER JOIN GAME GA ON GA.MemberID = CH.MEMBERID AND GA.Approved='Y' and GA.ChildNumber =CH.ChildNumber WHERE CH.MEMBERID =" + Session["CustIndID"].ToString() + " AND GetDate() <= DATEADD(day, 1,GA.EndDate)) SELECT DISTINCT  TBL.ChildNumber,TBL.ChildName ,TBL.Grade ,MAX(TBL.LoginID ) as LoginID ,MAX(TBL.Password ) as 'Password'  FROM TBL GROUP BY TBL.ChildNumber ,TBL.ChildName ,TBL.Grade  order by ChildName asc";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlString);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvChild.DataSource = ds;
                gvChild.DataBind();
                Panel2.Visible = true;
            }
            else
            {
                lblStatus.Text = " No Records found.";
            }
        }
        catch (SqlException ex)
        {
            lblResult.ForeColor = Color.Red;
            lblResult.Text = ex.Message.ToString();
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            DataUpdate();
        }
    }
    protected void DataUpdate()
    {
            lblResult.Text  = "";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, " UPDATE GAME SET ChildLoginID='" + txtEmailID.Text.Trim() + "', ChildPWD ='" + txtPassword.Text.Trim() + "' WHERE MemberID=" + Session["CustIndID"].ToString() + " AND ChildNumber= " + hdn_childID.Value + " AND GetDate() <= DATEADD(day, 1,EndDate)");
                lblResult.ForeColor = Color.Green ;
                lblResult.Text = "Record Updated successfully.";
                PopulateData();
            }
            catch (SqlException ex)
            {
                lblResult.ForeColor = Color.Red;
                lblResult.Text = ex.Message.ToString();
            }  
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            lblResult.Text = "";
            DataUpdate();
        }       
    }
    protected void gvChild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        for (int i = 0; i < e.Row.Cells.Count; i++)
        {
            e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
            if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
        }
    }
}
