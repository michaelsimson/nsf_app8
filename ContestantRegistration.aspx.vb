'Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL

Partial Class ContestantRegistration
    Inherits System.Web.UI.Page

    Public nRegFee As Decimal = 0
    Dim conn As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            Session("LATEFEE") = 0   'this is a variable that is going to be used in the future
            nRegFee = 0
            conn = New SqlConnection(Application("ConnectionString"))
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Not Page.IsPostBack Then
                If Session("EventYear") Is Nothing Then
                    Session("EventYear") = Now.Year
                End If
                LoadChildren()
                LoadAvailableContests()
                LoadPendingContests()
                DisplayPaidContests()
            End If
            tdWorkShop.InnerHtml = " <span class=subHeading>Eligible Contests for " & " <font style='color:Maroon'>" & ddlChildren.SelectedItem.Text & "</font> </span>"
            lblWarning.Visible = False
            lblWarning2.Visible = False
        Catch ex As Exception
            lblNoContestMsg.ForeColor = Drawing.Color.Red
            lblNoContestMsg.Text = "Please update Child Information[Grade] and Register"
        End Try
    End Sub

    Private Sub DisplayPaidContests()
        Select Case Session("EventID")
            Case 1
                DisplayNationalPaidContests()
            Case 2
                DisplayRegionalPaidContests()
            Case Else
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End Select
    End Sub

    Private Sub DisplayNationalPaidContests()
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}
        Dim strSQL As String

        strSQL = ""
        If conn.State = ConnectionState.Closed Then conn.Open()

        strSQL = " SELECT b.ContestDate, b.StartTime as ContestTime,  case when cast(WC.PriorityDeadline as date) < cast(getdate() as date) then case when cast(WC.WaitListDeadline as date)<cast(getdate() as date) then CAST(b.RegistrationDeadLine as date) ELSE  cast(WC.WaitListDeadline as date) END else cast(WC.PriorityDeadline as date) end as RegistrationDeadline, b.CheckInTime, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, b.StartTime, b.EndTime, " & _
                 " c.NationalFinalsFee as Fee, a.ContestCode as ContestID, " & _
                 " c.ContestCategoryID, c.ContestCode as ContestAbbr, " & _
                 " c.ContestDesc, a.ContestYear, a.ParentID, a.ChildNumber, " & _
                 " REPLACE(e.FIRST_NAME + ' ' + COALESCE (e.MIDDLE_INITIAL, '') + ' ' + e.LAST_NAME, ' ', ' ') AS ContestantName, " & _
                 " a.contestant_id, a.NationalInvitee, b.NSFChapterID,'' as State,'' as City, a.PaymentDate, a.PaymentReference, " & _
                 " '' as VolunteerName, '' as VolunteerPhone, a.photo_image " & _
                " FROM Contestant a INNER JOIN Contest b " & _
                " On b.ContestID = a.Contestcode " & _
                " Left Join WeekCalendar WC on WC.EventId = b.EventId and (cast(WC.satday1 as date)=cast(b.contestdate as date) or cast(WC.SunDay2 as date)=cast(b.contestdate as date)) " & _
                " INNER JOIN ContestCategory c " & _
                " ON b.ContestCategoryID = c.ContestCategoryID " & _
                " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber " & _
                " AND a.ParentID = e.MEMBERID WHERE (a.ParentID =  " & Session("CustIndID") & " ) " & _
                " and a.ContestYear =    " & Now.Year & "   and b.EventID = 1 " & _
                " ORDER BY ContestId  "

        SqlHelper.FillDataset(conn, CommandType.Text, strSQL, dsContestant, tblConestant)

        Dim strWhere As String
        Dim dvPaidContests As DataView
        dvPaidContests = dsContestant.Tables(0).DefaultView
        strWhere = " PaymentDate IS NOT Null "
        dvPaidContests.RowFilter = strWhere
        ' Validate if the child has 8th grade
        ViewState("PaidProductGroup") = ""

        If dvPaidContests.Count > 0 Then
            pnlPaidContests.Visible = True
            dgPaidContests.DataSource = dvPaidContests
            dgPaidContests.DataBind()
            PnlPending.Visible = True
            btnPayNow.Enabled = True

            ' Validate if the child has 8th grade
            Dim inx As Integer, strProductGrpCode As String = ""
            For inx = 0 To dsContestant.Tables(0).Rows.Count - 1
                strProductGrpCode = "," & dsContestant.Tables(0).Rows(inx).Item("ProductGroupCode")
            Next
            If strProductGrpCode.Length > 0 Then strProductGrpCode = strProductGrpCode.Substring(1)
            ViewState("PaidProductGroup") = strProductGrpCode

        Else
            pnlPaidContests.Visible = False
        End If

    End Sub

    Private Sub DisplayRegionalPaidContests()
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(3) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Session("EventYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@LateFeeDate"
        prmArray(2).Value = IIf(Session("EventID").ToString = "1", Application("LateRegistrationDate"), DBNull.Value)
        prmArray(2).Direction = ParameterDirection.Input

        If conn.State = ConnectionState.Closed Then conn.Open()
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)

        Dim strWhere As String
        Dim dvPaidContests As DataView
        dvPaidContests = dsContestant.Tables(0).DefaultView
        strWhere = " PaymentDate IS NOT NULL "
        dvPaidContests.RowFilter = strWhere
        If dvPaidContests.Count > 0 Then
            dvPaidContests = dsContestant.Tables(0).DefaultView
            dvPaidContests.RowFilter = strWhere & " AND TeamLead = 'Y'"

            pnlPaidContests.Visible = True
            If dvPaidContests.Count <= 0 Then
                dvPaidContests = dsContestant.Tables(0).DefaultView
                dvPaidContests.RowFilter = strWhere
            End If
            dgPaidContests.DataSource = dvPaidContests
            dgPaidContests.DataBind()
        Else
            pnlPaidContests.Visible = False
        End If

    End Sub

    Protected Sub dgPaidContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaidContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim pdt As Object = DataBinder.Eval(e.Item.DataItem, "PaymentDate")
                If Session("EventID").ToString = "1" Then
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = Application("NationalFinalsCity")
                Else
                    Dim VolunteerName As String, VolunteerPhone As String, City As String, State As String
                    VolunteerName = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerName"), String)
                    VolunteerPhone = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerPhone"), String)
                    City = CType(DataBinder.Eval(e.Item.DataItem, "City"), String)
                    State = CType(DataBinder.Eval(e.Item.DataItem, "State"), String)

                    Dim sb As StringBuilder = New StringBuilder
                    sb.Append(VolunteerName)
                    sb.Append("<br />")
                    sb.Append(VolunteerPhone)
                    sb.Append("<br />")
                    sb.Append(City)
                    sb.Append(", ")
                    sb.Append(State)
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = sb.ToString
                End If
        End Select
    End Sub

    Private Sub LoadAvailableContests()
        Try
            Dim ChildID As Integer = ddlChildren.SelectedValue
            If Session("EventID") = 1 Then
                DisplayNationalEligibility(ChildID)
            Else
                DisplayRegionalEligibility(ChildID)
            End If
        Catch ex As Exception
            lblNoContestMsg.ForeColor = Drawing.Color.Red
            lblNoContestMsg.Text = "Please update Child Information[Grade] and Register"
        End Try

    End Sub

    Private Sub DisplayNationalEligibility(ByVal ChildID As Integer)
        Dim strSQL As String
        Dim dsEligibleContests As DataSet = New DataSet
        Dim tblEligibleContests() As String = {"EligibleContests"}
        If conn.State = ConnectionState.Closed Then conn.Open()
        strSQL = " SELECT Coalesce(Convert(varchar(20),b.ContestDate,107),'To be Announced') as ContestDate,  ContestTime = case b.ContestDate when null then '' else  case WHEN b.checkintime = 'TBD' THEN '' ELSE b.checkintime  END End, Case when Ex.NewDeadline is Null then " & _
                 " case when  WC.PriorityDeadline is null or cast(WC.PriorityDeadline as date) < cast(getdate() as date) then  case when  WC.WaitListDeadline is null or cast(WC.WaitListDeadline as date)<cast(getdate() as date) then CAST(B.RegistrationDeadLine as date) ELSE  cast(WC.WaitListDeadline as date) END else cast(WC.PriorityDeadline as date) end  Else cast(Ex.NewDeadline as date) end as RegistrationDeadline," & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode,SelectedContestCode = COALESCE(Cnst.contestant_id,''), c.NationalFinalsFee as Fee, b.ContestID, c.ContestCategoryID, " & _
                 " c.ContestCode, c.ContestDesc, a.ContestYear, a.ParentID, a.ChildNumber, REPLACE(e.FIRST_NAME + ' ' + COALESCE (e.MIDDLE_INITIAL, '') + ' ' + e.LAST_NAME, ' ', ' ') AS ContestantName,e.First_Name, e.Last_Name, " & _
                 "  Cnst.contestant_id as ContestantID, a.NationalInvitee,  b.NSFChapterID, b.NSFChapterID as ChapterID,'' as State,'Burr Ridge' as City,(select distinct(chaptercode) from chapter chpt where chpt.chapterid=b.nsfChapterID) as ChapterCode, " & _
                 "  Case When Cnst.paymentreference is not Null then 'Paid' When Cnst.ChildNumber<>''   then 'Pending' else 'Select' End 'Status' " & _
                 " FROM Contestant a INNER JOIN ContestCategory c " & _
                 " ON a.ContestCategoryID = c.ContestCategoryID " & _
                 " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber AND a.ParentID = e.MEMBERID " & _
                 " INNER JOIN Contest B on B.ContestCategoryID =  c.ContestCategoryID " & _
                 " LEFT JOIN ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " Left Join Contestant Cnst ON A.ParentID = Cnst.ParentID and A.ChildNumber = cnst.ChildNumber and Cnst.EventId = 1 and a.ProductCode = cnst.ProductCode and a.ContestYear = Cnst.ContestYear and a.ContestCategoryID = cnst.ContestCategoryID " & _
                 " Left Join WeekCalendar WC on WC.EventId = b.EventId and (cast(WC.satday1 as date)=cast(b.contestdate as date) or cast(WC.SunDay2 as date)=cast(b.contestdate as date)) " & _
                 " WHERE (a.ParentID = " & Session("CustIndID") & " )" & _
                 " and a.ContestYear = " & Now.Year() & " and a.NationalInvitee = 1 and  " & _
                 " a.ChildNumber = " & ChildID & " And b.NSFChapterID = 1 " & _
                 " UNION Select Coalesce(Convert(varchar(20),b.ContestDate,107),'To be Announced') as ContestDate, ContestTime = case b.ContestDate when null then '' else  case WHEN b.checkintime = 'TBD' THEN '' ELSE b.checkintime  END End, " & _
                 " Case when Ex.NewDeadline is Null then case when   WC.PriorityDeadline is null or  cast(WC.PriorityDeadline as date) < cast(getdate() as date) then case when   WC.WaitListDeadline is null or cast(WC.WaitListDeadline as date)<cast(getdate() as date) then CAST(b.RegistrationDeadLine as date) ELSE  cast(WC.WaitListDeadline as date) END else cast(WC.PriorityDeadline as date) end  Else cast(Ex.NewDeadline as date) end as RegistrationDeadline," & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode,'' as SelectedContestCode, a.NationalFinalsFee as Fee, b.ContestID, a.ContestCategoryID, " & _
                 " a.ContestCode, a.ContestDesc, a.ContestYear," & _
                 "  " & Session("CustIndID") & " as ParentID, " & ChildID & " as ChildNumber, " & _
                 " '' as ContestantName,'' as First_Name, '' as Last_Name, cnst.contestant_id as ContestantID, '1' as NationalInvitee,  b.NSFChapterID, b.NSFChapterID as ChapterID,'' as State,'Burr Ridge' as City,(select distinct(chaptercode) from chapter chpt where chpt.chapterid=b.nsfChapterID) as ChapterCode ,Case When Cnst.paymentreference is not Null then 'Paid' When Cnst.ChildNumber<>''   then 'Pending' else 'Select' End 'Status'" & _
                 " from Contest b   Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " Left Join WeekCalendar WC on WC.EventId = b.EventId and (cast(WC.satday1 as date) =cast(b.contestdate as date) or cast(WC.SunDay2 as date) =cast(b.contestdate as date)) " & _
                 " INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID " & _
                 " and b.Contest_Year =  " & Now.Year() & _
                 " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' and  a.GradeBased=1 " & _
                 " and (Select Grade from Child where childnumber = " & ChildID & " ) " & _
                 " between GradeFrom and GradeTo left join Contestant cnst ON cnst.ParentID = " & Session("CustIndID") & " AND cnst.ChildNumber = " & ChildID & " AND cnst.EventId = 1 AND cnst.ContestID = b.ContestID AND cnst.ContestYear = b.Contest_Year " & _
                 " UNION Select Coalesce(Convert(varchar(20),b.ContestDate,107),'To be Announced') as ContestDate, ContestTime = case b.ContestDate when null then '' else  case WHEN b.checkintime = 'TBD' THEN '' ELSE b.checkintime  END End, " & _
                "  Case when Ex.NewDeadline is Null then case when WC.PriorityDeadline is null or cast(WC.PriorityDeadline as date) < cast(getdate() as date) then case when WC.WaitListDeadline is null or cast(WC.WaitListDeadline as date)<cast(getdate() as date) then CAST(b.RegistrationDeadLine as date) ELSE  cast(WC.WaitListDeadline as date) END else cast(WC.PriorityDeadline as date) end Else cast(Ex.NewDeadline as date) end as RegistrationDeadline, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode,'' as SelectedContestCode, a.NationalFinalsFee as Fee, b.ContestID, a.ContestCategoryID,  " & _
                 " a.ContestCode, a.ContestDesc, a.ContestYear, " & _
                 " " & Session("CustIndID") & " as ParentID, " & ChildID & " as ChildNumber, " & _
                 " '' as ContestantName,'' as First_Name, '' as Last_Name, Cnst.contestant_id as Contestant_ID, '1' as NationalInvitee,  b.NSFChapterID,  b.NSFChapterID as ChapterID,'' as State,'Burr Ridge' as City,(select distinct(chaptercode) from chapter chpt where chpt.chapterid=b.nsfChapterID) as ChapterCode,Case When Cnst.paymentreference is not Null then 'Paid' When Cnst.ChildNumber<>''   then 'Pending' else 'Select' End 'Status'" & _
                 " from Contest b   Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " Left Join WeekCalendar WC on WC.EventId = b.EventId and (cast(WC.satday1 as date)=cast(b.contestdate as date) or cast(WC.SunDay2 as date)=cast(b.contestdate as date)) " & _
                 " INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID " & _
                 " and b.Contest_Year =  " & Now.Year() & _
                 " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' and a.GradeBased=0  " & _
                 " and (select DATEDIFF(YY,Date_OF_Birth,GEtdate()) from Child where childnumber = " & ChildID & " ) " & _
                 " between AgeFrom and AgeTo left join Contestant cnst ON cnst.ParentID = " & Session("CustIndID") & " AND cnst.ChildNumber = " & ChildID & " AND cnst.EventId = 1 AND cnst.ContestID = b.ContestID AND cnst.ContestYear = b.Contest_Year " & _
                 " ORDER BY b.ContestId"
        ''Testing **
        SqlHelper.FillDataset(conn, CommandType.Text, strSQL, dsEligibleContests, tblEligibleContests)

        Dim strChildName As String
        strChildName = ""
        dgSelectionList.Visible = False
        btnSubmitSelections.Visible = False
        If Not dsEligibleContests Is Nothing Then
            If dsEligibleContests.Tables.Count > 0 Then
                dgSelectionList.DataSource = dsEligibleContests.Tables(0).DefaultView
                dgSelectionList.DataBind()
                If dsEligibleContests.Tables(0).Rows.Count > 0 Then
                    strChildName = dsEligibleContests.Tables(0).Rows(0).Item("ContestantName")
                    dgSelectionList.Visible = True
                    btnSubmitSelections.Visible = True
                End If
            End If
        End If
        If dgSelectionList.Visible = False Then
            lblSelectedChild.Text = ddlChildren.SelectedItem.Text & " is not eligible for any contests."
        Else
            lblSelectedChild.Text = "Eligible Contests for " & ddlChildren.SelectedItem.Text & "."
            If dgSelectionList.Items.Count > 0 Then
                dgSelectionList.Visible = True
                btnSubmitSelections.Visible = True
            Else
                dgSelectionList.Visible = False
                btnSubmitSelections.Visible = False
                lblSelectedChild.Text = lblSelectedChild.Text & " No more contests to select. "
            End If
        End If
    End Sub

    Private Sub DisplayRegionalEligibility(ByVal ChildID As Integer)
        Dim dsEligibleContests As DataSet = New DataSet
        Dim tblEligibleContests() As String = {"EligibleContests"}
        Dim prmArray(4) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ChildNumber"
        prmArray(0).Value = ChildID
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Now.Year
        prmArray(1).Direction = ParameterDirection.Input
        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@ContestTypeID"
        prmArray(2).Value = ConfigurationManager.AppSettings("EventID")
        prmArray(2).Direction = ParameterDirection.Input

        prmArray(3) = New SqlParameter
        prmArray(3).ParameterName = "@IndID"
        prmArray(3).Value = Session("CustIndID")
        prmArray(3).Direction = ParameterDirection.Input

        If conn.State = ConnectionState.Closed Then conn.Open()
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetEligibleSelContests", dsEligibleContests, tblEligibleContests, prmArray)

        Dim strChildName As String
        strChildName = ""
        If Not dsEligibleContests Is Nothing Then
            If dsEligibleContests.Tables.Count > 0 Then
                dgSelectionList.SelectedIndex = -1
                Dim dvTemp As DataView = dsEligibleContests.Tables(0).DefaultView
                'dvTemp.RowFilter = "status <>'Paid' or status <>'Pending'"
                dvTemp.RowFilter = "status like '%Select%'"
                dgSelectionList.DataSource = dvTemp
                dgSelectionList.DataBind()
                If dsEligibleContests.Tables(0).Rows.Count > 0 Then
                    'get child name for display
                    strChildName = dsEligibleContests.Tables(0).Rows(0).Item("First_Name") & " " & dsEligibleContests.Tables(0).Rows(0).Item("Last_Name")
                Else
                End If
                'RegistrationDeadline
            End If
        End If

        If strChildName = "" Then
            lblSelectedChild.Text = ddlChildren.SelectedItem.Text & " is not eligible for any contests."
            dgSelectionList.Visible = False
            btnSubmitSelections.Visible = False
        Else
            lblSelectedChild.Text = "Eligible Contests for " & strChildName & "."
            If dgSelectionList.Items.Count > 0 Then
                dgSelectionList.Visible = True
                btnSubmitSelections.Visible = True
                enableDisableCheckBox()
                DisableGroups()
            Else
                dgSelectionList.Visible = False
                btnSubmitSelections.Visible = False
                lblSelectedChild.Text = lblSelectedChild.Text & " No more contests to select. "
            End If
        End If
    End Sub

    Private Sub LoadPendingContests()
        Dim dsPendingContests As DataSet
        If conn.State = ConnectionState.Closed Then conn.Open()
        Dim strSQL As String = "Select	CHLD.ChildNumber, CHLD.First_Name+' '+CHLD.Last_Name as ContestantName, "
        strSQL = strSQL & " CHPT.ChapterID,CHPT.ChapterCode,CHPT.City,CHPT.State,CTST.ContestID, CTST.ContestCode, "
        strSQL = strSQL & " Coalesce(Convert(varchar(20),CTST.ContestDate,107),'To be Announced') as ContestDate,"
        strSQL = strSQL & " case CTST.ContestDate when null then '' else case WHEN CTST.checkintime ='TBD' THEN '' ELSE CTST.checkintime END End AS ContestTime  ,"
        strSQL = strSQL & " CTST.ContestCategoryID as ContestCategoryID, "
        strSQL = strSQL & " CTST.NSFChapterID,  CTST.Contest_Year,  CTST.ContestTypeID, " 'Case when Ex.NewDeadline is Null then CTST.RegistrationDeadline Else Ex.NewDeadline end as RegistrationDeadline,
        strSQL = strSQL & "  Case when Ex.NewDeadline is Null then "
        strSQL = strSQL & "  case when WC.PriorityDeadline is null or cast(WC.PriorityDeadline as date) < cast(getdate() as date) then case when WC.WaitListDeadline is null or cast(WC.WaitListDeadline as date)<cast(getdate() as date) then CAST(CTST.RegistrationDeadLine as date) ELSE  cast(WC.WaitListDeadline as date) END else cast(WC.PriorityDeadline as date) end Else cast(Ex.NewDeadline as date) end as RegistrationDeadline,"
        strSQL = strSQL & " CTST.EventID, CTST.EventCode, CTST.ProductID, "
        strSQL = strSQL & " CTST.ProductCode, CTST.ProductGroupID, CTST.ProductGroupCode,  SelectedConltestCode = COALESCE(CONT.contestant_id,''),"
        strSQL = strSQL & " 'Pending' as Status , CTGY.ContestDesc, " & IIf(Session("EventID") = 2, "CTGY.RegionalFee as Fee", "CTGY.NationalFinalsFee as Fee") & ", Cont.Contestant_ID as ContestantID"
        strSQL = strSQL & " from Contestant CONT INNER JOIN  Child CHLD ON CONT.ChildNumber=CHLD.ChildNumber"
        strSQL = strSQL & " INNER JOIN Contest CTST ON CTST.Contest_Year = CONT.ContestYear "
        strSQL = strSQL & " LEFT JOIN ExContestant Ex On Ex.ChapterID=CTST.NSFChapterID and Ex.ProductID=CTST.ProductId and Ex.ProductGroupID=CTST.ProductGroupId and Ex.ContestYear=CTST.Contest_Year and Ex.EventID=CTST.EventId and Ex.ChildNumber=CONT.ChildNumber"
        strSQL = strSQL & " Left Join WeekCalendar WC on WC.EventId = CTST.EventId and (cast(WC.satday1 as date)=cast(CTST.contestdate as date) or cast(WC.SunDay2 as date)=cast(CTST.contestdate as date)) "
        strSQL = strSQL & " INNER JOIN ContestCategory CTGY ON CTGY.ContestCategoryID = CTST.ContestCategoryID"
        strSQL = strSQL & " INNER JOIN Chapter CHPT ON CHPT.ChapterID = CTST.NSFChapterID AND CONT.ContestID = CTST.ContestID"
        strSQL = strSQL & " AND CONT.ParentID = CHLD.MEMBERID  where CONT.PaymentReference is Null AND CONT.ContestYear >= " & Session("EventYear") & " AND CONT.ParentID = " & Session("CustIndID") & "  AND CONT.EventId=" & Session("EventID") & " Order By CTGY.ContestCategoryID"
        Try
            dsPendingContests = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL)
        Catch ex As Exception

        End Try
        If Not dsPendingContests Is Nothing Then
            If dsPendingContests.Tables.Count > 0 Then
                DGPending.SelectedIndex = -1
                DGPending.DataSource = dsPendingContests.Tables(0)
                DGPending.DataBind()
                'RegistrationDeadline
            End If
        End If

        If DGPending.Items.Count > 0 Then
            DGPending.Visible = True
            PnlPending.Visible = True
            btnPayNow.Enabled = True
            calcPendingAmt()
        Else
            PnlPending.Visible = False
            DGPending.Visible = False
            btnPayNow.Enabled = False
        End If
    End Sub

    Protected Sub DGPending_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim ContestantID As Label
            ContestantID = CType(e.Item.FindControl("lblContestantID"), Label)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM Contestant WHERE Contestant_ID =" + ContestantID.Text)
            Catch se As SqlException
                Return
            End Try
            LoadAvailableContests()
            LoadPendingContests()
        End If
    End Sub

    Protected Sub DGPending_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.Cells(0).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
        End If
    End Sub

    Private Sub calcPendingAmt()
        Dim dgItem As DataGridItem
        Dim Lbl2 As Label
        Dim totalAmt As Double
        totalAmt = 0.0
        For Each dgItem In DGPending.Items
            Lbl2 = dgItem.FindControl("lblFee")
            totalAmt = totalAmt + CInt(Lbl2.Text)
        Next
        lblPendAmt.Text = totalAmt.ToString().Trim()
    End Sub

    Protected Sub dgSelectionList_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As CheckBox, lblDupl As Label
            btn = CType(e.Item.Cells(1).FindControl("chkEvent"), CheckBox)
            lblDupl = CType(e.Item.Cells(1).FindControl("lblDuplicate"), Label)
            Dim str As String
            If Not CType(DataBinder.Eval(e.Item.DataItem, "Status"), String) = "Pending" Then
                str = "SELECT COUNT(*) FROM Contestant  Inner Join Contest on   Contest.ContestID = Contestant.ContestID "
                str = str & " WHERE  Contestant.ContestYear = " & Session("EventYear") & " And Contestant.ChildNumber = " & ddlChildren.SelectedValue & " And Contestant.ParentID =  " & Session("CustIndID")
                str = str & " and  Contest.ProductID =" & CType(DataBinder.Eval(e.Item.DataItem, "ProductID"), Integer)
                Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str)
                If count > 0 Then
                    str = "SELECT COUNT(*) FROM Contestant  Inner Join Contest on   Contest.ContestID = Contestant.ContestID and DateDiff(d,Contest.ContestDate,GETDATE ())>0"
                    str = str & " WHERE Contestant.PaymentReference is Not Null and Contestant.ContestYear = " & Session("EventYear") & " And Contestant.ChildNumber = " & ddlChildren.SelectedValue & " And Contestant.ChapterID <> " & CType(DataBinder.Eval(e.Item.DataItem, "ChapterID"), Integer) & " And Contestant.ParentID =  " & Session("CustIndID")
                    str = str & " and  Contest.ProductID =" & CType(DataBinder.Eval(e.Item.DataItem, "ProductID"), Integer) & " and Contestant.PaymentDate is Not Null and ((Contestant.score1 is  Null and Contestant.score2 is Null and Contestant.score3  is  Null) OR (Contestant.score1+Contestant.score2+ISNULL(Contestant.score3,0))=0)"
                    Dim cntDupl As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str)
                    If cntDupl > 0 Then
                        lblDupl.Text = "Duplicate"
                        btn.Attributes.Add("onclick", "return confirm('This is a Duplicate Registration, Do you want to register for second chapter?');")
                    Else
                        btn.Visible = False
                    End If
                Else
                    lblDupl.Text = "Select"
                End If
            ElseIf Session("EventID") = 1 And CType(DataBinder.Eval(e.Item.DataItem, "Status"), String) = "Pending" Then
                btn.Visible = False
            End If
        End If
    End Sub

    Private Sub enableDisableCheckBox()
        Dim dgItem As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim Lbl3 As Label
        Dim totalAmt As Double
        totalAmt = 0.0
        tdSubmit.Style.Add("display", "none")
        tdWant.Style.Add("display", "none")
        Session("ContestsSelected") = Nothing
        For Each dgItem In dgSelectionList.Items

            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")
            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblFee")
                totalAmt = totalAmt + CInt(Lbl2.Text)
                Lbl3 = dgItem.FindControl("lblProductCode")
                '************************************************************************************************************************************
                '** This Session("ContestsSelected") is not used now, We are using Session("ContestsSelected") from contestSelectionSum.aspx.vb page **
                '************************************************************************************************************************************
                Session("ContestsSelected") = Session("ContestsSelected") & Lbl3.Text & "(" & ddlChildren.SelectedValue & ")(" & Session("CustIndChapterID") & ")(" & Lbl2.Text & ")"
                Lbl1.Style.Add("color", "Red")
                selectRadioButton.Text = "Remove"
                selectRadioButton.Attributes.Add("style", "color:Red")
                tdSubmit.Style.Add("display", "block")
                tdWant.Style.Add("display", "block")
            End If
            lblTotalFee.Text = totalAmt.ToString()
        Next
    End Sub

    Private Sub LoadChildren()
        Try
            Dim objChild As New Child
            Dim dsChild As New DataSet
            Dim strWhere As String
            conn = New SqlConnection()
            conn.ConnectionString = Application("ConnectionString")
            strWhere = " Grade < 13 AND (MemberId='" & Session("CustIndID") & "' or SpouseID='" & Session("CustIndID") & "')"
            objChild.SearchChildWhere(Application("ConnectionString"), dsChild, strWhere)

            If dsChild.Tables.Count > 0 Then
                If dsChild.Tables(0).Rows.Count > 0 Then
                    Dim dvChildren As New DataView
                    dvChildren = dsChild.Tables(0).DefaultView
                    Dim dtMdate As Date = FormatDateTime(("8/1/" & (Now.Year - 1)), DateFormat.ShortDate) ' FormatDateTime(("10/1/" & (Now.Year - 1)), DateFormat.ShortDate)
                    Dim strFilter As String = "((ModifyDate >= '" & dtMdate & "') or (CreateDate >= '" & dtMdate & "' and  ModifyDate is Null ))"
                    dvChildren.RowFilter = strFilter

                    ddlChildren.DataSource = dvChildren
                    ddlChildren.DataBind()
                    Session("ChildCount") = dsChild.Tables(0).Rows.Count
                    ddlChildren.SelectedIndex = 0
                    Session("SelectedChildID") = ddlChildren.SelectedValue
                End If
            End If
            If conn.State <> ConnectionState.Closed Then conn.Close()
        Catch ex As Exception
            lblNoContestMsg.ForeColor = Drawing.Color.Red
            lblNoContestMsg.Text = "Please update Child Information[Grade] and Register"
        End Try
    End Sub

    Protected Sub chkEvent_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblWarning2.Text = String.Empty
        Dim objCheckBox As CheckBox
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblFee As Label
        Dim lbl3 As Label
        Dim strSelected As String
        Dim totalFee As Double
        totalFee = 0.0

        objCheckBox = sender
        strSelected = ""
        If objCheckBox.Checked = True Then
            For Each dgItem In dgSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If

                    Lbl1 = dgItem.FindControl("lblProductCode")
                    For Each dgItem1 In dgSelectionList.Items
                        lbl3 = dgItem.FindControl("lblStatus")
                        If lbl3.Text <> "Pending" Then
                            If dgSelectionList.DataKeys(dgItem.ItemIndex) <> dgSelectionList.DataKeys(dgItem1.ItemIndex) Then
                                Lbl2 = dgItem1.FindControl("lblProductCode")
                                If Lbl2.Text.Equals(Lbl1.Text) Then
                                    selectRadioButton1 = dgItem1.FindControl("chkEvent")
                                    selectRadioButton1.Checked = False
                                    selectRadioButton1.Enabled = False
                                End If
                            End If
                        End If
                    Next
                End If
            Next
        ElseIf objCheckBox.Checked = False Then
            strSelected = ""
            For Each dgItem In dgSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked = True Then
                    lblFee = dgItem.FindControl("lblFee")
                    If lblFee.Text.Trim().Length > 0 Then
                        totalFee = totalFee + Double.Parse(lblFee.Text)
                    End If
                    Lbl1 = dgItem.FindControl("lblProductCode")
                    strSelected = strSelected + "," + Lbl1.Text + "-" + dgSelectionList.DataKeys(dgItem.ItemIndex).ToString()
                End If
            Next
            Dim strAr() As String
            Dim str1() As String
            Dim i As Integer
            Dim itmType As String
            Dim itmIndex As String
            strAr = strSelected.Split(",")
            If strAr.Length > 1 Then
                For i = 0 To strAr.Length - 1
                    str1 = strAr(i).Split("-")
                    If str1.Length = 2 Then
                        itmType = str1(0)
                        itmIndex = str1(1)
                        For Each dgItem In dgSelectionList.Items
                            selectRadioButton = dgItem.FindControl("chkEvent")
                            Lbl1 = dgItem.FindControl("lblProductCode")
                            lbl3 = dgItem.FindControl("lblStatus")
                            If lbl3.Text <> "Pending" Then
                                If Lbl1.Text.Equals(itmType) And dgSelectionList.DataKeys(dgItem.ItemIndex) <> itmIndex Then
                                    selectRadioButton.Enabled = False
                                Else
                                    selectRadioButton.Enabled = True
                                End If
                            End If
                        Next
                    End If
                Next
            Else
                For Each dgItem In dgSelectionList.Items
                    Lbl2 = dgItem.FindControl("lblStatus")
                    If Lbl2.Text <> "Pending" Then
                        selectRadioButton = dgItem.FindControl("chkEvent")
                        selectRadioButton.Enabled = True
                    End If
                Next
            End If
        End If
        totalFee = 0.0
        For Each dgItem In dgSelectionList.Items
            Lbl2 = dgItem.FindControl("lblStatus")
            Lbl1 = dgItem.FindControl("lblFee")
            selectRadioButton = dgItem.FindControl("chkEvent")
            Dim ChildNumber As Integer, myTableCell As TableCell, ProductCode As String = "", RegDeadline As Date, Chapter As String, ContestDesc As String
            Dim PaidRegistrations As Integer, Capacity As Integer, ContestID As Integer, ChapterId As Integer, EventId As Integer
            myTableCell = dgItem.Cells(4)
            ContestID = myTableCell.Text

            myTableCell = dgItem.Cells(3)
            ChildNumber = myTableCell.Text

            myTableCell = dgItem.Cells(14)
            ChapterId = myTableCell.Text

            myTableCell = dgItem.Cells(8)
            EventId = myTableCell.Text

            myTableCell = dgItem.Cells(15)
            RegDeadline = Date.Parse(myTableCell.Text)

            myTableCell = dgItem.Cells(14)
            Chapter = myTableCell.Text

            myTableCell = dgItem.Cells(13)
            ProductCode = myTableCell.Text

            myTableCell = dgItem.Cells(18)
            ContestDesc = myTableCell.Text
            ' Added on Jun02,2015- to allow
            'a) When parent selects, if the capacity is exceeded and if the selected contest for the selected child is NOT on the exception list, do not allow selection and give message, "Capacity was reached. This contest cannot be selected."
            'b) When parent selects, if the capacity is exceeded and if the selected contest for the selected child is on the exception list, allow selection.
            PaidRegistrations = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant Where ContestCode =" & ContestID & " and ContestYear= " & Session("EventYear") & " and PaymentReference is not null"))
            Capacity = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest Where ContestID =" & ContestID & " and Contest_Year= " & Session("EventYear")))
            Dim ExceptCnt As Integer = 0
            If PaidRegistrations >= Capacity Then
                ExceptCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ExContestant Where ContestYear= " & Session("EventYear") & " and Memberid=" & Session("CustIndID") & " and ChildNumber=" & ChildNumber & " and ProductCode='" & ProductCode & "' and EventId=" & EventId & " and Chapterid=" & ChapterId & " and NewDeadLine>=GetDate()"))
            End If
            If selectRadioButton.Checked = True And Lbl2.Text.Equals("Select") Then
                If RegDeadline < Now.Date Then
                    lblWarning2.Text = lblWarning2.Text & " <br> Sorry Registration Deadline " & RegDeadline.ToShortDateString & " Passed for " & ContestDesc & " - " & Chapter
                    lblWarning2.Visible = True
                    Exit For
                End If
                If PaidRegistrations >= Capacity And ExceptCnt = 0 And PaidRegistrations <> 0 And Capacity <> 0 Then
                    ' selectRadioButton.Checked = False
                    lblWarning2.Text = " <br> Capacity was reached. The contest '" & ContestDesc & "' cannot be selected."
                    lblWarning2.Visible = True
                    Exit For
                End If
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            ElseIf selectRadioButton.Checked = False And Lbl2.Text.Equals("Pending") Then
                totalFee = totalFee + Double.Parse(Lbl1.Text)
            End If
        Next
        lblTotalFee.Text = totalFee.ToString()
    End Sub

    Public Sub DisableGroups()
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim lblIt1 As Label
        Dim lblStatus As Label
        Dim strSelected As String
        strSelected = ""
        For Each dgItem In dgSelectionList.Items
            selectRadioButton = dgItem.FindControl("chkEvent")
            Lbl1 = dgItem.FindControl("lblStatus")
            If Lbl1.Text.Equals("Pending") Then
                Lbl2 = dgItem.FindControl("lblProductCode")
                For Each dgItem1 In dgSelectionList.Items
                    lblIt1 = dgItem1.FindControl("lblProductCode")
                    lblStatus = dgItem1.FindControl("lblStatus")
                    If lblStatus.Text <> "Pending" And lblIt1.Text.Equals(Lbl2.Text) Then
                        selectRadioButton1 = dgItem1.FindControl("chkEvent")
                        selectRadioButton1.Enabled = False
                    End If
                Next
            End If
        Next
    End Sub

    Protected Sub ddlChildren_OnSelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadAvailableContests()
    End Sub

    Function InsertContestant(ByVal ContestID As Integer, ByVal flag As Integer) As Boolean
        Dim cmd As New SqlCommand
        Dim dsContest As New DataSet
        Dim da As New SqlDataAdapter
        cmd.Connection = conn
        cmd.CommandType = CommandType.Text
        cmd.CommandText = "SELECT C.ContestID, CASE WHEN C.EventId = 2 THEN CC.RegionalFee ELSE CC.NationalFinalsFee END as Fee, C.NSFChapterID as ChapterID, C.ContestCategoryID, C.ContestDate, C.Contest_Year AS ContestYear, C.RegistrationDeadline, C.EventId, C.EventCode, C.ProductGroupId, C.ProductGroupCode, C.ProductId, C.ProductCode FROM Contest C Inner Join contestcategory CC ON CC.ContestCategoryID = C.ContestCategoryID where C.ContestID=" & ContestID
        da.SelectCommand = cmd
        da.Fill(dsContest)
        If dsContest.Tables(0).Rows.Count > 0 Then
            Try
                Dim param(16) As SqlParameter
                param(0) = New SqlParameter("@ChapterID", dsContest.Tables(0).Rows(0)("ChapterID"))
                param(1) = New SqlParameter("@ContestID", dsContest.Tables(0).Rows(0)("ContestID"))
                param(2) = New SqlParameter("@ContestYear", dsContest.Tables(0).Rows(0)("ContestYear"))
                param(3) = New SqlParameter("@ParentID", Session("CustIndID"))
                param(4) = New SqlParameter("@ChildNumber", ddlChildren.SelectedValue)
                param(5) = New SqlParameter("@Fee", dsContest.Tables(0).Rows(0)("Fee"))
                param(6) = New SqlParameter("@ContestCategoryID", dsContest.Tables(0).Rows(0)("ContestCategoryID"))
                param(7) = New SqlParameter("@EventId", dsContest.Tables(0).Rows(0)("EventID"))
                param(8) = New SqlParameter("@EventCode", dsContest.Tables(0).Rows(0)("EventCode"))
                param(9) = New SqlParameter("@ProductGroupId", dsContest.Tables(0).Rows(0)("ProductGroupID"))
                param(10) = New SqlParameter("@ProductGroupCode", dsContest.Tables(0).Rows(0)("ProductGroupCode"))
                param(11) = New SqlParameter("@ProductId", dsContest.Tables(0).Rows(0)("ProductID"))
                param(12) = New SqlParameter("@ProductCode", dsContest.Tables(0).Rows(0)("ProductCode"))
                param(13) = New SqlParameter("@CreateDate", Now)
                param(14) = New SqlParameter("@CreatedBy", Session("LoginID"))
                param(15) = New SqlParameter("@RetValue", SqlDbType.Int)
                param(15).Direction = ParameterDirection.Output
                If conn.State = ConnectionState.Closed Then conn.Open()
                If Session("CustIndID") = 0 Then
                    Return False
                    Exit Function
                End If
                If flag = 0 Then
                    ' General Insert
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_InsertContestant", param)
                Else
                    'Duplicate Insert
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_InsertDuplicateContestant", param)
                End If
                Return True
                Exit Function
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End If
        Return False
    End Function

    Dim PossibleReg As Integer

    Private Function IsValidNIO(PId As String, ChapterId As String, ChildCount As Integer) As Boolean
        Try
            Dim strMembers As String = ""
            Dim iEventId As Integer = Convert.ToInt32(Session("EventId"))
            strMembers = " select Convert(varchar,ind.automemberid) +','+ convert(varchar,case when ind.DonorType='IND' then isnull(sp.automemberid,'') else isnull(ind.relationship,'') end) from indspouse ind"
            strMembers = strMembers & " left join indspouse sp on ind.automemberid=sp.relationship  where ind.automemberid =" & Session("LoginID")
            strMembers = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strMembers)

            Dim iCOIValid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "Select count(*) from IndSpouse where CountryOfOrigin='IN' and AutoMemberId in (" & strMembers & ")")
            If iCOIValid = 0 Then
                ' kk = #of all paid registrations for the particular Year, Product, chapter, Payment reference is not null
                'nn= #of Non-Indian origin paid registrations for particular Year, Product, chapter, Payment reference is not null and COI='N'
                'Limit = max [5, Integer(NIOP*kk/100)], kk=#of all paid registrations  
                Dim TotalPaid As Long = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "Select count(*) from Contestant where ContestYear=" & Session("EventYear") & " and EventId=" & iEventId & " and ProductId=" & PId & " and paymentreference is not null")

                Dim strText As String = "Select count(*) from Contestant C where ContestYear=" & Session("EventYear") & " and EventId=" & iEventId & " and ProductId=" & PId & " and paymentreference is not null and exists(Select * from IndSpouse where CountryOfOrigin <> 'IN' and automemberid = C.ParentId)"
                strText = strText & " and exists ( Select * from IndSpouse where CountryOfOrigin <> 'IN' and relationship = C.ParentId)"
                Dim TotalNIOPaid As Long = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, strText)

                Dim limit As Decimal = 5
                strText = "select isnull(NIOP,0) from Chapter where ChapterId=" & ChapterId
                If iEventId = 13 Or iEventId = 20 Then
                    strText = "select isnull(NIOP,0) from Chapter where ChapterCode='Coaching, US'"
                End If
                Dim NIOPVal As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, strText)
                'TotalNIOPaid = 4
                'TotalPaid = 100
                'limit = 5
                Dim limitTo As Long = Convert.ToInt64(NIOPVal * (TotalPaid / 100))
                If (limit < limitTo) Then
                    limit = limitTo
                End If
                PossibleReg = limit - TotalNIOPaid
                If (TotalNIOPaid + ChildCount) > limit Then
                    Return False
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
        Return True
    End Function

    Protected Sub btnSubmitSelections_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim bFlagEnabled As Boolean = False
        btnSubmitSelections.Enabled = False
        If btnPayNow.Enabled = True Then
            bFlagEnabled = True
            btnPayNow.Enabled = False
        End If
        'insert into Contestant table
        Dim dgItem As DataGridItem
        lblWarning2.Text = String.Empty
        Dim selectRadioButton As CheckBox
        Dim intKeyValue As Integer
        Dim Lbl1 As Label
        Dim Lbl2, lblDupl As Label
        Dim bolSelected As Boolean
        Dim totalFee As Double
        Dim strAssignSelection As String = ""
        totalFee = 0.0

        Dim htNIOP As New Hashtable()
        Try
            bolSelected = False
            For Each dgItem In dgSelectionList.Items
                selectRadioButton = dgItem.FindControl("chkEvent")
                If selectRadioButton.Checked = True Then
                    ' if the child is in 8th grade , validate GB and BB
                    If Session("EventYear").ToString() <> "2016" And Session("EventID").ToString() <> "1" Then ' relaxation for this year 2016
                        Dim ChildNumber As Integer = dgItem.Cells(3).Text
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Child where ChildNumber =" & ChildNumber & " and Grade=8") > 0 Then
                            Dim lblPrdGrpCode As Label = dgItem.FindControl("lblProductGroupCode")
                            strAssignSelection = strAssignSelection & "," & lblPrdGrpCode.Text
                            If Not ViewState("PaidProductGroup") Is Nothing Then
                                Dim str As String = ViewState("PaidProductGroup")
                                ' if paid contest contains GB/BB and the selection is BB/GB
                                If (str.Contains("GB") And lblPrdGrpCode.Text.Contains("BB")) Or (str.Contains("BB") And lblPrdGrpCode.Text.Contains("GB")) Then
                                    'Display Warning message.
                                    lblWarning.Text = "You cannot take both BB and GB together due to timing conflicts."
                                    lblWarning.Visible = True
                                    GoTo lblEnd
                                End If
                            End If
                            ' if the selection contains both GB an BB
                            If strAssignSelection.Contains("BB") And strAssignSelection.Contains("GB") Then
                                lblWarning.Text = "You cannot take both BB and GB together due to timing conflicts."
                                lblWarning.Visible = True
                                'Show error message
                                GoTo lblEnd
                            End If
                        End If
                    End If
                    Dim myTableCell As TableCell, Productcode, Chapter As String, ContestDesc As String
                    Dim RegDeadline As Date
                    myTableCell = dgItem.Cells(15)
                    RegDeadline = Date.Parse(myTableCell.Text)
                    myTableCell = dgItem.Cells(2)
                    Productcode = myTableCell.Text
                    myTableCell = dgItem.Cells(14)
                    Chapter = myTableCell.Text
                    myTableCell = dgItem.Cells(18)
                    ContestDesc = myTableCell.Text
                    Dim PId As String
                    myTableCell = dgItem.Cells(12)
                    PId = myTableCell.Text

                    If RegDeadline < Now.Date Then
                        lblWarning2.Text = lblWarning2.Text & " <br> Sorry Registration Deadline " & RegDeadline.ToShortDateString() & " Passed for " & ContestDesc & " - " & Chapter
                        lblWarning2.Visible = True
                        GoTo lblEnd
                    End If
                    Dim iNoOfChild As Integer = 1
                    If htNIOP.Contains(PId) Then
                        Dim objR = CType(htNIOP.Item(PId), NIOP)
                        iNoOfChild = objR.ChildCount + 1
                        htNIOP.Remove(PId)
                    End If
                    Dim obj As New NIOP
                    obj.ChildCount = iNoOfChild
                    obj.ContestDesc = ContestDesc
                    obj.Chapter = Chapter
                    htNIOP.Add(PId, obj)

                    'If IsValidNIO(PId, Chapter) = False Then
                    '    'Display Warning message.
                    '    lblWarning.Text = "Capacity is reached for " & ContestDesc & "."
                    '    lblWarning.Visible = True
                    '    GoTo lblEnd
                    'End If
                    bolSelected = True
                End If
            Next 
            Dim denum As IDictionaryEnumerator = htNIOP.GetEnumerator()
            Dim dentry As DictionaryEntry

            While denum.MoveNext()
                dentry = CType(denum.Current, DictionaryEntry)
                Dim newObj = CType(dentry.Value, NIOP)

                If IsValidNIO(dentry.Key, newObj.Chapter, newObj.ChildCount) = False Then
                    'Display Warning message.
                    lblWarning.Text = "***Capacity is reached for " & newObj.ContestDesc & ".***"
                    If (PossibleReg > 0) Then
                        lblWarning.Text = "You can only register " & PossibleReg & " child(ren) for " & newObj.ContestDesc & " since the capacity is limited."
                    End If
                    lblWarning.Visible = True
                    GoTo lblEnd
                End If
            End While


            If bolSelected = False Then
                'Display Warning message.
                lblWarning.Text = "No Contest selected, please select at least one event."
                lblWarning.Visible = True
                GoTo lblEnd
            End If
            conn = New SqlConnection()
            conn.ConnectionString = Application("ConnectionString")
            conn.Open()
            For Each dgItem In dgSelectionList.Items
                totalFee = 0.0
                selectRadioButton = dgItem.FindControl("chkEvent")
                Lbl1 = dgItem.FindControl("lblStatus")
                lblDupl = dgItem.FindControl("lblDuplicate")
                If selectRadioButton.Checked = True And Lbl1.Text <> "Pending" Then
                    'Insert Record Into Contestant
                    'GetContestId 
                    Dim myTableCell As TableCell
                    Dim RegDeadline As Date
                    Dim Productcode, Chapter As String, ContestDesc As String
                    Dim ChildNumber As Integer
                    Dim PaidRegistrations As Integer, Capacity As Integer, ContestID As Integer, ChapterId As Integer, EventId As Integer
                    myTableCell = dgItem.Cells(15)
                    RegDeadline = Date.Parse(myTableCell.Text)
                    myTableCell = dgItem.Cells(13)
                    Productcode = myTableCell.Text
                    myTableCell = dgItem.Cells(17)
                    Chapter = myTableCell.Text
                    myTableCell = dgItem.Cells(3)
                    ChildNumber = myTableCell.Text
                    myTableCell = dgItem.Cells(4)
                    ContestID = myTableCell.Text
                    myTableCell = dgItem.Cells(14)
                    ChapterId = myTableCell.Text
                    myTableCell = dgItem.Cells(8)
                    EventId = myTableCell.Text
                    myTableCell = dgItem.Cells(18)
                    ContestDesc = myTableCell.Text

                    '********* Added on June02,2013 to limit the number of Registrations based on Capacity Value from Contest table for Finals*********'
                    PaidRegistrations = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant Where ContestCode =" & ContestID & " and ContestYear= " & Session("EventYear") & " and PaymentReference is not null"))
                    Capacity = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest Where ContestID =" & ContestID & " and Contest_Year= " & Session("EventYear")))
                    Dim ExceptCnt As Integer = 0
                    ExceptCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ExContestant Where ContestYear= " & Session("EventYear") & " and Memberid=" & Session("CustIndID") & " and ChildNumber=" & ChildNumber & " and ProductCode='" & Productcode & "' and EventId=" & EventId & " and Chapterid=" & ChapterId))
                    If PaidRegistrations >= Capacity And PaidRegistrations <> 0 And Capacity <> 0 Then
                        If ExceptCnt = 0 Then
                            lblWarning2.Text = lblWarning2.Text & " <br> Capacity was reached. The contest '" & ContestDesc & "' cannot be selected."
                            lblWarning2.Visible = True
                            Continue For
                        Else
                            RegDeadline = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Max(NewDeadLine) from ExContestant Where ContestYear= " & Session("EventYear") & " and Memberid=" & Session("CustIndID") & " and ChildNumber=" & ChildNumber & " and ProductCode='" & Productcode & "' and EventId=" & EventId & " and Chapterid=" & ChapterId)
                        End If
                    End If

                    If Not RegDeadline < Now.Date Then
                        intKeyValue = dgSelectionList.DataKeys(dgItem.ItemIndex)
                        If lblDupl.Text.Trim = "Duplicate" And ExceptCnt = 0 Then
                            InsertContestant(intKeyValue, 1)
                            lblWarning2.Text = lblWarning2.Text & "Your duplicate registration " & ContestDesc & " - " & Chapter & " was sent for approval. It may take 2 to 3 days for approval."
                            lblWarning2.Visible = True
                        ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Child where ChildNumber =" & ChildNumber & " and Grade=0") > 0 Then
                            Session("ContestID") = intKeyValue
                            Session("bolSelected") = bolSelected
                            tblGradeWarng.Visible = True
                            GoTo lblEnd
                        Else
                            InsertContestant(intKeyValue, 0)
                        End If
                    Else
                        lblWarning2.Text = lblWarning2.Text & " <br> Sorry Registration Deadline " & RegDeadline.ToShortDateString & " Passed for " & ContestDesc & " - " & Chapter
                        lblWarning2.Visible = True
                    End If
                ElseIf selectRadioButton.Checked = True And Lbl1.Text.Equals("Pending") Then
                    intKeyValue = dgSelectionList.DataKeys(dgItem.ItemIndex)
                    Lbl2 = dgItem.FindControl("lblRegId")
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, "Delete from Contestant where Contestant_ID=" & Lbl2.Text & " AND PaymentReference is Null")
                End If
            Next
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
            If bolSelected = True Then
                If Session("EventID") = 1 Then
                    InsertContestantsForMeal()
                End If
                LoadAvailableContests()
                LoadPendingContests()
            End If
            lblWarning.Text = " Submitted Successfully"
            lblWarning.Visible = True
        Catch ex As Exception
            If conn.State = ConnectionState.Open Then
                conn.Close()
            End If
        End Try
lblEnd:
        If bFlagEnabled = True Then
            btnPayNow.Enabled = True
        End If
        btnSubmitSelections.Enabled = True
    End Sub

    Private Sub InsertContestantsForMeal()
        Dim dsContestant As New DataSet, drNew As DataRow, prmArrayInsert(12) As SqlParameter
        Dim tblConestant() As String = {"ContestantForMC"}

        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Now.Year 'Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        If conn.State = ConnectionState.Closed Then conn.Open()
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)
        If dsContestant.Tables(0).Rows.Count > 0 Then
            For Each drNew In dsContestant.Tables(0).Rows
                Try
                    Dim amount As Decimal = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "(select mealday1 as MealAmt from WeekCALENDAR where SatDay1='" & drNew.Item("ContestDate") & "') Union select mealday2 as MealAmt from WeekCALENDAR where SunDay2='" & drNew.Item("ContestDate") & "'")
                    '** Ferdine Silvaa Testing
                    'Response.Write("<BR>" & amount)
                    prmArrayInsert(0) = New SqlParameter("@EventName", "Finals")
                    prmArrayInsert(1) = New SqlParameter("@ContestYear", Now.Year)
                    prmArrayInsert(2) = New SqlParameter("@AutoMemberID", drNew.Item("ParentID"))
                    prmArrayInsert(3) = New SqlParameter("@ChildNumber", drNew.Item("ChildNumber"))
                    prmArrayInsert(4) = New SqlParameter("@ContestDate", drNew.Item("ContestDate"))
                    prmArrayInsert(5) = New SqlParameter("@Name", drNew.Item("ContestantName"))
                    prmArrayInsert(6) = New SqlParameter("@ModifiedDate", Now)
                    prmArrayInsert(7) = New SqlParameter("@ModifiedUserID", Session("CustIndID"))
                    prmArrayInsert(8) = New SqlParameter("@Gender", drNew.Item("Gender"))
                    prmArrayInsert(9) = New SqlParameter("@Amount", amount)
                    prmArrayInsert(10) = New SqlParameter("@MealType", "Lunch")
                    prmArrayInsert(11) = New SqlParameter("@RetValue", SqlDbType.Int)
                    prmArrayInsert(11).Direction = ParameterDirection.Output
                    If conn.State = ConnectionState.Closed Then conn.Open()
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_InsertContestantForMealCharge", prmArrayInsert)
                Catch ex As SqlException
                    ' Response.Write(ex.ToString())
                End Try
            Next
        End If
    End Sub

    Private Function IsThisDataInDB(ByVal ChildNumber As Integer, ByVal ContestId As Integer) As Boolean
        Dim dsDataInDB As New DataSet
        Dim strSQL As String
        IsThisDataInDB = False
        If conn.State = ConnectionState.Closed Then conn.Open()
        strSQL = " Select * from Contestant WHERE " & _
                 " ContestYear = " & Session("EventYear") & _
                 " and ChapterId = 1 and ChildNumber = " & ChildNumber & _
                 " and ContestCode = " & ContestId
        dsDataInDB = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL)
        If Not dsDataInDB Is Nothing Then
            If dsDataInDB.Tables(0).Rows.Count > 0 Then
                IsThisDataInDB = True
            End If
        End If
    End Function

    Protected Sub btnPayNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayNow.Click
        Session("RegFee") = lblPendAmt.Text
        Session("LateFee") = 0
        Dim htNIOP As New Hashtable()

       
        If Session("EventID").ToString = "1" Then 'National
            'Check whether selection made
            If Session("ContestsSelected") = "" And DGPending.Items.Count = 0 And dgPaidContests.Items.Count = 0 Then
                lblNoContestMsg.ForeColor = Drawing.Color.Red
                lblNoContestMsg.Text = "No contests are selected.  You cannot proceed to the next page."
            ElseIf CheckBadgeNumber() = True Then
                lblNoContestMsg.ForeColor = Drawing.Color.Red
                lblNoContestMsg.Text = "Since badge numbers were already issued, please remove the pending selections; otherwise you will get charged, but cannot participate."
            Else
                ''Write an check for Reg deadline passed
                Dim i As Integer
                Dim flag As Boolean = False
                lblPendingErr.Text = String.Empty

                For i = 0 To DGPending.Items.Count - 1
                    Dim RegDeadline As Date
                    Dim productcode As String, PId As String
                    Dim ContestID As Integer
                    Dim myTableCell As TableCell
                    Dim PaidRegistrations As Integer
                    Dim Capacity As Integer, ChildNumber As Integer, ChapterId As Integer, EventId As Integer, ContestDesc As String
                    'RegDeadline By ** Ferdine Silvaa
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(15)
                    RegDeadline = Date.Parse(myTableCell.Text)
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(13)
                    productcode = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(4)
                    ContestID = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(3)
                    ChildNumber = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(14)
                    ChapterId = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(8)
                    EventId = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(12)
                    PId = myTableCell.Text

                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(18)
                    ContestDesc = myTableCell.Text
                    If RegDeadline < Now.Date Then
                        lblPendingErr.Text = lblPendingErr.Text & " <br> Sorry Registration Deadline Passed for " & ContestDesc & ". Please delete it to proceed."
                        lblPendingErr.Visible = True
                        flag = True
                        Exit For
                    Else
                        '********* Added on 03-05-2013 to limit the number of Registrations based on Capacity Value from Contest table for Finals*********'
                        PaidRegistrations = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant Where ContestCode =" & ContestID & " and ContestYear= " & Now.Year() & " and PaymentReference is not null"))
                        Capacity = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest Where ContestID =" & ContestID & " and Contest_Year= " & Now.Year()))
                        If Capacity > 0 Then
                            If PaidRegistrations >= Capacity Then
                                lblPendingErr.Visible = True
                                'commented by bindhu- Allow user to click paynow if capacity is exceed
                                ' Validate if it is in excontestant list then allow
                                Dim ExceptCnt As Integer = 0
                                ExceptCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ExContestant Where ContestYear= " & Now.Year() & " and Memberid=" & Session("CustIndID") & " and ChildNumber=" & ChildNumber & " and ProductCode='" & productcode & "' and EventId=" & EventId & " and chapterid=" & ChapterId & " and NewDeadLine>=getdate()"))
                                If ExceptCnt > 0 Then
                                    lblPendingErr.Text = lblPendingErr.Text & " <br> Capacity has been filled up for the contest " & ContestDesc & "."
                                    flag = False
                                Else
                                    lblPendingErr.Text = lblPendingErr.Text & " <br> Capacity has been filled up.  Cannot register for the contest ," & ContestDesc & "."
                                    flag = True
                                    Exit For
                                End If

                            End If

                            Dim iNoOfChild As Integer = 1
                            If htNIOP.Contains(PId) Then
                                Dim objR = CType(htNIOP.Item(PId), NIOP)
                                iNoOfChild = objR.ChildCount + 1
                                htNIOP.Remove(PId)
                            End If
                            Dim obj As New NIOP
                            obj.ChildCount = iNoOfChild
                            obj.ContestDesc = ContestDesc
                            obj.Chapter = ChapterId
                            htNIOP.Add(PId, obj)


                            'If IsValidNIO(PId, ChapterId) = False Then
                            '    'Display Warning message.
                            '    lblPendingErr.Text = "Capacity is reached for " & ContestDesc & "."
                            '    lblPendingErr.Visible = True
                            '    GoTo lblEnd
                            'End If
                        End If


                    End If
                Next
                 
                Dim denum As IDictionaryEnumerator = htNIOP.GetEnumerator()
                Dim dentry As DictionaryEntry

                While denum.MoveNext()
                    dentry = CType(denum.Current, DictionaryEntry)
                    Dim newObj = CType(dentry.Value, NIOP) 
                    If IsValidNIO(dentry.Key, newObj.Chapter, newObj.ChildCount) = False Then
                        'Display Warning message.
                        lblWarning.Text = "***Capacity is reached for " & newObj.ContestDesc & ".***"
                        If (PossibleReg > 0) Then
                            lblWarning.Text = "You can only register " & PossibleReg & " child(ren) for " & newObj.ContestDesc & " since the capacity is limited."
                        End If
                        lblWarning.Visible = True
                        GoTo lblEnd
                    End If
                End While


                If flag = False Then
                    Dim redirectURL As String
                    redirectURL = "~/MealCharge.aspx"
                    Page.Response.Redirect(redirectURL)
                End If
            End If
        Else
            '** Ferdine Silva
            Dim i As Integer
            Dim flag As Boolean = False
            lblPendingErr.Text = String.Empty
            For i = 0 To DGPending.Items.Count - 1
                Dim RegDeadline As Date
                Dim productcode As String, PId As String
                Dim myTableCell As TableCell
                Dim PaidRegistrations As Integer, ContestID As Integer, ContestDesc As String
                Dim Capacity As Integer, ChildNumber As Integer, ChapterId As Integer, EventId As Integer

                'RegDeadline By ** Ferdine Silvaa
                myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(15)
                RegDeadline = Date.Parse(myTableCell.Text)
                myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(13)
                productcode = myTableCell.Text

                myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(18)
                ContestDesc = myTableCell.Text
                If RegDeadline < Now.Date Then
                    lblPendingErr.Text = lblPendingErr.Text & " <br> Sorry Registration Deadline Passed for " & ContestDesc & ". Please delete it to proceed."
                    lblPendingErr.Visible = True
                    flag = True
                    Exit For
                Else
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(4)
                    ContestID = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(3)
                    ChildNumber = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(14)
                    ChapterId = myTableCell.Text
                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(8)
                    EventId = myTableCell.Text

                    myTableCell = DGPending.Items.SyncRoot.Item(i).Cells(12)
                    PId = myTableCell.Text

                    '********* Added on 03-05-2013 to limit the number of Registrations based on Capacity Value from Contest table for Finals*********'
                    PaidRegistrations = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from Contestant Where ContestCode =" & ContestID & " and ContestYear= " & Session("EventYear") & " and PaymentReference is not null"))
                    Capacity = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Capacity,0) as Capacity From Contest Where ContestID =" & ContestID & " and Contest_Year= " & Session("EventYear")))
                    If Capacity > 0 Then
                        If PaidRegistrations >= Capacity Then
                            'lblPendingErr.Text = lblPendingErr.Text & " <br> Capacity has been filled up.  Cannot register for the contest ," & productcode & "."
                            lblPendingErr.Text = lblPendingErr.Text & " <br> Capacity has been filled up for the contest " & ContestDesc & "."
                            lblPendingErr.Visible = True
                            'commented by bindhu- Allow user to click paynow if capacity is exceed
                            flag = True
                            'Exit For
                            ' Validate if it is in excontestant list theb allow
                            Dim ExceptCnt As Integer = 0
                            ExceptCnt = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ExContestant Where ContestYear= " & Session("EventYear") & " and Memberid=" & Session("CustIndID") & " and ChildNumber=" & ChildNumber & " and ProductCode='" & productcode & "' and EventId=" & EventId & " and chapterid=" & ChapterId & " and NewDeadLine>=getdate()"))
                            If ExceptCnt > 0 Then
                                flag = False
                            Else
                                flag = True
                                Exit For
                            End If
                        End If
                        Dim iNoOfChild As Integer = 1
                        If htNIOP.Contains(PId) Then
                            Dim objR = CType(htNIOP.Item(PId), NIOP)
                            iNoOfChild = objR.ChildCount + 1
                            htNIOP.Remove(PId)
                        End If
                        Dim obj As New NIOP
                        obj.ChildCount = iNoOfChild
                        obj.ContestDesc = ContestDesc
                        obj.Chapter = ChapterId
                        htNIOP.Add(PId, obj)

                        'If IsValidNIO(PId, ChapterId) = False Then
                        '    'Display Warning message.
                        '    lblPendingErr.Text = "Capacity is reached for " & ContestDesc & "."
                        '    lblPendingErr.Visible = True
                        '    GoTo lblEnd
                        'End If

                    End If
                End If
            Next
            Dim denum As IDictionaryEnumerator = htNIOP.GetEnumerator()
            Dim dentry As DictionaryEntry

            While denum.MoveNext()
                dentry = CType(denum.Current, DictionaryEntry)
                Dim newObj = CType(dentry.Value, NIOP)
                If IsValidNIO(dentry.Key, newObj.Chapter, newObj.ChildCount) = False Then
                    'Display Warning message.
                    lblWarning.Text = "***Capacity is reached for " & newObj.ContestDesc & ".***"
                    If (PossibleReg > 0) Then
                        lblWarning.Text = "You can only register " & PossibleReg & " child(ren) for " & newObj.ContestDesc & " since the capacity is limited."
                    End If
                    lblWarning.Visible = True
                    GoTo lblEnd
                End If
            End While

            If flag = False Then
                Session("contestSelSumNo") = 0
                Page.Response.Redirect("contestSelectionSum.aspx")
            End If
        End If
lblEnd:
    End Sub

    Protected Function CheckBadgeNumber() As Boolean
        Dim i As Integer
        Dim myTableCell As TableCell
        Dim contestID As Integer
        Dim flag As Boolean = False
        If DGPending.Items.Count > 0 Then
            For i = 0 To DGPending.Items.Count - 1
                myTableCell = DGPending.Items(i).Cells(4)
                contestID = CInt(myTableCell.Text)
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(C.contestant_id) from contestant C, Contest Cn where C.contestyear = Cn.Contest_Year and C.EventID = cn.EventId and C.productid = Cn.ProductId and C.chapterid = Cn.NSFChapterID and C.badgenumber is not null and Cn.ContestID=" & contestID & "") > 0 Then
                    Return True
                    Exit Function
                End If
            Next
        End If
        Return flag
    End Function

    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        tblGradeWarng.Visible = False
        InsertContestant(Session("ContestID"), 0)
        If Session("bolSelected") = True Then
            If Session("EventID") = 1 Then
                InsertContestantsForMeal()
            End If
            LoadAvailableContests()
            LoadPendingContests()
        End If
        lblWarning.Text = " Submitted Successfully"
        lblWarning.Visible = True
    End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        tblGradeWarng.Visible = False
    End Sub
End Class


Class NIOP
    Public ChildCount As Integer
    Public ContestDesc As String
    Public Chapter As String
End Class

