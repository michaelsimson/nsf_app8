<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="UploadPhotos.aspx.cs" Inherits="UploadPhotos" Title="Upload Photos" %>
 
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <br />
    <br />
    <div><u><b>Contestant Photo Management</b></u> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
        <asp:DropDownList ID="ddlyear" runat="server" Visible="false" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
            <asp:Button runat="server" Text="Export to Excel" ID="btnExport" Visible="false" onclick="btnExport_Click" />
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;     
            <asp:Button runat="server" Text="Finished Photos List" ID="btnFsdPhotos" Width="160px" Visible="false" onclick="btnFsdPhotos_Click" />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        <asp:Button ID="btnAddRegis" runat="server" Text="Add/Update Contests Registered later"  onclick="btnAddRegis_Click" Width="250px" />

    </div>
    <br />
    <br />
    <div>
    <table border="0" cellpadding = "3" cellspacing = "0"><tr><td>
        <asp:RadioButtonList ID="rdoFilter" runat="server" RepeatDirection="Horizontal" Visible="false"> <%--OnSelectedIndexChanged="rdoFilter_SelectedIndexChanged" AutoPostBack="true"--%>
            <asp:ListItem Selected="True" Value="All">All</asp:ListItem>
            <asp:ListItem Value="Approved" Text="Approved"></asp:ListItem>
            <asp:ListItem Value="Downloaded" Text="Downloaded"></asp:ListItem>
            <asp:ListItem Value="Submitted" Text="Submitted"></asp:ListItem>
            <asp:ListItem Value="Rejected" Text="Rejected"></asp:ListItem>
            <asp:ListItem Value="Unwilling" Text="Unwilling"></asp:ListItem>
            <asp:ListItem Value="UnableToFind" Text="Missing Photos"></asp:ListItem>
        </asp:RadioButtonList></td><td><asp:Button ID="btnContinue" runat="server" 
                Text="Continue" onclick="btnContinue_Click" Visible="false"  />
        </td></tr>
        <tr><td>
            <asp:Label ID="LblTshirtErr" runat="server" Text="" ForeColor ="Red" Font-Bold="true"></asp:Label>
            </td><td>&nbsp;</td></tr></table>
        <br />
        <asp:GridView ID="gvContestantPhotos" runat="server" 
            AutoGenerateColumns="false" OnRowCommand="gvContestantPhotos_RowCommand" onrowdatabound="gvContestantPhotos_RowDataBound"
            AllowSorting="true" BorderStyle="Solid" BorderWidth="1" 
            BorderColor="black" CellPadding="4" CellSpacing="0" AllowPaging="true" 
            PageSize="10" PagerSettings-Mode="Numeric" 
            OnSorting="gvContestantPhotos_Sorting" 
            OnPageIndexChanging="gvContestantPhotos_PageIndexChanging" >
            <AlternatingRowStyle  backcolor=WhiteSmoke font-size=small/>
            <RowStyle backcolor=white Wrap="False" Font-Size=Small />
			<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
			<FooterStyle BackColor="Gainsboro" ></FooterStyle>
			<%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
				            
            <Columns>
                <asp:BoundField HeaderText="First Name" DataField="first_name" SortExpression="first_name"  />
                <asp:BoundField HeaderText="Last Name" DataField="last_name" SortExpression="last_name"  />                
                <asp:ButtonField ButtonType="Button" CommandName="Download" Text="Download" Visible="false" HeaderText="Download"/>
                <asp:TemplateField HeaderText="Contestant Photo">
                    <ItemTemplate>
                        <asp:HyperLink ID="lnkFullPhoto" runat="server" Target="_blank">
                            <asp:Image ID="ChildImage" runat="server" />
                        </asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField Visible="false" HeaderText="Reviewer Comments">
                    <ItemTemplate>
                        <asp:TextBox ID="ReviewerComments" runat="server" TextMode="MultiLine" Rows="3"></asp:TextBox>
                    </ItemTemplate>
                </asp:TemplateField>                
                <asp:TemplateField Visible="false" HeaderText="Photo Status">
                    <ItemTemplate>
                        <asp:DropDownList ID="ReviewerStatus" runat="server">
                            <asp:ListItem Value="Approved" Text="Approved"></asp:ListItem>
                            <asp:ListItem Value="Submitted" Text="Submitted"></asp:ListItem>
                            <asp:ListItem Value="Pending" Text="Pending"></asp:ListItem>
                            <asp:ListItem Value="Rejected" Text="Rejected"></asp:ListItem>
                            <asp:ListItem Value="Unwilling" Text="Unwilling"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField ButtonType="Button" CommandName="ChangeStatus" Text="Change Status" Visible="false" HeaderText="Change Status"/>
                <asp:BoundField HeaderText="City" DataField="City"  />
                <asp:BoundField HeaderText="State" DataField="State"  />
                <asp:BoundField HeaderText="Contests" DataField="Contests"  />                
                <asp:BoundField HeaderText="Email" DataField="indEmail"  />
                
                <asp:BoundField HeaderText="Spouse Email" DataField="spouseEmail"  />
                <asp:BoundField HeaderText="ChapterId" DataField="ChapterId"  visible="true" />
                <asp:BoundField HeaderText="Photo File Name" DataField="PhotoFileName"  />
                <asp:BoundField HeaderText="Status Flag" DataField="StatusFlag"  />
                <asp:BoundField HeaderText="Comments" DataField="Comments"  />
                
                
                <asp:BoundField HeaderText="Review Date" DataField="ReviewDate"  DataFormatString="{0:d}" HtmlEncode="False"/>
                <asp:BoundField HeaderText="Reviewer Id" DataField="ReviewerId"  />
                <asp:TemplateField HeaderText="Select Photo">
                    <ItemTemplate>
                        <asp:FileUpload ID="FileUpload1" runat="server" />
                        <br />
                        <asp:CheckBox ID="Unwilling" runat="server" Text="Don't want photo in Bee Book." ForeColor="red" AutoPostBack="true" OnCheckedChanged="OnUnwillingStatusChanged" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:ButtonField ButtonType="Button" CommandName="Upload" Text="Upload" HeaderText="Upload" />
                <asp:BoundField  HeaderText="Member Id" DataField="MemberId"  visible="true" />
                <asp:BoundField  HeaderText="Child Number" DataField="childnumber" visible="true" SortExpression="childnumber" />
                
                
                <asp:BoundField HeaderText="Photo Id" DataField="ContestantPhotoId"  />
                             
                <asp:TemplateField HeaderText="T-Shirt Size">
                    <ItemTemplate>
                        <asp:DropDownList ID="TShirtPhoto" runat="server">
                            <asp:ListItem Value="Select" Text="Select T-Shirt Size"></asp:ListItem> 
                            <asp:ListItem Value="YM" Text="YM (upto 12 years)"></asp:ListItem> 
                            <asp:ListItem Value="YL" Text="YL (upto 16 years)"></asp:ListItem>
                            <asp:ListItem Value="YXL" Text="YXL (>16 Years)"></asp:ListItem>
                        </asp:DropDownList>
                    </ItemTemplate>
                </asp:TemplateField>
                   
            </Columns>
        </asp:GridView>
        <br />
        <div align="right">
         <asp:Button ID="BtnTshirt" Visible="false" runat="server" style="margin-right: 10px" Text="Update T-ShirtSize" Width="150px" onclick="BtnTshirt_Click" /><br />
         <asp:HiddenField ID="hdnValue" runat="server" />
        </div>
        <br />
        <br />
        <asp:Label ID="lblMessage" runat="server" Width="523px" ForeColor="red"></asp:Label></div>
        <br />
        <br />
        <ol style="margin:0 0 0 10">
        <li>The file size of your picture should not exceed 2 Megabytes</li>

        <li>If the size exceeds 2 megabytes, save your picture at a resolution to reduce the 
            size, but minimum size to get acceptable quality should be 500 K or higher. </li>

        <li>Accepted file formats for pictures: &quot;.jpg&quot; and &quot;.tif&quot; ONLY. NO other formats are 
            accepted</li>

        <li>For questions, please send a mail to nsfphoto@gmail.com</li>
        </ol>
        <br />
		<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="../Public/USContests/Finals/beephoto.aspx" Target="_blank">More Instructions for Photographs</asp:hyperlink>
        <br />
        <br />
		<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
		<asp:hyperlink id="hlinkVolunteerHome" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
        <div><asp:Label ID="lbldebug" runat="server" ForeColor="white"></asp:Label></div>
        <div><asp:Label ID="lblRoleId" runat="server" ForeColor="white"></asp:Label></div>
        <div><asp:Label ID="lblMemberId" runat="server" ForeColor="white"></asp:Label></div>
        <div><asp:Label ID="lblManagePhotos" runat="server" ForeColor="white"></asp:Label></div>
         <div><asp:Label ID="lblFlag" runat="server" ForeColor="white"></asp:Label></div>
</asp:Content>
 
 