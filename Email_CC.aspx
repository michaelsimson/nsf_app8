

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Email_CC.aspx.vb" Inherits="Email_CC" title="Chapter Coordinator's - Send EMail" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <script type="text/javascript" language="javascript">
      function hide_chapter(var1)
      {
      document.getElementById(var1).style.display = "none";
      }
   </script>
   <table style="width: 680px">
      <tr>
         <td style="width: 680px;">
            <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="284px" ></asp:HyperLink>
         </td>
      </tr>
   </table>
   <table>
      <tr>
         <td align="center" style="width: 542px">
            <asp:Label ID="lblerr" runat="server" ForeColor="Red" Text="You are not allowed to use this application." Visible="False" Width="471px"></asp:Label>
         </td>
      </tr>
   </table>
   <table>
      <tr>
         <td style="width: 756px" align="center">
            <asp:ListBox ID="lstchapter" runat="server" AutoPostBack="true" SelectionMode="Multiple" DataValueField="chapterID" DataTextField="ChapterCode" Rows="15">
            </asp:ListBox>
            <asp:TextBox ID="txtselectedchapter" runat="server" TextMode="MultiLine" Enabled="false" Width="378px" Height="230px"></asp:TextBox>
            <br />
            <asp:Label ID="lblselected" runat="server" Font-Size="Small" ></asp:Label>
            &nbsp;
            <asp:DropDownList ID="drpChapter" runat="server"></asp:DropDownList>
            <br /> 
            <asp:Label ID="lblmessage" runat="server" ForeColor="red"></asp:Label>
            <br />
            <asp:Button ID="btnsubmit" runat="server" Text="Submit" />
         </td>
      </tr>
   </table>
   <table id="tabletarget" runat="server" width="100%" border="0" cellpadding="0" cellspacing="0">
      <tr style="height :25px">
         <td align="right" style="height: 25px; width: 408px;">
            <asp:Label ID="lbltarget" runat="server" Text="Target:" Width="150px" Font-Bold="true"></asp:Label>
         </td>
         <td style="width: 10%; height: 25px;">
            <asp:RadioButton ID="rbtall" runat="server" Text="All" GroupName="Group1" AutoPostBack="true" OnCheckedChanged="rbtall_CheckedChanged" Width="53px" />
         </td>
         <td style="width: 25%; height: 25px;" colspan="2">
            <asp:RadioButton ID="btnregisteredparetns" runat="server" Text="Registered Parents" AutoPostBack="true" OnCheckedChanged="btnregisteredparetns_CheckedChanged" Checked="true" GroupName="Group1" Width="148px" />
         </td>
         <td style="width: 25%; height: 25px;" colspan="2">
            <asp:RadioButton ID="rbtassigndvolunteer" runat="server" Text="Assigned Volunteers" AutoPostBack="true" OnCheckedChanged="rbtassigndvolunteer_CheckedChanged" GroupName="Group1" />
         </td>
         <td style="height: 25px; width:25%" colspan="2">
            <asp:RadioButton ID="rbtunassigndvolunteer" runat="server" Text="UnAssigned Volunteers" AutoPostBack="true" OnCheckedChanged="rbtunassigndvolunteer_CheckedChanged" GroupName="Group1" />
         </td>
         <td style="height: 25px; width:15%" colspan="2">
            <asp:RadioButton ID="rbtownlist" runat="server" Text="Own List" AutoPostBack="true" OnCheckedChanged="rbtownlist_CheckedChanged" GroupName="Group1" />
         </td>
      </tr>
      <tr style="height:25px">
         <td style="width: 408px"></td>
      </tr>
      <tr>
         <td style="height: 15px; width: 408px;"></td>
         <td style="width:57px; height: 15px;"></td>
         <td style="width: 84px; height: 15px;">
            <asp:Label ID="lblevent" runat="server" Text="Event:" Font-Bold="true"></asp:Label>
         </td>
         <td style="width: 332px; height: 14px;">
            <asp:DropDownList id="drpevent" Width="140px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpevent_SelectedIndexChanged">
               <asp:ListItem Value="2" Text="Chapter Contest" Selected="True"></asp:ListItem>
               <asp:ListItem Value="3" Text="Workshop"></asp:ListItem>
               <asp:ListItem Value="1" Text="National Finals"></asp:ListItem>
                <asp:ListItem Value="19" Text="PrepClub"></asp:ListItem>
               
               <%--<asp:ListItem Value="13" Text="Coaching"></asp:ListItem>--%>
            </asp:DropDownList> &nbsp;
            <br /><br />
         </td>
         <td style="width: 35px; text-align:right" rowspan="2">
            <asp:Label ID="lblassignrole" Text="Role:" runat="server" Font-Bold="true"></asp:Label>
         </td>
         <td rowspan="2" style="width: 72px">
            <asp:ListBox id="lstAssignRole" Height="100px" Enabled="false" DataValueField="RoleID" DataTextField="Name" SelectionMode="Multiple" Width="200px" runat="server" OnSelectedIndexChanged="lstAssignRole_SelectedIndexChanged" AutoPostBack="true" >
            </asp:ListBox>
         </td>
         <td style="width: 40px;text-align:right" rowspan="2">
            <asp:Label ID="lblunassignrole" Text="Role:" runat="server" Font-Bold="true"  ></asp:Label>
         </td>
         <td rowspan="2">
            <asp:ListBox id="lstUnassignRole" DataValueField="VolunteerTaskID" DataTextField="TaskDescription" Enabled="false" SelectionMode="Multiple" width="200px" Height="100px" runat="server" OnSelectedIndexChanged="lstUnassignRole_SelectedIndexChanged" AutoPostBack="true" >
            </asp:ListBox>
         </td>
      </tr>
      <tr style="height:25px">
         <td style="height: 14px; width: 408px;"></td>
         <td style="width: 57px; height: 14px;"></td>
         <td style="height: 13px; width: 84px">
            <asp:Label ID="lblregistrationtype" runat="server" Text="Type of Registration:" Width="140px" Font-Bold="true"></asp:Label>
         </td>
         <td style="height: 13px">
            <asp:DropDownList ID="drpregistrationtype" runat="server" >
            </asp:DropDownList>
         </td>
      </tr>
      <tr style="height:25px">
         <td style="width: 408px"></td>
         <td style="width: 57px"></td>
         <td style="width: 84px">
            <asp:Label ID="lblproductgroup" Text="Product Group:" Width="140px" runat="server" Font-Bold="true"></asp:Label>
         </td>
         <td style="width: 332px">
            <asp:ListBox id="lstProductGroup" DataValueField="ProductGroupID" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" >
            </asp:ListBox>
            <br /><br />
         </td>
           <td colspan="5" align="right"><div id="divchooseexcelownlist" runat="server" visible="false">
                          <br />
                          <br />
                          <asp:Label ID="lblattfile" ForeColor="Red" Text="Upload Excel file with email list in column A:" runat="server"></asp:Label> <asp:FileUpload ID="AttachmentFile" runat="server"/>

                                                    </div></td>

                          </tr>
      </tr>
      <tr style="height:25px">
         <td style="width: 408px"></td>
         <td style="width: 57px"></td>
         <td style="width: 84px">
            <asp:Label ID="lblProductid" Text="Product:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
         </td>
         <td style="width: 332px">
            <asp:ListBox id="lstProductid" Enabled="false" DataValueField="ProductCode" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged">
            </asp:ListBox>
            <br /><br />
         </td>
      </tr>
      <tr style="height:25px">
         <td style="width: 408px"></td>
         <td style="width: 57px"></td>
         <td style="width: 84px">
            <asp:Label ID="lblyear" Text="Event Year:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
         </td>
         <td style="width: 332px">
            <asp:ListBox id="lstyear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server" OnSelectedIndexChanged="lstyear_SelectedIndexChanged" AutoPostBack="true">
            </asp:ListBox>
         </td>
         <td colspan="3">
            <asp:Label ID="lblNote" runat="server" ForeColor="Red" 
               Text="**Note:  Sends email if and only if <br /> a) Valid Email flag is NULL and  <br /> b) Newsletter is not in 2 or 3" 
               Visible="true"></asp:Label>
         </td>
      </tr>
      <tr style="height:25px">
         <td style="width: 408px"></td>
         <td colspan="3"></td>
      </tr>
      <tr>
         <td style="height: 24px; width: 408px;"></td>
         <td style="height: 24px; width: 57px;"></td>
         <td style="width: 253px; height: 24px;" colspan="2">
            <asp:CheckBox ID="chkPaid" runat="server" Text="Exclude Paid Registrations" />
         </td>
      </tr>
      <tr>
         <td style="height: 24px; width: 408px;"></td>
         <td style="height: 24px; width: 57px;"></td>
         <td style="width: 253px; height: 24px;" colspan="2">
            <asp:Button ID="btnselectmail" runat="server" Text="Continue" />
         </td>
      </tr>
   </table>
</asp:Content>

