<%@ Page Language="VB" AutoEventWireup="false" CodeFile="PrepclubRegistration.aspx.vb"
    Inherits="PrepClubRegistration" MasterPageFile="NSFMasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">
    <table cellspacing="1" cellpadding="1" border="0" id="Table1" runat="server" style="width: 900px">
        <tr>
            <td class="Heading" align="center" colspan="2">
                PrepClub Registration
            </td>
        </tr>
        <tr>
            <td colspan="3" style="height: 18px">
                <asp:HyperLink ID="hlinkChild" runat="server" NavigateUrl="MainChild.aspx">Back to Children List</asp:HyperLink></td>
        </tr>
        <tr>
            <td colspan="3">
            </td>
        </tr>
    </table>
    <asp:Label ID="lblNoContestMsg" runat="server" Font-Bold="True" ForeColor="ForestGreen"></asp:Label>
    <asp:Panel ID="pnChild" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table id="tblEligibleContests" runat="server" width="900px">
            <tr>
                <td style="width: 35%;" class="mediumwordingbold">
                    <asp:Label ID="lblSelectedChild" runat="server" CssClass="smallwordingbold" Width="100%"
                        Font-Bold="True"></asp:Label>
                </td>
                <td colspan="2" style="font-weight: bold; color: red; font-size: 11px; width: 50%;">
                    <asp:Label ID="lblWarning2" runat="server" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td style="width: 50%;" class="mediumwordingbold">
                    Children:
                    <asp:DropDownList ID="ddlChildren" runat="server" AutoPostBack="true" CssClass="mediumwordingbold"
                        Width="50%" DataTextField="First_Name" DataValueField="ChildNumber" OnSelectedIndexChanged="ddlChildren_OnSelectedIndexChanged">
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="mediumwordingbold" style="color: Maroon">
                    Register and submit selections for one child at a time</td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlEligibleContests" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table>
            <tr id="trChild1" runat="server">
                <td class="SmallFont">
                    Paid&nbsp; (Completed Transactions)</td>
            </tr>
            <tr>
                <td style="width: 819px">
                    <asp:DataGrid ID="dgPaidList" runat="server" CssClass="GridStyle" DataKeyField="RegistrationId"
                        CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%">
                        <FooterStyle CssClass="GridFooter"></FooterStyle>
                        <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
                        <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Prepclub" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" SortExpression="Grade"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Prepclub Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblEligibleContests" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RegDeadLine" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegDeadLine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegDeadline","{0:d}") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Venue" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblVenue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid></td>
            </tr>
            <tr>
                <td class="ItemCenter" align="center" colspan="2">
                    <asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red"
                        Visible="False"></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlWkSelectionList" runat="server" Style="width: 900px; border-color: Black;
        border-width: 1px; border-style: solid">
        <table id="tblWkSelectionList" runat="server" width="100%" border="0">
            <tr>
                <td colspan="2" class="SmallFont" id="tdPrepclub" runat="server">
                </td>
            </tr>
            <tr>
                <td align="left" colspan="2">
                    <asp:Label ID="lblTotalFeeMsg" runat="server" Text="Amount For Selected Prepclubs"
                        CssClass="SmallFont">
                    </asp:Label>
                    <asp:Label ID="lblTotalFee" runat="server" Text="0" CssClass="SmallFont">
                    </asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 819px">
                    <asp:DataGrid ID="dgWkSelectionList" runat="server" CssClass="GridStyle" DataKeyField="PrepClubCalID"
                        CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True"
                        Width="100%">
                        <FooterStyle CssClass="GridFooter"></FooterStyle>
                        <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                        <AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
                        <ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
                        <HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:CheckBox ID="chkEvent" runat="server" OnCheckedChanged="chkEvent_OnCheckedChanged"
                                        CssClass="SmallFont" AutoPostBack="true"></asp:CheckBox>
                                    <asp:Label ID="lblRegId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegistrationId") %>'
                                        CssClass="SmallFont" Visible="false">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Status" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblStatus" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Status") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Prepclub" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000"
                                HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                    
                                    <asp:Label ID="lblProductCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>'
                                        CssClass="SmallFont" Visible="false">
                                    </asp:Label>
                                    
                                    <asp:Label ID="lblEventCode" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventCode") %>'
                                        CssClass="SmallFont" Visible="false">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" SortExpression="Grade"
                                HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
                                <ItemTemplate>
                                    <asp:Label ID="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>'
                                        CssClass="SmallFont">
                                    </asp:Label>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Prepclub Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="RegDeadLine" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegDeadLine" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegDeadline","{0:d}") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Venue" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
                                HeaderStyle-ForeColor="#990000">
                                <ItemTemplate>
                                    <asp:Label ID="lblVenue" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>'
                                        CssClass="SmallFont"></asp:Label>&nbsp;
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                    </asp:DataGrid></td>
            </tr>
            <tr>
                <td style="font-weight: bold; color: red; font-size: 11px; width: 50%;">
                    <asp:Label ID="lblWarning" runat="server" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 15%;" class="mediumwordingbold" align="center">
                    <asp:Button ID="btnSubmitSelections" runat="server" Text="Submit Selections" ForeColor="Red"
                        Font-Bold="true" />
                </td>
            </tr>
            <tr>
                <td style="width: 819px; display: none" align="right" id="tdSubmit" runat="server">
                    I have <font style="color: Red">submitted</font> sections
                </td>
            </tr>
            <tr>
                <td style="width: 819px; display: none" align="right" id="tdWant" runat="server">
                    for each child.I want to:</td>
            </tr>
            <tr>
                <td style="width: 819px" align="right">
                    <asp:Button ID="btnPayNow" runat="server" Text="Pay Now" ForeColor="Blue" Font-Bold="true"
                        Enabled="false" /></td>
            </tr>
            <tr>
                <td style="width: 819px; font-weight: bold; color: Red" align="center">
                    Complete selections for all children before pressing Pay Now.</td>
            </tr>
            <tr>
                <td style="width: 819px; font-weight: bold; color: Red" align="center">
                    Without submitting, your selections will not be registered.</td>
            </tr>
            <tr>
                <td class="ItemCenter" align="center" colspan="2">
                    <asp:Label ID="lblWkSelectionList" runat="server" CssClass="SmallFont" ForeColor="Red"
                        Visible="False"></asp:Label></td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <table style="width: 819px;">
        <tr>
            <td class="ContentSubTitle" align="left" colspan="2">
                <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink></td>
        </tr>
    </table>
</asp:Content>
