<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="BadgeNumbers_Gen.aspx.vb" Inherits="BadgeNumbers_Gen" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<table style="margin-left:400px; vertical-align:middle" >
<tr >
					<td class="Heading" style="text-align:center; vertical-align:middle">
					<asp:Label ID="lbltitle" runat="server" Text="Generate Badge Numbers" Height="43px"  ></asp:Label>
					</td>       
				</tr>
				</table>
				
    <asp:Label ID="lblmsg" runat="server" Text="Label" BorderWidth="5" BorderColor="white"></asp:Label><br />
    <br />
    <asp:GridView ID="GrdGenbadge" runat="server" style="margin-left: 10px" AutoGenerateColumns="False">
    <Columns>
    <asp:BoundField  ReadOnly="True" HeaderText="Count" 
                  DataField="Count" >
       <ItemStyle ForeColor="Black" Width="150px" HorizontalAlign="Center"/>      
        <HeaderStyle Font-Size="Small" ForeColor="Black" />
   </asp:BoundField>
   </Columns>
    </asp:GridView><br />
    <br />
    <table ID="tblFinalCount" runat="server" visible="false" style="margin-left: 10px" width="150px" border="1">
    <tr><td align="center"><asp:Label ID="lblFinCount" runat="server" Text="" BorderColor="white" ></asp:Label></td></tr> <br /><br />
    <tr><td align="center"><asp:Label ID="lblFinRowCount1" runat="server" Text="" BorderColor="white"></asp:Label></td></tr>
     </table>
     <br />
    <asp:HyperLink runat="server" ID="hlnkContinue" Text="Continue" BorderWidth="5" BorderColor="white"  ></asp:HyperLink>
  
</asp:Content>

