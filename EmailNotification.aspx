<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="EmailNotification.aspx.vb" Inherits="VRegistration.EmailNotification" title="Unsubscribe Email" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
    <div align="left">
        <asp:hyperlink id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink></div>
     <div style="text-align: center" align="left"> 
           
   <table border = "0" cellpadding = "0" cellspacing = "0" width = "1004px">
   <tr><td class="title02" vAlign="top" align="center">
						Unsubscribe to Email Notification
					</td>
				</tr>
   <tr><td align="center"> 
         <asp:HiddenField ID="hdIndID" runat="server" />
        <asp:HiddenField ID="hdSpID" runat="server" />	
    <table id="tblEmail" runat="server" width="700px" cellpadding="3" cellspacing="0" border="0">
      
				<tr>
					<td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Current Email:</td>
					<td  align="left"><asp:textbox id="txtOldEmail" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px"></asp:textbox>
     </td>
				</tr>
				<tr>
					<td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Password:</td>
                   	<td  align="left"><asp:textbox id="txtPassword" runat="server" CssClass="smFont" TextMode="password" Width="150px"></asp:textbox>
                          </td>
				</tr>
				
				<tr>
				    <td  align="right">
                        <%--<asp:HyperLink Text="Forgot Email"  CssClass="btn_02"  runat="server" NavigateUrl="~/Forgot_loginid.aspx"
                    ID="HyperLink2"></asp:HyperLink>--%><asp:Button ID="btnForgotPwd" runat="server" Text="Forgot Password" /></td>  
					<td align="left" colSpan="1" style="HEIGHT: 26px">
                        &nbsp;&nbsp;&nbsp;&nbsp;
                        &nbsp;&nbsp;<asp:button id="btnContinue" runat="server" CssClass="FormButton" Text="Continue"></asp:button>
                       
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        
                        <asp:Button ID="btnLogout" runat="server" Text="LOGOUT" Visible="false"  />
                        
                    </td>
				</tr>
				
				<tr>
				    <td colspan="2"  align="center">
                        <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        
                    </td>
				</tr></table>
        <table><div id="divEmailCommunicaiton" visible="false" runat="server" >
            <tr><td align="left" colspan="2">
    <asp:LinkButton ID="lbtnEComm" runat="server">Email Communication</asp:LinkButton></td> </tr> 
        <tr>
            <td >
                <div  align="center" width="250px" >

   <table border = "0" cellpadding = "3" cellspacing = "0">
   <tr><td align="center" ><b><asp:Literal ID="LitInd2" runat="server"></asp:Literal></b></td><td align = "center" ><b><asp:Literal ID="litSp2" runat="server"></asp:Literal></b></td></tr>
   <tr><td align = "left" ><asp:DropDownList ID="ddlECommInd" runat="server">
       <asp:ListItem Value="1" Text="1.No email of monthly e-newsletter"></asp:ListItem>
       <asp:ListItem Value="2" Text="2.No email on contests, workshops, coaching, etc."></asp:ListItem>
       <asp:ListItem Value="3" Text="3.No email of e-newsletter or on contests, wrokshops, etc."></asp:ListItem>
       <asp:ListItem Value="9" Text="9. Emails are fine with me"></asp:ListItem>       
       </asp:DropDownList>
      <%-- 5 : is used for technical break in the email list sent to the customers So please avoid 5 here as value --%>
       </td><td align = "left">
       <asp:DropDownList ID="ddlECommSp"   runat="server">
       <asp:ListItem Value="1" Text="1.No email of monthly e-newsletter"></asp:ListItem>
       <asp:ListItem Value="2" Text="2.No email on contests, workshops, coaching, etc."></asp:ListItem>
       <asp:ListItem Value="3" Text="3.No email of e-newsletter or on contests, wrokshops, etc."></asp:ListItem>
        <asp:ListItem Value="9" Text="9. Emails are fine with me"></asp:ListItem>
      </asp:DropDownList>
      <%-- 5 : is used for technical break in the email list sent to the customers So please avoid 5 here as value --%>
      </td></tr>
   <tr><td align = "center" colspan="3" > <asp:Button ID="btnECommInd" OnClick="btnECommInd_Click" Width="140px"  runat="server" Text="Save Selection" /></td>
  </tr>
   </table>
    </div> 

            </td>
        </tr>
    </div></table>
      </td></tr></table>
      </div>
    <asp:Label ID="HlblSpouse" ForeColor="white" runat="server"></asp:Label>
<asp:Label ID="lblCustIndID" ForeColor="white" runat="server"></asp:Label>
<asp:Label ID="lblCustIndChapter" ForeColor="white" runat="server"></asp:Label>	
</asp:Content>


 

 
 
 