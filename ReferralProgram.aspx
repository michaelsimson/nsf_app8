﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ReferralProgram.aspx.cs" Inherits="ReferralProgram" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function ConfirmDelete() {
                if (confirm("Are you sure you want to delete?")) {
                    document.getElementById('<%= btnConfirmDelete.ClientID%>').click();
                }
            }


            function PopupPicker() {
                var PopupWindow = null;
                settings = 'width=600,height=550,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=yes,resizable=no,dependent=no';
                PopupWindow = window.open('ReferralProgramHelp.html', 'Referral Help', settings);
                PopupWindow.focus();
            }

        </script>
    </div>
    <asp:Button ID="btnConfirmDelete" Style="display: none;" runat="server" OnClick="btnConfirmDelete_Click" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/UserFunctions.aspx" runat="server">Back to Parent Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Referral Program

                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div style="float: right;">
        <a href="javascript:PopupPicker();">About the Program</a>
    </div>
    <div style="clear: both; margin-bottom: 5px;"></div>
    <div align="center" style="margin-left: auto; margin-right: auto; width: 900px;">
        <table cellpadding="2" style="width: 900px;">
            <tr>
                <td style="font-weight: bold;">Select</td>
                <td>

                    <asp:DropDownList ID="DDLSelect" runat="server" Style="width: 250px;" AutoPostBack="true">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Persons I Recommended to join NSF</asp:ListItem>
                        <asp:ListItem Value="2">The Person Who referred NSF to me</asp:ListItem>
                    </asp:DropDownList>

                </td>
                <td style="font-weight: bold;">Enter Email Address</td>
                <td>
                    <asp:TextBox ID="txtEmail" runat="server" Style="width: 300px;"></asp:TextBox></td>
                <td style="font-weight: bold;">
                    <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" /></td>

            </tr>
        </table>

        <div style="clear: both; margin-bottom: 10px;"></div>

        <div align="center">
            <asp:Label ID="lblStatus" runat="server" ForeColor="Red"></asp:Label>
        </div>

        <div style="clear: both; margin-bottom: 15px;"></div>
        <div align="center">
            <span id="spnTable2Title" style="font-weight: bold;" runat="server" visible="true"></span>
        </div>

        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="grdReferrals" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 800px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="grdReferrals_RowCommand">
            <Columns>



                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Action
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteR" />
                        <div style="display: none;">
                            <asp:Label ID="lblRefProgID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"RefProgID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="FirstName" HeaderText="First Name"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="Chapter" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="CreateDate" HeaderText="CreateDate"></asp:BoundField>


            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>

        <div style="clear: both;"></div>
        <div align="center">
            <span id="spnStatus" runat="server" style="color: red;"></span>
        </div>

    </div>
    <input type="hidden" id="hdnRefProgID" runat="server" />
</asp:Content>
