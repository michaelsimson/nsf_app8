﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="WebExTRSReport.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="WebExTRSReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            $(document).on("click", "#ancConfName", function (e) {
                var date = $(this).attr("attr-date");
                var confName = $(this).attr("attr-topic");
                document.getElementById("<%=hdnDate.ClientID%>").value = date;
                document.getElementById("<%=hdnTopic.ClientID%>").value = confName;
                document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();

            });

            $(document).on("click", "#btnGenerateReportUsage", function (e) {
                document.getElementById('<%= BtnExportExcelUsage.ClientID%>').click();
            });
            $(document).on("click", "#btnGenerateReportAttendee", function (e) {
                document.getElementById('<%= BtnExportAttendee.ClientID%>').click();
            });
        </script>
        <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
        <asp:Button ID="BtnExportExcelUsage" Style="display: none;" runat="server" OnClick="BtnExportExcelUsage_onClick" />
        <asp:Button ID="BtnExportAttendee" Style="display: none;" runat="server" OnClick="BtnExportAttendee_onClick" />
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            WebEx Training Sessions Report
                     <br />
            <br />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <asp:Label ID="lblErrMsg" ForeColor="Red" runat="server"></asp:Label>
        </div>
        <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">
            <tr class="ContentSubTitle" align="center">
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="ddYear" runat="server" Width="75px">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="2014">2014</asp:ListItem>
                        <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                    </asp:DropDownList>

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlCoach" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="DDlCoach_SelectedIndexChanged">
                    </asp:DropDownList>
                    <br />
                    <br />
                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlProduct" runat="server" Width="75px" OnSelectedIndexChanged="DDlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <br />
                    <br />
                </td>

                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SessionNo</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlSessionNo" runat="server" Width="75px">
                    </asp:DropDownList>

                </td>

                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:TextBox ID="TxtStartDate" runat="server" Width="80px"></asp:TextBox>(MM/dd/yyyy)

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:TextBox ID="TxtEndDate" runat="server" Width="80px"></asp:TextBox>(MM/dd/yyyy)

                </td>


            </tr>
            <tr>
                <td colspan="12" align="center">
                    <asp:Button ID="BtnGenerateReport" runat="server" Text="Generate Report" OnClick="BtnGenerateReport_Click" />
                </td>
            </tr>
        </table>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" id="dvUsageReport" runat="server">
            <asp:Literal ID="LtrUsageReport" runat="server"></asp:Literal>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" id="dvAttendeeReport" runat="server">
            <asp:Literal ID="LtrAttendeeReport" runat="server"></asp:Literal>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <asp:Button ID="BtnCloseTable2" Visible="false" runat="server" Text="Close Table 2" OnClick="BtnCloseTable2_Click" />
        </div>

        <input type="hidden" id="hdnDate" runat="server" value="0" />
        <input type="hidden" id="hdnUserID" runat="server" value="0" />
        <input type="hidden" id="hdnPwd" runat="server" value="0" />
        <input type="hidden" id="hdnTopic" runat="server" value="0" />
        <input type="hidden" id="hdnIsExcel" runat="server" value="0" />
    </div>
</asp:Content>
