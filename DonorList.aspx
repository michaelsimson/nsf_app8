﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" EnableEventValidation="false" AutoEventWireup="false" CodeFile="DonorList.aspx.vb" Inherits="DonorList" title="Donor List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left" >&nbsp;&nbsp;
 <asp:LinkButton ID="NvgBtn"  CssClass="btn_02" runat="server" EnableEventValidation ="false" >Back to Volunteer Functions</asp:LinkButton>
</div>
<div align="center" >
<table cellpadding="3px" border = "0px" cellspacing = "0px">
<tr><td align="center" colspan="2" ><b>Donor List</b></td></tr>
<tr><td align="left" >
    <asp:Label ID="lblTxt" ForeColor="Blue" runat="server"></asp:Label>
    </td><td align="left" >
    <asp:DropDownList ID="ddlCoverage" Enabled="false"  runat="server" Width="150px">
        <asp:ListItem Value="5">Chapter</asp:ListItem>
        <asp:ListItem Value="4">Cluster</asp:ListItem>
        <asp:ListItem Value="3">Zone</asp:ListItem>
        <asp:ListItem Value="2">National</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left" >Type of Donor</td><td align="left" >
    <asp:DropDownList ID="ddlType" OnSelectedIndexChanged ="ddlType_SelectedIndexChanged" AutoPostBack ="true"  runat="server" Width="150px">
        <asp:ListItem Selected="True" Value="IND">Individual</asp:ListItem>
        <asp:ListItem Value="OWN">Corporations</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left" >Donor Detail</td><td align="left" >
    <asp:DropDownList ID="ddlDonorDetail" runat="server" Width="150px">
        <asp:ListItem Selected="True" Value="Family">Family Total</asp:ListItem>
        <asp:ListItem Value="Each">By Each Person</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left" >Level of Aggregation</td><td align="left" >
    <asp:DropDownList ID="ddlLevel" runat="server" Width="150px">
        <asp:ListItem Value="Detail">Transaction Detail</asp:ListItem>
        <asp:ListItem Value="Total" Selected="True" >Total of all </asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left" >Nature of Report</td><td align="left" >
    <asp:DropDownList ID="DdlReport" runat="server" Width="150px">
        <asp:ListItem Selected="True">Short</asp:ListItem>
        <asp:ListItem>Long</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align="left" >Start date</td><td align="left" >
    <asp:TextBox ID="txtStartDate" runat="server" Width="100px"></asp:TextBox>
</td></tr>
<tr><td align="left" >End Date</td><td align="left" >
    <asp:TextBox ID="txtEnddate" runat="server" Width="100px"></asp:TextBox>
</td></tr>
<tr><td align="center" colspan="2" > 
    <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" /> 
    &nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnExport" runat="server" Text="Export" Visible ="false" OnClick ="btnExport_Click" />
    </td></tr>
<tr><td align="center" colspan="2" > 
    <asp:Label ID="lblerr" runat="server" ForeColor="Red"  ></asp:Label>
    </td></tr>
</table>
 <asp:DataGrid ID="dgDonationList" HorizontalAlign="Center" BorderWidth="1px"  OnPageIndexChanged ="dgDonationList_PageIndexChanged"    runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" PageSize="50"  AllowPaging="true" >
            	<FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000" 
                        Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
            <asp:BoundColumn ItemStyle-HorizontalAlign ="center" HeaderText="MemberID" DataField="MemberID" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="FirstName" DataField="FirstName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="LastName" DataField="LastName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="PubName" DataField="PubName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="Sp_FirstName" DataField="SFirstName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="Sp_LastName" DataField="SlastName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Right" HeaderText="Amount" DataField="amount" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" DataFormatString="{0:c}"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Right" HeaderText="DonationDate" DataField="DonationDate" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" DataFormatString="{0:d}"  />                
               <%-- <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="ChapterCode" DataField="ChapterCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />--%>
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="Address" DataField="Address" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="zip" DataField="zip" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="HPhone" DataField="HPhone" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="CPhone" DataField="CPhone" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="EMail" DataField="EMail" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="City" DataField="City" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="left" HeaderText="State" DataField="State" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" />
                
            </Columns>
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
           	<PAGERSTYLE ForeColor="Brown" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PAGERSTYLE>
	
         </asp:DataGrid>
</div>
</asp:Content>

