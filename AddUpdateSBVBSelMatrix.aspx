﻿<%@ Page Language="C#"  AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="AddUpdateSBVBSelMatrix.aspx.cs" Inherits="AddUpdateSBVBSelMatrix" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
     <script language="Javascript" type="text/javascript">

         function onlyNos(e, t) {
             try {
                 if (window.event) {
                     var charCode = window.event.keyCode;
                 }
                 else if (e) {
                     var charCode = e.which;
                 }
                 else { return true; }
                 if (charCode > 31 && (charCode < 48 || charCode > 57)) {
                     return false;
                 }

                 return true;

             }
             catch (err) {
                 //  alert(err.Description);
             }
         }
         </script>


    <table align="center">
         <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                
            </td>

        </tr>
        <tr>

            <td>
                   <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> Add/Update Spelling/Vocab Selection Matrix</strong> </div>
            </td>
        </tr>
      <tr>

            <td align="right">
                
            </td>
        </tr>
       <tr>
           <td align="center">
               

                            <%--<asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />--%>
                <table width="50%" visible="true" >
                     
                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlYear" AutoPostBack="True" runat="server">
                            </asp:DropDownList>
                        </td>
                         <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"  >
                                  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Finals</asp:ListItem>
                          <asp:ListItem Value="2">Chapter</asp:ListItem>
                        
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProductGroup" AutoPostBack="True" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" >
                                <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="SB" >Spelling</asp:ListItem>
                          <asp:ListItem Value="VB">Vocabulary</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        
                        <td>Product</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True"  >
                               <%-- <asp:ListItem Value="0">Select one</asp:ListItem>
                          <asp:ListItem Value="JVB">Junior Vocabulary</asp:ListItem>
                          <asp:ListItem Value="IVB">Intermediate Vocabulary</asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>
                        
                        <td>Phase</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPhase" AutoPostBack="True" runat="server"  >
                                <%--  <asp:ListItem Value="0" >Select one</asp:ListItem>
                          <asp:ListItem Value="1" >Phase I</asp:ListItem>
                          <asp:ListItem Value="2">Phase II</asp:ListItem>--%>
                        
                            </asp:DropDownList>
                       
                        </td>
                          <td align="center">
                        
                              <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"/>
                     </td>  
                             </tr>
                       <tr><td colspan="11" align="center"><asp:Label ID="lblErr" runat="server" Font-Bold="true" ForeColor="Red" Visible="true"></asp:Label></td></tr>

                  
                  
                </table>
     
        </td>
   
         
         

     </tr>
     <tr align="center">

         <td colspan="11"><div style="width:800px; height:212px; overflow:auto;">
             
             <asp:GridView ID="gvVocabSelMatrix" runat="server" Width="80%" AutoGenerateColumns="false" OnRowDataBound="gvVocabSelMatrix_RowDataBound">
                 <Columns>
                     <asp:TemplateField>
                         <HeaderTemplate>
                             <tr>
                                  <th id="throundtype" runat="server" rowspan="2">Round Type</th>
                                  <th id="colspanning" runat="server" colspan="2">Round Count</th>
                                  <th id="thpubunpub" runat="server" rowspan="2">Pub/Unpub</th>

                                  <th id="thlevel" runat="server" rowspan="2">Level</th>
                                  <th id="thsublevel" runat="server" rowspan="2">Sub-level</th>
                                  <th id="thw1" runat="server" rowspan="2">Words1</th>
                                 <th id="thw2" runat="server" rowspan="2">Words2</th>
                                 <th id="thw3" runat="server" rowspan="2">Words3</th>
                                 <th id="thw4" runat="server" rowspan="2">Words4</th>

                             </tr>
                             <tr>
                                   
                                   <th>From</th>
                                   <th>To</th>
                                   <%--<th></th>
                                   <th></th>
                                   <th></th>
                                   <th></th>
                                   <th></th>
                                   <th></th>
                                   <th></th>--%>
                             </tr>
                         </HeaderTemplate>
                         <ItemTemplate>

                             <tr id="itemTemplateTR" runat="server">
                             <td><asp:HiddenField ID="SBVBSelMatrixID" Value='<%# Bind("SBVBSelMatrixID")%>'  runat="server" />
                                 <asp:Label ID="RoundType" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "RoundType")%>'></asp:Label>
                                 </td>
                             <%--<td id="tbRoundCount" runat="server">
                
                <asp:TextBox ID="RoundCount" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "RoundCount")%>' ></asp:TextBox>
                            </td>--%>
                             <td id="tdRoundFrom" runat="server">
                                  <asp:TextBox ID="RoundFrom" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "RoundFrom")%>' ></asp:TextBox>
                             </td >
                              <td id="tdRoundTo" runat="server">
                                  <asp:TextBox ID="RoundTo" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "RoundTo")%>' ></asp:TextBox>
                             </td>
                             <td id="tdPubUnpub" runat="server"> 
                                 <asp:DropDownList ID="ddlPubUnpub" SelectedValue='<%# Bind ("PubUnpub") %>' AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="P">P</asp:ListItem>
                          <asp:ListItem Value="U">U</asp:ListItem>
                          </asp:DropDownList>
                                 </td>
                             <td id="tdLevel" runat="server"> 
                                 <asp:DropDownList ID="ddlLevel" SelectedValue='<%# Bind ("Level") %>' AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="1" >1</asp:ListItem>
                          <asp:ListItem Value="2">2</asp:ListItem>
                          <asp:ListItem Value="3">3</asp:ListItem>
                          </asp:DropDownList>

                             </td>
                             <td id="tdSublevel" runat="server">
                                 <asp:DropDownList ID="ddlSublevel" SelectedValue='<%# Bind ("SubLevel") %>' AutoPostBack="True" runat="server"  >            
                          <asp:ListItem Value="1" >1</asp:ListItem>
                          <asp:ListItem Value="2">2</asp:ListItem>
                          </asp:DropDownList>
                                 </td>
                             <td id="tdWords1" runat="server"> 
                                 <asp:TextBox ID="Words1" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "Words1")%>'></asp:TextBox>

                             </td>
                             <td id="tdWords2" runat="server">
                                 <asp:TextBox ID="Words2" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "Words2")%>'></asp:TextBox>
                                                              </td>
                             <td id="tdWords3" runat="server">
                                 <asp:TextBox ID="Words3" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "Words3")%>'></asp:TextBox>
                             </td>
                             <td id="tdWords4" runat="server">
                                 <asp:TextBox ID="Words4" runat="server" Width="30px" onkeypress="return onlyNos(event,this);" Text='<%#DataBinder.Eval(Container.DataItem, "Words4")%>'></asp:TextBox>
                             </td>
                                 </tr>
            </ItemTemplate>
                     </asp:TemplateField>

      
                     
        
        </Columns>


             </asp:GridView>
         </div></td>

     </tr>

<tr align="center">

    <td colspan="10">
        <asp:Button ID="btnUpdate" runat="server" Visible="false" Text="Update" OnClick="btnUpdate_Click" /></td>

               

       </tr>

  
        
                </table>
      
       </asp:Content>
    