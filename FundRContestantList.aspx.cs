﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Drawing;


public partial class FundRContestantList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        LblErrMsg.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {

            fillCheckinList();
            fillProductGroup();
            fillProduct();
        }
    }

    public void fillCheckinList()
    {
        string ChapterID = string.Empty;
        if (Request.QueryString["Chap"] != null)
        {
            ChapterID = Request.QueryString["Chap"].ToString();
        }
        string CmdText = string.Empty;
        CmdText = "select EventDescription,FundRCalID from FundRaisingCal where EventID=" + ddEvent.SelectedValue + " and EventYear=" + DDlYear.SelectedValue + " and ChapterID=" + ChapterID + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            DDlCheckInList.DataValueField = "FundRCalID";
            DDlCheckInList.DataTextField = "EventDescription";
            DDlCheckInList.DataSource = ds;

            DDlCheckInList.DataBind();
            DDlCheckInList.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                DDlCheckInList.Enabled = true;
            }
            else
            {
                DDlCheckInList.Enabled = false;
                DDlCheckInList.SelectedIndex = 1;
            }
        }
    }
    public void fillProductGroup()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        try
        {
            int year = DateTime.Now.Year;
            cmdText = "select distinct FC.ProductGroupID,PG.Name from FundRContestReg FC inner join ProductGroup PG on (FC.ProductGroupID=PG.ProductGroupID) where FC.EventId=" + ddEvent.SelectedItem.Value + " and FC.EventYear=" + year + "";

            //cmdText = "select ProductGroupID,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedValue + "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "Name";
            ddlProductGroup.DataValueField = "ProductGroupID";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("All", "All"));
        }
        catch (Exception ex)
        {
        }
    }
    public void fillProduct()
    {
        string cmdText = "";

        DataSet ds = new DataSet();
        try
        {
            if (ddlProductGroup.SelectedValue == "All")
            {
                ddlProduct.Items.Clear();
                ddlProduct.Items.Insert(0, new ListItem("All", "All"));
                ddlProduct.Enabled = false;
            }
            else
            {
                ddlProduct.Items.Clear();
                int year = DateTime.Now.Year;
                //year = 2014;
                cmdText = "select distinct FC.ProductID,P.Name from FundRContestReg FC inner join Product P on (FC.ProductID=P.ProductID) where FC.ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and FC.EventYear=" + year + " and FC.EventID=9";

                //cmdText = "select ProductID,ProductCode from Product where ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "Name";
                ddlProduct.DataValueField = "ProductID";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("All", "All"));
                ddlProduct.Enabled = true;
            }
        }
        catch (Exception ex)
        {
        }
    }
    public string validateCheckinList()
    {
        string RetVal = "1";
        if (DDlYear.SelectedValue == "0")
        {
            RetVal = "-1";
            LblErrMsg.Text = "Please select Year";
        }

        else if (DDlCheckInList.SelectedValue == "0")
        {
            RetVal = "-1";
            LblErrMsg.Text = "Please select Check-in List";
        }
        return RetVal;
    }

    protected void btnsubmit_Click(object sender, EventArgs e)
    {
        if (validateCheckinList() == "1")
        {
            LoadContestantList();
        }
    }

    public void LoadContestantList()
    {
        try
        {
            string CmdText = "";
            DataSet ds = new DataSet();
            string ChapterID = string.Empty;
            if (Request.QueryString["Chap"] != null)
            {
                ChapterID = Request.QueryString["Chap"].ToString();
            }
            CmdText = "select (select case when(select COUNT(*) as Count from Contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' else null end) as Prior,FCR.MemberID, FCR.ProductID,FCR.ProductGroupID, FCR.ProductCode,IP.Hphone,IP.CPhone,FCR.Approved,FCR.PaymentReference,C.First_Name,C.Last_Name,C.ChildNumber,IP.Email,IP.CPhone,IP.HPhone,IP.Address1,IP.State,IP.City,IP.Zip,IP.FirstName,IP.LastName,FCR.Fee from FundRContestReg FCR inner join IndSpouse IP on (FCR.MemberID=IP.AutoMemberID) inner join Child C on (FCR.ChildNumber=C.ChildNumber) where FCR.EventYear=" + DDlYear.SelectedValue + " and FCR.EventID=" + ddEvent.SelectedValue + " and FCR.FundRCalID=" + DDlCheckInList.SelectedValue + " and  FCR.ChapterID=" + ChapterID + "  ";

            if (DDLListType.SelectedValue == "Paid")
            {
                CmdText += " And FCR.PaymentReference is not null and FCR.Approved='Y'";
            }
            else if (DDLListType.SelectedValue == "Pending")
            {
                CmdText += " And FCR.PaymentReference is null and FCR.Approved is null";
            }

            if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
            {
                CmdText += " And FCR.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
            {
                CmdText += " And FCR.ProductGroupID=" + ddlProductGroup.SelectedValue + " And FCR.ProductID=" + ddlProduct.SelectedValue + "";
            }
            CmdText += " order by C.Last_Name, C.First_Name ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dvExcel.Visible = true;

                    string MemberID = string.Empty;
                    int count = 0;
                    string tblHtml = string.Empty;

                    tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1300px; border:1px solid black; border-collapse:collapse; font-family:verdana,arial;'>";

                    tblHtml += "<tr style='color:Green;background-color:#FFFFCC;font-weight:bold; font-family:verdana,arial;'>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ser#</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child Number</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Prior</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ch FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ch LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Status</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Fees</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Count</td>";
                    if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                    {
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Spelling</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Math</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Science</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Geography</td>";
                    }
                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                    {
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contest</td>";
                    }
                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                    {

                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>" + ddlProduct.SelectedItem.Text + "</td>";


                    }
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>MemberID</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Email</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>HPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>CPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Address</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>City</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>ST</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Zip</td>";
                    tblHtml += "</tr>";

                    string ChildNumber = string.Empty;
                    int totalCount = 0;
                    int FeesCount = 0;
                    int SBCount = 0;
                    int MathCount = 0;
                    int GGCount = 0;
                    int SCCount = 0;
                    int FeesAmount = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        if (ChildNumber != dr["ChildNumber"].ToString())
                        {
                            string Status = string.Empty;
                            string PChildNumber = dr["ChildNumber"].ToString();
                            int ProductCount = 0;
                            string ProductName = string.Empty;
                            string ProductList = string.Empty;
                            string SPPrdName = string.Empty;
                            string MathPrdName = string.Empty;
                            string SCPrdName = string.Empty;
                            string GGPrdName = string.Empty;
                            count++;
                            tblHtml += "<tr>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + count + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["ChildNumber"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Prior"].ToString() + "</td>";
                                                      
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["First_Name"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Last_Name"].ToString() + "</td>";
                            if (dr["Approved"].ToString() == "Y")
                            {
                                Status = "Paid";
                            }
                            else
                            {
                                Status = "Pending";
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Status + "</td>";

                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Fee"].ToString() + "</td>";
                            FeesCount = FeesCount + Convert.ToInt32(dr["Fee"].ToString());
                            foreach (DataRow dr2 in ds.Tables[0].Rows)
                            {
                                if (dr2["ChildNumber"].ToString() == PChildNumber)
                                {
                                    totalCount++;
                                    ProductCount++;
                                    if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                                    {
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            SPPrdName += " " + dr2["ProductCode"].ToString() + ",";
                                            SBCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            MathPrdName += " " + dr2["ProductCode"].ToString() + ",";
                                            MathCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            GGPrdName += " " + dr2["ProductCode"].ToString() + ",";
                                            GGCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            SCPrdName += " " + dr2["ProductCode"].ToString() + ",";
                                            SCCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }

                                    }
                                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                                    {
                                        ProductList += " " + dr2["ProductCode"].ToString() + ",";
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SBCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            MathCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            GGCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SCCount++;
                                        }
                                    }
                                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                                    {
                                        ProductName = dr2["ProductCode"].ToString();
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SBCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            MathCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            GGCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SCCount++;
                                        }
                                    }
                                }
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductCount + "</td>";

                            if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                            {
                               
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + SPPrdName.TrimEnd(',') + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + MathPrdName.TrimEnd(',') + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + SCPrdName.TrimEnd(',') + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + GGPrdName.TrimEnd(',') + "</td>";
                            }
                            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                            {
                                ProductList.TrimEnd(',');
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductList.TrimEnd(',') + "</td>";
                            }
                            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                            {

                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductName + "</td>";
                            }

                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["MemberID"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["FirstName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["LastName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Email"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["HPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["CPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Address1"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["City"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["State"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Zip"].ToString() + "</td>";
                        }
                        ChildNumber = dr["ChildNumber"].ToString();
                    }
                    tblHtml += "<tr>";
                    tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'><b>Total</b></td>";
                    tblHtml += "<td Colspan='21'>";
                    tblHtml += "<div style='font-weight:bold; float:left; width:150px;'> Total Count = " + totalCount + "</div>";
                    tblHtml += "       <div style='float:left; width:150px; font-weight:bold;'>Total Fees = " + FeesAmount + "</div>";
                    tblHtml += "       <div style='float:left; width:150px; font-weight:bold;'>SB = " + SBCount + "</div>";
                    tblHtml += "       <div style='float:left; width:150px; font-weight:bold;'>MB = " + MathCount + "</div>";
                    tblHtml += "       <div style='float:left; width:150px; font-weight:bold;'>SC = " + SCCount + "</div>";
                    tblHtml += "       <div style='float:left; width:150px; font-weight:bold;'>GB = " + GGCount + "</div>";


                    tblHtml += "</td>";
                    tblHtml += "</tr>";
                    tblHtml += "</table>";

                    LtrContestantList.Text = tblHtml;

                }
                else
                {
                    
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public void ExortToExcel()
    {
        try
        {

            string CmdText = "";
            DataSet ds = new DataSet();
            string ChapterID = string.Empty;
            if (Request.QueryString["Chap"] != null)
            {
                ChapterID = Request.QueryString["Chap"].ToString();
            }
            CmdText = "select (select case when(select COUNT(*) as Count from Contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' else null end) as Prior,FCR.MemberID, FCR.ProductID,FCR.ProductGroupID, FCR.ProductCode,IP.Hphone,IP.CPhone,FCR.Approved,FCR.PaymentReference,C.First_Name,C.Last_Name,C.ChildNumber,IP.Email,IP.CPhone,IP.HPhone,IP.Address1,IP.State,IP.City,IP.Zip,IP.FirstName,IP.LastName,FCR.Fee from FundRContestReg FCR inner join IndSpouse IP on (FCR.MemberID=IP.AutoMemberID) inner join Child C on (FCR.ChildNumber=C.ChildNumber) where FCR.EventYear=" + DDlYear.SelectedValue + " and FCR.EventID=" + ddEvent.SelectedValue + " and FCR.FundRCalID=" + DDlCheckInList.SelectedValue + " and  FCR.ChapterID=" + ChapterID + "  ";

            if (DDLListType.SelectedValue == "Paid")
            {
                CmdText += " And FCR.PaymentReference is not null and FCR.Approved='Y'";
            }
            else if (DDLListType.SelectedValue == "Pending")
            {
                CmdText += " And FCR.PaymentReference is null and FCR.Approved is null";
            }

            if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
            {
                CmdText += " And FCR.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
            {
                CmdText += " And FCR.ProductGroupID=" + ddlProductGroup.SelectedValue + " And FCR.ProductID=" + ddlProduct.SelectedValue + "";
            }
            CmdText += " order by IP.LastName, IP.FirstName ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            string tblHtml = string.Empty;
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    dvExcel.Visible = true;

                    string MemberID = string.Empty;
                    int count = 0;


                    tblHtml += "<table style='background-color:White;border-color:#E7E7FF;border-width:1px;border-style:None;border-collapse:collapse; width:1300px; border:1px solid black; border-collapse:collapse; font-family:verdana,arial;'>";

                    tblHtml += "<tr style='color:Green;background-color:#FFFFCC;font-weight:bold; font-family:verdana,arial;'>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ser#</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Child Number</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Prior</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ch FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Ch LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Status</td>";

                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Fees</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Count</td>";
                    if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                    {
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Spelling</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Math</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Science</td>";
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Geography</td>";
                    }
                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                    {
                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Contest</td>";
                    }
                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                    {

                        tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>" + ddlProduct.SelectedItem.Text + "</td>";


                    }
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>MemberID</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>FirstName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>LastName</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Email</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>HPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>CPhone</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Address</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>City</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>ST</td>";
                    tblHtml += "<td style='color:Green; border:1px solid black; border-collapse:collapse; font-family:verdana,arial; font-size:7pt;'>Zip</td>";
                    tblHtml += "</tr>";

                    string ChildNumber = string.Empty;
                    int totalCount = 0;
                    int FeesCount = 0;
                    int SBCount = 0;
                    int MathCount = 0;
                    int GGCount = 0;
                    int SCCount = 0;
                    int FeesAmount = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        if (ChildNumber != dr["ChildNumber"].ToString())
                        {
                            string Status = string.Empty;
                            string PChildNumber = dr["ChildNumber"].ToString();
                            int ProductCount = 0;
                            string ProductName = string.Empty;
                            string ProductList = string.Empty;
                            string SPPrdName = string.Empty;
                            string MathPrdName = string.Empty;
                            string SCPrdName = string.Empty;
                            string GGPrdName = string.Empty;
                            count++;
                            tblHtml += "<tr>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + count + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["ChildNumber"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Prior"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["First_Name"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Last_Name"].ToString() + "</td>";
                            if (dr["Approved"].ToString() == "Y")
                            {
                                Status = "Paid";
                            }
                            else
                            {
                                Status = "Pending";
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + Status + "</td>";

                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Fee"].ToString() + "</td>";
                            FeesCount = FeesCount + Convert.ToInt32(dr["Fee"].ToString());
                            foreach (DataRow dr2 in ds.Tables[0].Rows)
                            {
                                if (dr2["ChildNumber"].ToString() == PChildNumber)
                                {
                                    totalCount++;
                                    ProductCount++;
                                    if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                                    {
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            SPPrdName += dr2["ProductCode"].ToString() + ", ";
                                            SBCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            MathPrdName += dr2["ProductCode"].ToString() + ", ";
                                            MathCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            GGPrdName += dr2["ProductCode"].ToString() + ", ";
                                            GGCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            SCPrdName += dr2["ProductCode"].ToString() + ", ";
                                            SCCount++;
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                        }

                                    }
                                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                                    {
                                        ProductList += dr2["ProductCode"].ToString() + ", ";
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SBCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            MathCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            GGCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SCCount++;
                                        }
                                    }
                                    else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                                    {
                                        ProductName = dr2["ProductCode"].ToString();
                                        if (dr2["ProductGroupID"].ToString() == "8")
                                        {

                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SBCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "10")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            MathCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "11")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            GGCount++;
                                        }
                                        else if (dr2["ProductGroupID"].ToString() == "26")
                                        {
                                            FeesAmount = FeesAmount + Convert.ToInt32(dr2["Fee"].ToString());
                                            SCCount++;
                                        }
                                    }
                                }
                            }
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductCount + "</td>";

                            if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
                            {
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + SPPrdName + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + MathPrdName + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + SCPrdName + "</td>";
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + GGPrdName + "</td>";
                            }
                            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue == "All")
                            {
                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductList + "</td>";
                            }
                            else if (ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
                            {

                                tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + ProductName + "</td>";
                            }

                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["MemberID"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["FirstName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["LastName"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Email"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["HPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["CPhone"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Address1"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["City"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["State"].ToString() + "</td>";
                            tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'>" + dr["Zip"].ToString() + "</td>";
                        }
                        ChildNumber = dr["ChildNumber"].ToString();
                    }
                    tblHtml += "<tr>";
                    tblHtml += "<td style='border:1px solid black; border-collapse:collapse;'><b>Total</b></td>";
                    tblHtml += "<td Colspan='21'>Total Count = " + totalCount + "  Total Fees = " + FeesAmount + "  SB = " + SBCount + "  MB = " + MathCount + "  SC = " + SCCount + "  GB = " + GGCount + "</td>";

                    tblHtml += "</tr>";
                    tblHtml += "</table>";


                }
            }
            DateTime dt = DateTime.Now;
            string month = dt.ToString("MMM");
            string day = dt.ToString("dd");
            string year = dt.ToString("yyyy");
            string monthDay = month + "" + day;
            string filename = "ContestantList_" + monthDay + "_" + year + ".xls";
            Response.Clear();
            Response.AddHeader("content-disposition", "attachment;filename=" + filename + "");
            Response.Charset = "";

            Response.ContentType = "application/vnd.xls";
            StringWriter stringWrite = new StringWriter();
            HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);
            Response.Write("<br>Check-in List<br>");
            Response.Write(tblHtml.ToString());
            Response.End();
        }
        catch (Exception ex)
        {
        }



    }
    protected void BtnExcel_Click(object sender, EventArgs e)
    {
        ExortToExcel();
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
    }
}