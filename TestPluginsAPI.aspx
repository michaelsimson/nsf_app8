﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="TestPluginsAPI.aspx.cs" Inherits="TestPluginsAPI" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js"></script>
    <link href="css/Confirm/jquery.confirm.css" rel="stylesheet" />

    <script src="css/Confirm/jquery.confirm.js"></script>
    <script src="Scripts/jquery.toastmessage.js"></script>
    <link href="css/jquery.toastmessage.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {

            $().toastmessage('showToast', {
                text: 'Hello World',
                sticky: true,
                position: 'top-right',
                type: 'success',
                close: function () { console.log("toast is closed ..."); }
            });
        });
    </script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="page">

            <div class="item">

                <div class="delete"></div>
            </div>

            <!-- Other "item" divs -->

        </div>

        
    </form>
</body>
</html>
