Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data
Partial Class GenerateParticipantCertificates
    Inherits System.Web.UI.Page
    Dim TCFlag As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim badge As New Badge()
            '/*******************Added on 02-16-2015 to generate certificates for current year and previous year.******************************/
            ddlEventYear.Items.Insert(0, New ListItem(Now.Year, Now.Year))
            ddlEventYear.Items.Insert(1, New ListItem(Now.Year - 1, Now.Year - 1))
            ddlEventYear.SelectedIndex = 0
            Session("Event_Year") = ddlEventYear.SelectedValue

            If Session("RoleID") = 1 Or Session("RoleID") = 2 Then
                TrEventYear.Visible = True
                ddlEventYear.Enabled = True
                If Session("Page") = "ManageScoresheet.aspx" Then
                    Session("Event_Year") = Session("EventYear")
                Else
                    Session("Event_Year") = ddlEventYear.SelectedValue
                End If
            Else
                TrEventYear.Visible = False
                ddlEventYear.Enabled = False
            End If
            ddlEventYear.SelectedValue = Session("Event_Year")

            '/******************************************************************************************************************************************************************/
            Dim dsContestDates As New DataSet
            dsContestDates = badge.GetContestDates(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year")) 'ddlEventYear.SelectedValue)
            If Session("Page") = "ManageScoresheet.aspx" Then
                dsContestDates = badge.GetContestDates_Score(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Session("Event_Year"), Session("ContestID"))
            End If
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                lblChapter.Text = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If

            lblChapter.Font.Bold = True
            If dsContestDates.Tables(0).Rows.Count > 0 Then
                pnlData.Visible = True
                lstContestDates.Visible = True
                lstContestDates.DataTextField = "ContestDate"
                lstContestDates.DataValueField = "ContestDate"
                lstContestDates.DataSource = dsContestDates
                lstContestDates.DataBind()
                'lstContestDates.Items.Insert(0, New ListItem("Select", "Select"))
                lstContestDates.SelectedIndex = 0
                CheckTCSchedule()
                pnlMessage.Visible = False
            Else
                pnlData.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Contest Dates found."
                lstContestDates.Visible = False
                btnGenerate.Enabled = False
                btnExport.Enabled = False
                btnExportPDF.Enabled = False
            End If
            Dim dsSignatures As New DataSet
            dsSignatures = badge.GetAllSignatures(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Convert.ToInt32(ddlEventYear.SelectedValue))
            If dsSignatures.Tables(0).Rows.Count > 0 Then
                dgSignatures.DataSource = dsSignatures
                dgSignatures.DataBind()
            Else
                dgSignatures.Visible = False
                pnlMessage.Visible = True
                lblMessage.Text = "No Signature names."
                btnGenerate.Enabled = False
                btnExport.Enabled = False
                btnExportPDF.Enabled = False
            End If
            CheckBadgenumbers()
            Session("LeftSignatureName") = Nothing
            Session("RightSignatureName") = Nothing
            Session("LeftSignatureTitle") = Nothing
            Session("RightSignatureTitle") = Nothing
            Session("LeftTitle") = Nothing
            Session("RightTitle") = Nothing
            Session("LeftSignatureImage") = Nothing
            Session("LeftSignatureImage") = Nothing
            setRange()
            If Session("Page") = "ManageScoresheet.aspx" Then
                lblPage.Text = "(ScoreSheet RankCertificates)"
                ddlCertificate.SelectedValue = "Rank_Fill"
                ddlCertificate.Enabled = False
                trrange.Visible = False
                'Server.Transfer("ShowRankCertificatesPhase3.aspx")
                For i As Integer = 0 To dsSignatures.Tables(0).Rows.Count - 1
                    If dsSignatures.Tables(0).Rows(i)("ProductCode") = Session("ProductCode") Then
                        dgSignatures.SelectedIndex = i
                    Else
                        dgSignatures.Items.Item(i).Visible = False
                    End If
                Next
            Else
                lblPage.Text = "(CC-Contests)"
            End If
        End If
        If Convert.ToInt32(Session("SelChapterID")) = 1 Then
            btnGenerate.Enabled = True
            btnExport.Enabled = True
            btnExportPDF.Enabled = True
        End If

    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Session("Event_Year") = ddlEventYear.SelectedValue

        Dim badge As New Badge()
        Dim dsContestDates As New DataSet
        dsContestDates = badge.GetContestDates(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), ddlEventYear.SelectedValue)
        If Session("Page") = "ManageScoresheet.aspx" Then
            dsContestDates = badge.GetContestDates_Score(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), ddlEventYear.SelectedValue, Session("ContestID"))
        End If
        If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
            lblChapter.Text = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
        End If

        lblChapter.Font.Bold = True
        If dsContestDates.Tables(0).Rows.Count > 0 Then
            pnlData.Visible = True
            lstContestDates.Visible = True
            lstContestDates.DataTextField = "ContestDate"
            lstContestDates.DataValueField = "ContestDate"
            lstContestDates.DataSource = dsContestDates
            lstContestDates.DataBind()
            'lstContestDates.Items.Insert(0, New ListItem("Select", "Select"))
            lstContestDates.SelectedIndex = 0
            CheckTCSchedule()
            pnlMessage.Visible = False
        Else
            pnlData.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Contest Dates found."
            lstContestDates.Visible = False
            btnGenerate.Enabled = False
            btnExport.Enabled = False
            btnExportPDF.Enabled = False
        End If
        Dim dsSignatures As New DataSet
        dsSignatures = badge.GetAllSignatures(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Convert.ToInt32(ddlEventYear.SelectedValue))
        If dsSignatures.Tables(0).Rows.Count > 0 Then
            dgSignatures.DataSource = dsSignatures
            dgSignatures.DataBind()
        Else
            dgSignatures.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Signature names."
            btnGenerate.Enabled = False
            btnExport.Enabled = False
            btnExportPDF.Enabled = False
        End If
        CheckBadgenumbers()
        Session("LeftSignatureName") = Nothing
        Session("RightSignatureName") = Nothing
        Session("LeftSignatureTitle") = Nothing
        Session("RightSignatureTitle") = Nothing
        Session("LeftTitle") = Nothing
        Session("RightTitle") = Nothing
        Session("LeftSignatureImage") = Nothing
        Session("LeftSignatureImage") = Nothing
        setRange()
        If Session("Page") = "ManageScoresheet.aspx" Then
            lblPage.Text = "(ScoreSheet RankCertificates)"
            ddlCertificate.SelectedValue = "Rank_Fill"
            ddlCertificate.Enabled = False
            trrange.Visible = False
            'Server.Transfer("ShowRankCertificatesPhase3.aspx")
            For i As Integer = 0 To dsSignatures.Tables(0).Rows.Count - 1
                If dsSignatures.Tables(0).Rows(i)("ProductCode") = Session("ProductCode") Then
                    dgSignatures.SelectedIndex = i
                Else
                    dgSignatures.Items.Item(i).Visible = False
                End If
            Next
        Else
            lblPage.Text = "(CC-Contests)"
        End If

        If Convert.ToInt32(Session("SelChapterID")) = 1 Then
            btnGenerate.Enabled = True
            btnExport.Enabled = True
            btnExportPDF.Enabled = True
        End If
    End Sub

    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        Try
            'trrange.Visible = True

            If ddlCertificate.SelectedValue = "Rank_Blank" Or ddlCertificate.SelectedValue = "Rank_Fill" Then
                trrange.Visible = False
                If Session("Page") = "ManageScoresheet.aspx" Then ' Session("Page") =
                    Server.Transfer("ShowRankCertificatesPhase3New.aspx")
                ElseIf ddlCertificate.SelectedValue = "Rank_Fill" Then
                    LoadRanks()
                    Server.Transfer("ShowRankCertificatesPhase3New.aspx")
                Else
                    Server.Transfer("ShowRankCertificateNew.aspx")
                End If
            Else
                If ddlCertificate.SelectedValue = "Participant" Then
                    If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                    Server.Transfer("ShowParticipantCertificatePDF.aspx")
                ElseIf ddlCertificate.SelectedValue = "Volunteer-Blank" Then
                    If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                    If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                        pnlMessage.Visible = True
                        lblMessage.Text = "Select Valid Signature Name"
                    Else
                        Server.Transfer("ShowVolunteerCertificateBlank.aspx")
                    End If
                Else
                    If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                        pnlMessage.Visible = True
                        lblMessage.Text = "Select Valid Signature Name"
                    Else
                        Server.Transfer("ShowVolunteerCertificateNewPDF.aspx")
                    End If
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Public Sub LoadRanks()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select Distinct EventId,ContestID,ProductGroupCode from Contest where Contest_Year=" & ddlEventYear.SelectedValue() & " and ProductCode ='" & Session("ProductCode") & "' and NSFChapterId=" & Session("SelChapterID"))
        Dim ProductGroup As String = ""
        If ds.Tables(0).Rows.Count > 0 Then
            Session("ContestID") = ds.Tables(0).Rows(0)("ContestID")
            Session("EventId") = ds.Tables(0).Rows(0)("EventId")
            ProductGroup = ds.Tables(0).Rows(0)("ProductGroupCode")
            '**************************************************************************************************'
            '*********************Rank Certificates generated based on rules***********************************'
            '***For Regionals,EventID=2 ,Number of ranks is determined by the #of paid registrations.**********' 
            '***********a) No rank certificates if the paid registrations is less than 5.**********************'
            '***********b) Rank 1 only if the paid registrations are 5, 6, 7***********************************'
            '***********c) Rank 1 and Rank 2 if the paid registrations are 8, 9********************************'
            '***********d) All three ranks if the paid registrations are 10 or more ***************************'
            '**For Finals (eventid = 1 and chapterid=1), use the following rules:******************************'
            '************i) Rank 1 through 10 for product groups SB, VB, GB, MB, SC, BB and Count>40***********'
            '************ii) Ranks 1, 2, 3 for EW and PS and Count<40******************************************'
            '**************************************************************************************************'
            Dim Conts_Cnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM ScoreDetail WHERE EventID =" & Session("EventId") & " and ChapterID= " & Session("SelChapterID") & " and ContestYear=" & ddlEventYear.SelectedValue() & " and ContestID=" & Session("ContestID") & " and Case when AttendanceFlag IS NULL  Then '' Else AttendanceFlag End not like '%N%'")
            Dim Rank_Cnt As Integer = 0
            If Session("EventId") = 2 Then
                If Conts_Cnt < 5 Then
                    Rank_Cnt = 0
                ElseIf Conts_Cnt >= 5 And Conts_Cnt <= 7 Then
                    Rank_Cnt = 1
                ElseIf Conts_Cnt > 7 And Conts_Cnt < 10 Then
                    Rank_Cnt = 2
                ElseIf Conts_Cnt >= 10 Then
                    Rank_Cnt = 3
                End If
            End If
            If Session("EventId") = 1 Then
                If Conts_Cnt >= 40 And (ProductGroup = "SB" Or ProductGroup = "VB" Or ProductGroup = "GB" Or ProductGroup = "MB" Or ProductGroup = "SC" Or ProductGroup = "BB") Then ' 
                    Rank_Cnt = 10
                ElseIf Conts_Cnt < 40 And (ProductGroup = "EW" Or ProductGroup = "PS") Then '
                    Rank_Cnt = 3
                End If
            End If
            Session("TopRank") = Rank_Cnt
        End If
    End Sub
    Public ReadOnly Property ContestDates() As String
        Get
            Return GetSelectedContestDates()
        End Get
    End Property
    Private Function GetSelectedContestDates() As String
        Dim strSelectedContestDates As String
        Dim i As Integer = 0
        For i = 0 To lstContestDates.Items.Count - 1
            If lstContestDates.Items(i).Selected = True Then
                If Len(strSelectedContestDates) > 0 Then
                    strSelectedContestDates = strSelectedContestDates & ",'" & lstContestDates.Items(i).Text & "'"
                Else
                    strSelectedContestDates = "'" & lstContestDates.Items(i).Text & "'"
                End If
            End If
        Next
        Return strSelectedContestDates
    End Function
    Private Sub CheckTCSchedule()
        '************No Certificates will be generated if TCs are not Scheduled for any of the Cotnests in the selected chapter****************/
        Try
            lblError.Text = ""
            Dim ContestDates As String = GetSelectedContestDates()
            If lstContestDates.Items.Count > 0 Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from Contest where Contest_Year=" & ddlEventYear.SelectedValue() & " and nsfChapterId=" & Session("SelChapterID") & " and contestDate in (" & ContestDates & ") and ExamRecId is null") > 0 Then
                    'TCFlag = False
                    btnGenerate.Enabled = False
                    btnExport.Enabled = False
                    btnExportPDF.Enabled = False
                    lblError.Text = "Technical Co-Ordinators are not scheduled for all contests.<br />Certificates cannot be generated."
                    Exit Sub
                Else
                    lblError.Text = ""
                    TCFlag = True
                    btnGenerate.Enabled = True
                    btnExport.Enabled = True
                    btnExportPDF.Enabled = True
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub CheckBadgenumbers()
        Try
            lblErrorBdg.Text = ""
            Dim ContestDates As String = GetSelectedContestDates()
            If ddlCertificate.SelectedValue <> "Volunteer_Fill" And ddlCertificate.SelectedValue <> "Volunteer-Blank" Then
                If lstContestDates.Items.Count > 0 Then
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From contestant Cn inner join Contest C on C.ContestId=Cn.contestCode and C.Contest_year=Cn.ContestYear and C.NsfChapterId=Cn.ChapterID where Cn.ContestYear=" & ddlEventYear.SelectedValue() & " and Cn.ChapterID = " & Convert.ToInt32(Session("SelChapterID")) & " and Cn.BadgeNumber is null and Cn.PaymentReference is not null and C.ContestDate in (" & ContestDates & ")") > 0 Then
                        'Select COUNT(*) From contestant where ContestYear=" & ddlEventYear.SelectedValue() & " and ChapterID =" & Convert.ToInt32(Session("SelChapterID")) & " and BadgeNumber is null and PaymentReference is not null") > 0 Then
                        lblErrorBdg.Text = "Badge numbers were not generated for all Contests. Please do this first."
                        btnGenerate.Enabled = False
                        btnExport.Enabled = False
                        btnExportPDF.Enabled = False
                        Exit Sub
                    ElseIf TCFlag = True Then
                        lblErrorBdg.Text = ""
                        btnGenerate.Enabled = True
                        btnExport.Enabled = True
                        btnExportPDF.Enabled = True
                    Else
                        If lblError.Text <> "" Then
                            lblErrorBdg.Text = lblError.Text
                            lblError.Text = ""
                        End If
                    End If
                Else
                    lblErrorBdg.Text = "No Contest dates Present"
                End If
            Else
                lblErrorBdg.Text = ""
                If TCFlag = True Then
                    lblError.Text = ""
                    btnGenerate.Enabled = True
                    btnExport.Enabled = True
                    btnExportPDF.Enabled = True
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub lstContestDates_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        CheckTCSchedule()
        CheckBadgenumbers()
    End Sub

    Protected Sub dgSignatures_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgSignatures.SelectedIndexChanged
        lblError.Text = ""
        lblMessage.Text = ""
        If ddlCertificate.SelectedValue <> "Volunteer_Fill" And ddlCertificate.SelectedValue <> "Volunteer-Blank" Then
            CheckBadgenumbers()
        Else
            lblError.Text = ""
            btnGenerate.Enabled = True
            btnExport.Enabled = True
            btnExportPDF.Enabled = True
        End If
        Session("LeftSignatureName") = dgSignatures.SelectedItem.Cells(3).Text & " " & dgSignatures.SelectedItem.Cells(4).Text
        Session("LeftSignatureTitle") = dgSignatures.SelectedItem.Cells(5).Text
        Session("LeftTitle") = dgSignatures.SelectedItem.Cells(2).Text
        Session("RightSignatureName") = dgSignatures.SelectedItem.Cells(7).Text & " " & dgSignatures.SelectedItem.Cells(8).Text
        Session("RightSignatureTitle") = dgSignatures.SelectedItem.Cells(9).Text
        Session("RightTitle") = dgSignatures.SelectedItem.Cells(6).Text
        If Convert.ToInt32(Session("SelChapterID")) = 1 Then
            Session("LeftSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(10).Text
            Session("RightSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(11).Text
        Else
            'Session("LeftSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            'Session("RightSignatureImage") = "http://www.northsouth.org/app8/Images/signs/white.jpg"
            If dgSignatures.SelectedItem.Cells(10).Text = "'" Then
                Session("LeftSignatureImage") = "http://www.northsouth.org/app8/Images/white.jpg"
            Else
                Session("LeftSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(10).Text
            End If
            If dgSignatures.SelectedItem.Cells(11).Text = "'" Then
                Session("RightSignatureImage") = "http://www.northsouth.org/app8/Images/white.jpg"
            Else
                Session("RightSignatureImage") = "http://www.northsouth.org/signs/" & dgSignatures.SelectedItem.Cells(11).Text
            End If
        End If

        Session("ProductCode") = dgSignatures.SelectedItem.Cells(12).Text
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Me.Export = True
        'trrange.Visible = True
        If ddlCertificate.SelectedValue = "Rank_Blank" Or ddlCertificate.SelectedValue = "Rank_Fill" Then
            trrange.Visible = False
            If Session("Page") = "ManageScoresheet.aspx" Then
                Server.Transfer("ShowRankCertificatesPhase3New.aspx")
            ElseIf ddlCertificate.SelectedValue = "Rank_Fill" Then
                LoadRanks()
                Server.Transfer("ShowRankCertificatesPhase3New.aspx")
            Else
                Server.Transfer("ShowRankCertificateNew.aspx")
            End If

        Else
            If ddlCertificate.SelectedValue = "Participant" Then
                If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                Server.Transfer("ShowParticipantCertificateNew.aspx")
            ElseIf ddlCertificate.SelectedValue = "Volunteer-Blank" Then
                If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                    pnlMessage.Visible = True
                    lblMessage.Text = "Select Valid Signature Name"
                Else
                    Server.Transfer("ShowVolunteerCertificateBlank.aspx")
                End If
            Else
                If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                    pnlMessage.Visible = True
                    lblMessage.Text = "Select Valid Signature Name"
                Else
                    Server.Transfer("ShowVolunteerCertificateNew.aspx")
                End If
            End If
        End If

    End Sub
    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPDF.Click
        Me.Export = True
        ' trrange.Visible = True
        If ddlCertificate.SelectedValue = "Rank_Blank" Or ddlCertificate.SelectedValue = "Rank_Fill" Then
            trrange.Visible = False
            If Session("Page") = "ManageScoresheet.aspx" Then
                Server.Transfer("ShowRankCertificatesPhase3NewPDF.aspx")
            ElseIf ddlCertificate.SelectedValue = "Rank_Fill" Then
                LoadRanks()
                Server.Transfer("ShowRankCertificatesPhase3NewPDF.aspx")
            Else
                Server.Transfer("ShowRankCertificatePDF.aspx")
            End If


        Else
            If ddlCertificate.SelectedValue = "Participant" Then
                If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                Server.Transfer("ShowParticipantCertificatePDF.aspx")
            ElseIf ddlCertificate.SelectedValue = "Volunteer-Blank" Then
                If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
                If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                    pnlMessage.Visible = True
                    lblMessage.Text = "Select Valid Signature Name"
                Else
                    Server.Transfer("ShowVolunteerCertificateBlankPDF.aspx")
                End If
            Else
                If Convert.ToInt32(Session("SelChapterID")) <> 1 And (Trim(Session("LeftSignatureName")) = "" Or Trim(Session("RightSignatureName")) = "" Or Trim(Session("LeftSignatureTitle")) = "" Or Trim(Session("RightSignatureTitle")) = "" Or Trim(Session("LeftTitle")) = "" Or Trim(Session("RightTitle")) = "") Then
                    pnlMessage.Visible = True
                    lblMessage.Text = "Select Valid Signature Name"
                Else
                    Server.Transfer("ShowVolunteerCertificateNewPDF.aspx")
                End If
            End If
        End If

    End Sub

    Private _export As Boolean
    Public Property Export() As Boolean
        Get
            Return _export
        End Get
        Set(ByVal value As Boolean)
            _export = value
        End Set
    End Property

    Protected Sub ddlCertificate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Participant
        If ddlCertificate.SelectedValue = "Participant" Then
            setRange()
        Else
            trrange.Visible = False
        End If
        CheckTCSchedule()
        CheckBadgenumbers()

    End Sub

    Private Sub setRange()
        If Session("SelChapterID") = "1" Then
            Dim Strdates As String = String.Empty
            Dim i As Integer
            'If lstContestDates.Items.Count = 0 Then Exit Sub
            For i = 0 To lstContestDates.Items.Count - 1
                If Strdates.Length = 0 Then
                    Strdates = "'" & lstContestDates.Items(i).Text & "'"
                Else
                    Strdates = Strdates & ",'" & lstContestDates.Items(i).Text & "'"
                End If
            Next
            'Range for participants
            trrange.Visible = True
            Dim childcnt As Integer
            Try
                childcnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(ddlEventYear.SelectedValue) & "")
            Catch ex As Exception
                'Response.Write("<br /><br /><br />" & "select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(ddlEventYear.SelectedValue) & "")
                'Response.Write(ex.ToString)
                Exit Sub
            End Try
            Dim Itemcount As Integer
            Itemcount = childcnt / 200
            If Itemcount * 200 < childcnt Then
                Itemcount = Itemcount + 1
            End If
            For i = 1 To Itemcount
                Dim li As ListItem = New ListItem()
                li.Text = (((i - 1) * 200) + 1) & "  To  " & (i * 200)
                li.Value = (((i - 1) * 200) + 1) & "_" & (i * 200)
                ddlRange.Items.Insert(i - 1, li)
            Next

            'test
            ''ddlRange.Items.Insert(0, New ListItem("1 to 25 ", "1_25"))
            ' ''ddlRange.Items.Insert(0, New ListItem("1 to 2 ", "1_2"))
            ''ddlRange.Items.Insert(1, New ListItem("501 to 525 ", "501_525"))
            ''ddlRange.Items.Insert(2, New ListItem("901 to 925 ", "901_925"))
            ddlRange.SelectedIndex = 0
        Else
            trrange.Visible = False
        End If
    End Sub
End Class
