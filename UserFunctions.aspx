<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.UserFunctions" CodeFile="UserFunctions.aspx.vb" Title="North South Foundation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <link href="css/jquery.qtip.min.css" rel="stylesheet" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>

    <script type="text/javascript">
      
        function showhide(targetID) {
            var elementmode = document.getElementById(targetID).style;
            elementmode.display = (!elementmode.display) ? 'none' : ''; 
        }

        $(function (e) { 
            $("#ancCoachLevelDet").qtip({ // Grab some elements to apply the tooltip to
                content: { 
                    text: function (event, api) { 
                        var dvHtml = "";  
                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/MathCountsLevelDetermination.aspx" target=_blank>Mathcounts - Level Determination</a></div> ';

                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/PreMathCountsLevelDetermination.aspx" target=_blank>Pre-Mathcounts Level Determination</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        return dvHtml; 
                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Level Determination</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                } 
            })

            $("#anCoachFAQ").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {

                        var dvHtml = ""; 

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/mathCountsFAQ.aspx" target=_blank>Mathcounts FAQ</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/PremathCountsFAQ.aspx" target=_blank>Pre-Mathcounts FAQ</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/uscontests/coaching/SATACT_Coaching_FAQ.pdf" target=_blank>SAT Coaching FAQ</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/Science.aspx" target=_blank>Science FAQ</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/uscontests/coaching/FAQ_Geography_Coaching.pdf" target=_blank>Geography FAQ</a></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>'; 
                        return dvHtml; 
                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">FAQ</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                } 
            })
        });
        $(document).ready(function () {
            var surl = "MailConfig.aspx/GetAllSupportMail";
            $.ajax({
                type: 'POST',
                contentType: "application/json; charset=utf-8",
                url: surl,
                success: function (responseData, textStatus, jqXHR) {
                    $('#spWrkSupportMail').text(responseData.d.Workshop);
                    $('#spCoachingSupportMail').text(responseData.d.Coaching);
                },
                error: function (responseData, textStatus, errorThrown) {
                }
            });
        });

    </script>
    <style type="text/css">
        .qTipWidth {
            max-width: 450px !important;
        }

        .blink {
            animation-duration: 700ms;
            animation-name: blink;
            animation-iteration-count: infinite;
            animation-direction: alternate;
            -webkit-animation: blink 700ms infinite; /* Safari and Chrome */
            font-size: 20px;
        }

        @keyframes blink {
            from {
                color: #46A71C;
            }

            to {
                color: red;
            }
        }

        @-webkit-keyframes blink {
            from {
                color: #46A71C;
            }

            to {
                color: white;
            }
        }

        .badge1 {
            position: relative;
        }

            .badge1[data-badge]:after {
                content: attr(data-badge);
                position: absolute;
                top: -10px;
                right: -10px;
                font-size: .7em;
                background: green;
                color: white;
                width: 18px;
                height: 18px;
                text-align: center;
                line-height: 18px;
                border-radius: 50%;
                box-shadow: 0 0 1px #333;
            }
    </style>
    <table>
        <tr>
            <td style="width: 700px; text-align: center" class="title02">Parent Functions  
            </td>
            <td align="right">
                <asp:HyperLink CssClass="btn_02" Font-Size="Medium" ID="Hyperlink23" runat="server" NavigateUrl="SupportTracking.aspx?NewTicket=true">File Support Ticket</asp:HyperLink>
            </td>
        </tr>
    </table>
    <div id="CCDetails" runat="server" visible="false">
        <table width="100%">
            <tr>
                <td><b>Chapter:</b> <span id="spChapter" runat="server"></span>,&nbsp;  &nbsp; <b>Coordinator:</b> <span id="spCoordinator" runat="server"></span>,&nbsp;  &nbsp;    
                <b>Email:</b> <span id="spEmail" runat="server"></span>,&nbsp;  &nbsp;  <b>Phone:</b> <span id="spPhone" runat="server"></span>
                    <br />
                    <b>Online Workshop: </b><span id="spWrkSupportMail"></span>  &nbsp;  &nbsp;   <b>Online Coaching: </b><span id="spCoachingSupportMail"></span>  
                </td>
            </tr>
        </table>
    </div>
    <table id="Table1" class="tableclass" bordercolor="#cc6600" cellspacing="1" bordercolordark="#ffffff" cellpadding="3"
        bgcolor="#ff9966" bordercolorlight="#ffff66" border="2">
        <tr id="trMessage" runat="server">
            <td colspan="3" class="title04" bordercolor="#ffff99" bgcolor="#ffffcc">
                <asp:Label CssClass="link" runat="server" ID="lblMessage"></asp:Label>
            </td>
            <td class="title04" bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    &nbsp;
                </p>
            </td>
        </tr>
        <tr>
            <th bordercolor="#ffff99" bgcolor="#ffffcc" width="20%" align="center">Application</th>
            <th bordercolor="#ffff99" bgcolor="#ffffcc" width="25%" align="center">Main Option</th>
            <th bordercolor="#ffff99" bgcolor="#ffffcc" width="30%" align="center">Reports</th>
            <th bordercolor="#ffff99" bgcolor="#ffffcc" width="25%" align="center">Bulletin Board</th>
        </tr>

        <tr id="trParent" runat="server">
            <td align="left" bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Contests � Regional
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkNationalRegistration" runat="server">Register for Regional Contests</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink7" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeCenter.aspx">Change Center</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink3" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeContest.aspx">Change Contest</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink ID="hlinkExcelathonReg" CssClass="btn_02" runat="server" Enabled="false" NavigateUrl="DThonRegis.aspx?Ev=18">Excel-a-thon</asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">

                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lnkViewCCalendar" runat="server">View Contest Calendar </asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LinkButton1" runat="server" PostBackUrl="Parents/RegistrationStatus.aspx?ParentUpdate=True&EventID=2">View Status for Regional Contests &amp; Download</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkScores" runat="server" NavigateUrl="Parents/ViewScores.aspx?ParentUpdate=True">View Regional Scores </asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkBulletinBoard" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Regional/StepByStepInstructions_RegionalContests.pdf">Step-by-step Instructions to register</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink10" Target="_blank" runat="server" NavigateUrl="FAQ Customer Service_Final.pdf">FAQ during Contest Registration</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="Hyperlink90" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div12');">How to make Your Child a Winner</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="Hyperlink37" CssClass="btn_02" runat="server" NavigateUrl="https://attendee.gotowebinar.com/register/319609718737404929">Parents Orientation Session</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="Hyperlink38" CssClass="btn_02" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/LearningOutcomes_2017AR.pdf">Learning Outcomes</asp:HyperLink>
                </p>
                <div id="div12" style="display: none;">
                    <p>&nbsp;&nbsp;&nbsp;<asp:HyperLink class="btn_03" ID="Hyperlink28" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/Preparation_Spelling.pdf">Spelling</asp:HyperLink></p>
                    <p>
                        &nbsp;&nbsp;&nbsp;<asp:HyperLink class="btn_03" ID="Hyperlink29" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/Preparation_Vocabulary.pdf">Vocabulary</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;<asp:HyperLink class="btn_03" ID="Hyperlink30" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/Preparation_Math.pdf">Math</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;<asp:HyperLink class="btn_03" ID="Hyperlink31" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/Preparation_Science.pdf">Science</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;<asp:HyperLink class="btn_03" ID="Hyperlink32" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Parent/Regional/Preparation_Geography.pdf">Geography</asp:HyperLink>
                    </p>
                </div>
            </td>
        </tr>
        <tr id="trparent2" runat="server">
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Contests � National Finals
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton ID="hlnkNationFinals" CssClass="btn_02" runat="server">Register for National Finals</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkDeclineInvitees" CssClass="btn_02" runat="server" NavigateUrl="Parents/FinalsInvitee_Decline.aspx">Decline Finals Invitation</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkBeeBookPhotos" CssClass="btn_02" runat="server" NavigateUrl="UploadPhotos.aspx">Upload photos for the Bee Book</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkExcelathon" CssClass="btn_02" runat="server" Enabled="false" NavigateUrl="DThonRegis.aspx?Ev=18">Excel-a-thon</asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink ID="hlinkRegStatus" CssClass="btn_02" runat="server" NavigateUrl="Parents/RegistrationStatus.aspx?ParentUpdate=True&EventID=1">View Status for National Finals &amp; Download</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkViewNtnlScore" CssClass="btn_02" Enabled="true" runat="server" NavigateUrl="Parents/ViewNationalScores.aspx?ParentUpdate=True">View National Scores </asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>
        <tr id="TrOWkShop" runat="server">
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Online Workshop</td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkRegisForOnlneWksop" runat="server">Register for Online Workshop</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LinkButton4" runat="server" PostBackUrl="~/Admin/ChangeSubjectDate_OWKShop.aspx">Change Subject/Date</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="lnkonlineWrkShop_vw" runat="server" NavigateUrl="Parents/vwOnlineWrkShopCal.aspx">View Online Workshop Calendar</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink25" runat="server" NavigateUrl="Parents/OnlineWrkshpRegistrationStatus.aspx?ParentUpdate=True&EventID=20">View Status for Online Workshop &amp; Download</asp:HyperLink>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>
        <tr id="trParent1" runat="server">
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Onsite Workshop</td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkRegisterForWorkshop" runat="server">Register for Onsite WorkShop</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink1" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeWrkshpCenter.aspx">Change Onsite Workshop Center</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lnkWrkShopCalendar" runat="server">View Workshop Calendar</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hLinkViewWrkshpStatus" runat="server" NavigateUrl="Parents/WrkshpRegistrationStatus.aspx?ParentUpdate=True&EventID=3">View Status for Onsite WorkShop</asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>

        <tr id="TrPrepClub" runat="server">
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">PrepClub (In-person Classes)</td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkRegisterForPrepClub" runat="server">Register for PrepClub</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LinkButton2" runat="server" PostBackUrl="Admin/changeSubject.aspx">Change Subject</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink18" runat="server" NavigateUrl="Parents/PrepClubRegistrationStatus.aspx?ParentUpdate=True&EventID=19">View Status for PrepClub</asp:HyperLink>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>
        <tr id="tr1" runat="server">
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Coaching</td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02 blink" Style="text-decoration: none;" ID="hlinkRegisterforcoaching" runat="server">Register for Coaching</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkChangeCoach" runat="server" PostBackUrl="Admin/changeCoaching.aspx">Change Coach</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkAssessmentExam" runat="server" PostBackUrl="childAssessmentForm.aspx">Assessment Exam</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lbtnSATTesting" runat="server">Enter Answers</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="bttnCreateStudentLogin" runat="server" CssClass="btn_02">Manage Student Login/Email</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink8" runat="server" NavigateUrl="coachingstatus.aspx">View Status for Coaching</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink26" runat="server" NavigateUrl="~/Reports/CoachClassCalStudentsRpt.aspx">Class Calendar</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink16" runat="server" NavigateUrl="ViewSATTestScores.aspx">View Scores</asp:HyperLink>
                </p>
                <%--NavigateUrl="~/Parents/SATtestStatus.aspx"--%>
                <%--  <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink17" Target="_blank" runat="server" NavigateUrl="~/PSAT_Instructions.pdf" Enabled="false"> Download SAT Test Instructions</asp:HyperLink>
                </p>--%>
                <%--<p><asp:hyperlink CssClass="btn_02" id="Hyperlink15" Target="_blank" Enabled="false" runat="server" NavigateUrl="~/PSAT_Questions.pdf">Download SAT Test Paper</asp:hyperlink></p>
                --%>
                <p>
                    <asp:LinkButton ID="bttnDownloadPapers" runat="server" CssClass="btn_02">Download Papers</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink19" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/InstructionsForCoachingTools_Parents.pdf">Instructions for NSF Web Tools</asp:HyperLink>
                </p>
                <p>
                    <a id="ancCoachLevelDet" style="font-weight: bold; color: blue; cursor: pointer;">Level Determination </a>
                </p>
                <p>
                    <a id="anCoachFAQ" style="font-weight: bold; color: blue; cursor: pointer;">FAQ</a>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink11" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/MathCountsLevelDetermination.aspx">Mathcounts - Level Determination</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink12" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/PreMathCountsLevelDetermination.aspx">Pre-Mathcounts Level Determination</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink13" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/mathCountsFAQ.aspx">Mathcounts FAQ</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink14" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/PremathCountsFAQ.aspx">Pre-Mathcounts FAQ</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink15" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/SATACT_Coaching_FAQ.pdf">SAT Coaching FAQ</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink35" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/Science.aspx">Science FAQ</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink34" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/FAQ_Geography_Coaching.pdf">Geography FAQ</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink24" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/UVOrientationGuide.pdf">UV Orientation Guide</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink17" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/NSF Orientation for Parents.pdf">Orientation for Parents</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink36" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/GradingToolInstruction.docx">Guide to Entering Math Answers</asp:HyperLink>
                </p>
            </td>
        </tr>
        <tr>
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Games
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkRegisterForGame" runat="server">Register for Game</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="lbtnCreateChildLogin" runat="server" CssClass="btn_02">Create Child Login</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <asp:HyperLink ID="HyperLink20" runat="server" CssClass="btn_02" NavigateUrl="~/Parents/GameRegistrationStatus.aspx?ParentUpdate=True&amp;EventID=4">View Status for Games</asp:HyperLink>
                <p>
                    <asp:HyperLink ID="HyperLink33" runat="server" CssClass="btn_02" NavigateUrl="~/GamePerf.aspx">Game Performance</asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink22" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Game/GameGuide.pdf">Game Guide</asp:HyperLink>
                </p>
            </td>
        </tr>
        <tr>
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Giving
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton ID="hlinkdonate" CssClass="btn_02" runat="server" Text="Donate"></asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkExcelathon1" CssClass="btn_02" runat="server" Enabled="false" NavigateUrl="DThonRegis.aspx?Ev=18">Register for Excel-a-thon</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkWalkathon" runat="server" Enabled="false" NavigateUrl="Don_athon_selection.aspx?Ev=5">Register for Walk-a-thon</asp:HyperLink>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lnkDas" runat="server" Text="Register for Dollar-a-Square (DAS)"></asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lnkFundRaising" runat="server" Text="Register for Fundraising"></asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink6" Enabled="true" runat="server" NavigateUrl="~/DonationReceipt.aspx">Donation Receipt</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="HyperLink9" NavigateUrl="~/FundRStatus.aspx?id=1" runat="server">View Fundraising Status </asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="HyperLink2" NavigateUrl="~/DonateAThonStatus.aspx" runat="server">View WalkaThon/ExcelaThon Status </asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>
        <tr>
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">Shop
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="hlinkShop" runat="server">Shop</asp:LinkButton>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="HyperLink4" NavigateUrl="Shoppingstatus.aspx" runat="server">Purchase Status</asp:HyperLink>
                </p>
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc"></td>
        </tr>
        <tr>
            <td bordercolor="#ffff99" bgcolor="#ffffcc" class="title04">General
            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink183" runat="server" NavigateUrl="VolunteerSignUpnew.aspx" Enabled="true">Volunteer Signup</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlReferralProgram" runat="server" NavigateUrl="~/ReferralProgram.aspx" Enabled="true">Referral Program</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlFillOutSurvey" CssClass="blink" runat="server" NavigateUrl="~/SurveyList.aspx?source=FS" Enabled="true">Fill out a Survey</asp:HyperLink>

                </p>
                <div id="dvSurveyNotification" style="float: right; text-align: center; border-radius: 10px; position: relative; top: -31px; width: 23px; right: 46px; height: 20px; background-color: green; color: white;" runat="server">
                    <asp:Label runat="server" ID="lblNotCount" Style="text-align: center; font-weight: bold; color: white;"></asp:Label>
                </div>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkParentInfo" runat="server" NavigateUrl="Registration.aspx?ParentUpdate=True" Visible="False">Update Parent Information</asp:HyperLink>
                </p>
                <p>
                    <asp:LinkButton ID="btnupdateParentInfo" runat="server" CssClass="btn_02">Update Parent Information</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton ID="lbtnchildinfo" runat="server" CssClass="btn_02">Add/Update Child Information</asp:LinkButton>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkPerRef" runat="server" NavigateUrl="PersonalPref.aspx?id=1" Enabled="true">Personal Preferences</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkReqRefund" runat="server" NavigateUrl="Refund.aspx" Enabled="True">Request for Refund</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink27" runat="server" NavigateUrl="RemoveFromSuppressionList.aspx" Enabled="True">Remove from Suppression List</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlinkChgPending" runat="server" NavigateUrl="Refund.aspx">Change Pending to Paid</asp:HyperLink>
                    &nbsp;
                </p>

            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink5" runat="server" NavigateUrl="ContPerfRecord.aspx">Performance Record of Contestants</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink274" runat="server" NavigateUrl="SupportTracking.aspx?view=yes&showmine=true">Support Ticket Tracking</asp:HyperLink>
                </p>

            </td>
            <td bordercolor="#ffff99" bgcolor="#ffffcc">
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink21" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Parent/Coaching/DonationRefund_Instructions.pdf">Donation Refund Instructions</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="hlSplashPage" runat="server" NavigateUrl="~/SplashPageContents.aspx">Splash Page</asp:HyperLink>
                    &nbsp;
                  
                </p>
            </td>
        </tr>
    </table>
</asp:Content>

