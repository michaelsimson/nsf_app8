﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using System.Drawing;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Collections;
using VRegistration;
using System.Net;
using System.IO;
using NativeExcel;

public partial class ZoomSessionsUsageRepor : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            LoadVolunteer();
            FillZoomSessions();
            

        }
    }

    protected void FillZoomSessions()
    {
        try
        {
            string fillqry = " select distinct wc.year, wc.RoleId, wc.ConfId, wc.EventId, wc.memberId, wc.TeamId, wc.Date, wc.Time, wc.Duration, wc.SessionType, wc.Sessionkey, wc.HostURl, wc.JoinURL,IP.FirstName, IP.LastName, IP.FirstName+' '+IP.Lastname as name, E.EventCode, VT.TeamName, V.RoleCode, wc.Vroom, VL.HostId, wc.joinURL from WebConfSessions wc inner join Indspouse IP on (IP.AutomemberId=wc.memberId) inner join Volteammatrix VT on (VT.teamId=wc.teamId) inner join Volunteer V on (V.RoleId=wc.Roleid and V.memberId=v.memberId) inner join VirtualRoomLookup VL on (wc.Vroom=VL.Vroom) inner join Event E on (E.EventId=wc.EventId) where wc.year=2018";
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            {
                if (ddlVolunteer.SelectedValue != "-1")
                {
                    fillqry += " and wc.MemberId=" + ddlVolunteer.SelectedValue + "";
                }
                if (txtFrom.Text != "" && txtTo.Text != "")
                {
                    fillqry += " and wc.Date between '" + txtFrom.Text + "' and '" + txtTo.Text + "'";
                }

                fillqry += " order by Ip.FirstName, IP.LastName ASC";
            }
            else
            {
                fillqry += " and wc.MemberId=" + Session["LoginId"].ToString() + "";
            }

            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvOnlineWkShopZoom.DataSource = ds;
                    gvOnlineWkShopZoom.DataBind();
                    gvOnlineWkShopZoom.Visible = true;

                    lblNoRecord.Visible = false;

                    for (int i = 0; i < gvOnlineWkShopZoom.Rows.Count; i++)
                    {
                        string TodayDate = DateTime.Now.ToShortDateString();
                        DateTime drDateToday = Convert.ToDateTime(TodayDate);
                        string MeetingDate = gvOnlineWkShopZoom.Rows[i].Cells[7].Text.ToString().Trim();
                        DateTime dtMeetDate = Convert.ToDateTime(MeetingDate);
                        string SessionType = gvOnlineWkShopZoom.Rows[i].Cells[10].Text.ToString();

                        if (SessionType.ToString().Trim() == "3")
                        {
                            gvOnlineWkShopZoom.Rows[i].Cells[10].Text = "Recurring";
                        }
                        else
                        {
                            gvOnlineWkShopZoom.Rows[i].Cells[10].Text = "Single";
                        }
                    }
                }
                else
                {
                    lblNoRecord.Visible = true;
                    gvOnlineWkShopZoom.DataSource = ds;
                    gvOnlineWkShopZoom.DataBind();
                }

            }
            else
            {
                lblNoRecord.Visible = true;
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void BtnExport_Click(object sender, EventArgs e)
    {
        exportExcel();
    }

    public void exportExcel()
    {
        string fileName = string.Empty;

        string fillqry = " select distinct wc.year, wc.RoleId, wc.ConfId, wc.EventId, wc.memberId, wc.TeamId, wc.Date, wc.Time, wc.Duration, wc.SessionType, wc.Sessionkey, wc.HostURl, wc.JoinURL,IP.FirstName, IP.LastName, IP.FirstName+' '+IP.Lastname as name, E.EventCode, VT.TeamName, V.RoleCode, wc.Vroom, VL.HostId, wc.joinURL from WebConfSessions wc inner join Indspouse IP on (IP.AutomemberId=wc.memberId) inner join Volteammatrix VT on (VT.teamId=wc.teamId) inner join Volunteer V on (V.RoleId=wc.Roleid and V.memberId=v.memberId) inner join VirtualRoomLookup VL on (wc.Vroom=VL.Vroom) inner join Event E on (E.EventId=wc.EventId) where wc.year=2018";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
        {
            if (ddlVolunteer.SelectedValue != "-1")
            {
                fillqry += " and wc.MemberId=" + ddlVolunteer.SelectedValue + "";
            }
            if (txtFrom.Text != "" && txtTo.Text != "")
            {
                fillqry += " and wc.Date between '" + txtFrom.Text + "' and '" + txtTo.Text + "'";
            }

            fillqry += " order by Ip.FirstName, IP.LastName ASC";
        }
        else
        {
            fillqry += " and wc.MemberId=" + Session["LoginId"].ToString() + "";
        }
        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables[0] != null)
            {

                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);

                    oSheet = oWorkbooks.Worksheets.Add();

                    oSheet.Name = "Zoom Usage Report";
                    oSheet.Range["A1:L1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "Zoom Usage Report";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Year";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Role";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "Team";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "Event";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "Meeting Date";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "Time";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "Duration";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "Session Type";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Vroom";
                    oSheet.Range["K3"].Font.Bold = true;

                    oSheet.Range["L3"].Value = "SessionKey";
                    oSheet.Range["L3"].Font.Bold = true;



                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["Year"].ToString();

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["Name"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["Rolecode"].ToString().Replace("&nbsp;", " ");

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["TeamName"].ToString();

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["EventCode"].ToString();

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["Time"].ToString();

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["Duration"].ToString();

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = (dr["SessionType"].ToString() == "3" ? "Recurring" : "Single");

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["Vroom"].ToString();

                        CRange = oSheet.Range["L" + iRowIndex.ToString()];
                        CRange.Value = dr["SessionKey"].ToString();



                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    fileName = "ZoomUsageReport";
                    string filename = fileName + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);
                    Response.End();

                }

            }
        }
        catch
        {
        }
    }

    public void LoadVolunteer()
    {
        try
        {
            string fillqry = " select distinct wc.year,IP.FirstName, IP.LastName, IP.FirstName+' '+IP.Lastname as name,IP.AutoMemberId from WebConfSessions wc inner join Indspouse IP on (IP.AutomemberId=wc.memberId) inner join Volteammatrix VT on (VT.teamId=wc.teamId) inner join Volunteer V on (V.RoleId=wc.Roleid and V.memberId=v.memberId) inner join VirtualRoomLookup VL on (wc.Vroom=VL.Vroom) inner join Event E on (E.EventId=wc.EventId)";
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            {


                fillqry += " order by Ip.FirstName, IP.LastName ASC";
            }
            else
            {
                fillqry += " and wc.MemberId=" + Session["LoginId"].ToString() + "";
            }

            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlVolunteer.DataTextField = "name";
                    ddlVolunteer.DataValueField = "AutoMemberId";
                    ddlVolunteer.DataSource = ds;
                    ddlVolunteer.DataBind();
                    ddlVolunteer.Items.Insert(0, new ListItem("Select", "-1"));

                    if (ds.Tables[0].Rows.Count == 1)
                    {
                        ddlVolunteer.SelectedValue = ds.Tables[0].Rows[0]["AutoMemberId"].ToString();
                    }

                    ddlYear.DataValueField = "year";
                    ddlYear.DataTextField = "year";
                    ddlYear.DataSource = ds;
                    ddlYear.DataBind();
                    ddlYear.Items.Insert(0, new ListItem("Select", "-1"));
                    ddlYear.SelectedValue = DateTime.Now.Year.ToString();
                }
            }
        }
        catch
        {
        }
    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        FillZoomSessions();
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        txtFrom.Text = "";
        txtTo.Text = "";
        LoadVolunteer();
        FillZoomSessions();
    }
}