﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.Sql;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Data;

public partial class EmailFundRaising : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            int first_year = 2006;
            int year = Convert.ToInt32(DateTime.Now.Year) + 2;
            int count = year - first_year;

            for (int i = 0; i < count; i++)
            {

                lstYear.Items.Insert(i, new ListItem(Convert.ToString(year - (i + 1))));
                //year = year - 1
            }
            lstYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
        }
    }

    public void LoadEmails()
    {
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        if (ddlRegistype1.SelectedValue == "1")
        {
            CmdText = "select Distinct(IP.EMail) from FundRReg FR inner join IndSpouse IP on (FR.MemberID=IP.AutoMemberID)where FR.EventYear=" + lstYear.SelectedValue + " and FR.FundRCalID=3 and FR.PaymentReference is not null";
        }
        else if (ddlRegistype1.SelectedValue == "2")
        {
            CmdText = "select Distinct(IP.EMail) from FundRReg FR inner join IndSpouse IP on (FR.MemberID=IP.AutoMemberID)where FR.EventYear=" + lstYear.SelectedValue + " and FR.FundRCalID=3 and FR.PaymentReference is null and Not(FR.ProductGroupCode='Admission' and FR.ProductCode in ('Adult', 'Child') and FR.TotalAmt=0)";
        }
        StringBuilder sbEmailList = new StringBuilder();
        DataSet dsEmails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != dsEmails && dsEmails.Tables.Count > 0)
        {
            if (dsEmails.Tables[0].Rows.Count > 0)
            {
                for (int ctr = 0; ctr < dsEmails.Tables[0].Rows.Count; ctr++)
                {
                    sbEmailList.Append(dsEmails.Tables[0].Rows[ctr]["EMail"].ToString());
                    if (ctr <= dsEmails.Tables[0].Rows.Count - 2)
                        sbEmailList.Append(",");
                }
                Session["emaillist"] = sbEmailList.ToString();
                Session["sentemaillistenable"] = "No";
                Session["EmailSource"] = "EmailFundR";
                //Response.Write(Session["emaillist"].ToString());
                Response.Redirect("emaillist.aspx");
            }
            else
            {
                lblerr1.Text = "Sorry No email found";
            }
        }
        else
        {

            lblerr1.Text = "Sorry No email found";
        }

    }
    protected void ParentStudEmail_Click(object sender, EventArgs e)
    {
        LoadEmails();
    }
}