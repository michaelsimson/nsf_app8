﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="WebAccessMgmt.aspx.vb" Inherits="WebAccessMgmt" title="Web Access Management" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<script language="javascript" type="text/javascript">
<!--



function validate()
{
    //lblURL

    var obj = document.getElementById("<%=ddlAccessType.ClientID%>");
    var obj2 = document.getElementById("<%=lblURL.ClientID%>");
    return confirm('Are you sure to give access to URL ' + obj2.innerHTML + ' with ' + obj.value + ' Access permission?');
                 
}

// -->
</script>
<div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    <asp:LinkButton ID="LinkButton1" PostBackUrl="WebPageMgmtMain.aspx" runat="server">Back</asp:LinkButton>
</div>
<div runat ="server" id="divMulti" visible = "false" style ="width:800px; text-align : center ">
<table border="0px" width="100%" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align = "center" colspan="2" >Select Folder : 
    <asp:DropDownList ID="ddlselectFolder" AutoPostBack="true" OnSelectedIndexChanged=" ddlselectFolder_SelectedIndexChanged" DataValueField="FolderListID" DataTextField="DisplayName" runat="server">
    </asp:DropDownList> <br/>
    <asp:Label ID="lblVErr" runat="server" ></asp:Label>
    </td> </tr> </table></div>
<div  runat ="server" id="divAll" visible = "true" style ="width:800px; text-align : center ">
<table border="0px" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align="center" width="800px">
<div align="center">
<table border="0" cellpadding ="3" cellspacing ="0"> 
 <tr><td align="center" colspan="3"><h5>Manage Access Privileges  </h5></td></tr>
<tr><td align="left">Add/Update Volunteer : </td><td align="left">
        <asp:TextBox  Width="145px" ID="txtParent" runat="server" Enabled="false"></asp:TextBox></td>
        <td align="left">
            <asp:Button ID="Search" runat="server" Text="Search" /></td></tr>
            <tr><td align="center" colspan ="3"><asp:Label ID="lblerr" ForeColor = "Red" runat="server" ></asp:Label></td></tr></table>

<asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF member</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="right">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    </td>
    	    <td  align="left">					
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
 </div>
</td></tr>
<tr><td align="center">
<table runat = "server" visible = "false" id="tblddls" border="0" cellpadding = "3" cellspacing = "0">
<tr><td align = "left" > Level 1 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel1"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged" Width="150px" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 2 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel2"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 3 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel3"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 4 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel4"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 5 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel5"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel5_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 6 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel6"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel6_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 7 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel7"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel7_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 8 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel8"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel8_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 9 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel9"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel9_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 10 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel10"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel10_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 11 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel11"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel11_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 12 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel12"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel12_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Type Of Access</td><td align = "left" >
    <asp:DropDownList ID="ddlAccessType" runat="server">
        <asp:ListItem Value="Read" Selected="True">Read (Download Only)</asp:ListItem>
        <asp:ListItem Value="Write">Write (Download/Upload)</asp:ListItem>
        <asp:ListItem Value="Delete">Delete (Delete folder/file)</asp:ListItem>
        <asp:ListItem Value="Create Sub-folders">Create Sub-folder</asp:ListItem>
        <asp:ListItem Value="All">All</asp:ListItem>
    </asp:DropDownList>
</td></tr>
<tr><td align = "left" >IP Address</td><td align = "left" >
    <asp:TextBox ID="txtIPAddress" runat="server"></asp:TextBox></td></tr>
<tr><td align = "center" colspan="2"><asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>&nbsp;</td></tr>


<%--<tr><td align = "center" colspan="2" > 
    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick ="btnContinue_Click"/>
</td></tr>--%>

</table> 
</td></tr>
<tr><td align="center"> 
    <asp:Label ID="lblURL" runat="server" ></asp:Label></td></tr>
    <tr id="trbuttons" visible = "false" runat ="server"><td align = "center"  > 
    <asp:Button ID="btnNewAccess" runat="server" Text="Give New Access" OnClick ="btnNewAccess_Click"/>&nbsp;&nbsp;
     &nbsp;&nbsp;
          &nbsp;
               <asp:Button ID="BtnClearAll" runat="server" Text="Clear" OnClick ="BtnClearAll_Click" />
</td></tr>
<tr><td align="center">
    <asp:Label ID="lblTable11"  Visible = "false" runat="server" Text="Table 1: Volunteer Access Table" Font-Bold="true"></asp:Label>
<asp:DataGrid ID="DGVolunteer" runat="server" DataKeyField="VolDocAccessID" 
        AutoGenerateColumns="False" OnItemCommand="DGVolunteer_ItemCommand" 
        CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="LightBlue" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
            <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete"></asp:buttoncolumn>
			<asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel"  EditText="Edit"></asp:EditCommandColumn> 
            <asp:Boundcolumn  DataField="volDocAccessID"  HeaderText="volDocAccessID" Visible="false" />
            <asp:TemplateColumn HeaderText="Volunteer Name">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblVname" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Vname")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="Folder URL">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblFolderURL" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FolderURL")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="AccessType">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblAccessType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AccessType")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn ><asp:TemplateColumn HeaderText="IPAddress">
              <HEADERSTYLE Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblIPAddress" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.IPAddress")%>'></asp:Label>
                  </ItemTemplate>      
                  <EditItemTemplate>
                  <asp:TextBox ID="txtIPAddress" BorderColor="Blue" BorderStyle="Double" BorderWidth="1px" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.IPAddress")%>'></asp:TextBox><br />
                   <asp:Label ID="lblIPAddresstxt" runat="server" Text="Enter IP Address in nnn.nnn.nnn.nnn Format"></asp:Label>
                   </EditItemTemplate>                                                                  
              </asp:TemplateColumn >  
              


<asp:TemplateColumn HeaderText="Role">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RoleCode")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="EMail">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EMail")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="HPhone">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblHPhone" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.HPhone")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="CPhone">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblCPhone" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CPhone")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="City">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblCity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.City")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="State">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
<asp:TemplateColumn HeaderText="Chapter">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblChapter" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >      
        </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>
</td></tr>
<tr><td>
<div align="center">
<asp:Label ID="lblTable2"  Visible = "false" runat="server" Text="Table 2:  Volunteers with Assigned Roles  " Font-Bold="true"></asp:Label>

 <asp:DataGrid ID="DGVolunteer2" runat="server" DataKeyField="volunteerID" 
        AutoGenerateColumns="False"  OnItemCommand="DGVolunteer2_ItemCommand" 
        CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="LightBlue" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
           <ASP:TemplateColumn>
           <ItemTemplate>
						<asp:LinkButton id="lbtnSelect" runat="server" CausesValidation="false" CommandName="Select" Text="Select" ></asp:LinkButton>
			</ItemTemplate>
						</ASP:TemplateColumn>
           <asp:Boundcolumn DataField="volunteerID"  HeaderText="Volunteer ID" Visible="false" />
             <asp:Boundcolumn DataField="Memberid"  HeaderText="Memberid" Visible="false" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Vname"  HeaderText="Volunteer Name" />
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="EMail" HeaderText="EMail"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="City" HeaderText="City"/>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="State" HeaderText="State"/>
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="RoleCode" HeaderText="Role"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="HPhone" HeaderText="Home Phone"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="CPhone" HeaderText="Cell Phone"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChapterCode" HeaderText="Chapter"/>
                  </COLUMNS>         
               <HeaderStyle BackColor="White" />
    </asp:DataGrid><br />
     <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
 </div>
</td></tr>
<tr id="Tr1" runat="server" visible = "false"><td align = "center"  >
 <asp:Label ID="HlblMemberID" runat="server" Visible="false"></asp:Label>
    <asp:Label ID="lblLevel" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblParentFID" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblPath" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblRenameFolder" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblRenamePath" runat="server" visible = "false"></asp:Label>    
    </td></tr>
</table></div>
</asp:Content>

