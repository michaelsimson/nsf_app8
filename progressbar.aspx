﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true" CodeFile="progressbar.aspx.cs" Inherits="progressbar" %>
<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="ajaxToolkit"%>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<script language="javascript" type="text/javascript">

    function StartProgressBar() {

        var myExtender = $find('ProgressBarModalPopupExtender');

        myExtender.show();

        return true;

    }

</script>



.ModalBackground
{
    background-color:white;
    filter:alpha(opacity=50);
    -moz-opacity:0.5;
    -khtml-opacity: 0.5;
    opacity: 0.5;
    }
   

<asp:ScriptManager ID="ScriptManager1" runat="server" />

<asp:UpdatePanel ID="UpdatePanel1" runat="server">
    <ContentTemplate>
       <div>
            <asp:Button ID="btnSubmit" onclick="btnSubmit_Click" OnClientClick="StartProgressBar()"
                     runat="server" Text="Submit Time" Width="170px" />
            <ajaxToolkit:ModalPopupExtender ID="ProgressBarModalPopupExtender" runat="server"
                     BackgroundCssClass="ModalBackground" behaviorID="ProgressBarModalPopupExtender"
                     TargetControlID="hiddenField" PopupControlID="Panel1" />
            <asp:Panel ID="Panel1" runat="server" Style="display: none; background-color: #C0C0C0;">
                 <img src="progressbar.gif" alt="" />
            </asp:Panel>
            <asp:HiddenField ID="hiddenField" runat="server" />
       </div>

   </ContentTemplate>
</asp:UpdatePanel>

</asp:Content>

