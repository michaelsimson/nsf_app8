﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;

public partial class VirtualRoomRequirement : System.Web.UI.Page
{
    #region Variable Declaration

    string[] daysx;
    string Preclass;
    string Postclass;
    string str1grid;
    int cycle1;
    int cycle; int MaxVRoomNumber, SignUpID, rownum;
    DateTime startime;
   // string UpdateCalSignUpByYear = "Update CalSignUp set Cycle=0,VRoom=0 where  EventYear='"+DDyear.SelectedValue+"'";

    #endregion


    protected void Page_Load(object sender, EventArgs e)
    {
        LBmain.Visible = false;
        Session["LoginID"] = 12345;
        Session["RoleId"] = 1;
        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}

        //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
        //{
        //    Response.Redirect("~/login.aspx?entry=p");
        //}
        //if (Convert.ToInt32(Session["RoleId"]) != 1)
        //{
        //    Response.Redirect("~/VolunteerFunctions.aspx");
        //}
        if (!IsPostBack)
        {  
           // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, UpdateCalSignUpByYear);
            DDpre.Items.Insert(0, "30");
            DDpre.Items.Insert(0, "25");
            DDpre.Items.Insert(0, "20");
            DDpre.Items.Insert(0, "15");
            DDpre.Items.Insert(0, "Select");

            DDpost.Items.Insert(0, "60");
            DDpost.Items.Insert(0, "45");
            DDpost.Items.Insert(0, "30");
            DDpost.Items.Insert(0, "15");
            DDpost.Items.Insert(0, "Select");
            PopulateYear(DDyear);
          
        }
        lblalert.Visible = false;
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            //for (int i = 2011; i <= MaxYear; i++)
            //{
            //    list.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            for (int i = MaxYear; i >= 2011; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
          
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }

    public void VRoomcalculation()
    {
        DataTable dtDay = new DataTable();
        DataTable dtTemp = new DataTable();
        CalculateVRoomByDay("Sunday");
        CalculateVRoomByDay("Monday");
        CalculateVRoomByDay("Tuesday");
        CalculateVRoomByDay("Wednesday");
        CalculateVRoomByDay("Thursday");
        CalculateVRoomByDay("friday");
        CalculateVRoomByDay("Saturday");
    }
   
    protected void DisplayDataInGrid()
    {
       
        try
        {
             VRoomcalculation();
             DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetQueryByDay(string.Empty));
             ds.AcceptChanges();
             foreach (DataRow dr in ds.Tables[0].Rows)
             {
                 dr.BeginEdit();
                 switch (dr["Day"].ToString().ToLower())
                 {
                     case "saturday":
                         dr["D"] = 1;
                         break;
                     case "sunday":
                         dr["D"] = 2;
                         break;
                     case "monday":
                         dr["D"] = 3;
                         break;

                     case "tuesday":
                         dr["D"] = 4;
                         break;
                     case "wednesday":
                         dr["D"] = 5;
                         break;
                     case "thursday":
                         dr["D"] = 6;
                         break;

                     case "friday":
                         dr["D"] = 7;
                         break;

                 }
                 dr.EndEdit();
             }
             ds.AcceptChanges();

             DataView dv = ds.Tables[0].DefaultView;
             dv.Sort = "D Asc";
             DataTable sortedDT = dv.ToTable();
             //DataTable dsne = sortedDT.Copy();
             DataTable dtn = sortedDT.Copy();
             int i = 1; foreach (DataRow r in dtn.Rows) r["RowNum"] = i++;
             DataRow[] drs = dtn.Select("D='1'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex-1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }

            drs = dtn.Select("D='2'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }
             drs = dtn.Select("D='3'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }

             drs = dtn.Select("D='4'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }

             drs = dtn.Select("D='5'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }
             drs = dtn.Select("D='6'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }
             drs = dtn.Select("D='7'");
             {
                 if (null != drs && drs.Length > 0)
                 {
                     for (int previousRowIndex = 0; previousRowIndex < drs.Length; previousRowIndex++)
                     {
                         rownum = Convert.ToInt16(drs[previousRowIndex]["RowNum"].ToString());
                         int vr = Convert.ToInt16(drs[previousRowIndex]["VRoom"].ToString());
                         int cyclen = Convert.ToInt16(drs[previousRowIndex]["cycle"].ToString());
                         for (int nextRowIndex = previousRowIndex + 1; nextRowIndex < drs.Length; nextRowIndex++)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["next"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         for (int nextRowIndex = previousRowIndex - 1; nextRowIndex >= 0; nextRowIndex--)
                         {
                             if (vr == Convert.ToInt32(drs[nextRowIndex]["VRoom"]))
                             {
                                 drs[previousRowIndex]["Previous"] = drs[nextRowIndex]["RowNum"];
                                 break;
                             }
                         }
                         if (drs[previousRowIndex]["next"] == null || DBNull.Value.Equals(drs[previousRowIndex]["next"]))
                         {
                             drs[previousRowIndex]["next"] = 0;
                         }
                         if (drs[previousRowIndex]["Previous"] == null || DBNull.Value.Equals(drs[previousRowIndex]["Previous"]))
                         {
                             drs[previousRowIndex]["Previous"] = 0;
                         }
                     }
                 }
             }
           
           
            ds.AcceptChanges();
            DataSet dsgrid = new DataSet();
            dsgrid.Tables.Add(dtn);
            gridview_VR.DataSource = dsgrid;
            gridview_VR.DataBind();
            Session["Sessionno"] = dsgrid;
        }
        catch (Exception ex)
        {
           // Response.Write(ex.ToString());
        }

    }
    
    protected void ExportToExcel()
    {
        try
        {
           DataSet dsExport = (DataSet)Session["Sessionno"];
           DataTable dt = dsExport.Tables[0];
           Response.Clear();
           Response.Buffer = true;
           Response.AddHeader("content-disposition",
                "attachment;filename=VirtualRoomData.csv");
           Response.Charset = "";
           Response.ContentType = "application/text";
           StringBuilder sb = new StringBuilder();
            for (int k = 0; k < dt.Columns.Count; k++)
            {
                sb.Append(dt.Columns[k].ColumnName + ',');
            }
            sb.Append("\r\n");
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                for (int k = 0; k < dt.Columns.Count; k++)
                {
                    sb.Append(dt.Rows[i][k].ToString().Replace(",", ";") + ',');
                }

                sb.Append("\r\n");
            }
            Response.Output.Write(sb.ToString());
            Response.Flush();
            Response.End();
        }

        catch (Exception ex)
        {
        }
    }
   
    protected void Virtual_Click(object sender, EventArgs e)
    {
           
            //string UpdateCalSignUpByYear = "Update CalSignUp set Cycle=0,VRoom=0 where  EventYear='" + DDyear.SelectedValue + "'";
            if ((DDpre.SelectedItem.Text != "Select") && (DDpost.SelectedItem.Text != "Select") && (DDyear.SelectedItem.Text != "Select Year"))
            {
                //string droptempqry = "drop table [#CalSignupTemp]";
                //int i = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, droptempqry);
              

                string createTempTable = "create table CalSignupTemp (SignupID int,MemberID int,EventYear int,EventID int,EventCode nvarchar(25),Phase nvarchar(50),";
                createTempTable = createTempTable + "ProductGroupID int,ProductGroupCode nvarchar(50),ProductID int,ProductCode nvarchar(50),[Level] nvarchar(50),";
                createTempTable = createTempTable + "SessionNo int,Day nvarchar(50),Time time(7),Preference int,MaxCapacity int,MinCapacity int,StartDate smalldatetime,";
                createTempTable = createTempTable + "EndDate smalldatetime,CreateDate smalldatetime,CreatedBy int,ModifiedDate smalldatetime,ModifiedBy int,Accepted nvarchar(1),";
                createTempTable = createTempTable + "Duration float,[Begin] time(7),[End] time(7),Cycle int,VRoom int,UserID varchar(255),PWD varchar(255),UpFlag varchar(15)) insert into CalSignupTemp select * from CalSignup where EventYear="+DDyear.SelectedValue+" select * from CalSignupTemp";

            DataSet dsTemp=SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, createTempTable);

            DataSet tt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select * from CalSignupTemp");
                string strqry = "select * from CalSignup where EventYear=" + DDyear.SelectedValue + "";
                DataSet dset = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry);
                DataTable dtable = dset.Tables[0];

                GetVRoomLookUpDetails();
                string updateqry="Update CalSignupTemp set Cycle=0,VRoom=0 where  EventYear='" + DDyear.SelectedValue + "'";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
                starttime1();
                DisplayDataInGrid();
                Btnsubmit.Visible = false;
                excel.Visible = true;
                lblpre.Visible = false;
                lblpost.Visible = false;
                DDpre.Visible = false;
                DDpost.Visible = false;
                DDyear.Visible = false;
                Label1.Visible = false;
                // hlinkChapterFunctions.Visible = false;
                LBmain.Visible = true;
                gridview_VR.Visible = true;
            }
            else
            {
                lblalert.Visible = true;
            }

    }

    public void CalculateVRoomByDay(string DyName)
    {
        try
        {
            MaxVRoomNumber = 0;
            cycle = 0;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, GetQueryByDay(DyName));
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    startime = Convert.ToDateTime(dt.Rows[i]["Begin"].ToString());
                    TimeSpan StartTime = (TimeSpan)dt.Rows[i]["Begin"];
                    rownum = Convert.ToInt16(dt.Rows[i]["RowNum"].ToString());
                    SignUpID = Convert.ToInt16(dt.Rows[i]["SignUpID"].ToString());
                    DataRow []drs=dt.Select("RowNum<"+rownum);
                    bool isNeedToCheck = false;
                    if (null != drs && drs.Length > 0)
                    {
                        foreach (DataRow dr in drs)
                        {
                            TimeSpan endTime;
                            int hr = 24;
                            int min;
                            int sec;
                            //Add 24 hrs with end time
                            //When Start time is less than end time;
                            if ((TimeSpan)dr["Begin"] > (TimeSpan)dr["End"])
                            {
                                endTime = (TimeSpan)dr["End"];
                                hr = endTime.Hours + 24;
                                min = endTime.Minutes;
                                sec = endTime.Seconds;
                                if (hr < startime.Hour)
                                {
                                    isNeedToCheck = true;
                                    break;
                                }
                                else if (hr == startime.Hour)
                                {
                                    if (min < startime.Minute)
                                    {
                                        isNeedToCheck = true; break;
                                    }
                                    else if (min == startime.Minute)
                                    {
                                        if (sec < startime.Second)
                                        {
                                            isNeedToCheck = true; break;
                                        }
                                    }
                                }
                            }
                            else
                            {
                                if ((TimeSpan)dr["End"] < StartTime)
                                {
                                    isNeedToCheck = true;break;
                                }
                            }
                        }
                    }
                    if (isNeedToCheck)
                    {
                        int k = 0;
                        MaxVRoomNumber = MaxVRoom(DyName);
                        for (int j = 1; j <= MaxVRoomNumber; j++)
                        {
                            cycle = MaxCycleByVirtual(j,DyName);
                            DataRow[] drsDuplicate = dt.Select("VRoom='" + j.ToString()+"' AND Cycle='"+cycle.ToString()+"'");
                            List<DataRow> drList = new List<DataRow>();

                            if (null != drsDuplicate && drsDuplicate.Length > 0)
                            {
                                foreach (DataRow dr in drsDuplicate)
                                {
                                    TimeSpan endTime;
                                    int hr = 24;
                                    int min;
                                    int sec;
                                    //Add 24 hrs with end time
                                    //When Start time is less than end time;
                                    if ((TimeSpan)dr["Begin"] > (TimeSpan)dr["End"])
                                    {
                                        endTime = (TimeSpan)dr["End"];
                                        hr = endTime.Hours + 24;
                                        min = endTime.Minutes;
                                        sec = endTime.Seconds;
                                        if (hr < startime.Hour)
                                            drList.Add(dr);
                                        else if (hr == startime.Hour)
                                        {
                                            if (min < startime.Minute)                                            
                                                drList.Add(dr);                                            
                                            else if (min == startime.Minute)
                                            {
                                                if (sec < startime.Second)                                                
                                                    drList.Add(dr);                                               
                                            }
                                        }
                                    }
                                    else
                                    {
                                        if ((TimeSpan)dr["End"] < StartTime)                                         
                                            drList.Add(dr);
                                    }
                                }
                            }
                            if (drList.Count > 0)
                            {
                                TimeSpan endTime;
                                int hr = 24;
                                int min;
                                int sec;
                                //Add 24 hrs with end time
                                //When Start time is less than end time;
                                bool bCheck = false;
                                if ((TimeSpan)drList[0]["Begin"] > (TimeSpan)drList[0]["End"])
                                {
                                    endTime = (TimeSpan)drList[0]["End"];
                                    hr = endTime.Hours + 24;
                                    min = endTime.Minutes;
                                    sec = endTime.Seconds;
                                    if (hr < startime.Hour)                                    
                                        bCheck = true;                                   
                                    else if (hr == startime.Hour)
                                    {
                                        if (min < startime.Minute)                                        
                                            bCheck = true;                                        
                                        else if (min == startime.Minute)
                                        {
                                            if (sec < startime.Second)                                            
                                                bCheck = true;
                                           
                                        }
                                    }
                                }
                                else
                                {
                                    if ((TimeSpan)drList[0]["End"] < StartTime)                                   
                                        bCheck = true;
                                }
                                if (bCheck)
                                {
                                    cycle++;
                                    MaxVRoomNumber = j;
                                    k = 1;
                                    dt.Rows[i].BeginEdit();
                                    dt.Rows[i]["Cycle"] = cycle;
                                    dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                                    dt.Rows[i].EndEdit();
                                    UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                                    break;
                                }
                            }
                        }
                        if (k == 0)
                        {
                            cycle = 1;
                            MaxVRoomNumber = MaxVRoom(DyName);
                            MaxVRoomNumber++;
                            dt.Rows[i].BeginEdit();
                            dt.Rows[i]["Cycle"] = cycle;
                            dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                            dt.Rows[i].EndEdit();
                            UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                        }
                    }
                    else
                    {
                        MaxVRoomNumber = MaxVRoom(DyName);
                        MaxVRoomNumber++;
                        cycle = 1;
                        dt.Rows[i].BeginEdit();
                        dt.Rows[i]["Cycle"] = cycle;
                        dt.Rows[i]["VRoom"] = MaxVRoomNumber;
                        dt.Rows[i].EndEdit();
                        UpdateCalSignUp(cycle, MaxVRoomNumber, SignUpID, DyName);
                    }

                }
            }
        }
        catch (Exception ex)
        {
           // Response.Write(ex.ToString());
        }
    }

    public int MaxCycleByVirtual(int VRoomNumber,string DyName)
    {
        try
        {
            int s;
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString()
              , CommandType.Text, "select isnull(max(Cycle),0) from CalSignupTemp where VRoom=" + VRoomNumber + " and EventYear='" + DDyear.SelectedValue + "' and  Accepted = 'Y' and EventId=13 and Day='" + DyName + "'");
            DataTable dt = ds.Tables[0];
            if (dt.Rows.Count > 0)
            {
                s = Convert.ToInt16(dt.Rows[0]["column1"].ToString());
                return s;
            }
            else
            {
                return 1;
            }
        }
        catch (Exception ex)
        {
            throw ex;
        }
    }
    protected void starttime1()
    {
        try
        {
            daysx = new string[7];
            daysx[0] = "Friday";
            daysx[1] = "Monday";
            daysx[2] = "Saturday";
            daysx[3] = "Sunday";
            daysx[4] = "Thursday";
            daysx[5] = "Tuesday";
            daysx[6] = "Wednesday";
            foreach (string s in daysx)
            {
                DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,
                       "select C.SignUpID,C.ProductID,E.Duration from CalSignupTemp C inner join EventFees E on C.ProductID=E.ProductID where E.EventYear='" + DDyear.SelectedValue + "' and C.Accepted = 'Y' and Day='" + s + "'");
                DataTable dt1 = ds1.Tables[0];
                if (dt1.Rows.Count > 0)
                {
                    for (int i = 0; i < dt1.Rows.Count; i++)
                    {
                        int poductid = Convert.ToInt16(dt1.Rows[i]["ProductID"].ToString());
                        string Duration1 = dt1.Rows[i]["Duration"].ToString();
                        int signid = Convert.ToInt16(dt1.Rows[i]["SignUpID"].ToString());
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,
                        "update CalSignupTemp set [begin]=cast([Time]as datetime)-cast('" + ViewState["Preclass"].ToString() + "' as datetime), [End]=cast([Time]as datetime)+cast('" + ViewState["Postclass"].ToString() + "' as datetime)-cast('00:02:00' as datetime)+cast(DATEADD(mi, ('" + Duration1 + "' - FLOOR('" + Duration1 + "')) * 60, DATEADD(hh, FLOOR('" + Duration1 + "'), CAST ('00:00:00' AS TIME))) as datetime) where SignUpID='" + signid + "'");
                    }
                }
            }
        }

        catch (Exception ex)
        {
          //  Response.Write(ex.ToString());
        }
    }
    public string process(object myval)
    {
        if (myval.ToString().Length == 1)
        {
            myval = "northsouth." + "0" + myval.ToString() + "@live.com";
            return myval.ToString();
        }
        else
        {
            return "northsouth." + myval.ToString() + "@live.com";
        }
    }

    public string processpwd(object myval)
    {
        if (myval.ToString().Length == 1)
        {
            myval = "nsfaccount." + "0" + myval.ToString();
            return myval.ToString();
        }
        else
        {
            return "nsfaccount" + myval.ToString();
        }
    }
    public int MaxVRoom(string DyName)
    {
        try
        {
            object oRoomNumber = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text,
                "select Isnull(max(VRoom),0) from CalSignupTemp where EventYear='" + DDyear.SelectedValue + "'  and Accepted = 'Y' and  [Day]='" + DyName + "' and EventId=13 ");

            if (!DBNull.Value.Equals(oRoomNumber) && Convert.ToInt32(oRoomNumber) > 0)
            {
                return Convert.ToInt32(oRoomNumber);
            }
            else
            { return 0; }
        }
        catch
        {
        }
        return 1;
    }
    public int CheckDuplicateVRoom(int cycle, int vir, DateTime StartTime,string DyName)
    {
        try
        {
            Object oEndTime = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "sp_chkdup",
                new SqlParameter("@start", StartTime), new SqlParameter("@rownum", rownum),
                 new SqlParameter("@cycle", cycle), new SqlParameter("@vr", vir), new SqlParameter("@da", DyName));

            if (!DBNull.Value.Equals(oEndTime))
            {
                DateTime EndTime = Convert.ToDateTime(oEndTime);
                if (StartTime > EndTime)
                {
                    return 1;
                }

                else
                {
                    return 0;
                }
            }
        }
        catch
        {
        }
        return 1;
    }

    protected void Button3_Click(object sender, EventArgs e)
    {   
    }

    #region UserDefined Methods
    private string GetPreviousNextQueryByDay(string DayName)
    {
        return "Select   top 250 ROW_NUMBER()  over(order by C.Time) AS RowNum,C.SignUpID,  C.Cycle,C.VRoom from CalSignupTemp C "
            + " inner Join  EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID  "
            + "  inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and DAY='" + DayName + "'"
            + " and  c.EventID='13'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "'  group BY c.SignUpID,c.Time ,c.MemberID,c.EventYear,c.ProductCode,C.ProductGroupCode,c.Level,c.Accepted, I.FirstName,i.LastName,E.Duration,C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom,c.Time ";
    }
   
    private string GetQueryByDay(string DayName)
    {
        string Query = "Select top 250 ROW_NUMBER() over(order by  C.Time,E.Duration)  AS RowNum, C.SignUpID, C.MemberID, C.EventYear, C.ProductCode, C.ProductGroupCode, C.Level, c.Accepted,I.FirstName,I.LastName,E.Duration, C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom,0 as Previous,0 as Next,UserID, Pwd as Password from CalSignupTemp C "
               + " inner Join  EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID    inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and c.EventID='13'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "'";
        if (!string.IsNullOrEmpty(DayName))
        {
            Query += "and  Day in ('" + DayName + "')";
        }
        else
        {
            Query = "Select top 250  0 as RowNum, C.SignUpID, C.MemberID, C.EventYear, C.ProductCode, C.ProductGroupCode, C.Level, c.Accepted,I.FirstName,I.LastName,E.Duration,0 as D, C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom,0 as Previous,0 as Next ,UserID,PWD as Password from CalSignupTemp C "
              + " inner Join EventFees E on E.productid=C.productid and E.ProductGroupID=C.ProductGroupID    inner join IndSpouse I on I.AutoMemberID =C.Memberid where c.EventYear='" + DDyear.SelectedValue + "'  and c.EventID='13'  and C.Accepted = 'Y'  and e.EventID='13' and E.EventYear='" + DDyear.SelectedValue + "' order by C.Day, C.Time,C.VRoom";
        }
         // + "  group BY c.SignUpID,c.Time ,c.MemberID,c.EventYear,c.ProductCode,c.Level,c.Accepted, I.FirstName,i.LastName,E.Duration,C.Day,C.Time,C.[Begin],C.[End],C.Cycle,C.VRoom ";
        return Query;
    }
    private DataTable GetVRCalculateProc(DateTime StartTime, int RowNumber)
    {
        try
        {
            DataSet dsVRoom = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(),
                        CommandType.StoredProcedure, "sp_vrcalc", new SqlParameter("@start", StartTime), new SqlParameter("@rownum", RowNumber));
            if (null != dsVRoom && dsVRoom.Tables.Count > 0)
            {
                return dsVRoom.Tables[0];
            }
        }
        catch
        {
        }
        return null;
    }
    private void UpdateCalSignUp(int Cycle, int VRoom, int SignUpID, string DayName)
    {
        try
        {
            DataTable dtVRoomLookUp = (DataTable)ViewState["tblVRoom"];
            DataRow[] drResult;  
            string cmdText;
            cmdText = "update CalSignupTemp set Cycle=" + Cycle + " ,VRoom=" + VRoom + " where SignUpID=" + SignUpID + " and [Day]='" + DayName + "' AND EventYear='" + DDyear.SelectedValue + "'";

            if (!DBNull.Value.Equals(dtVRoomLookUp))
            {
               drResult = dtVRoomLookUp.Select("VRoom =" + VRoom);
               if (drResult.Length > 0)
               {
                   cmdText = "update CalSignupTemp set Cycle=" + Cycle + " ,VRoom=" + VRoom + ", UserID='" + drResult[0]["UserId"].ToString() + "',PWD = '" + drResult[0]["Pwd"].ToString() + "' where SignUpID='" + SignUpID + "' and [Day]='" + DayName + "' AND EventYear='" + DDyear.SelectedValue + "'";
              
               }
            }
             SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text,cmdText);
        }
        catch (Exception ex)
        {
           // Response.Write(ex.ToString());
        }
    }
    private void GetVRoomLookUpDetails()
    {
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT VROOM,UserId,Pwd FROM VirtualRoomLookUp");
            ViewState["tblVRoom"] = ds.Tables[0];
            
        }
        catch
        {
        }
    }

    #endregion
    protected void excel_Click(object sender, EventArgs e)
    {
        ExportToExcel();
    }
    protected void DDpre_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["Preclass"] = "00:"+DDpre.SelectedItem.Value+":00";
       
    }
    protected void DDpost_SelectedIndexChanged(object sender, EventArgs e)
    {
        ViewState["Postclass"] = "00:" + DDpost.SelectedItem.Value + ":00";
        if (DDpost.SelectedItem.Value =="60")
        {
            ViewState["Postclass"] = "01:00:00";
        }
    }
    protected void LinkButton1_Click(object sender, EventArgs e)
    {
        hlinkChapterFunctions.Visible = true;
        Btnsubmit.Visible = true;
        lblpost.Visible = true;
        DDpost.Visible = true;
        lblpre.Visible = true;
        DDpre.Visible = true;
        DDyear.Visible = true;
        Label1.Visible = true;
        excel.Visible = false;
        gridview_VR.Visible = false;

    }
}























