﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="FocusGroup.aspx.cs" Inherits="FocusGroup" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; width: 1050px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Focus Group
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center">
        <tr>
            <td align="center">
                <div align="center">
                    <div style="float: left;">
                        <table>
                            <tr>
                                <td style="font-weight: bold;">Cluster</td>
                                <td>
                                    <asp:DropDownList ID="DDLCluster" runat="server" Style="width: 90px;" OnSelectedIndexChanged="DDLCluster_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList></td>
                                <td style="font-weight: bold;">Chapter</td>
                                <td>
                                    <asp:DropDownList ID="DDLChapter" runat="server" Style="width: 90px;">
                                    </asp:DropDownList></td>
                                <td style="font-weight: bold;">Type of Partcipants</td>
                                <td>
                                    <asp:DropDownList ID="DDLParticipantsType" runat="server" OnSelectedIndexChanged="DDLParticipantsType_SelectedIndexChanged" AutoPostBack="true" Style="width: 90px;">
                                        <asp:ListItem Value="Partcipants">Partcipants</asp:ListItem>
                                        <asp:ListItem Value="Non-Participants">Non-Participants</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                        </table>
                    </div>
                    <div id="dvParticipants" runat="server" style="float: left;">
                        <table>
                            <tr>
                                <td style="font-weight: bold;">From (Years)</td>
                                <td>
                                    <asp:DropDownList ID="DDLFrom" runat="server" OnSelectedIndexChanged="DDLFrom_SelectedIndexChanged" AutoPostBack="true" Style="width: 90px;">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5 Years or more">5 Years or more</asp:ListItem>

                                    </asp:DropDownList><b></b></td>

                                <td style="font-weight: bold;" id="tdTo" runat="server">To (Years)</td>
                                <td id="tdToCtrl" runat="server">
                                    <asp:DropDownList ID="DDLTo" runat="server">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                    </asp:DropDownList><b></b></td>
                                <td style="font-weight: bold;">Target (Years)</td>
                                <td>
                                    <asp:DropDownList ID="DDLTarget" runat="server">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                    </asp:DropDownList><b></b></td>
                            </tr>
                        </table>
                    </div>
                    <div id="dvNonParticipants" runat="server" style="float: left;" visible="false">
                        <table>
                            <tr>
                                <td style="font-weight: bold;">Scenario</td>
                                <td>
                                    <asp:DropDownList ID="DDLScenario" runat="server" Width="200px">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1) First time in 2014, not in 2015; first time in 2013, not 2015; first time in 212, but not    in 2015
                                        </asp:ListItem>
                                        <asp:ListItem Value="2">2) 3 out of 6 years, but not in 2013 & 2014</asp:ListItem>
                                        <asp:ListItem Value="3">3) 4 out of last 10 years, but not in 2012, 2013, 2014
                                        </asp:ListItem>
                                    </asp:DropDownList><b></b></td>
                                <%-- <td style="font-weight: bold;">Output: </td>--%>
                                <%-- <td>
                                    <asp:DropDownList ID="DDlStart" runat="server">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>

                                    </asp:DropDownList><b></b></td>

                                <td style="font-weight: bold;" id="td1" runat="server">Out of</td>
                                <td id="td2" runat="server">
                                    <asp:DropDownList ID="DDlEnd" runat="server">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="1">1</asp:ListItem>
                                        <asp:ListItem Value="2">2</asp:ListItem>
                                        <asp:ListItem Value="3">3</asp:ListItem>
                                        <asp:ListItem Value="4">4</asp:ListItem>
                                        <asp:ListItem Value="5">5</asp:ListItem>
                                        <asp:ListItem Value="6">6</asp:ListItem>
                                        <asp:ListItem Value="7">7</asp:ListItem>
                                        <asp:ListItem Value="8">8</asp:ListItem>
                                        <asp:ListItem Value="9">9</asp:ListItem>
                                        <asp:ListItem Value="10">10</asp:ListItem>
                                    </asp:DropDownList>
                                    <b>(Years)</b></td>--%>
                            </tr>
                        </table>
                    </div>
                    <div id="dvSubmit" runat="server" style="float: left;">
                        <asp:Button ID="BtnSubmit" runat="server" Text="Submit" OnClick="BtnSubmit_Click" />
                        <asp:Button ID="BtnExportToExcel" runat="server" Text="Export To Excel" Visible="false" OnClick="BtnExportToExcel_Click" />
                    </div>
                </div>
            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <span style="font-weight: bold; color: red;" id="spnErrMsg" runat="server"></span>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <span style="font-weight: bold;" id="spnTitle" runat="server"></span>
    </div>
    <div align="center">
        <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdFocusGroup" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" EnableModelValidation="True" PageSize="100" AllowPaging="true" OnPageIndexChanging="GrdFocusGroup_PageIndexChanging">
            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="ContestYear" HeaderText="Year"></asp:BoundField>--%>
                <asp:BoundField DataField="AutoMemberID" HeaderText="MemberID"></asp:BoundField>

                <asp:BoundField DataField="FirstName" HeaderText="First Name"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Email"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="HPhone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="CPhone"></asp:BoundField>

                <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>

                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
        <span runat="server" id="spnStatus" style="color: red;" visible="false">No record exists</span>
    </div>
</asp:Content>
