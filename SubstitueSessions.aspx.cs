﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections;
using VRegistration;

public partial class SubstitueSessions : System.Web.UI.Page
{
    //Created By: Michael Simson
    //Modified By: Michael Simson
    //Modified Date: 02/19/2016
    //Reason: Code optimizing

    //purpose of this application: Assigning substitute coach for the particular recurring session in this application.

    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                loadyear();
                loadPhase(ddlPhase);
                fillMeetingGrid("All");
                LoadEvent(ddEvent);

            }
        }
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }



    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;

            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlPhase.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            //ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 0;
                ddlProduct.Enabled = false;
                loadLevel();

                if (Session["RoleID"].ToString() == "88")
                {

                }


            }
            else
            {

                ddlProduct.Enabled = true;
            }

        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillProduct();

    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {

        LoadSessionNo();

    }

    protected void ddlSession_SelectedIndexChanged(object sender, EventArgs e)
    {

        populateRecClassDate();

    }

    protected void DDlSubDate_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillSubstituteCoach();

    }

    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedValue + "' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }

    public void fillSubstituteCoach()
    {
        try
        {

            string cmdText = "";

            string roleId = string.Empty;
            if (Session["RoleID"] != null)
            {
                roleId = Session["RoleID"].ToString();
            }

            if (roleId == "88")
            {

                cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.Accepted='Y' and V.MemberID<>" + ddlCoach.SelectedValue + " and V.Semester='" + ddlPhase.SelectedValue + "' order by ID.FirstName ASC";
            }
            else if (roleId == "1" || roleId == "2" || roleId == "96" || roleId == "89")
            {
                cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + "  and V.Accepted='Y' and V.MemberID<>" + ddlCoach.SelectedValue + " and V.Semester='" + ddlPhase.SelectedValue + "' order by ID.FirstName ASC";
            }

            DataSet ds = new DataSet();
            try
            {

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (ds.Tables[0] != null)
                {

                    DDLSubstituteCoach.DataSource = ds;
                    DDLSubstituteCoach.DataTextField = "Name";
                    DDLSubstituteCoach.DataValueField = "AutoMemberID";
                    DDLSubstituteCoach.DataBind();
                    DDLSubstituteCoach.Items.Insert(0, new ListItem("Select", "0"));
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                    {
                        DDLSubstituteCoach.Enabled = true;
                    }
                    else
                    {

                    }

                }

            }
            catch (Exception ex)
            {
            }
        }
        catch
        {
        }
    }

    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {


        loadLevel();

    }

    public void loadLevel()
    {

        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and V.Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
                LoadSessionNo();
            }
        }
    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 1))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;

                ddchapter.SelectedValue = "112";
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    fillCoach();
                }
                else
                {
                    fillCoach();
                    fillProductGroup();
                    fillSubstituteCoach();
                }
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }

        }
        else
        {

            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);
        fillCoach();
    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validatemeeting() == "1")
        {
            if (CheckWeekNoExists() > 0)
            {
                ValidateSubstituteSession();
            }
            else
            {
                lblerr.Text = "Regular class not yet added for the week# " + TxtWeekNo.Text + "";
            }

        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        //tblAddNewMeeting.Visible = false;
        TxtWeekNo.Enabled = true;
        TxtWeekNo.Text = "0";
        DDLSubstituteCoach.SelectedValue="0";
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid("false");
        btnAddNewMeeting.Visible = false;
        tblAddNewMeeting.Visible = false;
    }


    //validate and assign substitute coach for the original coach
    public void ValidateSubstituteSession()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;


            CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.SignupID,CS.MeetingKey,CS.HostJoinURL,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.SubstituteMeetKey,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey,VC.VideoConfLogID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join WebConfLog VC on (VC.SessionKey=CS.MeetingKey) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.Vroom is not null  and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo='" + ddlSession.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "' and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.MemberID=" + ddlCoach.SelectedValue + "";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (ds.Tables[0] != null)
            {



                string Meetinkey = string.Empty;
                string webConfLogID = string.Empty;
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        count++;

                        int IsSameDay = 0;
                        int IsSameTime = 0;
                        string WebExID = dr["UserID"].ToString();
                        string Pwd = dr["PWD"].ToString();
                        webConfLogID = dr["VideoConfLogID"].ToString();
                        int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                        string ScheduleType = dr["ScheduleType"].ToString();
                        Meetinkey = dr["meetingKey"].ToString();
                        string year = dr["EventYear"].ToString();
                        string eventID = dr["EventID"].ToString();
                        string chapterId = ddchapter.SelectedValue;
                        string ProductGroupID = dr["ProductGroupID"].ToString();
                        string ProductGroupCode = dr["ProductGroupCode"].ToString();
                        string ProductID = dr["ProductID"].ToString();
                        string ProductCode = dr["ProductCode"].ToString();
                        string Semester = dr["Semester"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string MeetingPwd = "training";

                        string Date = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string Time = dr["Time"].ToString();
                        string Day = dr["Day"].ToString();
                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        string MeetingURL = dr["HostJoinURL"].ToString();
                        string signupID = dr["SignupID"].ToString();
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }

                        if (txtDuration.Text != "")
                        {
                            int Duration = Convert.ToInt32(txtDuration.Text);
                        }
                        string timeZoneID = string.Empty;

                        timeZoneID = ddlTimeZone.SelectedValue;

                        string TimeZone = ddlTimeZone.SelectedItem.Text;
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;
                        string sDateFrom = BeginTime;
                        string sDateTo = EndTime;
                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                        {
                            TimeSpan TS = dTo - dFrom;
                            Mins = TS.TotalMinutes;

                        }
                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }



                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }

                        if (timeZoneID == "4")
                        {
                            TimeZone = "EST/EDT – Eastern";
                        }
                        else if (timeZoneID == "7")
                        {
                            TimeZone = "CST/CDT - Central";
                        }
                        else if (timeZoneID == "6")
                        {
                            TimeZone = "MST/MDT - Mountain";
                        }
                        string userID = Session["LoginID"].ToString();


                        string cmdText = "";

                        string meetingURL = hdnHostURL.Value;

                        string practiseDate = txtDate.Text;

                        string strQuery;
                        SqlCommand cmd;

                        strQuery = "update WebConfLog set SubstituteCoachID=@SubCoachID where VideoConfLogID=@VideoConfLogID";
                        cmd = new SqlCommand(strQuery);
                        cmd.Parameters.AddWithValue("@SubCoachID", DDLSubstituteCoach.SelectedValue);
                        cmd.Parameters.AddWithValue("@VideoConfLogID", webConfLogID);
                        NSFDBHelper objNSF = new NSFDBHelper();
                        objNSF.InsertUpdateData(cmd);

                        //CmdText = "update WebConfLog set SubstituteCoachID=" + DDLSubstituteCoach.SelectedValue + " where VideoConfLogID=" + webConfLogID + "";
                        //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                        strQuery = "update CalSignup set SubstituteCoachID=@SubCoachID where SignupID=@SignupID";
                        cmd = new SqlCommand(strQuery);
                        cmd.Parameters.AddWithValue("@SubCoachID", DDLSubstituteCoach.SelectedValue);

                        cmd.Parameters.AddWithValue("@SignupID", signupID);

                        objNSF.InsertUpdateData(cmd);


                        CmdText = "Update coachClassCal set  Substitute=" + DDLSubstituteCoach.SelectedValue + ", ModifiedBy='" + Session["LoginID"].ToString() + "', ModifyDate=GetDate() where MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " AND Semester='" + ddlPhase.SelectedValue + "' and WeekNo=" + TxtWeekNo.Text + " and ClassType='Regular' and Status='On'";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);


                        //if (ddlProductGroup.SelectedValue != "42")
                        //{

                        //    CmdText = "select CoachPaperId from CoachPapers where EventYear=" + ddYear.SelectedValue + " and EventId=13 and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and WeekId=" + TxtWeekNo.Text + " and DocType='Q'";
                        //    int iCoachPaperId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, CmdText));
                        //    if (iCoachPaperId > 0)
                        //    {

                        //        CmdText = "select coachRelID from coachreldates where EventYear=" + ddYear.SelectedValue + "  and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and Session=" + ddlSession.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and CoachPaperID=" + iCoachPaperId + "";
                        //        int coachRelID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, CmdText));
                        //        if (coachRelID > 0)
                        //        {
                        //            DateTime QRDate = Convert.ToDateTime(DDlSubDate.SelectedValue);
                        //            DateTime QDDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(5);

                        //            DateTime ARDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(7);
                        //            DateTime SRDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(-1);

                        //            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                        //            string sqlCommand = "usp_Update_CoachRelDate";
                        //            SqlParameter[] param = new SqlParameter[8];
                        //            param[0] = new SqlParameter("@CoachRelID", iCoachPaperId);
                        //            param[1] = new SqlParameter("@QReleaseDate", QRDate.ToShortDateString());
                        //            param[2] = new SqlParameter("@QDeadlineDate", QDDate.ToShortDateString());
                        //            param[3] = new SqlParameter("@AReleaseDate", ARDate.ToShortDateString());
                        //            param[4] = new SqlParameter("@SReleaseDate", SRDate.ToShortDateString());
                        //            param[5] = new SqlParameter("@SessionNO", ddlSession.SelectedValue);
                        //            param[6] = new SqlParameter("@ModifiedBy", Int32.Parse(Session["LoginID"].ToString()));
                        //            param[7] = new SqlParameter("@ModifyDate", System.DateTime.Now);
                        //            SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                        //        }
                        //        else
                        //        {
                        //            DateTime QRDate = Convert.ToDateTime(DDlSubDate.SelectedValue);
                        //            DateTime QDDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(5);

                        //            DateTime ARDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(7);
                        //            DateTime SRDate = Convert.ToDateTime(DDlSubDate.SelectedValue).AddDays(-1);
                        //            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

                        //            string sqlCommand = "usp_Insert_CoachRelDate";
                        //            SqlParameter[] param = new SqlParameter[10];
                        //            param[0] = new SqlParameter("@CoachPaperId", iCoachPaperId);
                        //            param[1] = new SqlParameter("@Semester", "1");
                        //            param[2] = new SqlParameter("@SessionNo", ddlSession.SelectedValue);
                        //            param[3] = new SqlParameter("@QReleaseDate", QRDate.ToShortDateString());
                        //            param[4] = new SqlParameter("@QDeadlineDate", QDDate.ToShortDateString());
                        //            param[5] = new SqlParameter("@AReleaseDate", ARDate.ToShortDateString());
                        //            param[6] = new SqlParameter("@SReleaseDate", SRDate.ToShortDateString());
                        //            param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
                        //            param[8] = new SqlParameter("@CreatedBy", Int32.Parse(Session["LoginID"].ToString()));
                        //            param[9] = new SqlParameter("@MemberId", Int32.Parse(ddlCoach.SelectedItem.Value));

                        //            SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                        //        }
                        //    }
                        //    else
                        //    {
                        //    }
                        //}

                        fillMeetingGrid("false");
                        lblSuccess.Text = "Substitute coach assigned for the session successfully";
                    }
                }
                else
                {
                    lblerr.Text = "No sessions are scheduled for the coach";
                }

            }



        }
        catch (Exception ex)
        {

        }

    }


    //populating all substitue sessions from SQL table "WebConfLog"
    public void fillMeetingGrid(string IsFilter)
    {
        try
        {

            btnAddNewMeeting.Visible = false;
            string cmdtext = "";
            DataSet ds = new DataSet();
            spnTable1Title.Visible = true;
            GrdMeeting.Visible = true;

            cmdtext = " select CL.EventYear,E.EventCode,'coaching, US' as ChapterCode,IP.FirstName +' '+ IP.LastName as SubCoach,IP1.FirstName +' '+ IP1.LastName as Coach,CL.ProductID,CL.ProductGroupID,CL.EventID,VC.ChapterID,CS.ProductGroupCode,CS.ProductCode,CL.Date,CL.Time,CL.Semester,CL.Level,VC.SessionKey,VC.EndDate,VC.BeginTime,VC.EndTime,Cl.Duration,VC.TimeZone,VC.TimeZoneID,VC.Status,VC.MeetingUrl, CL.memberId, Cl.Substitute,VL.UserId,VL.PWD,CL.Day, IP.FirstName,IP.LastName,CL.Date as SubstituteDate, Cl.WeekNo,VL.Vroom,CS.Time, VL.HostID, Cl.SessionNo as Session, VC.MeetingPwd, vc.VideoConfLogID   from CoachClassCal CL inner join Indspouse IP on (CL.Substitute=IP.AutomemberId) inner join Indspouse IP1 on (Cl.MemberId=IP1.AutoMemberId) inner join Event E on (Cl.EventId=E.EventId) left join WebConfLog VC on (CL.memberID=VC.MemberId and VC.SessionType='Recurring Meeting' and Cl.Substitute is not null and vc.ProductGroupId=cl.productGroupid and vc.Productid=cl.productid and vc.level=cl.level and vc.session=cl.Sessionno and vc.Semester=cl.Semester and vc.Eventyear=cl.Eventyear) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) left join VirtualRoomLookUp VL on (VL.Vroom=CS.Vroom) where (CL.Substitute is not null and CL.Substitute <>'' and CL.Substitute <>0) and CL.EventYear='" + ddYear.SelectedValue + "' and (Cl.ClassType='Regular' or Cl.ClassType='Holiday') and Cl.Status='On' and Cl.Semester='" + ddlPhase.SelectedValue + "'";

            if (IsFilter == "true")
            {

                if (ddlFilterBy.SelectedValue == "Year")
                {

                    cmdtext += " and Cl.EventYear=" + ddlYearFilter.SelectedValue + "";
                }
                else if (ddlFilterBy.SelectedValue == "ProductGroup")
                {
                    cmdtext += " and Cl.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
                }
                else if (ddlFilterBy.SelectedValue == "Product")
                {
                    cmdtext += " and Cl.ProductID=" + ddlProductFilter.SelectedValue + "";
                }
                else if (ddlFilterBy.SelectedValue == "Coach")
                {

                    cmdtext += " and Cl.MemberID=" + ddlCoachFilter.SelectedValue + "";
                }
            }
            else if (IsFilter == "false")
            {
                cmdtext += " and Cl.memberid=" + ddlCoach.SelectedValue + " and Cl.ProductGroupID=" + ddlProductGroup.SelectedValue + " and Cl.ProductID=" + ddlProduct.SelectedValue + " and Cl.Level='" + ddlLevel.SelectedValue + "' and Cl.Sessionno='" + ddlSession.SelectedValue + "' and Cl.Semester='" + ddlPhase.SelectedValue + "' and Cl.Status='On'";
            }

            cmdtext += " order by Cl.EventYear DESC, Cl.DAte DESC, SubCoach ASC";

            try
            {

                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (ds.Tables[0] != null)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        spnTblMsg.InnerText = "";
                        GrdMeeting.DataSource = ds;
                        GrdMeeting.DataBind();
                        fillPgGroupSection();
                        fillPrdSection();
                        fillCoachSection();
                        for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            if (Session["RoleID"].ToString() == "1")
                            {
                                if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                                {
                                    ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                    ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                                }
                                else
                                {
                                    ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                    ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                                }
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }

                            string startDate = GrdMeeting.Rows[i].Cells[18].Text;
                            DateTime dtStartDate = Convert.ToDateTime(startDate);
                            string currentDate = DateTime.Now.ToShortDateString();
                            DateTime dtCurrentDate = Convert.ToDateTime(currentDate);
                            if (dtStartDate >= dtCurrentDate)
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = false;
                            }
                        }
                    }
                    else
                    {
                        GrdMeeting.DataSource = ds;
                        GrdMeeting.DataBind();
                        spnTblMsg.InnerText = "No record exists.";
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        catch
        {
        }
    }

    public string ValidateCreateSession()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }
        else if (DDLSubstituteCoach.SelectedValue == "0" || DDLSubstituteCoach.SelectedValue == "")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (ddlCoach.SelectedValue == "0" || ddlCoach.SelectedValue == "")
        {
            lblerr.Text = "Please select Substitute Coach";
            retVal = "-1";
        }
        return retVal;
    }

    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "-1" || ddchapter.SelectedValue == "")
        {
            lblerr.Text = "Please select Chapter";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "0")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlSession.SelectedValue == "-1" || ddlSession.SelectedValue == "0")
        {
            lblerr.Text = "Please select Session No";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }

        else if (TxtWeekNo.Text == "")
        {
            lblerr.Text = "Please enter Regular class week#";
            retVal = "-1";
        }
        else if (Convert.ToInt32(TxtWeekNo.Text) < 0)
        {
            lblerr.Text = "Week# should be greater than 0.";
            retVal = "-1";
        }
        else if (DDLSubstituteCoach.SelectedValue == "" || DDLSubstituteCoach.SelectedValue == "0")
        {
            lblerr.Text = "Please select Substitute Coach";
            retVal = "-1";
        }
        else if (ddlCoach.SelectedValue == DDLSubstituteCoach.SelectedValue)
        {
            retVal = "-1";
            lblerr.Text = "Coach name and Substitute Coach name should not be same.";
        }




        return retVal;
    }


    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                if (hdnMeetingStatus.Value == "SUCCESS")
                {



                    string CmdText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", makeUpAttendeeID=" + hdnMeetingAttendeeID.Value + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y' and Level='" + Level + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }

                if (countEmail > 0)
                {

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert('" + ProductCode + "','" + hdnCoachname.Value + "');", true);


                }
                else
                {
                    lblerr.Text = "Only authorized child can join ito the training session.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string WeekNo = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                WeekNo = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWeekNo") as Label).Text;
                hdnWeekNo.Value = WeekNo;
                TxtWeekNo.Text = WeekNo;
                TxtWeekNo.Enabled = false;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;
                loadLevel();

                btnCancel.Visible = true;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                LoadSessionNo();
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;

                fillSubstituteCoach();
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;
                txtDate.Text = GrdMeeting.Rows[selIndex].Cells[15].Text;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;
                txtTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                txtEndTime.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[11].Text;
                //tblAddNewMeeting.Visible = true;
                string subDate = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSubDate") as Label).Text;
                subDate = Convert.ToDateTime(subDate).ToString("MM/dd/yyyy");
                if (subDate != "")
                {
                    DDlSubDate.SelectedValue = subDate;
                }
                string subCoachID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSubstituteCoachID") as Label).Text;
                DDLSubstituteCoach.SelectedValue = subCoachID;
                btnCreateMeeting.Text = "Assign Substitute Coach";
                hdnSessionKey.Value = sessionKey;
                txtEndDate.Text = GrdMeeting.Rows[selIndex].Cells[16].Text;


                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                ddlCoach.Enabled = false;
                ddlProductGroup.Enabled = false;
                ddlProduct.Enabled = false;
                ddlLevel.Enabled = false;
                ddlSession.Enabled = false;

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[14].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string coachName = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                hdnSessionKey.Value = sessionKey;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now.AddMinutes(60);
                double mins = 40.0;
                string duration = "-" + ((Label)GrdMeeting.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                int iduration = Convert.ToInt32(duration);
                if (duration == "-")
                {
                    iduration = 120;
                }
                else
                {
                    iduration = Convert.ToInt32(duration);
                }

                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    if (mins < iduration)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                    }
                    else
                    {
                        hdnHostID.Value = hostID;
                        try
                        {
                            listLiveMeetings();
                        }
                        catch
                        {
                        }
                        getmeetingIfo(sessionKey, hostID);
                        string cmdText = "";
                        getmeetingIfo(sessionKey, hostID);

                        cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        string meetingLink = hdnHostURL.Value;
                        try
                        {
                            CoachingExceptionLog.createExceptionLog(coachName, "Success", "", "Substitute Sessions");
                        }
                        catch
                        {
                        }


                        hdnZoomURL.Value = meetingLink;
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                    }
                }
                else
                {
                    try
                    {
                        CoachingExceptionLog.createExceptionLog(coachName, "failure", "", "Substitute Sessions");
                    }
                    catch
                    {
                    }
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                }
            }
            else if (e.CommandName == "unassigned")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;

                string WebExID = string.Empty;
                string WebExPwd = string.Empty;

                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[14].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnSessionNo.Value = GrdMeeting.Rows[selIndex].Cells[10].Text;
                string WeekNo = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWeekNo") as Label).Text;
                hdnWeekNo.Value = WeekNo;

                string sessionStartDate = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSubDate") as Label).Text;
                hdnStartDate.Value = sessionStartDate;

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteMeeting();", true);
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", "", "Substitute Sessions");
        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid("false");
    }

    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        try
        {

            GrdMeeting.EditIndex = e.RowIndex;
            int rowIndex = e.RowIndex;
            GrdMeeting.EditIndex = -1;
            string sessionKey = string.Empty;
            sessionKey = GrdMeeting.Rows[rowIndex].Cells[13].Text;
            hdnSessionKey.Value = sessionKey;
            string WebExID = string.Empty;
            string WebExPwd = string.Empty;
            WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
            WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
            string WebUID = string.Empty;
            string WebUPwd = string.Empty;
            WebUID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebUID") as Label).Text;
            WebUPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("LabelblWebPwd") as Label).Text;
            string Vroom = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblVroom") as Label).Text;

            string WebConfLogID = string.Empty;
            WebConfLogID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebConfLogID") as Label).Text;
            hdnWebConflogID.Value = WebConfLogID;
            hdnSessionKey.Value = sessionKey;
            hdnWebExID.Value = WebUID;
            hdnWebExPwd.Value = WebUPwd;
            hdnVroom.Value = Vroom;
            hdnSubWebExID.Value = WebExID;
            hdnSubWebExPwd.Value = WebExPwd;




            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);

        }
        catch (Exception ex)
        {
        }
    }

    //populate selected coach's children from the SQL table "CoachReg"
    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {
        try
        {

            trChildList.Visible = true;
            DataSet ds = new DataSet();
            string CmdText = "";
            CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level,CR.SubstituteURL,CR.SubstituteAttendeeID,CR.SubstituteRegID from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + ddYear.SelectedValue + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + HdnLevel.Value + "' order by C1.Last_Name, C1.First_Name Asc";



            try
            {
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        GrdStudentsList.DataSource = ds;
                        GrdStudentsList.DataBind();
                        lblChildMsg.Text = "";
                        btnClosetable2.Visible = true;

                        for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                        {
                            if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                            {
                                ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                            }
                            else
                            {
                                ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                            }
                        }
                    }
                    else
                    {
                        GrdStudentsList.DataSource = ds;
                        GrdStudentsList.DataBind();
                        lblChildMsg.Text = "No record found";
                        btnClosetable2.Visible = false;

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        catch
        {
        }
    }





    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {

        DelMeetingAttendee(hdnSessionKey.Value, hdnSubWebExID.Value, hdnSubWebExPwd.Value, hdnVroom.Value);

    }

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlCoach.SelectedValue != "0")
        {
            fillProductGroup();

            fillSubstituteCoach();
        }
    }

    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }


    public string valdiatePriorClassStatus()
    {
        string retVal = "1";
        if (DDlSubDate.SelectedValue != "0" && txtDate.Text != "")
        {
            int selIndex = DDlSubDate.SelectedIndex;
            string recClassPrevDate = DDlSubDate.SelectedValue;
            string cmdText = string.Empty;
            int count = 0;


            if (selIndex == 1)
            {
                DataSet ds = new DataSet();
                string priorClassDate = Convert.ToDateTime(DDlSubDate.SelectedItem.Text).AddDays(-7).ToShortDateString();

                cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + priorClassDate + "'";

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                int holidayCount = 0;
                cmdText = "select count(*) as CountSet from CoachingDateCal where EventYear=" + ddYear.SelectedValue + " and EventID=13 and (ScheduleType='Christmas' or ScheduleType='Thanksgiving'  or ScheduleType='Diwali' or ScheduleType='Halloween') and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and  ('" + priorClassDate + "' between StartDate and EndDate) or (StartDate = '" + priorClassDate + "') or (EndDate='" + priorClassDate + "')";

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        holidayCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                if (count == 0 && holidayCount == 0)
                {
                    retVal = "-1";
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                }
                else
                {

                    if (holidayCount == 0)
                    {
                        count = 1;


                    }
                    else
                    {
                        priorClassDate = Convert.ToDateTime(DDlSubDate.SelectedItem.Text).AddDays(-14).ToShortDateString();

                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + priorClassDate + "'";

                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }
                    }

                    if (holidayCount == 0 && count == 0)
                    {
                    }
                    else if (holidayCount > 0 && count > 0)
                    {
                        try
                        {
                            int cntWeek = 0;
                            cntWeek = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select max(weekno) from coachclasscal where eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and memberID=" + ddlCoach.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + ""));
                            cntWeek = cntWeek + 1;

                            string coachClassText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Reason, CreateDate, CreatedBy)";
                            coachClassText += "select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo,'" + Convert.ToDateTime(DDlSubDate.SelectedItem.Text).AddDays(-7).ToShortDateString() + "',Day,Time,Duration," + cntWeek + "," + cntWeek + ",'Cancelled','" + ddlReason.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + " from calsignup where Eventyear=" + ddYear.SelectedValue + " and MemberId=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Accepted='Y'";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                        }
                        catch
                        {
                        }
                    }

                    if (count > 0)
                    {
                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date between '" + recClassPrevDate + "' and  '" + Convert.ToDateTime(recClassPrevDate).AddDays(7).ToShortDateString() + "' and Status='Makeup'";


                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }

                        if (count == 0)
                        {

                        }
                        else
                        {
                            lblerr.Text = "Duplicate exists.";
                            retVal = "-1";
                        }
                    }
                    else
                    {
                        retVal = "-1";
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                    }

                }
            }
            else if (selIndex > 1)
            {
                recClassPrevDate = DDlSubDate.Items[selIndex - 1].Value;
                cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + recClassPrevDate + "'";
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                int holidayCount = 0;
                cmdText = "select count(*) as CountSet from CoachingDateCal where EventYear=" + ddYear.SelectedValue + " and EventID=13 and (ScheduleType='Christmas' or ScheduleType='Thanksgiving'  or ScheduleType='Diwali' or ScheduleType='Halloween') and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and  ('" + recClassPrevDate + "' between StartDate and EndDate) or (StartDate = '" + recClassPrevDate + "') or (EndDate='" + recClassPrevDate + "')";

                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        holidayCount = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                    }
                }

                if (count == 0 && holidayCount == 0)
                {

                    retVal = "-1";
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                }
                else
                {
                    if (holidayCount == 0)
                    {
                        count = 1;
                        try
                        {

                        }
                        catch
                        {
                        }
                    }
                    else
                    {
                        if (selIndex > 2)
                        {
                            recClassPrevDate = DDlSubDate.Items[selIndex - 1].Value;
                        }
                        else
                        {
                            recClassPrevDate = Convert.ToDateTime(DDlSubDate.Items[selIndex - 1].Value).AddDays(-7).ToShortDateString();
                        }

                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date='" + recClassPrevDate + "'";

                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }
                    }

                    if (holidayCount == 0 && count == 0)
                    {
                    }
                    else if (holidayCount > 0 && count > 0)
                    {
                        try
                        {
                            int cntWeek = 0;
                            cntWeek = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select max(weekno) from coachclasscal where eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and memberID=" + ddlCoach.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + ""));
                            cntWeek = cntWeek + 1;

                            string coachClassText = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, Reason, CreateDate, CreatedBy)";
                            coachClassText += "select MemberID, EventID,EventYear, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester,[Level],SessionNo,'" + DDlSubDate.Items[selIndex - 1].Value + "',Day,Time,Duration," + cntWeek + "," + cntWeek + ",'Cancelled','" + ddlReason.SelectedValue + "',GetDate()," + Session["LoginID"].ToString() + " from calsignup where Eventyear=" + ddYear.SelectedValue + " and MemberId=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and Accepted='Y'";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, coachClassText);
                        }
                        catch
                        {
                        }
                    }

                    if (count > 0)
                    {
                        cmdText = "select count(*) as CountSet from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and Date between '" + DDlSubDate.SelectedItem.Text + "' and  '" + Convert.ToDateTime(recClassPrevDate).AddDays(7).ToShortDateString() + "' and Status='Makeup'";
                        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        if (null != ds && ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                            }
                        }

                        if (count == 0)
                        {
                        }
                        else
                        {
                            lblerr.Text = "Duplicate exists.";
                            retVal = "-1";
                        }
                    }
                    else
                    {
                        retVal = "-1";
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlertmsgClassStatus();", true);
                    }
                }

            }
        }
        return retVal;

    }

    public void populateRecClassDate()
    {

        string cmdText = string.Empty;
        cmdText = "select [day],cd.StartDate,cd.EndDate from CalSignup CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventYear=CD.EventYear and CD.ScheduleType='Term' and CD.Semester=CS.Semester) where  CS.MemberID=" + ddlCoach.SelectedValue + " and CS.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CS.ProductID=" + ddlProduct.SelectedValue + " and CS.EventYear=" + ddYear.SelectedValue + " and CS.Accepted='Y' and CS.Level='" + ddlLevel.SelectedValue + "' and CS.SessionNo=" + ddlSession.SelectedValue + " and CS.Semester='" + ddlPhase.SelectedValue + "'";

        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DateTime dtStartDate = new DateTime();
                    DateTime dtEndDate = new DateTime();
                    DateTime dtTodayDate = DateTime.Now;
                    DateTime dtAdvanceDate = new DateTime();
                    DateTime dtPrevDate = new DateTime();


                    dtStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString());
                    string strStartDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("MM/dd/yyyy");
                    string EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    dtEndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString());
                    string day = Convert.ToDateTime(ds.Tables[0].Rows[0]["StartDate"].ToString()).ToString("dddd");
                    string classDay = ds.Tables[0].Rows[0]["day"].ToString();
                    DateTime dtClassDate = Convert.ToDateTime(dtStartDate);
                    DDlSubDate.Items.Clear();
                    int x = 0;
                    int y = 0;
                    if (day == classDay)
                    {
                        int k = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");

                            //  string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");
                            if (dtClassDate > dtTodayDate.AddDays(-7))
                            {

                                k++;
                                dtTodayDate = dtTodayDate.AddDays(7);
                                dtAdvanceDate = dtTodayDate.AddDays(7);
                                string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                                dtTodayDate = dtTodayDate.AddDays(-7);
                                dtPrevDate = dtTodayDate.AddDays(-7);
                                string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                                //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}
                                //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}

                                DDlSubDate.Items.Insert(k - 1, new ListItem(date, date));
                            }
                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    else
                    {
                        for (int i = 1; i <= 7; i++)
                        {
                            dtClassDate = dtClassDate.AddDays(i);
                            string strDay = Convert.ToDateTime(dtClassDate).ToString("dddd");
                            if (strDay == classDay)
                            {
                                break;
                            }
                        }
                        int j = 0;
                        while (dtClassDate <= dtEndDate)
                        {
                            string date = Convert.ToDateTime(dtClassDate).ToString("MM/dd/yyyy");
                            if (dtClassDate > dtTodayDate.AddDays(-7))
                            {

                                j++;
                                // string date = Convert.ToDateTime(dtClassDate).ToString("dd/MM/yyyy");

                                // dtTodayDate = dtTodayDate.AddDays(7);
                                dtAdvanceDate = dtTodayDate.AddDays(7);
                                string strAdvanceDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");
                                //  dtTodayDate = dtTodayDate.AddDays(-7);
                                dtPrevDate = dtTodayDate.AddDays(-7);
                                string strPrevDate = Convert.ToDateTime(dtTodayDate).ToString("dd/MM/yyyy");

                                //if (dtPrevDate >= dtClassDate && dtClassDate <= dtAdvanceDate)
                                //{
                                DDlSubDate.Items.Insert(j - 1, new ListItem(date, date));
                                // }
                                //if (dtClassDate >= dtAdvanceDate && dtPrevDate <= dtClassDate)
                                //{
                                //    DDlSubDate.Items.Insert(0, new ListItem(date, date));
                                //}

                            }
                            dtClassDate = dtClassDate.AddDays(7);
                        }
                    }
                    DDlSubDate.Items.Insert(0, new ListItem("Select", "0"));
                }
            }
        }
        catch (Exception ex)
        {
        }
    }




    public void DelMeetingAttendee(string sessionKey, string WebExID, string Pwd, string Vroom)
    {


        try
        {
            if (hdnMeetingStatus.Value.ToLower() == "success")
            {
                string cmdText = "delete from WebConfLog where VideoConfLogID='" + hdnWebConflogID.Value + "'";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                cmdText = "update CalSignUp set status=null, substituteURL=null,substituteMeetKey=null,substitutePwd=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where substituteMeetKey='" + sessionKey + "'";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                cmdText = "update CoachReg set status=null, substituteURL=null,SubstituteAttendeeID=null,SubstituteRegID=null,substituteDate=null,SubstituteTime=null,substituteDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y'";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                lblSuccess.Text = "Substitute Coach deleted Successfully";
            }
            else
            {
                lblerr.Text = hdnException.Value;
            }
        }

        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

        fillMeetingGrid("false");

    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
                populateRecClassDate();
            }
        }

    }

    public void createZoomMeeting()
    {
        try
        {


            string URL = string.Empty;
            string durationHrs = "60";
            string service = "1";
            URL = "https://api.zoom.us/v1/meeting/create";
            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = txtDate.Text;
            string time = txtTime.Text;
            string dateTime = date + "T" + time + "Z";
            string topic = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "-" + ddlSession.SelectedValue;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            if (DateTime.TryParse(time, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;
                dtTo = dtFromS.AddHours(4);
            }
            string timeFormat = dtTo.Hour + ":" + dtTo.Minute + ":" + dtTo.Second;
            dateTime = date + "T" + timeFormat + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            urlParameter += "&topic=" + topic + "";
            urlParameter += "&password=training";
            urlParameter += "&type=2";
            urlParameter += "&start_time=" + dateTime + "";
            urlParameter += "&duration=" + durationHrs + "";
            urlParameter += "&timezone=GMT-5:00";
            urlParameter += "&option_jbh=true";
            urlParameter += "&option_host_video=true";
            urlParameter += "&option_audio=both";


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch
        {
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnHostURL.Value = json["start_url"].ToString();
                hdnHostURL.Value = json["join_url"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }

            if (serviceType == "6")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    if (m["host_id"].ToString() == hdnHostID.Value)
                    {
                        termianteMeeting(m["id"].ToString(), hdnHostID.Value);
                    }

                }

            }
            if (serviceType == "7")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);

                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            }

            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);


        }
        catch
        {
            hdnMeetingStatus.Value = "Failure";
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }
    protected void ddlFilterBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFilterBy.SelectedValue == "None")
        {
            dvFilterSection.Visible = false;
            fillMeetingGrid("false");
        }
        else if (ddlFilterBy.SelectedValue == "Year")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = true;
        }
        else if (ddlFilterBy.SelectedValue == "ProductGroup")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = true;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
        else if (ddlFilterBy.SelectedValue == "Product")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = true;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
        else if (ddlFilterBy.SelectedValue == "Coach")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = true;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }

    public void fillPgGroupSection()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();

            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and Accepted='Y')";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroupFilter.DataSource = ds;
                ddlProductGroupFilter.DataTextField = "ProductGroupCode";
                ddlProductGroupFilter.DataValueField = "ProductGroupId";
                ddlProductGroupFilter.DataBind();

            }
        }
        catch
        {
        }
    }
    public void fillPrdSection()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();

            Cmdtext = "select ProductId,ProductCode from Product where EventID=13 and ProductID in (select distinct(ProductId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + " ) and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and Accepted='Y')";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductFilter.DataSource = ds;
                ddlProductFilter.DataTextField = "ProductCode";
                ddlProductFilter.DataValueField = "ProductId";
                ddlProductFilter.DataBind();

            }
        }
        catch
        {
        }
    }
    public void fillCoachSection()
    {
        try
        {

            string cmdText = "";


            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' order by ID.FirstName ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoachFilter.DataSource = ds;
                ddlCoachFilter.DataTextField = "Name";
                ddlCoachFilter.DataValueField = "AutoMemberID";
                ddlCoachFilter.DataBind();

            }

        }
        catch
        {
        }
    }
    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        deleteMeeting();
    }
    public void deleteMeeting()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "Update WebConfLog set SubstituteCoachID=null where SessionKey=" + hdnSessionKey.Value + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            cmdText = "Update CalSignup set SubstituteCoachID=null, SubstituteDate=null where MeetingKey=" + hdnSessionKey.Value + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            string cmdCoachRegText = string.Empty;

            DateTime dtClassDate = Convert.ToDateTime(hdnStartDate.Value);
            DateTime dtToday = DateTime.Now;
            //if (dtToday < dtClassDate)
            //{
            cmdCoachRegText = "Update CoachClassCal set Status='Cancelled', Substitute=null  where MemberID=" + hdnCoachID.Value + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and SessionNo=" + hdnSessionNo.Value + " and [Date]='" + hdnStartDate.Value + "' and Semester='" + ddlPhase.SelectedValue + "' and WeekNo=" + hdnWeekNo.Value + " and ClassType='Regular' and Status='On'";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdCoachRegText);
            //}
            ResettingWeekNo("decrement");
            fillMeetingGrid("false");
            lblSuccess.Text = "Removed substitute coach successfully";
        }

        catch
        {
        }
    }

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }

    }

    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void termianteMeeting(string sessionkey, string hostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "7";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hostID + "";
            urlParameter += "&id=" + sessionkey + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }
    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }
    protected void btnSeacrh_Click(object sender, EventArgs e)
    {
        fillMeetingGrid("false");
    }

    public int CheckWeekNoExists()
    {
        int Retval = -1;
        string Cmdtext = string.Empty;
        string Memberid = ddlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = ddlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string SessionNo = ddlSession.SelectedValue;
        string Semester = ddlPhase.SelectedValue;
        int RegularClassCount = 0;

        Cmdtext = "select count(*) as RegulatCount from coachClasscal where Eventyear=" + ddYear.SelectedValue + " and Semester='" + Semester + "'  and memberID=" + Memberid + "  and ProductGroupID=" + PGId + " and ProductID=" + PId + " and Level ='" + Level + "' and sessionNo=" + SessionNo + " and WeekNo=" + TxtWeekNo.Text + " and (ClassType='Regular' or ClassType='Holiday')";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                RegularClassCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RegulatCount"].ToString());
                Retval = RegularClassCount;
            }
        }
        return Retval;
    }


    public void ResettingWeekNo(string WeekNoStimulation)
    {
        int Retval = -1;
        try
        {
            string Memberid = hdnCoachID.Value;
            string PGId = hdnProductGroupID.Value;
            string PId = hdnProductID.Value;
            string Level = HdnLevel.Value;
            string SessionNo = hdnSessionNo.Value;
            string Semester = ddlPhase.SelectedValue;
            string cmdText = string.Empty;
            string CoachClassId = string.Empty;

            cmdText = " select * from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "'";

            cmdText += " and Eventyear=" + ddYear.SelectedValue + " order by WeekNo, Date ASC";
            DataSet dsUniqueDate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            string Status = "On";
            string PrevStatus = "On";
            int PrevWeekNo = 0;
            string Classtype = "";
            int weekNo = PrevWeekNo;
            if (dsUniqueDate.Tables.Count > 0)
            {
                if (dsUniqueDate.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsUniqueDate.Tables[0].Rows)
                    {
                        CoachClassId = dr["CoachClassCalId"].ToString();


                        Classtype = dr["ClassType"].ToString();
                        if (PrevStatus == "On" && Classtype != "Extra")
                        {
                            weekNo = weekNo + 1;
                        }

                        string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                        //  cmdUpdateText += UpdateRelDates;
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

                        PrevWeekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                        PrevStatus = dr["Status"].ToString();
                        Retval = 1;
                    }

                    int iweekNo = Convert.ToInt32(hdnWeekNo.Value);
                  

                    if (WeekNoStimulation == "decrement")
                    {
                        ResettingReleaseDatesDecrement("", WeekNoStimulation, iweekNo);
                    }
                    else if (WeekNoStimulation == "increment")
                    {
                        ResettingReleaseDatesIncrement("", WeekNoStimulation, iweekNo);
                    }
                }
            }
        }
        catch
        {
        }

    }

    public void ResettingReleaseDatesDecrement(string Action, string WeekNoStimulation, int iWeekNo)
    {
        string Memberid = hdnCoachID.Value;
        string PGId = hdnProductGroupID.Value;
        string PId = hdnProductID.Value;
        string Level = HdnLevel.Value;
        string SessionNo = hdnSessionNo.Value;
        string Semester = ddlPhase.SelectedValue;
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo>=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + "";

        if (Action == "Delete")
        {
            cmdText += " and CoachClassCalID>=" + CoachClassId + " ";
        }
        else
        {
            cmdText += " and CoachClassCalID>" + CoachClassId + " ";
        }
        cmdText += " order by WeekNo ASC";

        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    string dtClassDate = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                    string PreviousWeekNo = string.Empty;

                    if (WeekNoStimulation == "decrement")
                    {
                        //if (dr["WeekNo"].ToString() != PreviousWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == iWeekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                        {

                        }
                        else
                        {
                            string coachPaperId = "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + " and DocType='Q'";
                            try
                            {
                                coachPaperIdBeforRst = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, coachPaperId).ToString();

                                dtHwDate = Convert.ToDateTime(dtClassDate).AddDays(0).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + coachPaperIdBeforRst + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);


                            }
                            catch
                            {
                            }

                        }
                        PreviousWeekNo = dr["WeekNo"].ToString();
                        //}
                    }
                }
            }
        }
    }
    public void ResettingReleaseDatesIncrement(string Action, string WeekNoStimulation, int iWeekNo)
    {
        string Memberid = hdnCoachID.Value;
        string PGId = hdnProductGroupID.Value;
        string PId = hdnProductID.Value;
        string Level = HdnLevel.Value;
        string SessionNo = hdnSessionNo.Value;
        string Semester = ddlPhase.SelectedValue;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo>=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " order by WeekNo ASC;";
        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    string previouWeekNo = string.Empty;

                    if (WeekNoStimulation == "increment")
                    {
                        //if (dr["WeekNo"].ToString() != previouWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == iWeekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra" || dr["ClassType"].ToString() == "Regular"))
                        {

                            string ClassDate = string.Empty;
                            ClassDate = Convert.ToDateTime(txtDate.Text).ToShortDateString();

                            dtHwDate = Convert.ToDateTime(ClassDate).ToShortDateString();
                            dtHwDueDate = Convert.ToDateTime(ClassDate).AddDays(5).ToShortDateString();
                            dtARelDate = Convert.ToDateTime(ClassDate).AddDays(7).ToShortDateString();
                            dtSRelDate = Convert.ToDateTime(ClassDate).AddDays(-1).ToShortDateString();

                            string CoachPaperIdIncAft = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + weekNo + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + "").ToString();

                            string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdIncAft + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                            SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                        }
                        else
                        {



                            try
                            {
                                string CoachPaperIdInc = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + "").ToString();

                                string dtClassDate = string.Empty;
                                dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                                dtHwDate = Convert.ToDateTime(dtClassDate).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdInc + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                            }
                            catch
                            {
                            }

                        }
                        //}
                        previouWeekNo = dr["WeekNo"].ToString();
                    }
                }
            }
        }
    }
}