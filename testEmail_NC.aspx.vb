Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
' Created by :Shalini

Partial Class Email_NC
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'User code to initialize the page here
        'only Roleid = 1 or 2, Can access this page
        If Not Page.IsPostBack Then
            Session("EmailSource") = "Email_NC"
            Session("Productid") = ""
            Dim roleid As Integer = Session("RoleID")
            If (roleid = 1) Or (roleid = 2) Or (roleid = 5 And Session("SelChapterID") = 1) Then

                ' Load Year into ListBox
                Dim year As Integer = 0
                'year = Convert.ToInt32(DateTime.Now.Year)
                Dim first_year As Integer = 2006

                year = Convert.ToInt32(DateTime.Now.Year) + 2
                Dim count As Integer = year - first_year

                For i As Integer = 0 To count - 1 'first_year To Convert.ToInt32(DateTime.Now.Year)
                    lstyear.Items.Insert(i, Convert.ToString(year - (i + 1)))
                    'year = year - 1
                Next
                'lstyear.Items.Insert(0, Convert.ToString(year + 1))
                'lstyear.Items.Insert(1, DateTime.Now.Year.ToString())
                'lstyear.Items.Insert(2, Convert.ToString(year - 1))
                'lstyear.Items.Insert(3, Convert.ToString(year - 2))
                'lstyear.Items.Insert(4, Convert.ToString(year - 3))
                'lstyear.Items.Insert(5, Convert.ToString(year - 4))
                'lstyear.Items.Insert(6, Convert.ToString(year - 5))
                'lstyear.Items.Insert(7, Convert.ToString(year - 6))
                'lstyear.Items.Insert(8, Convert.ToString(year - 7))
                'lstyear.Items.Insert(9, Convert.ToString(year - 8))
                'lstyear.Items.Insert(10, Convert.ToString(year - 9))

                lstyear.Items(1).Selected = True
                Session("Year") = lstyear.Items(1).Text
                loadRegionalCategory()
                LoadProductGroup()   ' method to  Load Product group ListBox
                LoadProductID()      ' method to  Load Product ID ListBox
                LoadRoleforassignedVol() ' method to  Load Assigned Volunteer ListBox
                LoadRoleforUnassignedVol() ' method to  Load UnAssigned Volunteer ListBox
            Else
                lblerr.Visible = True
                tabletarget.Visible = False
            End If
        End If
        Session("emaillist") = ""
    End Sub

    Private Sub LoadProductGroup()
        Dim eventid As String = drpevent.SelectedValue
        Dim strSql As String
        '"SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
        If eventid = "1" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
        ElseIf eventid = "2" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
        ElseIf eventid = "3" Then
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
        End If
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()
        lstProductGroup.Items.Insert(0, "All")
        lstProductGroup.Items(0).Selected = True
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim eventid As String = drpevent.SelectedValue
        Dim productgroup As String = ""
        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            lstProductid.Enabled = False
            Dim sbvalues As New StringBuilder
            Dim strSql1 As String '= "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ")  and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            If eventid = "1" Then
                strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
            ElseIf eventid = "2" Then
                strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
            ElseIf eventid = "3" Then
                strSql1 = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If
        Dim strSql As String = "Select ProductCode, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductid.DataSource = drproductid
        lstProductid.DataBind()
        lstProductid.Items.Insert(0, "All")
        lstProductid.Items(0).Selected = True
    End Sub

    Private Sub LoadRoleforassignedVol()
        Dim strSql As String = "Select RoleID, Name from Role where Chapter = 'Y' order by RoleID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstAssignRole.DataSource = drrole
        lstAssignRole.DataBind()
        lstAssignRole.Items.Insert(0, "All")
        lstAssignRole.Items(0).Selected = True
    End Sub

    Private Sub LoadRoleforUnassignedVol()
        Dim strSql As String = "Select  VolunteerTaskID, TaskDescription from VolunteerTasks where LevelCode in (1,2,3,4) order by VolunteerTaskID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstUnassignRole.DataSource = drrole
        lstUnassignRole.DataBind()
        lstUnassignRole.Items.Insert(0, "All")
        lstUnassignRole.Items(0).Selected = True
    End Sub

    Protected Sub btnselectmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnselectmail.Click
        ' Continue to Email button
        ' will select emails depending on selections made
        ' tableemail.Visible = True
      




        Dim dsEmails As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        Dim eventid As String = drpevent.SelectedValue
        ' Dim chapterid As String = ""

        Session("mailEventID") = drpevent.SelectedItem.Value.ToString()

        Dim strPaidOption As String = ""
        Select Case ddlPaid.SelectedValue.ToString()
            Case "-1"
                'Select one
                strPaidOption = ""
            Case "1"
                'Chapter Contest 
                strPaidOption = "and not exists (select ParentID from Contestant where EventID=2 and contestyear=YEAR(GETDATE()) and PaymentReference is not null and (AutoMemberID=ParentID or Relationship=ParentID)) "

            Case "2"
                'Finals
                strPaidOption = "and not exists (select ParentID from Contestant where EventID=1 and contestyear=YEAR(GETDATE()) and PaymentReference is not null and (AutoMemberID=ParentID or Relationship=ParentID))"

            Case "3"
                'Workshops
                strPaidOption = "and not exists(select MemberID from Registration where EventID=3 and eventyear=YEAR(GETDATE()) and PaymentReference is not null and (AutoMemberID = MemberID or Relationship=MemberID))"
            Case "4"
                'Coaching
                strPaidOption = "and not exists(select PMemberID from CoachReg where EventID=13 and eventyear=YEAR(GETDATE()) and PaymentReference is not null and (AutoMemberID= PMemberID or Relationship= PMemberID))"
            Case "5"
                'PrepClub
                strPaidOption = "and not exists(select MemberID from Registration_PrepClub where EventID=19 and eventyear=YEAR(GETDATE()) and PaymentReference is not null  and (AutoMemberID= MemberID or Relationship= MemberID))"
            Case Else
                Exit Select
        End Select


        If rbtall.Checked = True Then   ' If selected value is ALL from Target

            ' ***************************************** commented code is IF we want to use Stored procedure *************************
            'Dim prmArray(2) As SqlParameter

            'prmArray(0) = New SqlParameter
            'prmArray(0).ParameterName = "@radiooption"
            'prmArray(0).Value = 1
            'prmArray(0).Direction = ParameterDirection.Input

            'prmArray(1) = New SqlParameter
            'prmArray(1).ParameterName = "@chapterid"
            'prmArray(1).Value = chapterid
            'prmArray(1).Direction = ParameterDirection.Input

            'dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetEmailList_forEmailCC", prmArray)
            'Dim strAll As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ChapterID in (" & chapterid & ")"

            'Dim strAll As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>''"
            If ddlnfYears.SelectedItem.Value = "0" Then
                lblError.Text = "Please Select Number of Years."
                Exit Sub
            Else
                lblError.Text = ""
            End If
            Dim cYear As Integer = DateTime.Now.AddYears(Val(ddlnfYears.SelectedItem.Value)).Year

            ' Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null AND (year(CreateDate) >=" & cYear.ToString() & " or year(ModifyDate)>=" & cYear.ToString() & ") or exists(select * from nfg_Transactions N where AutoMemberID=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & ") or exists(select * from nfg_Transactions N where Relationship=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & ") AND (newsletter not in ('2','3') OR (Newsletter is null)) " & strPaidOption

            Dim strAll As String = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and ((year(CreateDate) >=" & cYear.ToString() & " or year(ModifyDate) >=" & cYear.ToString() & ")or exists(select * from nfg_Transactions N where AutoMemberID=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & ") or exists(select * from nfg_Transactions N where Relationship=N.MemberId and Year([Payment Date])>=" & cYear.ToString() & "))  " & strPaidOption


            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, strAll)

        ElseIf btnregisteredparetns.Checked = True Then 'If selected value is Registered parents from Target
            If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
                Dim sbvalues As New StringBuilder
                'strSql = "Select ProductGroupID as ID, Name from ProductGroup where EventID in (" & eventid & ") order by ProductGroupID"
                'strSql = "Select Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                If eventid = "1" Then
                    strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and P.EventId =1 AND C.NationalFinalsStatus = 'Active' order by P.ProductGroupID"
                ElseIf eventid = "2" Then
                    strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupCode = C.ProductGroupCode and C.ContestYear in (" & Session("Year") & ") and c.RegionalStatus = 'Active' and P.EventId in (" & eventid & ")  order by P.ProductGroupID"
                ElseIf eventid = "3" Then
                    strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P  where P.EventId in (3)  and exists (select * from EventFees where EventYear in (" & Session("Year") & ") and P.ProductGroupID = ProductGroupID)"
                End If
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("ProductGroupId") = sbvalues.ToString
                End If
            End If

            If lstProductid.Items(0).Selected = True And Session("Productid") = "" Then  ' if selected item in ProductId is ALL
                Dim sbvalues As New StringBuilder
                'strSql = "Select ProductID as ID, Name from Product where EventID in (" & eventid & ") and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                strSql = "Select ProductCode as ID, Name from Product where EventID in (" & eventid & ") and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append("'")
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        sbvalues.Append("'")
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Productid") = sbvalues.ToString
                End If
            End If

            'Dim prmArray(6) As SqlParameter

            'prmArray(0) = New SqlParameter
            'prmArray(0).ParameterName = "@radiooption"
            'prmArray(0).Value = 2
            'prmArray(0).Direction = ParameterDirection.Input

            'prmArray(1) = New SqlParameter
            'prmArray(1).ParameterName = "@chapterid"
            'prmArray(1).Value = chapterid
            'prmArray(1).Direction = ParameterDirection.Input

            'prmArray(2) = New SqlParameter
            'prmArray(2).ParameterName = "@year"
            'prmArray(2).Value = Session("Year")
            'prmArray(2).Direction = ParameterDirection.Input

            'prmArray(3) = New SqlParameter
            'prmArray(3).ParameterName = "@Productid"
            'prmArray(3).Value = Session("Productid")
            'prmArray(3).Direction = ParameterDirection.Input

            'prmArray(4) = New SqlParameter
            'prmArray(4).ParameterName = "@option"
            'prmArray(4).Value = drpregistrationtype.SelectedItem.Value
            'prmArray(4).Direction = ParameterDirection.Input

            'prmArray(5) = New SqlParameter
            'prmArray(5).ParameterName = "@eventid"
            'prmArray(5).Value = eventid
            'prmArray(5).Direction = ParameterDirection.Input
            'dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetEmailList_forEmailCC", prmArray)
            Dim dsPaid As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct a.ParentId From Contestant a Where a.ContestYear=" & Now.Year() & " and a.Productcode in (" & Session("Productid") & ") and eventid =" & eventid & " and a.PaymentReference is not null")
            Dim PaidParents As String = ""
            Dim PaidParentsId As String = ""

            If dsPaid.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsPaid.Tables(0).Rows.Count - 1
                    PaidParentsId = PaidParentsId + dsPaid.Tables(0).Rows(i)("ParentId").ToString() + ","
                Next

                If ddlPaid.SelectedValue <> -1 Then ' If chkPaid.Checked = True Then
                    PaidParents = " and not (PaymentReference is not null and Contestyear=" & Now.Year() & ")"
                    ''''and not exists(select * from Contestant where contestyear=Year(GETDATE()) and PaymentReference is not null and (AutoMemberID=ParentID or Relationship=ParentID))"
                    ''''and (a.PaymentReference is null and a.ContestYear=" & Now.Year() & ")" 'a.ParentID not in (" & PaidParents.Trim(",") & ")"
                Else
                    PaidParents = ""
                End If
            End If
            If eventid = 1 Then
                'If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                '    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                '    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                'ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                '    'Dim StrPendingregistration As String = " SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                '    Dim StrPendingregistration As String = " SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                'ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                '    ' Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & ")))) "
                '    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & ")))) "
                '    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                'End If
                Dim StrSQL1 As String
                If drpregistrationtype.SelectedItem.Value = 1 Then   'All invitees
                    StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ")  and a.nationalinvitee=1 and invite_decline_comments is null  and a.EventID in (2)" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ")  and a.nationalinvitee=1 and invite_decline_comments is null and a.EventID in (2)" & PaidParents & "))) group by Email"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'Paid Parent's Emails
                    StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")" & PaidParents & "))) group by Email"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'Pending Parent's Emails
                    StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productid") & ")" & PaidParents & "))) group by Email"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 4 Then  'invitees less Paid
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and a.nationalinvitee=1 and invite_decline_comments is null  and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1 and paymentreference is not null" & PaidParents & ");SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))   and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                ElseIf drpregistrationtype.SelectedItem.Value = 5 Then  'invitees less Paid less Pending
                    StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productid") & ") and a.nationalinvitee=1 and invite_decline_comments is null  and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productid") & ") and eventid=1" & PaidParents & ");SELECT distinct(Email) as EmailID From IndSpouse where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
                End If

            ElseIf eventid = 2 Then
                If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>''  AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                    '**
                    'lblerr.Text = StrPaidregistration
                    'lblerr.Visible = True
                    'Exit Sub
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                    'Dim StrPendingregistration As String = " SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & "))))"
                    Dim StrPendingregistration As String = " SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                    ' Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")))) "
                    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a where contestyear in (" & Session("Year") & ") and parentid = b.AutoMemberID and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select parentid from contestant a  where contestyear in (" & Session("Year") & ") and parentid = b.relationship and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & "))) "
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                End If

            ElseIf eventid = 3 Then
                If drpregistrationtype.SelectedItem.Value = 1 Then   'If selected value is Paid Registrations from Type of registration
                    ' Dim StrPaidregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and and ProductID in (" & Session("Productid") & "))))"
                    Dim StrPaidregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null))  and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is not null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is not null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPaidregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'If selected value is pending Registrations from Type of registration
                    '  Dim StrPendingregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & "))))"
                    Dim StrPendingregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>''  AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and PaymentReference is null and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrPendingregistration)
                ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'If selected value is Both from Type of registration
                    ' Dim StrBothregistration As String = "SELECT distinct(AutoMemberID), Email as EmailID From IndSpouse b where email<>'' and ((donortype = 'IND' and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))) or (donortype = 'SPOUSE' and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") and ProductID in (" & Session("Productid") & "))))"
                    Dim StrBothregistration As String = "SELECT distinct(Email) as EmailID From IndSpouse b where email<>'' AND ValidEmailFlag is Null and ((donortype = 'IND' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select memberid from Registration a where eventyear in (" & Session("Year") & ") and memberid = b.AutoMemberID and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")) or (donortype = 'SPOUSE' AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (select memberid from Registration a  where eventyear in (" & Session("Year") & ") and memberid = b.relationship and EventID in (" & eventid & ") and Productcode in (" & Session("Productid") & ")" & PaidParents & ")))"
                    dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrBothregistration)
                End If
            End If
        ElseIf rbtassigndvolunteer.Checked = True Then  'If selected value is Assigned Volunteer from Target
            If lstAssignRole.Items(0).Selected = True Then   'If selected value is ALL from Assigned Volunteer
                Dim sbvalues As New StringBuilder
                Dim strSql1 As String = "Select  RoleID as ID, Name from Role"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("Assignvolunteer") = sbvalues.ToString
                End If
            End If
            'Dim prmArray(3) As SqlParameter

            'prmArray(0) = New SqlParameter
            'prmArray(0).ParameterName = "@radiooption"
            'prmArray(0).Value = 3
            'prmArray(0).Direction = ParameterDirection.Input

            'prmArray(1) = New SqlParameter
            'prmArray(1).ParameterName = "@chapterid"
            'prmArray(1).Value = chapterid
            'prmArray(1).Direction = ParameterDirection.Input

            'prmArray(2) = New SqlParameter
            'prmArray(2).ParameterName = "@Assignvolunteer"
            ''Dim StrX As String = Session("AssignVolunteer")
            ''Dim StrY As String = StrX.Replace("""", String.Empty)
            ''System.Console.WriteLine(StrY)
            'prmArray(2).Value = Session("AssignVolunteer").ToString()
            'prmArray(2).Direction = ParameterDirection.Input

            'dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetEmailList_forEmailCC", prmArray)
            'Dim StrAssign As String = "SELECT  AutoMemberID, Email as EmailID  From IndSpouse b where email<>'' and exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in ( " & chapterid & " ) and   RoleID in ( " & Session("AssignVolunteer") & " ))"
            ' Dim StrAssign As String = "SELECT  AutoMemberID, Email as EmailID  From IndSpouse b where email<>'' and exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and RoleID in ( " & Session("AssignVolunteer") & " ))"
            Dim StrAssign As String = "SELECT  distinct(Email) as EmailID  From IndSpouse b where email<>''  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and RoleID in ( " & Session("AssignVolunteer") & " ))"
            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrAssign)
        ElseIf rbtunassigndvolunteer.Checked = True Then       'If selected value is UnAssigned Volunteer from Target
            If lstUnassignRole.Items(0).Selected = True Then   'If selected value is ALL from UnAssigned Volunteer
                Dim sbvalues As New StringBuilder
                Dim strSql1 As String = "Select  VolunteerTaskID as ID, TaskDescription from VolunteerTasks where LevelCode in (1,2,3,4) order by VolunteerTaskID"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                If ds.Tables.Count > 0 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                        If i < ds.Tables(0).Rows.Count - 1 Then
                            sbvalues.Append(",")
                        End If
                    Next
                    Session("UnAssignvolunteer") = sbvalues.ToString
                End If
            End If
            'Dim prmArray(3) As SqlParameter

            'prmArray(0) = New SqlParameter
            'prmArray(0).ParameterName = "@radiooption"
            'prmArray(0).Value = 4
            'prmArray(0).Direction = ParameterDirection.Input

            'prmArray(1) = New SqlParameter
            'prmArray(1).ParameterName = "@chapterid"
            'prmArray(1).Value = chapterid
            'prmArray(1).Direction = ParameterDirection.Input

            'prmArray(2) = New SqlParameter
            'prmArray(2).ParameterName = "@unAssignvolunteer"
            'prmArray(2).Value = Session("UnAssignvolunteer")
            'prmArray(2).Direction = ParameterDirection.Input

            'dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetEmailList_forEmailCC", prmArray)
            'Dim StrUnAssign As String = " SELECT  AutoMemberID, Email as EmailID From IndSpouse b where Email <> '' and (VolunteerRole1 in ( " & Session("UnAssignvolunteer") & ") OR VolunteerRole2 in (" & Session("UnAssignvolunteer") & ") OR VolunteerRole3 in (" & Session("UnAssignvolunteer") & ")) and ChapterID in (" & chapterid & ") and not exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID and ChapterID in (" & chapterid & "))"

            ' Dim StrUnAssign As String = " SELECT  AutoMemberID, Email as EmailID From IndSpouse b where Email <> '' and (VolunteerRole1 in ( " & Session("UnAssignvolunteer") & ") OR VolunteerRole2 in (" & Session("UnAssignvolunteer") & ") OR VolunteerRole3 in (" & Session("UnAssignvolunteer") & ")) and not exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID )"
            Dim StrUnAssign As String = " SELECT  distinct(Email) as EmailID From IndSpouse b where Email <> ''  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and (VolunteerRole1 in ( " & Session("UnAssignvolunteer") & ") OR VolunteerRole2 in (" & Session("UnAssignvolunteer") & ") OR VolunteerRole3 in (" & Session("UnAssignvolunteer") & ")) and not exists (Select MemberID From Volunteer where MemberID = b.AutoMemberID )"
            dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrUnAssign)
            GridView1.DataSource = dsEmails.Tables(0)
            GridView1.DataBind()
            Response.Write("dsEmails =<br>")
        ElseIf rbtownlist.Checked = True Then ' If Own List radio button is selected
            Session("sentemaillistenable") = "Yes"
            'Response.Redirect("emaillist.aspx")
        End If

        ' depending on selections  emails list will be selected
        Dim tblEmails() As String = {"EmailContacts"}
        Dim sbEmailList As New StringBuilder
        If dsEmails.Tables(0).Rows.Count > 0 Then
            Dim ctr As Integer
            For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                If ctr <= dsEmails.Tables(0).Rows.Count - 2 Then
                    sbEmailList.Append(",")
                End If
            Next
            Session("emaillist") = sbEmailList.ToString
        End If
        Session("sentemaillistenable") = "No"
        ' Response.Redirect("emaillist.aspx")

        Response.Write("sb =<br>" & sbEmailList.ToString)


    End Sub

    Protected Sub rbtall_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtall.CheckedChanged
        'on selection change in All from Target
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssigncategory.Enabled = False
        lstUnassigncategory.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
        ddlnfYears.Enabled = True
    End Sub

    Protected Sub btnregisteredparetns_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnregisteredparetns.CheckedChanged
        'on selection change in registered parents  from Target
        ddlnfYears.Enabled = False
        drpregistrationtype.Enabled = True
        drpevent.Enabled = True
        lstProductGroup.Enabled = True
        lstProductid.Enabled = False
        lstyear.Enabled = True
        lstAssigncategory.Enabled = False
        lstUnassigncategory.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
    End Sub

    Protected Sub rbtassigndvolunteer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtassigndvolunteer.CheckedChanged
        'on selection change in AssignedVolunteer  from Target
        LoadRoleforassignedVol()
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssigncategory.Enabled = True
        lstUnassigncategory.Enabled = False
        lstAssignRole.Enabled = True
        lstUnassignRole.Enabled = False
        ddlnfYears.Enabled = False
    End Sub

    Protected Sub rbtunassigndvolunteer_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtunassigndvolunteer.CheckedChanged
        'on selection change in Unassigned Volunteer  from Target
        LoadRoleforassignedVol()
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssigncategory.Enabled = False
        lstUnassigncategory.Enabled = True
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = True
        ddlnfYears.Enabled = False
    End Sub

    'Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
    '    'on selection change in Event from Registered Parents
    '    LoadProductGroup()
    'End Sub
    Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Event from Registered Parents
        If drpevent.SelectedValue = 1 Then
            loadFinalCategory()
        Else
            loadRegionalCategory()
        End If
        LoadProductGroup()
    End Sub
    Protected Sub loadRegionalCategory()
        drpregistrationtype.Items.Clear()
        drpregistrationtype.Items.Add(New ListItem("Paid Registrations", "1"))
        drpregistrationtype.Items.Add(New ListItem("Pending Registrations", "2"))
        drpregistrationtype.Items.Add(New ListItem("Both", "3"))
    End Sub

    Protected Sub loadFinalCategory()
        drpregistrationtype.Items.Clear()
        drpregistrationtype.Items.Add(New ListItem("All Invites", "1"))
        drpregistrationtype.Items.Add(New ListItem("Paid Registrants", "2"))
        drpregistrationtype.Items.Add(New ListItem("Pending", "3"))
        drpregistrationtype.Items.Add(New ListItem("Invites less Paid", "4"))
        drpregistrationtype.Items.Add(New ListItem("Invites less Paid less Pending", "5"))
    End Sub

    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productid") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productid") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub lstyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in year from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstyear.Items.Count - 1
            If lstyear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstyear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Year") = sbvalues.ToString
        End If
    End Sub

    Protected Sub lstAssignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAssigncategory.SelectedIndexChanged, lstAssignRole.SelectedIndexChanged
        'on selection change in Role froom Assigned Volunteer 
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstAssignRole.Items.Count - 1
            If lstAssignRole.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstAssignRole.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Assignvolunteer") = sbvalues.ToString
        End If
    End Sub

    Protected Sub lstUnassignRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Role from Unassigned Volunteer
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstUnassignRole.Items.Count - 1
            If lstUnassignRole.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstUnassignRole.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("UnAssignvolunteer") = sbvalues.ToString
        End If
    End Sub

    Protected Sub lstAssigncategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstAssigncategory.SelectedIndexChanged
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstAssigncategory.Items.Count - 1
            If lstAssigncategory.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstAssigncategory.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("UnAssignvolunteerCategory") = sbvalues.ToString
        End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String
        Dim dsrole0 As New DataSet
        Dim dsrole1 As New DataSet
        Dim dsrole2 As New DataSet
        Dim dsrole3 As New DataSet
        Dim dsrole4 As New DataSet
        Dim dsrole5 As New DataSet
        Dim dsrole6 As New DataSet

        If lstAssigncategory.SelectedItem.Value = 0 Then
            strSql = "Select  RoleID, Name from Role"
            dsrole0 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("0") = dsrole0

        ElseIf lstAssigncategory.SelectedItem.Value = 1 Then
           strSql = "Select  RoleID, Name from Role where 'National' ='Y' order by RoleID"
            dsrole1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("1") = dsrole1

        ElseIf lstAssigncategory.SelectedItem.Value = 2 Then
            strSql = "Select  RoleID, Name from Role where Zonal = 'Y' order by RoleID"
            dsrole2 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("2") = dsrole2

        ElseIf lstAssigncategory.SelectedItem.Value = 3 Then
            strSql = "Select  RoleID, Name from Role where Cluster ='Y' or Chapter ='Y' order by RoleID"
            dsrole3 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("3") = dsrole3

        ElseIf lstAssigncategory.SelectedItem.Value = 4 Then
            strSql = "Select  RoleID, Name from Role where Chapter ='Y' order by RoleID"
            dsrole4 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("4") = dsrole4

        ElseIf lstAssigncategory.SelectedItem.Value = 5 Then
            strSql = "Select  RoleID, Name from Role where Finals ='Y' order by RoleID"
            dsrole5 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("5") = dsrole5

        ElseIf lstAssigncategory.SelectedItem.Value = 6 Then
            strSql = "Select  RoleID, Name from Role where IndiaChapter = 'Y' order by RoleID"
            dsrole6 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("6") = dsrole6

        End If
        'Dim strSql As String = "Select  VolunteerTaskID, TaskDescription from VolunteerTasks where LevelCode in (" + Session("UnAssignvolunteerCategory") + ") order by VolunteerTaskID"
        'Dim drrole As SqlDataReader
        'Dim dsrole As DataSet
        'Dim conn As New SqlConnection(Application("ConnectionString"))
        'dsrole = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        dsrole0.Merge(dsrole1)
        dsrole0.Merge(dsrole2)
        dsrole0.Merge(dsrole3)
        dsrole0.Merge(dsrole4)
        dsrole0.Merge(dsrole5)
        dsrole0.Merge(dsrole6)

        lstAssignRole.DataSource = dsrole0
        lstAssignRole.DataBind()
        lstAssignRole.Items.Insert(0, "All")
        lstAssignRole.Items(0).Selected = True
    End Sub
    Protected Sub lstUnassigncategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles lstUnassigncategory.SelectedIndexChanged
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstUnassigncategory.Items.Count - 1
            If lstUnassigncategory.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstUnassigncategory.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("UnAssignvolunteerCategory") = sbvalues.ToString
        End If
        Dim strSql As String = "Select  VolunteerTaskID, TaskDescription from VolunteerTasks where LevelCode in (" + Session("UnAssignvolunteerCategory") + ") order by VolunteerTaskID"
        Dim drrole As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drrole = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstUnassignRole.DataSource = drrole
        lstUnassignRole.DataBind()
        lstUnassignRole.Items.Insert(0, "All")
        lstUnassignRole.Items(0).Selected = True
    End Sub

    Protected Sub rbtownlist_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles rbtownlist.CheckedChanged
        'if own list radio button is checked from target
        drpregistrationtype.Enabled = False
        drpevent.Enabled = False
        lstProductGroup.Enabled = False
        lstProductid.Enabled = False
        lstyear.Enabled = False
        lstAssigncategory.Enabled = False
        lstUnassigncategory.Enabled = False
        lstAssignRole.Enabled = False
        lstUnassignRole.Enabled = False
    End Sub

End Class
