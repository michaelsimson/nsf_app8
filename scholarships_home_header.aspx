<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1" />
<title>North South Foundation : About Us</title>
<link href="/public/css/style.css" rel="stylesheet" type="text/css" />
<link href="/public/css/menu_style.css" rel="stylesheet" type="text/css" />
<link href="/public/css/glossymenu.css" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="/public/js/layer.js"></script>
<script type="text/javascript" src="/public/js/jquery.min.js"></script>
<script type="text/javascript" src="/public/js/ddaccordion.js"></script>
<script type="text/javascript" src="/public/js/nsfddaccordion.js"></script>
<script type="text/javascript" src="/public/js/imageswap.js"></script>
</head>
<body>
<!--#include file="/public/main/header.aspx"-->
  <tr>
                  <td valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top"><table width="93%" border="0" align="center" cellpadding="0" cellspacing="0">
                          <tr>
                            <td valign="top">
                              <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                
                                <tr>
                                  <td width="4%">&nbsp;</td>
                                  <td width="96%"><table width="88%" border="0" align="left" cellpadding="0" cellspacing="0">
                                    <tr>
                                      <td>&nbsp;</td>
                                    </tr>
                                    <tr>
                                      <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                          <td>
                                         <div class="glossymenu">
                                            <a class="menuitem submenuheader" href="#">India chapters </a>
                                            <div id="id1" class="submenu">                                           
                                  
                                             <a class="menuitem submenuheader1" href="#">Andhra Pradhesh</a>
                                            <div id="Div1" class="submenu1">                                           
                                            <ul>
                                            <li><a  href="Chapters/AP_Tanuku/default.aspx">Tanuku</a></li>
                                             <li><a  href="Chapters/AP_Kurnool/default.aspx">Kurnool</a></li>
                                             <li><a  href="Chapters/AP_Vijayawada/default.aspx">Vijayawada</a></li>
                                             <li><a  href="Chapters/AP_Vizag/default.aspx">Visakhapatnam</a></li>
                                           
                                             </ul>
                                             </div>   
                                              <a class="menuitem submenuheader1" href="#">Assam</a>
                                            <div id="Div2" class="submenu1">                                           
                                            <ul>
                                            <li><a  href="Chapters/AS_Guwahati/default.aspx">Guwahati</a></li>
                                           
                                             </ul>
                                             </div>   
                                      <a class="menuitem submenuheader1" href="#">Bihar</a>
                                            <div id="Div3" class="submenu1">                                           
                                            <ul>
                                           
                                             <li><a  href="Chapters/BR_Patna/default.aspx">Patna</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Chandigarh</a>
                                               
                                            <div id="Div4" class="submenu1">                                           
                                            <ul>
                                           
                                             <li><a  href="Chapters/CH_Chandigarh/default.aspx">Chandigarh</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Delhi</a>
                                               
                                            <div id="Div17" class="submenu1">                                           
                                            <ul>
                                          
                                             <li><a  href="Chapters/DL_Delhi/default.aspx">Delhi</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Gujarat</a>
                                            <div id="Div5" class="submenu1">                                           
                                            <ul>
                                           
                                            <li><a  href="Chapters/GJ_Ahmedabad/default.aspx">Ahmedabad</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Haryana</a>
                                            <div id="Div6" class="submenu1">                                           
                                            <ul>
                                           
                                           <li><a  href="Chapters/HR_Panchkula/default.aspx">Panchkula</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Karnataka</a>
                                            <div id="Div7" class="submenu1">                                           
                                            <ul>
                                           
                                            <li><a  href="Chapters/KA_Bangalore/default.aspx">Bangalore</a></li>
                                            <li><a  href="Chapters/KA_Mysore/default.aspx">Mysore</a></li>
                                             </ul>
                                             </div>   
                                           
                                              <a class="menuitem submenuheader1" href="#">Kerala</a>
                                            <div id="Div9" class="submenu1">                                           
                                            <ul>
                                           
                                              <li><a  href="Chapters/KL_Kochi/default.aspx">Kochi</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Maharashtra</a>
                                            <div id="Div10" class="submenu1">                                           
                                            <ul>
                                             <li><a  href="Chapters/MH_Aurangabad/default.aspx">Aurangabad</a></li>
                                             <li><a  href="Chapters/MH_Mumbai/default.aspx">Mumbai</a></li>
                                             <li><a  href="Chapters/MH_Pune/default.aspx">Pune</a></li>
                                        
                                             </ul>
                                             </div>  
                                              <a class="menuitem submenuheader1" href="#">Madhya Pradesh</a>
                                            <div id="Div8" class="submenu1">                                           
                                            <ul>
                                           <li><a  href="Chapters/MP_Bhopal/default.aspx">Bhopal</a></li>
                                           <li><a  href="Chapters/MP_Satna/default.aspx">Satna</a></li>
                                           
                                             </ul>
                                             </div>    
                                               <a class="menuitem submenuheader1" href="#">Orissa</a>
                                            <div id="Div11" class="submenu1">                                           
                                            <ul>
                                           
                                           <li><a  href="Chapters/OR_Bhubaneswar/default.aspx">Bhubaneswar</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Punjab</a>
                                            <div id="Div12" class="submenu1">                                           
                                            <ul>
                                           
                                          <li><a  href="Chapters/PB_Patiala/default.aspx">Patiala</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">Rajasthan</a>
                                            <div id="Div13" class="submenu1">                                           
                                            <ul>
                                           <li><a  href="Chapters/RJ_Jodphur/default.aspx">Jodhpur</a></li>
                                             <li><a  href="Chapters/RJ_Udaipur/default.aspx">Udaipur</a></li>
                                           
                                             </ul>
                                             </div>   
                                              <a class="menuitem submenuheader1" href="#">Tamil Nadu</a>
                                            <div id="Div14" class="submenu1">                                           
                                            <ul>
                                         
                                             <li><a  href="Chapters/TN_Chennai/default.aspx">Chennai</a></li>
                                             <li><a  href="Chapters/TN_Madurai/default.aspx">Madurai</a></li>
                                             <li><a  href="Chapters/TN_Nagercoil/default.aspx">Nagercoil</a></li>
                                           
                                             </ul>
                                             </div> 
                                               <a class="menuitem submenuheader1" href="#">Telangana</a>
                                            <div id="Div18" class="submenu1">                                           
                                            <ul>
                                         
                                             <li><a  href="Chapters/TS_Hyderabad/default.aspx">Hyderabad</a></li>
                
                                             </ul>
                                             </div>       
                                              <a class="menuitem submenuheader1" href="#">Uttar Pradesh</a>
                                            <div id="Div15" class="submenu1">                                           
                                            <ul>
                                            <li><a  href="Chapters/UP_Kanpur/default.aspx">Kanpur</a></li>
                                             <li><a  href="Chapters/UP_Moradabad/default.aspx">Moradabad</a></li>
                                           
                                             </ul>
                                             </div>   
                                               <a class="menuitem submenuheader1" href="#">West Bengal</a>
                                            <div id="Div16" class="submenu1">                                           
                                            <ul>
                                           <li><a  href="Chapters/WB_Kolkata/default.aspx">Kolkata</a></li>
                                           
                                             </ul>
                                             </div>   
                                             </div>                                          
                                            <a class="menuitem submenuheader" href="#">Scholarship types</a>
                                            <div  id="id2"  runat="server" class="submenu">                                           
                                            <ul>
                                             <li><a  href="schcollege.aspx">College</a></li>
                                             <li><a  href="schdesignated.aspx">Designated</a></li>
                                             <li><a  href="schhighschool.aspx">High school</a></li>   
                                             </ul>
                                             </div> 
                                            <a class="menuitem submenuheader" href="#">Application process</a>
                                            <div  id="id3" runat="server" class="submenu">                                           
                                            <ul>
                                             <li><a  href="generalinfo.aspx">General info</a></li>
                                             <li><a  href="schguidelines.aspx">Guidelines</a></li>
                                             <li><a  href="how_to_apply.aspx">How to apply</a></li>
                                              <li><a  href="faq.aspx">Frequently Asked Questions</a></li>
                                              <li><a  href="scholarshiprenewal.aspx">Scholarship Renewal</a></li>
                                             </ul>
                                             </div> 
                                            <a class="menuitem submenuheader" href="#">Scholarship history</a>
                                            <div  id="id4" runat="server" class="submenu">                                           
                                            <ul>
                                            <li><a  href="Scolarship2013_2014.aspx">2013-14</a></li>
                                            <li><a  href="Scolarship2012_2013.aspx">2012-13</a></li>
                                            <li><a  href="Scolarship2011_2012.aspx">2011-12</a></li>
                                            <li><a  href="Scolarship2010_2011.aspx">2010-11</a></li>
		                                    <li><a  href="Scolarship2009_2010.aspx">2009-10</a></li>
		                                    <li><a  href="Scolarship2008_2009.aspx">2008-09</a></li>
		                                    </ul>
                                            </div>
                                            <a class="menuitem" href="testimonials.aspx">Testimonials</a>
                                            <a class="menuitem" href="alumni.aspx">Alumni</a>
                                            <a class="menuitem" href="contact.aspx">Contact</a>
                                            <a class="menuitem" href="Indiachapterlocation.aspx">Chapter Location</a>
                                            <a class="menuitem" href="ProjectVaaradhi.aspx">Project Vaaradhi</a>
                                             <a class="menuitem  submenuheader" href="#">Volunteer</a>
                                            <div  id="id5" runat="server" class="submenu">                                           
                                            <ul>
                                            <li><a  href="generalinfo1.aspx">General information</a></li>
		                                    <li><a  href="newchapter.aspx">Start a new chapter</a></li>
		         
		                                    </ul>
                                            </div> 
                                          </div>
                                          </td>
                                        </tr>
                                      </table></td>
                                    </tr>
                                    
                                  </table></td>
                                </tr>
                            </table>
                           </td>
                          </tr>
                          <tr>
                            <td align="center">&nbsp;</td>
                          </tr>
                          <tr>
                            <td align="center">
                             <!--#include file="/public/main/qlinkinc.aspx"-->
                            </td>
                          </tr>
                      </table></td>
                      <td width="76%" valign="top">
                      <table width="98%" border="0" align="left" cellpadding="0" cellspacing="6"><tr><td valign="top" width="66%">