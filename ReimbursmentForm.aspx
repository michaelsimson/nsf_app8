
<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master"  CodeFile="ReimbursmentForm.aspx.cs" Inherits="ExpenseJournal_ReimbursmentForm" %>
<%--MaintainScrollPositionOnPostback="false"--%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <%--<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
      <HTML>
      	<HEAD>
      		<title>Reimbursment Form</title>
      		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
      		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
      		<meta name="vs_defaultClientScript" content="JavaScript">
      		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
      		<LINK href="Styles.css" type="text/css" rel="stylesheet">
      		<script language="javascript">
      			window.history.forward(1);
      		</script>--%>
   <script language="javascript" type="text/javascript">
       function PopupPicker(ctl, w, h) {
           var PopupWindow = null;
           settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
           PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
           PopupWindow.focus();
       }
   </script>
   <script type="text/javascript" language="javascript">
       function ConfirmOnDelete() {
           if (confirm("Are you sure to delete?") == true)
               return true;
           else
               return false;
       }
       function showhide(targetID) {
           var elementmode = document.getElementById(targetID).style;
           elementmode.display = (!elementmode.display) ? 'none' : '';
       }
   </script>
   <%--<style type="text/css">
      <!--
      body {
      	margin-left: 0px;
      	margin-top: 0px;
      	margin-right: 0px;
      	margin-bottom: 0px;
      }
      -->
      </style>
      		
      	</HEAD>
      	<body>
      --%>		
<%--   <form id="Form1Main" method="post" runat="server">
--%>      <table border="0" cellpadding ="0" cellspacing = "0" width ="980">
         <tr>
            <td colspan ="2" align="center" >
               <%-- <img src="images/trilogo.gif" width="980px" alt ="" />--%>
            </td>
         </tr>
         <tr>
            <td align="left" style="width :16%">
               <asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
            </td>
            <td align="center" style="font-size:16px; font-weight:bold ; font-family:Calibri ">
               Reimbursement Form
            </td>
         </tr>
      </table>
      <br />
      <table border="0" cellpadding ="0" cellspacing = "0" width ="1004">
         <tr>
            <td align="left" style="padding-left:10px; width:30%">
               <asp:Label CssClass="SmallFont"  ID="lblAddress" runat="server" > </asp:Label>
            </td>
            <td align="left">
               <table cellpadding="2" cellspacing="0" border="0" width="400px">
                  <tr>
                     <td style="height: 27px" align="left" width="50%">
                        <asp:Label CssClass="SmallFont" ID="Label3" runat="server" >Select Chapter</asp:Label>
                     </td>
                     <td style="height: 27px" align="left">
                        <asp:DropDownList ID="ddlChapters" runat="server" Width="175px" AutoPostBack="True" OnSelectedIndexChanged="ddlChapters_SelectedIndexChanged" ></asp:DropDownList>
                        &nbsp;&nbsp;                 
                     </td>
                  </tr>
                  <tr>
                     <td  align="center" colspan="2">
                        <asp:Label ID="lblReportError" runat="server" Visible="False" ForeColor="Red"></asp:Label>
                     </td>
                  </tr>
                  <tr  runat="server" id="RwEventYear">
                     <td style="height: 27px" align="left">
                        <asp:Label  CssClass="SmallFont" ID="lbleventYear" runat="server" Text="Label">Event Year</asp:Label>
                     </td>
                     <td style="height: 27px" align="left">
                        <asp:DropDownList ID="ddlYear"  Width="175px"  runat="server" AutoPostBack="false" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged" Height="20px" >
                        </asp:DropDownList>
                        &nbsp;&nbsp;
                     </td>
                  </tr>
                  <tr>
                     <td align="left" Class="SmallFont">
                        Transaction Type
                     </td>
                     <td align="left" style="height: 27px">
                        <asp:DropDownList ID="ddlTransactionsType" AutoPostBack="true"  Enabled="false"   Width="185px"  
                           runat="server" Height="20px" 
                           onselectedindexchanged="ddlTransactionsType_SelectedIndexChanged">
                        </asp:DropDownList>
                     </td>
                  </tr>
                  <tr runat="server" id="reportnew" visible="false">
                     <td align="left" style="height: 27px">
                        <asp:Label  CssClass="SmallFont" ID="lblReport" runat="server" Text="Filing Date"></asp:Label>
                     </td>
                     <td align="left" style="height: 27px">
                        <asp:TextBox ID="txtReport" runat="server" Height="20px" Width="95px"  AutoPostBack="true"
                           ontextchanged="txtReport_TextChanged"></asp:TextBox>
                        <a href="javascript:PopupPicker('txtReport', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                        <br />
                        <asp:Label   CssClass="SmallFont" ID="Label16" runat="server" 
                           ForeColor="Brown" Text="Filing date of the Reimb Form "></asp:Label>
                     </td>
                  </tr>
                  <tr runat="server" id="reportold" visible="false">
                     <td align="left" style="height: 27px">
                        <asp:Label CssClass="SmallFont" ID="Label14" runat="server" Text="Filing Date"></asp:Label>
                        &nbsp;
                     </td>
                     <td align="left" style="height: 27px">
                        <asp:DropDownList ID="ddlReportDate" Width="175px" runat="server" Height="20px" AutoPostBack="true" OnSelectedIndexChanged="ddlReportDate_SelectedIndexChanged" >
                        </asp:DropDownList>
                        &nbsp;<br />
                        <asp:Label ID="Label15" runat="server" CssClass="SmallFont"
                           ForeColor="Brown" Text="Filing date of the Reimb Form "></asp:Label>
                     </td>
                  </tr>
                  <tr>
                     <td></td>
                     <td>
                        <asp:Button Id="btnClearDate" runat="server" Text="Clear Date" Visible="false" 
                           onclick="btnClearDate_Click" />
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
         <%--<tr><td width="50%" align="center"><asp:RadioButton ForeColor="black"  CssClass="SmallFont" ID="RbtnExpense" runat="server" AutoPostBack="True" OnCheckedChanged="RbtnExpense_CheckedChanged"
            Text="Expense" GroupName="Rbtn" />
            </td>
            <td align="center"><asp:RadioButton ID="RbtnRev" ForeColor="black" CssClass="SmallFont" runat="server" AutoPostBack="true" GroupName="RBtn" Text="Revenue" OnCheckedChanged="RbtnRev_CheckedChanged" />
            </td></tr>--%>
      </table>
      <table runat="server" id="tblShowall" visible = "false" >
         <tr>
            <td width="100%">
               <table>
                  <tr>
                     <td width="50%">
                        <asp:Panel ID="Panel1" runat="server"  Visible="false"  Width="100%" >
                           <table width="100%">
                              <tr>
                                 <td style="height: 27px">
                                    &nbsp;
                                 </td>
                                 <td style="height: 27px">
                                    <asp:Label  CssClass="subHeading" ID="lblExpText" runat="server"></asp:Label>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px; height: 27px">
                                 </td>
                                 <td style="height: 27px">
                                    <asp:Label  CssClass="SmallFont" ID="ErrorMsg" runat="server" ForeColor="Red"></asp:Label>
                                 </td>
                              </tr>
                              <tr  runat="server" id="RwEvent">
                                 <td style="height: 27px" align="left">
                                    <asp:Label  CssClass="SmallFont" ID="lblEvent" runat="server" Text="Label">Event</asp:Label>
                                 </td>
                                 <td style="height: 27px" align="left">
                                    <asp:DropDownList ID="ddlEvent"   Width="175px"  runat="server" Height="20px">
                                    </asp:DropDownList>
                                    &nbsp;            
                                 </td>
                              </tr>
                              <tr>
                                 <td style="height: 27px; width: 192px;">
                                    <asp:Label ID="lblincurEx" runat="server" CssClass="SmallFont" Text="Label">Individual Incurring Expenses</asp:Label>
                                 </td>
                                 <td style="height: 27px">
                                    <asp:TextBox ID="txtIncurrEx" runat="server" Enabled="False"></asp:TextBox>
                                    <asp:Button ID="incueredExpFind" runat="server" CausesValidation="False" 
                                       OnClick="Find_Click" Text="Search" />
                                    &nbsp; 
                                    <asp:Button ID="BtnIncClear" runat="server" CausesValidation="False" 
                                       OnClick="BtnIncClear_Click" Text="Clear" />
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblReimburse" runat="server" Text="Person to be Reimbursed"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtReimburse" runat="server" Enabled="False"></asp:TextBox>
                                    <asp:Button ID="ReimburExpFind" runat="server" Text="Search" OnClick="Find_Click" CausesValidation="False"  />
                                    &nbsp;
                                    <asp:Button ID="btnCopy" runat="server" CausesValidation="False" 
                                       OnClick="btnCopy_Click" Text="Same as above" />
                                 </td>
                              </tr>
                              <tr id="trvendorname" runat="server"  >
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="Label17" runat="server" Text="Label">Vendor name</asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtVendorName" runat="server"></asp:TextBox>
                                    <asp:Button ID="FindVendorName" Visible="false"  runat="server" CausesValidation="False" 
                                       OnClick="Find_Click" Text="Search" />
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblExCategory" runat="server" Text="Label">Expense Category</asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="ddlExCategory" runat="server" onselectedindexchanged="ddlExCategory_SelectedIndexChanged" AutoPostBack="true">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr  runat="server" id="trToChapter">
                                 <td style="height: 27px" align="left">
                                    <asp:Label  CssClass="SmallFont" ID="Label20" runat="server" Text="Label">Allocate To Chapter</asp:Label>
                                 </td>
                                 <td style="height: 27px" align="left">
                                    <asp:DropDownList ID="ddlToChapter"  Width="175px"  runat="server" Height="20px" >
                                    </asp:DropDownList>
                                    &nbsp;            
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblExAmount" runat="server" Text="Label">Expense Amount</asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtExAmount" runat="server"></asp:TextBox>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                  <td style="width: 192px"><asp:Label  CssClass="SmallFont" ID="lblSpAmount" runat="server" Text="Label">Sponsor Amount</asp:Label></td>
                                  <td> <asp:TextBox ID="txtSpAmount" runat="server"></asp:TextBox></td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblExDes" runat="server" Text="Comments"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtExDes" runat="server" TextMode="MultiLine"></asp:TextBox>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblDate" runat="server" Text="">Date Incurred <br /> (mm/dd/yyyy)</asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtDate" runat="server"></asp:TextBox>
                                    <a href="javascript:PopupPicker('txtDate', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                                    &nbsp;
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server"  ErrorMessage="Enter Date in MM/DD/YYYY Format " ControlToValidate="txtDate" ValidationExpression="^\s*\d{1,2}(/|-)\d{1,2}\1(\d{4}|\d{2})\s*$"></asp:RegularExpressionValidator>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblTreatement" runat="server" Text="Treatment"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="ddlTreatement" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr id="Panel2" runat="server" visible="false">
                                 <td style="height: 32px; width: 192px;">
                                    <asp:Label  CssClass="SmallFont" ID="lblChapterApprovalFlag" runat="server" Text="ChapterApprovalFlag"></asp:Label>
                                 </td>
                                 <td style="width: 347px; height: 32px;">
                                    <asp:DropDownList ID="ddlChapterApprovalFlag" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr id="Panel5"  runat="server" visible="false">
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="lblNationalApproverFlag" runat="server" Text="NationalApproverFlag"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="ddlNationalApproverFlag" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                           </table>
                        </asp:Panel>
                     </td>
                     <td>
                        <asp:Panel height="337px" ID="PnlRev" runat="server"  Visible="false"  Width="100%" >
                           <table width="100%" runat="server" id="tblRevenue">
                              <tr>
                                 <td style="height: 27px">
                                    &nbsp;
                                 </td>
                                 <td style="height: 27px">
                                    <asp:Label  CssClass="subHeading" ID="lblRevText" runat="server"></asp:Label>
                                 </td>
                              </tr>
                              <tr id="Tr1" runat="server">
                                 <td style="height: 27px">
                                    &nbsp;
                                 </td>
                                 <td style="height: 27px">
                                    <asp:Label  CssClass="SmallFont" ID="ErrorRev" runat="server" ForeColor="Red"></asp:Label>
                                 </td>
                              </tr>
                              <tr>
                                 <td style="height: 27px" align="left">
                                    <asp:Label  CssClass="SmallFont" ID="Label19" runat="server" Text="Label">Event</asp:Label>
                                 </td>
                                 <td style="height: 27px" align="left">
                                    <asp:DropDownList ID="ddlRevEvent"  Width="175px"  runat="server" Height="20px" >
                                    </asp:DropDownList>
                                    &nbsp;            
                                 </td>
                              </tr>
                              <tr ID="Rtr1" runat="server" visible="true">
                                 <td style="height: 27px">
                                    <asp:Label ID="Label4" runat="server" CssClass="SmallFont" 
                                       Text="Revenue Source"></asp:Label>
                                 </td>
                                 <td style="height: 27px">
                                    <asp:DropDownList ID="DdlRevSource" runat="server" AutoPostBack="True" 
                                       Height="20px" OnSelectedIndexChanged="DdlRevSource_SelectedIndexChanged" 
                                       Width="160px">
                                       <asp:ListItem>Sponsorship</asp:ListItem>
                                       <asp:ListItem>Donation</asp:ListItem>
                                       <asp:ListItem>Fees</asp:ListItem>
                                       <asp:ListItem>Sales</asp:ListItem>
                                       <asp:ListItem Selected="True">Select</asp:ListItem>
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr  id="Rtr2"  visible="false" runat="server">
                                 <td>
                                    <asp:Label  CssClass="SmallFont" ID="Label6" runat="server" Text="Sponsoring For"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="DdlRevSprFor" runat="server" Enabled="False" Height="20px" Width="220px">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr id="Rtr3"  visible="false"  runat="server" >
                                 <td>
                                    <asp:Label  CssClass="SmallFont" ID="Label5" runat="server" Text="Type of Sponsor"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="DdlRevSprType" runat="server" Enabled="False" Height="20px" Width="160px">
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr   id="Rtr5"   visible="false" runat="server">
                                 <td style="height: 24px">
                                    <asp:Label  CssClass="SmallFont" ID="Label8" runat="server" Text="Donation Purpose"></asp:Label>
                                 </td>
                                 <td style="height: 24px">
                                    <asp:DropDownList ID="DdlDonationPurpose" runat="server" Height="20px" Width="160px" Enabled="False">
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr   id="Rtr6"  visible="false"  runat="server">
                                 <td>
                                    <asp:Label  CssClass="SmallFont" ID="Label10" runat="server" Text="Type of Fee" ></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="DdlRevFeetype" runat="server" Height="20px" Width="160px" Enabled="False">
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr id="Rtr7"  visible="false" runat="server" >
                                 <td style="height: 14px">
                                    <asp:Label  CssClass="SmallFont" ID="Label11" runat="server" Text="Sales Type"></asp:Label>
                                 </td>
                                 <td style="width: 347px; height: 14px;">
                                    <asp:DropDownList ID="ddlRevSalesType" runat="server" Height="20px" Width="160px" Enabled="False">
                                    </asp:DropDownList>
                                 </td>
                              </tr>
                              <tr   id="Rtr8"  runat="server" >
                                 <td style="width: 156px">
                                    <asp:Label  CssClass="SmallFont" ID="Label12" runat="server" Text="Revenue Amount"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="TxtRevAmt" runat="server" Height="20px" Width="160px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr   id="Rtr9"  runat="server" >
                                 <td style="width: 156px">
                                    <asp:Label  CssClass="SmallFont" ID="Label18" runat="server" Text="Giver's Name "></asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtGiverName" runat="server" Height="20px" Width="160px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr id="RTr10"  runat="server" >
                                 <td style="width: 156px">
                                    <asp:Label  CssClass="SmallFont" ID="Label1" runat="server" Text="Cheque/Cash/CC"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="DdlRevType" runat="server" Height="20px" Width="160px">
                                       <asp:ListItem Selected="True">Select One</asp:ListItem>
                                       <asp:ListItem>Check</asp:ListItem>
                                       <asp:ListItem>Cash</asp:ListItem>
                                       <asp:ListItem>CC(Credit Card)</asp:ListItem>
                                       <asp:ListItem>DC(Debit Card)</asp:ListItem>
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr id="RTr11"  runat="server">
                                 <td style="width: 156px">
                                    <asp:Label  CssClass="SmallFont" ID="Label2" runat="server" Text="Comments"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtRevComments" runat="server" TextMode="MultiLine" Height="50px" Width="160px"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr   id="Rtr12" runat="server">
                                 <td>
                                    <asp:Label  CssClass="SmallFont" ID="Label9" runat="server" Text="">Date Received<br /> (mm/dd/yyyy)</asp:Label>
                                 </td>
                                 <td>
                                    <asp:TextBox ID="txtRevDate" runat="server" Height="20px" Width="160px"></asp:TextBox>
                                    &nbsp;
                                    <a href="javascript:PopupPicker('txtRevDate', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator2" runat="server"  ErrorMessage="Enter Date in MM/DD/YYYY Format " ControlToValidate="txtRevDate" ValidationExpression="^\s*\d{1,2}(/|-)\d{1,2}\1(\d{4}|\d{2})\s*$"></asp:RegularExpressionValidator>
                                 </td>
                              </tr>
                              <tr id="Panel7" runat="server" visible="false">
                                 <td style="height: 32px; width: 192px;">
                                    <asp:Label  CssClass="SmallFont" ID="Label7" runat="server" Text="ChapterApprovalFlag"></asp:Label>
                                 </td>
                                 <td style="width: 347px; height: 32px;">
                                    <asp:DropDownList ID="DdlRevChapterApproval" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                              <tr id="Panel8"  runat="server" visible="false">
                                 <td style="width: 192px">
                                    <asp:Label  CssClass="SmallFont" ID="Label13" runat="server" Text="NationalApproverFlag"></asp:Label>
                                 </td>
                                 <td>
                                    <asp:DropDownList ID="DdlRevNationalApproval" runat="server">
                                    </asp:DropDownList>
                                    &nbsp;
                                 </td>
                              </tr>
                           </table>
                        </asp:Panel>
                     </td>
                  </tr>
                  <tr>
                     <td colspan = "2"  align="center" style="height: 32px">
                        <div id="div11" visible="false" style="width :950px"   runat="server" align="center" >
                           <br /><br />
                           <table border="0" cellpadding="3" cellspacing ="0">
                              <tr>
                                 <td class="SmallFont" align="left" > Bank</td>
                                 <td align="left" >
                                    <asp:DropDownList ID="ddlBankId" runat="server" >
                                    </asp:DropDownList>
                                 </td>
                                 <td class="SmallFont" align="left"   >
                                    <div runat =server visible="false" id="trans1">
                                       Restriction Type : 
                                       <asp:DropDownList ID="ddlRestTypeFrom" runat="server">
                                          <asp:ListItem Value="Perm Restricted">Perm Restricted</asp:ListItem>
                                          <asp:ListItem Value="Temp Restricted">Temp Restricted</asp:ListItem>
                                          <asp:ListItem Selected="True" Value="Unrestricted">Unrestricted</asp:ListItem>
                                       </asp:DropDownList>
                                    </div>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="SmallFont"  align="left" >
                                    <asp:Label ID="lblPayTo" CssClass="SmallFont"  runat="server" Text="Pay To"></asp:Label>
                                 </td>
                                 <td align="left" >
                                    <asp:DropDownList ID="ddlPayTo" DataTextField="ReimbursedTo" Visible="false" DataValueField="Value" runat="server">
                                    </asp:DropDownList>
                                    <asp:ListBox ID="lstChPayTo" SelectionMode="Multiple" Visible="false"  
                                       DataTextField="ReimbursedTo" DataValueField="Value" runat="server" Rows="5"></asp:ListBox>
                                 </td>
                                 <td class="SmallFont" align="left" visible="false"  >
                                    <div runat = server visible="false" id="trans2">
                                       Restriction Type : 
                                       <asp:DropDownList ID="ddlRestTypeTo" runat="server">
                                          <asp:ListItem Value="Perm Restricted">Perm Restricted</asp:ListItem>
                                          <asp:ListItem Value="Temp Restricted">Temp Restricted</asp:ListItem>
                                          <asp:ListItem Selected="True"  Value="Unrestricted">Unrestricted</asp:ListItem>
                                       </asp:DropDownList>
                                    </div>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="SmallFont"  align="left" > Check Number</td>
                                 <td align="left" >
                                    <asp:TextBox ID="txtCheckNumber" runat="server"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="SmallFont"  align="left" > Date Paid</td>
                                 <td align="left" >
                                    <asp:TextBox ID="txtCheckAllPaid" runat="server"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td class="SmallFont"  align="left" > Total Amount Paid</td>
                                 <td align="left" >
                                    <asp:TextBox ID="txtTotalAmount" runat="server"></asp:TextBox>
                                 </td>
                              </tr>
                              <tr>
                                 <td colspan ="2" align ="center">
                                    <asp:Button ID="BtnUpdatePaidAll" runat="server" Text="Update All" 
                                       onclick="BtnUpdatePaidAll_Click" />
                                    &nbsp;
                                    <asp:Button 
                                       ID="BtnUpdatePaidClose" runat="server"
                                       Text="Cancel" onclick="BtnUpdatePaidClose_Click" />
                                    <br />
                                    <asp:HiddenField ID="hdnTransID" runat="server" />
                                    <asp:Label ID="lblerrpaidAll" ForeColor="Red"  runat="server" CssClass="ErrorFont"></asp:Label>
                                 </td>
                              </tr>
                           </table>
                           <br /><br />
                        </div>
                        <div align="center">
                           <asp:Button ID="btnAdd" runat="server" Text="Add" OnClick="btnAdd_Click" />
                           <asp:Button ID="btnCancel" runat="server" Text="Clear" CausesValidation="False" OnClick="btnCancel_Click" />
                           <asp:Label  CssClass="SmallFont" ID="lblMessage" runat="server" Text=""></asp:Label>
                           &nbsp;&nbsp;&nbsp;&nbsp; 
                           <asp:Button ID="btnApproveAll" runat="server" Enabled="false"  
                              Text="Approve All" onclick="btnApproveAll_Click" />
                           &nbsp;&nbsp;&nbsp;&nbsp;
                           <asp:Button ID="BtnPaid" runat="server" Enabled="false"  
                              Text="Paid On All" onclick="BtnPaid_Click"   />
                        </div>
                     </td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
      <br />
      <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
         <b> Search NSF member</b>   
         <div align = "center">
            <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Donor Type:</td>
                  <td align="left" >
                     <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server" 
                        onselectedindexchanged="ddlDonorType_SelectedIndexChanged">
                        <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                        <asp:ListItem Value="Organization">Organization</asp:ListItem>
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
                  <td align="left" >
                     <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
                  <td  align ="left" >
                     <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Organization Name:</td>
                  <td  align ="left" >
                     <asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
                  <td align="left">
                     <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox>
                  </td>
               </tr>
               <tr>
                  <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
                  <td align="left" >
                     <asp:DropDownList ID="ddlState" runat="server">
                     </asp:DropDownList>
                  </td>
               </tr>
               <tr>
                  <td align="right">
                     <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                  </td>
                  <td  align="left">
                     <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>
                  </td>
               </tr>
            </table>
            <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
            <br />
            <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
               <b> Search Result</b>
               <asp:GridView   HorizontalAlign="Left" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                  <Columns>
                     <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                     <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                     <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                     <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                     <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
                     <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                     <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                     <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                     <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                     <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                     <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                  </Columns>
               </asp:GridView>
            </asp:Panel>
         </div>
      </asp:Panel>
      <br />
      <asp:Panel runat="server" ID="panel3"  Width="100%" Visible="False">
         <asp:Label  CssClass="SmallFont" ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Approve"></asp:Label>
         <br />
         <br />
         <asp:GridView CssClass="SmallFont" ForeColor="Black"  ID="GridView1" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
            BorderColor="#336666"  OnPageIndexChanging="grdView_PageIndexChanging" AllowPaging="true" PageSize="20"  PagerStyle-Mode ="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small"  AllowSorting="True">
            <Columns>
               <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
               <asp:ButtonField ButtonType="Button" CommandName="Approve" Text="Approve" HeaderText="Approve" />
               <asp:ButtonField ButtonType="Button" CommandName="Paid" Text="Paid" HeaderText="Paid" />
               <%--<asp:ButtonField ButtonType="Button" CommandName="DeleteRow" Text="Delete" HeaderText="Delete" />--%>
               <asp:TemplateField>
                  <ItemTemplate>
                     <asp:LinkButton ID="LinkDelete" runat="server" CommandName="DeleteRow"  CommandArgument='<%# Eval("TransactionID") %>' OnClientClick="return ConfirmOnDelete();">Delete</asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="IncName"  HeaderText="IncurredBy" />
               <asp:BoundField DataField="ExpCatCode" HeaderText="ExpCatCode"/>
               <asp:BoundField DataField="TreatDesc" HeaderText="TreatCode" />
               <asp:BoundField DataField="ReportDate" HeaderText="ReportDate"  DataFormatString="{0:d}"/>
               <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}"/>
               <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}"/>
               <asp:BoundField DataField="SpAmount" HeaderText="SpAmount" DataFormatString="{0:c}"/>
               
               <asp:BoundField DataField="RName" HeaderText="ReimbursedTo" />
               <asp:BoundField DataField="DonorType" HeaderText="DonorType" />
               <asp:BoundField DataField="ToChapterCode" HeaderText="ToChapter" />
               <asp:BoundField DataField="Providername" HeaderText="Vendor Name" />
               <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval"/>
               <asp:BoundField DataField="Comments" HeaderText="Comments" />
               <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover"/>
               <asp:BoundField DataField="NationalApprover" HeaderText="NatApprover"/>
               <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval"/>
               <asp:BoundField DataField="CheckNumber" HeaderText="CheckNumber"/>
               <asp:BoundField DataField="BankID" HeaderText="BankID"/>
               <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}"/>
               <asp:BoundField DataField="ExpCatID" HeaderText="ExpCatID" />
               <asp:BoundField DataField="Account" HeaderText="Account" />
               <asp:BoundField DataField="TreatID" HeaderText="TreatID" />
               <asp:BoundField DataField="ReimbMemberID" HeaderText="ReMembID"/>
               <asp:BoundField DataField="EventName" HeaderText="Event"/>
               <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"/>
            </Columns>
         </asp:GridView>
         <div  style="vertical-align:middle; text-align:center" >
            <asp:Label ID="lblExpenseTot" runat="server"></asp:Label>
            &nbsp;&nbsp;&nbsp;
            <asp:Button ID="BtnExpExport" Enabled="false" runat="server" Text="Export to Excel"  onclick="BtnRevExport_Click" />
            &nbsp;&nbsp;&nbsp;
            <asp:Label ID="lblTobePaid" runat="server"></asp:Label>
         </div>
         <div>
            <table align="center">
               <tr>
                  <td align="center">
                     <asp:Label ID="lblExpError" runat="server" Text="" ForeColor="Red" Font-Bold="true"></asp:Label>
                  </td>
               </tr>
            </table>
         </div>
      </asp:Panel>
      <asp:GridView CssClass="SmallFont" ForeColor="Black"  ID="gvTransfer" OnRowCommand="gvTransfer_RowCommand" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
         BorderColor="#336666"  OnPageIndexChanging="gvTransfer_PageIndexChanging" AllowPaging="true" PageSize="20"  PagerStyle-Mode ="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small"  AllowSorting="True">
         <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="CheckNumber"  HeaderText="CheckNumber" />
            <asp:BoundField DataField="TransType"  HeaderText="Transaction_Type" />
            <asp:BoundField DataField="ReportDate" HeaderText="ReportDate"  DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}"/>
            <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="BankFrom" HeaderText="BankFrom" />
            <asp:BoundField DataField="BankTo" HeaderText="BankTo" />
            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"/>
            <asp:BoundField DataField="RestTypeFrom" HeaderText="RestTypeFrom"/>
            <asp:BoundField DataField="RestTypeTo" HeaderText="RestTypeTo"/>
         </Columns>
      </asp:GridView>
      <br/>
      <asp:Panel runat="server" ID="panel6"  Width="100%" Visible="False">
         <asp:Label  CssClass="SmallFont" ID="Pnl6Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>
         <br />
         <br />
         <asp:GridView ID="GridView2" CssClass="SmallFont" ForeColor="Black" runat="server" DataKeyNames="TransactionID" AutoGenerateColumns="False"  OnRowCommand="GridView2_RowCommand" GridLines=Both CellPadding=4 BackColor="White" BorderWidth="3px" BorderStyle="Double"
            BorderColor="#336666"  OnPageIndexChanging="grdView1_PageIndexChanging" AllowPaging="true" PageSize=20  PagerStyle-Mode ="numericpages" PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size="Small"  AllowSorting="True">
            <Columns>
               <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
               <asp:ButtonField ButtonType="Button" CommandName="Approve" Text="Approve" HeaderText="Approve" />
               <%-- <asp:ButtonField ButtonType="Button" CommandName="DeleteRow" Text="Delete" HeaderText="Delete" />--%>
               <asp:TemplateField>
                  <ItemTemplate>
                     <asp:LinkButton ID="LinkDelete" runat="server" CommandName="DeleteRow"  CommandArgument='<%# Eval("TransactionID") %>' OnClientClick="return ConfirmOnDelete();">Delete</asp:LinkButton>
                  </ItemTemplate>
               </asp:TemplateField>
               <asp:BoundField DataField="RevSource"  HeaderText="RevSource" />
               <asp:BoundField DataField="RevCatCode" HeaderText="From"/>
               <asp:BoundField DataField="SponsorCode" HeaderText="Given For" />
               <asp:BoundField DataField="ReportDate" HeaderText="ReportDate"  DataFormatString="{0:d}"/>
               <asp:BoundField DataField="DateIncurred" HeaderText="Date Incurred" DataFormatString="{0:d}"/>
               <asp:BoundField DataField="DonPurposeCode" HeaderText="DonPurpose"/>
               <asp:BoundField DataField="ExpenseAmount" HeaderText="Amount" DataFormatString="{0:c}"/>
               <asp:BoundField DataField="FeesType" HeaderText="Fee Type"/>
               <asp:BoundField DataField="Comments" HeaderText="Comments" />
               <asp:BoundField DataField="ProviderName" HeaderText="Giver's Name" />
               <asp:BoundField DataField="SalesCatCode" HeaderText="SalesCat"/>
               <asp:BoundField DataField="PaymentMethod" HeaderText="Payment Method"/>
               <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
               <asp:BoundField DataField="CreatedDate" HeaderText="Created Date"  DataFormatString="{0:d}"/>
               <asp:BoundField DataField="ChapterApprovalFlag" HeaderText="ChApproval"/>
               <asp:BoundField DataField="ChapterApprover" HeaderText="ChApprover"/>
               <asp:BoundField DataField="NationalApprovalFlag" HeaderText="NatApproval"/>
               <asp:BoundField DataField="NationalApprover" HeaderText="NatApproval" />
               <asp:BoundField DataField="Account" HeaderText="Account" />
               <asp:BoundField DataField="EventName" HeaderText="Event"/>
               <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"/>
            </Columns>
         </asp:GridView>
             <div style="vertical-align:middle; text-align:center">
                    <asp:Label ID="lblRevenueTot" runat="server"></asp:Label>&nbsp;&nbsp;&nbsp;
                    <asp:Button ID="BtnRevExport" runat="server" Enabled="false"  Text="Export to Excel" onclick="BtnRevExport_Click" />
             </div>
      </asp:Panel>
      <br />
      <table border="0" cellpadding= "2" cellspacing = "0">
         <tr>
            <td>
               <asp:HyperLink CssClass="SmallFont" ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
            </td>
            <td width="10px">
            </td>
            <td>
               <asp:HyperLink  CssClass="SmallFont" ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx" Visible="false">[Logout]</asp:HyperLink>
            </td>
            <td>  <asp:HiddenField ID="hdnSQlQuery" runat="server" />
                  <asp:HiddenField ID="hdnTransferTransID" runat="server" />
            </td>
         </tr>
      </table>
 <%--  </form>--%>
</asp:Content>
<%--
   </body> 
   </HTML> 
   --%>