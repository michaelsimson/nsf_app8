<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="BankTransTDA.aspx.vb" Inherits="VRegistration.BankTransTDA" title="TDA Bank Transactions" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
     <script language="javascript" type="text/javascript">
            function PopupPicker(ctl) {
                      var PopupWindow = null;
                      settings = 'width=320,height=250,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                      PopupWindow = window.open('BankTransTDAHelp.aspx?ID=' + ctl, 'BankTransTDA Help', settings);
                      PopupWindow.focus();
                    }
	</script>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" OnClick="lbtnVolunteerFunctions_Click" runat="server">Back to Volunteer Functions</asp:LinkButton>

<%--<asp:hyperlink CssClass="SmallFont" id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>
--%>&nbsp;&nbsp;&nbsp;<br />
<table cellpadding="3" cellspacing="0" style="width:1000px"><tr><td align="center">
<table cellpadding="3" cellspacing="0" bgcolor="#3366cc"><tr><td>
    <table border ="0" cellpadding ="2px" cellspacing ="0" bgcolor="white">
    <tr><td colspan ="4" align = "center"  class="title" bgcolor="#3366cc">
        Brok Transactions</td></tr>
    <tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnUploadFile" Text="Upload Transaction Files" GroupName="Uploadfiles" runat="server" /></td> <td align="left">
    <asp:DropDownList ID="ddlBankUpload" Width="160px"   DataTextField="BankCode" DataValueField ="BankID" runat="server">
    
</asp:DropDownList></td><td align="left" colspan="2"><asp:FileUpLoad id="FileUpLoad1"  runat="server" /><a href="javascript:PopupPicker('File');">Help</a></td></tr>
<tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnVerifyTrans" Text="Verify Transactions" GroupName="Uploadfiles" runat="server" /></td> <td align="left">
    <asp:DropDownList ID="ddlVerifyTrans"  Width="160px"    DataTextField="BankCode" DataValueField ="BankID" runat="server">  
    <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem></asp:DropDownList>

</td><td align="left"></td><td></td></tr>
<tr id="TrMsg" runat = "server" visible = "false"><td colspan = "4" align ="center" >
    <asp:Literal ID="Literal1" runat="server"></asp:Literal>  &nbsp;&nbsp;
    <asp:Button ID="BtnNo" runat="server" Text="Yes"  Width="50px" OnClick="BtnNo_Click"  />
    &nbsp;&nbsp;&nbsp; 
         <asp:Button ID="BtnYes" runat="server" Text="No" OnClick="BtnYes_Click" Width="50px" />
    </td></tr>
<tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnEditUpdate" Text="Edit/Update Transactions" GroupName="Uploadfiles" runat="server" /></td> <td align="left">
<asp:DropDownList ID="ddlEditUpdate" runat="server"  Width="160px"  DataTextField="BankCode" DataValueField ="BankID">
   
    <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem></asp:DropDownList>
</td><td align="left">
 <asp:DropDownList ID="ddlEditUpdateColumn" Width ="150px" OnSelectedIndexChanged = "ddlEditUpdateColumn_SelectedIndexChanged" AutoPostBack = "true" runat="server">
 <asp:ListItem Value="0">Search Column</asp:ListItem>
 <asp:ListItem Value="1">Parse_Desc</asp:ListItem>
 <asp:ListItem Value="2">Posted</asp:ListItem>
 <asp:ListItem Value="3">Brok_BankID</asp:ListItem>
 <asp:ListItem Value="4">Brok_BankCode</asp:ListItem>
 <asp:ListItem Value="5">Brok_Ticker</asp:ListItem>
 <asp:ListItem Value="6">Brok_ExpCheckNo</asp:ListItem>
 <asp:ListItem Value="7">Brok_DepCheckNo</asp:ListItem>
 <asp:ListItem Value="8">Brok_Medium</asp:ListItem>
 <asp:ListItem Value="9">Brok_TransType</asp:ListItem>
 <asp:ListItem Value="10">Brok_AssetClass</asp:ListItem>
 <asp:ListItem Value="11">Brok_TransCat</asp:ListItem>
 </asp:DropDownList></td><td><asp:DropDownList ID="ddlEditUpdateSel" runat="server">
        <asp:ListItem Value="0">Null</asp:ListItem>
        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
    </asp:DropDownList></td></tr>
<tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnPostDB" Text="Post Transactions to DB" GroupName="Uploadfiles" runat="server" /></td> <td align="left">
    <asp:DropDownList ID="ddlPostDB"  Width="160px"   DataTextField="BankCode" DataValueField ="BankID" runat="server">   
   </asp:DropDownList>
</td><td align="left"></td><td></td></tr>
<tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnEditUpdateDB" Text="Edit/Update Trans DB" GroupName="Uploadfiles" runat="server" /></td> <td align="left">
<asp:DropDownList ID="ddlEditUpdateDB" runat="server"  Width="160px"    DataTextField="BankCode" DataValueField ="BankID" >
   </asp:DropDownList>
</td><td align="left">
 <asp:DropDownList ID="ddlEditUpdateColumnDB"  Width="150px" OnSelectedIndexChanged = "ddlEditUpdateColumnDB_SelectedIndexChanged" AutoPostBack = "true" runat="server">
 <asp:ListItem Value="0">Search Column</asp:ListItem>
 <asp:ListItem Value="1">Ticker</asp:ListItem>
 <asp:ListItem Value="2">TransType</asp:ListItem>
 <asp:ListItem Value="3">AssetClass</asp:ListItem>
 <asp:ListItem Value="4">TransCat</asp:ListItem>
 <asp:ListItem Value="5">ExpCheckNo</asp:ListItem>
 <asp:ListItem Value="6">DepCheckNo</asp:ListItem>
 <asp:ListItem Value="7">Medium </asp:ListItem>
 </asp:DropDownList>
</td><td>
<asp:DropDownList ID="ddlEditUpdateSelDB" runat="server">
        <asp:ListItem Value="0">Null</asp:ListItem>
        <asp:ListItem Selected="True" Value="1">Yes</asp:ListItem>
    </asp:DropDownList>

</td></tr>

<tr><td align="left"><asp:RadioButton CssClass="SmallFont" ForeColor="Black" ID="RbtnReports" Text="Reports" GroupName="Uploadfiles" runat="server"  Enabled="false"/></td> 
<td align="left"><asp:DropDownList ID="ddlReports"  Width="160px"  DataTextField="BankCode" DataValueField ="BankID" runat="server" Enabled="false">
     <asp:ListItem Selected="True" Value="0">Select Transaction Type</asp:ListItem></asp:DropDownList>
</td><td align="left"></td><td></td></tr>
<tr><td colspan = "4" align ="center" ><asp:Button id="Button1" Text="Continue" OnClick="Button1_Click" runat="server" /></td></tr>
<tr><td colspan = "4" align ="center" ><asp:Label id="Label1" runat="server" ForeColor="Red"/></td></tr>
<tr><td colspan = "4" align ="center" style="height :5px; background-color:#3366cc" ></td></tr>
        <tr>
            <td align="left" colspan="4">
                <asp:Label ID="Label3" CssClass="SmallFont" ForeColor="Black" runat="server" Text="Add/Modify Tables : "></asp:Label> 
                <asp:Button ID="btnChase" runat="server" Text="BrokDesc" Width="80px" />&nbsp;
                <asp:Button ID="btnTicker" runat="server" Text="Ticker" Width="120px" />&nbsp;</td>
        </tr>
    </table>

</td></tr></table>
</td></tr></table>

<asp:Panel ID="PnlChaseTemp" runat="server" Visible="false">
<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="lblChaseTemp" CssClass="keytext" runat="server" Text="BrokTemp"></asp:Label><br />     
    <asp:DataGrid ID="ChaseResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="BrokTempID"
					Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
					<ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>  
					<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor="Gainsboro" ></FooterStyle>
					<%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                            <Columns>
                                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                                <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
                                <asp:BoundColumn DataField="BrokTempID"   HeaderText="BrokTempID" readonly=true Visible="true" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="AcctNumber" ItemStyle-Width="10%">
                                <ItemTemplate>
                                <asp:Label ID="lblAcctNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AcctNumber")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtAcctNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AcctNumber")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>  
                                 <asp:TemplateColumn HeaderText="AcctDesc" ItemStyle-Width="10%">
                                <ItemTemplate>
                                <asp:Label ID="lblAcctDesc" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AcctDesc")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtAcctDesc" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AcctDesc")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>                                                               
                                <asp:BoundColumn DataField="Date" headerText="Date" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Description" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>' Height = "125px" Width = "200px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Symbol" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblSymbol" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Symbol")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtSymbol" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Symbol")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
				                <asp:TemplateColumn HeaderText="Type" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Type")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Type")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>

				                <asp:TemplateColumn HeaderText="Quantity" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Quantity")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtQuantity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Quantity")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
				                <asp:TemplateColumn HeaderText="Price" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblPrice" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Price")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtPrice" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Price")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
				                <asp:TemplateColumn HeaderText="Comm" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblComm" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Comm")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtComm" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Comm")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="NetAmount"  HeaderText="Amount" readonly=true Visible="true" DataFormatString="{0:c}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="TranID"  HeaderText="TranID" readonly=true Visible="true" ></asp:BoundColumn>

                               <asp:TemplateColumn HeaderText="AssetType" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblAssetType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AssetType")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtAssetType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AssetType")%>'  ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Parse_Desc" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblParse_Desc" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtParse_Desc" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Parse_Desc")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Posted"  HeaderText="Posted" readonly=true Visible="True"></asp:BoundColumn>
                                <asp:BoundColumn DataField="Brok_BankID" HeaderText="Brok_BankID" readonly=true  Visible="True" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Brok_Ticker" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_Ticker" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_Ticker")%>' Height = "125px" ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtBrok_Ticker" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_Ticker")%>' Height = "125px" ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="Brok_TransType" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_TransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_TransType")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                               <asp:TextBox ID="txtBrok_TransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_TransType")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Brok_AssetClass" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_AssetClass" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_AssetClass")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                               <asp:TextBox ID="txtBrok_AssetClass" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_AssetClass")%>'></asp:TextBox>
                             </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Brok_ExpCheckNo" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_ExpCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_ExpCheckNo")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtBrok_ExpCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_ExpCheckNo")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Brok_DepCheckNo" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_DepCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_DepCheckNo")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtBrok_DepCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_DepCheckNo")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Brok_TransCat" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_TransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_TransCat")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtBrok_TransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_TransCat")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Brok_Medium" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblBrok_Medium" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_Medium")%>'></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtBrok_Medium" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Brok_Medium")%>'></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                </Columns>
</asp:DataGrid></asp:Panel>
<asp:Panel ID="pnlBankTrans" runat="server" Visible="false">
<br />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" CssClass="keytext" runat="server" Text="BrokTrans"></asp:Label><br /> 
   
<asp:DataGrid ID="BankTransResults" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="BrokTransID"
					Height="14px" GridLines=Both CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666"  AllowPaging=true PageSize=50  PagerStyle-Mode =numericpages PagerStyle-BackColor="#99cccc" PagerStyle-Font-Size=Small>
					<AlternatingItemStyle backcolor=WhiteSmoke font-size=small></AlternatingItemStyle>
					<ItemStyle backcolor=white Wrap="False" Font-Size=Small ></ItemStyle>  
					<HeaderStyle  BorderStyle=Solid Font-Size=Small Font-Bold="True" backcolor=Gainsboro></HeaderStyle>
					<FooterStyle BackColor="Gainsboro" ></FooterStyle>
					<%--IMPORTANT: Whenever a new column is added or removed, also update the cell index accordingly for all the controls in the .vb file (updatecommand)--%>
                            <Columns>
                                <asp:EditCommandColumn ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                                <asp:buttoncolumn ButtonType="LinkButton"  CommandName="Delete" Text="Delete" Visible="false" ></asp:buttoncolumn>
                                <asp:BoundColumn DataField="BrokTransID"   HeaderText="BrokTransID" readonly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="BankID"   HeaderText="BankID" readonly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="TransDate" headerText="TransDate" ReadOnly=true Visible="true" DataFormatString="{0:d}" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="TransType" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblTransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtTransType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransType")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="TransCat" ItemStyle-Width="30%">
                                <ItemTemplate>
                                     <asp:Label ID="lblTransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransCat")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                    <asp:DropdownList ID="ddlTranscat" runat="server" OnPreRender="SetTransCat_Dropdown">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
					                        <asp:ListItem Value="BUY" Text="BUY"></asp:ListItem>
                                            <asp:ListItem Value="DIV" Text="DIV"></asp:ListItem>
                                            <asp:ListItem Value="Donation" Text="Donation"></asp:ListItem>
                                            <asp:ListItem Value="Grants" Text="Grants"></asp:ListItem>
                                            <asp:ListItem Value="SELL" Text="SELL"></asp:ListItem>
                                            <asp:ListItem Value="Split" Text="Split"></asp:ListItem>
                                            <asp:ListItem Value="Reinvest" Text="Reinvest"></asp:ListItem>
                                            <asp:ListItem Value="STCGDiv" Text="STCGDiv"></asp:ListItem>
                                            <asp:ListItem Value="OrdDiv" Text="OrdDiv"></asp:ListItem>
                                            <asp:ListItem Value="LTCGDiv" Text="LTCGDiv"></asp:ListItem>
                                            <asp:ListItem Value="TransferIn" Text="TransferIn"></asp:ListItem>
                                            <asp:ListItem Value="TransferOut" Text="TransferOut"></asp:ListItem>
                                     </asp:DropdownList>
<%--                                <asp:TextBox ID="txtTransCat" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TransCat")%>' ></asp:TextBox>
--%>                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="Medium" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblMedium" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Medium")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                            <asp:DropdownList ID="ddlMedium" runat="server" OnPreRender="SetMedium_Dropdown">
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
					                        <asp:ListItem Value="Check" Text="Check"></asp:ListItem>
                                            <asp:ListItem Value="IntTransfer" Text="IntTransfer"></asp:ListItem>
                                            <asp:ListItem Value="ElecTransfer" Text="ElecTransfer"></asp:ListItem>                                           
                                     </asp:DropdownList>
                               <%-- <asp:TextBox ID="txtMedium" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Medium")%>' ></asp:TextBox>--%>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="AssetClass" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblAssetClass" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AssetClass")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtAssetClass" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.AssetClass")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="Ticker" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblTicker" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Ticker")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtTicker" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Ticker")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                
                                <asp:TemplateColumn HeaderText="Quantity" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblQuantity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Quantity")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtQuantity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Quantity")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                               
                               <asp:TemplateColumn HeaderText="Price" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblPrice" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Price")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtPrice" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Price")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                               
                               
                               <asp:TemplateColumn HeaderText="NetAmount" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.NetAmount")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.NetAmount")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                               
                              
                               
                               <%-- <asp:BoundColumn DataField="Quantity"   HeaderText="Quantity" readonly=true Visible="true" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="Price"   HeaderText="Price" readonly=true Visible="true"  DataFormatString="{0:c}" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="NetAmount"   HeaderText="NetAmount" readonly=true Visible="true"  DataFormatString="{0:c}" ></asp:BoundColumn>
--%>                                <asp:BoundColumn DataField="Comm"   HeaderText="Comm" readonly=true Visible="true"  DataFormatString="{0:c}" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="TranID" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblTranID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TranID")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtTranID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TranID")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ExpCheckNo" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblExpCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ExpCheckNo")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtExpCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ExpCheckNo")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DepCheckNo" ItemStyle-Width="30%">
                                <ItemTemplate>
                                <asp:Label ID="lblDepCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepCheckNo")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                <asp:TextBox ID="txtDepCheckNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepCheckNo")%>' ></asp:TextBox>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="Description"   HeaderText="Description" readonly=true Visible="true" ></asp:BoundColumn>
                               <asp:TemplateColumn HeaderText="RestType" ItemStyle-Width="30%">
                                <ItemTemplate>
                                     <asp:Label ID="lblRestType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RestType")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                      <asp:DropdownList ID="ddlRestType" runat="server" OnPreRender="SetRestType_Dropdown"> 
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="Unrestricted" Text="Unrestricted"></asp:ListItem>
                                            <asp:ListItem Value="Temp Restricted " Text="Temp Restricted "></asp:ListItem>
                                            <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>  
                                      </asp:DropdownList>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ToRestType" ItemStyle-Width="30%">
                                <ItemTemplate>
                                     <asp:Label ID="lblToRestType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ToRestType")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                    <asp:DropdownList ID="ddlToRestType" runat="server" OnPreRender="SetToRestType_Dropdown">  
                                            <asp:ListItem Value="" Text="Select"></asp:ListItem>
                                            <asp:ListItem Value="Unrestricted" Text="Unrestricted"></asp:ListItem>
                                            <asp:ListItem Value="Temp Restricted " Text="Temp Restricted "></asp:ListItem>
                                            <asp:ListItem Value="Perm Restricted" Text="Perm Restricted"></asp:ListItem>  
                                      </asp:DropdownList>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn HeaderText="Donation" ItemStyle-Width="30%">
                                <ItemTemplate>
                                     <asp:Label ID="lblDonation" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donation")%>' ></asp:Label>
                                </ItemTemplate>      
                                <EditItemTemplate>
                                        <asp:DropdownList ID="ddlDonation" runat="server" OnPreRender="SetToRestType_Donation">  
                                            <asp:ListItem Value="N" Text="No"></asp:ListItem>
                                            <asp:ListItem Value="Y" Text="Yes"></asp:ListItem>
                                       </asp:DropdownList>
                                </EditItemTemplate>                                                                  
                                </asp:TemplateColumn>
                                
                                
                                </Columns>
  			                </asp:DataGrid>
  			         </asp:Panel>
<asp:Panel ID="pnlChaseDesc" runat="server" Visible="false">
    <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004"><tr><td align = "center"> 
        <center>
   <table cellpadding="3" cellspacing="0" border="2"  ><tr><td align = "center"> 
    <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
        <tr>
            <td align="center" colspan="3">
                <b>
                Add/Update BrokDesc</b>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
        </tr>
         <tr>
            <td align="left">
                Token</td>
            <td>
            </td>
            <td align="left">
    <asp:TextBox ID="txtToken" runat="server" Width="150px" Height="18px"></asp:TextBox>
            </td>
        </tr>
    <tr><td align = "left">
       Bk_TransCat</td><td></td><td align="left"> <asp:TextBox ID="txtBk_TransCat" runat="server" Width="150px" Height="18px"></asp:TextBox>
           </td>
    </tr>    
       
        <tr>
            <td align="left">
                 Bk_VendorCust</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtBk_VendorCust" runat="server" Width="150px" Height="18px"></asp:TextBox>&nbsp;
                </td>
        </tr>
        <tr>
            <td align="left">
               Bk_Reason</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtBk_Reason" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
                <asp:Button ID="btnClose" runat="server" Text="Close Panel" /></td>
        </tr>
        <tr>
           <td align="center" colspan="3">
               <asp:GridView ID="GridChaseDesc" runat="server" AutoGenerateColumns="False" 
                   DataKeyNames="BrokDescID" OnRowCommand="GridChaseDesc_RowCommand">
                   <Columns>
                       <asp:ButtonField ButtonType="Button" CommandName="Modify" HeaderText="Modify" 
                           Text="Modify" />
                       <asp:BoundField DataField="BrokDescID" HeaderText="BrokDescID" />
                       <asp:BoundField DataField="Token" HeaderText="Token" />
                       <asp:BoundField DataField="Brok_TransCat" HeaderText="Brok_TransCat" />
                       <asp:BoundField DataField="Brok_Medium" HeaderText="Brok_Medium" />
                       <asp:BoundField DataField="Notes" HeaderText="Notes(Bk_Reason)" />
                   </Columns>
               </asp:GridView>
               <asp:Label ID="lblChaseDesc" runat="server" Text=""></asp:Label>
            </td>
        </tr>
        
        </table>        
   
    </td></tr>  </table> 
    </center>
    </td></tr></table>
    </asp:Panel>
<asp:Panel ID="pnlTicker" runat="server" Visible="false">
    <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004"><tr><td align = "center"> 
        <center>
   <table cellpadding="3" cellspacing="0" border="2"  ><tr><td align = "center"> 
    <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
        <tr>
            <td align="center" colspan="3">
                <b>
                Add/Update Ticker</b>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
        </tr>
         <tr>
            <td align="left">
                Name</td>
            <td>
            </td>
            <td align="left">
    <asp:TextBox ID="txtName" runat="server" Width="150px" Height="18px"></asp:TextBox>
            </td>
        </tr>
    <tr><td align = "left">
       Ticker</td><td></td><td align="left"> <asp:TextBox ID="txtTicker" runat="server" Width="150px" Height="18px"></asp:TextBox>
           </td>
    </tr>    
       
        <tr>
            <td align="left">
                 Class</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtClass" runat="server" Width="150px" Height="18px"></asp:TextBox>&nbsp;
                </td>
        </tr>
        <tr>
            <td align="left">
               Year</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtYear" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
        </tr>
        <tr>
	            <td align="left">
	                Asset Class</td>
	            <td>
	            </td>
	            <td align="left">
	                <asp:TextBox ID="txtAssetClass" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnTicAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnTicCancel" runat="server" Text="Cancel" Width = "60" />
                <asp:Button ID="btnTicClose" runat="server" Text="Close Panel" /></td>
        </tr>
        <tr>
           <td align="center" colspan="3">
               <asp:Label ID="lblTicker" runat="server" Text="" ForeColor="Red"></asp:Label>
            </td>
        </tr>
        
        </table>        
   
    <asp:GridView ID="GridTicker" runat="server" DataKeyNames="ID" AutoGenerateColumns="False"  OnRowCommand="GridTicker_RowCommand" >
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="ID"  HeaderText="ID" />
            <asp:BoundField DataField="Name" HeaderText="Name"/>
            <asp:BoundField DataField="Ticker" HeaderText="Ticker" />
            <asp:BoundField DataField="Class" HeaderText="Class" />
            <asp:BoundField DataField="Year" HeaderText="Year"/>
            <asp:BoundField DataField="AssetClass" HeaderText="AssetClass"/> 
       </Columns>
    </asp:GridView>
    </td></tr>  </table> 
    </center>
    </td></tr></table>
    </asp:Panel> 
</asp:Content>




