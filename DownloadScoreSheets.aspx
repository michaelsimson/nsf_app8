﻿<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<%@ Import Namespace="NativeExcel"%>

<script language="vb" runat="server">
Sub Page_Load(Sender As Object, E As EventArgs)
     
    Dim File_Name As String = Session("File_Name").ToString()
    Dim Product_ID As Integer =Session("Product_ID")
    Dim Chapter_ID As Integer = Session("Chapter_ID")

    Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & File_Name & ".xls"))
    Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\Master\" & File_Name & ".xls"))
    Dim Sheet1 As IWorksheet
    Sheet1 = xlWorkBook.Worksheets(1)
    Dim i As Integer
    Dim x as Integer
      
    Dim str As IRange = Sheet1.Range(5, 28)
      'Sheet1.Unprotect(PWD)
               Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheetPhase3", New SqlParameter("@ContestID", Product_ID))

                Dim MaxValue As Integer = 0

                If ds1.Tables(0).Rows.Count > 0 Then
                    i = 7
                    For x = 0 To ds1.Tables(0).Rows.Count - 1
                        If x = 0 Then
                            Sheet1.Range(2, 1).Value = ds1.Tables(0).Rows(x)("center")
                        End If
                        Sheet1.Range(i, 1).Value = ds1.Tables(0).Rows(x)("BadgeNumber")
                        Sheet1.Range(i, 2).Value = ds1.Tables(0).Rows(x)("ParticipantName")
                        Sheet1.Range(i, 3).Value = ds1.Tables(0).Rows(x)("DOB")
                        Sheet1.Range(i, 4).Value = ds1.Tables(0).Rows(x)("Grade")
                        'store the contestant id in column AA
                        Sheet1.Range(i, 27).Value = ds1.Tables(0).Rows(x)("ContestantID")
                        'Sheet1.Range(i, rank).Value = ds1.Tables(0).Rows(x)("P1_P2_Rank")
                        If MaxValue < ds1.Tables(0).Rows(x)("P1_P2_Rank") Then
                            MaxValue = ds1.Tables(0).Rows(x)("P1_P2_Rank")
                        End If
                        i = i + 1
                    Next
                End If
    Dim ds2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select COUNT(*) as Count,MAX(P1_P2_Rank) as MaxiValue from dbo.ScoreDetail c WHERE(c.P1_P2_Rank <= 15) and c.ContestID =11790")
        
          str = Sheet1.Cells(10, 28)
          Dim printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString()
          Sheet1.PageSetup.PrintArea = printarea
          'Sheet1.Protect(PWD)
          Session("P3flag") =True
    
        Try
            If file.Exists Then 'set appropriate headers
                Response.Write("FileName" & File_Name)
                Response.Clear()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                
                Response.AddHeader("Content-Disposition", "attachment;filename=" & File_Name & ".xls")
               
                xlWorkBook.SaveAs(Response.OutputStream)
                Response.WriteFile(file.FullName)
                    
  
                    
            
                               
                
                 Response.End()  
            Else
                Response.Write("This file does not exist.")
            End If 
        Catch ex As Exception
                Response.Write(ex.ToString)
        End Try
       Response.Redirect("ManageScoresheet.aspx")
         
End Sub
</script>

