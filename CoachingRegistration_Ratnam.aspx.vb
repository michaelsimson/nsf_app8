Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System
Partial Class CoachingRegistration
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("EntryToken") = "Parent"
        ''**Debug 
        'Session("CustIndID") = 15405
        'Session("LoginID") = 15405
        'Session("LoggedIn") = "True"
        'Session("EventID") = 13
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        Page.MaintainScrollPositionOnPostBack = True
        If Not Page.IsPostBack Then
            Session("Checklog") = 0
            LoadSelectedCoaching()
            Loadchild(Session("CustIndID"))
        End If
        If HlblBtnDonate.Text = "Y" Then
            btnDonate.Visible = True
        End If
        lblNote.Text = " Note:Product Level Pricing flag 'N' Calculates the Reg fee only once for the Product group as a whole. <br />"

    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "select First_name+' '+Last_name as Name, ChildNumber From Child where memberid=" & ParentID
        Dim drChild As SqlDataReader
        drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        ddlChild.Enabled = True
        LoadProduct()
        LoadGrade()
        'ddlChild.Items(0).Selected = True
    End Sub
    Private Sub LoadCoaching()
        lblerr.Text = ""
        TrDeadlineDate.Visible = False
        'TrLateDLDate.Visible.Visible = False
        If Not ddlProduct.SelectedIndex = 0 Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(CR.CoachRegID) from CoachReg CR  Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) and CR.EventYear=C.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo where CR.ChildNumber=" & ddlChild.SelectedValue & " and CR.ProductID = " & ddlProduct.SelectedValue & "  and CR.EventYear=year(GEtDate())") < 1 Then 'and GETDATE()< C.Enddate
                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim SQLStr As String
                SQLStr = "select C.SignUpID,P.Name as ProductName,EF.RegFee,Case WHEN GETDATE() >DateAdd(dd, 1, EF.DeadlineDate) THEN  EF.LateFee ELSE '' End as LateFee,EF.ProductLevelPricing,I.FirstName + ' ' + I.LastName as CoachName,"
                'SQLStr = SQLStr & "Case When Ch.Grade In (6,7,8) and P.ProductGroupCode in ('SAT') then 'Junior' else case when Ch.GRADE in(9,10,11,12) and P.ProductGroupCode in ('SAT') then 'Senior' else C.Level End End as Level,"
                SQLStr = SQLStr & "C.Level,"
                SQLStr = SQLStr & "C.MaxCapacity,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100) + ' EST' as Time,C.StartDate,C.Enddate,EF.LateRegDLDate,I.City,I.State,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone,I.Email"
                SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END + ' hr(s)' as Duration, "
                SQLStr = SQLStr & "(select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null) AND CR1.EventYear=C1.EventYear AND C1.Phase = CR1.Phase AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount from  Child Ch"

                'SQLStr = "select  C.SignUpID,P.ProductCode as ProductName,EF.RegFee,EF.LateFee,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,C.CoachDayTime,C.StartDate,C.Enddate,I.City,I.State,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,CASE WHEN C.CoachDayTime LIKE '%EST%' THEN 1 ELSE CASE WHEN C.CoachDayTime LIKE '%CST%' THEN 2 ELSE CASE WHEN C.CoachDayTime LIKE '%MST%' THEN 3 ELSE 4 END END END AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and CR1.[Level]=C1.[Level] AND CR1.EventYear=C1.EventYear AND C1.Phase = CR1.Phase where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount from  Child Ch"
                SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear >=" & Now.Year() & " and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)"
                SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
                SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and P.ProductId=" & ddlProduct.SelectedValue
                SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                SQLStr = SQLStr & " Left Join ChildCoachLevel CL ON ((Case when C.ProductGroupCode not in('UV') then CL.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CL.Level end)Is null) and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'"
                SQLStr = SQLStr & " where ch.ChildNumber = " & ddlChild.SelectedValue & " and C.Accepted='Y' and( Ch.GRADE Between EF.GradeFrom and EF.GradeTo  or  P.ProductGroupCode='UV' )"
                If ddlProduct.SelectedItem.Value = 106 Or ddlProduct.SelectedValue = 107 And (ddlProduct.SelectedValue <> 109 And ddlProduct.SelectedValue <> 110 And ddlProduct.SelectedValue <> 111) Then 'Only for SAT
                    If (ddlGrade.SelectedValue = 6 Or ddlGrade.SelectedValue = 7 Or ddlGrade.SelectedValue = 8) Then
                        SQLStr = SQLStr & " and C.Level in ('Junior')"
                    Else
                        SQLStr = SQLStr & " and C.Level in ('Senior')"
                    End If

                End If
                If ddlProduct.SelectedValue = 109 Or ddlProduct.SelectedValue = 110 Or ddlProduct.SelectedValue = 111 Then 'Only for SAT
                    If (ddlGrade.SelectedValue = 1 Or ddlGrade.SelectedValue = 2 Or ddlGrade.SelectedValue = 3) Then
                        SQLStr = SQLStr & " and C.Level in ('Junior')"
                    ElseIf (ddlGrade.SelectedValue = 4 Or ddlGrade.SelectedValue = 5 Or ddlGrade.SelectedValue = 6 Or ddlGrade.SelectedValue = 7 Or ddlGrade.SelectedValue = 8) Then
                        SQLStr = SQLStr & " and C.Level in ('Intermediate')"
                    Else
                        SQLStr = SQLStr & " and C.Level in ('Senior')"
                    End If

                End If

                'SQLStr = SQLStr & " and GETDATE()< C.Enddate AND ((EF.CoachSelCriteria IS NULL OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' AND CL.Eligibility IS NOT NULL)) Order BY LevelID,C.StartDate,TimeZone,I.LastName,I.FirstName"
                SQLStr = SQLStr & " AND ((EF.CoachSelCriteria IS NULL OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' AND CL.Eligibility IS NOT NULL)) Order BY P.ProductId,LevelID,CASE C.Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end,CAST(C.TIME AS TIME),I.LastName,I.FirstName" ' ,C.StartDate,TimeZone,C.Level"

                'Response.Write(SQLStr & "<br />")
                Dim drCoaching As SqlDataReader
                Try
                    drCoaching = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
                Catch ex As Exception
                    'Response.Write(ex.ToString() & "<br />" & SQLStr)
                End Try
                dgCoachSelection.DataSource = drCoaching
                dgCoachSelection.DataBind()
                pnlCoachSelection.Visible = True
                If dgCoachSelection.Items.Count > 0 Then
                    dgCoachSelection.Visible = True
                    lblCoachSelection.Text = ""
                Else
                    dgCoachSelection.Visible = False
                    lblCoachSelection.ForeColor = Color.Red
                    lblCoachSelection.Text = "No Eligible Coaching session Available"
                End If
            Else
                lblerr.Text = "Sorry, you had selected this product already"
                pnlCoachSelection.Visible = False
            End If
            Dim dsEvent As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select DeadlineDate,LateRegDLDate From EventFees Where EventID=" & Session("EventID") & "  and EventYear >=" & Now.Year() & " and ProductID= " & ddlProduct.SelectedValue & "")       ' and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)")
            If dsEvent.Tables(0).Rows.Count > 0 Then
                TrDeadlineDate.Visible = True
                'TrLateDLDate.Visible.Visible = True
                lblDLDate.Text = dsEvent.Tables(0).Rows(0)("DeadlineDate")
                lblLateDLDate.Text = dsEvent.Tables(0).Rows(0)("LateRegDLDate")
            Else
                TrDeadlineDate.Visible = False
                'TrLateDLDate.Visible.Visible = False
            End If
        Else
            pnlCoachSelection.Visible = False
        End If
    End Sub
    Private Sub LoadProduct()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        Dim SQLStr1 As String
        'SQLStr = "select Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductID from  Child Ch"
        SQLStr = "select P.Name as ProductName,P.ProductID from  Child Ch"
        SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & "  and Ef.EventYear >=" & Now.Year()
        SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
        SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear"
        SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
        SQLStr = SQLStr & " where ch.ChildNumber = " & ddlChild.SelectedValue & " and (Ch.GRADE Between EF.GradeFrom and Ef.GradeTo or  P.ProductGroupCode='UV')"
        'SQLStr = SQLStr & " and GETDATE()< C.Enddate and GETDATE()< DATEADD(Hour,24,EF.LateRegDLDate) and P.Status='O' Group by P.Name,P.ProductCode,P.ProductID"
        SQLStr = SQLStr & " and GETDATE()< DATEADD(Hour,24,EF.LateRegDLDate) and P.Status='O' Group by P.Name,P.ProductCode,P.ProductID"

        'Response.Write(SQLStr)

        Dim drProduct As SqlDataReader
        Try
            drProduct = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            ddlProduct.DataSource = drProduct
            ddlProduct.DataBind()
           
            If ddlProduct.Items.Count > 0 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
                lblerr.Text = ""
                If (ddlProduct.Items.FindByText("Universal Values 201").Text = "Universal Values 201") Then
                    SQLStr1 = SQLStr1 & " Select Count(CoachRegID) as count from CoachReg where EventYear<2014 and ProductCode = 'UV101' and Approved='Y' and childnumber =   " & ddlChild.SelectedValue & ""
                    'Response.Write(SQLStr1)
                    Dim drContestCategory As SqlDataReader
                    drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr1)
                    If drContestCategory.Read() Then
                        If drContestCategory("count") = 0 Then
                            Dim removeListItem As ListItem = ddlProduct.Items.FindByText("Universal Values 201")
                            ddlProduct.Items.Remove(removeListItem)
                        End If
                    End If
               
                ElseIf (ddlProduct.Items.FindByText("Universal Values 301").Text = "Universal Values 301") Then
                    SQLStr1 = SQLStr1 & " Select Count(CoachRegID) as count from CoachReg where EventYear<2014 and ProductCode = 'UV201' and Approved='Y' and childnumber =   " & ddlChild.SelectedValue & ""
                    'Response.Write(SQLStr1)
                    Dim drContestCategory As SqlDataReader
                    drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr1)
                    If drContestCategory.Read() Then
                        If drContestCategory("count") = 0 Then
                            Dim removeListItem As ListItem = ddlProduct.Items.FindByText("Universal Values 301")
                            ddlProduct.Items.Remove(removeListItem)
                        End If
                    End If
                End If
                If (ddlProduct.Items.FindByText("Universal Values 101").Text = "Universal Values 101") Then
                    SQLStr1 = SQLStr1 & " Select Count(CoachRegID) as count from CoachReg where EventYear<2014 and ProductCode = 'UV101' and Approved='Y' and childnumber =   " & ddlChild.SelectedValue & ""
                    'Response.Write(SQLStr1)
                    Dim drContestCategory As SqlDataReader
                    drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr1)
                    If drContestCategory.Read() Then
                        If drContestCategory("count") > 0 Then
                            Dim removeListItem As ListItem = ddlProduct.Items.FindByText("Universal Values 101")
                            ddlProduct.Items.Remove(removeListItem)
                        End If
                    End If
                End If
            Else
                lblerr.ForeColor = Color.Red
                lblerr.Text = "No eligible coaching is available.  Select another child."
            End If
        Catch ex As Exception
            lblerr.Text = SQLStr & "<br>" & ex.ToString()

        End Try
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
        LoadProduct()
        LoadGrade()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadCoaching()
    End Sub
    Private Sub clear()
        pnlCoachSelection.Visible = False
        ddlProduct.Enabled = False
        TrDeadlineDate.Visible = False
        'TrLateDLDate.Visible.Visible = False
    End Sub
    Private Sub LoadGrade()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "select Distinct Grade From Child where ChildNumber=" & ddlChild.SelectedValue
        Dim drGrade As SqlDataReader
        drGrade = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlGrade.DataSource = drGrade
        ddlGrade.DataBind()
        ddlGrade.Enabled = False
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim intCtr As Integer
        lblNotReg.Text = "Unable To Register for the following Coaching since Capacity is Full"
        lblNotReg.Visible = False
        lblRegErr.Text = " You cannot select following coach(s) since you already have a coach for the same Product Group."
        lblRegErr.Visible = False
        Dim myTableCell As TableCell, chkSelected As CheckBox, SignUpID As Integer
        If dgCoachSelection.Items.Count <= 0 Then Exit Sub
        For intCtr = 0 To dgCoachSelection.Items.Count - 1
            myTableCell = dgCoachSelection.Items.SyncRoot.Item(intCtr).Cells(1)
            chkSelected = myTableCell.Controls(1)
            If chkSelected.Checked = True Then
                myTableCell = dgCoachSelection.Items.SyncRoot.Item(intCtr).Cells(0)
                SignUpID = CInt(myTableCell.Text)
                Dim result As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN count(CR.CoachRegID)<(select MaxCapacity from CalSignUp where SignUpID=" & SignUpID & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) and CR.EventYear=C.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo where CR.Approved='Y' AND C.SignUpID=" & SignUpID & "")

                If result.ToLower = "true" Then
                    InsertChild(SignUpID)
                Else
                    lblNotReg.Visible = True
                    lblNotReg.ForeColor = Color.Red
                    lblNotReg.Text = lblNotReg.Text & " <br>   " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + ' - Level : ' + C.Level from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & "")
                End If
            End If
        Next
        'btnSubmit.Visible = False
        pnlCoachSelection.Visible = False
        LoadCoaching()
        LoadSelectedCoaching()
        lblerr.Text = ""
        PayError.Text = ""
    End Sub
    Protected Sub chkEvent_OnCheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dgItem As DataGridItem
        Dim dgItem1 As DataGridItem
        'Dim selectRadButton As CheckBox
        Dim selectRadioButton As CheckBox
        Dim selectRadioButton1 As CheckBox
        Dim index1, index2 As Integer
        Dim i As Integer = dgCoachSelection.SelectedIndex
        lblGridError.Text = ""
        For Each dgItem In dgCoachSelection.Items
            selectRadioButton = dgItem.FindControl("chkCoachSelect")
            If selectRadioButton.Checked = True Then
                index1 = dgItem.ItemIndex '.Items.DataKeys(dgItem.ItemIndex)
            End If
            For Each dgItem1 In dgCoachSelection.Items
                selectRadioButton1 = dgItem1.FindControl("chkCoachSelect")
                If selectRadioButton1.Checked = True Then
                    index2 = dgItem1.ItemIndex
                End If

                If index1 <> index2 Then
                    If selectRadioButton1.Checked = True And selectRadioButton.Checked = True Then
                        If selectRadioButton1.Checked = True Then 'Session("ChkIndex") = index1 And
                            selectRadioButton.Checked = False
                        ElseIf selectRadioButton.Checked = True Then 'Session("ChkIndex") = index2 And
                            selectRadioButton1.Checked = False
                        End If
                    End If
                End If
            Next
        Next
    End Sub
    Private Sub InsertChild(ByVal SignUpID As Integer)

        '"Select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp CC ON CC.Level = CR.Level and CC.EventYear=CR.EventYear and CC.ProductID=CR.ProductID where CR.childnumber=" & ddlChild.SelectedValue & " and CC.SignUpID=" & SignUpID & " and CC.EndDate > GETDATE()"
        Dim SqlCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp CC ON CC.ProductGroupID  = CR.ProductGroupID and CC.EventYear=CR.EventYear AND CC.Phase = CR.Phase and CC.SessionNo = CR.SessionNo   where CR.childnumber=" & ddlChild.SelectedValue & " and CC.SignUpID=" & SignUpID & "") ' and CC.EndDate > GETDATE()")
        Dim SQlPrd_Pricing As String = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select ProductLevelPricing From EventFees Where EventID=" & Session("EventID") & "  and EventYear >=" & Now.Year() & " and ProductID= " & ddlProduct.SelectedValue & "")       ' and GETDATE() < DateAdd(dd, 1, EF.LateRegDLDate)")

        If CheckChildData(SignUpID) = True Then
            Exit Sub
        End If

        If SqlCnt < 1 Then  ' Then And SQlPrd_Pricing = "Y"
            Try
                SqlHelper.ExecuteNonQuery(Application("connectionString"), CommandType.Text, "Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Phase,SessionNo,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N'," & childGrade(ddlChild.SelectedValue) & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Phase,C.SessionNo, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") ' Case When GETDATE()> DateAdd((dd, 1,EF.DeadlineDate) Then EF.RegFee+Ef.LateFee Else
                lblCoachSelection.Text = "Inserted Successfully"
            Catch ex As Exception
                'Response.Write(ex.ToString() & "Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Phase,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N'," & childGrade(ddlChild.SelectedValue) & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Phase, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") '"Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved, CreateDate, CreatedBy,Phase,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N',GETDATE()," & Session("loginID") & ",C.Phase,EF.RegFee  from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") '''Case When GETDATE()> EF.DeadlineDate Then EF.RegFee+Ef.LateFee Else
            End Try
        ElseIf SqlCnt >= 1 And SQlPrd_Pricing = "N" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("connectionString"), CommandType.Text, "Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Phase,SessionNo,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N'," & childGrade(ddlChild.SelectedValue) & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Phase,C.SessionNo, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") ' Case When GETDATE()> DateAdd((dd, 1,EF.DeadlineDate) Then EF.RegFee+Ef.LateFee Else
                lblCoachSelection.Text = "Inserted Successfully"
            Catch ex As Exception
                'Response.Write(ex.ToString() & "Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved,Grade,ChapterID, CreateDate, CreatedBy,Phase,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N'," & childGrade(ddlChild.SelectedValue) & "," & Session("CustIndChapterID") & ",GETDATE()," & Session("loginID") & ",C.Phase, EF.RegFee from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") '"Insert Into CoachReg(CMemberID, PMemberID, ChildNumber, EventYear, EventID, ProductID, ProductCode, [Level], ProductGroupID, ProductGroupCode, Approved, CreateDate, CreatedBy,Phase,Fee) select C.MemberID," & Session("CustIndID") & "," & ddlChild.SelectedValue & ", C.EventYear,13,C.ProductID,C.ProductCode,C.[Level],C.ProductGroupID,C.ProductGroupCode,'N',GETDATE()," & Session("loginID") & ",C.Phase,EF.RegFee  from CalSignUp C Inner Join EventFees EF ON EF.EventID=13 and C.EventYear = EF.eventYear and C.ProductID = Ef.ProductID  where C.SignUpID=" & SignUpID & "") '''Case When GETDATE()> EF.DeadlineDate Then EF.RegFee+Ef.LateFee Else
            End Try
        Else
            lblRegErr.Text = lblRegErr.Text & " <br> " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + case when C.Level is null then '' else + ' - Level : ' + C.level end as Level from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & "")
            lblRegErr.Visible = True
        End If
    End Sub
    Public Function CheckChildData(ByVal SignUpId As Integer) As Boolean
        Dim dgItem As DataGridItem
        Dim Lbl1 As Label
        Dim LblDay As Label
        Dim LblTime As Label
        Dim RetString As Boolean = False
        lblGridError.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Day,CAST(TIME AS TIME) as Time From CalSignUp Where SignUpId=" & SignUpId)
        If ds.Tables(0).Rows.Count > 0 Then

            For Each dgItem In dgselected.Items
                Dim SignUpID_1 As Integer = dgItem.Cells(4).Text
                Lbl1 = dgItem.FindControl("lblChildName")
                LblDay = dgItem.FindControl("lblCoachDay")
                LblTime = dgItem.FindControl("lblTime")
                Dim Time1, Time2 As DateTime

                If ddlChild.SelectedItem.Text = Lbl1.Text Then
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Day,CAST(TIME AS TIME) as Time From CalSignUp Where SignUpId=" & SignUpID_1)
                    If LblDay.Text = ds.Tables(0).Rows(0)("Day") Then
                        Time1 = Convert.ToDateTime(ds.Tables(0).Rows(0)("Time").ToString())
                        Time2 = Convert.ToDateTime(ds1.Tables(0).Rows(0)("Time").ToString())
                        If DateDiff(DateInterval.Hour, Time1, Time2) >= -2 And DateDiff(DateInterval.Hour, Time1, Time2) <= 2 Then
                            lblGridError.Text = "The selected item is too close to the other selections already made for the child."
                            lblGridError.Visible = True
                            RetString = True
                        End If
                    End If
                End If
            Next
        End If

        Return RetString
    End Function
    Function childGrade(ByVal childnumber As Integer) As Integer
        Dim Grade As Integer = 0
        Grade = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Grade from child where Childnumber=" & childnumber & "")
        Return Grade
    End Function

    Protected Sub dgCoachSelection_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgCoachSelection.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim SignUpID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "SignUpID"), Integer)
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "  Select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp CC ON CC.MemberID = CR.CMemberID and ((Case when CC.ProductGroupCode not in('UV') then CR.Level end)=CC.Level Or (Case when CC.ProductGroupCode in('UV') then CR.Level end)Is null) and CC.ProductID=CR.ProductID and CR.EventYear=CC.EventYear AND CC.Phase = CR.Phase and CC.SessionNo = CR.SessionNo  where CR.childnumber=" & ddlChild.SelectedValue & " and CR.EventYear=" & Now.Year & " and CC.SignUpID=" & SignUpID & "") > 0 Then
                    CType(e.Item.FindControl("chkCoachSelect"), CheckBox).Visible = False
                    Dim lbl As Label = CType(e.Item.FindControl("lblStatus"), Label)
                    lbl.Text = "Selected"
                    lbl.Visible = True
                Else
                    btnSubmit.Visible = True
                End If
        End Select
    End Sub

    Private Sub LoadSelectedCoaching()
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim SQLStr As String
            Dim ProductFee As Double = 0.0
            Dim ProductGroupFee As Double = 0.0
            SQLStr = "select CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.ProductCode as ProductName,P.Name,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level," 'Case When Ch.Grade In (6,7,8) and P.ProductGroupCode in ('SAT') then 'Junior' else case when Ch.GRADE in(9,10,11,12) and P.ProductGroupCode in ('SAT') then 'Senior' else C.Level End End as Level,"
            SQLStr = SQLStr & " C.MaxCapacity,CR.ChildNumber,C.Day, CONVERT(varchar(15),CAST(C.TIME AS TIME),100)+ ' EST' as Time, C.StartDate,C.Enddate,EF.LateRegDLDate,EF.RegFee,Case When GETDATE()> DateAdd(dd, 1,EF.DeadlineDate) Then EF.LateFee Else '' End As LateFee,EF.ProductLevelPricing,I.City,I.State,Case when CR.Approved ='N' then 'Pending' Else Case when CR.PaymentReference IS NULL THEN 'Approved' ELSE 'Paid' End End as Status,CR.Approved,CASE WHEN C.Level = 'Beginner' THEN 1 ELSE CASE WHEN C.Level = 'Intermediate' THEN 2 ELSE 3 END END AS LEVELID,'%EST%' AS TIMEZone, (select count(CR1.CoachRegID) from CoachReg CR1 Inner Join CalSignUp C1 ON CR1.CMemberID = C1.MemberID and CR1.ProductID=C1.ProductID and ((Case when C1.ProductGroupCode not in('UV') then CR1.Level end)=C1.Level Or (Case when C1.ProductGroupCode in('UV') then CR1.Level end)Is null) and  C1.EventYear = CR1.EventYear AND C1.Phase = CR1.Phase AND C1.SessionNo = CR1.SessionNo where CR1.Approved='Y' AND C1.SignUpID=C.SignUpID) AS ApprovedCount,I.Email "
            SQLStr = SQLStr & ",Case When EF.Duration is Null Then Case when C.ProductGroupCode in('MB') then '2' else Case when C.ProductGroupCode in ('SAT') then '1.5' Else Case when C.ProductGroupCode in ('UV') then '1' End End END	Else convert(varchar(20),EF.Duration)  END +' hr(s)' as Duration "
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join  EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & ""
            'SQLStr = SQLStr & " and GETDATE()< C.Enddate and CR.EventYear=" & Now.Year & " ORDER BY CR.ChildNumber,CR.ProductID,LevelID,TimeZone,I.LastName,I.FirstName"
            SQLStr = SQLStr & " and CR.EventYear=" & Now.Year & " and C.Accepted='Y' ORDER BY CR.ChildNumber,CR.ProductID,LevelID,CASE C.Day WHEN 'Monday' THEN 1 WHEN 'Tuesday' THEN 2 WHEN 'Wednesday' THEN 3 WHEN 'Thursday' THEN 4 WHEN 'Friday' THEN 5 WHEN 'Saturday' THEN 6 WHEN 'Sunday' THEN 7 end, CAST(C.TIME AS TIME), I.LastName, I.FirstName"

            'Response.Write(SQLStr)
            pnlSeleted.Visible = True
            'lblSelected.Text = SQLStr
            Dim drCoaching As SqlDataReader = SqlHelper.ExecuteReader(conn, CommandType.Text, SQLStr)
            dgselected.DataSource = drCoaching
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                dgselected.Visible = True
                Dim Total_Amount As Double = 0.0
                Dim Amt_Product As Double
                Dim Amt_ProductGroup As Double
                Dim RegFee, LateFee As Double
                Dim DiscntProds As String = ""
                Dim DiscountAmt As Double = 0.0
                Dim DiscountFlag As Boolean = False
                Dim dsChild As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Childnumber from CoachReg Where PMemberid=" & Session("CustIndID") & " and EventYear >=" & Now.Year() & " and EventID=" & Session("EventID"))
                For i As Integer = 0 To dsChild.Tables(0).Rows.Count - 1 ' Done for Each child
                  
                    'SP A ProductLevelPricing 'Y'
                    '*****************Sum of UpPaid(PaymentReference is Null) Products under ProductLevelPricing 'N'**************'

                    Dim SqlProdGroupY As String = ""
                    SqlProdGroupY = "Select Distinct CR.ProductGroupID From CoachReg CR  Inner Join EventFees EF on EF.EventYear = CR.EventYear and EF.EventId = CR.EventID and EF.ProductGroupID=CR.ProductGroupID "
                    SqlProdGroupY = SqlProdGroupY & " Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & Now.Year() & " and CR.EventID=" & Session("EventID") & " and EF.ProductLevelPricing ='Y' and CR.ChildNumber=" & dsChild.Tables(0).Rows(i)("ChildNumber") & ""
                    SqlProdGroupY = SqlProdGroupY & " and CR.PaymentReference is Null and CR.Approved='N'" ''Added on 28-09-2013 to include only UnPaid Transactions

                    Dim dsPrGroup1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SqlProdGroupY)

                    For j As Integer = 0 To dsPrGroup1.Tables(0).Rows.Count - 1
                        Try
                            Dim SQLProduct As String = "Select EF.RegFee, Case When GETDATE()>DateAdd(dd, 1,EF.DeadlineDate) Then EF.LateFee Else 0.00 End as LateFee from CoachReg CR Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID and EF.ProductLevelPricing='Y' where CR.PMemberid=" & Session("CustIndID") & " and CR.ChildNumber=" & dsChild.Tables(0).Rows(i)("ChildNumber") & " and CR.ProductGroupID=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & " and CR.PaymentReference is Null  and CR.Approved='N' and C.Accepted='Y' and CR.EventYear>=Year(Getdate())" 'And GETDATE() < C.Enddate
                            Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, SQLProduct)
                            If ds1.Tables(0).Rows.Count > 0 Then
                                ' l As Integer = 0 To ds1.Tables(0).Rows.Count - 1
                                'RegFee = RegFee + ds1.Tables(0).Rows(j)("RegFee")
                                'LateFee = LateFee + ds1.Tables(0).Rows(j)("LateFee")
                                'Amt_Product = Amt_Product + ds1.Tables(0).Rows(j)("RegFee") + ds1.Tables(0).Rows(j)("LateFee") 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProduct)
                                RegFee = RegFee + ds1.Tables(0).Rows(0)("RegFee")
                                LateFee = LateFee + ds1.Tables(0).Rows(0)("LateFee")
                                Amt_Product = Amt_Product + ds1.Tables(0).Rows(0)("RegFee") + ds1.Tables(0).Rows(0)("LateFee") 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProduct)

                                ' Next
                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignUp where (MemberID=" & Session("CustIndID") & " Or MemberID =(Select AutoMemberID From IndSpouse where Relationship=" & Session("CustIndID") & ")) and EventYear=" & Now.Year() & " and ProductGroupId=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID")) > 0 Then
                                    If Not DiscntProds.Contains(dsPrGroup1.Tables(0).Rows(j)("ProductGroupID")) Then
                                        Amt_Product = Amt_Product - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & "")) ' * Amt_Product)
                                        RegFee = RegFee - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & "")) '* RegFee)
                                        'LateFee = LateFee - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & "")) '* LateFee)
                                        DiscntProds = DiscntProds & dsPrGroup1.Tables(0).Rows(j)("ProductGroupID") & ","
                                        lblDiscount.Text = "Parent is a Coach.Child is eligible for Discount "
                                    End If
                                Else
                                    lblDiscount.Text = ""
                                End If
                            End If
                        Catch ex As Exception
                            'Response.Write(ex.ToString())
                        End Try
                    Next
                    'SP B ProductLevelPricing 'N'
                    ''''Sum of UpPaid(PaymentReference is Null) Products under ProductLevelPricing 'N'
                    Dim SqlProdGroupN As String = ""
                    SqlProdGroupN = SqlProdGroupN & " Select Distinct CR.ProductGroupID From CoachReg CR  Inner Join EventFees EF on EF.EventYear = CR.EventYear and EF.EventId = CR.EventID and EF.ProductGroupID=CR.ProductGroupID "
                    SqlProdGroupN = SqlProdGroupN & " Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & Now.Year() & " and CR.EventID=" & Session("EventID") & " and EF.ProductLevelPricing ='N' and CR.ChildNumber=" & dsChild.Tables(0).Rows(i)("ChildNumber") & ""
                    SqlProdGroupN = SqlProdGroupN & " and CR.PaymentReference is Null and CR.Approved='N'" ''Added on 28-09-2013 to include only UnPaid Transactions

                    Dim dsPrGroup2 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SqlProdGroupN)
                    Try
                        For k As Integer = 0 To dsPrGroup2.Tables(0).Rows.Count - 1
                            Try
                                Dim SQLPrdGp_Amt As String = ""
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " select EF.RegFee , Case When GETDATE()>DateAdd(dd, 1,EF.DeadlineDate) Then EF.LateFee Else '' End as LateFee from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductGroupID=C.ProductGroupID AND ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON "
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductGroupID = Ef.ProductGroupID and EF.ProductLevelPricing='N' "
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " where CR.PMemberid=" & Session("CustIndID") & " and CR.ChildNumber= " & dsChild.Tables(0).Rows(i)("ChildNumber") & " and CR.ProductGroupID=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & "  and CR.PaymentReference is Null "
                                SQLPrdGp_Amt = SQLPrdGp_Amt & " and CR.Approved='N' and C.Accepted='Y' and CR.EventYear>=Year(Getdate())" ' and GETDATE()< C.Enddate 
                                Dim ds2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, SQLPrdGp_Amt)
                                If ds2.Tables(0).Rows.Count > 0 Then
                                    'For m As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                                    'RegFee = RegFee + ds2.Tables(0).Rows(k)("RegFee")
                                    'LateFee = LateFee + ds2.Tables(0).Rows(k)("LateFee")
                                    'Amt_ProductGroup = Amt_ProductGroup + ds2.Tables(0).Rows(k)("RegFee") + ds2.Tables(0).Rows(k)("LateFee") ' SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLPrdGp_Amt)

                                    RegFee = RegFee + ds2.Tables(0).Rows(0)("RegFee")
                                    LateFee = LateFee + ds2.Tables(0).Rows(0)("LateFee")
                                    Amt_ProductGroup = Amt_ProductGroup + ds2.Tables(0).Rows(0)("RegFee") + ds2.Tables(0).Rows(0)("LateFee") ' SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLPrdGp_Amt)

                                    'Next
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignUp where(MemberID=" & Session("CustIndID") & " Or MemberID =(Select AutoMemberID From IndSpouse where Relationship=" & Session("CustIndID") & ")) and EventYear=" & Now.Year() & " and ProductGroupId=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID")) > 0 Then
                                        If Not DiscntProds.Contains(dsPrGroup2.Tables(0).Rows(k)("ProductGroupID")) Then
                                            Amt_ProductGroup = Amt_ProductGroup - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & "")) '* Amt_Product)
                                            RegFee = RegFee - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & "")) '* RegFee)
                                            'LateFee = LateFee - (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & "")) '* LateFee)
                                            DiscntProds = DiscntProds & "," & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & ""
                                            DiscountAmt = DiscountAmt + SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select DiscountAmt from EventFees Where EventId=" & Session("EventID") & " and EventYear=" & Now.Year() & "and  ProductGroupId=" & dsPrGroup2.Tables(0).Rows(k)("ProductGroupID") & "")
                                            DiscountFlag = True
                                        Else
                                            'lblDiscount.Text = ""
                                        End If
                                    Else
                                        lblDiscount.Text = ""
                                    End If
                                End If
                            Catch ex As Exception
                                'Response.Write(ex.ToString)
                            End Try
                        Next
                    Catch ex As Exception
                        'Response.Write(ex.ToString)
                    End Try
                Next

                Total_Amount = Total_Amount + Amt_Product + Amt_ProductGroup

                Session("TotalFee") = Total_Amount
                Session("RegFee") = RegFee
                Session("LateFee") = LateFee
                If DiscountFlag = True Then
                    lblDiscount.Visible = True
                Else
                    lblDiscount.Visible = False
                End If
                'Dim amount As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN COUNT(EF.RegFee)=0 Then 0.00 Else  SUM(EF.RegFee + Case When GETDATE()>DateAdd(dd, 1,EF.DeadlineDate) Then EF.LateFee Else '' End) End as Donationamt from CoachReg CR Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where CR.PMemberid=" & Session("CustIndID") & " and CR.PaymentReference is Null and GETDATE()< C.Enddate and CR.Approved='N' and CR.EventYear>=Year(Getdate())")
                'lblSelected.Text = "Total Amout to be paid : " & Format$(amount, "Currency")
                lblSelected.Text = "Total Amout to be paid : " & Format$(Total_Amount, "Currency")
                lblDiscount.Text = "Discount Given: " & Format$(DiscountAmt, "Currency")
                Session("DiscountAmt") = DiscountAmt
            Else
                dgselected.Visible = False
                lblSelected.ForeColor = Color.Red
                lblSelected.Text = "No Coaching was selected"
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgselected.ItemCommand
        lblPendingErr.Text = ""
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim CoachRegID As Integer = CInt(e.Item.Cells(0).Text)
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from CoachReg Where CoachRegID=" & CoachRegID & " and Approved='N' and pmemberid=" & Session("CustIndID") & "")
                LoadSelectedCoaching()
                LoadCoaching()
                PayError.Text = ""
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Remove failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub dgselected_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgselected.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim Status As String = CType(DataBinder.Eval(e.Item.DataItem, "status"), String)
                Dim Approved As String = CType(DataBinder.Eval(e.Item.DataItem, "Approved"), String)
                If Status.Trim = "Paid" Then
                    CType(e.Item.FindControl("lbtnRemove"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lblStatus1"), Label).Text = "Paid"
                ElseIf Approved.Trim = "Y" Then
                    CType(e.Item.FindControl("lbtnRemove"), LinkButton).Visible = False
                    CType(e.Item.FindControl("lblStatus1"), Label).Text = "Approved"
                Else
                    HlblBtnDonate.Text = "Y"
                    btnDonate.Visible = True
                    Dim lbl As Label = CType(e.Item.FindControl("lblStatus1"), Label)

                    '************************************************************************************************************************************
                    '** This Session("ContestsSelected") is not used now, We are using Session("ContestsSelected") from CoachingSelectionSum.aspx.vb page **
                    '************************************************************************************************************************************
                    If Not Session("ContestsSelected") = CType(DataBinder.Eval(e.Item.DataItem, "ProductCode"), String) & "(" & CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), String) & ")(" & Session("CustIndID") & ")" Then
                        Session("ContestsSelected") = Session("ContestsSelected") & CType(DataBinder.Eval(e.Item.DataItem, "ProductCode"), String) & "(" & CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), String) & ")(" & Session("CustIndID") & ")"
                    Else
                        Session("ContestsSelected") = CType(DataBinder.Eval(e.Item.DataItem, "ProductCode"), String) & "(" & CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), String) & ")(" & Session("CustIndID") & ")"
                    End If
                End If
        End Select
    End Sub

    Protected Sub btnDonate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDonate.Click
        'Session("Donation")
        ' CheckChildData()
        Dim Product_Flag As Boolean = False
        Dim Flag_Count As Integer = 0
        Dim Row_cnt As Integer = 0
        Dim ProdGroupErr As String = ""

        'Session("RegFee") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN COUNT(EF.RegFee)=0 Then 0.00 Else  SUM(EF.RegFee) End as Donationamt from CoachReg CR Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where CR.PMemberid=" & Session("CustIndID") & "and CR.PaymentReference is Null and GETDATE()< C.Enddate and CR.Approved='N' and CR.EventYear>=Year(Getdate())")
        'Session("LATEFEE") = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Case When GETDATE()>DateAdd(dd, 1,EF.DeadlineDate) Then EF.LateFee Else '' End as LateFee from CoachReg CR Inner Join CalSignUp C ON  CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID where CR.PMemberid=" & Session("CustIndID") & "and CR.PaymentReference is Null and GETDATE()< C.Enddate and CR.Approved='N' and CR.EventYear>=Year(Getdate())")

        Dim SQLChild As String = ""
        Dim SQLProdGroup As String = ""
        Dim SQLProduct As String = ""
        Dim SQLProuctChk As String = ""

        SQLChild = SQLChild & "Select Distinct ChildNumber,Grade From CoachReg Where PMemberID=" & Session("CustIndID") & " and EventYear=" & Now.Year() & " and EventID=" & Session("EventID") & ""
        Dim ds_Child As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLChild)

        If ds_Child.Tables(0).Rows.Count > 0 Then
            For i As Integer = 0 To ds_Child.Tables(0).Rows.Count - 1
                SQLProdGroup = "Select Distinct CR.ProductGroupID,CR.ProductGroupCode from CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID and EF.ProductLevelPricing ='N' Where CR.PMemberID = " & Session("CustIndID") & " And CR.EventYear = " & Now.Year() & " And CR.EventID = " & Session("EventID") & "  And CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber") & " And Grade =" & ds_Child.Tables(0).Rows(i)("Grade")
                Dim ds_ProdGroup As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLProdGroup)
                If ds_ProdGroup.Tables(0).Rows.Count > 0 Then
                    For j As Integer = 0 To ds_ProdGroup.Tables(0).Rows.Count - 1
                        SQLProduct = "Select Count(Distinct CR.ProductID) From CoachReg CR Inner join EventFees EF on EF.EventId =CR.EventID and EF.EventYear= CR.EventYear and EF.ProductGroupID =CR.ProductGroupID  and EF.ProductLevelPricing ='N' Where CR.PMemberID=" & Session("CustIndID") & " and CR.EventYear=" & Now.Year() & " and CR.EventID=" & Session("EventID") & " and CR.ChildNumber = " & ds_Child.Tables(0).Rows(i)("ChildNumber") & " and Grade= " & ds_Child.Tables(0).Rows(i)("Grade") & " and CR.ProductGroupID=" & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID") & ""
                        Dim ProductID_Count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProduct)
                        SQLProuctChk = "Select Count(Distinct P.ProductID) from Product P Inner Join EventFees EF on EF.ProductGroupId = EF.ProductGroupID  and  P.ProductId=EF.ProductID and Ef.EventID=P.EventID Where EF.EventID= " & Session("EventID") & " and EF.EventYear=" & Now.Year() & " and P.Status ='O' and EF.ProductLevelPricing='N' and EF.ProductGroupID = " & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupID")
                        Dim ProductIDChk_Count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLProuctChk)
                        If ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count <> ProductIDChk_Count Then
                            'Product_Flag = False
                            ProdGroupErr = ProdGroupErr & ds_ProdGroup.Tables(0).Rows(j)("ProductGroupCode") & ","
                        ElseIf ProductID_Count > 0 And ProductIDChk_Count > 0 And ProductID_Count = ProductIDChk_Count Then
                            ProdGroupErr = ProdGroupErr & ""
                            'Product_Flag = True
                        End If
                    Next
                End If
            Next
        End If

        If ProdGroupErr <> "" Then  'Row_cnt > 0 And Product_Flag = False Then
            Product_Flag = False
            PayError.Text = "Please Select all the Products for the ProductGroup(s) " & ProdGroupErr.TrimEnd(",")
            Exit Sub
        Else
            Product_Flag = True
        End If
        'Exit Sub
        If Session("RegFee") < 1 Then
            lblerr.Text = "No coaching is Pending to be paid"
        ElseIf chkcapacity() And Product_Flag = True Then
            Response.Redirect("CoachingSelectionSum.aspx")
        Else
            lblerr.Text = "Please Change the Coach"
        End If

    End Sub
    Function chkcapacity() As Boolean
        Dim intCtr As Integer
        Dim flag As Boolean = True
        lblNotReg.Text = "There is no more room for following coach.  Please change coach."
        lblNotReg.Visible = False
        Dim myTableCell As TableCell, SignUpID As Integer, status As String
        Try
            If dgselected.Items.Count <= 0 Then Exit Function

            For intCtr = 0 To dgselected.Items.Count - 1
                myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(6)
                Session("CoachEmail") = myTableCell.Text
               
                myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(1)
                status = myTableCell.Text

                If status = "Pending" Then
                    Dim LateRegDate As Date
                    Dim ProductCode As String

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(3)
                    ProductCode = myTableCell.Text.ToString
                    Session("ProductCodeCoach") = ProductCode

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(4)
                    SignUpID = CInt(myTableCell.Text)

                    myTableCell = dgselected.Items.SyncRoot.Item(intCtr).Cells(5)
                    LateRegDate = Date.Parse(myTableCell.Text)

                    Dim currentcount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(CR.CoachRegID) from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear=CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo where CR.paymentreference is NULL and CR.PMemberID = " & Session("CustIndID") & " AND C.SignUpID=" & SignUpID & "")
                    Dim result As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE WHEN count(CR.CoachRegID)+" & currentcount & "<=(select MaxCapacity from CalSignUp where SignUpID=" & SignUpID & ") THEN 'TRUE' ELSE 'FALSE' END from CoachReg CR Inner Join CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear=CR.EventYear and  C.Phase = CR.Phase and C.SessionNo = CR.SessionNo where CR.Approved='Y' AND C.SignUpID=" & SignUpID & "")
                    If result.ToLower = "false" Then
                        flag = False
                        lblNotReg.Visible = True
                        lblNotReg.ForeColor = Color.Red
                        lblNotReg.Text = lblNotReg.Text & " <br> " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select 'Product Name : ' +  P.ProductCode  + ' - Coach Name : ' + I.FirstName + ' ' + I.LastName  + ' - Level : ' + C.Level from CalSignUp C Inner Join Product P ON P.ProductId = C.ProductID  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where C.SignUpID = " & SignUpID & "")
                    End If
                    If Now.Date() > LateRegDate Then
                        lblPendingErr.Text = lblPendingErr.Text & " <br> Sorry Registration Deadline Passed for " & ProductCode & ". Please delete it to proceed."
                        lblPendingErr.Visible = True
                        flag = False
                        Exit For
                    End If
                End If
            Next

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
        Return flag
    End Function


End Class
