﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using VRegistration;

public partial class ExtraSessions : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                loadyear();
                loadPhase(ddlSemester);
                fillCoach();

                if (Session["RoleID"] != "88")
                {
                    fillMeetingGrid();
                }

            }
        }
    }


    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }
    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.Semester='" + ddlSemester.SelectedItem.Value + "' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlCoach.Enabled = false;
                    ddlCoach.SelectedValue = ds.Tables[0].Rows[0]["AutoMemberID"].ToString().Trim();
                    fillProductGroup();
                }
                else
                {
                    ddlCoach.Enabled = true;
                }
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
                fillProductGroup();

            }

        }

    }

    public void fillProductGroup()
    {

        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }

    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlSemester.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            // ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 0;
                ddlProduct.Enabled = false;
                loadLevel();
                LoadSessionNo();


            }
            else
            {
                //loadLevel();
                //LoadSessionNo();

                ddlProduct.Enabled = true;
            }

        }

    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and Semester='" + ddlSemester.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
                LoadSessionNo();
            }
        }

    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }

    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadPhase(ddlSemester);
        fillCoach();
    }
    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevel();
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }

    protected void BtnReset_Click(object sender, EventArgs e)
    {
        loadyear();
        loadPhase(ddlSemester);
        fillCoach();
        fillMeetingGrid();
    }
    public void fillMeetingGrid()
    {

        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;
        if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,vl.vroom,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SignupID,vl.hostid from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.ExtraSessionKey=VC.SessionKey and VC.MemberID=cs.MemberID) inner join virtualroomlookup vl on (vl.userid=vc.userid) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=13 and VC.ChapterID=112 and VC.MemberID=" + ddlCoach.SelectedValue + " ";
            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }
            else if (ddlCoach.SelectedValue != "0" && ddlCoach.SelectedValue != "")
            {
                cmdtext += " and VC.MemberID=" + ddlCoach.SelectedValue + "";
            }


            cmdtext += "order by VC.ProductGroupCode";


        }
        else
        {
            cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SignupID,vl.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup cs on (cs.ExtrasessionKey=vc.Sessionkey) inner join virtualroomlookup Vl on (Vl.UserID=VC.userID) where VC.EventYear='" + ddYear.SelectedValue + "' and VC.EventID=13 and VC.ChapterID=112";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdtext += " and VC.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }
            else if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdtext += " and VC.ProductID=" + ddlProduct.SelectedValue + "";
            }



            cmdtext += " order by VC.ProductGroupCode";
        }

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                }
            }
        }
        catch
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string coachName = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now.AddMinutes(60);
                double mins = 40.0;
                string duration = "-" + ((Label)GrdMeeting.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                int iduration = 0;
                try
                {
                    iduration = Convert.ToInt32(duration);
                }
                catch
                {
                    iduration = 120;
                }
                if (duration == "-")
                {
                    iduration = 120;
                }
                else
                {
                    iduration = Convert.ToInt32(duration);
                }

                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    if (mins < iduration)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                    }
                    else
                    {
                        hdnHostID.Value = hostID;
                        try
                        {
                            listLiveMeetings();
                        }
                        catch
                        {
                        }
                        getmeetingIfo(sessionKey, hostID);
                        string cmdText = "";
                        cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        string meetingLink = hdnHostURL.Value;

                        try
                        {
                            // logerrors(coachName, "Success", "");
                            CoachingExceptionLog.createExceptionLog(coachName, "Success", "", "Extra Sessions");
                        }
                        catch
                        {
                        }

                        hdnZoomURL.Value = meetingLink;
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                    }

                }
                else
                {
                    try
                    {
                        CoachingExceptionLog.createExceptionLog(coachName, "Failed", "", "Makeup Sessions");
                        // logerrors(coachName, "Failed", "");
                    }
                    catch
                    {
                    }
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);




                }
            }
        }
        catch
        {
        }
    }
    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }
            if (serviceType == "6")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    if (m["host_id"].ToString() == hdnHostID.Value)
                    {
                        termianteMeeting(m["id"].ToString(), hdnHostID.Value);
                    }

                }

            }
            if (serviceType == "7")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);

                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            }
        }
        catch
        {
        }
    }
    public void termianteMeeting(string sessionkey, string hostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "7";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hostID + "";
            urlParameter += "&id=" + sessionkey + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }
    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }

    }

    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid();
    }
}