﻿<%@ Page Language="VB" AutoEventWireup="false"  ValidateRequest ="false"  CodeFile="newslettersendmail.aspx.vb" Inherits="newslettersendmail" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
-->
</style>
    <title>Sending News Letter</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <table border="0" cellpadding ="0" cellspacing = "0" width ="1000">
    <tr><td align="center">
   <img src="images/trilogo.gif" width="980px" alt ="" />
	</td></tr>
	<tr>
  <td align="left"> &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
    </td> </tr>
  <tr>
  <td align="center">
    <table border = "0" cellpadding = "3" cellspacing = "0" width="550px">
    <tr><td></td><td></td></tr>
     <tr><td colspan="2" align="center"><h3>&nbsp;Newsletter</h3></td></tr>
     <tr><td align="left">Test EMail ID</td><td align="left">
         <asp:TextBox ID="txttestEmail" runat="server" Width="300px"></asp:TextBox></td></tr>
     <tr><td align="left">Subject </td><td  align="left">
         <asp:TextBox ID="txtSubject" runat="server" Text="NSF Newsletter" Width="300px"></asp:TextBox></td></tr>
           <tr><td colspan="2" align="center">
               <asp:Button ID="btnLoad" runat="server" Text="Load Emails" />&nbsp;&nbsp;&nbsp; <asp:Button ID="btnSend" runat="server" OnClick="btnSend_Click" Text="Send to All" Width="130px" /> &nbsp;&nbsp;&nbsp; 
               <asp:Button ID="btnCheckEmail" runat="server" Text="Check Test Email " Width="130px" />&nbsp;&nbsp;&nbsp; 
               </td></tr>
           <tr><td colspan="2" align="center">
                   <asp:Label ID="lblRemaining" runat="server" ></asp:Label>
               </td></tr>
           <tr><td colspan="2" align="left">
                  * Before sending Newsletter Please upload the New Newsletter into Newsletter folder with filename  <b>Newsletter_M_YYYY.html</b><i> M-Month(1-12), YYYY - Year </i><br />
                  <center><i>You can check newsletter displayed below.</i></center>
                   <br />                               
                  </td></tr>
          </table>
     </td> </tr>
     <tr>
  <td align="center">
              <table border = "0" cellpadding = "3" cellspacing = "0">
           <tr><td align="left" valign="top">
                  <asp:Label ID="lblemailstatus" runat="server" ></asp:Label> </td>
                  <td align="left" valign="top"> 
                 <asp:Label ID="lblBounced" ForeColor="Red" runat="server" ></asp:Label> </td></tr>
    </table>
    </td> </tr>
     <tr>
  <td align="center">
      
           <asp:Literal ID="ltlNewsletter" runat="server"></asp:Literal>
           </td> </tr>
     <tr>
  <td align="left">
   <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table>
  </td></tr>  </table> 
 
    </div>
    </form>
</body>
</html>
