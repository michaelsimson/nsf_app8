﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;


public partial class SurveyTemplate : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // testSurvey();

        LblMsg.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            try
            {

                string SurveyID = Request.QueryString["SurveyID"].ToString();
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Title from SurveyList where SurveyID=" + SurveyID + "");
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        txtSurveyTitle.Text = ds.Tables[0].Rows[0]["Title"].ToString();
                        txtSurveyTitle.Enabled = false;
                        hdnSurveyID.Value = SurveyID;
                        hdnSurveyText.Value = ds.Tables[0].Rows[0]["Title"].ToString();
                    }
                }

                loadQuestions();
            }
            catch
            {
            }
        }


    }

    public void InsertNewQuestion()
    {
        string CmdText = string.Empty;

        string SurveyID = Request.QueryString["SurveyID"].ToString();

        string Title = txtQuestionTitle.Text;
        string QuestType = DDlQuestionType.SelectedValue;
        string ChoiceFormat = DDlChoiceFormat.SelectedValue;
        if (BtnSave.Text == "Save")
        {
            CmdText = "Insert into SurveyQuestions (SurveyID, Title, QuestionType, ChoiceFormat,CreatedDate, CreatedBy) values(" + SurveyID + ",'" + Title + "','" + QuestType + "','" + ChoiceFormat + "',GetDate()," + Session["LoginID"].ToString() + ")";
        }
        else
        {
            CmdText = "Update SurveyQuestions set  Title='" + Title + "', QuestionType='" + QuestType + "', ChoiceFormat='" + ChoiceFormat + "',ModifiedDate=getDate(), ModifiedBy='" + Session["LoginId"].ToString() + "' where SurveyID=" + SurveyID + "";
        }
        try
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            dvMsg.Visible = true;
            if (BtnSave.Text == "Save")
            {
                LblMsg.Text = "Saved Successfully";
            }
            else
            {
                LblMsg.Text = "Updated Successfully";
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        loadQuestions();
    }

    public void loadQuestions()
    {
        String CmdText = string.Empty;
        CmdText = "select SQ.QuestionID,SQ.SurveyID,SQ.Title,SQ.QuestionType,SQ.ChoiceFormat, (select COUNT(*) from QuestionRows where QuestionID=SQ.QuestionID) as cnt from SurveyQuestions SQ";
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdQuestionList.DataSource = ds;
                    GrdQuestionList.DataBind();

                    for (int i = 0; i < GrdQuestionList.Rows.Count; i++)
                    {
                        if (((Label)GrdQuestionList.Rows[i].FindControl("lblQuestCount") as Label).Text == "0")
                        {
                            ((Button)GrdQuestionList.Rows[i].FindControl("BtnEditSurvey") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdQuestionList.Rows[i].FindControl("BtnEditSurvey") as Button).Enabled = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void BtnSave_Click(object sender, EventArgs e)
    {
        InsertNewQuestion();
    }


    protected void GrdQuestionList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {

                GridViewRow row = null;

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;


                string SurveyID = string.Empty;
                string QuestType = string.Empty;
                string QuestTitle = string.Empty;
                string ChoiceFormat = string.Empty;
                SurveyID = GrdQuestionList.Rows[selIndex].Cells[2].Text;
                QuestTitle = GrdQuestionList.Rows[selIndex].Cells[4].Text;
                QuestType = GrdQuestionList.Rows[selIndex].Cells[5].Text;
                ChoiceFormat = GrdQuestionList.Rows[selIndex].Cells[6].Text;

                txtQuestionTitle.Text = QuestTitle;
                DDlQuestionType.SelectedValue = QuestType;
                DDlChoiceFormat.SelectedValue = ChoiceFormat;


                BtnSave.Text = "Update";
            }
            else if (e.CommandName == "Add Rows/Columns")
            {
                GridViewRow row = null;

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                dvRow.Visible = true;
                if (DDlChoiceFormat.SelectedValue == "Text")
                {
                    dvColumn.Visible = false;
                }
                else
                {
                    dvColumn.Visible = true;
                }

                string SurveyID = string.Empty;
                string QuestType = string.Empty;
                string QuestTitle = string.Empty;
                string ChoiceFormat = string.Empty;
                SurveyID = GrdQuestionList.Rows[selIndex].Cells[2].Text;
                QuestTitle = GrdQuestionList.Rows[selIndex].Cells[4].Text;
                QuestType = GrdQuestionList.Rows[selIndex].Cells[5].Text;
                ChoiceFormat = GrdQuestionList.Rows[selIndex].Cells[6].Text;

                txtQuestionTitle.Text = QuestTitle;
                DDlQuestionType.SelectedValue = QuestType;
                DDlChoiceFormat.SelectedValue = ChoiceFormat;
                hdnChoiceFormat.Value = ChoiceFormat;
                hdnSurveyID.Value = SurveyID;
                HdnQuestionID.Value = GrdQuestionList.Rows[selIndex].Cells[3].Text;
                hdnUserID.Value = Session["LoginID"].ToString();

            }
            else if (e.CommandName == "Edit Rows/Columns")
            {
                GridViewRow row = null;

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                dvRow.Visible = true;
                if (DDlChoiceFormat.SelectedValue == "Text")
                {
                    dvColumn.Visible = false;
                }
                else
                {
                    dvColumn.Visible = true;
                }
                string SurveyID = string.Empty;
                string QuestType = string.Empty;
                string QuestTitle = string.Empty;
                string ChoiceFormat = string.Empty;
                SurveyID = GrdQuestionList.Rows[selIndex].Cells[2].Text;
                QuestTitle = GrdQuestionList.Rows[selIndex].Cells[4].Text;
                QuestType = GrdQuestionList.Rows[selIndex].Cells[5].Text;
                ChoiceFormat = GrdQuestionList.Rows[selIndex].Cells[6].Text;

                txtQuestionTitle.Text = QuestTitle;
                DDlQuestionType.SelectedValue = QuestType;
                DDlChoiceFormat.SelectedValue = ChoiceFormat;
                hdnChoiceFormat.Value = ChoiceFormat;
                hdnSurveyID.Value = SurveyID;
                HdnQuestionID.Value = GrdQuestionList.Rows[selIndex].Cells[3].Text;
                hdnUserID.Value = Session["LoginID"].ToString();

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "getQuestionRows();", true);


            }
            else if (e.CommandName == "DeleteQns")
            {
                GridViewRow row = null;

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string SurveyID = string.Empty;
                string QuestType = string.Empty;
                string QuestTitle = string.Empty;
                string ChoiceFormat = string.Empty;
                SurveyID = GrdQuestionList.Rows[selIndex].Cells[2].Text;
                QuestTitle = GrdQuestionList.Rows[selIndex].Cells[4].Text;
                QuestType = GrdQuestionList.Rows[selIndex].Cells[5].Text;
                ChoiceFormat = GrdQuestionList.Rows[selIndex].Cells[6].Text;
                HdnQuestionID.Value = GrdQuestionList.Rows[selIndex].Cells[3].Text;
                hdnSurveyID.Value = SurveyID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmDeleteQns();", true);

            }
        }
        catch (Exception ex)
        {
        }
    }

    public class SurveyQuestions
    {
        public string SurveyID { get; set; }
        public string SurveyTitle { get; set; }
        public string QuestionID { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionRowID { get; set; }
        public string QuestionRowTitle { get; set; }
        public string QuestionColumnID { get; set; }
        public string QuestionColumnTitle { get; set; }
        public string CretaedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public int Mode { get; set; }
    }
    [WebMethod]
    public static int PostQuestionRows(SurveyQuestions objSurvey)
    {
        int retVal = -1;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            if (objSurvey.Mode == 1)
            {
                cmdText = "Insert into QuestionRows (SurveyID, QuestionID, Title,CreatedDate,CreatedBy) values(" + objSurvey.SurveyID + "," + objSurvey.QuestionID + ",'" + objSurvey.QuestionRowTitle + "',GetDate()," + objSurvey.CreatedBy + ")";
            }
            else
            {
                cmdText = "Update QuestionRows set Title ='" + objSurvey.QuestionRowTitle + "', ModifiedDate=GetDate(), ModifiedBy=" + objSurvey.CreatedBy + " where QuestionID=" + objSurvey.QuestionID + " and QuestRowID=" + objSurvey.QuestionRowID + "";
            }
            SqlCommand cmd = new SqlCommand(cmdText, cn);

            cmd.ExecuteNonQuery();
            retVal = 1;
        }
        catch (Exception ex)
        {
        }
        return retVal;
    }

    [WebMethod]
    public static int PostQuestionColumns(SurveyQuestions objSurvey)
    {
        int retVal = -1;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string cmdText = string.Empty;
            if (objSurvey.Mode == 1)
            {
                cmdText = "Insert into SurveyQuestionColumns (SurveyID, QuestionID, Title,CreatedDate,CreatedBy) values(" + objSurvey.SurveyID + "," + objSurvey.QuestionID + ",'" + objSurvey.QuestionColumnTitle + "',GetDate()," + objSurvey.CreatedBy + ")";
            }
            else
            {
                cmdText = "Update SurveyQuestionColumns set Title ='" + objSurvey.QuestionRowTitle + "', ModifiedDate=GetDate(), ModifiedBy=" + objSurvey.CreatedBy + " where QuestionID=" + objSurvey.QuestionID + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;
        }
        catch (Exception ex)
        {
        }
        return retVal;
    }

    [WebMethod]
    public static int DeleteQuestions(SurveyQuestions objSurvey)
    {
        int retVal = -1;
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string cmdText = string.Empty;

            cmdText = "Delete from SurveyQuestions where QuestionID=" + objSurvey.QuestionID + "; Delete from QuestionRows where QuestionID=" + objSurvey.QuestionID + "; Delete from SurveyQuestionColumns where QuestionID=" + objSurvey.QuestionID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;


        }
        catch (Exception ex)
        {

        }
        return retVal;
    }

    [WebMethod]
    public static List<SurveyQuestions> GetQuestionRows(SurveyQuestions objSurvey)
    {
        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select * from QuestionRows where QuestionID=" + objSurvey.QuestionID + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.QuestionID = dr["QuestionID"].ToString();
                        sur.QuestionRowTitle = dr["title"].ToString();
                        sur.QuestionRowID = dr["QuestRowID"].ToString();
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;
    }

    [WebMethod]
    public static List<SurveyQuestions> GetQuestionColumns(SurveyQuestions objSurvey)
    {
        int retVal = -1;
        List<SurveyQuestions> objListSurvey = new List<SurveyQuestions>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select * from SurveyQuestionColumns where QuestionID=" + objSurvey.QuestionID + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyQuestions sur = new SurveyQuestions();
                        sur.QuestionID = dr["QuestionID"].ToString();
                        sur.QuestionColumnTitle = dr["title"].ToString();
                        sur.QuestionColumnID = dr["QuestColID"].ToString();
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;
    }
    [WebMethod]
    public static List<Survey> testSurvey()
    {

        string apiKey = "cqrfdehmyse424xpqy4njg3e";
        string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
        var sm = new SurveyMonkeyApi(apiKey, token);
        var settings = new GetSurveyListSettings
        {
            //Title = "2015 NSF Finals - Parent Survey",
            StartDate = Convert.ToDateTime("01/01/2015")
            
        };
        List<Survey> surveys = sm.GetSurveyList();

        return surveys;

    }
}