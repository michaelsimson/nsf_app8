﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" MaintainScrollPositionOnPostback="true"  AutoEventWireup="true"
    CodeFile="GBQuestion.aspx.cs" Inherits="GBQuestion"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div><script language="javascript" type="text/javascript">
          function PopupPicker(ctl, w, h) {
              var PopupWindow = null;
              settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
              PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
              PopupWindow.focus();
          }
		</script></div>
    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="~/VolunteerFunctions.aspx"
            runat="server"> Back to Volunteer Functions</asp:HyperLink>
        &nbsp&nbsp&nbsp&nbsp
            <asp:LinkButton ID="LBbacktofront" CssClass="btn_02" runat="server" Text="Previous"
                Visible="false">Back to Front Page</asp:LinkButton>

    </div>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        Geography Database	
             <br />
        <br />
    </div>
    <div>

        <table>
            <tr>
                <td style="width: 104px">

                    <asp:Label ID="LbId" runat="server" Text="ID"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtID" runat="server" Enabled="false"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Lbyear" runat="server" Text="Year"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DDListYear" Width="153px" runat="server"
                        AutoPostBack="True" 
                        OnSelectedIndexChanged="DDListYear_SelectedIndexChanged">
                    </asp:DropDownList>
                    <asp:Label ID="Lbyearval" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="LbSource" runat="server" Text="Source Date"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtSource" runat="server"></asp:TextBox>
                    <a href="javascript:PopupPicker('<%=TxtSource.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                        <asp:RequiredFieldValidator ControlToValidate="TxtSource" ID="RequiredFieldValidator2"
                        runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
     <asp:CompareValidator ID="CompareValidator3" runat="server" ControlToValidate="TxtSource"
                        Display="Dynamic" ErrorMessage="Enter Date"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="date"></asp:CompareValidator>
                </td>
                <td>
                    <asp:Label ID="Lbpreparer" runat="server" Text="Preparer"></asp:Label></td>
                <td>
                    <asp:TextBox ID="Txtpreparer" runat="server" Enabled="false"></asp:TextBox></td>
            </tr>
            <tr>
                <td>

                    <asp:Label ID="Label4" runat="server" Text="Source"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TextSouce" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="TextSouce" ID="RequiredFieldValidator9"
                        runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                </td>
                <td>
                    <asp:Label ID="Label5" runat="server" Text="Seq No #"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtSeq" runat="server"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="TxtSeq" ID="TxtSeq1" runat="server"
                        ErrorMessage="*"></asp:RequiredFieldValidator>
                    <asp:CompareValidator ID="CompareValidator1" runat="server" ControlToValidate="TxtSeq"
                        Display="Dynamic" ErrorMessage="Numeric Values"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                </td>
                <td>
                    <asp:Label ID="Label6" runat="server" Text="Page #"></asp:Label></td>
                <td>
                    <asp:TextBox ID="Txtpage" runat="server"></asp:TextBox>
                    <asp:CompareValidator ID="CompareValidator2" runat="server" ControlToValidate="Txtpage"
                        Display="Dynamic" ErrorMessage="Numeric Values"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="Double"></asp:CompareValidator>
                </td>

            </tr>
            <tr>
                <td>

                    <asp:Label ID="Lbcat" runat="server" Text="Category"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DDListcategory" Width="153px" runat="server" 
                        AutoPostBack="True" 
                        OnSelectedIndexChanged="DDListcategory_SelectedIndexChanged"></asp:DropDownList>
                    <asp:Label ID="Lbcatval" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
                <td>
                    <asp:Label ID="Label2" runat="server" Text="Level"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DDListLevel" Width="153px" runat="server">
                        <asp:ListItem Value="0">Select Level</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ControlToValidate="DDListLevel" ID="RequiredFieldValidator3"
                        runat="server" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Label ID="Label3" runat="server" Text="Sub-Level"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DDLIstSublevel" Width="153px" runat="server">
                        <asp:ListItem Value="0">Select Sub-Level</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ControlToValidate="DDLIstSublevel" ID="RequiredFieldValidator4"
                        runat="server" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>

            </tr>

            <tr>
                <td>

                    <asp:Label ID="Label1" runat="server" Text="Multiple choice"></asp:Label></td>
                <td> 
                    <asp:DropDownList ID="DDMultichoice" Width="120px" runat="server" AutoPostBack="true"
                        OnSelectedIndexChanged="DDMultichoice_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select Choice</asp:ListItem>
                        <asp:ListItem Value="1">Y</asp:ListItem>
                        <asp:ListItem Value="2">N</asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ControlToValidate="DDMultichoice" ID="RequiredFieldValidator5"
                        runat="server" InitialValue="0" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
                <td>
                    <asp:Label ID="Label7" runat="server" Text="Choices#"></asp:Label></td>
                <td>
                    <asp:DropDownList ID="DDOddChoice" Width="120px" runat="server" Enabled="false"
                        OnSelectedIndexChanged="DDOddChoice_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="0">Select Choice</asp:ListItem>
                        <asp:ListItem Value="1">1</asp:ListItem>
                        <asp:ListItem Value="2">2</asp:ListItem>
                        <asp:ListItem Value="3">3</asp:ListItem>
                        <asp:ListItem Value="4">4</asp:ListItem>
                        <asp:ListItem Value="5">5</asp:ListItem>
                    </asp:DropDownList> <asp:Label ID="Lblvalid" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
               
            </tr>

        </table>
        <table runat="server" ID="table1">

            <tr>
                <td>
                    <asp:Label ID="LbQuestion" runat="server" Text="Question"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtQuestion" runat="server" Width="600px" Height="100px" TextMode="MultiLine"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="TxtQuestion" ID="RequiredFieldValidator7"
                        runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label9" runat="server" Text="Choice A"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtchoiceA" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label10" runat="server" Text="Choice B"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtchoiceB" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label11" runat="server" Text="Choice C"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtchoiceC" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label12" runat="server" Text="Choice D"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtchoiceD" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label13" runat="server" Text="Choice E"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtchoiceE" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="Label14" runat="server" Text="Letter Answer"></asp:Label>

                </td>
                <td>
                    <asp:DropDownList ID="DDLetterAnswer" Width="153px" runat="server"  Enabled="false"
                        AutoPostBack="true"
                        OnSelectedIndexChanged="DDLetterAnswer_SelectedIndexChanged">
                        <%--                 <asp:ListItem Value="0">Select LetterAnswer</asp:ListItem>
                        <asp:ListItem Value="2">A</asp:ListItem>
                        <asp:ListItem Value="1">B</asp:ListItem>
                          <asp:ListItem Value="2">C</asp:ListItem>
                        <asp:ListItem Value="1">D</asp:ListItem>
                          <asp:ListItem Value="1">E</asp:ListItem>--%>
                    </asp:DropDownList><asp:Label ID="LbAsnw" runat="server" ForeColor="Red" Text="*" Visible="false"></asp:Label>
                </td>
            </tr>
            <tr>
                <td style="width: 104px">
                    <asp:Label ID="Label15" runat="server" Text="Answer"></asp:Label>

                </td>
                <td>
                    <asp:TextBox ID="TxtAnswer" runat="server" Width="600px"></asp:TextBox>
                    <asp:RequiredFieldValidator ControlToValidate="TxtAnswer" ID="RequiredFieldValidator6"
                        runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>

                </td>
            </tr>
        </table>
        <table>
            <tr>
                <td>

                    <asp:Label ID="Label16" runat="server" Text="Assigned To"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtAssigned" runat="server" Width="155px"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label17" runat="server" Text="Reviewer 1"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtRev1" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label18" runat="server" Text="Reviewer 2"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtReview2" runat="server" Width="150px"></asp:TextBox></td>

            </tr>

            <tr>
                <td>

                    <asp:Label ID="Label19" runat="server" Text="Is this a Duplicate ?"></asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DDListDuplicate" Width="160px" runat="server">
                     <asp:ListItem Value="1">N</asp:ListItem>
                        <asp:ListItem Value="2">Y</asp:ListItem>
                       
                    </asp:DropDownList>
                
                </td>
                <td>
                    <asp:Label ID="Label20" runat="server" Text="Audio Link"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtAudio" runat="server"></asp:TextBox></td>
                <td>
                    <asp:Label ID="Label21" runat="server" Text="Video Link"></asp:Label></td>
                <td>
                    <asp:TextBox ID="TxtVideo" runat="server" Width="150px"></asp:TextBox></td>

            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 104px">
                    <asp:Label ID="Label22" runat="server" Text="Link"></asp:Label>
                </td>
                <td>
                    <asp:TextBox ID="TxtLink" runat="server" Width="600px"></asp:TextBox></td>
            </tr>
        </table>
        <table>
            <tr>
                <td style="width: 287px"></td>
                <td>
                    <asp:Button ID="BtnAdd" runat="server" Text="Add" OnClick="BtnAdd_Click" />
                </td>
                <td>
                   
                </td>
                <td>
                    <asp:Button ID="Btnupdate" runat="server" Text="Update" 
                        OnClick="Btnupdate_Click"  Enabled="false"/></td>  <td>
                   
                </td>   <td>
                    <asp:Button ID="Button1" runat="server" Text="Cancel" 
                          OnClick="Button1_Click"/></td>
            </tr>
           
              
        </table>
        <table><tr><td style="width: 250px"></td><td>
           <asp:Label ID="LabelQuest" runat="server"  ForeColor="Red"  Text="Please Enter All selected choices"
                        Visible="false"></asp:Label></td></tr>
                        <tr><td style="width: 200px"></td><td>
           <asp:Label ID="LByearDateValid" runat="server"  ForeColor="Red"  Text=""
                        Visible="false"></asp:Label></td></tr>
        </table>
        <table>
            <tr>
                <td style="width: 397px"></td>
                <td>
                    <b>
                        <asp:Label ID="Label23" runat="server" Text="Table 1: Questions "></asp:Label></b>
                </td>
                          </tr></table>

      
        <asp:UpdatePanel ID="UpdatePanel1" runat="server" UpdateMode="Always"> 
        <ContentTemplate>
        <div><font><b><i>Please click on the clear button before proceeding with the next search criterion</i></b></font></div>
         <br />
                         <table>
                             <tr>
                                 <td>
                                     <asp:Label ID="Label24" runat="server" Text="Sort By"></asp:Label>
                                 </td>
                                 <td>
                                     <asp:DropDownList ID="DDSort" runat="server" AutoPostBack="True" 
                                         OnSelectedIndexChanged="DDSort_SelectedIndexChanged" Width="153px">
                                         <asp:ListItem Value="0">Select Sort By</asp:ListItem>
                                         <asp:ListItem Value="1">ID</asp:ListItem>
                                         <asp:ListItem Value="2">Source</asp:ListItem>
                                         <asp:ListItem Value="3">Category</asp:ListItem>
                                     </asp:DropDownList>
                                 </td>
                                 <td style="width: 40px"></td>
                                 <td>
                                     <asp:Label ID="LbSearch" runat="server" Text="Search:"></asp:Label>
                                 </td>
                                 <td>
                                     <asp:DropDownList ID="DDSearch" runat="server" AutoPostBack="true" 
                                         OnSelectedIndexChanged="DDSearch_SelectedIndexChanged" Width="153px">
                                         <asp:ListItem Value="0">Select Search By</asp:ListItem>
                                         <asp:ListItem Value="1">All</asp:ListItem>
                                         <asp:ListItem Value="3">Source</asp:ListItem>
                                         <asp:ListItem Value="4">Year</asp:ListItem>
                                         <asp:ListItem Value="5">SourceDate</asp:ListItem>
                                         <asp:ListItem Value="6">PersonEntering</asp:ListItem>
                                         <asp:ListItem Value="7">Question </asp:ListItem>
                                         <asp:ListItem Value="8">Answer</asp:ListItem>
                                         <asp:ListItem Value="9">Createdate </asp:ListItem>
                                         <asp:ListItem Value="10">Createdby</asp:ListItem>
                                     </asp:DropDownList>
                                 </td>
                                 <td>
                                     <asp:TextBox ID="TxtSearch" runat="server" Width="150px"></asp:TextBox>
                                 </td>
                                 <td>
                                     <asp:Button ID="BtnSearch" runat="server" OnClick="BtnSearch_Click" 
                                         Text="Search" />
                                 </td>
                                 <td>
                                     <asp:Button ID="BtnsearchClear" runat="server" OnClick="BtnsearchClear_Click" 
                                         Text="Clear" />
                                 </td>
                             </tr>


        </table>
    
        <br />
        <%--  <asp:GridView ID="GridQuesDisplay" runat="server" align="center" 
                           AllowPaging="True" PageSize="30" OnPageIndexChanging="GridQuesDisplay_PageIndexChanging"    
                
                >
           
            </asp:GridView>--%>
             <div id="Divdatesearch" align="left" runat="server" visible="false" >
            <asp:Panel id="PnlDate" runat="server">
                <asp:Label ID="Label8" runat="server" Text="From Date"></asp:Label>
                <asp:TextBox ID="TxtFrom" runat="server"></asp:TextBox>
                 <a href="javascript:PopupPicker('<%=TxtFrom.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                      
     <asp:CompareValidator ID="CompareValidator4" runat="server" ControlToValidate="TxtFrom"
                        Display="Dynamic" ErrorMessage="Enter Date"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="date"></asp:CompareValidator>
              &nbsp; &nbsp;  <asp:Label ID="Label25" runat="server" Text="To Date"></asp:Label>
                <asp:TextBox ID="TxtTo" runat="server"></asp:TextBox>
                 <a href="javascript:PopupPicker('<%=TxtTo.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a>
                   
     <asp:CompareValidator ID="CompareValidator5" runat="server" ControlToValidate="TxtTo"
                        Display="Dynamic" ErrorMessage="Enter Date"
                        Operator="DataTypeCheck" SetFocusOnError="True" Type="date"></asp:CompareValidator>
            </asp:Panel></div>


                <div id="Div1" align="center" runat="server" >
         <asp:Label ID="lbsearchresults" runat="server"  ForeColor="Red"  Text=""
                        Visible="false"></asp:Label></div>
            <br />
         
        <asp:Panel ID="PDonations" runat="server">

            <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left"               ID="GridQuesDisplay" AllowPaging="true"
                AutoGenerateColumns="false" runat="server"
                RowStyle-CssClass="SmallFont"
                PageSize="30" OnRowCommand="GridQuesDisplay_RowCommand" 
                OnPageIndexChanging="GridQuesDisplay_PageIndexChanging1">
                <Columns>
                    <asp:ButtonField Text="Select" />
                    <asp:BoundField DataField="Id" HeaderText="Id"></asp:BoundField>
                    <asp:BoundField DataField="Year" HeaderText="Year"></asp:BoundField>
                    <asp:BoundField DataField="SourceDate" HeaderText="Source Date" DataFormatString="{0:d}"  ></asp:BoundField>
                    <asp:BoundField DataField="Source" HeaderText="Source"></asp:BoundField>
                    <asp:BoundField DataField="SeqNo" HeaderText="Seq No"></asp:BoundField>
                    <asp:BoundField DataField="Category" HeaderText="Category"></asp:BoundField>
                    <asp:BoundField DataField="levelNo" HeaderText="level"></asp:BoundField>
                    <asp:BoundField DataField="SublevelNo" HeaderText="Sublevel"></asp:BoundField>
                    <asp:BoundField DataField="Question" HeaderText="Question"></asp:BoundField>
                    <asp:BoundField DataField="MultipleChoice" HeaderText="Multiple Choice"></asp:BoundField>
                    <asp:BoundField DataField="choices" HeaderText="choices"></asp:BoundField>
                    <asp:BoundField DataField="A" HeaderText="A"></asp:BoundField>
                    <asp:BoundField DataField="B" HeaderText="B"></asp:BoundField>
                    <asp:BoundField DataField="C" HeaderText="C"></asp:BoundField>
                    <asp:BoundField DataField="D" HeaderText="D"></asp:BoundField>
                    <asp:BoundField DataField="E" HeaderText="E"></asp:BoundField>
                    <asp:BoundField DataField="Answer" HeaderText="Answer"></asp:BoundField>
                    <asp:BoundField DataField="LetterAnswer" HeaderText="Letter Answer"></asp:BoundField>
                    <asp:BoundField DataField="Duplicate" HeaderText="Duplicate"></asp:BoundField>
                    <asp:BoundField DataField="Audio" HeaderText="Audio"></asp:BoundField>
                    <asp:BoundField DataField="Video" HeaderText="Video"></asp:BoundField>
                    <asp:BoundField DataField="Link" HeaderText="Link"></asp:BoundField>
                    <asp:BoundField DataField="PersonEntering" HeaderText="Preparer"></asp:BoundField>
                     <asp:BoundField DataField="Assign" HeaderText="Assign"></asp:BoundField>
                    <asp:BoundField DataField="page" HeaderText="Page#"></asp:BoundField>




                </Columns>
                <PagerSettings PageButtonCount="40" />
                <RowStyle CssClass="SmallFont" HorizontalAlign="Left" />
            </asp:GridView>

        </asp:Panel>
        
    </div>


      </ContentTemplate>
        </asp:UpdatePanel>


  <asp:Label ID="lblVAl"  ForeColor="White"  runat="server" Visible="false"></asp:Label>

    </div>
</asp:Content>

