
Partial Class Logout
    Inherits System.Web.UI.Page

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        Session("ChapterID") = Nothing
        Session("ContestsSelected") = Nothing
        Session("CustIndID") = Nothing
        Session("CustSpouseID") = Nothing
        Session("Donation") = Nothing
        Session("DonationFor") = Nothing
        Session("EvenID") = Nothing
        Session("EventYear") = Nothing
        Session("FatherID") = Nothing
        Session("LateFee") = Nothing
        Session("LoggedIn") = Nothing
        Session("LoginChapterID") = Nothing
        Session("LoginEmail") = Nothing
        Session("LoginEventID") = Nothing
        Session("LoginID") = Nothing
        Session("LoginRole") = Nothing
        Session("LoginTeamLead") = Nothing
        Session("LoginTeamMember") = Nothing
        Session("LoginZoneID") = Nothing
        Session("MealsAmount") = Nothing
        Session("MotherID") = Nothing
        Session("NavPath") = Nothing
        Session("RoleId") = Nothing
        Session("sSQL") = Nothing
        Session("sSQLORG") = Nothing
        Session("SplashPage") = Nothing

        Session.Abandon()
        Session.RemoveAll()
        Response.Redirect("Maintest.aspx")
    End Sub

End Class

