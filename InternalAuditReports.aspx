﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="InternalAuditReports.aspx.vb" Inherits="InternalAuditReports" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
      <asp:HyperLink ID="HyperLink1" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink> &nbsp;&nbsp;
       <asp:LinkButton ID="hlnkMainPage"  CssClass="btn_02" runat="server">Add/Update Bank Balances</asp:LinkButton>&nbsp;&nbsp;
       <asp:LinkButton ID="hlnkInvPositions"  CssClass="btn_02" runat="server">Add/Update  Inv Positions</asp:LinkButton>
       <br /><br />
<div id="divmain" runat="server"  align="center">
<table cellpadding="2" width="500px"  style ="background-color:#3EAA09"  cellspacing = "2" border ="0" align="center" >
<tr><td colspan="2" align="center" class="title03" > 
  Audit Reports
    </td> 
</tr>
<tr><td colspan="2" align="center" class="title03" > 
  <table cellpadding="2" width="500px"  style ="background-color:#FFFFFF"  cellspacing = "2" border ="0" align="center" >
  
<tr><td width="30%" align="right"> Bank </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlbank" Width="225px" runat="server" AutoPostBack="true">
    </asp:DropDownList>
</td>
</tr>
<tr><td  align="right"> Year </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlYear" Width="225px" runat="server" AutoPostBack="true">
     </asp:DropDownList>
</td>
</tr>
<tr><td colspan="2" align="center" >
 
    <asp:RadioButtonList ID="RbtnReport"  AutoPostBack="true" OnSelectedIndexChanged = "RbtnReport_SelectedIndexChanged"  Width="575px" runat="server" 
        RepeatDirection="Horizontal">
        <asp:ListItem Selected="True" Value="1">Internal Audit</asp:ListItem>
        <asp:ListItem Value="2">Cost Basis</asp:ListItem>
          <asp:ListItem Value="5">External Audit</asp:ListItem>
          <asp:ListItem Value="3">Supplement</asp:ListItem>
           <asp:ListItem Value="4">Cost Basis Balance Update</asp:ListItem>
    </asp:RadioButtonList>
</td>
</tr>
<tr><td colspan="2" align="center" > 
    <asp:Button ID="btnGenerate" OnClick ="btnGenerate_Click" runat="server" Text="Generate" />
    </td> 
</tr>
<tr><td colspan="2" align="center" > 
    <asp:Label ID="lblErr" runat="server" ForeColor = "Red" ></asp:Label>
    </td> 
</tr></table>
  </td> 
</tr>
</table>
    <asp:GridView ID="GridView1" AutoGenerateColumns="true"  runat="server">
    </asp:GridView>
<br /><br />
</div>
<div id="divaddupdateBankBalances" runat="server" visible = "false"  align="center">
<table cellpadding="2" width="400px"  style ="background-color:#3EAA09"  cellspacing = "2" border ="0" align="center" >
<tr><td colspan="2" align="center" class="title03" > 
  Beginning Balances Add/Edit
    </td> 
</tr>
<tr><td colspan="2" align="center" class="title03" > 
  <table cellpadding="2" width="400px"  style ="background-color:#FFFFFF"  cellspacing = "2" border ="0" align="center" >
  
<tr><td width="150px" align="right"> Bank </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlbalBank" Width="150px" runat="server">
    </asp:DropDownList>
</td>
</tr>
<tr><td  align="right"> Year </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlbalYear" AutoPostBack = "true" OnSelectedIndexChanged = "ddlbalYear_SelectedIndexChanged" Width="150px" runat="server">
           </asp:DropDownList>
</td>
</tr>
<tr><td align="right"> Balance Amount </td> 
<td align="left" > &nbsp;
    <asp:TextBox ID="txtBalance" runat="server"></asp:TextBox>
</td>
</tr>
<tr>
<td colspan ="2" align="center">
    <asp:Button ID="BtnAddUpdate" OnClick="BtnAddUpdate_Click" runat="server" Text="Add" />&nbsp;&nbsp;
    <asp:Button ID="BtnClear" OnClick ="BtnClear_Click" runat="server" Text="Clear" /> 
   <br />
    <asp:Label ID="lblBalErr" runat="server"   ForeColor ="Red" ></asp:Label>
</td>
</tr>
<tr>
<td colspan ="2" align="center">
    <asp:GridView ID="gvbankbalances" runat="server" AutoGenerateColumns="False" >
      <Columns>    
     <asp:BoundField DataField ="EndDate" HeaderText="EndDate" DataFormatString="{0:d}" />
      <asp:BoundField DataField ="BegDate" HeaderText="BegDate" DataFormatString="{0:d}" />
     <asp:BoundField DataField ="BankCode" HeaderText="Bank" />
      <asp:BoundField ItemStyle-HorizontalAlign="Right" DataField ="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
    </Columns> 
    </asp:GridView>
</td>
</tr>

</table> </td> </tr> </table> 
</div>
<div id="divInvPositions" runat="server" visible = "false"  align="center">
<table cellpadding="2" width="400px"  style ="background-color:#3EAA09"  cellspacing = "2" border ="0" align="center" >
<tr><td colspan="2" align="center" class="title03" > 
 Market Values  Add/Update
    </td> 
</tr>
<tr><td colspan="2" align="center" class="title03" > 
  <table cellpadding="2" width="400px"  style ="background-color:#FFFFFF"  cellspacing = "2" border ="0" align="center" >
  
<tr><td width="150px" align="right"> Bank </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlInvPositionsBank" AutoPostBack="true" OnSelectedIndexChanged ="ddlInvPositionsBank_SelectedIndexChanged" Width="150px" runat="server">
    </asp:DropDownList>
</td>
</tr>
<tr><td  align="right"> Date </td> 
<td align="left" > &nbsp;
    <asp:DropDownList ID="ddlInvPositionsYear" AutoPostBack = "true" OnSelectedIndexChanged = "ddlInvPositionsbalYear_SelectedIndexChanged" Width="150px" runat="server">
           </asp:DropDownList>
</td>
</tr>
<tr><td colspan="2" align="center">
<table id="trupdate" visible="false" cellpadding="2" cellspacing = "0" border = "0" runat ="server" >
<tr><td align="left"> Symbol </td> 
<td align="left" > &nbsp;
    <asp:TextBox ID="txtSymbol" runat="server"></asp:TextBox>
</td>
</tr>
<tr><td align="left"> Quantity </td> 
<td align="left" > &nbsp;
    <asp:TextBox ID="txtQuantity" runat="server"></asp:TextBox>
</td>
</tr>
<tr><td align="left"> Price </td> 
<td align="left" > &nbsp;
    <asp:TextBox ID="txtPrice" runat="server"></asp:TextBox>
</td>
</tr>
<tr><td align="left"> Amount </td> 
<td align="left" > &nbsp;
    <asp:TextBox ID="txtAmount" runat="server"></asp:TextBox>
</td>
</tr>
<tr><td align="center" colspan = "2"> 
    <asp:Button ID="btnInvPositionsUpdate" OnClick="BtnInvPositionsUpdate_Click" runat="server" Text="Update" /> &nbsp;
     &nbsp;&nbsp;
    <asp:Button ID="btnInvPositions" OnClick ="BtnInvPositions_Click" runat="server" Text="Clear" /> 
    </td>
</tr>
<tr>
<td colspan ="2" align="center">
    <asp:HiddenField ID="hdnMarketValueID" runat="server" />
</td>
</tr>

</table>
</td>
</tr>
<tr><td align="center" colspan ="2">
<asp:Label ID="lblInvPositions" runat="server"   ForeColor ="Red" ></asp:Label>
</td>
</tr>
<tr>
<td align="center" colspan="2" style="height: 30px">
  <asp:FileUpload ID="FileUpload" Visible="true"  Enabled ="true" Width="200px" runat="server" />
</td></tr>

<tr><td align="center" colspan="2">
    <asp:Button ID="btnUpload" Visible="true" runat="server" Text="Upload" Height="26px" Width="60px" />&nbsp;
    <br />
    <asp:Label ID="lblWarng" runat="server" Text="*** Note: (Avoid Data with(,) in .csv files.)" ForeColor ="blue"></asp:Label></td>
</tr>
<tr><td align="center" colspan="2"><asp:Label ID="lblmktSuccess" runat="server" ForeColor="Red" Text=""></asp:Label></td></tr> 

<tr>
<td colspan ="2" align="center">
    <asp:GridView ID="gvInvPositions" DataKeyNames="MarketValueID" runat="server" AutoGenerateColumns="False" OnRowCommand="gvInvPositions_RowCommand" >
      <Columns>    
     <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
     <asp:BoundField DataField ="Bankcode" HeaderText="Bank Code" />
     <asp:BoundField DataField ="Date" HeaderText="Date" DataFormatString="{0:d}" />
     <asp:BoundField DataField ="Symbol" HeaderText="Symbol"/>
     <asp:BoundField DataField ="Quantity" HeaderText="Quantity" />
     <asp:BoundField DataField ="Price" HeaderText="Price" DataFormatString="{0:c}" />
     <asp:BoundField DataField ="MktValue" HeaderText="Amount" DataFormatString="{0:c}" />
      </Columns> 
    </asp:GridView>
</td>
</tr>

</table> </td> </tr> </table> 
</div>
</asp:Content>

