﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SupportTrackingHelpGuide.aspx.cs" Inherits="SupportTrackingHelpGuide" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td></td>
                    <td class="" valign="top" nowrap align="center">
                        <h1>Help Guide</h1>
                    </td>
                </tr>
            </table>
            <br />
            <table cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td style="font-weight: bold;">Contests :
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="StepByStepInstructions_RegionalContests.pdf">Step-by-step Instructions to register 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="FAQ Customer Service_Final.pdf">FAQ during Contest Registration 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/DonationRefund_Instructions.pdf">Donation Refund Instructions
                        </a>
                        <br />
                        <br />
                    </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Coaching :
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/InstructionsForCoachingTools_Parents.pdf">Instructions for NSF Web Tools 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc">Mathcounts - Level Determination 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc">Pre-Mathcounts Level Determination 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/FAQ - MathCounts.docx">Mathcounts FAQ 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/FAQ - Pre-MathCounts.docx">Pre-Mathcounts FAQ 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/NSF_SATCoaching_FAQ_Parents.pdf">SAT Coaching FAQ 
                        </a>
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Coaching/UVOrientationGuide.pdf">UV Orientation Guide 
                        </a>
                        <br />
                        <br />
                    </td>
                </tr>

                <tr>
                    <td style="font-weight: bold;">Games :
                    </td>
                </tr>
                <tr>
                    <td>
                        <a href="BulletinBoard/Parent/Game/GameGuide.pdf">Game Guide
                        </a>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
