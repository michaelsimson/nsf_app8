<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ViewSATTestScores.aspx.vb" Inherits="ViewSATTestScores" Title="View Scores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div>
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>
        <script type="text/javascript">

            function ConfirmReset() {
                if (confirm("Are you sure you want to reset?")) {
                    document.getElementById('<%= btnConfirmDelete.ClientID%>').click();
            }
        }

            function SetButtonScroll() {
                var height = document.getElementById("<%=DGCoachPapers.ClientID%>").scrollHeight;
                var width = document.body.scrollWidth;

                window.scrollTo(0, 550);
            }


            function SetAnswerScroll() {
                var height = document.getElementById("<%=GVAnswers.ClientID%>").scrollHeight;
                var width = document.body.scrollWidth;

                // window.scrollTo(0, height);
                document.getElementById('dvScores').scrollIntoView(true);
            }

            $(function (e) {

                $("#ancHelpLink").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {

                            var dvHtml = "";

                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">1. Make selections below and press <span style="font-weight:bold; color:blue;">View Scores</span> to View Scores.</span></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">2. Press <span style="font-weight:bold; color:blue;">Class performance</span> to check questions missed by students.</span></div> ';

                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">3. Press <span style="font-weight:bold; color:blue;">Homework Submissions</span> to view homework by children.</span></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">4. Press <span style="font-weight:bold; color:blue;">Calculate Scores</span> to calculate scores for all students.</span></div> ';
                            return dvHtml;

                        },

                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">Instructions to follow</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })

                $("#ancHelpparent").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {

                            var dvHtml = "";
                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">1. Make selections below and press <span style="font-weight:bold; color:blue;">Submit.</span></span></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';
                            dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">2. Press <span style="font-weight:bold; color:blue;">Select</span> on the appropriate row in table to view Scores.</span></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            return dvHtml;

                        },

                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">Instructions to follow</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })
            });

        </script>
        <style type="text/css">
            .qTipWidth {
                max-width: 450px !important;
            }
        </style>
    </div>
    <div align="left">


          <asp:Button ID="btnConfirmDelete" Style="display: none;" runat="server" OnClick="btnConfirmDelete_Click" />

        <asp:LinkButton ID="lnkVolFunction" runat="server" PostBackUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions</asp:LinkButton><br />
        <asp:LinkButton ID="lnkParentPage" runat="server" PostBackUrl="~/UserFunctions.aspx">Back to Parent Functions</asp:LinkButton><br />
        <asp:LinkButton ID="lnkstudentPage" runat="server" PostBackUrl="~/StudentFunctions.aspx">Back to Student Functions</asp:LinkButton><br />
    </div>
    <div>
        <table align="center" border="0" width="90%">
            <tr>
                <td align="center" colspan="14">
                    <asp:Label runat="server" Font-Bold="true" Font-Size="Small" Text="View Scores" ForeColor="Brown"></asp:Label></td>
            </tr>
            <tr id="TrInsVol" runat="server" visible="false">
                <td colspan="14">
                    <table width="100%" style="color: Gray;">
                        <tr align="center">
                            <td colspan="2" align="left" style="font-size: large; color: #6B33EE"><a id="ancHelpLink" style="color: blue; cursor: pointer;">Help?</a>

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="TrInsParStd" runat="server" visible="false">
                <td colspan="14">
                    <table width="100%" style="color: Gray;">
                        <tr align="center">
                            <td colspan="2" align="left" style="font-size: large; color: #6B33EE"><a id="ancHelpparent" style="color: blue; cursor: pointer;">Help?</a>

                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
            <tr id="TrCoach" runat="server">


                <td align="center" colspan="14">
                    <%--<div style="float: left;">
                        <b>Semester:</b>
                       
                    </div>--%>
                    <div style="float: left; margin-left: 10px;">
                        <b>Coach Name:</b>
                        <asp:DropDownList ID="ddlVolName" AutoPostBack="true" DataTextField="Name" DataValueField="MemberID" runat="server"></asp:DropDownList><asp:Label CssClass="announcement_text" ID="lblCoach" runat="server" ForeColor="Green" Text="*Please Select Coach and proceed"></asp:Label>
                    </div>

                </td>
            </tr>

            <tr>
                <td colspan="14">
                    <br />
                </td>
            </tr>
            <tr>
                <td align="center">
                    <table width="80%">
                        <tr>
                            <td><b>Event</b></td>
                            <td>
                                <asp:DropDownList ID="ddlEvent" runat="server">
                                    <asp:ListItem Value="13">Coaching</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td><b>EventYear</b></td>
                            <td>
                                <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged"></asp:DropDownList>
                            </td>

                            <td id="tdSemCoach" runat="server"><b>Semester</b></td>
                            <td id="tdSemCoachDr" runat="server">
                                <asp:DropDownList ID="ddlSemester" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" runat="server"></asp:DropDownList>
                            </td>

                            <td><b>PaperType</b></td>
                            <td>
                                <asp:DropDownList ID="ddlPaperType" runat="server">
                                    <asp:ListItem Value="-1">Select</asp:ListItem>
                                    <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                                    <asp:ListItem Value="PT">Pretest</asp:ListItem>
                                    <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                                    <asp:ListItem Value="FT">Final Test</asp:ListItem>
                                </asp:DropDownList></td>

                            <td><b><span id="spnSemTitle" runat="server">Semester</span></b></td>
                            <td>
                                <asp:DropDownList ID="ddlPSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPSemester_SelectedIndexChanged">
                                </asp:DropDownList></td>

                            <td id="TdChild" runat="server" visible="false"><b>Student Name</b></td>
                            <td id="Td_ddlChild" runat="server" visible="false">
                                <asp:DropDownList ID="ddlChild_Par" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_Par_SelectedIndexChanged" DataTextField="ChildName" DataValueField="ChildNumber" runat="server">
                                    <asp:ListItem Value="-1">Select Child Name</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>

                        <tr>
                            <td><b>ProductGroup</b></td>
                            <td>
                                <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID"
                                    OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" Enabled="false" runat="server">
                                    <asp:ListItem Value="-1">Select Product Group</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><b>Product</b></td>
                            <td>
                                <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" Enabled="false" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                                    <asp:ListItem Value="-1">Select Product</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><b>Level</b></td>
                            <td>
                                <asp:DropDownList ID="ddlLevel" DataTextField="Level" DataValueField="Level" Enabled="false" runat="server">
                                    <asp:ListItem Value="-1">Select Level</asp:ListItem>
                                </asp:DropDownList></td>
                            <td><b>Session</b></td>
                            <td>
                                <asp:DropDownList ID="ddlSession" runat="server" DataTextField="SessionNo" DataValueField="SessionNo" OnSelectedIndexChanged="ddlSession_SelectedIndexChanged" AutoPostBack="true">
                                    <asp:ListItem Value="-1">Select Session</asp:ListItem>
                                </asp:DropDownList></td>
                            <td id="tdWeekNoTitle" runat="server"><b>Week#</b></td>
                            <td id="tdWeekNo" runat="server">
                                <asp:DropDownList ID="ddlWeekNo" runat="server" DataTextField="WeekId" DataValueField="Coachpaperid" OnSelectedIndexChanged="ddlWeekNo_SelectedIndexChanged">
                                    <asp:ListItem Value="-1">Select Week</asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="center">
                                <asp:Button ID="Btn_Submit" runat="server" Text="Submit" OnClick="Btn_Submit_Click" />
                                <%-- <asp:Label ID="lblNotemsg" runat="server" Text ="Note **:Submit to display the Coach Papers" ForeColor="Green"></asp:Label>
                                --%>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                                <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label></td>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <table align="center" style="width: 1050px;">

            <tr>
                <td align="center" runat="server" visible="false">
                    <asp:Label ForeColor="Green" ID="lblNotemsg" runat="server" Visible="false" Text=""></asp:Label><br />
                    <%--  Note **:Submit to display the Coach Papers--%>
                </td>
            </tr>
            <tr>
                <td align="center" colspan="20">
                    <div>
                        <asp:Label ForeColor="Red" ID="lblScoreErr" runat="server" Visible="true" Text=""></asp:Label><br />
                    </div>
                </td>
            </tr>
            <tr id="TrAnsExp" runat="server" visible="false">
                <td align="center" colspan="20">
                    <asp:Label ID="lblRelError" runat="server" Font-Bold="true" ForeColor="Red" Text="Child has not submitted before the deadline.  Do you want to give an exception and give one week to submit the answers online"></asp:Label><br />
                    <asp:Button ID="BtnYes" runat="server" Text="Yes" />&nbsp;
                   <asp:Button ID="BtnNo" runat="server" Text="No" />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="14">
                    <asp:Label ID="lblNote" runat="server" Text="" ForeColor="Green" Font-Bold="true"></asp:Label></td>
                <%--  Note**: Select the coach paper before you can press any button on the line below.--%>
            </tr>
            <tr id="TrStudReport" runat="server">
                <td align="center" colspan="14">
                    <asp:HiddenField ID="hdnCoachPaperID" runat="server" />
                    <%--<asp:Button ID="btnSubmit" runat="server" Text="Submit" />--%>&nbsp;
                    <asp:Button ID="BtnMissedAnswers" runat="server" Text="Class Performance" Enabled="false" OnClick="BtnMissedAnswers_Click" />
                    &nbsp;
                    <asp:Button ID="BtnHWSubmission" runat="server" Text="Homework Submissions" Enabled="false" />
                    &nbsp;
                    <asp:Button ID="BtnCalcualteScores" runat="server" Text="Calculate Scores" Enabled="false" />&nbsp;

                </td>
                <td><b>Student Name</b><br />

                </td>
                <td>
                    <asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="ChildName" DataValueField="ChildNumber" runat="server">
                        <asp:ListItem Value="-1">Select Child Name</asp:ListItem>
                    </asp:DropDownList>
                    <br />


                </td>
                <td>
                    <asp:Button ID="BtnViewSCores" runat="server" Text="View Score" Enabled="false" />
                    <asp:Button ID="BtnResetGrade" runat="server" Visible="false" Text="Reset Grade" OnClick="BtnResetGrade_Click" />
                </td>
                <td>
                    <asp:Button ID="BtnGradeCard" runat="server" Text="GradeCard" Enabled="false" Visible="false" />
                    <asp:Button ID="btnGradeCard_Parent" runat="server" Text="GradeCard" Visible="true" Enabled="false" />
                    &nbsp;
                </td>
            </tr>
            <tr>
                <td></td>
            </tr>
        </table>
    </div>
    <div>
        <table id="Table1" runat="server" align="center">
            <tr>
                <td align="center">
                    <asp:Label CssClass="announcement_text" ForeColor="Brown" ID="lblCoachPapers" runat="server" Text="Coach Papers" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td align="center" runat="server"></td>
            </tr>
            <tr>
                <td id="Td1" runat="server" align="center">
                    <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                        BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                        OnItemCommand="DGCoachPapers_ItemCommand" OnItemDataBound="DGCoachPapers_ItemDataBound">
                        <FooterStyle BackColor="#CCCCCC" />
                        <ItemStyle BackColor="White" />
                        <%--<SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />  --%>
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                        <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />

                        <Columns>

                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventCode" HeaderText="EventCode"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroupCode"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="ProductCode"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>
                            <%-- <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QReleaseDate" HeaderText="QReleaseDate" Visible="false"></asp:BoundColumn>
                                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QDeadlineDate" HeaderText="QDeadlineDate" Visible="false"></asp:BoundColumn>
                            --%>
                        </Columns>
                    </asp:DataGrid></td>
            </tr>


        </table>
    </div>
    <%--<div align="center">
    
                <asp:datagrid id="dgViewScores" AutoGenerateColumns="false" runat="server" Width="784px" Height="304px" BorderColor="#999999" BorderStyle="None" BorderWidth="1px" BackColor="White" CellPadding="3" GridLines="Vertical" OnPageIndexChanged="dgViewScores_PageIndexChanged">				
                <FooterStyle ForeColor="Black" BackColor="#CCCCCC"></FooterStyle>
				<SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
				<AlternatingItemStyle BackColor="Gainsboro"></AlternatingItemStyle>
				<ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
				<HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#000084"></HeaderStyle>
				<PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
				<COLUMNS>
                <asp:BOUNDCOLUMN DataField="ChildTestSumID" HeaderText="ChildTestSumID" ItemStyle-HorizontalAlign="Left" Visible="false" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="ChildNumber" HeaderText="ChildNumber" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Grade" HeaderText="Grade" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TestID" HeaderText="TestID" > </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="Year" HeaderText="Year"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="S1Score" HeaderText="S1Score"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="S2Score" HeaderText="S2Score"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="S3Score" HeaderText="S3Score"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="S4Score" HeaderText="S4Score"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="S5Score" HeaderText="S5Score"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="CRRawScore" HeaderText="CRRawScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="WSRawScore" HeaderText="WSRawScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MathRawScore" HeaderText="MathRawScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="EngRawScore" HeaderText="EngRawScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="CRScaledScore" HeaderText="CRScaledScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="WSScaledScore" HeaderText="WSScaledScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="MathScaledScore" HeaderText="MathScaledScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="EngScaledScore" HeaderText="EngScaledScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TotalRawScore" HeaderText="TotalRawScore"> </asp:BOUNDCOLUMN>
                <asp:BOUNDCOLUMN DataField="TotalScaledScore" HeaderText="TotalScaledScore"> </asp:BOUNDCOLUMN>
                </COLUMNS>                
            <ItemStyle HorizontalAlign="Right" BackColor="#EEEEEE" Font-Names="Verdana" Font-Size="X-Small" ForeColor="Black" />
            <SelectedItemStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
            <AlternatingItemStyle BackColor="Gainsboro" />
		</asp:datagrid>
</div>--%>
    <div>
        <table id="Table2" runat="server" align="center">
            <tr>
                <td align="center">
                    <asp:Label CssClass="announcement_text" ForeColor="Brown" ID="lblMissedAns" runat="server" Text="Class Performance" Visible="false"></asp:Label></td>
            </tr>
            <tr id="TrSort" runat="server" visible="false">
                <td align="center">
                    <asp:Button ID="btnSort_Ques" runat="server" Text="Sort By Question# " />&nbsp;
                    <asp:Button ID="btnSort_Missed" runat="server" Text="Sort By # of Wrong Answers" /></td>
            </tr>
            <tr>
                <td id="Td3" runat="server" align="center">
                    <center>
                        <asp:Label ID="lblCPPrdlevelWeek" runat="server" Font-Bold="true"></asp:Label></center>
                    <div style="clear: both; margin-bottom: 5px;"></div>

                    <asp:DataGrid ID="DGMissedAns" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                        BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black">
                        <%-- OnItemCommand ="DGMissedAns_ItemCommand" OnItemDataBound="DGMissedAns_ItemDataBound"--%>
                        <FooterStyle BackColor="#CCCCCC" />
                        <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                        <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                        <ItemStyle BackColor="White" />
                        <Columns>

                            <%--  <asp:TemplateColumn>
                                   <ItemTemplate><asp:LinkButton id="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
	                               </ItemTemplate>
		                        </asp:TemplateColumn>  --%>

                            <%--<asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                            --%>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" Visible="false" HeaderText="CoachPaperID"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionNumber" HeaderText="Section#"></asp:BoundColumn>
                            <%--<asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SessionNo" HeaderText="Session#"></asp:BoundColumn>
                            --%>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumber" HeaderText="Question#"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CorrectCount" HeaderText="#Correct" ItemStyle-ForeColor="Green" ItemStyle-Font-Bold="true"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="InCorrectcount" HeaderText="#Wrong" ItemStyle-ForeColor="Red" ItemStyle-Font-Bold="true"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TotalCount" HeaderText="Total"></asp:BoundColumn>

                        </Columns>

                    </asp:DataGrid></td>
            </tr>
        </table>

    </div>
    <div>
        <table runat="server" align="center">
            <tr>
                <td align="center">
                    <asp:Label CssClass="announcement_text" ForeColor="Brown" ID="lblHW_Sub" runat="server" Text="HomeWork Submissions" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td runat="server" align="center">
                    <center>
                        <asp:Label ID="lblPrdlevelWeek" runat="server" Font-Bold="true"></asp:Label></center>
                    <div style="clear: both; margin-bottom: 5px;"></div>
                    <asp:DataGrid ID="Dg_HwSubmit" runat="server" DataKeyField="CoachPaperID" OnItemDataBound="Dg_HwSubmit_ItemDataBound" AutoGenerateColumns="False" CellPadding="4"
                        BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black" OnItemCommand="Dg_HwSubmit_ItemCommand">
                        <%-- OnItemCommand ="DGMissedAns_ItemCommand" OnItemDataBound="DGMissedAns_ItemDataBound"--%>
                        <FooterStyle BackColor="#CCCCCC" />
                        <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                        <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                        <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                        <ItemStyle BackColor="White" />
                        <Columns>

                            <%--  <asp:TemplateColumn>
                                   <ItemTemplate><asp:LinkButton id="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
	                               </ItemTemplate>
		                        </asp:TemplateColumn>  --%>

                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product Code" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level" Visible="false"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="Week#" Visible="false"></asp:BoundColumn>

                            <asp:TemplateColumn HeaderText="Student Name">
                                <HeaderStyle Width="125px" Font-Bold="True" ForeColor="black"></HeaderStyle>
                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:Label ID="lblCOachPaperId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CoachPaperID")%>'></asp:Label>
                                        <asp:Label ID="lblStudentId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChildNumber")%>'></asp:Label>
                                    </div>
                                    <asp:LinkButton ID="lnkStudentName" runat="server" CommandName="StudentName" Text='<%#DataBinder.Eval(Container, "DataItem.StudentName")%>'></asp:LinkButton>
                                </ItemTemplate>


                            </asp:TemplateColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Completed" HeaderStyle-HorizontalAlign="Center" HeaderText="Answered (Y) / <br> Override (O)" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Score" HeaderText="Student Score" HeaderStyle-HorizontalAlign="Center" ItemStyle-HorizontalAlign="Center"></asp:BoundColumn>
                            <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CScore" HeaderText="CScore" Visible="false"></asp:BoundColumn>

                        </Columns>

                    </asp:DataGrid></td>
            </tr>
        </table>

    </div>


    <div align="center" style="margin-left: auto; margin-right: auto;" id="dvScores">
        <div align="center">
            <asp:Label ID="lblErrorMsg" runat="server" Visible="false" Text="No answer key available for the selected coach paper." ForeColor="Red"></asp:Label>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <table>
            <tr>
                <td id="Td2" colspan="2" align="center" runat="server">
                    <asp:Label ID="lblScores" CssClass="announcement_text" runat="server" ForeColor="Green" Text="Student Test Scores" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td align="right">
                    <asp:Label ID="lblCScoreErr" runat="server" ForeColor="Red" Text=""></asp:Label>&nbsp;&nbsp;
                </td>
                <td></td>
                <td></td>
            </tr>
            <tr>
                <td>
                    <asp:GridView ID="GVTotalScore" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4">
                        <Columns>
                            <asp:BoundField DataField="CoachPaperID" HeaderText="CoachPaperID" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="S1Score" HeaderText="Section 1" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="S2Score" HeaderText="Section 2" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="S3Score" HeaderText="Section 3" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="S4Score" HeaderText="Section 4" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="S5Score" HeaderText="Section 5" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="WSRawScore" HeaderText="Writing Skills" HeaderStyle-ForeColor="white" Visible="false" />
                            <asp:BoundField DataField="CRRawScore" HeaderText="Critical Reading" HeaderStyle-ForeColor="white" Visible="false" />
                            <asp:BoundField DataField="MathRawScore" HeaderText="Math" HeaderStyle-ForeColor="white" Visible="false" />

                            <asp:BoundField DataField="TotalRawScore" HeaderText="Total Score" ItemStyle-Font-Bold="true" HeaderStyle-ForeColor="white" />
                        </Columns>
                        <HeaderStyle BackColor="#99cc00" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <center>
                        <asp:Label ID="lblAnsPrdLevelWeek" runat="server" Font-Bold="true"></asp:Label></center>
                    <div style="clear: both; margin-bottom: 1px;"></div>
                    <div style="float: right; margin-right: 3px;">
                        <asp:Button runat="server" ID="btnGVAnsUpdate" Text="Update" OnClick="GVAnswersUpdate_Click" Visible="false" />
                    </div>
                    <div style="clear: both; margin-bottom: 1px;"></div>
                    <asp:GridView ID="GVAnswers" runat="server" AutoGenerateColumns="False" BackColor="White" OnRowDataBound="GVAnswers_RowDataBound"
                        BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4">
                        <Columns>
                            <asp:BoundField DataField="CoachPaperID" HeaderText="PaperID" HeaderStyle-ForeColor="white" Visible="false" />
                            <asp:BoundField DataField="SectionNumber" HeaderText="Sec#" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="QuestionNumber" HeaderText="Question#" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" />

                            <%--<asp:BoundField DataField="Score" HeaderText="Score" HeaderStyle-ForeColor="white" />--%>



                            <asp:BoundField DataField="CorrectAnswer" HeaderText="Answer Key" ItemStyle-Font-Bold="true" ItemStyle-ForeColor="#999999" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="Answer" HeaderText="Studentís Response" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="CscorePar" HeaderText="Cscore" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" Visible="false" />

                            <asp:BoundField DataField="Manual" HeaderText="Manual Update" ItemStyle-HorizontalAlign="Center" HeaderStyle-ForeColor="white" Visible="false" />

                            <asp:TemplateField HeaderText="Score" HeaderStyle-ForeColor="white" ControlStyle-Width="50px">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtManual" runat="server" Style="text-align: center" Text="<%# Bind('Score') %>"></asp:TextBox>
                                    <div style="display: none;">
                                        <asp:Label ID="lblScore" runat="server" Text="<%# Bind('Score') %>"></asp:Label>
                                        <asp:Label ID="lblCoachPaperId" runat="server" Text="<%# Bind('CoachPaperID') %>"></asp:Label>
                                        <asp:Label ID="LblManualUpdate" runat="server" Text="<%# Bind('Manual') %>"></asp:Label>
                                        <asp:Label ID="lblCScore" runat="server" Text="<%# Bind('cScore') %>"></asp:Label>
                                    </div>
                                </ItemTemplate>
                            </asp:TemplateField>

                            <asp:TemplateField HeaderText="CScore" HeaderStyle-ForeColor="white" ControlStyle-Width="50px" Visible="false">
                                <ItemTemplate>
                                    <asp:TextBox ID="txtCScore" runat="server" Text="<%# Bind('CScore') %>"></asp:TextBox>
                                </ItemTemplate>
                            </asp:TemplateField>



                        </Columns>
                        <HeaderStyle BackColor="#99cc00" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </td>
                <td></td>
                <td>
                    <asp:GridView ID="GVScaledScore" runat="server" AutoGenerateColumns="False" BackColor="White"
                        BorderColor="#3366CC" BorderStyle="Solid" BorderWidth="1px" CellPadding="4">
                        <Columns>
                            <asp:BoundField DataField="CoachPaperID" HeaderText="CoachPaperID" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="WSScaledScore" HeaderText="WSScaledScore" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="CRScaledScore" HeaderText="CRScaledScore" HeaderStyle-ForeColor="white" />
                            <asp:BoundField DataField="MathScaledScore" HeaderText="MathScaledScore" HeaderStyle-ForeColor="white" />

                        </Columns>
                        <RowStyle BackColor="White" ForeColor="#003399" />
                        <HeaderStyle BackColor="#99cc00" Font-Bold="True" ForeColor="#CCCCFF" />
                    </asp:GridView>
                </td>

            </tr>
        </table>
    </div>

    <asp:HiddenField ID="hdnTotalSMarks" runat="server" Value="0" />
    <asp:HiddenField ID="hdnChildNumbers" runat="server" Value="0" />

</asp:Content>


























