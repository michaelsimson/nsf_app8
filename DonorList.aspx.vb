﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class DonorList
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Session("RoleID") Is Nothing Then
            Response.Redirect("volunteerfunctions.aspx")
        End If
        If (Not Page.IsPostBack) Then
            If Request.QueryString("id") = "3" And Not Session("selZoneID") = Nothing Then
                ddlCoverage.SelectedIndex = ddlCoverage.Items.IndexOf(ddlCoverage.Items.FindByValue("3"))
                lblTxt.Text = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Name from zone where ZoneId=" & Session("selZoneID") & "")
            ElseIf Request.QueryString("id") = "4" And Not Session("selClusterID") = Nothing Then
                ddlCoverage.SelectedIndex = ddlCoverage.Items.IndexOf(ddlCoverage.Items.FindByValue("4"))
                lblTxt.Text = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Name from Cluster where ClusterId=" & Session("selClusterID") & "")
            ElseIf Request.QueryString("id") = "5" And Not Session("selChapterID") = Nothing Then
                ddlCoverage.SelectedIndex = ddlCoverage.Items.IndexOf(ddlCoverage.Items.FindByValue("5"))
                lblTxt.Text = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select ChapterCode from Chapter where ChapterId=" & Session("selChapterID") & "")
            ElseIf Session("RoleID") = "2" Or Session("RoleID") = "1" Then
                ddlCoverage.SelectedIndex = ddlCoverage.Items.IndexOf(ddlCoverage.Items.FindByValue("2"))
            Else
                Response.Redirect("volunteerfunctions.aspx")
            End If
        End If

    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If IsDate(txtStartDate.Text) = False Or IsDate(txtEnddate.Text) = False Then
            lblerr.Text = "Please enter Start Date and End Date in format MM/DD/YYYY"
            Exit Sub
        ElseIf Convert.ToDateTime(txtStartDate.Text) > Convert.ToDateTime(txtEnddate.Text) Then
            lblerr.Text = "Start Date must be less than End Date"
            Exit Sub
        End If
        lblerr.Text = ""
        LoadGrid(dgDonationList, 2)
    End Sub
    Private Sub LoadGrid(ByVal Dg As DataGrid, ByVal Status As Integer)
        Dim dsDonation As New DataSet
        Dim strSql As String = ""
        Dim strChapterID As String = ""
        If ddlCoverage.SelectedValue = "5" Then
            strChapterID = Session("selChapterID")
        ElseIf ddlCoverage.SelectedValue = "4" Then
            strSql = "Select chapterid, chaptercode, state from chapter where clusterid in (" & Session("selClusterID") & ") order by state, chaptercode"
            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
        ElseIf ddlCoverage.SelectedValue = "3" Then
            strSql = "Select chapterid, chaptercode, state from chapter where Zoneid in (" & Session("selZoneID") & ") order by state, chaptercode"
            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
        End If

        '**************************** Starts Here ***************************
        If ddlDonorDetail.SelectedValue = "Family" And ddlType.SelectedValue = "IND" Then
            strSql = "SELECT I.Automemberid,I1.AutoMemberID as SpouseMemberID,I.firstname ,I.lastname,I.PubName,I1.FirstName as SFirstName,I1.LastName as SLastName,I.ADDRESS1,I.ADDRESS2 ,I.ZIP,I.HPHONE,I.CPhone,I.EMAIL,I.city,I.state,I.ChapterID into #TmpIndSpouse FROM IndSpouse I Left Join IndSpouse I1 ON I1.Relationship = I.AutoMemberID  Where I.Automemberid In(SELECT Distinct D.MEMBERID from DonationsInfo D Where D.DonorType ='Ind' AND D.donationdate BETWEEN '" & txtStartDate.Text & "' AND '" & txtEnddate.Text & "') OR (I1.Automemberid In (SELECT  Distinct D.MEMBERID from DonationsInfo D Where D.DonorType ='SPOUSE' AND D.donationdate BETWEEN '" & txtStartDate.Text & "' AND '" & txtEnddate.Text & "'))  Group by I.Automemberid,I1.AutoMemberID,I.firstname ,I.lastname,I1.FirstName, I.pubName,I1.FirstName,I1.LastName, I.ADDRESS1,I.ADDRESS2 ,I.ZIP,I.HPHONE,I.CPhone,I.EMAIL,I.city,I.state,I.ChapterID  order by I.AutoMemberID;" ',I.ChapterID ,I.ChapterID
        End If
        If ddlType.SelectedValue = "OWN" Then
            strSql = "SELECT "
            If Status = 1 Then
                strSql = strSql & " Top 100 "
            End If
            strSql = strSql & " B.Automemberid as Memberid, b.Organization_Name as FirstName , b.last_name as LastName,CASE WHEN b.PubName is Null Then b.Organization_Name Else b.PubName End as PubName,'' as SFirstName,'' as SLastName, a.amount,a.DonationDate" ', chapter.chapterCode 
            strSql = strSql & ", b.ADDRESS1 + ' ' + ISNULL(b.ADDRESS2,'') as Address,b.ZIP,b.PHONE as HPhone,'' as Cphone, b.EMAIL,b.city,b.state"
            strSql = strSql & "  FROM (DonationsInfo a left Join Event E ON A.EventId = E.EventId) Left Outer Join  chapter  on  a.chapterid = chapter.chapterid, OrganizationInfo b  where a.memberid = b.automemberid AND a.DonorType='OWN'"
        Else
            If Status = 1 Then
                strSql = strSql & " SELECT Top 100 "
            Else
                strSql = strSql & " SELECT "
            End If
            strSql = strSql & "  B.Automemberid as Memberid,CASE WHEN a.Anonymous ='Yes' Then '' Else b.firstname End as FirstName , CASE WHEN a.Anonymous ='Yes' Then 'Anonymous' Else b.lastname End as LastName,CASE WHEN a.Anonymous ='Yes' Then 'Anonymous' Else b.PubName End as PubName,"
            If ddlDonorDetail.SelectedValue = "Family" Then
                strSql = strSql & " CASE WHEN a.Anonymous ='Yes' Then '' Else b.SFirstName End as SFirstName, CASE WHEN a.Anonymous ='Yes' Then '' Else b.SLastName End SLastName, "
            Else
                strSql = strSql & " CASE WHEN a.Anonymous ='Yes' Then '' Else I1.FirstName End as SFirstName, CASE WHEN a.Anonymous ='Yes' Then '' Else I1.LastName End as SLastName, "
            End If
            strSql = strSql & " a.amount,a.DonationDate " ', chapter.chapterCode
            strSql = strSql & ", b.ADDRESS1 + ' ' + ISNULL(b.ADDRESS2,'') as Address,b.ZIP,b.HPHONE,b.CPhone,b.EMAIL,b.city,b.state"
            strSql = strSql & "  FROM (DonationsInfo a left Join Event E ON A.EventId = E.EventId) Left Outer Join  chapter  on  a.chapterid = chapter.chapterid, "
            If ddlDonorDetail.SelectedValue = "Family" Then
                strSql = strSql & "#TmpIndSpouse b  where ((a.memberid = b.automemberid) or (a.memberid = b.Spousememberid)) AND a.DonorType Not in ('OWN')"
            Else
                strSql = strSql & "IndSpouse b Left Join IndSpouse I1 ON I1.Relationship = b.AutoMemberID  where a.memberid = b.automemberid AND a.DonorType Not in ('OWN')"
            End If
        End If
        '***************** END HERE *******************************************
        If Not strChapterID = "" Then
            strSql = strSql & " AND b.ChapterID in ( " & strChapterID & ")"
        End If
        If ddlDonorDetail.SelectedValue = "Each" And ddlType.SelectedValue = "IND" Then
            strSql = strSql.Replace("b.PubName", "b.Firstname+' '+b.LastName")
        End If
        strSql = strSql & " AND donationdate BETWEEN '" & txtStartDate.Text & "' AND '" & txtEnddate.Text & "'"
        If ddlLevel.SelectedValue = "Total" And ddlType.SelectedValue = "OWN" Then
            strSql = strSql.Replace("a.amount,a.DonationDate", "SUM(a.amount) as Amount,Null as DonationDate")
            strSql = strSql & " Group By B.Automemberid,b.Organization_Name,b.last_name,b.PubName,b.ADDRESS1 , b.ADDRESS2,b.ZIP,b.PHONE,b.EMAIL,b.city,b.state ORDER BY Sum(a.Amount) Desc" ',chapter.chapterCode
        ElseIf ddlLevel.SelectedValue = "Total" Then
            strSql = strSql & " Group By  B.Automemberid,a.Anonymous, b.firstname , b.lastname ,b.PubName,"
            If ddlDonorDetail.SelectedValue = "Family" Then
                strSql = strSql & " b.SFirstName,b.SLastName, "
            Else
                strSql = strSql & " I1.FirstName,I1.LastName, "
            End If
            strSql = strSql & " b.ADDRESS1, b.ADDRESS2 ,b.ZIP,b.HPHONE,b.CPhone,b.EMAIL,b.city,b.state Having Sum(a.Amount) >= 25 ORDER BY Sum(a.Amount) Desc " 'chapter.chapterCode ,
            strSql = strSql.Replace("a.amount,a.DonationDate", "SUM(a.amount) as Amount,Null as DonationDate")
        Else
            strSql = strSql & " ORDER BY a.Amount Desc "
        End If
        If ddlDonorDetail.SelectedValue = "Family" And ddlType.SelectedValue = "IND" Then
            strSql = strSql & ";drop table #TmpIndSpouse "
        End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim tableName As String() = New String(0) {}
        tableName(0) = "Donation"
        Dim dadapter As SqlDataAdapter = New SqlDataAdapter()
        'Response.Write(strSql)

        Try
            SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsDonation, tableName)
            conn.ConnectionString = Application("ConnectionString")
            dadapter.SelectCommand = New SqlCommand(strSql, conn)
            conn.Open()
            dadapter.SelectCommand.CommandTimeout = 600
            dadapter.SelectCommand.CommandType = CommandType.Text
            dadapter.Fill(dsDonation)
            conn.Close()
            If dsDonation.Tables(0).Rows.Count > 0 And Status = 2 Then
                Dg.DataSource = dsDonation
                Dg.DataBind()
                'If ddlType.SelectedValue = "OWN" Then
                '    dsDonation.Tables(0).Columns.RemoveAt(4)
                '    dsDonation.Tables(0).Columns.RemoveAt(5)
                'End If
                'If DdlReport.SelectedValue = "Short" Then
                '    dsDonation.Tables(0).Columns.RemoveAt(9)
                '    dsDonation.Tables(0).Columns.RemoveAt(10)
                '    dsDonation.Tables(0).Columns.RemoveAt(11)
                '    dsDonation.Tables(0).Columns.RemoveAt(12)
                '    'dsDonation.Tables(0).Columns.RemoveAt(13)
                'End If
                'If Not ddlLevel.SelectedValue = "Detail" Then
                '    dsDonation.Tables(0).Columns.RemoveAt(7)
                'End If
                
            ElseIf dsDonation.Tables(0).Rows.Count > 0 Then
                If ddlType.SelectedValue = "OWN" Then
                    dgDonationList.Columns(4).Visible = False
                    dgDonationList.Columns(5).Visible = False
                Else
                    dgDonationList.Columns(4).Visible = True
                    dgDonationList.Columns(5).Visible = True
                End If
                dgDonationList.Visible = True
                dgDonationList.Columns(7).Visible = True
                dgDonationList.Columns(9).Visible = True
                dgDonationList.Columns(10).Visible = True
                dgDonationList.Columns(11).Visible = True
                dgDonationList.Columns(12).Visible = True
                dgDonationList.Columns(13).Visible = True
                dgDonationList.DataSource = dsDonation
                dgDonationList.CurrentPageIndex = 0
                dgDonationList.DataBind()
                Session("dgDonationList") = dsDonation
                'lblerr.Text = strSql
                If DdlReport.SelectedValue = "Short" Then
                    dgDonationList.Columns(9).Visible = False
                    dgDonationList.Columns(10).Visible = False
                    dgDonationList.Columns(11).Visible = False
                    dgDonationList.Columns(12).Visible = False
                    dgDonationList.Columns(13).Visible = False
                End If
                If ddlLevel.SelectedValue = "Detail" Then
                    dgDonationList.Columns(7).Visible = True
                Else
                    dgDonationList.Columns(7).Visible = False
                End If
                btnExport.Visible = True
            Else
                dgDonationList.Visible = False
                btnExport.Visible = False
                lblerr.Text = "No donations found."
            End If
        Catch ex As Exception
            'lblerr.Text = strSql & "<br>" & ex.ToString()
            lblerr.Text = "Error in Execution" & "<br>" & ex.ToString()
        End Try
    End Sub
    Protected Sub dgDonationList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        dgDonationList.CurrentPageIndex = e.NewPageIndex
        dgDonationList.DataSource = Session("dgDonationList")
        dgDonationList.DataBind()
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=DonorList.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        LoadGrid(dgexport, 2)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub

    Protected Sub ddlType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlType.SelectedValue = "IND" Then
            ddlDonorDetail.Enabled = True
        Else
            ddlDonorDetail.Enabled = False
        End If
    End Sub

    Protected Sub NvgBtn_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles NvgBtn.Click
        Session.Remove("dgDonationList")
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub
End Class
