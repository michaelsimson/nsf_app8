Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddupEvent
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            ddlStatus.Items.FindByText("Select Status").Selected = True
            GetRecords()
            GetYear()
        End If
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Event")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("EventID") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from Event where EventId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtEveCode.Text = dsRecords.Tables(0).Rows(0)("EventCode").ToString()
        txtEveName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        ddlyear.ClearSelection()
        txtEveCode.Enabled = False
        Dim i As Integer
        For i = 0 To ddlyear.Items.Count - 1
            If (ddlyear.Items(i).Text = dsRecords.Tables(0).Rows(0)("EventYear").ToString()) Then
                ddlyear.SelectedIndex = i
                Exit For
            End If
       Next
        ddlStatus.ClearSelection()
        For i = 0 To ddlStatus.Items.Count - 1
            If (ddlStatus.Items(i).Value = dsRecords.Tables(0).Rows(0)("Status").ToString()) Then
                ddlStatus.SelectedIndex = i
                Exit For
            End If
        Next         
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        Dim cnt As Integer = 0
        If BtnAdd.Text = "Add" Then
            If txtEveCode.Text.Length > 0 Then
                If txtEveName.Text.Length > 0 Then
                    If Not ddlStatus.Text = "Select Status" Then
                        Strsql = "Select count(EventCode) from Event where EventCode='" & txtEveCode.Text & "'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Strsql = "Insert Into Event( EventCode, Name, EventYear, Status, CreateDate, CreatedBy) Values ('"
                            Strsql = Strsql & txtEveCode.Text & "','" & txtEveName.Text & "'," & ddlyear.SelectedItem.Value & ",'"
                            Strsql = Strsql & ddlStatus.Text & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "','" & Session("LoginID") & "')"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetRecords()
                            clear()
                            Pnl3Msg.Text = "Record Added Successfully"
                        Else
                            Pnl3Msg.Text = "The Code already Exist"
                            txtEveCode.Focus()
                        End If
                    Else
                        Pnl3Msg.Text = "Please Select Valid Status"
                    End If
                    Else
                        Pnl3Msg.Text = "Please Enter Event Name"
                    End If

            Else
                Pnl3Msg.Text = "Please Enter Event Code"
            End If
        ElseIf BtnAdd.Text = "Update" Then
            If txtEveCode.Text.Length > 0 Then
                If txtEveName.Text.Length > 0 Then
                    If Not ddlStatus.Text = "Select Status" Then
                        Strsql = "Update Event Set EventYear=" & ddlyear.SelectedItem.Value & ", Status='" & ddlStatus.SelectedItem.Value & "', ModifyDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy='" & Session("LoginID") & "', EventCode='" & txtEveCode.Text & "',Name = '" & txtEveName.Text & "'  Where EventID=" & Session("EventID")
                        'MsgBox(Strsql)
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetRecords()
                        clear()
                        Pnl3Msg.Text = "Record Updated Successfully"
                        BtnAdd.Text = "Add"
                    Else
                        Pnl3Msg.Text = "Please Select Valid Status"
                    End If
                Else
                    Pnl3Msg.Text = "Please Enter Event Name"
                End If

            Else
                Pnl3Msg.Text = "Please Enter Event Code"
            End If
        End If
    End Sub
    Private Sub clear()
        txtEveCode.Text = ""
        txtEveName.Text = ""
        ddlStatus.ClearSelection()
        ddlStatus.Items.FindByText("Select Status").Selected = True
        GetYear()
        txtEveCode.Enabled = True
        BtnAdd.Text = "Add"
    End Sub
    Private Sub GetYear()
        ddlyear.Items.Clear()
        Dim year As Integer = Convert.ToInt32(DateTime.Now.Year)
        ddlyear.Items.Insert(0, New ListItem(Convert.ToString(year - 1), (year - 1)))
        ddlyear.Items.Insert(1, New ListItem(Convert.ToString(year), year))
        ddlyear.Items.Insert(2, New ListItem(Convert.ToString(year + 1), (year + 1)))
        ddlyear.Items.Insert(3, New ListItem(Convert.ToString(year + 2), (year + 2)))
        ddlyear.Items.Insert(4, New ListItem(Convert.ToString(year + 3), (year + 3)))
        ddlyear.Items(1).Selected = True
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
        GetYear()
    End Sub

    Protected Sub CheckBox1_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles CheckBox1.CheckedChanged
        If CheckBox1.Checked = True Then
            lblold.Visible = True
            lblnew.Visible = True
            txtOld.Visible = True
            txtNew.Visible = True
            btnUpYear.Visible = True
            txtOld.Text = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "SELECT MAX(EventYear) FROM Event")

        Else

            lblold.Visible = False
            lblnew.Visible = False
            txtOld.Visible = False
            txtNew.Visible = False
            btnUpYear.Visible = False
            lblyear.Visible = False
            lblyear.Visible = False

        End If
    End Sub

    Protected Sub btnUpYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpYear.Click
        If txtOld.Text.Length > 0 And Val(txtNew.Text) > 2009 And Val(txtNew.Text) < 3000 Then
            Dim Strsql As String
            Strsql = "Update Event Set EventYear=" & Val(txtNew.Text) & ", Status='N', ModifyDate=Null, ModifiedBy=Null, CreateDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', CreatedBy ='" & Session("LoginID") & "'  Where EventYear=" & Val(txtOld.Text)
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
            lblold.Visible = False
            lblnew.Visible = False
            txtOld.Visible = False
            txtNew.Visible = False
            btnUpYear.Visible = False
            lblyear.Visible = False
            lblyear.Text = ""
            GetRecords()
            CheckBox1.Checked = False
            txtOld.Text = ""
            txtNew.Text = ""
            Pnl3Msg.Text = "Year Modified Successfully"
        Else
            lblyear.Visible = True
            lblyear.Text = "Enter Old And New Year to Update"
        End If
    End Sub
End Class
