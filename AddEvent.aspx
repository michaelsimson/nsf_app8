<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.AddEvent" CodeFile="AddEvent.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Add Event</title>
		<meta name="GENERATOR" content="Microsoft Visual Studio .NET 7.1">
		<meta name="CODE_LANGUAGE" content="Visual Basic .NET 7.1">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
		<script language="javascript">
			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('DatePicker.aspx?Ctl=' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}
		</script>
		<script language="vbscript">
			function ValidateDate(src, arg)
				if isDate(args.Value) then
					args.IsValid = True
				else
					args.IsValid = False
			End Function
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center">
				<tr>
					<td colspan="2" class="Heading" align="right">
						Add&nbsp;Contests:
					</td>
				</tr>
				<tr>
					<td vAlign="top" noWrap align="right" class="ItemLabel" style="HEIGHT: 20px">Event 
						Year</td>
					<td vAlign="top" noWrap align="left" style="HEIGHT: 20px"><asp:dropdownlist id="ddlEventYear" runat="server" AutoPostBack="True" CssClass="SmallFont">
							<asp:ListItem>Select Event Year</asp:ListItem>
							<asp:ListItem Value="2005">2005</asp:ListItem>
							<asp:ListItem Value="2006">2006</asp:ListItem>
							<asp:ListItem Value="2007">2007</asp:ListItem>
							<asp:ListItem Value="2008">2008</asp:ListItem>
							<asp:ListItem Value="2009">2009</asp:ListItem>
							<asp:ListItem Value="2010">2010</asp:ListItem>
							<asp:ListItem Value="2011">2011</asp:ListItem>
							<asp:ListItem Value="2012">2012</asp:ListItem>
							<asp:ListItem Value="2013">2013</asp:ListItem>
							<asp:ListItem Value="2014">2014</asp:ListItem>
							<asp:ListItem Value="2015">2015</asp:ListItem>
							<asp:ListItem Value="2016">2016</asp:ListItem>
							<asp:ListItem Value="2017">2017</asp:ListItem>
							<asp:ListItem Value="2018">2018</asp:ListItem>
							<asp:ListItem Value="2019">2019</asp:ListItem>
							<asp:ListItem Value="2020">2020</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td align="left" nowrap class="ItemLabel">
						Contest&nbsp;Date:
					</td>
					<td align="left" nowrap style="FONT-WEIGHT: bold; COLOR: #006600; FONT-VARIANT: normal">
						<asp:TextBox id="txtContestDate" runat="server" CssClass="SmallFont"></asp:TextBox>
						<a href="javascript:PopupPicker('txtContestDate', 200, 200);"><img src="images/calendar.gif" border="0">
						</a>
						<asp:CustomValidator id="cuvContestDate" runat="server" CssClass="SmallFont" ErrorMessage="Date should be a valid Calendar Date"
							ControlToValidate="txtContestDate" Display="Dynamic" ClientValidationFunction="ValidateDate"></asp:CustomValidator>
						<BR>
						Please enter the date in MM/DD/YY form or pick the date from the Calander by 
						clicking on the Calender
					</td>
				</tr>
				<tr>
					<td align="left" nowrap class="ItemLabel">
						Contest&nbsp;Type:
					</td>
					<td align="left" nowrap>
						<asp:RadioButtonList id="rblContestType" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal">
							<asp:ListItem Value="1">National</asp:ListItem>
							<asp:ListItem Value="2">Chapter</asp:ListItem>
						</asp:RadioButtonList>
						<asp:RequiredFieldValidator id="rfvContestType" runat="server" CssClass="SmallFont" Display="Dynamic" ControlToValidate="rblContestType"
							ErrorMessage="Contest Type Need to be Selected" InitialValue="0"></asp:RequiredFieldValidator>
					</td>
				</tr>
				<tr>
					<td align="left" nowrap class="ItemLabel">
						Contests:
					</td>
					<td align="left" nowrap>
						<table>
							<tr>
								<td><p>
										1. Spelling</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkSpelling" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										2. Vocabulary</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkVocab" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										3. Math</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkMath" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										4. Geography</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkGeography" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										5. Essay</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkEssay" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										6. Public Speaking</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkSpeaking" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
							<tr>
								<td><p>
										7. Brain Bee</p>
								</td>
							</tr>
							<tr>
								<td>
									<asp:CheckBoxList id="chkBrain" runat="server" CssClass="SmallFont"></asp:CheckBoxList>
									<P></P>
								</td>
							</tr>
						</table>
					</td>
				</tr>
				<TR>
					<TD align="center" noWrap vAlign="top" colSpan="2"></TD>
				</TR>
				
				<tr>
					<td align="left" nowrap class="ItemLabel">&nbsp;
					</td>
					<td valign="top" nowrap>
					</td>
				</tr>
				<tr>
					<td colspan="2" Class="ItemCenter" nowrap>
						<asp:Button id="btnSubmit" runat="server" Text="Create " CssClass="FormButton"></asp:Button>
					</td>
				</tr>
			</table>
			<DIV>
				<asp:HyperLink id="hlinkChapterFunctions" runat="server" NavigateUrl="ChapterMain.aspx">Back to Chapter Functions</asp:HyperLink></DIV>
		</form>
	</body>
</HTML>

 

 
 
 