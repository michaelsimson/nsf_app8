<%@ Page Language="VB" AutoEventWireup="false" CodeFile="NewShowBadgesPdf.aspx.vb" Inherits="NewShowBadgesPdf" %>
<%@ Reference Page="~/generatebadges.aspx" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>NorthSouth Foundation</title>
     <script language="javascript" type="text/javascript">
        function printdoc()
        {
            document.getElementById('btnPrint').style.visibility="hidden";
            document.getElementById('hlnkMainMenu').style.visibility="hidden";            
            window.print();
            document.getElementById('btnPrint').style.visibility="visible";
            document.getElementById('hlnkMainMenu').style.visibility="visible";            
            return false;
        }
    </script>
    <style type="text/css"  >
        @page Section1
	        {
	             size:8.5in 11.0in;
	            margin:.5in 11.25pt 0in 11.25pt;
	            mso-header-margin:.5in;
	            mso-footer-margin:.5in;
	            mso-paper-source:4;      
	         }
            div.Section1
	        {
	            page:Section1;
	        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlData">        
          <div class="Section1">          
               <table cellspacing="0"  style='border-collapse:collapse;'  cellpadding="0" width="100%"  align="center" border="0" >                                
                    <asp:PlaceHolder runat="server" ID="BadgeHolder"></asp:PlaceHolder>  
    		   </table> 	    		    
    	    </div>	    
		</asp:Panel>	

      <asp:Panel runat="server" ID="pnlDataNat">        
          <div class="Section1">   
                   <asp:DataList ID="dl" runat="server"  RepeatDirection="Horizontal" RepeatColumns="2" Width="1000px" ItemStyle-BackColor="#3366cc">
                       <ItemTemplate>
                        <asp:Label ID="lblPhotoname" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Photo_Name")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblPhoImgSrc" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Photo_Image")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblWeekDays" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Week_Days")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblBadNum" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Badge_Numbers")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblTShirt" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "TshirtSize")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblLunchType" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "LunchType")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblChildNumber" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "ChildNumber")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblFName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "First_Name")%>' Visible="false"></asp:Label>
                        <asp:Label ID="lblLName" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Last_Name")%>' Visible="false"></asp:Label>                           
              
                           <table cellspacing="0" width="100%" align="center" border="1">
                               <tr>
                                   <td align='center' style='width: 350px;' bgcolor='#0000FF'>
                                       <table border="0" style="width: 80%; border-color: #33CCFF; text-align: center; background-color: white" cellspacing="0" cellpadding="3">
                                           <tr>
                                               <td valign="top" style="background-color: #F3F5DC">
                                                   <table border="1" style="width: 100%;" cellspacing="0">
                                                       <tr>
                                                           <td align='center' bgcolor='white' colspan="7">
                                                               <span style="font-family: 'Times New Roman'; font-weight: bold; font-size: 11px;color:navy">
                                                                  <%# DataBinder.Eval(Container.DataItem, "CurYear").ToString() +" "+ "NSF National Championship Finals" %>
                                                               </span>
                                                               <br />
                                                               <font style="font-family:'Times New Roman'; font-size: 10px;color:deeppink;"><b>
                                                                  <%# DataBinder.Eval(Container.DataItem, "Venue")%>
                                                               </b></font>
                                                               <font style="font-family:'Times New Roman'; color: red; font-size: 10px">(<i>                                                                  
                                                       <%#contestdate(DataBinder.Eval(Container.DataItem, "Contest_Dates")) + "," + DataBinder.Eval(Container.DataItem, "CurYear").ToString()%></i>)</font>
                                                           </td>
                                                       </tr>
                                                       <tr>
                                                           <td align='center' bgcolor='white' colspan="7">
                                                       <table border="1" style="width: 100%; border-collapse: collapse; border-color: linen;">
                                                        <asp:PlaceHolder runat="server" ID="NationalBadgeHolder"></asp:PlaceHolder>
                                                       <tr>
                                                           <td bgcolor='#F0E68C' align='center' colspan="3">
                                                               <font style="font-family: Verdana; font-size: 10px; color: red"><b>
                                                                   <%# DataBinder.Eval(Container.DataItem, "parent_id")%> - <%# DataBinder.Eval(Container.DataItem, "child_number")%> 
                                                               </b></font>
                                                           </td>
                                                           <td bgcolor='#F0E68C' align='center' colspan="3">
                                                               <font style="font-family: Verdana; font-size: 10px; color: red"><b><%# DataBinder.Eval(Container.DataItem, "city").ToString() + "," + DataBinder.Eval(Container.DataItem, "state")%>

                                                                   
                                                               </b></font>
                                                           </td>
                                                           <td bgcolor='#F0E68C' align='center' style="width: 25%">
                                                               <font style="font-family: Verdana; font-size: 10px; color: red"><b>Gr:&nbsp;<font color="blue">
                                                                 <%# DataBinder.Eval(Container.DataItem, "grade")%>
                                                                     </font>
                                                               </b></font>
                                                           </td>
                                                       </tr>
                                                                                                                </table>
                                                       </td>
                                               </tr>
                                                   </table>
                                               </td>
                                           </tr>
                                       </table>

                                   </td>
                               </tr>
                           </table>
                          
                       </ItemTemplate>
                   </asp:DataList>    		     		    
    	    </div>	    
		</asp:Panel>		
	  <asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" style='border-collapse:collapse' cellpadding="0" width="100%"  align="left" border="0" >
            <tr>
                    <td class="Heading" style="height: 19px">
                        <asp:Label runat="server" ID="lblMessage"></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
	  <div style="page-break-before:always">
		      <asp:HyperLink runat="server" Text="Back to Volunteer Functions" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
              <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />       
        </div>
    </form>
</body>
</html>


 
 
 