Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail

Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration
    Partial Class Forgot
        Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            lblErrMsg.Visible = False
            lblConfirm.Visible = False
            Dim SecureUrl As String
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost") Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
            'Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            'If (homeURL <> Nothing) Then
            'homeLink.HRef = homeURL
            'End If          
            ClientScript.RegisterOnSubmitStatement([GetType], "Form1", "if (this.submitted) return false; this.submitted = true; return true;")
        End Sub
        Private Sub btnSend_Click1(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim mailObj As New MailMessage
            Dim Password As String = ""
            Try

            
                Password = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "getPassword", New SqlParameter("@Email", txtEmail.Text.Trim))

                If Password = "" Then
                    lblErrMsg.Visible = True
                    lblConfirm.Visible = False

                Else
                    lblErrMsg.Visible = False
                    lblConfirm.Visible = True

                    '*** Sending New Registration Confirmation Message 
                    Dim re As TextReader
                    Dim emailbody As String
                    re = File.OpenText(Server.MapPath("SendForgotPassword.htm"))
                    emailbody = re.ReadToEnd
                    re.Close()
                    emailbody = emailbody.Replace("[USERID]", txtEmail.Text)
                    emailbody = emailbody.Replace("[PASSWORD]", Password)

                    SendEmail("North South Foundation Registration", emailbody, txtEmail.Text)

                End If
            Catch ex As Exception
                ' Response.Write(ex.Message.ToString())
            End Try
        End Sub

        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            'leave blank to use default SMTP server

            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = "<strong>" + sBody + "</strong>"

            'Send Email
            Dim client As New SmtpClient()
            Dim ok As Boolean = True
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            Try
                client.Send(email)
            Catch e As Exception
                ok = False
            End Try

        End Sub

        Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click

        End Sub
    End Class

End Namespace

