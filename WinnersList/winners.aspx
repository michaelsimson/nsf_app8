<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>




<!--#include file="finals_header.aspx"-->
<div class="title02">Past Winners at National Finals - 
<%
    Dim year As String
    If Request.QueryString("contestYear") <> "" Then
        year = Request.QueryString("contestYear")
    Else
        Year = "All Years"
    End If
 %>

 
 <%=Year%>  <%=Request.QueryString("contest")%>
</div>

<div class="btn_01" align="left">

	<%
	Dim pageurl As String
	    pageurl = "winners.aspx"
	    Dim contestyear As String
	    Dim contest As String
	    
	    contestyear = Request.QueryString("contestYear")
	    contest = Request.QueryString("contest")
	    'If Request.QueryString("contestYear") <> "" Then
	    '    contestyear = Request.QueryString("contestYear")
	    'Else
	    '    contestyear = DateTime.Now.Year
	    'End If
	%>
	
	<!-- Spelling Bee -->

    	<% If Request.QueryString("contest") = "All Categories" Then%>
	All Categories
	<%else%>
	<a href=<%=pageurl%>?contest=<%=Server.UrlEncode("All Categories")%>&contestyear=<%=Server.UrlEncode(contestyear)%>>All Categories</a>
	<% end if %>
	


	<% if request.querystring("contest") = "Junior Spelling" then%>
	|Junior Spelling
	<%else%>
	|<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Spelling")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Spelling </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Spelling" then%>
	| Senior Spelling
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Spelling")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Spelling </a>
	<% end if %>
	
	<!-- Vocabulary Bee -->
	<% if request.querystring("contest") = "Junior Vocabulary" then%>
	| Junior Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Vocabulary")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Vocabulary </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Vocabulary" then%>
	| Intermediate Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Vocabulary")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Intermediate Vocabulary </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Vocabulary" then%>
	| Senior Vocabulary
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Vocabulary")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Vocabulary </a>
	<% end if %>
	
	<br />
	<!-- Math Bee -->
	<% if request.querystring("contest") = "Math Level 1" then%>
	Math Level 1
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 1")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Math Level 1</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Math Level 2" then%>
	| Math Level 2
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 2")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Math Level 2</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Math Level 3" then%>
	| Math Level 3
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Math Level 3")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Math Level 3</a>
	<% end if %>
	
	<!-- Geography Bee -->
	<% if request.querystring("contest") = "Junior Geography" then%>
	| Junior Geography
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Geography")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Geography</a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Geography" then%>
	| Senior Geography
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Geography")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Geography</a>
	<% end if %>
	
	<br />
	<!-- Public Speaking -->
	<% if request.querystring("contest") = "Junior Public Speaking" then%>
	Junior Public Speaking
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Public Speaking")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Public Speaking </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Public Speaking" then%>
	| Senior Public Speaking
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Public Speaking")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Public Speaking </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Junior Science" then%>
	| Junior Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Science")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Science </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Science" then%>
	| Intermediate Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Science")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Intermediate Science </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Science" then%>
	| Senior Science
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Science")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Science </a>
	<% end if %>
	
    
	<br />
	<!-- Essay Writing -->
	<% if request.querystring("contest") = "Junior Essay Writing" then%>
	Junior Essay Writing
	<%else%>
	<a href=<%=pageurl%>?contest=<%=server.urlencode("Junior Essay Writing")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Junior Essay Writing </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Intermediate Essay Writing" then%>
	| Intermediate Essay Writing
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Intermediate Essay Writing")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Intermediate Essay Writing </a>
	<% end if %>
	
	<% if request.querystring("contest") = "Senior Essay Writing" then%>
	| Senior Essay Writing
	<%else%>
	| <a href=<%=pageurl%>?contest=<%=server.urlencode("Senior Essay Writing")%>&contestyear=<%=Server.UrlEncode(contestyear)%>> Senior Essay Writing </a>
	<% end if %><br />
     <br />
     <% If Request.QueryString("contestyear") = "All Years" Then%>
	All Years
	<%else%>
	<a href=<%=pageurl%>?contest=<%=Server.UrlEncode(contest)%>&contestyear=<%=server.urlencode("All Years")%>>All Years</a>
	<% end if %>
	
     <%  For i As Integer = DateTime.Now.Year To 1993 Step -1
            %>


     |<a href=<%=pageurl%>?contest=<%=server.urlencode(contest)%>&contestyear=<%=Server.UrlEncode(i.ToString)%>><%=i%></a>


    
   <% Next
    %>
</div>
 
 <br />
<table width="100%" class="txt02" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
   <tr>
               <td width="5%" height="24" class="btn_06">Year</td>
		<td width="20%" class="btn_06">Contest Name </td>
        <td width="5%" class="btn_06">Rank</td>
        <td width="18%" class="btn_06">First Name</td>
		<td width="18%" class="btn_06">Last Name</td>
              <td width="10%" height="24" class="btn_06">City</td>
		<td width="10%" class="btn_06">State</td>
        <td width="20%" class="btn_06">Chapter</td>
    </tr>


<%
    Dim queryContest1 As String
    Dim queryContestYear As String
        Dim queryContest As String
    Dim IsAnyError1 As Boolean
	
    IsAnyError1 = True
    queryContest1 = ""
	queryContestYear = ""
	If Request.QueryString.Count = 0 Then
        queryContest1 = ""
        queryContestYear = ""
        IsAnyError1 = False

    ElseIf Request.QueryString.Count > 0 Then
       
        'contest value can not be empty
        If Request.QueryString("contest") <> "" Or Request.QueryString("contestyear")<>"" Then
            'read contest value
            queryContest = Request.QueryString("contest")
           ' queryContestYear = Request.QueryString("contestyear")
            If Request.QueryString("contestYear") <> "" Then
                queryContestYear = Request.QueryString("contestYear")
            Else
                queryContestYear = DateTime.Now.Year
            End If
            IsAnyError1 = False
        Else
            IsAnyError1 = True
        End If
    
      
        
    'accept only one querystring value
     
        End If

        If IsAnyError1 = True Then
            Response.Redirect("http://www.northsouth.org")
        End If

        ' Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)
    Dim cn1 As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("ConnectionString").ConnectionString)
        Dim cmd1 As SqlCommand

        Dim rs1 As SqlDataReader
        Dim strquery1 As String
    Dim ContestCode As String
    Dim ContestCodeYear As String
        cn1.Open()
        Try
        If queryContestYear = "" And queryContest = "" Then
            If Request.QueryString("contestYear") <> "" Then
                contestyear = Request.QueryString("contestYear")
            Else
                contestyear = DateTime.Now.Year
            End If
            'strquery1 = "select distinct ContestYear,ProductCode,  Rank, First_Name, Last_Name, City,State, ChapterCode from V_Finals_TopRanks where rank in (1,2,3) and ContestYear =" & contestyear & " order by ContestYear,ProductCode,Rank"
            strquery1 = "select distinct VFT.ContestYear,P.name, VFT. Rank, VFT.First_Name, VFT.Last_Name, VFT.City,VFT.State,VFT. ChapterCode,VFT.Productgroupid,VFT.productid from V_Finals_TopRanks "
            strquery1 = strquery1 & "VFT left join Product p on VFT.ProductCode=p.ProductCode where rank in (1,2,3) and P.Eventid=1 order by VFT.ContestYear,VFT.Productgroupid,VFT.productid,VFT.Rank"
      
        Else
            
            If contestyear = "" And contest <> "" And contest <> "All Categories" Then
                ContestCodeYear = String.Empty
                ContestCode = "and P.Name='" & queryContest & "'"
            ElseIf contestyear <> "" And contest = "" And contestyear <> "All Years" Then
                ContestCodeYear = "and ContestYear = " & queryContestYear & ""
                ContestCode = String.Empty
            ElseIf contestyear <> "" And contest <> "" And contestyear <> "All Years" And contest <> "All Categories" Then
                ContestCodeYear = "and ContestYear = " & queryContestYear & ""
                ContestCode = "and P.Name='" & queryContest & "'"
            ElseIf contestyear = "All Years" And contest = "All Categories" Then
                ContestCodeYear = String.Empty
                ContestCode = String.Empty
            ElseIf contestyear = "" And contest = "All Categories" Then
                ContestCodeYear = String.Empty
                ContestCode = String.Empty
            ElseIf contestyear = "All Years" And contest = "" Then
                ContestCode = String.Empty
                ContestCodeYear = String.Empty
            ElseIf contestyear <> "" And contest = "All Categories" Then
                ContestCode = String.Empty
                ContestCodeYear = "and ContestYear = " & queryContestYear & ""
            ElseIf contestyear = "All Years" And contest <> "" Then
                ContestCode = "and P.Name='" & queryContest & "'"
                ContestCodeYear = String.Empty
            End If
            
          
            strquery1 = "select distinct VFT.ContestYear,P.name, VFT. Rank, VFT.First_Name, VFT.Last_Name, VFT.City,VFT.State,VFT. ChapterCode,VFT.Productgroupid,VFT.productid from V_Finals_TopRanks "
            strquery1 = strquery1 & "VFT left join Product p on VFT.ProductCode=p.ProductCode where  P.Eventid=1 and rank in (1,2,3)  " & ContestCodeYear & "  " & ContestCode & " order by VFT.ContestYear,VFT.Productgroupid,VFT.productid,VFT.Rank"
            
        End If
        
    Catch ex As Exception
        Response.Write("Contest details not yet updated")
    End Try
  
   
    cmd1 = New SqlCommand(strquery1, cn1)
    rs1 = cmd1.ExecuteReader()
        
    While rs1.Read()

  
%>
<tr>
		<td bgcolor="#FFFFFF"><%=rs1(0)%></td>
		<td bgcolor="#FFFFFF"><%=rs1(1).ToString()%></td>
		<td bgcolor="#FFFFFF"><%=rs1(2)%></td>
	
    <td bgcolor="#FFFFFF"><%=rs1(3)%></td>
		<td bgcolor="#FFFFFF"><%=rs1(4)%></td>
		<td bgcolor="#FFFFFF"><%=rs1(5)%></td>
        <td bgcolor="#FFFFFF"><%=rs1(6)%></td>
                <td bgcolor="#FFFFFF"><%=rs1(7)%></td>
	</tr>
<%


End While

cn1.Close()
%>
</table>

<!--#include file="finals_footer.aspx"-->



