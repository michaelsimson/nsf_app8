Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports Microsoft.ApplicationBlocks.Data

Partial Class ScheduleNationalTechTeam
    Inherits System.Web.UI.Page
    Public contestId As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("LoggedIn") Is Nothing) Or (Session("LoggedIn") <> "True") Then
                If (Not Session("entryToken") Is Nothing) Then
                    Response.Redirect("Login.aspx?entry=" + Session("entryToken").ToString.Substring(1, 1))
                End If
            End If
            
            lblError.Text = String.Empty
            If Not Page.IsPostBack Then
                Dim nsfMaster As VRegistration.NSFMasterPage = Me.Master
                nsfMaster.addBackMenuItem("volunteerfunctions.aspx")
                'LoadEvent()
                LoadNTTMember()
                LoadGrid()
                LoadProductGroup()
            End If
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub
    'Private Sub LoadEvent()
    '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT Distinct EventId,Name FROM Event Where EventId in (1,2) ")
    '    If ds.Tables(0).Rows.Count > 0 Then
    '        ddlEvent.DataSource = ds
    '        ddlEvent.DataBind()
    '    End If
    '    ddlEvent.Items.Insert(0, "SELECT")
    'End Sub
    Private Sub LoadNTTMember()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT I.FirstName + '' + I.LastName as Name,I.AutoMemberID as MemberID FROM IndSpouse  I Inner Join Volunteer V on V.MemberId=I.AutoMemberID Where V.RoleId =93")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlNTTMember.DataSource = ds
            ddlNTTMember.DataBind()
        End If
        ddlNTTMember.Items.Insert(0, "SELECT NTT Member")
    End Sub
    Private Sub LoadProductGroup()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT Distinct ProductGroupId,ProductGroupCode,Name FROM ProductGroup where EventId in (2) Order by ProductGroupId ") ' & ddlEvent.SelectedValue)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlProductGroup.DataSource = ds
            ddlProductGroup.DataBind()
        End If
        ddlProductGroup.Items.Insert(0, "SELECT")
    End Sub
   
    Protected Sub DGNatTechTeam_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGNatTechTeam.ItemCommand
        Dim NTT_ID As Integer = CInt(e.Item.Cells(2).Text)
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from NatTechTeam Where NTTID=" & NTT_ID & "")
                Clear()
                LoadGrid()
            Catch ex As Exception
                lblError.Text = ex.Message
                lblError.Text = (lblError.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            BtnAddUpdate.Text = "Update"
            lblNTTID.Text = NTT_ID.ToString()
            loadforUpdate(NTT_ID)
        End If
    End Sub
    Private Sub loadforUpdate(ByVal NTT_ID As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select NTTID,RTRIM(LTRIM(ProductGroupCode)) as ProductGroupCode,RTRIM(LTRIM(Name)) as ProductName,NTTMemberID  from NatTechTeam Where NTTID=" & NTT_ID & "")
        If ds.Tables(0).Rows.Count > 0 Then
            'ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(ds.Tables(0).Rows(0)("EventID")))
            ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(ds.Tables(0).Rows(0)("ProductGroupCode")))
            ddlNTTMember.SelectedIndex = ddlNTTMember.Items.IndexOf(ddlNTTMember.Items.FindByValue(ds.Tables(0).Rows(0)("NTTMemberID")))
        End If
    End Sub
    
    Private Sub LoadGrid()
        DGNatTechTeam.Visible = True
        Dim SQLStr As String = "SELECT N.NTTID,N.ProductGroupID,N.ProductGroupCode,IsNull(N.Name,'') as ProductName,N.NTTMemberID,I.FirstName + '' + I.LastName as Name From NatTechTeam N Inner Join IndSpouse I on I.AutoMemberID= N.NTTMemberID Order by ProductGroupID"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)

        If ds.Tables(0).Rows.Count > 0 Then
            DGNatTechTeam.DataSource = ds
            DGNatTechTeam.DataBind()
            DGNatTechTeam.Columns(2).Visible = False
            DGNatTechTeam.Columns(3).Visible = False
        Else
            DGNatTechTeam.Visible = False
        End If
    End Sub

   
    Protected Sub BtnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddUpdate.Click
        Dim SQLInsertStr As String = ""
        Dim SQLUpdateStr As String = ""
        SQLInsertStr = SQLInsertStr & "Insert into NatTechTeam (ProductGroupId,ProductGroupCode,Name,NTTMemberID,CreatedDate,CreatedBy) Values ("
        SQLInsertStr = SQLInsertStr & "(SELECT Top 1 ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' and EventId in (1,2))" & ",'" & ddlProductGroup.SelectedItem.Value & "','" & ddlProductGroup.SelectedItem.Text & "'," & ddlNTTMember.SelectedValue & ",GETDATE(),'" & Session("LoginID") & "')"

        SQLUpdateStr = SQLUpdateStr & "Update NatTechTeam Set ProductGroupId = (SELECT Top 1 ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' and EventId in (1,2))" & ",ProductGroupCode ='" & ddlProductGroup.SelectedItem.Value & "',Name='" & ddlProductGroup.SelectedItem.Text & "',NTTMEmberID=" & ddlNTTMember.SelectedValue & ",ModifiedDate=GETDATE(),ModifiedBy='" & Session("LoginID") & "' Where NTTID=" & lblNTTID.Text

        If ddlProductGroup.SelectedIndex = 0 Then
            lblError.Text = "Please select ProductGroup"
            Exit Sub
        ElseIf ddlNTTMember.SelectedIndex = 0 Then
            lblError.Text = "Please select NTT Member"
            Exit Sub
        End If

        If BtnAddUpdate.Text = "Add" Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Count(*) From NatTechTeam Where RTRIM(LTRIM(ProductGroupCode)) in ('" & ddlProductGroup.SelectedItem.Value & "')") = 0 Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLInsertStr) > 0 Then
                    lblError.Text = "Added Successfully"
                End If
            Else
                lblError.Text = "The Product Group already exist"
                Exit Sub
            End If
        ElseIf BtnAddUpdate.Text = "Update" Then
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLUpdateStr) > 0 Then
                lblError.Text = "Updated Successfully"
            End If
            BtnAddUpdate.Text = "Add"
        End If
        LoadGrid()
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        Clear()
    End Sub
    Private Sub Clear()
        Try
            ddlProductGroup.SelectedIndex = 0
            ddlNTTMember.SelectedIndex = 0
            lblError.Text = String.Empty
            BtnAddUpdate.Text = "Add"
        Catch ex As Exception
            lblError.Text = ex.Message
        End Try
    End Sub

End Class



