Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration

Partial Class UpdatePhotoStatus
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
        If Not Page.IsPostBack Then
            trPhoto.Visible = False
        End If
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Not Page.IsPostBack Then
            trPhoto.Visible = False
            trContestant.Visible = False
        End If
    End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            'Dim strLastName As String
            'Dim strFirstName As String

        Dim connContest As New SqlConnection(Application("ConnectionString"))
        Dim dsContestant As New DataSet
            'Dim intRow As Integer
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(3) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@FirstName"
        prmArray(0).Value = txtFirstName.Text
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@LastName"
        prmArray(1).Value = txtLastName.Text
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@ContestYear"
        prmArray(2).Value = Application("ContestYear")
        prmArray(2).Direction = ParameterDirection.Input

        SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContestsByName", dsContestant, tblConestant, prmArray)
        Dim dv As DataView
        dv = New DataView(dsContestant.Tables(0))
        If dv.Table.Rows.Count > 0 Then
            rblContestants.DataSource = dsContestant.Tables(0)
            rblContestants.DataTextField = "ContestantDetails"
            rblContestants.DataValueField = "ChildNumber"
            rblContestants.DataBind()
            trPhoto.Visible = True
            btnSubmit.Visible = False
            trContestant.Visible = True
        Else
            Dim strMessage As String = "No matching Contestant is found. Please verify the Last and First Name entered before submitting"
            Dim strScript As String = "<script language=JavaScript>"
            strScript += "alert(""" & strMessage & """);"
            strScript += "</script>"
                ClientScript.RegisterStartupScript([GetType], "Error", strScript)
        End If
        connContest = Nothing
    End Sub

    Private Sub btnUpdatePhoto_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnUpdatePhoto.Click

        Dim connContest As New SqlConnection(Application("ConnectionString"))
        Dim prmArray(3) As SqlParameter
            Dim strMessage As String = ""
            Dim strScript As String = ""
        If rblContestants.SelectedIndex = -1 Then
            strMessage = "Contestant need to be selected inorder to update the Photo Info"
        End If
        If txtPhotoFile.Text = "" Then
            strMessage = "Photo File name need to be entered to update the Photo Info"
        End If
        If rblContestants.SelectedIndex = -1 Or txtPhotoFile.Text = "" Then
            strScript = "<script language=JavaScript>"
                strScript += "alert(""" & strMessage.ToString & """);"
            strScript += "</script>"
                ClientScript.RegisterStartupScript([GetType], "Error", strScript)
            Exit Sub
        End If

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ChildNumber"
        prmArray(0).Value = rblContestants.SelectedItem.Value
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@PhotoFile"
        prmArray(2).Value = txtPhotoFile.Text
        prmArray(2).Direction = ParameterDirection.Input

        SqlHelper.ExecuteNonQuery(connContest, CommandType.StoredProcedure, "ust_UpdatePhotoLink", prmArray)

        trPhoto.Visible = False
        trContestant.Visible = False
        btnSubmit.Visible = True

        connContest = Nothing
        strScript = "<script language=JavaScript>"
        strScript += "alert(""" & "Photo Info Update Successful" & """);"
        strScript += "</script>"
            'RegisterStartupScript("Error", strScript)
            ClientScript.RegisterStartupScript([GetType], "Error", strScript)

    End Sub

    Private Sub btnReset_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnReset.Click
        Response.Redirect("UpdatePhotoStatus.aspx")
    End Sub
End Class

End Namespace

