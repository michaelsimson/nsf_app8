﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Configuration;
using System.IO;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using NativeExcel;



public partial class PriorParticipation : System.Web.UI.Page
{
    #region <<Variable Declaration>>
    int Table2RowCount = 0;
    #endregion
    #region <<Pre-defined Functions>>
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            fillFundRaiserReg();
        }
   
        }
           catch (Exception ex) {
               Response.Write("Err :" + ex.ToString());
           }
    }

    
    protected void gvFundRaiserReg_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvFundRaiserReg.PageIndex = e.NewPageIndex;
        fillFundRaiserReg();
    }
    protected void gvTable2_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvTable2.PageIndex = e.NewPageIndex;
        displayTable2(Session["CFirstName"].ToString(), Session["CLastName"].ToString(), Session["FFName"].ToString(), Session["FLName"].ToString(), Session["MFName"].ToString(), Session["MFName"].ToString());
    }
    #endregion

    #region <<User-defined Functions>>

    protected void Function_RowCommand(Object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Select")
        {
            GridViewRow gvr = (GridViewRow)((Control)e.CommandSource).NamingContainer;
            int rowIndex = gvr.RowIndex;

            //HiddenField hfId = (gvFundRaiserReg.Rows[rowIndex].Cells[0].FindControl("FundRContRegID") as HiddenField);
            //string strid = hfId.Value;

            Label hslblFN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[2].FindControl("Label1") as Label);
            Session["CFirstName"] = hslblFN.Text;

            Label hslblLN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[3].FindControl("Label2") as Label);
            Session["CLastName"] = hslblLN.Text;

            Label hslblFFN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[3].FindControl("Label3") as Label);
            Session["FFName"] = hslblFFN.Text;
            Label hslblFLN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[3].FindControl("Label4") as Label);
            Session["FLName"] = hslblFLN.Text;

            Label hslblMFN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[3].FindControl("Label9") as Label);
            Session["MFName"] = hslblMFN.Text;
            Label hslblMLN = (Label)(gvFundRaiserReg.Rows[rowIndex].Cells[3].FindControl("Label10") as Label);
            Session["MLName"] = hslblMLN.Text;
            Button1.Visible = true;
            divTable2.Visible = true;
            gvTable2.Visible = true;
            btnClose.Visible = true;
            displayTable2(Session["CFirstName"].ToString(), Session["CLastName"].ToString(), Session["FFName"].ToString(), Session["FLName"].ToString(), Session["MFName"].ToString(), Session["MLName"].ToString());
        }
    }
    protected void displayTable2(string CFirstName, string CLastName,string FFName,string FLName,string MFName, string MLName)
    {

        string strquery = "";
        strquery = "select (select case when(select COUNT(*) as Count from Contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' else null end) as Prior,C.ChildNumber,C.First_Name,C.Last_Name,IP.FirstName as FFFName,IP.LastName as FFLName,IP.AutoMemberID,IP.Email,IP.HPhone,IP.CPhone,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.Relationship,IP1.Email as MFEmail,IP.Address1,IP.City,IP.State,IP.Zip from Child c left join Indspouse IP on (C.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=C.MemberId) where c.First_Name='" + CFirstName + "' and c.Last_Name='" + CLastName + "' and IP.FirstName='" + FFName + "' and IP.LastName='" + FLName + "'";
        if (MFName != "")
        {
            strquery = strquery + " and IP1.FirstName=";
            strquery = strquery + "'" + MFName + "'";
        }



        if (MLName != "")
        {
            strquery = strquery + " and IP1.LastName=";
            strquery = strquery + "'" + MLName + "'";
        }
         
        //strquery = "select C.First_Name,C.Last_Name,IP.FirstName as FFFName,IP.LastName as FFLName,IP.MemberID,IP.Email,IP.HPhone,IP.CPhone,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.Relationship,IP1.Email as MFEmail,IP.Address1,IP.City,IP.State,IP.Zip from Child c left join Indspouse IP on (C.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=C.MemberId)  where c.First_Name like '%" + CFirstName + "%' and c.Last_Name like '%" + CLastName + "%' and IP.FirstName like '%" + FFName + "%' and IP1.FirstName like '%" + MFName + "%'";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        DataColumn column = dsJVB.Tables[0].Columns.Add("Ser#", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int iy = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["Ser#"] = iy;
            iy++;
        }
        Table2RowCount=dsJVB.Tables[0].Rows.Count;
        gvTable2.DataSource = dsJVB.Tables[0];
        gvTable2.DataBind();
        btnClose.Visible = true;
        btnClose.Focus();
        
    }
    protected void fillFundRaiserReg()
    {
        string strquery = "";
        strquery = "select distinct (select case when(select COUNT(*) as Count from Contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' else null end) as Prior,C.ChildNumber,C.First_Name,C.Last_Name,IP.FirstName as FFFName,IP.LastName as FFLName,IP.AutoMemberID,IP.Email,IP.HPhone,IP.CPhone,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.Relationship,IP1.Email as MFEmail,IP.Address1,IP.City,IP.State,IP.Zip from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID) left join IndSpouse IP1 on(IP1.Relationship=FR.MemberId) inner join Child C on (FR.ChildNumber=C.ChildNumber)  Order by C.Last_Name,C.First_Name ";
        //strquery = " select C.ChildNumber,C.First_Name,C.Last_Name,IP.FirstName as FFFName,IP.LastName as FFLName,IP.MemberID,IP.Email,IP.HPhone,IP.CPhone,IP1.LastName as MFLName,IP1.FirstName as MFFName,IP1.Relationship,IP1.Email as MFEmail,IP.Address1,IP.City,IP.State,IP.Zip from Child c where c.childNumber in (select FR.childnumber from FundRContestReg FR left join Indspouse IP on (FR.MemberID=IP.AutoMemberID)  left join IndSpouse IP1 on (IP1.Relationship=FR.MemberId)  left join FundRContestReg C on (FR.ChildNumber=C.ChildNumber) ) Order by C.Last_Name,C.First_Name ";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        

        DataColumn column = dsJVB.Tables[0].Columns.Add("Ser#", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int iy = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["Ser#"] = iy;
            iy++;

        }

        gvFundRaiserReg.DataSource = dsJVB.Tables[0];
        gvFundRaiserReg.DataBind();
        Session["FundRaiserReg"] = dsJVB;
    }
    protected void btnClose_Click(object sender, EventArgs e)
    {
        gvTable2.Visible = false;
        divTable2.Visible = false;
        Button1.Visible = false;
        btnClose.Visible = false;
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        string strquery = "";
        strquery = "update FundRContestReg set Prior=null; update FundRContestReg set Prior=((select case when(select count(*) from contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' Else null End)) From child c inner join FundRContestReg CR on c.ChildNumber=CR.ChildNumber";
        int ii = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        Label124.Visible = true;
        if (ii > 0)
            Label124.Text = "Inserted PriorParticipation Flag Successfully";
        Button1.Focus();
    }

    #endregion

    protected void gvTable2_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            Label lblChildNumber = (e.Row.FindControl("Label123") as Label);
           
            if (Table2RowCount > 1)
            {
                DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select * from FundRContestReg where ChildNumber=" + lblChildNumber.Text);
                if (ds1.Tables[0].Rows.Count > 0)
                {
                    e.Row.BackColor = Color.LightGreen;
                }
            }
        }
    }
   
    protected void Button2_Click(object sender, EventArgs e)
    {
        if (Session["FundRaiserReg"] == null)
        {
            return;
        }
        DataSet ds = (DataSet)Session["FundRaiserReg"];
        IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
        IWorksheet oSheet = default(IWorksheet);
        oSheet = oWorkbooks.Worksheets.Add();

        string FileName = "FundRaiserReg_ChildNames.xls";
        oSheet.Range["A1:Q1"].MergeCells = true;
        oSheet.Range["A1"].Value = "Name of Children from Fundraiser Registration";
        oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
        oSheet.Range["A1"].Font.Bold = true;
        oSheet.Range["A1"].Font.Color = Color.Blue;


        oSheet.Range["A3"].Value = "Ser#";
        oSheet.Range["A3"].Font.Bold = true;
        oSheet.Range["B3"].Value = "Prior";
        oSheet.Range["B3"].Font.Bold = true;
        oSheet.Range["C3"].Value = "ChildNumber";
        oSheet.Range["C3"].Font.Bold = true;

        oSheet.Range["D3"].Value = "First_Name";
        oSheet.Range["D3"].Font.Bold = true;
        oSheet.Range["E3"].Value = "Last_Name";
        oSheet.Range["E3"].Font.Bold = true;

        oSheet.Range["F3"].Value = "FFFName";
        oSheet.Range["F3"].Font.Bold = true;
        oSheet.Range["G3"].Value = "FFLName";
        oSheet.Range["G3"].Font.Bold = true;
        oSheet.Range["H3"].Value = "AutoMemberID";
        oSheet.Range["H3"].Font.Bold = true;
        oSheet.Range["I3"].Value = "Email";
        oSheet.Range["I3"].Font.Bold = true;
        oSheet.Range["J3"].Value = "HPhone";
        oSheet.Range["J3"].Font.Bold = true;
        oSheet.Range["K3"].Value = "CPhone";
        oSheet.Range["K3"].Font.Bold = true;
        oSheet.Range["L3"].Value = "MFLName";
        oSheet.Range["L3"].Font.Bold = true;

        oSheet.Range["M3"].Value = "MFFName";
        oSheet.Range["M3"].Font.Bold = true;
        oSheet.Range["N3"].Value = "Relationship";
        oSheet.Range["N3"].Font.Bold = true;
        oSheet.Range["O3"].Value = "Address1";
        oSheet.Range["O3"].Font.Bold = true;
        oSheet.Range["P3"].Value = "City";
        oSheet.Range["P3"].Font.Bold = true;
        oSheet.Range["Q3"].Value = "ZIP";
        oSheet.Range["Q3"].Font.Bold = true;
        int iRowIndex = 4;
        IRange CRange = default(IRange);
        int i = 0;
        while (i < ds.Tables[0].Rows.Count)
        {
            DataRow dr = ds.Tables[0].Rows[i];

            CRange = oSheet.Range["A" + iRowIndex.ToString()];
            CRange.Value = dr["Ser#"];

            CRange = oSheet.Range["B" + iRowIndex.ToString()];
            CRange.Value = dr["Prior"];

            CRange = oSheet.Range["C" + iRowIndex.ToString()];
            CRange.Value = dr["ChildNumber"];

            CRange = oSheet.Range["D" + iRowIndex.ToString()];
            CRange.Value = dr["First_Name"];

            CRange = oSheet.Range["E" + iRowIndex.ToString()];
            CRange.Value = dr["Last_Name"];

            CRange = oSheet.Range["F" + iRowIndex.ToString()];
            CRange.Value = dr["FFFName"];

            CRange = oSheet.Range["G" + iRowIndex.ToString()];
            CRange.Value = dr["FFLName"];

            CRange = oSheet.Range["H" + iRowIndex.ToString()];
            CRange.Value = dr["AutoMemberID"];

            CRange = oSheet.Range["I" + iRowIndex.ToString()];
            CRange.Value = dr["Email"];

            CRange = oSheet.Range["J" + iRowIndex.ToString()];
            CRange.Value = dr["HPhone"];

            CRange = oSheet.Range["K" + iRowIndex.ToString()];
            CRange.Value = dr["CPhone"];

            CRange = oSheet.Range["L" + iRowIndex.ToString()];
            CRange.Value = dr["MFLName"];

            CRange = oSheet.Range["M" + iRowIndex.ToString()];
            CRange.Value = dr["MFFName"];

            CRange = oSheet.Range["N" + iRowIndex.ToString()];
            CRange.Value = dr["Relationship"];

            CRange = oSheet.Range["O" + iRowIndex.ToString()];
            CRange.Value = dr["Address1"];

            CRange = oSheet.Range["P" + iRowIndex.ToString()];
            CRange.Value = dr["City"];

            CRange = oSheet.Range["Q" + iRowIndex.ToString()];
            CRange.Value = dr["Zip"];

            iRowIndex = iRowIndex + 1;
            i = i + 1;
        }
        Response.Clear();
        Response.ContentType = "application/vnd.ms-excel";
        Response.AddHeader("Content-Type", "application/vnd.ms-excel");
        Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
        oWorkbooks.SaveAs(Response.OutputStream);
        Response.End();
    }
    protected void Button3_Click(object sender, EventArgs e)
    {
        string strquery = "";
        strquery = "update FundRContestReg set Prior=null; update FundRContestReg set Prior=((select case when(select count(*) from contestant where ChildNumber=C.ChildNumber and Score1+Score2>0)>0 then 'Y' Else null End)) From child c inner join FundRContestReg CR on c.ChildNumber=CR.ChildNumber";
        int ii = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        Label156.Visible = true;
        if (ii > 0)
        {
            Label156.Text = "Inserted PriorParticipation Flag Successfully";
            fillFundRaiserReg();
        }
        
    }
}