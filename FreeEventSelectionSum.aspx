<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FreeEventSelectionSum.aspx.vb" Inherits="FreeEventSelectionSum" title="Summary of Your Free Event Selections" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left">
						<asp:hyperlink id="hlinkRegistration" runat="server">Back to Registration Page</asp:hyperlink>
						</td>
						<td class="ContentSubTitle" align="left">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</td> 
						<td class="ContentSubTitle" align="left">
						 
						 </td>
				</tr>
			</table>
    <table width="100%">
				<tr>
					<td class="Heading"  align="center" colspan="2">Summary of Your Event Selections
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label> - <asp:label ID="lblChapter" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2"  Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr><td>
							<asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td></tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont" Font-Bold="true"></asp:label></td>
							</tr>
							
							<tr><td>
				    	<asp:CheckBox ID="ChkEmail" runat="server" Visible="true" ForeColor ="Red" Font-Bold="True" Text="I agree to use this email to communicate with NSF at all times"/>&nbsp;
                        <asp:LinkButton  class="btn_02" ID="hlinkEmail" runat="server" >Change Email</asp:LinkButton>.(This will change your login too)
				        </td></tr>
							
<tr>
					<td class="ItemCenter" align="center"><asp:Label ID="lblEmailInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="True" ></asp:Label></td>
				</tr>
							
							
						</table>
					</td> 
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">&nbsp;
					</td>
				</tr>
				<tr>

                  <%-- REG.FrMemberId,FR.Year,FR.FreeEventId,C.chapterId, C.ChapterCode,FR.EventCode,FR.EventName  ,FR.VenueName,FR.VenueAddress,FR.EventDate,FR.EventDescription,
                   chREG.ChildId, chREG.ChildName, chREG.Email, chREG.PartType Type,Case when chREG.PartType='Parent' then FR.AdultFee Else FR.ChildFee END Fee, FR.Discount--%>

					<td style="width: 819px">
					<asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="childId"  
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" SortExpression="FIRST_NAME">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name")%>' CssClass="SmallFont">
										</asp:Label>
									
									</ItemTemplate>
								</asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Type" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Type")%>' CssClass="SmallFont">
										</asp:Label>
									
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Event Name" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" SortExpression="ContestCode">
									<ItemTemplate>
                                    <asp:Label id="lblProductCode" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode")%>' CssClass="SmallFont"/>
							        <asp:Label id="lblChildId" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildId")%>' CssClass="SmallFont"/>
									<asp:Label id="lblFrMemberId" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FrMemberId")%>' CssClass="SmallFont"/>
									<asp:Label id="lblChapterID" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.chapterId")%>' CssClass="SmallFont"/>
                                    <asp:Label id="lblDiscount" Visible="false" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Discount")%>' CssClass="SmallFont"/>
									<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventName")%>' CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"  SortExpression="Fee" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee", "{0:c}") %>'  CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Event Date" HeaderStyle-Font-Bold="true" SortExpression="EventDate" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="Labe2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate", "{0:d}")%>' CssClass="SmallFont">
										</asp:Label>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" SortExpression="Chapter" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="Labe3" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterCode")%>' CssClass="SmallFont">
										</asp:Label>
										
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Venue" HeaderStyle-Font-Bold="true" SortExpression="Venue" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										
										<asp:Label id="lblVenueName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VenueName") %>' CssClass="SmallFont">
										</asp:Label><br />
										<asp:Label id="lblVenueAddr" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.VenueAddress") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="ParentId" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000" Visible="False">
									<ItemTemplate>
										<asp:Label id="lblparent" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MemberID")%>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
						<div style="float:right">
                            <table style="text-align:right">
                                <tr><td>Total Fee:</td><td> <asp:Label ID="lblTotalAmount" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label></td></tr>

                                <tr runat="server" id="trDiscount"><td style="text-align:right">Discount:</td><td> <asp:Label ID="lblDiscount" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label></td></tr>
                                <tr runat="server" id="trDonation"><td style="text-align:right">Donation:</td><td> <asp:Label ID="lblDonation" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label></td></tr>
                                <tr><td style="text-align:right">Total amount to be Paid:</td><td> <asp:Label ID="lblTotalPaidAmount" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label></td></tr>
                            </table>
                            <asp:Label ID="Lblsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label>


						</div>
						</td>
				</tr>
				<tr id="TrChkConf" runat="server" visible="false">
				    <td>
			             <asp:CheckBox ID="CheckConfMsg" runat="server"  ForeColor="Red"  Font-Bold="True" Text ="I see a conflict due to multiple locations on the same date, including Paid transactions if any. I agree to resolve it." />
			        </td>
			        </tr>
				<tr id="TrChkChapConf" runat="server" visible="false">
			         <td>
			             <asp:CheckBox ID="CheckChapConf" runat="server"  ForeColor="Red"  Font-Bold="True" Text ="I agree that I have selected location(s) other than my own chapter intentionally." />
			        </td>
			    </tr>
				 
				</tr>
				<tr id="trMeal1" runat="server" >
					<td class="SmallFont" align="center">Meals Information 
					</td>
				</tr>
				<tr id="trmeal2" runat="server"><td class="SmallFont" align="center">
				<asp:datagrid id="dgMealList" runat="server" CssClass="GridStyle" DataKeyField="Name"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Visible="False" >
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name" SortExpression="Name">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
									
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Amount" SortExpression="Amount">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Date"  SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDate") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="MealType"  SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="MealsType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.MealType") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Comments"  SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="LblLunchType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LunchType") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
                                    <HeaderStyle Font-Bold="True" ForeColor="#990000" />
								</asp:TemplateColumn>
									</Columns>
						</asp:datagrid>
				
                    <asp:Label ID="lblmealsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label><br />
                    <asp:Label ID="lblgrandsum" runat="server" Font-Size="Small" ForeColor="DarkRed" Font-Bold="True"></asp:Label>
					</td>
				</tr>
								
				
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
				<tr>
				<td align="center" ><asp:Label ID ="lblErrorMsg" runat="server" ForeColor="Red"></asp:Label><br /></td>
				</tr>
				
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="MidnightBlue" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo1" runat="server" CssClass="SmallFont" ForeColor="MidnightBlue" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestDateInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
				
			</table>
			
    <asp:Label ID="lbltest" runat="server"></asp:Label>
</asp:Content>



 
 
 