
if exists( select * from sysobjects where name = 'usp_GetUserLoginID' and xtype='p' )
	drop procedure usp_GetUserLoginID
go

create procedure usp_GetUserLoginID @Email varchar(50), @Password varchar(50)
as
begin
	select login_id
	from login_master lm
	where lm.user_email = @Email
	and lm.user_pwd = @Password
end
go

if exists( select * from sysobjects where name = 'usp_GetIndSpouseID' and xtype='p' )
	drop procedure usp_GetIndSpouseID
go

create procedure usp_GetIndSpouseID @Email varchar(50)
as
begin
	select AutoMemberID
	from IndSpouse i
	where i.email = @Email
end
go


if exists( select * from sysobjects where name = 'usp_GetIndSpouseChapterID' and xtype='p' )
	drop procedure usp_GetIndSpouseChapterID
go

create procedure usp_GetIndSpouseChapterID @autoMemberId int
as
begin
	select chapterId
	from IndSpouse i
	where i.automemberid = @automemberid
end
go


if exists( select * from sysobjects where name = 'usp_GetVolunteerMemberID' and xtype='p' )
	drop procedure usp_GetVolunteerMemberID
go

create procedure usp_GetVolunteerMemberID @AutoMemberID int
as
begin
	select memberID
	from Volunteer v
	where v.memberID = @AutoMemberID
end
go




if exists( select * from sysobjects where name = 'usp_GetVolunteerRoleID' and xtype='p' )
	drop procedure usp_GetVolunteerRoleID
go

create procedure usp_GetVolunteerRoleID @volunteerid int
as
begin
	select min(roleid)
	from Volunteer v
	where v.volunteerid = @volunteerid	
end
go



if exists( select * from sysobjects where name = 'usp_GetVolunteerChapterID' and xtype='p' )
	drop procedure usp_GetVolunteerChapterID
go

create procedure usp_GetVolunteerChapterID @volunteerid int
as
begin
	select chapterid
	from Volunteer v
	where v.volunteerid = @volunteerid
end
go


if exists( select * from sysobjects where name = 'usp_AddEmailChangeRequest' and xtype='p' )
	drop procedure usp_AddEmailChangeRequest
go

create procedure usp_AddEmailChangeRequest @LoginID int, @NewEmail nvarchar(50)
as
begin
	update login_master
	set NewEmail = @NewEmail,
	RequestFlag = 'Yes',
	DateRequested = getdate()
	where login_id = @LoginID
end
go

if exists( select * from sysobjects where name = 'usp_IsDuplicateEmail' and xtype='p' )
	drop procedure usp_IsDuplicateEmail
go

create procedure usp_IsDuplicateEmail @NewEmail nvarchar(50)
as
begin
	if exists ( select * 
				from login_master
				where user_email = @NewEmail )
		select 'True'
	else
		select 'False'
	
end
go



if exists( select * from sysobjects where name = 'usp_GetIndAndSpouseRecords' and xtype='p' )
	drop procedure usp_GetIndAndSpouseRecords
go
create procedure usp_GetIndAndSpouseRecords( @autoMemberID int)
as
begin
		--returns both Ind record and Spouse record.
		select automemberID,
				Gender,
				DonorType,
				Relationship
		from IndSpouse 
		where automemberId =@autoMemberID
		or Relationship = @autoMemberID
end		
go	

if exists( select * from sysobjects where name = 'usp_GetChapters' and xtype='p' )
	drop procedure usp_GetChapters
go

create procedure usp_GetChapters 
as
begin
	select ChapterID,ChapterCode,Name from Chapter order by state asc
end
go

if exists( select * from sysobjects where name = 'usp_IsRegional' and xtype='p' )
	drop procedure usp_IsRegional
go

create procedure usp_IsRegional
as
begin
	declare @status as varchar(2)
	select @status = status
	from EVENT
	where eventid = 1

	if( @status = 'O')  --'O' for open
		select 'false'
	else
		select 'true'
end

go


if exists( select * from sysobjects where name = 'usp_GetOpenEvent' and xtype='p' )
	drop procedure usp_GetOpenEvent
go

create procedure usp_GetOpenEvent
as
begin
	select eventid
	from EVENT
	where eventid in (1,2)
	and lower(status) = 'o'
end
go



if exists( select * from sysobjects where name = 'usp_GetEventCodeByID' and xtype='p' )
	drop procedure usp_GetEventCodeByID
go

create procedure usp_GetEventCodeByID @EventID int
as
begin
	select eventcode
	from Event
	where eventid = @EventID
end
go


if exists( select * from sysobjects where name = 'usp_GetEvents' and xtype='p' )
	drop procedure usp_GetEvents
go

create procedure usp_GetEvents
as
begin
	select EventID,EventCode from Event order by EventCode
end
go


if exists( select * from sysobjects where name = 'usp_GetRoles' and xtype='p' )
	drop procedure usp_GetRoles
go

create procedure usp_GetRoles
as
begin
	select RoleID,RoleCode from Role order by RoleCode
end
go




if exists( select * from sysobjects where name = 'usp_GetZones' and xtype='p' )
	drop procedure usp_GetZones
go

create procedure usp_Getzones
as
begin
	select ZoneID ,ZoneCode+' - ['+Description +']' as ZoneCode 
	from Zone
	where status like 'A%'
	order by ZoneCode
end
go


if exists( select * from sysobjects where name = 'usp_GetClusters' and xtype='p' )
	drop procedure usp_GetClusters
go

create procedure usp_GetClusters
as
begin
	select ClusterID, ClusterCode+' - ['+Description +']'as ClusterCode 
	from Cluster 
	where status like 'A%'
	order by ClusterCode
end
go


if exists( select * from sysobjects where name = 'getContestCoordinatorInfo' and xtype in (N'FN', N'IF', N'TF') )
	drop function getContestCoordinatorInfo
go

create function getContestCoordinatorInfo( @contestChapterid int) returns varchar(500)
as
begin
	declare @name varchar(500)
	declare @c int
	set @c=0
	set @name=''

	select @c=count(*)
	from volunteer v
	where v.rolecode = 'chapterc'	
	and v.chapterid = @contestChapterid

	if (@c > 1)
	begin
		 select @name = firstname +' '+ lastname+ '<br>'+ email +'<br>' + HPhone
		 from volunteer v,indspouse i
		 where v.rolecode = 'chapterc'	
		 and v.teamlead='y'
		 and i.automemberid=v.memberid
		 and v.chapterid = @contestChapterid
	end
	else
	begin
		 select @name = firstname +' '+ lastname+ '<br>'+ email +'<br>' + HPhone
		 from volunteer v,indspouse i
		 where v.rolecode = 'chapterc'	
		 and i.automemberid=v.memberid
		 and v.chapterid = @contestChapterid
	end

	return @name
end
go

--exec usp_GetRegionalContests_test 12518,2007, '0C9F4064-45A28F3D-902-0ACF0'
if exists( select * from sysobjects where name = 'usp_GetRegionalContests' and xtype='p' )
	drop procedure usp_GetRegionalContests
go

create procedure usp_GetRegionalContests 
@ParentID	int,
@ContestYear	int,
@PaymentReference varchar(100)
AS
select	ch.First_Name +' ' +ch.Last_Name as 'ContestantName',
		cc.contestDesc	,
		ct.building as 'Building',
		chap.city as 'ChapterCity',
		chap.State as 'ChapterState',
		c.childNumber as 'ChildNumber',
		c.contestid  as 'ContestId',
		c.contestcode as 'ContestCode',
		dbo.getContestCoordinatorInfo( c.chapterid) as 'coordinatorName',
	    Coalesce(Convert(varchar(20),ct.ContestDate,107),'To be Announced') as ContestDate,
	    ContestTime = case ct.ContestDate when null then '' else  ct.CheckinTime End  ,
		c.Fee as 'Fee',
		c.PaymentDate as 'PaymentDate',
		c.paymentReference as 'PaymentReference',
		c.paymentMode as 'PaymentMode'
from contestant c, contestcategory cc,contest ct, child ch,chapter chap
where parentid = @parentid
and c.contestyear = @contestyear
and c.paymentreference = @paymentreference
and c.contestid = ct.contestid
and c.childnumber = ch.childnumber
and c.contestcategoryid = cc.contestcategoryid
and c.chapterid = chap.chapterid

go


if exists( select * from sysobjects where name = 'usp_GetMemberNameByID' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function usp_GetMemberNameByID
go

create function usp_GetMemberNameByID(@memberid int) returns varchar(60)
as
begin
	declare @name varchar(60)
	select @name = firstName + ' '+ LastName
	from IndSpouse i
	where i.automemberid = @memberid

	return @name
end
go

if exists( select * from sysobjects where name = 'usp_SelectWhereVolunteer' and xtype='p' )
	drop procedure usp_SelectWhereVolunteer
go
 
create PROCEDURE usp_SelectWhereVolunteer
	@WhereCondition nvarchar(500)
AS
begin
SET NOCOUNT ON
DECLARE @SQL nvarchar(3250)
SET @SQL = '
SELECT  dbo.usp_getMemberNameByID( memberid) as Name
	  ,[VolunteerId]
      ,v.[MemberId]
      ,v.[RoleId]
      ,v.[RoleCode]
      ,case len(v.TeamLead) when 0 then ''N'' else  v.TeamLead end as TeamLead
      ,v.[EventYear]
      ,v.[EventId]
      ,v.[EventCode]
      ,v.[ChapterId]
      ,v.[ChapterCode]
      ,v.[National]
      ,v.[ZoneId]
      ,v.[ZoneCode]
      ,v.[ClusterId]
      ,v.[ClusterCode]
      ,v.[Finals]
      ,v.[IndiaChapter]
      ,v.[ProductGroupID]
      ,v.[ProductGroupCode]
      ,v.[ProductId]
      ,v.[ProductCode]
      ,v.[AgentFlag]
      ,v.[WriteAccess]
      ,v.[Authorization]
      ,v.[Yahoogroup]
      ,v.[IndiaChapterName]
      ,v.[CreateDate]
      ,case len(v.createDate) when 0 then '''' else dbo.usp_getMemberNameByID(v.[CreatedBy]) end as CreatedBy
      ,v.[ModifyDate]
      ,v.[ModifiedBy]
  FROM [Volunteer] v
	Where	' + @WhereCondition +
' order by memberid'

 EXEC sp_executesql @SQL

end 
go


if exists( select * from sysobjects where name = 'usp_GetRolesWhere' and xtype='p' )
	drop procedure usp_GetRolesWhere
go

create procedure usp_GetRolesWhere 
	@wherecondition nvarchar(300)
as
begin
	declare @sql varchar(3000)
set @sql ='select roleid,rolecode from role ' + @whereCondition

 EXEC(@sql)

end
go




if exists( select * from sysobjects where name = 'usp_GetProductGroupByEvent' and xtype='p' )
	drop procedure usp_GetProductGroupByEvent
go

create procedure usp_GetProductGroupByEvent
	@eventId int
as
begin
	select productGroupId, productGroupCode,name
	from productGroup
	where eventId = @eventId	
end
go

if exists( select * from sysobjects where name = 'usp_GetProductByGroupAndEvent' and xtype='p' )
	drop procedure usp_GetProductByGroupAndEvent
go

create procedure usp_GetProductByGroupAndEvent
	@productGroupId int,@eventId int
as
begin
	select productId, productCode,name
	from product
	where eventId = @eventId
	and productGroupId = @productGroupId
end
go