﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using VRegistration;
using System.Data;
using System.Collections;
using System.Drawing;

public partial class TestAnswerKeyTest : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        lblValText.Text = "";
        if (Session["LoggedIn"] == null)
        {
            Server.Transfer("maintest.aspx");
        }

        if (!Page.IsPostBack)
        {
            LoadYear();
            loadPhase();
            LoadProductGroup();
            // LoadDropdown(ddlSetNoTM, 40);
            LoadDropdown(ddlSectionsTM, 10);
            ddlSectionsTM.SelectedValue = "1";
            if ((Session["RoleId"].ToString() == "1") | (Session["RoleId"].ToString() == "2") | (Session["RoleId"].ToString() == "96") | (Session["RoleId"].ToString() == "30"))
            {
                LoadProductGroup();
                string sqltext = "    select distinct P.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlEventYearTM.SelectedValue + " and cs.Accepted = 'Y'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString().ToString(), CommandType.Text, sqltext);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlProductTM.DataSource = ds;
                        ddlProductTM.DataBind();
                        if (ddlProductTM.Items.Count > 1)
                        {
                            ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                            ddlProductTM.Items[0].Selected = true;
                            ddlProductTM.Enabled = true;
                        }
                    }
                }
            }
            else if (Session["RoleId"].ToString() == "88" | Session["RoleId"].ToString() == "89")
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select count (*) from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + "  and ProductId is not Null")) > 1)
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    int i;
                    string prd = string.Empty;
                    string Prdgrp = string.Empty;
                    for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (prd.Length == 0)
                        {
                            prd = ds.Tables[0].Rows[i][1].ToString();
                        }
                        else
                        {
                            prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();
                        }

                        if (Prdgrp.Length == 0)
                        {
                            Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                        }
                        else
                        {
                            Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();
                        }
                    }

                    lblPrd.Text = prd;
                    lblPrdGrp.Text = Prdgrp;
                    LoadProductGroup();
                }
                else
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    string prd = string.Empty;
                    string Prdgrp = string.Empty;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        prd = ds.Tables[0].Rows[0][1].ToString();
                        Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                        lblPrd.Text = prd;
                        lblPrdGrp.Text = Prdgrp;
                    }

                    LoadProductGroup();
                }
            }
        }
    }
    private void LoadYear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlEventYearTM.Items.Add(new ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)));
        for (int i = 0; i <= 8; i++)
        {
            ddlEventYearTM.Items.Add(new ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))));
        }

        ddlEventYearTM.SelectedValue = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    private void LoadSectionNumber()
    {
        for (int i = 0; i <= 5; i++)
        {
            ddlSectionsTM.Items.Insert(0, new ListItem((i + 1).ToString()));
            //ddlSectionNo.Items.Insert(0, i + 1);
        }
    }

    private void LoadProductGroup()
    {
        ddlProductTM.Items.Clear();
        string strSql = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y')  WHERE PG.EventId=" + ddlEvent.SelectedValue + "  order by PG.ProductGroupID";
        SqlDataReader drproductgroup;
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
        ddlProductGroupTM.DataSource = drproductgroup;
        ddlProductGroupTM.DataBind();
        if (ddlProductGroupTM.Items.Count < 1)
        {
            lblValText.Text = "No Product is opened.";
        }
        else if (ddlProductGroupTM.Items.Count > 1)
        {
            ddlProductGroupTM.Items.Insert(0, new ListItem("Select", "0"));
            ddlProductGroupTM.Items[0].Selected = true;
            ddlProductGroupTM.Enabled = true;
        }
        else
        {
            ddlProductGroupTM.Enabled = false;
            LoadProductID();
            LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        }
    }

    private void LoadProductID()
    {
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        if (ddlProductGroupTM.Items[0].Selected == true & ddlProductGroupTM.SelectedItem.Text == "Select Product Group")
        {
            ddlProductTM.Enabled = false;
        }
        else
        {
            string strSql;
            try
            {
                strSql = "Select distinct P.ProductID, P.Name from Product  P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y') where P.EventID=" + ddlEvent.SelectedValue + " and P.ProductGroupID =" + ddlProductGroupTM.SelectedValue + " order by P.ProductID";
                SqlDataReader drproductid;
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
                ddlProductTM.DataSource = drproductid;
                ddlProductTM.DataBind();
                if (ddlProductTM.Items.Count > 1)
                {
                    ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                    ddlProductTM.Items[0].Selected = true;
                    ddlProductTM.Enabled = true;
                }
                else if (ddlProductTM.Items.Count < 1)
                {
                    ddlProductTM.Enabled = false;
                }
                else
                {
                    ddlProductTM.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                //lblErr.Text = ex.ToString;
            }
        }
    }


    protected void ddlProductGroupTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        ddlProductTM.Items.Clear();
        LoadProductID();
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        tblDGAnswerKey.Visible = false;
        tblDGTestSection.Visible = false;
    }

    protected void LoadDropdown(DropDownList ddlObject, int MaxLimit)
    {
        ddlObject.Items.Clear();
        for (int i = 0; i <= MaxLimit - 1; i++)
        {
            ddlObject.Items.Insert(i, Convert.ToString(i + 1));
        }

        if (ddlObject.Items.Count > 1)
        {
            ddlObject.Items.Insert(0, "Select");
        }
    }
    protected void ddlEventYearTMTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        tblDGAnswerKey.Visible = false;
        tblDGTestSection.Visible = false;
    }

    protected void ddlProductTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        string PgId = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductTM.SelectedValue + "").ToString();
        ddlProductGroupTM.SelectedValue = PgId;
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        tblDGAnswerKey.Visible = false;
        tblDGTestSection.Visible = false;
    }


    private void LoadLevel(DropDownList ddlObject, string ProductGroup)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        try
        {
            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" + ddlEventYearTM.SelectedValue + " and ProductGroupID=" + ddlProductGroupTM.SelectedValue + " and ProductID=" + ddlProductTM.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + "";
            SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
            SqlDataReader level;
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText);
            ddlObject.DataSource = level;
            ddlObject.DataValueField = "LevelCode";
            ddlObject.DataTextField = "LevelCode";
            ddlObject.DataBind();
            DataTable dt = new DataTable();
            dt.Load(level);
            if ((ddlObject.Items.Count > 1))
            {
                ddlObject.Items.Insert(0, "Select Level");
            }
            else
            {
                PopulateWeekNo();
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlWeekIDTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSetNoTM.SelectedIndex = ddlWeekIDTM.SelectedIndex;
    }

    protected void ddlLevelTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateWeekNo();
    }

    protected void ddlSemesterTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadProductGroup();
    }

    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {
            ddlSemesterTM.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlSemesterTM.SelectedValue = objCommon.GetDefaultSemester(ddlEventYearTM.SelectedValue);
    }

    protected void PopulateWeekNo()
    {
        string CmdText = "";
        CmdText = " select distinct CP.WeekId, cp.Coachpaperid from  CoachPapers CP where CP.eventyear=" + ddlEventYearTM.SelectedValue + " and CP.ProductGroupId=" + ddlProductGroupTM.SelectedValue + " and CP.ProductId=" + ddlProductTM.SelectedValue + " and CP.level='" + ddlLevelTM.SelectedValue + "' and CP.Semester='" + ddlSemesterTM.SelectedValue + "' and DOCType='Q' order by WeekId ASC";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlWeekIDTM.DataValueField = "CoachPaperId";
            ddlWeekIDTM.DataTextField = "WeekId";
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
            ddlWeekIDTM.Items.Insert(0, new ListItem("Select", "0"));
        }
        else
        {
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
        }
    }

    public string ValidateAnswerKey()
    {
        string retVal = "1";
        if (ddlEventYearTM.SelectedValue == "-1")
        {
            lblValText.Text = "Please select Event Year";
            retVal = "-1";
        }
        else if (ddlSemesterTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlProductGroupTM.SelectedValue == "0" || ddlProductGroupTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProductTM.SelectedValue == "Select Product" || ddlProductTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product";
            retVal = "-1";
        }
        else if (ddlLevelTM.SelectedValue == "Select Level" || ddlLevelTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Level";
            retVal = "-1";
        }
        else if (ddlPaperTypeTM.SelectedValue == "" || ddlPaperTypeTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Papertype";
            retVal = "-1";
        }
        else if (ddlWeekIDTM.SelectedValue == "-1" || ddlWeekIDTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select WeekId";
            retVal = "-1";
        }
        else if (ddlSectionsTM.SelectedValue == "Select" || ddlSectionsTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Sections";
            retVal = "-1";
        }

        return retVal;
    }

    private void LoadGrid_TestSections_AK(int TestSetUpSectionsId)
    {
        lblError.Text = "";
        tblDGTestSection.Visible = true;
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" + TestSetUpSectionsId + "  and SectionNumber=" + ddlSectionsTM.SelectedValue + "  order by SectionNumber");
        if (ds.Tables[0].Rows.Count > 0)
        {
            DGTestSection.DataSource = ds;
            DGTestSection.DataBind();
            lblTestSection.Text = "Test SetUp Sections";
            if ((ds.Tables[0].Rows.Count == 1))
            {
                try
                {
                    Session["CoachPaperID"] = ds.Tables[0].Rows[0]["CoachPaperID"].ToString();
                    Session["SectionNumber"] = ds.Tables[0].Rows[0]["SectionNumber"].ToString();
                    Session["QuestionType"] = ds.Tables[0].Rows[0]["QuestionType"].ToString();
                    if (ds.Tables[0].Rows[0]["QuestionType"].ToString() == "RadioButton")
                    {
                        Session["NoOfChoices"] = (ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString() != "" ? ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString() : "0");
                    }

                    Session["QuestionNumberFrom"] = ds.Tables[0].Rows[0]["QuestionNumberFrom"].ToString();
                    Session["QuestionNumberTo"] = ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString();
                    for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
                    {
                        DGTestSection.Items[i].BackColor = Color.White;
                    }

                    tblDGAnswerKey.Visible = false;
                    LoadAnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
                }
                catch (Exception ex)
                {
                }
            }
        }
        else
        {
            lblTestSection.Text = "No records exists in the Test Setup Sections table.";
            DGTestSection.DataSource = null;
            DGTestSection.DataBind();
        }
    }

    protected void LoadTestSections_AK()
    {
        if ((ValidateAnswerKey() == "1"))
        {
            string CoachPaperID = ddlWeekIDTM.SelectedValue;
            LoadGrid_TestSections_AK(Convert.ToInt32(CoachPaperID));
        }
    }

    protected void DGTestSection_Itemcommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        try
        {
            lblError.Text = "";
            Session["CoachPaperID"] = e.Item.Cells[3].Text;
            Session["SectionNumber"] = e.Item.Cells[4].Text;
            Session["Level"] = e.Item.Cells[5].Text;
            Session["QuestionType"] = e.Item.Cells[9].Text;
            if ((string)e.Item.Cells[9].Text == "RadioButton")
            {
                Session["NoOfChoices"] = (e.Item.Cells[10].Text != "" ? e.Item.Cells[10].Text : "0");
            }

            Session["QuestionNumberFrom"] = e.Item.Cells[11].Text;
            Session["QuestionNumberTo"] = e.Item.Cells[12].Text;
            for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
            {
                DGTestSection.Items[i].BackColor = Color.White;
            }

            if (e.CommandName == "Select")
            {
                tblDGAnswerKey.Visible = false;
                e.Item.BackColor = Color.Gainsboro;
                LoadAnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadAnswerKey(int CoachPaperID, int SectionNumber, string Level)
    {
        try
        {
            string StrSQLInsert = "";
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["Connectionstring"].ToString(), CommandType.Text, "Select Count(*) from TestAnswerKey where EventYear=" + ddlEventYearTM.SelectedValue + "and  CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"])) > 0)
            {
                LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level);
            }
            else
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumberFrom=" + Session["QuestionNumberFrom"] + " and QuestionNumberTo=" + Session["QuestionNumberTo"]);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    StrSQLInsert = StrSQLInsert + "Insert into TestAnswerKey (EventYear,CoachPaperID,SectionNumber,Level,QuestionNumber,CreatedBy,CreateDate) Values";
                    int Count = Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberTo"]) - Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberFrom"]);
                    for (int i = 0; i <= Count; i++)
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberFrom"]) + i <= Convert.ToInt32(ds.Tables[0].Rows[0]["QuestionNumberTo"]))
                        {
                            StrSQLInsert = StrSQLInsert + "(" + ddlEventYearTM.SelectedValue + "," + CoachPaperID + "," + SectionNumber + ",'" + Level + "', " + ds.Tables[0].Rows[0]["QuestionNumberFrom"] + i + "," + Session["LoginID"] + ",GETDATE()),";
                        }
                    }

                    StrSQLInsert = StrSQLInsert.TrimEnd(',');
                    if (Convert.ToInt32(SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, StrSQLInsert)) > 0)
                    {
                        LoadGrid_AnswerKey(CoachPaperID, SectionNumber, Level);
                    }
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadGrid_AnswerKey(int CoachPaperID, int SectionNumber, string Level)
    {
        try
        {
            tblDGAnswerKey.Visible = true;
            string CmdText = string.Empty;
            CmdText = "Select AnswerKeyRecID,CoachPaperID,SectionNumber,Level,QuestionNumber,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then CorrectAnswer End as CorrectAnswer_DD,Case When Correctanswer not in ('A','B','C','D','E','F','G','H','I','J') then IsNull(Correctanswer,'') end as CorrectAnswer,IsNull(DifficultyLevel,'') as DifficultyLevel,Case When Correctanswer in ('A','B','C','D','E','F','G','H','I','J') then 'RadioButton' Else 'TextArea'end  as QuestionType,IsNull(Manual,'') as Manual from TestAnswerKey where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + CoachPaperID + " and SectionNumber=" + SectionNumber + " and Level='" + Level + "' and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"] + " order by QuestionNumber";
            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables[0].Rows.Count > 0)
            {
                tblDGAnswerKey.Visible = true;
                dgAnswerKey.DataSource = ds;
                dgAnswerKey.DataBind();
                tblDGAnswerKey.Visible = true;
                foreach (DataGridItem row in dgAnswerKey.Items)
                {
                    try
                    {
                        if ((Session["RoleID"] == null))
                        {
                        }
                        else
                        {
                            if ((Session["RoleID"].ToString() == "88"))
                            {
                                DropDownList ddlCorrectAnswer;
                                ddlCorrectAnswer = (DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer");
                                ddlCorrectAnswer.Enabled = false;
                                DropDownList ddlDiffLevel;
                                ddlDiffLevel = (DropDownList)row.Cells[6].FindControl("ddlDiffLevel");
                                ddlDiffLevel.Enabled = false;
                                DropDownList ddlManual;
                                ddlManual = (DropDownList)row.Cells[6].FindControl("ddlManual");
                                ddlManual.Enabled = false;
                                btnSaveAnswers.Enabled = false;
                                btnCancel.Enabled = false;
                            }
                        }
                    }
                    catch (Exception ex)
                    {
                    }
                }

                lblAnswerKey.Text = "Test Answer Key";
                if (Session["QuestionType"] == "RadioButton")
                {
                    foreach (DataGridItem row in dgAnswerKey.Items)
                    {
                        row.Cells[5].FindControl("ddlCorrectAnswer").Visible = true;
                        row.Cells[5].FindControl("txtCorrectAnswer").Visible = false;
                        DropDownList ddlChoice;
                        ddlChoice = (DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer");
                        if (Convert.ToInt32(Session["NoOfChoices"]) + 1 < ddlChoice.Items.Count)
                        {
                            for (int i = Convert.ToInt32(Session["NoOfChoices"]) + 1; i <= ddlChoice.Items.Count - 1; i++)
                            {
                                ddlChoice.Items[i].Enabled = false;
                            }
                        }
                    }
                }
                else
                {
                    foreach (DataGridItem row in dgAnswerKey.Items)
                    {
                        row.Cells[5].FindControl("ddlCorrectAnswer").Visible = false;
                        row.Cells[5].FindControl("txtCorrectAnswer").Visible = true;
                    }
                }
            }
            else
            {
                tblDGAnswerKey.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void dgAnswerKey_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        TextBox txtTemp = (TextBox)e.Item.FindControl("ProjectDD");


    }
    private void LoadChoices(DropDownList ddlObject, int Choices)
    {
        Choices = Choices + 65;
        for (int i = 65; i <= Choices - 1; i++)
        {
            ddlObject.Items.Add(Convert.ToChar(i).ToString());
        }

        ddlObject.Items.Insert(0, "Select");
    }
    protected void dgAnswerKey_PageIndexChanged(object source, System.Web.UI.WebControls.DataGridPageChangedEventArgs e)
    {
        lblError.Text = "";
        dgAnswerKey.Visible = true;
        dgAnswerKey.CurrentPageIndex = e.NewPageIndex;
        LoadGrid_AnswerKey(Convert.ToInt32(Session["CoachPaperID"]), Convert.ToInt32(Session["SectionNumber"]), Session["Level"].ToString());
    }
    public void txtCorrectAnswer_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool txtflag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp;
                    txtTemp = (TextBox)sender;
                    txtTemp.Text = CorrectAnswer;
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlCorrectAnswer_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool ddlFlag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp = (TextBox)sender;

                    txtTemp.Text = CorrectAnswer;
                }
                else
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    System.Web.UI.WebControls.DropDownList ddlTemp;
                    ddlTemp = (DropDownList)sender;
                    LoadChoices(ddlTemp, Convert.ToInt32(Session["NoOfChoices"]));
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((CorrectAnswer == "" ? "Select" : CorrectAnswer)));
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlDiffLevel_PreRender(object sender, System.EventArgs e)
    {
        try
        {
            string DifficultyLevel = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                DifficultyLevel = dr["DifficultyLevel"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((DifficultyLevel == "" ? "Select" : dr["DifficultyLevel"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void ddlManual_Prerender(object sender, System.EventArgs e)
    {
        try
        {
            string Manual = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                Manual = dr["Manual"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((Manual == "" ? "" : dr["Manual"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }


    protected void btnSaveAnswers_Click(object sender, System.EventArgs e)
    {
        try
        {
            string StrUpdate = "";
            string CorrectAnswer = "";
            string DiffLevel = "";
            string Manual = "";
            lblError.Text = "";
            foreach (DataGridItem row in dgAnswerKey.Items)
            {
                string s = ((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).SelectedValue;
                CorrectAnswer = (((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).Visible == true ? ((DropDownList)row.Cells[5].FindControl("ddlCorrectAnswer") as DropDownList).SelectedValue : ((TextBox)row.Cells[5].FindControl("txtCorrectAnswer") as TextBox).Text);

                DiffLevel = ((DropDownList)row.Cells[6].FindControl("ddlDiffLevel") as DropDownList).SelectedValue.Trim();
                Manual = ((DropDownList)row.Cells[6].FindControl("ddlManual") as DropDownList).SelectedValue.Trim();

                StrUpdate = StrUpdate + " Update TestanswerKey Set CorrectAnswer=" + (CorrectAnswer == "" ? "NULL" : "'" + CorrectAnswer + "'") + ",DifficultyLevel=" + (DiffLevel == "" ? "NULL" : "'" + DiffLevel + "'") + ",Manual = " + (Manual == "" ? "NULL" : "'" + Manual + "'") + ",ModifyDate=GETDATE(),ModifiedBy=" + Session["LoginID"] + " Where AnswerKeyRecID=" + row.Cells[0].Text + " and EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + row.Cells[1].Text + " and SectionNumber=" + row.Cells[2].Text + " and Level='" + row.Cells[3].Text + "' and QuestionNumber=" + row.Cells[4].Text + "";
            }
            if ((SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, StrUpdate) > 0))
            {
                lblError.Text = " Answer Keys Updated Successfully.";
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlEventYearTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
    }
    protected void btnSubmit_Click(object sender, System.EventArgs e)
    {
        Clear();
        tblDGAnswerKey.Visible = false;
        tblDGCoachPaper.Visible = false;
        LoadTestSections_AK();
    }
    protected void btnCancelPage_Click(object sender, System.EventArgs e)
    {
        lblError.Text = "";
        tblDGAnswerKey.Visible = false;
        tblDGTestSection.Visible = false;
        tblDGCoachPaper.Visible = false;
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        lblError.Text = "";
        DGTestSection.SelectedIndex = -1;
        tblDGAnswerKey.Visible = false;
    }

    private void Clear()
    {
        lblTestSection.Text = "";
        DGTestSection.SelectedIndex = -1;
        DGCoachPapers.SelectedIndex = -1;
        lblError.Text = "";
    }

    protected void btnManualAll_Click(object sender, EventArgs e)
    {
        try
        {
            if (Session["CoachPaperID"] != null)
            {
                SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, "Update TestAnswerKey SET Manual='Y' where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID =" + Session["CoachPaperID"] + " and SectionNumber=" + Session["SectionNumber"] + "and Level='" + Session["Level"] + "'and QuestionNumber between " + Session["QuestionNumberFrom"] + " and " + Session["QuestionNumberTo"]);
                lblError.Text = " Updated Successfully. Now a coach can provide score for their students";
                LoadGrid_AnswerKey(Convert.ToInt32(Session["CoachPaperID"].ToString()), Convert.ToInt32(Session["SectionNumber"].ToString()), Session["Level"].ToString());
            }
        }
        catch (Exception ex)
        {
            lblError.Text = "Getting problem while updating manual for all AnswerKeys";
        }
    }

    protected void ddlCorrectAnswer_PreRender1(object sender, EventArgs e)
    {
        try
        {
            string CorrectAnswer = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            bool ddlFlag = false;
            if ((dr != null))
            {
                if (dr["QuestionType"] == "TextArea")
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    TextBox txtTemp = (TextBox)sender;

                    txtTemp.Text = CorrectAnswer;
                }
                else
                {
                    CorrectAnswer = dr["CorrectAnswer"].ToString();
                    System.Web.UI.WebControls.DropDownList ddlTemp;
                    ddlTemp = (DropDownList)sender;
                    LoadChoices(ddlTemp, Convert.ToInt32(Session["NoOfChoices"]));
                    ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((CorrectAnswer == "" ? "Select" : CorrectAnswer)));
                }
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlDiffLevel_PreRender1(object sender, EventArgs e)
    {
        try
        {
            string DifficultyLevel = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                DifficultyLevel = dr["DifficultyLevel"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((DifficultyLevel == "" ? "Select" : dr["DifficultyLevel"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlManual_PreRender(object sender, EventArgs e)
    {
        try
        {
            string Manual = "";
            DataRow dr = ((DataRow)(ViewState["editRow"]));
            if ((dr != null))
            {
                Manual = dr["Manual"].ToString();
                System.Web.UI.WebControls.DropDownList ddlTemp;
                ddlTemp = (DropDownList)sender;
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText((Manual == "" ? "" : dr["Manual"].ToString())));
            }
            else
            {
                return;
            }
        }
        catch (Exception ex)
        {
        }
    }
}