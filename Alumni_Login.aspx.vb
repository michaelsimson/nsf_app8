﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports Microsoft.ApplicationBlocks.Data
Partial Class Alumni_Login
    Inherits System.Web.UI.Page
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsValid Then
            Return
            Exit Sub
        Else
            Dim SQLStr As String
            Try
                SQLStr = "select AlumniMemberid from Ind_Alumini_PersDetails where Email='" & txtPrimaryEmailInd.Text & "' and Password='" & txtPwd.Text & "'"
                Dim i As Integer = 0
                i = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLStr)
                If i = 0 Then
                    lblErr.Text = "Invalid Email/Password"
                Else
                    Session("AlumniMemberID") = i.ToString()
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from Ind_Alumini_ParentDetails where AlumniMemberid=" & i & "") < 1 Then
                        Response.Redirect("Alumni_Parent_Form.aspx")
                    ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from Ind_Alumini_EducationDetails where AlumniMemberid=" & i & "") < 1 Then
                        Response.Redirect("Alumni_Education_Form.aspx")
                    ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from ind_Alumini_WorkInformation where AlumniMemberid=" & i & "") < 1 Then
                        Response.Redirect("Alumni_WorkInformation.aspx")
                    End If
                    Response.Redirect("Alumni_MemView.aspx")
                End If
            Catch ex As Exception
                ' lblErr.Text = SQLStr.ToString() & "<br>" & ex.ToString()
                lblErr.Text = "Invalid Email/Password"
            End Try
        End If
    End Sub
    Private Sub clear()
        txtPrimaryEmailInd.Text = String.Empty
        txtPwd.Text = String.Empty
    End Sub

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub
End Class
