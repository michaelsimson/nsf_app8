<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="fetchreg.aspx.vb" Inherits="fetchreg" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
       <table class="withBottomBorder" cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
     <tr bgcolor="#FFFFFF" >
        <td colspan="3" class="Heading">View Registration Data</td>
     </tr>
     </table>
     <br />
     <br />
    <%--'<table border="1" width="100%" >--%> 
  <asp:Repeater id="Rptfetch" runat="server" >
 <%-- <HeaderTemplate>
  <table border="1">
  </HeaderTemplate>--%>
      <ItemTemplate>
      <table border="1">
      <tr>
     <td style="background-color:#CCCC99"> Date Registered </td>
     <td> <%#DataBinder.Eval(Container.DataItem, "date_added")%> </td>
     <td style="background-color:#CCCC99"> Badge Number </td>
     <td> <%#DataBinder.Eval(Container.DataItem, "BadgeNumber")%> </td>
     </tr>
       <tr>
      <td style="background-color:#CCCC99"> Contest </td>
      <td><%#DataBinder.Eval(Container.DataItem, "register_for")%> </td>
      <td style="background-color:#CCCC99">Center</td>
      <td><%#DataBinder.Eval(Container.DataItem, "Center")%> </td>
      </tr>
      <tr>
      <td style="background-color:#CCCC99">Child Name</td>
      <td><%#DataBinder.Eval(Container.DataItem, "Participant Name")%> </td>
      <td style="background-color:#CCCC99">Phone</td>
      <td><%#DataBinder.Eval(Container.DataItem, "Phone")%> </td>
      </tr>
      <tr>
      <td style="background-color:#CCCC99"> Parent's Name </td>
      <td><%#DataBinder.Eval(Container.DataItem, "Parent Name")%></td>
      <td style="background-color:#CCCC99">e-mail </td>
      <td><%#DataBinder.Eval(Container.DataItem, "E-Mail")%></td>
      </tr>
      <tr>
      <td style="background-color:#CCCC99">Address </td>
      <td><%#DataBinder.Eval(Container.DataItem, "address")%></td>
      <td style="background-color:#CCCC99">Spouse Name </td>
      <td><%#DataBinder.Eval(Container.DataItem, "SpouseName")%></td>
      </tr>
      <tr>
      <td style="background-color:#CCCC99">City, State, Zip</td>
      <td><%#DataBinder.Eval(Container.DataItem, "State")%></td>
      <td style="background-color:#CCCC99">AutomemberID</td>
      <td><%#DataBinder.Eval(Container.DataItem, "AutomemberID")%></td>
      </tr>
      </table>
  </ItemTemplate>
 <%--<FooterTemplate>
 </table>
 </FooterTemplate>--%>
  </asp:Repeater>
  <br />
  
    <asp:Button ID="Btnback" runat="server" Text="Back" />
  
</asp:Content>

