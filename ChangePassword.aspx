<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.ChangePassword" CodeFile="ChangePassword.aspx.vb" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
 <div align="left">
        <asp:hyperlink id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink> 
    </div>
   <div style="text-align: center; width :1004px">  
    <table width="1004px" border="0" cellpadding ="3" cellspacing = "0">
				<tr><td align="center" >
			<table width="600px" border="0" cellpadding ="3" cellspacing = "0">
				<tr>
					<td class="ContentSubTitle" noWrap align="center" width="25%" colSpan="2">
						<h1 align="center">Change Password</h1>
					</td>
				</tr>
				<tr>
				    <td align="right">Enter Email Address:</td>
				    <td align="left"><asp:TextBox runat="server" ID="txtEmail"></asp:TextBox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ErrorMessage="Enter Valid Email Address" ControlToValidate="txtEmail"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td  align="right">Old Password:</td>
					<td align="left"><asp:textbox id="txtOldPassword" runat="server" CssClass="smFont" TextMode="Password"></asp:textbox><asp:requiredfieldvalidator id="rfvOldpassword" runat="server" Display="Dynamic" ErrorMessage="Enter Old Password"
							ControlToValidate="txtOldPassword"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td  noWrap align="right">&nbsp;New Password :</td>
					<td align="left"><asp:textbox id="txtPassword" runat="server" CssClass="smFont" TextMode="Password"></asp:textbox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Valid New Password" ControlToValidate="txtPassword"></asp:RequiredFieldValidator></td>
				</tr>
				<tr>
					<td  noWrap align="right">&nbsp;Confirm New Password :</td>
					<td align="left"><asp:textbox id="txtConfirmPassword" runat="server" CssClass="smFont" TextMode="Password"></asp:textbox>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"  ControlToValidate="txtConfirmPassword" ErrorMessage="Enter Valid Confirm New Password"></asp:RequiredFieldValidator>
				<asp:CompareValidator ID="CompareValidator1" runat="server" ControlToCompare="txtPassword" ControlToValidate="txtConfirmPassword" ErrorMessage="New Password and Confirm Password are not matching"></asp:CompareValidator>
                        </td>
				</tr>
				<tr>
					<td align="center" colSpan="2" style="HEIGHT: 15px"><asp:button id="btnLogin" runat="server" CssClass="FormButton" Text="Update"></asp:button></td>
				</tr>
				
			</table>
			&nbsp;<asp:label id="txtErrMsg" runat="server" ForeColor="red" Visible="False"> Invalid Email Address OR Old Password.</asp:label><br>

			</td></tr> </table> 
		
    </div>
</asp:Content>
 

 
 
 