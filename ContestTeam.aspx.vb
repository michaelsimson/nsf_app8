﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class ContestTeam
    Inherits System.Web.UI.Page
    Dim dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("selChapterID") = 1
        'Session("LoginID") = 4240
        'Session("selChapterName") = "Finals, US"
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        'If (Session("RoleID") = 1 Or Session("RoleID") = 2) Then
        ''To Add Values to DropDownList

        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            If Convert.ToInt32(Session("selChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("selChapterID")
                ddlChapter.SelectedItem.Text = Session("selChapterName").ToString()
                ddlChapter.Enabled = False
                If (ddlChapter.SelectedValue = "1") Then
                    ddlEvent.SelectedValue = "1"
                Else
                    ddlEvent.SelectedValue = "2"
                End If
                ddlEvent.Enabled = False
                'GetProductGroup(ddlProductGroup)
                GetDate()
                ' LoadBldgID()
            End If
            ' GetProductGroup(ddlProductGroup)
            ' GetDate()
            ' LoadDisplayTime()
        End If
        ' End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        'ddlPurpose.DataTextField = "Purpose"
        'ddlPurpose.DataValueField = "Purpose"
        ddlPurpose.DataBind()
        ' ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 3
        ddlPurpose.Enabled = False
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        'ddlChapter.DataTextField = "ChapterCode"
        'ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        Try
            lblErr.Text = ""
            'Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedValue & "'"

            'Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='2015-08-29'"

            Dim StrSQl = "select distinct(ProductGroupId), ProductGroupCode from contest where Contest_Year=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and NSFChapterID=" & ddlChapter.SelectedValue & " and ContestDate = '" & ddlDate.SelectedValue & "'"


            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlObject.DataSource = ds
                'ddlProductGroup.DataTextField = "ProductGroup"
                'ddlProductGroup.DataValueField = "ProductGroupId"
                ddlObject.DataBind()

                ddlObject.Items.Insert(0, "Select ProductGroup")
                ddlObject.SelectedIndex = 0
            Else
                lblErr.Text = "No Product Groups Present"
            End If

        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub GetDate()
        Try
            lblErr.Text = ""
            Dim StrDateSQL As String = ""
            StrDateSQL = "Select Distinct  convert(VarChar(10), ContestDate, 101) as Date,ContestDate  From  contest Where Contest_Year=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and nsfChapterID=" & ddlChapter.SelectedValue & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlDate.DataSource = ds
                ddlDate.DataTextField = "Date"
                ddlDate.DataValueField = "ContestDate"
                ddlDate.DataBind()

                ddlDate.Items.Insert(0, New ListItem("Select ContestDate", "-1"))
                ddlDate.SelectedIndex = 0
            Else
                lblErr.Text = "No Contest Dates Present for the Selections"
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Sub LoadDisplayTime()
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "6:00:00 AM " To "8:00:00 PM "
        While StartTime <= "8:00:00 PM "

            dr = dt.NewRow()
            dr("ddlText") = StartTime.ToString("hh:mm tt")
            dr("ddlValue") = StartTime.ToString("hh:mm tt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddMinutes(15)
        End While

        'ddlStartTime.DataSource = dt
        'ddlStartTime.DataTextField = "ddlText"
        'ddlStartTime.DataValueField = "ddlValue"
        'ddlStartTime.DataBind()

        'ddlStartTime.Items.Insert(0, "Select Start time")
        'ddlStartTime.SelectedIndex = 0

        'ddlEndTime.DataSource = dt
        'ddlEndTime.DataTextField = "ddlText"
        'ddlEndTime.DataValueField = "ddlValue"
        'ddlEndTime.DataBind()
        'ddlEndTime.Items.Insert(0, "Select End time")
        'ddlEndTime.SelectedIndex = 0

    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            ddlPurpose.SelectedIndex = 3
            ddlPurpose.Enabled = False
            ClearDropDowns(True)
            LoadRolesVol()
            'LoadBldgID()
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        'GetProductGroup(ddlProductGroup)
        GetDate()
        ddlPurpose.SelectedIndex = 3
        ddlPurpose.Enabled = False
        ClearDropDowns(True)
        LoadRolesVol()
        'LoadBldgID()
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDropDowns(True)
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event/Chapter"
            Exit Sub
        End If

        LoadRoomSchData()
    End Sub
    Private Sub LoadRoomSchData()
        Dim SQLRoomsch As String = "Select * from Roomschedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterId=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomsch)
        Try
            GetDate()
            'GetProductGroup(ddlProductGroup) '
            'LoadBldgID()
            lblAddUPdate.Text = ""
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        ClearDropDowns(False)
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        LoadProduct(ddlProductGroup, ddlProduct)
        ' LoadPhase()

    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        LoadPhase()
    End Sub
    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ddlProductObj As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlObject.SelectedValue & " order by ProductId" 'and BldgId='" & ddlBldgID.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlProductObj.DataSource = ds
        'ddlProductObj.DataTextField = "ProductCode"
        'ddlProductObj.DataValueField = "ProductId"
        ddlProductObj.DataBind()

        ddlProductObj.Items.Insert(0, New ListItem("Select Product ", "0"))
        ddlProductObj.SelectedIndex = 0

    End Sub

    Private Sub LoadPhase()

        Dim StrSQL As String = "Select Distinct Phase from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlPhase.DataSource = ds
        'ddlPhase.DataTextField = "Phase"
        'ddlPhase.DataValueField = "Phase"
        ddlPhase.DataBind()

        ddlPhase.Items.Insert(0, New ListItem("Select Phase ", "0"))
        ddlPhase.SelectedIndex = 0

        LoadDGRoomSchedule()

    End Sub

    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        TrCopy.Visible = False
        ddlRoomNo.Enabled = True
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
        ClearRoles()
        LoadRolesVol()
        'LoadRoleData()
    End Sub
    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlSeqNo.DataSource = ds
        ddlSeqNo.DataBind()

        ddlSeqNo.Items.Insert(0, "Select SeqNo")
        ddlSeqNo.SelectedIndex = 0

        ddlRoomNo.DataSource = ds
        ddlRoomNo.DataBind()

        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0

        'GetProductGroup()
        'GetDate()
    End Sub

    Protected Sub ddlSeqNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSeqNo.SelectedIndexChanged
        Try
            lblAddUPdate.Text = ""
            TrCopy.Visible = False

            Dim StrSeqNoSQl As String = "Select Distinct RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and SeqNo='" & ddlSeqNo.SelectedItem.Text & "'"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSeqNoSQl)

            If ds.Tables(0).Rows.Count > 0 Then
                'ddlRoomNo.Enabled = True
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber")))
                'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
            Else
                'ddlRoomNo.Enabled = False
                ddlRoomNo.SelectedIndex = 0
                'ddlStartTime.SelectedIndex = 0
                'ddlEndTime.SelectedIndex = 0
                lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
            End If
            ClearRoles()
            LoadRolesVol()
            LoadRoleData()
            LoadDGRoomSchedule()
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged

        lblAddUPdate.Text = ""
        TrCopy.Visible = False

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and RTRIM(LTRIM(Purpose))='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and RTRIM(LTRIM(BldgId))='" & ddlBldgID.SelectedItem.Text.Trim() & "' and RTRIM(LTRIM(RoomNumber))='" & ddlRoomNo.SelectedItem.Text.Trim() & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            'ddlSeqNo.Enabled = True
            ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))
            'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
            'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
        Else
            'ddlSeqNo.Enabled = False
            ddlSeqNo.SelectedIndex = 0
            'ddlStartTime.SelectedIndex = 0
            'ddlEndTime.SelectedIndex = 0
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
        End If

        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadBldgID()
        ddlRoomNo.Enabled = True

        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""
        lblErr.Text = ""
        'StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedValue & "'and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""

        StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""


        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        ' ddlRoomNo.Items.Clear()
        If ds.Tables(0).Rows.Count > 0 Then
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)
            ddlBldgID.DataSource = ds
            ddlBldgID.DataTextField = "BldgID"
            ddlBldgID.DataValueField = "BldgID"
            ddlBldgID.DataBind()
        Else
            lblErr.Text = "No Buildings assigned"
        End If
        ddlBldgID.Items.Insert(0, "Select BldgID")
        ddlBldgID.SelectedIndex = 0
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        lblAddUPdate.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Or ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please do all the Selections"
            Exit Sub
        End If
        LoadBldgID()
        LoadTimeData()
        ' LoadRoleData()
        CheckRoomReqData()
        LoadDGRoomSchedule()
        LoadRolesVol()
    End Sub
    Private Sub LoadTimeData()
        Dim SQLContestTime As String = "Select * From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTime)

        'ddlStartTime.DataSource = ds
        'ddlStartTime.DataBind()

        'ddlEndTime.DataSource = ds
        'ddlEndTime.DataBind()
    End Sub

    Private Sub LoadRoleData()
        Dim SQLContestTeam As String = ""
        Dim ds As DataSet
        Try
            SQLContestTeam = "Select * From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTeam)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RMcMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RMcMemberID"))))
                ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("PronMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("PronMemberID"))))
                ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("CJMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("CJMemberID"))))
                ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("AJMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("AJMemberID"))))
                ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("LTJudMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("LTJudMemberID"))))
                ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("DictHMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("DictHMemberID"))))
                ddlGrading.SelectedIndex = ddlGrading.Items.IndexOf(ddlGrading.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID"))))
                ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("ProcMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("ProcMemberID"))))
                ddlTimer.SelectedIndex = ddlTimer.Items.IndexOf(ddlTimer.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("TimerMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("TimerMemberID"))))
            End If
            'CheckRoomReqData()

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub LoadDGRoomSchedule()
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim lblCapcty As Label
        Dim lbtnRemoveCont As LinkButton
        Dim lbtnEditCont As LinkButton
        Dim lbl_RoomMc As Label
        Dim lbl_Pronouncer As Label
        Dim lbl_AssoJudge As Label
        Dim lbl_ChiefJudge As Label
        Dim lbl_LTJudge As Label
        Dim lbl_DictH As Label
        Dim lbl_Grading As Label
        Dim lbl_Proctor As Label
        Dim lbl_Timer As Label
        Dim lbl_ContTeamID As Label
        Dim i As Integer = 0
        Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)

        DGRoomSchedule.DataSource = ds
        DGRoomSchedule.DataBind()
        Dim ds2 As DataSet  'ds1,

        Try
            For Each dgItem In DGRoomSchedule.Items
                lblCapcty = dgItem.FindControl("lblCapacity")
                Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")
                If ds1.Tables(0).Rows.Count > 0 Then
                    lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")
                Else
                    lblCapcty.Text = ""
                End If

                lbtnRemoveCont = dgItem.FindControl("lbtnRemove")
                lbtnEditCont = dgItem.FindControl("lbtnEdit")

                lbl_RoomMc = dgItem.FindControl("lblRoomMc")
                lbl_Pronouncer = dgItem.FindControl("lblPronouncer")
                lbl_AssoJudge = dgItem.FindControl("lblAssociateJudge")
                lbl_ChiefJudge = dgItem.FindControl("lblChiefJudge")
                lbl_LTJudge = dgItem.FindControl("lblLaptopJudge")
                lbl_DictH = dgItem.FindControl("lblDictHandler")
                lbl_Grading = dgItem.FindControl("lblGrading")
                lbl_Proctor = dgItem.FindControl("lblProctor")
                lbl_Timer = dgItem.FindControl("lblTimer")
                lbl_ContTeamID = dgItem.FindControl("lblContTeamID")

                DGRoomSchedule.Columns(15).Visible = True   'RoomMc 
                DGRoomSchedule.Columns(16).Visible = True   'Pronouncer
                DGRoomSchedule.Columns(17).Visible = True   'ChiefJudge
                DGRoomSchedule.Columns(18).Visible = True   'AssoJudge
                DGRoomSchedule.Columns(19).Visible = True   'LTJudge  
                DGRoomSchedule.Columns(20).Visible = True   'DictH
                DGRoomSchedule.Columns(21).Visible = True   'Grading  
                DGRoomSchedule.Columns(22).Visible = True   'Proctor
                DGRoomSchedule.Columns(23).Visible = True   'Timer

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") > 0 Then

                    ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'") ' " & IIf(ddlBldgID.SelectedIndex = 0, "", "  ''''' ") & IIf(ddlRoomNo.SelectedIndex <= 0, "", "
                    If ds2.Tables(0).Rows.Count > 0 Then
                        ' For j As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("RMcMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("RMcMemberID")), lbl_RoomMc)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("PronMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("PronMemberID")), lbl_Pronouncer)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("CJMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("CJMemberID")), lbl_ChiefJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("AJMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("AJMemberID")), lbl_AssoJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("LTJudMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("LTJudMemberID")), lbl_LTJudge)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("DictHMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("DictHMemberID")), lbl_DictH)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID")), lbl_Grading)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("ProcMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("ProcMemberID")), lbl_Proctor)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("TimerMemberID") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("TimerMemberID")), lbl_Timer)

                        lbl_ContTeamID.Text = ds2.Tables(0).Rows(0)("ContTeamID")

                        'lbl_RoomMc.Text = ds2.Tables(0).Rows(0)("RMcMemberID")
                        'lbl_Pronouncer.Text = ds2.Tables(0).Rows(0)("Pronouncer")
                        'lbl_AssoJudge.Text = ds2.Tables(0).Rows(0)("AssoJudge")
                        'lbl_ChiefJudge.Text = ds2.Tables(0).Rows(0)("ChiefJudge")
                        'lbl_LTJudge.Text = ds2.Tables(0).Rows(0)("LTJudge")
                        'lbl_DictH.Text = ds2.Tables(0).Rows(0)("DictH")
                        'lbl_Grading.Text = ds2.Tables(0).Rows(0)("Grading")
                        'lbl_Proctor.Text = ds2.Tables(0).Rows(0)("Proctor")
                        'lbl_ContTeamID.Text = ds2.Tables(0).Rows(0)("ContTeamID")
                        'Next
                    Else
                        lbl_RoomMc.Text = ""
                        lbl_Pronouncer.Text = ""
                        lbl_AssoJudge.Text = ""
                        lbl_ChiefJudge.Text = ""
                        lbl_LTJudge.Text = ""
                        lbl_DictH.Text = ""
                        lbl_Grading.Text = ""
                        lbl_Proctor.Text = ""
                        lbl_ContTeamID.Text = ""
                    End If
                Else
                    lbl_RoomMc.Text = ""
                    lbl_Pronouncer.Text = ""
                    lbl_AssoJudge.Text = ""
                    lbl_ChiefJudge.Text = ""
                    lbl_LTJudge.Text = ""
                    lbl_DictH.Text = ""
                    lbl_Grading.Text = ""
                    lbl_Proctor.Text = ""
                    lbl_ContTeamID.Text = ""
                End If

                Dim SQLRoomReq As String = "Select IsNull(RoomMc,0) as RoomMc,IsNull(Pronouncer,0) as Pronouncer,IsNull(CJudge,0) as CJudge,IsNull(AJudge,0) as AJudge,IsNull(LTJudge,0) as LTJudge,IsNull(DictH,0) as DictH,IsNull(Proctor,0) as Proctor,IsNull(Grader,0) as Grader,IsNull(Timer,0) as Timer From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & " "
                Dim dsRoomReq As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
                If dsRoomReq.Tables(0).Rows.Count > 0 Then
                    If dsRoomReq.Tables(0).Rows(0)("RoomMc") = 0 Then
                        DGRoomSchedule.Columns(17).Visible = False      'RoomMc
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("Pronouncer") = 0 Then
                        DGRoomSchedule.Columns(18).Visible = False      'Pronouncer
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("CJudge") = 0 Then
                        DGRoomSchedule.Columns(19).Visible = False      'ChiefJudge
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("AJudge") = 0 Then
                        DGRoomSchedule.Columns(20).Visible = False      'AssoJudge
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("LTJudge") = 0 Then
                        DGRoomSchedule.Columns(21).Visible = False      'LTJudge
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("DictH") = 0 Then
                        DGRoomSchedule.Columns(22).Visible = False      'DictH
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("Grader") = 0 Then
                        DGRoomSchedule.Columns(23).Visible = False      'Grading
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("Proctor") = 0 Then
                        DGRoomSchedule.Columns(24).Visible = False      'Proctor
                    End If
                    If dsRoomReq.Tables(0).Rows(0)("Timer") = 0 Then
                        DGRoomSchedule.Columns(25).Visible = False      'Timer
                    End If
                End If

                If (lbl_ContTeamID.Text <> "") Then
                    lbtnRemoveCont.Enabled = True
                    'lbtnRemoveCont.Visible = True
                    lbtnEditCont.Enabled = True
                    'lbtnEditCont.Visible = True
                Else
                    lbtnRemoveCont.Enabled = False
                    'lbtnRemoveCont.Visible = False
                    lbtnEditCont.Enabled = False
                    'lbtnEditCont.Visible = False
                End If
                i = i + 1
            Next
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Private Sub DisplayName(ByVal MemberID As Integer, ByVal lblRole As Label)
        If MemberID <> 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select (FirstName + '  ' + LastName ) as Name  From IndSpouse Where AutoMemberID =" & MemberID) 'Title + '.' +
            lblRole.Text = ds.Tables(0).Rows(0)("Name")
        End If
    End Sub


    Private Sub LoadRolesVol()

        LoadRoleNames(ddlRoomMc, "RoleID = 86")
        LoadRoleNames(ddlPronouncer, "(RoleID = 15 Or RoleID = 16)")
        LoadRoleNames(ddlChiefJudge, "(RoleID = 15 Or RoleID = 16)")
        LoadRoleNames(ddlAssociateJudge, "(RoleID = 17 Or RoleID = 18)")
        LoadRoleNames(ddlLaptopJudge, "(RoleID = 17 Or RoleID = 18)")
        LoadRoleNames(ddlDictHandler, "RoleID = 19")
        LoadRoleNames(ddlGrading, "RoleID = 21")
        LoadRoleNames(ddlProctor, "(RoleID = 15 Or RoleID = 16 Or RoleID = 20)")
        LoadRoleNames(ddlTimer, "RoleID = 91 ")

    End Sub
    Private Sub LoadRoleNames(ByVal ddlObject As DropDownList, ByVal Role As String)
        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( FirstName + '  ' + LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and " & Role & " ) Order By LastName,FirstName" 'Title + '.' + EventYear= " & ddlYear.SelectedValue & "  and
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( FirstName + '  '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and " & Role & " )  Order By LastName,FirstName" 'Title + '.' +  
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)
        ddlObject.DataSource = ds
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, New ListItem("Select", 0))
    End Sub
    Private Sub ClearRoles()

        ddlRoomMc.Items.Clear()
        ddlPronouncer.Items.Clear()
        ddlChiefJudge.Items.Clear()
        ddlAssociateJudge.Items.Clear()
        ddlLaptopJudge.Items.Clear()
        ddlDictHandler.Items.Clear()
        ddlGrading.Items.Clear()
        ddlProctor.Items.Clear()
        ddlTimer.Items.Clear()

        'ddlTimer.Items.Clear()
        'LoadRolesVol()
        ' LoadRoleData()
    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click

        Dim SQLContTeamInsert As String = ""
        Dim SQLContTeamUpdate As String = ""
        Dim SQLContTeamEval As String = ""
        Dim SQLContTeamExec As String = ""

        lblAddUPdate.Text = ""

        If ddlEvent.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlProduct.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Product"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Phase"
            Exit Sub
            'ElseIf ddlStartTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select StartTime"
            '    Exit Sub
            'ElseIf ddlEndTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select EndTime"
            '    Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select RoomNo"
            Exit Sub
        End If
        Try
            SQLContTeamInsert = "Insert into ContestTeamSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,"
            SQLContTeamInsert = SQLContTeamInsert & "RMcMemberID,PronMemberID,CJMemberID,AJMemberID,LTJudMemberID,DictHMemberID,GradMemberID,ProcMemberID,TimerMemberID,CreatedBy,CreatedDate)"
            SQLContTeamInsert = SQLContTeamInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlBldgID.SelectedItem.Text & "'," & ddlSeqNo.SelectedValue & ",'" & ddlRoomNo.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ","
            SQLContTeamInsert = SQLContTeamInsert & IIf(ddlRoomMc.SelectedValue = 0, "NULL", ddlRoomMc.SelectedValue) & "," & IIf(ddlPronouncer.SelectedValue = 0, "NULL", ddlPronouncer.SelectedValue) & "," & IIf(ddlChiefJudge.SelectedValue = 0, "NULL", ddlChiefJudge.SelectedValue) & "," & IIf(ddlAssociateJudge.SelectedValue = 0, "NULL", ddlAssociateJudge.SelectedValue) & ","
            SQLContTeamInsert = SQLContTeamInsert & IIf(ddlLaptopJudge.SelectedValue = 0, "NULL", ddlLaptopJudge.SelectedValue) & "," & IIf(ddlDictHandler.SelectedValue = 0, "NULL", ddlDictHandler.SelectedValue) & "," & IIf(ddlGrading.SelectedValue = 0, "NULL", ddlGrading.SelectedValue) & "," & IIf(ddlProctor.SelectedValue = 0, "NULL", ddlProctor.SelectedValue) & "," & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) & "," & Session("LoginID") & ",GetDate() )" ' & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) & ","

            SQLContTeamUpdate = "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "' ,RMcMemberID = " & IIf(ddlRoomMc.SelectedValue = 0, "NULL", ddlRoomMc.SelectedValue) & ",PronMemberID=" & IIf(ddlPronouncer.SelectedValue = 0, "NULL", ddlPronouncer.SelectedValue) & ",CJMemberID=" & IIf(ddlChiefJudge.SelectedValue = 0, "NULL", ddlChiefJudge.SelectedValue) & ",AJMemberID=" & IIf(ddlAssociateJudge.SelectedValue = 0, "NULL", ddlAssociateJudge.SelectedValue) & ","
            SQLContTeamUpdate = SQLContTeamUpdate & " LTJudMemberID=" & IIf(ddlLaptopJudge.SelectedValue = 0, "NULL", ddlLaptopJudge.SelectedValue) & ",DictHMemberID=" & IIf(ddlDictHandler.SelectedValue = 0, "NULL", ddlDictHandler.SelectedValue) & ",GradMemberID=" & IIf(ddlGrading.SelectedValue = 0, "NULL", ddlGrading.SelectedValue) & ",ProcMemberID=" & IIf(ddlProctor.SelectedValue = 0, "NULL", ddlProctor.SelectedValue) & ",TimerMemberID=" & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"     ' ",TimerMemberID=" & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) &
            SQLContTeamUpdate = SQLContTeamUpdate & "Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and SeqNo =" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text.Trim() & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            SQLContTeamEval = "Select Count(*) From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Value & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamEval) = 0 Then
                'SQLContTeamExec = SQLContTeamInsert
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamInsert)
                lblAddUPdate.Text = "Added Successfully"
            Else
                'SQLContTeamExec = SQLContTeamUpdate
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamUpdate)
                lblAddUPdate.Text = "Updated Successfully"
            End If
            LoadDGRoomSchedule()
            ' btnAddUpdate.Text = "Add"
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 3
        ddlDate.SelectedIndex = 0
        ClearDropDowns(True)
        ClearRoles()
    End Sub
    Private Sub ClearDropDowns(ByVal PrdGpFlag As Boolean)
        DGRoomSchedule.Visible = False
        lblErr.Text = ""
        lblAddUPdate.Text = ""
        If PrdGpFlag = True Then
            ddlProductGroup.Items.Clear() '.SelectedIndex = 0 '
        End If
        ddlProduct.Items.Clear()
        ddlPhase.Items.Clear()
        ' ddlDate.SelectedIndex = 0 '.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlSeqNo.Items.Clear()
        ddlRoomNo.Items.Clear()
        'ddlStartTime.Items.Clear()
        'ddlEndTime.Items.Clear()

        ClearRoles()
        TrRoomMc.Visible = True
        TrPronouncer.Visible = True
        TrChiefJudge.Visible = True
        TrAssociateJudge.Visible = True
        TrLaptopJudge.Visible = True
        TrDictHandler.Visible = True
        TrGrader.Visible = True
        TrProctor.Visible = True
        TrTimer.Visible = True
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ClearDropDowns(True)
        ClearRoles()
        ' GetProductGroup(ddlProductGroup)
        GetDate()
    End Sub

    Protected Sub DGRoomSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lblContestTeamId As Label
        lblContestTeamId = e.Item.FindControl("lblContTeamID")
        Dim ContTeamID As Integer = lblContestTeamId.Text ' CInt(e.Item.Cells(25).Text)

        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from ContestTeamSchedule Where ContTeamID=" & ContTeamID & "")
                LoadDGRoomSchedule()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            'lblRoomSchID.Text = ContTeamID.ToString()
            LoadForUpdate(ContTeamID)
        End If
    End Sub
    Private Sub LoadForUpdate(ByVal ContTeamID As Integer)

        ' btnAddUpdate.Text = "Update"
        lblAddUPdate.Text = ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ContTeamID,ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose, convert(VarChar(10),  Date, 101) as Date,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Phase,RoleID,MemberID,Initials,RMcMemberID,PronMemberID,CJMemberID,AJMemberID,LTJudMemberID,DictHMemberID,GradMemberID,ProcMemberID,CreatedBy,CreatedDate,ModifiedBy,ModifiedDate,TimerMemberID from ContestTeamSchedule Where ContTeamID=" & ContTeamID & "")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo")  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim BldgID As String = ""
        Try
            If ds.Tables(0).Rows.Count > 0 Then

                'ddlStartTime.DataSource = ds '.SelectedItem.Text = ds.Tables(0).Rows(0)("StartTime").ToString
                'ddlStartTime.DataBind()

                'ddlEndTime.DataSource = ds    '   SelectedItem.Text = ds.Tables(0).Rows(0)("EndTime").ToString
                'ddlEndTime.DataBind()

                'ddlStartTime.SelectedValue = ds.Tables(0).Rows(0)("StartTime")
                'ddlEndTime.SelectedValue = ds.Tables(0).Rows(0)("EndTime")
                ddlDate.SelectedIndex = ddlDate.Items.IndexOf(ddlDate.Items.FindByText(ds.Tables(0).Rows(0)("Date"))) '.ToString("MM/dd/yyyy"))) 'Convert.ToDateTime(

                'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

                ddlBldgID.SelectedIndex = ddlBldgID.Items.IndexOf(ddlBldgID.Items.FindByValue(ds.Tables(0).Rows(0)("BldgID")))
                ddlRoomNo.Items.Clear()
                LoadRoomNumber(ds.Tables(0).Rows(0)("BldgID").ToString.Trim)
                ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo").ToString.Trim()))
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber").ToString))

                ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("RMcMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("RMcMemberID"))))
                ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("PronMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("PronMemberID"))))
                ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("CJMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("CJMemberID"))))
                ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("AJMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("AJMemberID"))))
                ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("LTJudMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("LTJudMemberID"))))
                ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("DictHMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("DictHMemberID"))))
                ddlGrading.SelectedIndex = ddlGrading.Items.IndexOf(ddlGrading.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID"))))
                ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("ProcMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("ProcMemberID"))))
                ddlTimer.SelectedIndex = ddlTimer.Items.IndexOf(ddlTimer.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("TimerMemberID") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("TimerMemberID"))))

            End If
            'CheckRoomReqData()
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub CheckRoomReqData()
        TrRoomMc.Visible = True
        TrPronouncer.Visible = True
        TrChiefJudge.Visible = True
        TrAssociateJudge.Visible = True
        TrLaptopJudge.Visible = True
        TrDictHandler.Visible = True
        TrGrader.Visible = True
        TrProctor.Visible = True
        TrTimer.Visible = True

        'Dim SQLRoomReq As String = "Select IsNull(RoomMc,0) as RoomMc,IsNull(Pronouncer,0) as Pronouncer,IsNull(CJudge,0) as CJudge,IsNull(AJudge,0) as AJudge,IsNull(LTJudge,0) as LTJudge,IsNull(DictH,0) as DictH,IsNull(Proctor,0) as Proctor,IsNull(Grader,0) as Grader,IsNull(Timer,0) as Timer From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & " "
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)

        'If ds.Tables(0).Rows.Count > 0 Then
        '    If ds.Tables(0).Rows(0)("RoomMc") = 0 Then
        '        TrRoomMc.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("Pronouncer") = 0 Then
        '        TrPronouncer.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("CJudge") = 0 Then
        '        TrChiefJudge.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("AJudge") = 0 Then
        '        TrAssociateJudge.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("LTJudge") = 0 Then
        '        TrLaptopJudge.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("DictH") = 0 Then
        '        TrDictHandler.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("Grader") = 0 Then
        '        TrGrader.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("Proctor") = 0 Then
        '        TrProctor.Visible = False
        '    End If
        '    If ds.Tables(0).Rows(0)("Timer") = 0 Then
        '        TrTimer.Visible = False
        '    End If
        'End If


    End Sub
    Protected Sub BtnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopy.Click
        TrCopy.Visible = True
        GetProductGroup(ddlToPG)
    End Sub

    Protected Sub ddlToPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToPG.SelectedIndexChanged
        LoadProduct(ddlToPG, ddlToProduct)
    End Sub

    Protected Sub ddlToProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        Dim StrRoomNoSQl As String = "Select * from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "" ' and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim dsTime As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If dsTime.Tables(0).Rows.Count > 0 Then
            'ddlStartCpTime.DataSource = dsTime
            'ddlStartCpTime.DataBind()
            'ddlEndCpTime.DataSource = dsTime
            'ddlEndCpTime.DataBind()
        Else
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule to copy data"
        End If
    End Sub

    Protected Sub BtnCopySchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopySchedule.Click

        Dim SQLContTeamCpInsert As String = ""
        Dim SQLContTeamCpUpdate As String = ""
        Dim SQLContTeamCpEval As String = ""
        Dim SQLContTeamCpExec As String = ""
        Dim StrFromContest As String = ""
        Dim StrFromRoomSch As String = ""
        Dim ContestTeamID As String = ""
        Dim i, count As Integer
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        lblAddUPdate.Text = ""
        If ddlToPG.SelectedIndex = 0 Then
            lblErr.Text = "Please Select Copy Contest"
            Exit Sub
        End If

        StrFromRoomSch = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"
        Dim dsRoomSch As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromRoomSch)

        StrFromContest = "Select * From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromContest)

        Try

            If dsRoomSch.Tables(0).Rows.Count > 0 And ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "Insert into ContestTeamSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "RMcMemberID,PronMemberID,CJMemberID,AJMemberID,LTJudMemberID,DictHMemberID,GradMemberID,ProcMemberID,TimerMemberID,CreatedBy,CreatedDate)"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & dsRoomSch.Tables(0).Rows(i)("BldgID") & "','" & dsRoomSch.Tables(0).Rows(i)("SeqNo") & "','" & dsRoomSch.Tables(0).Rows(i)("RoomNumber") & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlToPG.SelectedValue & ",'" & ddlToPG.SelectedItem.Text & "'," & ddlToProduct.SelectedValue & ",'" & ddlToProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ","
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ","
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & "," & IIf(ds.Tables(0).Rows(i)("TimerMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("TimerMemberID")) & "," & Session("LoginID") & ",GetDate() )"

                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "', BldgID='" & ds.Tables(0).Rows(i)("BldgID") & "',SeqNo=" & ds.Tables(0).Rows(i)("SeqNo") & ", RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "',StartTime='" & ddlStartCpTime.SelectedValue & "',EndTime='" & ddlEndCpTime.SelectedValue & "',RMcMemberID = " & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & ",PronMemberID=" & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & ",CJMemberID=" & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & ",AJMemberID=" & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ","
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " LTJudMemberID=" & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & ",DictHMemberID=" & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & ",GradMemberID=" & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & ",ProcMemberID=" & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

                Next
                SQLContTeamCpEval = "Select Count(*) From ContestTeamSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""      'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") 'Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                If i <= count Then 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamCpEval) > 0 Then
                        Dim ds3 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select ContTeamID From ContestTeamSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "") ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & ")

                        For j As Integer = 0 To ds3.Tables(0).Rows.Count - 1
                            ContestTeamID = ContestTeamID & ds3.Tables(0).Rows(j)("ContTeamID") & ","
                        Next
                        ContestTeamID = ContestTeamID.Trim(",")
                        SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Delete From ContestTeamSchedule Where ContTeamID in(" & ContestTeamID & ")")

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Updated)"
                    Else
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Added)"
                    End If
                ElseIf i > count Then
                    lblErr.Text = "RoomSchedule data  has fewer records for the Contest"
                Else
                    lblErr.Text = "RoomSchedule data is not yet added for the Contest"
                End If
            End If
            LoadDGRoomSchedule()
            'btnAddUpdate.Text = "Add"
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub btnExportReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportReport.Click
        If ddlChapter.SelectedItem.Text = "Select" Then
            lblAddUPdate.Text = "Please Select Chapter To Export"
            Exit Sub
        End If
        Dim dt As DataTable = GetData()
        Dim ChapterCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Replace(Replace(ChapterCode,',','_'),' ','') as Chaptercode from chapter Where ChapterId=" & ddlChapter.SelectedValue)
        Dim attachment As String = "attachment; filename=" & ddlYear.SelectedValue & "_ContestTeamSchedule_" & ChapterCode & ".xls" '& "_" & ddlProductGroup.SelectedItem.Text & "_" & ddlProduct.SelectedItem.Text
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.xls"
        Dim tab As String = ""
        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = vbTab
        Next
        Response.Write(vbLf)

        Dim i As Integer
        For Each dr As DataRow In dt.Rows
            tab = ""
            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab & dr(i).ToString())
                tab = vbTab
            Next
            Response.Write(vbLf)
        Next
        Response.End()
    End Sub
    Function GetData() As DataTable

        Dim sql As String = ""

        'sql = "SELECT ContestYear,EventID,Event,Chapter,Purpose,BldgID,SeqNo,RoomNumber,CONVERT(VARCHAR(10), DATE, 101) AS Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,StartTime,EndTime FROM ContestTeamSchedule Where Contestyear=" + ddlYear.SelectedValue + " and ChapterID=" & ddlChapter.SelectedValue & " order by Date, productgroupID, productID"
        sql = sql & "SELECT C.ContestYear,C.EventID,C.Event,C.Chapter,C.Purpose,C.ProductGroupId,C.ProductGroupCode,C.ProductId,C.ProductCode,CONVERT(VARCHAR(10), C.DATE, 101) AS Date,C.BldgID,C.SeqNo,C.RoomNumber"
        sql = sql & ",Phase,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where I.AutoMemberID = C.RMcMemberId)  as RoomMc ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.PronMemberID) as Pronouncer ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.CJMemberID) as ChiefJudge ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.AJMemberID)as AssociateJudge ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.LTJudMemberID) as LaptopJudge,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.DictHMemberID) as DictHandler ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.GradMemberID) as Grading ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.ProcMemberID) as Proctor ,"
        sql = sql & "(Select (I.Title+'.'+ I.FirstName +' '+ I.MiddleInitial + ' '+ I.LastName) From IndSpouse I where AutoMemberID = C.TimerMemberID) as Timer ,C.StartTime,C.EndTime"
        sql = sql & " FROM ContestTeamSchedule C  Where Contestyear = " + ddlYear.SelectedValue + " and eventID=" & ddlEvent.SelectedValue & " and ChapterID=" + ddlChapter.SelectedValue + " order by Date, productgroupID, productID,Phase"

        Using myConnection As New SqlConnection(Application("ConnectionString"))
            Using myCommand As SqlCommand = New SqlCommand(sql, myConnection)
                myConnection.Open()
                Using myReader As SqlDataReader = myCommand.ExecuteReader()
                    Dim myTable As DataTable = New DataTable()
                    myTable.Load(myReader)
                    myConnection.Close()
                    Return myTable
                End Using
            End Using
        End Using
    End Function

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        ClearDropDowns(True)
        GetProductGroup(ddlProductGroup)
    End Sub
End Class

