﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class FundRStatus
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            'loadgrid()
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                hlnkMainPage.NavigateUrl = "userfunctions.aspx"
                hlnkMainPage.Text = "Back to Parent Functions"
            ElseIf Session("entryToken").ToString.ToUpper() = "DONOR" Then
                hlnkMainPage.NavigateUrl = "DonorFunctions.aspx"
                hlnkMainPage.Text = "Back to Donor Functions"
            Else
                hlnkMainPage.Text = "Back to Volunteer Functions"
                hlnkMainPage.NavigateUrl = "VolunteerFunctions.aspx"
            End If
            'If Not Request.QueryString("Id") Is Nothing Then
            '    Session("FundDonorType") = "IND"
            'End If
            If Session("FundDonorType") Is Nothing Then
                Session("FundDonorType") = "IND"
            End If
            loadgrid()
        End If
    End Sub
    Private Sub loadgrid()
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference, convert(varchar(10),FR.EventDate,101) EventDate,(select Distinct(ORGANIZATION_NAME) From OrganizationInfo Org inner join FundRaisingCal FC on Org.automemberid=FC.VenueId where FC.FundRCalID=FR.FundRCalID) Venue "
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
        strSQL = strSQL & " WHERE FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "' and FF.ProductCode not in ('Child','Adult')"
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            Dim Count As Integer = dt.Rows.Count
            Dim dv As DataView = New DataView(dt)
            If dt.Rows.Count > 0 Then
                dgCatalog.DataSource = dt
                dgCatalog.DataBind()
                strSQL = "select isnull(sum(discount),0) from NFG_Transactions where paymentnotes is not null and Memberid=" & Session("CustIndID") & " and EventYear=Year(GetDate()) and EventId=9 "
                ' Response.Write(strSQL)
                Dim iDiscount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSQL)
                If iDiscount > 0 Then
                    ltrDiscount.Text = "<b> Discount Given  :</b>  $" & iDiscount.ToString
                End If
            Else
                ltrl1.Text = "No fundraising registration found"
            End If

            strSQL = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
            strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference, convert(varchar(10),FR.EventDate,101) EventDate,(select Distinct(ORGANIZATION_NAME) From OrganizationInfo Org inner join FundRaisingCal FC on Org.automemberid=FC.VenueId where FC.FundRCalID=FR.FundRCalID) Venue "
            strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
            strSQL = strSQL & " WHERE FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "' and FF.ProductCode in ('Child','Adult')"
            strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"

            Dim dsAttendees As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dtAttendees As DataTable = dsAttendees.Tables(0)
            lblAdult.Text = 0
            lblChildren.Text = 0
            Dim i As Integer, dr As DataRow
            For i = 0 To dtAttendees.Rows.Count - 1
                dr = dtAttendees.Rows(i)
                If dr("ProductCode").ToString.ToLower = "adult" Then
                    lblAdult.Text = dr("Quantity")
                End If
                If dr("ProductCode").ToString.ToLower = "child" Then
                    lblChildren.Text = dr("Quantity")
                End If
            Next
        Catch ex As Exception
            ltrl1.Text = "No fundraising registration found"
        End Try
       
    End Sub

    Protected Sub dgCatalog_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label
                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("PaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf PaymentMode = "Check" Then
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                ElseIf PaymentMode = "In Kind" Then
                    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Text = lblamount.Text
                    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    'Function CalcAmount() As Decimal
    '    Dim totamount As Decimal = 0.0
    '    Dim chkAmt As Decimal = 0.0
    '    Dim CreditAmt As Decimal = 0.0
    '    Dim InkindAmt As Decimal = 0.0
    '    Dim item As DataGridItem
    '    For Each item In dgCatalog.Items
    '        Dim lblamount As Label = CType(item.FindControl("lblAmount"), Label)
    '        Dim lblCreditCardAmt As Label = CType(item.FindControl("lblCreditCardAmt"), Label)
    '        Dim lblCheckAmt As Label = CType(item.FindControl("lblCheckAmt"), Label)
    '        Dim lblInKindAmt As Label = CType(item.FindControl("lblInKindAmt"), Label)
    '        Dim PaymentMode As String = CType(item.FindControl("lblSelPaymentMode"), Label).Text.Trim
    '        'If PaymentMode = "Credit Card" Then
    '        '    lblCreditCardAmt.Text = lblamount.Text
    '        '    lblCreditCardAmt.Visible = True
    '        '    lblCheckAmt.Visible = False
    '        '    lblInKindAmt.Visible = False
    '        '    If lblamount.Text.Length > 0 Then
    '        '        CreditAmt = CreditAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '        '    End If
    '        'Else
    '        If PaymentMode = "Check" Then
    '            lblCreditCardAmt.Visible = False
    '            lblCheckAmt.Visible = True
    '            lblInKindAmt.Visible = False
    '            lblCheckAmt.Text = lblamount.Text
    '            If lblamount.Text.Length > 0 Then
    '                chkAmt = chkAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '            End If
    '            'ElseIf PaymentMode = "In Kind" Then
    '            '    lblInKindAmt.Text = lblamount.Text
    '            '    lblCreditCardAmt.Visible = False
    '            '    lblCheckAmt.Visible = False
    '            '    lblInKindAmt.Visible = True
    '            '    If lblamount.Text.Length > 0 Then
    '            '        InkindAmt = InkindAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '            '    End If
    '        End If
    '        'lblCheck.Text = String.Format(chkAmt, "{0:c}")
    '        ''lblCredit.Text = String.Format(CreditAmt, "{0:F2}")
    '        'lblInKind.Text = String.Format(InkindAmt, "{0:F2}")
    '        If lblamount.Text.Length > 0 Then
    '            totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
    '        End If
    '    Next
    '    Return totamount
    'End Function
End Class
