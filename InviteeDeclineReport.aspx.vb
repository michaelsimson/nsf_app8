﻿Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class Invitee_Decline_Report
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        Else
            HyperLink2.Visible = True
        End If
        If IsPostBack = False Then
            loadyear()
        End If
    End Sub
    Protected Sub ddlyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ltlYear.Text = ddlyear.SelectedValue
        'loadcount(ddlyear.SelectedValue, GVInvitee)
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year - 4 Step -1
            ddlyear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlyear.Items(0).Selected = True
    End Sub
End Class
