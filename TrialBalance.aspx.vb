﻿
Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports NativeExcel
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class TrialBalance
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                Dim year As Integer = Now.Year
                Dim i As Integer
                For i = -4 To -0
                    year = Now.Year + i
                    ddlYearFrom.Items.Add(New ListItem(year.ToString().ToString(), year))
                    ddlYearTo.Items.Add(New ListItem(year.ToString().ToString(), year))
                Next
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub

    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf ddlYearFrom.SelectedValue > ddlYearTo.SelectedValue Then
            lblErr.Text = "Invalid Year"
            Exit Sub
        ElseIf ddlYearFrom.SelectedValue = ddlYearTo.SelectedValue And DdlFromMM.SelectedValue > DdlToMM.SelectedValue Then
            lblErr.Text = "Invalid Month"
            Exit Sub
        Else
            lblErr.Text = ""
            dgIS.DataSource = Nothing
            dgIS.DataBind()
            dgFE.DataSource = Nothing
            dgFE.DataBind()
            If ddlCategory.SelectedValue = "1" Then
                GetStatementofActivities(0)
            ElseIf ddlCategory.SelectedValue = "2" Then
                GetFunctionalExpenses(0)
            ElseIf ddlCategory.SelectedValue = "4" Then
                ''FunctionalExpenses

            ElseIf ddlCategory.SelectedValue = "5" Then
                ''Income stmt
                Response.Redirect("IncomeStatement.aspx?From=" & DdlFromMM.SelectedItem.Text & "," & ddlYearFrom.SelectedItem.Text & "&To=" & DdlToMM.SelectedItem.Text & "," & ddlYearTo.SelectedItem.Text)
            End If
        End If
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf ddlYearFrom.SelectedValue > ddlYearTo.SelectedValue Then
            lblErr.Text = "Invalid Year"
            Exit Sub
        ElseIf ddlYearFrom.SelectedValue = ddlYearTo.SelectedValue And DdlFromMM.SelectedValue > DdlToMM.SelectedValue Then
            lblErr.Text = "Invalid Month"
            Exit Sub
        Else
            lblErr.Text = ""
            If ddlCategory.SelectedValue = "1" Then
                GetStatementofActivities(1)
            ElseIf ddlCategory.SelectedValue = "2" Then
                GetFunctionalExpenses(1)
            ElseIf ddlCategory.SelectedValue = "6" Then
                GetStatementsofFinancialPosition(1)
            End If
        End If
    End Sub

    Public Sub GetFunctionalExpenses(ByVal Type As Integer)
        Try
            Dim Enddate As Date = DdlToMM.SelectedValue & "/01/" & ddlYearTo.SelectedValue
            Enddate = Enddate.AddMonths(1).AddDays(-1)

            Dim sSql As String   'Functional Expenses
            sSql = " Select N.AccNo,NA.AccNo as HeaderAccNo,NA.Description as HeaderDesc,N.Description,-SUM(G.Amount) as Amount,-SUM(CASE WHEN G.AccountType NOT IN ('G','A') THEN G.Amount ELSE 0 END) as PSAmount,-SUM(CASE WHEN G.AccountType IN ('G','A') THEN G.Amount ELSE 0 END) as GAAmount,G.AccountType "
            sSql = sSql & " from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account INNER JOIN NSFAccounts NA ON NA.AccNo = Convert(int,LEFT(N.AccNo,LEN(N.AccNo) -3) + '000')  where N.AccType IN ('E') "
            sSql = sSql & " AND G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
            sSql = sSql & " Group By N.AccNo,N.Description,NA.Description,AccountType,NA.AccNo order By N.AccNo,NA.AccNo "
            'Response.Write(sSql)
            'Exit Sub
            If Type = 1 Then
                '' For Excel - Start
                Dim rsBankTran As SqlDataReader
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                Dim oSheet As IWorksheet
                oSheet = oWorkbooks.Worksheets.Add()
                Dim FileName As String = "FE_" & Now.Date() & ".xls" 'ddlYearFrom.SelectedValue & ".xls"
                oSheet.Range("A1:C1").MergeCells = True
                oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION"
                oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignLeft
                oSheet.Range("A1").Font.Bold = True
                oSheet.Range("D1:F1").MergeCells = True
                oSheet.Range("D1").Value = "Trial Balance"
                oSheet.Range("D1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                oSheet.Range("D1").Font.Bold = True
                oSheet.Range("A2").Value = "Statement of Functional Expenses"
                oSheet.Range("A2").Font.Bold = True
                oSheet.Range("A3").Value = "For 1 " & DdlFromMM.SelectedItem.Text & "," & ddlYearFrom.SelectedValue & " - " & Enddate.ToString("dd") & " " & DdlToMM.SelectedItem.Text & "," & ddlYearTo.SelectedValue & " "
                oSheet.Range("A3").Font.Bold = True
                oSheet.Range("D2").Font.Bold = True
                Dim CRange As IRange
                Dim InitialRow As Integer
                Dim CurrentRow As Integer
                Dim CurrTicker As String
                Dim BegBalTot As String = ""
                Dim EndBalTot As String = ""
                Dim TotBalTot As String = ""
                Dim oldHeader As String = ""
                InitialRow = 5
                CurrTicker = ""
                Dim flag As Boolean = False
                
                oSheet.Range("A" & Str(InitialRow) & "").Value = "AccontNo"
                oSheet.Range("B" & Str(InitialRow) & "").Value = "Functional Expenses"
                oSheet.Range("C" & Str(InitialRow) & "").Value = "Program Services"
                oSheet.Range("D" & Str(InitialRow) & "").Value = "General & Administrative"
                oSheet.Range("E" & Str(InitialRow) & "").Value = "Total"

                InitialRow = InitialRow + 2
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                oSheet.Range("A" & Trim(Str(InitialRow))).Value = 66000
                oSheet.Range("B" & Str(InitialRow) & "").Value = "Salaries & Related Expenses "
                oSheet.Range("C" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("C" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                oSheet.Range("D" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("D" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                oSheet.Range("E" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("E" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                InitialRow = InitialRow + 2
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                oSheet.Range("A" & Trim(Str(InitialRow))).Value = 75000
                oSheet.Range("B" & Str(InitialRow) & "").Value = "Contract Services Expenses "
                oSheet.Range("C" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("C" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                oSheet.Range("D" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("D" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                oSheet.Range("E" & Trim(Str(InitialRow))).Value = 0
                oSheet.Range("E" & Trim(Str(InitialRow))).NumberFormat = "$* #,##0_);_($* (#,##0)"


                'Contract Services Expenses

                InitialRow = InitialRow + 2
                CurrentRow = InitialRow
                While rsBankTran.Read()
                    flag = True
                    If CurrTicker.Trim.ToLower <> rsBankTran("HeaderAccNo").ToString().Trim.ToLower Then
                        If CurrTicker <> "" Then
                            '' '' update colum totals
                            CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                            CRange.Value = " Total " & oldHeader.Replace("Expenses", "") & " Expenses"
                            CRange.Font.Bold = True
                            If BegBalTot.Length = 0 Then
                                BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                                TotBalTot = "=E" & Trim(Str(CurrentRow + 1))
                            Else
                                BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                                TotBalTot = TotBalTot & "+E" & Trim(Str(CurrentRow + 1))
                            End If
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            InitialRow = CurrentRow + 3
                            CurrentRow = InitialRow
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc").ToString().Replace("Expenses", "") & " Expenses"
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        Else
                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc").ToString().Replace("Expenses", "") & " Expenses"
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        End If
                    End If
                    CurrentRow = CurrentRow + 1
                    CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("AccNo")
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Description")

                    If rsBankTran("AccountType").ToString().ToLower.Trim = "g" Or rsBankTran("AccountType").ToString().ToLower.Trim = "a" Then
                        CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                        CRange.Value = rsBankTran("Amount")
                        CRange.NumberFormat = "#,##0_);(#,##0)"
                    Else
                        CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                        CRange.Value = rsBankTran("Amount")
                        CRange.NumberFormat = "#,##0_);(#,##0)"
                    End If

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"
                End While

                If flag = False Then
                    lblErr.Text = "no record to Show"
                    Exit Sub
                Else
                    If CurrTicker <> "" Then
                        ''update colum totals
                        CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                        CRange.Value = " Total " & oldHeader.Replace("Expenses", "") & " Expenses"
                        CRange.Font.Bold = True
                        If BegBalTot.Length = 0 Then
                            BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                            TotBalTot = "=E" & Trim(Str(CurrentRow + 1))
                        Else
                            BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                            TotBalTot = TotBalTot & "+E" & Trim(Str(CurrentRow + 1))
                        End If
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                        CurrentRow = CurrentRow + 1
                    End If
                End If

                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 3)))
                CRange.Value = "Total Functional Expenses"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = BegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 3)))
                CRange.Value = EndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = TotBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("A" & Trim(Str(CurrentRow + 8)))
                CRange.Value = "The accompanying notes are an integral part of these statements."

                'Save the worksheet
                'Stream workbook 
                oSheet.Name = "FE_" & ddlYearFrom.SelectedValue
                Response.Clear()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                oWorkbooks.SaveAs(Response.OutputStream)
                Response.End()
                '' End of Excel Generation
            Else
                '' Grid to Show
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, sSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).NewRow
                    Dim i As Integer
                    Dim PSAmount, GAAmount As Decimal
                    PSAmount = 0.0
                    GAAmount = 0.0
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        PSAmount = PSAmount + ds.Tables(0).Rows(i)("PSAmount")
                        GAAmount = GAAmount + ds.Tables(0).Rows(i)("GAAmount")
                    Next
                    row("PSAmount") = PSAmount
                    row("GAAmount") = GAAmount

                    row("Description") = "Total"
                    ds.Tables(0).Rows.InsertAt(row, ds.Tables(0).Rows.Count + 1)
                    dgFE.DataSource = ds.Tables(0)
                    dgFE.DataBind()
                    dgFE.Visible = True
                Else
                    dgFE.Visible = False
                    lblErr.Text = "No result to show"
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub GetStatementofActivities(ByVal Type As Integer)
        Try
            Dim Enddate As Date = DdlToMM.SelectedValue & "/01/" & ddlYearTo.SelectedValue
            Enddate = Enddate.AddMonths(1).AddDays(-1)

            Dim sSql As String   'Functional Expenses
            sSql = " Select N.AccNo,NA.AccNo as HeaderAccNo,NA.Description as HeaderDesc,N.Description,ISNULL(N.RestrictionType,'Unrestricted') as RestrictionType, -SUM(CASE WHEN N.RestrictionType= 'Perm Restricted'  "
            sSql = sSql & " THEN G.Amount Else 0 END) as PRAmount,-SUM(CASE WHEN N.RestrictionType= 'Temp Restricted' THEN G.Amount Else 0 END) as TRAmount,-SUM(CASE WHEN N.RestrictionType= 'Unrestricted' OR N.RestrictionType IS NULL THEN G.Amount Else 0 END) as URAmount,SUM(G.Amount),AccountType  from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account INNER JOIN NSFAccounts NA ON NA.AccNo = Convert(int,LEFT(N.AccNo,LEN(N.AccNo) -3) + '000') where   N.AccType IN ('I') AND "
            sSql = sSql & " G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
            sSql = sSql & " Group By N.AccNo,N.Description,N.RestrictionType,NA.Description,AccountType,NA.AccNo order By N.AccNo,NA.AccNo "
            'Response.Write(sSql)
            'Exit Sub
            If Type = 1 Then
                '' For Excel - Start
                Dim rsBankTran As SqlDataReader
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                Dim oSheet As IWorksheet
                oSheet = oWorkbooks.Worksheets.Add()
                Dim FileName As String = "IS_" & Now.Date() & ".xls" 'ddlYearFrom.SelectedValue & ".xls"
                oSheet.Range("A1:C1").MergeCells = True
                oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION"
                oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignLeft
                oSheet.Range("A1").Font.Bold = True
                oSheet.Range("D1:F1").MergeCells = True
                oSheet.Range("D1").Value = "Trial Balance"
                oSheet.Range("D1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                oSheet.Range("D1").Font.Bold = True
                oSheet.Range("A2").Value = "Statement of Activities"
                oSheet.Range("A2").Font.Bold = True
                oSheet.Range("A3").Value = "For 1 " & DdlFromMM.SelectedItem.Text & "," & ddlYearFrom.SelectedValue & " - " & Enddate.ToString("dd") & " " & DdlToMM.SelectedItem.Text & "," & ddlYearTo.SelectedValue & " "
                oSheet.Range("A3").Font.Bold = True
                oSheet.Range("D2").Font.Bold = True
                Dim CRange As IRange
                Dim InitialRow As Integer
                Dim CurrentRow As Integer
                Dim CurrTicker As String
                Dim BegBalTot As String = ""
                Dim EndBalTot As String = ""
                Dim PrTotal As String = ""
                Dim TotBalTot As String = ""
                Dim oldHeader As String = ""
                InitialRow = 5
                CurrTicker = ""
                Dim flag As Boolean = False
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                oSheet.Range("A" & Str(InitialRow) & "").Value = "AccontNo"
                oSheet.Range("B" & Str(InitialRow) & "").Value = ""
                oSheet.Range("C" & Str(InitialRow) & "").Value = "Unrestricted"
                oSheet.Range("D" & Str(InitialRow) & "").Value = "Temporarily Restricted"
                oSheet.Range("E" & Str(InitialRow) & "").Value = "Permanently Restricted"
                oSheet.Range("F" & Str(InitialRow) & "").Value = "Total"
                InitialRow = InitialRow + 1
                CurrentRow = InitialRow
                While rsBankTran.Read()
                    flag = True
                    If CurrTicker.Trim.ToLower <> rsBankTran("HeaderAccNo").ToString().Trim.ToLower Then
                        If CurrTicker <> "" Then
                            '' '' update colum totals
                            CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                            CRange.Value = " Total " & oldHeader
                            CRange.Font.Bold = True
                            If BegBalTot.Length = 0 Then
                                BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                                PrTotal = "=E" & Trim(Str(CurrentRow + 1))
                                TotBalTot = "=F" & Trim(Str(CurrentRow + 1))
                            Else
                                BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                                PrTotal = PrTotal & "+E" & Trim(Str(CurrentRow + 1))
                                TotBalTot = TotBalTot & "+F" & Trim(Str(CurrentRow + 1))
                            End If
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("F" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            InitialRow = CurrentRow + 3
                            CurrentRow = InitialRow
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc")
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        Else
                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc")
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        End If
                    End If
                    CurrentRow = CurrentRow + 1
                    CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("AccNo")
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Description")

                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("URAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("TRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("PRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"
                End While

                If flag = False Then
                    lblErr.Text = "no record to Show"
                    Exit Sub
                Else
                    If CurrTicker <> "" Then
                        ''update colum totals
                        CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                        CRange.Value = " Total " & oldHeader
                        CRange.Font.Bold = True
                        If BegBalTot.Length = 0 Then
                            BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                            PrTotal = "=E" & Trim(Str(CurrentRow + 1))
                            TotBalTot = "=F" & Trim(Str(CurrentRow + 1))
                        Else
                            BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                            PrTotal = PrTotal & "+E" & Trim(Str(CurrentRow + 1))
                            TotBalTot = TotBalTot & "+F" & Trim(Str(CurrentRow + 1))
                        End If
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                        oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("F" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"
                        CurrentRow = CurrentRow + 1
                    End If
                End If

                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 2)))
                CRange.Value = "Investment Income:"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 2)))
                CRange.Value = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("F" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CurrentRow = CurrentRow + 2

                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 2)))
                CRange.Value = "Net assets released from restrictions"


                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 2)))
                CRange.Value = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CRange = oSheet.Range("F" & Trim(Str(CurrentRow + 2)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                'CRange.Font.Bold = True

                CurrentRow = CurrentRow + 2


                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 3)))
                CRange.Value = "Total Public Support and Other Revenues"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = BegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 3)))
                CRange.Value = EndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = PrTotal
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("F" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = TotBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True



                Dim TBegBalTot, TEndBalTot, TPrTotal, TTotBalTot As String
                TBegBalTot = "=C" & Trim(Str(CurrentRow + 3))
                TEndBalTot = "=D" & Trim(Str(CurrentRow + 3))
                TPrTotal = "=E" & Trim(Str(CurrentRow + 3))
                TTotBalTot = "=F" & Trim(Str(CurrentRow + 3))

                ''** For Distributions and Expenses

                CurrentRow = CurrentRow + 3

                sSql = " Select 70000 as AccNo,NA.AccNo as HeaderAccNo,'Program Service Expenses' as HeaderDesc,'Grants and Distributions' as Description,'Unrestricted' as RestrictionType, 0 as PRAmount, 0 as TRAmount, -SUM(G.Amount) as URAmount,G.AccountType"
                sSql = sSql & " from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account INNER JOIN NSFAccounts NA ON NA.AccNo = Convert(int,LEFT(N.AccNo,LEN(N.AccNo) -3) + '000') where  N.AccType = 'G' "
                sSql = sSql & "  AND G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
                sSql = sSql & " Group By AccountType,NA.AccNo  Union All "
                sSql = sSql & " Select 0 as AccNo,0  as HeaderAccNo,'Program Service Expenses' as HeaderDesc,'Other Program Services' as Description,'Unrestricted' as RestrictionType,0 as PRAmount, 0 as TRAmount, -SUM(G.Amount) as URAmount,G.AccountType "
                sSql = sSql & " from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account  where  N.AccType = 'E' "
                sSql = sSql & " AND G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
                sSql = sSql & " Group By AccountType "
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                Dim secondflag As Boolean = False
                While rsBankTran.Read()
                    secondflag = True
                    If oldHeader.Trim.ToLower <> rsBankTran("HeaderDesc").ToString().Trim.ToLower Then
                        CurrTicker = rsBankTran("HeaderAccNo").ToString()
                        oldHeader = rsBankTran("HeaderDesc").ToString()
                        InitialRow = CurrentRow + 3
                        oSheet.Range("B" & Str(InitialRow) & "").Value = "Distributions and Expenses"
                        oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        InitialRow = InitialRow + 1
                        CurrentRow = InitialRow
                        'oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                        oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc")
                        'oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                    End If

                    CurrentRow = CurrentRow + 1
                    If rsBankTran("AccNo") = 70000 Then
                        CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                        CRange.Value = rsBankTran("AccNo")
                    End If
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Description")

                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("URAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("TRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("PRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                End While

                If secondflag = True Then
                    CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                    CRange.Value = "Total " & oldHeader
                    CRange.Font.Bold = True
                    oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                    oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                    oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                    oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                    oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                    oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                    oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                    oSheet.Range("F" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                    BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                    EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                    PrTotal = "=E" & Trim(Str(CurrentRow + 1))
                    TotBalTot = "=F" & Trim(Str(CurrentRow + 1))
                    CurrentRow = CurrentRow + 2
                End If


                'For General and Administrative Expenses

                sSql = " Select 0 as AccNo,0  as HeaderAccNo,'General and Administrative Expenses' as HeaderDesc,'General and Administrative Expenses' as Description,'Unrestricted' as RestrictionType, 0 as PRAmount, 0 as TRAmount, -SUM(G.Amount) as URAmount,G.AccountType   from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account  where  N.AccType = 'E' AND G.AccountType IN ('G','A') "
                sSql = sSql & "  AND G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
                sSql = sSql & " Group By AccountType  "
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                Dim Thirdflag As Boolean = False
                While rsBankTran.Read()
                    CurrentRow = CurrentRow + 3
                    Thirdflag = True
                    'CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                    'CRange.Value = rsBankTran("AccNo")
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Description")
                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("URAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("TRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("PRAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                End While

                If Thirdflag = False Then
                    CurrentRow = CurrentRow + 3
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = "General and Administrative Expenses"
                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = 0
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = 0
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Value = 0
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                End If
                BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow))
                EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow))
                PrTotal = PrTotal & "+E" & Trim(Str(CurrentRow))
                TotBalTot = TotBalTot & "+F" & Trim(Str(CurrentRow))


                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "Total Distributions and Expenses"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = BegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 1)))
                CRange.Value = EndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = PrTotal
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("F" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = TotBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                TBegBalTot = TBegBalTot & "-C" & Trim(Str(CurrentRow + 1))
                TEndBalTot = TEndBalTot & "-D" & Trim(Str(CurrentRow + 1))
                TPrTotal = TPrTotal & "-E" & Trim(Str(CurrentRow + 1))
                TTotBalTot = TTotBalTot & "-F" & Trim(Str(CurrentRow + 1))
                CurrentRow = CurrentRow + 2



                InitialRow = CurrentRow
                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "Change in Net assets"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = TBegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 1)))
                CRange.Value = TEndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = TPrTotal
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("F" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = TTotBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True
                CurrentRow = CurrentRow + 1

                sSql = " SELECT   Year, Unrestricted, TempRestricted, PermRestricted FROM [IS] WHERE     Year =  " & ddlYearFrom.SelectedValue
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                While rsBankTran.Read()
                    CurrentRow = CurrentRow + 1
                    'CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                    'CRange.Value = rsBankTran("AccNo")
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = "Net assets at beginning of year"
                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Unrestricted")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("TempRestricted")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("PermRestricted")
                    CRange.NumberFormat = "#,##0_);(#,##0)"
                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                    CRange.Formula = "=C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow))
                    CRange.NumberFormat = "#,##0_);(#,##0)"
                End While

                CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                CRange.Value = "Net assets at end of year"
                CRange.Font.Bold = True
                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                oSheet.Range("F" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                PrTotal = "=E" & Trim(Str(CurrentRow + 1))
                TotBalTot = "=F" & Trim(Str(CurrentRow + 1))
                CurrentRow = CurrentRow + 2

                'SELECT  Year, Unrestricted, TempRestricted, PermRestricted FROM [IS] WHERE     (Year = 2010)


                CRange = oSheet.Range("A" & Trim(Str(CurrentRow + 6)))
                CRange.Value = "The accompanying notes are an integral part of these statements."

                'Save the worksheet
                'Stream workbook 
                oSheet.Name = "IS_" & ddlYearFrom.SelectedValue
                Response.Clear()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                oWorkbooks.SaveAs(Response.OutputStream)
                Response.End()
                '' End of Excel Generation
            Else
                '' Grid to Show
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, sSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).NewRow
                    Dim i As Integer
                    Dim URAmount, TRAmount, PRAmount As Decimal
                    URAmount = 0.0
                    TRAmount = 0.0
                    PRAmount = 0.0
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        URAmount = URAmount + ds.Tables(0).Rows(i)("URAmount")
                        TRAmount = TRAmount + ds.Tables(0).Rows(i)("TRAmount")
                        PRAmount = PRAmount + ds.Tables(0).Rows(i)("PRAmount")
                    Next
                    row("URAmount") = URAmount
                    row("TRAmount") = TRAmount
                    row("PRAmount") = PRAmount

                    row("Description") = "Total"
                    ds.Tables(0).Rows.InsertAt(row, ds.Tables(0).Rows.Count + 1)
                    dgIS.DataSource = ds.Tables(0)
                    dgIS.DataBind()
                    dgIS.Visible = True
                Else
                    dgIS.Visible = False
                    lblErr.Text = "No result to show"
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub GetStatementsofFinancialPosition(ByVal Type As Integer)
        Try
            Dim Enddate As Date = DdlToMM.SelectedValue & "/01/" & ddlYearTo.SelectedValue
            Enddate = Enddate.AddMonths(1).AddDays(-1)

            Dim sSql As String   'Functional Expenses
            sSql = " Select 11000 as AccNo,11000 as HeaderAccNo,'Current Assets' as HeaderDesc,'Cash and Cash Equivalents' as Description,SUM(G.Amount) as Amount,G.AccountType,BS.Amount as PSAmount from GeneralLedger G INNER JOIN NSFAccounts N ON N.AccNo = G.Account Left Join BS ON BS.AssetType = 'A' AND BS.Year = " & ddlYearTo.SelectedValue - 1 & " where  N.AccType='A' AND  N.AccNo < 11403 "
            sSql = sSql & "  AND G.BankDate is Not Null and G.BankDate Between '" & DdlFromMM.SelectedValue & "/01/" & ddlYearFrom.SelectedValue & "' AND '" & Enddate.ToString().Replace("12:00:00 AM", " 23:59") & "'"
            sSql = sSql & " Group By AccountType,BS.Amount"
            'Response.Write(sSql)
            'Exit Sub
            If Type = 1 Then
                '' For Excel - Start
                Dim rsBankTran As SqlDataReader
                rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
                Dim oSheet As IWorksheet
                oSheet = oWorkbooks.Worksheets.Add()
                Dim FileName As String = "BS_" & Now.Date() & ".xls" 'ddlYearFrom.SelectedValue & ".xls"
                oSheet.Range("A1:C1").MergeCells = True
                oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION"
                oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignLeft
                oSheet.Range("A1").Font.Bold = True
                oSheet.Range("D1:F1").MergeCells = True
                oSheet.Range("D1").Value = "Trial Balance"
                oSheet.Range("D1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                oSheet.Range("D1").Font.Bold = True
                oSheet.Range("A2").Value = "Statements of FinancialPosition"
                oSheet.Range("A2").Font.Bold = True
                oSheet.Range("A3").Value = "For 1 " & DdlFromMM.SelectedItem.Text & "," & ddlYearFrom.SelectedValue & " - " & Enddate.ToString("dd") & " " & DdlToMM.SelectedItem.Text & "," & ddlYearTo.SelectedValue & " "
                oSheet.Range("A3").Font.Bold = True
                oSheet.Range("D2").Font.Bold = True
                Dim CRange As IRange
                Dim InitialRow As Integer
                Dim CurrentRow As Integer
                Dim CurrTicker As String
                Dim BegBalTot As String = ""
                Dim EndBalTot As String = ""
                Dim PrTotal As String = ""
                Dim TotBalTot As String = ""
                Dim oldHeader As String = ""
                InitialRow = 5
                CurrTicker = ""
                Dim flag As Boolean = False
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                oSheet.Range("A" & Str(InitialRow) & "").Value = "AccontNo"
                oSheet.Range("B" & Str(InitialRow) & "").Value = "ASSETS"
                oSheet.Range("C" & Str(InitialRow) & "").Value = ddlYearTo.SelectedValue
                oSheet.Range("D" & Str(InitialRow) & "").Value = ddlYearTo.SelectedValue - 1
              
                InitialRow = InitialRow + 1
                CurrentRow = InitialRow
                While rsBankTran.Read()
                    flag = True
                    If CurrTicker.Trim.ToLower <> rsBankTran("HeaderAccNo").ToString().Trim.ToLower Then
                        If CurrTicker <> "" Then
                            '' '' update colum totals
                            CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                            CRange.Value = " Total " & oldHeader
                            CRange.Font.Bold = True
                            If BegBalTot.Length = 0 Then
                                BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                            Else
                                BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                                EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                            End If
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)"

                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            InitialRow = CurrentRow + 3
                            CurrentRow = InitialRow
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc")
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        Else
                            CurrTicker = rsBankTran("HeaderAccNo").ToString()
                            oldHeader = rsBankTran("HeaderDesc").ToString()
                            oSheet.Range("A" & Str(InitialRow) & "").Value = rsBankTran("HeaderAccNo")
                            oSheet.Range("B" & Str(InitialRow) & "").Value = rsBankTran("HeaderDesc")
                            oSheet.Range("B" & Str(InitialRow) & "").Font.Bold = True
                        End If
                    End If
                    CurrentRow = CurrentRow + 1
                    CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("AccNo")
                    CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Description")

                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                    CRange.Value = rsBankTran("PSAmount")
                    CRange.NumberFormat = "#,##0_);(#,##0)"

                  
                End While

                If flag = False Then
                    lblErr.Text = "no record to Show"
                    Exit Sub
                Else
                    If CurrTicker <> "" Then
                        ''update colum totals
                        CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                        CRange.Value = " Total " & oldHeader
                        CRange.Font.Bold = True
                        If BegBalTot.Length = 0 Then
                            BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                        Else
                            BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                            EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                        End If
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                        oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "$* #,##0_);_($* (#,##0)" '"#,##0.00_);(#,##0.00)"

                        CurrentRow = CurrentRow + 1
                    End If
                End If


                CurrentRow = CurrentRow + 1
                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "Investments"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 1)))
                CRange.Value = 0
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True


                If BegBalTot.Length = 0 Then
                    BegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                    EndBalTot = "=D" & Trim(Str(CurrentRow + 1))
                Else
                    BegBalTot = BegBalTot & "+C" & Trim(Str(CurrentRow + 1))
                    EndBalTot = EndBalTot & "+D" & Trim(Str(CurrentRow + 1))
                End If


                CurrentRow = CurrentRow + 2
                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "TOTAL ASSETS"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                CRange.Formula = BegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 1)))
                CRange.Value = EndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True



                Dim TBegBalTot, TEndBalTot, TPrTotal, TTotBalTot As String
                TBegBalTot = "=C" & Trim(Str(CurrentRow + 1))
                TEndBalTot = "=D" & Trim(Str(CurrentRow + 1))
               
                CurrentRow = CurrentRow + 4




                'LIABILITIES AND NET ASSETS


                InitialRow = CurrentRow
                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "LIABILITIES AND NET ASSETS"
                CRange.Font.Bold = True

                CurrentRow = CurrentRow + 1


                'Liabilities


                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 1)))
                CRange.Value = "Liabilities"
                CRange.Font.Bold = True

                CurrentRow = CurrentRow + 2

                CRange = oSheet.Range("B" & Trim(Str(CurrentRow + 3)))
                CRange.Value = "Total LIABILITIES AND NET ASSETS"
                CRange.Font.Bold = True

                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 3)))
                CRange.Formula = TBegBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True

                CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 3)))
                CRange.Value = TEndBalTot
                CRange.NumberFormat = "$* #,##0_);_($* (#,##0)"
                CRange.Font.Bold = True



                CRange = oSheet.Range("A" & Trim(Str(CurrentRow + 6)))
                CRange.Value = "The accompanying notes are an integral part of these statements."

                'Save the worksheet
                'Stream workbook 
                oSheet.Name = "BS_" & ddlYearFrom.SelectedValue
                Response.Clear()
                Response.ContentType = "application/vnd.ms-excel"
                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                oWorkbooks.SaveAs(Response.OutputStream)
                Response.End()
                '' End of Excel Generation
            Else
                '' Grid to Show
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, sSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    Dim row As DataRow = ds.Tables(0).NewRow
                    Dim i As Integer
                    Dim URAmount, TRAmount, PRAmount As Decimal
                    URAmount = 0.0
                    TRAmount = 0.0
                    PRAmount = 0.0
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        URAmount = URAmount + ds.Tables(0).Rows(i)("URAmount")
                        TRAmount = TRAmount + ds.Tables(0).Rows(i)("TRAmount")
                        PRAmount = PRAmount + ds.Tables(0).Rows(i)("PRAmount")
                    Next
                    row("URAmount") = URAmount
                    row("TRAmount") = TRAmount
                    row("PRAmount") = PRAmount

                    row("Description") = "Total"
                    ds.Tables(0).Rows.InsertAt(row, ds.Tables(0).Rows.Count + 1)
                    dgIS.DataSource = ds.Tables(0)
                    dgIS.DataBind()
                    dgIS.Visible = True
                Else
                    dgIS.Visible = False
                    lblErr.Text = "No result to show"
                End If
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

End Class
