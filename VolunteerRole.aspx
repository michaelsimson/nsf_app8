<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" Inherits="VRegistration.VolunteerRole" CodeFile="VolunteerRole.aspx.vb" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
			<table>
				<tr>
					<td class="Heading" vAlign="top" noWrap align="left" colSpan="2">Select Volunteer 
						Role&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
					</td>
				</tr>
				<tr>
					<td>
						<table id="tblIndRole" width="100%" runat="server">
							<TBODY>
								<tr>
									<td class="ContentSubTitle" vAlign="top" noWrap align="left" colSpan="2"><asp:label id="lblIndName" CssClass="LargeFont" Runat="server"></asp:label></td>
								</tr>
								<tr>
									<td class="ContentSubTitle" vAlign="top" noWrap align="left" colSpan="2">You may 
										select upto 3 Roles from the entire list of Roles for the Individual
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left">
										<p>1. Organizational Support: non-event specific / General:</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkOrganizational" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left">
										<p>2. Chapters in US/Canada (including Regional Contests):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkUSChapters" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left">
										<p>3. National Finals (Only for this event):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkNationalFinals" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td style="HEIGHT: 18px" vAlign="top" noWrap align="left">
										<p>4. Chapters in India (Scholarships in India / Starting Contests in India / 
											Liaison work with India chapters):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkNSFIndia" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
				<tr>
					<td>
						<table id="tblSpouseRole" width="100%" runat="server">
							<TBODY>
								<tr>
									<td class="ContentSubTitle" vAlign="top" noWrap align="left" colSpan="2"><asp:label id="lblSPName" CssClass="LargeFont" Runat="server"></asp:label></td>
								</tr>
								<tr>
									<td class="ContentSubTitle" vAlign="top" noWrap align="left" colSpan="2">You may 
										select upto 3 Roles from the entire list of Roles for the Spouse
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left" style="height: 14px">
										<p>1. Organizational Support: non-event specific / General:</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkOrganizationalSP" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left">
										<p>2. Chapters in US/Canada (including Regional Contests):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkUSChaptersSP" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="left">
										<p>3. National Finals (Only for this event):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkNationalFinalsSP" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
								<tr>
									<td style="HEIGHT: 18px" vAlign="top" noWrap align="left">
										<p>4. Chapters in India (Scholarships in India / Starting Contests in India / 
											Liaison work with India chapters):</p>
									</td>
								</tr>
								<tr>
									<td vAlign="top" noWrap align="center" colSpan="2"><asp:checkboxlist id="chkNSFIndiaSP" runat="server" CssClass="SmallFont" RepeatColumns="3" RepeatDirection="Horizontal"></asp:checkboxlist></td>
								</tr>
							</TBODY>
						</table>
					</td>
				</tr>
				<tr>
					<td class="ItemCenter"><asp:label id="lblError" CssClass="SmallFont" Runat="server" ForeColor="#C00000" Visible="False"
							Font-Size="Medium"></asp:label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="left" colSpan="2"><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Continue"></asp:button></td>
				</tr>
				<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="Registration.apx?Action=2006Reg" Visible="False">Back to Profile Page</asp:hyperlink>
					</td>
				</tr>
			</table>
</div>
<</asp:Content>

 

 
 
 