Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic

Partial Class PrepareSchedule
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If LCase(Session("LoggedIn")) <> "true" Then
        '    Server.Transfer("maintest.aspx")
        'ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
        '    Server.Transfer("login.aspx?entry=v")
        'End If

        Session("RoleID") = "2"
        Session("LoginID")="4240"
        If Not Page.IsPostBack Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.Items.Insert(2, Convert.ToString(year - 1))
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))

            ''Options for Volunteer Name Selection

            'If (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
            Dim ds_Vol As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID  " & IIf(Session("RoleID") = 89, " Where I.AutoMemberID=" & Session("LoginID"), "") & " order by I.FirstName")
            If ds_Vol.Tables(0).Rows.Count > 0 Then
                ddlVolName.DataSource = ds_Vol
                ddlVolName.DataBind()
                If ds_Vol.Tables(0).Rows.Count > 1 Then
                    ddlVolName.Items.Insert(0, "Select Volunteer")
                    ddlVolName.SelectedIndex = 0
                End If
            Else
                lblError.Text = "No Volunteers present for Calendar Sign up"
            End If

            'End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
                LoadProductGroup()
            Else
                If Session("RoleId").ToString() = "89" Then
                    Dim ds As DataSet
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                        'more than one 
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        LoadProductGroup()
                    Else
                        'only one
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                            lblPrd.Text = prd
                            lblPrdGrp.Text = Prdgrp
                        End If
                        LoadProductGroup()
                    End If
                End If
            End If
            LoadRole()
            LoadVolunteer()
            LoadEvent(ddlEvent)
            LoadWeekdays()
            'LoadGrid(DGSchedule)
            lblError.Text = ""
            oDiv.Attributes.Add("onscroll", "setonScroll(this,'" + DGSchedule.ClientID + "');")
        End If
    End Sub
    Public Sub LoadRole()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct RoleID,RoleCode From Volunteer Where RoleID in (" & Session("RoleID") & ")")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRole.DataSource = ds
            ddlRole.DataBind()
        End If

    End Sub

    Public Sub LoadVolunteer()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.FirstName +''+I.LastName as Name,C.MemberID From CalSignUp C Inner Join IndSpouse I on I.AutoMemberID=C.MemberID " & IIf(Session("RoleID") = 89, " where C.MemberID=" & Session("LoginID"), ""))
        If ds.Tables(0).Rows.Count > 0 Then
            ddlVolName.DataSource = ds
            ddlVolName.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlVolName.Items.Insert(0, New ListItem("Select Event", "-1"))
            Else
                ddlVolName.Enabled = False
            End If
            ddlVolName.SelectedIndex = 0
        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        ' "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
            End If
            ddlObject.SelectedIndex = 0
        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Private Sub LoadWeekdays()
        For i As Integer = 1 To 7
            ddlWeekDay.Items.Add(WeekdayName(i))
        Next
        ddlWeekDay.SelectedIndex = ddlWeekDay.Items.IndexOf(ddlWeekDay.Items.FindByText("Saturday"))
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblError.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
            End If
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & "  order by P.ProductID " 'and P.Status='O'
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, "Select Product")
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                End If
            End If
        Catch ex As Exception
            lblError.Text = lblError.Text & "<br>" & ex.ToString
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            'If ddlVolName.SelectedItem.Text = "" Then 'Session("RoleID") = "89" And
            '    lblError.Text = "Please select Volunteer"
            'Else
            If ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblError.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblError.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblError.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblError.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblError.Text = "Please select Product"
            Else
                lblError.Text = ""
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Phase=" & ddlPhase.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue) > 0 Then '< 1 Then '" & IIf(Session("RoleID") = "89", " Memberid=" & ddlVolName.SelectedValue & " and ", "") & "
                    LoadGrid(DGSchedule)
                Else
                    DGSchedule.Visible = False
                    btnExport.Enabled = False
                    lblError.Text = "No record Exists."
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Function WeekDayNumber(ByVal weekName As String) As Integer
        Dim weekNames As New Dictionary(Of String, Integer)
        For i As Integer = 1 To 7
            weekNames.Add(WeekdayName(i), i)
        Next
        Return weekNames(weekName)
    End Function

    Public Sub LoadGrid(ByVal DGSchedule)
        Try
            Dim StrSQl As String = ""
            Dim ds As DataSet
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignUp Where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & " And ProductGroupID = " & ddlProductGroup.SelectedValue & " And ProductID = " & ddlProduct.SelectedValue & "") > 0 Then '" & IIf(Session("RoleID") = 89, " MemberID=" & Session("LoginID") & " and ", "") & "

                Dim prmArray(12) As SqlParameter
                prmArray(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
                prmArray(1) = New SqlParameter("@EventID", ddlEvent.SelectedValue)
                prmArray(2) = New SqlParameter("@Phase", ddlPhase.SelectedValue)
                prmArray(3) = New SqlParameter("@ProductGroupID", ddlProductGroup.SelectedValue)
                prmArray(4) = New SqlParameter("@ProductID", ddlProduct.SelectedValue)
                'prmArray(5) = New SqlParameter("@MemberID", IIf(Session("RoleID") = 89, Session("LoginID"), "NULL"))

                Dim j As Integer = -2 '' Set to assign weekday names for Headers in the DataGris at appropriate Columns.
                Dim WeekDayVal As Integer = WeekDayNumber(ddlWeekDay.SelectedItem.Text)
                '***********Assigning Weekday parameters for the SP*************'
                For i As Integer = 5 To 11
                    j = j + 4
                    If WeekDayVal > 7 Then
                        WeekDayVal = WeekDayVal - 7
                    End If

                    prmArray(i) = New SqlParameter("@Day" & i - 4, WeekdayName(WeekDayVal))
                    DGSchedule.Columns(j).HeaderText = WeekdayName(WeekDayVal) '.ToString.Substring(0, 3) ' "Sunday"

                    WeekDayVal = WeekDayVal + 1
                Next

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_PrepSchCalSignup", prmArray)
                'Dim dt As DataTable = ds.Tables(0)

                If ds.Tables(0).Rows.Count > 0 Then
                    DGSchedule.DataSource = ds
                    DGSchedule.DataBind()

                    '************************Enable only the filled Cells in Grid****************************'
                    For Each row As DataGridItem In DGSchedule.Items
                        'For Each DataGridItem In DGSchedule.Items
                        Dim l As Integer = 2
                        For k As Integer = 0 To 6
                            If row.Cells(l).Text.ToString() = " " Then 'coltext.Length > 0 Then
                                DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).Enabled = False '.Text()
                            Else
                            End If
                            l = l + 4
                        Next
                    Next
                    '******************************************************************************************'
                    DGSchedule.Visible = True
                    btnExport.Enabled = True
                Else
                    lblError.Text = "No Data Exists for the selections made."
                End If

            Else
                lblError.Text = "No Data Exists."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub clear()
        lblError.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProductGroup.Enabled = False
        ddlProduct.Items.Clear()
        ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlPhase.SelectedIndex = 0
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        DGSchedule.Visible = False
        DGSchedule.Visible = False
        LoadEvent(ddlEvent)

    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblError.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
            lblError.Text = "Please select Valid Product"
        Else
            'LoadGrid(DGSchedule)
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadProductID()
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblError.Text = "Please select Valid Product"
        Else
            ' LoadGrid(DGSchedule)
        End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblError.Text = ""
    End Sub

    Protected Sub btnSaveApprovals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprovals.Click
        Try
            Dim StrUpdate As String = ""
            'For i As Integer = 0 To DGSchedule.Items.Count - 1
            'For Each DataGridRow In DGSchedule.Items
            For Each row As DataGridItem In DGSchedule.Items
                'For Each DataGridItem In DGSchedule.Items
                Dim j As Integer = 2
                For k As Integer = 0 To 6
                    If row.Cells(j).Text <> " " Then 'coltext.Length > 0 Then
                        'Accepted = DirectCast(row.FindControl("txtAcc" & g), TextBox).Text().Trim()
                        StrUpdate = StrUpdate & " Update CalSignUp Set Accepted=" & IIf(DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Text.Trim() = "Sel", "NULL", "'" & DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Value.Trim() & "'") & ",ModifiedBy=" & Session("LoginId") & ",ModifiedDate=GETDATE() Where MemberID=" & row.Cells(0).Text & " and CONVERT(varchar(15),CAST(TIME AS TIME),100) ='" & row.Cells(j).Text & "' and Day ='" & DGSchedule.Columns(j).HeaderText & "' and EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & ";"
                    Else
                    End If
                    j = j + 4
                Next
            Next
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate) > 0 Then
                lblError.Text = " Scheduled Successfully."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    'Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim Accepted As String = ""
    '    Dim dr As DataRow = CType(Cache("editRow"), DataRow)

    '    If (Not dr Is Nothing) Then
    '        If (Not dr.Item("Accepted") Is DBNull.Value) Then
    '            Accepted = dr.Item("Accepted")
    '        End If
    '        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
    '        ddlTemp = sender
    '        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
    '    Else
    '        Return
    '    End If
    'End Sub
    Protected Sub DGSchedule_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGSchedule.PageIndexChanged
        DGSchedule.CurrentPageIndex = e.NewPageIndex
        DGSchedule.EditItemIndex = -1
        LoadGrid(DGSchedule)
    End Sub
    'Protected Sub DGSchedule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGSchedule.ItemCreated
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    For i As Integer = 0 To 6
    '        '        If e.Item.Cells(i + 2).Text = "" Then '.FindControl("lblEditRegDeadLine")) Is Nothing Then
    '        '            CType(e.Item.Cells(5).FindControl("ddlAccept" & i + 1), DropDownList).Enabled = False
    '        '        End If
    '        '    Next
    '        'Next
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    Dim j As Integer = -2
    '        '    For i As Integer = 1 To 7
    '        '        j = j + 4
    '        '        If e.Item.Cells(j).Text = "" Then '.FindControl("lblEditRegDeadLine")) Is Nothing Then
    '        '            CType(e.Item.Cells(j + 3).FindControl("ddlAccept" & i), DropDownList).Enabled = False
    '        '            Response.Write(i & "***" & j & "$$$" & "ddlAccept" & i & "<br />")
    '        '        End If
    '        '        DGSchedule.Columns(j).HeaderText = WeekdayName(WeekDayVal) '.ToString.Substring(0, 3) ' "Sunday"
    '        '        i = i + 1
    '        '        WeekDayVal = WeekDayVal + 1
    '        '    Next
    '        'Next
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    'For Each DataGridItem In DGSchedule.Items
    '        '    Dim j As Integer = 2
    '        '    For k As Integer = 0 To 6
    '        '        If row.Cells(j).Text = "" Then 'coltext.Length > 0 Then
    '        '            'Accepted = DirectCast(row.FindControl("txtAcc" & g), TextBox).Text().Trim()
    '        '            CType(e.Item.Cells(j + 2).FindControl("ddlAccept" & k + 1), DropDownList).Enabled = False
    '        '            row.Cells(j + 2)..Enabled = False
    '        '            ' StrUpdate = StrUpdate & " Update CalSignUp Set Accepted='" & DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Value.Trim() & "' Where MemberID=" & row.Cells(0).Text & " and CONVERT(varchar(15),CAST(TIME AS TIME),100) ='" & row.Cells(j).Text & "' and Day ='" & DGSchedule.Columns(j).HeaderText & "' and EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & ";"
    '        '        Else
    '        '        End If
    '        '        j = j + 4
    '        '    Next
    '        'Next
    '    End If
    'End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Prepare_Schedule_" & ddlEventYear.SelectedItem.Text & "_" & ddlEvent.SelectedItem.Text & "_" & ddlProduct.SelectedItem.Text & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        Dim dt As DataTable = LoadTable()
        dgexport.DataSource = dt
        dgexport.DataBind()
        'LoadGrid(dgexport)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Function LoadTable() As DataTable
        Dim dt1 As DataTable = New DataTable()

        'With dt1.Columns
        dt1 = New DataTable()
        dt1.Columns.Add("CoachName", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(2).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S1", Type.GetType("System.String"))
        dt1.Columns.Add("P1", Type.GetType("System.String"))
        dt1.Columns.Add("A1", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(6).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S2", Type.GetType("System.String"))
        dt1.Columns.Add("P2", Type.GetType("System.String"))
        dt1.Columns.Add("A2", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(10).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S3", Type.GetType("System.String"))
        dt1.Columns.Add("P3", Type.GetType("System.String"))
        dt1.Columns.Add("A3", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(14).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S4", Type.GetType("System.String"))
        dt1.Columns.Add("P4", Type.GetType("System.String"))
        dt1.Columns.Add("A4", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(18).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S5", Type.GetType("System.String"))
        dt1.Columns.Add("P5", Type.GetType("System.String"))
        dt1.Columns.Add("A5", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(22).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S6", Type.GetType("System.String"))
        dt1.Columns.Add("P6", Type.GetType("System.String"))
        dt1.Columns.Add("A6", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(26).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S7", Type.GetType("System.String"))
        dt1.Columns.Add("P7", Type.GetType("System.String"))
        dt1.Columns.Add("A7", Type.GetType("System.String"))

        ' End With

        Dim row As DataGridItem
        Dim drow As DataRow
        For Each row In DGSchedule.Items  '.Rows
            ' For i As Integer = 0 To DGSchedule.Items.Count - 1
            drow = dt1.NewRow()

            drow("CoachName") = row.Cells(1).Text 'row.Cells("chk").Value

            drow(DGSchedule.Columns(2).HeaderText) = row.Cells(2).Text
            drow("S1") = row.Cells(3).Text
            drow("P1") = row.Cells(4).Text

            Dim ddlCountry As DropDownList = DirectCast(DGSchedule.FindControl("ddlAccept1"), DropDownList)
            Dim str As String

            'Dim ddl1 As DropDownList = row.FindControl("")


            'row.FindControl("A1").ToString()
            Dim name As DropDownList = TryCast(row.FindControl("ddlAccept1"), DropDownList)
            str = name.SelectedItem.Text
            drow("A1") = ddlCountry.SelectedItem.Text
            drow(DGSchedule.Columns(6).HeaderText) = row.Cells(6).Text
            drow("S2") = row.Cells(7).Text
            drow("P2") = row.Cells(8).Text
            drow("A2") = row.Cells(9).Text

            drow(DGSchedule.Columns(10).HeaderText) = row.Cells(10).Text
            drow("S3") = row.Cells(11).Text
            drow("P3") = row.Cells(12).Text
            drow("A3") = row.Cells(13).Text

            drow(DGSchedule.Columns(14).HeaderText) = row.Cells(14).Text
            drow("S4") = row.Cells(15).Text
            drow("P4") = row.Cells(16).Text
            drow("A4") = row.Cells(17).Text

            drow(DGSchedule.Columns(18).HeaderText) = row.Cells(18).Text
            drow("S5") = row.Cells(19).Text
            drow("P5") = row.Cells(20).Text
            drow("A5") = row.Cells(21).Text

            drow(DGSchedule.Columns(22).HeaderText) = row.Cells(22).Text
            drow("S6") = row.Cells(23).Text
            drow("P6") = row.Cells(24).Text
            drow("A6") = row.Cells(25).Text

            drow(DGSchedule.Columns(26).HeaderText) = row.Cells(26).Text
            drow("S7") = row.Cells(27).Text
            drow("P7") = row.Cells(28).Text
            drow("A7") = row.Cells(29).Text

            dt1.Rows.Add(drow)
        Next

        Return dt1

    End Function

    Private Function DropDownList(p1 As Object) As DropDownList
        Throw New NotImplementedException
    End Function

    Private Function e() As Object
        Throw New NotImplementedException
    End Function

    Private Function DropDownList() As WebControls.DropDownList
        Throw New NotImplementedException
    End Function

End Class

