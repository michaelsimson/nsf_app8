﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class FacilitiesAvailability
    Inherits System.Web.UI.Page
    Public dt, dt1 As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        'If (Session("RoleID") = 1 Or Session("RoleID") = 2) Then
        ''To Add Values to DropDownList
       
        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            Dim RoleID As Integer = Convert.ToInt32(Session("RoleID"))
            If ((RoleID = 3) Or (RoleID = 5) Or (RoleID = 52)) Then
                ddlChapter.SelectedValue = Session("selChapterID")
                ddlChapter.SelectedItem.Text = Session("selChapterName").ToString()
                ddlChapter.Enabled = False
                If (ddlChapter.SelectedValue = "1") Then
                    ddlEvent.SelectedValue = "1"
                Else
                    ddlEvent.SelectedValue = "2"
                End If
                LoadDBData()
            End If

                lblHeading.Text = ddlPurpose.SelectedItem.Text
            End If
                ' End If
    End Sub

    Private Sub GetContestYear(ByVal ddlYear As DropDownList)

        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        ddlPurpose.DataBind()
        ' ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Name,ProductGroupId from ProductGroup Where EventId =" & ddlEvent.SelectedValue)
        ddlProductGroup.DataSource = ds
        ddlProductGroup.DataBind()
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        ddlChapter.DataTextField = "ChapterCode"
        ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub

    Protected Sub BtnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAddUpdate.Click
        Dim StrSQL As String = ""
        Dim StrSQLValue As String = ""
        Dim StrSQLInsert As String = ""
        Dim StrEvalStr As String = ""
        Dim StrUpdateStr As String = ""
        Dim StrSQLExec As String = ""
        If ddlEvent.SelectedIndex = 0 Then
            lblAddUpdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUpdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUpdate.Text = "Please Select Chapter"
            Exit Sub

        ElseIf ddlBldgID.Text = "" Then
            lblAddUpdate.Text = "Please Select Building ID"
            Exit Sub
        ElseIf ddlBldgName.Text = "" Then
            lblAddUpdate.Text = "Please Select Building Name"
            Exit Sub
        ElseIf ddlRoomNo.Text = "" Then
            lblAddUpdate.Text = "Please Select Room Number"
            Exit Sub
        End If

        'Insertion for new Record
        StrSQL = "Insert into FacilitiesSchedule (ContestYear,ChapterID,Chapter,EventID,Event,Purpose,ProductGroupId,ProductGroup,Phase,Day,BldgID,BldgName,RoomNo,Floor,Capacity,"
        StrSQL = StrSQL & "Podium,PodMic,PodLapTop,OverheadProj,JudTable,JudChairs,CJMic,ChildMic,AudMixer,ChMicStand,DesktopMicStand,AudioWires,ExtraMikes,Laptop,Tables,Chairs,MicStand,IntAccess,CreatedBy,CreatedDate)"
        StrSQLValue = StrSQLValue & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "'," & IIf(TrProductGroup.Visible = True, ddlProductGroup.SelectedValue, "NULL") & ",'" & IIf(TrProductGroup.Visible = True, ddlProductGroup.SelectedItem.Text, "NULL") & "'," & IIf(TrPhase.Visible = True, ddlPhase.SelectedValue, "NULL") & "," & IIf(TrDay.Visible = True, ddlDay.SelectedValue, "NULL") & ",'" & ddlBldgID.SelectedItem.Text & "','" & ddlBldgName.SelectedItem.Text & "','" & ddlRoomNo.SelectedItem.Text & "'," & ddlFloor.SelectedItem.Text & "," & txtCapacity.Text & "," & ""
        StrSQLValue = StrSQLValue & IIf(TrPodium.Visible = True, txtPodium.Text, "NULL") & "," & IIf(TrPodiumMic.Visible = True, txtPodiumMic.Text, "NULL") & "," & IIf(TrPodiumLaptop.Visible = True, txtPodiumLaptop.Text, "NULL") & "," & IIf(TrProjector.Visible = True, txtProjector.Text, "NULL") & "," & IIf(TrJudgesTable.Visible = True, txtJTable.Text, "NULL") & "," & IIf(TrJudgeChairs.Visible = True, txtJChair.Text, "NULL") & "," & IIf(TrCJMic.Visible = True, txtCJMic.Text, "NULL") & "," & IIf(TrChildMic.Visible = True, txtChMic.Text, "NULL") & "," & IIf(TrAudioMixer.Visible = True, txtAMixer.Text, "NULL") & ","
        StrSQLValue = StrSQLValue & IIf(TrChildMicStand.Visible = True, txtChMicStand.Text, "NULL") & "," & IIf(TrDesktopMicStand.Visible = True, txtDsMcStand.Text, "NULL") & "," & IIf(TrAudioWire.Visible = True, txtAWires.Text, "NULL") & "," & IIf(TrExtraMikes.Visible = True, txtExtraMic.Text, "NULL") & "," & IIf(TrLaptop.Visible = True, txtLaptop.Text, "NULL") & "," & IIf(TrTable.Visible = True, txtTable.Text, "NULL") & "," & IIf(TrChair.Visible = True, txtChair.Text, "NULL") & "," & IIf(TrMicStand.Visible = True, txtMicStand.Text, "NULL") & "," & IIf(TrInternetAccess.Visible = True, txtIntAccess.Text, "NULL") & "," & Session("LoginID") & ",GETDATE())"
        StrSQLInsert = StrSQL & StrSQLValue

        'Updating for Existing record
        StrUpdateStr = StrUpdateStr & "Update FacilitiesSchedule set Floor=" & ddlFloor.SelectedItem.Text & ",Capacity=" & txtCapacity.Text & ",Podium=" & IIf(TrPodium.Visible = True, txtPodium.Text, "NULL") & ",PodMic=" & IIf(TrPodiumMic.Visible = True, txtPodiumMic.Text, "NULL") & ",PodLapTop=" & IIf(TrPodiumLaptop.Visible = True, txtPodiumLaptop.Text, "NULL") & ",OverheadProj=" & IIf(TrProjector.Visible = True, txtProjector.Text, "NULL") & ",JudTable=" & IIf(TrJudgesTable.Visible = True, txtJTable.Text, "NULL") & ",JudChairs=" & IIf(TrJudgeChairs.Visible = True, txtJChair.Text, "NULL") & ",CJMic=" & IIf(TrCJMic.Visible = True, txtCJMic.Text, "NULL") & ",ChildMic=" & IIf(TrChildMic.Visible = True, txtChMic.Text, "NULL") & ",AudMixer=" & IIf(TrAudioMixer.Visible = True, txtAMixer.Text, "NULL") & ""
        StrUpdateStr = StrUpdateStr & ",ChMicStand=" & IIf(TrChildMicStand.Visible = True, txtChMicStand.Text, "NULL") & ",DesktopMicStand=" & IIf(TrDesktopMicStand.Visible = True, txtDsMcStand.Text, "NULL") & ",AudioWires=" & IIf(TrAudioWire.Visible = True, txtAWires.Text, "NULL") & ",ExtraMikes=" & IIf(TrExtraMikes.Visible = True, txtExtraMic.Text, "NULL") & ",Laptop=" & IIf(TrLaptop.Visible = True, txtLaptop.Text, "NULL") & ",Tables=" & IIf(TrTable.Visible = True, txtTable.Text, "NULL") & ",Chairs=" & IIf(TrChair.Visible = True, txtChair.Text, "NULL") & ",MicStand=" & IIf(TrMicStand.Visible = True, txtMicStand.Text, "NULL") & ",IntAccess=" & IIf(TrInternetAccess.Visible = True, txtIntAccess.Text, "NULL") & ", ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GETDATE() "
        StrUpdateStr = StrUpdateStr & "Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and " & IIf(ddlProductGroup.Visible = True, "ProductGroupId=" & ddlProductGroup.SelectedValue & " and Phase=" & ddlPhase.SelectedValue, "Day=" & ddlDay.SelectedValue & "") & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and BldgName='" & ddlBldgName.SelectedItem.Text & "' and RoomNo='" & ddlRoomNo.SelectedItem.Text & "'"

        Try
            StrEvalStr = "Select Count(*) From FacilitiesSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and " & IIf(ddlProductGroup.Visible = True, "ProductGroupId=" & ddlProductGroup.SelectedValue & " and Phase=" & ddlPhase.SelectedValue, " Day=" & ddlDay.SelectedValue & "") & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and BldgName='" & ddlBldgName.SelectedItem.Text & "' and RoomNo='" & ddlRoomNo.SelectedItem.Text & "'"
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrEvalStr) = 0 Then
                StrSQLExec = StrSQLInsert
            Else
                StrSQLExec = StrUpdateStr
            End If
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQLExec)
            lblAddUpdate.Text = "Added/Updated Successfully"
            LoadGrid()
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        lblAddUpdate.Text = ""
    End Sub

    Private Sub LoadGrid()

        DGFacilities.Visible = True
        Dim StrSQL As String = ""

        StrSQL = "SELECT Chapter, ContestYear,Purpose, BldgID ,BldgName, RoomNo,Floor ,Capacity,Podium,PodMic,PodLaptop," ',InfoDesk,RecepDesk,NSFDesk,ChildAct,ParentAct,Awards,Grading,Judges,Food,Registration,Sponsors
        StrSQL = StrSQL + " OverHeadProj,JudTable,JudChairs,CJMic,ChildMic,AudMixer,ChMicStand,DesktopMicStand,AudioWires,ExtraMikes,Laptop,Tables,Chairs,MicStand,IntAccess "
        StrSQL = StrSQL + " FROM FacilitiesSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and " & IIf(ddlProductGroup.Visible = True, "ProductGroupId=" & ddlProductGroup.SelectedValue & " and Phase=" & ddlPhase.SelectedValue, "Day=" & ddlDay.SelectedValue & "") & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and BldgName='" & ddlBldgName.SelectedItem.Text & "' and RoomNo='" & ddlRoomNo.SelectedItem.Text & "'"
        'WHERE(ContestYear = " & ddlYear.SelectedValue & " And EventID = " + ddlEvent.SelectedValue + " And ChapterID = " + ddlChapter.SelectedValue + """)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

        DGFacilities.DataSource = ds
        DGFacilities.DataBind()
        For i As Integer = 8 To 25
            DGFacilities.Columns(i).Visible = True
        Next


        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Then '3
            DGFacilities.Columns(21).Visible = False
            DGFacilities.Columns(22).Visible = False
            DGFacilities.Columns(23).Visible = False
            DGFacilities.Columns(24).Visible = False
            DGFacilities.Columns(25).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Awards" Then '1
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ParentAct" Then '9
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(20).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ChildAct" Then '2
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(21).Visible = False
            DGFacilities.Columns(24).Visible = False
            DGFacilities.Columns(25).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Judges" Then '7
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(24).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then '5
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(24).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Registration" Then '11

            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(24).Visible = False
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(21).Visible = False
            DGFacilities.Columns(24).Visible = False
            DGFacilities.Columns(25).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Sponsors" Then
            DGFacilities.Columns(8).Visible = False
            DGFacilities.Columns(9).Visible = False
            DGFacilities.Columns(10).Visible = False
            DGFacilities.Columns(11).Visible = False
            DGFacilities.Columns(12).Visible = False
            DGFacilities.Columns(13).Visible = False
            DGFacilities.Columns(14).Visible = False
            DGFacilities.Columns(15).Visible = False
            DGFacilities.Columns(16).Visible = False
            DGFacilities.Columns(17).Visible = False
            DGFacilities.Columns(18).Visible = False
            DGFacilities.Columns(19).Visible = False
            DGFacilities.Columns(20).Visible = False
            DGFacilities.Columns(21).Visible = False
            DGFacilities.Columns(24).Visible = False
            DGFacilities.Columns(25).Visible = False
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Food" Then '4
            DGFacilities.Visible = False

        End If
    End Sub
    Private Sub LoadValues()
        Dim SQLRoomReq As String = ""
        SQLRoomReq = "Select Distinct ProductGroupId,ProductGroup,Day from RoomRequirement Where EventID=" & ddlEvent.SelectedValue & " and EventYear=" & ddlYear.SelectedValue & " and Purpose = '" & ddlPurpose.SelectedItem.Text.Trim() & "'" 'and chapterID=" & ddlChapter.SelectedValue & "
        Dim dsRoomReq As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
        If dsRoomReq.Tables(0).Rows.Count > 0 Then
            ddlProductGroup.DataSource = dsRoomReq
            ddlProductGroup.DataTextField = "ProductGroup"
            ddlProductGroup.DataValueField = "ProductGroupId"
            ddlProductGroup.DataBind()
            ddlProductGroup.Items.Insert(0, "Select Contest")

            ddlDay.DataSource = dsRoomReq
            ddlDay.DataTextField = "Day"
            ddlDay.DataValueField = "Day"
            ddlDay.DataBind()
            ddlDay.Items.Insert(0, "Select Day")
        End If
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDDLData()
        ClearRoomListData()
        ClearRoomReqData()
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblErrorMsg.Text = "Select Event/Chapter"
            ddlPurpose.SelectedIndex = 0
            Exit Sub
        Else
            lblErrorMsg.Text = ""
        End If
        lblHeading.Text = ddlPurpose.SelectedItem.Text

        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
            TrProductGroup.Visible = True
            TrPhase.Visible = True
            TrDay.Visible = False
            ddlDay.Items.Clear()
        Else
            TrDay.Visible = True
            TrProductGroup.Visible = False
            TrPhase.Visible = False
            ddlProductGroup.Items.Clear()
            ddlPhase.Items.Clear()
        End If

        LoadlblValues()
        LoadValues()
        LoadDBData()
        'LoadRoomReqData()
    End Sub
    Private Sub LoadlblValues()
        setDDLVisibility()
        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Then '3
            TrPodium.Visible = True
            TrPodiumMic.Visible = True
            TrPodiumLaptop.Visible = True
            TrProjector.Visible = True
            TrJudgesTable.Visible = True
            TrJudgeChairs.Visible = True
            TrCJMic.Visible = True
            TrChildMic.Visible = True
            TrAudioMixer.Visible = True
            TrChildMicStand.Visible = True
            TrDesktopMicStand.Visible = True
            TrAudioWire.Visible = True
            TrExtraMikes.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Awards" Then '1
            'TrRoomGuide.Visible = True
            'TrRoomMc.Visible = True
            TrProjector.Visible = True
            TrLaptop.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            'TrMic.Visible = True
            TrMicStand.Visible = True
            TrAudioMixer.Visible = True
            TrAudioWire.Visible = True
            TrInternetAccess.Visible = True
            TrExtraMikes.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ParentAct" Then '9
            'TrRoomGuide.Visible = True
            'TrRoomMc.Visible = True
            TrProjector.Visible = True
            TrLaptop.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            'TrMic.Visible = True
            TrMicStand.Visible = True
            TrAudioMixer.Visible = True
            TrAudioWire.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ChildAct" Then '2
            'TrRoomGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Judges" Then '7
            'TrRoomGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then '5
            'TrRoomGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Registration" Then '11
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Sponsors" Then
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Food" Then '4
            'TrTeamLead.Visible = True
            'TrFoodServe.Visible = True
        End If
    End Sub
    Private Sub setDDLVisibility()
        'TrRoomGuide.Visible = False
        'TrRoomMc.Visible = False
        'TrProctor.Visible = False
        TrPodium.Visible = False
        TrPodiumMic.Visible = False
        TrPodiumLaptop.Visible = False
        TrProjector.Visible = False
        TrJudgesTable.Visible = False
        TrJudgeChairs.Visible = False
        TrCJMic.Visible = False
        TrChildMic.Visible = False
        TrAudioMixer.Visible = False
        TrChildMicStand.Visible = False
        TrDesktopMicStand.Visible = False
        TrAudioWire.Visible = False
        TrExtraMikes.Visible = False
        TrInternetAccess.Visible = False
        'TrRoomMc.Visible = False
        TrLaptop.Visible = False
        TrTable.Visible = False
        TrChair.Visible = False
        'TrMic.Visible = False
        TrMicStand.Visible = False
    End Sub
    Private Sub LoadDBData()
        Try
            Dim SQlRoomList As String = ""
            Dim SQLRoomReq As String = ""
            '  Dim dsRoomlist As DataSet
            '  Dim dsRoomReq As DataSet
            SQlRoomList = "Select Distinct BldgID From RoomListFacilities Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & ""

            Dim dsRoomlist As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQlRoomList)
            If dsRoomlist.Tables(0).Rows.Count > 0 Then
                ddlBldgID.DataSource = dsRoomlist
                ddlBldgID.DataValueField = "BldgID"
                ddlBldgID.DataTextField = "BldgID"
                ddlBldgID.DataBind()
                ddlBldgID.Items.Insert(0, "Select Building ID")
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub ClearRoomReqData()
        ClearTRentData()
        lblPodiumReq.Text = ""
        lblPodiumMicReq.Text = ""
        lblPodiumLaptopReq.Text = ""
        'lblProctorReq.Text = ""
        lblProjectorReq.Text = ""
        lblJTableReq.Text = ""
        lblJChairReq.Text = ""
        lblCJMicReq.Text = ""
        lblChMicReq.Text = ""
        lblAMixerReq.Text = ""
        lblChMicStandReq.Text = ""
        lblDsMcStandReq.Text = ""
        lblAWiresReq.Text = ""
        lblExtraMicReq.Text = ""
        lblLaptopReq.Text = ""
        lblTableReq.Text = ""
        lblChairReq.Text = ""
        'lblMicReq.Text = ""
        lblMicStandReq.Text = ""
        lblIntAccessReq.Text = ""
    End Sub
    Private Sub ClearTRentData()
        DGFacilities.Visible = False
        txtPodium.Text = ""
        txtPodiumMic.Text = ""
        txtPodiumLaptop.Text = ""
        txtProjector.Text = ""
        txtJTable.Text = ""
        txtJChair.Text = ""
        txtCJMic.Text = ""
        txtChMic.Text = ""
        txtAMixer.Text = ""
        txtChMicStand.Text = ""
        txtDsMcStand.Text = ""
        txtAWires.Text = ""
        txtExtraMic.Text = ""
        txtLaptop.Text = ""
        txtTable.Text = ""
        txtChair.Text = ""
        txtMicStand.Text = ""
        txtIntAccess.Text = ""
    End Sub
    Private Sub LoadTRent()
        'Requires - Available = To be Rented
        txtPodium.Text = IIf(TrPodium.Visible = True, lblPodiumReq.Text - txtPodiumAvl.Text, "NULL")
        txtPodiumMic.Text = IIf(TrPodiumMic.Visible = True, lblPodiumMicReq.Text - txtPodiumMicAvl.Text, "NULL")
        txtPodiumLaptop.Text = IIf(TrPodiumLaptop.Visible = True, lblPodiumLaptopReq.Text - txtPodiumLaptopAvl.Text, "NULL")
        txtProjector.Text = IIf(TrProjector.Visible = True, lblProjectorReq.Text - txtProjectorAvl.Text, "NULL")
        txtJTable.Text = IIf(TrJudgesTable.Visible = True, lblJTableReq.Text - txtJTableAvl.Text, "NULL")
        txtJChair.Text = IIf(TrJudgeChairs.Visible = True, lblJChairReq.Text - txtJChairAvl.Text, "NULL")
        txtCJMic.Text = IIf(TrCJMic.Visible = True, lblCJMicReq.Text - txtCJMicAvl.Text, "NULL")
        txtChMic.Text = IIf(TrChildMic.Visible = True, lblChMicReq.Text - txtChMicAvl.Text, "NULL")
        txtAMixer.Text = IIf(TrAudioMixer.Visible = True, lblAMixerReq.Text - txtAMixerAvl.Text, "")
        txtChMicStand.Text = IIf(TrChildMicStand.Visible = True, lblChMicStandReq.Text - txtChMicStandAvl.Text, "NULL")
        txtDsMcStand.Text = IIf(TrDesktopMicStand.Visible = True, lblDsMcStandReq.Text - txtDsMcStandAvl.Text, "NULL")
        txtAWires.Text = IIf(TrAudioWire.Visible = True, lblAWiresReq.Text - txtAWiresAvl.Text, "")
        txtExtraMic.Text = IIf(TrExtraMikes.Visible = True, lblExtraMicReq.Text - txtExtraMicAvl.Text, "NULL")
        txtLaptop.Text = IIf(TrLaptop.Visible = True, lblLaptopReq.Text - txtLaptopAvl.Text, "NULL")
        txtTable.Text = IIf(TrTable.Visible = True, lblTableReq.Text - txtTableAvl.Text, "NULL")
        txtChair.Text = IIf(TrChair.Visible = True, lblChairReq.Text - txtChairAvl.Text, "NULL")
        txtMicStand.Text = IIf(TrMicStand.Visible = True, lblMicStandReq.Text - txtMicStandAvl.Text, "NULL")
        txtIntAccess.Text = IIf(TrInternetAccess.Visible = True, lblIntAccessReq.Text - txtIntAccessAvl.Text, "NULL")
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        ClearDDLData()
        ClearRoomListData()
        ClearRoomReqData()
        GetPurpose()
        LoadDBData()
    End Sub

    Private Sub ClearDDLData()
        lblAddUpdate.Text = ""
        ddlProductGroup.Items.Clear()
        ddlPhase.Items.Clear()
        ddlDay.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlBldgName.Items.Clear()
        ddlRoomNo.Items.Clear()
        ddlFloor.Items.Clear()
        txtCapacity.Text = ""
    End Sub
    Private Sub ClearRoomListData()
        ClearTRentData()
        ddlFloor.Items.Clear()
        txtCapacity.Text = ""
        txtCapacity.Text = ""
        txtPodiumAvl.Text = ""
        txtPodiumMicAvl.Text = ""
        txtPodiumLaptopAvl.Text = ""
        'txtProctorAvl.Text = ""
        txtProjectorAvl.Text = ""
        txtJTableAvl.Text = ""
        txtJChairAvl.Text = ""
        txtCJMicAvl.Text = ""
        txtChMicAvl.Text = ""
        txtAMixerAvl.Text = ""
        txtChMicStandAvl.Text = ""
        txtDsMcStandAvl.Text = ""
        txtAWiresAvl.Text = ""
        txtExtraMicAvl.Text = ""
        txtLaptopAvl.Text = ""
        txtTableAvl.Text = ""
        txtChairAvl.Text = ""
        'txtMicAvl.Text = ""
        txtMicStandAvl.Text = ""
        txtIntAccessAvl.Text = ""
    End Sub
    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        ddlRoomNo.Items.Clear()
        ClearRoomListData()
        Dim SQLBldgDt As String = ""
        Dim ds As DataSet
        SQLBldgDt = "Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLBldgDt) '"Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'")
        ddlBldgName.DataSource = ds
        ddlBldgName.DataTextField = "BldgName"
        ddlBldgName.DataValueField = "BldgName"
        ddlBldgName.DataBind()
        ddlBldgName.Items.Insert(0, "Select Building Name")

    End Sub

    Protected Sub ddlBldgName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgName.SelectedIndexChanged
        ddlRoomNo.Items.Clear()
        ClearRoomListData()
        Dim SQLBldgNameDt As String = ""
        Dim ds As DataSet
        SQLBldgNameDt = "Select Distinct RoomNumber From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "' and BldgName='" & ddlBldgName.SelectedItem.Text & "'"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLBldgNameDt) '"Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'")
        ddlRoomNo.DataSource = ds
        ddlRoomNo.DataTextField = "RoomNumber"
        ddlRoomNo.DataValueField = "RoomNumber"
        ddlRoomNo.DataBind()
        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
    End Sub

    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged
        ClearRoomListData()
        Dim SQLRoomList As String = ""
        Dim ds As DataSet
        SQLRoomList = "Select * From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "' and BldgName='" & ddlBldgName.SelectedItem.Text & "' and RoomNumber ='" & ddlRoomNo.SelectedItem.Text & "'"
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomList) '"Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'")
            ddlFloor.DataSource = ds
            ddlFloor.DataTextField = "Floor"
            ddlFloor.DataValueField = "Floor"
            ddlFloor.DataBind()

            txtCapacity.Text = ds.Tables(0).Rows(0)("Capacity")
            txtPodiumAvl.Text = ds.Tables(0).Rows(0)("Podium")
            txtPodiumMicAvl.Text = ds.Tables(0).Rows(0)("PodMic")
            txtPodiumLaptopAvl.Text = ds.Tables(0).Rows(0)("PodLT")
            'txtProctorAvl.Text = ds.Tables(0).Rows(0)("Podium")
            txtProjectorAvl.Text = ds.Tables(0).Rows(0)("Projector")
            txtJTableAvl.Text = ds.Tables(0).Rows(0)("JTable")
            txtJChairAvl.Text = ds.Tables(0).Rows(0)("JChair")
            txtCJMicAvl.Text = ds.Tables(0).Rows(0)("CJMic")
            txtChMicAvl.Text = ds.Tables(0).Rows(0)("ChildMic")
            txtAMixerAvl.Text = ds.Tables(0).Rows(0)("AMixer")
            txtChMicStandAvl.Text = ds.Tables(0).Rows(0)("ChMicStand")
            txtDsMcStandAvl.Text = ds.Tables(0).Rows(0)("DeskMicStand")
            txtAWiresAvl.Text = ds.Tables(0).Rows(0)("AWires")
            txtExtraMicAvl.Text = ds.Tables(0).Rows(0)("ExtraMic")
            txtLaptopAvl.Text = ds.Tables(0).Rows(0)("Laptop")
            txtTableAvl.Text = ds.Tables(0).Rows(0)("Tables")
            txtChairAvl.Text = ds.Tables(0).Rows(0)("Chair")
            'txtMicAvl.Text = ds.Tables(0).Rows(0)("Podium")
            txtMicStandAvl.Text = ds.Tables(0).Rows(0)("MicStand")
            txtIntAccessAvl.Text = ds.Tables(0).Rows(0)("IntAccess")

            LoadTRent()
        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        ClearRoomReqData()
        Dim SQLRoomReq As String = ""
        Dim ds As DataSet
        SQLRoomReq = "Select * From RoomRequirement Where EventId=" & ddlEvent.SelectedValue & " and EventYear =" & ddlYear.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomReq) '"Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'")
            FillRoomReqData(ds)
            LoadTRent()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlDay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDay.SelectedIndexChanged
        ClearRoomReqData()
        Dim SQLRoomReq As String = ""
        Dim ds As DataSet
        SQLRoomReq = "Select * From RoomRequirement Where EventId=" & ddlEvent.SelectedValue & " and EventYear =" & ddlYear.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Day=" & ddlDay.SelectedValue & ""

        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomReq) '"Select Distinct BldgName From RoomListFacilities Where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and BldgId='" + ddlBldgID.SelectedItem.Text + "'")
            FillRoomReqData(ds)
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try

    End Sub
    Private Sub FillRoomReqData(ByVal ds As DataSet)

        lblPodiumReq.Text = IIf(ds.Tables(0).Rows(0)("Podium") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Podium"))
        lblPodiumMicReq.Text = IIf(ds.Tables(0).Rows(0)("PodMic") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("PodMic"))
        lblPodiumLaptopReq.Text = IIf(ds.Tables(0).Rows(0)("PodLT") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("PodLT"))
        lblProjectorReq.Text = IIf(ds.Tables(0).Rows(0)("Projector") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Projector"))
        lblJTableReq.Text = IIf(ds.Tables(0).Rows(0)("JTable") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("JTable"))
        lblJChairReq.Text = IIf(ds.Tables(0).Rows(0)("JChair") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("JChair"))
        lblCJMicReq.Text = IIf(ds.Tables(0).Rows(0)("CJMic") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("CJMic"))
        lblChMicReq.Text = IIf(ds.Tables(0).Rows(0)("ChildMic") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("ChildMic"))
        lblAMixerReq.Text = IIf(ds.Tables(0).Rows(0)("AMixer") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("AMixer"))
        lblChMicStandReq.Text = IIf(ds.Tables(0).Rows(0)("ChMicStand") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("ChMicStand"))
        lblDsMcStandReq.Text = IIf(ds.Tables(0).Rows(0)("DeskMicStand") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("DeskMicStand"))
        lblAWiresReq.Text = IIf(ds.Tables(0).Rows(0)("AWires") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("AWires"))
        lblExtraMicReq.Text = IIf(ds.Tables(0).Rows(0)("ExtraMic") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("ExtraMic"))
        lblLaptopReq.Text = IIf(ds.Tables(0).Rows(0)("Laptop") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Laptop"))
        lblTableReq.Text = IIf(ds.Tables(0).Rows(0)("Tables") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Tables"))
        lblChairReq.Text = IIf(ds.Tables(0).Rows(0)("Chair") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Chair"))
        lblMicStandReq.Text = IIf(ds.Tables(0).Rows(0)("MicStand") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("MicStand"))
        lblIntAccessReq.Text = IIf(ds.Tables(0).Rows(0)("IntAccess") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("IntAccess"))

    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        GetChapter(ddlChapter)
        GetPurpose()
        ClearDDLData()
        ClearRoomListData()
        ClearRoomReqData()
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            LoadDBData()
        Else
            ddlChapter.Enabled = True
        End If
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        GetEvent()
        GetChapter(ddlChapter)
        GetPurpose()
        ClearDDLData()
        ClearRoomListData()
        ClearRoomReqData()
        'LoadDBData()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Dim SQLRoomReq As String = ""
        SQLRoomReq = "Select Distinct Phase from RoomRequirement Where EventID=" & ddlEvent.SelectedValue & " and EventYear=" & ddlYear.SelectedValue & " and Purpose = '" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & "" 'and chapterID=" & ddlChapter.SelectedValue & "
        Dim dsRoomReq As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
        If dsRoomReq.Tables(0).Rows.Count > 0 Then
            ddlPhase.DataSource = dsRoomReq
            ddlPhase.DataTextField = "Phase"
            ddlPhase.DataValueField = "Phase"
            ddlPhase.DataBind()
            ddlPhase.Items.Insert(0, "Select Phase")
        End If
    End Sub
End Class
