﻿Imports System.Reflection
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Imports System
Imports System.Data
Imports System.Collections
Imports NorthSouth.DAL
Partial Class RoomSchedule
    Inherits System.Web.UI.Page
    Dim dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        'If (Session("RoleID") = 1 Or Session("RoleID") = 2) Then
        ''To Add Values to DropDownList

        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            If Convert.ToInt32(Session("selChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("selChapterID")
                ddlChapter.SelectedItem.Text = Session("selChapterName").ToString()
                ddlChapter.Enabled = False
                If (ddlChapter.SelectedValue = "1") Then
                    ddlEvent.SelectedValue = "1"
                Else
                    ddlEvent.SelectedValue = "2"
                End If
                ddlEvent.Enabled = False
                'GetProductGroup(ddlProductGroup)
                GetDate()
                LoadBldgID()
            End If
            'LoadDisplayTime(ddlStartTime)
            'LoadDisplayTime(ddlEndTime)
        End If
        ' End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        ddlPurpose.DataTextField = "Purpose"
        ddlPurpose.DataValueField = "Purpose"
        ddlPurpose.DataBind()
        ' ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 3
    End Sub
    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        ddlChapter.DataTextField = "ChapterCode"
        ddlChapter.DataValueField = "ChapterId"
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        ddlProduct.Items.Clear()
        'ddlPhase.SelectedIndex = 0
        Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from Contest Where Contest_Year=" & ddlYear.SelectedValue & " and NSFChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ContestDate='" & ddlDate.SelectedValue & "'"

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlObject.DataSource = ds
        ddlObject.DataTextField = "ProductGroupCode"
        ddlObject.DataValueField = "ProductGroupId"
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, "Select ProductGroup")
        ddlObject.SelectedIndex = 0
    End Sub
    Private Sub GetDate()
        Dim StrDateSQL As String = ""
        StrDateSQL = "Select Distinct convert(VarChar(10), ContestDate, 101) as ContestDate From Contest Where Contest_Year=" & ddlYear.SelectedValue & " and NSFChapterID=" & ddlChapter.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
        ddlDate.DataSource = ds
        ddlDate.DataTextField = "ContestDate"
        ddlDate.DataValueField = "ContestDate"
        ddlDate.DataBind()
        ddlDate.Items.Insert(0, "Select ContestDate")
        ddlDate.SelectedIndex = 0
    End Sub
    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        'GetEvent()
        'GetChapter(ddlChapter)
        'GetPurpose()
        ClearList()
        GetDate()
        'GetProductGroup(ddlProductGroup)
        LoadBldgID()
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        TrCopy.Visible = False
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            ddlProduct.Items.Clear()
            ddlPhase.SelectedIndex = 0
            'GetProductGroup(ddlProductGroup)
            GetDate()
            LoadBldgID()
            Session("selChapterID") = 1
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        TrCopy.Visible = False
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        ddlProduct.Items.Clear()
        ddlPhase.SelectedIndex = 0
        'GetProductGroup(ddlProductGroup)
        GetDate()
        LoadBldgID()
        Session("selChapterID") = ddlChapter.SelectedValue
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrSeqUpdate.Visible = False
        ddlPhase.SelectedIndex = 0
        'ddlStartTime.SelectedIndex = 0
        'ddlEndTime.SelectedIndex = 0
        DGRoomSchedule.Visible = False
        'LoadBldgID()
        'LoadDGRoomSchedule()
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        ddlPhase.SelectedIndex = 0
        'ddlStartTime.SelectedIndex = 0
        'ddlEndTime.SelectedIndex = 0
        GetProduct(ddlProductGroup, ddlProduct)

        LoadDGRoomSchedule()
        'LoadDGRoomSchedule()
    End Sub

    Private Sub GetProduct(ByVal ddlobject As DropDownList, ByVal ddlObjectSub As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from Contest Where Contest_Year=" & ddlYear.SelectedValue & " and NSFChapterID=" & ddlChapter.SelectedValue & " and ProductGroupId=" & ddlobject.SelectedValue & " Order by ProductId"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlObjectSub.DataSource = ds
        ddlObjectSub.DataTextField = "ProductCode"
        ddlObjectSub.DataValueField = "ProductId"
        ddlObjectSub.DataBind()

    End Sub


    Protected Sub ddlProduct_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        ddlPhase.SelectedIndex = 0
        'ddlStartTime.SelectedIndex = 0
        'ddlEndTime.SelectedIndex = 0
        LoadDGRoomSchedule()
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadDGRoomSchedule()
        End If
    End Sub
    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrSeqUpdate.Visible = False
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
    End Sub
    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrSeqUpdate.Visible = False
    End Sub

    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)
        ddlRoomNo.Enabled = True
        Dim StrRoomNoSch As String = ""
        Dim StrRoomNoList As String = ""
        Dim StrRoomNoSQl As String = ""
        Dim ds1, ds2 As DataSet

        StrRoomNoSch = "Select Distinct RTRIM(RoomNumber) as RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'" 'ddlBldgId.SelectedItem.Text & "'"
        StrRoomNoList = "Select Distinct RTRIM(RoomNumber) as RoomNumber from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'" 'ddlBldgId.SelectedItem.Text & "'"

        ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSch)
        ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoList)

        If ds1.Tables(0).Rows.Count < ds2.Tables(0).Rows.Count Then
            StrRoomNoSQl = StrRoomNoList
        ElseIf ds1.Tables(0).Rows.Count = ds2.Tables(0).Rows.Count Then
            StrRoomNoSQl = StrRoomNoSch
        Else
            lblAddUPdate.Text = "Rooms List has fewer rooms "
            Exit Sub
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlRoomNo.DataSource = ds
        ddlRoomNo.DataTextField = "RoomNumber"
        ddlRoomNo.DataValueField = "RoomNumber"
        ddlRoomNo.DataBind()

        'txtCapacity.Text = ds.Tables(0).Rows(0)("Capacity")
        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0

    End Sub
    Private Sub LoadBldgID()
        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""
        Dim StrBldgRoomSch As String = ""
        Dim StrBldgRoomList As String = ""
        Dim RoomSchCnt As String = ""
        Dim RoomListCnt As String = ""

        Dim ds1, ds2 As DataSet

        StrBldgRoomSch = "Select Distinct RTRIM(BldgID) as BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & ""
        StrBldgRoomList = "Select Distinct RTRIM(BldgID) as BldgID from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & ""

        ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgRoomSch)
        ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgRoomList)

        If ds1.Tables(0).Rows.Count < ds2.Tables(0).Rows.Count Then
            StrBldgSQl = StrBldgRoomList
        ElseIf ds1.Tables(0).Rows.Count = ds2.Tables(0).Rows.Count Then
            StrBldgSQl = StrBldgRoomSch
        Else
            lblAddUPdate.Text = "Rooms List has fewer rooms "
            Exit Sub
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        'ddlRoomNo.Items.Clear()
        ddlBldgID.DataSource = ds
        ddlBldgID.DataTextField = "BldgID"
        ddlBldgID.DataValueField = "BldgID"
        ddlBldgID.DataBind()

        ddlBldgID.Items.Insert(0, "Select BldgID")
        ddlBldgID.SelectedIndex = 0
    End Sub
    Public Sub LoadDisplayTime(ByVal ddlObject As DropDownList)
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "6:00:00 AM " To "8:00:00 PM "
        While StartTime <= "8:00:00 PM "

            dr = dt.NewRow()
            dr("ddlText") = StartTime.ToString("hh:mm tt")
            dr("ddlValue") = StartTime.ToString("hh:mm tt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddMinutes(15)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        ddlObject.Items.Insert(0, "Select Start time")
        ddlObject.SelectedIndex = 0

        'ddlEndTime.DataSource = dt
        'ddlEndTime.DataTextField = "ddlText"
        'ddlEndTime.DataValueField = "ddlValue"
        'ddlEndTime.DataBind()
        'ddlEndTime.Items.Insert(0, "Select End time")
        'ddlEndTime.SelectedIndex = 0

    End Sub
    Private Sub LoadDGRoomSchedule()
        lblErr.Text = ""
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim ds1 As DataSet
        Dim lblCapcty As Label
        Dim i As Integer = 0
        Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)
        DGRoomSchedule.Columns(2).Visible = True
        DGRoomSchedule.DataSource = ds
        DGRoomSchedule.Columns(2).Visible = False
        DGRoomSchedule.DataBind()

        Try
            If ds.Tables(0).Rows.Count > 0 Then
                'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByText(ds.Tables(0).Rows(0)("StartTime").ToString))
                'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByText(ds.Tables(0).Rows(0)("EndTime").ToString))
            Else
                DGRoomSchedule.Visible = False
                'ddlStartTime.SelectedIndex = 0
                'ddlEndTime.SelectedIndex = 0
            End If

            For Each dgItem In DGRoomSchedule.Items
                lblCapcty = dgItem.FindControl("lblCapacity")
                ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId").ToString.Trim & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber").ToString.Trim & "'")
                lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")
                i = i + 1
            Next

        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try

    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click

        lblAddUPdate.Text = ""

        If ddlEvent.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Phase"
            Exit Sub
            'ElseIf ddlStartTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select StartTime"
            '    Exit Sub
            'ElseIf ddlEndTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select EndTime"
            '    Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlSeqNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select SequenceNo"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select RoomNumber"
            Exit Sub
        ElseIf Not txtStartBadge.Text.ToString().Equals("") Then
            Dim intStartBadge As Integer
            If Integer.TryParse(txtStartBadge.Text.ToString(), intStartBadge) Then
                intStartBadge = Convert.ToInt32(txtStartBadge.Text.ToString())
                If (intStartBadge < 0 Or intStartBadge > 200) Then
                    lblAddUPdate.Text = "The StartBadge values must of numerals between 1 and 200"
                    Exit Sub
                End If
            Else
                lblAddUPdate.Text = "The StartBadge values must of numerals between 1 and 200"
                Exit Sub
            End If
           
        ElseIf Not txtEndBadge.Text.ToString().Equals("") Then
            Dim intEndBadge As Integer
            If Integer.TryParse(txtEndBadge.Text.ToString(), intEndBadge) Then
                intEndBadge = Convert.ToInt32(txtEndBadge.Text.ToString())
                If (intEndBadge < 0 Or intEndBadge > 200) Then
                    lblAddUPdate.Text = "The EndBadge values must of numerals between 1 and 200"
                    Exit Sub
                End If
            Else
                lblAddUPdate.Text = "The EndBadge values must of numerals between 1 and 200"
                Exit Sub
            End If
        End If





        Try
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where SeqNo in(" & ddlSeqNo.SelectedValue & " )and ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and (BldgID<> '" & ddlBldgID.SelectedItem.Text & "' OR RoomNumber<>'" & ddlRoomNo.SelectedItem.Text & "')") > 0 Then
                [lblErr].Text = "Sequence Number Already exists for a record.Do You want to replace it?"
                TrSeqUpdate.Visible = True
            Else
                AddUpdateRoomSchData()
            End If
            'LoadDGRoomSchedule()
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try

    End Sub
    Private Sub AddUpdateRoomSchData()

        TrSeqUpdate.Visible = False
        lblErr.Text = ""

        Dim SQLRoomSchInsert As String = ""
        Dim SQLRoomSchUpdate As String = ""
        Dim SQLRoomSchEval As String = ""
        Dim SQLRoomSchExec As String = ""

        Try
            SQLRoomSchInsert = "Insert into RoomSchedule(ContestYear, ChapterID,Chapter, EventID,Event,Purpose ,Date,ProductGroupID,ProductGroupCode,ProductID ,ProductCode ,Phase,BldgID,SeqNo,RoomNumber,StartBadgeNo ,EndBadgeNo ,CreatedBy,CreatedDate)" ',Capacity
            SQLRoomSchInsert = SQLRoomSchInsert & " Values(" & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ",'" & ddlBldgID.SelectedItem.Text & "'," & ddlSeqNo.SelectedValue & ",'" & ddlRoomNo.SelectedItem.Text & "','" & txtStartBadge.Text.Trim & "','" & txtEndBadge.Text.Trim & "'," & Session("LoginID") & ",GETDATE())" ''" & txtCapacity.Text & "',

            'Update
            SQLRoomSchUpdate = "Update RoomSchedule set SeqNo=" & ddlSeqNo.SelectedValue & ", Date='" & ddlDate.SelectedItem.Text & "',StartBadgeNo='" & txtStartBadge.Text.Trim & "' ,EndBadgeNo='" & txtEndBadge.Text.Trim & "',ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GETDATE() "  'Capacity ='" & txtCapacity.Text & "',
            SQLRoomSchUpdate = SQLRoomSchUpdate & "Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"

            SQLRoomSchEval = "Select Count(*) From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & 

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLRoomSchEval) = 0 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLRoomSchInsert)
                lblAddUPdate.Text = "Added Successfully"
            Else
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLRoomSchUpdate)
                lblAddUPdate.Text = "Updated Successfully"
            End If

            LoadDGRoomSchedule()
            btnAddUpdate.Text = "Add"
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub DGRoomSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        Dim RoomSchID As Integer = CInt(e.Item.Cells(2).Text)
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from RoomSchedule Where RoomSchID=" & RoomSchID & "")
                LoadDGRoomSchedule()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            lblRoomSchID.Text = RoomSchID.ToString()
            LoadForUpdate(RoomSchID)
        End If
    End Sub
    Private Sub LoadForUpdate(ByVal RoomSchID As Integer)

        btnAddUpdate.Text = "Update"
        lblAddUPdate.Text = ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from RoomSchedule Where RoomSchID=" & RoomSchID & "")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo")  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim BldgID As String = ""
        Try
            If ds.Tables(0).Rows.Count > 0 Then
                'ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                'ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
                ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))
                ddlBldgID.SelectedIndex = ddlBldgID.Items.IndexOf(ddlBldgID.Items.FindByValue(ds.Tables(0).Rows(0)("BldgID").ToString.Trim))

                ddlRoomNo.Items.Clear()
                LoadRoomNumber(ds.Tables(0).Rows(0)("BldgID").ToString.Trim)
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber").ToString.Trim))

                txtStartBadge.Text = IIf(ds.Tables(0).Rows(0)("StartBadgeNo") Is DBNull.Value, "", ds.Tables(0).Rows(0)("StartBadgeNo"))
                txtEndBadge.Text = IIf(ds.Tables(0).Rows(0)("EndBadgeNo") Is DBNull.Value, "", ds.Tables(0).Rows(0)("EndBadgeNo"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        ClearList()
    End Sub
    Private Sub ClearList()
        btnAddUpdate.Text = "Add"
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrSeqUpdate.Visible = False
        DGRoomSchedule.Visible = False
        TrCopy.Visible = False
        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 3
        ddlSeqNo.SelectedIndex = 0
        ddlBldgID.SelectedIndex = 0 '.Items.Clear()
        ddlRoomNo.Items.Clear()
        ddlProductGroup.SelectedIndex = 0 '.Items.Clear()
        ddlProduct.Items.Clear()
        ddlDate.SelectedIndex = 0 '.Items.Clear()
        ddlPhase.SelectedIndex = 0
        'ddlStartTime.SelectedIndex = 0
        'ddlEndTime.SelectedIndex = 0
        'txtCapacity.Text = ""
        txtStartBadge.Text = ""
        txtEndBadge.Text = ""

    End Sub

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        AddUpdateRoomSchData()
    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        TrSeqUpdate.Visible = False
        lblErr.Text = "No record Updated"
    End Sub

    Protected Sub btnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCopy.Click
        TrCopy.Visible = True

        'GetProductGroup(ddlFromPG)
        GetProductGroup(ddlToPG)
        'LoadDisplayTime(ddlStartCpTime)
        'LoadDisplayTime(ddlEndCpTime)

    End Sub

    'Protected Sub ddlFromPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlFromPG.SelectedIndexChanged
    '    ddlFromProduct.Items.Clear()
    '    GetProduct(ddlFromPG, ddlFromProduct)
    'End Sub
    Protected Sub ddlToPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToPG.SelectedIndexChanged
        ddlToProduct.Items.Clear()
        GetProduct(ddlToPG, ddlToProduct)
    End Sub

    Protected Sub BtnCopySchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopySchedule.Click

        lblAddUPdate.Text = ""
        If ddlToPG.SelectedIndex = 0 Then
            lblErr.Text = "Please Select Copy Contest"
            Exit Sub
            'ElseIf ddlStartCpTime.SelectedIndex = 0 Then
            '    lblErr.Text = "Please Select Start Time"
            '    Exit Sub
            'ElseIf ddlEndCpTime.SelectedIndex = 0 Then
            '    lblErr.Text = "Please Select End Time"
            'Exit Sub
        End If

        Dim StrFromContest As String = ""
        Dim StrToContestInsert As String = ""
        Dim StrToContestUpdate As String = ""
        Dim SQLRoomSchEval As String = ""
        Dim SQLRoomSchEvalCopy As String = ""
        Dim SQLCopyExec As String = ""
        Dim RoomScheID As String = ""

        StrFromContest = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromContest)

        Try

            If ds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    StrToContestInsert = StrToContestInsert & "Insert into RoomSchedule(ContestYear, ChapterID,Chapter, EventID,Event,Purpose ,Date,ProductGroupID,ProductGroupCode,ProductID ,ProductCode ,Phase,BldgID,SeqNo,RoomNumber,CreatedBy,CreatedDate)" ',Capacity ,Phase,StartTime,EndTime ,StartBadgeNo ,EndBadgeNo ,
                    StrToContestInsert = StrToContestInsert & " Values(" & ds.Tables(0).Rows(i)("ContestYear") & "," & ds.Tables(0).Rows(i)("ChapterID") & ",'" & ds.Tables(0).Rows(i)("Chapter") & "'," & ds.Tables(0).Rows(i)("EventID") & ",'" & ds.Tables(0).Rows(i)("Event") & "','" & ds.Tables(0).Rows(i)("Purpose") & "','" & ds.Tables(0).Rows(i)("Date") & "'," & ddlToPG.SelectedValue & ",'" & ddlToPG.SelectedItem.Text & "'," & ddlToProduct.SelectedValue & ",'" & ddlToProduct.SelectedItem.Text & "'," & ds.Tables(0).Rows(i)("Phase") & ",'" & ds.Tables(0).Rows(i)("BldgID") & "'," & ds.Tables(0).Rows(i)("SeqNo") & ",'" & ds.Tables(0).Rows(i)("RoomNumber") & "'," & Session("LoginID") & ",GETDATE())" ''" & txtCapacity.Text & "', " & ddlPhase.SelectedValue & ",'" & ddlStartTime.SelectedItem.Text & "','" & ddlEndTime.SelectedItem.Text & "', '" & txtStartBadge.Text.Trim & "','" & txtEndBadge.Text.Trim & "',

                    'StrToContestUpdate = StrToContestUpdate & "Update RoomSchedule set  BldgID='" & ds.Tables(0).Rows(i)("BldgID") & "',RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "',SeqNo=" & ds.Tables(0).Rows(i)("SeqNo") & ", Date='" & ds.Tables(0).Rows(i)("Date") & "',StartTime='" & ddlStartCpTime.SelectedItem.Text & "',EndTime='" & ddlEndCpTime.SelectedItem.Text & "',ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GETDATE() "  'Capacity ='" & txtCapacity.Text & "',
                    'StrToContestUpdate = StrToContestUpdate & "Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ";" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
                Next

                SQLRoomSchEvalCopy = "Select Count(*) From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & 

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLRoomSchEvalCopy) > 0 Then
                    Dim ds3 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select RoomSchID From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "") ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & ")
                    For j As Integer = 0 To ds3.Tables(0).Rows.Count - 1
                        RoomScheID = RoomScheID & ds3.Tables(0).Rows(j)("RoomSchID") & ","
                    Next
                    RoomScheID = RoomScheID.Trim(",")
                    SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Delete From RoomSchedule Where RoomSchID in(" & RoomScheID & ")")

                    SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrToContestInsert)
                    lblAddUPdate.Text = " Copied Sucessfully (Updated)"
                Else
                    SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrToContestInsert)
                    lblAddUPdate.Text = " Copied Sucessfully(Added)"
                End If


                LoadDGRoomSchedule()
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try

    End Sub

    Protected Sub btnExportReport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportReport.Click
        If ddlChapter.SelectedItem.Text = "Select" Then
            lblAddUPdate.Text = "Please Select Chapter To Export"
            Exit Sub
        End If
        Dim dt As DataTable = GetData()
        Dim ChapterCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Replace(Replace(ChapterCode,',','_'),' ','') as Chaptercode from chapter Where ChapterId=" & ddlChapter.SelectedValue)
        Dim attachment As String = "attachment; filename=" & ddlYear.SelectedValue & "_RoomSchedule_" & ChapterCode & ".xls" '& "_" & ddlProductGroup.SelectedItem.Text & "_" & ddlProduct.SelectedItem.Text
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.xls"
        Dim tab As String = ""
        For Each dc As DataColumn In dt.Columns
            Response.Write(tab + dc.ColumnName)
            tab = vbTab
        Next
        Response.Write(vbLf)

        Dim i As Integer
        For Each dr As DataRow In dt.Rows
            tab = ""
            For i = 0 To dt.Columns.Count - 1
                Response.Write(tab & dr(i).ToString())
                tab = vbTab
            Next
            Response.Write(vbLf)
        Next
        Response.End()
    End Sub
    Function GetData() As DataTable

        Dim sql As String = ""
        'If ddlEvent.SelectedValue = 2 Then
        '    sql = "SELECT EventId,ProductGroupCode,productCode,ChildNumber,ContestantID,BadgeNumber,CONVERT(VARCHAR(10), DOB, 101) AS DOB,Grade, AttendanceFlag,Phase1Score,Phase1_TB1,Phase1_TB2,Phase1_TB3,Phase1_TB4,Phase2Score,Ph2_Round1,Ph2_Round2,Ph2_Round3,Ph2_Round4,Ph2_Round5,Ph2_Round6,Ph2_Round7,Ph2_Round8,P1_P2_Rank,Final_Rank,Rank_Alpha FROM RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ContestID=" & ddlProduct.SelectedValue & " order by BadgeNumber"
        'ElseIf ddlEvent.SelectedValue = 1 Then
        '    sql = "SELECT EventId,ProductGroupCode,productCode,ChildNumber,ContestantID,BadgeNumber,CONVERT(VARCHAR(10), DOB, 101) AS DOB,Grade, AttendanceFlag,Phase1Score,Phase1_TB1,Phase1_TB2,Phase1_TB3,Phase1_TB4,Phase2Score,Ph2_Round1,Ph2_Round2,Ph2_Round3,Ph2_Round4,Ph2_Round5,Ph2_Round6,Ph2_Round7,Ph2_Round8,P1_P2_Rank ,Phase3Score,Ph3_Round1,Ph3_Round2,Ph3_Round3,Ph3_Round4,Ph3_Round5,Ph3_Round6,Ph3_Round7,Ph3_Round8,Ph3_Round9,Ph3_Round10,Ph3_Round11,Ph3_Round12,Ph3_Round13,Ph3_Round14,Ph3_Round15,Ph3_Round16,Ph3_Round17,Ph3_Round18,Ph3_Round19,Ph3_TB_Score,Ph3_Rank,Final_Rank,Rank_Alpha FROM ScoreDetail Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlContest.SelectedValue & " and ContestID=" & ddlProduct.SelectedValue & " order by BadgeNumber"
        'End If
        sql = "SELECT ContestYear,EventID,Event,Chapter,Purpose,ProductGroupId,ProductGroupCode,ProductId,ProductCode,CONVERT(VARCHAR(10), DATE, 101) AS Date,BldgID,SeqNo,RoomNumber,Capacity,Phase,StartBadgeNo,EndBadgeNo FROM RoomSchedule Where Contestyear=" + ddlYear.SelectedValue + " and ChapterID=" & ddlChapter.SelectedValue & " order by Date, productgroupID, productID,Phase"
        Using myConnection As New SqlConnection(Application("ConnectionString"))
            Using myCommand As SqlCommand = New SqlCommand(sql, myConnection)
                myConnection.Open()
                Using myReader As SqlDataReader = myCommand.ExecuteReader()
                    Dim myTable As DataTable = New DataTable()
                    myTable.Load(myReader)
                    myConnection.Close()
                    Return myTable
                End Using
            End Using
        End Using

    End Function

    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged

        GetProductGroup(ddlProductGroup)
    End Sub
End Class
