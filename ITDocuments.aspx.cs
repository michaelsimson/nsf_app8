﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class ITDocuments : System.Web.UI.Page
{
    string Qurywhere;
     #region " Class Level Variables "

    //class level object
    EntityTestPaper m_objETP = new EntityTestPaper();

    #endregion
    // **************** Roles Allowed to Access in this Page
    //  Role 1,  Role 9 National,  Role 31 and 32 have full access.
    //  Role 9 chapter and  Role 33 can only download test papers for the current year.  
    
    #region " Event Handlers "
    protected void Page_Load(object sender, EventArgs e)
    {
        lblallfield.Visible = false;
        lblPGc.Visible = false;

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                gvSortDirection = "ASC";
                gvSortExpression = "ProductCode";

                if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "2") || (Session["RoleId"].ToString() == "42")))
                {
                    GetDropDownChoice(dllfileChoice, true);
                    dllfileChoice.Visible = true;
                }

                else
                {
                    Panel2.Visible = false;
                    Panel3.Visible = false;
                    lblNoPermission.Visible = true;
                    lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
                }
            }
        }
    }
    protected void Entycode(DropDownList ddEntry)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct EntryCode from ITLinks");
        ddEntry.DataSource = ds;
        ddEntry.DataTextField = ds.Tables[0].Columns[0].ColumnName;
        ddEntry.DataBind();
        ddEntry.Items.Insert(0, "[Select Entrycode]");
    }
   
   
    protected void dllfileChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        txtDescription.Text = "";
        TxtPassword.Text = "";
        gvTestPapers.Visible = false;
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {
           
            LoadSearchAndDownloadPanels(true);
           

        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            LoadUploadPanel();
           
           
            
        }
    }


    protected void UploadBtn_Click(object sender, EventArgs e)
    {
        //Response.Write(ddleventcode.SelectedValue);
        if ((ddlEntrycode.SelectedItem.Text != "[Select Entrycode]") && (ddleventcode.SelectedItem.Text != "[Select PanelName]") && (ddcolumncode.SelectedItem.Text != "[Select ColumnCode]") && (ddApplication.SelectedItem.Text != "[Select Application]"))
        {
            lblallfield.Visible = false;
            int value = 0;

            if (FileUpLoad1.HasFile)
            {

                EntityTestPaper objETP = new EntityTestPaper();


                // objETP. = FileUpLoad1.FileName;
                objETP.TestFileName = FileUpLoad1.FileName;
                objETP.EntryCode = ddlEntrycode.SelectedItem.Text;



                // objETP.EventCode = ddleventcode.SelectedItem.Text;
                objETP.Panelname = ddleventcode.SelectedItem.Text;
                objETP.columncode = ddcolumncode.SelectedItem.Text;
                objETP.Application = ddApplication.SelectedItem.Text;
                objETP.panelshort = ddleventcode.SelectedValue;
                objETP.Applicationshort = ddApplication.SelectedValue;

                objETP.Description = txtDescription.Text;
                objETP.Password = TxtPassword.Text;
                objETP.CreateDate = System.DateTime.Now;
                objETP.CreatedBy = int.Parse(Session["LoginID"].ToString());


                if (ValidateFileName(objETP))
                {
                    try
                    {
                        value = TestPapers_Insert(objETP);

                        if (value != 0)
                        {
                            SaveFile(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"], FileUpLoad1);

                            lblMessage.Text = "File Uploaded: " + FileUpLoad1.FileName;

                        }
                        else
                        {
                            //lblMessage.Text = "You are inserting duplicate record";
                            FileUpLoad1.SaveAs(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"].ToString()), "Temp_" + FileUpLoad1.FileName.ToString()));
                            hdnTempFileName.Value = "Temp_" + FileUpLoad1.FileName.ToString();
                            Panel1.Visible = false;
                            Panel5.Visible = true;
                            //dllfileChoice.Enabled = false;
                        }
                    }
                    catch (Exception err)
                    {
                        lblMessage.Text = err.Message;
                    }
                }
            }
            else
            {
                lblMessage.Text = "No File Uploaded.";
            }
        }
        else
        {
            lblallfield.Visible = true;
        }
    }

     protected void btnYes_Click(object sender, EventArgs e)
    {
        dllfileChoice.Enabled = true;
        if (hdnTempFileName.Value.Length > 0)
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update TestPaperTemplates SET ModifiedBy=" + Session["LoginID"].ToString() + ",ModifyDate=GetDate()  where  testFileName='" + hdnTempFileName.Value.Replace("Temp_", "").ToString() + "'");
            File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
            //File.Copy(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value), Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), true);
            Panel5.Visible = false;
            Panel1.Visible = true;
            lblMessage.Text = "File Replaced : " + hdnTempFileName.Value.Replace("Temp_", "").ToString();
        }
        else
        {
            Panel5.Visible = false;
            Panel1.Visible = true;
        }

    }

     protected void btnNo_Click(object sender, EventArgs e)
     {
         dllfileChoice.Enabled = true;
         if (hdnTempFileName.Value.Length > 0)
             File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"].ToString()), hdnTempFileName.Value));
         Panel5.Visible = false;
         Panel1.Visible = true;
     }
  

     protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
     {
         int index = -1;
         LblexamRecErr.Text = String.Empty;
         if (e.CommandArgument != null)
         {
             if (int.TryParse(e.CommandArgument.ToString(), out index))
             {
                 EntityTestPaper objETP = new EntityTestPaper();
                 index = int.Parse((string)e.CommandArgument);
                 objETP.TestPaperTempId = int.Parse(gvTestPapers.Rows[index].Cells[0].Text);
                 objETP.TestFileName = gvTestPapers.Rows[index].Cells[3].Text;
                 //objETP.DocType = gvTestPapers.Rows[index].Cells[10].Text;
                 if (e.CommandName == "DeleteTestPaper")
                 {
                     DeleteTestPaper(objETP);
                     //lblMessage.Text = DeleteTestPaperFromFTPSite(objETP);
                     DeleteFile(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"], objETP);
                     //GetTestPapers(m_objETP);
                 }
                 else if (e.CommandName == "Download")
                 {

                     DownloadFile(System.Configuration.ConfigurationManager.AppSettings["ITDocumentsPath"], objETP);

                     // }

                 }
             }
         }
     }
            
    

    protected void gvTestPapers_Sorting(object sender, System.Web.UI.WebControls.GridViewSortEventArgs e)
    {
        if (e.SortExpression == "CreateDate")
        {
            if (this.gvSortDirection == "ASC")
            {
                e.SortExpression = "YEAR DESC, MONTH DESC, DAY DESC";
                this.gvSortDirection = "DESC";
            }
            else
            {
                e.SortExpression = "YEAR ASC, MONTH ASC, DAY ASC";
                this.gvSortDirection = "ASC";
            }
        }

        else if (e.SortExpression == "ProductId")
        {
            if(this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {

                this.gvSortDirection = "ASC";
            }
            
            this.gvSortExpression = "ProductId";

        }
        else
        {
            if (this.gvSortDirection == "ASC")
            {
                this.gvSortDirection = "DESC";
            }
            else
            {
                this.gvSortDirection = "ASC";
            }
        }
       
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
       // m_objETP.ProductId = int.Parse(ddlFlrProduct.SelectedValue);
      //  m_objETP.ProductGroupId = int.Parse(ddlFlrProductGroup.SelectedValue);
      
        gvTestPapers.Visible = true;

        GetTestPaperssearch(m_objETP);
    }
    protected void btnReset_Click(object sender, EventArgs e)
    {
        LblexamRecErr.Text = "";
      //  ddlFlrProduct.SelectedIndex = 0;
       // ddlFlrProductGroup.SelectedIndex = 0;
        //DDeventD.SelectedIndex = 0;
        ddlflrEventcode.SelectedIndex = 0;
        DDentrycode.SelectedIndex = 0;

        ddlApplication.SelectedIndex = 0;
        ddcolumn.SelectedIndex = 0;
        gvTestPapers.Visible = false;
        lblnorecord.Visible = false;
      
       
    }

 

    #endregion

    #region " Private Methods - Data Access Layer "

    private int TestPapers_Insert(EntityTestPaper objETP)
    {
        object value;


        string sqlstring = "if Not Exists(Select * from ITDocuments where TestFileName ='" + objETP.TestFileName + "')Begin INSERT INTO .[dbo].[ITDocuments] ([EntryCode],[PanelName],[ColumnCode],[Applicationname],[MemberID],[TestFileName] ,[Description] ,[Password] ,[CreateDate] ,[CreatedBy]) VALUES ('" + objETP.EntryCode + "','" + objETP.Panelname + "', '" + objETP.columncode + "','" + objETP.Application + "'," + Convert.ToInt32(Session["LoginID"]) + ", '" + objETP.TestFileName + "', '" + objETP.Description + "','" + objETP.Password + "','" + objETP.CreateDate + "', '" + objETP.CreatedBy + "')select @@rowcount End  else select 0 ";
        value = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, sqlstring);
       
        if (value == null)
        {
            return -1;
            // lblMessage.Text = "You are inserting duplicate record";
        }
        return (int)value;
    }
    private void getProductCode(DropDownList ddlObjectload)
    {
       
        if (((Session["RoleId"].ToString() == "93") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "N")) || ((Session["RoleId"].ToString() == "30") && (hdnTechNational.Value == "Y") && (hdnteamlead.Value == "Y")))
        {
            
            //dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct [ProductGroupId], [ProductGroupCode] AS EventCodeAndProductGroupCode from volunteer where [ProductGroupCode] is not null and EventID='" + EventId + "' and  MemberID = " + Session["LoginID"] + "");
            DataSet dsproductgroupload = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT distinct V.ProductGroupId, V.MemberID,V.ProductGroupCode as EventCodeAndProductGroupCode    FROM Volunteer V inner join product  p on  V.ProductGroupCode=p.ProductGroupCode inner join Event E on E.EventId=P.eventId where E.EventId in (1,2) and  V.memberId= " + Session["LoginID"] + " and  V.ProductGroupCode is not null");
             DataTable dt = dsproductgroupload.Tables[0];

             if (dt.Rows.Count == 1)
             {
                 ddlObjectload.DataSource = dsproductgroupload;

                 ddlObjectload.DataTextField = "EventCodeAndProductGroupCode";
                 ddlObjectload.DataValueField = "ProductGroupId";
                 ddlObjectload.DataBind();
                 // ddlObjectload.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                // GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlProduct, true);
                // GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlFlrProduct, true);
                ddlObjectload.Enabled = false;
             }
             else
             {
                 ddlObjectload.DataSource = dsproductgroupload;

                 ddlObjectload.DataTextField = "EventCodeAndProductGroupCode";
                 ddlObjectload.DataValueField = "ProductGroupId";
                 ddlObjectload.DataBind();
                  ddlObjectload.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
                  //GetProductCodes(0, ddlFlrProduct, true);
                  //GetProductCodes(int.Parse(ddlObjectload.SelectedValue), ddlFlrProduct, true);
                // GetProductCodes(0, ddlProduct, true);
                
            }
              
                
             
        }

       
     
        // GetProductCodes(0, ddlFlrProduct, true);
    }
   

    private void GetFlrContextYear()
    {
        object year = DateTime.Now.Year;
      //  ddlFlrYear.Items.Insert(0, new ListItem(year.ToString()));
       
       
    }
  

    private void GetWeek(DropDownList ddlWeek, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsWeek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select WeekID, (Convert(varchar(14),SatDay1,106) +  ' - ' + Convert(varchar(14),sunDay2,106)) as Week  from weekCalendar Where Year(SatDay1)=Year(GETDATE()) Or Year(sunDay2)= YEAR(GETDATE())");
            ddlWeek.DataSource = dsWeek;
            ddlWeek.DataTextField = "Week";
            ddlWeek.DataValueField = "WeekID";
            ddlWeek.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlWeek.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlWeek.SelectedIndex = 0;
        }
        catch(SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }

    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    private void GetWeeks(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {
            DataSet dsweek = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_getWeekDays");
            ddlObject.DataSource = dsweek;
            ddlObject.DataTextFormatString = "{0:d}";
            ddlObject.DataTextField = "WeekDay";
            ddlObject.DataValueField = "WeekId";
            ddlObject.DataBind();
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Week]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            lblMessage.Text = se.Message;
            return;
        }
    }
   


    private void GetDropDownChoice(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        try
        {


            if (Session["RoleId"].ToString() == "30")
            {
                string[] Choice = { "DownloadScreen" };
                ddlObject.DataSource = Choice;
                ddlObject.DataBind();

            }
            else
            {
                string[] Choice = { "UploadScreen", "DownloadScreen" };
                ddlObject.DataSource = Choice;
                ddlObject.DataBind();
            }

            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Screen]", "-1"));
            }
            ddlObject.SelectedIndex = 0;

        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }


    private void GetTestPaperssearch(EntityTestPaper objETP)
    {
        //string valueEvent = DDeventD.SelectedItem.Text;
        try
        {
            string searchqry;

            if ((DDentrycode.SelectedItem.Text != "[Select Entrycode]") || (ddlflrEventcode.SelectedItem.Text != "[Select PanelName]") || (ddcolumn.SelectedItem.Text != "[Select ColumnCode]") || (ddlApplication.SelectedItem.Text != "[Select Application]"))
            {
                searchqry = "select distinct ITdocId,EntryCode,PanelName,TestFileName,Description,Password from ITDocuments";
                Qurywhere = searchqry + genWhereConditons();
            }
            else
            {
                searchqry = "select distinct ITdocId,EntryCode,PanelName,TestFileName,Description,Password from ITDocuments";
                Qurywhere = searchqry;
            }
           
            
                DataSet dsQuery = new DataSet();
                dsQuery = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qurywhere);
                DataTable dt = dsQuery.Tables[0];
                if (dt.Rows.Count > 0)
                {
                    gvTestPapers.DataSource = dsQuery;
                    gvTestPapers.DataBind();
                    lblnorecord.Visible = false;

                }
                else
                {
                    lblnorecord.Visible = true;
                    gvTestPapers.Visible = false;
                }
        }

        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
            
    

    protected string genWhereConditons()
    {
        
            string iCondtions = string.Empty;
            if (DDentrycode.SelectedItem.Text != "[Select Entrycode]")
            {
                if (iCondtions != string.Empty)
                {
                    iCondtions += " and  EntryCode='" + DDentrycode.SelectedItem.Text + "'";
                }

                else { iCondtions += " EntryCode='" + DDentrycode.SelectedItem.Text + "'"; }
            }
            if (ddlflrEventcode.SelectedItem.Text != "[Select PanelName]")
            {
                if (iCondtions != string.Empty)
                {
                    iCondtions += " and PanelName='" + ddlflrEventcode.SelectedItem.Text + "'";
                }
                else
                { iCondtions += " PanelName='" + ddlflrEventcode.SelectedItem.Text + "'"; }
            }

            if (ddcolumn.SelectedItem.Text != "[Select ColumnCode]")
            {
                if (iCondtions != string.Empty)
                {
                    iCondtions += " and ColumnCode='" + ddcolumn.SelectedItem.Text + "'";
                }
                else { iCondtions += " ColumnCode='" + ddcolumn.SelectedItem.Text + "'"; }

            }
            if (ddlApplication.SelectedItem.Text != "[Select Application]")
            {
                if (iCondtions != string.Empty)
                {
                    iCondtions += " and Applicationname='" + ddlApplication.SelectedItem.Text + "'";
                }
                else
                {
                    iCondtions += " Applicationname='" + ddlApplication.SelectedItem.Text + "'";}
            }

            return "  where" + iCondtions;
        }
       

        
       
    
    private void GetTestPapers(EntityTestPaper objETP)
    {
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct TestPaperTempId,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates");

        gvTestPapers.DataSource = ds;
        gvTestPapers.DataBind();


    }
    private int GetTestPapers(int memberid, int roleid)
    {
        DataSet ds = new DataSet();
        ds= SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text,"select distinct TestPaperTempId,ProductCode,ProductGroupCode,TestFileName,Description,Password from TestPaperTemplates");
        if (ds.Tables[0].Rows.Count > 0)
        {
            DataTable dt = ds.Tables[0];
            DataTable dtNew = dt.Clone();
            String CurrTestFileName;
            String CurrTestFilePrefix;
            String PrevTestFilePrefix = "";
            try
            {
                //Following loop is to filter out the Test Paprers, which belong to the same product code and same set but with 
                //different number of children. For example: If there are Set2_2008_SB_SSB_TestP_15.zip, Set2_2008_SB_SSB_TestP_20.zip, Set2_2008_SB_SSB_TestP_25.zip records exist then
                //this loop filters keeps only Set2_2008_SB_SSB_TestP_15.zip and filters out the other two.
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    CurrTestFileName = dt.Rows[i].ItemArray[10].ToString();
                    CurrTestFilePrefix = CurrTestFileName.Substring(0, CurrTestFileName.Length - 6);
                    if (!(PrevTestFilePrefix.ToLower().Equals(CurrTestFilePrefix.ToLower())))
                    {
                        PrevTestFilePrefix = CurrTestFilePrefix;
                        DataRow dr = dt.Rows[i];
                        dtNew.ImportRow(dr);
                    }
                }

            
            }

            catch (Exception ex)
            {
                //Response.Write(ex.ToString());
            }
            DataView dv = new DataView(dtNew);
            //dv.Sort = string.Concat(gvSortExpression, " ", gvSortDirection);

            gvTestPapers.DataSource = dv;
            gvTestPapers.DataBind();

            return dtNew.Rows.Count;
        }
        else
        {
            return 0;
        }
    }
    private void DeleteTestPaper(EntityTestPaper objDelETP)
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "TestPapers_Delete";
        SqlParameter[] param = new SqlParameter[1];
        param[0] = new SqlParameter("@TestPaperTempId", objDelETP.TestPaperTempId);
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, sqlCommand, param);

    }
    private string DeleteTestPaperFromFTPSite(EntityTestPaper objDelETP)
    {
        string sresult = string.Concat(objDelETP.TestFileName, " deleted successfully");
        try
        {
            //'Create a FTP Request Object and Specfiy a Complete Path 
            FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(objDelETP.getCompleteFTPFilePath(objDelETP.TestFileName));

            //'Call A FileUpload Method of FTP Request Object
            reqObj.Method = WebRequestMethods.Ftp.DeleteFile;

            //'If you want to access Resourse Protected You need to give User Name and PWD
            reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
            reqObj.Proxy = null;
            FtpWebResponse response = (FtpWebResponse)reqObj.GetResponse();
        }
        catch (Exception err)
        {
            sresult = string.Concat("Unable to delete ", objDelETP.TestFileName, ". Error: ", err.Message);
        }
        return sresult;
    }
    #endregion
    
    #region " Private Methods - File Operations "
    private void SaveFile(string sVirtualPath, FileUpload objFileUpload)
    {
        if (objFileUpload.HasFile)
        {
            objFileUpload.SaveAs(string.Concat(Server.MapPath(sVirtualPath), objFileUpload.FileName));
            //lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength;
        }
        else
        {
            lblMessage.Text = "No uploaded file";
        }
    }

    private void DownloadFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            Response.Write(objETP.TestFileName);
            
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");
          
        }
        catch (Exception ex)
        {
           lblMessage.Text = ex.ToString();
        }
    }

    private void DeleteFile(string sVirtualPath, EntityTestPaper objETP)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), objETP.TestFileName);
            if (System.IO.File.Exists(filePath))
            {
                System.IO.File.Delete(filePath);
            }
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
    }
    #endregion

    #region " Private Methods - FTP "
    private static void uploadFileUsingFTP(string CompleteFTPPath, Stream streamObj)
    {

        //'Create a FTP Request Object and Specfiy a Complete Path 
        FtpWebRequest reqObj = (FtpWebRequest)WebRequest.Create(CompleteFTPPath);

        //'Call A FileUpload Method of FTP Request Object
        reqObj.Method = WebRequestMethods.Ftp.UploadFile;

        //'If you want to access Resourse Protected You need to give User Name and PWD
        reqObj.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);
        reqObj.Proxy = null;

        Stream requestStream = reqObj.GetRequestStream();

        //'Store File in Buffer
        byte[] buffer = new byte[streamObj.Length];
        //'Read File from Buffer
        streamObj.Read(buffer, 0, buffer.Length);

        //'Close FileStream Object 
        streamObj.Close();

        requestStream.Write(buffer, 0, buffer.Length);
        requestStream.Close();
    }

    private void DownloadFileFromFTP(EntityTestPaper objETP)
    {

        try
        {
            // Get the object used to communicate with the server.
            FtpWebRequest objRequest = (FtpWebRequest)WebRequest.Create(objETP.getCompleteFTPFilePath(objETP.TestFileName));
            objRequest.Method = WebRequestMethods.Ftp.DownloadFile;

            objRequest.Credentials = new NetworkCredential(System.Configuration.ConfigurationManager.AppSettings["FTPUserName"], System.Configuration.ConfigurationManager.AppSettings["FTPPassword"]);

            FtpWebResponse objResponse = (FtpWebResponse)objRequest.GetResponse();

            StreamReader objSR;

            Stream objStream = objResponse.GetResponseStream();
            objSR = new StreamReader(objStream);
            Response.Clear();
            Response.AddHeader("Content-Disposition", "attachment; filename=" + objETP.TestFileName);
            Response.ContentType = "application/octet-stream";
            Response.Write(objSR.ReadToEnd());
            Response.Flush();
            Response.End();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

    }

    #endregion

    #region " Private Methods - Helper Functions "

    private string GetProductGroupCode(string EventCodeAndProductGroupCode)
    {
       
        string ProductGroupCode = EventCodeAndProductGroupCode;
        return ProductGroupCode;
    }

    

    private void LoadSearchAndDownloadPanels(bool searchPanel)
    {

        try
        {
            Entycode(DDentrycode);
           
            Panel1.Visible = false;
            Panel2.Visible = searchPanel;
            Panel3.Visible = true;
            if (searchPanel == false)
            {
                Panel4.Visible = true;
                GetWeek(ddlFlrWeekForExamReceiver, true);
            }
           
            GetFlrContextYear();
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }

       
    }
 
    protected void dd_columncode(DropDownList ddcolumndup,DropDownList ddentrydup)
    {
        ddcolumndup.Items.Clear();
        ddcolumndup.Items.Insert(0, new ListItem("[Select ColumnCode]", "0"));
        ddcolumndup.Items.Add("Main Option");
        ddcolumndup.Items.Add("Reports");
       

    }
 

    protected void  ddcolumncode_SelectedIndexChanged(object sender, EventArgs e)
      {
          DDApplicationname(ddApplication, ddlEntrycode, ddleventcode, ddcolumncode);
        
      }
    protected void DDApplicationname(DropDownList ddApplication,DropDownList ddentrylevel,DropDownList ddpanellevel,DropDownList ddcollevel)
    {
        try
        {
            string Qrycolumncode;
            DataSet ds;
            if (ddcollevel.SelectedItem.Text == "Main Option")
            {
                Qrycolumncode = "select distinct MainOptionLong,MainOptionShort from ITLinks where MainOptionLong is not null and EntryCode='" + ddentrylevel.SelectedItem.Text + "' and PanelNameLong='" + ddpanellevel.SelectedItem.Text + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycolumncode);
                ddApplication.DataSource = ds;
                ddApplication.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddApplication.DataValueField = "MainOptionShort";
                ddApplication.DataBind();
                ddApplication.Items.Insert(0, "[Select Application]");

            }
            else
            {
                Qrycolumncode = "select distinct ReportsLong,ReportsShort from ITLinks where ReportsLong is not null and EntryCode='" + ddentrylevel.SelectedItem.Text + "' and PanelNameLong='" + ddpanellevel.SelectedItem.Text + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycolumncode);
                ddApplication.DataSource = ds;
                ddApplication.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                ddApplication.DataValueField = "ReportsShort";
                ddApplication.DataBind();
                ddApplication.Items.Insert(0, "[Select Application]");
            }
           // ddApplication.Items.Insert(0, "[Select Application]");
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    private void LoadUploadPanel()
    {
        try
        {
            Panel1.Visible = true;
            Panel2.Visible = false;
            Panel3.Visible = true;
            Entycode(ddlEntrycode);
           
        }
        catch (Exception ex)
        {
            lblMessage.Text = ex.ToString();
        }
        
      
    }
    #endregion

    #region " Private Methods - Validations "
    private bool ValidateFileName(EntityTestPaper objETP)
    {
        string TFileName = string.Empty;
        string ProductGroupCode;
        ProductGroupCode = objETP.ProductGroupCode;
       // string NoOfContestants = objETP.NoOfContestants.ToString();
        bool IsValidFileName = false;
        string []exArr=new string[]{"doc","docx","zip"};
        // Correct number of contestants if the selected value is "5"
       

        // Any product code other than MB, EW, and PS should have number of contestants
        

        // Get the initial part of the file name

        TFileName = string.Format("{0}_{1}_{2}_{3}", objETP.EntryCode, objETP.panelshort, objETP.columncode, objETP.Applicationshort); //Set1_SB_05_JSB_2007.pdf or .doc

      
            if (Array.IndexOf(exArr, objETP.TestFileName.Substring(objETP.TestFileName.LastIndexOf('.') + 1)) >= 0)
            {
                if (string.Compare(objETP.TestFileName.Substring(0, objETP.TestFileName.LastIndexOf('.')), TFileName,true) == 0)
                
      
                {
                    IsValidFileName = true;
                }
                else
                {
                    //filename
                }
            }
            else
            {
                //ext
            }
        
        // For MB the file name can have Q or A as suffix
       
            

        // Display error message if the filename doesn't meet all rules
        if (!IsValidFileName)
        {
            lblMessage.Text = "File Name '" + FileUpLoad1.FileName + "' doesn't follow the naming convention. Required file name format is : '" + string.Concat(TFileName.ToString().ToLower() + "(.doc, .docx. .zip)");
            return false;
        }
        else
        {
            return true;
        }
    }
    #endregion

    #region " Private Properties "
    private string gvSortExpression
    {
        get
        {
            return (string)ViewState["gvSortExpression"];
        }
        set
        {
            ViewState["gvSortExpression"] = value;
        }
    }
    private string gvSortDirection
    {
        get
        {
            return (string)ViewState["gvSortDirection"];
        }
        set
        {
            ViewState["gvSortDirection"] = value;
        }
    }

    #endregion


    protected void ddlFlrWeekForExamReceiver_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (dllfileChoice.SelectedItem.Text == ScreenChoice.DownloadScreen.ToString())
        {

            LoadSearchAndDownloadPanels(true);
           

        }
        else if (dllfileChoice.SelectedItem.Text == ScreenChoice.UploadScreen.ToString())
        {
            LoadUploadPanel();
         
        
        }
    }
    protected void ButtonForExamReceiver_Click(object sender, EventArgs e)
    {
        LoadRecordsForExamRcvr();
    }

    protected void LoadRecordsForExamRcvr()
    {
        int selectedWeekId = int.Parse(ddlFlrWeekForExamReceiver.SelectedValue);
        lblNoPermission.Visible = false;
      
    }


   


#region "Enums" 
public enum ScreenChoice
{
    DownloadScreen,
    UploadScreen

}

#endregion

#region " Class EntityTestPaper (ETP) "
public class EntityTestPaper
{
    public Int32 TestPaperTempId = -1;
    public Int32 ProductId = -1;
    public string ProductCode = string.Empty;
    public Int32 ProductGroupId = -1;
    public string ProductGroupCode = string.Empty;
   // public string EventCode = string.Empty;
    //public Int32 WeekId = -1;
    //public Int32 SetNum = -1;
   // public Int32 NoOfContestants = -1;
    
    public string EntryCode = string.Empty;
    public string Panelname = string.Empty;
    public string columncode = string.Empty;
    public string Application = string.Empty;
    public string panelshort = string.Empty;
    public string Applicationshort = string.Empty;
    //public string EventCode = string.Empty;
    public string TestFileName = string.Empty;
    public string Description = string.Empty;
    public string Password = string.Empty;
    public DateTime CreateDate = new System.DateTime(1900, 1, 1);
    public Int32 CreatedBy = -1;
    public DateTime? ModifyDate = null;
    public Int32? ModifiedBy = null;
    public string ContestYear = string.Empty;
     public string DocType = string.Empty;
    public string WeekOf = string.Empty;
    public string ReceivedBy = string.Empty;
    public DateTime ReceivedDate = new System.DateTime(1900, 1, 1);
   
   

    public string getCompleteFTPFilePath(String TestFileName)
    {
        return String.Format("{0}/{1}",
                  System.Configuration.ConfigurationManager.AppSettings["FTPTestPapersPath"], TestFileName);
    }
    public EntityTestPaper()
    {
    }

}
protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
{
    //dd_panelName(ddlEntrycode,ddleventcode);
    dd_columncode(ddcolumncode, ddlEntrycode);
    ddpanelname(ddleventcode, ddlEntrycode);
   // dd_ApplicationName(ddlEntrycode, ddleventcode, ddcolumncode, ddApplication);
}
protected void ddpanelname(DropDownList DDpanel,DropDownList ddlentry)
{
    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct PanelNameLong,PanelNameShort  from ITLinks where EntryCode='"+ddlentry.SelectedItem.Text+"'");
    DDpanel.DataSource = ds;
    DDpanel.DataTextField = ds.Tables[0].Columns[0].ColumnName;
    DDpanel.DataValueField = "PanelNameShort";
    DDpanel.DataBind();
    DDpanel.Items.Insert(0, "[Select PanelName]");
}
protected void DDentrycode_SelectedIndexChanged(object sender, EventArgs e)
{
    ddpanelname(ddlflrEventcode, DDentrycode);
    dd_columncode(ddcolumn, DDentrycode);
  
}
protected void ddlflrEventcode_SelectedIndexChanged(object sender, EventArgs e)
{

}
protected void ddleventcode_SelectedIndexChanged(object sender, EventArgs e)
{
    dd_columncode(ddcolumncode, ddlEntrycode);
   
   
}
protected void ddcolumn_SelectedIndexChanged(object sender, EventArgs e)
{
    DDApplicationname(ddlApplication, DDentrycode, ddlflrEventcode, ddcolumn);
  
}
}
#endregion

 