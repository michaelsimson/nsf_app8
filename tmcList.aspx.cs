using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Web.Configuration;

public partial class Reports_tmcList : System.Web.UI.Page
{
    string Year = "";
    protected void Page_Load(object sender, EventArgs e)
    {
        Year = System.Web.Configuration.WebConfigurationManager.AppSettings["Contest_Year"].ToString();
        GetList();
    }
    private void GetList()
    {
        // connect to the peoducts database
        string connectionString =
            System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();

        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ChapterID, City, State from Chapter WHERE status ='A' ORDER BY city";

        // create the command object and set its
        // command string and connection

        SqlDataAdapter daContests = new SqlDataAdapter(commandString, connection);
        DataSet dsContests = new DataSet();
        daContests.Fill(dsContests);
        DataGrid1.DataSource = dsContests.Tables[0];
        DataGrid1.DataBind();
    }
    public string GetDate(int ChapterID)
    {
        // connect to the peoducts database
        string connectionString = 
        System.Web.Configuration.WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString();
        // create and open the connection object
        System.Data.SqlClient.SqlConnection connection =
            new System.Data.SqlClient.SqlConnection(connectionString);
        connection.Open();

        // get records from the products table
        string commandString = "Select ContestDate from Contest where NSFChapterID = " + ChapterID +
            " AND Contest_Year=" + Year;

        // create the command object and set its
        // command string and connection
        System.Data.SqlClient.SqlCommand command =
            new System.Data.SqlClient.SqlCommand();
        command.CommandText = commandString;
        command.Connection = connection;
        System.Data.SqlClient.SqlDataReader reader = command.ExecuteReader();
        string aString = "";
        DateTime firstValue = DateTime.Parse("1/1/2020");
        DateTime readValue = DateTime.Parse("1/1/1900");
        while (reader.Read())
        {
            aString = reader.GetValue(0).ToString();
            if (aString != "")
            {
                readValue = Convert.ToDateTime(aString);
                if (readValue < firstValue && readValue > DateTime.Parse("1/1/1900"))
                    firstValue = readValue;
            }
        }
        // close connection, return values
        connection.Close();
        if (readValue > DateTime.Parse("1/1/1900"))
            return firstValue.ToShortDateString();
        else return "";

    }
}

 