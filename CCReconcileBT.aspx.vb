Imports System
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Specialized
Imports System.Data.OleDb
Imports NativeExcel

Namespace VRegistration
    Partial Class CCReconcile
        Inherits System.Web.UI.Page

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Session("LoggedIn") = "true"
            'Session("EntryToken") = "VOLUNTEER"
            'Session("RoleId") = 1
            'Session("LoginID") = "4240"
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If
             
            'Put user code to initialize the page here
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Try
                        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Or (Session("RoleID") = "38") Then
                            'for Volunteer more than roleid 1,84,85  
                            Response.Write("<script language='javascript'>")
                            Response.Write(" window.open('ccreconcilestatus.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
                            Response.Write("</script>")
                        Else
                            Response.Redirect("VolunteerFunctions.aspx")
                        End If
                    Catch ex As Exception
                        Response.Redirect("VolunteerFunctions.aspx")
                    End Try
                End If
            End If
        End Sub

        Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Label1.Text = ""
            closepanel()
            If RbtnPostData.Checked = True Then
                Dim dt As Date = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull(max(ms_transdate),0) from MSCharge")
                '1) From Date must be after the maximum date in MSCharge table.  If not, give a message, �Last Date in MS Charge is  mm/nn/yyyy.  From Date is earlier than the last date.  So there is an overlap.  Do you still want to continue?�  If the user confirms, proceed.  Otherwsie, stop and let the user change the From Date. 2) Conversely, if there is gap between From Date and Last Date (max date) [From Date > Last date +1], then give a message, �Last Date in MS Charge is  mm/nn/yyyy.  There is a gap between From Date and Last Date. Do you still want to continue?�  If the user confirms, proceed.  Otherwsie, stop and let the user change the From Date.
                If Not IsDate(txtPostBTFromDate.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy format"
                    Exit Sub
                ElseIf Not IsDate(txtPostBTToDate.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy format"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostBTFromDate.Text) > Convert.ToDateTime(txtPostBTToDate.Text) Then
                    Label1.Text = "To Date is earlier than From Date. Please enter correct duration"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostBTFromDate.Text) < Convert.ToDateTime(dt) Then
                    Literal1.Text = "Last Date in MS Charge is " & dt & ". From Date is earlier than the last date.  So there is an overlap.  Do you still want to continue?"
                    TrMsg.Visible = True
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostBTFromDate.Text) > Convert.ToDateTime(dt).AddDays(1) Then
                    Literal1.Text = "Last Date in MS Charge is " & dt & ". There is a gap between From Date and Last Date. Do you still want to continue?"
                    TrMsg.Visible = True
                    Exit Sub
                End If

                MSTempPostData() 'transfer records from MSChargeTemp table to MSCharge Table.

            ElseIf RbtnPPMS.Checked = True Then
                Dim dt As Date = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select isnull(max(pp_date),0) from ChargeRec")
                '1) From Date must be after the maximum date in MSCharge table.  If not, give a message, �Last Date in MS Charge is  mm/nn/yyyy.  From Date is earlier than the last date.  So there is an overlap.  Do you still want to continue?�  If the user confirms, proceed.  Otherwsie, stop and let the user change the From Date. 2) Conversely, if there is gap between From Date and Last Date (max date) [From Date > Last date +1], then give a message, �Last Date in MS Charge is  mm/nn/yyyy.  There is a gap between From Date and Last Date. Do you still want to continue?�  If the user confirms, proceed.  Otherwsie, stop and let the user change the From Date.
                If Not IsDate(txtPostMSFromDate.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy format"
                    Exit Sub
                ElseIf Not IsDate(txtPostMSToDate.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy format"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostMSFromDate.Text) > Convert.ToDateTime(txtPostMSToDate.Text) Then
                    Label1.Text = "To Date is earlier than From Date. Please enter correct duration"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostMSFromDate.Text) < Convert.ToDateTime(dt) Then
                    Literal2.Text = "Last Date in ChargeReg is " & dt & ". From Date is earlier than the last date.  So there is an overlap.  Do you still want to continue?"
                    TrMS.Visible = True
                    Exit Sub
                ElseIf Convert.ToDateTime(txtPostMSFromDate.Text) > Convert.ToDateTime(dt).AddDays(1) Then
                    Literal2.Text = "Last Date in ChargeReg is " & dt & ". There is a gap between From Date and Last Date. Do you still want to continue?"
                    TrMS.Visible = True
                    Exit Sub
                End If
                MSChargePostData()

                'Dim StrPPCharge_Update As String = ""
                'Dim StrMSCharge_Update As String = ""
                'Dim i As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec")
                'Dim j As Integer = 0
                ''Reconcile Charge Data (Reconcile LinkPoint with Merchant Services) and inserting Values into ChargeRec
                'Dim records As SqlDataReader
                'Dim cmdText As String = "[usp_UpdateReconcile]"
                'Dim Param(1) As SqlParameter
                'Param(0) = New SqlParameter("@LoginID", Session("LoginID"))
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, cmdText, Param)

                'Dim BeginRecID, EndRecID As Integer
                'Try
                '    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(ChargeRecID) from ChargeRec Where ChargeRecid is not null") + 1
                'Catch ex As Exception
                '    BeginRecID = 0
                'End Try
                'Inserting Records into Charge Rec
                ' 1

                'cmdText = "  declare @CurYear int set @CurYear = year(GETDATE()) "
                'cmdText = cmdText & " insert into ChargeRec(PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, PPChargeID, MSChargeID, PP_MS_Match, CreateDate,CreatedBy,Brand,MemberID) Select PP.PP_Date, PP.PP_OrderNumber, PP.PP_CN1, PP.PP_CN2, PP.PP_Amount, PP.PP_Approval, MS.MS_TransDate, MS.MS_Amount, MS.MS_CN1, Ms.MS_CN2, Ms.MS_CN3,PP.PPChargeID, MS.MSChargeID,'Y',GetDate()," & Session("LoginID") & ",Case when PP.PP_Type<>'Sale' Then PP.PP_Type Else NULL End,PP.MemberID From MSCharge MS,PPCharge PP where  PP.PPChargeid = MS.PPChargeID and PP.ChargeRecID is Null "
                'cmdText = cmdText & " and PP.PP_Date >= '04/01/'+convert(varchar,(@CurYear-1)) and MS.MS_TransDate >= '04/01/'+convert(varchar,(@CurYear-1)) and PP.PPChargeID Not in (Select Distinct PPChargeid from ChargeRec where PPChargeid is not null) order by PP.PPChargeID"
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                'Dim k As Integer = 0
                '  Try
                '    '** Reconciled for refunded data to fix the Payment and Refund amount with the MSCharge in credit cards
                '    Dim CmdString As String = "select Distinct P.PPchargeID AS PPchargeID1,PP.PPchargeID AS PPchargeID2, P.PP_Amount as PP_Amount1, PP.PP_Amount as PP_Amount2, (P.PP_Amount+PP.PP_Amount) AS Amount,P.PP_CN1 as CN1, P.PP_CN2 as CN2,P.PP_Date,MS.MSChargeID,MS.MS_Amount,MS.MS_TransDate from PPCharge P "
                '    CmdString = CmdString & "INNER JOIN PPCharge PP ON P.PP_CN1=PP.PP_CN1 AND P.PP_CN2=PP.PP_CN2 and DateDiff(d,P.PP_date,PP.PP_Date)=0 " 'P.PP_OrderNumber = PP.PP_OrderNumber AND
                '    CmdString = CmdString & "INNER JOIN MSCharge MS ON MS.MS_CN1=PP.PP_CN1 AND MS.MS_CN3=PP.PP_CN2  "
                '    CmdString = CmdString & " where(P.MSchargeId Is null And PP.MSchargeId Is Null And MS.PPchargeId Is Null And P.PPChargeID <> PP.PPChargeID And (P.PP_Amount + PP.PP_Amount) > 0 And MS.MS_Amount = (P.PP_Amount + PP.PP_Amount) And DateDiff(d, PP.PP_Date, MS.MS_TransDate) >= 0 And DateDiff(d, PP.PP_Date, MS.MS_TransDate) < 3 And PP.PP_Amount < 0 And P.PP_Amount > 0 And ABS(PP.PP_Amount) <= P.PP_Amount)"
                '    records = SqlHelper.ExecuteReader(Application("connectionstring"), CommandType.Text, CmdString)
                '    cmdText = ""
                '    While records.Read()
                '        k = k + 2
                '        cmdText = " Update PPCharge set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", MS_Match='Y',MSChargeID = " & records("MSChargeID") & "  where PP_CN1 ='" & records("CN1") & "' and PP_CN2='" & records("CN2") & "' and MSChargeId is null and (MS_Match is Null or MS_Match <>'Y') and PPChargeID in (" & records("PPChargeID1") & "," & records("PPChargeID2") & ") and PPChargeid not in (Select Distinct PPChargeid from MSCharge where PPChargeid is not null) "
                '        cmdText = cmdText & " Update MSCharge set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",PP_Match='Y',PPChargeID = " & records("PPChargeID1") & " from MSCharge MS where PPChargeId is null and (PP_Match is Null or PP_Match <>'Y') and MSChargeID=" & records("MSChargeID") & " "
                '        cmdText = cmdText & " Insert into ChargeRec(PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, MS_TransDate, MS_Amount, MS_CN1, MS_CN2, MS_CN3, PPChargeID, MSChargeID, PP_MS_Match, CreateDate,CreatedBy,Brand,MemberID) Select PP.PP_Date, PP.PP_OrderNumber, PP.PP_CN1, PP.PP_CN2, PP.PP_Amount, PP.PP_Approval, MS.MS_TransDate, PP.PP_Amount, MS.MS_CN1, Ms.MS_CN2, Ms.MS_CN3,PP.PPChargeID, MS.MSChargeID,'Y',GetDate()," & Session("LoginID") & ",Case when PP.PP_Type<>'Sale' Then PP.PP_Type Else NULL End,PP.MemberID From PPCharge PP Left Join MSCharge MS ON PP.MSChargeid = MS.MSChargeID  where  PP.PPChargeID Not in (Select Distinct PPChargeid from ChargeRec where PPChargeid is not null) AND PP.PPChargeid in (" & records("PPChargeID1") & "," & records("PPChargeID2") & ")  order by PP.PPChargeID"
                '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                '    End While
                '    records.Close()
                'Catch ex As Exception
                '    Label1.Text = "Error In Second Phase of Reconciling " & ex.ToString()
                'End Try
                ''Posting into CCPostinglog file
                'Try
                '    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                'Catch ex As Exception
                '    EndRecID = 0
                'End Try

                'Dim nodataflag As Boolean = False
                'If BeginRecID = 0 And EndRecID > 0 Then
                '    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                '    nodataflag = True
                'End If
                'If EndRecID < BeginRecID And nodataflag = False Then
                '    BeginRecID = 0
                '    EndRecID = 0
                'ElseIf EndRecID = BeginRecID And nodataflag = True Then
                '    BeginRecID = 0
                '    EndRecID = 0
                'End If
                'Dim BeginDate, EndDate As String
                'If BeginRecID > 0 Then
                '    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(PP_Date) from ChargeRec where PP_date is Not Null and ChargeRecID >=" & BeginRecID & "") & "'"
                'Else
                '    BeginDate = "NULL"
                'End If
                'If EndRecID > 0 Then
                '    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(PP_Date) from ChargeRec where PP_date is Not Null") & "'"
                'Else
                '    EndDate = "Null"
                'End If
                '2
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values(0," & BeginRecID & "," & EndRecID & ",0," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
                ''Updating The Populating PPCharge and MSCharge with ChargeRecID's
                '3
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update PPCharge set ChargeRecID = C.ChargeRecID from ChargeRec C,PPCharge PP where C.PPChargeid=PP.PPChargeID and PP.ChargeRecID is Null and PP.MS_Match is Not Null;update MSCharge set ChargeRecID = C.ChargeRecID from ChargeRec C,MSCharge MS where C.MSChargeid=MS.MSChargeID and MS.ChargeRecID is Null and MS.PP_Match is Not Null")
                'Label1.Text = " Total Rows Reconciled & Inserted Into Charge Rec : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChargeRec") - i

            ElseIf RbtnSDNSF.Checked = True Then
                'Reconcile ChargeRec Data/NSF - NFG_Transactions table
                Dim intialcount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec where NFG_uniqueid is not null")
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND (ChargeRec.PP_OrderNumber = b.asp_session_id) AND (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5)))  AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL)")
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND (ChargeRec.PP_OrderNumber = b.asp_session_id) AND (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5)))  AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL)")

                '************* Added on 09-07-2014 to update NFG_UniqueID in ChargeREc using CCsubmitLog table values.[Matching CCSubmitLog.RefundReference = ChargeRec.PP_OrderNumber and CCSubmitLog.PaymentRefernce=NFG_Transactions.asp_session_id]
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=4240,NFG_UniqueID = b.[Unique_ID], NFG_PaymentDate = b.[Payment Date] FROM ChargeRec CROSS JOIN NFG_Transactions AS b  Inner join CCSubmitLog CC on CC.RefundReference =PP_OrderNumber and CC.PaymentReference =b.asp_session_id and CC.Amount =PP_Amount and CC.Amount =b.TotalPayment and CC.PaymentDate =b.[Payment Date]  WHERE (LEN(ChargeRec.PP_OrderNumber) > 5) AND  (ChargeRec.PP_Amount = b.TotalPayment) AND  (ChargeRec.NFG_UniqueID IS NULL) OR (LEN(ChargeRec.PP_OrderNumber) < 6) AND (ChargeRec.PP_OrderNumber = CAST(b.Id AS char(5))) AND (ChargeRec.PP_Amount = b.TotalPayment) AND (ChargeRec.NFG_UniqueID IS NULL) and CC.AspxPage ='Refund'") ' and b.MS_TransDate = ChargeRec.MS_TransDate ")
                '******************************************************************************************************************************************************************************************'

                '************* Added on 07-07-2014 to update Reference/Comments from NFG_Transactions table if the values are not updated from EMCharge table ****************************'
                ExecuteQuery("Update ChargeRec set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", Reference= (Select top 1 Case When EventID in (1,2,3,13,19) then case When CharIndex('Chapter',Name)>0 then Replace(Name,'Chapter','') Else Name End + ' Registration' Else Name END  From Event Where EventId=b.EventID) , Comments=b.PaymentNotes , MemberID=b.MemberID FROM ChargeRec CROSS JOIN NFG_Transactions AS b WHERE ChargeRec.PP_OrderNumber = b.asp_session_id AND ChargeRec.PP_Amount = b.TotalPayment AND ChargeRec.NFG_UniqueID = b.Unique_ID AND ChargeRec.NFG_PaymentDate =b.[Payment Date] and (ChargeRec.Reference is null Or ChargeRec.Comments is null)")
                '******************************************************************************************************************************************************************************************'

                Label1.Text = "Rows Reconciled :" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec where NFG_uniqueid is not null") - intialcount
            ElseIf RbtnVwSDNSF.Checked = True Then
                ' View Charge Rec Data/NSF
                If DdlVwSDNSF.SelectedValue > 0 Then
                    VwSDNSF()
                Else
                    Label1.Text = "Please Select data type to View Charge Rec Data/NSF Data"
                End If
            ElseIf RbtnPostNSFTrans.Checked = True Then
                Dim intialcount As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "UPDATE_MSTransDate_Brand")
                Label1.Text = "" & intialcount & " rows updated in NFG_Transactions Table"
            ElseIf RbtnViewNFGData.Checked = True Then
                If Not IsDate(txtFromDate.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Not IsDate(txtToDate.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtFromDate.Text) > Convert.ToDateTime(txtToDate.Text) Then
                    Label1.Text = "From date is greater than To Date.Please verify"
                    Exit Sub
                Else
                    VwNFGData(txtFromDate.Text, txtToDate.Text)
                End If
                ' VwNFGData()
            ElseIf RbtnPostNFGSupp.Checked = True Then
                Response.Redirect("NFG_Supplement.aspx")
            ElseIf RbtnViewUnRecData.Checked = True Then
                If Not IsDate(txtDateFrom.Text) Then
                    Label1.Text = "Please enter <b>From date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Not IsDate(txtDateTo.Text) Then
                    Label1.Text = "Please enter <b>To date</b> in mm/dd/yyyy fromat"
                    Exit Sub
                ElseIf Convert.ToDateTime(txtDateFrom.Text) > Convert.ToDateTime(txtDateTo.Text) Then
                    Label1.Text = "From date is greater than To Date.Please verify"
                    Exit Sub
                Else
                    VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
                End If
                'VwNFGMatchedData()
            Else
                Label1.Text = "Please Choose the option"
            End If
            clearddl()
        End Sub

        Sub reset()
            TrMsg.Visible = False
            TrMS.Visible = False
        End Sub
        Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
            MSTempPostData()
            reset()
        End Sub
        Protected Sub btnMSYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnMSYes.Click
            MSChargePostData()
            reset()
        End Sub
        Sub MSChargePostData()
            Try
                Dim TempRecRead, BeginRecID, EndRecID, TempRecRej As Integer
                Dim i As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChargeRec")
                Try
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(ChargeRecID) from ChargeRec Where ChargeRecid is not null") + 1
                Catch ex As Exception
                    BeginRecID = 0
                End Try

                Dim cmdText As String = ""
                cmdText = " INSERT INTO ChargeRec (PP_Date,MS_TransDate,PP_OrderNumber,Brand,MSChargeId,MS_Amount,MS_CN1,MS_CN2,MS_CN3,CreateDate,CreatedBy) "
                cmdText = cmdText & " SELECT MS_TransDate,DisbDate,PaymentReference,Brand,MSChargeId,MS_Amount,MS_CN1,MS_CN2,MS_CN3,GETDATE()," & Session("LoginID") & " FROM MSCharge where DisbDate is not null and Cast(DisbDate as date) >= '" & Convert.ToDateTime(txtPostMSFromDate.Text) & "' and cast(DisbDate as date)<='" & Convert.ToDateTime(txtPostMSToDate.Text) & "'"
                Dim iCnt As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                Label1.Text = Label1.Text & " <br> Number of Records Posted Successfully: " & iCnt
                'Posting into CCPostinglog file
                Try
                    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                Catch ex As Exception
                    EndRecID = 0
                End Try

                Dim nodataflag As Boolean = False
                If BeginRecID = 0 And EndRecID > 0 Then
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(ChargeRecID) from ChargeRec Where ChargeRecid is not null")
                    nodataflag = True
                End If
                If EndRecID < BeginRecID And nodataflag = False Then
                    BeginRecID = 0
                    EndRecID = 0
                ElseIf EndRecID = BeginRecID And nodataflag = True Then
                    BeginRecID = 0
                    EndRecID = 0
                End If
                Dim BeginDate, EndDate As String
                If BeginRecID > 0 Then
                    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(PP_Date) from ChargeRec where PP_date is Not Null and ChargeRecID >=" & BeginRecID & "") & "'"
                Else
                    BeginDate = "NULL"
                End If
                If EndRecID > 0 Then
                    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(PP_Date) from ChargeRec where PP_date is Not Null") & "'"
                Else
                    EndDate = "Null"
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values(0," & BeginRecID & "," & EndRecID & ",0," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
                Label1.Text = " Total Rows Reconciled & Inserted Into Charge Rec : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChargeRec") - i

                ''Posting log file
                'Try
                '    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(MSChargeID) from MSCharge Where MSChargeid is not null")
                'Catch ex As Exception
                '    EndRecID = 0
                'End Try
                'Dim nodataflag As Boolean = False
                'If BeginRecID = 0 And EndRecID > 0 Then
                '    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(MSChargeID) from MSCharge Where MSChargeid is not null")
                '    nodataflag = True
                'End If
                'TempRecRej = TempRecRead - (EndRecID - (BeginRecID - 1))
                'If EndRecID < BeginRecID And nodataflag = False Then
                '    TempRecRej = TempRecRead
                '    BeginRecID = 0
                '    EndRecID = 0
                'ElseIf EndRecID = BeginRecID And nodataflag = True Then
                '    TempRecRej = TempRecRead
                '    BeginRecID = 0
                '    EndRecID = 0
                'ElseIf EndRecID = 0 Or BeginRecID = 0 Then
                '    TempRecRej = TempRecRead
                'End If
                'Dim BeginDate, EndDate As String
                'If BeginRecID > 0 Then
                '    'select MS_Transdate from MScharge where MSChargeID=1 
                '    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(MS_transdate) from MSCharge where MS_Transdate is Not Null and MSChargeID>=" & BeginRecID & "") & "'"
                'Else
                '    BeginDate = "NULL"
                'End If
                'If EndRecID > 0 Then
                '    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(MS_Transdate) from MScharge where MS_Transdate is Not Null") & "'"
                'Else
                '    EndDate = "Null"
                'End If
                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(InputFileName,TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values('" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select top 1 filename from  fileupload where FileType='MS' And filename is not null order by fileid desc") & "'," & TempRecRead & "," & BeginRecID & "," & EndRecID & "," & TempRecRej & "," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
            Catch ex As Exception
                Label1.Text = ex.ToString()
            End Try
        End Sub

        Sub MSTempPostData()
            Try
                Dim TempRecRead, BeginRecID, EndRecID, TempRecRej As Integer
                Dim i As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from MSCharge")

                Try
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(MSChargeId) from MSCharge Where MSChargeId is not null") + 1
                Catch ex As Exception
                    BeginRecID = 0
                End Try
                Dim cmdText As String = ""
                cmdText = " INSERT INTO MSCharge (MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,Token,Brand,TransType,Email,CHName,MS_Amount,MS_CN1,MS_CN2,MS_CN3,PP_Match,PPChargeID,ChargeRecID,CreateDate,CreatedBy,ModifiedDate,ModifiedBy,PP_Date,CardType,AuthCode) "
                cmdText = cmdText & " SELECT MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,Token,Brand,TransType,Email,CHName,MS_Amount,MS_CN1,MS_CN2,MS_CN3,PP_Match,PPChargeID,ChargeRecID,CreateDate,CreatedBy,ModifiedDate,ModifiedBy,PP_Date,CardType,AuthCode FROM MSChargeTEMP where Cast(DisbDate as date) >= '" & Convert.ToDateTime(txtPostBTFromDate.Text) & "' and cast(DisbDate as date)<='" & Convert.ToDateTime(txtPostBTToDate.Text) & "'"
                Dim iCnt As Integer = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                Label1.Text = Label1.Text & " <br> Number of Records Posted Successfully: " & iCnt
                'Posting into CCPostinglog file
                Try
                    EndRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(MSChargeId) from MSCharge Where MSChargeId is not null")
                Catch ex As Exception
                    EndRecID = 0
                End Try

                Dim nodataflag As Boolean = False
                If BeginRecID = 0 And EndRecID > 0 Then
                    BeginRecID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select MIN(MSChargeId) from MSCharge Where MSChargeId is not null")
                    nodataflag = True
                End If
                If EndRecID < BeginRecID And nodataflag = False Then
                    BeginRecID = 0
                    EndRecID = 0
                ElseIf EndRecID = BeginRecID And nodataflag = True Then
                    BeginRecID = 0
                    EndRecID = 0
                End If
                Dim BeginDate, EndDate As String
                If BeginRecID > 0 Then
                    BeginDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Min(MS_TransDate) from MSCharge where MS_TransDate is Not Null and MSChargeId >=" & BeginRecID & "") & "'"
                Else
                    BeginDate = "NULL"
                End If
                If EndRecID > 0 Then
                    EndDate = "'" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Max(MS_TransDate) from MSCharge where MS_TransDate is Not Null") & "'"
                Else
                    EndDate = "Null"
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into CCPostingLog(TempRecRead,BeginRecID,EndRecID,TempRecRej,Postedby,CreatedDate,BeginDate,EndDate) values(0," & BeginRecID & "," & EndRecID & ",0," & Session("loginid") & " ,Getdate()," & BeginDate & "," & EndDate & ")")
                Label1.Text = " Total Rows Reconciled & Inserted Into MSCharge : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from MSCharge") - i
            Catch ex As Exception
                Label1.Text = ex.ToString()
            End Try
        End Sub

        Private Sub GetVwChargeRec()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from ChargeRec  order By  ChargeRecID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdChargeRec.DataSource = dt
            GrdChargeRec.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = "No Data"
                pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                pnlChargeRec.Visible = True
            End If
        End Sub
        Private Sub GetVwPP()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from PPCharge where MS_Match<>'Y' or MS_Match is Null  order By  PPChargeID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            grdVwPP.DataSource = dt
            grdVwPP.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = "No PP Data"
                'pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                PnlVwPPMS.Visible = True
            End If
        End Sub
        Protected Sub grdVwPP_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVwPP.PageIndexChanging
            GetVwPP()
            grdVwPP.PageIndex = e.NewPageIndex
            grdVwPP.DataBind()
        End Sub
        Protected Sub GrdVwNFGData_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwNFGData.PageIndexChanging
            If RbtnViewNFGData.Checked = True Then
                VwNFGData(txtFromDate.Text, txtToDate.Text)
            Else
                VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
            End If
            GrdVwNFGData.PageIndex = e.NewPageIndex
            GrdVwNFGData.DataBind()

        End Sub

        Private Sub GetVwMS()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select Top 500 * from MSCharge where PP_Match<>'Y' or PP_Match is Null  order By  MSChargeID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            grdVwMS.DataSource = dt
            grdVwMS.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No MS Data"
                'pnlChargeRec.Visible = False
            Else
                Label1.Text = ""
                PnlVwPPMS.Visible = True
            End If
        End Sub

        'Private Sub VwSDEM()
        '    Dim dsRecords As New DataSet
        '    Dim str As String = "is Null"
        '    If DdlVwSDEM.SelectedValue = 1 Then
        '        str = "is Not Null"
        '    ElseIf DdlVwSDEM.SelectedValue = 2 Then
        '        If ddlUnRecSDEM.SelectedValue = "Brand" Then
        '            str = str & " and Brand is null "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "Reference" Then
        '            str = str & " and Reference is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "Comments" Then
        '            str = str & " and Comments is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "MemberID" Then
        '            str = str & " and MemberID is null  "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "NFG_UniqueID" Then
        '            str = str & " and NFG_UniqueID is null   "
        '        ElseIf ddlUnRecSDEM.SelectedValue = "NFG_PaymentDate" Then
        '            str = str & " and NFG_PaymentDate is null "
        '        End If
        '    End If

        '    dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select ChargeRecID, EMChargeid, PP_OrderNumber, PP_Date, PP_Amount, MS_TransDate, Brand, Comments, MemberID,  PP_CN1, PP_CN2, Reference  from ChargeRec where EMChargeid " & str & "  order by ChargeRecID Desc")
        '    Dim dt As DataTable = dsRecords.Tables(0)
        '    GrdVwSDEM.DataSource = dt
        '    GrdVwSDEM.DataBind()
        '    If (dt.Rows.Count = 0) Then
        '        Label1.Text = Label1.Text & "No SDEM Data"
        '        pnlVwSDEM.Visible = False
        '    Else
        '        Label1.Text = ""
        '        pnlVwSDEM.Visible = True
        '    End If
        'End Sub
        Private Sub VwSDNSF()
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"
            If DdlVwSDNSF.SelectedValue = 1 Then
                str = "is Not Null"
                'ElseIf DdlVwSDEM.SelectedValue = 2 Then
                '    If ddlUnRecSDNSF.SelectedValue = "Brand" Then
                '        str = str & " and Brand is null "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "Reference" Then
                '        str = str & " and Reference is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "Comments" Then
                '        str = str & " and Comments is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "MemberID" Then
                '        str = str & " and MemberID is null  "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "NFG_UniqueID" Then
                '        str = str & " and NFG_UniqueID is null   "
                '    ElseIf ddlUnRecSDNSF.SelectedValue = "NFG_PaymentDate" Then
                '        str = str & " and NFG_PaymentDate is null "
                '    End If
            End If
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select ChargeRecID, PP_Date, PP_OrderNumber, PP_CN1, PP_CN2, PP_Amount, PP_Approval, NFG_UniqueID, NFG_PaymentDate, CreateDate, CreatedBy, ModifiedDate, ModifiedBy  from ChargeRec  where NFG_UniqueID " & str & " order by ChargeRecID desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwSDNSF.DataSource = dt
            GrdVwSDNSF.DataBind()
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No ChargeRec Data/NSF Data"
                pnlVwSDNSF.Visible = False
            Else
                Label1.Text = ""
                pnlVwSDNSF.Visible = True
            End If
        End Sub
        Private Sub VwNFGData(ByVal Frmdate As DateTime, ByVal Todate As DateTime)
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"

            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select * From NFG_Transactions where Brand is null and MS_Transdate is null and [Payment Date] between '" & Frmdate & "' and '" & Todate & "' order by unique_id desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwNFGData.DataSource = dt
            GrdVwNFGData.DataBind()
            Session("ds_NFGData") = dsRecords
            lblVwNFGData.Text = "View missing Brand and MS_Transdate in NFG_Trans table"
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No NFG Transactions Data"
                pnlVwNFGData.Visible = False
            Else
                Label1.Text = ""
                pnlVwNFGData.Visible = True
            End If
        End Sub

        Private Sub VwNFGMatchedData(ByVal dateFrm As DateTime, ByVal dateTo As DateTime)
            Dim dsRecords As New DataSet
            Dim str As String = "is Null"

            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select * From NFG_Transactions where MatchedStatus " & IIf(ddlMatchedStatus.SelectedIndex = 0, " is null", " ='" & ddlMatchedStatus.SelectedValue & "'") & " and [Payment Date] between '" & dateFrm & "' and '" & dateTo & "' order by unique_id desc")
            Dim dt As DataTable = dsRecords.Tables(0)
            GrdVwNFGData.DataSource = dt
            GrdVwNFGData.DataBind()
            Session("ds_NFGData") = dsRecords

            lblVwNFGData.Text = "NFG_Trans with records with selected Matched Status flag"
            If (dt.Rows.Count = 0) Then
                Label1.Text = Label1.Text & "No NFG Transactions Data"
                pnlVwNFGData.Visible = False
            Else
                Label1.Text = ""
                pnlVwNFGData.Visible = True
            End If
        End Sub

        Protected Sub GrdVwSDNSF_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwSDNSF.PageIndexChanging
            VwSDNSF()
            GrdVwSDNSF.PageIndex = e.NewPageIndex
            GrdVwSDNSF.DataBind()
        End Sub
        Protected Sub GrdVwSDEM_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwSDEM.PageIndexChanging
            'VwSDEM()
            GrdVwSDEM.PageIndex = e.NewPageIndex
            GrdVwSDEM.DataBind()
        End Sub
        Protected Sub grdVwMS_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grdVwMS.PageIndexChanging
            GetVwMS()
            grdVwMS.PageIndex = e.NewPageIndex
            grdVwMS.DataBind()
        End Sub

        Protected Sub GrdChargeRec_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdChargeRec.PageIndexChanging
            GetVwChargeRec()
            GrdChargeRec.PageIndex = e.NewPageIndex
            GrdChargeRec.DataBind()
        End Sub

        'Protected Sub GrdVwMsdNSF_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles GrdVwMsdNSF.PageIndexChanging
        '    GetVwMsdNSF()
        '    GrdVwMsdNSF.PageIndex = e.NewPageIndex
        '    GrdVwMsdNSF.DataBind()
        'End Sub
        Sub closepanel()
            pnlVwSDNSF.Visible = False
            PnlVwPPMS.Visible = False
            pnlChargeRec.Visible = False
            'PnlVwMsdNSF.Visible = False
            pnlVwSDEM.Visible = False
        End Sub
        Sub clearddl()
            '' ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
            'ddlCSVUpload.SelectedIndex = ddlCSVUpload.Items.IndexOf(ddlCSVUpload.Items.FindByValue(0))
            'If Not RbtnVwSDNSF.Checked Then DdlVwSDNSF.SelectedIndex = DdlVwSDNSF.Items.IndexOf(DdlVwSDNSF.Items.FindByValue(0))
            'If Not RbtnVwPPMS.Checked Then DdlVwPPMS.SelectedIndex = DdlVwPPMS.Items.IndexOf(DdlVwPPMS.Items.FindByValue(0))
            ''If Not RbtnVwSDEM.Checked Then DdlVwSDEM.SelectedIndex = DdlVwSDEM.Items.IndexOf(DdlVwSDEM.Items.FindByValue(0))
        End Sub
        Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
            TrMsg.Visible = False
            '  ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
        End Sub
        Sub ExecuteQuery(ByVal StrSQL As String)
            Dim conn As New SqlConnection
            conn.ConnectionString = Application("ConnectionString")
            Dim cmd As SqlCommand = New SqlCommand(StrSQL, conn)
            conn.Open()
            cmd.CommandTimeout = 600
            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            conn.Close()
        End Sub

        'Protected Sub DdlVwSDEM_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlVwSDEM.SelectedIndexChanged
        '    If DdlVwSDEM.SelectedValue <= 1 Then
        '        ddlUnRecSDEM.SelectedIndex = 0
        '        ddlUnRecSDEM.Enabled = False
        '    Else
        '        ddlUnRecSDEM.Enabled = True
        '    End If
        'End Sub

        Protected Sub DdlVwSDNSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles DdlVwSDNSF.SelectedIndexChanged
            If DdlVwSDNSF.SelectedValue <= 1 Then
                ddlUnRecSDNSF.SelectedIndex = 0
                ddlUnRecSDNSF.Enabled = False
            Else
                ddlUnRecSDNSF.Enabled = True
            End If
        End Sub

        'Protected Sub ddlVwMsdNSF_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVwMsdNSF.SelectedIndexChanged
        '    If ddlVwMsdNSF.SelectedValue <= 1 Then
        '        ddlUnRecVwMsdNSF.SelectedIndex = 0
        '        ddlUnRecVwMsdNSF.Enabled = False
        '    Else
        '        ddlUnRecVwMsdNSF.Enabled = True
        '    End If
        'End Sub

        Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        End Sub
        Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
            Try


                Dim TempGrid As New GridView
                Dim Filename As String = lblVwNFGData.Text.Replace(" ", "_")
                Response.Clear()
                Response.AddHeader("content-disposition", "attachment;filename=" & Filename & ".xls")
                Response.Charset = ""
                Response.ContentType = "application/vnd.xls"
                Dim stringWrite As New System.IO.StringWriter()
                Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
                'If RbtnViewNFGData.Checked = True Then
                '    VwNFGData(txtFromDate.Text, txtToDate.Text)
                'Else
                '    VwNFGMatchedData(txtDateFrom.Text, txtDateTo.Text)
                'End If
                ' If Session("ds_NFGData") <> "" Then
                Dim ds As DataSet = Session("ds_NFGData")
                TempGrid.DataSource = ds
                TempGrid.DataBind()
                'End If
                TempGrid.RenderControl(htmlWrite)
                Response.Write(stringWrite.ToString())
                Response.[End]()
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End Sub

    End Class
End Namespace