﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingSummary.aspx.vb" Inherits="CoachingSummary" title="Eligible Coaching" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

					<table width="100%"  border="0" cellpadding = "0" cellspacing ="0">
				<tr>
					<td  align="left" >
						<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
				</tr>
					<td class="Heading"  align="center" colspan="2">
                        Coaching Summary: <asp:Label ID="btnYear" runat="server" ></asp:Label>
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px" class="txt01">
						<table>
							<tr>
								<td><asp:label CssClass="txt01_strong" id="lblParentName" Runat="server" ></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" ></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" ></asp:label></td>
							</tr>
<%--							<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>--%>
							<tr>
								<td><asp:label id="lblStateZip" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont"><asp:Label ID="lblHeader" runat="server">Child/Children Detailed Information</asp:Label>
					</td>
				</tr>
				<tr>
					<td style="width: 819px"><asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<HeaderStyle HorizontalAlign ="Center" />
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" SortExpression="FIRST_NAME">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FIRST_NAME") %>' CssClass="SmallFont">
										</asp:Label>&nbsp;
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LAST_NAME") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="School Name" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" SortExpression="SchoolName">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SchoolName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Grade" HeaderStyle-Font-Bold="true"  SortExpression="Grade" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# IIf(DataBinder.Eval(Container, "DataItem.GRADE")="0","",DataBinder.Eval(Container, "DataItem.GRADE")) %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Type" HeaderStyle-Font-Bold="true" SortExpression="Type" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblType" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.MemberType") %>' ></asp:Label> 
                                        <div style="display:none"><asp:Label id="lblMemberId" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container, "DataItem.MemberId")%>'  ></asp:Label> </div>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Eligible Coaching Product" HeaderStyle-Font-Bold="true" SortExpression="Grade" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblEligibleContests" runat="server" CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								
								
								<asp:TemplateColumn Visible="false" HeaderText="Details" HeaderStyle-Font-Bold="true" SortExpression="Grade" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblLevel" runat="server" CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								
								<%--<asp:TemplateColumn HeaderText="Location" HeaderStyle-Font-Bold="true" SortExpression="Grade" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblLocation" runat="server" CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>--%>
							</Columns>
						</asp:datagrid></td>
				</tr>
								<tr><td class="SmallFont">Registration fee is 2/3rd  tax- deductible.  Support a poor child go to college in India.					</td>
				<td></tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Click here to Register" ></asp:button></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestDateInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
			</table>
    <asp:Label ID="hlbl" runat="server" Visible="false"></asp:Label>
</asp:Content>

