﻿Imports System.IO
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports NorthSouth.BAL
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Partial Class ShowRankCertificatesPhase3NewPDF
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadCertificates()
            Session("Page") = ""
        End If
    End Sub
    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()
        Try
            generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try
        Try
            dsCertificates = badge.GetRankCertificatesPhase3(Application("ConnectionString"), Session("SelChapterID"), Now.Year, generateBadge.ContestDates, Session("ContestID"), Session("TopRank"))
        Catch ex As Exception
            Response.Write(ex.ToString)
        End Try

        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            Try
                rptCertificate.DataSource = dsCertificates
                rptCertificate.DataBind()
            Catch ex As Exception
                Response.Write(ex.ToString)
            End Try
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found with Ranks.Please Upload the Scores"
            Exit Sub
        End If
        'If generateBadge.Export = True And dsCertificates.Tables(0).Rows.Count > 0 Then
        '    Response.Clear()
        '    Response.Buffer = True
        '    'Response.Charset = ""
        '    Response.ContentType = "application/vnd.word"
        '    Dim strChapterName As String
        '    If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
        '        strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
        '    End If
        '    Response.AddHeader("content-disposition", "attachment;filename=" & Session("FileName") & ".doc") 'Certificates_Rank_" & strChapterName & ".doc")
        '    Dim stringWrite As New System.IO.StringWriter()
        '    Dim htmlWrite As New HtmlTextWriter(stringWrite)
        '    rptCertificate.RenderControl(htmlWrite)
        '    Response.Write("<html>")
        '    Response.Write("<head>")
        '    Response.Write("<style>")
        '    Response.Write("@page Section1 {size:11.69in 8.27in;mso-page-orientation:landscape;margin:0.7in 1.5in 0.5in 1.5in;mso-header-margin:0.5in;mso-footer-margin:0.25in;mso-paper-source:0;}")
        '    Response.Write("div.Section1 {page:Section1;}")
        '    Response.Write("@page Section2 {size:11.69in 8.27in;mso-page-orientation:landscape;margin:0.7in 1.5in 0.5in 1.5in;mso-header-margin:0.5in;mso-footer-margin:0.25in;mso-paper-source:0;}")
        '    Response.Write("div.Section2 {page:Section2;}")
        '    Response.Write("</style>")
        '    Response.Write("</head>")
        '    Response.Write("<body>")
        '    Response.Write("<div class='Section2'>")
        '    Response.Write(stringWrite.ToString())
        '    Response.Write("</div>")
        '    Response.Write("</body>")
        '    Response.Write("</html>")
        '    Response.End()
        'ElseIf dsCertificates.Tables(0).Rows.Count <= 0 Then
        '    rptCertificate.Visible = False
        '    pnlMessage.Visible = True
        '    lblMessage.Text = "No Certificate Details found with Ranks.Please Upload the Scores."
        '    Exit Sub
        'End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.pdf"
            'FontFactory.Register(Server.MapPath("Font\\SCRIPTBL.TTF"))
            'Dim Font As New Font(iTextSharp.text.Font.FontFamily.HELVETICA, 15, iTextSharp.text.Font.BOLD, BaseColor.BLACK)


            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Participants_" & strChapterName & ".pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            rptCertificate.DataSource = dsCertificates.Tables(0)
            rptCertificate.DataBind()
            rptCertificate.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            'Dim docWorkingDocument As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 1, 1, 0, 0)
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER.Rotate(), 72, 72, 56, 0)
            'Dim pgSize As New iTextSharp.text.Rectangle(600, 600)
            'Dim pdfdoc As New iTextSharp.text.Document(pgSize, 5, 5, 45, 0)
            Dim htmlparser As New HTMLWorker(pdfDoc)
            PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            pdfDoc.Open()
            htmlparser.Parse(sr)
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.End()
        ElseIf dsCertificates.Tables(0).Rows.Count <= 0 Then
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found with Ranks.Please Upload the Scores."
            Exit Sub
        End If
    End Sub
    Protected Sub rptCertificate_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        'Dim obj As ImageButton = e.Item.FindControl("imgLogo")
        'Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfwww.jpg")
        'jpg.ScaleToFit(120.0F, 120.0F)
        'obj.ImageUrl = jpg.ToString()

        'Dim Img As Image = (Image)obj
        'Dim dv As DataRowView = TryCast(e.Item.DataItem, DataRowView)
        'If dv IsNot Nothing Then
        '    Dim rptOrderDetails As Repeater = TryCast(e.Item.FindControl("imgLogo"), Repeater)

        '    If rptOrderDetails IsNot Nothing Then
        '        Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
        '        jpg.ScaleToFit(120.0F, 120.0F)
        '        rptOrderDetails.DataSource = jpg
        '        rptOrderDetails.DataBind()
        '    End If
        'End If
        FontFactory.Register(Server.MapPath("Font"))

        ' ''Dim yourFont As BaseFont = BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), BaseFont.WINANSI, BaseFont.EMBEDDED)
        ' ''Dim SCRIPTBL As New Font(yourFont, 15, Font.NORMAL)
        ' ''lbl1.Font.Name = "yourFont"
        ' ''Dim bfR As iTextSharp.text.pdf.BaseFont
        ' ''bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED)
        ' ''Dim pathBlackItalic As String = HttpContext.Current.Server.MapPath("Font\\Scriptbl.ttf")
        ' ''Dim blackItalic As BaseFont = BaseFont.CreateFont(pathBlackItalic, BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
        ' ''Dim fontnew As New Font(blackItalic, 22)

        'Dim bfR As iTextSharp.text.pdf.BaseFont
        'bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED)
        'Dim mainFont As New Font(bfR, 16, Font.NORMAL)
        'Me.lbl1.Font = New Font("mainFont", 20, FontStyle.Regular)
        FontFactory.Register(Server.MapPath("Font\\ScriptMT.ttf"))
        FontFactory.Register(Server.MapPath("Font\\georgiaz.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comic.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comicbd.ttf"))
        FontFactory.Register(Server.MapPath("Font\\ARIALNI.TTF"))
        'FontFactory.GetFont("ScriptMT", 18)
        Dim item As RepeaterItem = e.Item
        Dim drv As System.Data.DataRowView = DirectCast((e.Item.DataItem), System.Data.DataRowView)
        Dim Newlbl1 As Label = DirectCast(item.FindControl("lblTitle1"), Label)
        Dim Newlbl2 As Label = DirectCast(item.FindControl("lblTitle2"), Label)
        Dim Newlbl3 As Label = DirectCast(item.FindControl("lblLeftTitle"), Label)
        Dim Newlbl5 As Label = DirectCast(item.FindControl("lblRightTitle"), Label)
        Dim Newlbl7 As Label = DirectCast(item.FindControl("lblSigTitle"), Label)
        Dim Newlbl8 As Label = DirectCast(item.FindControl("lblRightSigTitle"), Label)
        Dim Newlbl9 As Label = DirectCast(item.FindControl("lblcomm"), Label)
        Dim Newlbl10 As Label = DirectCast(item.FindControl("lblNSF"), Label)
        Dim bfR As iTextSharp.text.pdf.BaseFont
        bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont As New Font(bfR)
        Dim bfR1 As iTextSharp.text.pdf.BaseFont
        bfR1 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\georgiaz.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont1 As New Font(bfR1)
        Dim bfR2 As iTextSharp.text.pdf.BaseFont
        bfR2 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comic.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont2 As New Font(bfR2)
        Dim bfR3 As iTextSharp.text.pdf.BaseFont
        bfR3 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comicbd.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont3 As New Font(bfR3)
        Dim bfR4 As iTextSharp.text.pdf.BaseFont
        bfR4 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ARIALNI.TTF"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont4 As New Font(bfR4)
        'mainFont = FontFactory.GetFont("Arial", 18)
        Newlbl1.Font.Name = mainFont.Familyname.ToString()
        Newlbl2.Font.Name = mainFont.Familyname.ToString()
        Newlbl3.Font.Name = mainFont3.Familyname.ToString()
        Newlbl5.Font.Name = mainFont3.Familyname.ToString()
        Newlbl7.Font.Name = mainFont2.Familyname.ToString()
        Newlbl8.Font.Name = mainFont2.Familyname.ToString()
        Newlbl9.Font.Name = mainFont3.Familyname.ToString()
        Newlbl10.Font.Name = mainFont4.Familyname.ToString()
        'Response.Write(mainFont.Familyname.ToString())
        Dim Str As String
        Dim Str1 As String
        Dim Str2 As String
        Dim Str3 As String
        Dim Str4 As String
        Str1 = mainFont1.Familyname.ToString()
        Str = mainFont.Familyname.ToString()
        Str2 = mainFont2.Familyname.ToString()
        Str3 = mainFont3.Familyname.ToString()
        Str4 = mainFont4.Familyname.ToString()
        Newlbl1.Attributes.Add("Style", "font-face:" + Str + ";font-size:36pt;font-weight:bold;")
        Newlbl2.Attributes.Add("Style", "font-face:" + Str + ";font-size:26pt;font-weight:bold;")
        Newlbl3.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl5.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl7.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
        Newlbl8.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
        Newlbl9.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl10.Attributes.Add("Style", "font-face:" + Str4 + ";font-size:11pt;")
        'Response.Write(mainFont.Familyname.ToString())
        'Dim s As Style = New Style
        's.Font.Name = mainFont.Familyname.ToString()
        'Newlbl.ApplyStyle(s)
        'Newlbl.ControlStyle.Font.Name = mainFont.Familyname.ToString()
        'Newlbl.RenderControl()
        'rptCertificate.Controls.Add(DirectCast(item.FindControl("lblnew"), Label))
        ' Newlbl.Font.Name = mainFont.
        'ApplyFontStyleRecursively(Me.Page, Newlbl.Font)
    End Sub
    Protected Function GetLeftSignatureName(ByVal ProductCode As String) As String
        Dim strLeftSignatureName As String
        strLeftSignatureName = ""
        Dim dsLeftSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsLeftSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsLeftSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsLeftSignatureList.Tables(0).Rows.Count - 1
                strLeftSignatureName = dsLeftSignatureList.Tables(0).Rows(i)(6).ToString() & " " & dsLeftSignatureList.Tables(0).Rows(i)(0).ToString() & " " & dsLeftSignatureList.Tables(0).Rows(i)(1).ToString()
            Next
        Else
            strLeftSignatureName = ""
        End If
        Return strLeftSignatureName
    End Function
    Protected Function GetLeftSignatureTitle(ByVal ProductCode As String) As String
        Dim strLeftSignatureTitle As String
        strLeftSignatureTitle = ""
        Dim dsLeftSignatureTitle As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsLeftSignatureTitle = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsLeftSignatureTitle.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsLeftSignatureTitle.Tables(0).Rows.Count - 1
                strLeftSignatureTitle = dsLeftSignatureTitle.Tables(0).Rows(i)(2).ToString()
            Next
        Else
            strLeftSignatureTitle = ""
        End If
        Return strLeftSignatureTitle
    End Function
    Protected Function GetRightSignatureName(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(7).ToString() & " " & dsRightSignatureList.Tables(0).Rows(i)(3).ToString() & " " & dsRightSignatureList.Tables(0).Rows(i)(4).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetLeftSignature(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(8).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetRightSignature(ByVal ProductCode As String) As String
        Dim strRightSignatureName As String
        strRightSignatureName = ""
        Dim dsRightSignatureList As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureList = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureList.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureList.Tables(0).Rows.Count - 1
                strRightSignatureName = dsRightSignatureList.Tables(0).Rows(i)(9).ToString()
            Next
        Else
            strRightSignatureName = ""
        End If
        Return strRightSignatureName
    End Function

    Protected Function GetRightSignatureTitle(ByVal ProductCode As String) As String
        Dim strRightSignatureTitle As String
        strRightSignatureTitle = ""
        Dim dsRightSignatureTitle As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsRightSignatureTitle = badge.GetSignatureNameByProductCode(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, ProductCode)
        If dsRightSignatureTitle.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsRightSignatureTitle.Tables(0).Rows.Count - 1
                strRightSignatureTitle = dsRightSignatureTitle.Tables(0).Rows(i)(5).ToString()
            Next
        Else
            strRightSignatureTitle = ""
        End If
        Return strRightSignatureTitle
    End Function

    Public Function ShowImage(ByVal imageURL As String, ByVal spacerURL As String, ByVal imagePath As String)

        Dim f As New IO.FileInfo(Server.MapPath(imagePath))
        If f.Exists Then

            Return imageURL
        Else
            Return spacerURL
        End If

    End Function

End Class
