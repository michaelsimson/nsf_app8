﻿<%@ Page Language="VB" AutoEventWireup="false"   CodeFile="QuickFundRForm.aspx.vb" Inherits="QuickFundRForm" %>
<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>        
        </title>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

<link href="css/style.css" rel="stylesheet" type="text/css" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	text-align:center;
	width:100%;
}
.style1 {
	color: #FFFFFF;
	font-family: "Arial Rounded MT Bold";
	font-size: 14px;
}
    .style2
    {
        height: 28px;
    }
.SmallFont
{
    font-family: Arial, Helvetica, sans-serif;
	font-size: 12px;
	line-height:normal;
	color: #000000;
	text-decoration: none;
}
-->
</style>

    <script src="https://www.google.com/recaptcha/api.js"></script>

      
</head>
<body>
    <form id="form1" runat="server">
    
   
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:100%; text-align:center">
    <tr><td align="center">
    <table  border="1"  cellpadding = "0" cellspacing = "0" bordercolor="#189A2C" style="width:1000px; text-align:center">
    <tr><td>
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:1000px; text-align:center">
    <tr><td> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="right"  >
                      <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                        <map name="Map" id="Map">
                          <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                        </map></td>
                      <td width="76%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="51%"  background="images_new/topbg_1.jpg"><img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                              <td width="49%" valign="top" background="images_new/img_03.jpg"><table width="89%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="30%">&nbsp;</td>
                                  <td width="70%">
                                  <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td valign="middle" style="padding-left:10px; padding-top:5px">
                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                        <Items>                    
                                       
                                        </Items>     
                                        </asp:Menu>
                                        </td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr><td colspan ="2" align="center" ><br />
                                    <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server" ></asp:Label></td></tr>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="35" valign="middle" background="images_new/menubg.jpg">
<table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    <tr><td style="text-align:center; width :10px" >  </td><td style="vertical-align:middle; text-align:left;" class="style1">
        <asp:Label ID="lblPageCaption" runat="server" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" Text="25th Anniversary Celebrations!" ></asp:Label>
        <asp:Literal ID="ltlEvents" runat="server"></asp:Literal> </td><td style="text-align:center; width :10px" ></td></tr> 
    </table>
</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
    </td></tr>
  
      
        <tr><td bgcolor="#ffffff" align="center">
        <table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    
   
     <tr><td align="center">
           &nbsp;</td> </tr> 
      <tr runat = "server"  id="trlogin"><td align="center">
            <table runat = "server" id="Table1"  border="0" cellpadding="0" cellspacing="0" align="center" width="58%">
            <tr>
            <td width="6px" height="6px" background="images/tl.gif"></td>
            <td  height="6px" background="images/t.gif"></td>
            <td width="11px" height="6px" background="images/tr.gif"></td>
            </tr>

            <tr>
            <td  background="images/l.gif"></td>
            <td align="center" style="font-size:15px;">
                <div style="text-align:left" class="txt01_strong"> Existing NSF Parent Sign On:</div>
         <table id="tblLogin" runat="server" border="0">
     
        <tr>
            <td valign="top" class="txt01_strong" nowrap align="right" >
                Login </td>
            <td  align="left" class="txt01_strong">
                <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" Width="300" CssClass="text1"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="RFVLEmail" runat="server" Display="Dynamic"
                    ErrorMessage="Enter Login Id." ControlToValidate="txtUserId"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="rgvEmail" runat="server" CssClass="smFont" Display="Dynamic"
                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td valign="top" class="txt01_strong" nowrap align="right" >
                Password&nbsp;
            </td>
            <td  align="left">
                <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" Width="300" CssClass="text1"
                    TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RFVPwd"
                        runat="server" ErrorMessage="Enter Password."  ControlToValidate="txtPassword"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            
            <td  colspan="2" align="center">
                <asp:Button ID="btnLogin" runat="server"  OnClick="btnLogin_Click"  Text="Login"></asp:Button></td>
        </tr>
       
        <tr>
            <td  colspan="2" >
                 <table id="tblloginlock" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
   <asp:Label ID="lblloginlock" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
                <asp:Label ID="lblstudentloginerror" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>
                 <table id="tblErrorLogin" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p id="se">
                    Login Attempt failed. Invalid Email and/or password. Please try again.
                </p>
            </td>
        </tr>
    </table> <table id="tblInvalidEmail" visible="false" runat="server" style="width:950px">
        <tr>
            <td class="ErrorFont" align="center"><br />
                <p align="justify">
                   We have discovered a problem with your email not being unique.  A message will be sent to Customer Service team.  If you don’t hear in two or three days, please send an email to nsfcontests@northsouth.org.  Press to continue.
                </p><br /><br />
                <asp:Button ID="btnContinue1" runat="server" CssClass="FormButton" Text="Continue"></asp:Button></td>
        </tr>
    </table>
                <asp:Label ID="lblErr" runat="server"></asp:Label>
                </td>
        </tr>
       
    </table>
 </td> 
            <td background="images/r.gif"></td>
            </tr>

            <tr>
            <td width="6px" height="12px" background="images/bl.gif"></td>
            <td height="tpx" background="images/b.gif"></td>
            <td width="11px" height="12px" background="images/br.gif"></td>
            </tr>
            </table>
      </td> </tr> 
    <tr><td align="center" style ="height :250px"   >
       <table runat = "server" id="trall"  border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
            <td width="6px" height="6px" background="images/tl.gif"></td>
            <td  height="6px" background="images/t.gif"></td>
            <td width="11px" height="6px" background="images/tr.gif"></td>
            </tr>

            <tr>
            <td  background="images/l.gif"></td>
            <td align="center" style="font-size:15px;">
             
             <table cellpadding="3" id="tblinput" runat="server"  cellspacing="5" border="0">
                 <tr><td colspan="4"> <b> New Parents: Please enter the following information to register <span style="color:red;font-weight:normal">(All fields are mandatory)</span> :<br />
                     </b>
                      <center> <asp:Label ID="lblMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label></center>
                     </td></tr>
              <tr><td align="left">First Name <span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;</span></td><td align="left">: 
                  <asp:TextBox ID="txtFname" Width="150px" runat="server"></asp:TextBox>
              </td><td align="left">Last Name<span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;</span></td><td align="left">: 
                    <asp:TextBox ID="txtLname"  Width="150px"  runat="server"></asp:TextBox>
                    
</td></tr>

              <tr><td align="left"> EMail  </td><td align="left">:
                     <asp:TextBox ID="txtEmail"  Width="150px"  runat="server"></asp:TextBox>  
                     <br />
                     <asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
										ErrorMessage="Enter Valid EMail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
                     </td><td align="left"> Address  </td><td align="left">:
                     <asp:TextBox ID="txtAddress" TextMode="SingleLine" Width="150px"   runat="server"></asp:TextBox>
                    </td></tr>
                  
                  <tr><td align="left"> City</td><td align="left">:
                     <asp:TextBox ID="txtCity"  Width="150px"  runat="server"></asp:TextBox> 
                     </td><td align="left"> State  </td><td align="left">:
                      <asp:DropDownList ID="ddlState" runat="server" Width="150px">
                      </asp:DropDownList> 
                      <asp:TextBox ID="txtState" runat="server" Width="150px" Visible="false" ></asp:TextBox>
                     </td></tr>
                  
                  <tr><td align="left" width="85px"> Country  </td><td align="left">:
                     <asp:dropdownlist id="ddlCountry" OnSelectedIndexChanged='ddlCountry_SelectedIndexChanged' runat="server" width="150px" AutoPostBack="True">										
										<asp:ListItem Value="IN">India</asp:ListItem>
										<asp:ListItem Value="US" Selected="True" >United States</asp:ListItem>
										<asp:ListItem Value="CA">Canada</asp:ListItem>										
									</asp:dropdownlist>
                     </td><td align="left" width="85px"> Zip Code  </td><td align="left">:
                     <asp:TextBox ID="txtZip"  Width="150px"  runat="server"></asp:TextBox> 
                                    
                     </td></tr>
                  
                    <tr runat="server" id="trNewUser"><%--<td style="padding:0px" align="left" colspan ="4">
                    <table width="100%" border="0px" cellpadding = "2px" cellspacing = "0px"><tr>--%>
								<td align="left" >Gender</td>
								<td align="left" > : <asp:dropdownlist id="ddlGenderInd" runat="server" CssClass="SmallFont">
										<asp:ListItem Value=" ">Select Gender</asp:ListItem>
										<asp:ListItem Value="Male">Male</asp:ListItem>
										<asp:ListItem Value="Female">Female</asp:ListItem>
									</asp:dropdownlist> </td>
							
								<td align="left" style="height: 14px;width:97px">Home Phone</td>
								<td align="left" style="height: 14px">: 
                                    <asp:textbox id="txtHomePhoneInd" runat="server" CssClass="SmallFont" Width="150px"></asp:textbox>
						    </td></tr>
							
							
                   <%-- </table>
                    </td> </tr> --%>
                  
                    <tr>
								<td align="left" nowrap="nowrap">Password</td>
								<td align="left" > : <asp:textbox id="txtNewPassword" runat="server" CssClass="SmallFont" TextMode="Password" Width="150px"></asp:textbox> </td>
							
								<td align="left" style="height: 14px;" nowrap="nowrap">ReType Password</td>
								<td align="left" style="height: 14px"> : <asp:textbox id="txtNewRePassword" runat="server" CssClass="SmallFont" TextMode="Password" Width="150px"></asp:textbox> </td></tr>
							
							
                    <tr>
								<td align="left" nowrap="nowrap">Chapter</td>
								<td align="left" > : <asp:dropdownlist id="ddlChapter" tabIndex="7" runat="server" CssClass="SmallFont" Width="152px"></asp:dropdownlist>
                                </td>
							
								<td align="left"  nowrap="nowrap" colspan="2">
                                    <div class="g-recaptcha" id="rcaptcha" data-sitekey="6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"></div>
                                    <noscript>
                                        <div style="width: 302px; height: 722px;">
                                            <div style="width: 302px; height: 422px; position: relative;">
                                                <div style="width: 302px; height: 422px; position: absolute;">
                                                    <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"
                                                        frameborder="0" scrolling="no"
                                                        style="width: 302px; height: 422px; border-style: none;"></iframe>
                                                </div>
                                            </div>
                                            <div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                                                <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                                                    class="g-recaptcha-response"
                                                    style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"
                                                    rows="5" cols="210">
        </textarea>
                                            </div>
                                        </div>
                                    </noscript>
                                    <span id="spCaptcha" style="margin-left: 100px; color: red" runat="server" visible="false">You can't leave Captcha Code empty</span>

                                </td></tr>
							
							
                     <tr><td align="center" colspan ="4" >
                                    <asp:regularexpressionvalidator id="revHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic"
										ErrorMessage="Home Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:regularexpressionvalidator>
                                   
                                    <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtZip"
                                        ErrorMessage="Zip code wrong format" ValidationExpression="\d{5,6}(-\d{4})?"></asp:RegularExpressionValidator>
                     
                    </td> </tr> 
                        <tr><td align="center" colspan="4"> 
                       <asp:Button ID="btnSubmit" runat="server" Text="Save & Proceed" 
                           onclick="btnSubmit_Click" />
                           <br />
                           
                            </td></tr>
                   
                 
              </table>               
                <table cellpadding="3" id="tblContinue" runat="server"  visible="false"  cellspacing="0" border="0">
              <tr><td align="left"><b>Congratulations!  Your email address and details were accepted. Your login info was sent to your email address for your use in the future in accessing our system. 
						 Keep them in a safe place. Without them, you cannot access our system. 
                  <asp:HyperLink ID="hlink" CssClass="btn_02" runat="server">Click Here </asp:HyperLink>
                  to continue.</b>
						<asp:HiddenField ID="hdnEventID" runat="server" />
                 
                       <asp:Label ID="hdnMemberID" runat="server"  Visible="false"></asp:Label>
						</td> </tr> </table> 
              </td> 
            <td background="images/r.gif"></td>
            </tr>

            <tr>
            <td width="6px" height="12px" background="images/bl.gif"></td>
            <td height="tpx" background="images/b.gif"></td>
            <td width="11px" height="12px" background="images/br.gif"></td>
            </tr>
            </table>
        </td></tr>
       
            <tr><td  bgcolor="#99CC33" style="height:25px; vertical-align:middle ;" class="style2">
               <table width="100%">
                    <tr>
        <td height="25" align="left" class="btn_05"><a class="btn_01" href="/public/main/privacy.aspx">Copyright</a> &copy; 2015 North South Foundation.</td>
        <td height="25" align="right" valign="middle" class="btn_05">Follow Us: 
        <a href="http://twitter.com/NSFBee" target="_blank"><img src="images_new/icon_twitter_15x15.gif" alt="Twitter" width="15" height="15" border="0" /></a>
        <a href="http://www.facebook.com/NorthSouthFoundation" target="_blank"><img src="images_new/icon_facebook_15x15.gif" alt="Facebook" width="15" height="15" border="0" /></a>
        <a href="http://www.youtube.com/user/northsouthusa#g/a" target="_blank"><img src="images_new/icon_youtube_35x15.gif" alt="Youtube" width="35" height="15" border="0" /></a>
        </td>
        <!-- <td align="right" class="btn_05">Designed &amp; Developed by: <a href="http://www.metasenseusa.com/" target="_blank">www.metasenseusa.com</a></td> -->
      </tr>
    </table>
                   
                   
</td> 
            </tr>
    </table></td> </tr> </table> </td> </tr> </table>
    </td> </tr> 
    </table>
   
    
   
    </form>
</body>
</html>

