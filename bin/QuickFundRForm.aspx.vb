﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.IO
Imports System.Net.Mail
Imports System
Imports System.Diagnostics
Imports System.Drawing
Imports System.Drawing.Imaging
Imports System.Data
Imports System.Configuration
Imports System.Web
Imports System.Web.Security
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.WebControls.WebParts
Imports System.Web.UI.HtmlControls
Imports System.Net
Imports System.Web.Script.Serialization

Partial Class QuickFundRForm
    Inherits System.Web.UI.Page

    Private CAPTCHA_ID As String = "__CaptchaImage__"
    Dim _result As String = ""

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If Not IsPostBack Then
            tblContinue.Visible = False
            Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            addMenuItemAt(0, "Home", homeURL)
            If (Session("LoggedIn") = "True") Then
                addLogoutMenuItem()
            Else
                If (loginURL <> Nothing And Session("entryToken") <> Nothing) Then
                    loginURL = loginURL + Session("entryToken").ToString().Substring(0, 1)
                    addMenuItem("Login", loginURL)
                End If
            End If

            loadStates()
            FillChapter()
        End If
    End Sub

    Public Sub addMenuItem(ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addMenuItem(ByVal itemText As String, ByVal href As String)
        addMenuItem(New MenuItem("&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal itemText As String, ByVal href As String)
        addMenuItemAt(index, New MenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addLogoutMenuItem()
        addMenuItemAt(NavLinks.Items.Count, "Logout", "logout.aspx") 'add to the end
    End Sub

    Protected Sub Populate(ByVal automemberid As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select top 1 * from IndSpouse Where automemberid=" & automemberid & " order by DonorType")
        ' MsgBox(ds.Tables(0).Rows.Count)
        If ds.Tables(0).Rows.Count > 0 Then
            hdnMemberID.Text = ds.Tables(0).Rows(0)("AutoMemberID")
            'ddlTitle.SelectedIndex = ddlTitle.Items.IndexOf(ddlTitle.Items.FindByValue(ds.Tables(0).Rows(0)("Title")))
            txtFname.Text = ds.Tables(0).Rows(0)("FirstName")
            txtLname.Text = ds.Tables(0).Rows(0)("lastName")
            txtAddress.Text = ds.Tables(0).Rows(0)("Address1")
            txtEmail.Text = ds.Tables(0).Rows(0)("EMail")
            txtCity.Text = ds.Tables(0).Rows(0)("City")
            ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue(ds.Tables(0).Rows(0)("Country")))
            loadStates()

            If ddlCountry.SelectedValue = "CA" Then
                txtState.Text = ds.Tables(0).Rows(0)("State")
                txtState.Visible = True
                ddlState.Visible = False
            Else
                txtState.Visible = False
                ddlState.Visible = True
                ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(ds.Tables(0).Rows(0)("State")))
            End If
            txtZip.Text = ds.Tables(0).Rows(0)("Zip")
        End If
    End Sub
    Sub FillChapter()
        Dim liChapter As New ListItem
        Dim dsChapters As DataSet
        liChapter.Text = "Select Chapter"
        liChapter.Value = "0"

        Try
            dsChapters = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetChapters")
        Catch se As SqlException
            lblMessage.Visible = True
            lblMessage.Text = se.Message
            Return
        End Try

        If dsChapters.Tables.Count > 0 Then
            ddlChapter.DataSource = dsChapters.Tables(0)
            ddlChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
            ddlChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
            ddlChapter.DataBind()
            ddlChapter.Items.Add(liChapter)

            ddlChapter.Items.FindByValue("0").Selected = True

        End If
    End Sub
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            spCaptcha.Visible = False
            lblMessage.Visible = True
            lblMessage.Text = ""
            Dim errFlag As Boolean = False
            If Trim(txtFname.Text) = "" Then
                lblMessage.Text = "User First Name should not be Blank"
                Exit Sub
            End If
            If Trim(txtLname.Text) = "" Then
                lblMessage.Text = "User Last Name should not be Blank"
                Exit Sub
            End If
            If Trim(txtEmail.Text) = "" Then
                lblMessage.Text = "User EMail should not be Blank"
                Exit Sub
            End If
            Dim objReg As New Regex("")
            If Not Regex.IsMatch(txtEmail.Text.Trim(), "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase) Then
                lblMessage.Text = "EMail id Should be a Valid Email Address"
                Exit Sub
            End If
            If revPrimaryEmailInd.IsValid = False Then
                Exit Sub
            End If
            If Trim(txtAddress.Text) = "" Then
                lblMessage.Text = "Address should not be Blank"
                Exit Sub
            End If
            If Trim(txtCity.Text) = "" Then
                lblMessage.Text = "City should not be Blank"
                Exit Sub
            End If

            Dim state As String
            If (ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US") Then
                If ddlState.SelectedValue = "" Then
                    lblMessage.Text = "Please select State"
                    Exit Sub
                Else
                    lblMessage.Text = ""
                    state = ddlState.SelectedItem.Value
                End If
            Else
                state = Trim(txtState.Text)
                lblMessage.Text = "Please Enter State"
                Exit Sub
            End If

            If state.Length < 1 Then
                lblMessage.Text = "Select State"
                Exit Sub
            End If
            If Trim(txtZip.Text) = "" Then
                lblMessage.Text = "Please Enter Zipcode"
                Exit Sub
            End If
            If RegularExpressionValidator1.IsValid = False Then
                Exit Sub
            End If

            If ddlGenderInd.SelectedIndex = 0 Then
                lblMessage.Text = "Select Gender"
                Exit Sub
            End If
            If Trim(txtHomePhoneInd.Text) = "" Then
                lblMessage.Text = "Please Enter HomePhone"
                Exit Sub
            End If
            If revHomePhoneInd.IsValid = False Then
                Exit Sub
            End If
            If Trim(txtNewPassword.Text) = "" Then
                lblMessage.Text = "Password cannot be a Blank"
                Exit Sub
            End If
            If Trim(txtNewRePassword.Text) = "" Then
                lblMessage.Text = "Retype Password cannot be a Blank"
                Exit Sub
            End If
            If Trim(txtNewRePassword.Text) <> Trim(txtNewPassword.Text) Then
                lblMessage.Text = "Password cannot be a Blank"
                Exit Sub
            End If
            If ddlChapter.SelectedValue = 0 And Not (Session("entryToken") = "Donor") Then
                lblMessage.Text = "Chapter cannot be Blank"
                Exit Sub
            End If
            ' If Page.IsValid = True Then

            If Validate() Then

                Dim conn As String = Application("ConnectionString")

                Dim chapterID, ChapterCode As String
                'If Page.IsValid = True Then


                Dim displayString As String = ""

                Dim ChildMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT email from child where email='" & txtEmail.Text.Trim() & "'"))
                Dim IndSpouseMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT email from indspouse where email='" & txtEmail.Text.Trim() & "'"))
                Dim LmasterMail As String = Convert.ToString(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select user_email from login_master where user_email='" & txtEmail.Text.Trim() & "'"))

                If ChildMail <> "" Then
                    displayString = "An account with that email already exists in Child record "
                    errFlag = True
                End If
                If IndSpouseMail <> "" Then
                    displayString = "An account with that email already exists in Parent record please use other email id."
                    errFlag = True
                End If
                If LmasterMail <> "" Then
                    displayString = "An account with that email already exists in our system."
                    errFlag = True
                End If


                If errFlag = True Then
                    lblMessage.Visible = True
                    lblMessage.Text = displayString
                Else
                    lblMessage.Visible = False
                    ' insert into login_master
                    Dim param(12) As SqlParameter
                    param(0) = New SqlParameter("@ChapterID", IIf(ddlChapter.SelectedItem.Value = 0, 1, ddlChapter.SelectedItem.Value))
                    param(1) = New SqlParameter("@date_expires", DateAdd("m", 1, Now))
                    param(2) = New SqlParameter("@date_added", Now)
                    param(3) = New SqlParameter("@last_login", Now)
                    param(4) = New SqlParameter("@date_updated", Now)
                    param(5) = New SqlParameter("@view_tasks", "Yes")
                    param(6) = New SqlParameter("@user_name", Server.HtmlEncode(txtFname.Text))
                    param(7) = New SqlParameter("@parent_email", DBNull.Value)
                    param(8) = New SqlParameter("@active", "Yes")
                    param(9) = New SqlParameter("@user_email", Server.HtmlEncode(txtEmail.Text).Trim)
                    param(10) = New SqlParameter("@user_pwd", Server.HtmlEncode(txtNewPassword.Text))
                    param(11) = New SqlParameter("@user_role", "Parent")
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_AddNSFUser", param)
                    chapterID = ddlChapter.SelectedItem.Value
                    ChapterCode = ddlChapter.SelectedItem.Text
                    'Insert into Indspouse 
                    Dim strSQL As String = ""
                    strSQL = "INSERT INTO IndSpouse(ReferredBy,Relationship,MemberSince,DeletedFlag,chapter,Gender,HPhone,DonorType,ChapterID,Email, FirstName, LastName, Address1,Address2, City, State, Zip, Country, CreateDate) VALUES("
                    strSQL = strSQL & "'" & lblHeading.Text.Replace("-", "") & "',0,GetDate(),'No','" & ChapterCode & "'," & IIf(ddlGenderInd.SelectedValue = " ", "Null", "'" & ddlGenderInd.SelectedValue & "'") & ",'" & txtHomePhoneInd.Text & "','IND'," & chapterID & ","
                    strSQL = strSQL & "'" & txtEmail.Text & "','" & txtFname.Text.Replace("'", "''") & "','" & txtLname.Text.Replace("'", "''") & "','" & txtAddress.Text.Replace("'", "''") & "','','" & txtCity.Text.Replace("'", "''") & "','" & state & "','" & txtZip.Text & "','" & ddlCountry.SelectedValue & "',GETDATE()); Select Scope_Identity()"
                    hdnMemberID.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSQL)
                    SetSessionVariables(txtEmail.Text, hdnMemberID.Text)
                    '*** Sending New Registration Confirmation Message 
                    Dim re As TextReader
                    Dim emailbody As String
                    re = File.OpenText(Server.MapPath("NewRegistration.htm"))
                    emailbody = re.ReadToEnd
                    re.Close()
                    emailbody = emailbody.Replace("[USERID]", param(9).Value.ToString)
                    emailbody = emailbody.Replace("[PASSWORD]", param(10).Value.ToString)
                    ''NOTE Remember to uncomment the following before sending the source
                    SendEmail("North South Foundation Bee Registration", emailbody, Session("LoginEmail"))
                    Response.Redirect("FundRReg.aspx")
                End If
            Else
                spCaptcha.Visible = True
            End If
            'Else
            'lblMessage.Text = "Please Enter all data"
            'End If
        Catch ex As Exception
            lblMessage.Text = ex.ToString()
            'Response.Write(strSQL)
        End Try

    End Sub


    Public Function GetRandomPasswordUsingGUID(ByVal length As Integer) As String
        'Get the GUID
        Dim guidResult As String = System.Guid.NewGuid().ToString()

        'Remove the hyphens
        guidResult = guidResult.Replace("-", String.Empty)

        'Make sure length is valid
        If length <= 0 OrElse length > guidResult.Length Then
            Throw New ArgumentException("Length must be between 1 and " & guidResult.Length)
        End If

        'Return the first length bytes
        Return guidResult.Substring(0, length)
    End Function

    Function encode(ByVal inputText As String) As String
        Dim bytesToEncode As Byte() = Encoding.UTF8.GetBytes(inputText)
        Dim encodedText As String = Convert.ToBase64String(bytesToEncode)
        Return encodedText
    End Function



    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = "nsfcontests@gmail.com"
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Try
            client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        loadStates()
    End Sub
    Private Sub loadStates()
        If ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US" Then
            ddlState.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet
            Dim strCountry As String = IIf(ddlCountry.SelectedValue = "IN", "usp_GetIndiaStates", "usp_GetStates")
            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, strCountry)
            ddlState.Items.Clear()
            ddlState.DataSource = dsIndiaStates.Tables(0)
            ddlState.DataTextField = dsIndiaStates.Tables(0).Columns(2).ToString
            ddlState.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
            ddlState.DataBind()
            ddlState.Items.Insert(0, New ListItem("Select State", String.Empty))
            txtState.Visible = False
        Else
            ddlState.Visible = False
            txtState.Visible = True
        End If
    End Sub

    Private Sub SetSessionVariables(ByVal email As Object, ByVal autoMemberId As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim drIndSpouse As SqlDataReader

        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndAndSpouseRecords", New SqlParameter("@autoMemberID", autoMemberId))

        Dim donorType As String
        Dim gender As String
        Dim relationshipId As Integer
        Session("entryToken") = "Parent"
        Session("ChapterID") = ddlChapter.SelectedItem.Value
        Session("userid") = hdnMemberID.Text
        Session("EventId") = 9
        Session("LoginID") = autoMemberId
        Session("LoggedIn") = "True"
        Session("LoginEmail") = email
        Session("CustIndID") = Nothing
        Session("FatherID") = Nothing
        Session("MotherID") = Nothing
        Session("CustSpouseID") = Nothing
        gender = Nothing
        If (drIndSpouse.HasRows()) Then ''drIndSpouse will have b oth Ind and Spouse records (if there is any)
            Do While (drIndSpouse.Read())
                donorType = drIndSpouse("DonorType")
                If Not (drIndSpouse("Gender") Is DBNull.Value) Then
                    gender = drIndSpouse("Gender")
                    If (gender <> Nothing And gender.ToLower = "male") Then
                        Session("FatherID") = autoMemberId
                    Else
                        Session("MotherID") = autoMemberId
                    End If
                End If

                autoMemberId = drIndSpouse("autoMemberId")
                relationshipId = drIndSpouse("Relationship")

                If (donorType <> Nothing And donorType.ToUpper() = "IND") Then
                    Session("CustIndID") = autoMemberId
                ElseIf (donorType <> Nothing And donorType.ToUpper() = "SPOUSE") Then
                    Session("CustSpouseID") = autoMemberId
                    Session("CustIndID") = relationshipId
                End If
            Loop

        End If

    End Sub

    Protected Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim userID As String = ""  'Login_master
        Dim autoMemberID As String = ""  'IndSpouse
        Dim volunteerMemberID As String = "" 'Volunteer
        Dim entryToken As String = ""
        Session("entryToken") = "Parent"
        If (Not Session("entryToken") Is Nothing) Then
            entryToken = Session("entryToken")
        Else

        End If
        Session("UserID") = txtUserId.Text

        ' if user fails to login for 3 attempts, userid will be locked for 24 hours
        Dim date1 As DateTime = DateTime.Now.AddDays(-1)
        Dim i As Integer
        Dim srr As String = "select Sum(attempt) from login_attempts where userid = '" + txtUserId.Text + "' and login_time < '" + DateTime.Now + "' and login_time > '" + date1 + "' and entrytoken = '" + entryToken.ToString + "'"
        'Response.Write(srr)
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, srr)
            'If Not DBNull.Value.Equals(ds.Tables(0).Rows(0)(0)) Then
            '    i = ds.Tables(0).Rows(0)(0)
            'End If
        Catch ex As Exception
            'Response.Write(srr)
            'Response.Write(ex.ToString)
        End Try
        If i >= 3 Then
            tblloginlock.Visible = True
            lblloginlock.Text = "Account for userid = " + txtUserId.Text + " is locked, please try after 24 hours or contact nsfgame@gmail.com for more Information."
        Else
            Dim StrSql2 As String = "Delete from login_attempts where userid = '" + txtUserId.Text + "' and entrytoken = '" + entryToken.ToString + "'"
            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, StrSql2)

            tblErrorLogin.Visible = False

            If ViewState("TryCount") > 2 Then
                btnLogin.Visible = False
                tblLogin.Visible = False
                ' Session.Abandon()
            End If

            'End If
            'authenticate
            userID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
            '  Response.Write("userID" & userID)
            If (userID <> "") Then
                Dim emailcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT COUNT(*) FROM IndSpouse where email = '" & Session("UserID") & "' GROUP BY Email HAVING Count(*)>1")
                '    Response.Write("emailcnt = " & emailcnt)
                If emailcnt > 0 Then
                    Dim INDcnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(AutoMemberID) from IndSpouse where email = '" & Session("UserID") & "' AND donortype = 'IND'")
                    Dim Spousecnt As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(AutoMemberID) 	from IndSpouse where email = '" & Session("UserID") & "' AND donortype = 'SPOUSE'")
                    tblLogin.Visible = False
                    tblErrorLogin.Visible = False
                    Dim strsq As String = "Insert into EmailError(Email, IndCount, SpouseCount, CreatedDate,Status,Remarks) Values ('" & Session("UserID") & "'," & INDcnt & "," & Spousecnt & ",'" & DateTime.Now & "','Pending','Email has a duplicate in IndSpouse Table')"
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strsq)
                    'End If
                End If
                '  If tblInvalidEmail.Visible = False Then

                Select Case entryToken.ToString.ToUpper
                    Case "PARENT"
                        ' catch users with no profile info in IndSpouse
                        autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
                        Response.Write("autoMemberID = " & autoMemberID)
                        If (autoMemberID <> "") Then
                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                            'Get the ChapterId 
                            Session("CustIndChapterID") = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetChapterId_IndParent", New SqlParameter("@CustIndID", Session("CustIndID")))
                            ' silva added for testing
                            Session("ChapterID") = Session("CustIndChapterID")
                            ''**SendEmailValidationMail(Session("CustIndID"))
                            Response.Redirect("FundRReg.aspx")
                        Else
                            'user profile not present in Indspouse                            
                            lblMessage.Text = "Profile for userid = " + txtUserId.Text + " is not in NSF Database"

                        End If
                End Select
                'End If
            Else
                Dim str As String = "insert into login_attempts(UserId,attempt,login_time, entrytoken) values ('" + txtUserId.Text + "', 1, '" + DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss") + "','" + entryToken.ToString() + " ')"
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str)
                tblErrorLogin.Visible = True
            End If
        End If
    End Sub

    Protected Sub RbtnRegType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)


    End Sub
    Public Function Validate() As Boolean
        Try

            Dim Response As String = Request("g-recaptcha-response")

            'Getting Response String Append to Post Method
            Dim Valid As Boolean = False
            'Request to Google Server
            Dim req As HttpWebRequest = DirectCast(WebRequest.Create(" https://www.google.com/recaptcha/api/siteverify?secret=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ&response=" & Response), HttpWebRequest)

            'Google recaptcha Response
            Using wResponse As WebResponse = req.GetResponse()

                Using readStream As New StreamReader(wResponse.GetResponseStream())
                    Dim jsonResponse As String = readStream.ReadToEnd()

                    Dim js As New JavaScriptSerializer()
                    Dim data As MyObject = js.Deserialize(Of MyObject)(jsonResponse)
                    ' Deserialize Json
                    Valid = Convert.ToBoolean(data.success)
                End Using
            End Using

            Return Valid
        Catch ex As WebException
            'Response.Write("Error :" & ex.ToString())
            Return False
        End Try

    End Function

    Public Class MyObject
        Private _success As String
        Public Property success() As String
            Get
                Return _success
            End Get
            Set(value As String)
                _success = value
            End Set
        End Property


    End Class

End Class