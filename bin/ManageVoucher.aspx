<%@ Page Language="VB" EnableEventValidation="false" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ManageVoucher.aspx.vb" Inherits="ManageVoucher"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
       <tr bgcolor="#FFFFFF" >
             <td colspan="2" align="left">
                    <asp:LinkButton ID="hlnkMainPage" OnClick="hlnkMainPage_Click" CausesValidation="false"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:LinkButton>
             </td>
       </tr>
       <tr bgcolor="#FFFFFF" >
             <td colspan="2" align="center" class="Heading">Manage Cash Receipts / Vouchers</td>
             <td align="Right"><asp:Button ID ="BtnShowGL" runat="server" Text="Show GL Date Ranges" /></td> 
        </tr>
        <tr bgcolor="#FFFFFF" >
             <td colspan="2" align="center" class="txt01"><asp:Label ID="LblMessage" runat="server" ></asp:Label><br />
             </td>
       </tr>
       <tr bgcolor="#FFFFFF">
            <td colspan="2" align="center" class="txt01">
                <table cellspacing="0" cellpadding="2"  align="center" border="0" >
                     <tr><td align="left">Category  </td>
                         <td align="left">: 
                              <asp:DropDownList AutoPostBack="true" ID="ddlCategory" OnSelectedIndexChanged = "ddlCategory_SelectedIndexChanged" runat="server">
                                   <asp:ListItem Value="0">Select Category </asp:ListItem>
                                   <asp:ListItem Value="1">Cash Receipts - Donation Checks</asp:ListItem>
                                   <asp:ListItem Value="2">Cash Receipts - Other Deposits</asp:ListItem>
                                   <asp:ListItem Value="3">Cash Receipts - Credit Card Revenues</asp:ListItem>
                                   <asp:ListItem Value="4">Cash Receipts � Investment Income</asp:ListItem>
                                   <asp:ListItem Value="5">Vouchers - Expense checks</asp:ListItem>
                                   <asp:ListItem Value="6">Cash Receipts - ACH/EFT  Donations</asp:ListItem>
                                   <asp:ListItem Value="7">Vouchers - Bank Service Charges / Credit Card Fees</asp:ListItem>
                                   <asp:ListItem Value="8">Vouchers - TransferOut</asp:ListItem>
                                   <asp:ListItem Value="9">Vouchers - Buy Transactions</asp:ListItem>
                                   <asp:ListItem Value="10">Vouchers- Sell Transaction</asp:ListItem>
                               </asp:DropDownList>
           
                        </td>
                        <td></td>
                     </tr> 
                     <tr runat ="server" id="trbanks" visible = "false">
                            <td align="left">Account  </td><td align="left">:<asp:ListBox ID="lstBank" SelectionMode="Multiple" runat="server"></asp:ListBox>
                            </td> 
                            <td></td>
                      </tr> 
                      <tr><td align="left">From </td><td align="left"> : 
                            <asp:TextBox ID="TxtFrom" runat="server"></asp:TextBox></td> <td>
                                <asp:RequiredFieldValidator ControlToValidate="TxtFrom" ID="RequiredFieldValidator1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                            </td>
                      </tr>
                      <tr><td align="left">To </td><td align="left"> : 
                           <asp:TextBox ID="txtTo" runat="server"></asp:TextBox></td> <td>
                           <asp:RequiredFieldValidator ControlToValidate="txtTo" ID="RequiredFieldValidator2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                           </td>
                      </tr>
                      <tr><td align="center" colspan="3">
                             <asp:Button ID="BtnSubmit" OnClick="BtnSubmit_Click" runat="server" Text="Submit" />
                             &nbsp;&nbsp;&nbsp;
                          <asp:Button ID="btnGenAll" OnClick="btnGenAll_Click"  Visible="false"  runat="server" Text="Generate All QB" />
                            &nbsp;&nbsp;&nbsp;<asp:HiddenField ID="hdnSQL" runat="server" />
                            &nbsp;<table border ="1" style ="border-style:dotted" cellpadding="0" cellspacing="0"><tr><td align="center">
                          <asp:Button ID="btnAddUpdateGL" OnClick="btnAddUpdateGL_Click"  Visible="false"  runat="server" Text="Add/Update GL" />
                          &nbsp;&nbsp;&nbsp;
                          <asp:Button ID="btnGLReports" OnClick="btnGLReports_Click"   runat="server" Text="GL Reports" />
                          &nbsp;&nbsp;&nbsp;
                          <asp:Button ID="btnGLExport" OnClick="btnGLExport_Click"   runat="server" Text="Export GL Reports" />
                          <br />
                          (GL for All categories)</td> </tr></table>
                         </td> 
                      </tr>
                      <tr><td align="center" colspan="3">
                            <table id="tblDupRec" runat="server" visible="false">
                                <tr><td>
                                    <asp:Label ID="lblRecDup" runat="server" Text="GL records were created earlier for this period. Do you want to delete the old and add new ones?" ForeColor="Red">
                                    </asp:Label>
                                </td></tr>
                                <tr><td align="center" colspan="3">
                                        <asp:Button ID="BtnYes" runat="server" ForeColor="Red" Text="Yes" /> 
                                        <asp:Button ID="BtnNo" runat="server" ForeColor="Red" Text="No" />
                                </td></tr>
                            </table>
                          </td>
                      </tr> 
                      <tr><td align="center" colspan="2"><asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label></td></tr>
                </table>
            </td>
       </tr>
       <tr>
       <td align="left"><asp:Button ID="BtnExportList" runat="server" Text="Export List To Excel"  Visible="false" /></td>
        <td align="right"><asp:Button ID ="BtnDetRawData" runat="server" Text="Detailed Raw Data" Visible="false" /></td>
        
        </tr>
       <tr runat="server" id="TrDetailView" visible="false" bgcolor="#FFFFFF">
          <td  align="center">   
             <div align="center" ><span class="title04">Panel 2</span> </div>
                <asp:Button ID="btnExport" runat="server" OnClick="btnExport_Click" Text="Export to Excel" /><br />
                <asp:DataGrid OnUpdateCommand="grdEditVoucher_UpdateCommand" OnEditCommand="grdEditVoucher_EditCommand" ID="grdEditVoucher" runat="server"   AutoGenerateColumns="False" AllowSorting="True" DataKeyField="DonationID"
					 GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<HeaderStyle Font-Bold="true" ForeColor="White"  />
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                 <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                                 <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="DonationID" ReadOnly="true" DataField="DonationID"></asp:BoundColumn>
                                 <asp:BoundColumn  HeaderStyle-ForeColor="White" HeaderText="DepositSlip"  ReadOnly="true"  DataField="DepositSlip"></asp:BoundColumn>
                                 <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="Name"  ReadOnly="true"  DataField="DName"></asp:BoundColumn>
                                  <asp:TemplateColumn  HeaderText="Donationdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:Label>
                                      </ItemTemplate>
                                     <%-- <EditItemTemplate>
                                       <asp:TextBox ID="txtDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Depositdate")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                 <asp:TemplateColumn  HeaderText="Depositdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositDate")%>'></asp:Label>
                                      </ItemTemplate>
                                     <%-- <EditItemTemplate>
                                       <asp:TextBox ID="txtDepositdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Depositdate")%>'></asp:TextBox>
                                      </EditItemTemplate>--%>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                
                                <asp:TemplateColumn HeaderText="DonorType"  ItemStyle-Width="10%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblDonorType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonorType")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                      <asp:DropDownList ID="ddlDonorType" runat="server"  OnPreRender="SetDropDown_DonorType" AutoPostBack="false">
                                      <asp:ListItem>OWN</asp:ListItem>
                                       <asp:ListItem>IND</asp:ListItem>
                                        <asp:ListItem>SPOUSE</asp:ListItem>
	                                    </asp:DropDownList>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="true" />
                                 </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="BusType" >
                                      <ItemTemplate>
                                          <asp:Label ID="lblBusType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.BusType")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                  <asp:DropDownList runat="server" ID="ddlBusType" OnPreRender="SetDropDown_BusType" AutoPostBack="false">
					               <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>
					                <asp:ListItem Text="NRI,Business" Value="NRI,Business"></asp:ListItem>
					                <asp:ListItem Text="NRI, Ethnic" Value="NRI, Ethnic"></asp:ListItem>
					                <asp:ListItem Text="NRI, Professional" Value="NRI, Professional"></asp:ListItem>
					                <asp:ListItem Text="NRI, Other" Value="NRI, Other"></asp:ListItem>
					                <asp:ListItem Text="Press, India" Value="Press, India"></asp:ListItem>
					                <asp:ListItem Text="Press, NRI" Value="Press, NRI"></asp:ListItem>
					                <asp:ListItem Text="Press, US" Value="Press, US"></asp:ListItem>
					                <asp:ListItem Text="Religious, Temples" Value="Religious, Temples"></asp:ListItem>
					                <asp:ListItem Text="Religious, Other" Value="Religious, Other"></asp:ListItem>
					                <asp:ListItem Text="University_College_School" Value="University_College_School"></asp:ListItem>
					                <asp:ListItem Text="Corporation_LLC" Value="Corporation_LLC"></asp:ListItem>
					                <asp:ListItem Text="United Way" Value="United Way"></asp:ListItem>
					                <asp:ListItem Text="Other Non-Profit" Value="Other Non-Profit"></asp:ListItem>
					                <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
					                <asp:ListItem></asp:ListItem>
					              </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="IRSCat"  ItemStyle-Width="14%" HeaderStyle-Width="14%">
                                      <ItemTemplate><asp:Label ID="lblIRSCat" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.IRSCat")%>'></asp:Label>
                                      </ItemTemplate>
                                     <EditItemTemplate>
                                    <asp:DropDownList runat="server" ID="ddlIRSCat"  OnPreRender="SetDropDown_IRSCat" AutoPostBack="false">
	                                <asp:ListItem Text="Profit" Value="Profit"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,501(c)(3)" Value="Non-proft,501(c)(3)"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,PAC" Value="Non-proft,PAC"></asp:ListItem>
			                        <asp:ListItem Text="Non-proft,Other" Value="Non-proft,Other"></asp:ListItem>
                                    <asp:ListItem></asp:ListItem>
                                    </asp:DropDownList></EditItemTemplate>
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="DonationType"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblDonationType" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DonationType")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                         <asp:DropDownList ID="ddldonationtype" runat="server"  OnPreRender="SetDropDown_donationtype" AutoPostBack="false">
                        <asp:ListItem Value="1" Text="Unrestricted" Selected="True"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Temp Restricted"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Perm Restricted"></asp:ListItem></asp:DropDownList> 
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="ChapterCode"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblChapterCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ChapterCode")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList  DataTextField="WebFolderName" DataValueField="ChapterID"   ID="ddlChapterCode"  runat="server" OnPreRender="SetDropDown_Chapter"  AutoPostBack="false">
					                 <asp:ListItem>SPOUSE</asp:ListItem>
					                      </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Amount"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:C}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtAmount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Amount","{0:F}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn Visible="false"  HeaderText="Memberid" >
                                      <ItemTemplate>
                                         <asp:Label ID="lblMemberid" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>'></asp:Label>
                                      </ItemTemplate>                                     
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                </Columns>
  				</asp:DataGrid>
                &nbsp; &nbsp;&nbsp;
               <asp:Button ID="btnClose" Visible="false" OnClick="btnClose_Click"  runat="server" Text="Close Panel 2" />
               <br />
               <br />
            </td>
       </tr>
       <tr  runat="server" id="TrDonation" bgcolor="#FFFFFF" ><%--id="trEdit" --%>
          <td  align="center"> 
            <asp:GridView Width="1000px"  Visible="false" DataKeyNames="DepositSlip"  ID="gvDonation"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                <Columns>                
                    <asp:BoundField DataField ="DepositSlip"  HeaderText="DepositSlip #" />
                    <asp:HyperLinkField DataNavigateUrlFields="DepositSlip,DepositDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}"  HeaderText="Voucher" Text="View/Print" />                               
                    <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                    <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>
                    <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                    <asp:BoundField DataField="DepositDate" HeaderText="DepositDate" DataFormatString="{0:d}" />
                    <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                    <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                    <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                    <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                     <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID" />
                    <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                    <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                    <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                    <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />                               
                    <asp:HyperLinkField DataNavigateUrlFields="DepositSlip,DepositDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DSlip={0}&DType=RawData&DDate={1:MM/dd/yyyy}"  HeaderText="Detailed Raw Data" Text="Detailed Raw Data" />   
                   
                </Columns>
                <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="1000px"  Visible="false" DataKeyNames="DepositSlip"  ID="GVTDA"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                <Columns>                
                    <asp:BoundField DataField ="DepositSlip"  HeaderText="TDA A/C" />
                    <asp:BoundField DataField="StartDate" HeaderText="StartDate" DataFormatString="{0:d}" />
                    <asp:BoundField DataField="EndDate" HeaderText="EndDate" DataFormatString="{0:d}" />
                    <asp:HyperLinkField DataNavigateUrlFields="StartDate,EndDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DType=CC&SDt={0:MM/dd/yyyy}&EDt={1:MM/dd/yyyy}"  HeaderText="Voucher" Text="View/Print" />                               
                    <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                    <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>
                    <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                    <asp:BoundField DataField="DepositDate" HeaderText="DepositDate" DataFormatString="{0:d}" />
                    <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                </Columns>
                <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                <FooterStyle BackColor="White" ForeColor="#000066" />
                <RowStyle ForeColor="#000066" />
                <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" DataKeyNames="CheckNumber"  ID="gvExp"  runat="server" AllowPaging="false" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:BoundField DataField ="CheckNumber"  HeaderText="Check#" />
                        <asp:HyperLinkField DataNavigateUrlFields="CheckNumber,DatePaid" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DSlip={0}&DType=Ex&DDate={1:MM/dd/yyyy}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="DateCashed" HeaderText="DateCashed" DataFormatString="{0:d}" /><%--Added on 29-04-2013 to display all checks--%>
                        <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                        <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                        <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID" ItemStyle-HorizontalAlign="Center" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false"   ID="GVCCRevenues"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        
                        <asp:HyperLinkField DataNavigateUrlFields="StartDate,EndDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DType=CC&SDt={0:MM/dd/yyyy}&EDt={1:MM/dd/yyyy}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                         <%--<asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate" HeaderText="End_Date" DataFormatString="{0:d}" />
                        <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                        <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                        <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />
                        <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />--%>
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVACT_EFTDon"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:HyperLinkField DataNavigateUrlFields="TransDate,BankID,BankTransID" DataNavigateUrlFormatString="~/ShowVoucher.aspx?&DType=ACT_EFT&TDate={0:MM/dd/yyyy}&BankID={1}&BankTransID={2}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                        <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="TransCat" HeaderText="TransCat" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransType" HeaderText="Trans Type"  Visible="False" />
                        <asp:BoundField DataField="VendCust" HeaderText="VendCust"  Visible="False" />
                        <asp:BoundField DataField="Reason" HeaderText="Reason"  Visible="False" />
                        <asp:BoundField DataField="Description" HeaderText="Description"  Visible="False" />
                        <asp:BoundField DataField="Addinfo" HeaderText="Addinfo"  Visible="False" />
                        <asp:BoundField DataField="DonorType" HeaderText="DonorType"  Visible="False" />
                        <asp:BoundField DataField="BankTransID" HeaderText="BankTransID"  Visible="True" />
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVBnk_CCFees"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:HyperLinkField DataNavigateUrlFields="TransDate,BankID,VoucherNo,VendCust,ExpCatCode" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DType=CCFees&TDate={0:MM/dd/yyyy}&BankID={1}&VoucherNo={2}&VendCust={3}&ExpCatCode={4}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                          <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID"/>
                          <asp:BoundField DataField="TransType" HeaderText="TransType" Visible="False"/>
                          <%--<asp:BoundField DataField="BankTransID" HeaderText="BankTransID" Visible="False"/>--%>
                          <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate" DataFormatString="{0:d}" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <asp:BoundField DataField="AccountName" HeaderText="AccountName" Visible="False"/>
                      <asp:BoundField DataField="ExpCatCode" HeaderText="ExpCatCode" Visible="True" />
                      <asp:BoundField DataField="VendCust" HeaderText="VendCust" Visible="True" />
                      <asp:BoundField DataField="Reason" HeaderText="Reason" Visible="False"/>
                      <asp:BoundField DataField="AddInfo" HeaderText="AddInfo" Visible="False"/>
                   
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>  
            <asp:GridView Width="800px"  Visible="false" ID="GVTransfers"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:HyperLinkField DataNavigateUrlFields="TransDate,BankID,Ticker,Quantity,Amount,StartDate,EndDate" DataNavigateUrlFormatString="~/ShowVoucher.aspx?&DType=Tranfers&TDate={0:MM/dd/yyyy}&BankID={1}&Ticker={2}&Quantity={3}&Amount={4}&StartDate={5}&EndDate={6}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                       <%-- <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="BrokTransID" HeaderText="BrokTransID"  Visible="False"/>
                        <asp:BoundField DataField="BankCode" HeaderText="BankCode"  Visible="True"/>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="TransCat" HeaderText="TransCat"  Visible="False"/>
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}"  />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransType" HeaderText="Trans Type"  Visible="False" />
                        <asp:BoundField DataField="AssetClass" HeaderText="AssetClass" Visible="False" />
                        <asp:BoundField DataField="Ticker" HeaderText="Ticker"  />
                        <asp:BoundField DataField="Quantity" HeaderText="Quantity"/>
                        <asp:BoundField DataField="Price" HeaderText="Price" Visible="False" />
                        <asp:BoundField DataField="RestType" HeaderText="RestType" Visible="False" />
                        <asp:BoundField DataField="RestTypeTo" HeaderText="RestTypeTo" Visible="False" />
                        <asp:BoundField DataField="ToBankID" HeaderText="ToBankID" Visible="True" />
                        <asp:BoundField DataField="Description" HeaderText="Description" Visible="False"/>
                        
                      <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false" ID="GVBuyTrans"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:HyperLinkField DataNavigateUrlFields="TransDate,BankID,TransCat,StartDate,EndDate,AssetClass" DataNavigateUrlFormatString="~/ShowVoucher.aspx?&DType=Buy_Sell&TDate={0:MM/dd/yyyy}&BankID={1}&TransCat={2}&StartDate={3}&EndDate={4}&AssetClass={5}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                       <%-- <asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="BankID" HeaderText="BankID"  Visible="True"/>
                        <asp:BoundField DataField="BankCode" HeaderText="BankCode"  Visible="True"/>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Voucher#" />
                        <asp:BoundField DataField="TransDate" HeaderText="TransDate"  Visible="True" DataFormatString="{0:d}"/>
                        <asp:BoundField DataField="TransCat" HeaderText="TransCat"  Visible="False"/>
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate"  HeaderText="End_Date" DataFormatString="{0:d}"  />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="TransType" HeaderText="Trans Type"  Visible="False" />
                        <asp:BoundField DataField="AssetClass" HeaderText="AssetClass"  />
                       <%-- <asp:BoundField DataField="Ticker" HeaderText="Ticker"  />--%>
                        <asp:BoundField DataField="Description" HeaderText="Description" Visible="False"/>
                        
                        </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            <asp:GridView Width="800px"  Visible="false"   ID="GVTDAIncome"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        
                        <asp:HyperLinkField DataNavigateUrlFields="StartDate,EndDate,BankID" DataNavigateUrlFormatString="~/ShowVoucher.aspx?DType=CC&SDt={0:MM/dd/yyyy}&EDt={1:MM/dd/yyyy}&BID={2}"  HeaderText="Voucher" Text="View/Print" />                               
                        <asp:ButtonField CommandName="GenerateQB"  Text="Generate QB" headerText="Generate QB" ></asp:ButtonField>
                         <%--<asp:ButtonField CommandName="ViewTrans"  Text="View Trans/Export to Excel" headerText="View Trans/Export to Excel" ></asp:ButtonField>--%>
                        <asp:BoundField DataField="VoucherNo" HeaderText="Cash Receipt#" />
                        <asp:BoundField DataField="StartDate" HeaderText="Start_Date" DataFormatString="{0:d}" />
                        <asp:BoundField DataField="EndDate" HeaderText="End_Date" DataFormatString="{0:d}" />
                         <asp:BoundField DataField="BankCode" HeaderText="BankCode"  />
                        <%--<asp:BoundField DataField="DonorType" HeaderText="Donor Type" />
                        <asp:BoundField DataField="BusType" HeaderText="Business Type" />
                        <asp:BoundField DataField="DonationType" HeaderText="Donation Type" />--%>
                        <asp:BoundField DataField="BankID" ItemStyle-HorizontalAlign="Center" HeaderText="BankID"   />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                       <%-- <asp:ButtonField CommandName="ViewTrans" ItemStyle-Wrap="true"   HeaderText="Reconcile"  DataTextField="Reconcile" />                               
                        <asp:BoundField  ItemStyle-Wrap="true"  DataField="Dataposted" HeaderText="Bank DB" />
                        <asp:HyperLinkField DataNavigateUrlFields="DepSlipNumber,DepositDate,BankID" DataNavigateUrlFormatString="~/BankTransEdit.aspx?DSlip={0}&DType=d&DDate={1:MM/dd/yyyy}&BankID={2}"  HeaderText="Dep_Slip_in_DB" DataTextField="DSlipNo" />   --%>                            
                    </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
            </asp:GridView>
            &nbsp; &nbsp;&nbsp;
            <br /><br />
            <asp:GridView Width="800px"  Visible="false"   ID="GVGLTemp"  OnPageIndexChanging  ="GVGLTemp_PageIndexChanging"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None" CssClass="GridStyle" CaptionAlign="Top" PageSize="100" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
                    <Columns>                
                        <asp:BoundField DataField="VoucherNo" HeaderText="VoucherNo" />
                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString ="{0:d}" />
                        <asp:BoundField DataField="TransType" HeaderText="TransType" />
                        <asp:BoundField DataField="BankDate" HeaderText="BankDate" DataFormatString ="{0:d}" />
                        <asp:BoundField DataField="Account" HeaderText="Account" />
                         <asp:BoundField DataField="Description" HeaderText="Description" />
                        <asp:BoundField DataField="Class" HeaderText="Class" />
                        <asp:BoundField DataField="Ticker" HeaderText="Ticker" />
                        <asp:BoundField ItemStyle-Wrap="false" DataField="Amount" HeaderText="Amount" DataFormatString="{0:c}" />
                        <asp:BoundField DataField="BankName" HeaderText="BankName" />
                        <asp:BoundField DataField="CheckNumber" HeaderText="CheckNumber" />
                        <asp:BoundField DataField="PayeeName" HeaderText="Payee Name" />
                         <asp:BoundField DataField="StartDate" HeaderText="StartDate" DataFormatString ="{0:d}" />
                         <asp:BoundField DataField="Enddate" HeaderText="Enddate" DataFormatString ="{0:d}" />
                       </Columns>
                    <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
                    <FooterStyle BackColor="White" ForeColor="#000066" />
                    <RowStyle ForeColor="#000066" />
                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
               </asp:GridView>
         </td>
       </tr>   
       
    </table>
   </div>
</asp:Content>