Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls


Partial Class ContRegSearchResult
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        Dim conn As New SqlConnection(Application("ConnectionString"))
        'Dim prmArray(6) As SqlParameter

        'prmArray(0) = New SqlParameter
        'prmArray(0).ParameterName = "@contestyear"
        'prmArray(0).Value = Session("contestyear")
        'prmArray(0).Direction = ParameterDirection.Input

        'prmArray(1) = New SqlParameter
        'prmArray(1).ParameterName = "@ChildName"
        'prmArray(1).Value = Session("contestantname")
        'prmArray(1).Direction = ParameterDirection.Input

        'prmArray(2) = New SqlParameter
        'prmArray(2).ParameterName = "@ParentName"
        'prmArray(2).Value = Session("ParentName")
        'prmArray(2).Direction = ParameterDirection.Input

        'prmArray(3) = New SqlParameter
        'prmArray(3).ParameterName = "@Email"
        'prmArray(3).Value = Session("email")
        'prmArray(3).Direction = ParameterDirection.Input

        'prmArray(4) = New SqlParameter
        'prmArray(4).ParameterName = "@Phone"
        'prmArray(4).Value = Session("phone")
        'prmArray(4).Direction = ParameterDirection.Input

        'prmArray(5) = New SqlParameter
        'prmArray(5).ParameterName = "@SpouseName"
        'prmArray(5).Value = Session("spousename")
        'prmArray(5).Direction = ParameterDirection.Input
        Dim SQl As String
        SQl = " Select isnull(convert(varchar(12),a.CreateDate,101),'1/1/2008') 'date_added', contestdesc 'register_for', "
        SQl = SQl & "c.ChapterCode as 'Center', d.first_name + ' ' + d.last_name 'Participant Name', d.gender, e.HPhone 'Phone', "
        SQl = SQl & "e.CPhone,e.automemberid, e.firstname + ' ' + e.lastname 'Parent Name',  e.gender 'PGender', e. MaritalStatus 'MStatus' ,"
        SQl = SQl & "e.email as 'E-Mail', e.chapter 'PChapter',  'Valid' Status,a.ContestYear 'Year', a.fee, "
        SQl = SQl & "CASE isnull(a.paymentreference,'Pending') WHEN 'Pending' then 'Pending' ELSE 'Paid' END 'RegFee_status', "
        SQl = SQl & "a.paymentreference as 'regfee_reference',a.paymentdate , a.childnumber, a.parentid, a.contestid, a.chapterid 'CChapter', "
        SQl = SQl & "a.BadgeNumber, a.grade, date_of_birth, d.schoolname, a.contestant_id, Rank, score1 'phase1_score', "
        SQl = SQl & "score2 as 'phase2_score', nationalinvitee 'national_invitee', photo_image, national_invitee_status, "
        SQl = SQl & "invite_decline_comments, a.eventid, a.productid, a.productcode, a.productgroupid, a.productgroupcode, "
        SQl = SQl & "d.last_name as  'lastname', d.first_name  'firstname' , contestdate, e.address1+ ' ' +e.address2 'address', "
        SQl = SQl & "e.city+ ' ' + e.zip+ ' ' +e.state 'State', g.firstname + ' ' + g.lastname 'SpouseName'"
        SQl = SQl & "from contestant a"
        SQl = SQl & " Inner Join contest f on a.contestid = f.contestid"
        SQl = SQl & " Inner Join contestcategory b on f.contestcategoryid = b.contestcategoryid"
        SQl = SQl & " Inner Join chapter c on c.chapterid = f.nsfchapterID  "
        SQl = SQl & " Inner Join child d on a.childnumber = d.childnumber  "
        SQl = SQl & " Inner Join indspouse e on a.parentid = e.automemberid  "
        If Session("spousename").ToString().Length > 0 Then
            SQl = SQl & " Inner Join indspouse g on a.parentid = g.relationship  "
        Else
            SQl = SQl & " Left Join indspouse g on a.parentid = g.relationship  "
        End If
        SQl = SQl & " where(d.memberid = e.automemberid)"
        SQl = SQl & " and a.ContestYear = " & Session("contestyear")
        If Session("contestantname").ToString.Length > 0 Then SQl = SQl & " and d.first_name + ' ' + d.last_name  like '%" & Session("contestantname") & "%' "
        If Session("ParentName").ToString.Length > 0 Then SQl = SQl & " and e.firstname + ' ' + e.lastname like '%" & Session("ParentName") & "%'"
        If Session("email").ToString.Length > 0 Then SQl = SQl & " and (e.email like '%" & Session("email") & "%' or g.email like '%" & Session("email") & "%') "
        If Session("phone").ToString.Length > 0 Then SQl = SQl & " and e.Hphone like '%" & Session("phone") & "%'"
        If Session("spousename").ToString.Length > 0 Then SQl = SQl & " and g.firstname + ' ' + g.lastname like '%" & Session("spousename") & "%'"
        Dim ds As DataSet = Nothing
        'Response.Write(SQl)
        Try
            ' ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_Contest_Registration_Search", prmArray)
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQl)
        Catch se As SqlException
            Response.Write(se.ToString())
            Return
        End Try

        If (ds.Tables(0).Rows.Count > 0) Then

            drgsearchrresut.DataSource = ds.Tables(0)
            drgsearchrresut.DataBind()
            lblErr.Text = ""
        Else
            lblErr.Text = "No Record found"

        End If

    End Sub


    Protected Sub drgsearchrresut_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles drgsearchrresut.PageIndexChanging

        drgsearchrresut.PageIndex = e.NewPageIndex
        drgsearchrresut.DataBind()
    End Sub
End Class
