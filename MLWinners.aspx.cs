﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;

public partial class MLWinners : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {


        //if (Session["LoginID"] == null)
        //{
        //    Response.Redirect("~/Maintest.aspx");
        //}


        if (!IsPostBack)
        {
            PopulateYear(ddlContestYear);
            PopulateGrade(ddlGrade);
            PopulateChapter(ddlChapter);
            PopulatePlace(ddlPlace);
            populateState(ddlState);
            PopulateContestName(ddlContestName);
            fetchRecord();
        }
    }


    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            //for (int i = 1993; i <= MaxYear; i++)
            //{
            //    list.Add(new ListItem(i.ToString(), i.ToString()));
            //}
            for (int i = MaxYear; i >= 1993; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }

            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }

    }
    protected void PopulateGrade(DropDownList ddlObject)
    {
        ArrayList list = new ArrayList();
        for (int i = 1; i <= 13; i++)
        {
            list.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlObject.Items.Clear();
        ddlObject.DataSource = list;
        ddlObject.DataTextField = "Text";
        ddlObject.DataValueField = "Value";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select Grade", "-1"));
    }
    protected void PopulateChapter(DropDownList ddlObject)
    {
        try
        {
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID,ChapterCode from Chapter order by state,ChapterCode");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ChapterCode";
                ddlObject.DataValueField = "ChapterID";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select Chapter]", "-1"));
            }
        }
        catch (Exception ex) { }
    }
    protected void PopulatePlace(DropDownList ddlObject)
    {
        try
        {
            ArrayList list = new ArrayList();
            for (int i = 1; i <= 50; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Place]", "-1"));
        }
        catch (Exception ex) { }
    }
    protected void populateState(DropDownList ddlObject)
    {
        try
        {
            DataSet dsState = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Distinct StateCode,Name FROM StateMaster where statecode<>'US' ORDER BY NAME");
            if (dsState.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "StateCode";
                ddlObject.DataSource = dsState;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select State]", "-1"));
            }
        }
        catch (Exception ex) { }
    }
    protected void PopulateContestName(DropDownList ddlObject)
    {
        try
        {

            ArrayList list = new ArrayList();
            list.Add(new ListItem("Scripps National Spelling Bee", "1"));
            list.Add(new ListItem("National Geographic Bee", "3"));
            list.Add(new ListItem("MATHCOUNTS National Round", "4"));
            list.Add(new ListItem("National Middle School Science Bowl ", "5"));
            list.Add(new ListItem("You be the Chemist Challenge ", "8"));
            list.Add(new ListItem("Jeopardy Teen Tournament", "6"));
            list.Add(new ListItem("National History Bee – Elementary School", "7"));
            list.Add(new ListItem("USA Math Olympiad", "9"));
            list.Add(new ListItem("USA IMO", "10"));
            list.Add(new ListItem("White House Science Fair", "11"));
            list.Add(new ListItem("Who wants to be a Mathematician", "12"));

            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("Select Contest", "-1"));


        }
        catch (Exception ex) { }
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        divChildSearch.Visible = true;
        lblA.Text = "";
        txtChName.Text = "";
        txtParentName.Text = "";
        txtEmail.Text = "";
        txtPhone.Text = "";


    }
    protected bool dataCheck()
    {
        bool state = true;
        lblA.Text = "";
        if (txtChildName.Text == string.Empty)
        {
            lblA.Text = "Please enter child Name.";
            state = false;
            return state;
        }
        if (hdnChildID.Value == string.Empty)
        {
            lblA.Text = "Please search for child.";
            state = false;
            return state;
        }

        if (ddlContestYear.SelectedItem.Value == "-1")
        {
            lblA.Text = "Please select contest year.";
            state = false;
            return state;
        }

        //if (ddlGrade.SelectedItem.Value == "-1")
        //{
        //    if (int.Parse(ddlContestYear.SelectedItem.Text) > 2009)
        //    {
        //        lblA.Text = "Please select grade.";
        //        state = false;
        //        return state;
        //    }
        //}
        if (ddlChapter.SelectedItem.Value == "-1")
        {
            lblA.Text = "Please select Chapter.";
            state = false;
            return state;
        }
        if (ddlContestName.SelectedItem.Value == "-1")
        {
            lblA.Text = "Please select ContestName.";
            state = false;
            return state;
        }

        //if (ddlPlace.SelectedItem.Value == "-1")
        //{
        //    lblA.Text = "Please select Place.";
        //    state = false;
        //    return state;
        //}
        //if ((txtAmount.Text == string.Empty | txtAmount.Text == ""))
        //{
        //    lblA.Text = "Please enter Amount.";
        //    state = false;
        //    return state;
        //}

        if (string.IsNullOrEmpty(TxtCity.Text))
        {
            lblA.Text = "Please enter City.";
            state = false;
            return state;
        }
        if (ddlState.SelectedItem.Value == "-1")
        {
            lblA.Text = "Please select State.";
            state = false;
            return state;
        }


        else
        {
            int num;
            bool result = int.TryParse(txtAmount.Text, out num);
            if (result)
            {
                if (num < 1)
                {
                    lblA.Text = "Please enter Valid Amount.";
                    state = false;
                    return state;
                }
            }
        }


        return state;
    }
    protected void fetchRecord()
    {
        string SqlStr = "select M.MJLeagueID,M.Contestyear,M.Childnumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName,st.Name as state,I.chapter,"
        + " M.Chapterid,M.MemberID,M.Grade,M.Place,M.Contest,cast([Amount] as decimal(19,0)) as Amount,M.City,M.ST from MLWinners M INNER JOIN "
        + " CHILD CH ON CH.ChildNumber=M.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=M.MemberID left join StateMaster st on st.StateCode=M.ST  order by M.contestyear desc, M.contest,M.place ";
        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlStr);
        DataTable dt = dsRecords.Tables[0];
        GVRecords.DataSource = dt;
        GVRecords.DataBind();
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        lblchError.Text = "";
        bool executeQuery = false;
        string WhCont = string.Empty;
        string sqlString = "SELECT  CH.ChildNumber,CH.FIRST_NAME +' ' + CH.LAST_NAME as ChildName, ";
        sqlString += " I.FirstName +' ' + I.LastName as ParentName,CH.MEMBERID,I.Chapter as Center,I.City,I.State,";
        sqlString += " I.Email as ParentsEmail,I.HPhone as ParentsPhone ";
        sqlString += "  FROM   CHILD CH Inner JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID  ";
        sqlString += "  left join StateMaster St on st.StateCode=I.State ";
        if (txtChName.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
            }
        }
        if (txtParentName.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
        }

        if (txtEmail.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where (I.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and (I.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
        }
        if (txtPhone.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
        }
        try
        {
            DataSet ds = null;
            if (executeQuery == true)
            {
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlString + WhCont);
            }
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvChSearch.DataSource = ds.Tables[0];
                gvChSearch.DataBind();
                lblchError.Text = "";
            }
            else
            {
                lblchError.Text = "No Record found";
            }
        }
        catch (Exception ex) { }
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
                //sep = ";";
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {

                    Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                }
                Response.Write("\n");
            }

            // Response.Write(sw.ToString());
            //response.End()                           //       Thread was being aborted.
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        try
        {

            lblA.Text = "";
            string strSql;
            int cnt = 0;
            string GradeVal;
            string AmountVal;
            bool datavalid = dataCheck();
            if (datavalid == false) { return; }
            if (!this.IsValid) { return; }
            string place = string.Empty;
            if (ddlPlace.SelectedItem.Value == "-1")
            {
                place = "null";
            }
            else
            {
                place = ddlPlace.SelectedItem.Value;
            }


            if (ddlGrade.SelectedItem.Value == "-1")
            {
                GradeVal = "null";
            }
            else
            {
                GradeVal = ddlGrade.SelectedValue;
            }
            if ((txtAmount.Text == string.Empty | txtAmount.Text == ""))
            {
                AmountVal = "null";
            }
            else
            {
                AmountVal = txtAmount.Text;
            }

            strSql = "SELECT COUNT(MJleagueID) FROM MLWinners WHERE  [ChildNumber]=" + hdnChildID.Value
              + " and [ChapterID] =" + ddlChapter.SelectedItem.Value + " and [MemberID]=" + hdnMemberid.Value
              + " and [Grade] =" + GradeVal + " and [Place]=" + place + " and [ContestYear] ="
              + ddlContestYear.SelectedItem.Text + " and city='" + TxtCity.Text + "' and Amount=" + AmountVal + " and ST='" + ddlState.SelectedValue + "' ";


            if (btnAdd.Text == "Add")
            {
                if (cnt == 0)
                {
                    cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql).ToString());
                    string StrSqlInsert = "Insert Into [MLWinners](contestyear,childnumber,Chapterid,MemberID,Grade,Place,Contest,Amount,City,ST,createddate,createdby) Values ("
                        + ddlContestYear.SelectedValue + "," + hdnChildID.Value + "," + ddlChapter.SelectedItem.Value + "," + hdnMemberid.Value + "," + GradeVal
                        + "," + place + ",'" + ddlContestName.SelectedItem.Text + "'," + AmountVal + ",'" + TxtCity.Text + "','"
                        + ddlState.SelectedValue + "', getdate()," + Session["LoginID"].ToString() + ")";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, StrSqlInsert);
                    lblA.Text = "Record Added Successfully";
                    Clear();
                    fetchRecord();
                }
                else
                {
                    lblA.Text = "Record Already Exists";
                }
            }
            else if (btnAdd.Text == "Update")
            {
                string StrsqlUpdate = "update  MLWinners set contestyear=" + ddlContestYear.SelectedValue + ""
                                                          + ",childnumber=" + hdnChildID.Value + ""
                                                          + ",Chapterid=" + ddlChapter.SelectedItem.Value + ""
                                                          + ",MemberID=" + hdnMemberid.Value + ""
                                                          + ",Grade=" + ddlGrade.SelectedValue + ""
                                                          + ",Place=" + place + ""
                                                          + ",Contest='" + ddlContestName.SelectedItem.Text + "'"
                                                          + ",Amount=" + AmountVal + ""
                                                          + ",City='" + TxtCity.Text + "'"
                                                          + ",ST='" + ddlState.SelectedValue + "'"
                                                          + ",modifiedDate=getdate()"
                                                          + ",ModifiedBy=" + Session["LoginID"].ToString() + " where MJLeagueID=" + Session["LeagueID"] + "";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, StrsqlUpdate);
                lblA.Text = "Record updated Successfully";
                Clear();
                fetchRecord();
            }
        }
        catch
        {
        }
    }
    protected void gvChSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int index = -1;

        try
        {

            if (e.CommandArgument != null)
            {

                if (int.TryParse(e.CommandArgument.ToString(), out index))
                {
                    index = int.Parse((string)e.CommandArgument);
                    if (e.CommandName == "Select")
                    {

                        txtChildName.Text = gvChSearch.Rows[index].Cells[2].Text;
                        hdnChildID.Value = gvChSearch.Rows[index].Cells[1].Text;
                        hdnMemberid.Value = gvChSearch.Rows[index].Cells[4].Text;
                        ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByText(gvChSearch.Rows[index].Cells[5].Text));


                        txtChildName.Enabled = false;
                        divChildSearch.Visible = false;

                    }

                }
            }
        }

        catch (Exception ex) { }
    }
    private void Clear()
    {
        btnAdd.Text = "Add";
        hdnChildID.Value = string.Empty;
        hdnMemberid.Value = string.Empty;
        ddlContestYear.SelectedIndex = 0;
        ddlGrade.SelectedIndex = 0;
        ddlChapter.SelectedIndex = 0;
        ddlPlace.SelectedIndex = 0;
        txtChildName.Text = string.Empty;
        txtChildName.Enabled = true;
        TxtCity.Text = string.Empty;
        ddlState.SelectedIndex = 0;
        txtAmount.Text = string.Empty;
        ddlContestName.SelectedIndex = 0;

    }

    protected void gvChSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
                if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
            }
        }
        catch (Exception ex) { }

    }

    protected void GVRecords_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            lblA.Text = "";
            int index = int.Parse(e.CommandArgument.ToString());
            int LeagueID;
            LeagueID = Int32.Parse(GVRecords.Rows[index].Cells[2].Text);

            if (e.CommandName == "Select")
            {
                btnAdd.Text = "Update";
                Session["LeagueID"] = LeagueID;
                loadforUpdate(LeagueID);

            }
        }
        catch (Exception ex) { }
    }
    private void loadforUpdate(int LeagID)
    {
        try
        {
            string sqlStr = "SELECT  M.MJLeagueID, M.ChildNumber,M.MemberID,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, M.Contest,"
            + " I.ChapterID,I.Chapter,M.Grade,M.Place,M.ContestYear,M.city,M.ST,cast(M.[Amount] as decimal(19,0)) as Amount From MLWinners M INNER JOIN CHILD CH"
            + " ON CH.ChildNumber=M.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=M.MemberID WHERE MJLeagueID=" + LeagID;

            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, sqlStr);
            if (ds.Tables[0].Rows.Count > 0)
            {
                txtChildName.Text = ds.Tables[0].Rows[0]["ChildName"].ToString();
                hdnChildID.Value = ds.Tables[0].Rows[0]["ChildNumber"].ToString();
                hdnMemberid.Value = ds.Tables[0].Rows[0]["MemberID"].ToString();
                ddlContestYear.SelectedIndex = ddlContestYear.Items.IndexOf(ddlContestYear.Items.FindByText(ds.Tables[0].Rows[0]["ContestYear"].ToString()));
                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByText(ds.Tables[0].Rows[0]["Chapter"].ToString()));
                ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByText(ds.Tables[0].Rows[0]["Grade"].ToString()));
                ddlPlace.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByText(ds.Tables[0].Rows[0]["Place"].ToString()));
                ddlContestName.SelectedIndex = ddlContestName.Items.IndexOf(ddlContestName.Items.FindByText(ds.Tables[0].Rows[0]["Contest"].ToString()));
                txtAmount.Text = ds.Tables[0].Rows[0]["Amount"].ToString();
                ddlState.SelectedValue = ds.Tables[0].Rows[0]["ST"].ToString();
                TxtCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
                lblA.Text = "";
            }
        }
        catch (Exception ex) { }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        Clear();
        divChildSearch.Visible = false;
        gvChSearch.Visible = false;
    }
    protected void btnExport1_Click(object sender, EventArgs e)
    {
        fetchRecordForExport();
        GeneralExport((DataTable)Session["LeaguesList"], "MajorLeagues.xls");
    }
    protected void fetchRecordForExport()
    {
        string sqlStr = " select M.MJLeagueID,M.Contestyear,M.Childnumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName,"
                       + " case when I.Gender='Female' then I.FirstName +' ' + I.LastName else I1.FirstName+' '+I1.LastName end as MotherName,"
                       + " case when I.Gender='Male' then I.FirstName +' ' + I.LastName else I1.FirstName+' '+I1.LastName"
                       + " end as FatherName, I.chapter,M.Chapterid,M.MemberID,M.Grade,M.Place,M.Contest,"
                       + " case when I.Gender='Female' then I.hphone else I1.hphone end as Motherhphone,"
                       + " case when I.Gender='Male' then I.hphone else I1.hphone end as Fatherhphone,"
                       + " case when I.Gender='Female' then I.Cphone else I1.hphone end as MotherCphone,"
                       + " case when I.Gender='Male' then I.Cphone else I1.Cphone end as FatherCphone,"
                       + " case when I.Gender='Female' then I.Email else I1.Email end as MotherEmail,"
                       + " case when I.Gender='Male' then I.Email else I1.Email end as FatherEmail,"
                       + " cast([Amount] as decimal(19,0)) as Amount,M.City from MLWinners M INNER JOIN  CHILD CH ON"
                       + " CH.ChildNumber=M.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=M.MemberID"
                       + " left join IndSpouse I1  on((I.DonorType='IND' AND I.AutoMemberID =I1.Relationship) OR "
                       + " (I.DonorType='SPOUSE' AND  I.Relationship = I1.AutoMemberID)) order by Contestyear desc, Contest, place, CH.LAST_NAME, CH.FIRST_NAME ASC";

        DataSet dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
        DataTable dt = dsRecords.Tables[0];
        Session["LeaguesList"] = dt;


    }
    protected void btnclose_Click(object sender, EventArgs e)
    {
        divChildSearch.Visible = false;
        gvChSearch.Visible = false;
    }
    protected void GVRecords_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        GVRecords.PageIndex = e.NewPageIndex;
        fetchRecord();
    }
}