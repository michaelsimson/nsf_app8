Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class BadgeNumbers
    Inherits System.Web.UI.Page
    Dim chapterid As String
    Dim Contestcode As String
    Dim Roleid As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        hlnkback.NavigateUrl = "VolunteerFunctions.aspx"

        If Not Page.IsPostBack Then
            ' get roleid from Sessions
            Roleid = Session("RoleID")
            If (Roleid < 1 Or Roleid > 5) Then
                lblmsg.Visible = True
                lblmsg.Text = "You do not have access to this task."
                lblnotemsg.Visible = False
                lnkbadge.Visible = False
                lblbadgenumber.Visible = False
                grdbadgenumber.Visible = False
                lblgenbadgenumber.Visible = False
                grdgenbadgenumber.Visible = False
                lblregdate.Visible = False
                grdregdate.Visible = False
            Else
                Dim strSql As String
                Dim StrSql1 As String
                Dim StrSql2 As String
                Dim StrSql3 As String

                'if Chapter ID is not available
                StrSql3 = "Select nsfchapterid from contest where nsfchapterid = " & Session("SelChapterID") & " "
                Dim con1 As New SqlConnection(Application("ConnectionString"))
                Dim ds3 As DataSet = Nothing
                ds3 = SqlHelper.ExecuteDataset(con1, CommandType.Text, StrSql3)

                If ds3.Tables(0).Rows.Count = 0 Then
                    lblmsg.Visible = True
                    lblmsg.Text = "Selected Chapter is not available"
                    lblnotemsg.Visible = False
                    lnkbadge.Visible = False
                    lblgenbadgenumber.Visible = False
                    lblregdate.Visible = False
                    lblbadgenumber.Visible = False

                Else
                    If Session("SelChapterID") = 1 Then 'Added For JSB/JVB and  IVB.SSB at Finals  13/07/2012
                        strSql = "select c.chaptercode,case when b.contestcode = 'JSB' then 'JSB/JVB' else case when b.ContestCode='SSB' then 'SSB/IVB' else b.ContestCode End End as ContestCode from contest a, contestcategory b, chapter c where nsfchapterid = 1 and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate() and b.ContestCode not in('JVB','IVB') and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null)  Order by a.ProductID"
                        StrSql1 = "select c.chaptercode,case when b.contestcode = 'JSB' then 'JSB/JVB' else case when b.ContestCode='SSB' then 'SSB/IVB' else b.ContestCode End End as ContestCode from contest a, contestcategory b, chapter c where nsfchapterid = 1 and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate() and b.ContestCode not in('JVB','IVB') and contestid in (select distinct contestcode from contestant where badgenumber is not null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
                        StrSql2 = "select c.chaptercode,case when b.contestcode = 'JSB' then 'JSB/JVB' else case when b.ContestCode='SSB' then 'SSB/IVB' else b.ContestCode End End as ContestCode from contest a, contestcategory b, chapter c where nsfchapterid = 1 and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) > getdate() and b.ContestCode not in('JVB','IVB') and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"

                    Else 'get Chapter and Contest from data
                        strSql = "select c.chaptercode, b.contestcode from contest a, contestcategory b, chapter c where nsfchapterid = " & Session("SelChapterID") & "  and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate()and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
                        StrSql1 = "select c.chaptercode, b.contestcode from contest a, contestcategory b, chapter c where nsfchapterid = " & Session("SelChapterID") & "  and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate()and contestid in (select distinct contestcode from contestant where badgenumber is not null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
                        StrSql2 = "select c.chaptercode, b.contestcode from contest a, contestcategory b, chapter c where nsfchapterid = " & Session("SelChapterID") & "  and c.chapterid = a.nsfchapterid and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) > getdate()and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
                    End If

                    Dim con As New SqlConnection(Application("ConnectionString"))
                    Dim ds As DataSet = Nothing
                    Dim ds1 As DataSet = Nothing
                    Dim ds2 As DataSet = Nothing

                    Try
                        ds = SqlHelper.ExecuteDataset(con, CommandType.Text, strSql)
                        ds1 = SqlHelper.ExecuteDataset(con, CommandType.Text, StrSql1)
                        ds2 = SqlHelper.ExecuteDataset(con, CommandType.Text, StrSql2)

                    Catch se As SqlException
                        lblmsg.Text = se.Message
                        Return
                    End Try

                    'bind data to gridviews 
                    If (ds.Tables(0).Rows.Count > 0) Then
                        grdbadgenumber.DataSource = ds.Tables(0)
                        grdbadgenumber.DataBind()
                    Else
                        lblbadgenumber.Visible = False
                    End If
                    If (ds1.Tables(0).Rows.Count > 0) Then
                        grdgenbadgenumber.DataSource = ds1.Tables(0)
                        grdgenbadgenumber.DataBind()
                    Else
                        lblgenbadgenumber.Visible = False
                    End If

                    If (ds2.Tables(0).Rows.Count > 0) Then
                        grdregdate.DataSource = ds2.Tables(0)
                        grdregdate.DataBind()
                    Else
                        lblregdate.Visible = False
                    End If
                End If
            End If
        End If
    End Sub

    Protected Sub lnkbadge_Click(ByVal sgender As Object, ByVal e As System.EventArgs) Handles lnkbadge.Click

        Dim strSql As String
        If Session("SelChapterID") = 1 Then
            strSql = "select nsfchapterid ,case when b.contestcode = 'JSB' then 'JSB/JVB' else case when b.ContestCode='SSB' then 'SSB/IVB' else b.ContestCode End End as ContestCode from contest a, contestcategory b where nsfchapterid = " & Session("SelChapterID") & "  and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate()and  b.ContestCode not in('JVB','IVB') and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
        Else
            strSql = "select nsfchapterid , b.contestcode from contest a, contestcategory b where nsfchapterid = " & Session("SelChapterID") & "  and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and dateadd(dd,1,registrationdeadline) < getdate()and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Order by a.ProductID"
        End If
        'strSql = "select nsfchapterid , b.contestcode from contest a Left Join ExContestant Ex On Ex.ChapterID=a.NSFChapterID and Ex.ProductID=a.ProductId and Ex.ProductGroupID=a.ProductGroupId and Ex.ContestYear=a.Contest_Year and Ex.EventID=a.EventId, contestcategory b where nsfchapterid = " & Session("SelChapterID") & "  and a.contestcategoryid = b.contestcategoryid and contest_year = year(getdate()) and b.contestyear = year(getdate())and Case when Ex.NewDeadline is Null then dateadd(dd,1,registrationdeadline) Else dateadd(dd,1,Ex.NewDeadline)  END < getdate()and contestid in (select distinct contestcode from contestant where badgenumber is null and contestyear = year(getdate()) and paymentreference is not null) Group By nsfchapterid , b.contestcode Order by contestcode"
        Dim ContestCodeArray As String = ""
        Dim con As New SqlConnection(Application("ConnectionString"))
        Dim ds As DataSet = Nothing
        ds = SqlHelper.ExecuteDataset(con, CommandType.Text, strSql)

        'find row of selected radio button
        Dim i As Integer
        For i = 0 To grdbadgenumber.Rows.Count - 1

            Dim rd As GroupRadioButton = DirectCast(grdbadgenumber.Rows(i).FindControl("MyRadioBadgenumbers"), GroupRadioButton)
            If (rd.Checked = True) Then
                chapterid = ds.Tables(0).Rows(i)(0)
                Contestcode = ds.Tables(0).Rows(i)(1)
                ContestCodeArray = ContestCodeArray & "," & Contestcode
            End If
        Next i

        'pass chapter and contest to next page
        Dim url = "BadgeNumbers_Gen.aspx?"
        url += "cid=" & chapterid & "&"
        url += "contestcode=" & Contestcode
        Response.Redirect(url)
    End Sub

    
End Class
