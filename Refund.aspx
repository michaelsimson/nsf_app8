<%@ Page Language="vb" AutoEventWireup="false" MasterPageFile="~/NSFMasterPage.master" Inherits="VRegistration.Refund" CodeFile="Refund.aspx.vb" Title="Refund Request Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
  <table id="tblLogin"  runat="server" cellpadding="3" border = "0" cellspacing ="0" style="width :700px">
    <tr>
           <td class="ContentSubTitle" valign="top" nowrap align="center" colspan="2"><h1>Refund Request / Change Pending to Paid</h1></td>
    </tr>
    <tr>
      <td colspan="2"><asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
      </td>
    </tr>
    <tr>
      <td style="width :40%">&nbsp;</td> <td style="width :60%">&nbsp;</td>
    </tr>
    <tr id="trNaStatus" runat = "server" visible ="false">
      <td  class="ItemLabel" valign="top" nowrap align="right" >NA Status</td> <td>
          <asp:DropDownList ID="ddlNa_Status" runat="server">
              <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
              <asp:ListItem Value="1">Approved</asp:ListItem>
              <asp:ListItem Value="2">Declined</asp:ListItem>
              <asp:ListItem Value="3">Pending</asp:ListItem>
          </asp:DropDownList></td>
    </tr>
    <tr runat="server" id="trEmail" visible="true">
      <td class="ItemLabel" valign="top" nowrap align="right"> Email ID</td>
      <td><asp:TextBox ID="txtUserId" runat="server" CssClass="SmallFont" Width="300" MaxLength="50"></asp:TextBox>&nbsp;<br />
        &nbsp;
        <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
    </tr>
    <tr  runat="server" id="trNewUpdate" visible="true">
      <td class="ItemLabel" valign="top" nowrap align="right">New/Update</td> 
      <td><asp:DropDownList ID="DdlNewUpdate" runat="server" AutoPostBack="false">
          <asp:ListItem Value="1">New</asp:ListItem>
          <asp:ListItem Value="2">Update</asp:ListItem>
          <asp:ListItem Value="-1" Selected="True">Select Option</asp:ListItem>
          </asp:DropDownList></td>
    </tr>
    <tr runat="server" id="trContinue" visible="false">
      <td colspan="2" align="center">
      <asp:Button ID="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue"></asp:Button>
      </td>
    </tr>
    <tr>
      <td colspan="2" align="center"><asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label></td>
    </tr>
     </table>
     
     
   
      <table id="tblSelection"  runat="server" visible="false" cellpadding="3" border = "0" cellspacing ="0"  style="width :700px">
    <tr>
      <td class="ItemLabel" valign="top" nowrap align="right">Event</td>
      <td>
          <asp:DropDownList ID="DdlEventID" runat="server">
           <asp:ListItem Value="2">Contests, Regional</asp:ListItem>
            <asp:ListItem Value="1">Contests, Finals</asp:ListItem>
            <asp:ListItem Value="3">Workshop</asp:ListItem>
             <asp:ListItem Value="4">Game</asp:ListItem>
             <asp:ListItem Value="11">Donation</asp:ListItem>
              <asp:ListItem Value="13">Coaching</asp:ListItem>
             
            <asp:ListItem Value="-1" Selected="True">Select Event</asp:ListItem>
            
              <asp:ListItem Value="5">Walk-a-thon</asp:ListItem>
            
          </asp:DropDownList></td>
    </tr>    
    
    <tr>
      <td class="ItemLabel" valign="top" nowrap align="right" style="width :40%">Reason for Refund/Adjustment</td>
      <td  style="width :60%">
          <asp:DropDownList ID="DdlReason" runat="server">
           <asp:ListItem Value="-1" Selected="True">Select Reason</asp:ListItem>
           <asp:ListItem Value="1">Duplicate Charges</asp:ListItem>
           <asp:ListItem Value="2">Pending, but was Charged</asp:ListItem>  
           <asp:ListItem Value="3">Event Cancelled by NSF</asp:ListItem>
           <asp:ListItem Value="4">Refund Donation</asp:ListItem>
           <asp:ListItem Value="5">Hardship</asp:ListItem>               
          </asp:DropDownList> <asp:HyperLink  Text="Help" runat="server" ID="hlnkView" NavigateUrl="javascript:var w=window.open('RefundHelp.html','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=no,width=685,height=648;');"  ></asp:HyperLink>
          </td>    </tr>  
     <tr>
      <td colspan="2" align="center"> <asp:Button ID="btnSel1Continue" runat="server" CssClass="FormButtonCenter" Text="Continue"></asp:Button></td>
    </tr>   
    </table> 
    
    <table border = "0" cellpadding = "0" cellspacing = "0" id="tblData"  runat="server" visible="false">
    <tr>
    <td>
    <table  cellpadding="3" border = "0" cellspacing ="0"  style="width :350px">
    <tr ><td style="text-align:center" class="style10" colspan="2">Parent Section</td></tr>
    <tr ><td class="ItemLabel" valign="top" nowrap align="right">Refund Amount</td>
      <td>
          <asp:TextBox ID="txtRefundAmt" runat="server"></asp:TextBox></td> </tr>    
    <tr >
      <td class="ItemLabel" valign="top" nowrap align="right" style="width :40%">Amount Charged on your bill</td>
      <td >
          <asp:TextBox ID="txtAmtCharged" runat="server"></asp:TextBox>
          
          </td>         
    </tr>      
<tr ><td class="ItemLabel" valign="top" nowrap align="right">
Date of Charge on your bill</td>
      <td  class="ItemText">
          <asp:TextBox ID="txtDOCharged" runat="server"></asp:TextBox><br />
           Enter date as MM/DD/YYYY
          </td> </tr>    
<tr ><td class="ItemLabel" valign="top" nowrap align="right">
Detailed Explanation 
</td>
      <td>
          <asp:TextBox ID="txtExplanation" runat="server" TextMode="MultiLine" Height = "65px"></asp:TextBox></td> </tr> 
<tr ><td class="ItemLabel" valign="top" nowrap align="right">
First four digits of Credit Card 
</td>
      <td>
          <asp:TextBox ID="txtCardFNo" runat="server" MaxLength="4" Width="75px"></asp:TextBox></td> </tr> 
          <tr>
          <td class="ItemText" colspan="2" style="text-align :right " >        
          Note: please provide for the card you used for the first charge
          </td>
          </tr>
<tr><td class="ItemLabel" valign="top" nowrap align="right">
Last four digits of Credit Card
</td>
      <td >
          <asp:TextBox ID="txtCardLNo" runat="server" MaxLength="4" Width="75px"></asp:TextBox>
      </td></tr> 
           <tr>
          <td class="ItemText" colspan="2" style="text-align :right">        
          Note: please provide for the card you used for the first charge
          </td>
          </tr>
<tr><td class="ItemLabel" valign="top" nowrap align="right">
 Order Number (Paid)
</td><td><asp:TextBox ID="TxtONo" runat="server" Enabled = "false" Width="215px" ></asp:TextBox></td> </tr> 
<tr ><td class="ItemLabel" valign="top" nowrap align="right" style="height: 39px">
Payment Date
</td>
      <td>
          <asp:TextBox ID="TxtPDate" runat="server" Enabled="false" ></asp:TextBox></td> </tr> 
<tr ><td class="ErrorFont" valign="top" nowrap align="center" colspan="2">
    <asp:Label ID="lblDataErr" runat="server" Text=""></asp:Label>
 </td> </tr> 
<tr id="trParentButton" runat="server"><td colspan="2" align="center"><asp:Button ID="btnSubmit" runat="server" CssClass="FormButtonCenter" Text="Submit"></asp:Button> &nbsp;&nbsp; <asp:Button ID="BtnCancel" runat="server" CssClass="FormButtonCenter" Text="Cancel"></asp:Button></td>
    </tr>    
    <tr id ="TrTotalFee" runat ="server" visible ="false"><td align="right" >Total Amount Selected :  </td><td align="left"> <asp:Label ID="lblTotalFeeMsg" runat="server" Text="" CssClass="SmallFont"></asp:Label></td></tr>
    </table>
    </td>
    
    <%--Chapter Cooridnators functions--%>
    <td  style="vertical-align:top">
        <asp:Panel ID="pnlChapter" Visible = "false" runat="server">
        <table cellpadding="3" border = "0" cellspacing ="0"  style="width :300px">
        <tr><td style="text-align:center" class="style10" colspan="2">Chapter Review</td></tr>
    <tr><td class="ItemLabel" valign="top" nowrap align="right">Refund Amount</td>
      <td>
          <asp:TextBox ID="txtChRefundAmt" runat="server"></asp:TextBox></td> </tr>    
    
<tr><td class="ItemLabel" valign="top" nowrap align="right">
Detailed Explanation 
</td>
      <td>
          <asp:TextBox ID="txtChExplanation" runat="server" TextMode="MultiLine" Height = "65px"></asp:TextBox></td> </tr> 


<tr><td class="ItemLabel" valign="top" nowrap align="right">
Status
</td>
      <td>
          <asp:DropDownList ID="DdlChStatus" runat="server">
              <asp:ListItem Selected="True" Value ="0">Pending</asp:ListItem>
              <asp:ListItem Value="1">Approved</asp:ListItem>
              <asp:ListItem Value="2">Declined</asp:ListItem>
          </asp:DropDownList></td> </tr>    
<tr ><td class="ErrorFont" valign="top" nowrap align="center" colspan="2">
    <asp:Label ID="Label1" runat="server" Text=""></asp:Label>
 </td> </tr> 
<tr id="TrChButton" runat="server"><td colspan="2" align="center"><asp:Button ID="btnChSubmit" Enabled="false" runat="server" CssClass="FormButtonCenter" Text="Submit"></asp:Button> &nbsp;&nbsp; <asp:Button ID="BtnChCancel" runat="server" CssClass="FormButtonCenter" Text="Cancel"></asp:Button></td>
    </tr></table>
        </asp:Panel>
    </td>
    
     <%--National Cooridnators functions--%>
    <td valign ="bottom" >
        <asp:Panel ID="PnlNational" runat="server" Visible="false">
        <table cellpadding="3" border = "0" cellspacing ="0"  style="width :350px">
        <tr><td style="text-align:center" class="style10" colspan="2">National Review</td></tr>
    <tr><td class="ItemLabel" valign="top" nowrap align="right">Refund Amount</td>
      <td>
          <asp:TextBox ID="txtNaRefundAmt" runat="server"></asp:TextBox>
          <asp:Label ID="hdnPaidAmount" runat="server" ForeColor="White" ></asp:Label>
        </td> </tr>  
          <tr id="trAmtCharged" runat ="Server" visible ="false"><td class="ItemLabel" valign="top" nowrap align="right">Amount Charged</td>
      <td>
          <asp:TextBox ID="txtNaAmtCharged" runat="server"></asp:TextBox></td> </tr>      
<tr><td class="ItemLabel" valign="top" nowrap align="right">
Detailed Explanation 
</td>
      <td>
          <asp:TextBox ID="txtNaExplanation" runat="server" TextMode="MultiLine" Height = "65px"></asp:TextBox></td> </tr> 

<tr><td class="ItemLabel" valign="top" nowrap align="right">
 Order Number (Paid)
</td><td><asp:TextBox ID="TxtNaONo" runat="server" Width="215px" ></asp:TextBox></td> </tr> 
<tr ><td class="ItemLabel" valign="top" nowrap align="right">
Payment Date
</td>
      <td>
          <asp:TextBox ID="TxtNaPDate" runat="server" ></asp:TextBox></td> </tr> 
<tr><td class="ItemLabel" valign="top" nowrap align="right">
 Payment Notes
</td>
      <td>
          <asp:TextBox ID="TxtNaPNotes" runat="server" Height="80px" TextMode="MultiLine"></asp:TextBox></td> </tr> 
<tr><td class="ItemLabel" valign="top" nowrap align="right">
Status
</td>
      <td>
          <asp:DropDownList ID="DdlNaStatus" runat="server">
              <asp:ListItem Selected="True" Value ="0">Pending</asp:ListItem>
              <asp:ListItem Value="1">Approved</asp:ListItem>
              <asp:ListItem Value="2">Declined</asp:ListItem>
          </asp:DropDownList></td> </tr>    
<tr><td class="ErrorFont" valign="top" nowrap align="center" colspan="2">
    <asp:Label ID="lblNaErr" runat="server" Text=""></asp:Label>
 </td> </tr> 
<tr id="trNaButton" runat="server"><td colspan="2" align="center"><asp:Button OnClick="btnNaSubmit_Click" ID="btnNaSubmit" Enabled="false" runat="server" CssClass="FormButtonCenter" Text="Submit"></asp:Button> &nbsp;&nbsp; <asp:Button ID="btnNaCancel" runat="server" CssClass="FormButtonCenter" Text="Cancel"></asp:Button></td>
    </tr>
    <tr>
    <td colspan ="2" align = "center" >
     <asp:Label ID="lblMessage" runat="server" CssClass="keytext"  Text=""></asp:Label>
    </td>
    </tr>
    
    
    </table>
        </asp:Panel>
    </td>
    </tr>
    </table>  


<asp:Panel runat="server" ID="pnlNFG"   Visible="False">
  &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font class="style10">Panel 1</font> <br />
    &nbsp;&nbsp;&nbsp;&nbsp; <asp:Label ID="Label2" CssClass="keytext" runat="server" Text="Refunds"></asp:Label>
  <br />
 
     <asp:Label ID="Pnl1Msg"  ForeColor="red" Font-Bold ="true"  Font-Size ="12px"  runat="server" Text=""></asp:Label>   
     <asp:GridView ID="GridRefund"  runat="server" DataKeyNames="RefundID" AutoGenerateColumns="False" OnRowCommand="GridRefund_RowCommand"   AllowPaging="True" AllowSorting="True" >
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Edit" HeaderText="Edit" />
            <%-- <asp:ButtonField ButtonType="Link" CommandName="Delet"  Text="Delete" HeaderText="Delete" />--%>
             <asp:ButtonField Text="Delete" HeaderText="Delete" CommandName="Delete1" />
            <asp:BoundField DataField="chaptercode"  HeaderText="Chapter" />
            <asp:BoundField DataField="Pa_Paymentdate" HeaderText="Date Paid"  DataFormatString="{0:d}"/>
            <asp:BoundField DataField="Parent_Name" HeaderText="Parent_Name" />
            <asp:BoundField DataField="Pa_OrderNumber" HeaderText="Order Number" />
            <asp:BoundField DataField="Pa_ReqRefund" HeaderText="Refund Requested" />
            <asp:BoundField DataField="EventCode" HeaderText="Event"/>
            <asp:BoundField DataField="Pa_Reason" HeaderText="Reason" />
            <asp:BoundField DataField="Pa_ReqDate" HeaderText="Request Date"  DataFormatString="{0:d}"/>            
            <asp:BoundField DataField="pa_AmountCharged" HeaderText="Charged Amount" />
            <asp:BoundField DataField="Pa_ChargedDate" HeaderText="Charged Date" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="Ch_Status" HeaderText="Ch_Status" />
            <asp:BoundField DataField="Ch_Amount" HeaderText="Ch_Amount"/>
            <asp:BoundField DataField="Ch_DateApproved" HeaderText="Ch_DateApproved" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="Ch_ApprovedBy" HeaderText="Ch_ApprovedBy"/> 
            <asp:BoundField DataField="Na_Status" HeaderText="Na_Status" />
            <asp:BoundField DataField="Na_Amount" HeaderText="Na_Amount" /> 
            <asp:BoundField DataField="Na_DateApproved" HeaderText="Na_DateApproved"  DataFormatString="{0:d}" />
            <asp:BoundField DataField="Na_ApprovedBy" HeaderText="Na_ApprovedBy" />
            <asp:BoundField DataField="Pa_ChargedDate" HeaderText="ChargedDate"  DataFormatString="{0:d}" />
            <asp:BoundField DataField="Alpha" HeaderText="Alpha" />
            <asp:BoundField DataField="Beta" HeaderText="Beta" />
            <asp:BoundField DataField="Pa_Comments" HeaderText="Parent Comments" />           
            <asp:BoundField DataField="Ch_Comments" HeaderText="Ch_Comments" />
            <asp:BoundField DataField="Na_Comments" HeaderText="Na_Comments" />
             <asp:BoundField DataField="TempLink" HeaderText="TempLink" DataFormatString="{0:d}" />           
                     
        </Columns>    
    </asp:GridView>
    <br />
       &nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label3" CssClass="keytext" runat="server" Text="Payments"></asp:Label><br />
      
      <asp:Label ID="pnl1Msg2" Font-Size="12px" Font-Bold ="true"   ForeColor="red" runat="server" Text=""></asp:Label>   
     
    <asp:GridView Visible="true" ID="GridNFG" SelectedRowStyle-BackColor="#B3B7E8" runat="server" DataKeyNames="Unique_ID" AutoGenerateColumns="False"  OnRowCommand="GridNFG_RowCommand" AllowPaging="True" AllowSorting="True" >
        <Columns>    
            <asp:ButtonField ButtonType="Button" CommandName="Details" Text="Details" HeaderText="Details" /> 
            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select" HeaderText="Selection" />
            <asp:BoundField DataField="Payment Date"  HeaderText="Date Paid" DataFormatString="{0:d}" />
            <asp:BoundField DataField="Parent_Name" HeaderText="Parent Name"/>
            <asp:BoundField DataField="Order_No" HeaderText="Order Number"  />
            <asp:BoundField DataField="Fee" HeaderText="Fees" >
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="MealsAmount" HeaderText="Meal Charge" >
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Donation" HeaderText="Donation"/>
            <asp:BoundField DataField="Chapter" HeaderText="Chapter">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>             
            <asp:BoundField DataField="EventID" HeaderText="Event"/>            
            <asp:BoundField DataField="HPhone" HeaderText="H Phone">
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="CPhone" HeaderText="C Phone" />
            <asp:BoundField DataField="Email" HeaderText="Email"/>
            <asp:BoundField DataField="Address1" HeaderText="Street"/>
            <asp:BoundField DataField="City" HeaderText="City"/>
            <asp:BoundField DataField="State" HeaderText="State"/> 
            <asp:BoundField DataField="Zip" HeaderText="Zip"/> 
            <asp:BoundField DataField="TotalPayment" HeaderText="TotalPayment" Visible="false" /> 
            <asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes" Visible="false" /> 
        
        </Columns>    
        <SelectedRowStyle BackColor="#B3B7E8" />
    </asp:GridView>  
    
</asp:Panel>
     <br />  
    <asp:Panel ID="PanelDetails" runat="server" Visible="false">
   <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font class="style10"> Panel 2</font>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Close" runat="server" Text="Close Panel 2" /><br />
   <br />
   <asp:Label ID="lblDetailsErr" CssClass="keytext" runat="server" Text="Sorry! No Details Found." Visible="false" ></asp:Label>
   <asp:Panel ID="Details1" runat="server" Visible="false">
   <asp:Label ID="lblDetails1" CssClass="keytext" runat="server" Text="Contests"></asp:Label>   
    <br />
    <asp:GridView Visible="true" ID="GridDetails1"  runat="server" DataKeyNames="contestant_id" AutoGenerateColumns="False"  >
        <Columns> 
            <asp:BoundField DataField="Chapter"  HeaderText="Chapter" />
	        <asp:BoundField DataField="ContestYear" HeaderText="Contest Year"/>
            <asp:BoundField DataField="Parent_Name" HeaderText="Parent Name"/>           
            <asp:BoundField DataField="Child_Name" HeaderText="Child Name" />
            <asp:BoundField DataField="BadgeNumber" HeaderText="BadgeNumber" />
            <asp:BoundField DataField="Score1" HeaderText="Score1"/>
            <asp:BoundField DataField="Score2" HeaderText="Score2"/>
            <asp:BoundField DataField="Score3" HeaderText="Score3"/>
            <asp:BoundField DataField="Rank" HeaderText="Rank"/>
            <asp:BoundField DataField="Fee" HeaderText="Fee"/>
            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date" DataFormatString="{0:d}" />             
            <asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference"/>            
            <asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes" />
            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" DataFormatString="{0:d}" />
            <asp:BoundField DataField="EventCode" HeaderText="EventCode"/>
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"/>
            <asp:BoundField DataField="Created" HeaderText="CreatedBy"/>
        </Columns>    
    </asp:GridView>	
    </asp:Panel>
    <asp:Panel ID="Details2" runat="server" Visible="false">
    <br />
     <asp:Label ID="lblDetails2" CssClass="keytext" runat="server" Text="Meal Charge"></asp:Label>
    <br />
    <asp:GridView Visible="true" ID="GridDetails2"  runat="server" DataKeyNames="MealChargeID" AutoGenerateColumns="False"  >
        <Columns>               
            <asp:BoundField DataField="Chapter"  HeaderText="Chapter" />
    	    <asp:BoundField DataField="ContestYear" HeaderText="Contest Year"/>
            <asp:BoundField DataField="Parent_Name" HeaderText="Parent Name"/>           
            <asp:BoundField DataField="Name" HeaderText="Person Name" />
            <asp:BoundField DataField="RelationType" HeaderText="RelationType" />
            <asp:BoundField DataField="ContestDate" HeaderText="ContestDate" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="ChildFlag" HeaderText="ChildFlag"/>
            <asp:BoundField DataField="ChaperoneFlag" HeaderText="ChaperoneFlag"/>
            <asp:BoundField DataField="EventName" HeaderText="EventName"/>
            <asp:BoundField DataField="Amount" HeaderText="Amount"/>
            <asp:BoundField DataField="PaymentDate" HeaderText="Payment Date"  />             
            <asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference"/>            
            <asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes" />
            <asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" DataFormatString="{0:d}" />
            <asp:BoundField DataField="ModifiedUserID" HeaderText="ModifiedUserID"/>           
        </Columns>    
</asp:GridView>	
</asp:Panel>
<asp:Panel ID="Details3" runat="server" Visible="false">
<br />
 <asp:Label ID="lblDetails3" runat="server" CssClass="keytext" Text="Workshop"></asp:Label>
<br />
    <asp:Panel ID="Details4" runat="server" Visible="false">
        <br />
        <asp:Label ID="lblDetails4" runat="server" CssClass="keytext" Text="Donations"></asp:Label>
        <br />
        <asp:GridView ID="GridDetails4" runat="server" AutoGenerateColumns="False" 
            DataKeyNames="DonationID" Visible="true">
            <Columns>
                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />
                <asp:BoundField DataField="EventYear" HeaderText="Event Year" />
                <asp:BoundField DataField="Parent_Name" HeaderText="Parent Name" />
                <asp:BoundField DataField="DonorType" HeaderText="DonorType" />
                <asp:BoundField DataField="AMOUNT" HeaderText="AMOUNT" />
                <asp:BoundField DataField="TRANSACTION_NUMBER" 
                    HeaderText="TRANSACTION_NUMBER" />
                <asp:BoundField DataField="DonationDate" DataFormatString="{0:d}" 
                    HeaderText="DonationDate" />
                <asp:BoundField DataField="PURPOSE" HeaderText="PURPOSE" />
                <asp:BoundField DataField="EVENT" HeaderText="EVENT" />
                <asp:BoundField DataField="STATUS" HeaderText="STATUS" />
                <asp:BoundField DataField="CreateDate" DataFormatString="{0:d}" 
                    HeaderText="CreateDate" />
                <asp:BoundField DataField="TaxDeduction" HeaderText="TaxDeduction" />
                <asp:BoundField DataField="DonationID" HeaderText="DonationID" />
            </Columns>
        </asp:GridView>
    </asp:Panel>
<asp:GridView Visible="true" ID="GridDetails3"  runat="server" DataKeyNames="RegId" AutoGenerateColumns="False"  >
        <Columns>   
            
            <asp:BoundField DataField="ChapterCode"  HeaderText="Chapter" />
	        <asp:BoundField DataField="EventYear" HeaderText="Contest Year"/>
            <asp:BoundField DataField="Parent_Name" HeaderText="Parent Name"/>           
            <asp:BoundField DataField="Child_Name" HeaderText="Child Name" />
            <asp:BoundField DataField="Fee" HeaderText="Fee" />
            <asp:BoundField DataField="PaymentDate" HeaderText="PaymentDate" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference"/>
            <asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes"/>
            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="EventCode" HeaderText="EventCode"/>
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"  />             
            <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy"/>
	</Columns>    
</asp:GridView>	
</asp:Panel>
<asp:Panel ID="Details5" runat="server" Visible="false">
<br />
 <asp:Label ID="lblDetails5" runat="server" CssClass="keytext" Text="Coaching Registration"></asp:Label>
<br />
<asp:GridView Visible="true" ID="GridDetails5"  runat="server" DataKeyNames="CoachRegID" AutoGenerateColumns="False"  >
        <Columns>    
            <asp:BoundField DataField="childnumber" Visible="false"  HeaderText="childnumber" />
	        <asp:BoundField DataField="ProductCode" Visible="false" HeaderText="ProductCode"/>
            <asp:BoundField DataField="ChildName" HeaderText="Child Name"/>           
            <asp:BoundField DataField="ProductName" HeaderText="ProductName" />
            <asp:BoundField DataField="CoachName" HeaderText="CoachName" />
            <asp:BoundField DataField="Level" HeaderText="Level"/>
            <asp:BoundField DataField="Approved" HeaderText="Approved"/>
            <asp:BoundField DataField="PaymentDate" HeaderText="PaymentDate" DataFormatString="{0:d}"/>
            <asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference" />
            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate"  DataFormatString="{0:d}"  />             
            <asp:BoundField DataField="State" HeaderText="State"/>
	        <asp:BoundField DataField="city" HeaderText="city"/>
	          <asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes"/>
	</Columns>    
</asp:GridView>	
</asp:Panel>
</asp:Panel>
<asp:Panel ID="Panel3" runat="server" Visible="false">
   <br />
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <font class="style10"> Panel 3</font>
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="Close3" runat="server" Text="Close Panel 3" /><br />
   <br />
   <asp:Label ID="lblPendDetailsErr" CssClass="keytext" runat="server" Text="Sorry! No Details Found." Visible="false" ></asp:Label>
   <asp:Panel ID="PendDetails1" runat="server" Visible="false">
   <asp:Label ID="lblPendDetails1" CssClass="keytext" runat="server" Text="Contests"></asp:Label>   
    <br />
    <table id="tblContests" runat="server" visible="true">
<tr>
<td align="left">
<asp:Label ID="lblContestsTotalFeeMsg" runat="server" Text="Amount For Selected Contests :" CssClass="SmallFont" Visible="false"></asp:Label>
<asp:Label ID="lblContestsTotalFee" runat="server" Text="0" CssClass="SmallFont" Visible="false"></asp:Label>
 </td></tr><tr><td>      
<asp:DataGrid ID="DataGridContests" runat="server" DataKeyField="contestant_id" CellPadding="0"  AutoGenerateColumns="False" AllowSorting="True" Width="100%">
<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
<AlternatingItemStyle  Wrap="False"></AlternatingItemStyle>
<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
<Columns>
<asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false">
<ItemTemplate>
<asp:CheckBox ID="chkContests" runat="server" CssClass="SmallFont" AutoPostBack="false"></asp:CheckBox>
<asp:Label ID="lblContestsRegId" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.contestant_id") %>' CssClass="SmallFont" Visible="false"> </asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsChapter" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Chapter") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Contest Year" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsEventYear" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ContestYear") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Parent Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsParentName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Parent_Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Child Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsChildName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Child_Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="BadgeNumber" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsBadgeNumber" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.BadgeNumber") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Score1" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsScore1" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Score1") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Score2" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsScore2" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Score2") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Score3" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsScore3" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Score3") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Rank" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsRank" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Rank") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" SortExpression="Grade"
HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>'
CssClass="SmallFont"></asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblContestsPaymentDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Reference" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsPaymentReference" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentReference") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="PaymentNotes" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsPaymentNotes" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentNotes") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="CreateDate" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblContestsCreateDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CreateDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="EventCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsEventCode" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.EventCode") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ProductCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsProductCode" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ProductCode") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="CreatedBy" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsCreatedBy" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Created") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="TempLink" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblContestsTempLink" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TempLink","{0:d}") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

</Columns>
</asp:DataGrid>
</td></tr></table>
    </asp:Panel>
    <asp:Panel ID="PendDetails2" runat="server" Visible="false">
    <br />
     <asp:Label ID="lblPendDetails2" CssClass="keytext" runat="server" Text="Meal Charge"></asp:Label>
    <br />
    <table id="tblMealCharge" runat="server" visible="true">
<tr>
<td align="left">
<asp:Label ID="lblMealChargeTotalFeeMsg" runat="server" Text="Amount For Selected MealCharge : " CssClass="SmallFont" Visible="false"></asp:Label>
<asp:Label ID="lblMealChargeTotalFee" runat="server" Text="0" CssClass="SmallFont" Visible="false"></asp:Label>
 </td></tr><tr><td>      
<asp:DataGrid ID="DataGridMealCharge" runat="server" DataKeyField="MealChargeID" CellPadding="0"  AutoGenerateColumns="False" AllowSorting="True" Width="100%">
<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
<AlternatingItemStyle  Wrap="False"></AlternatingItemStyle>
<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>

<Columns>
<asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false">
<ItemTemplate>
<asp:CheckBox ID="chkMealCharge" runat="server" CssClass="SmallFont" AutoPostBack="false"></asp:CheckBox>
<asp:Label ID="lblMealChargeRegId" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.MealChargeID") %>' CssClass="SmallFont" Visible="false"> </asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeChapter" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Chapter") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Contest Year" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeEventYear" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ContestYear") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Parent Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeParentName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Parent_Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Person Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargePersonName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="RelationType" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeRelationType" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.RelationType") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ContestDate" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblMealChargeContestDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ChildFlag" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeChildFlag" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ChildFlag") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ChaperoneFlag" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeChaperoneFlag" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ChaperoneFlag") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="EventName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeEventName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.EventName") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>


<asp:TemplateColumn HeaderText="Amount" HeaderStyle-Font-Bold="true" SortExpression="Grade"
HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Amount","{0:N2}") %>'
CssClass="SmallFont"></asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblMealChargePaymentDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Reference" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargePaymentReference" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentReference") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="PaymentNotes" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargePaymentNotes" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentNotes") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ModifiedDate" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblMealChargeModifiedDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ModifiedDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ModifiedUserID" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeCreatedBy" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ModifiedUserID") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="TempLink" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblMealChargeTempLink" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TempLink","{0:d}") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

</Columns>
</asp:DataGrid>
</td></tr></table>
</asp:Panel>
<asp:Panel ID="PendDetails3" runat="server" Visible="false">
<br />
 <asp:Label ID="lblPendDetails3" runat="server" CssClass="keytext" Text="Workshop"></asp:Label>
<br />
<table id="tblWrkShp" runat="server" visible="true">
<tr>
<td align="left">
<asp:Label ID="lblWrkshpTotalFeeMsg" runat="server" Text="Amount For Selected WorkShops : " CssClass="SmallFont" Visible="false"></asp:Label>
<asp:Label ID="lblWrkshpTotalFee" runat="server" Text="0" CssClass="SmallFont" Visible="false"></asp:Label>
 </td></tr><tr><td>      
<asp:DataGrid ID="DataGridWrkshp" runat="server" DataKeyField="RegId" CellPadding="0"  AutoGenerateColumns="False" AllowSorting="True" Width="100%">
<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
<AlternatingItemStyle  Wrap="False"></AlternatingItemStyle>
<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
<Columns>
<asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false">
<ItemTemplate>
<asp:CheckBox ID="chkWrkshp" runat="server" CssClass="SmallFont" AutoPostBack="false"></asp:CheckBox>
<asp:Label ID="lblWrkshpRegId" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.RegId") %>' CssClass="SmallFont" Visible="false"> </asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpChapter" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Contest Year" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpEventYear" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.EventYear") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Parent Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpParentName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Parent_Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Child Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpChildName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Child_Name") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" SortExpression="Grade"
HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>'
CssClass="SmallFont"></asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblWrkshpPaymentDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Reference" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpPaymentReference" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentReference") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="PaymentNotes" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpPaymentNotes" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentNotes") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="EventCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpEventCode" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.EventCode") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="ProductCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpProductCode" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ProductCode") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="CreatedBy" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpCreatedBy" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CreatedBy") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="TempLink" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblWrkshpTempLink" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TempLink","{0:d}") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>



</Columns>
</asp:DataGrid>
</td></tr></table>
</asp:Panel>
<asp:Panel ID="PendDetails4" runat="server" Visible="false">
<br />
 <asp:Label ID="lblPendDetails4" runat="server" CssClass="keytext" Text="Coaching Registration"></asp:Label>
<br />
<table id="tblCoaching" runat="server" visible="true">
<tr>
<td align="left">
<asp:Label ID="lblCoachingTotalFeeMsg" runat="server" Text="Amount For Selected Coaching : " CssClass="SmallFont" Visible="false"></asp:Label>
<asp:Label ID="lblCoachingTotalFee" runat="server" Text="0" CssClass="SmallFont" Visible="false"></asp:Label>
 </td></tr><tr><td>      
<asp:DataGrid ID="DataGridCoaching" runat="server" DataKeyField="CoachRegId" CellPadding="0"  AutoGenerateColumns="False" AllowSorting="True" Width="100%">
<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
<AlternatingItemStyle  Wrap="False"></AlternatingItemStyle>
<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
<Columns>
<asp:TemplateColumn HeaderText="Check" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green"
HeaderStyle-Font-Underline="false">
<ItemTemplate>
<asp:CheckBox ID="chkCoaching" runat="server" CssClass="SmallFont" AutoPostBack="false"></asp:CheckBox>
<asp:Label ID="lblCoachingRegId" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.CoachRegId") %>' CssClass="SmallFont" Visible="false"> </asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Child Name" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingChildName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ChildName") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Event Year" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingEventYear" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.EventYear") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Product" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingProductName" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.ProductName") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Level" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingLevel" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Level") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Approved" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingApproved" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Approved") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Coach Name" HeaderStyle-Font-Bold="true" SortExpression="Grade"
HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CoachName") %>'
CssClass="SmallFont"></asp:Label></ItemTemplate></asp:TemplateColumn>
<asp:TemplateColumn HeaderText="City" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingCity" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.City") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>
<asp:TemplateColumn HeaderText="State" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingState" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.State") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false"
HeaderStyle-ForeColor="green"><ItemTemplate>
<asp:Label ID="lblCoachingPaymentDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.PaymentDate","{0:d}") %>'
CssClass="SmallFont"></asp:Label>&nbsp;</ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="Payment Reference" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingPaymentReference" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentReference") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>

<asp:TemplateColumn HeaderText="PaymentNotes" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingPaymentNotes" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.PaymentNotes") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>


<asp:TemplateColumn HeaderText="TempLink" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingTempLink" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.TempLink","{0:d}") %>' CssClass="SmallFont">
</asp:Label></ItemTemplate></asp:TemplateColumn>


<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true" SortExpression="Grade"
HeaderStyle-ForeColor="green" HeaderStyle-Font-Underline="false"><ItemTemplate>
<asp:Label ID="lblCoachingFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>'
CssClass="SmallFont"></asp:Label></ItemTemplate></asp:TemplateColumn>

</Columns>
</asp:DataGrid>
</td></tr></table>
</asp:Panel>
    <asp:Label ID="lblHidden" runat="server" Text="" Visible="false"></asp:Label>
    <asp:Label ID="lblHiddenMemberID" runat="server" Text="" Visible="False"></asp:Label>
<center>    
    <asp:Button ID="BtnSelect" runat="server" Text="Selection done" Visible="false"/>
    <asp:Button ID="BtnReSelect" runat="server" Text="Change Selection" Visible="false" />
    <asp:Button ID="BtnPnl3Cancel" runat="server" Text="Cancel" />
</center>
</asp:Panel>
</div>
</asp:Content>

