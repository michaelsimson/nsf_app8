﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="PersonalPref.aspx.vb" Inherits="PersonalPref" title="Personal Preferences" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left"><asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
</div>
<asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<div align = "center">  
<b> Search NSF member</b>       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="center" colspan="2">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;			
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" Visible="false" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
 <table border = "0" width="850px" id = "Table1" runat = "server" cellpadding = "3" cellspacing = "0">
<tr><td align="center">
<table border = "0" width="750px" id = "tblPersPref" runat = "server" cellpadding = "3" cellspacing = "0">
<tr><td align="center"><h4>Personal Preferences </h4> </td></tr>
<tr><td align="left">
    <asp:LinkButton ID="lknChangeChapter" OnClick="lknChangeChapter_Click" runat="server">1. Chapter</asp:LinkButton></td></tr>
<tr><td align="left">

<div id= "DivChChapter" align="center" width="250px" runat ="server" visible = "false" >
<table border = "0" cellpadding = "3" cellspacing = "0" >
<tr><td align="center" ><asp:Literal ID="LitInd1" runat="server"></asp:Literal></td><td align = "center" ></td><td align = "center" ><asp:Literal Visible="false"  ID="LitSp1" runat="server"></asp:Literal></td></tr>
<tr><td align = "left" ><asp:DropDownList ID="ddlChapterInd" DataTextField="ChapterCode" Width="160px" DataValueField = "ChapterID" runat="server">
    </asp:DropDownList></td><td align = "center" ></td><td align = "left" ><asp:DropDownList ID="ddlChapterSp" DataTextField="ChapterCode" Width="160px" DataValueField = "ChapterID" runat="server">
    </asp:DropDownList></td></tr>    
 <tr><td align = "center" >Change Chapter*<br />( for all purposes )</td><td align = "center" ></td><td align = "center" >
     <asp:Label ID="lblSp1" runat="server">Change Chapter<br />( only for volunteering )</asp:Label></td></tr>  
 <tr><td align = "center"  colspan="3">    <asp:Button ID="BtnChChapter" OnClick="BtnChChapter_Click" Width="140px"  runat="server" Text="Change Chapter" />
</td></tr>
     </table>
   <div align="Justify" style="width:90%">* - select a chapter close to  home. If changed to another chapter for child registration, you should change it back after the purpose is served.</div>

</div></td></tr>
<tr><td align="left">
    <asp:LinkButton ID="lbtnEComm" OnClick="lbtnEComm_Click" runat="server">2. Email Communication</asp:LinkButton></td> </tr> 
<tr><td align="center">
<div id= "divEComm" align="center" width="250px" runat ="server" visible = "false" >

   <table border = "0" cellpadding = "3" cellspacing = "0">
   <tr><td align="center" ><asp:Literal ID="LitInd2" runat="server"></asp:Literal></td><td   align = "center"></td><td align = "center" ><asp:Literal Visible="false"  ID="litSp2" runat="server"></asp:Literal></td></tr>
   <tr><td align = "left" ><asp:DropDownList ID="ddlECommInd" runat="server">
       <asp:ListItem Value="1">1.No email of monthly e-newsletter</asp:ListItem>
       <asp:ListItem Value="2">2.No email on contests, workshops, coaching, etc.</asp:ListItem>
       <asp:ListItem Value="3">3.No email of e-newsletter or on contests, wrokshops, etc.</asp:ListItem>
       <asp:ListItem Value="9">9. Emails are fine with me</asp:ListItem>       
       </asp:DropDownList>
      <%-- 5 : is used for technical break in the email list sent to the customers So please avoid 5 here as value --%>
       </td><td   align = "center"></td><td align = "left">
       <asp:DropDownList ID="ddlECommSp" Visible="false"  runat="server">
       <asp:ListItem Value="1">1.No email of monthly e-newsletter</asp:ListItem>
       <asp:ListItem Value="2">2.No email on contests, workshops, coaching, etc.</asp:ListItem>
       <asp:ListItem Value="3">3.No email of e-newsletter or on contests, wrokshops, etc.</asp:ListItem>
        <asp:ListItem Value="9">9. Emails are fine with me</asp:ListItem>
      </asp:DropDownList>
      <%-- 5 : is used for technical break in the email list sent to the customers So please avoid 5 here as value --%>
      </td></tr>
   <tr><td align = "center" colspan="3" > <asp:Button ID="btnECommInd" OnClick="btnECommInd_Click" Width="140px"  runat="server" Text="Save Selection" /></td>
  </tr>
   </table>
    </div> 
</td> </tr> 
<tr><td align="left">
    <asp:LinkButton ID="lbtnSponsor" OnClick="lbtnSponsor_Click" runat="server">3. Sponsorship*</asp:LinkButton></td> </tr> 
<tr><td align="center">
<div id= "divSponsor" align="center" width="250px" runat ="server" visible = "false" >
<table border = "0" cellpadding = "5" cellspacing = "0">
<tr><td></td><td  align = "center" >
    <asp:Literal ID="litInd" runat="server"></asp:Literal></td><td   align = "center"></td> <td   align = "center">
        <asp:Literal ID="litIndSp" runat="server" Visible="false" ></asp:Literal></td></tr>
<tr><td align="left">Sponsor</td><td  noWrap align="left"><asp:radiobuttonlist id="rbSponsorInd" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:radiobuttonlist></td><td   align = "center"></td><td  noWrap align="left"><asp:radiobuttonlist Visible="false"  id="rbSponsorIndSp" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:radiobuttonlist></td></tr>                                     
                                <tr><td align="center" colspan="4">
                                    <asp:Button ID="btnAddSponsor" OnClick="btnAddSponsor_Click" runat="server" Text="Add/Modify" /> 
                                    </td></tr>       
</table>
 <div align="Justify" style="width:80%">* - interested sponsoring events like contests and fundraising.</div>
</div>
</td></tr> 
<tr><td align="left">
    <asp:LinkButton ID="lbtnVolunteer" OnClick="lbtnVolunteer_Click" runat="server">4. Volunteering*</asp:LinkButton></td> </tr> 
<tr><td align="center">
<div id= "divvolunteer" align="center" width="250px" runat ="server" visible = "false" >
<table border = "0" cellpadding = "5" cellspacing = "0">
<tr><td></td><td  align = "center" >
    <asp:Literal ID="litInd3" runat="server"></asp:Literal></td><td   align = "center"></td><td   align = "center">
        <asp:Literal ID="litSp3" runat="server" Visible="false" ></asp:Literal></td></tr>
                                <tr><td align="left">Liaison</td><td  noWrap align="left"><asp:radiobuttonlist id="rbLiaisonInd" runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:radiobuttonlist></td><td   align = "center"></td><td  noWrap align="left"><asp:radiobuttonlist id="rbLiaisonIndSp" Visible="false"  runat="server" CssClass="SmallFont" RepeatDirection="Horizontal" RepeatLayout="Flow">
                                    <asp:ListItem Value="Yes">Yes</asp:ListItem>
                                    <asp:ListItem Value="No">No</asp:ListItem>
                                </asp:radiobuttonlist></td></tr>       
                                <tr><td align="center" colspan="4">
                                    <asp:Button ID="btnAddVolunteer" OnClick="btnAddVolunteer_Click" runat="server" Text="Add/Modify" /> 
                                    </td></tr>       
</table>
 <div align="Justify" style="width:80%">* - interested in being a liaison with India Chapters.</div> 
</div> 

</td></tr> 
<tr><td align="left"><asp:LinkButton ID="lbtnChangeEmail" runat="server">5. Change Email</asp:LinkButton></td> </tr> <%--PostBackUrl="~/ChangeEmail.aspx"--%>
<tr><td align="left"><asp:LinkButton ID="lbtnChangePassword" runat="server" PostBackUrl="~/ChangePassword.aspx">6. ChangePassword</asp:LinkButton></td></tr> 
<tr><td align="left"><asp:LinkButton ID="lbtnUniqueEmailID" runat="server" >7. Making your EmailIDs Unique</asp:LinkButton></td></tr> 
<tr><td align="left">
    <asp:LinkButton ID="lbtnPubName" OnClick="lbtnpubname_Click" runat="server">8. Published Name</asp:LinkButton></td> </tr> 
<tr><td align="center">
<div id= "divPubName" align="center" width="250px" runat ="server" visible = "false" >
<table border = "0" cellpadding = "5" cellspacing = "0">
<tr><td></td><td  align = "center" >Published Name </td>
<td   align = "center">
    <asp:TextBox ID="txtPubname" runat="server" MaxLength="25"></asp:TextBox>
    </td></tr>
<tr><td align="center" colspan="3"><asp:Button ID="btnAddPubName" OnClick="btnAddPubName_Click" runat="server" Text="Add/Modify" /> 
</td></tr>       
</table></div>
 

</td></tr>
<tr><td align="center"> <asp:Label ID="lblErr" runat="server" ForeColor = "Red"></asp:Label></td></tr>
</table> </td> </tr> </table> 
<asp:Label ID="HlblSpouse" ForeColor="white" runat="server"></asp:Label>
<asp:Label ID="lblCustIndID" ForeColor="white" runat="server"></asp:Label>
<asp:Label ID="lblCustIndChapter" ForeColor="white" runat="server"></asp:Label>
</asp:Content>

