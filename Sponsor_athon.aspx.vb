Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
'Page modified From Donor_donate.aspx
Namespace VRegistration
    Partial Class Sponsor_athon
        Inherits System.Web.UI.Page
        Protected lbBack As System.Web.UI.WebControls.LinkButton
        Protected Requiredfieldvalidator2 As System.Web.UI.WebControls.RequiredFieldValidator
        Protected WithEvents CustomValidator1 As System.Web.UI.WebControls.CustomValidator
        Dim strSqlQuery As String
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Session("LoggedIn") <> "True" Then
                Server.Transfer("login.aspx?entry=" + Session("entryToken").ToString().Substring(0, 1))
            End If
            If Session("entryToken").ToString() = "Parent" Then
                lnkback.Text = "Back to Parent Functions Page"
            ElseIf Session("entryToken").ToString() = "Donor" Then
                lnkback.Text = "Back to Donor Functions Page"
            ElseIf Session("entryToken").ToString() = "Volunteer" Then
                lnkback.Text = "Back to Volunteer Functions Page"
            End If

            If Not IsPostBack Then
                ' ddlChapterLiaison.SelectedIndex = 0
                LoadDonationPurpose()
                LoadDonationEvent()

                Dim ds As DataSet = Nothing
                If Session("entryToken") = "Parent" Or Session("entryToken") = "Donor" Then
                    ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByText("Donation"))
                    If (Not Session("CustIndID") Is Nothing) Then

                        Dim conn As New SqlConnection(Application("ConnectionString"))
                        Dim prmArray(6) As SqlParameter

                        prmArray(0) = New SqlParameter
                        prmArray(0).ParameterName = "@ParentID"
                        'old 'prmArray(0).Value = Convert.ToInt32(Session("ParentID").ToString)
                        prmArray(0).Value = Convert.ToInt32(Session("CustIndID").ToString)
                        prmArray(0).Direction = ParameterDirection.Input

                        prmArray(1) = New SqlParameter
                        prmArray(1).ParameterName = "@ContestYear"
                        prmArray(1).Value = Application("ContestYear")
                        prmArray(1).Direction = ParameterDirection.Input

                        Try
                            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "usp_GetContestCharity", prmArray)
                        Catch se As SqlException
                            lblMessage.Text = se.Message
                            Return
                        End Try
                        If ds Is Nothing Then
                            Return
                        End If
                        If (ds.Tables.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows.Count <= 0) Then
                            Return
                        End If
                        If (ds.Tables(0).Rows(0)("PaymentDate").ToString <> "") Then
                            Session("Donation") = CType(0, Decimal)
                            Session("DONATIONFOR") = ""
                            'Response.Redirect("reg_Pay.aspx")
                        Else
                            Dim nDonation As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("Donationamount").ToString)
                            txtDonation.Text = nDonation.ToString("0.00")
                            If (ds.Tables(0).Rows(0)("SupportFor1").ToString <> "1") Then
                                'cbSupport1.Checked = True
                            End If
                        End If
                    Else
                        lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session vars CustIndID  not available")
                        Return
                    End If
                Else
                    txtDonation.Text = 0
                    HlnkTellme.Visible = False
                    lblInv.Visible = False
                    Dim conn As New SqlConnection(Application("ConnectionString"))
                    Dim strquery As String = "Select * from Contest_Charity where ContestYear = " & Application("ContestYear")
                    Try
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strquery)
                    Catch se As SqlException
                        lblMessage.Text = se.Message
                        Return
                    End Try
                    If ds Is Nothing Then
                        Return
                    End If
                    If (ds.Tables.Count <= 0) Then
                        Return
                    End If
                    If (ds.Tables(0).Rows.Count <= 0) Then
                        Return
                    End If
                    If (ds.Tables(0).Rows(0)("PaymentDate").ToString <> "") Then
                        Session("Donation") = CType(0, Decimal)
                        Session("DONATIONFOR") = ""
                    Else
                        Dim nDonation As Decimal = Convert.ToDecimal(ds.Tables(0).Rows(0)("Donationamount").ToString)
                        txtDonation.Text = nDonation.ToString("0.00")
                    End If
                End If
            End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>

        Private Sub InitializeComponent()

        End Sub

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
             If Requiredfieldvalidator4.IsValid = False And Session("entryToken") = "Volunteer" Then
                Exit Sub
            End If
            If ((CInt(Me.txtDonation.Text) > 0) AndAlso (ddlDonationFor.SelectedIndex = 0)) Then
                DonationForLabel.Visible = True
                DonationForLabel.ForeColor = Color.Red
                DonationForLabel.Text = "Please select an option."
            Else
                'get member's chapterid
                Dim chapterId As Integer = 0
                Dim currentDate As Date = Now

                Dim EID As Integer = ddlEvent.SelectedItem.Value
                Dim Eve As String = ddlEvent.SelectedItem.ToString
                Dim anonymous As String = dllAnonymous.SelectedItem.Value.ToString
                Dim Matching As String = ddlmatching.SelectedItem.Value.ToString

                Session("@EID") = EID
                Session("@EVE") = Eve
                Session("@Anonymous1") = anonymous
                Session("@Matching") = Matching

                If Session("entryToken") = "Parent" Or Session("entryToken") = "Donor" Then
                    If ((Not Session("CustIndID") Is Nothing)) Then
                        Dim memberId As Integer = CInt(Session("CustIndID"))
                        ViewState("MemberID") = memberId
                        Dim eventId As Integer = 0
                        ViewState("EventID") = eventId

                        If (Not Session("EventID") Is Nothing) Then
                            eventId = CInt(Session("EventID").ToString())
                        End If
                        SP()
                    Else
                        lblMessage.Text = (lblMessage.Text + "<BR>Error: The required session var CustIndID is not available")
                        Return
                    End If
                ElseIf ((Not Session("CustIndID") Is Nothing)) Then
                    Dim memberId As Integer = CInt(Session("CustIndID"))
                    ViewState("MemberID") = memberId
                    Dim eventId As Integer = 0
                    ViewState("EventID") = eventId
                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString())
                    End If
                    SP()
                Else
                    Dim memberId As Integer = CInt(Session("LoginID"))
                    ViewState("MemberID") = memberId
                    Dim eventId As Integer = 0

                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString())
                    End If
                    SP()
                End If
            End If
        End Sub

        Private Sub txtDonation_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtDonation.TextChanged
            lblMessage.ForeColor = Color.Blue
            lblMessage.Text = ""
            txtDonation.Text = txtDonation.Text.Trim
            Dim fDonation As Double = 0
            Try
                fDonation = Convert.ToDouble(txtDonation.Text)
                rfvDonationFor.Enabled = (fDonation > 0)
                TrPurpose.Visible = (fDonation > 0)
            Catch exception As System.Exception
                lblMessage.ForeColor = Color.Red
                lblMessage.Text = "Invalid number, please enter 0 or an amount of your choice"
            End Try
        End Sub

        Private Sub LoadDonationPurpose()
            strSqlQuery = "SELECT PurposeCode ,PurposeDesc FROM DonationPurpose   "
            Dim drPurpose As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drPurpose = SqlHelper.ExecuteReader(conn, CommandType.Text, strSqlQuery)
            While drPurpose.Read()
                ddlDonationFor.Items.Add(New ListItem(drPurpose(1).ToString(), drPurpose(0).ToString()))
            End While
            ddlDonationFor.Items.Insert(0, New ListItem("Select Purpose", ""))
            ddlDonationFor.SelectedIndex = 4
        End Sub

        Private Sub LoadDonationEvent()
            Dim drEvents1 As DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))

            If (Session("EventID") > 0 And Session("EventID") <> 11) Then
                strSqlQuery = "Select EventID,EventCode, Name from event where eventid = " & Session("EventID")
                drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
                Dim Count1 As Integer = 0
                If (drEvents1.Tables(0).Rows.Count > 0) Then
                    Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

                    Dim i As Integer
                    For i = 0 To Count1 - 1
                        Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                        Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                        ddlEvent.DataTextField = "Name"
                        ddlEvent.DataValueField = "EventID"
                        Dim li As ListItem = New ListItem(Name)
                        li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                        li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                        ddlEvent.Items.Add(li)
                    Next i

                    ddlEvent.SelectedIndex = Session("EventID")
                    ddlEvent.Enabled = False
                End If

            Else
                strSqlQuery = "SELECT EventID,EventCode,Name FROM Event where EventID in ( 5 ,6, 9, 11, 12,18) "
                drEvents1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSqlQuery)
                Dim Count1 As Integer = 0
                If (drEvents1.Tables(0).Rows.Count > 0) Then
                    Count1 = Convert.ToInt32(drEvents1.Tables(0).Rows.Count.ToString())

                    Dim i As Integer
                    For i = 0 To Count1 - 1
                        Dim EventID As String = drEvents1.Tables(0).Rows(i)(0).ToString()
                        Dim Name As String = drEvents1.Tables(0).Rows(i)(2).ToString()
                        ddlEvent.DataTextField = "Name"
                        ddlEvent.DataValueField = "EventID"
                        Dim li As ListItem = New ListItem(Name)
                        li.Text = drEvents1.Tables(0).Rows(i)(2).ToString()
                        li.Value = drEvents1.Tables(0).Rows(i)(0).ToString()
                        ddlEvent.Items.Add(li)
                    Next i
                    ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
                    ddlEvent.SelectedIndex = 0

                End If
            End If

        End Sub


        Protected Sub lnkback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkback.Click
            If Session("entryToken") = "Parent" Then
                Response.Redirect("UserFunctions.aspx")
            ElseIf Session("entryToken") = "Donor" Then
                Response.Redirect("DonorFunctions.aspx")
            ElseIf Session("entryToken") = "Volunteer" Then
                Response.Redirect("VolunteerFunctions.aspx")
            End If
        End Sub

        Protected Sub SP()
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim chapterId As Integer = 0
            Try
                Dim ret As Object = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_getIndspouseChapterId", New SqlParameter("@autoMemberId", ViewState("MemberID")))
                If (Not ret Is Nothing) Then
                    chapterId = CType(ret, Integer)
                End If
                Session("CustIndChapterID") = chapterId
            Catch se As SqlException
                lblMessage.Text = se.Message
                lblMessage.Text = (lblMessage.Text + "<BR>Error:There is no Chapter associated with this loginid")
                Return
            End Try
            Dim strquery As String = "DELETE FROM Contest_Charity WHERE MEMBERID=<MEMBERID> and ContestYear=<CONTESTYEAR>;"
            strquery = (strquery + " INSERT INTO Contest_Charity(MEMBERID, ContestYear, DonationAmount, DonationFor, SupportFor1, Support" & _
            "For2, NSFIndiaChapterInfo, VolunteerInterestArea,EventId,ChapterId,createdDate)")
            strquery = (strquery + " VALUES(<MEMBERID>, <CONTESTYEAR>, <DONATIONAMOUNT>, '<DONATIONFOR>', <SUPPORTFOR1>, <SUPPORTFOR2>, '" & _
            "<NSFINDIACHAPTERINFO>', '<VOLUNTEERINTERESTAREA>','<EVENTID>',<CHAPTERID>,<CREATEDDATE>)")

            strquery = strquery.Replace("<MEMBERID>", ViewState("MemberID"))
            strquery = strquery.Replace("<CONTESTYEAR>", Application("ContestYear").ToString)
            strquery = strquery.Replace("<DONATIONAMOUNT>", txtDonation.Text.ToString)
            Session("DONATIONFOR") = ""
            If (ddlDonationFor.SelectedIndex >= 0) Then
                strquery = strquery.Replace("<DONATIONFOR>", ddlDonationFor.SelectedItem.Value.ToString)
                Session("DONATIONFOR") = ddlDonationFor.SelectedItem.Value.ToString
            Else
                strquery = strquery.Replace("<DONATIONFOR>", "")
            End If
            strquery = strquery.Replace("<SUPPORTFOR1>", "0")
            strquery = strquery.Replace("<SUPPORTFOR2>", "0")
            strquery = strquery.Replace("<NSFINDIACHAPTERINFO>", "''")
            strquery = strquery.Replace("<VOLUNTEERINTERESTAREA>", "")
            strquery = strquery.Replace("<EVENTID>", Session("@EID"))
            strquery = strquery.Replace("<CHAPTERID>", chapterId.ToString())
            strquery = strquery.Replace("<CREATEDDATE>", "'" + System.DateTime.Today.ToShortDateString + "'")
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strquery)
            Catch se As SqlException
                lblMessage.Text = se.Message
                lblMessage.Text = (lblMessage.Text + "<BR>Update failed. Please correct your data and try again ")
                Return
            End Try

            Session("Donation") = Convert.ToDecimal(txtDonation.Text.ToString)
            Response.Redirect("Donor_Pay.aspx")
        End Sub
    End Class
End Namespace