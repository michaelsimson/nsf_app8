Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Imports NativeExcel

Partial Class AddUpdChapter
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            EnableFlag(False)
            GetRecords()
            GetCluster()
            GetState()
            GetZone()
        End If
    End Sub
    Private Sub GetRecords()
        Pnl3Msg.ForeColor = Color.Red
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Chapter order by state,name")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub
    Protected Sub EnableFlag(bFlag As Boolean)
        spChapterFlag.Visible = bFlag
        ddlNewflag.Visible = bFlag
    End Sub
    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        BtnAdd.Text = "Update"
        Pnl3Msg.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("ChapterID") = TransID
            EnableFlag(True)
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select ChapterID, ChapterCode, Name, State, City, Status, ZoneId, ZoneCode, ClusterId, ClusterCode, NewChapterFlag, WebFolderName, isnull(NIO, 'N') 'NIO', ISNULL(NIOP,0) NIOP from Chapter where ChapterId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtChapName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ZoneID").ToString()))
        ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ClusterID").ToString()))
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(dsRecords.Tables(0).Rows(0)("Status").ToString()))
        ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue(dsRecords.Tables(0).Rows(0)("State").ToString()))
        txtCity.Text = dsRecords.Tables(0).Rows(0)("City").ToString()
        ddlNewflag.SelectedIndex = ddlNewflag.Items.IndexOf(ddlNewflag.Items.FindByValue(dsRecords.Tables(0).Rows(0)("NewChapterFlag").ToString().Trim))
        ddlNIO.SelectedIndex = ddlNIO.Items.IndexOf(ddlNIO.Items.FindByValue(dsRecords.Tables(0).Rows(0)("NIO").ToString().Trim))
        ddlNIOP.SelectedIndex = ddlNIOP.Items.IndexOf(ddlNIOP.Items.FindByValue(dsRecords.Tables(0).Rows(0)("NIOP").ToString().Trim))
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        Pnl3Msg.ForeColor = Color.Red
        If ddlZone.SelectedValue = "0" Then
            Pnl3Msg.Text = "Please select Zone"
            Exit Sub
        ElseIf ddlCluster.SelectedValue = "0" Then
            Pnl3Msg.Text = "Please select Cluster"
            Exit Sub
        ElseIf txtChapName.Text.Length < 1 Then
            Pnl3Msg.Text = "Please Enter Chapter Name"
            Exit Sub
        ElseIf ddlState.SelectedValue = "0" Then
            Pnl3Msg.Text = "Please select State"
            Exit Sub
        ElseIf txtCity.Text.Length < 1 Then
            Pnl3Msg.Text = "Please Enter City"
            Exit Sub
        ElseIf ddlStatus.SelectedValue = "0" Then
            Pnl3Msg.Text = "Please select Status"
            Exit Sub
        ElseIf ddlNIO.SelectedValue = "Y" Then
            If ddlNIOP.SelectedValue = "0" Then
                Pnl3Msg.Text = "Please select NIOP"
                Exit Sub
            End If
        End If
        Dim cnt As Integer = 0
        If BtnAdd.Text = "Add" Then
            Strsql = "Select count(ChapterCode) from Chapter where ChapterCode='" & txtChapName.Text & ", " & ddlState.SelectedValue & "'"
            cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
            If cnt = 0 Then
                Strsql = "Insert Into Chapter(ChapterCode, Name, State, City, Status, ZoneId, ZoneCode, ClusterId, ClusterCode, NewChapterFlag, WebFolderName, CreateDate, CreatedBy, NIO, NIOP) Values ('"
                Strsql = Strsql & txtChapName.Text & ", " & ddlState.SelectedValue & "','" & txtChapName.Text & "','" & ddlState.SelectedValue & "','" & txtCity.Text & "','" & ddlStatus.SelectedValue & "'," & ddlZone.SelectedItem.Value & ",'" & ddlZone.SelectedItem.Text & "'," & ddlCluster.SelectedItem.Value & ",'"
                Strsql = Strsql & ddlCluster.SelectedItem.Text & "','Y','" & ddlState.SelectedValue & "_" & txtChapName.Text.Replace(" ", "") & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "'," & Session("LoginID")
                If ddlNIO.SelectedValue = "Y" Then
                    Strsql = Strsql & ", 'Y'"
                Else
                    Strsql = Strsql & ", NULL"
                End If
                If ddlNIOP.SelectedValue = "0" Then
                    Strsql = Strsql & ", NULL"
                Else
                    Strsql = Strsql & ", " + ddlNIOP.SelectedValue
                End If
                Strsql = Strsql & ")"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                GetRecords()
                clear()
                Pnl3Msg.ForeColor = Color.Blue
                Pnl3Msg.Text = "Record Added Successfully"
            Else
                Pnl3Msg.Text = "Chapter Already found"
            End If
        ElseIf BtnAdd.Text = "Update" Then
            Strsql = "Select count(ChapterCode) from Chapter where ChapterCode='" & txtChapName.Text & ", " & ddlState.SelectedValue & "' and ChapterID<>" & Session("ChapterID")
            cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
            If cnt = 0 Then
                Strsql = "Update Chapter Set ChapterCode='" & txtChapName.Text & ", " & ddlState.SelectedValue & "', Name='" & txtChapName.Text & "', State='" & ddlState.SelectedValue & "', City='" & txtCity.Text & "', Status='" & ddlStatus.SelectedValue & "',NewChapterFlag='" & ddlNewflag.SelectedValue & "', ZoneId=" & ddlZone.SelectedItem.Value & ", ZoneCode='" & ddlZone.SelectedItem.Text & "', ClusterId=" & ddlCluster.SelectedItem.Value & ", ClusterCode='" & ddlCluster.SelectedItem.Text & "',WebFolderName='" & ddlState.SelectedValue & "_" & txtChapName.Text.Replace(" ", "") & "',ModifyDate=Getdate(), ModifiedBy=" & Session("LoginID")

                If ddlNIO.SelectedValue = "Y" Then
                    Strsql = Strsql & " , NIO ='Y'"
                Else
                    Strsql = Strsql & " , NIO = NULL"
                End If
                If ddlNIOP.SelectedValue = "0" Then
                    Strsql = Strsql & " , NIOP = NULL"
                Else
                    Strsql = Strsql & " , NIOP = " + ddlNIOP.SelectedValue
                End If

                Strsql = Strsql & " Where ChapterID=" & Session("ChapterID")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                GetRecords()
                clear()
                Pnl3Msg.ForeColor = Color.Blue
                Pnl3Msg.Text = "Record Updated Successfully"
                BtnAdd.Text = "Add"
            Else
                Pnl3Msg.Text = "Chapter Already found"
            End If
        End If
    End Sub
    Private Sub clear()
        EnableFlag(False)
        txtChapName.Text = ""
        BtnAdd.Text = "Add"
        txtCity.Text = String.Empty
        txtChapName.Text = String.Empty
        Pnl3Msg.Text = ""
        ddlZone.SelectedIndex = ddlZone.Items.IndexOf(ddlZone.Items.FindByValue("0"))
        ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue("0"))
        ddlCluster.SelectedIndex = ddlCluster.Items.IndexOf(ddlCluster.Items.FindByValue("0"))
        ddlState.SelectedIndex = ddlState.Items.IndexOf(ddlState.Items.FindByValue("0"))
        ddlNIO.SelectedIndex = ddlNIO.Items.IndexOf(ddlNIO.Items.FindByValue("0"))
        ddlNIOP.SelectedIndex = ddlNIOP.Items.IndexOf(ddlNIOP.Items.FindByValue("0"))
    End Sub
    Private Sub GetZone()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Zone order by Zonecode Desc")
        While dsRecords.Read
            ddlZone.Items.Insert(++i, New ListItem(dsRecords("ZoneCode"), dsRecords("ZoneId")))
        End While
    End Sub
    Private Sub GetState()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM StateMaster order by Name Desc")
        While dsRecords.Read
            ddlState.Items.Insert(++i, New ListItem(dsRecords("Name"), dsRecords("StateCode")))
        End While
    End Sub
    Private Sub GetCluster()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        Dim strsql As String = "SELECT * FROM Cluster order by ClusterCode Desc"
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        While dsRecords.Read
            ddlCluster.Items.Insert(++i, New ListItem(dsRecords("ClusterCode"), dsRecords("ClusterId")))
        End While
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
    End Sub

    Protected Sub BtnExportToExcel_Click(sender As Object, e As EventArgs)
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT ChapterID,	ChapterCode,	Name,	State,	City,	ClusterCode,	ZoneCode,	Status,	NewChapterFlag,	WebFolderName,	NIO	,NIOP FROM Chapter order by state,name")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dtDate As DateTime = DateTime.Now
        Dim month As String = dtDate.ToString("MMM")
        Dim day As String = dtDate.ToString("dd")
        Dim year As String = dtDate.ToString("yyyy")

        Dim monthDay As String = month & "" & day

        Dim Excelname = "ChapterList_" & monthDay & "_" & year & ".xls"
        Dim xlWorkBook As IWorkbook = NativeExcel.Factory.CreateWorkbook
        If dt.Rows.Count > 0 Then
            Dim Sheet1 As IWorksheet = xlWorkBook.Worksheets.Add()

            Sheet1.Name = dt.TableName
            Dim colCount As Integer = dt.Columns.Count
            Dim i As Integer = 1
            For Each dc As DataColumn In dt.Columns
                Sheet1.Range(1, i).Value = dc.ColumnName
                Sheet1.Range(1, i).Font.Bold = True
                i = i + 1
            Next

            Dim j As Integer
            i = 2
            For Each dr As DataRow In dt.Rows
                For j = 0 To dt.Columns.Count - 1
                    Sheet1.Range(i, j + 1).Value = dr(j).ToString()
                Next
                i = i + 1
            Next

            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            'Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Excelname)
            xlWorkBook.SaveAs(HttpContext.Current.Response.OutputStream)
            Response.End()

            'Response.Clear()
            '' Response.ContentType = "application/vnd.ms-excel"
            '' Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            'Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            'Response.AddHeader("Content-Disposition", "attachment;filename=" & Excelname)
            'xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
            'Response.End()
        End If

    End Sub
End Class
