Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Imports System.Net.Mail

Partial Class ContestSelectionSum
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            trMeal1.Visible = False
            trmeal2.Visible = False
            '***************************************************
            '***Get IndID and SpouseID for the givn Logon Person
            '***************************************************
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim StrIndSpouse As String = ""
            Dim intIndID As Integer = 0
            Dim dsIndSpouse As New DataSet

            StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

            Dim objIndSpouse As New IndSpouse10
            objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

            If dsIndSpouse.Tables.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                    If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                        lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") & _
                        " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                    Else
                        If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        Else
                            intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                        End If
                        lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                    End If
                    Session("IndID") = intIndID
                    Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                    Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                    '**************************
                    '*** Spouse Info Capturing
                    '**************************
                    Dim StrSpouse As String = ""
                    Dim intSpouseID As Integer = 0
                    Dim dsSpouse As New DataSet
                    StrSpouse = "Relationship='" & Session("IndID") & "'"


                    objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                    If dsSpouse.Tables.Count > 0 Then
                        If dsSpouse.Tables(0).Rows.Count > 0 Then
                            intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        End If
                    End If

                    Session("SpouseID") = intSpouseID

                    '********************************************************
                    '*** Populate Parent Info on the Page
                    '********************************************************
                    Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                    lblAddress1.Text = drIndSpouse.Item("Address1")
                    lblAddress2.Text = drIndSpouse.Item("Address2")
                    lblCity.Text = drIndSpouse.Item("City") & " , " & drIndSpouse.Item("State") & " " & drIndSpouse.Item("Zip")
                    lblChapter.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ChapterCode From Chapter Where ChapterID=" & drIndSpouse.Item("ChapterID"))
                    If drIndSpouse.Item("HPhone").ToString <> "" Then
                        lblHomePhone.Text = "Tel : " & drIndSpouse.Item("HPhone") & "(Home)"
                    Else
                        lblHomePhone.Text = "Home Phone Not Provided"
                    End If
                    lblEMail.Text = "E-Mail : " & drIndSpouse.Item("EMail")

                End If
            End If
            '********************************************************
            '*** Show Selected Contests
            '********************************************************
            Dim cmd As New SqlCommand
            Dim dsInvitees As New DataSet
            Dim da As New SqlDataAdapter
            Dim i As Integer
            Dim Sum As Decimal = 0
            Try
                cmd.Connection = conn
                conn.Open()
                cmd.CommandType = CommandType.Text
                cmd.CommandText = " Select CHLD.ChildNumber, CTST.ParentID, contestant_id,"
                cmd.CommandText = cmd.CommandText & " REPLACE(CHLD.First_Name + ' ' + COALESCE (CHLD.Middle_Initial, '') + ' ' + CHLD.Last_Name, ' ', ' ') as 'CName',"
                cmd.CommandText = cmd.CommandText & " PR.Name,PR.ProductCode,CTST.ChapterID, CTST.Fee, REPLACE(CONVERT(VARCHAR(11), CT.ContestDate, 106), ' ', '-') as ContestDate,CT. CheckinTime,CHPT.ChapterCode as 'Chapter',"
                cmd.CommandText = cmd.CommandText & " Case When Org.Organization_name<>'' then Org.Organization_name else 'To be determined' End 'Venue',CT.contestID From Contestant CTST Inner Join Child CHLD"
                cmd.CommandText = cmd.CommandText & " ON CTST.ChildNumber = CHLD.ChildNumber INNER JOIN IndSpouse IND ON IND.AutoMemberID = CTST.ParentID INNER JOIN Chapter  CHPT ON CHPT.ChapterID = CTST.ChapterID"
                cmd.CommandText = cmd.CommandText & " Inner Join Contest CT on CT.NSFChapterID = CTST.ChapterID and CT.ProductID = CTST.ProductID and CTST.ContestYear = CT.Contest_Year and CTST.EventID = CT.EventID and ct.ContestID = ctst.ContestCode"
                cmd.CommandText = cmd.CommandText & " Left Join ExContestant Ex On Ex.ChapterID=CT.NSFChapterID and Ex.ProductID=CT.ProductId and Ex.ProductGroupID=CT.ProductGroupId and Ex.ContestYear=CT.Contest_Year and Ex.EventID=CT.EventId and Ex.ChildNumber= CTST.ChildNumber"
                cmd.CommandText = cmd.CommandText & " Inner Join Product PR on PR.ProductID = CTST.ProductID Left Join OrganizationInfo org On org.automemberid = CT.VenueID where CTST.ContestYear >= " & Session("EventYear") & " and"
                cmd.CommandText = cmd.CommandText & " CTST.paymentreference is null and CTST.ParentID = " & Session("CustIndID") & " and  Case when Ex.NewDeadline is Null then Datediff(d,CT.RegistrationDeadline,Getdate()) Else Datediff(d,Ex.NewDeadline,Getdate()) END <=0  and CTST.EventID = " & Session("EventID") & " order by  CTST.Childnumber, CTST.ProductID"
                'Response.Write(cmd.CommandText)
                da.SelectCommand = cmd
                da.Fill(dsInvitees)
                conn.Close()
            Catch ex As Exception
                'Response.Write(ex.ToString())
                ' MsgBox(ex.ToString)
            End Try
            ' To Avoid Errors during Payment due to Transportaion errors
            If dsInvitees.Tables.Count > 0 Then
                If dsInvitees.Tables(0).Rows.Count <= 0 Then
                    lblContestDateInfo.Text = "No Contest Selected"
                    lblContestDateInfo.Visible = True
                    btnRegister.Enabled = False
                Else
                    'MsgBox(dsInvitees.Tables(0).Rows.Count)
                    lblContestInfo.Visible = False
                    dgChildList.DataSource = dsInvitees.Tables(0)
                    dgChildList.DataBind()
                    For i = 0 To dsInvitees.Tables(0).Rows.Count - 1
                        Sum = Sum + dsInvitees.Tables(0).Rows(i).Item("Fee")
                        'MsgBox(dsInvitees.Tables(0).Rows(0).Item("Fee"))
                    Next
                    CalculateTotal()
                    Lblsum.Text = "Total Fee to be Paid : " & Format$(Sum, "Currency")
                    lblContestInfo.Text = "Note: Please verify your selections for each child along with event date, chapter and location."
                    lblContestInfo1.Text = " The date and location are subject to change.  Visit www.northsouth.org for the latest information."

                    lblContestDateInfo.Text = "Once paid, your payment is non-refundable."
                    lblContestInfo.Visible = True
                    lblContestInfo1.Visible = True
                    lblContestDateInfo.Visible = True
                    btnRegister.Enabled = True
                    '********************************************************
                    '*** Show Meals Details By Ferdine Silva 18/05/2010
                    '********************************************************
                End If
                If Session("EventID").ToString = "1" Then

                    Dim dsMeals As New DataSet
                    Dim dmeal As New SqlDataAdapter
                    Dim j As Integer
                    Dim Sum1 As Decimal = 0
                    Dim cmd1 As New SqlCommand
                    Try
                        cmd1.Connection = conn
                        conn.Open()
                        cmd1.CommandType = CommandType.StoredProcedure
                        cmd1.CommandText = "usp_GetPendingTran_Meals"
                        'Testing Done by silva
                        cmd1.Parameters.Add(New SqlParameter("@ContestYear", Session("EventYear")))
                        cmd1.Parameters.Add(New SqlParameter("@MemberID", Session("CustIndID")))
                        dmeal.SelectCommand = cmd1
                        dmeal.Fill(dsMeals)
                    Catch ex As Exception
                        'MsgBox(ex.ToString)
                    End Try

                    If dsMeals.Tables(0).Rows.Count <= 0 Then

                    Else
                        dgMealList.DataSource = dsMeals.Tables(0)
                        dgMealList.DataBind()
                        For j = 0 To dsMeals.Tables(0).Rows.Count - 1
                            Sum1 = Sum1 + dsMeals.Tables(0).Rows(j).Item("Amount")
                            'MsgBox(dsInvitees.Tables(0).Rows(0).Item("Fee"))
                        Next
                        CalculateTotal()
                        lblmealsum.Text = "Total Meals to be Paid : " & Format$(Sum1, "Currency")
                        Session("RegFee") = Double.Parse(Session("RegFee"))
                        lblgrandsum.Text = "Grand Total to be Paid : " & Format$(Sum + Sum1, "Currency")
                        trMeal1.Visible = True
                        trmeal2.Visible = True
                        dgMealList.Visible = True
                        btnRegister.Enabled = True
                    End If
                End If
            Else
                '  Errors handled here During the Payment Errors due to Transportaion errors
                If Session("contestSelSumNo") = 0 Then
                    Session("contestSelSumNo") = 1
                    Response.Redirect("contestSelectionSum.aspx")
                Else
                    SendDasMessage("Error in Contest Registration(Selection Summary)", "maheswarimadhavan@gmail.com")
                    lblContestDateInfo.Text = "Sorry for the Inconvenience. Request cannot be processed now due to server overload. Please Try after some time."
                    lblContestDateInfo.Visible = True
                    btnRegister.Enabled = False
                End If
            End If

            CheckChapterConf()
        End If
    End Sub
    Private Sub CalculateTotal()
        Dim dgItem As DataGridItem
        Dim strPaymentNotes As String
        Dim Lbl1 As Label
        Dim Lbl2 As Label
        Dim Lbl3 As Label
        Dim Lbl4 As Label
        strPaymentNotes = ""
        Dim totalFee As Double
        totalFee = 0.0
        For Each dgItem In dgChildList.Items
            Lbl1 = dgItem.FindControl("Label2")
            totalFee = totalFee + Double.Parse(Lbl1.Text.Replace("$", ""))
            Lbl2 = dgItem.FindControl("lblProductCode")
            strPaymentNotes = strPaymentNotes + Lbl2.Text
            Lbl3 = dgItem.FindControl("lblChildNumber")
            strPaymentNotes = strPaymentNotes + "(" + Lbl3.Text + ")"
            Lbl4 = dgItem.FindControl("lblChapterID")
            strPaymentNotes = strPaymentNotes + "(" + Lbl4.Text + ")"
            strPaymentNotes = strPaymentNotes + "(" + Lbl1.Text.Replace("$", "") + ")"
        Next
        Session("RegFee") = totalFee
        Session("ContestsSelected") = strPaymentNotes
    End Sub
    Private Sub CheckChapterConf()
        Dim dgItem As DataRow
        Dim LbChildNumber As Label = New Label()
        Dim LbProductCode As Label = New Label()
        Dim LblChapterID As Label = New Label()
        Dim LbContestDate As Label = New Label()
        Dim DupliFlag As Boolean = False
        Dim ChapFlag As Boolean = False
        Dim ParentChapterID As Integer = Session("ChapterID")

        LblChapterID.Text = "0"
        LbContestDate.Text = ""
        Session("EventYear") = "2016"
        Dim StrSQL As String = "SELECT CTST.ChapterID,REPLACE(CONVERT(VARCHAR(11), CT.ContestDate, 106), ' ', '-') as ContestDate From Contestant CTST Inner Join Contest CT on CT.NSFChapterID = CTST.ChapterID and CT.ProductID = CTST.ProductID and CTST.ContestYear = CT.Contest_Year and CTST.EventID = CT.EventID and ct.ContestID = ctst.ContestCode Where CTST.ContestYear>=" & Session("EventYear") & " and CTST.EventId =" & Session("EventID") & " and  CTST.ParentId=" & Session("CustIndID") & " Group By CTST.ChapterID,CT.ContestDate ORDER BY ContestDate "
        'Response.Write(StrSQL)
        Dim dsContest As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, StrSQL)
        lblEmailInfo.Text = ""
        Try
            If dsContest.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsContest.Tables(0).Rows.Count - 1
                    For Each dgItem In dsContest.Tables(0).Rows
                        'LbChildNumber.Text = dgItem("ChildNumber").ToString()
                        LblChapterID.Text = dgItem("ChapterID").ToString()
                        LbContestDate.Text = dgItem("ContestDate").ToString()

                        If LbContestDate.Text.Equals(dsContest.Tables(0).Rows(i)("ContestDate")) And Not LblChapterID.Text.Equals(dsContest.Tables(0).Rows(i)("ChapterID").ToString()) Then
                            DupliFlag = True
                            'Response.Write(LbContestDate.Text & " " & LblChapterID.Text & "<BR>")
                        End If
                        If ParentChapterID <> Convert.ToInt32(LblChapterID.Text) Then
                            ChapFlag = True
                        End If
                        'LblChapterID.Text = dgItem("ChapterID").ToString()
                        'LbContestDate.Text = dgItem("ContestDate").ToString()
                    Next
                    If DupliFlag = True Then
                        TrChkConf.Visible = True
                    Else
                        TrChkConf.Visible = False
                    End If
                    If ChapFlag = True And (Session("EventID") <> 1 Or LblChapterID.Text <> "1") Then
                        TrChkChapConf.Visible = True
                    Else
                        TrChkChapConf.Visible = False
                    End If
                Next

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        'Response.Redirect("~/TermsAndConditions.aspx")
        If ChkEmail.Checked = False Then
            lblErrorMsg.Text = "Please Enable the Email CheckBox"
            Exit Sub
        ElseIf TrChkConf.Visible = True And CheckConfMsg.Checked = False Then 'Then     If ChkEmail.Checked = True And
            lblErrorMsg.Text = "Please Enable the Location Conflict CheckBox"
            Exit Sub
        ElseIf TrChkChapConf.Visible = True And CheckChapConf.Checked = False Then
            lblErrorMsg.Text = "Please Enable the Chapter Conflict CheckBox"
            Exit Sub
        Else
            Page.Response.Redirect("TermsAndConditions.aspx")
        End If
    End Sub
    Private Sub SendDasMessage(ByVal sSubject As String, ByVal sMailTo As String)

        'Build Email Message
        Dim email As New MailMessage
        Dim sBody As String
        email.From = New MailAddress("nsfcontests@northsouth.org") 'nsfcontests@gmail.com")
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        sBody = "Error Catched During the Registartion of " & Session("LoginEmail") & ". Please investigate."
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))
        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        client.Host = host

        Try
            client.Send(email)
        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub hlinkEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkEmail.Click
        'lblEmailInfo.Text = "Please Refresh the Page after Email Change and Proceed"
        Response.Redirect("ChangeEmail.aspx?ID=1")
        'Response.Write("<script language='javascript'>window.open('ChangeEmail.aspx','_blank','left=150,top=0,width=800,height=500,toolbar=0,location=0,scrollbars=1');</script> ")
    End Sub
End Class
