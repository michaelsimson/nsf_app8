Imports System.IO
Imports System.Web.UI.HtmlControls
Imports System.Text
Imports iTextSharp.text
Imports iTextSharp.text.pdf
Imports iTextSharp.text.html
Imports iTextSharp.text.html.simpleparser
Imports NorthSouth.BAL
Partial Class ShowVolunteerCertificateBlankPDF
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateParticipantCertificates
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))

            End If

            'Dim jpg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
            'jpg.ScaleToFit(120.0F, 120.0F)
            LoadCertificates()

        End If
    End Sub

    Protected Sub rptCertificate_OnItemDataBound(ByVal sender As Object, ByVal e As RepeaterItemEventArgs)
        'Dim obj As ImageButton = e.Item.FindControl("imgLogo")
        'Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfwww.jpg")
        'jpg.ScaleToFit(120.0F, 120.0F)
        'obj.ImageUrl = jpg.ToString()

        'Dim Img As Image = (Image)obj
        'Dim dv As DataRowView = TryCast(e.Item.DataItem, DataRowView)
        'If dv IsNot Nothing Then
        '    Dim rptOrderDetails As Repeater = TryCast(e.Item.FindControl("imgLogo"), Repeater)

        '    If rptOrderDetails IsNot Nothing Then
        '        Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
        '        jpg.ScaleToFit(120.0F, 120.0F)
        '        rptOrderDetails.DataSource = jpg
        '        rptOrderDetails.DataBind()
        '    End If
        'End If
        FontFactory.Register(Server.MapPath("Font"))

        ' ''Dim yourFont As BaseFont = BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), BaseFont.WINANSI, BaseFont.EMBEDDED)
        ' ''Dim SCRIPTBL As New Font(yourFont, 15, Font.NORMAL)
        ' ''lbl1.Font.Name = "yourFont"
        ' ''Dim bfR As iTextSharp.text.pdf.BaseFont
        ' ''bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED)
        ' ''Dim pathBlackItalic As String = HttpContext.Current.Server.MapPath("Font\\Scriptbl.ttf")
        ' ''Dim blackItalic As BaseFont = BaseFont.CreateFont(pathBlackItalic, BaseFont.IDENTITY_H, BaseFont.EMBEDDED)
        ' ''Dim fontnew As New Font(blackItalic, 22)

        'Dim bfR As iTextSharp.text.pdf.BaseFont
        'bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.EMBEDDED)
        'Dim mainFont As New Font(bfR, 16, Font.NORMAL)
        'Me.lbl1.Font = New Font("mainFont", 20, FontStyle.Regular)
        FontFactory.Register(Server.MapPath("Font\\ScriptMT.ttf"))
        FontFactory.Register(Server.MapPath("Font\\georgiaz.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comic.ttf"))
        FontFactory.Register(Server.MapPath("Font\\comicbd.ttf"))
        FontFactory.Register(Server.MapPath("Font\\ARIALNI.TTF"))
        'FontFactory.GetFont("ScriptMT", 18)
        Dim item As RepeaterItem = e.Item
        Dim drv As System.Data.DataRowView = DirectCast((e.Item.DataItem), System.Data.DataRowView)
        Dim Newlbl As Label = DirectCast(item.FindControl("lblnew"), Label)
        Dim Newlbl1 As Label = DirectCast(item.FindControl("lblTitle1"), Label)
        Dim Newlbl2 As Label = DirectCast(item.FindControl("lblTitle2"), Label)
        Dim Newlbl3 As Label = DirectCast(item.FindControl("lblLeftTitle"), Label)
        Dim Newlbl4 As Label = DirectCast(item.FindControl("lblLeftSignature"), Label)
        Dim Newlbl5 As Label = DirectCast(item.FindControl("lblRightTitle"), Label)
        Dim Newlbl6 As Label = DirectCast(item.FindControl("lblRightSignature"), Label)
        Dim Newlbl7 As Label = DirectCast(item.FindControl("lblSigTitle"), Label)
        Dim Newlbl8 As Label = DirectCast(item.FindControl("lblRightSigTitle"), Label)
        Dim Newlbl9 As Label = DirectCast(item.FindControl("lblcomm"), Label)
        Dim Newlbl10 As Label = DirectCast(item.FindControl("lblNSF"), Label)
        Dim bfR As iTextSharp.text.pdf.BaseFont
        bfR = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ScriptMT.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont As New Font(bfR)
        Dim bfR1 As iTextSharp.text.pdf.BaseFont
        bfR1 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\georgiaz.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont1 As New Font(bfR1)
        Dim bfR2 As iTextSharp.text.pdf.BaseFont
        bfR2 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comic.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont2 As New Font(bfR2)
        Dim bfR3 As iTextSharp.text.pdf.BaseFont
        bfR3 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\comicbd.ttf"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont3 As New Font(bfR3)
        Dim bfR4 As iTextSharp.text.pdf.BaseFont
        bfR4 = iTextSharp.text.pdf.BaseFont.CreateFont(Server.MapPath("Font\\ARIALNI.TTF"), iTextSharp.text.pdf.BaseFont.WINANSI, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)
        Dim mainFont4 As New Font(bfR4)
        'mainFont = FontFactory.GetFont("Arial", 18)
        Newlbl.Font.Name = mainFont1.Familyname.ToString()
        Newlbl1.Font.Name = mainFont.Familyname.ToString()
        Newlbl2.Font.Name = mainFont.Familyname.ToString()
        Newlbl3.Font.Name = mainFont3.Familyname.ToString()
        Newlbl4.Font.Name = mainFont3.Familyname.ToString()
        Newlbl5.Font.Name = mainFont3.Familyname.ToString()
        Newlbl6.Font.Name = mainFont3.Familyname.ToString()
        Newlbl7.Font.Name = mainFont2.Familyname.ToString()
        Newlbl8.Font.Name = mainFont2.Familyname.ToString()
        Newlbl9.Font.Name = mainFont3.Familyname.ToString()
        Newlbl10.Font.Name = mainFont4.Familyname.ToString()
        'Response.Write(mainFont.Familyname.ToString())
        Dim Str As String
        Dim Str1 As String
        Dim Str2 As String
        Dim Str3 As String
        Dim Str4 As String
        Str1 = mainFont1.Familyname.ToString()
        Str = mainFont.Familyname.ToString()
        Str2 = mainFont2.Familyname.ToString()
        Str3 = mainFont3.Familyname.ToString()
        Str4 = mainFont4.Familyname.ToString()
        Newlbl.Attributes.Add("Style", "font-face:" + Str1 + ";font-size:18pt;font-weight:bold;")
        Newlbl1.Attributes.Add("Style", "font-face:" + Str + ";font-size:36pt;font-weight:bold;")
        Newlbl2.Attributes.Add("Style", "font-face:" + Str + ";font-size:26pt;font-weight:bold;")
        Newlbl3.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl4.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl5.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl6.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;")
        Newlbl7.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
        Newlbl8.Attributes.Add("Style", "font-face:" + Str2 + ";font-size:14pt;")
        Newlbl9.Attributes.Add("Style", "font-face:" + Str3 + ";font-size:14pt;font-weight:bold;")
        Newlbl10.Attributes.Add("Style", "font-face:" + Str4 + ";font-size:11pt;")
        'Response.Write(mainFont.Familyname.ToString())
        'Dim s As Style = New Style
        's.Font.Name = mainFont.Familyname.ToString()
        'Newlbl.ApplyStyle(s)
        'Newlbl.ControlStyle.Font.Name = mainFont.Familyname.ToString()
        'Newlbl.RenderControl()
        'rptCertificate.Controls.Add(DirectCast(item.FindControl("lblnew"), Label))
        ' Newlbl.Font.Name = mainFont.
        'ApplyFontStyleRecursively(Me.Page, Newlbl.Font)
    End Sub
    Public Sub ApplyFontStyleRecursively(ByVal parentControl As System.Web.UI.Control, ByVal fontInfo As FontInfo)
        If TypeOf parentControl Is Label Then
            DirectCast(parentControl, Label).Font.CopyFrom(fontInfo)
        End If

        For Each c As System.Web.UI.Control In parentControl.Controls
            ApplyFontStyleRecursively(c, fontInfo)
        Next
    End Sub
    'Private Sub LoadDataList()
    '    'Dim objDI As New IO.DirectoryInfo(Server.MapPath("Images\"))
    '    Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
    '    jpg.ScaleToFit(120.0F, 120.0F)
    '    Me.rptCertificate.DataSource = jpg
    '    Me.rptCertificate.DataBind()
    'End Sub
    'Public Sub GetEvents()
    '    Dim Image As New ImageField
    '    Dim jpg As iTextSharp.text.Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
    '    jpg.ScaleToFit(120.0F, 120.0F)

    'End Sub

    Private Sub LoadCertificates()
        Dim dsCertificates As New DataSet
        Dim badge As New Badge()

        generateBadge = CType(Context.Handler, GenerateParticipantCertificates)
        dsCertificates = badge.GetVolunteerCertificates(Application("ConnectionString"), Session("SelChapterID"))
        If dsCertificates.Tables(0).Rows.Count > 0 Then
            rptCertificate.Visible = True
            pnlMessage.Visible = False
            rptCertificate.DataSource = dsCertificates.Tables(0)
            rptCertificate.DataBind()
        Else
            rptCertificate.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Certificate Details found."
        End If


        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            'Response.Charset = ""
            Response.ContentType = "application/vnd.pdf"
            'FontFactory.Register(Server.MapPath("Font\\SCRIPTBL.TTF"))
            'Dim Font As New Font(iTextSharp.text.Font.FontFamily.HELVETICA, 15, iTextSharp.text.Font.BOLD, BaseColor.BLACK)


            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Certificates_Volunteers_" & strChapterName & ".pdf")
            Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim sw As New StringWriter()
            Dim hw As New HtmlTextWriter(sw)
            rptCertificate.DataSource = dsCertificates.Tables(0)
            rptCertificate.DataBind()
            rptCertificate.RenderControl(hw)
            Dim sr As New StringReader(sw.ToString())
            'Dim docWorkingDocument As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.A4.Rotate(), 1, 1, 0, 0)
            Dim pdfDoc As New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER.Rotate(), 72, 72, 56, 0)
            'Dim pgSize As New iTextSharp.text.Rectangle(600, 600)
            'Dim pdfdoc As New iTextSharp.text.Document(pgSize, 5, 5, 45, 0)
            '*************************************************************************************
            '' ''Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            '' ''writer.SetFullCompression()
            '' ''writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5)
            '' ''writer.CompressionLevel = PdfStream.BEST_COMPRESSION
            '' ''writer.SetFullCompression();
            'Or
            'writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5);
            'writer.CompressionLevel = PdfStream.BEST_COMPRESSION;
            'writer.SetFullCompression();
            '***************************************************************************************

            Dim htmlparser As New HTMLWorker(pdfDoc)
            ''''PdfWriter.GetInstance(pdfDoc, Response.OutputStream)
            Dim writer As PdfWriter = PdfWriter.GetInstance(pdfDoc, Response.OutputStream) 'newly added
            writer.SetFullCompression() 'newly added
            writer.SetPdfVersion(PdfWriter.PDF_VERSION_1_5) 'newly added
            writer.CompressionLevel = PdfStream.BEST_COMPRESSION 'newly added
            writer.SetFullCompression() 'newly added
            ' pdfDoc.Compress = False
            pdfDoc.Open()
            'Dim reader As iTextSharp.text.pdf.PdfReader = Nothing
            'Dim stamper As iTextSharp.text.pdf.PdfStamper = Nothing
            'Dim pageCount As Integer = 0

            'pageCount = reader.NumberOfPages()
            'htmlparser.Parse(sr)
            'Dim int2 As New Integer
            'int2 = pdfDoc.PageCount

            htmlparser.Parse(sr)
            'For pageIndex As Integer = 0 To 3
            '    Dim jpg As Image = iTextSharp.text.Image.GetInstance("http://www.northsouth.org/app8/Images/nsfww.jpg")
            '    jpg.ScaleToFit(120.0F, 120.0F)
            '    jpg.SetAbsolutePosition(200, 200)
            '    pdfDoc.Add(jpg)
            'Next
            pdfDoc.Close()
            Response.Write(pdfDoc)
            Response.End()
        End If
    End Sub

    Protected Function GetProductName(ByVal ChildNumber As Double) As String
        Dim strProductName As String
        strProductName = ""
        Dim dsProductNames As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        dsProductNames = badge.GetProductNames(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Now.Year, generateBadge.ContestDates, ChildNumber)
        If dsProductNames.Tables(0).Rows.Count > 0 Then
            For i = 0 To dsProductNames.Tables(0).Rows.Count - 1
                If Len(strProductName) > 0 Then
                    If i = dsProductNames.Tables(0).Rows.Count - 1 Then
                        strProductName = strProductName & " and " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    Else
                        strProductName = strProductName & ", " & dsProductNames.Tables(0).Rows(i)(0).ToString()
                    End If
                Else
                    strProductName = dsProductNames.Tables(0).Rows(i)(0).ToString()
                End If
            Next
        Else
            strProductName = ""
        End If
        Return strProductName
    End Function


End Class

