 <%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false"
    Trace="false" Inherits="VRegistration.Login" CodeFile="Login_Test.aspx.vb" %> 
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
  
    <asp:Panel runat="server" ID="pnlParent">
        <table width="100%" id="tblParent" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                    Welcome to North South Foundation&nbsp;<br />
                            PARENT LOGIN PAGE&nbsp;<br>
                   
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p class="txt01_strong">Dear Parents, 
                    </p>
                   <p>
                        <font class="txt02">If you are a first timer to NSF, please select the option �New to NSF.�
                          You will be asked to fill out your profile including your spouse. For registering contests 
                         or workshops, we need profiles on both parents. </font>
                    </p>
                    <p>
                        <font class="txt02">If you are not a first timer, you need your email and password to
                         access the system. The email must be the same one you used last time to enter this system.
                          If you forgot the email you used last time, please follow one of the options given below
                           to retrieve it. </font>
                    </p>
                    <p style="text-align :justify">
                        <font class="txt02">By logging in below as a parent and registering for NSF Workshops or Contests, you agree to the following terms:<br /><span class="txt01_strong">
"As a parent or legal guardian of my ward, I voluntarily give consent for him/her to participate in all activities associated with NSF Workshops and/or Contests.
 Further, I hereby release and discharge NSF, their officers, sponsors, agents, servants, and volunteers, and persons, firms, or corporations contracting with,
  or acting on behalf of, the NSF including the organizations providing the venue and facilities therein, their employees and agents with respect to all
   activities associated with NSF Workshops and Contests, as well as their heirs, executors, administrators, successors, or assigns, from any cause of action
    of any nature whatsoever arising from the participation of my ward as well as the participation of the remaining members of my family and friends in any
     and all activities associated with the Workshops and Contests. I hereby give my consent for NSF to use any photographs, video or other recordings of the
      aforesaid persons in relation to the Contests and Workshops in any promotional material or media releases of NSF. Any waivers to these conditions shall
       not be valid unless issued in writing by a duly authorized representative of the North South Foundation."</span></font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlVolunteers">
        <table width="100%" id="tblVolunteers" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                  Welcome to North South Foundation
                            <br />
                            VOLUNTEER LOGIN PAGE&nbsp;<br>
                   
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="txt02">Our goal is to make your Internet experience more user-friendly
                            so that your time is well spent in serving the cause of the Foundation. We want
                            to automate more functions over tine in order to achieve more bang for the valuable
                            time you spend with NSF. Please send your feedback to <a href="Mailto:nsfcontests@northsouth.org">
                                nsfcontests@northsouth.org</a> Please be as specific as possible so that we can be
                            more effective in making improvements. </font>
                    </p>
                    <p>
                        <font class="txt02">If you are a first timer to NSF, please select the option �New
                            to NSF.� You will be asked to fill out your profile. For married people, we request
                            profiles on both spouses. </font>&nbsp;</p>
                    <p>
                        <font class="txt02">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to enter
                            this system. If you forgot the email you used last time, please follow one of the
                            options given below to retrieve it. </font>
                    </p>
                    <p>
                        <font class="txt02">You should use the same email and password whether you are entering
                            the system as a parent, donor or volunteer. Further, if you have a spouse, each
                            spouse should use a different email to login. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDonors">
        <table width="100%" id="tblDonors" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                   Welcome to North South Foundation<br />
                            DONOR LOGIN PAGE&nbsp;<br>
                   
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="txt02">Thank you for being a donor to the North South Foundation. We
                            value your contribution very highly. People like you make it possible for NSF to
                            continue its mission year after year. Our goal is to make your online donation experience
                            a pleasant one. </font>
                    </p>
                                        <center><b>New to NSF</b></center>  <p>
                        <font class="txt02">If you are a first timer to NSF, please <a href="createuser.aspx" class="btn_02" >click here</a>.  </font>&nbsp;</p>
                   <center><b>Existing Users</b></center><p>
                        <font class="txt02">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to login.
                            If you forgot the email you used last time, please follow one of the options given
                            below to retrieve it. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlTHON" Visible="false" >
        <table width="100%" id="tblTHON" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                    Welcome to North South Foundation<br />
                            WALK-A-THON/MARATHON LOGIN PAGE&nbsp;<br>                   
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="txt02">Thank you for being a Walk-a-thon/Marathon walker/Runner to the North South Foundation. We
                            value your contribution very highly. People like you make it possible for NSF to
                            continue its mission year after year. Our goal is to make your online donation experience
                            a pleasant one. </font>
                    </p>
                    <p>
                        <font class="txt02">If you are a first timer to NSF, please select the option �New
                            to NSF.� You will be asked to fill out a form. For uniformity, we use the same form
                            for donors, parents and volunteers. Although we love to have all the information
                            in our records, you only need to provide your name, address, email and phone number
                            to meet IRS requirements. We do not share your information with any third party.
                        </font>&nbsp;</p>
                    <p>
                        <font class="txt02">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to login.
                            If you forgot the email you used last time, please follow one of the options given
                            below to retrieve it. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
     <asp:Panel runat="server" ID="PnlGame">
        <table width="100%" id="Table1" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                   Welcome to North South Foundation&nbsp;<br />
                            GAME LOGIN PAGE&nbsp;<br>
                  
                </td>
            </tr>
            <tr>
                <td style="height: 150px;">
                    <p>
                        <font class="txt01_strong">Dear Child, </font>
                    </p>
                    <p>
                        <font class="txt02">You need <b>your own separate </b>login ID and password to 
                        access the game. If you forgot your login ID or password, please ask your 
                        parents.If you are having problems, ask your parents to login as a parent and 
                        verify your Login ID and password in the Game section by clicking on �Create 
                        Child Login�.</font></p>
                    <p>
                        <font class="txt02">If you are new to the game you are interested, 
                        ask your parents to register you for the game using Parent Login from the home page.
                        If you like, you can also try the demo by pressing the appropriate button below. </font>
                        &nbsp;</p>
                    
                </td>
            </tr>
            <tr>
            <td><asp:HyperLink ID="hypAboutNSFOnlineGames" CssClass="btn_02" runat="server" Text="About NSF Online Games" BorderStyle="Solid" NavigateUrl="~/GameHelp.aspx" BackColor="Silver" BorderColor="Silver" ForeColor="MediumBlue" Font-Size="medium" Font-Bold="true" ></asp:HyperLink></td>
            </tr>
            <tr>
            <td><br /><asp:HyperLink ID="hypSpellingdemo" CssClass="btn_02"  runat="server" Text="Spelling Bee Game Demo" BorderStyle="Solid" NavigateUrl="~/Spelling_Game/Demo_SpellingGame.aspx" BackColor="Silver" BorderColor="Silver" ForeColor="MediumBlue" Font-Size="medium" Font-Bold="true" ></asp:HyperLink></td>
            </tr>
            <tr>
            <td><br /><asp:HyperLink ID="HypbtnVocabdemo" CssClass="btn_02"  runat="server" Text="Vocabulary Game Demo" BorderStyle="Solid" NavigateUrl="~/Vocab_Game/demo_vocab_category_selection.aspx" BackColor="Silver" BorderColor="Silver" ForeColor="MediumBlue" Font-Size="medium" Font-Bold="true" ></asp:HyperLink></td>
            </tr>
            <%--<tr>
            <td><asp:Button ID="btnSpellingdemo" runat="server" Text="Spelling Bee Game Demo" CausesValidation="false"  /></td></tr>
            <tr><td><asp:Button ID="btnVocabdemo" runat="server" Text="Vocabulary Game Demo" CausesValidation="false" /></td>
            </tr>--%>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID ="pnlStudent" Height="181px">
        <table width="100%" id="Table3" runat="server">
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                    Welcome to North South Foundation&nbsp;<br />
                    STUDENT LOGIN PAGE&nbsp;<br>
                </td>
            </tr>
            <tr>
                <td style="height: 150px;">
                    <p>
                        <font class="txt01_strong">Dear Child, </font>
                    </p>
                    <p>
                        <font class="txt02">You need login ID and password to access the Student 
                        Materials. If you forgot your login ID or password, please ask your parents.
                        </font>
                    </p>
                    <p>
                        <font class="txt02">&nbsp;</font>&nbsp;</p>
                </td>
            </tr>
            <%--<tr>
            <td><asp:Button ID="btnSpellingdemo" runat="server" Text="Spelling Bee Game Demo" CausesValidation="false"  /></td></tr>
            <tr><td><asp:Button ID="btnVocabdemo" runat="server" Text="Vocabulary Game Demo" CausesValidation="false" /></td>
            </tr>--%>
        </table>
    </asp:Panel>
    <table id="tblErrorLogin" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p id="se">
                    Login Attempt failed. Invalid Email and/or password. Please try again.
                </p>
            </td>
        </tr>
    </table>
    <table id="tblloginlock" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
   <asp:Label ID="lblloginlock" runat="server" ForeColor="red" Font-Bold="true"></asp:Label>
                <asp:Label ID="lblstudentloginerror" runat="server" Font-Bold="True"></asp:Label>
            </td>
        </tr>
    </table>
    <table id="tblLogin" runat="server" style="width: 65%"   border="0">
        <tr >
                        <td class="title04" valign="middle"  align="left" style="width: 500px;">
               
                    Sign On
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap align="right" style="width: 403px; height: 20px;">
               <asp:Label ID="lbllogin" CssClass="txt01_strong" runat="server" ></asp:Label> </td>
            <td style="width: 689px; height: 37px;">
                <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" Width="300" CssClass="text1"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Enter Login Id." ControlToValidate="txtUserId"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td valign="top" class="txt01_strong" nowrap align="right" style="width: 403px; height: 26px;">
            Password&nbsp;
            </td>
            <td style="width: 689px; height: 26px;">
                <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" Width="300" CssClass="text1"
                    TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                        runat="server" ErrorMessage="Enter Password."  ControlToValidate="txtPassword"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 403px">
                &nbsp;</td>
            <td align="center" style="width: 689px">
               <%-- <div align="center">
                    <center>
                        <div class="g-recaptcha" id="rcaptcha" data-sitekey="6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"></div>
                        <noscript>
                            <div style="width: 302px;">
                                <div style="width: 302px; height: 422px; position: relative;">
                                    <div style="width: 302px; height: 422px; position: absolute;">
                                        <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"
                                            frameborder="0" scrolling="no"
                                            style="width: 302px; height: 422px; border-style: none;"></iframe>
                                    </div>
                                </div>
                                <div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                                    <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                                        class="g-recaptcha-response"
                                        style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"
                                        rows="5" cols="210">
        </textarea>
                                </div>

                            </div>
                        </noscript>

                        <span id="spCaptcha" style="margin-left:100px;color:red" runat="server" visible="false" >You can't leave Captcha Code empty</span>
                      </center></div>--%>

                   <%-- <div id="responsive_recaptcha" style="display: none">

        <div id="recaptcha_image"></div>
        <div class="recaptcha_only_if_incorrect_sol" style="color: red">Incorrect please try again</div>

        <label class="solution">
            <span class="recaptcha_only_if_image">Type the text:</span>
            <span class="recaptcha_only_if_audio">Enter the numbers you hear:</span>

            <input type="text" id="recaptcha_response_field" name="recaptcha_response_field" />
        </label>
        <div class="options">
            <a href="javascript:Recaptcha.reload()" id="icon-reload">Get another CAPTCHA</a>
            <a class="recaptcha_only_if_image" href="javascript:Recaptcha.switch_type('audio')" id="icon-audio">Get an audio CAPTCHA</a>
            <a class="recaptcha_only_if_audio" href="javascript:Recaptcha.switch_type('image')" id="icon-image">Get an image CAPTCHA</a>
            <a href="javascript:Recaptcha.showhelp()" id="icon-help">Help</a>
        </div>
    </div>

    <script type="text/javascript"
        src="http://www.google.com/recaptcha/api/challenge?k=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ">
 </script>

    <noscript>
        <iframe src="http://www.google.com/recaptcha/api/noscript?k=6LetKAwTAAAAAGCkRxhFOur1D4tzMG6yUNgUq3oQ"
            height="300" width="500" frameborder="0"></iframe>
        <br>
        <textarea name="recaptcha_challenge_field" rows="3" cols="40">
   </textarea>
        <input type="hidden" name="recaptcha_response_field"
            value="manual_challenge">
    </noscript>--%>


                <div id="recaptcha_widget_div" style="" class=" recaptcha_nothad_incorrect_sol recaptcha_isnot_showing_audio"><div id="recaptcha_area"><table id="recaptcha_table" class="recaptchatable recaptcha_theme_red"> 
  <tbody>
     <tr> 
        <td colspan="6" class="recaptcha_r1_c1"></td> 
     </tr> 
     <tr> 
        <td class="recaptcha_r2_c1"></td> 
        <td colspan="4" class="recaptcha_image_cell">
            <center><div id="recaptcha_image" style="width: 300px; height: 57px;">
                  <img id="recaptcha_challenge_image" alt="reCAPTCHA challenge image" height="57" width="300" src="http://www.google.com/recaptcha/api/image?c=03AHJ_VutTaFjCI-gV3f3W2_M6gix7arVpF-9EOz-f773U5LmDrl33mKCn9wMXYGe0t8-xy-1HD0ysHzOI_NYyOtxOxD_a4Jj5G5h4bDMalKBQ5PDJaaE6ur8K44ilzimisHHYX6xZJ4y9xeuP6lT4vQa59-nNPju3VrlolnYgbM6oKgD7el1Rr9cpbRojjc_2zFraHkTjxyUU"></div>
            </center>
         </td> 
         <td class="recaptcha_r2_c2"></td> 
      </tr> 
      <tr> 
         <td rowspan="6" class="recaptcha_r3_c1"></td> 
         <td colspan="4" class="recaptcha_r3_c2"></td> 
         <td rowspan="6" class="recaptcha_r3_c3"></td> 
      </tr> 
      <tr> 
         <td rowspan="3" class="recaptcha_r4_c1" height="49"> 
              <div class="recaptcha_input_area"> 
                  <span id="recaptcha_challenge_field_holder" style="display: none;">                      
                      <input type="hidden" name="recaptcha_challenge_field" id="recaptcha_challenge_field" value="03AHJ_VutTaFjCI-gV3f3W2_M6gix7arVpF-9EOz-f773U5LmDrl33mKCn9wMXYGe0t8-xy-1HD0ysHzOI_NYyOtxOxD_a4Jj5G5h4bDMalKBQ5PDJaaE6ur8K44ilzimisHHYX6xZJ4y9xeuP6lT4vQa59-nNPju3VrlolnYgbM6oKgD7el1Rr9cpbRojjc_2zFraHkTjxyUU"></span>
                      <input name="recaptcha_response_field" id="recaptcha_response_field" type="text" autocorrect="off" autocapitalize="off" placeholder="Type the text" autocomplete="off" style="font-style: italic;"> 
                           <span id="recaptcha_privacy" class="recaptcha_only_if_privacy"><a href="http://www.google.com/intl/en/policies/" target="_blank">Privacy &amp; Terms</a></span> 
              </div> 
          </td> 
          <td rowspan="4" class="recaptcha_r4_c2"></td> 
          <td><a id="recaptcha_reload_btn" title="Get a new challenge">
              <img id="recaptcha_reload" width="25" height="17" src="http://www.google.com/recaptcha/api/img/red/refresh.gif" alt="Get a new challenge"></a></td> 
          <td rowspan="4" class="recaptcha_r4_c4"></td> 
      </tr> 
      <tr> 
          <td><a id="recaptcha_switch_audio_btn" class="recaptcha_only_if_image" title="Get an audio challenge">
               <img id="recaptcha_switch_audio" width="25" height="16" alt="Get an audio challenge" src="http://www.google.com/recaptcha/api/img/red/audio.gif"></a><a id="recaptcha_switch_img_btn" class="recaptcha_only_if_audio" title="Get a visual challenge">
               <img id="recaptcha_switch_img" width="25" height="16" alt="Get a visual challenge" src="http://www.google.com/recaptcha/api/img/red/text.gif"></a>
          </td> 
      </tr> 
      <tr> 
          <td><a id="recaptcha_whatsthis_btn" title="Help">
               <img id="recaptcha_whatsthis" width="25" height="16" src="http://www.google.com/recaptcha/api/img/red/help.gif" alt="Help"></a>
          </td> 
      </tr> 
      <tr> 
          <td class="recaptcha_r7_c1"></td> <td class="recaptcha_r8_c1"></td> 
      </tr> 
  </tbody>
 </table> 
</div> 
</div>


            </td>
        </tr>
       
        <tr>
            <td style="width: 403px">
            </td>
            <td align="center" style="width: 689px">
                <asp:Button ID="btnLogin" runat="server"   Text="Login"></asp:Button></td>
        </tr>
       
    </table>
    <table id="Table2" runat="server" style="width: 70%;" border="0">
        
        <tr>
            <td style="width: 342px">
            </td>
            <td class="txt01" style="width: 689px">
                Click here if you
                <asp:HyperLink ID="Hyperlink1" runat="server"  CssClass="btn_02"  NavigateUrl="Forgot.aspx">forgot your password</asp:HyperLink>&nbsp;&nbsp;/
                &nbsp;&nbsp;<asp:HyperLink Text="change password"   CssClass="btn_02" runat="server" NavigateUrl="~/ChangePassword.aspx"
                    ID="lnkChangePwd"></asp:HyperLink></td>
        </tr>
        
        <tr>
            <td style="width: 342px">
            </td>
            <td class="txt01" style="width: 689px">
                Click here if you
                <asp:HyperLink Text="forgot login ID"  CssClass="btn_02"  runat="server" NavigateUrl="~/Forgot_loginid.aspx"
                    ID="HyperLink2"></asp:HyperLink>
                    &nbsp;&nbsp;/&nbsp;&nbsp;
                    <asp:HyperLink ID="HyperLink4"  CssClass="btn_02" runat="server"  NavigateUrl="ChangeEmail.aspx">email address for login is no longer valid</asp:HyperLink>
            </td>
        </tr>
       
        <tr>
            <td >
            </td>
            <td class="txt01" style="width: 689px">
                Click here if you are
                <asp:HyperLink ID="HyperLink3" CssClass="btn_02" runat="server"  NavigateUrl="createuser.aspx">new to NSF</asp:HyperLink>
            &nbsp; /&nbsp;&nbsp; <asp:HyperLink ID="HyperLink5" CssClass="btn_02" runat="server"  NavigateUrl="EmailNotification.aspx">Unsubscribe to email notification</asp:HyperLink>
            </td>
        </tr>
    </table>
    
        <table id="tblMissingRole" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p>
                   <%-- Sorry, we don't yet have any assigned roles for you in our records. Please consult
                    your contact or if you don't have a contact, send an email to <a href="mailto: nsfvolunteer@gmail.com"
                        target="_blank">nsfvolunteer@gmail.com</a> with all your details.--%>
                       We have your registration.  You are all set.  Next step is to assign you a role in our system.  This can be done by your chapter coordinator or contact the person who requested you to register.
                </p>
            </td>
        </tr>
    </table>
    <table id="tblMissingProfile" visible="false" align="center"  runat="server" style="width:950px">
        <tr>
            <td class="ErrorFont" align="center">
                <p>
                    Your personal profile is not in our records. Please press Continue to enter your
                    personal profile
                </p>
                <asp:Button ID="btnContinue" runat="server" CssClass="FormButton" Text="Continue"></asp:Button></td>
        </tr>
    </table>
    <table id="tblInvalidEmail" visible="false" runat="server" style="width:950px">
        <tr>
            <td class="ErrorFont" align="center"><br />
                <p align="justify">
                   We have discovered a problem with your email not being unique.  A message will be sent to Customer Service team.  If you don�t hear in two or three days, please send an email to nsfcontests@northsouth.org.  Press to continue.
                </p><br /><br />
                <asp:Button ID="btnContinue1" runat="server" CssClass="FormButton" Text="Continue"></asp:Button></td>
        </tr>
    </table>
    <table id="trIE" visible="false" runat="server" style="width:950px">
        <tr>
            <td class="ErrorFont" align="center"><br />
                <p align="center">
                   We have discovered a problem with your Browser. Please use Internet Explorer browser only.
                </p><br /><br />   </td>             
        </tr>
    </table>
    <table id="tblMissingToken" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p>
                Error: Missing entry token.
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a parent,<a href="login.aspx?entry=p"> login here.</a>
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a volunteer, <a href="login.aspx?entry=v">login here.</a>
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a donor , <a href="login.aspx?entry=d">login here .</a>
            </td>
        </tr>
    </table>
    <!--
			<table id="tblLinks" runat="server" style="width: 659px; height: 1px">
				<tr>
					<td>
						<a id="homeLink" runat="server">  Home</a>
						
					</td>
				</tr>
				</table>
				-->
</asp:Content>
