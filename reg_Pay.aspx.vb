Imports System
Imports System.Text
Imports System.Configuration
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections.Specialized
Imports System.Globalization
Imports LinkPointTransaction
Imports System.Text.RegularExpressions
Imports NorthSouth.BAL
Imports nsf.Entities
Imports System.Net.Mail
'Imports GDE4
'Imports GDE4.Service
'Imports GDE4.Transaction
Imports Braintree
Imports System.IO

Namespace VRegistration
    ' <summary>
    ' Summary description for regnlregnet_Pay.
    ' </summary>
    Partial Class reg_Pay
        Inherits LinkPointAPI_cs.LinkPointTxn_Page

        'Private us As CultureInfo = New CultureInfo("en-US")
        Protected ClientToken As String = String.Empty

        'http://regxlib.com/REDetails.aspx?regexp_id=540
        Private nRegFee As Decimal = 0
        Private SaleAmt As Decimal = 0.0
        Private FundRAmt As Decimal = 0.0
        Private nDonationAmt As Decimal = 0
        Private nDisAmt As Decimal = 0
        Private nTotalAmt As Decimal = 0
        Private nMealsAmt As Decimal = 0.0
        Private nLateFee As Decimal = 0
        Dim isTestMode As Boolean = False
        Protected RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
        '*****************************************
        Dim Cust_id As String = String.Empty
        Dim BT_BankMessage As String = String.Empty
        Dim BT_Auth As String = String.Empty
        Dim BT_Tag As String = String.Empty
        Dim BT_result As String = String.Empty
        Dim BT_Message As String = String.Empty
        Dim BT_Error As String = String.Empty
        Dim BT_Code As String = String.Empty
        Dim BT_Id As String = String.Empty
        Dim BT_Pass As String = String.Empty
        Dim BT_Platform As String
        Protected EmailID As String
        Dim txnStatus As String = String.Empty
        Dim txtAuth As String = String.Empty
        Dim custIndId As Integer = 0
        Dim BT_ETG_Response As String = String.Empty
        Dim BT_Token As String = String.Empty
        Dim BT_Brand As String = String.Empty
        Dim Semester As String = String.Empty
        ' Brain Tree Gateway
        Dim gateway As BraintreeGateway
        Dim ccReq As TransactionCreditCardRequest = New TransactionCreditCardRequest()
        Dim ccAddrReq As CreditCardAddressRequest = New CreditCardAddressRequest()
        Dim tReq As TransactionRequest = New TransactionRequest()

        Dim yr As Integer
        Dim g As New GlobalVariable

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load

            Dim blnContinue As Boolean = True
            Semester = ""
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx")
            End If
            If Session("EventId") = 13 Then
                yr = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString())

                If (Not Session("CustIndId") Is Nothing) Then
                    custIndId = Session("CustIndId").ToString
                End If
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" & custIndId & "   AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & yr) > 0 Then 'and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
                    Semester = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 isnull(C.Semester,'') FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID and CR.Level = C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" & custIndId & "   AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & yr)
                End If
            ElseIf Session("EventId") = "MC" Then
                yr = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(Year) from Freeevent")
            Else
                ' Get event year
                If (yr.ToString().Length = 0 Or yr.ToString() = "0") Then
                    yr = g.GetYear(Application("ConnectionString"), Session("EventID"))
                End If
            End If 

            If Session("DoPay") Is Nothing Then
                If Session("entryToken") = "Parent" Then
                    Response.Redirect("UserFunctions.aspx")
                ElseIf Session("entryToken") = "Volunteer" Then
                    Response.Redirect("VolunteerFunctions.aspx")
                Else
                    Response.Redirect("DonorFunctions.aspx")
                End If
            End If
            If (Not Request.QueryString("eid") Is Nothing) Then
                Session("EventId") = Request.QueryString("eid")
            End If
            If (Session("ContestsSelected") = Nothing) Then
                If (Session("EventId") = 1) Then
                    If (Session("Mealcharges") = Nothing) Then
                        blnContinue = False
                    End If
                ElseIf (Session("EventId") = 13) Then
                    ''**lblRgfee.Visible = False
                    blnContinue = True
                    If (Session("RegFee") = Nothing) Then
                        blnContinue = False
                    End If
                ElseIf (Session("EventId") = 5 Or Session("EventId") = 12 Or Session("EventId") = 17) Then
                    lblRgfee.Visible = False
                    blnContinue = True
                ElseIf (Session("EventId") = 10) Then
                    lblRgfee.Visible = False
                    trsale.Visible = True
                    blnContinue = True
                    Session("RegFee") = Nothing
                    Session("LateFee") = Nothing
                ElseIf (Session("EventId") = 9) Then
                    Label1.Text = "Tickets : "
                    lblRgfee.Visible = False
                    trDonation.Visible = True
                    trsale.Visible = False
                    trFundR.Visible = True
                    blnContinue = True
                Else
                    blnContinue = False
                End If
            End If
            If blnContinue = False Then
                If Session("EventId") = 3 Then
                    Response.Redirect("WkShopRegistration.aspx")
                ElseIf Session("EventID") = 19 Then
                    Response.Redirect("PrepClubRegistration.aspx")
                ElseIf Session("EventID") = 20 Then
                    Response.Redirect("OnlineWkshopRegistration.aspx")
                ElseIf Session("EventId") = 13 Then
                    Response.Redirect("CoachingRegistration.aspx")
                ElseIf Session("EventId") = "MC" Then
                    Response.Redirect("FreeEvent/FreeEventReg.aspx")
                Else
                    Response.Redirect("ContestantRegistration.aspx")
                End If
            End If
            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)
            lblRegFee.Text = ""
            lblDonation.Text = ""
            lblTotalAmount.Text = ""
            lblMealsAmount.Text = ""
            lblLateFee.Text = ""
            lblSaleAmount.Text = ""
            lblFundRAmt.Text = "" 
            nTotalAmt = 0
            nDisAmt = 0

            If (Not (Session("SaleAmt")) Is Nothing) And Session("EventId") = 10 Then
                SaleAmt = CType(Session("SaleAmt"), Integer)
                lblSaleAmount.Text = SaleAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += SaleAmt
                Session("RegFee") = Nothing
                Session("LateFee") = Nothing
            End If
            If (Not (Session("RegFee")) Is Nothing) Then
                'Response.Write("Registration fee:" + Session("RegFee").ToString + "<br>")
                nRegFee = CType(Session("RegFee"), Integer)
                lblRegFee.Text = nRegFee.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nRegFee
            Else
                'Response.Write("Session(RegFee) value not set" + "<br>")

            End If
            If (Not (Session("MealCharges")) Is Nothing) Then 
                nMealsAmt = CType(Session("MealCharges"), Integer)
                lblMealsAmount.Text = nMealsAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nMealsAmt
            End If
            If (Not (Session("LATEFEE")) Is Nothing) Then
                nLateFee = CType(Session("LATEFEE"), Integer)
                lblLateFee.Text = nLateFee.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nLateFee
            End If
           
            If (Not (Session("FundRAmt")) Is Nothing) Then
                FundRAmt = CType(Session("FundRAmt"), Integer)
                If (Not (Session("Donation")) Is Nothing) Then
                    FundRAmt = FundRAmt - Session("Donation")
                    If Session("Donation") = 0 Then
                        trDonation.Visible = False
                    End If
                End If
                lblFundRAmt.Text = FundRAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += FundRAmt
            End If
            If (Not (Session("Donation")) Is Nothing) Then
                nDonationAmt = CType(Session("Donation"), Integer)
                lblDonation.Text = nDonationAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt = (nTotalAmt + nDonationAmt)
            End If
            If (Not (Session("Discount")) Is Nothing) Then
                nDisAmt = CType(Session("Discount"), Integer)
                lblDiscount.Text = "-" & nDisAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt = (nTotalAmt + CType(lblDiscount.Text, Integer))
            End If
            If lblMealsAmount.Text.Trim().Equals("") Or lblMealsAmount.Text.Trim().Equals("$0.00") Then
                lblMeals.Visible = False
                lblMealsAmount.Visible = False
            End If
            If lblLateFee.Text.Trim().Equals("") Or lblLateFee.Text.Trim().Equals("$0.00") Then
                lblLatefeetext.Visible = False
                lblLateFee.Visible = False
            End If
            If lblDiscount.Text.Trim().Equals("") Or lblDiscount.Text.Trim().Equals("$0.00") Then
                lblDiscount.Visible = False
                lblDiscountText.Visible = False
            End If
            If (nTotalAmt = 0) Then
                If Session("EventId") = 1 Or Session("EventId") = 2 Then
                    Response.Redirect("ContestantRegistration.aspx")
                ElseIf Session("EventId") = 3 Then
                    Response.Redirect("WkShopRegistration.aspx")
                ElseIf Session("EventId") = 19 Then
                    Response.Redirect("PrepClubRegistration.aspx")
                ElseIf Session("EventID") = 20 Then
                    Response.Redirect("OnlineWkshopRegistration.aspx")
                ElseIf Session("EventId") = 13 Then
                    Response.Redirect("CoachingRegistration.aspx")
                ElseIf Session("EventId") = 5 Or Session("EventId") = 12 Or Session("EventId") = 17 Then
                    Response.Redirect("Don_athon_custom.aspx?id=" & Session("WalkMarathonID"))
                ElseIf Session("EventId") = 10 Then
                    Response.Redirect("ShoppingCatalog.aspx")
                ElseIf Session("EventId") = "MC" Then
                    Response.Redirect("FreeEvent/FreeEventReg.aspx")
                ElseIf Session("EventId") = 9 Then
                    If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
                        Response.Redirect("FundRReg.aspx")
                    Else
                        Response.Redirect("FundRReg.aspx?id=1")
                    End If
                Else
                    Response.Redirect("UserFunctions.aspx")
                End If
            End If
            lblTotalAmount.Text = nTotalAmt.ToString("c", New CultureInfo("en-US")) 
            If Not Page.IsPostBack Then
                GenerateClientToken()
                Dim countries As DataSet = New DataSet
                countries.ReadXml(MapPath("countries.xml"))
                ddlCountry.DataSource = countries
                ddlCountry.DataValueField = "value"
                ddlCountry.DataTextField = "text"
                ddlCountry.DataBind()
                Dim dsStates As DataSet
                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlState.DataSource = dsStates.Tables(0)
                    ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlState.DataBind()
                End If
                GetIPAddress(lblIP)
                PrePopulate()
                ddlYear.Items.Clear()
                Dim li As ListItem = New ListItem(" ", "")
                ddlYear.Items.Add(li)
                Dim i As Integer = System.DateTime.Today.Year
                Do While (i _
                            <= (System.DateTime.Today.Year + 9))
                    Dim j As Integer = (i - 2000)
                    li = New ListItem(i.ToString, j.ToString)
                    ddlYear.Items.Add(li)
                    i = (i + 1)
                Loop

                Dim nsfMaster As NSFMasterPage = Me.Master
                If Session("EventID") = 13 Then
                    nsfMaster.addBackMenuItem("CoachingRegistration.aspx")
                ElseIf Session("EventID") = 13 Then
                    nsfMaster.addBackMenuItem("Reg_Donate.aspx")
                ElseIf Session("EventID") = 3 Then
                    nsfMaster.addBackMenuItem("WkShopRegistration.aspx")
                ElseIf Session("EventID") = 19 Then
                    nsfMaster.addBackMenuItem("PrepClubRegistration.aspx")
                ElseIf Session("EventID") = 20 Then
                    nsfMaster.addBackMenuItem("OnlineWkshopRegistration.aspx")
                ElseIf Session("EventID") = 10 Then
                    nsfMaster.addBackMenuItem("ShoppingCatalog.aspx")
                ElseIf Session("EventId") = 12 Then
                ElseIf Session("EventId") = "MC" Then
                    nsfMaster.addBackMenuItem("FreeEvent/FreeEventreg.aspx")
                    ''** update Feature for WMSponsorDonation
                ElseIf Session("EventId") = 9 Then
                    If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
                        nsfMaster.addBackMenuItem("FundRReg.aspx")
                    Else
                        nsfMaster.addBackMenuItem("FundRReg.aspx?id=1")
                    End If
                Else
                    nsfMaster.addBackMenuItem("ContestantRegistration.aspx")
                End If
            End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub
        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub

        Private Sub PrePopulateOld()
            Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
            objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), Session("CustIndID"))
            If objIndSpouseExt.Id > 0 Then
                txtAddress1.Text = objIndSpouseExt.Address1
                txtAddress2.Text = objIndSpouseExt.Address2
                txtCity.Text = objIndSpouseExt.City
                ddlState.Items.FindByValue(objIndSpouseExt.State).Selected = True
                ddlCountry.Items.FindByValue(objIndSpouseExt.Country).Selected = True
                txtZip.Text = objIndSpouseExt.Zip
            End If
        End Sub
        Private Sub PrePopulate()

            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim drIndSpouse As SqlDataReader
            Try
                If Session("EventId") = 9 Then
                    If Session("FundDonorType") = "OWN" Then
                        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.Text, "select  O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.[STATE],O.Zip,Ch.ChapterCode,'OWN' as DonorType,CASE WHEN O.COUNTRY like 'United States' THEN 'US' ELSE O.COUNTRY END as Country  from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE O.AutoMemberID = " & Session("CustIndID"))
                        ' Iterate through DataReader, should only be one 
                        While (drIndSpouse.Read())
                            If Not drIndSpouse.IsDBNull(0) Then
                                nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                                txtAddress1.Text = drIndSpouse("Address1").ToString()
                                txtCity.Text = drIndSpouse("City").ToString()
                                If drIndSpouse("State").ToString().Trim.Length > 0 Then ddlState.Items.FindByValue(drIndSpouse("State").ToString().Trim).Selected = True
                                ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                                txtZip.Text = drIndSpouse("Zip").ToString()
                                lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                            End If
                        End While
                    Else
                        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndSpouseByID", New SqlParameter("@autoMemberID", Session("CustIndID")))
                        ' Iterate through DataReader, should only be one 
                        While (drIndSpouse.Read())
                            If Not drIndSpouse.IsDBNull(0) Then
                                nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                                txtAddress1.Text = drIndSpouse("Address1").ToString()
                                txtAddress2.Text = drIndSpouse("Address2").ToString()
                                txtCity.Text = drIndSpouse("City").ToString()
                                ddlState.Items.FindByValue(drIndSpouse("State").ToString()).Selected = True
                                ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                                txtZip.Text = drIndSpouse("Zip").ToString()
                                lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                            End If
                        End While
                    End If
                Else
                    drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndSpouseByID", New SqlParameter("@autoMemberID", Session("CustIndID")))
                    ' Iterate through DataReader, should only be one 
                    While (drIndSpouse.Read())
                        If Not drIndSpouse.IsDBNull(0) Then
                            nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                            txtAddress1.Text = drIndSpouse("Address1").ToString()
                            txtAddress2.Text = drIndSpouse("Address2").ToString()
                            txtCity.Text = drIndSpouse("City").ToString()
                            ddlState.Items.FindByValue(drIndSpouse("State").ToString()).Selected = True
                            ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                            txtZip.Text = drIndSpouse("Zip").ToString()
                            lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                        End If
                    End While
                End If
                If Not drIndSpouse Is Nothing Then drIndSpouse.Close()
            Finally
                drIndSpouse = Nothing
            End Try
        End Sub

        Protected Sub PrepareFormData()
            '***Testing Purposes only
            subtotal = Request.Form("subtotal")
            tax = Request.Form("tax")
            shipping = nDonationAmt.ToString
            total = nTotalAmt.ToString

            refnumber = Request.Form("refnumber")
            If Session("EventID") = 12 Then
                comments = " Donation:" + nDonationAmt.ToString
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            ElseIf (Not (Session("SaleItems")) Is Nothing) And Session("EventID") = 10 Then
                comments = CType(Session("SaleItems"), String)
                comments = (comments + (" SaleAmt:" + SaleAmt.ToString))
                comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            ElseIf (Not (Session("ContestsSelected")) Is Nothing) Then
                comments = CType(Session("ContestsSelected"), String)
                comments = (comments + (" RegFee:" + nRegFee.ToString))
                comments = (comments + (" Mealsamount:" + nMealsAmt.ToString))
                comments = comments + (" LateFee:" + nLateFee.ToString)
                comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString)) 
            ElseIf (Not (Session("FundRItems")) Is Nothing) Then
                comments = CType(Session("FundRItems"), String)
                comments = (comments + (" FundRAmt:" + FundRAmt.ToString))
                comments = (comments + (" Discount:" + nDisAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            Else
                comments = ""
            End If

            If Session("EventID") = 10 Then
                referred = ("Sale on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 13 Then
                referred = ("Coaching Registration on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 12 Then
                referred = ("Walkathon Donation on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 9 Then
                referred = ("Fundraising Payment on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 3 Then
                referred = ("Workshop Registration on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 19 Then
                referred = ("PrepClub Registration on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 20 Then
                referred = ("Online Workshop Registration on " + System.DateTime.Now.ToLongTimeString)
            Else
                referred = ("Contest Registration on " + System.DateTime.Now.ToLongTimeString)
            End If
            result = Request.Form("result")
            origin = Request.Form("origin")
        End Sub
        Private Sub TrimTextBoxEntries(ByRef txtbox As TextBox)
            txtbox.Text = txtbox.Text.Trim
        End Sub

        Public Enum CreditCardTypeType
            Visa
            MasterCard
            Discover
            Amex
            Switch
            Solo
        End Enum

        'Private Const cardRegex As String = "^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$"
        Private Const cardRegex As String = "^(?:(?<Visa>4\d{3})|(?<MasterCard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<Amex>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(Amex)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$"

        Private Function GetCardTypeFromNumber(ByVal cardNum As String) As String
            'Create new instance of Regex comparer with our
            'credit card regex patter
            Dim cardTest As New Regex(cardRegex)

            'Compare the supplied card number with the regex
            'pattern and get reference regex named groups
            Dim gc As GroupCollection = cardTest.Match(cardNum).Groups

            'Compare each card type to the named groups to 
            'determine which card type the number matches
            If gc(CreditCardTypeType.Amex.ToString()).Success Then
                'Return CreditCardTypeType.Amex
                Return "Amex"
            ElseIf gc(CreditCardTypeType.MasterCard.ToString()).Success Then
                'Return CreditCardTypeType.MasterCard
                Return "MasterCard"
            ElseIf gc(CreditCardTypeType.Visa.ToString()).Success Then
                '  Return CreditCardTypeType.Visa
                Return "Visa"
            ElseIf gc(CreditCardTypeType.Discover.ToString()).Success Then
                'Return CreditCardTypeType.Discover
                Return "Discover"
            Else
                'Card type is not supported by our system, return null
                '(You can modify this code to support more (or less)
                ' card types as it pertains to your application)
                Return Nothing
            End If
        End Function
        ' Function to Check for CreditCard.
        Public Function IsCreditCard(ByVal strToCheck As String) As Boolean
            Dim objAlphaNumericPattern As Regex = New Regex("^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$")
            Return objAlphaNumericPattern.IsMatch(strToCheck)
        End Function

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
            Try
                'Response.Write("txtcvv.Text.Trim().Substring(0, 1) =" & txtcvv.Text.Trim().Substring(0, 1))
                'sandhya - to take care of session time out issues
                If LCase(Session("LoggedIn")) <> "true" Or Session("LoggedIn") Is Nothing Then
                    Server.Transfer("login.aspx?entry=p")
                    Exit Sub
                End If 
                lblCardError.Text = ""
                lblMessage.Text = ""
                TrimTextBoxEntries(txtCardNumber)
                If (IsCreditCard(txtCardNumber.Text) = False) Then
                    lblCardError.Text = "Invalid card number"
                    Return
                End If
                If ((Convert.ToInt32(ddlMonth.SelectedValue.ToString) < System.DateTime.Today.Month) _
                            AndAlso (Convert.ToInt32(ddlYear.SelectedValue.ToString) _
                            <= (System.DateTime.Today.Year - 2000))) Then
                    lblCardError.Text = "For a valid card the Card Expiration must be in future."
                    Return
                End If
                lblErrCvv.Text = ""
                If Len(txtcvv.Text.Trim) = 0 Then
                    lblErrCvv.Text = "CVV should not be blank."
                    Return 
                Else
                    Dim sCardType As String = GetCardTypeFromNumber(txtCardNumber.Text)
                    If sCardType Is Nothing Then
                        lblErrCvv.Text = "Enter valid credit card number"
                        Return
                    End If
                    If sCardType = "Amex" Or sCardType = "MasterCard" Or sCardType = "Visa" Or sCardType = "Discover" Then
                        Dim iDigit As Integer = 3
                        If sCardType = "Amex" Then
                            iDigit = 4
                        End If
                        If (Len(txtcvv.Text.Trim)) <> iDigit Then
                            lblErrCvv.Text = "Given Card card number does not have a " & Len(txtcvv.Text) & " digit CVV code"
                            Return
                        End If
                    End If
                End If

                If Page.IsValid Then
                    TrimTextBoxEntries(txtCardHolderName)
                    TrimTextBoxEntries(txtAddress1)
                    TrimTextBoxEntries(txtAddress2)
                    TrimTextBoxEntries(txtCity)
                    TrimTextBoxEntries(txtZip)
                    lblMessage.Text = ""  
                    lbContinue.Enabled = False
                    ' Parse form data
                    PrepareFormData()
                    ' process order
                    If PreviouslyPaid() = False Then
                        If Len(comments.ToString) < 1 Then
                            tblError.Visible = True
                            lblMessage.Text = "Payment Note is empty Please register again."
                        Else
                            tblError.Visible = False
                            ProcessOrder()
                        End If
                    Else
                        RecurrentPay()
                        tblError.Visible = True
                        lblMessage.Text = "Please wait for some time and try again." 
                        Return
                    End If

                End If
            Catch ex As Exception

            End Try
        End Sub
        Private Sub ProcessOrder()
            '*************************************************
            '*              Modified 19/08/2013              *
            '*              By Joseph Remilton               *
            '*              Modified by Bindhu on Mar 09,2015*
            '*************************************************       
            Dim logData As String = ""
            Dim NoCardIssue As Boolean = True
            Try
                Dim IsValidCustomer As Boolean = True
                tblError.Visible = True
                lblMessage.Text = ""
                lblMessage.ForeColor = Color.Red
                Dim cAddress As String
                Dim chkErrWithCustomerCreation As Boolean = False, statusReturnByCust As String = ""
                'Initialize Brain Tree Gateway
                InitializeBT()
                logData = "Initialized" & vbCrLf & "Member Id :"
                If Not Session("CustIndId") Is Nothing Then
                    logData = logData & Session("CustIndId")
                ElseIf Not Session("LoginId") Is Nothing Then
                    logData = logData & Session("LoginId")
                End If
                logData = logData & " : EventID :" & Session("EventId") & vbCrLf

                PreUpdate()
                logData = logData & " - Pre Updated" & vbCrLf
                cAddress = txtAddress1.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + txtZip.Text + "|" + ddlCountry.SelectedValue.ToString

                'Billing Address
                ccAddrReq.StreetAddress = cAddress.Trim().Replace("'", "''")
                ccAddrReq.Locality = txtCity.Text
                ccAddrReq.Region = ddlState.SelectedValue.ToString
                ccAddrReq.CountryName = ddlCountry.SelectedValue.ToString
                ccAddrReq.PostalCode = txtZip.Text

                ' Credit Card Details
                ccReq.CardholderName = txtCardHolderName.Text.Trim()
                ccReq.ExpirationMonth = ddlMonth.SelectedValue
                ccReq.ExpirationYear = ddlYear.SelectedValue
                ccReq.Number = txtCardNumber.Text.Trim()
                ccReq.CVV = txtcvv.Text.Trim()

                ' Search Customer
                Dim req = New CustomerSearchRequest().FirstName.Is(nameLabel.Text.Trim).Email.Is(lblmail.Text) '.CreditCardNumber.Is(txtCardNumber.Text)
                logData = logData & " - CustomerSearchReq " & vbCrLf
                Dim customerId As String = ""
                Dim c As Customer
                Try
                    Dim coll As Braintree.ResourceCollection(Of Customer) = gateway.Customer.Search(req)
                    For Each c In coll
                        customerId = c.Id
                    Next
                Catch ex As Exception
                    logData = logData & " - CustomerSearchReq : throws Error: " & ex.ToString & vbCrLf
                    lblMessage.Text = "Error while doing transaction with Gateway "
                    lbContinue.Enabled = True
                End Try
                Dim customer As New CustomerRequest
                customer.FirstName = nameLabel.Text
                customer.Email = lblmail.Text
                ' Commented by bindhu to pass CC information to BT from violating the Cvv/Avs rules  
 
                If customerId = "" Then
                    ' Create New Customer
                    logData = logData & " - Create New Customer" & vbCrLf
                    Dim newCustomer As Braintree.Result(Of Customer)
                    Try
                        newCustomer = gateway.Customer.Create(customer)
                        If newCustomer.IsSuccess Then
                            customerId = newCustomer.Target.Id
                            logData = logData & " - Created New Customer : Success" & vbCrLf
                        Else
                            logData = logData & " - Create New Customer : Failed" & vbCrLf
                            IsValidCustomer = False

                            GoTo lblInValidCustomer
                        End If
                    Catch ex As Exception
                        logData = logData & " - Create New Customer: throws error : " & ex.ToString & vbCrLf
                        If Not newCustomer Is Nothing Then
                            statusReturnByCust = newCustomer.Message.ToString()
                        End If
                        chkErrWithCustomerCreation = True
                    End Try
                Else
                    logData = logData & " - Update New Customer " & vbCrLf
                    Dim updateCustomer As Braintree.Result(Of Customer)
                    Try
                        updateCustomer = gateway.Customer.Update(customerId, customer)
                        If updateCustomer.IsSuccess Then
                            customerId = updateCustomer.Target.Id
                            logData = logData & " - Update Customer : Success" & vbCrLf
                        Else
                            logData = logData & " - Update New Customer : Failure" & vbCrLf
                            IsValidCustomer = False

                            GoTo lblInValidCustomer
                        End If
                    Catch ex As Exception
                        logData = logData & " - Update New Customer:Throw exception1 :" & ex.ToString
                        If Not updateCustomer Is Nothing Then
                            statusReturnByCust = updateCustomer.Message.ToString()
                        End If
                        logData = logData & " - Update New Customer:Throw exception2 :" & statusReturnByCust & vbCrLf
                        BT_Error = ex.ToString
                        chkErrWithCustomerCreation = True
                    End Try
                End If

                Try
                    logData = logData & " - BT Transaction : CreditCardVerification : start" & vbCrLf
                    Dim ccVer = New CreditCardVerificationSearchRequest().CreditCardCardholderName().Is(txtCardHolderName.Text.Trim()).CreditCardExpirationDate().Is(ddlMonth.SelectedValue.ToString() & "/" & ddlYear.SelectedValue.ToString()).CreditCardNumber().Is(txtCardNumber.Text.Trim()).BillingAddressDetailsPostalCode().Is(txtZip.Text)
                    Dim cc As Braintree.ResourceCollection(Of CreditCardVerification) = gateway.CreditCardVerification.Search(ccVer)
                    logData = logData & " - BT Transaction : CreditCardVerification : MaximumCount" & cc.MaximumCount & vbCrLf
                    Dim ccv As CreditCardVerification
                    For Each ccv In cc
                        Dim CVVRes As String = ccv.CvvResponseCode.ToString()
                        Dim AvsPostalRes As String = ccv.AvsPostalCodeResponseCode.ToString()
                        logData = logData & " - BT Transaction : CreditCardVerification : CVVRes : " & CVVRes & vbCrLf
                        logData = logData & " - BT Transaction : CreditCardVerification : AvsPostalRes : " & AvsPostalRes & vbCrLf
                    Next
                Catch ex1 As Exception
                    logData = logData & " - BT Transaction : CreditCardVerification : Error " & ex1.ToString() & vbCrLf
                End Try

                tReq.Amount = nTotalAmt.ToString("f2")
                tReq.CustomerId = customerId
                tReq.BillingAddress = ccAddrReq
                tReq.CreditCard = ccReq
                ''''' Device Data
                tReq.DeviceData = Request.Form("device_data")
                ''''
                Dim result As Braintree.Result(Of Transaction)
                Try
                    If Session("LoggedIn") Is Nothing Then
                        Server.Transfer("login.aspx?entry=p")
                        Exit Sub
                    End If
                    logData = logData & " - BT Initialized :" & vbCrLf
                    result = gateway.Transaction.Sale(tReq)
                    If (result.IsSuccess) Then
                        logData = logData & " - BT : Success" & vbCrLf
                        Dim tran As Transaction = result.Target                '
                        Dim re As Braintree.Result(Of Transaction) = gateway.Transaction.SubmitForSettlement(tran.Id)
                        'PaymentReference
                        'Status Return
                        Try
                            BT_Auth = tran.Id + "-" + tran.Customer.Id
                            logData = logData & " - BT Transaction : Success :BT_Auth" & BT_Auth & vbCrLf

                            BT_Brand = tran.CreditCard.CardType.ToString()
                            logData = logData & " - BT_Brand" & BT_Brand & vbCrLf

                            BT_ETG_Response = tran.ProcessorAuthorizationCode.ToString
                            logData = logData & " - BT_ETG_Response" & BT_ETG_Response & vbCrLf

                            BT_BankMessage = tran.ProcessorResponseText.ToString()
                            logData = logData & " - BT_BankMessage" & BT_BankMessage & vbCrLf

                            BT_Token = tran.CreditCard.Token.ToString()
                            logData = logData & " - BT_Token" & BT_Token & vbCrLf

                        Catch ex As Exception
                            logData = logData & " - BT Transaction : throws exception:" & ex.ToString & vbCrLf
                            BT_Error = ex.ToString
                        End Try
                        lblMessage.ForeColor = Color.Blue
                        lblMessage.Text = "Success Details"
                        lblMessage.Text = (lblMessage.Text + ("<BR>Authorization Code : " + tran.ProcessorAuthorizationCode.ToString))
                    Else
                        logData = logData & " - BT Transaction : Failure " & vbCrLf
                        lblMessage.Text = "The credit card issuer has declined to process this transaction. Please log out and log back in, and use a different credit card for payment."
                        lbContinue.Enabled = True
                        If Not result Is Nothing Then
                            BT_result = result.Message.ToString
                            BT_Error = result.Message.ToString
                            logData = logData & " - BT Transaction : Failure(BT_Error): " & BT_Error & vbCrLf
                            If result.Target Is Nothing Then
                                BT_BankMessage = result.Message.ToString
                                logData = logData & " - BT Transaction : Failure(Target Err): " & result.Message.ToString & vbCrLf
                            Else
                                BT_BankMessage = result.Target.ProcessorResponseText.ToString()
                                BT_ETG_Response = result.Target.ProcessorAuthorizationCode.ToString()
                                logData = logData & " - BT Transaction : Failure(Target Res): " & BT_BankMessage & vbCrLf
                            End If
                            lblMessage.Text = lblMessage.Text & result.Message.ToString() & Request.Form("device_data")

                            Dim cvvCode As String = ""
                            Dim avsCode As String = ""
                            If Not result.Transaction Is Nothing Then
                                If Not result.Transaction.CvvResponseCode Is Nothing Then
                                    cvvCode = result.Transaction.CvvResponseCode.ToString()
                                    avsCode = result.Transaction.AvsPostalCodeResponseCode.ToString()
                                End If
                            End If
                            If cvvCode.Length > 0 Then
                                If cvvCode <> "M" Then
                                    NoCardIssue = False
                                    If avsCode <> "M" Then
                                        lblMessage.Text = "CVV Code and Postal code do not match. Please enter the correct codes."
                                    Else
                                        lblMessage.Text = "CVV Code does not match. Please enter the correct code."
                                    End If
                                ElseIf cvvCode = "M" And avsCode <> "M" Then
                                    NoCardIssue = False
                                    lblMessage.Text = "Postal Code does not match. Please enter the correct code."
                                End If
                            End If
                        Else
                            logData = logData & " - BT Transaction : Failure(Result object Err)" & vbCrLf
                        End If
                    End If

                Catch ex As Exception
                    BT_Error = ex.ToString
                    logData = logData & " - BT Transaction : Exception : " & ex.ToString & vbCrLf
                    lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in "
                    lblMessage.Text = lblMessage.Text & "<br>" & ex.Message.ToString()
                    lbContinue.Enabled = True
                End Try
lblInValidCustomer:
                If chkErrWithCustomerCreation = True Then
                    BT_BankMessage = statusReturnByCust
                End If
                If IsValidCustomer = False Or chkErrWithCustomerCreation = True Then
                    lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in, and use a different credit card for payment."
                    lblMessage.Text = lblMessage.Text & "<br>" & BT_BankMessage
                    lbContinue.Enabled = True
                End If
                PostUpdate()
                If BT_BankMessage = "Approved" Or (Not result Is Nothing And Not result.Target Is Nothing) Then
                    If BT_BankMessage = "Approved" Or result.Target.ProcessorResponseCode.ToString() = "1000" Then
                        logData = logData & " - BT Transaction : Processor responded"
                        'Store transaction data on Session and redirect                  
                        If (SavePaymentInfo() = True) Then
                            logData = logData & " - BT Transaction : Save payment"
                            WriteToErrorLog(logData, "", "Payment Success") 
                            Session("PaymentReference") = BT_Auth ' Trans ID - CustomerID
                            Session("R_Approved") = "APPROVED"
                            Session("DoPay") = Nothing
                            If Session("EventId") = 3 Then
                                Response.Redirect("Wrkshop_Success.aspx")
                            ElseIf Session("EventId") = 19 Then
                                Response.Redirect("PrepClub_Success.aspx")
                            ElseIf Session("EventID") = 20 Then
                                Response.Redirect("OnlineWkshop_Success.aspx")
                            ElseIf Session("EventId") = 13 Then
                                Response.Redirect("CoachingSuccess.aspx")
                            ElseIf Session("EventId") = 10 Then
                                Response.Redirect("SaleSuccess.aspx")
                            ElseIf Session("EventId") = 9 Then
                                Response.Redirect("FundRSuccess.aspx")
                            ElseIf Session("EventId") = 4 Then
                                Response.Redirect("reg_success_game.aspx")
                            Else
                                Response.Redirect("reg_Success_Final2.aspx")
                            End If
                        Else
                            logData = logData & " - BT Transaction : Err with NFG_Transactions"
                            lblMessage.Text = "Error while saving the payment information to NFG_TRANSACTIONS table, but the Credit Card Transaction is successful."
                            lbContinue.Enabled = True
                        End If
                    End If
                Else
                    Dim strEmail As String = ""
                    Dim MailBody As String = ""
                    If BT_BankMessage.ToLower() = "gateway rejected: fraud" Then
                        'validate fraud 
                        Dim strCmd As String = "select count(*) from ccsubmitlog where memberid=" & Session("CustIndId") & " and paymentdate between dateadd(dd,-5, getdate()) and getdate() and StatusReturn='Gateway Rejected: fraud'"
                        Dim iCnt As Integer = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd))
                        If (iCnt >= 3) Then
                            'send message to parentSend an email to <<EMAIL>>
                            Select Case Session("EventId")
                                Case 13, 20
                                    strEmail = "Please send an email to nsfprogramleads@gmail.com"
                                Case Else
                                    strEmail = "Please send an email to nsfcontests@gmail.com"
                            End Select
                            strCmd = "select Email,FirstName + ' ' + lastname as Name, HPhone,isnull(CPhone,'') CPhone from indspouse where automemberid=" & Session("CustIndId")
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strCmd)
                            Dim dr As DataRow = ds.Tables(0).Rows(0)
                            ' send mail to volunteer
                            MailBody = "Dear Volunteer, <br><br>Note : Found fraud rejections"
                            MailBody = MailBody & "<br><br>Parent Name: " & dr("Name") & " Email: " & dr("Email") & "<br>Home Phone: " & dr("HPone") & "<Br>Cell Phone: " & dr("CPhone")
                            MailBody = MailBody & "<br><br>This parent encountered three or more fraud rejections."
                            SendMessageToVolunteer("Encountered Froud Rejections", MailBody)
                        End If
                    End If
                    logData = logData & " - BT Transaction : Not Approved"
                    If NoCardIssue = True Then
                        lblMessage.Text = "We have encountered the following error while processing your credit card"
                        lblMessage.Text = (lblMessage.Text + ("<BR>Card Status:" + BT_BankMessage)) 'Approved
                        lblMessage.Text = (lblMessage.Text + "<BR>Please check card information and try again or try a different card")
                        lblMessage.Text = lblMessage.Text & strEmail
                    End If
                    tblError.Visible = True
                    Try
                        logData = logData & " - BT Transaction : Write log to CCError"
                        Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
                        Dim eventId As Integer = 0
                        Dim eventCode As String
                        If (Not Session("EventID") Is Nothing) Then
                            eventId = CInt(Session("EventID").ToString)
                        End If
                        eventCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", eventId))
                        Dim param(10) As SqlParameter
                        param(0) = New SqlParameter("@ChapterID", Session("ChapterId"))
                        param(1) = New SqlParameter("@EventID", Session("EventId"))
                        param(2) = New SqlParameter("@EventCode", eventCode)
                        param(3) = New SqlParameter("@EventYear", yr.ToString())
                        param(4) = New SqlParameter("@MemberID", Session("CustIndId"))
                        param(5) = New SqlParameter("@CardStatus", BT_BankMessage)
                        param(6) = New SqlParameter("@Error", BT_Error)
                        param(7) = New SqlParameter("@Message", BT_Message)
                        param(8) = New SqlParameter("@FraudCode", BT_ETG_Response)
                        param(9) = New SqlParameter("@CreateDate", Now())
                        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_CCError_Insert", param)
                        'write code Send email.
                        '  SendDasMessage("Error While processing your credit card", lblMessage.Text, "chitturi9@gmail.com")
                        lbContinue.Enabled = True
                    Catch
                    End Try
                End If
            Catch ex As Exception
                logData = logData & " - BT : Error"
                lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in, and use a different credit card for payment."
                lblMessage.Text = lblMessage.Text & "<br>" & BT_BankMessage
                lbContinue.Enabled = True
            End Try
            WriteToErrorLog(logData, "", "Payment Page")
        End Sub
        Public Sub WriteToErrorLog(ByVal msg As String, ByVal stkTrace As String, ByVal title As String)
            Try
                'check and make the directory if necessary; this is set to look in the application folder, you may wish to place the error log in 
                'another location depending upon the user's role and write access to different areas of the file system
                Dim path As String = Server.MapPath("~/Errors/")
                If Not System.IO.Directory.Exists(path) Then
                    System.IO.Directory.CreateDirectory(path)
                End If
                Dim fileName As String = Server.MapPath("~\Errors\Reg_Pay_log.txt")
                'check the file

                Dim sw As StreamWriter
                If (Not File.Exists(fileName)) Then
                    sw = New StreamWriter(fileName, True)
                Else
                    File.WriteAllText(fileName, "")
                    sw = File.AppendText(fileName)
                End If
                sw.Write("Title: " & title & vbCrLf)
                sw.Write("Message: " & msg & vbCrLf)
                sw.Write("StackTrace: " & stkTrace & vbCrLf)
                sw.Write("Date/Time: " & DateTime.Now.ToString() & vbCrLf)
                sw.Write("================================================" & vbCrLf)
                sw.Close()
                SendMessage(title, msg, fileName)
            Catch ex As Exception 
            End Try
        End Sub


        Private Sub SendMessageToVolunteer(ByVal sSubject As String, ByVal sBody As String)
            Dim EmailTo As String = New GlobalVariable().EmailAddress("contest") '"nsfcontests@northsouth.org"
            ''nsfprogramleads@northsouth.org
            Select Case Session("EventId")
                Case 13
                    EmailTo = New GlobalVariable().EmailAddress("coaching") '"nsfprogramleads@northsouth.org"
                Case 20
                    EmailTo = New GlobalVariable().EmailAddress("workshop") '"nsfprogramleads@northsouth.org"
            End Select
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress(New GlobalVariable().EmailAddress("contest")) '"nsfcontests@northsouth.org")
            email.To.Add(EmailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            Try
                client.Send(email)
            Catch ex As Exception
                ok = False
            End Try
        End Sub

        Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal strLogFile As String)
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress(New GlobalVariable().EmailAddress("contest")) '"nsfcontests@northsouth.org")
            email.To.Add("bindhu.rajalakshmi@capestart.com")
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(strLogFile)
            email.Attachments.Add(oAttch)
            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            Try
                client.Send(email)
            Catch ex As Exception
                ok = False
            End Try
        End Sub
        Private Function gDate() As String
            Dim dt As String
            dt = ddlMonth.SelectedValue.ToString() + Right(ddlYear.Text, 2)
            Return dt
        End Function
        Private Sub GetIPAddress(ByRef ilbl As Label)
            Dim IPAdd As String = String.Empty
            Try
                IPAdd = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                If String.IsNullOrEmpty(IPAdd) Then
                    IPAdd = Request.ServerVariables("REMOTE_ADDR")
                End If

                ilbl.Text = IPAdd.ToString
            Catch ex As Exception
            End Try
        End Sub

        Private Function SavePaymentInfo() As Boolean
            'save to NFG transactions
            Dim sb As StringBuilder = New StringBuilder
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim eventId As Integer = 0
            Dim eventCode As String
            Dim parentLName As String = ""
            Dim parentFName As String = ""
            Dim parentEmail As String = ""
            Dim DonorType As String = ""
            Dim NFG_Flag As Boolean = False
            Dim DonationType As String = ""
            Semester = ""
            'Dim loginEmail As String = ""
            Dim custIndId As String = DBNull.Value.ToString
            Dim ds As DataSet
            If (Not Session("EventID") Is Nothing) Then
                eventId = CInt(Session("EventID").ToString)
            End If
            eventCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", eventId))
            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If
            Try
                If Session("EventId") = 9 Then
                    If Session("FundDonorType") = "OWN" Then
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select ORGANIZATION_NAME as firstName,email from OrganizationInfo where automemberId = " + custIndId)
                        If (ds.Tables.Count > 0) Then
                            If (ds.Tables(0).Rows.Count > 0) Then
                                parentLName = ""
                                parentFName = ds.Tables(0).Rows(0)("firstname")
                                If ds.Tables(0).Rows(0)("email").ToString().Trim.Length > 0 Then parentEmail = ds.Tables(0).Rows(0)("email").ToString().Trim
                                DonorType = "OWN"
                            End If
                        End If
                    Else
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstName,lastName,email from indspouse where automemberId = " + custIndId)
                        If (ds.Tables.Count > 0) Then
                            If (ds.Tables(0).Rows.Count > 0) Then
                                parentLName = ds.Tables(0).Rows(0)("lastname")
                                parentFName = ds.Tables(0).Rows(0)("firstname")
                                parentEmail = ds.Tables(0).Rows(0)("email")
                                DonorType = "IND"
                            End If
                        End If
                    End If
                Else
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstName,lastName,email from indspouse where automemberId = " + custIndId)
                    If (ds.Tables.Count > 0) Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            parentLName = ds.Tables(0).Rows(0)("lastname")
                            parentFName = ds.Tables(0).Rows(0)("firstname")
                            parentEmail = ds.Tables(0).Rows(0)("email")
                            DonorType = "IND"
                        End If
                    End If
                End If
            Catch ex As Exception
                Exit Function
            End Try

            If (nDonationAmt > 0) Then
                'save to donations info
                sb.Append(" declare @donationnumber int ")
                sb.Append(" SELECT @donationnumber = MAX(DonationNumber) + 1 ")
                sb.Append(" FROM DonationsInfo WHERE [MEMBERID]=<PARENTID> ")
                sb.Append(" ")
                sb.Append("  if @donationnumber is null ")
                sb.Append("	begin ")
                sb.Append(" set @donationnumber = 1 ")
                sb.Append("	end ")
                sb.Append(" else ")
                sb.Append(" begin ")
                sb.Append(" set @donationnumber = @donationnumber + 1  ")
                sb.Append("	end ")
                sb.Append(" INSERT INTO DonationsInfo([MEMBERID], [DonationNumber], [DonorType],  ")
                sb.Append(" [AMOUNT], [TRANSACTION_NUMBER], [Anonymous], [DonationDate], [METHOD],  ")
                sb.Append(" [PURPOSE], [EVENT], [STATUS], [CreateDate], [DeletedFlag], ")
                sb.Append(" [EVENTID],[EVENTYEAR],[CHAPTERID],[TAXDEDUCTION],[BankID],[DonationType]) ")
                sb.Append(" VALUES(<PARENTID>, @donationnumber, 'IND',  ")
                sb.Append(" <DONATIONAMOUNT>, '<PAYMENTREFERENCE>','No', getdate(), 'Credit Card',  ")
                sb.Append(" '<DONATIONPURPOSE>', 'Registration', 'Completed', getdate(), 'No', ")
                sb.Append(" <EVENTID>, '<EVENTYEAR>', <CHAPTERID>, <TAXDEDUCTION>,1,'Unrestricted') ")
            End If
            'save to Contestants
            Try
                If Session("EventId") = 4 Then
                    If Session("GameIDS") Is Nothing Then
                        Response.Redirect("SummarySelections_Game.aspx")
                    End If
                    sb.Append(" ")
                    sb.Append(" UPDATE GAME ")
                    sb.Append(" SET [PaymentReference]='<PAYMENTREFERENCE>', ")
                    sb.Append(" [Approved] ='Y', ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentDate] =getdate(), ")
                    sb.Append(" [startdate] = getdate(), ")
                    sb.Append(" [EndDate] = dateadd(year,+1,getdate()), ")
                    sb.Append(" [ModifyDate] = getdate(), ")
                    sb.Append(" [ModifiedBy] = " & Session("LoginID"))
                    sb.Append(" WHERE [GameID] IN (" & Session("GameIDS") & ")")
                    NFG_Flag = True
                ElseIf Session("EventId") = 3 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE Registration ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" and EventDate  >= GetDate() And  Eventyear >= " & yr)
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From Registration WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND EVENTID=" & Session("EventId") & " and EventDate  >= GetDate() And  Eventyear >= " & Year(Today()) & " ") > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 19 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE Registration_PrepClub ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [Approved] ='Y', ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>',ModifiedBy=" & Session("LoginID") & ",ModifyDate=GETDATE() ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" and EventDate  >= GetDate() And  Eventyear >= " & yr)

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From Registration_PrepClub WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND EVENTID=" & Session("EventId") & " and EventDate  >= GetDate() And  Eventyear >= " & Year(Today()) & " ") > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventID") = 20 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE Registration_OnlineWkshop ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [Approved] ='Y', ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>',ModifiedBy=" & Session("LoginID") & ",ModifyDate=GETDATE() ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" and EventDate  >= GetDate() And  Eventyear >= " & yr)

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From Registration_OnlineWkshop WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND EVENTID=" & Session("EventId") & " and EventDate  >= GetDate() And  Eventyear >= " & Year(Today()) & " ") > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 12 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE WMSponsor ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>', ")
                    sb.Append(" [PaidAmount]=<TOTALAMOUNT> ")
                    sb.Append(" WHERE [WMSponsorID]=" & Session("WMSponsorID") & "")

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From WMSponsor WHERE [WMSponsorID]=" & Session("WMSponsorID") & " and PaymentReference is null") > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 13 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE CR ")
                    sb.Append(" SET CR.[PaymentDate]=getdate(),  ")
                    sb.Append(" CR.[PaymentMode]='Credit Card',  ")
                    sb.Append(" CR.[PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" CR.[PaymentReference]='<PAYMENTREFERENCE>', CR.Approved='Y' ")
                    sb.Append(" FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID and CR.Level = C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo   WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=<PARENTID> ") ' and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null)'Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
                    sb.Append(" AND CR.EVENTID=" & Session("EventId"))
                    sb.Append(" And  CR.Eventyear >= " & yr)
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID and  CR.Level =C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" & custIndId & "   AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & yr) > 0 Then 'and GETDATE()< C.Enddate '((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) ''Updated on 09-12-2014 as Level is newly added for UV simialr to SAT
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 10 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE SaleTran ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" And  Eventyear >= " & yr)
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From SaleTran WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & "  AND EVENTID=" & Session("EventId") & " And  Eventyear >= " & yr) > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 9 Then
                    DonationType = "Credit Card"
                    sb.Append(" ")
                    sb.Append(" UPDATE FundRReg ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID"))
                    sb.Append(" AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "'")
                    sb.Append(" And Eventyear <= " & yr)
                    sb.Append(" And ProductCode not in ('adult','child')")
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From FundRReg WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID") & " AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "' And  Eventyear <= " & yr) > 0 Then
                        NFG_Flag = True
                    End If
                    sb.Append("; UPDATE BeeRankSponsor ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID"))
                    sb.Append(" AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "'")
                    sb.Append(" And  Eventyear >= " & Year(Today()))
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From BeeRankSponsor  WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID") & "  AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "' And  Eventyear >= " & yr) > 0 Then
                        NFG_Flag = True
                    End If
                    sb.Append("; UPDATE FundRContestReg ")
                    sb.Append(" SET Approved='Y',  ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> AND FundRCalID =" & Session("FundRCalID"))
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" And  Eventyear = " & yr)
                Else
                    sb.Append(" ")
                    sb.Append(" UPDATE C ")
                    sb.Append(" SET C.[PaymentDate]=getdate(),  ")
                    sb.Append(" C.[PaymentMode]='Credit Card',  ")
                    sb.Append(" C.[PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" C.[PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" From Contestant C INNER JOIN Contest b ON b.ContestID = C.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=b.Contest_Year and Ex.EventID=b.EventId and Ex.ChildNumber=C.ChildNumber")
                    sb.Append(" WHERE C.BadgeNumber is null and C.[PaymentReference] Is NULL And C.[ParentID]= <PARENTID> ")
                    sb.Append(" And  C.Contestyear >= " & yr)
                    sb.Append(" AND C.EVENTID=" & Session("EventId"))
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From  Contestant C INNER JOIN Contest b ON b.ContestID = C.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=b.Contest_Year and Ex.EventID=b.EventId and Ex.ChildNumber=C.ChildNumber WHERE C.[PaymentReference] Is NULL and C.BadgeNumber is null And C.[ParentID]= " & custIndId & "  And  C.Contestyear >= " & yr) > 0 Then
                        NFG_Flag = True
                    End If
                End If

                'save meals charge.
                If (eventId = 1) Then
                    sb.Append(" ")
                    sb.Append(" UPDATE MealCharge ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [AutoMemberID]=<PARENTID>  ")
                    sb.Append(" And  Contestyear >= " & Year(Today()))
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From MealCharge  WHERE [PaymentReference] is NULL and [AutoMemberID]=" & custIndId & "  And  Contestyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                End If
                If NFG_Flag = True Then
                    sb.Append(" INSERT INTO NFG_TRANSACTIONS([Last Name], ")
                    sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
                    sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
                    sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
                    sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear,")
                    sb.Append(" PaymentNotes,Brand,DonorType,Discount,[Donation Type], Semester")
                    sb.Append(" )  VALUES ( '<LASTNAME>', ")
                    sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
                    sb.Append(" GETDATE(), 'NSF', <DONATIONAMOUNT>, GETDATE(), ")
                    sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', <MEALSAMOUNT>, <LATEFEE>, ")
                    sb.Append(" <EVENTID>,'<EVENTFOR>','<DONATIONPURPOSE>',<PARENTID>,<CHAPTERID>,<REGFEE>,<TOTALAMOUNT>,'<EVENTYEAR>',")
                    sb.Append(" '<COMMENTS>','<Brand>','" & DonorType & "',<Discount>,'" & DonationType & "','<SEMESTER>')")
                Else
                    sb.Append(" INSERT INTO PaymentError([Last Name], ")
                    sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
                    sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
                    sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
                    sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear,PaymentNotes,DonorType")
                    sb.Append(" )  VALUES ( '<LASTNAME>', ")
                    sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
                    sb.Append(" GETDATE(), 'NSF', <DONATIONAMOUNT>, GETDATE(), ")
                    sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', <MEALSAMOUNT>, <LATEFEE>, ")
                    sb.Append(" <EVENTID>,'<EVENTFOR>','<DONATIONPURPOSE>',<PARENTID>,<CHAPTERID>,<REGFEE>,<TOTALAMOUNT>,'<EVENTYEAR>','<COMMENTS>','" & DonorType & "')")
                End If
            Catch ex As Exception
            End Try
            'save to contest charity
            sb.Append(" ")
            sb.Append(" UPDATE Contest_Charity ")
            sb.Append(" SET [PaymentDate]=getdate(),  ")
            sb.Append(" [PaymentMode]='Credit Card', ")
            sb.Append(" [PaymentNotes]='<COMMENTS>', ")
            sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
            sb.Append(" WHERE [PaymentReference] is NULL and [MEMBERID]=<PARENTID>  ")

            'now replace all the parameters with actual values
            sb.Replace("<PARENTID>", custIndId)
            sb.Replace("<LASTNAME>", parentLName.Replace("'", "''"))
            sb.Replace("<FIRSTNAME>", parentFName.Replace("'", "''"))
            sb.Replace("<ADDRESS>", (txtAddress1.Text.Trim + (" " + txtAddress2.Text.Trim)).Replace("'", "''"))
            'sb.Replace("<ADDRESS>", (baddr1.Trim + (" " + baddr2.Trim)).Replace("'", "''"))
            'sb.Replace("<CITY>", bcity.Trim.Replace("'", "''"))
            'sb.Replace("<STATE>", bstate.Replace("'", "''"))
            'sb.Replace("<ZIP>", bzip)
            sb.Replace("<CITY>", txtCity.Text.Replace("'", "''"))
            sb.Replace("<STATE>", ddlState.SelectedValue.ToString.Replace("'", "''"))
            sb.Replace("<ZIP>", txtZip.Text.Replace("'", "''"))
            sb.Replace("<PARENTEMAIL>", parentEmail.Replace("'", "''"))
            sb.Replace("<REGFEE>", nRegFee.ToString)
            sb.Replace("<DONATIONAMOUNT>", nDonationAmt.ToString)
            'sb.Replace("<PAYMENTREFERENCE>", R_OrderNum)
            'sb.Replace("<APPROVAL_CODE>", R_Code)
            'sb.Replace("<APPROVAL_STATUS>", R_Approved)
            sb.Replace("<PAYMENTREFERENCE>", BT_Auth)
            sb.Replace("<APPROVAL_CODE>", Cust_id)
            sb.Replace("<APPROVAL_STATUS>", BT_BankMessage.Replace("'", "''"))
            sb.Replace("<STATUS>", "Complete")
            sb.Replace("<COMMENTS>", comments.Replace("'", "''"))
            sb.Replace("<DONATIONPURPOSE>", Session("DONATIONFOR").ToString)
            sb.Replace("<MEALSAMOUNT>", nMealsAmt.ToString)
            sb.Replace("<LATEFEE>", nLateFee.ToString)
            sb.Replace("<EVENTID>", eventId)
            sb.Replace("<EVENTFOR>", eventCode)
            sb.Replace("<EVENTYEAR>", yr.ToString)
            sb.Replace("<CHAPTERID>", Session("CustIndChapterID").ToString) 'coming from reg_donate page.
            sb.Replace("<TAXDEDUCTION>", CType(100, String))
            sb.Replace("<TOTALAMOUNT>", nTotalAmt.ToString)
            sb.Replace("<Brand>", BT_Brand)
            sb.Replace("<Discount>", nDisAmt.ToString)
            sb.Replace("<SEMESTER>", Semester.ToString)
            Dim saved As Boolean = True
            Dim test As String = sb.ToString
            'lbltest.Text = test
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sb.ToString)
            Catch se As SqlException
                SendDasMessage("NSF - Error In Payment", sb.ToString() + "<br> Error <br>" + se.ToString(), "bindhu.rajalakshmi@capestart.com")
                lblMessage.Text = "The following error occured while saving the payment information.Please contact for technical support" & _
                "."
                lblMessage.Text = (lblMessage.Text) ' + se.Message)
                tblError.Visible = True
                saved = False
            End Try
            Return saved
        End Function

        Private Sub lbRegistration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbRegistration.Click
            Dim redirectURL As String
            If Session("entryToken") = "Volunteer" Then
                redirectURL = "VolunteerFunctions.aspx"
            ElseIf Session("EventId") = 9 And Session("entryToken") = "Parent" Then
                ' redirectURL = "UserFunctions.aspx"
                redirectURL = "FundRReg.aspx"
            ElseIf Session("EventId") = 9 And Session("entryToken") = "Donor" Then
                'redirectURL = "DonorFunctions.aspx"
                redirectURL = "FundRReg.aspx"
            Else
                redirectURL = "UserFunctions.aspx"
            End If
            Response.Redirect(redirectURL)
        End Sub
        Private Sub SendDasMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress(New GlobalVariable().EmailAddress("contest")) '"nsfcontests@northsouth.org"nsfcontests@gmail.com
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))
            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            Try
                client.Send(email)
            Catch e As Exception
                'lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End Sub
        Private Sub PreUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim cAddress As String
            Dim eventId As Integer = 0
            Dim ccL As String
            Dim ccF As String
            Dim pName As String = System.IO.Path.GetFileNameWithoutExtension(Request.Path)
            Dim sqlparams(28) As SqlClient.SqlParameter

            ccL = Right(txtCardNumber.Text, 4)
            ccF = Left(txtCardNumber.Text, 4)
            If (Not Session("EventID") Is Nothing) Then
                eventId = CInt(Session("EventID").ToString)
            End If

            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If
            cAddress = txtAddress1.Text + "|" + txtZip.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + ddlCountry.SelectedValue.ToString
            sqlparams(0) = New SqlParameter("@MemberID", SqlDbType.Int)
            sqlparams(0).Value = CInt(custIndId.ToString)
            sqlparams(1) = New SqlParameter("@EventYear", SqlDbType.Int)
            sqlparams(1).Value = CInt(yr.ToString)
            sqlparams(2) = New SqlParameter("@EventID", SqlDbType.Int)
            sqlparams(2).Value = CInt(eventId.ToString)
            sqlparams(3) = New SqlParameter("@AspxPage", SqlDbType.VarChar)
            sqlparams(3).Value = pName.ToString
            sqlparams(4) = New SqlParameter("@PaymentDate", SqlDbType.SmallDateTime)
            sqlparams(4).Value = System.DateTime.Now.ToString
            sqlparams(5) = New SqlParameter("@IPAddress", SqlDbType.VarChar)
            sqlparams(5).Value = lblIP.Text
            sqlparams(6) = New SqlParameter("@TransType", SqlDbType.VarChar)
            sqlparams(6).Value = "00"
            sqlparams(7) = New SqlParameter("@Platform", SqlDbType.VarChar)
            sqlparams(7).Value = BT_Platform
            sqlparams(8) = New SqlParameter("@StatusSend", SqlDbType.VarChar)
            sqlparams(8).Value = "Sent"
            sqlparams(9) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(9).Value = DBNull.Value
            sqlparams(10) = New SqlParameter("@Amount", SqlDbType.Float)
            sqlparams(10).Value = nTotalAmt.ToString("f2")
            sqlparams(11) = New SqlParameter("@Fee", SqlDbType.Float)
            sqlparams(11).Value = nRegFee.ToString
            sqlparams(12) = New SqlParameter("@LateFee", SqlDbType.Float)
            sqlparams(12).Value = nLateFee.ToString
            sqlparams(13) = New SqlParameter("@Meal", SqlDbType.Float)
            sqlparams(13).Value = nMealsAmt.ToString
            sqlparams(14) = New SqlParameter("@Donation", SqlDbType.Float)
            sqlparams(14).Value = nDonationAmt.ToString("f2")
            sqlparams(15) = New SqlParameter("@CCF4", SqlDbType.VarChar)
            sqlparams(15).Value = ccF.ToString
            sqlparams(16) = New SqlParameter("@CCL4", SqlDbType.VarChar)
            sqlparams(16).Value = ccL.ToString
            sqlparams(17) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(17).Value = System.DBNull.Value
            sqlparams(18) = New SqlParameter("@CCName", SqlDbType.VarChar)
            sqlparams(18).Value = txtCardHolderName.Text
            sqlparams(19) = New SqlParameter("@CCZip", SqlDbType.VarChar)
            sqlparams(19).Value = txtZip.Text
            sqlparams(20) = New SqlParameter("@ClientEmail", SqlDbType.VarChar)
            sqlparams(20).Value = lblmail.Text
            sqlparams(21) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar)
            sqlparams(21).Value = comments.ToString()
            sqlparams(22) = New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)
            sqlparams(22).Value = System.DateTime.Now.ToString
            sqlparams(23) = New SqlParameter("@CreatedBy", SqlDbType.Int)
            sqlparams(23).Value = CInt(custIndId.ToString)
            sqlparams(24) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(24).Value = System.DateTime.Now.ToString
            sqlparams(25) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(25).Value = custIndId.ToString
            sqlparams(26) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(26).Direction = ParameterDirection.Output
            sqlparams(27) = New SqlParameter("@Discount", SqlDbType.Float)
            sqlparams(27).Value = nDisAmt.ToString("f2")
            sqlparams(28) = New SqlParameter("@Semester", SqlDbType.VarChar)
            sqlparams(28).Value = Semester.ToString()
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertCCSubmitLog", sqlparams)
                Cust_id = sqlparams(26).Value.ToString()
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support" & _
              "."
                lblMessage.Text = (lblMessage.Text + ex.Message)
                tblError.Visible = True
            End Try
        End Sub

        Private Sub PostUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim sqlparams(8) As SqlClient.SqlParameter
            sqlparams(0) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(0).Value = BT_BankMessage
            sqlparams(1) = New SqlParameter("@ETG_Response", SqlDbType.VarChar)
            sqlparams(1).Value = BT_ETG_Response
            sqlparams(2) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(2).Value = BT_Auth
            sqlparams(3) = New SqlParameter("@Token", SqlDbType.VarChar)
            sqlparams(3).Value = BT_Token
            sqlparams(4) = New SqlParameter("@Brand", SqlDbType.VarChar)
            sqlparams(4).Value = BT_Brand
            sqlparams(5) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(5).Value = System.DateTime.Now
            sqlparams(6) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(6).Value = custIndId
            sqlparams(7) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(7).Value = Cust_id.ToString
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_UpdateCCSubmitLog", sqlparams)
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support" & _
              "."
                lblMessage.Text = (lblMessage.Text) ' + ex.Message)
                tblError.Visible = True
            End Try
        End Sub
        Sub GenerateClientToken()
            Try
                InitializeBT()
                'ClientToken = gateway.ClientToken.generate()
            Catch ex As Exception

            End Try
        End Sub
        Sub InitializeBT()
            Dim BT As NameValueCollection = CType(ConfigurationManager.GetSection("BT/crd"), NameValueCollection)
            Dim domainDir As String = Request.Url.AbsolutePath.ToString().ToLower
            If domainDir.Contains("app9/") = True Then
                gateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, BT("MerchantId"), BT("PublicKey"), BT("PrivateKey"))
            Else
                gateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
            End If
            BT_Platform = BT("Platform")
        End Sub
        Private Sub SuccessRedirect()
            Session("DoPay") = Nothing
            If Session("R_Approved") = "APPROVED" Then
                If Session("EventId") = 3 Then
                    Response.Redirect("Wrkshop_Success.aspx")
                ElseIf Session("EventId") = 19 Then
                    Response.Redirect("PrepClub_Success.aspx")
                ElseIf Session("EventId") = 20 Then
                    Response.Redirect("OnlineWkshop_Success.aspx")
                ElseIf Session("EventId") = 13 Then
                    Response.Redirect("CoachingSuccess.aspx")
                ElseIf Session("EventId") = 10 Then
                    Response.Redirect("SaleSuccess.aspx")
                ElseIf Session("EventId") = 9 Then
                    Response.Redirect("FundRSuccess.aspx")
                ElseIf Session("EventId") = 4 Then
                    Response.Redirect("reg_success_game.aspx")
                Else
                    Response.Redirect("reg_Success_Final2.aspx")
                End If
            End If
        End Sub
        Private Sub RecurrentPay()
            Dim comment As String = comments.ToString

            Dim SQLStrS As String = "SELECT COUNT(*) FROM CCSubmitLog C where  MemberID =" & Session("LoginId").ToString() & "  and DATEDIFF(S, PaymentDate, GETDATE())<= 60  and StatusReturn ='Approved' and PaymentNotes ='" & comment & "'"
            Dim PayR As Integer = Convert.ToInt16(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLStrS))
            If PayR > 0 Then
                SuccessRedirect()
            End If
        End Sub
        Private Function PreviouslyPaid() As Boolean
            Dim comment As String = comments.ToString
            'Dim SQLStr As String = "SELECT COUNT(*) FROM CCSubmitLog C where  MemberID =" & Session("LoginId").ToString() & "  and DATEDIFF(MI, PaymentDate, GETDATE())< 20  and CONVERT(DATE,PaymentDate) = CONVERT(DATE,GETDATE()) and StatusReturn ='Approved' and PaymentNotes ='" & comment & "'"
            Dim SQLStr As String = "SELECT COUNT(*) FROM CCSubmitLog C where  MemberID =" & Session("LoginId").ToString() & "  and DATEDIFF(M, PaymentDate, GETDATE())<= 6  and StatusReturn ='Approved' and PaymentNotes ='" & comment & "'"
            Dim PayCount As Integer = Convert.ToInt16(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLStr))
            If PayCount > 0 Then
                Return True
            Else
                Return False
            End If
        End Function
    End Class
End Namespace


