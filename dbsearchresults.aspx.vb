Imports System.Web.Configuration
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class dbsearchresults
    Inherits System.Web.UI.Page

    Dim sSQL, sSQLORG As String
    Dim dataAdapterInd, dataAdapterOrg As Data.SqlClient.SqlDataAdapter
    Dim dsInd, dsOrg As New Data.DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If LCase(Session("LoggedIn")) <> "true" Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If
        'If Len(Trim("" & Session("LoginID"))) = 0 Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If
        'If Len(Trim("" & Session("entryToken"))) = 0 Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If

        sSQL = Session("sSQL")
        sSQLORG = Session("sSQLORG")
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        'Response.End()
        If (Not IsPostBack) Then

            Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
            dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
            Try
                dataAdapterInd.Fill(dsInd)
                gvIndSpouse.DataSource = dsInd.Tables(0)
                gvIndSpouse.DataBind()

            Catch ex As Exception
                'LblMessage.Text = ex.Message
            End Try

            Dim dataCommandOrg As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQLORG, dataConnection)
            dataAdapterOrg = New Data.SqlClient.SqlDataAdapter(dataCommandOrg)

            Try
                dataAdapterOrg.Fill(dsOrg)
                gvOrg.DataSource = dsOrg.Tables(0)
                gvOrg.DataBind()
            Catch ex As Exception
                'LblMessage.Text = ex.Message
            End Try

            If dsInd.Tables.Count > 0 Then
                If dsInd.Tables(0).Rows.Count < 1 Then
                    LblMessage.Text = "No matching records found!"
                End If
            ElseIf dsOrg.Tables.Count > 0 Then
                If dsOrg.Tables(0).Rows.Count < 1 Then
                    LblMessage.Text = "No matching records found!"
                End If
            Else
                Response.Redirect("DBSearch.aspx")
            End If
            End If
        ''If (Session("RoleID") = 1) Or (Session("RoleID") = 84) Or (Session("RoleID") = 85) Or (Session("RoleID") = 37) Or (Session("RoleID") = 38) Then
        ''    Dim strSQl1 As String = "select 'National' from Volunteer where RoleID = " & Session("RoleID") & " and memberid = " & Session("LoginID")
        ''    strSQl1 = Replace(strSQl1, "'", """")
        ''    Dim ds As DataSet = SqlHelper.ExecuteDataset(dataConnection, CommandType.Text, strSQl1)
        ''    If ds.Tables(0).Rows.Count < 1 Then
        ''        gvIndSpouse.Columns(2).Visible = False
        ''        gvOrg.Columns(2).Visible = False
        ''    End If
        ''Else
        ''    gvIndSpouse.Columns(2).Visible = False
        ''    gvOrg.Columns(2).Visible = False
        ''End If

        If Session("RoleID") = 1 Or (Session("RoleID") = 84) Or (Session("RoleID") = 85) Or (Session("RoleID") = 37) Or (Session("RoleID") = 38 Or Session("RoleID") = 92) Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From Volunteer Where [National]='Y' and MemberID=" & Session("LoginID") & " ") > 0 Then
                If Not Session("RoleID") = 1 Then
                    lblgvMsg.Visible = True
                End If
            Else
                gvIndSpouse.Columns(2).Visible = False
                gvOrg.Columns(2).Visible = False
                lblgvMsg.Visible = False
            End If
        Else
            gvIndSpouse.Columns(2).Visible = False
            gvOrg.Columns(2).Visible = False
            lblgvMsg.Visible = False
        End If

    End Sub

    Protected Sub gvOrg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOrg.PageIndexChanging
        gvOrg.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQLORG, dataConnection)
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvOrg.DataSource = dsInd.Tables(0)
            gvOrg.DataBind()
        Catch ex As Exception
            LblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvIndSpouse_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvIndSpouse.RowDataBound
        Select Case e.Row.RowType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim hlnkDonation As HyperLink = CType(e.Row.Cells(2).FindControl("hlnkDonations"), HyperLink)
                If Session("RoleID") = "5" Then
                    hlnkDonation.Enabled = False
                ElseIf Len(Session("LoginChapterID")) > 0 Then
                    hlnkDonation.Enabled = False
                End If
                Dim strDonorType As String = e.Row.DataItem("DONORTYPE").ToString()
                If strDonorType.Trim.ToLower = "spouse" And Not Session("RoleID") = "1" Then
                    hlnkDonation.Enabled = False
                End If

                hlnkDonation.NavigateUrl = "~/ViewDonations.aspx?Id=" & e.Row.Cells(0).Text & "&type=IND"
        End Select
    End Sub

    Protected Sub gvOrg_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvOrg.RowDataBound
        Select Case e.Row.RowType
            Case ListItemType.Item, ListItemType.AlternatingItem
                Dim hlnkOrgDonations As HyperLink = CType(e.Row.Cells(2).FindControl("hlnkOrgDonations"), HyperLink)
                If Session("RoleID") = "5" Then
                    hlnkOrgDonations.Enabled = False
                ElseIf Len(Session("LoginChapterID")) > 0 Then
                    hlnkOrgDonations.Enabled = False
                End If
                hlnkOrgDonations.NavigateUrl = "~/ViewDonations.aspx?Id=" & e.Row.Cells(0).Text & "&type=OWN"
        End Select
    End Sub

    Protected Sub gvIndSpouse_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIndSpouse.PageIndexChanging
        gvIndSpouse.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvIndSpouse.DataSource = dsInd.Tables(0)
            gvIndSpouse.DataBind()
        Catch ex As Exception
            LblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub HyperLink1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles HyperLink1.Click
        Session.Remove("GetIndSpouseID")
        Session.Remove("CustIndID")
        Session.Remove("GetSpouseID")
        Session.Remove("CustSpouseID")
        Response.Redirect("Registration.aspx?Type=Individual")
    End Sub

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=SearchResults_INDSpouse.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Session("sSQL"))
        If ds.Tables(0).Rows.Count > 0 Then
            ds.Tables(0).Columns.RemoveAt(0)
            ds.Tables(0).Columns.RemoveAt(1) '("CustINDID").ColumnMapping = MappingType.Hidden
            dgexport.DataSource = ds
            dgexport.DataBind()
        End If
        dgexport.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Public Overloads Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)

        ' Verifies that the control is rendered
    End Sub

 
    Protected Sub BtnExport_Org_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport_Org.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=SearchResults_Organization.xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        
        dgexport.RenderControl(htmlWrite)

        gvOrg.AllowPaging = False
        gvOrg.Columns(1).Visible = False
        gvOrg.Columns(2).Visible = False
        gvOrg.HeaderStyle.ForeColor = Color.Black
        gvOrg.RenderControl(htmlWrite)

        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
End Class
