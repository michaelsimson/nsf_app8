<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddOrganization.aspx.vb" Inherits="AddOrganization" title="Add Organization" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table id="tblMain" width="90%" align="center">
				<tr bgcolor="#FFFFFF"  align="middle" >
					<td class="Heading" colSpan="2">Add Organization</td>
				</tr>
				<tr>
					<td width="30%">
						<table id="tblIndividual" align="center">						
				<tr bgcolor="#FFFFFF">
			        <td colspan="2">
				        <asp:hyperlink  CssClass="btn_02" id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp;
				        <asp:hyperlink  CssClass="btn_02" id="hlnkSearch" Visible=false runat="server" NavigateUrl="~/dbsearchresults.aspx">Back to Search Results</asp:hyperlink>
				       </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Organization Name:</td>
					<td noWrap align="left" >
					<asp:textbox id="txtOrgName" runat="server" CssClass="inputBox" MaxLength="75" Width="300px"></asp:textbox><asp:requiredfieldvalidator id="rfvFirstName" runat="server" ErrorMessage="Enter Valid First Name" Display="Dynamic"
							ControlToValidate="txtOrgName"></asp:requiredfieldvalidator>
					</td>					
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Contact Person Title:</td>
					<td noWrap align="left" >
					    <asp:DropDownList runat="server" ID="ddlTitle" CssClass="inputBox">
					        <asp:ListItem Text="Mr." Value="Mr."></asp:ListItem>
					        <asp:ListItem Text="Mrs." Value="Mrs."></asp:ListItem>
					        <asp:ListItem Text="Ms." Value="Ms."></asp:ListItem>
					        <asp:ListItem Text="Dr." Value="Dr."></asp:ListItem>
					        <asp:ListItem Text="Prof." Value="Prof."></asp:ListItem>
					    </asp:DropDownList>					    
					 </td>
				</tr>		
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >First Name:</td>
					<td noWrap align="left" ><asp:textbox id="txtFirstName" runat="server" CssClass="inputBox" MaxLength="35"></asp:textbox>
                        </td>					
				</tr>		
				<tr bgcolor="#FFFFFF" id="trinv1" runat = "server" visible = "false" >
					<td  noWrap align="right" >Middle&nbsp;Initial:</td>
					<td noWrap align="left" ><asp:textbox id="txtMiddleName" runat="server" CssClass="inputBox" MaxLength="25"></asp:textbox></td>
					
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Last Name:</td>
					<td noWrap align="left" ><asp:textbox id="txtLastName" runat="server" CssClass="inputBox" MaxLength="35"></asp:textbox>
                        </td>					
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Address1:</td>
					<td noWrap align="left" ><asp:textbox id="txtAddress1" runat="server" CssClass="inputBox" MaxLength="60"></asp:textbox><asp:requiredfieldvalidator id="rfvLastName" runat="server" ErrorMessage="Enter Valid Address" Display="Dynamic"
							ControlToValidate="txtAddress1"></asp:requiredfieldvalidator></td>
				</tr>
				<tr bgcolor="#FFFFFF"  id="trinv2" runat = "server" visible = "false" >
					<td  noWrap align="right" >Address2:</td>
					<td noWrap align="left" ><asp:textbox id="txtAddress2" runat="server" CssClass="inputBox"></asp:textbox>
				    </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right">City:</td>
					<td noWrap align="left"><asp:textbox id="txtCity" runat="server" CssClass="inputBox"></asp:textbox>
					<asp:requiredfieldvalidator id="rfvBirthDate" runat="server" ErrorMessage="Enter Valid City"
							Display="Dynamic" ControlToValidate="txtCity"></asp:requiredfieldvalidator>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" style="height: 24px" >State:</td>
					<td noWrap align="left" style="height: 24px" ><asp:dropdownlist id="ddlState" runat="server" CssClass="inputBox">							
					    </asp:dropdownlist>&nbsp;
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server" ControlToValidate="ddlState"
                            ErrorMessage="Select Valid State" InitialValue="0"></asp:RequiredFieldValidator></td>
				</tr>				
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Zip/Postal Code:</td>
					<td noWrap align="left" ><asp:textbox id="txtZip" runat="server" CssClass="inputBox" ></asp:textbox><asp:requiredfieldvalidator id="rfvSchoolName" runat="server" ErrorMessage="Enter Valid Zip Code" Display="Dynamic"
							ControlToValidate="txtZip"></asp:requiredfieldvalidator>
                        <asp:RangeValidator ID="RangeValidator1" runat="server" ErrorMessage="Enter Numeric & Five Digits only" ControlToValidate="txtZip" MaximumValue="99999" MinimumValue="0" Type="Integer"></asp:RangeValidator>
							
							</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right"  height="21">Country: </td>
					<td noWrap align="left"  height="21"><asp:dropdownlist AutoPostBack="true" id="ddlCountry" runat="server" CssClass="inputBox">
							<asp:ListItem Value="0">Select Country</asp:ListItem>
							<asp:ListItem Value="United States"  Selected="True">United States</asp:ListItem>							
							<asp:ListItem Value="Canada">Canada</asp:ListItem>	
							<asp:ListItem Value="India">India</asp:ListItem>						
						</asp:dropdownlist><asp:requiredfieldvalidator id="rfvCountryofBirth" runat="server" ErrorMessage="Select  Valid Country"
							Display="Dynamic" ControlToValidate="ddlCountry" InitialValue="0"></asp:requiredfieldvalidator></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Phone Number:</td>
					<td noWrap align="left" ><asp:textbox id="txtPhone" runat="server" CssClass="inputBox"  MaxLength="20"	></asp:textbox>
                        <asp:regularexpressionvalidator id="revWorkPhoneInd" runat="server" ControlToValidate="txtPhone" Display="Dynamic"
										ErrorMessage="Phone No should be in xxx-xxx-xxxx format" ValidationExpression="((\(\d{3}\) ?)|(\d{3}-))?\d{3}-\d{4}"></asp:regularexpressionvalidator></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Fax Number:</td>
					<td noWrap align="left" ><asp:textbox id="txtFax" runat="server" CssClass="inputBox"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >E-mail:</td>
					<td noWrap align="left" ><asp:textbox id="txtEmail" runat="server" CssClass="inputBox" MaxLength="30" Width="250px"  ></asp:textbox>
                        </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Secondary Email:</td>
					<td noWrap align="left" ><asp:textbox id="txtEmail2" runat="server" CssClass="inputBox" MaxLength="30" Width="250px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Matching Gift:</td>
					<td noWrap align="left" >
					    <asp:DropDownList runat="server" ID="ddlMatchingGift" CssClass="inputBox">
					        <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					        <asp:ListItem Text="No" Value="No" Selected ></asp:ListItem>
					    </asp:DropDownList>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Send Email:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlSendMail" CssClass="inputBox">
					    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected ></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Referred By:</td>
					<td noWrap align="left" ><asp:textbox id="txtReferredBy" runat="server" CssClass="inputBox"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" style="height: 35px" >NSF Chapter:</td>
					<td noWrap align="left" style="height: 35px" >
					  <asp:DropDownList runat="server" ID="ddlNSFChapter" CssClass="inputBox">					    
					  </asp:DropDownList>
                        <asp:RequiredFieldValidator ID="RequiredFieldValidator6" runat="server" ControlToValidate="ddlNSFChapter"
                            ErrorMessage="Select Valid Chapter" InitialValue="0"></asp:RequiredFieldValidator></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Send News Letter:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" CssClass="inputBox" ID="ddlSendNewsLetter">
					    <asp:ListItem Text="Yes" Value="Yes" ></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected=true></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Generate Mailing Label:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlMailingLabel" CssClass="inputBox">
					    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected=true></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Send Receipt:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlSendReceipt" CssClass="inputBox">
					    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected=true></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Liaison Person:</td>
					<td noWrap align="left" ><asp:textbox id="txtLiasonPerson" runat="server" CssClass="inputBox"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >IRS Cat:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlIRSCat" CssClass="inputBox">
	                        <asp:ListItem Text="Profit" Value="Profit"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,501(c)(3)" Value="Non-proft,501(c)(3)"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,PAC" Value="Non-proft,PAC"></asp:ListItem>
			                <asp:ListItem Text="Non-proft,Other" Value="Non-proft,Other"></asp:ListItem>				  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Organization Type:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlBusinessType" CssClass="inputBox">
					   <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>
					    <asp:ListItem Text="NRI,Business" Value="NRI,Business"></asp:ListItem>
					    <asp:ListItem Text="NRI, Ethnic" Value="NRI, Ethnic"></asp:ListItem>
					    <asp:ListItem Text="NRI, Professional" Value="NRI, Professional"></asp:ListItem>
					    <asp:ListItem Text="NRI, Other" Value="NRI, Other"></asp:ListItem>
					    <asp:ListItem Text="Press, India" Value="Press, India"></asp:ListItem>
					    <asp:ListItem Text="Press, NRI" Value="Press, NRI"></asp:ListItem>
					    <asp:ListItem Text="Press, US" Value="Press, US"></asp:ListItem>
					    <asp:ListItem Text="Religious, Temples" Value="Religious, Temples"></asp:ListItem>
					    <asp:ListItem Text="Religious, Other" Value="Religious, Other"></asp:ListItem>
					    <asp:ListItem Text="University_College_School" Value="University_College_School"></asp:ListItem>
					    <asp:ListItem Text="Corporation_LLC" Value="Corporation_LLC"></asp:ListItem>
					    <asp:ListItem Text="United Way" Value="United Way"></asp:ListItem>
					    <asp:ListItem Text="Other Non-Profit" Value="Other Non-Profit"></asp:ListItem>
					    <asp:ListItem Text="Other" Value="Other"></asp:ListItem>
					  </asp:DropDownList>
					  &nbsp;<asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server" 
                            ControlToValidate="ddlBusinessType" ErrorMessage="Select Organization Type" 
                            InitialValue="0" SetFocusOnError="True"></asp:RequiredFieldValidator></td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Venue:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlVenue" CssClass="inputBox">
					    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected=true></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Sponsor:</td>
					<td noWrap align="left" >
					  <asp:DropDownList runat="server" ID="ddlSponsor" CssClass="inputBox">
					    <asp:ListItem Text="Yes" Value="Yes"></asp:ListItem>
					    <asp:ListItem Text="No" Value="No" Selected=true></asp:ListItem>
					  </asp:DropDownList>
					  </td>
				</tr>
				<tr bgcolor="#FFFFFF">
					<td  noWrap align="right" >Website:</td>
					<td noWrap align="left" ><asp:textbox id="txtWebsite" runat="server" CssClass="inputBox" MaxLength="50" Width="250px"  ></asp:textbox></td>
				</tr>

				<tr bgcolor="#FFFFFF" id="TrGiftURL" runat="server">
					<td  noWrap align="right" >Matching_Gift_URL:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiFtURL" runat="server" CssClass="inputBox" MaxLength="75" Width="400px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftAccNo" runat="server">
					<td  noWrap align="right" >Matching_Gift_Account_Number:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftAccNo" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftUserID" runat="server">
					<td  noWrap align="right" >Matching_Gift_User_Id:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftUserId" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
				<tr bgcolor="#FFFFFF" id="TrGiftPwd" runat="server">
					<td  noWrap align="right" >Matching_Gift_Password:</td>
					<td noWrap align="left" ><asp:textbox id="txtGiftPwd" runat="server" CssClass="inputBox" MaxLength="50" Width="125px"  ></asp:textbox></td>
				</tr>
				
					<tr bgcolor="#FFFFFF">
					<td class="ItemCenter" align="center" colSpan="2"><asp:button id="btnSubmit" runat="server" CssClass="FormButton" Text="Save" OnClick="btnSubmit_Click"></asp:button>
					</td>
				</tr>
				<tr bgcolor="#FFFFFF">
				    <td colspan="2" align="center" >
				        <asp:Label runat="server" ID="lblMessage" CssClass="red"></asp:Label>
				    </td>
				</tr>			
			</table>
			</td> 
			</tr> 
		</table>
</asp:Content>

