﻿<%@ Page Language="VB"   EnableEventValidation="false"  %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
    Public dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs)
        loadgrid(GVList)
    End Sub
    
    Private Sub loadgrid(ByVal gv As GridView)
        Dim StrSQl As String = "select * from Ind_AluminiScholars"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            gv.DataSource = ds
            gv.DataBind()
            btnExport.Visible = True
            lblErr.Text = ""
        Else
            btnExport.Visible = False
            lblErr.Text = "No data found"
        End If
    End Sub
    

    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim gvtemp As New GridView
        loadgrid(gvtemp)
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=AlumniRegistration_" & Now.Year.ToString() & ".xls")
        Response.Charset = ""
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        gvtemp.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
</script>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>NSF scholar Alumni Registration Report</title>
</head>
<body>
    <form id="form1" runat="server">
    <center><h2>NSF scholar Alumni Registration Report</h2></center>
   <%-- <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp;  --%>      <asp:Button ID="btnExport" runat="server" Text="Export to Excel" Visible="false"  OnClick="BtnExport_Click" Width="160px" />
<br /><br />
    <div align="center">
        <asp:GridView ID="GVList" runat="server" BackColor="White" AutoGenerateColumns = "True" 
            BorderColor="#999999" BorderStyle="None" BorderWidth="1px" CellPadding="3" 
            GridLines="Vertical">
            <RowStyle BackColor="#EEEEEE" ForeColor="Black" />
            <FooterStyle BackColor="#CCCCCC" ForeColor="Black" />
            <PagerStyle BackColor="#999999" ForeColor="Black" HorizontalAlign="Center" />
            <SelectedRowStyle BackColor="#008A8C" Font-Bold="True" ForeColor="White" />
            <HeaderStyle BackColor="#000084" Font-Bold="True" ForeColor="White" />
            <AlternatingRowStyle BackColor="#DCDCDC" />
             <Columns>
                               
             </Columns> 
    
        </asp:GridView>
        <asp:Label ID="lblErr" runat="server" ForeColor = "Red" ></asp:Label>
    </div>
    </form>
</body>
</html>
