﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NPOI.XSSF.UserModel
Imports System.IO
Imports System.Net.Mail

'usp_GetContestantsforScoreShee is used in this page
Partial Class ManageScoresheet
    Inherits System.Web.UI.Page
    Dim Uploadflag As Boolean = False

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("RoleID") = 1
        'Session("LoginID") = 4240

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        lbldwError.Text = ""
        lblErr.Text = ""
        LblMasterErr.Text = ""
        Session("DnFlag") = False

        If Not IsPostBack() Then
            If Session("RoleID") = 9 Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from Volunteer where RoleId=9 and [National]='Y' and MemberID = " & Session("LoginID")) > 0 Then
                    ExamRecNational.Value = "Y"
                Else
                    ExamRecNational.Value = "N"
                End If
            Else
                ExamRecNational.Value = "N"
            End If
            If ExamRecNational.Value = "N" And Not Request.QueryString("id") Is Nothing Then
                'Response.Write("select V.ChapterID, Ch.ChapterCode from Volunteer V inner Join Chapter Ch ON V.ChapterID= Ch.ChapterID  where  V. MemberID = " & Session("LoginID") & " AND V.VolunteerId =" & Request.QueryString("id") & "")
                Dim Reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select V.ChapterID, Ch.ChapterCode from Volunteer V inner Join Chapter Ch ON V.ChapterID= Ch.ChapterID  where  V. MemberID = " & Session("LoginID") & " AND V.VolunteerId =" & Request.QueryString("id") & "")
                While Reader.Read()
                    lblChapter.Text = Reader("ChapterCode")
                    lblChapter.Visible = True
                    hdnChapterID.Value = " AND C.NSFChapterID = " & Reader("ChapterID")
                End While
            End If

            ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
            ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
            ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
            ddlYear.Items(1).Selected = True

            ddlMYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
            ddlMYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
            ddlMYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
            ddlMYear.Items(1).Selected = True

            Try
                getevent()
                'getName_Role_Products()
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try

            If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 93 Then
                BtnScoreDetExport.Visible = True
            Else
                BtnScoreDetExport.Visible = False
            End If
            If (Session("RoleID") = 16 Or (Session("RoleID") = 18)) Then
                checkContestTeamScheduleByRoleID()
            End If
        End If
    End Sub
    Private Sub getName_Role_Products()
        Try
            Dim StrSQL_Name As String = "Select FirstName + ' ' + Lastname as Name from IndSpouse where AutoMemberID = " & Session("LoginID")
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL_Name)

            Dim RoleCode As String = (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Top 1 Name from Role where RoleID=" & Session("RoleID")))
            If ds.Tables(0).Rows.Count > 0 Then
                lblLoginRole_Produts.Text = ds.Tables(0).Rows(0)("Name") & " logged in as " & RoleCode
            End If

            Dim StrSQL As String = ""

            If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
                StrSQL = "Select  Distinct P.ProductCode,P.ProductID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll And P.EventID = " & ddlContest.SelectedValue & hdnChapterID.Value & " Order By P.ProductId "
            ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then ' Or Session("RoleID") = 16 Or Session("RoleID") = 18
                StrSQL = "Select Distinct P.ProductCode,P.ProductID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll AND P.EventID=" & ddlContest.SelectedValue & " Order By P.ProductId"
            ElseIf Session("RoleID") = 16 Or Session("RoleID") = 18 Or ExamRecNational.Value = "Y" Then
                StrSQL = " Select Distinct P.ProductCode,P.ProductID,CT.Phase from Product P "
                StrSQL = StrSQL & " Inner Join ContestTeamschedule CT On CT.ProductGroupId = p.ProductGroupId and CT.ProductId = p.ProductId "
                StrSQL = StrSQL & " Inner Join RoomSchedule R On R.ContestYear=CT.ContestYear and R.EventId=CT.EventID and R.ChapterID=CT.ChapterID and R.ProductGroupId=CT.ProductGroupID and R.ProductId=CT.ProductID and R.Phase=CT.Phase and  R.RoomNumber =CT.RoomNumber  and R.SeqNo=CT.SeqNo and R.BldgID=CT.BldgID"
                StrSQL = StrSQL & " Inner join contest C  on C.Contest_Year = CT.ContestYear and C.EventId=CT.EventID and C.NSFChapterID=CT.ChapterID and C.ProductGroupId=CT.ProductGroupID and C.ProductId=CT.ProductID"
                StrSQL = StrSQL & " Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID "
                StrSQL = StrSQL & " Left Join Contestant Cn ON Cn.ContestID = C.ContestID  "
                StrSQL = StrSQL & " where C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not Null and  C.NSFChapterID=" & ddlChapter.SelectedValue
                StrSQL = StrSQL & " AND P.EventID= " & ddlContest.SelectedValue & hdnChapterID.Value
                If Session("RoleID") = 16 Then
                    StrSQL = StrSQL & " and CT.CJMemberId in(" & Session("LoginID") & ")"
                ElseIf Session("RoleID") = 18 Then
                    StrSQL = StrSQL & " and CT.LTJudMemberId in(" & Session("LoginID") & ") "
                End If
                StrSQL = StrSQL & " Order By P.ProductId "
            ElseIf Session("RoleID") = 93 Then
                StrSQL = "select Distinct P.ProductCode from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID Left Join NatTechTeam N on N.ProductGroupCode = C.ProductGroupCode where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & " and N.NTTMemberID=" & Session("LoginID") & " Order By P.ProductId"
            End If

            Dim Prds As String = ""
            Dim dsPrds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If dsPrds.Tables(0).Rows.Count > 0 Then
                For i As Integer = 0 To dsPrds.Tables(0).Rows.Count - 1
                    If (Session("RoleID") = 16 Or Session("RoleID") = 18) Then
                        Prds = Prds & dsPrds.Tables(0).Rows(i)("ProductCode") & " Phase " & dsPrds.Tables(0).Rows(i)("Phase") & ","
                    Else
                        Prds = Prds & dsPrds.Tables(0).Rows(i)("ProductCode") & ","
                    End If

                Next
                Prds = Prds.TrimEnd(",")
            End If

            lblLoginRole_Produts.Text = lblLoginRole_Produts.Text & " for products " & Prds
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub getMPhase()
        Try


            ddlMPhase.Items.Clear()
            If ddlMContest.SelectedValue = 1 Then
                ddlMPhase.Items.Insert(0, New ListItem("Global", "0"))
                'ddlMPhase.Items.Insert(1, New ListItem("Phase 1", "1"))
                ddlMPhase.Items.Insert(1, New ListItem("Phase 2", "2"))
                ddlMPhase.Items.Insert(2, New ListItem("Phase 3", "3"))
                ddlMPhase.Items.Insert(3, New ListItem("p3List", "4"))
                ddlMPhase.Items.Insert(4, New ListItem("Toppers", "5"))
            Else
                ddlMPhase.Items.Insert(0, New ListItem("Global", "0"))
                'ddlMPhase.Items.Insert(1, New ListItem("Phase 1", "1"))
                ddlMPhase.Items.Insert(1, New ListItem("Phase 2", "2"))
                ddlMPhase.Items.Insert(2, New ListItem("Toppers", "5"))
            End If
            If (ddlMProductGroup.SelectedValue = "SB" Or ddlMProductGroup.SelectedValue = "VB" Or ddlMProductGroup.SelectedValue = "GB") Then
                ddlMPhase.SelectedIndex = ddlMPhase.Items.IndexOf(ddlMPhase.Items.FindByValue("1"))
                ddlMPhase.Enabled = True
            Else
                ddlMPhase.SelectedIndex = ddlMPhase.Items.IndexOf(ddlMPhase.Items.FindByValue("1"))
                If ddlMContest.SelectedValue = 2 Then
                    ddlMPhase.Items(1).Enabled = False
                Else
                    ddlMPhase.Items(1).Enabled = False
                    ddlMPhase.Items(2).Enabled = False
                    ddlMPhase.Items(3).Enabled = False
                End If
                ddlMPhase.Enabled = True 'False
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub getevent()
        Try


            Dim strSQl As String = ""
            If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
                strSQl = " from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & hdnChapterID.Value '" and Cn.BadgeNumber is Not NUll" &
            ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
                loadMproductGroup()
                strSQl = "   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not Null"
            ElseIf Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                strSQl = "  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & hdnChapterID.Value & " and Cn.BadgeNumber is Not Null"
            ElseIf Session("RoleID") = 93 Then
                strSQl = " from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId Left Join NatTechTeam N on N.ProductGroupCode =C.ProductGroupCode Left Join Contestant Cn ON Cn.ContestID = C.ContestID and Cn.ContestYear = " & ddlYear.SelectedValue & " where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.EventId = " & ddlContest.SelectedValue & " and N.NTTMemberID=" & Session("LoginID") & ""
            End If
            If strSQl <> "" Then
                Dim Evntcnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(Distinct C.EventId) " & strSQl)
                If Evntcnt = 1 Then
                    Dim eventid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Distinct C.EventId " & strSQl)
                    ddlContest.SelectedIndex = ddlContest.Items.IndexOf(ddlContest.Items.FindByValue(eventid))
                    ddlContest.Enabled = False 'True
                ElseIf Evntcnt > 1 Then
                    Dim eventid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select Distinct C.EventId " & strSQl)
                    ddlContest.SelectedIndex = ddlContest.Items.IndexOf(ddlContest.Items.FindByValue(eventid))
                    ddlContest.Enabled = True
                ElseIf Evntcnt = 0 And (Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N")) Then
                    ddlContest.Enabled = False
                    lbldwError.Text = "You were not yet scheduled for any contest."
                    Exit Sub
                End If
                LoadContests()
            Else
                lbldwError.Text = "You are not authorized to use this application."
                Exit Sub
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadContests()
        Try


            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            Dim strSQl As String
            If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
                ddlYear.Enabled = False
                strSQl = "select  Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID and Cn.ContestYear = " & ddlYear.SelectedValue & " where C.examrecid=" & Session("loginid") & " and C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll  and C.EventId = " & ddlContest.SelectedValue & hdnChapterID.Value & " order by Ch.State,Ch.Name"
            ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
                tblMaster.Visible = True
                strSQl = "select Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID and Cn.ContestYear = " & ddlYear.SelectedValue & " where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.EventId = " & ddlContest.SelectedValue & " order by Ch.State,Ch.Name"
            ElseIf Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                strSQl = "select Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID and Cn.ContestYear = " & ddlYear.SelectedValue & " where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.EventId = " & ddlContest.SelectedValue & hdnChapterID.Value & " order by Ch.State,Ch.Name"
            ElseIf Session("RoleID") = 93 Then
                strSQl = "select Distinct Ch.ChapterID, Ch.ChapterCode,Ch.State,Ch.Name   from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId Left Join NatTechTeam N on N.ProductGroupCode =C.ProductGroupCode Left Join Contestant Cn ON Cn.ContestID = C.ContestID and Cn.ContestYear = " & ddlYear.SelectedValue & " where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.EventId = " & ddlContest.SelectedValue & "  and N.NTTMemberID=" & Session("LoginID") & " order by Ch.State,Ch.Name"
            Else
                lbldwError.Text = "You are not authorized to use this application."
                Exit Sub
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)

            If ds.Tables(0).Rows.Count = 1 Then
                ddlChapter.DataSource = ds
                ddlChapter.DataBind()
                ddlChapter.Enabled = False
                If ddlContest.SelectedValue = 1 Then LoadPhase3()
                BtnDownload.Enabled = True
                loadproductGroup()
            ElseIf ds.Tables(0).Rows.Count > 0 Then
                BtnDownload.Enabled = True
                ddlChapter.Enabled = True
                ddlChapter.Items.Clear()
                Dim li As ListItem
                If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                    Dim i As Integer
                    For i = 0 To ds.Tables(0).Rows.Count - 1
                        li = New ListItem()
                        li.Text = ds.Tables(0).Rows(i)(1).ToString()
                        li.Value = ds.Tables(0).Rows(i)(0).ToString()
                        ddlChapter.Items.Add(li)
                    Next i
                    ddlChapter.Items.Insert(0, New ListItem("Select", "0"))
                    ddlChapter.SelectedIndex = 0
                Else
                    ddlChapter.DataSource = ds
                    ddlChapter.DataBind()
                    ddlChapter.Items.Insert(0, New ListItem("Select", "0"))
                    ddlChapter.SelectedIndex = 0
                End If
                loadproductGroup()
            Else
                BtnDownload.Enabled = False
                lbldwError.Text = "No badge numbers are available"
            End If
            'Added on 17-06-2014 to Show the Products assigned for the login Volunteer
            getName_Role_Products()
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlContest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlContest.SelectedIndexChanged
        LoadContests()
        LoadPhase3()
    End Sub
    Private Sub LoadPhase3()
        TrConfirmDwnload.Visible = False
        Dim i As Integer
        If ddlContest.SelectedValue = 1 Then
            For i = 4 To 6
                ddlPhase.Items(i).Enabled = True
            Next
            ddlTypeofData1.Items(3).Enabled = True
            ddlTypeofData1.Items(4).Enabled = True
        ElseIf ddlContest.SelectedValue = 2 Then
            For i = 4 To 6 '8
                ddlPhase.Items(i).Enabled = False
            Next
            ddlTypeofData1.Items(3).Enabled = False
            ddlTypeofData1.Items(4).Enabled = False
        End If
    End Sub
    Protected Sub ddlMContest_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadMproductGroup()
    End Sub

    Private Sub loadproductGroup()
        Try
            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            TrConfirmDwnload.Visible = False
            Dim strSQl As String
            Dim ds As DataSet
            If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
                strSQl = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join ProductGroup P On C.ProductGroupId = p.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & hdnChapterID.Value & " Order BY P.ProductGroupID "
                ddlYear.Enabled = False
            ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
                strSQl = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join ProductGroup P On C.ProductGroupId = p.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & " Order BY P.ProductGroupID "
            ElseIf Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                strSQl = "select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID from ContestTeamschedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID Inner Join ProductGroup P on C.ProductGroupId = P.ProductGroupId WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue '& " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ")"
                If Session("RoleID") = 16 Then
                    strSQl = strSQl & " and  C.CJMemberId in(" & Session("LoginID") & ")"
                ElseIf Session("RoleID") = 18 Then
                    strSQl = strSQl & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
                End If
            ElseIf Session("RoleID") = 93 Then
                strSQl = "Select Distinct P.ProductGroupCode,P.Name,P.ProductGroupID From Contest C Inner Join Chapter Ch on NSFChapterId=Ch.ChapterID Inner Join ProductGroup P on C.ProductGroupId = P.ProductGroupId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID Left Join NatTechTeam N on N.ProductGroupCode=C.ProductGroupCode where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " AND P.EventID=" & ddlContest.SelectedValue & " and N.NTTMemberID in (" & Session("LoginID") & ") Order BY P.ProductGroupID "
            End If

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)

            If ds.Tables(0).Rows.Count = 1 Then
                ddlProductGroup.DataSource = ds
                ddlProductGroup.DataBind()
                LoadProduct()
                ddlProductGroup.Enabled = False
            ElseIf ds.Tables(0).Rows.Count > 0 Then
                ddlProductGroup.DataSource = ds
                ddlProductGroup.DataBind()
                ddlProductGroup.Enabled = True
                LoadProduct()
            Else
                ddlProductGroup.Items.Clear()
                ddlProduct.Items.Clear()
                ddlProductGroup.Items.Insert(0, New ListItem("", ""))
                ddlProduct.Items.Insert(0, New ListItem("", 0))

                ddlProductGroup.Enabled = False
                ddlProduct.Enabled = False
                TrPhase.Visible = False
                TrRoom.Visible = False
                TrTopRank.Visible = False
                BtnDownload.Enabled = False
                trupload.Visible = False
                FileUpload.Visible = False
                TrTypeofData1.Visible = False
                TrRoom1.Visible = False
                btnUpload.Visible = False
                lbldwError.Text = " No ProductGroups Assigned."
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub loadMproductGroup()
        Try
            LblMasterErr.Text = ""
            Dim strSQl As String
            Dim ds As DataSet
            If Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then
                strSQl = "select Distinct ProductGroupCode,Name,ProductGroupID  from ProductGroup WHERE EventID=" & ddlMContest.SelectedValue & " Order BY ProductGroupID" 'C.examrecid=" & Session("loginid") & " and
                tblMaster.Visible = True
            End If
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlMProductGroup.DataSource = ds
                ddlMProductGroup.DataBind()
                ddlMProductGroup.Enabled = True
                LoadMProduct()
            Else
                ddlMProductGroup.DataSource = Nothing
                ddlMProductGroup.DataBind()
                ddlMProduct.DataSource = Nothing
                ddlMProduct.DataBind()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub LoadProduct()
        Try
            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            TrConfirmDwnload.Visible = False
            'Dim Products As String = ""
            Dim strSQl As String
            If Session("RoleID") = 33 Or (Session("RoleID") = 9 And ExamRecNational.Value = "N") Then
                strSQl = "Select  Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where C.examrecid=" & Session("loginid") & " and  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "'  And P.EventID = " & ddlContest.SelectedValue & hdnChapterID.Value & " Order By P.ProductId "
                ddlYear.Enabled = False
            ElseIf Session("RoleID") = 1 Or Session("RoleID") = 2 Or ExamRecNational.Value = "Y" Then ' Or Session("RoleID") = 16 Or Session("RoleID") = 18
                strSQl = "Select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' AND P.EventID=" & ddlContest.SelectedValue & " Order By P.ProductId"
            ElseIf Session("RoleID") = 16 Or Session("RoleID") = 18 Or ExamRecNational.Value = "Y" Then
                'strSQl = "Select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID   where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' AND P.EventID=" & ddlContest.SelectedValue & hdnChapterID.Value & " Order By P.ProductId"
                strSQl = " Select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from Product P "
                strSQl = strSQl & " Inner Join ContestTeamschedule CT On CT.ProductGroupId = p.ProductGroupId and CT.ProductId = p.ProductId "
                strSQl = strSQl & " Inner Join RoomSchedule R On R.ContestYear=CT.ContestYear and R.EventId=CT.EventID and R.ChapterID=CT.ChapterID and R.ProductGroupId=CT.ProductGroupID and R.ProductId=CT.ProductID and R.Phase=CT.Phase and  R.RoomNumber =CT.RoomNumber  and R.SeqNo=CT.SeqNo and R.BldgID=CT.BldgID"
                strSQl = strSQl & " Inner join contest C  on C.Contest_Year = CT.ContestYear and C.EventId=CT.EventID and C.NSFChapterID=CT.ChapterID and C.ProductGroupId=CT.ProductGroupID and C.ProductId=CT.ProductID"
                strSQl = strSQl & " Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID "
                strSQl = strSQl & " Left Join Contestant Cn ON Cn.ContestID = C.ContestID  "
                strSQl = strSQl & " where C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not Null  and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and  P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' "
                strSQl = strSQl & " AND P.EventID= " & ddlContest.SelectedValue & hdnChapterID.Value

                If Session("RoleID") = 16 Then
                    strSQl = strSQl & " and  CT.CJMemberId in(" & Session("LoginID") & ")"
                ElseIf Session("RoleID") = 18 Then
                    strSQl = strSQl & " and CT.LTJudMemberId in(" & Session("LoginID") & ") "
                End If
                strSQl = strSQl & " Order By P.ProductId "
            ElseIf Session("RoleID") = 93 Then
                strSQl = "select Distinct P.ProductCode,P.Name,P.ProductId,C.ContestID  from contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId  Left Join Contestant Cn ON Cn.ContestID = C.ContestID Left Join NatTechTeam N on N.ProductGroupCode = C.ProductGroupCode where  C.Contest_year= " & ddlYear.SelectedValue & " and Cn.BadgeNumber is Not NUll and  C.NSFChapterID=" & ddlChapter.SelectedValue & " and P.ProductGroupCode='" & ddlProductGroup.SelectedItem.Value & "' AND P.EventID=" & ddlContest.SelectedValue & " and N.NTTMemberID=" & Session("LoginID") & " Order By P.ProductId"
            Else
                lbldwError.Text = "Sorry, You have no access"
                Exit Sub
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQl)
            If ds.Tables(0).Rows.Count = 1 Then
                ddlProduct.DataSource = ds
                ddlProduct.DataBind()
                checkscorehsheetdown()
                ddlProduct.Enabled = False
            ElseIf ds.Tables(0).Rows.Count > 0 Then
                ddlProduct.Enabled = True
                ddlProduct.DataSource = ds
                ddlProduct.DataBind()
                checkscorehsheetdown()

            Else
                ddlProduct.Items.Clear()
                ddlProduct.Enabled = False
                lbldwError.Text = " No Product Assigned."
            End If

            CheckVolChildContests(ddlProduct.SelectedValue)
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub CheckVolChildContests(ByVal ContestIDs As Integer)
        '*********** Added on 02-05-2013 to filter out the Contests in which the children of the Volunteer(TC) are Participating *********'
        Dim Products As String = ""
        Try
            If ContestIDs.ToString() <> "" Then
                Dim ds_products As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct ProductCode,ContestCode from Contestant where parentid=" & Session("LoginID") & " and ContestYear=" & Now.Year() & " and ContestCode in(" & ContestIDs & ")")
                If ds_products.Tables(0).Rows.Count > 0 Then
                    If ddlProduct.SelectedValue = ds_products.Tables(0).Rows(0)("ContestCode") Then
                        Products = ddlProduct.SelectedItem.Text 'ddlProduct.Items(i).Enabled = False
                    End If
                Else
                    Products = ""
                End If
            End If
            If Products <> "" Then
                lblPrdError.Text = "Your  child(ren) is(are) in the contest " & Products & ".So you cannot access this Contest."
                BtnDownload.Enabled = False
                btnUpload.Enabled = False
            Else
                lblPrdError.Text = ""
                BtnDownload.Enabled = True
                btnUpload.Enabled = True
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Private Sub LoadMProduct()
        Try

            lblMdwError.Text = ""
            LblMasterErr.Text = ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct ProductCode,Name from product where Eventid in (" & ddlMContest.SelectedValue & ") and Status='O' and ProductGroupCode='" & ddlMProductGroup.SelectedItem.Value & "'")
            getMPhase()
            If ds.Tables(0).Rows.Count = 1 Then
                ddlMProduct.DataSource = ds
                ddlMProduct.DataBind()
                ddlMProduct.Enabled = False
            ElseIf ds.Tables(0).Rows.Count > 0 Then
                ddlMProduct.Enabled = True
                ddlMProduct.DataSource = ds
                ddlMProduct.DataBind()
            Else
                ddlMProduct.Items.Clear()
                ddlMProduct.Enabled = False
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub BtnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnDownload.Click
        Try


            If ddlProduct.Items.Count > 0 Then
                lblUploadCondn.Text = ""
                lblDownLdError.Text = ""
                If TrPhase.Visible = True Then
                    If ddlPhase.SelectedValue = "10" Then
                        Try
                            Dim str As String = "SELECT count(*) FROM   dbo.Child d  INNER JOIN dbo.Contestant c ON d.MEMBERID = c.ParentID AND d.ChildNumber = c.ChildNumber " &
                                                               " INNER JOIN dbo.Contest b ON c.ContestCode = b.ContestID AND c.ChapterID = b.NSFChapterID INNER JOIN Chapter e ON b.NSFChapterID = e.Chapterid" &
                                                               " inner join scoredetail s on s.contestid=b.contestid and s.childnumber=d.childnumber and s.chapterid=c.chapterid " &
                                                               " WHERE(c.PaymentReference Is Not NULL Or c.OrderNo Is Not null) And c.BadgeNumber Is Not null and p1_p2_rank is not null And b.ContestID =" & ddlProduct.SelectedValue
                            If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, str) = 0) Then
                                lbldwError.Text = "You have to upload ranks using 'Ranks after Phase 1 & 2' to download 'Phase 1 & 2 Contestants in Ranks'."
                                Exit Sub
                            End If
                        Catch ex As Exception

                        End Try

                    End If
                End If
                filldata(False)
                ' To enable upload button
                checkscorehsheetdown()
            Else
                lbldwError.Text = "You have no valid scoresheet to download"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub filldata(ByVal P3flag As Boolean)
        Try


            lbldwError.Text = ""
            Dim extension As String = ".xlsx"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheet", New SqlParameter("@ContestID", ddlProduct.SelectedValue))
            If ds.Tables(0).Rows.Count > 0 Then
                Dim SrcFilename, FileName, ProductCode, ChapterCode As String
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select DISTINCT C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
                Dim SQLInsertPhase1 As String = ""
                Dim SQLScoreSheetLog As String = ""
                Dim SQLScoreSheetLogVal As String = ""
                Dim SQLRoomSch As String = ""
                Dim PhaseDFlag As String = ""
                Dim PhaseDValue As String = ""

                While readr.Read()
                    SrcFilename = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet"
                    FileName = SrcFilename & "_" & readr("Chaptercode")
                    If ddlPhase.SelectedValue = 2 Or ddlPhase.SelectedValue = 3 Then
                        SrcFilename = SrcFilename & "_p2"
                        FileName = FileName & "_p2"

                        If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                            'SrcFilename = SrcFilename & "_" & GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)
                            FileName = FileName & "_" & GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)
                        Else
                            If Not ddlRoom.SelectedValue = "ALL" Then
                                FileName = FileName & "_" & ddlRoom.SelectedValue
                            End If
                        End If
                    ElseIf ddlPhase.SelectedValue = 4 Or ddlPhase.SelectedValue = 10 Then 'Or ddlPhase.SelectedValue = "10" Summary Data
                        ' FileName = FileName & "_SummaryData" Then
                        FileName = FileName & "_p12"
                    ElseIf ddlPhase.SelectedValue = 5 Then
                        SrcFilename = SrcFilename & "_p3"
                        FileName = FileName & "_p3"
                    ElseIf ddlPhase.SelectedValue = 6 Then
                        FileName = FileName & "_p123"
                    ElseIf (ddlPhase.SelectedValue = "7") Then
                        SrcFilename = SrcFilename & "_Toppers"
                        FileName = FileName & "_Toppers"
                    ElseIf (ddlPhase.SelectedValue = "9") Then
                        SrcFilename = SrcFilename & "_p3List"
                        FileName = FileName & "_p3List"
                    ElseIf (ddlPhase.SelectedValue = "8") Then
                        FileName = FileName & "_Rank_Certificates"
                    End If
                    ProductCode = readr("ProductCode")
                    ChapterCode = readr("Chaptercode")
                End While
                readr.Close()

                Session("FileName") = FileName
                Dim fName As String = Server.MapPath("ScoreSheets\Master\" & SrcFilename & extension)
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(fName) '-- if the file exists on the server
                If Not file.Exists Then
                    extension = ".xls"
                    fName = Server.MapPath("ScoreSheets\Master\" & SrcFilename & ".xls")
                    file = New System.IO.FileInfo(fName)
                End If

                If Not file.Exists Then
                    lbldwError.Text = "Sorry Master file doesnot exist"
                    Exit Sub
                End If
                Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(fName)
                Dim Sheet1 As IWorksheet
                Sheet1 = xlWorkBook.Worksheets(1)
                Dim x As Integer
                Dim i As Integer = 7

                SQLScoreSheetLog = "Insert into ScoreSheetLog (ContestID,ContestYear,FileName,EventID,Event,ChapterID,Chapter,ProductGroupID,ProductID,ProductGroupCode ,ProductCode,PhaseID,Phase,CreatedBy,CreatedDate,"
                SQLScoreSheetLogVal = ") Values(" & ddlProduct.SelectedValue & "," & ddlYear.SelectedValue & ",'" & FileName & "'," & ddlContest.SelectedValue & ",'" & ddlContest.SelectedItem.Text & "'," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "',(Select DISTINCT ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductGroupCode from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductCode from Contest Where ContestID=" & ddlProduct.SelectedValue & ")," & ddlPhase.SelectedValue & ",'" & ddlPhase.SelectedItem.Text & "'," & Session("LoginID") & ",GETDATE(),"

                If (ddlPhase.SelectedValue = "7") Or (ddlPhase.SelectedValue = "8") Or (ddlPhase.SelectedValue = "9") Then
                    GoTo Phase3List
                End If

                Dim str As IRange = Sheet1.Range(5, 28)
                Dim PWD As String = str.Value.ToString()
                Dim InsertFlag As Boolean = False
                Dim Score1, score2, score3, rank, Phase3TB, PhaseValue, Attendance As Integer

                If Sheet1.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                    lbldwError.Text = "Level Mismatch in Sheet"
                    Exit Sub
                End If

                Score1 = Sheet1.Range(6, 28).Value
                score2 = Sheet1.Range(7, 28).Value
                score3 = Sheet1.Range(8, 28).Value
                Phase3TB = Sheet1.Range(20, 28).Value
                rank = Sheet1.Range(9, 28).Value
                Attendance = Sheet1.Range(11, 28).Value
                Dim StartCount, EndCount, MaxIndexPhase1, MaxIndexPhase2, MaxIndexPhase3 As Integer
                Dim StRoomSchCount, EndRoomschCount As Integer
                Dim Phase1_TB(10), Ph2_Round(20), Ph3_Round(30), index

                StartCount = Sheet1.Range(12, 28).Value
                EndCount = Sheet1.Range(12, 29).Value
                index = 1
                If StartCount > 0 And EndCount > 0 Then
                    For PhaseValue = StartCount To EndCount
                        Phase1_TB(index) = PhaseValue
                        index = index + 1
                        MaxIndexPhase1 = index
                    Next
                End If

                StartCount = Sheet1.Range(13, 28).Value
                EndCount = Sheet1.Range(13, 29).Value
                index = 1
                If StartCount > 0 And EndCount > 0 Then
                    For PhaseValue = StartCount To EndCount
                        Ph2_Round(index) = PhaseValue
                        index = index + 1
                        MaxIndexPhase2 = index
                    Next
                End If

                If ddlPhase.SelectedValue = "2" Or ddlPhase.SelectedValue = "3" Then
                    ''Copy from one sheet
                    lbldwError.Text = ""
                    Dim NRooms As Integer = 0
                    Dim RoomSchflag As Boolean = True
                    Dim StrSQL_NRooms As String

                    If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ContestCategory Where (Phase2_Reg='Y' Or Phase3_Reg='Y') and ContestYear=" & ddlYear.SelectedValue & " and ProductGroupCode= '" & ddlProductGroup.SelectedItem.Value & "' and ContestCode= '" & getProductcode(ddlProduct) & "'") > 0 Then

                        StrSQL_NRooms = "select count(Distinct C.SeqNo) from RoomSchedule C  WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " And C.Phase = 2 and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") "
                        NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL_NRooms)

                        If NRooms > 0 Then
                            RoomSchflag = True
                        End If
                        '***********************************************************************************************************************'
                        If NRooms <= 0 Then
                            NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph2Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                            RoomSchflag = False
                        End If

                    Else
                        NRooms = 1
                    End If

                    ''Added to Check Ph2split Flag of the Score Sheet to be 'Y' to allow Phase2 split for Rooms.
                    If Sheet1.Range(4, 28).Value <> "Y" Then
                        NRooms = 1
                    End If

                    Dim NContestants As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM Contestant WHERE ContestID =" & ddlProduct.SelectedValue & " AND BadgeNumber IS NOT NULL AND PaymentReference IS NOT NULL ")
                    Dim RoomCount, StartRoomCount, EndRoomCount, RoomCountRest As Integer
                    Dim MaxValue As Integer
                    Dim RoomNumber As Integer

                    Dim file2 As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFilename & extension)) '"_P2" & '-- if the file exists on the server

                    If file2.Exists Then
                        Dim xlWorkBook_Phase2 As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\Master\" & SrcFilename & extension)) '"_P2" & 
                        Dim SheetValue As IWorksheet
                        Dim SheetCount As Integer = xlWorkBook_Phase2.Worksheets.Count

                        For rvalue As Integer = NRooms + 1 To SheetCount
                            xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                        Next
                        MaxValue = NRooms

                        For RoomNumberCount As Integer = 1 To NRooms
                            i = 7
                            RoomNumber = RoomNumberCount
                            If NRooms > 0 Then

                                If RoomSchflag = False Then
                                    'To split Contestants uniformly in NRooms
                                    'Number of contestants in a room  = Truncated as Integer(total number of contestants / No. of Rooms + 0.95)

                                    RoomCount = Math.Floor((NContestants / NRooms) + 0.95)
                                    RoomCountRest = NContestants - ((NRooms - 1) * RoomCount)
                                    StartRoomCount = (RoomNumber - 1) * RoomCount
                                    EndRoomCount = (RoomNumber * RoomCount)
                                    If RoomNumber = MaxValue Then
                                        If Not RoomCountRest = 0 Then '(RoomCount / 2) 
                                            EndRoomCount = StartRoomCount + RoomCountRest 'EndRoomCount1 +
                                        End If
                                    End If
                                ElseIf RoomSchflag = True Then
                                    ''To get Data From RoomSchedule
                                    SQLRoomSch = "Select StartBadgeNo,EndBadgeNo,SeqNo From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlContest.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and  Phase=2 and SeqNo=" & IIf(TrRoom_phase.Visible = True, GetCode(ddlRoom_Phase.SelectedValue.ToString())(0), RoomNumberCount) & ""
                                    Dim dsRoomSch As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomSch)
                                    If dsRoomSch.Tables(0).Rows.Count > 0 Then

                                        StRoomSchCount = 1
                                        EndRoomschCount = 0
                                        Try
                                            Dim strStBdgeNo, strEndBadgeNo As String
                                            strStBdgeNo = dsRoomSch.Tables(0).Rows(0)("StartBadgeNo").ToString.Trim()
                                            strEndBadgeNo = dsRoomSch.Tables(0).Rows(0)("EndBadgeNo").ToString.Trim()
                                            If strStBdgeNo <> "" Then
                                                If strStBdgeNo.Contains("-") Then
                                                    StRoomSchCount = Integer.Parse(Regex.Replace(strStBdgeNo.Substring(strStBdgeNo.IndexOf("-")), "[^\d]", "")).ToString()
                                                Else
                                                    StRoomSchCount = Integer.Parse(Regex.Replace(strStBdgeNo, "[^\d]", "")).ToString()
                                                End If
                                            End If
                                            If strEndBadgeNo <> "" Then
                                                If strEndBadgeNo.Contains("-") Then
                                                    EndRoomschCount = Integer.Parse(Regex.Replace(strEndBadgeNo.Substring(strEndBadgeNo.IndexOf("-")), "[^\d]", "")).ToString()
                                                Else
                                                    EndRoomschCount = Integer.Parse(Regex.Replace(strEndBadgeNo, "[^\d]", "")).ToString()
                                                End If
                                            End If
                                        Catch ex As Exception
                                            StRoomSchCount = 1
                                            EndRoomschCount = 0
                                        End Try
                                        StartRoomCount = StRoomSchCount - 1
                                        EndRoomCount = EndRoomschCount

                                        If EndRoomCount = 0 Then
                                            lblDownLdError.Text = "Badge numbers are missing in Room Schedule for the Sequence number '" & IIf(TrRoom_phase.Visible = True, GetCode(ddlRoom_Phase.SelectedValue.ToString())(0), RoomNumberCount) & "'"
                                            Exit Sub
                                        End If
                                    Else
                                        lblDownLdError.Text = "Badge numbers are missing in Room Schedule for the Sequence number '" & IIf(TrRoom_phase.Visible = True, GetCode(ddlRoom_Phase.SelectedValue.ToString())(0), RoomNumberCount) & "'"
                                        Exit Sub
                                    End If
                                End If
                            Else
                                lblDownLdError.Text = "Room Data Is Not available"
                                StartRoomCount = 0
                                EndRoomCount = ds.Tables(0).Rows.Count
                            End If

                            Dim RoomNo_Value As Integer = IIf(TrRoom_phase.Visible = True, GetCode(ddlRoom_Phase.SelectedValue.ToString())(0), RoomNumberCount)
                            SheetValue = xlWorkBook_Phase2.Worksheets(RoomNo_Value)

                            If SheetValue.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                                lblDownLdError.Text = "Level Mismatch in Sheet"
                                Exit Sub
                            End If

                            str = SheetValue.Range(5, 28)
                            PWD = str.Value.ToString()
                            SheetValue.Unprotect(PWD)

                            If Not SheetValue.Range(4, 28).Value = "Y" Then
                                Exit Sub
                            End If
                            SheetValue.Range(4, 29).Value = IIf(TrRoom_phase.Visible = True, GetCode(ddlRoom_Phase.SelectedValue.ToString())(0), RoomNumberCount) 'RoomNumberCount
                            SheetValue.Range("A1", "I1").Value = "NSF NATIONAL " & DateTime.Now.Year & " SCORESHEETS"
                            For x = StartRoomCount To EndRoomCount - 1 'ds.Tables(0).Rows.Count - 1
                                If x = StartRoomCount Then
                                    SheetValue.Range(2, 1).Value = ds.Tables(0).Rows(x)("center")
                                End If
                                Try


                                    SheetValue.Range(i, 1).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                                    SheetValue.Range(i, 2).Value = ds.Tables(0).Rows(x)("ParticipantName")
                                    SheetValue.Range(i, 3).Value = ds.Tables(0).Rows(x)("DOB")
                                    SheetValue.Range(i, 4).Value = ds.Tables(0).Rows(x)("Grade")
                                    'store the contestant id in column AA
                                    SheetValue.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")
                                    If ddlPhase.SelectedValue = "3" Then
                                        SheetValue.Range(i, 5).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag From ScoreDetail Where ContestantID= " & SheetValue.Range(i, 27).Value)
                                    End If
                                    i = i + 1
                                Catch ex As Exception

                                End Try
                            Next

                            If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                                For rvalue As Integer = 1 To SheetCount 'NRooms
                                    If GetCode(ddlRoom_Phase.SelectedValue.ToString)(0) = rvalue Then 'Not
                                        xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetVisible '
                                    Else
                                        xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden  '.Delete()
                                    End If
                                Next
                            ElseIf Not ddlRoom.SelectedValue = "ALL" Then
                                For rvalue As Integer = 1 To NRooms
                                    If Not ddlRoom.SelectedValue = rvalue Then
                                        xlWorkBook_Phase2.Worksheets(rvalue).Visible = XlSheetVisibility.xlSheetHidden '.Delete()
                                    End If
                                Next
                            End If
                            'For SSB
                            str = SheetValue.Cells(10, 28)
                            Dim Phase2printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString  '
                            SheetValue.PageSetup.PrintArea = Phase2printarea
                            SheetValue.Protect(PWD)
                        Next
                        ''Flag set during Phase2 download
                        PhaseDFlag = "P2DList,RoomNumberD,RoomAllD"
                        If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                            PhaseDValue = "'Y'," & GetCode(ddlRoom_Phase.SelectedValue.ToString)(0) & "," & "NULL"
                        Else
                            PhaseDValue = "'Y'," & IIf(ddlRoom.SelectedValue = "ALL", -1, ddlRoom.SelectedValue) & "," & IIf(ddlRoom.SelectedValue = "ALL", "'Y'", "NULL")
                        End If


                        SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag
                        SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                        Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal

                        Try
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)
                            Response.Clear()
                            ' Response.ContentType = "application/vnd.ms-excel"
                            If extension = ".xls" Then
                                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                            Else
                                Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            End If
                            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & extension)
                            If extension = ".xls" Then
                                xlWorkBook_Phase2.SaveAs(Response.OutputStream, XlFileFormat.xlExcel97)
                            Else
                                xlWorkBook_Phase2.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
                            End If
                            Response.End()
                        Catch ex As Exception
                        End Try
                    Else
                        lblDownLdError.Text = "Sorry Phase2 Master file doesnot exist "
                        Exit Sub
                    End If
                ElseIf ddlPhase.SelectedValue = "5" Then 'Added to load Phase3 Contestants List...confirmation message yet to be added
                    Sheet1.Unprotect(PWD)
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheetPhase3", New SqlParameter("@P1_P2_Rank", ddlTopRank.SelectedValue), New SqlParameter("@ContestID", ddlProduct.SelectedValue))
                    Dim MaxValue As Integer = 0
                    If ds1.Tables(0).Rows.Count > 0 Then
                        i = 7
                        For x = 0 To ds1.Tables(0).Rows.Count - 1
                            If x = 0 Then
                                Sheet1.Range(2, 1).Value = ds1.Tables(0).Rows(x)("center")
                            End If
                            Sheet1.Range(i, 1).Value = ds1.Tables(0).Rows(x)("BadgeNumber")
                            Sheet1.Range(i, 2).Value = ds1.Tables(0).Rows(x)("ParticipantName")
                            Sheet1.Range(i, 3).Value = ds1.Tables(0).Rows(x)("DOB")
                            Sheet1.Range(i, 4).Value = ds1.Tables(0).Rows(x)("Grade")
                            'store the contestant id in column AA
                            Sheet1.Range(i, 27).Value = ds1.Tables(0).Rows(x)("ContestantID")

                            If MaxValue < ds1.Tables(0).Rows(x)("P1_P2_Rank") Then
                                MaxValue = ds1.Tables(0).Rows(x)("P1_P2_Rank") 'ddlTopRank.SelectedValue '
                            End If
                            i = i + 1
                        Next
                    End If
                    Dim ds2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select COUNT(*) as Count,MAX(P1_P2_Rank) as MaxiValue from dbo.ScoreDetail c WHERE(c.P1_P2_Rank <= " & MaxValue & ")  and c.ContestID =" & ddlProduct.SelectedValue)

                    If ds2.Tables(0).Rows(0)("Count") > ddlTopRank.SelectedValue And P3flag = False Then
                        TrConfirmDwnload.Visible = True
                        lblConfirmDwnload.Text = "Not all contestants with same rank (Rank " & MaxValue & ") are included in Phase 3"

                    Else
                        P3flag = True
                    End If

                    str = Sheet1.Cells(10, 28)
                    Dim printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString()
                    Sheet1.PageSetup.PrintArea = printarea
                    Sheet1.Protect(PWD)

                    PhaseDFlag = "P3DList"
                    PhaseDValue = "'Y'"

                    SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag
                    SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                    Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)
                    Try
                        If P3flag = True Then
                            Response.Clear()
                            If extension = ".xls" Then
                                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                            Else
                                Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            End If
                            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & extension)

                            If extension = ".xls" Then
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlExcel97)
                            Else
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
                            End If
                            Response.End()
                        End If
                    Catch ex As Exception
                        'Response.Write(ex.ToString)
                    End Try
                Else
                    Sheet1.Unprotect(PWD)
                    Dim wRange As IRange
                    If ddlProductGroup.SelectedValue = "MB" Then
                        wRange = Sheet1.Range(7, 18)
                        wRange.ColumnWidth() = 10
                    ElseIf ddlProductGroup.SelectedValue = "SC" Then
                        wRange = Sheet1.Range(7, 15)
                        wRange.ColumnWidth() = 10
                    End If
                    For x = 0 To ds.Tables(0).Rows.Count - 1
                        If x = 0 Then
                            Sheet1.Range(2, 1).Value = ds.Tables(0).Rows(x)("center")
                        End If
                        Sheet1.Range(i, 1).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                        Sheet1.Range(i, 2).Value = ds.Tables(0).Rows(x)("ParticipantName")
                        Sheet1.Range(i, 3).Value = ds.Tables(0).Rows(x)("DOB")
                        Sheet1.Range(i, 4).Value = ds.Tables(0).Rows(x)("Grade")
                        'store the contestant id in column AA
                        Sheet1.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")

                        If ddlPhase.SelectedValue = 1 Then
                            ' Storing data to ScoreDetail Table during Download
                            Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & Sheet1.Cells(i, 27).Value() & "")

                            If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ScoreDetail Where ContestantID=" & Sheet1.Range(i, 27).Value) = 0 Then
                                InsertFlag = True
                                SQLInsertPhase1 = SQLInsertPhase1 + "Insert into ScoreDetail (ContestantID,ContestID,BadgeNumber, DOB,Grade,Createdby,CreatedDate,EventID,ChapterID,ContestYear,ProductCode,ProductGroupCode,ProductID,ProductGroupID,ChildNumber,MemberID) Values (" & Sheet1.Range(i, 27).Value & "," & ddlProduct.SelectedValue & ",'" & Sheet1.Range(i, 1).Value & "','" & Sheet1.Range(i, 3).Value & "'," & Sheet1.Range(i, 4).Value & "," & Session("LoginID") & ",GETDATE()," & ddlContest.SelectedValue & "," & ddlChapter.SelectedValue & "," & ddlYear.SelectedValue & ",'" & getProductcode(ddlProduct) & "','" & ddlProductGroup.SelectedValue & "'," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ")"
                            End If
                        ElseIf ddlPhase.SelectedValue = 4 Or ddlPhase.SelectedValue = 10 Then 'Or ddlPhase.SelectedValue = 10 Then 'P12 -Composite Data
                            If Attendance > 0 Then
                                Sheet1.Range(i, Attendance).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                            End If
                            If Score1 > 0 Then
                                Sheet1.Range(i, Score1).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                                For index = 1 To MaxIndexPhase1 - 1
                                    Sheet1.Range(i, Phase1_TB(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1_TB" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Phase1_TB" & index & " Is Not Null")
                                Next
                            End If
                            If score2 > 0 Then
                                If ddlContest.SelectedValue = 1 Then '' Updated on 08-04-2014 to display Phase2 total score from DB for Finals and To display Phase2 total score for regionals based on formula in sheet.
                                    'Commented on 31-03-2014- As Formula in sheet is overwritten by DB values- Unable ot modify values
                                    Sheet1.Range(i, score2).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase2Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                                End If
                                For index = 1 To MaxIndexPhase2 - 1
                                    Sheet1.Range(i, Ph2_Round(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Ph2_Round" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Ph2_Round" & index & " Is Not Null")
                                Next
                            End If
                        ElseIf ddlPhase.SelectedValue = 6 Then 'P123-Composite Data
                            Dim dsG As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag,Phase1Score,Phase2Score,IsNull(Phase3Score,null) as Phase3Score,IsNull(Ph3_TB_Score,null) as Ph3_TB_Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                            If dsG.Tables(0).Rows.Count > 0 Then
                                If Attendance > 0 Then
                                    Sheet1.Range(i, Attendance).Value = dsG.Tables(0).Rows(0)("AttendanceFlag")
                                End If
                                Sheet1.Range(i, Score1).Value = dsG.Tables(0).Rows(0)("Phase1Score")
                                Sheet1.Range(i, score2).Value = dsG.Tables(0).Rows(0)("Phase2Score")
                                If score3 > 0 Then
                                    Sheet1.Range(i, score3).Value = dsG.Tables(0).Rows(0)("Phase3Score")
                                End If
                                If Phase3TB > 0 Then
                                    Sheet1.Range(i, Phase3TB).Value = dsG.Tables(0).Rows(0)("Ph3_TB_Score")
                                End If
                            End If

                            For index = 1 To MaxIndexPhase1 - 1
                                Sheet1.Range(i, Phase1_TB(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1_TB" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Phase1_TB" & index & " Is Not Null")
                            Next
                        End If
                        i = i + 1
                    Next
                    'For SSB
                    str = Sheet1.Cells(10, 28)
                    Dim printarea As String = "$A$1:$" & str.Value.ToString() & "$" & i.ToString()
                    Sheet1.PageSetup.PrintArea = printarea
                    Sheet1.Range(7, 31).Value = ""
                    Sheet1.Protect(PWD)
                    '//////////////////////
                    'Add Rank in Sheet 2 for Ph1 & 2 Contestants List
                    If ddlPhase.SelectedValue = "10" Then
                        AddRank(xlWorkBook)
                    End If
                    '//////////////////////
                    Try
                        'lbldwError.Text = "Downloaded Successfully"

                        'Stroing of data into ScoreDetail Table
                        If InsertFlag = True Then
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLInsertPhase1)
                            InsertFlag = False
                        End If

                        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) from scoresheet where contestId=" & ddlProduct.SelectedValue) = 0 Then
                            Dim SQLstr As String = "Insert into ScoreSheet(ProductCode, ChapterCode, ContestID, FileName, DownloadedBy, downloadedDate) VALUES ('"
                            SQLstr = SQLstr & getProductcode(ddlProduct) & "','" & ddlChapter.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & FileName & ".xlsx" & "'," & Session("LoginID") & ",Getdate())"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLstr)
                            checkscorehsheetdown()
                        End If
                        If ddlPhase.Visible = True Then
                            If ddlPhase.SelectedValue = "1" Then
                                PhaseDFlag = "P1DList"
                            ElseIf ddlPhase.SelectedValue = "4" Or ddlPhase.SelectedValue = "10" Then
                                PhaseDFlag = "P12DList"
                            ElseIf ddlPhase.SelectedValue = "5" Then
                                PhaseDFlag = "P3DList"
                            ElseIf ddlPhase.SelectedValue = "6" Then
                                PhaseDFlag = "P123DList"
                                'ElseIf ddlPhase.SelectedValue = "10" Then
                                'PhaseDFlag = "P12DList"
                            End If
                        Else
                            If ddlContest.SelectedValue = 1 Then
                                PhaseDFlag = "P123DList"
                            Else
                                PhaseDFlag = "P12DList"
                            End If
                        End If
                        PhaseDValue = "'Y'"

                        SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag & ""
                        SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                        Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)

                        'Stream workbook 
                        Response.Clear()
                        If extension = ".xls" Then
                            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                        Else
                            Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                        End If
                        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & extension)

                        If extension = ".xls" Then
                            xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlExcel97)
                        Else
                            xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
                        End If
                        Response.End()

                    Catch ex As Exception
                    End Try
                End If 'Phases Loop Ending
Phase3List:
                If (ddlPhase.SelectedValue = "7") Or (ddlPhase.SelectedValue = "8") Then

                    Dim ds2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select COUNT(*) as Count from dbo.ScoreDetail c WHERE(c.Final_Rank <= " & ddlTopRank.SelectedValue & ")  and c.ContestID =" & ddlProduct.SelectedValue)
                    If ds2.Tables(0).Rows(0)("Count") > ddlTopRank.SelectedValue And P3flag = False Then
                        TrConfirmDwnload.Visible = True
                        lblConfirmDwnload.Text = "Not all contestants with same rank (Rank " & ddlTopRank.SelectedValue & ") are included in Phase 3"
                    Else
                        P3flag = True
                    End If

                    If ddlPhase.SelectedValue = "7" Then
                        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetTopContestantsAfterPhase3", New SqlParameter("@Final_Rank", ddlTopRank.SelectedValue), New SqlParameter("@ContestID", ddlProduct.SelectedValue))
                        If ddlContest.SelectedValue = 2 Then
                            Sheet1.Range(2, 2).Value = ds.Tables(0).Rows(x)("center")
                        End If

                        If ds1.Tables(0).Rows.Count > 0 Then
                            i = 7
                            For x = 0 To ds1.Tables(0).Rows.Count - 1
                                Sheet1.Range(i, 2).Value = ds1.Tables(0).Rows(x)("Rank_Alpha")
                                Sheet1.Range(i, 3).Value = ds1.Tables(0).Rows(x)("BadgeNumber")
                                Sheet1.Range(i, 4).Value = ds1.Tables(0).Rows(x)("ParticipantName")
                                i = i + 1
                            Next
                        Else
                            lbldwError.Text = "Please upload Ranks before downloading Top List Of Contestants (Or) Rank Certificates."
                            Exit Sub
                        End If

                        PhaseDFlag = "TopListD"
                        PhaseDValue = "'Y'"

                        SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag & ""
                        SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                        Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)

                        If P3flag = True Then
                            Response.Clear()
                            If extension = ".xls" Then
                                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                            Else
                                Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            End If
                            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & extension)

                            If extension = ".xls" Then
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlExcel97)
                            Else
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
                            End If
                            Response.End()
                        End If
                    ElseIf ddlPhase.SelectedValue = "8" Then 'Rank Certificates

                        PhaseDFlag = "RankCertificateD"
                        PhaseDValue = "'Y'"

                        SQLScoreSheetLog = SQLScoreSheetLog & PhaseDFlag & ""
                        SQLScoreSheetLogVal = SQLScoreSheetLogVal & PhaseDValue & ")"
                        Dim SQLPhaseExec As String = SQLScoreSheetLog & SQLScoreSheetLogVal

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLPhaseExec)
                        Session("EventYear") = ddlYear.SelectedValue
                        Session("SelChapterID") = ddlChapter.SelectedValue.ToString()
                        Session("ContestID") = ddlProduct.SelectedValue
                        Session("ProductCode") = getProductcode(ddlProduct)
                        Session("TopRank") = ddlTopRank.SelectedValue
                        Session("Page") = "ManageScoresheet.aspx"
                        If ddlTopRank.SelectedValue > 0 And P3flag = True Then
                            Response.Redirect("GenerateParticipantCertificates.aspx")
                        Else
                            lbldwError.Text = "No Rank Certificates"
                            Exit Sub
                        End If

                    End If

                ElseIf (ddlPhase.SelectedValue = "9") Then
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheetPhase3", New SqlParameter("@P1_P2_Rank", ddlTopRank.SelectedValue), New SqlParameter("@ContestID", ddlProduct.SelectedValue))
                    Dim MaxValue As Integer = 0

                    If ds1.Tables(0).Rows.Count > 0 Then
                        i = 7
                        For x = 0 To ds1.Tables(0).Rows.Count - 1
                            Sheet1.Range(i, 3).Value = ds1.Tables(0).Rows(x)("BadgeNumber")
                            Sheet1.Range(i, 4).Value = ds1.Tables(0).Rows(x)("ParticipantName")
                            If MaxValue < ds1.Tables(0).Rows(x)("P1_P2_Rank") Then
                                MaxValue = ds1.Tables(0).Rows(x)("P1_P2_Rank") 'ddlTopRank.SelectedValue '
                            End If
                            i = i + 1
                        Next
                    End If
                    Dim ds2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "Select COUNT(*) as Count,MAX(P1_P2_Rank) as MaxiValue from dbo.ScoreDetail c WHERE(c.P1_P2_Rank <= " & MaxValue & ")  and c.ContestID =" & ddlProduct.SelectedValue)

                    If ds2.Tables(0).Rows(0)("Count") > ddlTopRank.SelectedValue And P3flag = False Then
                        TrConfirmDwnload.Visible = True
                        lblConfirmDwnload.Text = "Not all contestants with same rank (Rank " & MaxValue & ") are included in Phase 3"
                    Else
                        P3flag = True
                    End If

                    Try
                        If P3flag = True Then
                            'Stream(workbook)
                            Response.Clear()
                            If extension = ".xls" Then
                                Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                            Else
                                Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                            End If
                            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName & extension)

                            If extension = ".xls" Then
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlExcel97)
                            Else
                                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
                            End If
                            Response.End()
                        End If
                    Catch ex As Exception
                    End Try
                End If
            End If
        Catch ex As Exception

        End Try
    End Sub


    Sub AddRank(ByRef xlWorkBook As IWorkbook)
        Try


            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(2)
            Dim str As IRange = Sheet1.Range(5, 28)
            Dim PWD As String = str.Value.ToString()
            Sheet1.Unprotect(PWD)
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestantsforScoreSheetByRank", New SqlParameter("@ContestID", ddlProduct.SelectedValue))
            If ds.Tables(0).Rows.Count > 0 Then
                Dim SrcFilename, FileName, ProductCode, ChapterCode As String
                Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select DISTINCT C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
                Dim SQLInsertPhase1 As String = ""
                Dim SQLScoreSheetLog As String = ""
                Dim SQLScoreSheetLogVal As String = ""
                Dim SQLRoomSch As String = ""
                Dim PhaseDFlag As String = ""
                Dim PhaseDValue As String = ""

                While readr.Read()
                    SrcFilename = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet"
                    FileName = SrcFilename & "_" & readr("Chaptercode")
                    If ddlPhase.SelectedValue = 4 Or ddlPhase.SelectedValue = 10 Then 'Or ddlPhase.SelectedValue = "10" Summary Data

                        FileName = FileName & "_p12"

                    End If
                    ProductCode = readr("ProductCode")
                    ChapterCode = readr("Chaptercode")
                End While
                readr.Close()

                Dim x As Integer
                Dim i As Integer = 7

                Dim InsertFlag As Boolean = False
                Dim Score1, score2, score3, rank, Phase3TB, PhaseValue, Attendance As Integer

                If Sheet1.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                    lbldwError.Text = "Level Mismatch in Sheet"
                    Exit Sub
                End If

                Score1 = Sheet1.Range(6, 28).Value
                score2 = Sheet1.Range(7, 28).Value
                score3 = Sheet1.Range(8, 28).Value
                Phase3TB = Sheet1.Range(20, 28).Value
                rank = Sheet1.Range(9, 28).Value
                Attendance = Sheet1.Range(11, 28).Value
                Dim StartCount, EndCount, MaxIndexPhase1, MaxIndexPhase2, MaxIndexPhase3 As Integer
                Dim StRoomSchCount, EndRoomschCount As Integer
                Dim Phase1_TB(10), Ph2_Round(20), Ph3_Round(30), index

                StartCount = Sheet1.Range(12, 28).Value
                EndCount = Sheet1.Range(12, 29).Value
                index = 1
                If StartCount > 0 And EndCount > 0 Then
                    For PhaseValue = StartCount To EndCount
                        Phase1_TB(index) = PhaseValue
                        index = index + 1
                        MaxIndexPhase1 = index
                    Next
                End If

                StartCount = Sheet1.Range(13, 28).Value
                EndCount = Sheet1.Range(13, 29).Value
                index = 1
                If StartCount > 0 And EndCount > 0 Then
                    For PhaseValue = StartCount To EndCount
                        Ph2_Round(index) = PhaseValue
                        index = index + 1
                        MaxIndexPhase2 = index
                    Next
                End If

                Sheet1.Unprotect(PWD)
                Dim wRange As IRange
                If ddlProductGroup.SelectedValue = "MB" Then
                    wRange = Sheet1.Range(7, 18)
                    wRange.ColumnWidth() = 10
                ElseIf ddlProductGroup.SelectedValue = "SC" Then
                    wRange = Sheet1.Range(7, 15)
                    wRange.ColumnWidth() = 10
                End If
                For x = 0 To ds.Tables(0).Rows.Count - 1
                    If x = 0 Then
                        Sheet1.Range(2, 1).Value = ds.Tables(0).Rows(x)("center")
                    End If
                    Sheet1.Range(i, 1).Value = ds.Tables(0).Rows(x)("BadgeNumber")
                    Sheet1.Range(i, 2).Value = ds.Tables(0).Rows(x)("ParticipantName")
                    Sheet1.Range(i, 3).Value = ds.Tables(0).Rows(x)("DOB")
                    Sheet1.Range(i, 4).Value = ds.Tables(0).Rows(x)("Grade")
                    'store the contestant id in column AA
                    Sheet1.Range(i, 27).Value = ds.Tables(0).Rows(x)("contestant_id")

                    If ddlPhase.SelectedValue = 4 Or ddlPhase.SelectedValue = 10 Then 'Or ddlPhase.SelectedValue = 10 Then 'P12 -Composite Data
                        If Attendance > 0 Then
                            Sheet1.Range(i, Attendance).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select AttendanceFlag From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                        End If
                        If Score1 > 0 Then
                            Sheet1.Range(i, Score1).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                            For index = 1 To MaxIndexPhase1 - 1
                                Sheet1.Range(i, Phase1_TB(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase1_TB" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Phase1_TB" & index & " Is Not Null")
                            Next
                        End If
                        If score2 > 0 Then
                            If ddlContest.SelectedValue = 1 Then '' Updated on 08-04-2014 to display Phase2 total score from DB for Finals and To display Phase2 total score for regionals based on formula in sheet.
                                'Commented on 31-03-2014- As Formula in sheet is overwritten by DB values- Unable ot modify values
                                Sheet1.Range(i, score2).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Phase2Score From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value)
                            End If
                            For index = 1 To MaxIndexPhase2 - 1
                                Sheet1.Range(i, Ph2_Round(index)).Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Ph2_Round" & index & " From ScoreDetail Where ContestantID= " & Sheet1.Range(i, 27).Value & "and Ph2_Round" & index & " Is Not Null")
                            Next
                        End If

                    End If
                    i = i + 1
                Next

            End If
            Sheet1.Protect(PWD)
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUpload.Click
        Try
            Dim extension As String
            Dim masterfilename As String
            If FileUpload.HasFile Then
                extension = System.IO.Path.GetExtension(FileUpload.FileName).ToLower
                masterfilename = ddlYear.SelectedValue & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & ddlProductGroup.SelectedValue & "_" & getProductcode(ddlProduct) & "_ScoreSheet"
                Dim extMasterFile As String = ".xlsx"
                Dim fName As String = Server.MapPath("ScoreSheets\Master\" & masterfilename & extMasterFile)
                Dim file As System.IO.FileInfo = New System.IO.FileInfo(fName) '-- if the file exists on the server
                If Not file.Exists Then
                    extMasterFile = ".xls"
                    fName = Server.MapPath("ScoreSheets\Master\" & masterfilename & extMasterFile)
                    file = New System.IO.FileInfo(fName)
                End If
                If extension <> extMasterFile Then
                    lblErr.Text = "Only Excel file with " & extMasterFile & " is allowed."
                    Exit Sub
                End If

                Dim srcfilename As String = FileUpload.FileName
                Dim checkfile As String() = srcfilename.Split("_")
                Dim Length As Integer = checkfile.Length
                Dim sheetChapter As String = String.Empty
                Dim ContestantIDValue As String = "0"
                If (Length > 7) Then
                    sheetChapter = checkfile(5).Trim() & "_" & checkfile(6).Trim() & "," & checkfile(7).Trim().Substring(0, 2)
                Else
                    sheetChapter = checkfile(5).Trim() & "," & checkfile(6).Trim().Substring(0, 2)
                End If

                Session("UplFileName") = srcfilename

                If checkfile(0).Trim() <> ddlYear.SelectedValue Then
                    lbldwError.Text = "Invalid Year Selection"
                    Exit Sub
                ElseIf checkfile(1).Trim() <> IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") Then
                    lbldwError.Text = "Invalid Event Selection"
                    Exit Sub
                ElseIf checkfile(2).Trim() <> ddlProductGroup.SelectedValue Then
                    lbldwError.Text = "Invalid Contest Selection"
                    Exit Sub
                ElseIf checkfile(3).Trim() <> getProductcode(ddlProduct) Then
                    lbldwError.Text = "Invalid Level Selection"
                    Exit Sub
                ElseIf checkfile(4).Trim() <> "ScoreSheet" Then
                    lbldwError.Text = "ScoreSheet missing in the File name"
                    Exit Sub
                End If
                If ddlContest.SelectedValue <> 1 Then
                    If (sheetChapter <> ddlChapter.SelectedItem.Text.Replace(" ", "")) Then 'And (Not ddlContest.SelectedValue = 1) Then
                        lbldwError.Text = "Invalid Chapter name"
                        Exit Sub
                    End If
                End If

                FileUpload.PostedFile.SaveAs(Server.MapPath("ScoreSheets/" & FileUpload.FileName))
                If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 9 Or Session("RoleID") = 93 Then
                    CheckEnableUFlags(True)
                    If Uploadflag = False Then
                        Try
                            ''Added on 06-08-2014 to allow TCs to upload Phase 2score scores even if Phase2 sheet is not downloaded for the selected Room. -- Was added to avoid any Issues during Finals.
                            If ddlTypeofData1.SelectedValue = 2 Then ''Updated on 03-16-2014 for Issue with Upload of Global score sheet by tC s during regionals on Mar 16 2015.
                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from ScoreSheetLog where ContestID= " & ddlProduct.SelectedValue & " And EventID=" & ddlContest.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & " And ContestYear=" & ddlYear.SelectedValue & " and (RoomAllD='Y' Or (P2DList='Y' and RoomNumberD=" & ddlRoom1.SelectedValue & "))") = 0 Then
                                    TrPhase2Upload.Visible = True
                                    Session("UplFileName") = srcfilename
                                Else
                                    UploadScores(srcfilename)
                                End If
                            Else
                                UploadScores(srcfilename)
                            End If
                        Catch ex As Exception
                            lbldwError.Text = ex.ToString()
                        End Try
                    End If
                Else
                    UploadScores(srcfilename)
                End If
            Else
                lblErr.Text = "Please Click Browse to Select Filled Score Sheet."
            End If

        Catch ex As Exception

        End Try
    End Sub
    Private Sub UploadScores(ByVal srcfilename As String)
        Dim errorLog As String = ""
        Dim sRank As String = ""
        Try

            Dim checkfile As String() = srcfilename.Split("_")
            Dim Length As Integer = checkfile.Length
            Dim ContestantIDValue As String = "0"
            Dim sheetChapter As String = String.Empty

            If (Length > 7) Then
                sheetChapter = checkfile(5).Trim() & "_" & checkfile(6).Trim() & "," & checkfile(7).Trim().Substring(0, 2)
            Else
                sheetChapter = checkfile(5).Trim() & "," & checkfile(6).Trim().Substring(0, 2)
            End If

            Dim sheet As NPOI.SS.UserModel.ISheet
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("ScoreSheets\" & srcfilename))

            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)
            Dim x As Integer
            Dim i As Integer = 7
            Dim str As IRange = Sheet1.Range(5, 28)
            Dim PWD As String = str.Value.ToString()
            Sheet1.Unprotect(PWD)
            Dim chapterCode As String = Sheet1.Range(2, 1).Value.ToString().Replace(" ", "")

            If srcfilename.Contains(".xlsx") Then
                Try
                    Dim hssfwb As XSSFWorkbook
                    Dim f1 As FileStream = New FileStream(Server.MapPath("ScoreSheets\" & srcfilename), FileMode.Open, FileAccess.Read)
                    hssfwb = New XSSFWorkbook(f1)
                    sheet = hssfwb.GetSheet(Sheet1.Name)
                    'sheet.GetRow(8).GetCell(27, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).ToString()
                Catch ex As Exception
                    errorLog = ex.Message.ToString()
                End Try

            End If
            ''Checking with Sheet Data
            If ddlContest.SelectedValue <> 1 Then
                If (sheetChapter <> chapterCode) Then 'And (ddlContest.SelectedValue = 1)) '.Substring(0, (checkfile(5).Length - 4)
                    lbldwError.Text = "Invalid Chapter Selection"
                    Exit Sub
                End If
            End If
            If Sheet1.Range(5, 1).Value <> ddlProduct.SelectedItem.Text Then
                lbldwError.Text = "Level mismatch in Excel Data"
                Exit Sub
            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from contestant where ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "") = 0 Then
                lbldwError.Text = "No Contestant Data found in server"
                Exit Sub
            End If
            If ddlPhase.Visible = True And ddlTypeofData1.Visible = True Then
                Dim FileIndex, RoomIndex, MaxLength As Integer
                FileIndex = 7
                RoomIndex = 8
                MaxLength = 8

                If (ddlTypeofData1.SelectedValue = "0" Or ddlTypeofData1.SelectedValue = "1") And Length > FileIndex Then
                    lbldwError.Text = "File Name Mismatch"
                    Exit Sub
                ElseIf ddlTypeofData1.SelectedValue = "2" Then
                    If Length > FileIndex Then
                        If checkfile(FileIndex).Trim().Substring(0, 2) <> "p" & ddlTypeofData1.SelectedValue Then
                            lbldwError.Text = " Missing P" & ddlTypeofData1.SelectedValue & " in the filename "
                            Exit Sub
                        End If
                        If Length > RoomIndex Then
                            If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                                If checkfile(RoomIndex).Trim().Substring(0, 1) <> GetCode(ddlRoom_Phase.SelectedValue.ToString())(0) Then
                                    lbldwError.Text = "RoomNumber Mismatch in the filename "
                                    Exit Sub
                                End If
                            Else
                                If checkfile(RoomIndex).Trim().Substring(0, 1) <> ddlRoom1.SelectedValue Then
                                    lbldwError.Text = "RoomNumber Mismatch in the filename "
                                    Exit Sub
                                End If
                            End If
                        Else
                            If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                                If Not xlWorkBook.ActiveSheet.Index = GetCode(ddlRoom_Phase.SelectedValue.ToString())(0) Then
                                    xlWorkBook.ActiveSheet.EnableSelection = GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)
                                End If
                            Else
                                If Not xlWorkBook.ActiveSheet.Index = ddlRoom1.SelectedValue Then
                                    xlWorkBook.ActiveSheet.EnableSelection = ddlRoom1.SelectedValue
                                End If
                            End If
                        End If
                    Else
                        lbldwError.Text = "File Name Mismatch"
                        Exit Sub
                    End If
                ElseIf ((ddlTypeofData1.SelectedValue = "3") Or (ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 2) Or (ddlTypeofData1.SelectedValue = "5" And ddlContest.SelectedValue = 1) Then
                    If Length > MaxLength Then
                        lbldwError.Text = "Invalid File"
                        Exit Sub
                    ElseIf Length > FileIndex Then 'Or ddlTypeofData1.SelectedValue = "4") Then
                        If checkfile(FileIndex).Trim().Substring(0, 3) <> "p12" Then
                            lbldwError.Text = "File Missing P12 in the File Name"
                            Exit Sub
                        End If
                    ElseIf ddlContest.SelectedValue = 1 Then
                        lbldwError.Text = "File Name Mismatch"
                        Exit Sub
                    End If
                ElseIf (ddlTypeofData1.SelectedValue = "3" And ddlContest.SelectedValue = 1) Then
                    If Length > MaxLength Then
                        lbldwError.Text = "Invalid File"
                        Exit Sub
                    ElseIf Length > FileIndex Then 'Or ddlTypeofData1.SelectedValue = "4") Then
                        If checkfile(FileIndex).Trim().Substring(0, 4) <> "p123" Then
                            lbldwError.Text = "File Missing P123 in the File Name"
                            Exit Sub
                        End If
                    Else
                        lbldwError.Text = "File Name Mismatch"
                        Exit Sub
                    End If
                ElseIf ddlTypeofData1.SelectedValue = "6" And ddlContest.SelectedValue = 1 Then
                    If Length > MaxLength Then
                        lbldwError.Text = "Invalid File"
                        Exit Sub
                    ElseIf Length > FileIndex Then 'Or ddlTypeofData1.SelectedValue = "4") Then
                        If checkfile(FileIndex).Trim().Substring(0, 2) <> "p3" Then
                            lbldwError.Text = "File Missing P3 in the File Name"
                            Exit Sub
                        End If
                    Else
                        lbldwError.Text = "File Name Mismatch"
                        Exit Sub
                    End If
                End If
            End If

            Dim Score1, score2, score3, rank, rank3, PhaseValue, Attendance, Phase3TB As Integer
            Dim Phase_TB(10) As Integer
            Dim rank_alpha As Char
            Dim Phase1_TB(10), Ph2_Round(10), Ph3_Round(20) As Integer
            Score1 = Sheet1.Range(6, 28).Value
            score2 = Sheet1.Range(7, 28).Value
            score3 = Sheet1.Range(8, 28).Value
            Phase3TB = Sheet1.Range(20, 28).Value
            rank3 = Sheet1.Range(19, 28).Value
            rank = Sheet1.Range(9, 28).Value
            rank_alpha = Sheet1.Range(10, 28).Value.ToString
            Attendance = Sheet1.Range(11, 28).Value

            Dim SQLstr As String = ""
            Dim SQLScoreStr As String = ""
            Dim SQLScoreStrVal As String = ""
            Dim SQLScoreStrExe As String = ""

            Dim SQLScoreUpdateStr As String = ""
            Dim SQLScoreUpdateStrVal As String = ""
            Dim SQLScoreSheetLog As String = ""
            Dim SQLScoreSheetLogVal As String = ""
            Dim PhaseUFlag As String = ""
            Dim PhaseUVal As String = ""

            Dim flagcontestid As String = ""
            Dim printarea As String = Sheet1.PageSetup.PrintArea
            Dim Rowcnt As Integer = Val(printarea.Substring(printarea.LastIndexOf("$") + 1))
            Dim ValueExistFlag As Boolean = False
            Dim ScoreExistFlag As Boolean = False
            Dim StartCount, EndCount, MaxIndexPhase1, MaxIndexPhase2, MaxIndexPhase3 As Integer
            Dim index As Integer
            MaxIndexPhase1 = 1
            MaxIndexPhase2 = 1
            MaxIndexPhase3 = 1

            StartCount = Sheet1.Range(12, 28).Value
            EndCount = Sheet1.Range(12, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Phase1_TB(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase1 = index
                Next
            End If
            StartCount = Sheet1.Range(13, 28).Value
            EndCount = Sheet1.Range(13, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Ph2_Round(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase2 = index
                Next
            End If

            StartCount = Sheet1.Range(18, 28).Value
            EndCount = Sheet1.Range(18, 29).Value
            index = 1
            If StartCount > 0 And EndCount > 0 Then
                For PhaseValue = StartCount To EndCount
                    Ph3_Round(index) = PhaseValue
                    index = index + 1
                    MaxIndexPhase3 = index
                Next
            End If
            Dim Ph2Flag As Boolean = False
            Dim Ph3cnt As Integer = 0
            If ddlTypeofData1.SelectedValue = "2" Then  'Uploading and Updation of ScoreSheet For  Phase2Scores
                Dim SheetName As IWorksheet
                Dim SheetCount As Integer
                If Session("RoleId") = 16 Or Session("RoleID") = 18 Then
                    SheetCount = GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)
                Else
                    SheetCount = ddlRoom1.SelectedValue
                End If
                SheetName = xlWorkBook.Worksheets(SheetCount)

                score2 = SheetName.Range(7, 28).Value
                rank_alpha = SheetName.Range(10, 28).Value

                printarea = SheetName.PageSetup.PrintArea
                Rowcnt = Val(printarea.Substring(printarea.LastIndexOf("$") + 1))
                Dim Ph2cnt As Integer

                For i = 7 To Rowcnt - 1
                    Try
                        If SheetName.Cells(i, 27).Value().ToString().Trim() = "" Then
                            Exit For
                        End If
                        If Not IsNumeric(SheetName.Cells(i, 27).Value()) = True Then
                            lblErr.Text = "This spreadsheet has insufficient data to upload automatically" & SheetName.Cells(i, 27).Value()
                            Exit Sub
                        End If
                        'Check for Badge Number & Grade
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ChildNumber) from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where Ch.FIRST_NAME +' '+ Ch.LAST_NAME ='" & SheetName.Cells(i, 2).Value().ToString.Replace("'", "''") & "' AND DateDiff(d,ch.Date_OF_Birth,'" & SheetName.Cells(i, 3).Value() & "')=0 AND C.contestant_id =" & SheetName.Cells(i, 27).Value() & " and C.BadgeNumber='" & SheetName.Cells(i, 1).Value() & "' and C.Grade=" & SheetName.Cells(i, 4).Value() & " AND C.ProductCode = '" & getProductcode(ddlProduct) & "' and C.ChapterID=" & ddlChapter.SelectedItem.Value & "") = 0 Then
                            lblErr.Text = "ScoreSheet was tampered on row# " & i & " with BadgeNo. " & SheetName.Cells(i, 1).Value()
                            Exit Sub
                        End If

                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & SheetName.Cells(i, 27).Value() & "")
                        SQLScoreStr = SQLScoreStr & " ContestantID ,ContestID, BadgeNumber,DOB,Grade,ChildNumber,MemberID,AttendanceFlag"
                        SQLScoreStrVal = SQLScoreStrVal & "" & SheetName.Cells(i, 27).Value() & "," & ddlProduct.SelectedValue & ",'" & SheetName.Cells(i, 1).Value() & "','" & SheetName.Cells(i, 3).Value() & "'," & SheetName.Cells(i, 4).Value() & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ",'" & SheetName.Cells(i, Attendance).Value() & "'"
                        SQLScoreUpdateStr = " Update ScoreDetail Set ContestID =" & ddlProduct.SelectedValue & ",BadgeNumber= '" & SheetName.Cells(i, 1).Value() & "',DOB= '" & SheetName.Cells(i, 3).Value() & "', Grade=" & SheetName.Cells(i, 4).Value() & ",ChildNumber=" & dsChildNm.Tables(0).Rows(0)(0) & ",MemberID=" & dsChildNm.Tables(0).Rows(0)(1) & ",AttendanceFlag =  Case when AttendanceFlag = 'N' then '" & SheetName.Cells(i, Attendance).Value() & "' Else AttendanceFlag End " ''ContestantID = " & SheetName.Cells(i, 27).Value() & ",
                        SQLstr = SQLstr & "Update Contestant set "

                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ScoreDetail Where  AttendanceFlag like '%N%' and ContestantID=" & SheetName.Cells(i, 27).Value() & "") > 0 Then
                            lblErr.Text = lblErr.Text & "Contestant in row " & i & " was not Present for Phase1 <br />"
                            'GoTo EscapValid1
                        End If

                        If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                            If ddlContest.SelectedValue <> 1 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & SheetName.Cells(i, 27).Value() & " and BadgeNumber='" & SheetName.Cells(i, 1).Value() & "' and Grade=" & SheetName.Cells(i, 4).Value() & " AND  Phase1Score is Null " & "") > 0 Then
                                lblErr.Text = lblErr.Text & "No Phase1 Scores Present for the Contestants in Row " & i & "<br />"
                                Ph2Flag = True
                            ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*)From ScoreDetail Where ContestantID=" & SheetName.Cells(i, 27).Value() & "") = 0 Then
                                lblErr.Text = "No Contestant Details found for Phase1"
                                Exit Sub
                            End If
                        End If
                        If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                            If Not IsNumeric(SheetName.Cells(i, score2).Value()) = True Then
                                Ph2cnt = 1
                                Ph2Flag = True

                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                            Else
                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                SQLScoreStrVal = SQLScoreStrVal & "," & SheetName.Cells(i, score2).Value() & ""
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & SheetName.Cells(i, score2).Value()
                                ScoreExistFlag = True
                            End If
                            For index = 1 To MaxIndexPhase2 - 1
                                If Not IsNumeric(SheetName.Cells(i, Ph2_Round(index)).Value()) = True Then

                                    SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "0,"
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=0"
                                Else
                                    SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "," & SheetName.Cells(i, Ph2_Round(index)).Value() & "" ' where ContestantID =" & SheetName.Cells(i, 27).Value() & ""
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=" & SheetName.Cells(i, Ph2_Round(index)).Value() & ""
                                    ScoreExistFlag = True
                                End If
                            Next
                            If score2 > 0 Then
                                SQLstr = SQLstr & " Score2=" & CDbl(0 & SheetName.Cells(i, score2).Value()) & ","
                                If CDbl(0 & SheetName.Cells(i, score2).Value()) > 0 Then
                                    ScoreExistFlag = True
                                End If
                            Else
                                SQLstr = SQLstr & "Score2=0 " & ","
                            End If
                            If Ph2cnt = 1 Then
                                lblErr.Text = lblErr.Text.Trim.Trim(",") & " on Row#" & i & " with BadgeNo. " & SheetName.Cells(i, 1).Value() & "<br>"
                                Ph2cnt = 0
                            End If

                        ElseIf SheetName.Cells(i, Attendance).Value() = "N" Then
                            SQLScoreStr = SQLScoreStr & ", Phase2Score "
                            SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =NULL"
                            For index = 1 To MaxIndexPhase2 - 1
                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                SQLScoreStrVal = SQLScoreStrVal & "NULL,"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=NULL"
                            Next
                            SQLstr = SQLstr & " Score2=NULL,"
                            GoTo EscapValid1
                        End If

EscapValid1:
                        If flagcontestid = "" Then
                            flagcontestid = SheetName.Cells(i, 27).Value()
                        End If

                    Catch ex As Exception
                        lblErr.Text = "Score Sheet Missing Value(s)on Row #" & i
                        'Response.Write(ex.ToString())
                    End Try

                    SQLstr = SQLstr & "ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() Where contestant_id=" & SheetName.Cells(i, 27).Value() & ";"
                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() WHERE ContestantID=" & SheetName.Cells(i, 27).Value() & ";" '" Update ScoreDetail Set " 

                    Try
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & SheetName.Cells(i, 27).Value() & "") > 0 Then
                            SQLScoreStrExe = SQLScoreStrExe & SQLScoreUpdateStr
                        Else
                            SQLScoreStrExe = SQLScoreStrExe & "Insert into ScoreDetail(" & SQLScoreStr & ",Createdby,CreatedDate) Values(" & SQLScoreStrVal & "," & Session("LoginID") & ",GETDATE());"
                        End If

                        SQLScoreStr = ""
                        SQLScoreStrVal = ""
                        SQLScoreUpdateStr = ""

                    Catch ex As Exception
                        Exit Sub
                    End Try

                    If Not SheetName.Cells(i, Attendance).Value() = "N" Then
                        ContestantIDValue = ContestantIDValue & "," & SheetName.Cells(i, 27).Value()
                    End If
                Next

                ''Flag set during Phase2 Upload
                PhaseUFlag = "P2UList,RoomNumberU"
                If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                    PhaseUVal = "'Y'," & GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)
                Else
                    PhaseUVal = "'Y'," & ddlRoom1.SelectedValue & ""
                End If

            ElseIf ddlTypeofData1.SelectedValue = "6" Then 'Only Phase3 Scores 

                'Dim Ph3cnt As Integer
                For i = 7 To Rowcnt - 1
                    Try
                        If Sheet1.Cells(i, 27).Value().ToString().Trim() = "" Then
                            Exit For
                        End If
                        If Not IsNumeric(Sheet1.Cells(i, 27).Value()) = True Then
                            lblErr.Text = "This spreadsheet has insufficient data to upload automatically" & Sheet1.Cells(i, 27).Value()
                            Exit Sub
                        End If
                        'Check for Badge Number & Grade
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ChildNumber) from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where Ch.FIRST_NAME +' '+ Ch.LAST_NAME ='" & Sheet1.Cells(i, 2).Value().ToString.Replace("'", "''") & "' AND DateDiff(d,ch.Date_OF_Birth,'" & Sheet1.Cells(i, 3).Value() & "')=0 AND C.contestant_id =" & Sheet1.Cells(i, 27).Value() & " and C.BadgeNumber='" & Sheet1.Cells(i, 1).Value() & "' and C.Grade=" & Sheet1.Cells(i, 4).Value() & " AND C.ProductCode = '" & getProductcode(ddlProduct) & "' and C.ChapterID=" & ddlChapter.SelectedItem.Value & "") = 0 Then
                            'lblErr.Text = "ScoreSheet was tampered on row# " & "'" & Sheet1.Cells(i, 1).Value() '& "' i"
                            lblErr.Text = "ScoreSheet was tampered with BadgeNo. " & Sheet1.Cells(i, 1).Value() & " on row#" & i
                            Exit Sub
                        End If

                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & Sheet1.Cells(i, 27).Value() & "")
                        SQLScoreStr = SQLScoreStr & " ContestantID ,ContestID, BadgeNumber,DOB,Grade,ChildNumber,MemberID"
                        SQLScoreStrVal = SQLScoreStrVal & "" & Sheet1.Cells(i, 27).Value() & "," & ddlProduct.SelectedValue & ",'" & Sheet1.Cells(i, 1).Value() & "','" & Sheet1.Cells(i, 3).Value() & "'," & Sheet1.Cells(i, 4).Value() & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ","
                        SQLScoreUpdateStr = SQLScoreUpdateStr & " Update ScoreDetail Set ContestID =" & ddlProduct.SelectedValue & ",BadgeNumber= '" & Sheet1.Cells(i, 1).Value() & "',DOB= '" & Sheet1.Cells(i, 3).Value() & "', Grade=" & Sheet1.Cells(i, 4).Value() & ",ChildNumber=" & dsChildNm.Tables(0).Rows(0)(0) & ",MemberID=" & dsChildNm.Tables(0).Rows(0)(1) & "" ''ContestantID = " & Sheet1.Cells(i, 27).Value() & ",
                        SQLstr = SQLstr & "Update Contestant set "

                        'If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & Sheet1.Cells(i, 27).Value() & " and BadgeNumber='" & Sheet1.Cells(i, 1).Value() & "' and Grade=" & Sheet1.Cells(i, 4).Value() & " AND  Phase1Score is Null  AND Phase2Score is Null ") > 0 Then
                            lblErr.Text = lblErr.Text & "No Phase1,Phase2 Scores Present for the Contestants in Row " & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                            Ph2Flag = True
                        ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*)From ScoreDetail Where ContestantID=" & Sheet1.Cells(i, 27).Value() & "") = 0 Then
                            lblErr.Text = "No Contestant Details found for Phase1"
                            Exit Sub
                        End If
                        'End If

                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ScoreDetail Where  AttendanceFlag like '%N%' and ContestantID=" & Sheet1.Cells(i, 27).Value() & "") > 0 Then
                            lblErr.Text = lblErr.Text & "Contestant in row " & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value() & " was not Present"
                            GoTo EscapValid2
                        End If
                        'If Not Sheet1.Cells(i, Attendance).Value() = "N" Then

                        If score3 > 0 Then
                            SQLstr = SQLstr & " Score3=" & CDbl(0 & Sheet1.Cells(i, score3).Value()) & ","
                            If CDbl(0 & Sheet1.Cells(i, score3).Value()) > 0 Then
                                ScoreExistFlag = True
                            End If

                            If Not IsNumeric(Sheet1.Cells(i, score3).Value()) = True Then
                                Ph3cnt = 1
                                Ph2Flag = True
                                lblErr.Text = lblErr.Text & "Phase3 TotalScore,"
                                SQLScoreStr = SQLScoreStr & ", Phase3Score "
                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase3Score =0"
                            Else
                                SQLScoreStr = SQLScoreStr & ", Phase3Score "
                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score3).Value() & ""
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase3Score =" & Sheet1.Cells(i, score3).Value()
                                ScoreExistFlag = True
                            End If
                            For index = 1 To MaxIndexPhase3 - 1
                                If Not IsNumeric(Sheet1.Cells(i, Ph3_Round(index)).Value()) = True Then
                                    'Ph3cnt = 1
                                    'Ph2Flag = True
                                    'lblErr.Text = lblErr.Text & "Ph3_Round" & index & ","
                                    SQLScoreStr = SQLScoreStr & ",Ph3_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "0,"
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph3_Round" & index & "=0"
                                Else
                                    SQLScoreStr = SQLScoreStr & ",Ph3_Round" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Ph3_Round(index)).Value() & "" ' where ContestantID =" & SheetName.Cells(i, 27).Value() & ""
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph3_Round" & index & "=" & Sheet1.Cells(i, Ph3_Round(index)).Value() & ""
                                    ScoreExistFlag = True
                                End If
                            Next
                        Else
                            SQLstr = SQLstr & "Score3=0 " & ","
                        End If
                        If rank3 > 0 Then
                            If Not IsNumeric(Sheet1.Cells(i, rank3).Value()) = True Then
                                Ph3cnt = 1
                                lblErr.Text = lblErr.Text & "Ph3_Rank3,"
                                SQLScoreStr = SQLScoreStr & ",Ph3_Rank "
                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_Rank = 0"
                            Else
                                If Sheet1.Cells(i, rank3).Value() > 0 Then
                                    SQLScoreStr = SQLScoreStr & ",Ph3_Rank"
                                    SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, rank3).Value() & ""
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_Rank = " & Sheet1.Cells(i, rank3).Value() & ""
                                    ValueExistFlag = True
                                End If
                            End If
                        End If
                        If Phase3TB > 0 Then
                            If Not IsNumeric(Sheet1.Cells(i, Phase3TB).Value()) = True Then
                                'Ph3cnt = 1
                                'lblErr.Text = lblErr.Text & "Phase3TB,"
                                SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score "
                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = 0"
                            Else
                                If Sheet1.Cells(i, Phase3TB).Value() > 0 Then
                                    SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score"
                                    SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Phase3TB).Value() & ""
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = " & Sheet1.Cells(i, Phase3TB).Value() & ""
                                    ValueExistFlag = True
                                Else
                                    SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score "
                                    SQLScoreStrVal = SQLScoreStrVal & ",0"
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = 0"
                                End If
                            End If
                        End If
                        If Ph3cnt = 1 Then
                            lblErr.Text = lblErr.Text.Trim.Trim(",") & " on Row#" & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value() & " <br>"
                            Ph3cnt = 0
                        End If

EscapValid2:
                        If flagcontestid = "" Then
                            flagcontestid = Sheet1.Cells(i, 27).Value()
                        End If

                    Catch ex As Exception
                        lblErr.Text = "Score Sheet Missing Value(s)on Row #" & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                        'Response.Write(ex.ToString())
                    End Try

                    SQLstr = SQLstr & "ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() Where contestant_id=" & Sheet1.Cells(i, 27).Value() & ";"
                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() WHERE ContestantID=" & Sheet1.Cells(i, 27).Value() & ";" '" Update ScoreDetail Set " 

                    Try
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & Sheet1.Cells(i, 27).Value() & "") > 0 Then
                            SQLScoreStrExe = SQLScoreStrExe & SQLScoreUpdateStr
                        Else
                            SQLScoreStrExe = SQLScoreStrExe & "Insert into ScoreDetail(" & SQLScoreStr & ",Createdby,CreatedDate) Values(" & SQLScoreStrVal & "," & Session("LoginID") & ",GETDATE());"
                        End If
                        SQLScoreStr = ""
                        SQLScoreStrVal = ""
                        SQLScoreUpdateStr = ""

                    Catch ex As Exception
                        'Response.Write(ex.ToString())
                        Exit Sub
                    End Try

                    'If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                    ContestantIDValue = ContestantIDValue & "," & Sheet1.Cells(i, 27).Value()
                    'End If
                Next

                ''Flag set during Phase3 Upload
                PhaseUFlag = "P3UList"
                PhaseUVal = "'Y'"

            Else
                ' Uploading and Updation of ScoreSheet For Global and Phase1 Scores
                Dim Phasecnt As Integer
                For i = 7 To Rowcnt - 1
                    Try
                        If Sheet1.Cells(i, 27).Value.ToString() = "" Then
                            Exit For
                        End If
                        If Not IsNumeric(Sheet1.Cells(i, 27).Value()) = True Then
                            lblErr.Text = "This spreadsheet has insufficient data to upload automatically" & Sheet1.Cells(i, 27).Value()
                            Exit Sub
                        End If
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(C.ChildNumber) from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where Ch.FIRST_NAME +' '+ Ch.LAST_NAME ='" & Sheet1.Cells(i, 2).Value().ToString.Replace("'", "''") & "' AND DateDiff(d,ch.Date_OF_Birth,'" & Sheet1.Cells(i, 3).Value() & "')=0 AND C.contestant_id =" & Sheet1.Cells(i, 27).Value() & " and C.BadgeNumber='" & Sheet1.Cells(i, 1).Value() & "' and C.Grade=" & Sheet1.Cells(i, 4).Value() & " AND C.ProductCode = '" & getProductcode(ddlProduct) & "' and C.ChapterID=" & ddlChapter.SelectedItem.Value & "") = 0 Then
                            lblErr.Text = "ScoreSheet was tampered on row# " & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                            Exit Sub
                        End If
                        Dim dsChildNm As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select C.ChildNumber, C.ParentID from Contestant  C Inner Join Child Ch On C.ChildNumber = Ch.ChildNumber and C.ParentID = Ch.MEMBERID  where  C.contestant_id =" & Sheet1.Cells(i, 27).Value() & "")

                        SQLScoreStr = SQLScoreStr & " ContestantID ,ContestID, BadgeNumber,DOB,Grade,ChildNumber,MemberID"
                        SQLScoreStrVal = SQLScoreStrVal & "" & Sheet1.Cells(i, 27).Value() & "," & ddlProduct.SelectedValue & ",'" & Sheet1.Cells(i, 1).Value() & "','" & Sheet1.Cells(i, 3).Value() & "'," & Sheet1.Cells(i, 4).Value() & "," & dsChildNm.Tables(0).Rows(0)(0) & "," & dsChildNm.Tables(0).Rows(0)(1) & ""
                        SQLScoreUpdateStr = SQLScoreUpdateStr & " Update ScoreDetail Set ContestID =" & ddlProduct.SelectedValue & ",BadgeNumber= '" & Sheet1.Cells(i, 1).Value() & "',DOB= '" & Sheet1.Cells(i, 3).Value() & "', Grade=" & Sheet1.Cells(i, 4).Value() & ",ChildNumber=" & dsChildNm.Tables(0).Rows(0)(0) & ",MemberID=" & dsChildNm.Tables(0).Rows(0)(1) & "" ''ContestantID = " & Sheet1.Cells(i, 27).Value() & ",
                        SQLstr = SQLstr & "Update Contestant set "
                        If Attendance > 0 Then
                            SQLScoreStr = SQLScoreStr & ",AttendanceFlag"
                            SQLScoreStrVal = SQLScoreStrVal & ",'" & Sheet1.Cells(i, Attendance).Value() & "'"
                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",AttendanceFlag='" & Sheet1.Cells(i, Attendance).Value() & "'"
                            If Sheet1.Cells(i, Attendance).Value() = "N" Then
                                SQLstr = SQLstr & " Score1=Null, Score2=Null, Score3=Null, Rank= Null,"
                                SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =NULL"
                                For index = 1 To MaxIndexPhase1 - 1
                                    SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                    SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=NULL"
                                Next
                                GoTo EscapValid
                            End If
                        End If

                        If Not ddlTypeofData1.SelectedValue = "0" Then
                            If Attendance > 0 Then
                                If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                                    Try
                                        If Score1 > 0 Then
                                            SQLstr = SQLstr & " Score1=" & CDbl(0 & Sheet1.Cells(i, Score1).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, Score1).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, Score1).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase1Score,"
                                                SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =0"
                                            ElseIf Sheet1.Cells(i, Score1).Value() > 0 Then
                                                SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Score1).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =" & Sheet1.Cells(i, Score1).Value()
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score1=0 " & ","
                                        End If

                                        For index = 1 To MaxIndexPhase1 - 1
                                            If Not IsNumeric(Sheet1.Cells(i, Phase1_TB(index)).Value()) = True Then
                                                'Phasecnt = 1
                                                ' Ph2Flag = True
                                                'lblErr.Text = lblErr.Text & "Phase1_TB" & index & ","
                                                SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Phase1_TB(index)).Value() & "" ' where ContestantID =" & Sheet1.Cells(i, 27).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=" & Sheet1.Cells(i, Phase1_TB(index)).Value() & ""
                                                ScoreExistFlag = True
                                            End If
                                        Next
                                    Catch ex As Exception
                                        lblErr.Text = "ScoreSheet Missing Phase1 Value(s) on Row# " & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                                        'Response.Write(ex.ToString())
                                        Exit Sub
                                    End Try
                                Else
                                    If Sheet1.Cells(i, Attendance).Value().ToString() = "N" Then
                                        SQLstr = SQLstr & "Score1=0 " & ","
                                        SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                        SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =NULL"
                                        For index = 1 To MaxIndexPhase1 - 1
                                            SQLScoreStr = SQLScoreStr & ",Phase1_TB" & index & ""
                                            SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1_TB" & index & "=NULL"
                                        Next
                                    End If
                                End If
                            Else '' Added for Essay Writing and Public Speaking---Attendance not present
                                If Score1 > 0 Then
                                    SQLstr = SQLstr & " Score1=" & CDbl(0 & Sheet1.Cells(i, Score1).Value()) & ","
                                    If CDbl(0 & Sheet1.Cells(i, Score1).Value()) > 0 Then
                                        ScoreExistFlag = True
                                    End If
                                    If Not IsNumeric(Sheet1.Cells(i, Score1).Value()) = True Then
                                        Phasecnt = 1
                                        Ph2Flag = True
                                        lblErr.Text = lblErr.Text & "Phase1Score,"
                                        SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                        SQLScoreStrVal = SQLScoreStrVal & ",0"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =0"
                                    ElseIf Sheet1.Cells(i, Score1).Value() > 0 Then
                                        SQLScoreStr = SQLScoreStr & ",Phase1Score "
                                        SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Score1).Value() & ""
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase1Score =" & Sheet1.Cells(i, Score1).Value()
                                    End If
                                Else
                                    SQLstr = SQLstr & "Score1=0 " & ","
                                End If
                            End If
                        End If
                        If ((ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 2) Or (ddlTypeofData1.SelectedValue = "5" And ddlContest.SelectedValue = 1) Then
                            If Attendance > 0 Then
                                Try
                                    If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                                        If score2 > 0 Then
                                            SQLstr = SQLstr & " Score2=" & CDbl(0 & Sheet1.Cells(i, score2).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, score2).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, score2).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase2Score" '& i
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score2).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & Sheet1.Cells(i, score2).Value() & ""
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score2=0 " & ","
                                        End If
                                        If rank > 0 Then

                                            sRank = Sheet1.Cells(i, rank).Value()

                                            '' extension  ="xlsx"
                                            If (srcfilename.Contains(".xlsx")) Then
                                                Try

                                                    sRank = sheet.GetRow(i - 1).GetCell(rank - 1, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue.ToString()
                                                    'sheet.GetRow(i - 1).Cells(13).NumericCellValue.ToString()
                                                Catch ex As Exception
                                                    sRank = Sheet1.Cells(i, rank).Value()
                                                    errorLog = errorLog & ex.Message.ToString()
                                                End Try
                                            End If

                                            SQLstr = SQLstr & " Rank=" & CInt(0 & sRank) & ","
                                            If Not IsNumeric(sRank) = True Then 'Sheet1.Cells(i, rank).Value()
                                                Phasecnt = 1
                                                lblErr.Text = lblErr.Text & "Rank,"
                                                SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                                SQLScoreStrVal = SQLScoreStrVal & ",0,''"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0, Rank_Alpha = ''"
                                            Else
                                                If sRank > 0 Then 'Sheet1.Cells(i, rank).Value()
                                                    SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                                    SQLScoreStrVal = SQLScoreStrVal & "," & sRank & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & sRank & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"

                                                    If ddlContest.SelectedValue = 2 Then
                                                        SQLScoreStr = SQLScoreStr & ",Final_Rank"
                                                        SQLScoreStrVal = SQLScoreStrVal & "," & sRank 'Sheet1.Cells(i, rank).Value()
                                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Final_Rank = " & sRank 'Sheet1.Cells(i, rank).Value()
                                                    End If
                                                    ValueExistFlag = True
                                                End If
                                            End If

                                        Else
                                            SQLstr = SQLstr & "Rank= 0 " & ","
                                        End If
                                        For index = 1 To MaxIndexPhase2 - 1
                                            If Not IsNumeric(Sheet1.Cells(i, Ph2_Round(index)).Value()) = True Then
                                                'Phasecnt = 1
                                                'Ph2Flag = True
                                                'lblErr.Text = lblErr.Text & "Ph2_Round" & index & ","
                                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=0"

                                            Else
                                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Ph2_Round(index)).Value() & "" ' where ContestantID =" & Sheet1.Cells(i, 27).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=" & Sheet1.Cells(i, Ph2_Round(index)).Value() & ""
                                                ScoreExistFlag = True
                                            End If
                                        Next
                                    Else
                                        If Sheet1.Cells(i, Attendance).Value() = "N" Then
                                            SQLstr = SQLstr & "Score2=0 " & ","
                                            SQLScoreStr = SQLScoreStr & ",Phase2Score ,P1_P2_Rank ,Rank_Alpha"
                                            SQLScoreStrVal = SQLScoreStrVal & ",NULL,NULL,NULL"
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =NULL, P1_P2_Rank = NULL, Rank_Alpha = NULL"
                                            For index = 1 To MaxIndexPhase2 - 1
                                                SQLScoreStr = SQLScoreStr & ",Ph2_Round" & index & ""
                                                SQLScoreStrVal = SQLScoreStrVal & ",NULL"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph2_Round" & index & "=NULL"
                                            Next
                                        End If
                                    End If
                                Catch ex As Exception
                                    lblErr.Text = "Score Sheet Missing Phase2 Value(s) on Row#" & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                                End Try
                            ElseIf ddlContest.SelectedValue = 2 Then
                                ''Added For EW and PS-------Attendance not Present ---Regionals
                                If score2 > 0 Then
                                    SQLstr = SQLstr & " score2=" & CDbl(0 & Sheet1.Cells(i, score2).Value()) & ","
                                    If CDbl(0 & Sheet1.Cells(i, score2).Value()) > 0 Then
                                        ScoreExistFlag = True
                                    End If
                                    If Not IsNumeric(Sheet1.Cells(i, score2).Value()) = True Then
                                        Phasecnt = 1
                                        Ph2Flag = True
                                        lblErr.Text = lblErr.Text & "Phase2Score,"
                                        SQLScoreStr = SQLScoreStr & ",Phase2Score "
                                        SQLScoreStrVal = SQLScoreStrVal & ",0"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                                    ElseIf Sheet1.Cells(i, score2).Value() > 0 Then
                                        SQLScoreStr = SQLScoreStr & ",Phase2Score "
                                        SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score2).Value() & ""
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & Sheet1.Cells(i, score2).Value()
                                    End If
                                Else
                                    SQLstr = SQLstr & "Score2=0 " & ","
                                End If


                                If rank > 0 Then
                                    sRank = Sheet1.Cells(i, rank).Value()
                                    '' extension  ="xlsx"
                                    If (srcfilename.Contains(".xlsx")) Then
                                        Try
                                            sRank = sheet.GetRow(i - 1).GetCell(rank - 1, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue.ToString()
                                            'sheet.GetRow(i - 1).Cells(13).NumericCellValue.ToString()
                                        Catch ex As Exception
                                            sRank = Sheet1.Cells(i, rank).Value()
                                            errorLog = errorLog & ex.Message.ToString()
                                        End Try
                                    End If

                                    SQLstr = SQLstr & " Rank=" & CInt(0 & sRank) & ","
                                    If Not IsNumeric(sRank) = True Then 'Sheet1.Cells(i, rank).Value()
                                        Phasecnt = 1
                                        lblErr.Text = lblErr.Text & "Rank,"
                                        SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                        SQLScoreStrVal = SQLScoreStrVal & ",0,''"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0, Rank_Alpha = ''"
                                    Else
                                        If sRank > 0 Then 'Sheet1.Cells(i, rank).Value()
                                            SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Rank_Alpha"
                                            SQLScoreStrVal = SQLScoreStrVal & "," & sRank & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & sRank & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            'If ddlContest.SelectedValue = 2 Then
                                            SQLScoreStr = SQLScoreStr & ",Final_Rank"
                                            SQLScoreStrVal = SQLScoreStrVal & "," & sRank 'Sheet1.Cells(i, rank).Value()
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ",Final_Rank = " & sRank 'Sheet1.Cells(i, rank).Value()
                                            'End If
                                            ValueExistFlag = True
                                        End If
                                    End If
                                Else
                                    SQLstr = SQLstr & "Rank= 0 " & ","
                                End If
                            End If
                        End If

                        ' Phase2 done Seperately

                        If ((ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 1) Then 'Phase3 Scores
                            'Dim Ph3cnt As Integer

                            If Attendance > 0 Then
                                Try
                                    If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                                        If score2 > 0 Then
                                            SQLstr = SQLstr & " Score2=" & CDbl(0 & Sheet1.Cells(i, score2).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, score2).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, score2).Value()) = True Then
                                                Phasecnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Phase2Score "
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ", Phase2Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score2).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & Sheet1.Cells(i, score2).Value() & ""
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score2=0 " & ","
                                        End If
                                        If score3 > 0 Then
                                            SQLstr = SQLstr & " Score3=" & CDbl(0 & Sheet1.Cells(i, score3).Value()) & ","
                                            If CDbl(0 & Sheet1.Cells(i, score3).Value()) > 0 Then
                                                ScoreExistFlag = True
                                            End If
                                            If Not IsNumeric(Sheet1.Cells(i, score3).Value()) = True Then
                                                'Ph3cnt = 1
                                                'Ph2Flag = True
                                                'lblErr.Text = lblErr.Text & "Phase3 TotalScore,"
                                                SQLScoreStr = SQLScoreStr & ", Phase3Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase3Score =0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ", Phase3Score "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score3).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase3Score =" & Sheet1.Cells(i, score3).Value()
                                                ScoreExistFlag = True
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Score3=0 " & ","
                                        End If

                                        If rank3 > 0 Then
                                            If Not IsNumeric(Sheet1.Cells(i, rank3).Value()) = True Then
                                                Ph3cnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Ph3_Rank,"
                                                SQLScoreStr = SQLScoreStr & ", Ph3_Rank "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph3_Rank =0"
                                            Else
                                                SQLScoreStr = SQLScoreStr & ", Ph3_Rank "
                                                SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, rank3).Value() & ""
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ",Ph3_Rank =" & Sheet1.Cells(i, rank3).Value()
                                                ValueExistFlag = True
                                                'ScoreExistFlag = True
                                            End If
                                        End If

                                        If Phase3TB > 0 Then
                                            If Not IsNumeric(Sheet1.Cells(i, Phase3TB).Value()) = True Then

                                                SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score "
                                                SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = 0"
                                            Else
                                                If Sheet1.Cells(i, Phase3TB).Value() > 0 Then
                                                    SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score"
                                                    SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, Phase3TB).Value() & ""
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = " & Sheet1.Cells(i, Phase3TB).Value() & ""
                                                    ValueExistFlag = True
                                                Else
                                                    SQLScoreStr = SQLScoreStr & ",Ph3_TB_Score "
                                                    SQLScoreStrVal = SQLScoreStrVal & ",0"
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Ph3_TB_Score = 0"
                                                End If
                                            End If
                                        End If
                                        If rank > 0 And score3 <> 0 Then 'Global sheet with Phase1,2,3
                                            sRank = Sheet1.Cells(i, rank).Value()
                                            '' extension  ="xlsx"
                                            If (srcfilename.Contains(".xlsx")) Then
                                                Try
                                                    sRank = sheet.GetRow(i - 1).GetCell(rank - 1, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue.ToString()
                                                    'sheet.GetRow(i - 1).Cells(13).NumericCellValue.ToString()
                                                Catch ex As Exception
                                                    sRank = Sheet1.Cells(i, rank).Value()
                                                    errorLog = errorLog & ex.Message.ToString()
                                                End Try
                                            End If

                                            SQLstr = SQLstr & " Rank=" & CDbl(0 & sRank) & ","
                                            If Not IsNumeric(sRank) = True Then
                                                Ph3cnt = 1
                                                Ph2Flag = True
                                                lblErr.Text = lblErr.Text & "Final_Rank,"
                                                SQLScoreStr = SQLScoreStr & ",Final_Rank ,Rank_Alpha"
                                                SQLScoreStrVal = SQLScoreStrVal & ",0,''"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", Final_Rank = 0,Rank_Alpha=''"
                                            Else
                                                If sRank > 0 Then
                                                    SQLScoreStr = SQLScoreStr & ",Final_Rank,Rank_Alpha,Ph3_TB_Score"
                                                    SQLScoreStrVal = SQLScoreStrVal & "," & sRank & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", Final_Rank = " & sRank & ",Rank_Alpha='" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    ValueExistFlag = True
                                                End If
                                            End If
                                        ElseIf rank > 0 And score3 = 0 Then ' Global sheet with only Phase 1 and Phase 2
                                            sRank = Sheet1.Cells(i, rank).Value()
                                            '' extension  ="xlsx"
                                            If (srcfilename.Contains(".xlsx")) Then
                                                Try
                                                    sRank = sheet.GetRow(i - 1).GetCell(rank - 1, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue.ToString()
                                                    'sheet.GetRow(i - 1).Cells(13).NumericCellValue.ToString()
                                                Catch ex As Exception
                                                    sRank = Sheet1.Cells(i, rank).Value()
                                                    errorLog = errorLog & ex.Message.ToString()
                                                End Try
                                            End If
                                            SQLstr = SQLstr & " Rank=" & CInt(0 & sRank) & ","

                                            If Not IsNumeric(sRank) = True Then
                                                Phasecnt = 1
                                                lblErr.Text = lblErr.Text & "Rank,"
                                                SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Final_Rank,Rank_Alpha"
                                                SQLScoreStrVal = SQLScoreStrVal & ",0,0,''"
                                                SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0,Final_Rank=0, Rank_Alpha = ''"
                                            Else
                                                If sRank > 0 Then
                                                    SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Final_Rank,Rank_Alpha"
                                                    SQLScoreStrVal = SQLScoreStrVal & "," & sRank & "," & sRank & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & sRank & ",Final_Rank=" & sRank & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"
                                                    ValueExistFlag = True
                                                End If
                                            End If
                                        Else
                                            SQLstr = SQLstr & "Rank=0 " & ","
                                        End If
                                    ElseIf Sheet1.Cells(i, Attendance).Value() = "N" Then
                                        SQLstr = SQLstr & "Score2=0 " & ","
                                        SQLScoreStr = SQLScoreStr & ",Phase2Score,Phase3Score ,Ph3_Rank,Final_Rank ,Rank_Alpha"
                                        SQLScoreStrVal = SQLScoreStrVal & ",NULL,NULL,NULL,NULL,NULL"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =NULL, Phase3Score = NULL, Ph3_Rank = NULL,Final_Rank= NULL,Rank_Alpha= NULL"
                                    End If

                                Catch ex As Exception
                                    lblErr.Text = "Score Sheet Missing Phase2 Value(s) on Row#" & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value()
                                End Try
                            ElseIf ddlContest.SelectedValue = 1 Then
                                ''Added For EW and PS-------Attendance not Present 'Finals'
                                If score2 > 0 Then
                                    SQLstr = SQLstr & " score2=" & CDbl(0 & Sheet1.Cells(i, score2).Value()) & ","
                                    If CDbl(0 & Sheet1.Cells(i, score2).Value()) > 0 Then
                                        ScoreExistFlag = True
                                    End If
                                    If Not IsNumeric(Sheet1.Cells(i, score2).Value()) = True Then
                                        Phasecnt = 1
                                        Ph2Flag = True
                                        lblErr.Text = lblErr.Text & "Phase2Score,"
                                        SQLScoreStr = SQLScoreStr & ",Phase2Score "
                                        SQLScoreStrVal = SQLScoreStrVal & ",0"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =0"
                                    ElseIf Sheet1.Cells(i, score2).Value() > 0 Then
                                        SQLScoreStr = SQLScoreStr & ",Phase2Score "
                                        SQLScoreStrVal = SQLScoreStrVal & "," & Sheet1.Cells(i, score2).Value() & ""
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ",Phase2Score =" & Sheet1.Cells(i, score2).Value()
                                    End If
                                Else
                                    SQLstr = SQLstr & "Score2=0 " & ","
                                End If
                                If rank > 0 Then
                                    sRank = Sheet1.Cells(i, rank).Value()
                                    '' extension  ="xlsx" - to read rank 
                                    If (srcfilename.Contains(".xlsx")) Then
                                        Try
                                            sRank = sheet.GetRow(i - 1).GetCell(rank - 1, NPOI.SS.UserModel.MissingCellPolicy.RETURN_NULL_AND_BLANK).NumericCellValue.ToString()
                                            'sheet.GetRow(i - 1).Cells(13).NumericCellValue.ToString()
                                        Catch ex As Exception
                                            sRank = Sheet1.Cells(i, rank).Value()
                                            errorLog = errorLog & ex.Message.ToString()
                                        End Try
                                    End If

                                    SQLstr = SQLstr & " Rank=" & CInt(0 & sRank) & ","

                                    If Not IsNumeric(sRank) = True Then
                                        Phasecnt = 1
                                        lblErr.Text = lblErr.Text & "Rank,"
                                        SQLScoreStr = SQLScoreStr & ",P1_P2_Rank ,Final_Rank,Rank_Alpha"
                                        SQLScoreStrVal = SQLScoreStrVal & ",0,0,''"
                                        SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = 0,Final_Rank=0, Rank_Alpha = ''"
                                    Else
                                        If sRank > 0 Then
                                            SQLScoreStr = SQLScoreStr & ",P1_P2_Rank, Final_Rank,Rank_Alpha"
                                            SQLScoreStrVal = SQLScoreStrVal & "," & sRank & "," & sRank & ",'" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            SQLScoreUpdateStr = SQLScoreUpdateStr & ", P1_P2_Rank = " & sRank & ",Final_Rank=" & sRank & ", Rank_Alpha = '" & Sheet1.Range(rank_alpha & i).Value & "'"
                                            ValueExistFlag = True
                                        End If
                                    End If
                                Else
                                    SQLstr = SQLstr & "Rank= 0 " & ","
                                End If
                            End If
                        End If

                        If Phasecnt = 1 Or Ph3cnt = 1 Then 'Or Ph2Flag = True 
                            lblErr.Text = lblErr.Text.Trim.Trim(",")
                            lblErr.Text = lblErr.Text & " on Row#" & i & " with BadgeNo. " & Sheet1.Cells(i, 1).Value() & "<br>"
                            Phasecnt = 0
                            Ph3cnt = 0
                        End If

EscapValid:

                    Catch ex As Exception
                        'Response.Write(ex.ToString())
                    End Try
                    SQLstr = SQLstr & "ModifiedBy=" & Session("LoginID") & ",ModifiedDate = GETDATE() Where contestant_id=" & Sheet1.Cells(i, 27).Value() & ";"
                    SQLScoreUpdateStr = SQLScoreUpdateStr & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE() ,EventID=" & ddlContest.SelectedValue & ",ChapterID= " & ddlChapter.SelectedValue & ",ContestYear=" & ddlYear.SelectedValue & ",ProductCode='" & getProductcode(ddlProduct) & "',ProductGroupCode= '" & ddlProductGroup.SelectedValue & "',ProductID=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & " ,ProductGroupID=" & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & " WHERE ContestantID=" & Sheet1.Cells(i, 27).Value() & ";" '" Update ScoreDetail Set " 
                    Try
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoreDetail Where ContestantID =" & Sheet1.Cells(i, 27).Value() & "") > 0 Then
                            SQLScoreStrExe = SQLScoreStrExe & SQLScoreUpdateStr '
                        Else
                            SQLScoreStrExe = SQLScoreStrExe & "Insert into ScoreDetail(" & SQLScoreStr & ",Createdby,CreatedDate,EventID,ChapterID,ContestYear,ProductCode,ProductGroupCode,ProductID,ProductGroupID ) Values(" & SQLScoreStrVal & "," & Session("LoginID") & ",GETDATE()," & ddlContest.SelectedValue & "," & ddlChapter.SelectedValue & "," & ddlYear.SelectedValue & ",'" & getProductcode(ddlProduct) & "','" & ddlProductGroup.SelectedValue & "'," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductId from Product Where ProductCode='" & getProductcode(ddlProduct) & "'") & "," & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupID from Product Where ProductGroupCode='" & ddlProductGroup.SelectedValue & "'") & ");" 'Where ContestantID= " & Sheet1.Cells(i, 27).Value()  '
                        End If
                        SQLScoreStr = ""
                        SQLScoreStrVal = ""
                        SQLScoreUpdateStr = ""

                    Catch ex As Exception
                        'Response.Write(ex.ToString())
                    End Try
                    If Not Attendance = 0 Then
                        If Not Sheet1.Cells(i, Attendance).Value() = "N" Then
                            ContestantIDValue = ContestantIDValue & "," & Sheet1.Cells(i, 27).Value()
                        End If
                    End If
                Next
                ''Flag set during Phase1 upload
                If ddlTypeofData1.SelectedValue = "0" Then
                    PhaseUFlag = "P1UAttend"
                ElseIf ddlTypeofData1.SelectedValue = "1" Then
                    PhaseUFlag = "P1UScore"
                ElseIf ((ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 2) Then
                    PhaseUFlag = "P12UList"
                ElseIf ddlTypeofData1.SelectedValue = "5" And ddlContest.SelectedValue = 1 Then
                    PhaseUFlag = "P12UList"
                ElseIf ((ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 1) Then
                    PhaseUFlag = "P123UList"
                End If
                PhaseUVal = "'Y'"
            End If



            ''ScoreSheetLog file.. insertion of data..
            SQLScoreSheetLog = "Insert into ScoreSheetLog (ContestID, ContestYear,FileName,EventID,Event,ChapterID,Chapter,ProductGroupID,ProductID,ProductGroupCode ,ProductCode,TypeofDataID,TypeofData," & PhaseUFlag & ",CreatedBy,CreatedDate"
            SQLScoreSheetLogVal = SQLScoreSheetLogVal & ") Values(" & ddlProduct.SelectedValue & "," & ddlYear.SelectedValue & ",'" & srcfilename & "'," & ddlContest.SelectedValue & ",'" & ddlContest.SelectedItem.Text & "'," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "',(Select DISTINCT ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductGroupCode from Contest Where ContestID=" & ddlProduct.SelectedValue & "),(Select DISTINCT ProductCode from Contest Where ContestID=" & ddlProduct.SelectedValue & ")," & ddlTypeofData1.SelectedValue & ",'" & ddlTypeofData1.SelectedItem.Text & "'," & PhaseUVal & "," & Session("LoginID") & ",GETDATE())"

            SQLScoreStrExe = SQLScoreStrExe & SQLScoreSheetLog & SQLScoreSheetLogVal
            Sheet1.Protect(PWD)
            If ScoreExistFlag = False And (ddlTypeofData1.SelectedValue = "1" Or ddlTypeofData1.SelectedValue = "2") Then
                lblErr.Text = "No Scores found in sheet"
                Exit Sub
            End If
            If ValueExistFlag = False And (ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "5" Or ddlTypeofData1.SelectedValue = "6") Then
                lblErr.Text = "No Ranks found in sheet<br>Please Save the Score Sheet and Proceed"
                Exit Sub
            End If

            'BIDNHU
            ' Exit Sub

            If Ph2Flag = True Then
                lblWarngMsg.Text = "Score Sheet Missing the following Values <br> " & lblErr.Text & "<br>Please Save the Score Sheet and Proceed. "
            Else
                lblWarngMsg.Text = ""
            End If
            lblConfirm.Text = "Warning: Scores already exist, do you want to replace them?"
            If (ddlTypeofData1.SelectedValue = "4") Then
                lblConfirm.Text = "Warning: Scores/Ranks already exist, do you want to replace them?"
                If SQLstr.Length > 10 Then 'And ValueExistFlag = True 
                    Try
                        'SQLstr = SQLstr & "; Update sht set sht.UploadedDate=GETDATE(),sht.UploadedBy=" & Session("LoginID") & " from ScoreSheet sht Inner Join Contestant C On C.ContestID = sht.ContestID Where C.contestant_id = " & flagcontestid

                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from contestant where (score1 is not null OR score2 is not null OR score3 is not null Or Rank is not null ) AND ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "") > 0 Then
                            HdnScoreDetailSQL.Value = SQLScoreStrExe
                            HdnexecQuery.Value = SQLstr
                            lblConfirm.Visible = True
                            trconfirm.Visible = True
                            trAll.Visible = False
                        Else
                            'Commented for Testing 13/03/2012 commented taken on 31/03/2012
                            lblConfirm.Visible = False
                            trconfirm.Visible = False
                            trAll.Visible = True
                            'hidden by Sims
                            ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLstr)
                            '  SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLScoreStrExe)

                            lblErr.Text = "Successfully Uploaded"
                            CheckEnableDFlags()

                        End If
                    Catch ex As Exception
                    End Try

                Else
                    lblErr.Text = "No rank found, Please correct the scores in Scoresheet"
                End If
            Else
                SQLstr = ""
                If ddlTypeofData1.SelectedValue = "0" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where AttendanceFlag Is Not Null and  ContestantID  in (" & ContestantIDValue & ")") > 0 Then
                    lblConfirm.Text = "Warning: Attendance already exist, do you want to replace them?"
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "1" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where Phase1Score Is Not Null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "2" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where  Phase2Score Is Not Null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then ''and AttendanceFlag Is Null
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf (ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "5") And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where P1_P2_Rank is not null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then ' and Rank Is Not Null
                    lblConfirm.Text = "Warning: Ranks already exist, do you want to replace them?"
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "4" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where (Phase1Score Is Not Null Or Phase2Score is Not Null Or P1_P2_Rank is not null) and  ContestantID in (" & ContestantIDValue & ")") > 0 Then ' and Rank Is Not Null
                    lblConfirm.Text = "Warning: Scores / Ranks already exist, do you want to replace them?"
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                ElseIf ddlTypeofData1.SelectedValue = "6" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From ScoreDetail Where Phase3Score Is Not Null and  ContestantID in (" & ContestantIDValue & ")") > 0 Then
                    HdnScoreDetailSQL.Value = SQLScoreStrExe
                    lblConfirm.Visible = True
                    trconfirm.Visible = True
                    trAll.Visible = False
                Else
                    lblConfirm.Visible = False
                    trAll.Visible = True
                    Try
                        ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLScoreStrExe)
                        lblErr.Text = "Successfully Uploaded" '& SQLScoreStrExe
                        CheckEnableDFlags()
                    Catch ex As Exception
                    End Try
                End If
            End If

        Catch ex As Exception

        End Try
        ' lblErr.Text = lblErr.Text & errorLog
        If errorLog.Length > 0 Then
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@northsouth.org")
            email.To.Add("bindhu.rajalakshmi@capestart.com")
            email.Subject = "Error : ScoreSheet"
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = errorLog.ToString()
            'TODO Need to be fixed to get the Attachments file


            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            Try
                '  client.Send(email)
            Catch ex As Exception
                'lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End If
    End Sub


    Protected Sub btnUploadMaster_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadMaster_.Click
        Try
            lblMdwError.Text = ""
            LblMasterErr.Text = ""
            If FileUploadMaster.HasFile Then
                Dim srcfilename As String = FileUploadMaster.FileName
                If Not System.IO.Path.GetExtension(FileUploadMaster.FileName).ToLower = ".xlsx" Then
                    LblMasterErr.Text = "Only Excel file with .xlsx is allowed."
                    Exit Sub
                End If
                Dim checkfile As String() = srcfilename.Split("_")
                Try
                    If checkfile(0).Trim() <> ddlMYear.SelectedValue Then
                        LblMasterErr.Text = "Year Mismatch"
                        Exit Sub
                    ElseIf checkfile(1).Trim() <> IIf(ddlMContest.SelectedValue = 1, "Fin", "Reg") Then
                        LblMasterErr.Text = "Event Mismatch"
                        Exit Sub
                    ElseIf checkfile(2).Trim() <> ddlMProductGroup.SelectedValue Then
                        LblMasterErr.Text = "Contest Mismatch"
                        Exit Sub
                    ElseIf checkfile(3).Trim() <> getProductcode(ddlMProduct) Then
                        LblMasterErr.Text = "Level Mismatch"
                        Exit Sub
                    ElseIf checkfile.Length > 5 And (Not ddlMPhase.SelectedValue = "1") And (Not ddlMPhase.SelectedValue = "2") And (Not ddlMPhase.SelectedValue = "3") And (Not ddlMPhase.SelectedValue = "4") And (Not ddlMPhase.SelectedValue = "5") Then
                        LblMasterErr.Text = "Not a valid Master Scoresheet"
                        Exit Sub
                    ElseIf checkfile(4).Trim().ToLower().Substring(0, 10) <> "scoresheet" Then
                        LblMasterErr.Text = "Missing 'ScoreSheet' in Filename or Not valid"
                        Exit Sub
                    End If
                    If Not ddlMPhase.SelectedValue = "0" And Not ddlMPhase.SelectedValue = "1" Then
                        If ddlMPhase.SelectedValue = "2" Then
                            If checkfile(5).Trim().ToLower().Substring(0, 2) <> "p2" Then
                                LblMasterErr.Text = "Missing 'P2' in filename"
                                Exit Sub
                            End If
                        ElseIf ddlMPhase.SelectedValue = "3" Then
                            If checkfile(5).Trim().ToLower().Substring(0, 2) <> "p3" Then
                                LblMasterErr.Text = "Missing 'P3' in filename"
                                Exit Sub
                            End If
                        ElseIf ddlMPhase.SelectedValue = "4" Then
                            If checkfile(5).Trim().Substring(0, 6) <> "p3List" Then
                                LblMasterErr.Text = "Missing 'p3List' in filename"
                                Exit Sub
                            End If
                        ElseIf ddlMPhase.SelectedValue = "5" Then
                            If checkfile(5).Trim().Substring(0, 7) <> "Toppers" Then
                                LblMasterErr.Text = "Missing 'Toppers' in filename"
                                Exit Sub
                            End If
                        End If
                    End If
                Catch ex As Exception
                    LblMasterErr.Text = "File name is Not in desired format with '_'"
                    Exit Sub
                End Try
                FileUploadMaster.PostedFile.SaveAs(Server.MapPath("ScoreSheets/Master/" & FileUploadMaster.FileName))
                LblMasterErr.Text = "Uploaded Successfully"
            Else
                LblMasterErr.Text = "Please select Master scoresheet"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            LoadProduct()
            If (Session("RoleID") = 16 Or (Session("RoleID") = 18)) Then
                checkContestTeamScheduleByRoleID()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        Try
            If ddlChapter.SelectedItem.Value = 0 Then
                TrConfirmDwnload.Visible = False
                BtnDownload.Enabled = False
                btnUpload.Enabled = False
            Else
                loadproductGroup()

            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub ddlMProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMProductGroup.SelectedIndexChanged
        Try
            LoadMProduct()
        Catch ex As Exception
        End Try
    End Sub

    Function getProductcode(ByVal ddl As DropDownList) As String
        Try


            Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 ProductCode from Product where Name='" & ddl.SelectedItem.Text & "'")
        Catch ex As Exception

        End Try
    End Function

    Protected Sub BtnMDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnMDownload.Click
        Try

            LblMasterErr.Text = ""
            Dim SrcFileName As String
            Dim FileExistFlag As Boolean = False

            SrcFileName = ddlMYear.SelectedValue & "_" & IIf(ddlMContest.SelectedValue = 1, "Fin", "Reg") & "_" & ddlMProductGroup.SelectedValue & "_" & getProductcode(ddlMProduct) & "_ScoreSheet" '-- if the file exists on the server

            If ddlMPhase.Enabled = True Then
                If Not ddlMPhase.SelectedValue = 0 Then
                    If (ddlMPhase.SelectedValue = 2) Or (ddlMPhase.SelectedValue = 3) Then
                        SrcFileName = SrcFileName & "_p" & ddlMPhase.SelectedValue
                    ElseIf ddlMPhase.SelectedValue = 4 Then
                        SrcFileName = SrcFileName & "_p3List"
                    ElseIf ddlMPhase.SelectedValue = 5 Then
                        SrcFileName = SrcFileName & "_Toppers"
                    End If
                End If
            End If

            Dim ext As String = ".xlsx"
            Dim fName As String = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xlsx")
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(fName) '-- if the file exists on the server
            If Not file.Exists Then
                ext = ".xls"
                fName = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls")
                file = New System.IO.FileInfo(fName)
            End If

            If file.Exists Then 'set appropriate headers
                Response.Clear()
                Response.AddHeader("Content-Disposition", "attachment; filename=" & SrcFileName & ext)
                If ext = "xls" Then
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Else
                    Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                End If

                Response.AddHeader("Content-Length", file.Length.ToString())
                ' Response.ContentType = "application/octet-stream"

                If ext = "xls" Then
                    Response.WriteFile(file.FullName, XlFileFormat.xlExcel97)
                Else
                    Response.WriteFile(file.FullName, XlFileFormat.xlOpenXMLWorkbook)
                End If
                Response.End()
            Else
                lblMdwError.Text = "Master File Does not Exist"
            End If
            LblMasterErr.Text = ""
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        getevent()
        LoadContests()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        'check whether downloaded
        Try
            checkscorehsheetdown()
            CheckVolChildContests(ddlProduct.SelectedValue)

            If (Session("RoleID") = 16 Or (Session("RoleID") = 18)) Then
                checkContestTeamScheduleByRoleID()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub checkContestTeamScheduleByRoleID()
        Try
            Dim CmdText As String = String.Empty
            Dim iCount As Integer = 0
            CmdText = "Select COUNT( Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.Phase=2 and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") " 'and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))"
            If Session("RoleID") = 16 Then
                CmdText = CmdText & " and  C.CJMemberId in(" & Session("LoginID") & ")"
            ElseIf Session("RoleID") = 18 Then
                CmdText = CmdText & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
            End If
            iCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, CmdText)

            Dim ds As DataSet = New DataSet()

            If (iCount > 0) Then
                TrPhase.Visible = True
                BtnDownload.Enabled = True
                ddlPhase.Enabled = True
                For j As Integer = 0 To ddlPhase.Items.Count - 1
                    If Session("RoleId") = 16 Or Session("RoleId") = 18 Then
                        If j = 1 Or j = 2 Then
                            ddlPhase.Items(j).Enabled = True
                        Else
                            ddlPhase.Items(j).Enabled = False
                        End If
                    Else
                        ddlPhase.Items(j).Enabled = False
                    End If
                Next

                If ddlPhase.SelectedValue = 3 And (hdnP12ScoreFlag.Value = False And hdnP1AttendanceFlag.Value = False And hdnP1ScoreFlag.Value = False) Then
                    BtnDownload.Enabled = False
                    lblErr.Text = "Phase1 Attendance not yet Uploaded"
                Else
                    BtnDownload.Enabled = True
                    lblErr.Text = ""
                End If
            Else
                TrPhase.Visible = True
                ddlPhase.Enabled = False
                BtnDownload.Enabled = False
                For j As Integer = 0 To ddlPhase.Items.Count - 1
                    If Session("RoleId") = 16 Or Session("RoleId") = 18 Then
                        ddlPhase.Items(j).Enabled = False

                    End If
                Next

                lbldwError.Text = "You were not assigned to download data."
                lblErr.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    Private Sub checkscorehsheetdown()
        Try
            lblUploadCondn.Text = "Score Sheet with the Contestant List was not downloaded. You cannot upload until this is done."
            '"Upload button will show up after score sheet is downloaded, exit the application and come back again."
            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            If ddlProduct.Items.Count > 0 Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) from ScoresheetLog where ContestID=" & ddlProduct.SelectedValue) > 0 Then
                    FileUpload.Visible = True
                    btnUpload.Visible = True
                    trupload.Visible = True
                    lblUploadCondn.Text = ""
                    LoadUpPhase(False)
                    CheckEnableUFlags(False)
                Else
                    trupload.Visible = False
                    FileUpload.Visible = False
                    btnUpload.Visible = False
                    TrTypeofData1.Visible = False
                    TrTopRank.Visible = False
                    TrRoom1.Visible = False
                End If
            Else
                trupload.Visible = False
                FileUpload.Visible = False
                btnUpload.Visible = False
                TrTypeofData1.Visible = False

                TrRoom1.Visible = False
                lblErr.Text = "No files Downloaded"
            End If
            If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 93 Or Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                FileUpload.Visible = True
                btnUpload.Visible = True
                trupload.Visible = True
                lblUploadCondn.Text = ""
                LoadUpPhase(False)
            End If
            LoadPhase()
            If TrPhase.Visible = False Then  '25-03-2014
                ddlPhase.SelectedValue = "1"  '25-03-2014
            End If
            If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 93 Or Session("RoleID") = 16 Or Session("RoleID") = 18 Then '
                CheckEnableUFlags(False)
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub BtnConfirm_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConfirm.Click
        Try
            If HdnexecQuery.Value.Length > 0 Then
                If ddlTypeofData1.SelectedValue = "4" Then
                    ''Commented for Testing 13/03/2012 - Comment removed on 31/03/2012
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update Contestant set Score1=Null, Score2=Null , Score3=Null , RANK=Null  where ProductCode = '" & getProductcode(ddlProduct) & "' and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and EventId=" & ddlContest.SelectedValue & "")  'Contestant_id in(" & hdncontestantIds.Value & ")")
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, HdnexecQuery.Value)
                    lblErr.Text = "Successfully Uploaded"
                End If
            End If
            If HdnScoreDetailSQL.Value.Length > 0 Then
                If ddlTypeofData1.SelectedValue = "6" Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update ScoreDetail set Phase3Score=Null,Ph3_Rank=Null,Ph3_TB_Score=Null, Final_Rank=Null , Rank_Alpha=Null where ProductCode = '" & getProductcode(ddlProduct) & "' and EventID=" & ddlContest.SelectedValue & " and ChapterID=" & ddlChapter.SelectedItem.Value & " and ContestYear=" & ddlYear.SelectedValue & " and BadgeNumber is not null and ContestID=" & ddlProduct.SelectedValue & "")  'Contestant_id in(" & hdncontestantIds.Value & ")")
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, HdnScoreDetailSQL.Value)
                lblErr.Text = "Successfully Uploaded"
            End If
            lblWarngMsg.Text = ""
            lblConfirm.Text = ""
            trconfirm.Visible = False
            trAll.Visible = True
            HdnexecQuery.Value = ""
            HdnScoreDetailSQL.Value = ""
            CheckEnableDFlags()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        lblWarngMsg.Text = ""
        HdnexecQuery.Value = ""
        HdnScoreDetailSQL.Value = ""
        trconfirm.Visible = False
        trAll.Visible = True
    End Sub
    Private Sub LoadRoom(ByVal NRooms As Integer)
        Try
            Dim i As Integer
            ddlRoom.Items.Clear()
            For i = 0 To NRooms - 1
                ddlRoom.Items.Insert(i, New ListItem(i + 1, i + 1))
            Next

            If Session("RoleID") = 16 And Session("RoleID") = 18 Then
                ddlRoom.SelectedValue = GetCode(ddlRoom_Phase.SelectedValue.ToString())(0) 'ddlRoom_Phase.SelectedValue
            ElseIf Session("RoleID") <> 16 And Session("RoleID") <> 18 Then
                ddlRoom.Items.Insert(i, New ListItem("ALL", "ALL"))
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadUpRoom(ByVal NRooms As Integer)
        Try
            ddlRoom1.Items.Clear()
            For i As Integer = 0 To NRooms - 1
                ddlRoom1.Items.Insert(i, New ListItem(i + 1, i + 1))
            Next
            If Session("RoleID") = 16 And Session("RoleID") = 18 Then
                ddlRoom1.SelectedValue = GetCode(ddlRoom_Phase.SelectedValue.ToString())(0) 'ddlRoom_Phase.SelectedValue
            End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        Try
            If Session("RoleID") <> 16 And Session("RoleID") <> 18 Then
                LoadPhase()
                If TrPhase.Visible = False Then  '25-03-2014
                    ddlPhase.SelectedValue = "1"  '25-03-2014
                End If
            Else
                CheckEnableDFlags()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub ddlTypeofData1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTypeofData1.SelectedIndexChanged
        LoadUpPhase(True)
        CheckEnableUFlags(False)
    End Sub
    Private Sub LoadPhase()
        Try
            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            BtnDownload.Enabled = True
            'TrTopRank.Visible = False
            TrConfirmDwnload.Visible = False
            Dim StrSQL_NRooms As String = ""
            Dim NRooms As Integer = 0
            ''Added to test Ph2split flag of Score Sheet
            Dim SrcFileName As String
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
            While readr.Read()
                SrcFileName = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet" '"NSFSS" & Now.Year.ToString() & "_" & readr("ProductCode") & ".xls"
            End While
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xlsx")) '-- if the file exists on the server
            Dim fName As String = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xlsx")
            If Not file.Exists Then
                fName = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls")
                file = New System.IO.FileInfo(Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls"))
            End If

            If Not file.Exists Then
                lbldwError.Text = "Sorry Master file does not exist"
                Exit Sub
            End If

            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(fName)
            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)

            If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ContestCategory Where (Phase2_Reg='Y' Or Phase3_Reg='Y') and ContestYear=" & ddlYear.SelectedValue & " and ProductGroupCode= '" & ddlProductGroup.SelectedItem.Value & "' and ContestCode= '" & getProductcode(ddlProduct) & "'") > 0 Then
                If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                    '*****Commented and updtated on 10-06-2014 to show Rooms/Phase for RoleID=16,18 and LoadPhase after loading Rooms.**********************************************'
                    StrSQL_NRooms = "Select COUNT( Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") " 'and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))"
                    If Session("RoleID") = 16 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and  C.CJMemberId in(" & Session("LoginID") & ")"
                    ElseIf Session("RoleID") = 18 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
                    End If
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL_NRooms) '"Select COUNT(Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")  'And C.Phase = 2
                    'End If
                ElseIf NRooms <= 0 Then
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph2Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                End If
            Else
                NRooms = 1
            End If
            ''Added to Check Ph2split Flag of the Score Sheet to be 'Y' to allow Phase2 split for Rooms.
            If Sheet1.Range(4, 28).Value <> "Y" Then
                NRooms = 1
            End If

            LoadDDLRanks()
            If NRooms > 0 And (Session("RoleId") = 16 Or Session("RoleId") = 18) Then
                TrRoom_phase.Visible = True
                TrPhase.Visible = False
                '***********Added to Load Phase based on SeqNo from ContestTeamSchedule table for RoleIDs 16,18*******************************************************'.
                Dim strRoom_Phase As String = "Select Distinct 'R' + cast(c.SeqNo as nvarchar(10))+ '_Phase' + cast(c.Phase as nvarchar(10)) as Room_Phase,cast(c.seqno as varchar(10)) + '_' + Cast(c.Phase as varchar(10)) as SeqNo_Phase From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") " 'and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))"   'And C.Phase = 2

                If Session("RoleID") = 16 Then
                    strRoom_Phase = strRoom_Phase & " and C.CJMemberId in(" & Session("LoginID") & ")"
                ElseIf Session("RoleID") = 18 Then
                    strRoom_Phase = strRoom_Phase & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
                End If

                Dim dsRoom_Phase As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strRoom_Phase)
                If dsRoom_Phase.Tables(0).Rows.Count > 0 Then
                    ddlRoom_Phase.DataSource = dsRoom_Phase
                    ddlRoom_Phase.DataTextField = "Room_Phase"
                    ddlRoom_Phase.DataValueField = "SeqNo_Phase"
                    ddlRoom_Phase.DataBind()
                    If ddlRoom_Phase.Items.Count = 1 Then
                        LoadRoom_Phase_New()
                        LoadRoom(NRooms)
                    Else
                        ddlRoom_Phase.Items.Insert(0, New ListItem("Select Room/Phase", 0))
                        ddlRoom_Phase.SelectedIndex = 0
                    End If
                End If
                '*****************************************************************************************************************************************************'
            ElseIf NRooms > 1 Then
                TrPhase.Visible = True
                For j As Integer = 0 To ddlPhase.Items.Count - 1
                    If Session("RoleId") = 16 Or Session("RoleId") = 18 Then
                        If j <> 1 And j <> 2 Then
                            ddlPhase.Items(j).Enabled = False
                        End If
                    Else
                        ddlPhase.Items(j).Enabled = True
                    End If
                Next
                If ddlContest.SelectedValue = 2 Then
                    ddlPhase.Items(4).Enabled = False
                    ddlPhase.Items(5).Enabled = False
                    ddlPhase.Items(6).Enabled = False
                End If
                LoadRoom(NRooms)
                If (ddlPhase.SelectedValue = "2" Or ddlPhase.SelectedValue = "3") Then  'And
                    TrRoom.Visible = True
                Else
                    TrRoom.Visible = False
                End If
            ElseIf NRooms = 1 Then
                TrPhase.Visible = False
                TrRoom.Visible = False
                TrTopRank.Visible = False
                ddlRoom.DataSource = Nothing
                ddlRoom.DataBind()
            End If
            CheckEnableDFlags()
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadRoom_Phase_New()
        Try
            TrPhase.Visible = True

            lbldwError.Text = ""
            ddlPhase.Enabled = False
            TrTypeofData1.Visible = True
            ddlTypeofData1.Enabled = False


            If ddlRoom_Phase.SelectedItem.Text.Contains("Phase1") Then
                ddlPhase.SelectedValue = 1
                BtnDownload.Enabled = True

                ddlTypeofData1.SelectedValue = 0

            ElseIf ddlRoom_Phase.SelectedItem.Text.Contains("Phase2") Then
                ddlPhase.SelectedValue = 3
                BtnDownload.Enabled = True

                ddlTypeofData1.SelectedValue = 2
            ElseIf ddlRoom_Phase.SelectedItem.Text.Contains("Phase3") Then

                TrPhase.Visible = False
                BtnDownload.Enabled = False
                lbldwError.Text = "No download option available for Phase 3"

                ddlTypeofData1.SelectedValue = 6
            End If

            CheckEnableUFlags(True)
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadUpPhase(ByVal UpFlag As Boolean)
        Try
            lbldwError.Text = ""
            lblErr.Text = ""
            lblWarngMsg.Text = ""
            FileUpload.Enabled = True
            btnUpload.Enabled = True
            TrTypeofData1.Visible = False
            TrRoom1.Visible = False
            TrConfirmDwnload.Visible = False
            Dim NRooms As Integer = 0
            Dim StrSQL_NRooms As String = ""
            ''Added to test Ph2split flag of Score Sheet
            Dim SrcFileName As String
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
            While readr.Read()
                SrcFileName = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet"
            End While
            Dim fName As String = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xlsx")
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(fName) '-- if the file exists on the server 

            If Not file.Exists Then
                fName = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls")
                file = New System.IO.FileInfo(fName)
            End If

            If Not file.Exists Then
                lbldwError.Text = "Sorry Master file does not exist"
                Exit Sub
            End If
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(fName)
            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)

            ''Testing ContestCategory for Phase2 ,Phase3  
            If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ContestCategory Where (Phase2_Reg='Y' Or Phase3_Reg='Y') and ContestYear=" & ddlYear.SelectedValue & " and ProductGroupCode= '" & ddlProductGroup.SelectedItem.Value & "' and ContestCode= '" & getProductcode(ddlProduct) & "'") > 0 Then
                If Session("RoleID") = 16 Or Session("RoleID") = 18 Then

                    StrSQL_NRooms = "Select COUNT(Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ")" ' and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")  'And C.Phase = 2
                    If Session("RoleID") = 16 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and C.CJMemberId in(" & Session("LoginID") & ")"
                    ElseIf Session("RoleID") = 18 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
                    End If
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL_NRooms) ' "Select COUNT(Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")  'And C.Phase = 2

                ElseIf NRooms <= 0 Then
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph2Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                End If
            Else
                NRooms = 1
            End If

            ''Added to Check Ph2split Flag of the Score Sheet to be 'Y' to allow Phase2 split for Rooms.
            If Sheet1.Range(4, 28).Value <> "Y" Then
                NRooms = 1
            End If
            If NRooms > 0 And (Session("RoleId") = 16 Or Session("RoleId") = 18) Then
                TrRoom_phase.Visible = True
                TrTypeofData1.Visible = False
                '***********Added to Load Phase based on SeqNo from ContestTeamSchedule table for RoleIDs 16,18*******************************************************'.
                If ddlRoom_Phase.Items.Count = 1 Then
                    LoadRoom_Phase_New()
                    LoadUpRoom(NRooms)
                Else
                    ddlRoom_Phase.Items.Insert(0, New ListItem("Select Room/Phase", 0))
                    ddlRoom_Phase.SelectedIndex = 0
                End If

            ElseIf NRooms > 1 Then
                TrTypeofData1.Visible = True
                For j As Integer = 0 To ddlTypeofData1.Items.Count - 1
                    If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                        If j <> 0 And j <> 2 Then
                            ddlTypeofData1.Items(j).Enabled = False
                        End If
                    Else
                        ddlTypeofData1.Items(j).Enabled = True
                    End If
                Next
                If ddlContest.SelectedValue = 2 Then
                    ddlTypeofData1.Items(3).Enabled = False
                    ddlTypeofData1.Items(4).Enabled = False
                End If
                If UpFlag = False Then
                    ddlTypeofData1.SelectedIndex = 0
                End If
                LoadUpRoom(NRooms)
                If ddlTypeofData1.SelectedValue = "2" Then
                    TrRoom1.Visible = True
                Else
                    TrRoom1.Visible = False
                End If

                If NRooms > 1 And (Session("RoleID") = 16 Or Session("RoleId") = 18) Then
                    TrRoom1.Visible = True
                End If

            ElseIf NRooms = 1 Then
                TrTypeofData1.Visible = True
                For j As Integer = 0 To ddlTypeofData1.Items.Count - 1
                    ddlTypeofData1.Items(j).Enabled = False
                Next
                ddlTypeofData1.Items(5).Enabled = True
                ddlTypeofData1.Items(6).Enabled = True
                If UpFlag = False Then
                    ddlTypeofData1.SelectedIndex = 6
                End If
                TrRoom1.Visible = False
                ddlRoom1.DataSource = Nothing
                ddlRoom1.DataBind()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub CheckEnableDFlags()
        Try
            lbldwError.Text = ""
            ddlPhase.Enabled = True
            Dim P1AttendFlag As Boolean = False
            Dim P1ScoreFlag As Boolean = False
            Dim P2ScoreFlag As Boolean = False
            Dim P3ScoreFlag As Boolean = False
            Dim P12ScoreFlag As Boolean = False
            Dim P123ScoreFlag As Boolean = False
            Dim RoomUp As Integer = 0

            Dim readrddl As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Distinct P1UAttend,P1UScore,P2UList,P12UList,P3UList,P123UList,isNull(RoomNumberU,-1) as RoomNumberU From ScoreSheetLog Where ContestID= " & ddlProduct.SelectedValue & " And EventID=" & ddlContest.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & " And ContestYear=" & ddlYear.SelectedValue & "")
            While readrddl.Read()
                If readrddl("P1UAttend").ToString = "Y" Then
                    P1AttendFlag = True
                End If
                If readrddl("P1UScore").ToString = "Y" Then
                    P1ScoreFlag = True
                End If
                If readrddl("P3UList").ToString = "Y" Then
                    P3ScoreFlag = True
                End If
                If readrddl("P12UList").ToString = "Y" Then
                    P12ScoreFlag = True
                End If
                If readrddl("P123UList").ToString = "Y" Then
                    P123ScoreFlag = True
                End If
                For j As Integer = 0 To ddlRoom.Items.Count - 1
                    If readrddl("RoomNumberU") = j Then
                        RoomUp = RoomUp + 1
                        Exit For
                    Else
                        RoomUp = RoomUp
                    End If
                Next
            End While
            readrddl.Close()
            If RoomUp = ddlRoom.Items.Count - 1 Then '' All occurs in download part
                P2ScoreFlag = True
            Else
                P2ScoreFlag = False
            End If

            hdnP12ScoreFlag.Value = P12ScoreFlag
            hdnP1AttendanceFlag.Value = P1AttendFlag
            hdnP12ScoreFlag.Value = P1ScoreFlag
            If TrPhase.Visible = True Then
                If ddlPhase.SelectedValue <> "1" And ddlPhase.SelectedValue <> "2" Then '  Or ddlPhase.SelectedValue = 5 

                    If ddlPhase.SelectedValue = 3 And (P12ScoreFlag = False And P1AttendFlag = False And P1ScoreFlag = False) And (Session("RoleId") <> 18 And Session("RoleID") <> 16) Then
                        lbldwError.Text = "Phase1 Attendance not yet Uploaded"
                        TrRoom.Visible = False
                        BtnDownload.Enabled = False
                        Exit Sub
                    Else
                        BtnDownload.Enabled = True
                    End If

                    If (ddlPhase.SelectedValue = 5 Or ddlPhase.SelectedValue = 9) Then
                        If (P1ScoreFlag = False Or P2ScoreFlag = False) Then
                            lbldwError.Text = "Phase1score and Phase2 score not yet uploaded"
                            TrTopRank.Visible = False
                            BtnDownload.Enabled = False
                            Exit Sub
                        Else
                            BtnDownload.Enabled = True
                            If ddlPhase.SelectedValue = 5 Or ddlPhase.SelectedValue = 9 Then
                                TrTopRank.Visible = True
                            End If
                        End If
                    End If

                    If (ddlPhase.SelectedValue = 4 Or ddlPhase.SelectedValue = 10) And ddlContest.SelectedValue = 2 Then
                        If (P12ScoreFlag = False) And (P1ScoreFlag = False Or P2ScoreFlag = False) Then
                            lbldwError.Text = "Phase1,Phase2 Composite Data not yet uploaded"
                            TrTopRank.Visible = False
                            BtnDownload.Enabled = False
                            Exit Sub
                        End If
                    End If

                    If (ddlPhase.SelectedValue = 5 Or ddlPhase.SelectedValue = 9) Then
                        If (P12ScoreFlag = False) Then
                            lbldwError.Text = "Ranks after Phase1,Phase2 not yet uploaded"
                            TrTopRank.Visible = False
                            BtnDownload.Enabled = False
                            Exit Sub
                        Else
                            BtnDownload.Enabled = True
                            TrTopRank.Visible = True
                        End If
                    End If
                    If (ddlPhase.SelectedValue = 6) Then
                        If (P12ScoreFlag = False Or P3ScoreFlag = False) Then
                            lbldwError.Text = "Ranks after Phase1,Phase2 (Or) Phase3 Score not yet uploaded"
                            BtnDownload.Enabled = False
                            Exit Sub
                        Else
                            BtnDownload.Enabled = True
                        End If
                    End If
                    If (ddlPhase.SelectedValue = 7 Or ddlPhase.SelectedValue = 8) And ddlContest.SelectedValue = 2 Then
                        If P12ScoreFlag = False Then
                            lbldwError.Text = "Ranks for Certificates not yet uploaded"
                            BtnDownload.Enabled = False
                            TrTopRank.Visible = False
                            Exit Sub
                        Else
                            BtnDownload.Enabled = True
                            TrTopRank.Visible = True
                        End If
                    End If
                    If (ddlPhase.SelectedValue = 7 Or ddlPhase.SelectedValue = 8) And ddlContest.SelectedValue = 1 Then
                        If P123ScoreFlag = False Then
                            lbldwError.Text = "Ranks for Certificates not yet uploaded"
                            BtnDownload.Enabled = False
                            TrTopRank.Visible = False
                            Exit Sub
                        Else
                            BtnDownload.Enabled = True
                            TrTopRank.Visible = True
                        End If
                    End If
                End If

                LoadDDLRanks()

            ElseIf TrPhase.Visible = False Then
                If (P12ScoreFlag = True And ddlContest.SelectedValue = 2) Or (P123ScoreFlag = True And ddlContest.SelectedValue = 1) Then  'And ddlContest.SelectedValue = 2 Then
                    If Session("RoleID") <> 16 And Session("RoleID") <> 18 Then
                        TrPhase.Visible = True
                        ddlPhase.Enabled = True
                        For j As Integer = 1 To 6 'ddlPhase.Items.Count - 1
                            ddlPhase.Items(j).Enabled = False
                        Next
                        If Session("RoleId") = 1 Or Session("RoleId") = 2 Or Session("RoleId") = 93 Then
                            ddlPhase.Items(3).Enabled = True
                        End If
                        ddlPhase.Items(7).Enabled = True
                        ddlPhase.Items(8).Enabled = True
                        Session("DnFlag") = True
                        LoadDDLRanks()
                    End If
                End If
            End If
            If Session("RoleID") = "18" Or Session("RoleID") = "16" Then
                ddlPhase.Enabled = False
                If TrPhase.Visible = False Then
                    BtnDownload.Enabled = False
                Else
                    'Response.Write("Select c.SeqNo From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.Phase=" & ddlPhase.SelectedValue & " and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")
                    If (P12ScoreFlag = False And P1AttendFlag = False And P1ScoreFlag = False) Then
                        'ddlPhase.Items(2).Enabled = False
                        'ddlPhase.Items(1).Enabled = True
                    Else
                        'ddlPhase.Items(1).Enabled = False
                        'ddlPhase.Items(2).Enabled = True
                    End If
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct c.SeqNo From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and  (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))") 'C.Phase=2 and)

                    '**********Added on 10-06-2014*******************************************************'
                    If ds.Tables(0).Rows.Count > 0 Then
                        BtnDownload.Enabled = True
                        TrPhase.Visible = True
                        ddlPhase.Enabled = True
                    Else
                        BtnDownload.Enabled = False
                        lbldwError.Text = "No rooms assigned to show"
                        Exit Sub
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Private Sub CheckEnableUFlags(ByVal UpFlag As Boolean)
        Try
            Dim readrddl As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "Select Distinct P1DList,P2DList,P3DList,P12DList,P123DList,isNull(RoomNumberU,-1) as RoomNumberU,isNull(RoomNumberD,-1) as RoomNumberD,RoomAllD,TopListD,RankCertificateD From ScoreSheetLog Where ContestID= " & ddlProduct.SelectedValue & " And EventID=" & ddlContest.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & "And ContestYear=" & ddlYear.SelectedValue & "")
            Dim P1DFlag As Boolean = False
            Dim P2DFlag As Boolean = False
            Dim P12DFlag As Boolean = False
            Dim P2AllDFlag As Boolean = False
            Dim P3DFlag As Boolean = False
            Dim P123DFlag As Boolean = False
            Dim TopListD As Boolean = False
            Dim RankCertificateD As Boolean = False
            Dim RoomD As Integer
            Dim NRooms As Integer = 0
            Dim StrSQL_NRooms As String = ""
            ''Added to test Ph2split flag of Score Sheet
            Dim SrcFileName As String
            Dim readr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select C.Contest_Year,p.ProductGroupCode , P.ProductCode ,Replace(Replace(Ch.ChapterCode,',','_'),' ','') as Chaptercode,Replace(convert(varchar, contestdate, 111),'/','')  as Contestdate from Contest C Inner Join Chapter Ch ON C.NSFChapterID = Ch.ChapterID Inner Join Product P On C.ProductId = p.ProductId   where C.ContestID = " & ddlProduct.SelectedValue & "")
            While readr.Read()
                SrcFileName = readr("Contest_Year").ToString().Trim() & "_" & IIf(ddlContest.SelectedValue = 1, "Fin", "Reg") & "_" & readr("ProductGroupCode") & "_" & readr("ProductCode") & "_ScoreSheet"
            End While
            Dim fName As String = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xlsx")
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(fName) '-- if the file exists on the server
            If Not file.Exists Then
                fName = Server.MapPath("ScoreSheets\Master\" & SrcFileName & ".xls")
                file = New System.IO.FileInfo(fName)
            End If

            If Not file.Exists Then
                lbldwError.Text = "Sorry Master file doesnot exist"
                Exit Sub
            End If
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.OpenWorkbook(fName)
            Dim Sheet1 As IWorksheet
            Sheet1 = xlWorkBook.Worksheets(1)
            If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Count(*) From ContestCategory Where (Phase2_Reg='Y' Or Phase3_Reg='Y') and ContestYear=" & ddlYear.SelectedValue & " and ProductGroupCode= '" & ddlProductGroup.SelectedItem.Value & "' and ContestCode= '" & getProductcode(ddlProduct) & "'") > 0 Then
                'NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph2Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                If Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                    StrSQL_NRooms = "Select COUNT(Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ")" ' and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")  'And C.Phase = 2
                    If Session("RoleID") = 16 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and C.CJMemberId in(" & Session("LoginID") & ")"
                    ElseIf Session("RoleID") = 18 Then
                        StrSQL_NRooms = StrSQL_NRooms & " and C.LTJudMemberId in(" & Session("LoginID") & ") "
                    End If
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL_NRooms) ' "Select COUNT(Distinct c.SeqNo) From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & "))")  'And C.Phase = 2

                ElseIf NRooms <= 0 Then
                    NRooms = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select IsNull(Ph2Rooms,1) from Contest where contestId=" & ddlProduct.SelectedValue)
                End If
            Else
                NRooms = 1
            End If

            ''Added to Check Ph2split Flag of the Score Sheet to be 'Y' to allow Phase2 split for Rooms.
            If Sheet1.Range(4, 28).Value <> "Y" Then
                NRooms = 1
            End If

            Dim RoomNumberIndex As Integer
            Dim RoomDL As Integer = 0
            For RoomD = 0 To ddlRoom1.Items.Count - 1
                ddlRoom1.Items(RoomD).Enabled = False
            Next

            While readrddl.Read()
                RoomNumberIndex = readrddl("RoomNumberD") - 1
                If readrddl("P1DList").ToString = "Y" Then
                    P1DFlag = True
                End If
                If readrddl("P2DList").ToString = "Y" Then
                    P2DFlag = True
                End If
                If readrddl("P3DList").ToString = "Y" Then
                    P3DFlag = True
                End If
                If readrddl("P12DList").ToString = "Y" Then
                    P12DFlag = True
                End If
                If readrddl("P123DList").ToString = "Y" Then
                    P123DFlag = True
                End If
                If readrddl("TopListD").ToString = "Y" Then
                    TopListD = True
                End If
                If readrddl("RankCertificateD").ToString = "Y" Then
                    RankCertificateD = True
                End If
                If readrddl("RoomAllD").ToString = "Y" Or Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 9 Then ''Roles added on 06-08-2014 to enable all Rooms of Phase2  for upload for RoledID =1,2,9
                    For RoomD = 0 To ddlRoom1.Items.Count - 1
                        ddlRoom1.Items(RoomD).Enabled = True
                    Next
                    P2AllDFlag = True
                Else
                    For RoomD = 0 To ddlRoom1.Items.Count - 1
                        If RoomNumberIndex = RoomD Then
                            ddlRoom1.Items(RoomD).Enabled = True
                            RoomDL = RoomDL + 1
                            Exit For
                        Else
                            RoomDL = RoomDL
                        End If
                    Next
                    If RoomDL = ddlRoom1.Items.Count Then
                        P2AllDFlag = True
                    End If
                End If
            End While

            If Session("RoleID") <> 16 And Session("RoleID") <> 18 Then
                If ddlTypeofData1.SelectedValue = "2" And P2DFlag = False Then 'And (Session("RoleID") <> 16 And Session("RoleID") <> 18) Then
                    lblErr.Text = "Phase2 Contestant List Not yet Downloaded"
                    TrRoom1.Visible = False
                    btnUpload.Enabled = False
                    FileUpload.Enabled = False
                    Exit Sub
                ElseIf ddlTypeofData1.SelectedValue = "6" And P3DFlag = False Then
                    lblErr.Text = "Phase3 Contestant List Not yet Downloaded"
                    btnUpload.Enabled = False
                    FileUpload.Enabled = False
                    Exit Sub
                Else
                    btnUpload.Enabled = True
                    FileUpload.Enabled = True
                End If

                If Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 9 Or Session("RoleID") = 93 Then 'Or Session("RoleID") = 16 Or Session("RoleID") = 18 Then
                    lblUploadCondn.Text = ""
                    FileUpload.Visible = True
                    FileUpload.Enabled = True
                    btnUpload.Visible = True
                    btnUpload.Enabled = True
                    trupload.Visible = True
                    If UpFlag = True Then
                        If P123DFlag = False And P12DFlag = False And P1DFlag = False Then 'And ddlContest.SelectedValue = 2) Then
                            'lblErr.Text = "Blank Score Sheet with contestant list was not downloaded.  Do you still want to upload?  Please click  Yes or No"
                            TrUpCheck.Visible = True
                            trAll.Visible = False
                            UpFlag = False
                            Uploadflag = True
                        Else
                            Uploadflag = False
                        End If
                    End If
                Else
                    If (ddlTypeofData1.SelectedValue = "0" Or ddlTypeofData1.SelectedValue = "1") And P1DFlag = False Then
                        lblErr.Text = "Phase1 Contestant List Not yet Downloaded"
                        btnUpload.Enabled = False
                        FileUpload.Enabled = False
                        Exit Sub
                    Else
                        btnUpload.Enabled = True
                        FileUpload.Enabled = True
                    End If
                    If ((ddlTypeofData1.SelectedValue = "3" Or ddlTypeofData1.SelectedValue = "4") And ddlContest.SelectedValue = 2) Then ' And NRooms > 1 Then
                        If (P12DFlag = True) Or (P1DFlag = True And P2AllDFlag = True) Then
                            btnUpload.Enabled = True
                            FileUpload.Enabled = True
                        Else
                            lblErr.Text = "Phase1 and Phase2 Contestant List Not yet Downloaded"
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        End If
                    End If
                    If (ddlTypeofData1.SelectedValue = "3" And ddlContest.SelectedValue = 1) And NRooms > 1 Then
                        If (P123DFlag = False) Then
                            lblErr.Text = "Phase1,Phase2,Phase3 Composite Data Not yet Downloaded"
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        Else
                            btnUpload.Enabled = True
                            FileUpload.Enabled = True
                        End If
                    End If
                    If (ddlTypeofData1.SelectedValue = "4" And ddlContest.SelectedValue = 1) Then
                        If (TopListD = False Or RankCertificateD = False) Then
                            lblErr.Text = "Listof Top Contestants (Or) Rank Certifiactes Not yet Downloaded"
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        Else
                            btnUpload.Enabled = True
                            FileUpload.Enabled = True
                        End If
                    End If
                    If ddlTypeofData1.SelectedValue = "5" And P12DFlag = False Then
                        lblErr.Text = "Phase1,Phase2 Composite Data Not yet Downloaded"
                        btnUpload.Enabled = False
                        FileUpload.Enabled = False
                        Exit Sub
                    Else
                        btnUpload.Enabled = True
                        FileUpload.Enabled = True
                    End If
                End If
            End If


            If Session("RoleID") = "18" Or Session("RoleID") = "16" Then
                Try
                    If TrTypeofData1.Visible = False Then
                        FileUpload.Enabled = False
                        btnUpload.Enabled = False
                        Exit Sub
                    Else
                        If ddlTypeofData1.SelectedValue = "0" And P1DFlag = False Then
                            lblErr.Text = "Phase1 Contestant List Not yet Downloaded"
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        Else
                            btnUpload.Enabled = True
                            FileUpload.Enabled = True
                        End If

                        If ddlTypeofData1.SelectedValue = "2" And P2DFlag = False Then 'And (Session("RoleID") <> 16 And Session("RoleID") <> 18) Then
                            lblErr.Text = "Phase2 Contestant List Not yet Downloaded"
                            TrRoom1.Visible = False
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        ElseIf ddlTypeofData1.SelectedValue = "2" Then
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select c.SeqNo From ContestTeamSchedule C Inner Join RoomSchedule R On R.ContestYear=C.ContestYear and R.EventId=C.EventID and R.ChapterID=C.ChapterID and R.ProductGroupId=C.ProductGroupID and R.ProductId=C.ProductID and R.Phase=C.Phase and  R.RoomNumber =C.RoomNumber and R.SeqNo=C.SeqNo and R.BldgID=C.BldgID   WHERE  C.ContestYear=" & ddlYear.SelectedValue & " and C.EventID=" & ddlContest.SelectedValue & " and C.ChapterID=" & ddlChapter.SelectedValue & " and C.ProductGroupID=(Select ProductGroupID from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and C.ProductID=(Select ProductId from Contest Where ContestID=" & ddlProduct.SelectedValue & ") and (C.CJMemberId in(" & Session("LoginID") & ") Or C.LTJudMemberId in(" & Session("LoginID") & ")) ") ' and C.SeqNo=" & GetCode(ddlRoom_Phase.SelectedValue.ToString())(0)) 'and C.Phase=2 
                            If ds.Tables(0).Rows.Count > 0 Then
                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from ScoreSheetLog where ContestID= " & ddlProduct.SelectedValue & " And EventID=" & ddlContest.SelectedValue & " And ChapterID=" & ddlChapter.SelectedValue & " And ContestYear=" & ddlYear.SelectedValue & " and  (P2DList='Y' and RoomNumberD=" & GetCode(ddlRoom_Phase.SelectedValue.ToString())(0) & ")") > 0 Then  'ds.Tables(0).Rows(0)("SeqNo") & ")")'(RoomAllD='Y' Or)
                                    btnUpload.Enabled = True
                                    FileUpload.Enabled = True
                                Else
                                    lblErr.Text = "Phase2 Contestant List Not yet Downloaded"
                                    btnUpload.Enabled = False
                                    FileUpload.Enabled = False
                                End If
                            Else
                                btnUpload.Enabled = False
                                FileUpload.Enabled = False
                                lbldwError.Text = "No rooms assigned to show"
                                Exit Sub
                            End If
                        End If
                        If ddlTypeofData1.SelectedValue = "6" And P3DFlag = False Then
                            lblErr.Text = "Phase3 Contestant List Not yet Downloaded"
                            btnUpload.Enabled = False
                            FileUpload.Enabled = False
                            Exit Sub
                        ElseIf ddlTypeofData1.SelectedValue = "6" And P3DFlag = True Then
                            FileUpload.Enabled = True
                            btnUpload.Enabled = True
                        End If
                    End If
                Catch ex As Exception

                End Try
            End If
        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadDDLRanks()
        Try
            Dim i As Integer
            If ddlPhase.SelectedValue = "5" Or ddlPhase.SelectedValue = "7" Or ddlPhase.SelectedValue = "8" Or ddlPhase.SelectedValue = "9" Then
                TrTopRank.Visible = True
            Else
                TrTopRank.Visible = False
            End If
            If ddlPhase.SelectedValue = "7" Or ddlPhase.SelectedValue = "8" Then
                If ddlPhase.SelectedValue = "8" Then
                    If ddlProductGroup.SelectedValue = "EW" Or ddlProductGroup.SelectedValue = "PS" Or ddlProductGroup.SelectedValue = "BB" Then
                        ddlTopRank.Items(0).Enabled = True
                        ddlTopRank.Items(1).Enabled = True
                        ddlTopRank.Items(2).Enabled = True
                        ddlTopRank.Items(3).Enabled = True
                    Else
                        ddlTopRank.Items(0).Enabled = False
                        ddlTopRank.Items(1).Enabled = False
                        ddlTopRank.Items(2).Enabled = False
                        ddlTopRank.Items(3).Enabled = False
                    End If
                End If
                For i = 4 To 19 'Only Top 10
                    ddlTopRank.Items(i).Enabled = False
                Next
            ElseIf ddlPhase.SelectedValue = "5" Or ddlPhase.SelectedValue = "9" Then
                For i = 4 To 19
                    ddlTopRank.Items(i).Enabled = True
                Next
            End If
            Dim Conts_Cnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT COUNT(*) FROM ScoreDetail WHERE EventID =" & ddlContest.SelectedValue & " and ChapterID= " & ddlChapter.SelectedValue & " and ContestYear=" & ddlYear.SelectedValue & " and ContestID=" & ddlProduct.SelectedValue & " and Case when AttendanceFlag IS NULL  Then '' Else AttendanceFlag End not like '%N%'")
            Dim Rank_Cnt As Integer = 0
            If ddlContest.SelectedValue = 2 Then
                If Conts_Cnt < 5 Then
                    Rank_Cnt = 0
                ElseIf Conts_Cnt >= 5 And Conts_Cnt <= 7 Then
                    Rank_Cnt = 1
                ElseIf Conts_Cnt > 7 And Conts_Cnt < 10 Then
                    Rank_Cnt = 2
                ElseIf Conts_Cnt >= 10 Then
                    Rank_Cnt = 3
                End If
            End If
            If ddlContest.SelectedValue = 1 Then
                If Conts_Cnt < 40 And (ddlProductGroup.SelectedValue = "EW" Or ddlProductGroup.SelectedValue = "PS") Then
                    Rank_Cnt = 3
                ElseIf Conts_Cnt >= 40 And (ddlProductGroup.SelectedValue = "SB" Or ddlProductGroup.SelectedValue = "VB" Or ddlProductGroup.SelectedValue = "GB" Or ddlProductGroup.SelectedValue = "MB" Or ddlProductGroup.SelectedValue = "SC" Or ddlProductGroup.SelectedValue = "BB") Then
                    Rank_Cnt = 10
                End If
            End If

            If ddlPhase.SelectedValue = 7 Or ddlPhase.SelectedValue = 8 Then
                For i = 0 To ddlTopRank.Items.Count - 1
                    If i = Rank_Cnt Then
                        ddlTopRank.Items(i).Enabled = True
                    Else
                        ddlTopRank.Items(i).Enabled = False
                    End If
                Next
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlMPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMPhase.SelectedIndexChanged
        LblMasterErr.Text = ""
        lblMdwError.Text = ""
    End Sub

    Protected Sub ddlMProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMProduct.SelectedIndexChanged
        LblMasterErr.Text = ""
        lblMdwError.Text = ""
    End Sub

    Protected Sub ddlMYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMYear.SelectedIndexChanged
        LblMasterErr.Text = ""
        lblMdwError.Text = ""
    End Sub

    Protected Sub ddlRoom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoom.SelectedIndexChanged
        lblWarngMsg.Text = ""
        TrConfirmDwnload.Visible = False
    End Sub
    Protected Sub ddlRoom1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoom1.SelectedIndexChanged

        lblWarngMsg.Text = ""
        TrConfirmDwnload.Visible = False
    End Sub

    Protected Sub ddlMContest_SelectedIndexChanged1(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlMContest.SelectedIndexChanged
        lblMdwError.Text = ""
        getMPhase()
    End Sub

    Protected Sub BtnConfirmDwnload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnConfirmDwnload.Click
        filldata(True)
    End Sub

    Protected Sub BtnCancelDwnload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancelDwnload.Click
        TrConfirmDwnload.Visible = False
        trAll.Visible = True
    End Sub

    Protected Sub ddlTopRank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTopRank.SelectedIndexChanged
        lbldwError.Text = ""
        lblErr.Text = ""
        TrConfirmDwnload.Visible = False
    End Sub

    Protected Sub btnShowMasterList_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnShowMasterList.Click
        Try
            Response.Write("<script language='javascript'>")
            Response.Write(" window.open('ShowMasterTempList.aspx?year=" + ddlMYear.SelectedValue + "','_blank','left=200,top=50,width=1000,height=600,toolbar=0,location=0,scrollbars=1');")
            Response.Write("</script>")
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub BtnScoreDetExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnScoreDetExport.Click
        Try
            If ddlChapter.SelectedItem.Text = "Select" Then
                lbldwError.Text = "Please Select Chapter To Export"
                Exit Sub
            End If
            Dim dt As DataTable = GetData()
            Dim ChapterCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Replace(Replace(ChapterCode,',','_'),' ','') as Chaptercode from chapter Where ChapterId=" & ddlChapter.SelectedValue)
            Dim attachment As String = ddlYear.SelectedValue & "_ScoreDetail_" & ChapterCode & "_" & getProductcode(ddlProduct) & ".xlsx"
            Dim xlWorkBook As IWorkbook = NativeExcel.Factory.CreateWorkbook
            If dt.Rows.Count > 0 Then
                Dim Sheet1 As IWorksheet = xlWorkBook.Worksheets.Add()
                Sheet1.Name = dt.TableName
                Dim colCount As Integer = dt.Columns.Count
                Dim i As Integer = 1
                For Each dc As DataColumn In dt.Columns
                    Sheet1.Range(1, i).Value = dc.ColumnName
                    Sheet1.Range(1, i).Font.Bold = True
                    i = i + 1
                Next

                Dim j As Integer
                i = 2
                For Each dr As DataRow In dt.Rows
                    For j = 0 To dt.Columns.Count - 1
                        Sheet1.Range(i, j + 1).Value = dr(j).ToString()
                    Next
                    i = i + 1
                Next
                Response.Clear()
                ' Response.ContentType = "application/vnd.ms-excel"
                ' Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
                Response.AddHeader("Content-Disposition", "attachment;filename=" & attachment)
                xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
            End If

        Catch ex As Exception

        End Try
    End Sub

    Function GetData() As DataTable
        Dim sql As String = ""
        If ddlContest.SelectedValue = 2 Then
            sql = "SELECT EventId,ProductGroupCode,productCode,ChildNumber,ContestantID,BadgeNumber,CONVERT(VARCHAR(10), DOB, 101) AS DOB,Grade, AttendanceFlag,Phase1Score,Phase1_TB1,Phase1_TB2,Phase1_TB3,Phase1_TB4,Phase2Score,Ph2_Round1,Ph2_Round2,Ph2_Round3,Ph2_Round4,Ph2_Round5,Ph2_Round6,Ph2_Round7,Ph2_Round8,P1_P2_Rank,Final_Rank,Rank_Alpha FROM ScoreDetail Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlContest.SelectedValue & " and ContestID=" & ddlProduct.SelectedValue & " order by BadgeNumber"
        ElseIf ddlContest.SelectedValue = 1 Then
            sql = "SELECT EventId,ProductGroupCode,productCode,ChildNumber,ContestantID,BadgeNumber,CONVERT(VARCHAR(10), DOB, 101) AS DOB,Grade, AttendanceFlag,Phase1Score,Phase1_TB1,Phase1_TB2,Phase1_TB3,Phase1_TB4,Phase2Score,Ph2_Round1,Ph2_Round2,Ph2_Round3,Ph2_Round4,Ph2_Round5,Ph2_Round6,Ph2_Round7,Ph2_Round8,P1_P2_Rank ,Phase3Score,Ph3_Round1,Ph3_Round2,Ph3_Round3,Ph3_Round4,Ph3_Round5,Ph3_Round6,Ph3_Round7,Ph3_Round8,Ph3_Round9,Ph3_Round10,Ph3_Round11,Ph3_Round12,Ph3_Round13,Ph3_Round14,Ph3_Round15,Ph3_Round16,Ph3_Round17,Ph3_Round18,Ph3_Round19,Ph3_TB_Score,Ph3_Rank,Final_Rank,Rank_Alpha FROM ScoreDetail Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlContest.SelectedValue & " and ContestID=" & ddlProduct.SelectedValue & " order by BadgeNumber"
        End If

        Using myConnection As New SqlConnection(Application("ConnectionString"))
            Using myCommand As SqlCommand = New SqlCommand(sql, myConnection)
                myConnection.Open()
                Using myReader As SqlDataReader = myCommand.ExecuteReader()
                    Dim myTable As DataTable = New DataTable()
                    myTable.Load(myReader)
                    myConnection.Close()
                    Return myTable
                End Using
            End Using
        End Using

    End Function

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        'Uploadflag = True
        Dim UplFile As String = Session("UplFileName")
        UploadScores(UplFile)
        TrUpCheck.Visible = "False"

        If trconfirm.Visible = True Then
            trAll.Visible = False
        Else
            trAll.Visible = True
        End If
    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        TrUpCheck.Visible = "False"
        lblErr.Text = "No Scores Uploaded"
        trAll.Visible = True
    End Sub

    Protected Sub ddlRoom_Phase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoom_Phase.SelectedIndexChanged
        LoadRoom_Phase_New()
        Dim code As String() = ddlRoom_Phase.SelectedValue.Split("_")

        If (Session("RoleID") = 16 Or (Session("RoleID") = 18)) Then
            checkContestTeamScheduleByRoleID()
        End If
    End Sub
    Function GetCode(ByVal value As String) As String()
        Dim code As String() = value.Split("_")
        Return code
    End Function

    Protected Sub BtnPhaseUpYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPhaseUpYes.Click
        UploadScores(Session("UplFileName"))
        TrPhase2Upload.Visible = False
    End Sub

    Protected Sub BtnPhaseUpNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnPhaseUpNo.Click
        TrPhase2Upload.Visible = False
    End Sub
End Class