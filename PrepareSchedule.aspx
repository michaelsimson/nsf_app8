﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="PrepareSchedule.aspx.vb" Inherits="PrepareSchedule" Title="Prepare Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style>
        .popup {
            height: 100px;
            width: 100%;
            /* position: absolute; */
            top: 122%;
            background-color: rgb(223, 233, 222);
            z-index: 100;
            border-radius: 5px;
            right: 122px;
            opacity: 1;
            text-align: center;
            vertical-align: middle;
        }

        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 400px;
            height: 200px;
            top: 55%;
            left: 50%;
            margin-left: -200px;
            margin-top: -200px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }

        .qTipWidth {
            max-width: 900px !important;
        }
    </style>
    <script language="javascript" type="text/javascript">
        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CalSignUp Help', settings);
            PopupWindow.focus();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function setonScroll(divObj, DgID) {
            var datagrid = document.getElementById(DgID);
            var HeaderCells = datagrid.getElementsByTagName('th');
            var HeaderRow;
            if (HeaderCells == null || HeaderCells.length == 0) {
                var AllRows = datagrid.getElementsByTagName('tr');
                HeaderRow = AllRows[0];
            }
            else {
                HeaderRow = HeaderCells[0].parentNode;
            }

            var DivsTopPosition = parseInt(divObj.scrollTop);

            if (DivsTopPosition > 0) {
                HeaderRow.style.position = 'absolute';
                HeaderRow.style.top = (parseInt(DivsTopPosition - 2)).toString() + 'px';
                HeaderRow.style.width = datagrid.style.width;
                HeaderRow.style.zIndex = '1000';
            }
            else {
                divObj.scrollTop = 0;
                HeaderRow.style.position = 'relative';
                HeaderRow.style.top = '0';
                HeaderRow.style.bottom = '0';
                HeaderRow.style.zIndex = '0';
            }

        }

    </script>

    <link href="css/jquery.qtip.min.css" rel="stylesheet" />

    <script type="text/javascript" src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>

    <script type="text/javascript">
        function OpenConfirmationBox() {

            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }
        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvMemberDetails").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvMemberDetails").hide();
        }

        $(function (e) {

            $("#ancHelpLink").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {

                        var dvHtml = "";
                        dvHtml += " <table style='border: 1px solid black; border-collapse: collapse; font-weight: bold; margin-top: 15px;'>";
                        dvHtml += " <thead>";
                        dvHtml += " <tr>"
                        dvHtml += "<td colspan='5' style='border: 1px solid black; text-align: center;'>L : Level</td>";
                        dvHtml += "</tr>";
                        dvHtml += "</thead>";
                        dvHtml += "<tbody>";
                        dvHtml += "<tr>";
                        dvHtml += " <td style='border: 1px solid black'>B : Beginner";
                        dvHtml += " </td>";
                        dvHtml += "<td style='border: 1px solid black'>I : Intermediate";
                        dvHtml += "</td>";
                        dvHtml += "<td style='border: 1px solid black'>A : Advanced";
                        dvHtml += " </td>";
                        dvHtml += " <td style='border: 1px solid black'>J : Junior";
                        dvHtml += "</td>";
                        dvHtml += "<td style='border: 1px solid black'>S : Senior";
                        dvHtml += "</td>";
                        dvHtml += " </tr>";
                        dvHtml += " <tr>";
                        dvHtml += "   <td colspan='5' style='border: 1px solid black; text-align: center;'></td>";
                        dvHtml += "</tr>";
                        dvHtml += "<tr>";
                        dvHtml += " <td colspan='5' style='border: 1px solid black; text-align: center;'>S : Session Number (1, 2, 3, etc.)";
                        dvHtml += " </td>";
                        dvHtml += "</tr>";
                        dvHtml += "<tr>";
                        dvHtml += " <td colspan='5' style='border: 1px solid black; text-align: center;'></td>";
                        dvHtml += " </tr>";
                        dvHtml += "<tr>";
                        dvHtml += " <td colspan='5' style='border: 1px solid black; text-align: center;'>P: Preference (1, 2, 3, etc.)";

                        dvHtml += "</td>";
                        dvHtml += " </tr>";
                        dvHtml += "<tr>";
                        dvHtml += "    <td colspan='5' style='border: 1px solid black; text-align: center;'></td>";
                        dvHtml += " </tr>";
                        dvHtml += "<tr>";
                        dvHtml += "    <td colspan='5' style='border: 1px solid black; text-align: center;'>A1, A2, etc: Y - Accepted";

                        dvHtml += "  </td>";
                        dvHtml += " </tr>";
                        dvHtml += "</tbody>";
                        dvHtml += " </table>";

                        return dvHtml;

                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Instructions</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                },
                position: {
                    adjust: {
                        screen: true
                    },
                    my: "center right",
                    at: "left center"
                }

            })
        });

    </script>
    <%--  <div>--%>
    <table border="0" cellpadding="3" cellspacing="0" width="980">
        <tr>
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td align="center" style="font-size: 16px; font-weight: bold; font-family: Calibri">Prepare Schedule</td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td align="right" class="btn_02" style="width: 25px">Admin: </td>
            <td style="width: 25px">
                <asp:DropDownList ID="ddlRole" DataTextField="RoleCode" DataValueField="RoleID" runat="server"></asp:DropDownList></td>
            <td></td>
            <td align="right" class="btn_02" style="width: 35px">EventYear: </td>
            <td align="left" style="width: 20px">
                <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="120px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">Event: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="EventCode" DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">Semester: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" Width="100px" Height="20px" runat="server">
                </asp:DropDownList>&nbsp;</td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">ProductGroup: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">Product: </td>
            <td align="left" style="width: 150px">
                <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td>
                <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server" AutoPostBack="True" Height="20px" Width="150px" Visible="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label></td>
            <td colspan="2" align="center"></td>

            <td align="center" colspan="3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" />&nbsp;</td>
            <td align="center" colspan="4">
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></td>
            <td></td>
        </tr>
    </table>
    <asp:Panel ID="pnlCofirm" Visible="false" CssClass="popup" runat="server">
        <br />
        <br />
        <strong>Do you want to update current changes?</strong>
        <br />
        <asp:Button ID="btnYes" runat="server" Text="Yes" /><asp:Button ID="btnNo" runat="server" Text="Ignore" />
    </asp:Panel>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div style="float: left;">
        <asp:Button ID="btnExport" runat="server" Text="Export" Enabled="false" />
    </div>
    <div style="float: left; margin-left: 10px;">
        <asp:Button ID="btnSaveApprovals" runat="server" Text="SaveApprovals" />
    </div>
    <div style="float: right;">
        <a id="ancHelpLink" style="color: blue; font-weight: bold; cursor: pointer;">Help?</a>
        <input type="button" runat="server" id="btnHelp" visible="false" value="Help" onclick="OpenConfirmationBox()" />
    </div>
    <div style="float: right; margin-right: 10px;">
        <div style="float: left;">
            <asp:Label ID="lblSortBy" runat="server" Text="Sort By"></asp:Label>
        </div>
        <div style="float: left; margin-left: 10px;">
            <asp:DropDownList ID="ddlWeekDay" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlWeekDay_SelectedIndexChanged"></asp:DropDownList>
        </div>
    </div>

    <table width="100%">
        <tr class="noscroll">
            <td align="center">

                <div id="oDiv" runat="server" style="position: relative; top: 0 -2px; left: 0px; width: 100%;">
                    <%--overflow-y:auto--%>
                    <asp:DataGrid ID="DGSchedule" runat="server" DataKeyField="ID" AutoGenerateColumns="False" AllowPaging="true" PageSize="50" PagerStyle-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="numericpages" GridLines="Both" CellPadding="0" Width="100%" CellSpacing="1" BackColor="Navy" BorderWidth="3px" BorderStyle="Groove" BorderColor="Black" ForeColor="White" Font-Bold="True">
                        <FooterStyle ForeColor="Black" BackColor="Gainsboro" Height="30px"></FooterStyle>
                        <%--#CCCCCC"--%>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                        <ItemStyle ForeColor="Black" BackColor="#EEEEEE" Height="28px"></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false" Height="30px"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
                        <EditItemStyle ForeColor="Black" BackColor="#EEEEEE" />
                        <%-- HeaderStyle-BackColor="Gainsboro"--%>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Ser#">
                                <ItemTemplate>
                                    <%# (DGSchedule.PageSize * DGSchedule.CurrentPageIndex) + Container.ItemIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" ItemStyle-Width="50px" HeaderText="SignUpID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " ReadOnly="true" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CoachName" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderText="CoachName"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Day1" ItemStyle-Width="50px" HeaderText="Sat" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L1" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="S" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="P" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A1" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro">
                                <%--   <ItemTemplate><asp:TextBox ID="txtAccept1" runat="server" Width="25px"  Text='<%# Bind("A1") %>'></asp:TextBox>&nbsp; 
                                    </ItemTemplate>   --%>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept1" runat="server" SelectedValue="<%# Bind('A1') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                                <%--<HeaderStyle Width="180px" />
<ItemStyle Width="180px" />--%>
                            </asp:TemplateColumn>

                            <asp:BoundColumn DataField="Day2" ItemStyle-Width="50px" HeaderText="Sun" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L2" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S2" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P2" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A2" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White">
                                <%-- <ItemTemplate><asp:TextBox ID="txtAccept2" runat="server" Width="25px" Text='<%# Bind("A2") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                     </ItemTemplate> --%>

                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept2" runat="server" SelectedValue="<%# Bind('A2') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>


                            <asp:BoundColumn DataField="Day3" ItemStyle-Width="50px" HeaderText="Mon" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L3" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S3" HeaderText="S" HeaderStyle-Width="25px" ItemStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P3" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A3" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro">
                                <%--<ItemTemplate><asp:TextBox ID="txtAccept3" runat="server" Width="25px" Text='<%# Bind("A3") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                       </ItemTemplate>  --%>


                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept3" runat="server" SelectedValue="<%# Bind('A3') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>



                            <asp:BoundColumn DataField="Day4" ItemStyle-Width="50px" HeaderText="Tue" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L4" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S4" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P4" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A4" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White">
                                <%--    <ItemTemplate><asp:TextBox ID="txtAccept4" runat="server" Width="25px" Text='<%# Bind("A4") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                            </ItemTemplate>  
                                --%>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept4" runat="server" SelectedValue="<%# Bind('A4') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>



                            <asp:BoundColumn DataField="Day5" ItemStyle-Width="50px" HeaderText="Wed" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L5" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S5" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P5" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A5" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro">
                                <%--    <ItemTemplate><asp:TextBox ID="txtAccept5" runat="server" Width="25px" Text='<%# Bind("A5") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                      </ItemTemplate>  --%>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept5" runat="server" SelectedValue="<%# Bind('A5') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>



                            <asp:BoundColumn DataField="Day6" ItemStyle-Width="50px" HeaderText="Thu" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L6" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S6" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P6" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A6" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White">
                                <%--<ItemTemplate><asp:TextBox ID="txtAccept6" runat="server" Width="25px" Text='<%# Bind("A6") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                   </ItemTemplate> --%>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept6" runat="server" SelectedValue="<%# Bind('A6') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:BoundColumn DataField="Day7" ItemStyle-Width="50px" HeaderText="Fri" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="L7" ItemStyle-Width="50px" HeaderStyle-Width="50px" HeaderText="L" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S7" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P7" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="A7" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro">
                                <%--<ItemTemplate><asp:TextBox ID="txtAccept7" runat="server" Width="25px" Text='<%# Bind("A7") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                    </ItemTemplate>  --%>
                                <ItemTemplate>
                                    <asp:DropDownList ID="ddlAccept7" runat="server" SelectedValue="<%# Bind('A7') %>">
                                        <asp:ListItem Value="">Sel</asp:ListItem>
                                        <asp:ListItem Value="Y">Y</asp:ListItem>
                                    </asp:DropDownList>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                        </Columns>
                        <AlternatingItemStyle BackColor="#a09f9f" ForeColor="Gainsboro" />
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>


    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>
    <div id="dvMemberDetails" class="web_dialog">

        <center>
            <table style='border: 1px solid black; border-collapse: collapse; font-weight: bold; margin-top: 15px;'>
                <thead>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'>L : Level</td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='border: 1px solid black'>B : Beginner
                        </td>
                        <td style='border: 1px solid black'>I : Intermediate
                        </td>
                        <td style='border: 1px solid black'>A : Advanced
                        </td>
                        <td style='border: 1px solid black'>J : Junior
                        </td>
                        <td style='border: 1px solid black'>S : Senior
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'></td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'>S : Session Number (1, 2, 3, etc.)
                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'></td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'>P: Preference (1, 2, 3, etc.)

                        </td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'></td>
                    </tr>
                    <tr>
                        <td colspan="5" style='border: 1px solid black; text-align: center;'>A1, A2, etc: Y - Accepted

                        </td>
                    </tr>
                </tbody>
            </table>
        </center>

        <%--<center>
            <table style='border: 1px solid black; border-collapse: collapse;'>
                <thead>
                    <tr>
                        <td style='border: 1px solid black'>S : Session Number (1, 2, 3, etc.)
                        </td>
                    </tr>
                </thead>

            </table>
        </center>

        <center>
            <table style='border: 1px solid black; border-collapse: collapse;'>
                <thead>
                    <tr>
                        <td style='border: 1px solid black'>P: Preference (1, 2, 3, etc.)

                        </td>
                    </tr>
                </thead>

            </table>
        </center>

        <center>
            <table style='border: 1px solid black; border-collapse: collapse;'>
                <thead>
                    <tr>
                        <td style='border: 1px solid black'>A1, A2, etc: 

                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td style='border: 1px solid black'>Y : Accepted
                        </td>
                    </tr>
                </tbody>
            </table>
        </center>--%>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <input type="button" id="btnClose" onclick="HideDialog()" value="Close" />
        </center>
    </div>
</asp:Content>
