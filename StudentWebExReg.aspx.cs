﻿using System;
using System.Collections.Generic;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Win32;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;


public partial class StudentWebExReg : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            lblerr.Text = "";
            if (!IsPostBack)
            {

                LoadEvent(ddEvent);
                //fillStudentList();
                // TestCreateTrainingSession();
            }
        }
    }

    public void fillProductGroup()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CoachReg where EventYear='" + ddYear.SelectedValue + "' and ChildNumber=" + ddlChildrenList.SelectedValue + " and Approved='Y')";
        }
        else if (Session["RoleID"].ToString() == "88")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CoachReg where EventYear='" + ddYear.SelectedValue + "' and ChildNumber=" + ddlChildrenList.SelectedValue + " and Approved='Y')";
        }


        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CoachReg where EventYear='" + ddYear.SelectedValue + "' and ChildNumber=" + ddlChildrenList.SelectedValue + " and Approved='Y')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CoachReg where EventYear='" + ddYear.SelectedValue + "' and ChildNumber=" + ddlChildrenList.SelectedValue + ")";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 1;
                ddlProduct.Enabled = false;
                if (Session["RoleID"].ToString() == "88")
                {
                    //fillCoach();
                }
                loadLevel();
                fillCoach();
            }
            else
            {

                ddlProduct.Enabled = true;
            }

        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        fillStudentList();
    }

    public void fillCoach()
    {
        string cmdText = "";
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89" || Session["RoleID"].ToString() == "2")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " order by ID.FirstName ASC";
        }
        else
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name,ID.Lastname, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Accepted='Y' order by ID.FirstName ASC";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count > 1)
            {
                // ddlCoach.Items.Insert(1, new ListItem("All", "All"));
            }
            else if (ds.Tables[0].Rows.Count == 1)
            {
                ddlCoach.SelectedValue = Session["LoginId"].ToString();
            }

        }

        //if (ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
        //{
        //    ddlCoach.Items.Insert(0, new ListItem("All", "All"));
        //    ddlCoach.SelectedValue = "All";
        //    ddlCoach.Enabled = false;
        //}
        //else if (ddlProductGroup.SelectedValue == "All" || ddlProduct.SelectedValue == "All")
        //{
        //    ddlCoach.Items.Insert(0, new ListItem("All", "All"));
        //    ddlCoach.SelectedValue = "All";
        //    ddlCoach.Enabled = false;
        //}
        //else
        //{
        //    cmdText = "select (ID.FirstName+' '+ ID.LastName) as Name,ID.AutoMemberID from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.ProductID=" + ddlProduct.SelectedValue + "  and V.Accepted='Y' order by ID.Lastname ASC";

        //    //ddlCoach.Enabled = true;
        //}
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel();
        fillStudentList();
        fillCoach();
    }


    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CoachReg V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Approved='Y' and V.ChildNumber='" + ddlChildrenList.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Approved='Y' and and V.ChildNumber='" + ddlChildrenList.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                LoadSessionNo();
                ddlLevel.Enabled = false;

            }
        }
    }
    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        //fillCoach();

        LoadSessionNo();
        fillStudentList();

    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 1))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
                // *********Added on 26-11-2013 to disable Prepbclub for the current year
                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;
                // fillProductGroup();
                fillChild();
                ddchapter.SelectedValue = "112";
                // **************************************************************************'
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }
            // ddlObject.SelectedIndex = 0
            // btnCreateAllSessions.Visible = true;
        }
        else
        {
            btnCreateAllSessions.Visible = false;
            // lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlCoach.SelectedValue == "All")
        {

            ddlProductGroup.Items.Insert(0, new ListItem("All", "All"));
            ddlProductGroup.SelectedValue = "All";
            ddlProductGroup.Enabled = false;

            ddlProduct.Items.Insert(0, new ListItem("All", "All"));
            ddlProduct.SelectedValue = "All";
            ddlProduct.Enabled = false;
        }
        else if (ddlCoach.SelectedValue != "All")
        {
            //fillProductGroup();
        }

    }

    public void fillChild()
    {
        string cmdText = "";
        // cmdText = "select CH.FIRST_NAME +' '+CH.LAST_NAME as Name,C.ChildNumber from CoachReg C inner join child CH on(C.ChildNumber=CH.ChildNumber) where C.EventYear=" + ddYear.SelectedValue + " and C.EventID=" + ddEvent.SelectedValue + " and C.ProductGroupID=" + ddlProductGroup.SelectedValue + " and C.ProductID=" + ddlProduct.SelectedValue + " and C.Approved='Y'  and C.CMemberID in (select ID.AutoMemberID from CalSignUP V left join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + " and V.ProductID=" + ddlProduct.SelectedValue + "  and V.Accepted='Y')";

        //cmdText = "select distinct C.ChildNumber,CH.FIRST_NAME,CH.FIRST_NAME +' '+CH.LAST_NAME as Name from CoachReg C inner join child CH on(C.ChildNumber=CH.ChildNumber) where C.EventYear=" + ddYear.SelectedValue + " and C.EventID=" + ddEvent.SelectedValue + " and C.Approved='Y'  and C.CMemberID in (select ID.AutoMemberID from CalSignUP V left join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y') order by CH.FIRST_NAME asc";

        cmdText = "select distinct C.ChildNumber, C.FIRST_NAME, C.LAST_NAME, C.FIRST_NAME +' '+C.LAST_NAME as Name  from Child C inner join CoachReg CR on C.ChildNumber=CR.ChildNumber where CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.Approved='Y'  and CR.CMemberID in (select ID.AutoMemberID from CalSignUP V left join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y')";

        DataSet ds = new DataSet();
        try
        {

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlChildrenList.DataSource = ds;
                ddlChildrenList.DataTextField = "Name";
                ddlChildrenList.DataValueField = "ChildNumber";
                ddlChildrenList.DataBind();
                ddlChildrenList.Items.Insert(0, new ListItem("Select", "0"));

            }
        }
        catch
        {
        }
    }

    protected void ddlChildrenList_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlChildrenList.SelectedValue != "0")
        {
            fillProductGroup();

            string cmdText = "";
            //cmdText = "select C.CMemberID from CoachReg C  where C.EventYear=" + ddYear.SelectedValue + " and C.EventID=13 and C.ProductGroupID=" + ddlProductGroup.SelectedValue + " and C.ProductID=" + ddlProduct.SelectedValue + " and C.ChildNumber=" + ddlChildrenList.SelectedValue + "";
            DataSet ds = new DataSet();
            // ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            //if (ds.Tables[0] != null)
            //{
            string MemberID = string.Empty;
            //if (ds.Tables[0].Rows.Count > 0)
            // {
            //MemberID = ds.Tables[0].Rows[0]["CMemberID"].ToString();
            cmdText = "select CR.CoachRegID,CR.SessionNo,CR.PMemberID, C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,C1.OnlineClassEmail as WebExEmail, IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country, CR.AttendeeID, CR.RegisteredID, CR.ProductGroupCode, CR.productCode,CR.ProductGroupID,CR.ProductID,CR.Level, IP1.FirstName +' '+ IP1.LastName as CoachName, cs.UserID,cs.Pwd,cs.MeetingKey  from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=" + ddlChildrenList.SelectedValue + "  and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.Approved='Y') inner join Indspouse IP1 on(IP1.AutoMemberID=CR.CMemberID) inner join Calsignup cs on(cs.MemberID=cr.CMemberID and cs.SessionNo=cr.SessionNo and cs.Level=cr.Level and cs.EventYear=cr.EventYear and cs.Accepted='Y') where C1.ChildNumber=" + ddlChildrenList.SelectedValue + " and CR.EventYear='" + ddYear.SelectedValue + "' and CR.Approved='Y'";
            //and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "'
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "";

                    for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                    {
                        if (GrdStudentsList.Rows[i].Cells[13].Text.Trim() == "&nbsp;")
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnReRegister") as Button).Enabled = false;
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnDelete") as Button).Enabled = false;
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                        }
                        else
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnReRegister") as Button).Enabled = true;
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnDelete") as Button).Enabled = true;
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                        }
                    }
                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "No record found";

                }
            }
            //}
            //}
        }
    }

    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[8].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[12].Text;
                HdnLevel.Value = Level.Trim();
                string WebExID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblUserID") as Label).Text;
                string WebExPwd = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblPwd") as Label).Text;
                string SessionKey = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                hdnSessionKey.Value = SessionKey;
                string CoachRegID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCoachRegID") as Label).Text;
                string ProductGroupID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                string ProductID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductID") as Label).Text;

                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }

                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
                if (hdnMeetingStatus.Value == "SUCCESS")
                {

                    GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

                    string CmdText = "update CoachReg set AttendeeJoinURL='" + hdnMeetingUrl.Value + "', RegisteredID=" + hdsnRegistrationKey.Value + ", AttendeeID=" + hdnMeetingAttendeeID.Value + ", Status='Active' where CoachRegID=" + CoachRegID + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = "Selected child registered successfully.";
                    fillStudentList();
                }
            }
            else if (e.CommandName == "DeleteAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                GrdStudentsList.Rows[selIndex].BackColor = System.Drawing.Color.Gray;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[8].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[12].Text;
                HdnLevel.Value = Level.Trim();
                string ProductGroupCode = string.Empty;
                string ProductCode = string.Empty;
                ProductGroupCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                string WebExID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblUserID") as Label).Text;
                string WebExPwd = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblPwd") as Label).Text;
                string SessionKey = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                hdnSessionKey.Value = SessionKey;
                string ProductGroupID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                string ProductID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductID") as Label).Text;
                string CoachRegID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCoachRegID") as Label).Text;
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                HdnChildEmail.Value = Email;
                HdnCoachRegID.Value = CoachRegID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);

            }
            else if (e.CommandName == "ReRegister")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[8].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[12].Text;
                HdnLevel.Value = Level.Trim();
                string WebExID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblUserID") as Label).Text;
                string WebExPwd = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblPwd") as Label).Text;
                string SessionKey = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblMeetingKey") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                hdnSessionKey.Value = SessionKey;
                string CoachRegID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCoachRegID") as Label).Text;
                string ProductGroupID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductGroupID") as Label).Text;
                string ProductID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblProductID") as Label).Text;
                string SessionNo = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblSessionNo") as Label).Text;
                fillChild();
                ddlChildrenList.SelectedValue = ChildNumber;
                fillProductGroup();
                fillProduct();
                ddlProductGroup.SelectedValue = ProductGroupID;
                ddlProduct.SelectedValue = ProductID;
                loadLevel();
                LoadSessionNo();
                ddlLevel.SelectedValue = Level;
                ddlSession.SelectedValue = SessionNo;
                fillCoach();

            }
        }
        catch (Exception ex)
        {
        }
    }

    public void fillStudentList()
    {


        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select CR.CoachRegID,CR.SessionNo,CR.PMemberID, C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,C1.OnlineClassEmail as WebExEmail, IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country, CR.AttendeeID, CR.RegisteredID, CR.ProductGroupCode, CR.productCode,CR.ProductGroupID,CR.ProductID,CR.Level, IP1.FirstName +' '+ IP1.LastName as CoachName, cs.UserID,cs.Pwd,cs.MeetingKey  from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=" + ddlChildrenList.SelectedValue + "  and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.Approved='Y') inner join Indspouse IP1 on(IP1.AutoMemberID=CR.CMemberID) inner join Calsignup cs on(cs.MemberID=cr.CMemberID and cs.SessionNo=cr.SessionNo and cs.Level=cr.Level and cs.EventYear=cr.EventYear and cs.Accepted='Y') where C1.ChildNumber=" + ddlChildrenList.SelectedValue + " and CR.EventYear='" + ddYear.SelectedValue + "' and CR.Approved='Y'";

        try
        {


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "";

                    for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                    {
                        if (GrdStudentsList.Rows[i].Cells[13].Text.Trim() == "&nbsp;")
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnReRegister") as Button).Enabled = false;
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnDelete") as Button).Enabled = false;
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                        }
                        else
                        {
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnReRegister") as Button).Enabled = true;
                            ((Button)GrdStudentsList.Rows[i].FindControl("BtnDelete") as Button).Enabled = true;
                            ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                        }
                    }
                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    lblChildMsg.Text = "No record found";

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void GrdStudentsList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdStudentsList.PageIndex = e.NewPageIndex;
        fillStudentList();
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CoachReg V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Approved='Y' and V.ChildNumber='" + ddlChildrenList.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Approved='Y' and V.ChildNumber='" + ddlChildrenList.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }
    protected void BtnUpdateRegistration_Click(object sender, EventArgs e)
    {
        if (validateRegistration() == "1")
        {
            UpdateRegistration();
        }
    }

    public string validateRegistration()
    {
        string Retval = "1";
        if (ddlChildrenList.SelectedValue == "0")
        {
            Retval = "-1";
            lblerr.Text = "Please select child";
        }
        else if (ddlProductGroup.SelectedValue == "0")
        {
            Retval = "-1";
            lblerr.Text = "Please select ProductGroup";
        }
        else if (ddlProduct.SelectedValue == "0")
        {
            Retval = "-1";
            lblerr.Text = "Please select ProductGroup";
        }
        else if (ddlCoach.SelectedValue == "0")
        {
            Retval = "-1";
            lblerr.Text = "Please select Coach";
        }

        return Retval;
    }

    public void registerMeetingsAttendees(string Name, string Email, string City, string Country)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.RegisterMeetingAttendee\">\r\n";//

        strXML += "<attendees>";
        strXML += "<person>";
        strXML += "<name>" + Name + "</name>";
        strXML += "<title>title</title>";
        strXML += "<company>microsoft</company>";
        strXML += "<address>";
        strXML += "<addressType>PERSONAL</addressType>";
        strXML += "<city>" + City + "</city>";
        strXML += "<country>" + Country + "</country>";
        strXML += "</address>";
        //strXML += "<phones>0</phones>";
        strXML += "<email>" + Email + "</email>";
        strXML += "<notes>notes</notes>";
        strXML += "<url>https://</url>";
        strXML += "<type>VISITOR</type>";
        strXML += "</person>";
        strXML += "<joinStatus>ACCEPT</joinStatus>";
        strXML += "<role>ATTENDEE</role>";
        // strXML += "<sessionNum>1</sessionNum>";
        // strXML += "<emailInvitations>true</emailInvitations>";
        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "</attendees>";


        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";

        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = this.ProcessMeetingAttendeeResponse(xmlReply);
        ///lblMsg1.Text = result;
    }

    private string ProcessMeetingAttendeeResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                hdnMeetingStatus.Value = "SUCCESS";
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("Meeting Created Successfully.<br/><br/><b>Meeting Attendee Information</b>:</br>");
                string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText;

                string attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText;
                //xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).RemoveAll();

                //string meetingKey1 = (xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent", manager).LastChild).InnerText;
                //hdnSessionKey.Value = meetingKey;
                sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey + "<br/>");
                hdsnRegistrationKey.Value = meetingKey;
                hdnMeetingAttendeeID.Value = attendeeID;
                //hdsnRegistrationKey1.Value = meetingKey1;
                //sb.Append("Meeting Atendee Unique Registration Key:" + meetingKey1 + "<br/>");

                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                hdnMeetingStatus.Value = "Failure";
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                //hdnMeetingStatus.Value = error;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
                lblerr.Text = error;
            }
            else
            {
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void GetJoinUrlMeeting(string Name, string email, string RegsiterID)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);
        // Set the Method property of the request to POST.
        request.Method = "POST";
        // Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded";

        // Create POST data and convert it to a byte array.
        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + hdnWebExID.Value + "</webExID>\r\n";
        strXML += "<password>" + hdnWebExPwd.Value + "</password>\r\n";

        //strXML += "<siteID>690319</siteID>";
        //strXML += "<partnerID>g0webx!</partnerID>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.meeting.GetjoinurlMeeting\">\r\n";//ep.GetAPIVersion    meeting.CreateMeeting

        strXML += "<sessionKey>" + hdnSessionKey.Value + "</sessionKey>";
        strXML += "<attendeeName>" + Name + "</attendeeName>";
        strXML += "<attendeeEmail>" + email + "</attendeeEmail>";
        strXML += "<meetingPW>" + hdnMeetingPwd.Value + "</meetingPW>";
        strXML += "<RegID>" + RegsiterID + "</RegID>";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        // Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length;

        // Get the request stream.
        Stream dataStream = request.GetRequestStream();
        // Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length);
        // Close the Stream object.
        dataStream.Close();
        // Get the response.
        WebResponse response = request.GetResponse();

        // Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        string result = ProcessMeetingAttendeeJoinURLResponse(xmlReply);
        //string result = this.ProcessMeetingResponse(xmlReply);
        //if (!string.IsNullOrEmpty(result))
        //{
        //lblMsg2.Text += result;
        //}
    }

    private string ProcessMeetingAttendeeJoinURLResponse(XmlDocument xmlReply)
    {
        StringBuilder sb = new StringBuilder();
        try
        {
            XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

            string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;

            if (status == "SUCCESS")
            {
                //Process Success Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Green;
                sb.Append("<br/><br/><b>Meeting Attendee URL</b>:</br>");
                //string meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:registerMeetingURL", manager).InnerXml;

                //string meetingKey1 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:inviteMeetingURL", manager).InnerXml;

                string meetingKey2 = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml;
                string URL = meetingKey2.Replace("&amp;", "&");



                hdnMeetingUrl.Value = URL;
                //hdnSessionKey.Value = meetingKey;
                //sb.Append("<a href=" + meetingKey + " target='blank'>" + meetingKey + "</a></br>");
                sb.Append("<a href=" + URL + " target='blank'>" + URL + "</a></br>");

                //sb.Append("<a href=" + meetingKey2 + " target='blank'>" + meetingKey2 + "</a></br>");
                //string hostCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:host", manager).InnerText;
                //hostCalenderURL = "https://apidemoeu.webex.com";
                // sb.Append("Host iCalender Url:</br><a href='" + hostCalenderURL + "' target='blank'>  " + hostCalenderURL + "</a><br/>");

                // string attendeeCalenderURL = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:iCalendarURL/serv:attendee", manager).InnerText;
                //attendeeCalenderURL = "https://apidemoeu.webex.com";
                //sb.Append("Attendee iCalender Url:</br><a href='" + attendeeCalenderURL + "' target='blank'>  " + attendeeCalenderURL + "</a><br/>");
            }
            else if (status == "FAILURE")
            {
                //Process Failure Meeting Message
                //lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("Error in Meeting Creation. <br/><br/><b>Error Details</b>: <br/>");
                string error = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:reason", manager).InnerText;
                string exceptionID = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:exceptionID", manager).InnerText;
                sb.Append("Error:");
                sb.Append(error);
                sb.Append("<br/>Exception Id:" + exceptionID);
            }
            else
            {
                //lblMsg2.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.");
            }
        }
        catch (Exception e)
        {
            sb.Append("Error: " + e.Message);
        }

        return sb.ToString();
    }

    public void DelMeetingAttendee(string sessionKey, string WebExID, string Pwd, string email)
    {
        string strXMLServer = "https://northsouth.webex.com/WBXService/XMLService";


        WebRequest request = WebRequest.Create(strXMLServer);

        request.Method = "POST";

        request.ContentType = "application/x-www-form-urlencoded";


        string strXML = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>\r\n";


        strXML += "<serv:message xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\" xmlns:serv=\"http://www.webex.com/schemas/2002/06/service\" xsi:schemaLocation=\"http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\">\r\n";
        strXML += "<header>\r\n";
        strXML += "<securityContext>\r\n";
        strXML += "<webExID>" + WebExID + "</webExID>\r\n";
        strXML += "<password>" + Pwd + "</password>\r\n";
        strXML += "<siteName>northsouth</siteName>\r\n";
        strXML += "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>\r\n";
        strXML += "</securityContext>\r\n";
        strXML += "</header>\r\n";
        strXML += "<body>\r\n";
        strXML += "<bodyContent xsi:type=\"java:com.webex.service.binding.attendee.DelMeetingAttendee\">\r\n";

        strXML += "<attendeeEmail>\r\n";
        strXML += "<email>" + email + "</email>\r\n";
        strXML += "<sessionKey>" + sessionKey + "</sessionKey>\r\n";
        strXML += "</attendeeEmail>\r\n";

        strXML += "</bodyContent>\r\n";
        strXML += "</body>\r\n";
        strXML += "</serv:message>\r\n";
        byte[] byteArray = Encoding.UTF8.GetBytes(strXML);

        request.ContentLength = byteArray.Length;


        Stream dataStream = request.GetRequestStream();

        dataStream.Write(byteArray, 0, byteArray.Length);

        dataStream.Close();

        WebResponse response = request.GetResponse();


        dataStream = response.GetResponseStream();
        XmlDocument xmlReply = null;
        if (response.ContentType == "application/xml" || response.ContentType == "text/xml;charset=UTF-8")
        {

            xmlReply = new XmlDocument();
            xmlReply.Load(dataStream);

        }
        XmlNamespaceManager manager = new XmlNamespaceManager(xmlReply.NameTable);
        manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service");
        manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting");
        manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common");
        manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee");

        string status = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText;
        hdnMeetingStatus.Value = status;
        if (status == "SUCCESS")
        {
        }

    }

    public void UpdateRegistration()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select CR.CoachRegID,CR.SessionNo,CR.PMemberID, C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,C1.OnlineClassEmail as WebExEmail, IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country, CR.AttendeeID, CR.RegisteredID, CR.ProductGroupCode, CR.productCode,CR.ProductGroupID,CR.ProductID,CR.Level, IP1.FirstName +' '+ IP1.LastName as CoachName, cs.UserID,cs.Pwd,cs.MeetingKey  from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=" + ddlChildrenList.SelectedValue + "  and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.Approved='Y') inner join Indspouse IP1 on(IP1.AutoMemberID=CR.CMemberID) inner join Calsignup cs on(cs.MemberID=cr.CMemberID and cs.SessionNo=cr.SessionNo and cs.Level=cr.Level and cs.EventYear=cr.EventYear and cs.Accepted='Y') where C1.ChildNumber=" + ddlChildrenList.SelectedValue + " and CR.SessionNo=" + ddlSession.SelectedValue + " and CR.ProductGroupID=" + ddlProductGroup.SelectedValue + " and CR.ProductID=" + ddlProduct.SelectedValue + " and CR.Level='" + ddlLevel.SelectedValue + "' and CR.Approved='Y'";

        string Name = string.Empty;
        string Email = string.Empty;
        string City = string.Empty;
        string Country = string.Empty;
        string UserID = string.Empty;
        string Pwd = string.Empty;
        string SessionKey = string.Empty;
        string CoachRegID = string.Empty;

        try
        {


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Name = ds.Tables[0].Rows[0]["Name"].ToString();
                    Email = ds.Tables[0].Rows[0]["WebExEmail"].ToString();
                    City = ds.Tables[0].Rows[0]["City"].ToString();
                    Country = ds.Tables[0].Rows[0]["Country"].ToString();
                    CoachRegID = ds.Tables[0].Rows[0]["CoachRegID"].ToString();
                }
            }

            CmdText = "select MeetingKey, UserID, Pwd from CalSignup where ProductGroupID=" + ddlProductGroup.SelectedValue + "and ProductID=" + ddlProduct.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and EventID=13 and Level='" + ddlLevel.SelectedValue + "' and SessionNo=" + ddlSession.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y'";
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    UserID = ds.Tables[0].Rows[0]["UserID"].ToString();
                    Pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
                    SessionKey = ds.Tables[0].Rows[0]["MeetingKey"].ToString();
                    hdnWebExID.Value = UserID;
                    hdnWebExPwd.Value = Pwd;
                    hdnSessionKey.Value = SessionKey;
                }
            }

            if (SessionKey != "")
            {
                registerMeetingsAttendees(Name, Email.Trim(), City, Country);

                if (hdnMeetingStatus.Value == "SUCCESS")
                {

                    GetJoinUrlMeeting(Name, Email, hdsnRegistrationKey.Value);

                    CmdText = "update CoachReg set AttendeeJoinURL='" + hdnMeetingUrl.Value + "', RegisteredID=" + hdsnRegistrationKey.Value + ", AttendeeID=" + hdnMeetingAttendeeID.Value + ", Status='Active', CMemberID=" + ddlCoach.SelectedValue + " where CoachRegID=" + CoachRegID + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = "Child registered successfully.";
                    fillStudentList();
                }
            }
            else
            {
                lblerr.Text = "No registered WebEx session found for the coach " + ddlCoach.SelectedItem.Text + "";
            }

        }
        catch
        {
        }

    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        DelMeetingAttendee(hdnSessionKey.Value, hdnWebExID.Value, hdnWebExPwd.Value, HdnChildEmail.Value);
        //registerMeetingsAttendees(ChidName, Email.Trim(), City, Country);
        if (hdnMeetingStatus.Value == "SUCCESS")
        {

            //GetJoinUrlMeeting(ChidName, Email, hdsnRegistrationKey.Value);

            string CmdText = "update CoachReg set AttendeeJoinURL=null, RegisteredID=null, AttendeeID=null, Status=null where CoachRegID=" + HdnCoachRegID.Value + "";

            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            lblerr.Text = "Student removed from the session successfully";

            fillStudentList();
        }
    }
}