Imports System
Imports system.io
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Net
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class GameFunctions
    Inherits System.Web.UI.Page

    
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblsb.Text = ""
        lblvb.Text = ""
    End Sub

    Protected Sub lnkSB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkSB.Click
        ' when user click on Spelling game hyperlink
        Dim strSql As String

        Dim dssb As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Session("ProductCode") = "SB"

        strSql = "Select * from Product where Productcode = 'SB' and Eventid = 4 and Status='O'"
        dssb = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        If Not dssb.Tables(0).Rows.Count > 0 Then   ' if Spelling game is not active
            lblsb.Text = "Spelling Game is currently not open."
        Else
            strSql = "Select * from Game where ChildNumber = " & Session("childNumber") & " and ChildLoginID ='" & Session("Childloginid") & "' and ChildPWD ='" & Session("ChildPswd") & "' and ProductCode = 'SB' order by EndDate desc"
            dssb = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If Not dssb.Tables(0).Rows.Count > 0 Then  ' If user didnot register for spelling game
                lblsb.Text = "You do not have access to spelling game.  If you need further assistance, send an email to nsfgame@gmail.com."
            Else
                If dssb.Tables(0).Rows(0)(7).ToString = DBNull.Value.ToString Then    'if registration is in pending
                    lblsb.Text = "Your request for access is pending.  If you do not get access in the next 2 days, send an email to nsfgame@gmail.com."

                ElseIf dssb.Tables(0).Rows(0)(7) < Now.Date() Then  ' if userid expires
                    Dim strSql1 As String = "select RegFee as ReqDonation, ProductID, ProductCode from eventfees where productid = 45"
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                    Dim ReqDonation As Integer = ds1.Tables(0).Rows(0)(0)   ' amount required to register for a game
                    '  lblsb.Text = " Your access has expired.  You need to renew to get access.  If you need further assistance, send an email to nsfgame@gmail.com."
                    lblsb.Text = "The ID is expired and to renew make a tax-deductible donation of $" + ReqDonation.ToString() + " or more and send e-mail to nsfgame@gmail.com with login ID and proof of payment"
                Else
                    Response.Redirect("Spelling_Game\SpellingGame.aspx")
                End If
            End If
        End If

    End Sub

    Protected Sub lnkVB_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkVB.Click
        ' when user click on vocabulary game hyperlink
        Dim strSql As String

        Dim dssb As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Session("ProductCode") = "VB"

        strSql = "Select * from Product where Productcode = 'VB' and Eventid = 4 and Status='O'"
        dssb = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        If Not dssb.Tables(0).Rows.Count > 0 Then ' if Vocabulary game is not active
            lblvb.Text = "Vocabulary Game is currently not open."
        Else
            strSql = "Select * from Game where ChildNumber = " & Session("childNumber") & " and ChildLoginID ='" & Session("Childloginid") & "' and ChildPWD ='" & Session("ChildPswd") & "' and ProductCode = 'VB' order by EndDate desc"
            dssb = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If Not dssb.Tables(0).Rows.Count > 0 Then   ' If user didnot register for vocabulary game
                lblvb.Text = "You do not have access to vocabulary game.  If you need further assistance, send an email to nsfgame@gmail.com"
            Else
                If dssb.Tables(0).Rows(0)(7).ToString = DBNull.Value.ToString Then 'if registration is in pending
                    lblsb.Text = "Your request for access is pending.  If you do not get access in the next 2 days, send an email to nsfgame@gmail.com."

                ElseIf dssb.Tables(0).Rows(0)(7) < Now.Date() Then  ' if userid expires
                    Dim strSql1 As String = "select RegFee as ReqDonation, ProductID, ProductCode from eventfees where productid = 46"
                    Dim ds1 As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
                    Dim ReqDonation As Integer = ds1.Tables(0).Rows(0)(0)   ' amount required to register for a game
                    '  lblsb.Text = " Your access has expired.  You need to renew to get access.  If you need further assistance, send an email to nsfgame@gmail.com."
                    lblsb.Text = "The ID is expired and to renew make a tax-deductible donation of $" + ReqDonation.ToString() + " or more and send e-mail to nsfgame@gmail.com with login ID and proof of payment"
                Else
                    Response.Redirect("Vocab_Game\VocabGame.aspx")
                End If
            End If
        End If

    End Sub

End Class
