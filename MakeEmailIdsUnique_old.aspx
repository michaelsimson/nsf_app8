<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="MakeEmailIdsUnique.aspx.vb" Inherits="VRegistration.MakeEmailIdsUnique" title="Make EmailIds Unique" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
<div align="left">
   <asp:hyperlink id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
    &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
    &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink> 
     
   <table border = "0" cellpadding = "0" cellspacing = "0" width = "800px">
       <tr><td class="title02" vAlign="top" align="center" colspan="2"> Making Email unique between spouses	</td></tr>
       <tr><td align="center"> 
             <table id="tblEmail" runat="server" cellspacing="0" border="0" width="450px">
				    <tr>
					    <td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Current Email:</td>
					    <td  align="left"><asp:textbox id="txtOldEmail" runat="server" CssClass="smFont" Width="150px"></asp:textbox></td>
				    </tr>
				    <tr>
				        <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;Change:</td><td align="left">
				   		    <asp:DropDownList ID="ddlchange" runat="server" AutoPostBack="true">
				   		        <asp:ListItem Value="0" Text= "No">No </asp:ListItem>
				                <asp:ListItem Value="1" Text= "Yes">Yes	</asp:ListItem>
				            </asp:DropDownList>
				        </td> 
				    </tr>
				    <tr>
					    <td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Password:</td>
                   	   <td  align="left"><asp:textbox id="txtPassword" runat="server" CssClass="smFont" TextMode="password" Width="150px" Enabled="false"></asp:textbox>
                               &nbsp;
					   </td>
				    </tr>
				    <tr>
					    <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;New Email Address:
					    </td>
					    <td  align="left"><asp:textbox id="txtNewEmail" runat="server" CssClass="smFont" TextMode="SingleLine" Width="150px" Enabled="false"></asp:textbox>
					    </td>
				    </tr>
				    <tr>
					    <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;Confirm New Email Address:
					    </td>
					    <td  align="left"><asp:textbox id="txtConfirm" runat="server" CssClass="smFont" TextMode="SingleLine" Width="150px" Enabled="false"></asp:textbox>
					  	<br /></td>
				    </tr>

				    <tr>
				        <td></td>
					    <td align="center" colSpan="1" style="HEIGHT: 26px">
                            &nbsp;&nbsp;<asp:button id="btnUpdate" runat="server" CssClass="FormButton" Text="Update" Enabled="false"></asp:button>
                          
                        </td>
				    </tr>
		    </table></td><td align="left">
			<table id="tblEmail1" runat="server" cellspacing="0" border="0" width="450px">
				    <tr>
					    <td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Current Email:</td>
					    <td  align="left"><asp:textbox id="txtOldEmail1" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px"></asp:textbox></td>
				    </tr>
				    <tr>
				        <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;Change:</td><td align="left">
				   		    <asp:DropDownList ID="ddlChange1" runat="server" AutoPostBack="true">
				   		        <asp:ListItem Value="0" Text= "No">No </asp:ListItem>
				                <asp:ListItem Value="1" Text= "Yes">Yes	</asp:ListItem>
				            </asp:DropDownList>
				        </td> 
				    </tr>
				    <tr>
					    <td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Password:</td>
                   	    <td  align="left"><asp:textbox id="txtPassword1" runat="server" CssClass="smFont" TextMode="password" Width="150px" Enabled="false"></asp:textbox>
                               &nbsp;
					   </td>
				    </tr>
				    <tr>
					    <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;New Email Address:
					    </td>
					    <td  align="left"><asp:textbox id="txtNewEmail1" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px" Enabled="false"></asp:textbox>
					    </td>
				    </tr>
				    <tr>
					    <td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;Confirm New Email Address:
					    </td>
					    <td  align="left"><asp:textbox id="txtConfirm1" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px" Enabled="false"></asp:textbox>
					       <br /></td>
				    </tr>
				    <tr>
				        <td></td>
					    <td align="center" colSpan="1" style="HEIGHT: 26px">
                            &nbsp;&nbsp;
                            <asp:button id="btnUpdate1" runat="server" CssClass="FormButton" Text="Update" Enabled="false"></asp:button>
                                                   </td>
				    </tr>
			</table>
		</td></tr>
		<tr><td align="center" colspan="2">
			    <asp:label id="lblError" runat="server" ForeColor="red" Text=""></asp:label>
        </td></tr>
   </table>
            <table border = "0" cellpadding = "0" cellspacing ="0"  runat="server" align="center">
                    <tr visible = "false" id="trEmailChangeNw" runat="server">
                        <td align="center">
                          The new email address already exists as LoginID along with a password.  Please provide the password.ide the password.<br />
                              <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server"></asp:TextBox> &nbsp;&nbsp;
                              <asp:Button ID="btnUpdateNw" OnClick="btnUpdateNw_Click" runat="server" Text="Update" />
                        </td>
                    </tr> 
                    <tr visible = "false" id="DisplaySuccessMsg" runat="server">
                        <td align="center" ><br /><br />
                              <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">
                              Your record was updated successfully.  Please click <a href="Http://northsouth.org">here</a> to continue
                              </p>
                        </td>
                    </tr>
                    <tr visible = "false" id="DisplayFailMsg" runat="server">
                        <td align="center" ><br /><br />
                         <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
                          New Email already exists.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@gmail.com" target="_blank" >nsfcontests@gmail.com</a>.  Please click <a href="Http://northsouth.org">here</a> to continue.
                         </p>
                        </td>
                   </tr>
                  <tr visible = "false" id="DisplayFailMsg5" runat="server">
                        <td align="center" >
                          <br /><br />
                         <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
                          New Email already exists in multiple places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@gmail.com" target="_blank" >nsfcontests@gmail.com</a>. Please click<a href="Http://northsouth.org">here</a> to continue.
                          </p>
                       </td>
                  </tr>
                  <tr visible = "false" id="DisplayFailMsg6" runat="server">
                         <td align="center" >
                          <br /><br />
                         <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">  
                          Old email has duplicates in multiple places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@gmail.com" target="_blank" >nsfcontests@gmail.com</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
                          </p>
                        </td>
                  </tr>
                  <tr visible = "false" id="DisplayFailMsg2" runat="server">
                          <td align="center" >
                              <br /><br />
                             <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">  
                              Old email has duplicates in multiple places.  Further new email exists already.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@gmail.com" target="_blank" >nsfcontests@gmail.com</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
                              </p>
                          </td>
                   </tr>
                   <tr visible = "false" id="DisplayFailMsg3" runat="server">
                  <td align="center" >
                          <br /><br />
                          <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
                          Old email exists in more than two places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@gmail.com" target="_blank" >nsfcontests@gmail.com</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
                          </p>
                          </td>
                  </tr>      
                  <tr>
                      <td align="center" >
                      <br /><br />
                      <asp:label id="txtErrMsg" runat="server" ForeColor="red" Visible="False"> Invalid Current Email.</asp:label>
                       </td>
                  </tr>
            </table>	
     
  </div>	
</asp:Content>


 

 
 
 