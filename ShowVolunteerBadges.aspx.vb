Imports NorthSouth.BAL

Partial Class ShowVolunteerBadges
    Inherits System.Web.UI.Page
    Dim generateBadge As GenerateBadges
    Protected lblHeader1 As Label
    Protected lblHeader2 As Label
    Protected lblName As Label
    Protected lblTitle As Label
    Protected lblBadgeNumber As Label
    Protected lblChapterCode As Label
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            LoadBadges()
        End If
    End Sub
    Private Sub LoadBadges()
        Dim dsBadge As New DataSet
        Dim badge As New Badge()
        Dim i As Integer
        Dim j As Integer
        Dim iBadgeCount As Integer
        Dim iPerPageCount As Integer
        Dim strBandgeNumber As String
        generateBadge = CType(Context.Handler, GenerateBadges)
        dsBadge = badge.GetVolunteerBadges(Application("ConnectionString"), Session("SelChapterID"))
        If dsBadge.Tables(0).Rows.Count > 0 Then
            iPerPageCount = 10
            j = 1
            For i = 1 To dsBadge.Tables(0).Rows.Count
                iBadgeCount = 0
                strBandgeNumber = ""
                lblHeader1 = New Label
                lblHeader2 = New Label
                lblName = New Label
                lblChapterCode = New Label

                Dim dsBadgeNumber As New DataSet
                If i Mod 2 = 1 Then
                    BadgeHolder.Controls.Add(New LiteralControl("<tr style='height:3.0in;' >"))
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center' style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'>"))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.Font.Size = 14
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.Font.Size = 12
                    lblHeader2.ForeColor = Color.Navy
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("FirstName").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("LastName").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblTitle = New Label
                    lblTitle.Text = "VOLUNTEER"
                    lblTitle.Font.Size = 15
                    BadgeHolder.Controls.Add(lblTitle)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                Else
                    BadgeHolder.Controls.Add(New LiteralControl("<td align='center'  style='width:4.0in;padding:0in .75pt 0in .75pt;height:3.0in'> "))
                    lblHeader1.Text = "North South Foundation"
                    lblHeader1.Font.Bold = True
                    lblHeader1.Font.Name = "Verdana"
                    lblHeader1.ForeColor = Color.Navy
                    lblHeader1.Font.Size = 14
                    BadgeHolder.Controls.Add(lblHeader1)
                    lblHeader2.Text = "Regional Educational Contests"
                    lblHeader2.Font.Name = "Times New Roman"
                    lblHeader2.ForeColor = Color.Navy
                    lblHeader2.Font.Size = 12
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblHeader2)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblName.Text = dsBadge.Tables(0).Rows(i - 1)("FirstName").ToString() & " " & dsBadge.Tables(0).Rows(i - 1)("LastName").ToString()
                    lblName.Font.Name = "Arial"
                    lblName.Font.Bold = True
                    If Len(lblName.Text) > 22 Then
                        lblName.Font.Size = 12
                    Else
                        lblName.Font.Size = 15
                    End If
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(lblName)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    lblTitle = New Label
                    lblTitle.Text = "VOLUNTEER"
                    lblTitle.Font.Size = 15
                    BadgeHolder.Controls.Add(lblTitle)
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))
                    BadgeHolder.Controls.Add(New LiteralControl("<BR>"))

                    BadgeHolder.Controls.Add(New LiteralControl("</td>"))
                    BadgeHolder.Controls.Add(New LiteralControl("</tr>"))
                End If
                If iPerPageCount = j Then
                    j = 1
                    'BadgeHolder.Controls.Add(New LiteralControl("<tr><td colspan='2'>&nbsp;</td></tr>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<div style='page-break-after:always'>&nbsp;</div>"))
                    'BadgeHolder.Controls.Add(New LiteralControl("<P CLASS='breakhere'>"))
                End If
                j = j + 1
            Next
            pnlData.Visible = True
            pnlMessage.Visible = False
        Else
            pnlData.Visible = False
            pnlMessage.Visible = True
            lblMessage.Text = "No Badge Details found."
        End If
        If generateBadge.Export = True Then
            Response.Clear()
            Response.Buffer = True
            Dim strChapterName As String
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                strChapterName = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            Response.AddHeader("content-disposition", "attachment;filename=Badges_Volunteers_" & strChapterName & ".doc")
            Response.Charset = ""
            Response.ContentType = "application/vnd.word"
            Dim stringWrite As New System.IO.StringWriter()
            Dim htmlWrite As New HtmlTextWriter(stringWrite)
            pnlData.RenderControl(htmlWrite)
            Response.Write("<html>")
            Response.Write("<head>")
            Response.Write("<style>")
            Response.Write(" @page Section1 {size:8.5in 11.0in;margin:50.0pt .25in 0in .25in;mso-header-margin:.5in;mso-footer-margin:.5in;mso-paper-source:4;}")
            Response.Write("div.Section1 {page:Section1;}")
            Response.Write("</style>")
            Response.Write("</head>")
            Response.Write("<body>")
            Response.Write(stringWrite.ToString())
            Response.Write("</body>")
            Response.Write("</html>")
            Response.End()
        End If
    End Sub
End Class
