<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContestTeam.aspx.vb" Inherits="ContestTeam" Title="Contest Team Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div align="left">
        <asp:HyperLink ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx" CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
    </div>
    <table align="right">
        <tr>
            <td>
                <asp:Button ID="btnExportReport" runat="server" Text="Generate Report" /></td>
        </tr>
    </table>

    <table id="MainTable" runat="server" align="left" border="0">
        <tr>
            <td align="center">Contest Team Schedule </td>
        </tr>
        <tr>
            <td>
                <table border="1" runat="server" align="left">
                    <tr>
                        <td align="left" width="100px">ContestYear </td>
                        <td>
                            <asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year" AutoPostBack="true" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="100px">Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" DataTextField="Name" DataValueField="EventId" AutoPostBack="true" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="100px">Chapter</td>
                        <td>
                            <asp:DropDownList ID="ddlChapter" DataTextField="ChapterCode" DataValueField="ChapterId" AutoPostBack="true" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="100px">Purpose</td>
                        <td>
                            <asp:DropDownList ID="ddlPurpose" DataTextField="Purpose" DataValueField="Purpose" AutoPostBack="true" runat="server" Width="150px"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                        <td width="100px">Date </td>
                        <td>
                            <asp:DropDownList ID="ddlDate" runat="server" Width="125px" Visible="true" AutoPostBack="true"></asp:DropDownList></td>
                        <td align="left" style="width: 10px"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table border="0" runat="server" align="center">
                    <tr>
                        <td>
                            <table border="1" runat="server">
                                <tr id="TrProductGroup" runat="server">
                                    <td align="left" width="100px">ProductGroup </td>
                                    <td>
                                        <asp:DropDownList ID="ddlProductGroup" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrProduct" runat="server">
                                    <td width="100px">Product</td>
                                    <td>
                                        <asp:DropDownList ID="ddlProduct" DataTextField="ProductCode" DataValueField="ProductId" AutoPostBack="true" runat="server" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrPhase" runat="server">
                                    <td width="100px">Phase</td>
                                    <td>
                                        <asp:DropDownList ID="ddlPhase" AutoPostBack="true" DataTextField="Phase" DataValueField="Phase" runat="server" Width="125px">
                                            <%--  <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                                    <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                                    <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                                    <asp:ListItem Text="3" Value="3">3</asp:ListItem>--%>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="TrBldg" runat="server">
                                    <td>BldgName</td>
                                    <td>
                                        <asp:DropDownList ID="ddlBldgID" runat="server" AutoPostBack="true" DataTextField="BldgID" DataValueField="BldgID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrSeqNo" runat="server">
                                    <td align="left" width="100px">SeqNo</td>
                                    <td>
                                        <asp:DropDownList ID="ddlSeqNo" runat="server" AutoPostBack="true" DataTextField="SeqNo" DataValueField="SeqNo" Enabled="true" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrRoomNo" runat="server">
                                    <td align="left" width="100px">RoomNumber</td>
                                    <td>
                                        <asp:DropDownList ID="ddlRoomNo" runat="server" AutoPostBack="true" DataTextField="RoomNumber" DataValueField="RoomNumber" Enabled="false" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrRoomMc" runat="server">
                                    <td>Room MC</td>
                                    <td>
                                        <asp:DropDownList ID="ddlRoomMc" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>

                            </table>
                        </td>
                        <td align="center" runat="server">
                            <table border="1" runat="server" align="center">
                                <tr id="TrPronouncer" runat="server">
                                    <td>Pronouncer</td>
                                    <td>
                                        <asp:DropDownList ID="ddlPronouncer" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrChiefJudge" runat="server">
                                    <td>Chief Judge</td>
                                    <td>
                                        <asp:DropDownList ID="ddlChiefJudge" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrAssociateJudge" runat="server">
                                    <td>Associate Judge</td>
                                    <td>
                                        <asp:DropDownList ID="ddlAssociateJudge" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrLaptopJudge" runat="server">
                                    <td>Laptop Judge</td>
                                    <td>
                                        <asp:DropDownList ID="ddlLaptopJudge" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrDictHandler" runat="server">
                                    <td>Dict Handler</td>
                                    <td>
                                        <asp:DropDownList ID="ddlDictHandler" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrGrader" runat="server">
                                    <td>Grading</td>
                                    <td>
                                        <asp:DropDownList ID="ddlGrading" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Visible="true" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrProctor" runat="server">
                                    <td>Proctor</td>
                                    <td>
                                        <asp:DropDownList ID="ddlProctor" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                                </tr>
                                <tr id="TrTimer" runat="server">
                                    <td>Timer</td>
                                    <td>
                                        <asp:DropDownList ID="ddlTimer" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr id="TrCopy" runat="server" align="center" visible="false">
            <td>
                <table>
                    <tr>
                        <%--<td width="75px">From </td>--%>

                        <td width="120px" align="right">To(ProductGroup)</td>
                        <td>
                            <asp:DropDownList ID="ddlToPG" AutoPostBack="true" runat="server" DataTextField="ProductGroupCode" DataValueField="ProductGroupID" Width="125px" Enabled="True"></asp:DropDownList>
                            <asp:DropDownList ID="ddlToProduct" AutoPostBack="true" runat="server" DataTextField="Productcode" DataValueField="ProductID" Width="125px" Enabled="True"></asp:DropDownList>
                            &nbsp;&nbsp;
                        </td>
                        <%--<td>Phase</td><td><asp:DropDownList ID="ddlPhase1" AutoPostBack="true" runat="server" width="125px" Enabled="True">
                        <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                        <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                        <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                        <asp:ListItem Text="3" Value="3">3</asp:ListItem>
                         </asp:DropDownList> &nbsp;&nbsp;
                     </td>--%>
                        <%-- <td>
                StartTime</td><td><asp:DropDownList ID="ddlStartCpTime" DataTextField="StartTime" DataValueField="StartTime" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
               </td><td> EndTime</td><td><asp:DropDownList ID="ddlEndCpTime" DataTextField="EndTime" DataValueField="EndTime" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
           </td>--%>

                        <td>
                            <asp:Button ID="BtnCopySchedule" runat="server" Text="CopySchedule"></asp:Button></td>
                    </tr>
                </table>
            </td>
        </tr>

        <tr id="TrAddUpdate" runat="server">
            <td align="center">
                <asp:Button ID="BtnCopy" runat="server" Width="100px" Text="Copy"></asp:Button>
                <asp:Button ID="btnAddUpdate" runat="server" Width="100px" Text="Add/Update"></asp:Button>
                <asp:Button ID="BtnCancel" OnClick="BtnCancel_Click" runat="server" Text="Cancel" /></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblAddUPdate" runat="server" Text="" ForeColor="Red"></asp:Label>
                <asp:DataGrid ID="DGRoomSchedule" runat="server" DataKeyField="RoomSchID" AutoGenerateColumns="False" OnItemCommand="DGRoomSchedule_ItemCommand" CellPadding="4"
                    GridLines="Both" BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black">
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <ItemStyle BackColor="White" />
                    <Columns>
                        <%--0--%>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" Text="Delete" Enabled="False"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit" Enabled="false"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--2--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Chapter" HeaderText="Chapter" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Event" HeaderText="Event" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID" />
                        <%--6--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SeqNo" HeaderText="SequenceNo" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="RoomNumber" HeaderText="RoomNumber" />
                        <asp:TemplateColumn HeaderText="Capacity" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblCapacity" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Capacity") %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--<asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Date" DataFormatString="{0:d}" HeaderText="Date" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup" />
                        <%--11--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Phase" HeaderText="Phase" />
                       <%-- <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="StartTime" HeaderText="StartTime" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EndTime" HeaderText="EndTime" />--%>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="StartBadgeNo" HeaderText="StartBadgeNo" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EndBadgeNo" HeaderText="EndBadgeNo" />
                        <%--15--%>
                        <asp:TemplateColumn HeaderText="RoomMc" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblRoomMc" runat="server" Text=""></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Pronouncer" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblPronouncer" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="ChiefJudge" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblChiefJudge" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="AssociateJudge" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblAssociateJudge" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="LaptopJudge" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblLaptopJudge" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--20--%>
                        <asp:TemplateColumn HeaderText="DictHandler" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblDictHandler" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Grading" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblGrading" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Proctor" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblProctor" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn HeaderText="Timer" HeaderStyle-Font-Bold="true">
                            <ItemTemplate>
                                <asp:Label ID="lblTimer" runat="server"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <%--24--%>
                        <asp:TemplateColumn HeaderText="ContTeamID" HeaderStyle-Font-Bold="true" Visible="false">
                            <ItemTemplate>
                                <asp:Label ID="lblContTeamID" runat="server" Visible="false"></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateColumn>

                    </Columns>
                    <HeaderStyle BackColor="White" />
                </asp:DataGrid>

            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblErr" runat="server" Text="" ForeColor="Red"></asp:Label></td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblRoomSchID" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td>
        </tr>
    </table>

    <table align="left">
        <tr>
            <td></td>
        </tr>
    </table>
</asp:Content>

