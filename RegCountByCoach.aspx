﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="RegCountByCoach.aspx.cs" Inherits="RegCountByCoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">

        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx"
            runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    </div>
    <div id="Div1" runat="server" align="center">
        <div style="width: 1000px; margin-right: auto; margin-left: auto;">
            <center><span style="font-weight: bold; color: green; font-size: 18px;">Registration Count</span> </center>
            <div style="clear: both; margin-bottom: 20px;"></div>
            <div id="dvFrstRow">
                <div style="float: left; width: 210px;">
                    <div style="float: left; width: 90px;">
                        <span style="font-weight: bold;">Event Year</span>
                    </div>
                    <div style="float: left; width: 110px;">
                        <asp:DropDownList ID="lstYear" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="lstYear_SelectedIndexChanged"></asp:DropDownList>

                    </div>
                </div>
                <div style="float: left; width: 210px;">
                    <div style="float: left; width: 90px;">
                        <span style="font-weight: bold;">Semester</span>
                    </div>
                    <div style="float: left; width: 110px;">
                        <asp:DropDownList Width="100px" ID="ddlSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                            <asp:ListItem Value="Fall">Fall</asp:ListItem>
                            <asp:ListItem Value="Spring">Spring</asp:ListItem>
                            <asp:ListItem Value="Summer">Summer</asp:ListItem>

                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 250px;">
                    <div style="float: left; width: 90px;">
                        <span style="font-weight: bold;">Produt Group</span>
                    </div>
                    <div style="float: left; width: 150px;">
                        <asp:DropDownList ID="lstProductGroup" SelectionMode="Multiple" runat="server" Width="140px"
                            OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 250px;">
                    <div style="float: left; width: 90px;">
                        <span style="font-weight: bold;">Product</span>
                    </div>
                    <div style="float: left; width: 150px;">
                        <asp:DropDownList ID="lstProduct" SelectionMode="Multiple" runat="server" Width="140px" AutoPostBack="true" OnSelectedIndexChanged="lstProduct_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 10px;">
                    <asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick="BtnClear_Click" />
                </div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label></center>
        </div>
        <table style="display: none;">
            <tr>
                <td style="width: 80px"></td>
                <td>
                    <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
                        <tr>
                            <td>
                                <table border="0" cellpadding="3" cellspacing="0" align="center" width="400px" class="style2">
                                    <tr>
                                        <td align="Center" colspan="2" class="title02">Coaching Registration Count</td>
                                    </tr>
                                    <tr>
                                        <td align="left">Product Group Name </td>
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Product Name </td>
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="left"></td>
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="left">Semester</td>
                                        <td align="left"></td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2">
                                            <asp:Button ID="btnGetCount" runat="server" Text="Get Count"
                                                OnClick="btnGetCount_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td align="center" colspan="2"></td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
                <td>
                    <table>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </div>

    <br />
    <div align="center" id="Div5" visible="false"
        runat="server">
        <b>Table 1: Registrations by Coach </b>


    </div>
    <div>
        <div style="float: right; margin-right: 15px;">
            <div style="float: left;">
                <b>Sort : </b>
            </div>
            <div style="float: left; margin-left: 5px;">
                <asp:DropDownList Width="180px" ID="ddSorting" runat="server"
                    AutoPostBack="True" OnSelectedIndexChanged="ddSorting_SelectedIndexChanged">
                    <asp:ListItem Value="LastName,FirstName">By Last Name,First Name</asp:ListItem>
                    <asp:ListItem Value="DayTime">By Day/Time</asp:ListItem>
                    <asp:ListItem Value="Paid desc">By Number paid</asp:ListItem>
                    <asp:ListItem Value="Approved desc">By Approved</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <asp:DataGrid ID="grdCoaching" HorizontalAlign="Center" BorderWidth="1px" runat="server"
            AutoGenerateColumns="False"
            CellPadding="4" ForeColor="#333333" GridLines="None">
            <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000"
                Font-Bold="True" ForeColor="White"></HeaderStyle>
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderText="S.No." HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White" DataField="SNo" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="FirstName" HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White" DataField="FirstName" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="LastName" DataField="LastName"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="ProductCode" DataField="ProductCode"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Level" DataField="Level"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Semester" DataField="Semester"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Session" DataField="SessionNo"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Day" DataField="day"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Time" DataField="time"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />

                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="Capacity" DataField="Capacity"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="Paid" DataField="paid"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="Pending" DataField="Pending"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" HeaderText="Approved" DataField="Approved"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Right" HeaderText="paidAmount" DataField="PaidAmt"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" DataFormatString="{0:c}" />

            </Columns>
            <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
        </asp:DataGrid>
    </div>
    <div align="center" id="Div8"
        runat="server">
        <asp:Label ID="lberrcoach" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
    <br />
    <table border="0" cellpadding="2" cellspacing="0" style="text-align: left">
    </table>
    <div align="center" id="HeadText" visible="false"
        runat="server">
        <b>Table 2: Coaches with no Registrations</b>


    </div>
    <div>
        <asp:DataGrid ID="DataGridNoreg" HorizontalAlign="Center" BorderWidth="1px" runat="server"
            AutoGenerateColumns="False"
            CellPadding="4" ForeColor="#333333" GridLines="None">
            <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000"
                Font-Bold="True" ForeColor="White"></HeaderStyle>
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderText="S.No." HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White" DataField="SNo" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="FirstName" HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White" DataField="FirstName" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="LastName" DataField="LastName"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="ProductCode" DataField="ProductCode"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Level" DataField="Level"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Semester" DataField="Semester"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Session" DataField="SessionNo"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Day" DataField="day"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Time" DataField="time"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" />


            </Columns>
            <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
        </asp:DataGrid>
    </div>
    <div align="center" id="Div2"
        runat="server">
        <asp:Label ID="lblNoreg" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
    <br />

    <div align="center" id="Div3" visible="false"
        runat="server">
        <b>Table 3: Coaches Signed Up, but not yet Accepted
        </b>


    </div>
    <div>
        <asp:DataGrid ID="DGSigned" HorizontalAlign="Center" BorderWidth="1px" runat="server"
            AutoGenerateColumns="False" AllowPaging="true" PageSize="30"
            CellPadding="4" ForeColor="#333333" GridLines="None"
            OnPageIndexChanged="DGSigned_PageIndexChanged">
            <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
            <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000"
                Font-Bold="True" ForeColor="White"></HeaderStyle>
            <AlternatingItemStyle BackColor="White" />
            <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderText="S.No."
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" DataField="SNo">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="FirstName"
                    HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"
                    DataField="FirstName">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="LastName"
                    DataField="LastName" HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="ProductCode"
                    DataField="ProductCode" HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Level"
                    DataField="Level" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Semester"
                    DataField="Semester" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Session"
                    DataField="SessionNo" HeaderStyle-Font-Bold="true"
                    HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Day"
                    DataField="day" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">
                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>
                <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Time"
                    DataField="time" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">


                    <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                </asp:BoundColumn>


            </Columns>
            <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False" HorizontalAlign="Right" Mode="NumericPages"
                NextPageText="" PrevPageText=""></PagerStyle>
        </asp:DataGrid>
    </div>

    <div align="center" id="Div4"
        runat="server">
        <asp:Label ID="lbcalsignup" runat="server" Text="" ForeColor="Red"></asp:Label>
    </div>
    <br />
    <div align="center" id="Div6" visible="false"
        runat="server">
        <b>Table 4: Contact informations for Coaches Signed Up, but not yet Accepted
        </b>

        <div>
            <asp:DataGrid ID="DgridContactInfo" HorizontalAlign="Center"
                BorderWidth="1px" runat="server" AutoGenerateColumns="False"
                CellPadding="4" ForeColor="#333333" GridLines="None"
                AllowPaging="True" OnPageIndexChanged="DgridContactInfo_PageIndexChanged"
                PageSize="30">
                <FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
                <HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000"
                    Font-Bold="True" ForeColor="White"></HeaderStyle>
                <AlternatingItemStyle BackColor="White" />
                <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
                <Columns>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="center" HeaderText="S.No."
                        HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White" DataField="SNo">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Center"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="FirstName"
                        HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White"
                        DataField="FirstName">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="LastName"
                        DataField="LastName" HeaderStyle-Font-Bold="true"
                        HeaderStyle-ForeColor="White">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Email"
                        DataField="Email" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Hphone"
                        DataField="Hphone" HeaderStyle-Font-Bold="true"
                        HeaderStyle-ForeColor="White">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="Cphone"
                        DataField="Cphone" HeaderStyle-Font-Bold="true"
                        HeaderStyle-ForeColor="White">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="City"
                        DataField="City" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">
                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>
                    <asp:BoundColumn ItemStyle-HorizontalAlign="Left" HeaderText="State"
                        DataField="state" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="White">



                        <HeaderStyle Font-Bold="True" ForeColor="White"></HeaderStyle>

                        <ItemStyle HorizontalAlign="Left"></ItemStyle>
                    </asp:BoundColumn>



                </Columns>
                <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
                <PagerStyle Wrap="False" HorizontalAlign="Right" Mode="NumericPages"></PagerStyle>
            </asp:DataGrid>
        </div>

        <div align="center" id="Div7"
            runat="server">
            <asp:Label ID="lblContact" runat="server" Text="" ForeColor="Red"></asp:Label>
        </div>
    </div>
</asp:Content>

