﻿
Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class Shoppingstatus
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack Then
            Dim Year As Integer = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(Year - 1))
            ddlEventYear.Items.Insert(1, DateTime.Now.Year.ToString())
            ddlEventYear.Items(1).Selected = True
            loadgrid()
        End If
    End Sub

    Private Sub loadgrid()
        Dim strSQL As String = "SELECT C.CatID, C.Category, C.ShortName, C.Description, C.UnitPrice, S.SaleTranID,S.Quantity ,S.Amount,S.EventYear,Ch.ChapterCode,E.Name as EventName,S.PaymentDate,CASE WHEN S.PaymentReference IS NULL THEN 'Pending' ELSE  S.PaymentReference END as PaymentReference FROM Catalog C INNER JOIN SaleTran S ON C.CatID = S.CatID AND S.Memberid=" & Session("CustIndID") & " INNER JOIN Chapter Ch ON S.ChapterID=Ch.ChapterID INNER JOIN Event E ON S.SEventID=E.EventID WHERE S.EventYear=" & ddlEventYear.SelectedValue & " ORDER BY S.PaymentDate Desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        dgCatalog.DataSource = dt
        dgCatalog.DataBind()
        If (Count < 1) Then
            lblerr.Text = "No Record found."
            dgCatalog.Visible = False
        Else
            lblerr.Text = ""
            dgCatalog.Visible = True
        End If
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadgrid()
    End Sub
End Class
