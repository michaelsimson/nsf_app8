<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="IndiaScholarships.aspx.vb" Inherits="IndiaScholarships" title="India Scholarships" EnableEventValidation="false"%>
    
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div style="text-align:left">&nbsp;
            <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    </div>
    <table  cellpadding = "2" cellspacing = "0" border="0" width="1000px">
            <tr><td colspan="2" align="center"><div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong>Scholarship Designations</strong> </div></td></tr>
            <tr align ="right"><td colspan="2"><asp:Button ID ="BtnExport" runat="server" Text="Export To Excel"/></td></tr>
            <tr><td>
                <table  cellpadding = "5" cellspacing = "0" border="0" align="left">
                        <tr><td align="left">Donor</td>
                            <td align="left" ><asp:TextBox ID="txtDonor" runat="server" Width="150px" Height="18px"></asp:TextBox>
                            <asp:HiddenField ID="hdnMemberId" runat="server" Visible="false" />
                            <asp:Label ID="lblDonorType" runat="server" Visible ="false" ></asp:Label>
                            </td>
                            <td><asp:Button ID="BtnSearch" runat="server" Text="Search"/></td>
                        </tr><tr>
                            <td align="left">Designation Type</td>
                            <td align="left"><asp:DropDownList ID="ddlDesignation" Width="155px" Height="23px" runat="server">
                                                 <asp:ListItem Value="0" Text="Select"></asp:ListItem>
                                                 <asp:ListItem Value="College" Text="College"></asp:ListItem>
                                                 <asp:ListItem Value="HighSchool" Text="HighSchool"></asp:ListItem>
                                             </asp:DropDownList></td>
                        </tr><tr>
                            <td align="left">Number</td>
                            <td align="left"><asp:TextBox ID="txtNumber" runat="server"  Width="150px" Height="18px"></asp:TextBox></td>
                        </tr><tr>
                            <td align="left">Chapter</td>
                            <td align="left"><asp:DropDownList ID="ddlChapter" DataTextField="chapter" DataValueField="chapterId" AutoPostBack="true" runat="server" Width="155px" Height="23px"></asp:DropDownList></td>
                        </tr><tr >
                             <td align="left">Institution Name</td><td align="left"><asp:DropDownList ID="ddlInstitution" DataTextField="InstitutionName" DataValueField="InstitutionCode" runat="server" Width="155px" Height="23px" ></asp:DropDownList></td>
                        </tr>

                </table>
                 <table  cellpadding = "5" cellspacing = "0" border="0" align="left">
                            <td align="left">Geographic Region</td><td align="left"><asp:TextBox ID="txtGeoRegion" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr><tr>
                             <td align="left">Course</td>
                             <td align="left">
                                    <asp:DropDownList ID="ddlCourse" runat="server" Width="155px" Height="23px"  DataTextField ="crs" DataValueField="id">
                                        <%--<asp:ListItem Value="Engineering" Text="Engineering"></asp:ListItem>
                                        <asp:ListItem Value="Medicine" Text="Medicine"></asp:ListItem>
                                        <asp:ListItem Value="Others" Text="Others"></asp:ListItem>--%>
                                    </asp:DropDownList></td>
                        </tr><tr>
                            <td align="left">Comments</td><td align="left"><asp:TextBox ID="txtComments" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr>
                             <td align="left">Tenure</td>
                             <td align="left">
                                    <asp:DropDownList ID="ddlTenure" runat="server" Width="155px" Height="23px"  >
                                        <asp:ListItem Value="SelectTenure" Text="Select Tenure"></asp:ListItem>
                                        <asp:ListItem Value="One Year" Text="One Year"></asp:ListItem>
                                        <asp:ListItem Value="Perpetual" Text="Perpetual"></asp:ListItem>
                                    </asp:DropDownList></td>
                        </tr>
                        <tr>
                             <td align="left">Status</td>
                             <td align="left">
                                    <asp:DropDownList ID="ddlStatus" runat="server" Width="155px" Height="23px"  >
                                        <asp:ListItem Value="SelectStatus" Text="Select Status"></asp:ListItem>
                                        <asp:ListItem Value="Active" Text="Active"></asp:ListItem>
                                        <asp:ListItem Value="inactive" Text="Inactive"></asp:ListItem>
                                    </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="btnAdd" runat="server" Text="Add" /> &nbsp;
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                            </td>
                        </tr>
                </table>
    <asp:Panel ID="pIndSearch" runat="server" Visible="False">
       <b> Search NSF member</b>   
        <div align = "center">       
            <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
                 <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Donor Type:</td>
			        <td align="left" >
                        <asp:DropDownList ID="ddlDonorType" AutoPostBack="true" runat="server" 
                            onselectedindexchanged="ddlDonorType_SelectedIndexChanged">
                            <asp:ListItem Value="INDSPOUSE">IND/SPOUSE</asp:ListItem>
                            <asp:ListItem Value="Organization">Organization</asp:ListItem>
                        </asp:DropDownList></td>
    	        </tr>
                <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			        <td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	        </tr>
    	        <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			        <td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	        </tr>
    	        <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Organization Name:</td>
			        <td  align ="left" ><asp:TextBox ID="txtOrgName" runat="server"></asp:TextBox></td>
    	        </tr>
    	         <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			        <td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	        </tr>
    	         <tr>
                    <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			        <td align="left" >
                        <asp:DropDownList ID="ddlState" runat="server">
                        </asp:DropDownList></td>
    	        </tr>
            	
    	        <tr>
    	            <td align="right">
    	                <asp:Button ID="Button1" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	            </td>
    	            <td  align="left">					
		                <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		            </td>
    	        </tr>		
	        </table>
	        <asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>

        <br />

            
        </div>
	</asp:Panel>
    </td>
               
            </tr>
        
        <tr>
                <td align="center" >
                    <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
                     <asp:Label ID="lblSchDesg_ID" ForeColor="Red" runat="server" Visible ="false"></asp:Label>
                    
                    <br />
                </td>
            </tr>
        <tr>
            <td>
                <asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
             <b> Search Result</b>
                     <asp:GridView   HorizontalAlign="Left" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server"  RowStyle-CssClass="SmallFont" OnRowCommand="GridMemberDt_RowCommand">         
                     <Columns>
                                            <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                            <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                            <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                            <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                            <asp:BoundField DataField="DonorType" headerText="DonorType" ></asp:BoundField>
                                            <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                            <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                            <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                            <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                            <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                            <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                       
                     </Columns>        
                     
                  </asp:GridView>    
             </asp:Panel>
            </td>
        </tr>
    <tr>
        <td align="center">
            <b><u><asp:Label ID="lblTable1" runat="server" Text="Table 1: College Scholarship Designations"></asp:Label></u></b>
        </td>
    </tr>
            <tr><td>
                            <asp:datagrid id="DGDonorDesig"  runat="server" DataKeyField="SchDesg_ID" AutoGenerateColumns="False"
		                    Width="945px" Height="304px" BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" BackColor="White"
		                    CellPadding="3" GridLines="Horizontal" OnItemCommand="DGDonorDesig_ItemCommand">
		                    <Columns>
		                         <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
	                             <asp:ButtonColumn Text="Modify" ButtonType="PushButton" CommandName="Modify"></asp:ButtonColumn> 
	                             <asp:BoundColumn DataField ="SchDesg_ID" HeaderText="SchDesg_ID" ></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="DonorType" HeaderText="DonorType"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="MemberId" HeaderText="MemberId"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="DonorName" HeaderText="DonorName"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Designation" HeaderText="Designation"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Number" HeaderText="Number"></asp:BoundColumn>
                                <asp:BoundColumn DataField ="Tenure" HeaderText="Tenure"></asp:BoundColumn>
                                <asp:BoundColumn DataField ="Status" HeaderText="Status"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Chapter" HeaderText="Chapter"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="InstitutionName" HeaderText="InstitutionName"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="GeographicRegion" HeaderText="GeographicRegion"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Course" HeaderText="Course"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Comments" HeaderText="Comments"></asp:BoundColumn>
	                      
                            </Columns>
        		            <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
		                    <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C" BorderColor="blue"></SelectedItemStyle>
		                    <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
		                    <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF" ></ItemStyle>
		                    <HeaderStyle Font-Bold="True" ForeColor="Green" BackColor="SkyBlue"></HeaderStyle>
		                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
	                </asp:datagrid>
	        </td></tr>
            
  
            <tr><td align="center"><asp:Label ID="lblGridMsg" Text="" runat="server"></asp:Label></td></tr> 

      <tr>
        <td align="center">
            <b><u><asp:Label ID="lblTable2" runat="server" Text="Table 2: High School Scholarship Designations"></asp:Label></u></b>
        </td>
    </tr>
            <tr><td>
                            <asp:datagrid id="dgHighSchool"  runat="server" DataKeyField="SchDesg_ID" AutoGenerateColumns="False"
		                     BorderColor="#E7E7FF" BorderStyle="None" BorderWidth="1px" BackColor="White"
		                    CellPadding="3" GridLines="Horizontal" OnItemCommand="dgHighSchool_ItemCommand">
		                    <Columns>
		                         <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
	                             <asp:ButtonColumn Text="Modify" ButtonType="PushButton" CommandName="Modify"></asp:ButtonColumn> 
	                             <asp:BoundColumn DataField ="SchDesg_ID" HeaderText="SchDesg_ID" ></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="DonorType" HeaderText="DonorType"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="MemberId" HeaderText="MemberId"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="DonorName" HeaderText="DonorName"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Designation" HeaderText="Designation"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Number" HeaderText="Number"></asp:BoundColumn>
                                <asp:BoundColumn DataField ="Tenure" HeaderText="Tenure"></asp:BoundColumn>
                                <asp:BoundColumn DataField ="Status" HeaderText="Status"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Chapter" HeaderText="Chapter"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="InstitutionName" HeaderText="InstitutionName"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="GeographicRegion" HeaderText="GeographicRegion"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Course" HeaderText="Course"></asp:BoundColumn>
	                             <asp:BoundColumn DataField ="Comments" HeaderText="Comments"></asp:BoundColumn>
	                      
                            </Columns>
        		            <FooterStyle ForeColor="#4A3C8C" BackColor="#B5C7DE"></FooterStyle>
		                    <SelectedItemStyle Font-Bold="True" ForeColor="#F7F7F7" BackColor="#738A9C" BorderColor="blue"></SelectedItemStyle>
		                    <AlternatingItemStyle BackColor="#F7F7F7"></AlternatingItemStyle>
		                    <ItemStyle ForeColor="#4A3C8C" BackColor="#E7E7FF" ></ItemStyle>
		                    <HeaderStyle Font-Bold="True" ForeColor="Green" BackColor="SkyBlue"></HeaderStyle>
		                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
	                </asp:datagrid>
	        </td></tr>
            
  
            <tr><td align="center"><asp:Label ID="lblGridMsg2" Text="" runat="server"></asp:Label></td></tr> 
    </table> 
    
</asp:Content>

