﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using VRegistration;
using System.Data;
using System.Collections;
using System.Drawing;

public partial class TestTEstSections : System.Web.UI.Page
{
    protected void Page_Load(object sender, System.EventArgs e)
    {
        lblValText.Text = "";
        if (Session["LoggedIn"] == null)
        {
            Server.Transfer("maintest.aspx");
        }

        if (!Page.IsPostBack)
        {
            LoadYear();
            loadPhase();
            LoadProductGroup();
            LoadDropdown(ddlSetNoTM, 40);
            LoadDropdown(ddlSectionsTM, 10);
            ddlSectionsTM.SelectedValue = "1";
            if ((Session["RoleId"].ToString() == "1") | (Session["RoleId"].ToString() == "2") | (Session["RoleId"].ToString() == "96") | (Session["RoleId"].ToString() == "30"))
            {
                LoadProductGroup();
                string sqltext = "    select distinct P.ProductId, p.Name from  CalSignup cs inner join Product P on(P.productId = cs.productid) where cs.eventyear = " + ddlEventYearTM.SelectedValue + " and cs.Accepted = 'Y'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString().ToString(), CommandType.Text, sqltext);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ddlProductTM.DataSource = ds;
                        ddlProductTM.DataBind();
                        if (ddlProductTM.Items.Count > 1)
                        {
                            ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                            ddlProductTM.Items[0].Selected = true;
                            ddlProductTM.Enabled = true;
                        }
                    }
                }
            }
            else if (Session["RoleId"].ToString() == "88" | Session["RoleId"].ToString() == "89")
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "select count (*) from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + "  and ProductId is not Null")) > 1)
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    int i;
                    string prd = string.Empty;
                    string Prdgrp = string.Empty;
                    for (i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                    {
                        if (prd.Length == 0)
                        {
                            prd = ds.Tables[0].Rows[i][1].ToString();
                        }
                        else
                        {
                            prd = prd + "," + ds.Tables[0].Rows[i][1].ToString();
                        }

                        if (Prdgrp.Length == 0)
                        {
                            Prdgrp = ds.Tables[0].Rows[i][0].ToString();
                        }
                        else
                        {
                            Prdgrp = Prdgrp + "," + ds.Tables[0].Rows[i][0].ToString();
                        }
                    }

                    lblPrd.Text = prd;
                    lblPrdGrp.Text = Prdgrp;
                    LoadProductGroup();
                }
                else
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" + Session["LoginID"] + " and RoleId=" + Session["RoleId"] + " and ProductId is not Null ");
                    string prd = string.Empty;
                    string Prdgrp = string.Empty;
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        prd = ds.Tables[0].Rows[0][1].ToString();
                        Prdgrp = ds.Tables[0].Rows[0][0].ToString();
                        lblPrd.Text = prd;
                        lblPrdGrp.Text = Prdgrp;
                    }

                    LoadProductGroup();
                }
            }
        }
    }
    private void LoadYear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlEventYearTM.Items.Add(new ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)));
        for (int i = 0; i <= 8; i++)
        {
            ddlEventYearTM.Items.Add(new ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))));
        }

        ddlEventYearTM.SelectedValue = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    private void LoadSectionNumber()
    {
        for (int i = 0; i <= 5; i++)
        {
            ddlSectionNo.Items.Insert(0, new ListItem((i + 1).ToString()));
            //ddlSectionNo.Items.Insert(0, i + 1);
        }
    }

    private void LoadProductGroup()
    {
        ddlProductTM.Items.Clear();
        string strSql = "SELECT  Distinct PG.ProductGroupID, PG.Name from ProductGroup PG inner join CalSignup Cs on (PG.ProductGroupID=CS.ProductGroupID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y')  WHERE PG.EventId=" + ddlEvent.SelectedValue + "  order by PG.ProductGroupID";
        SqlDataReader drproductgroup;
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
        ddlProductGroupTM.DataSource = drproductgroup;
        ddlProductGroupTM.DataBind();
        if (ddlProductGroupTM.Items.Count < 1)
        {
            lblErr.Text = "No Product is opened.";
        }
        else if (ddlProductGroupTM.Items.Count > 1)
        {
            ddlProductGroupTM.Items.Insert(0, new ListItem("Select", "0"));
            ddlProductGroupTM.Items[0].Selected = true;
            ddlProductGroupTM.Enabled = true;
        }
        else
        {
            ddlProductGroupTM.Enabled = false;
            LoadProductID();
            LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
        }
    }

    private void LoadProductID()
    {
        SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
        if (ddlProductGroupTM.Items[0].Selected == true & ddlProductGroupTM.SelectedItem.Text == "Select Product Group")
        {
            ddlProductTM.Enabled = false;
        }
        else
        {
            string strSql;
            try
            {
                strSql = "Select distinct P.ProductID, P.Name from Product  P inner join CalSignup Cs on (P.ProductID=CS.ProductID and CS.Semester='" + ddlSemesterTM.SelectedValue + "' and CS.EventYear=" + ddlEventYearTM.SelectedValue + " and CS.Accepted='Y') where P.EventID=" + ddlEvent.SelectedValue + " and P.ProductGroupID =" + ddlProductGroupTM.SelectedValue + " order by P.ProductID";
                SqlDataReader drproductid;
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql);
                ddlProductTM.DataSource = drproductid;
                ddlProductTM.DataBind();
                if (ddlProductTM.Items.Count > 1)
                {
                    ddlProductTM.Items.Insert(0, new ListItem("Select Product"));
                    ddlProductTM.Items[0].Selected = true;
                    ddlProductTM.Enabled = true;
                }
                else if (ddlProductTM.Items.Count < 1)
                {
                    ddlProductTM.Enabled = false;
                }
                else
                {
                    ddlProductTM.Enabled = false;
                }
            }
            catch (Exception ex)
            {
                //lblErr.Text = ex.ToString;
            }
        }
    }


    protected void ddlProductGroupTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        ddlProductTM.Items.Clear();
        LoadProductID();
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
    }

    protected void LoadDropdown(DropDownList ddlObject, int MaxLimit)
    {
        ddlObject.Items.Clear();
        for (int i = 0; i <= MaxLimit - 1; i++)
        {
            ddlObject.Items.Insert(i, Convert.ToString(i + 1));
        }

        if (ddlObject.Items.Count > 1)
        {
            ddlObject.Items.Insert(0, "Select");
        }
    }

    protected void btnAdd_Click(object sender, System.EventArgs e)
    {
        try
        {
            lblError.Text = "";
            lblErr.Text = "";
            if (ddlSectionNo.Items.Count > 1 & ddlSectionNo.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select SectionNo";
            }
            else if (ddlSubject.Enabled == true & ddlSubject.Items.Count > 1 & ddlSubject.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Subject";
            }
            else if (txtTimeLimit.Text == "")
            {
                lblErr.Text = "Please Select Section Time Limit";
            }
            else if (ddlQuestionType.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Question Type";
            }
            else if (ddlQuestionType.SelectedValue == "RadioButton" & ddlNoOfChoices.SelectedIndex == 0)
            {
                lblErr.Text = "Please Select Number Of Choices";
            }
            else if (txtNoOfQues.Text == "")
            {
                lblErr.Text = "Please Select # of Questions";


            }
            else if (txtQuesNoFrom.Text == "")
            {
                lblErr.Text = "Please Select Question Number From";
            }
            else if (txtQuesNoTo.Text == "")
            {
                lblErr.Text = "Please Select Question Number To";
            }
            else if (Convert.ToInt32(txtQuesNoTo.Text) < Convert.ToInt32(txtQuesNoFrom.Text))
            {
                lblErr.Text = " Question Number To must be greater than QuestionNumber From";
            }
            else if (Convert.ToInt32(txtQuesNoTo.Text) > Convert.ToInt32(txtNoOfQues.Text) | Convert.ToInt32(txtQuesNoFrom.Text) > Convert.ToInt32(txtNoOfQues.Text))
            {
                lblErr.Text = " Question Number From /Question Number To cannot be greater than the Number of Questions";
            }
            else
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select NumberOfQuestions,QuestionNumberFrom,QuestionNumberto,QuestionType From TestSetUpSections Where CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " order by NumberOfQuestions desc");
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (btnAdd.Text == "Add")
                    {
                        if (Convert.ToInt32(ds.Tables[0].Rows[0]["NumberOfQuestions"]) > 0 & Convert.ToInt32(txtNoOfQues.Text) != Convert.ToInt32(ds.Tables[0].Rows[0]["NumberOfQuestions"]))
                        {
                            lblErr.Text = " Number of Questions must be the same in all rows of the same section/paper";
                            return;
                        }

                        for (int i = 0; i <= ds.Tables[0].Rows.Count - 1; i++)
                        {
                            if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) <= Convert.ToInt32(txtQuesNoFrom.Text) && Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) >= Convert.ToInt32(txtQuesNoTo.Text))
                            {
                                lblErr.Text = "QuestionNumbers already exists for the same section/paper";
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) == Convert.ToInt32(txtQuesNoFrom.Text) || Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) == Convert.ToInt32(txtQuesNoTo.Text))
                            {
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo already exists for the same section/paper";
                            }
                            else if (Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberFrom"]) == Convert.ToInt32(txtQuesNoTo.Text) || Convert.ToInt32(ds.Tables[0].Rows[i]["QuestionNumberTo"]) == Convert.ToInt32(txtQuesNoFrom.Text))
                            {
                                lblErr.Text = "QuestionNumberFrom/QuestionNumberTo mismatch exists for the same section/paper";
                            }
                        }
                    }
                }
            }

            if (lblErr.Text != "")
            {
                return;
            }

            string StrInsert = "";
            string StrUpdate = "";
            bool AddFlag = false;
            StrInsert = " Insert into TestSetUpSections (CoachPaperID,SectionNumber,Level,Subject,EventYear,SectionTimeLimit,NumberOfQuestions,QuestionType,NumberOfChoices,Penalty,QuestionNumberFrom,QuestionNumberTo,CreatedBy,CreateDate) Values ";
            StrInsert = StrInsert + "(" + txtTestNumber.Text + "," + ddlSectionNo.SelectedValue + "," + "'" + ddlLevel1.SelectedItem.Text + "'" + "," + (ddlSubject.Items.Count <= 0 ? "NULL" : "'" + ddlSubject.SelectedValue + "'") + "," + ddlEventYearTM.SelectedValue + "," + txtTimeLimit.Text + "," + txtNoOfQues.Text + ",'" + ddlQuestionType.SelectedValue + "'," + (ddlNoOfChoices.Enabled == true ? ddlNoOfChoices.SelectedItem.Value : "Null") + ",'" + ddlPenalty.SelectedValue + "'," + txtQuesNoFrom.Text + "," + txtQuesNoTo.Text + "," + Session["LoginID"] + ",GETDATE()" + ")";
            StrUpdate = " Update TestSetUpSections set CoachPaperID=" + txtTestNumber.Text + ",SectionNumber=" + ddlSectionNo.SelectedValue + ",Subject=" + (ddlSubject.Items.Count <= 0 ? "Null" : "'" + ddlSubject.SelectedValue + "'") + ",EventYear=" + ddlEventYearTM.SelectedValue + ",SectionTimeLimit=" + txtTimeLimit.Text + ",NumberOfQuestions=" + txtNoOfQues.Text + ",QuestionType='" + ddlQuestionType.SelectedValue + "',Penalty='" + ddlPenalty.SelectedValue + "',NumberOfChoices=" + (ddlNoOfChoices.Enabled == true ? ddlNoOfChoices.SelectedItem.Value : "Null") + " ,QuestionNumberFrom=" + txtQuesNoFrom.Text + ",QuestionNumberTo=" + txtQuesNoTo.Text + ",ModifiedBy=" + Session["LoginID"] + ",ModifyDate=GETDATE() ";
            StrUpdate = StrUpdate + " Where EventYear=" + ddlEventYearTM.SelectedValue + " and TestSetUpSectionsID=" + hdnTestSetUpSectionsId.Value + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " and Level='" + ddlLevel1.SelectedItem.Text + "'";
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + ddlSectionNo.SelectedValue + " and Level='" + ddlLevel1.SelectedItem.Text + "'" + " and QuestionNumberFrom =" + txtQuesNoFrom.Text + " and QuestionNumberTo=" + txtQuesNoTo.Text)) == 0 & btnAdd.Text == "Add")
            {
                if (ddlSectionNo.Items.Count > 1 & Convert.ToInt32(ddlSectionNo.SelectedValue) > 1)
                {
                    for (int i = 1; i <= Convert.ToInt32(ddlSectionNo.SelectedValue) - 1; i++)
                    {
                        if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + " and CoachPaperID=" + txtTestNumber.Text + " and SectionNumber=" + i + " and Level='" + ddlLevel1.SelectedItem.Text + "'")) == 0)
                        {
                            AddFlag = true;
                        }
                    }

                    if (AddFlag == true)
                    {
                        lblErr.Text = "Please add the Sections in Sequential Order.";
                        return;
                    }
                }

                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, StrInsert) > 0)
                {
                    lblErr.Text = "Inserted Successfully";
                    LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
                }
            }
            else if (btnAdd.Text == "Update")
            {
                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, StrUpdate) > 0)
                {
                    lblErr.Text = "Updated Successfully";
                    LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
                    btnAdd.Text = "Add";
                }
            }
            else
            {
                lblErr.Text = "Data already exists.Please test the Test Sections data.";
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlEventYearTM_SelectedIndexChanged(object sender, System.EventArgs e)
    {
    }

    private void LoadGrid_TestSections(int CoachPaperId)
    {
        lblError.Text = "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select * from TestSetUpSections where CoachPaperID=" + CoachPaperId + " and EventYear=" + ddlEventYearTM.SelectedValue + " order by SectionNumber,QuestionNumberFrom");
        if (ds.Tables[0].Rows.Count > 0)
        {
            tblDGTestSection.Visible = true;
            DGTestSection.Visible = true;
            lblTestSection.Text = "Test SetUp Sections";
            DGTestSection.DataSource = ds;
            DGTestSection.DataBind();
        }
        else
        {
            lblError.Text = "No records exists in the Test Setup Sections table.";
            tblDGTestSection.Visible = false;
            DGTestSection.Visible = false;
        }
    }



    private void FillCopyCoachPapers()
    {
        string StrWhereCndn = "";
        StrWhereCndn = StrWhereCndn + "EventYear=" + ddlEventYearTM.SelectedValue;
        if (ddlPaperTypeTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and PaperType= '" + ddlPaperTypeTM.SelectedValue + "'";
        }

        if (ddlProductGroupTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and ProductGroupID= " + ddlProductGroupTM.SelectedValue + "";
        }

        if (ddlProductTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and ProductID=" + ddlProductTM.SelectedValue + "";
        }

        if (ddlLevelTM.Enabled == true & ddlLevelTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and Level='" + ddlLevelTM.SelectedItem.Text + "'";
        }

        if (ddlSetNoTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " and SetNum=" + ddlSetNoTM.SelectedValue + "";
        }

        if (ddlWeekIDTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + " And WeekId = " + ddlWeekIDTM.SelectedItem.Text + "";
        }

        if (ddlSectionsTM.SelectedIndex > 0)
        {
            StrWhereCndn = StrWhereCndn + "  And Sections = " + ddlSectionsTM.SelectedValue + "";
        }

        if (lblPrdGrp.Text != "")
        {
            StrWhereCndn = StrWhereCndn + " and ProductGroupID in (" + lblPrdGrp.Text + ")";
        }

        if (lblPrd.Text != "")
        {
            StrWhereCndn = StrWhereCndn + " and ProductID in (" + lblPrd.Text + ")";
        }

        StrWhereCndn = StrWhereCndn + " and DocType in ('Q')";
        ddlCopyFrom.Items.Clear();
        ddlCopyFrom.Items.Insert(0, new ListItem("Select PaperID"));
        ddlCopyFrom.Enabled = false;
        ddlCopyTo.Items.Clear();
        ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
        ddlCopyTo.Enabled = false;
        DataSet dsCopyfrom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, "Select Distinct C.CoachPaperID From CoachPapers C Where C.CoachPaperID in (Select Top 50 CoachPaperID from CoachPapers Where " + StrWhereCndn + " order by CreateDate desc)  and C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ") order by C.CoachPaperID asc ");
        if (dsCopyfrom.Tables[0].Rows.Count > 0)
        {
            ddlCopyFrom.Enabled = true;
            ddlCopyFrom.DataSource = dsCopyfrom;
            ddlCopyFrom.DataBind();
            if (ddlCopyFrom.Items.Count > 1)
            {
            }
            else if (ddlCopyFrom.Items.Count == 1)
            {
                LoadCopyToPapers();
            }
        }
    }

    protected void btnCancel_Click(object sender, System.EventArgs e)
    {
        clear();
    }

    private void clear()
    {
        lblErr.Text = "";
        lblError.Text = "";
        if (ddlSectionNo.Items.Count > 0)
        {
            ddlSectionNo.Enabled = true;
            ddlSectionNo.SelectedIndex = 0;
        }

        ddlQuestionType.SelectedIndex = 0;
        ddlNoOfChoices.Enabled = false;
        txtTimeLimit.Text = "";
        txtNoOfQues.Text = "";
        txtQuesNoFrom.Text = "";
        txtQuesNoTo.Text = "";
        btnAdd.Text = "Add";
        for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
        {
            DGTestSection.Items[i].BackColor = Color.White;
        }
    }

    protected void DGTestSection_Itemcommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        lblErr.Text = "";
        int TestSetUpSectionsId = Convert.ToInt32(e.Item.Cells[2].Text);
        int CoachPaperID = Convert.ToInt32(e.Item.Cells[4].Text);
        string Level = (string)e.Item.Cells[5].Text;
        int SectionNumber = Convert.ToInt32(e.Item.Cells[6].Text);
        int QuestionNumberFrom = Convert.ToInt32(e.Item.Cells[13].Text);
        int QuestionNumberTo = Convert.ToInt32(e.Item.Cells[14].Text);
        bool Flag_DelAnsKey = false;
        for (int i = 0; i <= DGTestSection.Items.Count - 1; i++)
        {
            DGTestSection.Items[i].BackColor = Color.White;
        }

        if (e.CommandName == "Delete")
        {
            if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, "Select Count(*) From TestAnswerKey Where CoachPaperID = " + CoachPaperID + " and  Level='" + Level + "' and SectionNumber =" + SectionNumber + " and QuestionNumber  between " + QuestionNumberFrom + " and " + QuestionNumberTo)) > 0)
            {
                if (SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, "Delete From TestAnswerKey Where CoachPaperID = " + CoachPaperID + " and  Level='" + Level + "' and SectionNumber =" + SectionNumber + " and QuestionNumber  between " + QuestionNumberFrom + " and " + QuestionNumberTo) > 0)
                {
                    Flag_DelAnsKey = true;
                }
                else
                {
                    Flag_DelAnsKey = false;
                }
            }
            else
            {
                Flag_DelAnsKey = true;
            }

            if (Flag_DelAnsKey == true & SqlHelper.ExecuteNonQuery(Application["connectionstring"].ToString(), CommandType.Text, "Delete From TestSetUpSections Where TestSetUpSectionsId=" + TestSetUpSectionsId) > 0)
            {
                lblErr.Text = "Deleted Successfully";
                LoadGrid_TestSections(Convert.ToInt32(txtTestNumber.Text));
            }
        }
        else if (e.CommandName == "Edit")
        {
            btnAdd.Text = "Update";
            e.Item.BackColor = Color.Gainsboro;
            hdnTestSetUpSectionsId.Value = Convert.ToString(TestSetUpSectionsId);
            LoadforUpdate(TestSetUpSectionsId);
        }
        else
        {
            e.Item.BackColor = Color.White;
        }
    }

    private void LoadforUpdate(int TestSetUpSectionsId)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ("Select * from TestSetUpSections Where TestSetUpSectionsID =" + TestSetUpSectionsId));
        if ((ds.Tables[0].Rows.Count > 0))
        {
            txtTestNumber.Text = ds.Tables[0].Rows[0]["CoachPaperID"].ToString();
            ddlSectionNo.SelectedIndex = ddlSectionNo.Items.IndexOf(ddlSectionNo.Items.FindByValue(ds.Tables[0].Rows[0]["SectionNumber"].ToString()));
            ddlSectionNo.Enabled = false;
            if (!(ds.Tables[0].Rows[0]["Level"] == DBNull.Value))
            {
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(ds.Tables[0].Rows[0]["Level"].ToString()));
                ddlLevelTM.Enabled = false;
            }

            if ((ddlSubject.Enabled == true))
            {
                ddlSubject.SelectedIndex = ddlSubject.Items.IndexOf(ddlSubject.Items.FindByValue(ds.Tables[0].Rows[0]["Subject"].ToString()));
            }

            txtTimeLimit.Text = ds.Tables[0].Rows[0]["SectionTimeLimit"].ToString();
            txtNoOfQues.Text = ds.Tables[0].Rows[0]["NumberOfQuestions"].ToString();
            ddlQuestionType.SelectedIndex = ddlQuestionType.Items.IndexOf(ddlQuestionType.Items.FindByValue(ds.Tables[0].Rows[0]["Questiontype"].ToString()));
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByValue(((ds.Tables[0].Rows[0]["Penalty"] == DBNull.Value) ? "N" : ds.Tables[0].Rows[0]["Penalty"].ToString())));
            if ((ds.Tables[0].Rows[0]["QuestionType"] == "RadioButton"))
            {
                ddlPenalty.Enabled = true;
            }
            else
            {
                ddlPenalty.Enabled = false;
            }

            if ((ds.Tables[0].Rows[0]["NumberOfChoices"] == DBNull.Value))
            {
                ddlNoOfChoices.SelectedIndex = -1;
                ddlNoOfChoices.Enabled = false;
            }
            else
            {
                this.LoadDropdown(ddlNoOfChoices, 10);
                ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.IndexOf(ddlNoOfChoices.Items.FindByValue(ds.Tables[0].Rows[0]["NumberOfChoices"].ToString()));
                ddlNoOfChoices.Enabled = true;
            }

            txtQuesNoFrom.Text = ds.Tables[0].Rows[0]["QuestionNumberFrom"].ToString();
            txtQuesNoTo.Text = ds.Tables[0].Rows[0]["QuestionNumberTo"].ToString();
        }

    }

    protected void DGCoachPapers_ItemCommand(object source, System.Web.UI.WebControls.DataGridCommandEventArgs e)
    {
        int CoachPaperID = Convert.ToInt32(e.Item.Cells[3].Text);
        int sections = Convert.ToInt32(e.Item.Cells[10].Text);
        string ProductCode = (string)e.Item.Cells[5].Text;
        string ProductGroupCode = (string)e.Item.Cells[4].Text;
        if (e.CommandName == "Select")
        {
            DGTestSection.Visible = true;
            clear();
            LoadTestSectionSetUp(CoachPaperID, sections, ProductCode);
            LoadLevel(ddlLevel1, ProductGroupCode);
            if (ddlLevel1.Items.Count > 0)
            {
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText((string)e.Item.Cells[6].Text));
                ddlLevel1.Enabled = false;
            }

            LoadGrid_TestSections(CoachPaperID);
            LoadProductGroup();
            //string PGId = (Label)e.Item.FindControl("lblProductGroupID").Text;
            //ddlProductGroupTM.SelectedValue = PGId;
            //LoadProductID();
            //string PId = (Label)e.Item.FindControl("lblProductID").Text;
            //ddlProductTM.SelectedValue = PId;
            //LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
            //string Level = (string)e.Item.Cells[6].Text;
            //ddlLevelTM.SelectedValue = Level;
            //string PaperType = (string)e.Item.Cells[11].Text;
            //ddlPaperTypeTM.SelectedValue = PaperType;
            //string WeekNo = (string)e.Item.Cells[8].Text;
            //ddlWeekIDTM.SelectedValue = WeekNo;
            //string Setnumber = (string)e.Item.Cells[9].Text;
            //ddlSetNoTM.SelectedValue = Setnumber;
            //LoadLevel(ddlLevel1, ProductGroupCode);
            //ddlLevel1.SelectedValue = Level;
        }
    }

    private void LoadTestSectionSetUp(int CoachPaperID, int sections, string ProductCode)
    {
        tblTestSection.Visible = true;
        txtTestNumber.Text = Convert.ToString(CoachPaperID);
        LoadDropdown(ddlSectionNo, sections);
        LoadSubject(ProductCode);
    }

    private void LoadSubject(string ProductCode)
    {
        ddlSubject.Enabled = true;
        ddlSubject.Items.Clear();
        if (ProductCode.Contains("SATM") | ProductCode.Contains("MB2") | ProductCode.Contains("MB3"))
        {
            ddlSubject.Items.Insert(0, new ListItem("Math", "Math"));
            ddlSubject.Enabled = false;
        }
        else if (ProductCode.Contains("SATE"))
        {
            ddlSubject.Items.Insert(0, new ListItem("Select", ""));
            ddlSubject.Items.Insert(1, new ListItem("Critical Reading", "Critical Reading"));
            ddlSubject.Items.Insert(2, new ListItem("Writing", "Writing"));
        }
        else
        {
            ddlSubject.Enabled = false;
        }
    }

    private void LoadLevel(DropDownList ddlObject, string ProductGroup)
    {
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        try
        {
            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" + ddlEventYearTM.SelectedValue + " and ProductGroupID=" + ddlProductGroupTM.SelectedValue + " and ProductID=" + ddlProductTM.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + "";
            SqlConnection conn = new SqlConnection(Application["connectionstring"].ToString().ToString());
            SqlDataReader level;
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText);
            ddlObject.DataSource = level;
            ddlObject.DataValueField = "LevelCode";
            ddlObject.DataTextField = "LevelCode";
            ddlObject.DataBind();
            DataTable dt = new DataTable();
            dt.Load(level);
            if ((ddlObject.Items.Count > 1))
            {
                ddlObject.Items.Insert(0, "Select Level");
            }
            else
            {
                PopulateWeekNo();
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void ddlQuestionType_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        if (ddlQuestionType.SelectedValue == "RadioButton")
        {
            ddlNoOfChoices.Enabled = true;
            LoadDropdown(ddlNoOfChoices, 10);
            ddlNoOfChoices.SelectedIndex = ddlNoOfChoices.Items.Count - 1;
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("Yes"));
            ddlPenalty.Enabled = true;
        }
        else
        {
            ddlNoOfChoices.Enabled = false;
            ddlPenalty.SelectedIndex = ddlPenalty.Items.IndexOf(ddlPenalty.Items.FindByText("No"));
            ddlPenalty.Enabled = false;
        }
    }

    protected void ddlCopyFrom_SelectedIndexChanged(object sender, System.EventArgs e)
    {
        try
        {
            ddlCopyTo.Items.Clear();
            ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
            ddlCopyTo.Enabled = false;
            if (Convert.ToInt32(ddlCopyFrom.SelectedValue) > 0)
            {
                LoadCopyToPapers();
            }
        }
        catch (Exception ex)
        {
        }
    }

    private void LoadCopyToPapers()
    {
        try
        {
            if (Convert.ToInt32(ddlCopyFrom.SelectedValue) > -1)
            {
                string StrCopyTo = "";
                lblCopyErr.Text = "";
                StrCopyTo = StrCopyTo + " Select Distinct C1.CoachPaperID From CoachPapers C Inner Join CoachPapers C1 on C.EventYear =C1.EventYear and C.ProductGroupId =C1.ProductGroupId and C.ProductId=C1.ProductId and C.WeekId =C1.WeekId and C.SetNum =C1.SetNum and C.Sections =C1.Sections and C.PaperType =C1.PaperType and C.DocType =C1.DocType ";
                StrCopyTo = StrCopyTo + " and C.CoachPaperID <> C1.CoachPaperID and C.Level <> C1.Level Where C.CoachPaperID in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ") and C1.CoachPaperId not in (Select Distinct CoachPaperId from TestSetUpSections where EventYear=" + ddlEventYearTM.SelectedValue + ")";
                StrCopyTo = StrCopyTo + " and C.CoachPaperId = " + ddlCopyFrom.SelectedValue;
                DataSet dsCopyTo = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, StrCopyTo);
                if (dsCopyTo.Tables[0].Rows.Count > 0)
                {
                    ddlCopyTo.DataSource = dsCopyTo;
                    ddlCopyTo.DataBind();
                    ddlCopyTo.Enabled = true;
                    if (ddlCopyTo.Items.Count > 1)
                    {
                        ddlCopyTo.Items.Insert(0, new ListItem("Select PaperID"));
                    }
                }
                else
                {
                    ddlCopyTo.Enabled = false;
                    lblCopyErr.Text = "No eligible coach paper available to copy into.";
                }
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void btnContinue_Click(object sender, System.EventArgs e)
    {
        Response.Write("<script language='javascript'>window.open('TestSections_AnsKeyCopy.aspx?ID=AnsKey&From=" + ddlCopyFrom.SelectedValue + "&To=" + ddlCopyTo.SelectedValue + "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ");
    }

    protected void ddlProductTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        string PgId = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString().ToString(), CommandType.Text, "select ProductGroupId from Product where ProductId=" + ddlProductTM.SelectedValue + "").ToString();
        ddlProductGroupTM.SelectedValue = PgId;
        LoadLevel(ddlLevelTM, ddlProductGroupTM.SelectedItem.Text);
    }

    protected void btnRepTM_Click(object sender, EventArgs e)
    {
        if ((ValdiateTestSections() == "1"))
        {
            tblCopy.Visible = true;
            btnRepTM.Visible = false;
            FillCopyCoachPapers();
        }
    }

    protected void btnCancelCopy_Click(object sender, EventArgs e)
    {
        btnRepTM.Visible = true;
        tblCopy.Visible = false;
    }

    protected void ddlWeekIDTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlSetNoTM.SelectedIndex = ddlWeekIDTM.SelectedIndex;
    }

    protected void ddlLevelTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        PopulateWeekNo();
    }

    protected void ddlSemesterTM_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadProductGroup();
    }

    private void loadPhase()
    {
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {
            ddlSemesterTM.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlSemesterTM.SelectedValue = objCommon.GetDefaultSemester(ddlEventYearTM.SelectedValue);
    }

    protected void PopulateWeekNo()
    {
        string CmdText = "";
        CmdText = " select distinct CP.WeekId, cp.Coachpaperid from  CoachPapers CP where CP.eventyear=" + ddlEventYearTM.SelectedValue + " and CP.ProductGroupId=" + ddlProductGroupTM.SelectedValue + " and CP.ProductId=" + ddlProductTM.SelectedValue + " and CP.level='" + ddlLevelTM.SelectedValue + "' and CP.Semester='" + ddlSemesterTM.SelectedValue + "' and DOCType='Q' order by WeekId ASC";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlWeekIDTM.DataValueField = "CoachPaperId";
            ddlWeekIDTM.DataTextField = "WeekId";
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
            ddlWeekIDTM.Items.Insert(0, new ListItem("Select", "0"));
        }
        else
        {
            ddlWeekIDTM.DataSource = ds;
            ddlWeekIDTM.DataBind();
        }
    }

    protected void btnClearFilterTM_Click(object sender, EventArgs e)
    {
        LoadTestSections();
    }

    protected void LoadTestSections()
    {
        if ((ValdiateTestSections() == "1"))
        {
            DGTestSection.Visible = true;
            LoadTestSectionSetUp(Convert.ToInt32(ddlWeekIDTM.SelectedValue), Convert.ToInt32(ddlSectionsTM.SelectedValue), ddlProductTM.SelectedValue);
            LoadLevel(ddlLevel1, ddlProductGroupTM.SelectedValue);
            if (ddlLevel1.Items.Count > 0)
            {
                ddlLevel1.Enabled = false;
            }

            LoadGrid_TestSections(Convert.ToInt32(ddlWeekIDTM.SelectedValue));
            LoadLevel(ddlLevel1, ddlProductGroupTM.SelectedValue);
            ddlLevel1.SelectedValue = ddlLevelTM.SelectedValue;
        }
    }

    public string ValdiateTestSections()
    {
        string retVal = "1";
        if (ddlEventYearTM.SelectedValue == "-1")
        {
            lblValText.Text = "Please select Event Year";
            retVal = "-1";
        }
        else if (ddlSemesterTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlProductGroupTM.SelectedValue == "0" || ddlProductGroupTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProductTM.SelectedValue == "Select Product" || ddlProductTM.SelectedValue == "")
        {
            lblValText.Text = "Please select Product";
            retVal = "-1";
        }
        else if (ddlLevelTM.SelectedValue == "Select Level" || ddlLevelTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Level";
            retVal = "-1";
        }
        else if (ddlPaperTypeTM.SelectedValue == "" || ddlPaperTypeTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Papertype";
            retVal = "-1";
        }
        else if (ddlWeekIDTM.SelectedValue == "-1" || ddlWeekIDTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select WeekId";
            retVal = "-1";
        }
        else if (ddlSectionsTM.SelectedValue == "Select" || ddlSectionsTM.SelectedValue == "0")
        {
            lblValText.Text = "Please select Sections";
            retVal = "-1";
        }

        return retVal;
    }
}