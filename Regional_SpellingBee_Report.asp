<%@ Language=VBScript optionexplictit%>
<%
session.CodePage = 65001
response.CharSet = "utf-8"
%>
<HTML>
<HEAD>
</HEAD>
<TITLE>
</TITLE>
<BODY>
<!--#include file = "random.inc"-->
<div class=Section1>
<form name="Report" method="get">
<%
	dim selection
    dim r
	selection = request.querystring("contest")
	dim selectedJRSB,selectedSRSB,selectedJRVO,selectedSRVO
    if selection = "JRSB" then
    	selectedJRSB = "SELECTED"
	elseif selection = "SRSB" then
		selectedSRSB = "SELECTED"
	elseif selection = "JRVO" then
		selectedJRVO = "SELECTED"
	elseif selection = "SRVO" then
		selectedSRVO = "SELECTED"
	end if
%>
<table width=60% CELLSPACING="0" CELLPADDING="5" BORDER="0" >
<tr>
<td width=20%> <FONT FACE="Verdana" SIZE="1.5">
Choose the Content Type: </td>
<td width=20%> <FONT FACE="Verdana" SIZE="1.5">
Choose the Contest Center: </td>
<td width=20%> <FONT FACE="Verdana" SIZE="1.5">
Number of contestants: </td>
</tr>
<tr>
<td width=20%> <FONT FACE="Verdana" SIZE="2">
<select name="contest">
<option value="">-- Select One --
<option <%= selectedJRSB %> value="JRSB">Junior Spelling Bee
<option <%= selectedSRSB %> value="SRSB">Senior Spelling Bee
</select>
</td>
<%
dim Sconnection
dim strQuery,countQueryP,countQueryUP,locQuery
dim cn,rs,countrs
state = 0
set cn=server.CreateObject("ADODB.Connection")
set locrs=server.CreateObject("ADODB.Recordset")
'Sconnection=Session("Sconnection")
    Sconnection = "Provider=SQLOLEDB.1;Persist Security Info=False;Data Source=sql.northsouth.org,8778;Initial Catalog=nsfprod_copy;Trusted_Connection=No;UID=northsouthdev;Password=Devi$Coi09"

cn.Open Sconnection
%>
<td width=20%> <FONT FACE="Verdana" SIZE="2">
<select name="Center">
<option>Group - I
<option>Group - II
<option>Group - III
<option>Group - IV
<option>Group - V
</select>
</td>
<td width=20%> <FONT FACE="Verdana" SIZE="2">
<input type="text" name="nos" value="<%=request.querystring("nos")%>" size="5">
<input type="submit" value="Go">
</td>
</tr>
</table>
<%On Error Resume Next
    if selection <> "" then
    Dim WordRecordArrayPE(),WordRecordArrayPD(),WordRecordArrayUPE(),WordRecordArrayUPD()
		redim WordRecordArrayPE(10000)
		redim WordRecordArrayPD(10000)
		redim WordRecordArrayUPE(10000)
		redim WordRecordArrayUPD(10000)
    dim ctrPE  ' P = Published, E = easy
	dim	ctrPD   ' P = Published, D = Difficult
	dim	ctrUPE ' UP = UnPublished, E = English
	dim	ctrUPD 
     dim arraySlitArray
        redim arraySlitArray(10000)
    dim randomArrayPE,randomArrayPD,randomArrayUPD,randomArrayUPE
if (state = 0) then
	if (selection = "JRSB" or selection = "SRSB") then
		set rs=server.CreateObject("ADODB.Recordset")
		
		ctrPE = 0  ' P = Published, E = easy
		ctrPD = 0  ' P = Published, D = Difficult
		ctrUPE = 0 ' UP = UnPublished, E = English
		ctrUPD = 0 ' UP = UnPublished, D = Difficult

		'Get the Publised words from word_master_new
		strQuery = "select *, cast(Definitions as TEXT), cast(Sentence as TEXT) from  Word_Master_New wmn, PronMaster pm where wmn.word=pm.word"
		if selection = "JRSB" then
			strQuery = strQuery	 + " and Level_NSF = 1"
		elseif selection = "SRSB" then
			strQuery = strQuery + " and Level_NSF = 2"
		end if

		set rs=cn.Execute(strQuery)

		while not rs.eof
		 Regional_Flag =  rs.fields(5)
		 sublevel = rs.fields(3)
		 if (sublevel = 1) then
		 	sublevelStr = "E"
		 else
		 	sublevelStr = "D"
		 end if
		 
		 pronunciation = rs.fields(25)
		 StrFinalString = rs.fields(2)&"$#"&sublevelStr&"$#"&rs.fields(23)&"$#"&rs.fields(24)&"$#"&pronunciation&"$#"&rs.fields(26)&"$#"&rs.fields(27)&"$#"&rs.fields(28)
		 if (Regional_Flag = "Y" and sublevel = 1) then
			WordRecordArrayPE(ctrPE) = StrFinalString
			ctrPE = ctrPE + 1
		 elseif (Regional_Flag = "Y" and sublevel = 2) then
			WordRecordArrayPD(ctrPD) = StrFinalString
			ctrPD = ctrPD + 1
		 end if
		rs.movenext
		wend
        'Get the Unpublised words from Word_Master. Pick any word and make sure it is not in the Word_Master_New
		strQuery = "select *, cast(Definitions as TEXT), cast(Sentence as TEXT) from Word_Master wm, PronMaster pm where wm.word=pm.word and wm.word not in (select word from word_master_new where Regional_Flag = 'Y')"
		if selection = "JRSB" then
			strQuery = strQuery	 + " and Level = 1"
		elseif selection = "SRSB" then
			strQuery = strQuery + " and Level = 2"
		end if
    	strQuery = strQuery	 + " and regional_flag is null"
    	set rs=cn.Execute(strQuery)
    	while not rs.eof
			word1 = rs.fields(11)
			pos1 = rs.fields(12)
			sublevel = rs.fields(3)
			if (sublevel = 1) then
				sublevelStr = "E"
			else
				sublevelStr = "D"
			end if
			pronunciation = rs.fields(22)			
			StrFinalString = rs.fields(2)&"$#"&sublevelStr&"$#"&rs.fields(11)&"$#"&rs.fields(12)&"$#"&pronunciation&"$#"&rs.fields(14)&"$#"&rs.fields(23)&"$#"&rs.fields(24)
			if (sublevel = 2) then
				WordRecordArrayUPD(ctrUPD) = StrFinalString
				ctrUPD = ctrUPD + 1
			elseif (sublevel = 1) then
				WordRecordArrayUPE(ctrUPE) = StrFinalString
				ctrUPE = ctrUPE + 1
			end if
		rs.movenext
		wend
		cn.close
	end if
	state = 1
end if
 
	dim title
    if selection = "JRSB" then
    	title = "Junior Spelling Bee Words"
	elseif selection = "SRSB" then
		title = "Senior Spelling Bee Words"
	end if
 
if (selection = "JRSB" or selection = "SRSB") then

randomArrayPE = RangedRandomArray(1,ctrPE,ctrPE-1,false)
randomArrayPD = RangedRandomArray(1,ctrPD,ctrPD-1,false)
randomArrayUPE = RangedRandomArray(1,ctrUPE,ctrUPE-1,false)
randomArrayUPD = RangedRandomArray(1,ctrUPD,ctrUPD-1,false)
nos = cint(request.querystring("nos"))
	wCtr = (nos)*(10) '(Number of students) times (6 contests + 4)
	if (selection = "JRSB") then
		if wCtr > (ctrPE + ctrPD) then
			wCtr = (ctrPE + ctrPD)
		end if
	else
		if wCtr > ctrPE then
			wCtr = ctrPE
		end if
	end if
	numOfPagesP = Int(wCtr/15)
	if (wCtr Mod 15 <> 0) then
		numOfPagesP = numOfPagesP + 1
	end if
	nctrPE = 0
	nctrPD = 0
	nctrUPD = 0
	nctrUPE = 0
	pageCounter = 0
	wordNumber = 0
	printpages = numOfPagesP + 4
%>
<!--#include file = "print_instructions_sb.inc"-->
<%
Dim wordArray(25)

for nctr=0 to 9
		if (nctr < 5) then
            r = randomArrayPE(nctrPE)-1
			arraySlitArray = Split(WordRecordArrayPE(r), "$#")
			nctrPE = nctrPE + 1
		else
            r=randomArrayPD(nctrPD)-1
			arraySlitArray = Split(WordRecordArrayPD(r), "$#")
			nctrPD = nctrPD + 1
		end if
		if (nctr = 0) then
			response.write("<P style='page-break-before: always'> &nbsp;")
			 response.write("<h2>Phase I: " & title & " - Published &nbsp; &nbsp; &nbsp;</h2>") %>
			
			<table width=100% border=1 cellspacing=0>
			<tr>
			<td width=1%> <FONT FACE="Arial" SIZE="2">
			<b>SNo</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>Level</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Word</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>POS</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Pronunciation</b> </td>
			<td width=5%> <FONT FACE="Arial" SIZE="2">
			<b>Root</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Definitions</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Sentence</b> </td>
			</tr>
		<% end if %>
		<tr>
		<td width=1%> <FONT FACE="Arial" SIZE="2">
		<%=wordNumber+1%> </td>
		<td width=2%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(0) %> / <%=arraySlitArray(1)%></td>
		<td width=10%> <FONT FACE="Arial" SIZE="2">
		<% wordArray(wordNumber) = arraySlitArray(2)%>
		<%=arraySlitArray(2)%> </td>
		<td width=2% align="center"> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(3)%> </td>
		<td width=10% align="center"> <FONT FACE="Doulos SIL" SIZE="2">
		<%=arraySlitArray(4)%> </td>		<td width=5%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(5)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(6)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(7)%> </td>
		</tr>
<%
	wordNumber = wordNumber + 1
	Next
%>
</table>
<%
for nctr=0 to 14
		if (nctr < 5) then
            r=randomArrayUPE(nctrUPE)-1
			arraySlitArray = Split(WordRecordArrayUPE(r), "$#")
			nctrUPE = nctrUPE + 1
		else
            r=randomArrayUPD(nctrUPD)-1
			arraySlitArray = Split(WordRecordArrayUPD(r), "$#")
			nctrUPD = nctrUPD + 1
		end if
		if (nctr = 0 or nctr = 10) then
			response.write("<P style='page-break-before: always'> &nbsp;")
			%>
			<h2>
			<% response.write("Phase I: " & title & " - Unpublished &nbsp; &nbsp; &nbsp;") %>
			</h2>
			<table width=100% border=1 cellspacing=0>
			<tr>
			<td width=1%> <FONT FACE="Arial" SIZE="2">
			<b>SNo</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>Level</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Word</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>POS</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Pronunciation</b> </td>
			<td width=5%> <FONT FACE="Arial" SIZE="2">
			<b>Root</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Definitions</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Sentence</b> </td>
			</tr>
		<% end if %>
		<tr>
		<td width=1%> <FONT FACE="Arial" SIZE="2">
		<%=wordNumber+1%> </td>
		<td width=2%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(0) %> / <%=arraySlitArray(1)%></td>
		<td width=10%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(2)%> </td>
		<% wordArray(wordNumber) = arraySlitArray(2)%>		
		<td width=2% align="center"> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(3)%> </td>
		<td width=10% align="center"> <FONT FACE="Doulos SIL" SIZE="2">
		<%=arraySlitArray(4)%> </td>
		<td width=5%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(5)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(6)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(7)%> </td>
		</tr>
		<%	if (((nctr+1) mod 10 = 0) or ((nctr+1) mod 15 = 0)) then %>
		</table>
		<% end if %>		
<%
	wordNumber = wordNumber + 1
	Next
%>
<%
	dim category
    if selection = "JRSB" then
    	category = "Junior Spelling Bee"
	elseif selection = "SRSB" then
		category = "Senior Spelling Bee"
	end if
%>	
<%
	for nctr=0 to wCtr-1
		if (nctr < wCtr/2) then
            r=randomArrayPE(nctrPE)-1
			arraySlitArray = Split(WordRecordArrayPE(r), "$#")
			nctrPE = nctrPE + 1
		else
            r=randomArrayPD(nctrPD)-1
			arraySlitArray = Split(WordRecordArrayPD(r), "$#")
			nctrPD = nctrPD + 1
		end if
		if ((nctr) mod 15 = 0) then
			pageCounter = pageCounter + 1
				response.write("<P style='page-break-before: always'> &nbsp;")
			%>
			<h3>
			<%
			if selection = "JRSB" then
				easy = "1/E"
				dift = "1/D"
			else
				easy = "2/E"
				dift = "2/D"			
			end if			
			if pageCounter = 1 then
				response.write("For the first 3 rounds, use (" & easy & ") & for the next 3 rounds, use (" & dift & ") which start from SNo: " & 25 + (wCtr/2) + 1 & "<br>")
			end if
			%>		
			</h3>	
			<h2>
			<%
			response.write("Phase II: " & title & " - Published &nbsp; &nbsp; &nbsp;")
			response.write(" Pg " & cint(pageCounter) & " of " & cint(numOfPagesP))
			%>
			<h2>
			<table width=100% border=1 cellspacing=0>
			<tr>
			<td width=1%> <FONT FACE="Arail" SIZE="2">
			<b>SNo</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>Level</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Word</b> </td>
			<td width=2%> <FONT FACE="Arial" SIZE="2">
			<b>POS</b> </td>
			<td width=10%> <FONT FACE="Arial" SIZE="2">
			<b>Pronunciation</b> </td>
			<td width=5%> <FONT FACE="Arial" SIZE="2">
			<b>Root</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Definitions</b> </td>
			<td width=35%> <FONT FACE="Arial" SIZE="2">
			<b>Sentence</b> </td>
			</tr>
		<% end if %>
		<tr>
		<td width=1%> <FONT FACE="Arial" SIZE="2">
		<%=wordNumber+1%> </td>
		<td width=2%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(0) %> / <%=arraySlitArray(1)%></td>
		<td width=10%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(2)%> </td>
		<td width=2%  align="center"> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(3)%> </td>
		<td width=10%  align="center"> <FONT FACE="Doulos SIL" SIZE="2">
		<%=arraySlitArray(4)%> </td>
		<td width=5%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(5)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(6)%> </td>
		<td width=35%> <FONT FACE="Arial" SIZE="2">
		<%=arraySlitArray(7)%> </td>
		</tr>
		<%	if ((nctr+1) mod 15 = 0) then %>
		</table>
		<% end if  
	wordNumber = wordNumber + 1
	Next
if (nctr mod 15 <> 0) then
 response.write("</table>")
end if
end if
end if
             If Err.number<>0 then
         Response.Write "The Error Number is: " & Err.number & "<BR>"
      Response.Write "The Description given is: " & Err.Description & "<BR>"
        end if
%>
<!--#include file = 'Spelling_AnswerSheet.inc'-->
<!--#include file = 'Spelling_AnswerKey.inc'-->
</form>
</BODY>
</HTML>

 