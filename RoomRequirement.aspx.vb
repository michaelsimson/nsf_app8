﻿
Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class RoomRequirement
    Inherits System.Web.UI.Page
    Public dt As DataTable
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        'If (Session("RoleID") = 1 Or Session("RoleID") = 2) Then
        ''To Add Values to DropDownList

        If Not IsPostBack() Then
            LoadYear()
            getEvent()
            getPurpose()
            getProductGroup()
            LoadlblDropdown()
            LoadDDLValues()
            Loaddata()

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from RoomRequirement where Eventyear=" & Now.Year() & " and EventID=" & ddlEvent.SelectedValue) = 0 Then
                BtnCopyPrevYear.Visible = True
            Else
                BtnCopyPrevYear.Visible = False
            End If

        End If
        ' End If
    End Sub
    Private Sub LoadYear()
        ddlEventYear.Items.Clear()
        Dim year As Integer = Now.Year

        For i As Integer = 0 To 4
            ddlEventYear.Items.Insert(i, New ListItem(year - i, year - i))
        Next
        Dim year_index As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(EventYear) from RoomRequirement ")
        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByValue(year_index))

    End Sub
    Private Sub LoadYear1()

        ddlEventYear1.Items.Clear()
        Dim year As Integer = Now.Year

        For i As Integer = 0 To 4
            ddlEventYear1.Items.Insert(i, New ListItem(year - i, year - i))
        Next
        Dim year_index As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(EventYear) from RoomRequirement ")
        ddlEventYear1.SelectedIndex = ddlEventYear1.Items.IndexOf(ddlEventYear1.Items.FindByValue(year_index + 1))

    End Sub
    Public Sub LoadDDLValues()
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartCount, EndCount As Integer

        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.Int32"))

        StartCount = 0
        If ddlEvent.SelectedValue = "1" Then
            EndCount = 30
        ElseIf ddlEvent.SelectedValue = "2" Then
            EndCount = 10
        End If
        For i As Integer = StartCount To EndCount
            dr = dt.NewRow()
            dr("ddlText") = i
            dr("ddlValue") = i.ToString
            If i = 0 Then
                dr("ddlText") = "Select"
                dr("ddlValue") = i
            End If
            dt.Rows.Add(dr)
        Next
        LoadDDLTblValues()
    End Sub
    Private Sub getEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
    End Sub
    Private Sub getEvent1()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent1.DataSource = ds
        ddlEvent1.DataBind()
    End Sub
    Private Sub getPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        ddlPurpose.DataBind()
        ddlPurpose.SelectedIndex = ddlPurpose.Items.IndexOf(ddlPurpose.Items.FindByText("Contests"))
    End Sub
    Private Sub getPurpose1()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose1.DataSource = ds
        ddlPurpose1.DataBind()
        ddlPurpose1.SelectedIndex = ddlPurpose1.Items.IndexOf(ddlPurpose1.Items.FindByText("Contests"))
    End Sub

    Private Sub getProductGroup()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Name,ProductGroupId from ProductGroup Where EventId =" & ddlEvent.SelectedValue)
        ddlProductGroup.DataSource = ds
        ddlProductGroup.DataBind()
    End Sub

    Private Sub getProductGroup1()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Name,ProductGroupId from ProductGroup Where EventId =" & ddlEvent.SelectedValue)
        ddlProductGroup1.DataSource = ds
        ddlProductGroup1.DataBind()
    End Sub
    Private Sub LoadlblDropdown()
        setDDLVisibility()
        TableAddUpdtCncl.Visible = True

        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Then '3
            lblPurpose.Text = "Contests"
            TrRoomGuide.Visible = True
            TrRoomMC.Visible = True
            TrPronouncer.Visible = True
            TrChiefJudge.Visible = True
            TrAssociateJudge.Visible = True
            TrLaptopJudge.Visible = True
            TrDictHandler.Visible = True
            TrTimer.Visible = True
            TrGrader.Visible = True
            TrProctor.Visible = True
            TrPodium.Visible = True
            TrPodiumMic.Visible = True
            TrPodiumLaptop.Visible = True
            TrProjector.Visible = True
            TrJudgesTable.Visible = True
            TrJudgeChairs.Visible = True
            TrCJMic.Visible = True
            TrChildMic.Visible = True
            TrAudioMixer.Visible = True
            TrChildMicStand.Visible = True
            TrDesktopMicStand.Visible = True
            TrAudioWire.Visible = True
            TrExtraMikes.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Awards" Then '1
            lblPurpose.Text = "Awards"
            TrRoomGuide.Visible = True
            TrMC.Visible = True
            TrTeamLead.Visible = True
            TrProjector.Visible = True
            TrLaptop.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrMic.Visible = True
            TrMicStand.Visible = True
            TrAudioMixer.Visible = True
            TrAudioWire.Visible = True
            TrInternetAccess.Visible = True
            TrExtraMikes.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ParentAct" Then '9
            lblPurpose.Text = "Parent Activity"
            TrRoomGuide.Visible = True
            TrMC.Visible = True
            TrTeamLead.Visible = True
            TrProjector.Visible = True
            TrLaptop.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrMic.Visible = True
            TrMicStand.Visible = True
            TrAudioMixer.Visible = True
            TrAudioWire.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "ChildAct" Then '2
            lblPurpose.Text = "Children Activity"
            TrTeamLead.Visible = True
            TrRoomGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Judges" Then '7
            lblPurpose.Text = "Judges Room"
            TrTeamLead.Visible = True
            TrRoomGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then '5
            lblPurpose.Text = "Grading Room"
            TrTeamLead.Visible = True
            TrRoomGuide.Visible = True
            TrGrader.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Registration" Then '11
            lblPurpose.Text = "Registration"
            TrTeamLead.Visible = True
            TrRegGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
            TrLaptop.Visible = True
            TrInternetAccess.Visible = True
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10
            lblPurpose.Text = "InfoDesk/NSFDesk/ReceptionDesk"
            TrTeamLead.Visible = True
            TrDeskGuide.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Sponsors" Then
            lblPurpose.Text = "Sponsors"
            TrTeamLead.Visible = True
            TrTable.Visible = True
            TrChair.Visible = True
        ElseIf ddlPurpose.SelectedItem.Text.Trim() = "Food" Then '4
            lblPurpose.Text = "Food"
            TrTeamLead.Visible = True
            TrFoodServe.Visible = True
        End If
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        TrCopyFrom.Visible = False
        clearDDL()
        lblAddUpdate.Text = ""
        Loaddata()
        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
            TdProductGroup.Visible = True
            ddlProductGroup.Visible = True
            TdPhase.Visible = True
            ddlPhase.Visible = True
            getProductGroup()
            TdDay.Visible = False
            ddlDay.Visible = False
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 Then
            Loaddata()
            TdDay.Visible = False
            ddlDay.Visible = False
            ddlDay.Enabled = False
            TdProductGroup.Visible = False
            ddlProductGroup.Visible = False
            TdPhase.Visible = False
            ddlPhase.Visible = False
        Else
            TdDay.Visible = True
            ddlDay.Visible = True
            ddlDay.Enabled = True
            TdProductGroup.Visible = False
            ddlProductGroup.Visible = False
            TdPhase.Visible = False
            ddlPhase.Visible = False
            ddlProductGroup.Items.Clear()
        End If
        LoadlblDropdown()
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        TrCopyFrom.Visible = False
        clearDDL()
        getProductGroup()
        Loaddata()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        TrCopyFrom.Visible = False
        clearDDL()
        lblAddUpdate.Text = ""
        LoadlblDropdown()

        If ddlProductGroup.SelectedItem.Text = "Spelling" Then
            TrProctor.Visible = False
        ElseIf ddlProductGroup.SelectedItem.Text = "Vocabulary" Or ddlProductGroup.SelectedItem.Text.Trim() = "Geography" Or ddlProductGroup.SelectedItem.Text.Trim() = "Brain Bee" Then
            TrDictHandler.Visible = False
            TrProctor.Visible = False
        ElseIf ddlProductGroup.SelectedItem.Text = "Math" Or ddlProductGroup.SelectedItem.Text = "Science" Then
            TrChiefJudge.Visible = False
            TrAssociateJudge.Visible = False
            TrDictHandler.Visible = False
            TrTimer.Visible = False
        ElseIf ddlProductGroup.SelectedItem.Text = "Essay Writing" Then
            TrPronouncer.Visible = False
            'TrChiefJudge.Visible = False
            'TrAssociateJudge.Visible = False
            TrDictHandler.Visible = False
            TrTimer.Visible = False
            TrPodium.Visible = False
            TrPodiumMic.Visible = False
            TrPodiumLaptop.Visible = False
            TrChildMic.Visible = False
            TrAudioMixer.Visible = False
            TrChildMicStand.Visible = False
            TrDesktopMicStand.Visible = False
            TrAudioWire.Visible = False
            TrExtraMikes.Visible = False
        ElseIf ddlProductGroup.SelectedItem.Text = "Public Speaking" Then
            TrPronouncer.Visible = False
            'TrChiefJudge.Visible = False
            'TrAssociateJudge.Visible = False
            TrDictHandler.Visible = False
            TrTimer.Visible = False
            TrDesktopMicStand.Visible = False
            TrExtraMikes.Visible = False
        End If
        Loaddata()
    End Sub
    Private Sub setDDLVisibility()
        TrRoomGuide.Visible = False
        TrRoomMC.Visible = False
        TrPronouncer.Visible = False
        TrChiefJudge.Visible = False
        TrAssociateJudge.Visible = False
        TrLaptopJudge.Visible = False
        TrDictHandler.Visible = False
        TrTimer.Visible = False
        TrGrader.Visible = False
        TrProctor.Visible = False
        TrPodium.Visible = False
        TrPodiumMic.Visible = False
        TrPodiumLaptop.Visible = False
        TrProjector.Visible = False
        TrJudgesTable.Visible = False
        TrJudgeChairs.Visible = False
        TrCJMic.Visible = False
        TrChildMic.Visible = False
        TrAudioMixer.Visible = False
        TrChildMicStand.Visible = False
        TrDesktopMicStand.Visible = False
        TrAudioWire.Visible = False
        TrExtraMikes.Visible = False
        TrInternetAccess.Visible = False
        TrMC.Visible = False
        TrTeamLead.Visible = False
        TrLaptop.Visible = False
        TrTable.Visible = False
        TrChair.Visible = False
        TrMic.Visible = False
        TrMicStand.Visible = False
        TrRegGuide.Visible = False
        TrDeskGuide.Visible = False
        TrFoodServe.Visible = False
    End Sub
    Private Sub LoadDDLTblValues()
        ''Contests..
        ddlRoomGuide.DataSource = dt
        ddlRoomGuide.DataBind()

        ddlRoomMc.DataSource = dt
        ddlRoomMc.DataBind()

        ddlPronouncer.DataSource = dt
        ddlPronouncer.DataBind()

        ddlChiefJudge.DataSource = dt
        ddlChiefJudge.DataBind()

        ddlAssociateJudge.DataSource = dt
        ddlAssociateJudge.DataBind()

        ddlLaptopJudge.DataSource = dt
        ddlLaptopJudge.DataBind()

        ddlDictHandler.DataSource = dt
        ddlDictHandler.DataBind()

        ddlTimer.DataSource = dt
        ddlTimer.DataBind()

        ddlGrader.DataSource = dt
        ddlGrader.DataBind()

        ddlProctor.DataSource = dt
        ddlProctor.DataBind()

        ddlPodium.DataSource = dt
        ddlPodium.DataBind()

        ddlPodiumMic.DataSource = dt
        ddlPodiumMic.DataBind()

        ddlPodiumLaptop.DataSource = dt
        ddlPodiumLaptop.DataBind()

        ddlProjector.DataSource = dt
        ddlProjector.DataBind()

        ddlJudgesTable.DataSource = dt
        ddlJudgesTable.DataBind()

        ddlJudgeChairs.DataSource = dt
        ddlJudgeChairs.DataBind()

        ddlCJMic.DataSource = dt
        ddlCJMic.DataBind()

        ddlChildMic.DataSource = dt
        ddlChildMic.DataBind()

        ddlAudioMixer.DataSource = dt
        ddlAudioMixer.DataBind()

        ddlChildMicStand.DataSource = dt
        ddlChildMicStand.DataBind()

        ddlDesktopMicStand.DataSource = dt
        ddlDesktopMicStand.DataBind()

        ddlAudioWire.DataSource = dt
        ddlAudioWire.DataBind()

        ddlExtraMikes.DataSource = dt
        ddlExtraMikes.DataBind()

        ddlInternetAccess.DataSource = dt
        ddlInternetAccess.DataBind()

        ddlMC.DataSource = dt
        ddlMC.DataBind()

        ddlTeamLead.DataSource = dt
        ddlTeamLead.DataBind()

        ddlLaptop.DataSource = dt
        ddlLaptop.DataBind()

        ddlTable.DataSource = dt
        ddlTable.DataBind()

        ddlChair.DataSource = dt
        ddlChair.DataBind()

        ddlMicroPhone.DataSource = dt
        ddlMicroPhone.DataBind()

        ddlMicStand.DataSource = dt
        ddlMicStand.DataBind()

     
        ddlRegGuide.DataSource = dt
        ddlRegGuide.DataBind()

        ddlDeskGuide.DataSource = dt
        ddlDeskGuide.DataBind()

        ddlFoodServe.DataSource = dt
        ddlFoodServe.DataBind()


    End Sub
    Private Sub clearDDL()
        lblYearErr.Text = ""
        ddlRoomGuide.Items.Clear()
        ddlRoomMc.Items.Clear()
        ddlPronouncer.Items.Clear()
        ddlChiefJudge.Items.Clear()
        ddlAssociateJudge.Items.Clear()
        ddlLaptopJudge.Items.Clear()
        ddlDictHandler.Items.Clear()
        ddlTimer.Items.Clear()
        ddlGrader.Items.Clear()
        ddlProctor.Items.Clear()
        ddlPodium.Items.Clear()
        ddlPodiumMic.Items.Clear()
        ddlPodiumLaptop.Items.Clear()
        ddlProjector.Items.Clear()
        ddlJudgesTable.Items.Clear()
        ddlJudgeChairs.Items.Clear()
        ddlCJMic.Items.Clear()
        ddlChildMic.Items.Clear()
        ddlAudioMixer.Items.Clear()
        ddlChildMicStand.Items.Clear()
        ddlDesktopMicStand.Items.Clear()
        ddlAudioWire.Items.Clear()
        ddlExtraMikes.Items.Clear()
        ddlInternetAccess.Items.Clear()
        ddlMC.Items.Clear()
        ddlTeamLead.Items.Clear()
        ddlLaptop.Items.Clear()
        ddlTable.Items.Clear()
        ddlChair.Items.Clear()
        ddlMicroPhone.Items.Clear()
        ddlMicStand.Items.Clear()
        ddlRegGuide.Items.Clear()
        ddlDeskGuide.Items.Clear()
        ddlFoodServe.Items.Clear()

        LoadDDLValues()
        'LoadDDLTblValues()

    End Sub
    Protected Sub BtnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim SQLEvalStr As String = ""
        Dim SQLString As String = "" '"Insert into RoomRequirement (EventID,Event,EventYear,Purpose,"
        Dim SQLValue As String = "" ' "(" & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & Now.Year() & ",'" & ddlPurpose.SelectedItem.Text.Trim() & "'"
        Dim SQLstr As String = ""
        Dim AddFlag As Boolean = False
        lblYearErr.Text = ""
        SQLEvalStr = SQLEvalStr & "Select Count(*) from RoomRequirement Where EventYear=" & Now.Year() & " and EventID= " & ddlEvent.SelectedValue & " And  Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "'"

        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
            SQLEvalStr = SQLEvalStr & "And ProductGroup ='" & ddlProductGroup.SelectedItem.Text.Trim() & "'"
            If TrCopyFrom.Visible = True Then
                SQLEvalStr = SQLEvalStr & "And Phase =" & ddlPhase1.SelectedValue & ""
            Else
                SQLEvalStr = SQLEvalStr & "And Phase =" & ddlPhase.SelectedValue & ""
            End If
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 
            SQLEvalStr = SQLEvalStr & ""
        Else
            If TrCopyFrom.Visible = True Then
                SQLEvalStr = SQLEvalStr & " And Day =" & ddlDay1.SelectedValue & ""
            Else
                SQLEvalStr = SQLEvalStr & " And Day =" & ddlDay.SelectedValue & ""
            End If
        End If

        Try
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLEvalStr) = 0 Then
                AddFlag = True
                SQLString = "Insert into RoomRequirement (EventID,Event,EventYear,Purpose,"
                SQLValue = "(" & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & Now.Year() & ",'" & ddlPurpose.SelectedItem.Text.Trim() & "',"

                If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
                    SQLString = SQLString & "ProductGroupID,ProductGroup,Phase,"
                    SQLValue = SQLValue & IIf(ddlProductGroup.Visible = True, ddlProductGroup.SelectedValue, "NULL") & "," & IIf(ddlProductGroup.Visible = True, "'" & ddlProductGroup.SelectedItem.Text & "'", "NULL") & ","
                    If TrCopyFrom.Visible = True Then
                        SQLValue = SQLValue & IIf(ddlPhase1.SelectedValue = 0, "0", ddlPhase1.SelectedValue) & "," 'NULL
                    Else
                        SQLValue = SQLValue & IIf(ddlPhase.SelectedValue = 0, "0", ddlPhase.SelectedValue) & "," 'NULL
                    End If
                ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 
                    SQLString = SQLString & ""
                    SQLValue = SQLValue & ""
                Else
                    SQLString = SQLString & "Day,"
                    If TrCopyFrom.Visible = True Then
                        SQLValue = SQLValue & IIf(ddlDay1.SelectedValue = 0, "NULL", ddlDay1.SelectedValue) & ","
                    Else
                        SQLValue = SQLValue & IIf(ddlDay.SelectedValue = 0, "NULL", ddlDay.SelectedValue) & ","
                    End If
                End If

                SQLString = SQLString & "RoomGuide,RoomMC,Pronouncer,CJudge,AJudge,LTJudge,DictH,Timer,Proctor,Grader,Podium,"
                SQLString = SQLString & "PodMic,PodLT,Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,AWires,ExtraMic,IntAccess,"
                SQLString = SQLString & "TeamLead,MC,RegistGuide,FoodServe,DeskGuide,Laptop,Tables,Chair,Mic,MicStand,DeskMicStand,CreatedBy,CreatedDate ) Values "
                SQLValue = SQLValue & IIf(ddlRoomGuide.SelectedValue = 0, "NULL", ddlRoomGuide.SelectedValue) & "," & IIf(ddlRoomMc.SelectedValue = 0, "NULL", ddlRoomMc.SelectedValue) & "," & IIf(ddlPronouncer.SelectedValue = 0, "NULL", ddlPronouncer.SelectedValue) & "," & IIf(ddlChiefJudge.SelectedValue = 0, "NULL", ddlChiefJudge.SelectedValue) & "," & IIf(ddlAssociateJudge.SelectedValue = 0, "NULL", ddlAssociateJudge.SelectedValue) & "," & IIf(ddlLaptopJudge.SelectedValue = 0, "NULL", ddlLaptopJudge.SelectedValue) & "," & IIf(ddlDictHandler.SelectedValue = 0, "NULL", ddlDictHandler.SelectedValue) & ","
                SQLValue = SQLValue & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) & "," & IIf(ddlProctor.SelectedValue = 0, "NULL", ddlProctor.SelectedValue) & "," & IIf(ddlGrader.SelectedValue = 0, "NULL", ddlGrader.SelectedValue) & "," & IIf(ddlPodium.SelectedValue = 0, "NULL", ddlPodium.SelectedValue) & "," & IIf(ddlPodiumMic.SelectedValue = 0, "NULL", ddlPodiumMic.SelectedValue) & "," & IIf(ddlPodiumLaptop.SelectedValue = 0, "NULL", ddlPodiumLaptop.SelectedValue) & "," & IIf(ddlProjector.SelectedValue = 0, "NULL", ddlProjector.SelectedValue) & "," & IIf(ddlJudgesTable.SelectedValue = 0, "NULL", ddlJudgesTable.SelectedValue) & "," & IIf(ddlJudgeChairs.SelectedValue = 0, "NULL", ddlJudgeChairs.SelectedValue) & "," & IIf(ddlCJMic.SelectedValue = 0, "NULL", ddlCJMic.SelectedValue) & "," & IIf(ddlChildMic.SelectedValue = 0, "NULL", ddlChildMic.SelectedValue) & "," & IIf(ddlAudioMixer.SelectedValue = 0, "NULL", ddlAudioMixer.SelectedValue) & "," & IIf(ddlChildMicStand.SelectedValue = 0, "NULL", ddlChildMicStand.SelectedValue) & "," & IIf(ddlAudioWire.SelectedValue = 0, "NULL", ddlAudioWire.SelectedValue) & "," & IIf(ddlExtraMikes.SelectedValue = 0, "NULL", ddlExtraMikes.SelectedValue) & "," & IIf(ddlInternetAccess.SelectedValue = 0, "NULL", ddlInternetAccess.SelectedValue) & ","
                SQLValue = SQLValue & IIf(ddlTeamLead.SelectedValue = 0, "NULL", ddlTeamLead.SelectedValue) & "," & IIf(ddlMC.SelectedValue = 0, "NULL", ddlMC.SelectedValue) & "," & IIf(ddlRegGuide.SelectedValue = 0, "NULL", ddlRegGuide.SelectedValue) & "," & IIf(ddlFoodServe.SelectedValue = 0, "NULL", ddlFoodServe.SelectedValue) & "," & IIf(ddlDeskGuide.SelectedValue = 0, "NULL", ddlDeskGuide.SelectedValue) & "," & IIf(ddlLaptop.SelectedValue = 0, "NULL", ddlLaptop.SelectedValue) & "," & IIf(ddlTable.SelectedValue = 0, "NULL", ddlTable.SelectedValue) & "," & IIf(ddlChair.SelectedValue = 0, "NULL", ddlChair.SelectedValue) & "," & IIf(ddlMicroPhone.SelectedValue = 0, "NULL", ddlMicroPhone.SelectedValue) & "," & IIf(ddlMicStand.SelectedValue = 0, "NULL", ddlMicStand.SelectedValue) & "," & IIf(ddlDesktopMicStand.SelectedValue = 0, "NULL", ddlDesktopMicStand.SelectedValue) & "," & Session("LoginID") & ",GETDATE())"

                SQLstr = SQLString & SQLValue

            Else
                AddFlag = False
                SQLString = SQLString & "Update RoomRequirement Set EventID=" & ddlEvent.SelectedValue & ",Event='" & ddlEvent.SelectedItem.Text & "',EventYear=" & Now.Year() & ",Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "',"

                If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
                    SQLString = SQLString & "ProductGroupID=" & IIf(ddlProductGroup.Visible = True, ddlProductGroup.SelectedValue, "NULL") & ",ProductGroup=" & IIf(ddlProductGroup.Visible = True, "'" & ddlProductGroup.SelectedItem.Text & "'", "NULL") & ","
                    SQLString = SQLString & "Phase="
                    If TrCopyFrom.Visible = True Then
                        SQLString = SQLString & IIf(ddlPhase1.SelectedValue = 0, "NULL", ddlPhase1.SelectedValue) & ","
                    Else
                        SQLString = SQLString & IIf(ddlPhase.SelectedValue = 0, "NULL", ddlPhase.SelectedValue) & ","
                    End If
                ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 
                    SQLString = SQLString & ""
                Else
                    SQLString = SQLString & "Day="
                    If TrCopyFrom.Visible = True Then
                        SQLString = SQLString & IIf(ddlDay1.SelectedValue = 0, "NULL", ddlDay1.SelectedValue) & ","
                    Else
                        SQLString = SQLString & IIf(ddlDay.SelectedValue = 0, "NULL", ddlDay.SelectedValue) & ","
                    End If
                End If

                SQLString = SQLString & "RoomGuide=" & IIf(ddlRoomGuide.SelectedValue = 0, "NULL", ddlRoomGuide.SelectedValue) & ",RoomMC=" & IIf(ddlRoomMc.SelectedValue = 0, "NULL", ddlRoomMc.SelectedValue) & ",Pronouncer= " & IIf(ddlPronouncer.SelectedValue = 0, "NULL", ddlPronouncer.SelectedValue) & ",CJudge=" & IIf(ddlChiefJudge.SelectedValue = 0, "NULL", ddlChiefJudge.SelectedValue) & ",AJudge=" & IIf(ddlAssociateJudge.SelectedValue = 0, "NULL", ddlAssociateJudge.SelectedValue) & ",LTJudge=" & IIf(ddlLaptopJudge.SelectedValue = 0, "NULL", ddlLaptopJudge.SelectedValue) & ",DictH=" & IIf(ddlDictHandler.SelectedValue = 0, "NULL", ddlDictHandler.SelectedValue) & ",Timer=" & IIf(ddlTimer.SelectedValue = 0, "NULL", ddlTimer.SelectedValue) & ",Proctor=" & IIf(ddlProctor.SelectedValue = 0, "NULL", ddlProctor.SelectedValue) & ",Grader=" & IIf(ddlGrader.SelectedValue = 0, "NULL", ddlGrader.SelectedValue) & ",Podium=" & IIf(ddlPodium.SelectedValue = 0, "NULL", ddlPodium.SelectedValue) & ","
                SQLString = SQLString & "PodMic=" & IIf(ddlPodiumMic.SelectedValue = 0, "NULL", ddlPodiumMic.SelectedValue) & ", PodLT=" & IIf(ddlPodiumLaptop.SelectedValue = 0, "NULL", ddlPodiumLaptop.SelectedValue) & ", Projector=" & IIf(ddlProjector.SelectedValue = 0, "NULL", ddlProjector.SelectedValue) & ", JTable=" & IIf(ddlJudgesTable.SelectedValue = 0, "NULL", ddlJudgesTable.SelectedValue) & ", JChair=" & IIf(ddlJudgeChairs.SelectedValue = 0, "NULL", ddlJudgeChairs.SelectedValue) & ", CJMic=" & IIf(ddlCJMic.SelectedValue = 0, "NULL", ddlCJMic.SelectedValue) & ", ChildMic=" & IIf(ddlChildMic.SelectedValue = 0, "NULL", ddlChildMic.SelectedValue) & " , AMixer=" & IIf(ddlAudioMixer.SelectedValue = 0, "NULL", ddlAudioMixer.SelectedValue) & ", ChMicStand=" & IIf(ddlChildMicStand.SelectedValue = 0, "NULL", ddlChildMicStand.SelectedValue) & ", AWires=" & IIf(ddlAudioWire.SelectedValue = 0, "NULL", ddlAudioWire.SelectedValue) & ", ExtraMic=" & IIf(ddlExtraMikes.SelectedValue = 0, "NULL", ddlExtraMikes.SelectedValue) & ", IntAccess=" & IIf(ddlInternetAccess.SelectedValue = 0, "NULL", ddlInternetAccess.SelectedValue) & ","
                SQLString = SQLString & "TeamLead=" & IIf(ddlTeamLead.SelectedValue = 0, "NULL", ddlTeamLead.SelectedValue) & ",MC=" & IIf(ddlMC.SelectedValue = 0, "NULL", ddlMC.SelectedValue) & ",RegistGuide=" & IIf(ddlRegGuide.SelectedValue = 0, "NULL", ddlRegGuide.SelectedValue) & ",FoodServe=" & IIf(ddlFoodServe.SelectedValue = 0, "NULL", ddlFoodServe.SelectedValue) & ",DeskGuide=" & IIf(ddlDeskGuide.SelectedValue = 0, "NULL", ddlDeskGuide.SelectedValue) & ",Laptop=" & IIf(ddlLaptop.SelectedValue = 0, "NULL", ddlLaptop.SelectedValue) & ",Tables=" & IIf(ddlTable.SelectedValue = 0, "NULL", ddlTable.SelectedValue) & ",Chair=" & IIf(ddlChair.SelectedValue = 0, "NULL", ddlChair.SelectedValue) & ",Mic=" & IIf(ddlMicroPhone.SelectedValue = 0, "NULL", ddlMicroPhone.SelectedValue) & ",MicStand=" & IIf(ddlMicStand.SelectedValue = 0, "NULL", ddlMicStand.SelectedValue) & ",DeskMicStand=" & IIf(ddlDesktopMicStand.SelectedValue = 0, "NULL", ddlDesktopMicStand.SelectedValue) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate =GETDATE()" & ""
                SQLString = SQLString & "Where EventYear=" & Now.Year() & " and EventID=" & ddlEvent.SelectedValue & " And Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "'" 'and Event= '" & ddlEvent.SelectedItem.Text & "'

                If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
                    SQLString = SQLString & " And ProductGroup='" & ddlProductGroup.SelectedItem.Text & "'"
                    If TrCopyFrom.Visible = True Then
                        SQLString = SQLString & " And Phase=" & ddlPhase1.SelectedValue
                    Else
                        SQLString = SQLString & " And Phase=" & ddlPhase.SelectedValue
                    End If
                ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 
                    SQLString = SQLString & ""
                Else
                    If TrCopyFrom.Visible = True Then
                        SQLString = SQLString & " And Day=" & ddlDay1.SelectedValue
                    Else
                        SQLString = SQLString & " And Day=" & ddlDay.SelectedValue
                    End If
                End If
                SQLstr = SQLString
            End If

            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLstr)
            lblAddUpdate.Visible = "True"
            If AddFlag = True Then
                lblAddUpdate.Text = "Added Successfully"
            Else
                lblAddUpdate.Text = "Updated Successfully"
            End If

        Catch ex As Exception
            'Response.Write("" & ex.ToString)
        End Try

    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clearDDL()
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        TrCopyFrom.Visible = False
        Loaddata()
    End Sub
    Protected Sub ddlDay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDay.SelectedIndexChanged
        TrCopyFrom.Visible = False
        Loaddata()
    End Sub
    Private Sub Loaddata()
        'clearDDL()
        'LoadDDLTblValues()
        Dim SQLStr As String = "Select top 1 ISNULL(RoomGuide,0) AS RoomGuide,ISNULL(RoomMC,0) as RoomMC,ISNULL(Pronouncer,0) as Pronouncer,ISNULL(CJudge,0) as CJudge,ISNULL(AJudge,0) as AJudge,ISNULL(LTJudge,0) as LTJudge,ISNULL(DictH,0) as DictH,ISNULL(Timer,0) AS Timer,ISNULL(Proctor,0) AS Proctor,ISNULL(Grader,0) AS Grader,ISNULL(Podium,0) AS Podium,ISNULL(PodMic,0) AS PodMic,ISNULL(PodLT,0) AS PodLT,ISNULL(Projector,0) AS Projector,ISNULL(JTable,0) AS JTable,ISNULL(JChair,0) AS JChair,ISNULL(CJMic,0) AS CJMic,ISNULL(ChildMic,0) AS ChildMic,ISNULL(AMixer,0) AS AMixer,ISNULL(ChMicStand,0) AS ChMicStand,ISNULL(AWires,0) AS AWires,ISNULL(ExtraMic,0) AS ExtraMic,ISNULL(IntAccess,0) AS IntAccess,ISNULL("
        SQLStr = SQLStr & "TeamLead,0) AS TeamLead,ISNULL(MC,0) AS MC,ISNULL(RegistGuide,0) AS RegistGuide,ISNULL(FoodServe,0) AS FoodServe,ISNULL(DeskGuide,0) AS DeskGuide,ISNULL(Laptop,0)AS Laptop,ISNULL(Tables,0) AS Tables,ISNULL(Chair,0) AS Chair,ISNULL(Mic,0) AS Mic,ISNULL(MicStand,0) AS MicStand,ISNULL(DeskMicStand,0) AS DeskMicStand From RoomRequirement Where "
        SQLStr = SQLStr & " EventYear=" & Now.Year() & " and   EventID= " & ddlEvent.SelectedValue & " And Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "'" & ""

        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
            If ddlProductGroup.Items.Count > 0 Then
                SQLStr = SQLStr & "  And ProductGroupID= " & ddlProductGroup.SelectedValue & " And Phase=" & ddlPhase.SelectedValue & ""
            End If
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 Then
            SQLStr = SQLStr & ""
        Else
            SQLStr = SQLStr & " And Day =" & ddlDay.SelectedValue & ""
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)
        If ds.Tables(0).Rows.Count = 1 Then
            ddlRoomGuide.SelectedIndex = ddlRoomGuide.Items.IndexOf(ddlRoomGuide.Items.FindByValue(ds.Tables(0).Rows(0)("RoomGuide")))
            ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(ds.Tables(0).Rows(0)("RoomMC")))
            ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(ds.Tables(0).Rows(0)("Pronouncer")))
            ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(ds.Tables(0).Rows(0)("CJudge")))
            ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(ds.Tables(0).Rows(0)("AJudge")))
            ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(ds.Tables(0).Rows(0)("LTJudge")))
            ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(ds.Tables(0).Rows(0)("DictH")))
            ddlTimer.SelectedIndex = ddlTimer.Items.IndexOf(ddlTimer.Items.FindByValue(ds.Tables(0).Rows(0)("Timer")))
            ddlGrader.SelectedIndex = ddlGrader.Items.IndexOf(ddlGrader.Items.FindByValue(ds.Tables(0).Rows(0)("Grader")))
            ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(ds.Tables(0).Rows(0)("Proctor")))
            ddlPodium.SelectedIndex = ddlPodium.Items.IndexOf(ddlPodium.Items.FindByValue(ds.Tables(0).Rows(0)("Podium")))
            ddlPodiumMic.SelectedIndex = ddlPodiumMic.Items.IndexOf(ddlPodiumMic.Items.FindByValue(ds.Tables(0).Rows(0)("PodMic")))
            ddlPodiumLaptop.SelectedIndex = ddlPodiumLaptop.Items.IndexOf(ddlPodiumLaptop.Items.FindByValue(ds.Tables(0).Rows(0)("PodLT")))
            ddlProjector.SelectedIndex = ddlProjector.Items.IndexOf(ddlProjector.Items.FindByValue(ds.Tables(0).Rows(0)("Projector")))
            ddlJudgesTable.SelectedIndex = ddlJudgesTable.Items.IndexOf(ddlJudgesTable.Items.FindByValue(ds.Tables(0).Rows(0)("JTable")))
            ddlJudgeChairs.SelectedIndex = ddlJudgeChairs.Items.IndexOf(ddlJudgeChairs.Items.FindByValue(ds.Tables(0).Rows(0)("JChair")))
            ddlCJMic.SelectedIndex = ddlCJMic.Items.IndexOf(ddlCJMic.Items.FindByValue(ds.Tables(0).Rows(0)("CJMic")))
            ddlChildMic.SelectedIndex = ddlChildMic.Items.IndexOf(ddlChildMic.Items.FindByValue(ds.Tables(0).Rows(0)("ChildMic")))
            ddlAudioMixer.SelectedIndex = ddlAudioMixer.Items.IndexOf(ddlAudioMixer.Items.FindByValue(ds.Tables(0).Rows(0)("AMixer")))
            ddlChildMicStand.SelectedIndex = ddlChildMicStand.Items.IndexOf(ddlChildMicStand.Items.FindByValue(ds.Tables(0).Rows(0)("ChMicStand")))
            ddlDesktopMicStand.SelectedIndex = ddlDesktopMicStand.Items.IndexOf(ddlDesktopMicStand.Items.FindByValue(ds.Tables(0).Rows(0)("DeskMicStand")))
            ddlAudioWire.SelectedIndex = ddlAudioWire.Items.IndexOf(ddlAudioWire.Items.FindByValue(ds.Tables(0).Rows(0)("AWires")))
            ddlExtraMikes.SelectedIndex = ddlExtraMikes.Items.IndexOf(ddlExtraMikes.Items.FindByValue(ds.Tables(0).Rows(0)("ExtraMic")))
            ddlInternetAccess.SelectedIndex = ddlInternetAccess.Items.IndexOf(ddlInternetAccess.Items.FindByValue(ds.Tables(0).Rows(0)("IntAccess")))
            ddlMC.SelectedIndex = ddlMC.Items.IndexOf(ddlMC.Items.FindByValue(ds.Tables(0).Rows(0)("MC")))
            ddlTeamLead.SelectedIndex = ddlTeamLead.Items.IndexOf(ddlTeamLead.Items.FindByValue(ds.Tables(0).Rows(0)("TeamLead")))
            ddlLaptop.SelectedIndex = ddlLaptop.Items.IndexOf(ddlLaptop.Items.FindByValue(ds.Tables(0).Rows(0)("Laptop")))
            ddlTable.SelectedIndex = ddlTable.Items.IndexOf(ddlTable.Items.FindByValue(ds.Tables(0).Rows(0)("Tables")))
            ddlChair.SelectedIndex = ddlChair.Items.IndexOf(ddlChair.Items.FindByValue(ds.Tables(0).Rows(0)("Chair")))
            ddlMicroPhone.SelectedIndex = ddlMicroPhone.Items.IndexOf(ddlMicroPhone.Items.FindByValue(ds.Tables(0).Rows(0)("Mic")))
            ddlMicStand.SelectedIndex = ddlMicStand.Items.IndexOf(ddlMicStand.Items.FindByValue(ds.Tables(0).Rows(0)("MicStand")))
            ddlRegGuide.SelectedIndex = ddlRegGuide.Items.IndexOf(ddlRegGuide.Items.FindByValue(ds.Tables(0).Rows(0)("RegistGuide")))
            ddlDeskGuide.SelectedIndex = ddlDeskGuide.Items.IndexOf(ddlDeskGuide.Items.FindByValue(ds.Tables(0).Rows(0)("DeskGuide")))
            ddlFoodServe.SelectedIndex = ddlFoodServe.Items.IndexOf(ddlFoodServe.Items.FindByValue(ds.Tables(0).Rows(0)("FoodServe")))
        Else
            ddlRoomGuide.SelectedIndex = ddlRoomGuide.Items.IndexOf(ddlRoomGuide.Items.FindByValue(0))
            ddlRoomMc.SelectedIndex = ddlRoomMc.Items.IndexOf(ddlRoomMc.Items.FindByValue(0))
            ddlPronouncer.SelectedIndex = ddlPronouncer.Items.IndexOf(ddlPronouncer.Items.FindByValue(0))
            ddlChiefJudge.SelectedIndex = ddlChiefJudge.Items.IndexOf(ddlChiefJudge.Items.FindByValue(0))
            ddlAssociateJudge.SelectedIndex = ddlAssociateJudge.Items.IndexOf(ddlAssociateJudge.Items.FindByValue(0))
            ddlLaptopJudge.SelectedIndex = ddlLaptopJudge.Items.IndexOf(ddlLaptopJudge.Items.FindByValue(0))
            ddlDictHandler.SelectedIndex = ddlDictHandler.Items.IndexOf(ddlDictHandler.Items.FindByValue(0))
            ddlTimer.SelectedIndex = ddlTimer.Items.IndexOf(ddlTimer.Items.FindByValue(0))
            ddlGrader.SelectedIndex = ddlGrader.Items.IndexOf(ddlGrader.Items.FindByValue(0))
            ddlProctor.SelectedIndex = ddlProctor.Items.IndexOf(ddlProctor.Items.FindByValue(0))
            ddlPodium.SelectedIndex = ddlPodium.Items.IndexOf(ddlPodium.Items.FindByValue(0))
            ddlPodiumMic.SelectedIndex = ddlPodiumMic.Items.IndexOf(ddlPodiumMic.Items.FindByValue(0))
            ddlPodiumLaptop.SelectedIndex = ddlPodiumLaptop.Items.IndexOf(ddlPodiumLaptop.Items.FindByValue(0))
            ddlProjector.SelectedIndex = ddlProjector.Items.IndexOf(ddlProjector.Items.FindByValue(0))
            ddlJudgesTable.SelectedIndex = ddlJudgesTable.Items.IndexOf(ddlJudgesTable.Items.FindByValue(0))
            ddlJudgeChairs.SelectedIndex = ddlJudgeChairs.Items.IndexOf(ddlJudgeChairs.Items.FindByValue(0))
            ddlCJMic.SelectedIndex = ddlCJMic.Items.IndexOf(ddlCJMic.Items.FindByValue(0))
            ddlChildMic.SelectedIndex = ddlChildMic.Items.IndexOf(ddlChildMic.Items.FindByValue(0))
            ddlAudioMixer.SelectedIndex = ddlAudioMixer.Items.IndexOf(ddlAudioMixer.Items.FindByValue(0))
            ddlChildMicStand.SelectedIndex = ddlChildMicStand.Items.IndexOf(ddlChildMicStand.Items.FindByValue(0))
            ddlDesktopMicStand.SelectedIndex = ddlDesktopMicStand.Items.IndexOf(ddlDesktopMicStand.Items.FindByValue(0))
            ddlAudioWire.SelectedIndex = ddlAudioWire.Items.IndexOf(ddlAudioWire.Items.FindByValue(0))
            ddlExtraMikes.SelectedIndex = ddlExtraMikes.Items.IndexOf(ddlExtraMikes.Items.FindByValue(0))
            ddlInternetAccess.SelectedIndex = ddlInternetAccess.Items.IndexOf(ddlInternetAccess.Items.FindByValue(0))
            ddlMC.SelectedIndex = ddlMC.Items.IndexOf(ddlMC.Items.FindByValue(0))
            ddlTeamLead.SelectedIndex = ddlTeamLead.Items.IndexOf(ddlTeamLead.Items.FindByValue(0))
            ddlLaptop.SelectedIndex = ddlLaptop.Items.IndexOf(ddlLaptop.Items.FindByValue(0))
            ddlTable.SelectedIndex = ddlTable.Items.IndexOf(ddlTable.Items.FindByValue(0))
            ddlChair.SelectedIndex = ddlChair.Items.IndexOf(ddlChair.Items.FindByValue(0))
            ddlMicroPhone.SelectedIndex = ddlMicroPhone.Items.IndexOf(ddlMicroPhone.Items.FindByValue(0))
            ddlMicStand.SelectedIndex = ddlMicStand.Items.IndexOf(ddlMicStand.Items.FindByValue(0))
            ddlRegGuide.SelectedIndex = ddlRegGuide.Items.IndexOf(ddlRegGuide.Items.FindByValue(0))
            ddlDeskGuide.SelectedIndex = ddlDeskGuide.Items.IndexOf(ddlDeskGuide.Items.FindByValue(0))
            ddlFoodServe.SelectedIndex = ddlFoodServe.Items.IndexOf(ddlFoodServe.Items.FindByValue(0))
        End If
    End Sub

    Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopyFrom.Click

        TrCopyFrom.Visible = True
        LoadYear1()
        getEvent1()
        getPurpose1()
        getProductGroup1()
        ddlEventYear1.SelectedValue = ddlEventYear.SelectedValue
        ddlEvent1.SelectedValue = ddlEvent.SelectedValue
        ddlPurpose1.Text = ddlPurpose.SelectedItem.Text
        ddlEventYear1.Enabled = False
        ddlEvent1.Enabled = False
        ddlPurpose1.Enabled = False
        lblYearErr.Text = ""

        If ddlPurpose.SelectedItem.Text.Trim() = "Contests" Or ddlPurpose.SelectedItem.Text.Trim() = "Grading" Then
            ddlProductGroup1.SelectedItem.Text = ddlProductGroup.SelectedItem.Text
            ddlProductGroup1.Enabled = False
            TdProductGroup1.Visible = True
            ddlProductGroup1.Visible = True
            TdPhase1.Visible = True
            ddlPhase1.Visible = True
            TdDay1.Visible = False
            ddlDay1.Visible = False
        ElseIf (ddlPurpose.SelectedItem.Text.Trim() = "InfoDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "NSFDesk") Or (ddlPurpose.SelectedItem.Text.Trim() = "RecepDesk") Then '6\8\10 Then
            ddlPurpose1.Enabled = True
            TdProductGroup1.Visible = False
            ddlProductGroup1.Visible = False
            TdPhase1.Visible = False
            ddlPhase1.Visible = False
            TdDay1.Visible = False
            ddlDay1.Visible = False
        Else
            TdProductGroup1.Visible = False
            ddlProductGroup1.Visible = False
            TdPhase1.Visible = False
            ddlPhase1.Visible = False
            TdDay1.Visible = True
            ddlDay1.Visible = True
        End If
    End Sub

    Protected Sub BtnCopyPrevYear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopyPrevYear.Click
        Try
            lblYearErr.Text = ""
            Dim SQLCopy As String = "Insert into RoomRequirement(EventID,Event,EventYear,Purpose,ProductGroupID,ProductGroup,Phase,RoomGuide,RoomMC,Pronouncer,Cjudge,AJudge,LTJudge,DictH,"
            SQLCopy = SQLCopy & "Timer,Proctor,Grader,TeamLead,MC,RegistGuide,FoodServe,DeskGuide,Podium,PodMic,PodLT,Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,"
            SQLCopy = SQLCopy & "DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,Mic,MicStand,Day,CreatedBy,CreatedDate)"
            SQLCopy = SQLCopy & "Select " & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & Now.Year() & ",Purpose,ProductGroupID,ProductGroup,Phase,RoomGuide,RoomMC,Pronouncer,Cjudge,AJudge,LTJudge,DictH,"
            SQLCopy = SQLCopy & "Timer,Proctor,Grader,TeamLead,MC,RegistGuide,FoodServe,DeskGuide,Podium,PodMic,PodLT,Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,"
            SQLCopy = SQLCopy & "DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,Mic,MicStand,Day," & Session("LoginID") & ",GETDATE() " & " From RoomRequirement Where EventYear =" & Now.Year() - 1 & " and EventID=" & ddlEvent.SelectedValue & ""

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from RoomRequirement Where EventYear=" & Now.Year() - 1 & " and EventID=" & ddlEvent.SelectedValue & "") > 0 Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLCopy) > 0 Then
                    lblYearErr.Text = "Last Year's Data Copied Successfully ."
                    Loaddata()
                End If
            Else
                lblYearErr.Text = "No Data Exists for the previous year to copy"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub btnDisplay_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDisplay.Click
        Dim StrDisplay As String = ""
        StrDisplay = "Select * from RoomRequirement where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase=" & ddlPhase.SelectedValue
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDisplay)

        If ds.Tables(0).Rows.Count > 0 Then
            
            DGDisplay.DataSource = ds
            DGDisplay.DataBind()
        End If

    End Sub
End Class
