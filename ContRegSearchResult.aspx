<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContRegSearchResult.aspx.vb" Inherits="ContRegSearchResult" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style="text-align:left">
           <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/ContRegSearch.aspx" CssClass="btn_02" runat="server">Back to Contest Registrations Search</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
</div>
<table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >

     <tr bgcolor="#FFFFFF" >
        <td colspan="4" align="center" class="Heading">Search Results</td>
     </tr>
     </table>
     <br />
    <asp:Label ForeColor="Red" ID="lblErr" runat="server" ></asp:Label>
     <br />
<asp:GridView ID="drgsearchrresut" runat="server"  AllowPaging="true" AllowSorting="true" AutoGenerateColumns="false"   CellPadding="0" style="margin-left: 10px">

<Columns>           
               
                <asp:HyperLinkField  DataTextField="Center" DataNavigateUrlFields="parentid,childnumber" DataNavigateUrlFormatString="fetchreg.aspx?parentid={0}&childnumber={1}"  HeaderText="Center" >                               
                     
                     <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                     <ItemStyle Height="25px" Width="100px" />
                </asp:HyperLinkField>
               
               <asp:BoundField DataField="Participant Name" HeaderText="Child Name">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                 <asp:BoundField DataField="BadgeNumber" HeaderText="Badge#" >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="10px" />
                </asp:BoundField>
                <asp:BoundField DataField="Parent Name" HeaderText="Parent name" >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                <asp:BoundField DataField="Automemberid" HeaderText="MemberID" >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="E-Mail" HeaderText="Parent's Email">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="150px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="Phone" HeaderText="Parent's Phone">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="register_for" HeaderText="Registered For" >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="date_added" HeaderText="Registered Date">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="RegFee_status" HeaderText="Payment Status">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="50px" />
                </asp:BoundField>
                 <asp:BoundField DataField="PaymentDate" DataFormatString="{0:d}" HeaderText="Payment Date">
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="10px" />
                </asp:BoundField>
               
                <asp:BoundField DataField="regfee_reference" HeaderText="Payment Reference " >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="200px" />
                </asp:BoundField>
                
                <asp:BoundField DataField="SpouseName" HeaderText="Spouse Name" >
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="150px" />
                </asp:BoundField>
               
                
                <%--<asp:TemplateField HeaderText="Link">
                    <ItemTemplate>
                      <asp:HyperLink Id="hlkChangecenter" NavigateUrl= '<%# "transfer.asp?parentid="& Eval("parentid")& "&childnumber="& Eval("childnumber")&"&contestcode="& Eval("contestid")&"&chapterid="& Eval("CChapter")&"&productcode="& Eval("productcode")%>'  runat="server" Text="Change Center"> </asp:HyperLink>
                       <asp:HyperLink Id="hlkEnterScore" NavigateUrl='<%# "enterscores.asp?id="& Eval("contestant_id") %>' runat="server" Text='Enter Score'></asp:HyperLink>
                    </ItemTemplate>
                    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
                    <ItemStyle Height="25px" Width="100px" />
                </asp:TemplateField>--%>
                
               
                <asp:BoundField DataField="childnumber" HeaderText="ChildNumb" Visible="false" />
                <asp:BoundField DataField="parentid" HeaderText="parentid" Visible="false" />   
                <asp:BoundField DataField="contestid" HeaderText="contestid" Visible="False" />
                <asp:BoundField DataField="CChapter" Visible="false" />
                <asp:BoundField DataField="contestant_id" Visible="false" />
                <asp:BoundField DataField="productcode" Visible="false" />
  </Columns>

</asp:GridView>
</asp:Content>

