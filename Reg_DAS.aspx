<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Reg_DAS.aspx.vb" Inherits="Reg_DAS" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

		<table cellspacing="1" cellpadding="3" width="700px" style="margin-left:10px" border="0">
		
		<tr>
		<td></td>
		<td class="ContentSubTitle" align="center" colspan="4" style="height: 41px; width:500px; text-align:center ">
		<h1 align="center"><font face="Arial" color="#0000ff" size="5"><b> Dollar-A-Square</b></font>&nbsp;<br /></h1></td></tr>
		<tr>
		<td><asp:LinkButton ID="lnkback" runat="server" Text="Back to Main Page" CausesValidation="false" ></asp:LinkButton></td>
		<td style="text-align:center "><asp:HyperLink ID="hpldas" runat="server" Text="What is DAS?" NavigateUrl="javascript:var w=window.open('http://www.northsouth.org/st/kc/dollarasquare.asp','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes;');"  ></asp:HyperLink></td>
		<td style="text-align:center "><asp:HyperLink ID="lplddas" runat="server" Text="Download DAS Sheet" NavigateUrl="javascript:var w=window.open('http://www.northsouth.org/st/kc/docs/DAS_PledgeSheet.pdf ','_blank','toolbar=no,location=no,directories=no,status=yes,menubar=no,scrollbars=yes,resizable=yes;');"  ></asp:HyperLink></td>
		</tr>
		
		</table>
		<br />
		<br />
		<table cellspacing="1" cellpadding="3" width="700px" style="margin-left:10px" border="0">
		
        <tr>
        <td align="center" style="width: 25px; height: 10px;">
            </td>
        <td align="right" style="height: 10px; text-align:right">
            <asp:Label runat="server" ID="lblfn" Text="First Name:" />
        </td>
        <td style="width: 50px; height: 10px;"> <asp:TextBox ID="txtFname" runat="server"></asp:TextBox> </td></tr>
        
        <tr>
        <td style="height: 10px; width: 25px;" align="center"><asp:DropDownList runat="server" ID="ddlchapter" Width="155px" ></asp:DropDownList></td>
        <td align="right" style="height: 10px; text-align:right">
            <asp:Label runat="server" ID="lblln" Text="Last Name:" ></asp:Label></td>
        <td style="width: 50px; height:10px;"> <asp:TextBox ID="txtLname" runat="server"></asp:TextBox> </td> </tr>
        
        <tr>
         <td style="width: 25px"></td>
         <td align="right" style="height: 10px; text-align:right">
             <asp:Label runat="server" ID="lblemail" Text="Email:"></asp:Label>  </td>
        <td style="width: 50px"> <asp:TextBox ID="txtemail" runat="server"></asp:TextBox> </td> </tr> 
        
        <tr>
        <td style="width: 25px; height:10px;"></td>
        <td align="right" style="height: 10px; text-align:right ">
            <asp:Label runat="server" ID="lblSc" Text="Select a Child:"></asp:Label> </td>
        <td style="height: 46px; width: 10px;"> <asp:DropDownList  ID="drpchild" runat="server" Width="155px"></asp:DropDownList> </td> </tr>
        
        <tr>
        <td style="width: 25px"> <asp:Button ID="btnregister" runat="server" Text="Register for DAS" /> </td>
       
        <td style="width: 128px">  </td>
        <td><asp:Button ID="btnsubmit" runat="server" Text="Submit" /><asp:Label ID="lblerr1" runat="server" ForeColor="red"></asp:Label> </td> </tr>
        </table>

        <br />
      
        <div style="text-align:left ;">
           <asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label>
        </div>
        
        <br />
        
        <asp:GridView ID="grddsa" runat="server" AllowPaging="true" AutoGenerateColumns="false" OnRowEditing="grddsa_RowEditing" OnRowCancelingEdit="grddsa_RowCancelingEdit" OnRowUpdating="grddsa_RowUpdating" >
        <Columns>
        
        <asp:TemplateField >
    <ItemTemplate>
     <asp:LinkButton ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" CausesValidation="false" />
     </ItemTemplate>
     <EditItemTemplate>
      <asp:LinkButton ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />
      <asp:LinkButton ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" CausesValidation="false" />
      </EditItemTemplate>
      </asp:TemplateField> 
   
      <asp:TemplateField HeaderText="Child Name">
           <ItemTemplate>
              <asp:Label ID="lblcname" runat="server" Text='<%#Eval("ChildName")%>'></asp:Label>
           </ItemTemplate>
           <FooterTemplate>
            <asp:Label runat="server" ID="lblchild1" />
            </FooterTemplate>
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Width="150px" Font-Size="Small" ForeColor="Black" />
        </asp:TemplateField>
               
        <asp:TemplateField HeaderText="Registered Date" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center">
           <ItemTemplate>
              <asp:Label ID="lblcdate" runat="server" Text='<%#Eval("CreateDate","{0:MM/dd/yyyy}")%>'></asp:Label>
            </ItemTemplate>
            <FooterTemplate>
            <asp:Label runat="server" ID="lblcreatedate1" />
            </FooterTemplate>
            <HeaderStyle Width="25px" BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
        </asp:TemplateField>
                  
        <asp:TemplateField HeaderText="Amount Collected" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" ItemStyle-Height ="10px" ItemStyle-Width="90px"  FooterStyle-Height="10px" FooterStyle-Width="90px">
           <HeaderStyle Width="25px" BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
           <ItemTemplate>
                <%#Eval("AmountCol", "{0:n2}")%>
                <asp:TextBox ID="txtamtcol1" runat="server"  Visible="false" Width="65px" />
                </ItemTemplate>
           
           <EditItemTemplate>
               <asp:TextBox runat="server" ID="txtamtcol"  Text='<%#Eval("AmountCol","{0:n2}")%>' Width="65px" />
               <asp:RegularExpressionValidator  ID="REV1" runat="server" ControlToValidate="txtamtcol" ErrorMessage="Enter Valid Amount Format (nnn.nn)" ValidationExpression="^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$"></asp:RegularExpressionValidator> 
          </EditItemTemplate>
           <FooterTemplate>
           <asp:TextBox runat="server" ID="amountcoll" Width="65px"/>
          <asp:RegularExpressionValidator ID="REV2" runat="server" ControlToValidate="amountcoll" ErrorMessage="Enter Valid Amount Format (nnn.nn)" ValidationExpression ="^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$"></asp:RegularExpressionValidator> 
           </FooterTemplate>
        </asp:TemplateField>
             
        <asp:TemplateField HeaderText="Date Mailed" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Height ="10px" ItemStyle-Width="90px"  FooterStyle-Height="10px" FooterStyle-Width="90px">   
        <ItemTemplate> 
          <%#Eval("DateMailed", "{0:MM/dd/yyyy}")%>
          <asp:TextBox ID="txtdtcol1" runat="server"  Visible="false" Width="65px" />
            </ItemTemplate>
           <EditItemTemplate>
               <asp:TextBox runat="server" ID="txtdmail" Text='<%#Eval("DateMailed","{0:MM/dd/yyyy}")%>' Width="65px" />
               <asp:RegularExpressionValidator ID="REV3" runat="server" ControlToValidate="txtdmail" 
                            ErrorMessage="Enter Valid Date(MM/DD/YYYY)" ValidationExpression="(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d"></asp:RegularExpressionValidator>
           <%--<asp:RequiredFieldValidator ID="RFV3" runat="server" ControlToValidate="txtdmail" ErrorMessage="Enter Date Mailed"></asp:RequiredFieldValidator>--%>
           </EditItemTemplate>
           <FooterTemplate>
           <asp:TextBox runat="server" ID="txtdate1" Width="65px" />
           <asp:RegularExpressionValidator ID="REV4" runat="server" ControlToValidate="txtdate1" 
                            ErrorMessage="Enter Valid Date(MM/DD/YYYY)" ValidationExpression="(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d"></asp:RegularExpressionValidator>
           <%--<asp:RequiredFieldValidator ID="REF4" runat="server" ControlToValidate="txtdate1" ErrorMessage="Enter Date Mailed"></asp:RequiredFieldValidator>--%>
           </FooterTemplate>
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
        </asp:TemplateField>
         
        <asp:TemplateField HeaderText="Amount Received" ItemStyle-HorizontalAlign="Right" FooterStyle-HorizontalAlign="Right" ItemStyle-Height ="10px" ItemStyle-Width="90px"  FooterStyle-Height="10px" FooterStyle-Width="90px" >
           <HeaderStyle Width="25px" BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
           <ItemTemplate> 
           <%#Eval("AmountRec", "{0:n2}")%>  
            </ItemTemplate>
           <EditItemTemplate>
               <asp:TextBox runat="server" ID="txtarec" Text='<%#Eval("AmountRec","{0:n2}")%>' Width="65px" />
              <asp:RegularExpressionValidator ID="REV5" runat="server" ControlToValidate="txtarec" ErrorMessage="Enter Valid Amount Format (nnn.nn)" ValidationExpression = "^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$"></asp:RegularExpressionValidator> 
           </EditItemTemplate>
           <FooterTemplate>
           <asp:TextBox ID="txtamountrec1" runat="server" Width="65px"></asp:TextBox>
          <asp:RegularExpressionValidator ID="REV6" runat="server"  ControlToValidate="txtamountrec1" ErrorMessage="Enter Valid Amount Format (nnn.nn)" ValidationExpression="^\$?[0-9]+(,[0-9]{3})*(\.[0-9]{2})?$"></asp:RegularExpressionValidator>
           </FooterTemplate>
        </asp:TemplateField>
                        
         <asp:TemplateField HeaderText="Date Received" ItemStyle-HorizontalAlign="Center" FooterStyle-HorizontalAlign="Center" ItemStyle-Height ="10px" ItemStyle-Width="90px"  FooterStyle-Height="10px" FooterStyle-Width="90px">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
           <ItemTemplate> 
           <%#Eval("DateReceived", "{0:MM/dd/yyyy}")%>  
            </ItemTemplate>
           <EditItemTemplate>
               <asp:TextBox runat="server" ID="txtdrec" Text='<%#Eval("DateReceived","{0:MM/dd/yyyy}")%>' Width="65px" />
                <%--<asp:RequiredFieldValidator ID="RFV7" runat="server" ControlToValidate="txtdrec" ErrorMessage="Enter Date Received"></asp:RequiredFieldValidator>--%>
               <asp:RegularExpressionValidator ID="REV7" runat="server" ControlToValidate="txtdrec" 
                            ErrorMessage="Enter Valid Date(MM/DD/YYYY)" ValidationExpression="(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d"></asp:RegularExpressionValidator>
           </EditItemTemplate>
           <FooterTemplate>
           <asp:TextBox runat="server" ID="txtDateRec1" Width="65px"/>
            <%--<asp:RequiredFieldValidator ID="RFV7" runat="server" ControlToValidate="txtDateRec1" ErrorMessage="Enter Date Received"></asp:RequiredFieldValidator>--%>
           <asp:RegularExpressionValidator ID="REV8" runat="server" ControlToValidate="txtDateRec1" 
                            ErrorMessage="Enter Valid Date(MM/DD/YYYY)" ValidationExpression="(0[1-9]|1[012])[/](0[1-9]|[12][0-9]|3[01])[/](19|20)\d\d"></asp:RegularExpressionValidator>
           </FooterTemplate>
        </asp:TemplateField>
                
        <asp:TemplateField HeaderText="Parent Name" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Width="150px" Font-Size="Small" ForeColor="Black" />
           <ItemTemplate> 
           <asp:Label ID="lblpname" runat="server" Width="150" Text='<%#Eval("ParentName")%>'></asp:Label>  
            </ItemTemplate>
            <FooterTemplate>
            <asp:Label runat="server" ID="lblparentname1" />
            </FooterTemplate>
        </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Address" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" Width="150px" />
           <ItemTemplate> 
          <asp:Label ID="lbladd" runat="server" Text='<%#Eval("address1")%>'></asp:Label>
            </ItemTemplate>
            <FooterTemplate>
            <asp:Label runat="server" ID="lbladdress1" />
            </FooterTemplate>
        </asp:TemplateField>
     
      <asp:TemplateField HeaderText="City" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" Width = "75px" />
           <ItemTemplate> 
           <asp:Label ID="lblcity" runat="server" Text='<%#Eval("city")%>'></asp:Label>
            </ItemTemplate>
            <FooterTemplate>
            <asp:Label runat="server" ID="lblcity1" />
            </FooterTemplate>
      </asp:TemplateField>
        
         <asp:TemplateField HeaderText="State" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" Width="75px" />
           <ItemTemplate> 
           <asp:Label ID="lblstate" Width="100px" runat="server" Text='<%#Eval("state")%>'></asp:Label> 
            </ItemTemplate>
            <FooterTemplate>
            <asp:Label runat="server" ID="lblstate1" />
            </FooterTemplate>
         </asp:TemplateField>
        
         <asp:TemplateField HeaderText="Phone" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" Width="75px" />
           <ItemTemplate> 
           <asp:Label ID="lblphone" runat="server" Text='<%#Eval("hphone")%>'></asp:Label> 
            </ItemTemplate>
         </asp:TemplateField>  
         
         <asp:TemplateField HeaderText="DASIS" Visible="false" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="Small" ForeColor="Black" />
           <ItemTemplate> 
           <asp:Label ID="lbldasid" runat="server" Text='<%#Eval("DASID")%>'></asp:Label> 
            </ItemTemplate>
         </asp:TemplateField> 
    </Columns>
 </asp:GridView>
        
       <asp:Button ID="BtnAdd" runat="server" Text="ADD Child to DAS" Visible="false"  />
        
</asp:Content>

