<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Email_OnLineWkShop.aspx.vb" Inherits="Email_OnLineWkShop" Title="National Coordinator's - Send EMail" %>

<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <table style="width: 100%">
        <tr>
            <td width="30%">
                <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="284px"></asp:HyperLink>
            </td>
             <td class="ContentSubTitle" valign="top" align="left">
                <h2>Online Workshop Parents</h2>
            </td>
        </tr>
    </table> 
    <center><asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
                <asp:Label ID="lblerr" runat="server" ForeColor="Red" Text="You are not allowed to use this application." Visible="False" Width="471px"></asp:Label></center>
    
    <table>

        <tr style="height: 25px">
            <td align="right" style="height: 25px; width: 6px" nowrap="nowrap">
                <asp:Label ID="lbltarget" runat="server" Text="Target:" Width="136px" Font-Bold="True"></asp:Label></td>
            <td style="width: 64px; height: 25px;" nowrap="nowrap">
                <asp:RadioButton ID="rbtall" runat="server" Text="All" GroupName="Group1" AutoPostBack="true" OnCheckedChanged="rbtall_CheckedChanged" />&nbsp;&nbsp;&nbsp; </td>
              <td style="width: 70px; "></td>
            <td style="width: 25px;"></td>

            <td style="width: 130px;">
                <asp:RadioButton ID="btnregisteredparetns" runat="server" Text="Registered Parents" AutoPostBack="true" OnCheckedChanged="btnregisteredparetns_CheckedChanged" Checked="true" GroupName="Group1" Width="225px" />
            </td>
            <td>&nbsp;</td>
            <td style=" width: 25%">&nbsp;</td>
        </tr>
        <tr >
            <td dir="ltr" width="72px">&nbsp;<span style="font-size: small"><strong># of Years: </strong></span>
            </td>

            <td> <asp:DropDownList ID="ddlnfYears" runat="server" Enabled="False" Width="60px">
                <asp:ListItem Selected="True" Value="0">Years</asp:ListItem>
                <asp:ListItem Value="-1">1</asp:ListItem>
                <asp:ListItem Value="-2">2</asp:ListItem>
                <asp:ListItem Value="-3">3</asp:ListItem>
                <asp:ListItem Value="-4">4</asp:ListItem>
                <asp:ListItem Value="-5">5</asp:ListItem>
                <asp:ListItem Value="-6">6</asp:ListItem>
                <asp:ListItem Value="-7">7</asp:ListItem>
                <asp:ListItem Value="-8">8</asp:ListItem>
                <asp:ListItem Value="-9">9</asp:ListItem>
                <asp:ListItem Value="-10">10</asp:ListItem>
                <asp:ListItem Value="-11">11</asp:ListItem>
                <asp:ListItem Value="-12">12</asp:ListItem>
                <asp:ListItem Value="-13">13</asp:ListItem>
                <asp:ListItem Value="-14">14</asp:ListItem>
                <asp:ListItem Value="-15">15</asp:ListItem>
            </asp:DropDownList></td>
        </tr>
    </table>
    <table width="100%">


        <tr>

            <td width="20%"></td>
            <td>

                <table id="tabletarget" runat="server" width="100%" border="0" align="center" cellpadding="0" cellspacing="0">

                    <tr>
                        <td style="height: 28px;" nowrap="nowrap"></td>
                        <td style="width: 64px; height: 28px;"></td>
                        <td style="width: 118px; height: 28px;">
                            <asp:Label ID="lblevent" runat="server" Text="Event:" Font-Bold="true"></asp:Label>
                        </td>
                        <td style=" height: 28px;">
                            <asp:DropDownList Enabled="false" ID="drpevent" Width="140px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpevent_SelectedIndexChanged">
                                <asp:ListItem Value="20" Text="Online Workshop" Selected="True"></asp:ListItem>

                                <%--<asp:ListItem Value="13" Text="Coaching"></asp:ListItem>--%>
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr>
                        <td style="height: 30px;"></td>
                        <td style="height: 30px;"></td>
                        <td style="width: auto; height: 30px;">
                            <asp:Label ID="lblregistrationtype" runat="server" Text="Type of Registration:" Width="139px" Font-Bold="True"></asp:Label>
                        </td>
                       
                        <td style=" height: 30px;">
                            <asp:DropDownList ID="drpregistrationtype" runat="server">
                            </asp:DropDownList>
                        </td>

                    </tr>
                    <tr style="height: 25px">
                        <td style="height: 95px; width: 6px;"></td>
                        <td style="width: 64px; height: 95px;"></td>
                        <td style="height: 95px; width: 118px">
                            <asp:Label ID="lblproductgroup" Text="Product Group:" Width="140px" runat="server" Font-Bold="true"></asp:Label>
                        </td>
                        
                        <td style="height: 95px">
                            <asp:ListBox ID="lstProductGroup" DataValueField="ProductGroupID" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged"></asp:ListBox>
                        </td>
                    </tr>
                    <tr style="height: 25px">
                        <td style="width: 6px"></td>
                        <td style="width: 64px"></td>
                        <td style="width: 118px">
                            <asp:Label ID="lblProductid" Text="Product:" runat="server" Width="140px" Font-Bold="true" ></asp:Label>
                        </td>
                        
                        <td style="">
                            <asp:ListBox ID="lstProductid" Enabled="false" DataValueField="ProductID" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged"></asp:ListBox>
                            <br />
                            <br />
                        </td>
                    </tr>

                    <tr style="height: 25px">
                        <td style="width: 6px"></td>
                        <td style="width: 64px"></td>
                        <td style="width: 118px">
                            <asp:Label ID="lblyear" Text="Event Year:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
                        </td>
                        
                        <td style="">
                            <asp:ListBox ID="lstyear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server" OnSelectedIndexChanged="lstyear_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
                            <br />
                            <br />
                        </td>
                        <td></td>
                    </tr>
                    <tr style="height: 25px">
                        <td style="width: 6px"></td>
                        <td style="width: 64px"></td>
                        <td style="width: 118px">
                            <asp:Label ID="lblWsDate" Text="Workshop Date:" runat="server" Width="140px" Font-Bold="true"></asp:Label>&nbsp;</td>
                        
                        <td style="">
                            <asp:DropDownList ID="ddlWSDate" runat="server" DataTextField="EventDate" DataValueField="EventDate" Width="140px">
                            </asp:DropDownList>
                        </td>
                        <td rowspan="2">
                            <asp:Label ID="lblNote" runat="server" ForeColor="Red"
                                Text="**Note:  Sends email if and only if <br /> a) Valid Email flag is NULL and  <br /> b) Newsletter is not in 2 or 3"
                                Visible="true"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 6px; height: 30px;"></td>
                        <td style="width: 64px; height: 30px;"></td>
                        <td style="width: 118px; height: 30px;">
                            <asp:Label ID="Label1" Text="Exclude Paid Registrations:" runat="server" Width="183px" Font-Bold="True"></asp:Label>
                        </td>
                        <td style="width: 84px; height: 30px;">
                            <asp:CheckBox ID="ckboxExcludePaidReg" runat="server" />
                        </td>
                        <td style=" height: 30px;">
                            &nbsp;</td>
                    </tr>
                    <tr>
                        <td style="height: 24px; width: 6px;"></td>
                        <td style="height: 24px; width: 64px;"></td>
                        <td style="width: 253px; height: 24px;" align="left" colspan="3">
                            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            <asp:Button ID="btnselectmail" runat="server" Text="Continue" />
                        &nbsp;</td>
                    </tr>
                </table>
            </td>

        </tr>
    </table>
</asp:Content>

