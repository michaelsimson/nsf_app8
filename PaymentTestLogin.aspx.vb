Imports System
Imports System.Data
Imports System.IO
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Data

Partial Class PaymentTestLogin
    Inherits System.Web.UI.Page


    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim SecureUrl As String
        Dim entryToken As String
        If Not Page.IsPostBack Then
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost") Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
            ResetSessionVariables()
            entryToken = Request.QueryString("entry")

            If (entryToken Is Nothing) Then  'see if it is in the session
                entryToken = Session("entryToken")
            End If

            If (entryToken <> Nothing) Then
                If (entryToken = "P") Or (entryToken = "p") Then
                    Session("entryToken") = "Parent"
                ElseIf (entryToken = "V") Or (entryToken = "v") Then
                    Session("entryToken") = "Volunteer"
                ElseIf (entryToken = "D") Or (entryToken = "d") Then
                    Session("entryToken") = "Donor"
                End If
            End If

            ViewState("TryCount") = 0
            tblErrorLogin.Visible = False
            tblMissingProfile.Visible = False
        End If

        'Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
        'If (homeURL <> Nothing) Then
        'homeLink.HRef = homeURL
        'End If

        'eg. for accessing masterpage controls
        'Dim menu As Menu = CType(Master.FindControl("NavLinks"), Menu)
        'menu.Items.Add(New MenuItem("from Login", "", "", "login.aspx"))

        'eg. through exposed methods
        'Dim nsfMaster As NSFMasterPage = Me.Master
        'nsfMaster.addMenuItem(New MenuItem("newTest", "login.aspx"))
        'nsfMaster.addBackMenuItem(homeURL)



    End Sub

    Private Sub btnLogin_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim userID As String = ""  'Login_master
        Dim autoMemberID As String = ""  'IndSpouse
        Dim volunteerMemberID As String = "" 'Volunteer
        Dim entryToken As String = Session("entryToken")

        tblErrorLogin.Visible = False

        If ViewState("TryCount") > 2 Then
            btnLogin.Visible = False
            tblLogin.Visible = False
            Session.Abandon()
        End If
        'authenticate
        userID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetUserLoginID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)), New SqlParameter("@Password", Server.HtmlEncode(txtPassword.Text)))
        If (userID <> "") Then
            ' catch users with no profile info in IndSpouse
            autoMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetIndSpouseID", New SqlParameter("@Email", Server.HtmlEncode(txtUserId.Text)))
            If (autoMemberID <> "") Then
                If entryToken.ToString <> "" Then
                    Select Case entryToken.ToString.ToUpper
                        Case "PARENT"
                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                            'Response.Redirect("UserFunctions.aspx")
                            Response.Redirect("paymentTestPage.aspx") 'for test payment
                        Case "VOLUNTEER"
                            'match autoMemberID to volunteer memberID
                            volunteerMemberID = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetVolunteerMemberID", New SqlParameter("@autoMemberID", autoMemberID))
                            If (volunteerMemberID <> "") Then
                                SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                                Response.Redirect("VolunteerFunctions.aspx")
                            Else
                                'ResetSessionVariables()
                                tblMissingRole.Visible = True
                                tblLogin.Visible = False
                                Session.Abandon()
                            End If
                        Case "DONOR"
                            SetSessionVariables(Server.HtmlEncode(txtUserId.Text), autoMemberID)
                            Response.Redirect("DonorFunctions.aspx")
                    End Select
                Else
                    tblMissingToken.Visible = True
                End If

            Else
                'user profile not present in Indspouse
                Session("LoggedIn") = "true"
                Session("LoginEmail") = Server.HtmlEncode(txtUserId.Text)
                tblMissingProfile.Visible = True
                tblLogin.Visible = False
                tblErrorLogin.Visible = False
            End If
        Else
            tblErrorLogin.Visible = True

        End If

    End Sub
    Private Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnContinue.Click
        Response.Redirect("Registration.aspx")
    End Sub
    Private Sub ResetSessionVariables()
        Session("LoginID") = Nothing
        Session("LoggedIn") = Nothing
        Session("LoginEmail") = Nothing
        Session("CustIndID") = Nothing
        Session("FatherID") = Nothing
        Session("MotherID") = Nothing
        Session("CustSpouseID") = Nothing
        Session("ChapterID") = Nothing
        Session("ContestsSelected") = Nothing
        Session("EvenID") = Nothing
        Session("LateFee") = Nothing
        Session("MealsAmount") = Nothing
        Session("Donation") = Nothing
        Session("DonationFor") = Nothing
    End Sub
    Private Sub SetSessionVariables(ByVal email As Object, ByVal autoMemberId As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim drIndSpouse As SqlDataReader

        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndAndSpouseRecords", New SqlParameter("@autoMemberID", autoMemberId))

        Dim donorType As String
        Dim gender As String
        Dim relationshipId As Integer

        Session("LoginID") = autoMemberId
        Session("LoggedIn") = "True"
        Session("LoginEmail") = email
        Session("CustIndID") = Nothing
        Session("FatherID") = Nothing
        Session("MotherID") = Nothing
        Session("CustSpouseID") = Nothing

        If (Session("entryToken") = "Parent" Or Session("entryToken") = "Donor") Then
            If (drIndSpouse.HasRows()) Then ''drIndSpouse will have b oth Ind and Spouse records (if there is any)
                Do While (drIndSpouse.Read())
                    donorType = drIndSpouse("DonorType")
                    gender = drIndSpouse("Gender")
                    relationshipId = drIndSpouse("Relationship")

                    If (donorType <> Nothing And donorType.ToUpper() = "IND") Then
                        Session("CustIndID") = autoMemberId
                    ElseIf (donorType <> Nothing And donorType.ToUpper() = "SPOUSE") Then
                        Session("CustSpouseID") = autoMemberId
                    End If

                    If (gender <> Nothing And gender.ToLower = "male") Then
                        Session("FatherID") = autoMemberId
                    Else
                        Session("MotherID") = autoMemberId
                    End If
                Loop
            End If
        End If

    End Sub

    Private Function CheckDuplicateReAttempt(ByVal LoginID As String) As Boolean
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim prmArray(2) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@Email"
        prmArray(0).Value = Session("LoginEmail")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@RetValue"
        prmArray(1).SqlDbType = SqlDbType.Bit
        prmArray(1).Direction = ParameterDirection.Output


        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetDuplicateLogin", prmArray)
        Select Case prmArray(1).Value.ToString
            Case "1"
                Return True
            Case "0"
                Return False
        End Select
    End Function

End Class

