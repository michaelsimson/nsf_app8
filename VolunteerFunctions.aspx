<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VolunteerFunctions.aspx.cs" Inherits="Admin_VolunteerFunctions" Title="Volunteer Functions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />

        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>

        <script language="javascript" type="text/javascript">
      <!--
    function showhide(targetID) {
        var elementmode = document.getElementById(targetID).style;
        elementmode.display = (!elementmode.display) ? 'none' : '';
        document.getElementById('div11').style.display = 'none';
        if (targetID != 'div12')
            document.getElementById('div12').style.display = 'none';
        if (targetID != 'div13')
            document.getElementById('div13').style.display = 'none';
        if (targetID != 'div14')
            document.getElementById('div14').style.display = 'none';
        if (targetID != 'div15')
            document.getElementById('div15').style.display = 'none';
        if (targetID != 'div16')
            document.getElementById('div16').style.display = 'none';
        if (targetID != 'div17')
            document.getElementById('div17').style.display = 'none';
    }

    function validate() {
        var obj = document.getElementById("<%=ddlChapter.ClientID%>");
                if (obj.value == 'Select Chapter' || obj.value == '') {
                    alert('Please Select Chapter');
                    return false;
                }
            } function validateFundR() {
                var obj = document.getElementById("<%=ddlChapterFundR.ClientID%>");
                // alert(obj.value);
                if (obj.value == 'Select Chapter' || obj.value == '' || obj.value == '0') {
                    alert('Please Select Chapter');
                    return false;
                }
            }
            function validateRole() {

                var obj = document.getElementById("<%=ddlChapter.ClientID%>");

                if (obj.value == 'Select Chapter' || obj.value == '') {

                    alert('Please Select Chapter');

                    return false;
                }

            }
            function validateFacilities() {
                var obj = document.getElementById("<%=ddlChapter1.ClientID%>");
                if (obj.value == 'Select Chapter' || obj.value == '0') {
                    alert('Please Select Chapter');
                    return false;
                }
            }
            function validateHospitality() {
                var obj = document.getElementById("<%=ddlHospChapter.ClientID%>");
                 if (obj.value == 'Select Chapter' || obj.value == '0') {
                     alert('Please Select Chapter');
                     return false;
                 }
             }
            function validateWrkShop() {

                var obj = document.getElementById("<%=ddlChapterWkShop.ClientID%>");

                if (obj.value == 'Select Chapter' || obj.value == '0') {

                    alert('Please Select Chapter');

                    return false;
                }
            }
            function validatePrepClub() {

                var obj = document.getElementById("<%=ddlChapterPrepClub.ClientID%>");

                if (obj.value == 'Select Chapter' || obj.value == '0') {

                    alert('Please Select Chapter');

                    return false;
                }
            }
            function validateFinalCoordinator() {

                var obj = document.getElementById("<%=DdlRoleCategory1.ClientID%>");
                var obj1 = document.getElementById("<%=DDlRoleCode.ClientID%>");

                if (obj.value != 'Finals') {

                    alert('Please Select Role Category');
                    return false;
                }
                if (obj1.value == 'Select Role') {

                    alert('Please Select Role');

                    return false;
                }
            }


            function validateSelection() {
                var obj = document.getElementById("<%=DdlRole.ClientID%>");

                if (obj.value == 'Select Role') {
                    alert('Please Select Role');

                    return false;

                }
            }
            function validateZone() {

                var obj = document.getElementById("<%=DdlZonalCoordinator.ClientID%>")

                if (obj.value == '0' || obj.value == '') {

                    alert('Please Select Zone');

                    return false;
                }

            }
            function validateCluster() {

                var obj = document.getElementById("<%=DdlCluster.ClientID%>")

                if (obj.value == '0') {

                    alert('Please Select Cluster');

                    return false;
                }

            }
            $(function (e) {



                $("#ancCoachLevelDet").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {

                            var dvHtml = "";


                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/MathCountsLevelDetermination.aspx" target=_blank>Mathcounts - Level Determination</a></div> ';

                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/PreMathCountsLevelDetermination.aspx" target=_blank>Pre-Mathcounts Level Determination</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            return dvHtml;

                        },

                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">Level Determination</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })

                $("#anCoachFAQ").qtip({ // Grab some elements to apply the tooltip to
                    content: {

                        text: function (event, api) {

                            var dvHtml = "";


                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/mathCountsFAQ.aspx" target=_blank>Mathcounts FAQ</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/PremathCountsFAQ.aspx" target=_blank>Pre-Mathcounts FAQ</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/uscontests/coaching/SATACT_Coaching_FAQ.pdf" target=_blank>SAT Coaching FAQ</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/USContests/Coaching/Science.aspx" target=_blank>Science FAQ</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:#334499; cursor:pointer; " href="http://www.northsouth.org/public/uscontests/coaching/FAQ_Geography_Coaching.pdf" target=_blank>Geography FAQ</a></div> ';
                            dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';


                            return dvHtml;

                        },

                        title: function (event, api) {
                            return '<span style="font-weight:bold; font-size:14px;">FAQ</span>';
                        },
                        button: 'Close'
                    },
                    hide: {
                        event: false
                    },
                    style: {
                        classes: 'qtip-green qtip-shadow qTipWidth'

                    },
                    show: {
                        solo: true
                    }

                })
            });

    // -->
        </script>
    </div>

    <style type="text/css">
        .qTipWidth {
            max-width: 450px !important;
        }

        .blink {
            animation-duration: 700ms;
            animation-name: blink;
            animation-iteration-count: infinite;
            animation-direction: alternate;
            -webkit-animation: blink 700ms infinite; /* Safari and Chrome */
            font-size: 20px;
        }

        @keyframes blink {
            from {
                color: #46A71C;
            }

            to {
                color: red;
            }
        }

        @-webkit-keyframes blink {
            from {
                color: #46A71C;
            }

            to {
                color: white;
            }
        }

        .badge1 {
            position: relative;
        }

            .badge1[data-badge]:after {
                content: attr(data-badge);
                position: absolute;
                top: -10px;
                right: -10px;
                font-size: .7em;
                background: green;
                color: white;
                width: 18px;
                height: 18px;
                text-align: center;
                line-height: 18px;
                border-radius: 50%;
                box-shadow: 0 0 1px #333;
            }
    </style>

    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
   <asp:Label ID="lblMessage" runat="server" Visible="false"></asp:Label>
    <br />
    <div class="Heading">
        <table>
            <tr>
                <td class="title02">Volunteer Functions
                </td>
                <td></td>
                <td></td>
                <td>
                    <asp:DropDownList ID="DdlRole" runat="server" AppendDataBoundItems="True">
                        <asp:ListItem Value="Select Role">[Select Role]</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td></td>
                <td></td>
                <td>
                    <asp:Button ID="BtnContinue" runat="server" Height="23px" OnClick="BtnContinue_Click"
                        Text="Continue"
                        Visible="False" Width="72px" />
                </td>
                <td style="width: 300px;">&nbsp;</td>
                <td>

                    <asp:HyperLink CssClass="btn_02" Font-Size="Medium" ID="Hyperlnk209" runat="server" NavigateUrl="SupportTracking.aspx?NewTicket=true">File Support Ticket</asp:HyperLink>
                </td>
            </tr>
        </table>
    </div>
    <table id="table1" class="tableclass" runat="server" cellpadding="1" width="100%" border="2" style="border-right: #ffff99 thin solid; border-top: #ffff99 thin solid; border-left: #ffff99 thin solid; border-bottom: #ffff99 thin solid; background-color: #ffffcc;" language="javascript">
        <thead>
            <tr>
                <th style="height: 24px; width: 263px;">Application</th>
                <th style="height: 24px; width: 250px;">Main Option</th>
                <th style="height: 24px; width: 250px;">Reports</th>
                <th style="height: 24px; width: 250px;">Bulletin Board</th>
            </tr>
        </thead>
        <tr id="trAdmin" runat="server">
            <td class="title04" style="height: 140px; width: 263px;">&nbsp;Administration
            </td>
            <td style="height: 140px;">
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink2" NavigateUrl="~/AddUpdateRoles.aspx" Enabled="true" runat="server">Add/Update Roles</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink1" runat="server" NavigateUrl="VolunteerAssignRoles.aspx">Assign Roles to Volunteers</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink220" NavigateUrl="UpdateMissingPwd.aspx" Enabled="true" runat="server">Update Missing Passwords</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlinkSendEMails" Enabled="true" runat="server" NavigateUrl="~/email_NC.aspx">Send Email During Regionals</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink20" Enabled="true" runat="server" NavigateUrl="~/WebPageMgmtMain.aspx">Web Page Management</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkActList" runat="server" OnClick="lnkActList_Click">Activity List</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkSelMatrix" runat="server" OnClick="lnkSBVBSelMatrix_Click">SBVB Selection Criteria</asp:LinkButton>
                </p>
            </td>
            <td style="height: 140px">
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink57" runat="server" NavigateUrl="~/searchdonationreceipt.aspx"> Search Individual Donation Receipt </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink81" NavigateUrl="~/DonorList.aspx" Enabled="true" runat="server">Donor List</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink37" NavigateUrl="~/BulkMailPO.aspx" Enabled="true" runat="server">Bulk MailPO </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink84" NavigateUrl="~/Admin/SpellingBeeCriteria.aspx" Enabled="true" runat="server"> Generate Published SB Words</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink85" NavigateUrl="~/Admin/VocabBeeCriteria.aspx" Enabled="true" runat="server"> Generate Published VB Words </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink196" NavigateUrl="~/GenSBVBTestPapers.aspx" Enabled="true" runat="server"> Generate SBVB Paper Sets </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink27" Enabled="true" runat="server" NavigateUrl="~/SendEmailLog.aspx">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HlFocusGroup" Enabled="true" runat="server" NavigateUrl="~/FocusGroup.aspx">Focus Group Report</asp:HyperLink>
                </p>
            </td>
            <td style="height: 140px;"></td>
        </tr>
        <tr id="trNationalCoordinator" runat="server">
            <td class="title04" style="height: 470px; width: 263px;">National Coordinator&nbsp;&nbsp;&nbsp;
            </td>
            <td style="height: 470px;">
                <span class="announcement_text">Regionals</span>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink7" Enabled="True" NavigateUrl="AddUpEvent.aspx" runat="server">Add/Update Events</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink8" Enabled="True" NavigateUrl="UpdateWeekCalendar.aspx" runat="server">Update Dates for Contests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink9" Enabled="True" NavigateUrl="AddUpdProdGrp.aspx" runat="server">Add/Update Product Groups</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink10" Enabled="True" NavigateUrl="AddUpdProd.aspx" runat="server">Add/Update Products</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlAddUpdProdLevel" Enabled="True" NavigateUrl="~/AddUpdProdLevel.aspx" runat="server">Add/Update Product Level</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink11" Enabled="True" NavigateUrl="AddUpdZone.aspx" runat="server">Add/Update Zones</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink12" Enabled="True" NavigateUrl="AddUpdCluster.aspx" runat="server">Add/Update Clusters</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink13" Enabled="True" NavigateUrl="AddUpdChapter.aspx" runat="server">Add/Update Chapters</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink204" Enabled="True" NavigateUrl="ContestSettings.aspx" runat="server">Contest Settings</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink82" NavigateUrl="UpdateContestCategory.aspx" runat="server">Add Update ContestCategory</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlnkuploadtstpapers" NavigateUrl="ManageTestPapers.aspx" runat="server">Upload Test Papers</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink177" NavigateUrl="MaxScoresByPhase.aspx" runat="server">Max Scores By Phase</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlinkSendEMailsNC" Enabled="true" runat="server" NavigateUrl="~/email_NC.aspx">Send Email</asp:HyperLink>
                </p>
                <span class="announcement_text">Finals</span>
                <p>
                    <asp:LinkButton class="btn_02" ID="lnkPrepareFinals" Enabled="false" OnClick="lnkPrepareFinals_Click" runat="server">Prepare National Finals Calendar</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="hlinkSelectionCriteria" Enabled="true" runat="server" NavigateUrl="Reports/NationalCriteria.aspx">Select Criteria for National Invites</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="hlinkSendEmailInvite" Enabled="true" runat="server" NavigateUrl="Email_NFInvites.aspx">Send Email Invite to National Finals </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlinkManageContestantPhotos" NavigateUrl="UploadPhotos.aspx?managephotos=1" runat="server" Visible="False">Manage Contestant Photos</asp:HyperLink>
                </p>
                <span class="announcement_text">other</span>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink89" NavigateUrl="ManageScoresheet.aspx" runat="server"> Download/Upload Scoresheets</asp:HyperLink>
                </p>

                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink194" NavigateUrl="ReplicateMasterScoreSheets.aspx" runat="server">Replicate Master Score Sheets</asp:HyperLink>
                </p>
                <p style="display: none;">
                    <asp:LinkButton class="btn_02" ID="LinkButton1" runat="server" OnClick="LinkButton2_Click" Enabled="false">Send Email</asp:LinkButton>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink305" NavigateUrl="~/CreateZoomSessions.aspx" runat="server"> Manage Zoom Sessions (<=100 attendees)</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton22" OnClick="lnkSetupWebinar_Click" runat="server">Setup Webinar (>100 attendees)</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink14" NavigateUrl="volunteerAssignRoles.aspx" runat="server">Assign Roles to Volunteers</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink36" NavigateUrl="AddUpdateEventFees.aspx" runat="server">Add Update EventFees</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink132" NavigateUrl="Don_athon_schedule.aspx" runat="server"> Schedule Walkathon/Marathon</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton8" runat="server" OnClick="lnkActList_Click">Activity List</asp:LinkButton>
                </p>
                 <p class="btn_02">
                    <asp:HyperLink ID="hlDonVolAwards" CssClass="btn_02" runat="server" NavigateUrl="~/DonVolAwards.aspx" Enabled="false">Donor & Volunteer Awards</asp:HyperLink>
                </p>
            </td>
            <td style="height: 470px">
                <span class="announcement_text">Regionals</span>
                <%-- <p class="btn_02"><asp:hyperlink class="btn_02" id="hlinkWalkathon" runat="server" Enabled="false" NavigateUrl="Don_athon_selection.aspx?Ev=5">Register for Walk-a-thon</asp:hyperlink></p>--%>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink78" NavigateUrl="~/ContPerfRecord.aspx" Enabled="true" runat="server">Performance Record of Children</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink4" NavigateUrl="~/Reports/PercentilesByGrade.aspx?EventID=2" Enabled="true" runat="server">Percentiles for Regionals</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink216" NavigateUrl="~/Reports/DataAnalytics.aspx" Enabled="true" runat="server">Data Analytics</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink212" NavigateUrl="~/MissingScores.aspx" Enabled="true" runat="server">Missing Scores</asp:HyperLink>
                </p>
                <span class="announcement_text">Finals</span>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink65" NavigateUrl="~/Reports/MealsReport.aspx" Enabled="true" runat="server">View Meals Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink68" NavigateUrl="~/Reports/MealsStatistics.aspx" Enabled="true" runat="server">View Meals Statistics</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink167" NavigateUrl="~/Reports/MealsReportByContestant.aspx" Enabled="true" runat="server">View Meals Report By Contestant</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink69" NavigateUrl="~/Reports/StatisticsOnInvitees.aspx" Enabled="true" runat="server">Priority and Wait List Response</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink67" NavigateUrl="~/Reports/FinalsRegReport.aspx" Enabled="true" runat="server">View Finals Registration Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink73" NavigateUrl="~/Reports/FamilyCheckList.aspx" Enabled="true" runat="server">View Family Check List for the Finals</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink74" NavigateUrl="~/Reports/xlReport3.aspx?Chap=1" Enabled="true" runat="server">Tight Schedule Alert</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink140" NavigateUrl="~/Reports/InviteeDeclineReport.aspx" Enabled="true" runat="server">View Invitee Decline Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink149" NavigateUrl="~/Reports/PercentilesByGrade.aspx?EventID=1" Enabled="true" runat="server">Percentiles for Finals</asp:HyperLink>
                </p>
                <p class="btn_02">&nbsp;</p>
                <span class="announcement_text">Other</span>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink26" NavigateUrl="ViewContactList.aspx?Source=NC" Enabled="true" runat="server">View Contact List</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink52" NavigateUrl="ContactListUnassigned.aspx" Enabled="true" runat="server">View Unassigned Contact List</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink28" Enabled="true" runat="server" NavigateUrl="~/SendEmailLog.aspx">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink307" Enabled="true" runat="server" NavigateUrl="~/ZoomSessionsUsageRepor.aspx">Zoom Usage Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink222" runat="server" NavigateUrl="SupportTracking.aspx"> Support Ticket Tracking</asp:HyperLink>
                </p>
            </td>
            <td style="height: 470px;"></td>
        </tr>

        <tr id="trContestMGT" runat="server" visible="false">
            <td class="title04" style="height: 16px; width: 263px;">Contest Management<br />
            </td>
            <td style="height: 16px; vertical-align: top;">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink251" Enabled="True" NavigateUrl="AddUpEvent.aspx" runat="server">Add/Update Events</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink252" Enabled="True" NavigateUrl="UpdateWeekCalendar.aspx" runat="server">Update Dates for Contests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink253" NavigateUrl="UpdateContestCategory.aspx" runat="server">Add Update ContestCategory</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink254" Enabled="True" NavigateUrl="ContestSettings.aspx" runat="server">Contest Settings</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink255" NavigateUrl="ReplicateMasterScoreSheets.aspx" runat="server">Replicate Master Score Sheets</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton21" runat="server" OnClick="lnkSBVBSelMatrix_Click">SBVB Selection Criteria</asp:LinkButton>
                </p>
            </td>
            <td style="height: 16px; vertical-align: top;">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink256" NavigateUrl="~/Admin/SpellingBeeCriteria.aspx" Enabled="true" runat="server"> Generate Published SB Words</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink257" NavigateUrl="~/Admin/VocabBeeCriteria.aspx" Enabled="true" runat="server"> Generate Published VB Words </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink258" NavigateUrl="~/GenSBVBTestPapers.aspx" Enabled="true" runat="server"> Generate SBVB Paper Sets </asp:HyperLink>
                </p>
            </td>
            <td style="height: 16px">

                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink260" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/ChapterGuide.doc">Chapter Guide</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink261" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF_2016_Flyer.docx">Sample Contest Flyer</asp:HyperLink>
                </p>


                <p>
                    <asp:HyperLink ID="HyperLink262" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divBPCMGNT');">Best Practices</asp:HyperLink>
                </p>
                <div id="divBPCMGNT" style="display: none;">

                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink263" runat="server" Target="_blank" NavigateUrl="https://attendee.gotowebinar.com/recording/9066994375605624067">Part 1</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink264" runat="server" Target="_blank" NavigateUrl="https://attendee.gotowebinar.com/recording/5999509766227066627">Part 2</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink265" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSFCC-FAQ.pdf">FAQ</asp:HyperLink>
                    </p>
                </div>

                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink266" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/InsuranceInfo.docx">Insurance Info</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="Hyperlink267" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divContestGuide');">Useful Info on Contests/Workshops</asp:HyperLink>
                </p>
                <div id="divContestGuide" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink268" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSFContestGuide_Dec2010.doc">Chapter Contest Guide</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink269" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/Workshop_Policies_Final.doc">Workshop Policy</asp:HyperLink>
                        &nbsp;
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink270" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/Contest-Checklist_NSF_Oct2010_rev1.xls">Contest Checklist</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink271" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/FeedbackQuestionnaire.doc">Feedback Questionnaire</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink272" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divVolRol');">Step-by-step Instructions for CC</asp:HyperLink>
                </p>
                <div id="divVolRol" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink273" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/" Enabled="false">Assigning Volunteer Roles</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink274" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Adding_venue_sponsor_instructions_December8_10.doc">Adding Venue/Sponsor</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink275" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/setting_up_contest_calendar_March18_11.doc">Setting up Contest Calendar </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink276" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/sending_Email_March18_11.doc">Sending Email to Parents</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink277" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/SchedulingTechCoord_Instructions_February6_11.doc">Scheduling TechCs to the Contests</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink278" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/" Enabled="false">Generate Badge Numbers</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink279" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Instructions_Printing_Badges.doc">Printing Badges </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink280" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Instructions_Printing_Certificates.doc">Printing Certificates </asp:HyperLink>
                    </p>

                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink281" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Downloading_contest_material_by_tech_Coord_March18_11.doc">Download Contest Material by TechC (TC) </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink282" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Downloading_and_Uploading_Scoresheets_by_TCs.March18_11.doc">Download and Upload Score Sheets by TCs </asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink283" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divInsTec');">Instructions for Technical Team</asp:HyperLink>
                </p>
                <div id="divInsTec" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink284" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Spelling.zip">Spelling</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink285" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Vocabulary.zip">Vocabulary</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink286" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Math.zip">Math</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink287" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Science.zip">Science</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink288" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Geography.zip">Geography</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink289" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Essay.zip">Essay Writing</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink290" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/publicspeaking.zip">Public Speaking</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink291" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divFundr');">Fundraising</asp:HyperLink>
                </p>
                <div id="divFundr" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink292" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/DAS_PledgeSheet_no_year.pdf">DAS Information</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink293" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divOffNSFDoc');">Official NSF documents</asp:HyperLink>
                </p>
                <div id="divOffNSFDoc" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink294" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Letterhead_NSF.doc">Letterhead</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink295" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF_Logo_Final.jpg">Logo</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink296" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Sample1_Receipt.doc">Receipt</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink297" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/IRSApprovalLetter_501(c)(3).doc">NSF Non-profit/501(c)(3) Letter</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink298" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF-Consent-Form-revised.doc">Parent Consent Form</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink299" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divMisc');">Miscellaneous</asp:HyperLink>
                </p>
                <div id="divMisc" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
               <asp:HyperLink class="btn_03" ID="Hyperlink301" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Chapter_Coordinator_Qualification.doc">Basic Qualifications of a CC</asp:HyperLink>
                    </p>
                </div>


            </td>
        </tr>
        <tr id="TrIndiaScholarships" runat="server" visible="false">
            <td class="title04" style="height: 16px; width: 263px;">India Scholarships<br />
            </td>
            <td style="height: 16px">
                <p class="btn_02">
                    <asp:HyperLink class="btn_02" ID="HyperLink3" NavigateUrl="IndiaScholarships.aspx" Enabled="true" runat="server">Scholarship Designations</asp:HyperLink>
                </p>
            </td>
            <td style="height: 16px">
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink259" runat="server" NavigateUrl="~/DesignatedScholarshipRecipient.aspx">Designated Scholarship Recipients</asp:HyperLink>
                </p>
            </td>
            <td style="height: 16px"></td>
        </tr>
        <tr id="trZonalCoordinator" runat="server">
            <td class="title04" style="height: 61px; width: 263px;">Regional Director<br />
                <asp:ObjectDataSource ID="ZoneDS" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
                <br />
                <asp:DropDownList ID="DdlZonalCoordinator" runat="server"
                    Style="z-index: 100; position: absolute;" Width="160px" AppendDataBoundItems="True" OnSelectedIndexChanged="DdlZonalCoordinator_SelectedIndexChanged" AutoPostBack="True">
                    <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <br />
                <p class="btn_02"></p>
                <asp:DataList ID="List2" DataSourceID="ZoneDescDS" runat="server" OnSelectedIndexChanged="List2_SelectedIndexChanged">
                    <ItemTemplate>
                        <asp:TextBox ID="TextBox1" Text='<%#Eval("Description") %>' runat="server"></asp:TextBox>
                    </ItemTemplate>
                </asp:DataList>
            </td>
            <td style="height: 61px;">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkAssignRolesZonal" runat="server" OnClick="lnkAssignRolesZonal_Click">Assign Roles to Volunteers</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkEmail3" runat="server" OnClick="lnkEmail3_Click1">Send Email</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkMissingScoreZonal" Enabled="true" runat="server" OnClick="lnkMissingScoresZonal_Click">Missing Scores</asp:LinkButton>
                </p>
            </td>
            <td style="height: 61px">
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton4" runat="server" OnClick="lnkAssignRolesZonal_Click">Assign Roles to Volunteers</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContactlst" runat="server" OnClick="lnkContactlst_Click">View Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkUnContactlst" runat="server" OnClick="lnkUnassignlnk_Click">View Unassigned Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkZoneDonorList" runat="server" OnClick="lnkZoneDonorList_Click">Donor List</asp:LinkButton>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink35" NavigateUrl="#" Enabled="false" runat="server"> Reports</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink29" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="False">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton11" runat="server" OnClick="LinkButton11_Click">Send Email Log</asp:LinkButton>
                </p>
            </td>
            <td style="height: 61px;"></td>
        </tr>
        <tr id="trClusterCoordinator" runat="server">
            <td class="title04" style="height: 109px; width: 263px;">Cluster Coordinator<br />
                <asp:ObjectDataSource ID="ClusterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                    SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
                <br />
                <asp:DropDownList ID="DdlCluster" runat="server" Style="z-index: 101; position: absolute;"
                    Width="162px" OnSelectedIndexChanged="DropDownList1_SelectedIndexChanged" AppendDataBoundItems="True" AutoPostBack="True">
                    <asp:ListItem Value="0">[Select Cluster]</asp:ListItem>
                </asp:DropDownList>
                <br />
                <br />
                <asp:DataList ID="List1" DataSourceID="ClusterDescDS" runat="server">
                    <ItemTemplate>
                        <%--  <asp:TextBox ID="TextBox1" runat="server" Text='<%#Eval("Description") %>' Width="154px"></asp:TextBox>
                        --%>
                        <%# Eval("Description") %>
                    </ItemTemplate>
                </asp:DataList>
            </td>
            <td style="height: 109px;">
                <p class="btn_02"></p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkAssignRolesCluster" runat="server" OnClick="lnkAssignRolesCluster_Click">Assign Roles to Volunteers</asp:LinkButton>
                </p>
                <p class="btn_02">

                    <asp:LinkButton ID="lnkEmail4" runat="server" OnClick="lnkEmail4_Click">Send Email</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkMissingScorecluster" Enabled="true" runat="server" OnClick="lnkMissingScoreCluster_Click">Missing Scores</asp:LinkButton>
                </p>
            </td>
            <td style="height: 109px">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContactlist1" runat="server" OnClick="lnkContactlst1_Click">View Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkUnassignedCntLst1" runat="server" OnClick="lnkUnassignedCntLst1_Click">View Unassigned Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkClustDonorlist" runat="server" OnClick="lnkClustDonorlist_Click">Donor List</asp:LinkButton>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="HyperLink49" Enabled="false" NavigateUrl="#" runat="server">Reports </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink30" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="False">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton12" runat="server" OnClick="LinkButton12_Click">Send Email Log</asp:LinkButton>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trChapterCoordinator" runat="server" align="left">
            <td class="title04" style="height: 295px; width: 263px;">Chapter Director&nbsp;<br />
                <table style="z-index: 100; left: 18px;">
                    <tr>
                        <td style="width: 221px">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 221px; height: 22px;">&nbsp;Select a Chapter
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 221px; height: 65px;">
                            <asp:DropDownList ID="ddlChapter" runat="server" AppendDataBoundItems="True">
                                <asp:ListItem>Select Chapter</asp:ListItem>
                            </asp:DropDownList>
                            <asp:ObjectDataSource ID="ChapterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                                SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter"></asp:ObjectDataSource>
                        </td>
                    </tr>
                </table>
                &nbsp;
            <asp:Label ID="lblChapterError" runat="server" Text="Label"></asp:Label>
                <asp:ObjectDataSource ID="ZoneDescDS" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GetZoneDescriptionTableAdapters.ZoneTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ZoneId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ZoneCode" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Original_ZoneId" Type="Int32" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:Parameter Name="ZoneCode" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                    </InsertParameters>
                </asp:ObjectDataSource>
                <asp:ObjectDataSource ID="ChaptersinZoneDS" runat="server" DeleteMethod="Delete"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GetChaptersinZoneTableAdapters.ChapterTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ChapterID" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="Name" Type="String" />
                        <asp:Parameter Name="ChapterCode" Type="String" />
                        <asp:Parameter Name="ZoneId" Type="Int32" />
                        <asp:Parameter Name="Original_ChapterID" Type="Int32" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:ObjectDataSource>
            </td>
            <td style="height: 295px;">
                <p class="btn_02"></p>
                <span class="announcement_text">Contests</span>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContestCalender" runat="server" OnClick="LinkButton1_Click">Prepare Contest Calendar</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContestTiming" runat="server" OnClick="lnkContestTiming_Click">Contest Timings</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContestSchedule" runat="server" OnClick="lnkContestSchedule_Click">Contest Day Schedule</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnExp" runat="server" OnClick="lbtnExp_Click">Exception List/Contest Registration</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnSchedule" runat="server" Enabled="true" OnClick="lbtnSchedule_Click">Schedule Technical Coordinators</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkUploadSign" runat="server" Enabled="true" OnClick="lnkUploadSign_Click">Upload Signature</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkMissingScoresChapter" Enabled="true" runat="server" OnClick="lnkMissingScores_Click_chapter">Missing Scores</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkEthnicOrg" Enabled="true" runat="server" OnClick="lnkEthnicOrg_Click_chapter">Ethnic Organization</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink234" runat="server" NavigateUrl="~/ReferralProgram.aspx">Referral Program</asp:HyperLink>
                </p>
                 <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink309" runat="server" NavigateUrl="ApproveDuplicateRegistration.aspx">Approve Duplicates in Contests</asp:HyperLink>
                </p>
                <asp:ValidationSummary ID="vs_Details" runat="server" Font-Names="Century"
                    ShowMessageBox="True" ShowSummary="False" ValidationGroup="vg1" />
                <br />
                <div id="div1" style="display: none;">
                    <p class="btn_02">
                        <asp:HyperLink ID="lnkRentalNeeds1" runat="server" NavigateUrl="~/Facility.aspx">Prepare Facility Resources</asp:HyperLink>
                    </p>
                    <!-- End of Contest Schedules (hyderabad team) -->
                    <p>
                        <asp:HyperLink class="btn_02" ID="Hyperlink40" Enabled="false" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">View Registration Data</asp:HyperLink>
                        &nbsp;
                    </p>
                    <p>
                        <asp:HyperLink class="btn_02" ID="Hyperlink41" Enabled="false" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Generate Badge Numbers</asp:HyperLink>
                    </p>
                    <p>
                        <asp:HyperLink class="btn_02" ID="Hyperlink42" Enabled="false" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Download Registration Data</asp:HyperLink>
                    </p>
                    <p>
                        <asp:HyperLink class="btn_02" ID="Hyperlink43" Enabled="false" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">Make a Donation</asp:HyperLink>
                    </p>
                    <p>
                        <asp:HyperLink class="btn_02" ID="Hyperlink44" Enabled="false" runat="server" NavigateUrl="~/ChapterCoordinator/Test.aspx">View Donation History</asp:HyperLink>
                    </p>
                </div>
                <span class="announcement_text">Other</span>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkAssignRoles" runat="server" OnClick="lnkAssignRoles_Click">Assign Roles to Volunteers</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkEmail5" runat="server" OnClick="lnkEmail5_Click">Send Email</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkfreeEvent" runat="server" OnClick="lnkfreeEvent_Click">Set up an Event</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkChWebPageMgmt" runat="server" OnClick="lnkChWebPageMgmt_Click">Web Page Management</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkFundRaisingCal" runat="server" OnClick="lnkFundRaisingCal_Click">Fund Raising Calendar</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton9" runat="server" OnClick="lnkActList_Click">Activity List</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkInventory" runat="server" OnClick="lnkInventory_Click">Add/Update Inventory</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkManageBlockList" runat="server" OnClick="lnkManageBlockList_Click">Manage Block List</asp:LinkButton>
                </p>
            </td>
            <td style="height: 295px">
                <p class="btn_02"></p>
                <span class="announcement_text">Contests</span>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkGenerateBadgeNumber" runat="server" Enabled="true" OnClick="lnkGenerateBadgeNumber_Click">Generate Badge Numbers </asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="Hyperlink58" runat="server" Enabled="true" OnClick="Hyperlink58_Click">Generate Badges</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="HyperLink59" runat="server" Enabled="true" OnClick="HyperLink59_Click">Generate Certificates</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkViewRegistrations" runat="server" OnClick="LinkViewRegistrations_Click">View Contest Registrations</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkTopRanksList" runat="server" OnClick="LinkTopRanksList_Click">Top Ranks List at Regionals</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkTopRanksListFinals" runat="server" OnClick="LinkTopRanksListFinals_Click">Top Ranks List at Finals</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkBtnFinalists" runat="server" OnClick="lnkListOfFinalists_Click">List of Finalists</asp:LinkButton>
                    &nbsp;
                </p>
                <div id="divCCFinals" runat="server" visible="false">
                    <span class="announcement_text">Finals</span>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink241" NavigateUrl="~/Reports/MealsReport.aspx" Enabled="true" runat="server">View Meals Report</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink242" NavigateUrl="~/Reports/MealsStatistics.aspx" Enabled="true" runat="server">View Meals Statistics</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink243" NavigateUrl="~/Reports/MealsReportByContestant.aspx" Enabled="true" runat="server">View Meals Report By Contestant</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink244" NavigateUrl="~/Reports/StatisticsOnInvitees.aspx" Enabled="true" runat="server">Priority and Wait List Response</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink245" NavigateUrl="~/Reports/FinalsRegReport.aspx" Enabled="true" runat="server">View Finals Registration Report</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink246" NavigateUrl="~/Reports/FamilyCheckList.aspx" Enabled="true" runat="server">View Family Check List for the Finals</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink247" NavigateUrl="~/Reports/xlReport3.aspx?Chap=1" Enabled="true" runat="server">Tight Schedule Alert</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink248" NavigateUrl="~/Reports/InviteeDeclineReport.aspx" Enabled="true" runat="server">View Invitee Decline Report</asp:HyperLink>
                    </p>
                    <p class="btn_02">
                        <asp:HyperLink ID="HyperLink249" NavigateUrl="~/Reports/PercentilesByGrade.aspx?EventID=1" Enabled="true" runat="server">Percentiles for Finals</asp:HyperLink>
                    </p>
                </div>
                <span class="announcement_text">Other</span>
                <p class="btn_02">
                    <asp:LinkButton ID="lnk_VolSignupReport" runat="server" OnClick="AssignPanelName" Visible="false">Volunteer Signup Report</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContactlst2" runat="server" OnClick="lnkContactlst1_Click">View Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkUnassignedCntLst2" runat="server" OnClick="lnkUnassignedCntLst1_Click">View Unassigned Contact List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkChDonorList" runat="server" OnClick="lnkChDonorList_Click">Donor List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkMedalList" runat="server" OnClick="lnkMedalList_Click">View Counts on Medals/Trophies</asp:LinkButton>
                    &nbsp;
                </p>

                <p class="btn_02">
                    <asp:LinkButton ID="lnkRefProgReport" runat="server" OnClick="lnkRefProgReport_Click">Referral Program Report</asp:LinkButton>
                    &nbsp;
                </p>

                <p class="btn_02">
                    <asp:LinkButton ID="lnkSuppressionList" runat="server" OnClick="lnkSupList_Click">Suppression List</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlFreeEventReport" runat="server" NavigateUrl="~/FreeEvent/FreeEventReport.aspx" Visible="true">Event Report</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink31" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="False">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton13" runat="server" OnClick="LinkButton13_Click">Send Email Log</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkbtnSupportticket" runat="server"
                        OnClick="lnkbtnSupportticket_Click">Support Ticket Tracking</asp:LinkButton>
                </p>

            </td>
            <td style="height: 295px">
                <p class="btn_02"></p>
                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink156" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/ChapterGuide.doc">Chapter Guide</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink302" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF_MetricsforChapterDirectors.rtf">Chapter Directors Metrics</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink235" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF_2016_Flyer.docx">Sample Contest Flyer</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlBestPractises" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('divBP');">Best Practices</asp:HyperLink>
                </p>
                <div id="divBP" style="display: none;">

                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink236" runat="server" Target="_blank" NavigateUrl="https://attendee.gotowebinar.com/recording/9066994375605624067">Part 1</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink237" runat="server" Target="_blank" NavigateUrl="https://attendee.gotowebinar.com/recording/5999509766227066627">Part 2</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink238" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSFCC-FAQ.pdf">FAQ</asp:HyperLink>
                    </p>
                </div>

                <p>
                    <asp:HyperLink class="btn_02" ID="Hyperlink158" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/InsuranceInfo.docx">Insurance Info</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="Hyperlink88" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div17');">Useful Info on Contests/Workshops</asp:HyperLink>
                </p>
                <div id="div17" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink95" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSFContestGuide_Dec2010.doc">Chapter Contest Guide</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink96" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/Workshop_Policies_Final.doc">Workshop Policy</asp:HyperLink>
                        &nbsp;
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink97" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/Contest-Checklist_NSF_Oct2010_rev1.xls">Contest Checklist</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink98" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/FeedbackQuestionnaire.doc">Feedback Questionnaire</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink90" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div12');">Step-by-step Instructions for Chapter Director</asp:HyperLink>
                </p>
                <div id="div12" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink111" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/" Enabled="false">Assigning Volunteer Roles</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink114" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Adding_venue_sponsor_instructions_December8_10.doc">Adding Venue/Sponsor</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink115" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/setting_up_contest_calendar_March18_11.doc">Setting up Contest Calendar </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink116" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/sending_Email_March18_11.doc">Sending Email to Parents</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink117" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/SchedulingTechCoord_Instructions_February6_11.doc">Scheduling TechCs to the Contests</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink118" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/" Enabled="false">Generate Badge Numbers</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink119" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Instructions_Printing_Badges.doc">Printing Badges </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink120" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Instructions_Printing_Certificates.doc">Printing Certificates </asp:HyperLink>
                    </p>
                    <%--OnClick="lbtnAppSATAdminAnswerKeys_Click"--%>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink121" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Downloading_contest_material_by_tech_Coord_March18_11.doc">Download Contest Material by TechC (TC) </asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink122" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Downloading_and_Uploading_Scoresheets_by_TCs.March18_11.doc">Download and Upload Score Sheets by TCs </asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink91" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div13');">Instructions for Technical Team</asp:HyperLink>
                </p>
                <div id="div13" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink106" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Spelling.zip">Spelling</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink107" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Vocabulary.zip">Vocabulary</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink108" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Math.zip">Math</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink113" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Science.zip">Science</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink109" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Geography.zip">Geography</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink110" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Essay.zip">Essay Writing</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink112" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/publicspeaking.zip">Public Speaking</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink92" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div14');">Fundraising</asp:HyperLink>
                </p>
                <div id="div14" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink100" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/DAS_PledgeSheet_no_year.pdf">DAS Information</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink93" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div15');">Official NSF documents</asp:HyperLink>
                </p>
                <div id="div15" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink101" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Letterhead_NSF.doc">Letterhead</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink102" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF_Logo_Final.jpg">Logo</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink103" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Sample1_Receipt.doc">Receipt</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink104" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/IRSApprovalLetter_501(c)(3).doc">NSF Non-profit/501(c)(3) Letter</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink105" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NSF-Consent-Form-revised.doc">Parent Consent Form</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="hlNSFDisCard" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/NorthSouthFoundation_SPC_OfficeDepot.pdf">Office Depot Discount Card</asp:HyperLink>
                    </p>
                </div>
                <p>
                    <asp:HyperLink ID="Hyperlink94" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div16');">Miscellaneous</asp:HyperLink>
                </p>
                <div id="div16" style="display: none;">
                    &nbsp;&nbsp;&nbsp;
               <asp:HyperLink class="btn_03" ID="Hyperlink99" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Chapter_Coordinator_Qualification.doc">Basic Qualifications of a CC</asp:HyperLink>
                    </p>
                </div>
            </td>
        </tr>
        <tr id="trWrkShop" runat="server" visible="False">
            <td class="title04" style="height: 150px; width: 263px;">Workshop Coordinator&nbsp;
            <table style="z-index: 100; left: 18px;">
                <tr>
                    <td style="width: 221px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 22px;">&nbsp;Select a Chapter</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 65px;">
                        <asp:DropDownList ID="ddlChapterWkShop" runat="server" DataTextField="Chapter" DataValueField="ChapterID" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlChapterWkShop_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Chapter</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSource2" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter"></asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
            </td>
            <td style="height: 150px;" id="Td1">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkWorkShopCal" runat="server" Enabled="true" OnClick="lnkWorkShopCal_Click">WorkShop Calendar</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkWkShopEmail" runat="server" OnClick="lnkWkShopEmail_Click">Send Email</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkViewWorkshopRegistrations" runat="server" OnClick="LinkViewWorkshopRegistrations_Click">View Workshop Registrations</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink206" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="True">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkbtnWorkShopSupporttracking" runat="server"
                        OnClick="lnkbtnWorkShopSupporttracking_Click">Support Ticket Tracking</asp:LinkButton>
                    &nbsp;
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trPrepClub" runat="server" visible="False">
            <td class="title04" style="height: 150px; width: 263px;">PrepClub Coordinator&nbsp;
            <table style="z-index: 100; left: 18px;">
                <tr>
                    <td style="width: 221px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 22px;">&nbsp;Select a Chapter</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 65px;">
                        <asp:DropDownList ID="ddlChapterPrepClub" runat="server" DataTextField="Chapter" DataValueField="ChapterID" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlChapterPrepClub_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Chapter</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSource3" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter"></asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
            </td>
            <td style="height: 150px;" id="Td2">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkPrepClub" runat="server" Enabled="true" OnClick="lnkPrepClub_Click">Prep Club Calendar</asp:LinkButton>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink5" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeSubject.aspx">Change Subject</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkPrepClubUpdate" runat="server" Text="Update Child Registration Data" PostBackUrl="PrepClubRegistrationUpdate.aspx"></asp:LinkButton>
                </p>

                <p class="btn_02">
                    <asp:LinkButton ID="lnkPrepClubEmail" runat="server" OnClick="lnkPrepClubEmail_Click">Send Email</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink197" NavigateUrl="VolunteerSignupReport.aspx" runat="server" Visible="false">Volunteer Signup Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkViewPrepClub" runat="server" OnClick="LinkViewPrepClub_Click">View Prep Club Registrations</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink207" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="True">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkbtnPreclubSupporttracking" runat="server"
                        OnClick="lnkbtnPreclubSupporttracking_Click">Support Ticket Tracking</asp:LinkButton>
                    &nbsp;
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trOnlineWScal" runat="server" visible="false">
            <td class="title04" style="height: auto; width: 263px;">Online Workshop Coordinator&nbsp;
            </td>
            <td style="height: auto;" id="Td3">
                <p class="btn_02">
                    <asp:LinkButton ID="lnlAssignTeacher" runat="server" Text="Assign Teachers" PostBackUrl="AssignTeachers.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkOnlineWkShop" runat="server" OnClick="lnkOnlineWkShop_Click" Enabled="true">Online Workshop Calendar</asp:LinkButton>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink189" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeSubjectDate_OWKShop.aspx">Change Subject/Date</asp:HyperLink>
                    &nbsp;
                </p>

                <p class="btn_02">
                    <asp:LinkButton ID="lnkOnlineWkShopEmail" OnClick="lnkOnlineWkShopEmail_Click" runat="server">Send Email</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkSetupWebinar" OnClick="lnkSetupWebinar_Click" runat="server">Setup Webinar</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink198" NavigateUrl="VolunteerSignupReport.aspx" runat="server" Visible="false">Volunteer Signup Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkViewOnlineWkShop" OnClick="LinkViewOnlineWkShop_Click" runat="server">View Online Workshop Registrations</asp:LinkButton>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlViewRegCount" runat="server" NavigateUrl="Reports/OWkShopRegCounts.aspx" Visible="True">View Registration Counts</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink208" runat="server" NavigateUrl="~/SendEmailLog.aspx" Visible="True">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk204" runat="server" NavigateUrl="~/SupportTracking.aspx?NSFEvnt=20">Support Ticket Tracking</asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trRentalNeeds" runat="server" visible="False">
            <td class="title04" style="height: 150px; width: 263px;">Event Planning&nbsp;
            <table style="z-index: 100; left: 18px;">
                <tr>
                    <td style="width: 221px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 22px;">Select a Chapter</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 65px;">
                        <asp:DropDownList ID="ddlChapter1" runat="server" DataTextField="Chapter" DataValueField="ChapterID" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlChapter1_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Chapter</asp:ListItem>
                        </asp:DropDownList>
                        <asp:ObjectDataSource ID="ObjectDataSource1" runat="server" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter"></asp:ObjectDataSource>
                    </td>
                </tr>
            </table>
                &nbsp;
            <asp:Label ID="Label2" runat="server" Text="" Visible="False"></asp:Label>
            </td>
            <td style="height: 150px;" id="Facilities">
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton14" Style="cursor: pointer;" class="btn_02" runat="server" OnClick="LinkButton14_Click"> Set Up Teams </asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkTeamPlanning" Style="cursor: pointer;" class="btn_02" runat="server" OnClick="lnkTeamPlanning_Click"> Team Planning </asp:LinkButton>
                </p>
                <span class="announcement_text">Contest Planning</span>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRoomRequirement" class="btn_02" runat="server" Enabled="false" PostBackUrl="RoomRequirement.aspx">Room Requriements</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRoomList" class="btn_02" runat="server" Enabled="false" PostBackUrl="RoomList.aspx"> Room List </asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRentalNeeds" class="btn_02" runat="server" Enabled="false" PostBackUrl="RentalNeeds.aspx"> RentalNeeds </asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRoomSchedule" class="btn_02" runat="server" Enabled="false" PostBackUrl="RoomSchedule.aspx"> Room Schedule</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkAssignRolesFacilities" class="btn_02" runat="server" Enabled="false" OnClick="lnkAssignRolesFacilities_Click">Assign Roles to Volunteers</asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContestTeam" class="btn_02" runat="server" PostBackUrl="ContestTeam.aspx"> Contest Team</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRoomGuides" class="btn_02" runat="server" PostBackUrl="RoomGuideSchedule.aspx"> Room Guides</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkGraders" class="btn_02" runat="server" PostBackUrl="GraderSchedule.aspx"> Graders</asp:LinkButton>
                </p>
            </td>
            <td style="height: 150px;">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink199" NavigateUrl="VolunteerSignupReport.aspx" runat="server" Visible="false">Volunteer Signup Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkViewContestTeamSch" class="btn_02" runat="server" PostBackUrl="ViewContestTeamSchedule.aspx"> View Contest Team Schedule</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkEventContestDaySchedule" runat="server" OnClick="lnkEventContestSchedule_Click">Contest Day Schedule</asp:LinkButton>
                </p>
            </td>
            <td style="height: 150px;"></td>
        </tr>
         <tr id="trHospEvent" runat="server" visible="False">
            <td class="title04" style="width: 263px">Hospitality Event Planning
                <table style="z-index: 100; left: 18px;">
                <tr>
                    <td style="width: 221px">&nbsp;</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 22px;">&nbsp;Select a Chapter</td>
                </tr>
                <tr>
                    <td style="width: 221px; height: 65px;">
                        <asp:DropDownList ID="ddlHospChapter" runat="server" DataTextField="Chapter" DataValueField="ChapterID" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlHospChapter_SelectedIndexChanged">
                            <asp:ListItem Value="0">Select Chapter</asp:ListItem>
                        </asp:DropDownList>
                         
                    </td>
                </tr>
            </table>
            </td>
            <td>
                <p class="btn_02" >
                    <asp:LinkButton ID="lnkAirports" runat="server" OnClick="lnkAirports_Click">Add/Update Airports</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkHospEvent" runat="server" OnClick="lnkHospEvent_Click">Set up Hospitality Event</asp:LinkButton>
                </p> 
                <p class="btn_02">
                    <asp:HyperLink ID="hypInviteeList" runat="server" NavigateUrl="HospInvitee.aspx">Prepare Invitee List</asp:HyperLink>
                </p>
                 <p class="btn_02">
                    <asp:HyperLink ID="hypInvResp" runat="server" NavigateUrl="HospInvResponse.aspx">Invitee Response</asp:HyperLink>
                </p>
                 <p class="btn_02">
                    <asp:HyperLink ID="hypHospSendEmail" runat="server" NavigateUrl="HospSendEmail.aspx">Send Email</asp:HyperLink>
                </p>
            </td>
            <td>
                <asp:HyperLink class="btn_02" ID="HyperLink312" runat="server" NavigateUrl="HospInviteeRpt">Invitee Reports</asp:HyperLink>
            </td>
            <td></td>
        </tr>

        <tr id="trTeamLead">
            <td class="title04" style="width: 263px">Team Lead Functions
            <table style="display: none;">
                <tr>
                    <td>
                        <asp:TextBox ID="txtRoleCategory" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtRoleCode" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            </td>
            <td>
                <p class="btn_02" style="display: none">
                    <asp:HyperLink ID="Hyperlink39" runat="server" NavigateUrl="~/VolunteerAssignRoles.aspx">Assign Roles To Volunteer</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink213" runat="server" NavigateUrl="~/SetUpTeams.aspx">Set Up Teams</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton15" Style="cursor: pointer;" class="btn_02" runat="server" OnClick="lnkTeamPlanning_Click"> Team Planning </asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton class="btn_02" ID="LinkButton3" runat="server" OnClick="LinkButton2_Click" Enabled="false">Send Email</asp:LinkButton>
                </p>
            </td>
            <td>
                <asp:HyperLink class="btn_02" ID="HyperLink47" runat="server" NavigateUrl="ViewContactList.aspx?Source=NC">View Contact List</asp:HyperLink>
            </td>
            <td></td>
        </tr>
        <tr id="trTeamMember">
            <td class="title04" style="width: 263px">Team Member Functions 
            <table style="display: none;">
                <tr>
                    <td>
                        <asp:TextBox ID="txtRoleCategory2" runat="server"></asp:TextBox>
                    </td>
                </tr>
                <tr>
                    <td>
                        <asp:TextBox ID="txtRoleCode2" runat="server"></asp:TextBox>
                    </td>
                </tr>
            </table>
            </td>
            <td>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton16" Style="cursor: pointer;" class="btn_02" runat="server" OnClick="lnkTeamPlanning_Click"> Team Planning </asp:LinkButton>
                </p>
            </td>
            <td>
                <asp:HyperLink ID="HyperLink46" class="btn_02" runat="server" NavigateUrl="ViewContactList.aspx?Source=NC">View Contact List</asp:HyperLink>
            </td>
            <td></td>
        </tr>
        <tr id="trFinalCoordinator" runat="server">
            <td class="title04" style="height: 150px; width: 263px;">Finals Coordinator Functions
            <br />
                <br />
                <asp:SqlDataSource ID="RoleCodeDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                    SelectCommand="USP_GetRoleCodeByCategory" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlRoleCategory1" Name="RoleCategory" PropertyName="SelectedValue"
                            Type="String" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <br />
                <table style="z-index: 100; left: 18px;">
                    <tr>
                        <td style="height: 24px">
                            <asp:DropDownList ID="DdlRoleCategory1" runat="server"
                                Width="146px" AutoPostBack="True">
                                <asp:ListItem>[Select Role Category]</asp:ListItem>
                                <asp:ListItem>National</asp:ListItem>
                                <asp:ListItem>Finals</asp:ListItem>
                                <asp:ListItem>Zonal</asp:ListItem>
                                <asp:ListItem>Cluster</asp:ListItem>
                                <asp:ListItem>Chapter</asp:ListItem>
                                <asp:ListItem>India Chapter</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr>
                        <td style="height: 24px">
                            <asp:DropDownList ID="DDlRoleCode" runat="server" AppendDataBoundItems="True" DataSourceID="RoleCodeDS"
                                DataTextField="RoleCode" DataValueField="RoleID" Width="142px" EnableViewState="False">
                                <asp:ListItem Value="Select Role">[Select Role]</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                    </tr>
                </table>
            </td>
            <td style="height: 150px">
                <p class="btn_02">
                    <asp:HyperLink ID="lnkAssignRole" runat="server" NavigateUrl="~/VolunteerAssignRoles.aspx">Assign Roles To Volunteer</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <%--<asp:hyperlink id="Hyperlink5" runat="server" NavigateUrl="~/email_NC.aspx" >Send Email</asp:hyperlink>--%><asp:HyperLink class="btn_02" ID="Hyperlink162" Enabled="true" runat="server" NavigateUrl="Email_NFInvites.aspx">Send Email Invite to National Finals </asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink217" NavigateUrl="~/MissingScores.aspx" Enabled="true" runat="server">Missing Scores</asp:HyperLink>
                </p>
                <%-- OnClick="lbtnAppSATChildTestAns_Click"--%>                       
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink200" NavigateUrl="VolunteerSignupReport.aspx" runat="server" Visible="false">Volunteer Signup Report</asp:HyperLink>
                </p>
                <asp:HyperLink class="btn_02" ID="HyperLink51" runat="server" NavigateUrl="ViewContactList.aspx?Source=NC">View Contact List</asp:HyperLink>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk205" runat="server" NavigateUrl="~/SupportTracking.aspx?NSFEvnt=1">Support Ticket Tracking</asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trParent" runat="server">
            <td style="width: 263px; height: 174px;" class="title04">parent Functions
            <asp:SqlDataSource ID="ChapersDSet" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                SelectCommand="usp_GetChapterAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
                <asp:ObjectDataSource ID="ClusterDescDs" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
                    OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="GetClusterDescriptionTableAdapters.ClusterTableAdapter"
                    UpdateMethod="Update">
                    <DeleteParameters>
                        <asp:Parameter Name="Original_ClusterId" Type="Int32" />
                    </DeleteParameters>
                    <UpdateParameters>
                        <asp:Parameter Name="ClusterCode" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                        <asp:Parameter Name="Original_ClusterId" Type="Int32" />
                    </UpdateParameters>
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                    <InsertParameters>
                        <asp:Parameter Name="ClusterCode" Type="String" />
                        <asp:Parameter Name="Description" Type="String" />
                    </InsertParameters>
                </asp:ObjectDataSource>
                <asp:SqlDataSource ID="ChapInZonesDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                    SelectCommand="usp_GetChapterWithinZone" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlZonalCoordinator" Name="ZoneID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
                <asp:SqlDataSource ID="ChaWithinClustersDS" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
                    SelectCommand="usp_GetChapterWithinCluster" SelectCommandType="StoredProcedure">
                    <SelectParameters>
                        <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
                            Type="Int32" />
                    </SelectParameters>
                </asp:SqlDataSource>
            </td>
            <td style="height: 174px">
                <p>
                    <asp:HyperLink ID="hlinkNationalRegistration" class="btn_02" Enabled="false" runat="server" NavigateUrl="parents/NationalInvitation.aspx"> National Finals Registration</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkparentInfo" class="btn_02" Enabled="false" runat="server" NavigateUrl="Registration.aspx?parentUpdate=true"> parent Info Update</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkChildInfo" class="btn_02" Enabled="false" runat="server" NavigateUrl="MainChild.aspx?parentUpdate=true"> Child Info Update</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkRegStatus" class="btn_02" Enabled="false" runat="server" NavigateUrl="parents/RegistrationStatus.aspx?parentUpdate=true">Registration Status</asp:HyperLink>
                </p>
                <p>
                    <asp:HyperLink ID="hlinkScores" class="btn_02" Enabled="false" runat="server" NavigateUrl="parents/ViewScores.aspx?parentUpdate=true">View Scores </asp:HyperLink>
                </p>
            </td>
            <td style="height: 174px"></td>
            <td style="width: 97px; height: 174px;"></td>
        </tr>
        <tr id="trCustomer" runat="server">
            <td class="title04" style="width: 263px; height: 92px;">Customer Service Functions
            </td>
            <td style="height: 92px;">
                <span class="announcement_text">Contests</span>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink83" runat="server" NavigateUrl="ApproveDuplicateRegistration.aspx">Approve Duplicates in Contests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink86" runat="server" NavigateUrl="CustServSearch.aspx">Search Parent Records</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HlnkSearchContest" Enabled="true" runat="server" NavigateUrl="~/ContRegSearch.aspx">Search Contest Registrations</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hLinktransferContest" Enabled="true" runat="server" NavigateUrl="~/Admin/ChangeCenter.aspx">Change Center</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink16" runat="server" Enabled="true" NavigateUrl="~/Admin/ChangeContest.aspx">Change Contest</asp:HyperLink>
                    &nbsp;
                </p>
                <span class="announcement_text">Workshops</span>
                <p class="btn_02">
                    <asp:HyperLink ID="hLinktransferWrkshp" Enabled="true" runat="server" NavigateUrl="~/Admin/ChangeWrkshpCenter.aspx">Change WorkShop Center</asp:HyperLink>
                </p>
                <span class="announcement_text">other</span>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink170" runat="server" NavigateUrl="SupportTracking.aspx">Respond to Support Tickets</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="hlinkChangeEmail" runat="server" NavigateUrl="ChangeEmail2.aspx">Process Change EmailID Requests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlinkPerRef" runat="server" NavigateUrl="PersonalPref.aspx?CustServ=1" Enabled="true">Personal Preferences</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink76" runat="server" NavigateUrl="NewsletterUnsubscribe.aspx" Enabled="true">Newsletter Subscriber Management</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink77" runat="server" NavigateUrl="ShoppingCatalog.aspx?id=2" Enabled="true">Shop</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink19" runat="server" NavigateUrl="VolMkDonation.aspx" Enabled="true">Donate</asp:HyperLink>
                    &nbsp;
                </p>
                <%--<p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk203" runat="server" NavigateUrl="AddUpdSupportContacts.aspx"> Support Ticket Contacts</asp:HyperLink>
                </p>--%>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk203" runat="server" NavigateUrl="~/SupportTicketContacts.aspx">Support Ticket Contacts</asp:HyperLink>
                </p>
            </td>
            <td style="height: 92px">
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink53" Enabled="true" runat="server" NavigateUrl="~/viewduplicates.aspx">View Duplicates</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink154" runat="server" NavigateUrl="~/IndspouseDuplicateEmails.aspx">View Duplicate Emails</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink163" runat="server" NavigateUrl="~/FailComplaint.aspx">Failed/Complaints on Send Email</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink171" runat="server" NavigateUrl="SupportTracking.aspx">Support Ticket Tracking</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink201" NavigateUrl="VolunteerSignupReport.aspx" runat="server">Volunteer Signup Report</asp:HyperLink>
                </p>
            </td>
            <td style="height: 92px;"></td>
        </tr>
        <tr id="TrNationalTechC" visible="false" runat="server">
            <td class="title04" style="width: 263px; height: 99px;">National Technical Coordinator </td>
            <td style="height: 99px">
                <asp:LinkButton CssClass="btn_02" ID="lbtnNatTechCDnloadTstpapers" runat="server" OnClick="lbtnNatTechCDnloadTstpapers_Click" Enabled="true">Download Test Papers</asp:LinkButton>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink151" class="btn_02" runat="server" Enabled="true" NavigateUrl="ManageScoresheet.aspx"> Download/Upload Scoresheets</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink152" class="btn_02" runat="server" Enabled="false" NavigateUrl="ScheduleNationalTechTeam.aspx"> Schedule National Technical Team</asp:HyperLink>
                    &nbsp;
                </p>
                <%--  <p class="btn_02">
               <asp:LinkButton ID="lbtnNatTechCoach" runat="server" CssClass="btn_02" 
                  onclick="lbtnNatTechCoach_Click" Visible="true">Download Coach Papers</asp:LinkButton>
            </p>--%>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink24" runat="server" NavigateUrl="TestPaperTemplates.aspx">Test Paper Templates</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink38" runat="server" NavigateUrl="ITDocuments.aspx">IT Documents</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton10" runat="server" OnClick="lnkActList_Click">Activity List</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink146" runat="server" NavigateUrl="Email_TC.aspx">Send Email to TC and Others</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td style="height: 99px">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink214" NavigateUrl="~/MissingScores.aspx" Enabled="true" runat="server">Missing Scores</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink147" NavigateUrl="~/Reports/TechCoorList.aspx" Enabled="true" runat="server">Tech Coord List</asp:HyperLink>
                </p>
            </td>
            <td style="height: 99px"></td>
        </tr>
        <tr id="trTechnicalCoordinator" runat="server">
            <td class="title04" style="width: 263px">Technical Coordinator</td>
            <td>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lbtnTechCDnloadTstpapers" runat="server" OnClick="lbtnTechCDnloadTstpapers_Click" Enabled="true">Download Test Papers</asp:LinkButton>
                    <asp:Label ID="lblTCErrTP" runat="server" ForeColor="Red"></asp:Label>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="ltbnTechCManageScoresheet" runat="server" OnClick="ltbnTechCManageScoresheet_Click">Download/Upload Scoresheets</asp:LinkButton>
                    <asp:Label ID="lblTCErrSS" runat="server" ForeColor="Red"></asp:Label>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlMissingScores" NavigateUrl="~/MissingScores.aspx" Enabled="true" runat="server">Missing Scores</asp:HyperLink>
                </p>
                <br />
            </td>
            <td>
                <p class="btn_02">
                    <asp:LinkButton CssClass="btn_02" ID="lnkTCViewAbsentees" runat="server" OnClick="lnkTCViewAbsentees_Click">View Absentees List</asp:LinkButton>
                    <asp:Label ID="lblTCErrAbsentees" runat="server" ForeColor="Red"></asp:Label>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkTCContestTeam" runat="server" class="btn_02" OnClick="lnkTCViewAbsentees_Click">View Contest Team</asp:LinkButton>
                    <asp:Label ID="lblTCErrCTeam" runat="server" ForeColor="Red"></asp:Label>
                </p>
                <p class="btn_02">
                    <asp:LinkButton CssClass="btn_02" ID="lnkTCSSLog" runat="server" OnClick="lnkTCViewAbsentees_Click">View Score Sheet Log</asp:LinkButton>
                    <asp:Label ID="lblTCErrSSLog" runat="server" ForeColor="Red"></asp:Label>
                </p>
            </td>
            <td>
                <p>
                    <asp:HyperLink ID="Hyperlink123" CssClass="btn_02" runat="server" NavigateUrl="javascript:showhide('div2');">Instructions for Technical Team</asp:HyperLink>
                </p>
                <div id="div2" style="display: none;">
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink124" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Spelling.zip">Spelling</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink125" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Vocabulary.zip">Vocabulary</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink126" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Math.zip">Math</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink127" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Science.zip">Science</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink128" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Geography.zip">Geography</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink129" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Essay.zip">Essay Writing</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink130" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/publicspeaking.zip">Public Speaking</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink240" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/Brain_Bee.zip">Brain Bee</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink143" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/AccessingScoreSheets.ppt">Accessing Score Sheets</asp:HyperLink>
                    </p>
                    <p>
                        &nbsp;&nbsp;&nbsp;
                  <asp:HyperLink class="btn_03" ID="Hyperlink145" runat="server" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/ScoreSheetsProcess_Multiple_Phase2_Rooms.pdf"> Multiple Phase 2 Rooms</asp:HyperLink>
                    </p>
                </div>
            </td>
        </tr>
        <tr id="trExamDistributor" runat="server" visible="false">
            <td class="title04" style="width: 263px">Exam Distributor </td>
            <td>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lbtnExamDTloadTstpapers" runat="server" OnClick="lbtnTechCDnloadTstpapers_Click" Enabled="true">Upload/Download Test Papers</asp:LinkButton>
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trCoreTeamfunctions" runat="server" visible="false">
            <td class="title04" style="width: 263px">Core Team Functions Page</td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink25" runat="server" NavigateUrl="TestPaperTemplates.aspx">Test Paper Templates</asp:HyperLink>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LinkButton19" runat="server" OnClick="lbtnCoreTeamDTestPapers_Click" Enabled="true">Download Test Papers</asp:LinkButton>

                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkSBVBSelMatrixCoreT" runat="server" OnClick="lnkSBVBSelMatrix_Click">SBVB Selection Criteria</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="hylnkGenSBVBTestPaperCoreT" NavigateUrl="~/GenSBVBTestPapers.aspx" Enabled="true" runat="server"> Generate SBVB Paper Sets </asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trLaptopJudge" runat="server" visible="false">
            <td class="title04" style="width: 263px">Chief Judge/Laptop Judge</td>
            <td>
                <p>
                    <%--<asp:HyperLink class="btn_02" ID="HyperLink146" NavigateUrl="ManageScoresheet.aspx" runat="server" > Download/Upload Scoresheets</asp:HyperLink>
                    --%>
                    <asp:LinkButton CssClass="btn_02" ID="lbtnJudgeScoreSheet" runat="server" OnClick="lbtnJudgeScoreSheet_Click">Download/Upload Scoresheets</asp:LinkButton>
                </p>
            </td>
            <td>
                <asp:LinkButton CssClass="btn_02" ID="lbtnAbsenteesList" runat="server" PostBackUrl="ViewAbsenteeList.aspx?id=1">View Absentees List</asp:LinkButton>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkContestTeam1" runat="server" class="btn_02"
                        PostBackUrl="ViewContestTeam.aspx?ID=1">View Contest Team</asp:LinkButton>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trExamReceiver" visible="false" runat="server">
            <%-- <p class="btn_02"><asp:hyperlink class="btn_02" id="hlinkWalkathon" runat="server" Enabled="false" NavigateUrl="Don_athon_selection.aspx?Ev=5">Register for Walk-a-thon</asp:hyperlink></p>--%>
            <td class="title04" style="width: 263px">Exam Receiver</td>
            <td>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lbtnExamCDnloadTstpapers" runat="server" OnClick="lbtnTechCDnloadTstpapers_Click">Download Test Papers</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="ltbnExamCManageScoresheet" runat="server" OnClick="ltbnTechCManageScoresheet_Click">Download/Upload Scoresheets</asp:LinkButton>
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <%--     <tr id="trUploadTestPapers" visible="false" runat="server">
         <td class="title04" style="width: 263px">Exam Distributor</td>
         <td>
            <p class="btn_02">
               <asp:HyperLink ID="hlUploadTestPapers" NavigateUrl="ManageTestPapers.aspx" runat="server" >Upload Test Papers</asp:HyperLink>
            </p>
            <br />
         </td>
         <td></td>
         <td></td>
         </tr>--%>
        <tr id="trBeeBook" runat="server">
            <td class="title04" style="height: 26px; width: 263px;">Bee Book</td>
            <td style="height: 26px">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink45" NavigateUrl="UploadPhotos.aspx?managephotos=1" runat="server">Manage Contestant Photos</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink215" NavigateUrl="BeeUploadPhotos.aspx" runat="server">Upload Photo</asp:HyperLink>
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trFundRaising" visible="false" runat="server">
            <td class="title04" style="height: 26px; width: 263px;">Fundraising Functions<br />
                <table style="z-index: 100; left: 18px;">
                    <tr>
                        <td style="width: 221px">&nbsp;
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 221px; height: 22px;">&nbsp;Select a Chapter
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 221px; height: 65px;">
                            <asp:DropDownList ID="ddlChapterFundR" runat="server" DataTextField="Chapter" DataValueField="ChapterID" AutoPostBack="true" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlChapterFundR_SelectedIndexChanged">
                                <asp:ListItem Value="0">Select Chapter</asp:ListItem>
                            </asp:DropDownList>

                        </td>
                    </tr>
                </table>
            </td>
            <td style="height: 26px">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkFundRaisingCalFunc" runat="server" OnClick="lnkFundRaisingCalFunc_Click">Fund Raising Calendar</asp:LinkButton>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink131" NavigateUrl="FundRReg.aspx?id=1" runat="server">Register for Fundraising</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink221" NavigateUrl="PriorParticipation.aspx" runat="server">Prior Participation in Contests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton CssClass="btn_02" ID="LnkFundRSendEmail" OnClick="LnkFundRSendEmail_Click" runat="server">Send Email</asp:LinkButton>
                </p>
            </td>
            <td>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="HyperLink133" NavigateUrl="FundRReport.aspx" runat="server">View Fundraising Report</asp:HyperLink>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LnkFundRContestRegReport" OnClick="LnkFundRContestRegReport_Click" runat="server">Contest Registration Report</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="lnkFundRPendingRegReport" OnClick="lnkFundRPendingRegReport_Click" runat="server">Pending Registration Report</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LnkCheckinList" OnClick="LnkCheckinList_Click" runat="server">Check-in List</asp:LinkButton>
                </p>
                <p>
                    <asp:LinkButton CssClass="btn_02" ID="LnkContestantList" OnClick="LnkContestantList_Click" runat="server">Contestant List</asp:LinkButton>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trAccountingFunctions" runat="server">
            <td class="title04" style="width: 263px">Accounting Functions</td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink34" runat="server" NavigateUrl="ReimbursmentForm.aspx">Reimbursment Form</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink48" runat="server" NavigateUrl="~/dbsearch.aspx">Search/Update Records</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink50" runat="server" NavigateUrl="~/search_sponsor.aspx">Add/Update Sponsor/Venue</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkReqRefund" runat="server" NavigateUrl="Refund.aspx" Enabled="True">Refund Request</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton CssClass="btn_02" ID="lnkDAS" runat="server" Text="View/Update DAS Records" OnClick="lnkDAS_Click"></asp:LinkButton>
                    <asp:Label ID="lblerr" runat="server" ForeColor="red" Visible="false" Text=""></asp:Label>
                </p>
                <p class="btn_02">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink72" runat="server" NavigateUrl="~/CorpMatchGift.aspx" Enabled="False">Corp. Match Gift</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink136" CssClass="btn_02" runat="server" NavigateUrl="~/OtherDeposits.aspx">Other Deposits</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink class="btn_02" ID="HyperLink6" NavigateUrl="IndiaScholarships.aspx" runat="server">India Scholarship Designations</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkEndownment" runat="server" OnClick="lnkEndownment_Click">Endowments</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink66" CssClass="btn_02" runat="server" NavigateUrl="~/reports/reimbreport.aspx">Reimbursment Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink157" CssClass="btn_02" runat="server" NavigateUrl="~/reports/ExpSummaryReport.aspx">Expense Summary Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink56" CssClass="btn_02" runat="server" NavigateUrl="~/vieworganizations.aspx">View Organizations</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink54" CssClass="btn_02" runat="server" NavigateUrl="~/reports/DonationSummary.aspx">Donation Summary Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink159" CssClass="btn_02" runat="server" NavigateUrl="~/reports/RegistrationRevenueSummary.aspx">Registration Revenue Summary</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink160" CssClass="btn_02" runat="server" NavigateUrl="~/reports/RegCountSummary.aspx">Registration Count Summary</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink303" CssClass="btn_02" runat="server" NavigateUrl="~/DonorReceiptLog.aspx">Donor Receipt Log</asp:HyperLink>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink144" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/NSF_Reimbursement_Form_Instructions_April2012.pdf">Instructions for Reimbursement Form</asp:HyperLink>
                </p>
            </td>
        </tr>
        <tr id="trTreasuryFunctions" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Treasury Functions </td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink61" runat="server" NavigateUrl="~/CCReconcile.aspx" Enabled="false">Reconcile CC Trans</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink223" runat="server" NavigateUrl="~/CCReconcileBT.aspx">Reconcile BT Transactions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlCCFees" runat="server" NavigateUrl="~/UploadCCFees.aspx">Upload Credit Card Fees</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink203" runat="server" NavigateUrl="~/IRS1099K.aspx">IRS1099K Data</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink62" runat="server" NavigateUrl="~/BankTrans.aspx">Banking Transactions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink137" runat="server" NavigateUrl="~/BankTransTDA.aspx">Brokerage Transactions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink87" CssClass="btn_02" runat="server" NavigateUrl="~/ManageVoucher.aspx" Enabled="false"> Vouchers & Quick Book</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtUSSAwards" runat="server" OnClick="lbtUSSAwards_Click">US Scholarship Awards &amp; Payments</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkRestrictedFund" runat="server" OnClick="lnkRestrictedFund_Click">Restricted Funds</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink153" runat="server" NavigateUrl="~/PaymentTransValidation.aspx">Validating Payment Transactions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink141" CssClass="btn_02" runat="server" NavigateUrl="~/ChkPaymentNotes.aspx">Verify Payment Notes</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink161" CssClass="btn_02" runat="server" NavigateUrl="~/ScanFinDoc.aspx">Scanned Financial Documents</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink166" CssClass="btn_02" runat="server" NavigateUrl="~/DonVolAwards.aspx">Donor & Volunteer Awards</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink172" CssClass="btn_02" runat="server" NavigateUrl="~/MLWinners.aspx">Major League Winners</asp:HyperLink>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink63" runat="server" NavigateUrl="~/Reports/ccreports.aspx">Credit Card Reports</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink205" runat="server" NavigateUrl="~/IRS1099KDataReconciliation.aspx">IRS1099K Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink64" runat="server" NavigateUrl="~/Reports/BankTransReports.aspx">Banking Transactions Reports</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink210" runat="server" NavigateUrl="~/Reports/ReviewExpTrans.aspx">Review Expense Transactions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink134" runat="server" NavigateUrl="~/InternalAuditReports.aspx">Audit Reports</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink138" runat="server" NavigateUrl="~/TrialBalance.aspx">Trial Balance</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink135" runat="server" NavigateUrl="~/CreditCardFeesCalculation.aspx"> Credit Card Fees</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink168" runat="server" NavigateUrl="~/NSFChampionsList.aspx">NSF Champions List</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink169" runat="server" NavigateUrl="~/DonVolRecAwardsList.aspx">Donor/Volunteer Awards List</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlFailComplaintTreaFunc" runat="server" NavigateUrl="~/FailComplaint.aspx">Failed/Complaints on Send Email</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink300" runat="server" NavigateUrl="~/SuppressionList.aspx" onclick="lnkSupListHandling_Click">Suppression List Handling</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink218" runat="server" NavigateUrl="~/ChargeBack.aspx">Respond to Chargebacks</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink233" runat="server" NavigateUrl="~/BlockList.aspx">Manage BlockList</asp:HyperLink>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink211" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/NSF_GL_DataGeneration_UserDoc.pptx">Creating GL records</asp:HyperLink>
                </p>
            </td>
        </tr>
        <tr id="trgamefunction" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Game Functions</td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkGame" runat="server" Text="Search/Update Game Records" OnClick="lnkGame_Click"></asp:LinkButton>
                    <asp:Label ID="lblerr1" runat="server" ForeColor="red" Text="" Visible="False"></asp:Label>
                </p>
            </td>
            <td>
                <asp:LinkButton ID="lnkGameRegistrations" runat="server" Text="Game Registrations" OnClick="lnkGameRegistrations_Click" CssClass="btn_02"></asp:LinkButton>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink224" runat="server" NavigateUrl="~/GamePerf.aspx">Game Performance</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk206" runat="server" NavigateUrl="~/SupportTracking.aspx?NSFEvnt=4">Support Ticket Tracking</asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trCoachFunctions" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Coach Functions - Admin</td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnScheduleCoaches" runat="server" Text="Schedule Coaches" PostBackUrl="schedulecoach.aspx" Enabled="false"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Recruiting</span>
                </p>
                <p class="btn_02" runat="server" id="PWebConfControl" visible="false">
                    <asp:LinkButton ID="LinkButton17" runat="server" Text="Web Conf Control" PostBackUrl="~/WebConfControl.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <%--    <asp:LinkButton ID="lbtnSetUpWebExSessions" runat="server" Text="Setup WebEx Sessions" PostBackUrl="~/CreateMeetings.aspx"></asp:LinkButton>--%>

                    <asp:LinkButton ID="lbtnSetUpWebExSessions" runat="server" Text="Setup Zoom Sessions" PostBackUrl="~/SetUpZoomSessions.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LnkGuestAttendance" runat="server" Text="Guest Attendance" PostBackUrl="~/GuestAttendance.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkbtnCoachingDateCalendar" runat="server" Text="Coaching Date Calendar" PostBackUrl="CoachingDateCal.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnCalendarSignup" runat="server" Text="Calendar Signup" PostBackUrl="CalendarSignup.aspx?Role=Admin"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LnkAdminClassCalendar" Visible="true" runat="server" Text="Set Up Class Calendar" PostBackUrl="CoachClassCalNew.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnPrepareSchedule" runat="server" Text="Prepare Schedule" PostBackUrl="PrepareSchedule.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAssignCoach" runat="server" Text="Assign Coach" PostBackUrl="assigncoach.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Registration</span>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnChangeCoach" runat="server" Text="Change Coach for a Child" PostBackUrl="admin/ChangeCoaching.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnCoachingRegistrationUpdate" runat="server" Text="Update Child Registration Data" PostBackUrl="CoachingRegistrationUpdate.aspx"></asp:LinkButton>
                </p>

                <p class="btn_02">
                    <asp:LinkButton ID="lbtnExCoaching" runat="server" Text="Exception List for Coaching" PostBackUrl="ExceptionalCoaching.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnBulkChangeCoach" runat="server" Text="Bulk Change for a Coach" PostBackUrl="admin/BulkChangeCoach.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LnkWebExStudentReg" runat="server" Text="Change Zoom Reg of student" PostBackUrl="~/StudentZoomReg.aspx"></asp:LinkButton>
                </p>

                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Other</span>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LnkSurvey" runat="server" Text="Coaching Survey" OnClick="LnkSurvey_Click"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lblAssessform" runat="server" Text="Assessment Form" PostBackUrl="volAssessmentForm.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">

                    <asp:LinkButton ID="lnkBtnVirtualRoomReq" runat="server" Text="Virtual Room Requirement" PostBackUrl="~/VirtualRoomRequirement.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkBtnClassSchedule" runat="server" Text="Add/Update Class Schedule" PostBackUrl="ClassSchedule.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnCoachAdminSendEmail" runat="server" Text="Send Email" PostBackUrl="Emailcoach.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02" id="pnlAddUpdateEventFees" runat="server" visible="false">
                    <asp:HyperLink ID="HlnkAddUpdateEventFees" NavigateUrl="AddUpdateEventFees.aspx" runat="server">Add Update EventFees</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink225" NavigateUrl="AddUpdSpashPageContents.aspx" runat="server">Add Update Splash Page Contents</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink239" runat="server" NavigateUrl="~/SupportTicketContacts.aspx"> Support Ticket Contacts</asp:HyperLink>
                </p>

                <asp:Label ID="Label1" runat="server" ForeColor="red" Visible="false" Text=""></asp:Label>
            </td>
            <td>
                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Recruiting</span>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink202" NavigateUrl="VolunteerSignupReport.aspx" runat="server" Visible="false">Volunteer Signup Report</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink173" runat="server" Enabled="true" NavigateUrl="CalendarSignupCount.aspx">Calendar Signup Count</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink219" runat="server" Enabled="true" NavigateUrl="~/RecCoachesReport.aspx">Recruiting Coaches Report</asp:HyperLink>
                    &nbsp;
                </p>


                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink175" runat="server" Enabled="true" NavigateUrl="CalSignUpList.aspx">Coach List</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink176" runat="server" Enabled="true" NavigateUrl="CoachSchedReport.aspx">Schedule Report</asp:HyperLink>
                    &nbsp;
                </p>

                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Registration</span>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink70" runat="server" Enabled="true" NavigateUrl="Reports/coachRegisReport.aspx">Registration Report for  Coaching</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink181" runat="server" Enabled="true" NavigateUrl="CoachingRegCount.aspx">Coaching Registration Count</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink71" runat="server" Enabled="true" NavigateUrl="ViewContactList.aspx?coachflag=1">Contact Details for Coaches</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HlStudentEnrollment" runat="server" Enabled="true" NavigateUrl="~/StudentEnrollment.aspx">Student  Enrollment</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink174" runat="server" Enabled="true" NavigateUrl="SearchParentandChild.aspx?coachflag=1">Search Parent & Child</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <span class="announcement_text" style="font-weight: bold;">Other</span>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink148" Enabled="true" runat="server" NavigateUrl="~/SendEmailLog.aspx">Send Email Log</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk207" runat="server" NavigateUrl="~/SupportTracking.aspx?NSFEvnt=13">Support Ticket Tracking</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlStudentRoster" NavigateUrl="StudentRoster.aspx" runat="server">Student Roster</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlClassStatus" NavigateUrl="ClassStatus.aspx" runat="server">Class Status</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="hlExceptionTracking" NavigateUrl="CoachingExceptionLog.aspx" runat="server">Exception Log</asp:HyperLink>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HlWebExUsageRepor" runat="server" NavigateUrl="~/ZoomTRSReport.aspx">Zoom Usage Report</asp:HyperLink>
                </p>
            </td>
        </tr>
        <tr id="trCoachFunctionsCoach" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Coach Functions - Coaches</td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnCalendarSignup_Coach" runat="server" Text="Calendar Signup" PostBackUrl="CalendarSignup.aspx?Role=Coach"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton2" runat="server" Text="Set Up Class Calendar" PostBackUrl="CoachClassCalNew.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="btnDownloadCoachPapers" runat="server" OnClick="btnDownloadCoachPapers_Click">Upload/Download Coach Papers</asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="bttnCoachPapersReleaseDates" runat="server" OnClick="bttnCoachPapersReleaseDates_Click">Coach Paper Release Dates</asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="LnkCoachAnswerKeys" PostBackUrl="TestAnswerKey.aspx" runat="server" Text="Answer Keys"></asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="LnkMakeUpConfSessions" runat="server" Text="Makeup Sessions"
                        PostBackUrl="~/MakeupSessions.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="LinkButton18" runat="server" Text="Substitute Sessions"
                        PostBackUrl="~/SubstitueSessions.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LnkPractising" runat="server" Text="Practice Session"
                        PostBackUrl="~/PracticeSession.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="LinkButton20" runat="server" Text="Extra Session"
                        PostBackUrl="~/ExtraSessions.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="lnkLiveWebConfSessions" runat="server" Text="Terminate a Live Zoom Session
"
                        PostBackUrl="~/LiveWebConf.aspx"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnCoachSendEmail" runat="server" Text="Send Email" PostBackUrl="Emailcoach.aspx"></asp:LinkButton>
                </p>

            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink79" runat="server" Enabled="true" NavigateUrl="ViewContactList.aspx?coachflag=2">Contact Details of Students</asp:HyperLink>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink195" runat="server" Enabled="true" NavigateUrl="~/Reports/CoachClassCalReport.aspx">Class Calendar Report</asp:HyperLink>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink187" runat="server" Enabled="true" NavigateUrl="VConfRoomCal.aspx">Virtual Conf Room Calendar</asp:HyperLink>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink80" runat="server" NavigateUrl="RegCountByCoach.aspx">Coaching Registration Count</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink150" runat="server" Enabled="true" NavigateUrl="ViewSATTestScores.aspx">View Test Scores</asp:HyperLink>

                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink155" Enabled="true" runat="server" NavigateUrl="~/SendEmailLog.aspx">Send Email Log</asp:HyperLink>
                </p>
            </td>
            <td>


                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink21" runat="server" Enabled="true" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/CalendarSignupGuide.docx">Calendar Signup Guide</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink232" Target="_blank" runat="server" NavigateUrl="~/BulletinBoard/Volunteer/NSF Orientation for Coaches.pdf">Orientation for Coaches</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink22" runat="server" Enabled="true" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/InstructionsForNSFWebTools_Coaches.pdf">Instructions for NSF Web Tools</asp:HyperLink>
                    &nbsp;
                </p>
                <p>
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink250" Target="_blank" runat="server" NavigateUrl="https://www.northsouth.org/app9/BulletinBoard/Volunteer/SetupClassCalendar_Instructions_October_2017.pptx">Setup Class Calendar Instructions</asp:HyperLink>
                </p>
                <p>
                    <a id="ancCoachLevelDet" style="font-weight: bold; color: blue; cursor: pointer;">Level Determination </a>
                </p>
                <p>
                    <a id="anCoachFAQ" style="font-weight: bold; color: blue; cursor: pointer;">FAQ</a>
                </p>

                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink23" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/MathCountsLevelDetermination.aspx">Mathcounts - Level Determination</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink226" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/PreMathCountsLevelDetermination.aspx">Pre-Mathcounts Level Determination</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink227" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/mathCountsFAQ.aspx">Mathcounts FAQ</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink228" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/PremathCountsFAQ.aspx">Pre-Mathcounts FAQ</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink229" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/SATACT_Coaching_FAQ.pdf">SAT Coaching FAQ</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink230" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/USContests/Coaching/Science.aspx">Science FAQ</asp:HyperLink>
                </p>
                <p style="display: none">
                    <asp:HyperLink CssClass="btn_02" ID="Hyperlink231" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/FAQ_Geography_Coaching.pdf">Geography FAQ</asp:HyperLink>
                </p>




                <p>
                    <asp:HyperLink CssClass="btn_02" ID="HlMathGuide" Target="_blank" runat="server" NavigateUrl="http://www.northsouth.org/public/uscontests/coaching/GradingToolInstruction.docx">Guide to Entering Math Answers</asp:HyperLink>
                </p>

                <p class="btn_02" style="display: none">
                    <asp:HyperLink ID="HlWebExSessionFAQ" runat="server" Enabled="true" Target="_blank" NavigateUrl="~/WebExFAQ.aspx">WebEx Session - FAQ</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02" style="display: none">
                    <asp:HyperLink ID="Hyperlink184" runat="server" Enabled="False" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/WebexScheduling.pdf">How To Schedule a Training Session</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02" style="display: none">
                    <asp:HyperLink ID="Hyperlink185" runat="server" Enabled="False" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/WebexTrainingCourse.mpg">WebEx Training Session</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02" style="display: none">
                    <asp:HyperLink ID="Hyperlink186" runat="server" Enabled="False" Target="_blank" NavigateUrl="~/BulletinBoard/Volunteer/StepstoUploadContactsintoWebEx.pdf">How to Upload Contacts</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
        </tr>
        <tr id="trSATFunctionsAdmin" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Coach Paper Functions - Admin/Lead</td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAppSATAdminTests" OnClick="lbtnAppSATAdminTests_Click" runat="server" Text="Tests" Visible="False"></asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="lbtnAppSATAdminTestSections" PostBackUrl="TestSections.aspx" runat="server" Text="Test Sections"></asp:LinkButton>
                </p>
                <%--OnClick="lbtnAppSATAdminTestSections_Click" --%>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="lbtnAppSATAdminAnswerKeys" PostBackUrl="TestAnswerKey.aspx" runat="server" Text="Answer Keys"></asp:LinkButton>
                </p>
                <%--OnClick="lbtnAppSATAdminAnswerKeys_Click"--%>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAppSATAdminStudTests" OnClick="lbtnAppSATAdminStudTests_Click" runat="server" Text="Assign Tests to Student" Visible="False"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAppSATAdminTestStud" OnClick="lbtnAppSATAdminTestStud_Click" runat="server" Text="Assign Students to Test" Visible="False"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAppSATChildTestAns" PostBackUrl="ChildTestAnswers.aspx" runat="server" Text="Take a Test"></asp:LinkButton>
                </p>
                <%-- OnClick="lbtnAppSATChildTestAns_Click"--%>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnAppSATWebForm1" OnClick="lbtnAppSATWebForm1_Click" runat="server" Text="New Test" Visible="False"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton5" runat="server" OnClick="LinkButton5_Click">Upload/Download Coach Papers</asp:LinkButton>
                </p>
                <p class="btn_02" runat="server" visible="false">
                    <asp:LinkButton ID="LinkButton6" runat="server" OnClick="LinkButton6_Click">Coach Paper Release Dates</asp:LinkButton>
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink182" runat="server" Enabled="true" NavigateUrl="CoachingRegCount.aspx">Coaching Registration Count</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="LinkButton7" runat="server" PostBackUrl="CoachingClassesReport.aspx?Role=Admin">Scheduled Classes Report </asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink179" runat="server" Enabled="true" NavigateUrl="CalSignUpList.aspx">Coach List</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink180" runat="server" Enabled="true" NavigateUrl="CoachSchedReport.aspx">Schedule Report</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink15" runat="server" Enabled="true" NavigateUrl="ViewContactList.aspx?coachflag=1">Contact Details for Coaches</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink18" runat="server" Enabled="true" NavigateUrl="StudentLoginList.aspx?">Student Login List</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink17" runat="server" Enabled="true" NavigateUrl="ViewSATTestScores.aspx">View Test Scores</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk208" runat="server" NavigateUrl="~/SupportTracking.aspx?NSFEvnt=13">Support Ticket Tracking</asp:HyperLink>
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trWebPageMgmt" runat="server" visible="false">
            <td class="title04" style="height: 27px; width: 263px;">Webpage Management Functions</td>
            <td style="height: 27px">
                <p class="btn_02">
                    <asp:LinkButton ID="lnkWebPageMgmt" runat="server" PostBackUrl="WebPageMgmtMain.aspx" OnClick="lnkChWebPageMgmt_Click">Web Page Management</asp:LinkButton>
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trBadgesCertificates" visible="false" runat="server">
            <td class="title04" style="width: 263px">Badges & Certificates</td>
            <td></td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="lnkGenerateBadges" runat="server" Enabled="true" NavigateUrl="GenerateBadges.aspx">Generate Badges</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="lnkGenerateCertificates" runat="server" Enabled="true" NavigateUrl="GenerateParticipantCertificates.aspx">Generate Certificates</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trMedalsCert" visible="false" runat="server">
            <td class="title04" style="width: 263px">Medals and Certificates </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="lnkMedalsCert" runat="server" Enabled="true" NavigateUrl="Reports/AllContestsWeeklyList.aspx">View Counts on Medals</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="lnkOrderMedals" runat="server" Enabled="true" NavigateUrl="Reports/OrderMedals.aspx">Order Medals/Trophies</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink306" runat="server" Enabled="true" NavigateUrl="ChapterContestDates.aspx">View Chapter Contest Dates</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trBeeFunction" visible="false" runat="server">
            <td class="title04" style="width: 263px">Geography Bee Functions
            </td>
            <td>
                <p class="btn_02">

                    <asp:HyperLink ID="Hyperlink164" runat="server" Enabled="true" NavigateUrl="GBQuestion.aspx">Add/Update GB Questions</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">

                    <asp:HyperLink ID="Hyperlink165" runat="server" Enabled="true" NavigateUrl="QpaperGen.aspx">Question Paper Generation</asp:HyperLink>
                    &nbsp;
                </p>
            </td>
            <td></td>
            <td></td>
        </tr>
        <tr id="trVolRecruit" runat="server">
            <td class="title04" style="height: 26px; width: 263px;">Human Resources</td>
            <td style="height: 26px">
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink178" NavigateUrl="VolTeamMatrix.aspx" runat="server">Volunteer Teams Matrix</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink188" NavigateUrl="ProdTeamMatrix.aspx" runat="server">Product Team Matrix</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink191" NavigateUrl="AddUpdateVolunteerSignupGuide.aspx" runat="server">Add/Update Volunteer Signup Guide</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink304" NavigateUrl="~/SendEmail_HR.aspx" runat="server">Send Email to Volunteers</asp:HyperLink>
                </p>

            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="HyperLink190" NavigateUrl="VolunteerSignupReport.aspx" runat="server">Volunteer Signup Report</asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>
        <tr id="trGeneralFunctions" runat="server">
            <td class="title04" style="height: 26px; width: 263px;">General Functions</td>
            <td style="height: 26px"></td>
            <td style="height: 26px">
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink32" runat="server" NavigateUrl="Reports/ContestsByDate.aspx"> Upcoming Contests</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink60" runat="server" NavigateUrl="Reports/RegistrationReport.aspx">Contest Registration Count</asp:HyperLink>
                </p>

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink139" runat="server" NavigateUrl="Reports/ContestLessChapter.aspx">Chapters without a calendar</asp:HyperLink>
                </p>
                <p class="btn_02">

                    <asp:HyperLink ID="hlNewChapterCCs" runat="server" NavigateUrl="~/NewChaptersCCs.aspx">New Chapters/CCs</asp:HyperLink>


                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink142" runat="server" NavigateUrl="Reports/MissingTechCord.aspx">Missing Tech Coordinators by chapter</asp:HyperLink>
                </p>
            </td>
            <td></td>
        </tr>

        <tr id="trPersonal" runat="server">
            <td class="title04" style="height: 47px; width: 263px;">Personal Functions</td>
            <td style="height: 47px">

                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink183" runat="server" NavigateUrl="VolunteerSignUpnew.aspx" Enabled="true">Volunteer Signup</asp:HyperLink>
                </p>
                <p class="btn_02">

                    <asp:LinkButton ID="hlFillOutSurvey" runat="server" OnClick="hlFillOutSurvey_Click">Fill out a Survey</asp:LinkButton>
                    &nbsp;
                </p>
                <div id="dvSurveyNotification" style="float: right; text-align: center; border-radius: 10px; position: relative; top: -31px; width: 23px; right: 46px; height: 20px; background-color: green; color: white;" runat="server">
                    <asp:Label runat="server" ID="lblNotCount" Style="text-align: center; font-weight: bold; color: white;"></asp:Label>
                </div>
                 <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink308" runat="server" NavigateUrl="ZoomSessions.aspx" Enabled="true">Zoom Sessions</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink193" runat="server" NavigateUrl="VolunteerDisclosureForm.aspx" Enabled="true">Volunteer Disclosure Form</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink33" runat="server" NavigateUrl="Registration.aspx?parentUpdate=true" Enabled="true"> Update Personal Profile</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="hlinkdonate" runat="server" Text="Donate" OnClick="hlinkdonate_Click"></asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lnkFundraising" runat="server" Text="Register for Fundraising" OnClick="lnkFundraising_Click"></asp:LinkButton>
                </p>
                <%-- <p class="btn_02"><asp:hyperlink class="btn_02" id="hlinkWalkathon" runat="server" Enabled="false" NavigateUrl="Don_athon_selection.aspx?Ev=5">Register for Walk-a-thon</asp:hyperlink></p>--%>
                <p class="btn_02">
                    <asp:LinkButton class="btn_02" ID="hlinkWalkathon" runat="server" Text="Register for Walk-a-thon" Enabled="false" OnClick="hlinkWalkathon_Click"></asp:LinkButton>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:LinkButton ID="lbtnChildupdate" runat="server" CssClass="btn_02" OnClick="lbtnChildupdate_Click"> Add/Update Child Info</asp:LinkButton>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink75" runat="server" NavigateUrl="PersonalPref.aspx?id=2" Enabled="true">Personal Preferences</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink209" runat="server" NavigateUrl="RemoveFromSuppressionList.aspx" Enabled="true">Remove from Suppression List</asp:HyperLink>
                    &nbsp;
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink55" runat="server" NavigateUrl="~/DonationReceipt.aspx"> Donation Receipt</asp:HyperLink>
                    &nbsp;
                </p>

            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlink192" runat="server" NavigateUrl="VolunteerSignupGuide.aspx" Enabled="true">Volunteer Signup Guide</asp:HyperLink>
                </p>
                <p class="btn_02">
                    <asp:HyperLink ID="Hyperlnk210" runat="server" NavigateUrl="~/SupportTracking.aspx?showmine=true">Support Ticket Tracking</asp:HyperLink>
                </p>

            </td>
            <td>
                <p class="btn_02">
                    <asp:HyperLink ID="hlSplashPage" runat="server" NavigateUrl="~/SplashPageContents.aspx" Enabled="true">Splash Page</asp:HyperLink>
                </p>
            </td>
        </tr>
    </table>
</asp:Content>

