<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="GenerateBadges.aspx.vb" Inherits="GenerateBadges"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table  width="90%" align="center">
        <tr bgcolor="#ffffff"  align="center" >
            <td align="left" >
            <asp:HyperLink runat="server" Text="Back to Volunteer Functions" CssClass="btn_02" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
		<tr bgcolor="#ffffff"  align="center" >
		    <td class="Heading" colspan="2">Generate Badge</td>
		</tr>
		<tr>
		    <td>&nbsp;</td>
		</tr>
		<tr>
		    <td align="center">
			    <table align="center" width="60%">	
			       <tr bgcolor="#ffffff" >
					    <td style="text-align:right" width="200px"  colspan="1">
					        Badge Type:
					     </td>
					     <td align="left">&nbsp;&nbsp;
					       <asp:DropDownList runat="server" ID="ddlBadgeType" AutoPostBack="True">
					            <asp:ListItem Text="Select" Value="" Selected="True"></asp:ListItem>
					            <asp:ListItem Text="Participant" Value="Participant" ></asp:ListItem>
					            <asp:ListItem Text="Volunteer" Value="Volunteer"></asp:ListItem>
					            <asp:ListItem Text="Blank Volunteer" Value="Blank Volunteer"></asp:ListItem>
					            <asp:ListItem Text="Blank Guest" Value="Blank Guest"></asp:ListItem>
					       </asp:DropDownList>
                            <asp:RequiredFieldValidator runat="server" ControlToValidate="ddlBadgeType" ID="rfvBadgeType" ErrorMessage="Select Type of Badge" Display="Dynamic" ></asp:RequiredFieldValidator>
					    </td>					
				    </tr>
				      <asp:Panel runat="server" ID="pnlPages" Visible="false" >				        
				        <tr>
				            <td style="text-align:right"  colspan="1">
				                Page Count:
				             </td>
				             <td>
				                &nbsp;&nbsp;<asp:TextBox runat="server" ID="txtPageCount" Text="1"></asp:TextBox>
				            </td>
				        </tr>
				    </asp:Panel>
				    <asp:Panel runat="server" ID="pnlPaperSize" Visible="false" >
				    <tr bgcolor="#ffffff" >
					    <td style="text-align:right"  colspan="1">
					        Badge Paper:
					     </td>
					     <td align="left" >
					       &nbsp;&nbsp;<asp:DropDownList runat="server" ID="ddlPaperSize">
					            <asp:ListItem Text="10 Per Page" Value="10" Selected="True"></asp:ListItem>
					            <asp:ListItem Text="6 Per Page" Value="6"></asp:ListItem>
					       </asp:DropDownList>
					    </td>					
				    </tr>
				    <tr bgcolor="#ffffff" >
				         <td style="text-align:right" >
				           Select Contest Dates: 
				        </td>
					    <td  colspan="1" align="left">	&nbsp;&nbsp;				       
					       <asp:ListBox Rows="4" SelectionMode="Multiple" runat="server" ID="lstContestDates">
					            
					       </asp:ListBox>
					       <asp:Label runat="server" ID="lblChapter"></asp:Label>
					       <asp:RequiredFieldValidator runat="server" ControlToValidate="lstContestDates" ID="rfvContestDates" ErrorMessage="Select Contest Date" Display="Dynamic" ></asp:RequiredFieldValidator>
					    </td>					
				    </tr>
				    <tr runat = "server" id="trrange" visible =false bgcolor="#ffffff" >
				         <td style="text-align:right" >
				           Select Range : 
				        </td>
					    <td align="left">	&nbsp;&nbsp;				       
                            <asp:DropDownList ID="ddlRange" runat="server">
                            </asp:DropDownList>
					       </td>					
				    </tr>
				    <tr>
				        <td  class="ItemCenter" align="left"  colspan="2" >
                            <asp:Button runat="server" ID="btnGenerate" Text="View" CssClass="FormButton" Width="100"  />
                            <asp:Button runat="server" ID="btnExport" Text="Export" CssClass="FormButton" Width="100"  />
                         </td>
				    </tr>
				    </asp:Panel>
				    <asp:Panel runat="server" ID="pnlMessage" Visible="false">
				        <tr bgcolor="#ffffff" >
					        <td style="text-align:center" colspan="2">
					            <asp:Label runat="server" ID="lblMessage" ></asp:Label>
					        </td>
					    </tr> 
				    </asp:Panel>
				</table>
			</td> 
			</tr> 
		</table>
</asp:Content>



 
 
 