﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="DonateAThonStatus.aspx.vb" Inherits="DonateAThonStatus"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left">
 &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration"  runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></div>
<div align="center"  >
  Event Year &nbsp; : <asp:DropDownList ID="ddlEventYear"  AutoPostBack="true"   OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server">
    </asp:DropDownList>
<br /><div align="center" id="div2" runat ="server" >
    
    Select Event : <asp:DropDownList ID="ddlSelection" DataTextField="WName" AutoPostBack="true"  DataValueField="WalkMarID"  OnSelectedIndexChanged="ddlSelection_SelectedIndexChanged" runat="server">
    </asp:DropDownList>
    </div> 
<br /><br /> 
    <asp:HyperLink ID="HlinkCustomPage" Target="_blank" Visible="false"  runat="server">Click Here to View the Custom Page</asp:HyperLink><br /> 
    <asp:Label ID="lblSponsor" Font-Bold="true" runat="server" Text="Thanks to the following donors for their support!!"></asp:Label>
         <br />
<asp:GridView  Width="600px" Visible="false"  ID="GVwList" AutoGenerateColumns="False" runat="server" 
                   BackColor="White" BorderColor="#E7E7FF" BorderWidth="2px" 
                   CellPadding="3" GridLines="Horizontal">
                   <RowStyle BackColor="#FFFFFF" ForeColor="#4A3C8C" />
                   <FooterStyle BackColor="#B5C7DE" ForeColor="#4A3C8C" />
                   <PagerStyle BackColor="#E7E7FF" ForeColor="#4A3C8C" HorizontalAlign="Right" />
                   <SelectedRowStyle BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7" />
                   <HeaderStyle BackColor="#47a81d" Font-Bold="True" ForeColor="#F7F7F7" />
                   <AlternatingRowStyle BackColor="#88c800" />
                   <Columns >
                    <asp:BoundField DataField="Name" HeaderStyle-HorizontalAlign="Left" ItemStyle-HorizontalAlign ="Left"    HeaderText="Name"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                <%--<asp:BoundField  DataField="LastName"   HeaderText="Last Name"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >--%>
                <asp:BoundField  DataField="CreateDate" HeaderStyle-HorizontalAlign="Left"  ItemStyle-HorizontalAlign ="Left"    HeaderText="Date" Visible="false" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataFormatString = "{0:d}"></asp:BoundField>
                <asp:BoundField  DataField="PledgeAmount" HeaderStyle-HorizontalAlign="Left"   ItemStyle-HorizontalAlign ="Left"   HeaderText="Amount"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true" DataFormatString = "{0:c}"></asp:BoundField >
                <asp:BoundField  DataField="Comment" HeaderStyle-HorizontalAlign="Left"   ItemStyle-HorizontalAlign ="Left"   HeaderText="Comment"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true "></asp:BoundField >
                </Columns> 
                </asp:GridView>
                
    <asp:Label ID="lblErr" runat="server" ForeColor = "Red"></asp:Label>
    <div align="center" id="div1" runat ="server" style="height :250px" >
    </div> 
</div>
</asp:Content>

