Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class Reg_DAS
    Inherits System.Web.UI.Page
    Dim strSql As String


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        Dim LoginId As String = Session("LoginID")

        If Not Page.IsPostBack Then
            Dim token As String = Session("entryToken")
            If (Session("entryToken") = "Parent") Then
                LoadChapter()
                LoadChild()
                LoadParent()
                Bind()
                btnsubmit.Enabled = False
                ddlchapter.SelectedItem.Text = ViewState("Chapter")
                ddlchapter.Enabled = False
            Else
                LoadChapter()
                'LoadChild()
                'Response.Write(Session("ChapterID"))
                If Len(Session("ChapterID")) > 0 And Val(Session("RoleId")) > 5 Then
                    ddlchapter.SelectedValue = Session("ChapterID")
                    ddlchapter.Enabled = False
                End If
                drpchild.Enabled = False
                btnregister.Enabled = False
            End If
        End If
    End Sub

    Private Sub LoadChapter()

        strSql = "Select ChapterId, ChapterCode from Chapter"
        If Session("RoleID") = "5" Then 'And Session("SelchapterID") = 1 Then
            strSql = strSql & " Where ChapterID = " & Session("SelchapterID")
        End If
        strSql = strSql & " order by State,chaptercode "
        Dim drchapter As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drchapter = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        While drchapter.Read()
            ddlchapter.Items.Add(New ListItem(drchapter(1).ToString(), drchapter(0).ToString()))
        End While
        If Session("RoleID") = "5" Then 'And Session("SelchapterID") = 1 Then
            ddlchapter.Enabled = False
        Else
            ddlchapter.Items.Insert(0, New ListItem("Select chapter", ""))
        End If
        
    End Sub

    Private Sub LoadChild()
        Dim custindid As String = Session("CustIndID")
        strSql = "Select First_Name +' '+ Middle_initial +' '+ Last_Name 'ChildName' from child where memberid = " + custindid
        Dim drchild As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drchild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        While drchild.Read()
            drpchild.Items.Add(New ListItem(drchild(0).ToString()))
        End While
        drpchild.Items.Insert(0, New ListItem("Select child", ""))

    End Sub

    Private Sub LoadParent()

        Dim LoginId As String = Session("LoginID")
        strSql = "Select firstname, lastname, email, Chapter, Chapterid, address1, state, City, Hphone from indspouse where automemberid = " + LoginId
        Dim dsparent As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsparent = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        txtFname.Text = dsparent.Tables(0).Rows(0)(0)
        txtLname.Text = dsparent.Tables(0).Rows(0)(1)
        txtemail.Text = dsparent.Tables(0).Rows(0)(2)
        Dim Pname As String = txtFname.Text + "" + txtLname.Text
        Dim add As String = dsparent.Tables(0).Rows(0)(5).ToString()
        Dim Cit As String = dsparent.Tables(0).Rows(0)(6).ToString()
        Dim sta As String = dsparent.Tables(0).Rows(0)(7).ToString()
        Dim ph As String = dsparent.Tables(0).Rows(0)(8).ToString()
        'Dim Cnumb As String = dsparent.Tables(0).Rows(0)(1).ToString()
        Dim chapter As String = dsparent.Tables(0).Rows(0)(3).ToString()
        Dim chapterid As String = dsparent.Tables(0).Rows(0)(4).ToString()

        txtFname.Enabled = False
        txtLname.Enabled = False
        txtemail.Enabled = False
        ViewState("pname") = Pname
        ViewState("add") = add
        ViewState("Cit") = Cit
        ViewState("sta") = sta
        ViewState("ph") = ph
        ViewState("Chapter") = chapter
        ViewState("chapterid") = chapterid
        ' ViewState("Cnumb") = Cnumb
    End Sub

    Protected Sub grddsa_RowEditing(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewEditEventArgs)
        grddsa.EditIndex = e.NewEditIndex
        If (Session("entryToken") = "Parent") Then
            Dim LoginId As String = Session("LoginID")
            strSql = "Select c.First_Name+ ' '+ c.Last_Name as ChildName, c.Childnumber, a.firstname + ' ' + a.lastname as ParentName, a.email, a.chapterid, d.DASID, d.Childnumber,"
            strSql = strSql + " d.CreateDate, d.AmountCol, d.DateMailed, d.AmountRec, d.DateReceived, a.address1, a.city, a.state , a.hphone"
            strSql = strSql + " from Child c, IndSpouse a, DAS d where a.automemberID = c.memberid and c.childnumber = d.childnumber and a.automemberID =" + LoginId
            Dim dsdas As DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("dataset1") = dsdas
            If (dsdas.Tables(0).Rows.Count > 0) Then
                grddsa.DataSource = dsdas.Tables(0)
                grddsa.DataBind()
            End If
            'Bind()
            lblerr.Text = ""
            Dim pageIndex As Integer = grddsa.PageIndex
            grddsa.PageIndex = pageIndex
            ViewState("dt") = DirectCast(grddsa.DataSource, DataTable)
        Else
            GridBind()
        End If
    End Sub

    Protected Sub grddsa_RowCancelingEdit(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCancelEditEventArgs)
        grddsa.EditIndex = -1
        If (Session("entryToken") = "Parent") Then
            grddsa.DataSource = DirectCast(ViewState("dt"), DataTable)
            grddsa.DataBind()
        Else
            GridBind()
        End If
    End Sub

    Protected Sub grddsa_RowUpdating(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewUpdateEventArgs)

        Dim dsdas As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        Dim row As GridViewRow = DirectCast(grddsa.Rows(e.RowIndex), GridViewRow)
        Dim DASId As Label = DirectCast(row.FindControl("lbldasid"), Label)
        Dim AmountCol As TextBox = DirectCast(row.FindControl("txtamtcol"), TextBox)
        Dim DateMail As TextBox = DirectCast(row.FindControl("txtdmail"), TextBox)
        Dim AmountRec As TextBox = DirectCast(row.FindControl("txtarec"), TextBox)
        Dim Daterec As TextBox = DirectCast(row.FindControl("txtdrec"), TextBox)
        Dim LoginId As String = Session("LoginID")

        ' grddsa.EditIndex = -1
        cmd.Connection = conn
        conn.Open()
        cmd = New SqlCommand("update DAS Set AmountCol='" & AmountCol.Text & "',DateMailed='" & DateMail.Text & "',AmountRec='" & AmountRec.Text & "',DateReceived='" & Daterec.Text & "', ModifyDate='" & Now() & "', ModifiedBy ='" & LoginId & "' where DASID ='" & DASId.Text & "'", conn)
        cmd.ExecuteNonQuery()
        conn.Close()

        'Dim dsdas As DataSet
        If (AmountCol.Text = "") Then
            strSql = "Update Das set AmountCol = NULL where Amountcol = 0 and dasid = " & DASId.Text
            Dim conn1 As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
        End If
        If (DateMail.Text = "") Then
            strSql = "Update Das set DateMailed = NULL where DateMailed = 0 and dasid = " & DASId.Text
            Dim conn1 As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
        End If
        If (AmountRec.Text = "") Then
            strSql = "Update Das set AmountRec = NULL where AmountRec = 0 and dasid = " & DASId.Text
            Dim conn1 As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
        End If
        If (Daterec.Text = "") Then
            strSql = "Update Das set DateReceived = NULL where DateReceived = 0 and dasid = " & DASId.Text
            Dim conn1 As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
        End If
        
        'grddsa.DataSource = Nothing
        'grddsa.DataBind()

        If (Session("entryToken") = "Parent") Then
            grddsa.EditIndex = -1
            'Dim LoginId As String = Session("LoginID")
            strSql = "Select c.First_Name+ ' '+ c.Last_Name as ChildName, c.Childnumber, a.firstname + ' ' + a.lastname as ParentName, a.email, a.chapterid, d.DASID, d.Childnumber,"
            strSql = strSql + " d.CreateDate, d.AmountCol, d.DateMailed, d.AmountRec, d.DateReceived, a.address1, a.city, a.state , a.hphone"
            strSql = strSql + " from Child c, IndSpouse a, DAS d where a.automemberID = c.memberid and c.childnumber = d.childnumber and a.automemberID =" + LoginId
            'Dim dsdas As DataSet
            'Dim conn As New SqlConnection(Application("ConnectionString"))
            dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            ViewState("dataset1") = dsdas
            If (dsdas.Tables(0).Rows.Count > 0) Then
                grddsa.DataSource = dsdas.Tables(0)
                grddsa.DataBind()
            End If
            'Bind()
            lblerr.Text = ""
            Dim pageIndex As Integer = grddsa.PageIndex
            grddsa.PageIndex = pageIndex
            ViewState("dt") = DirectCast(grddsa.DataSource, DataTable)
            'Bind()
        Else
            GridBind()
        End If
    End Sub

    Protected Sub grddsa_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles grddsa.PageIndexChanging

        grddsa.PageIndex = e.NewPageIndex
        If (Session("entryToken") = "Parent") Then
            grddsa.DataSource = DirectCast(ViewState("dt"), DataTable)
            grddsa.DataBind()
            'Bind()
        Else
            GridBind()
        End If
        'grddsa.DataBind()
    End Sub

    Protected Sub Bind()
        grddsa.DataSource = Nothing
        grddsa.DataBind()

        Dim LoginId As String = Session("LoginID")
        strSql = "Select c.First_Name+ ' '+ c.Last_Name as ChildName, c.Childnumber, a.firstname + ' ' + a.lastname as ParentName, a.email, a.chapterid, d.DASID, d.Childnumber,"
        strSql = strSql + " d.CreateDate, d.AmountCol, d.DateMailed, d.AmountRec, d.DateReceived, a.address1, a.city, a.state , a.hphone"
        strSql = strSql + " from Child c, IndSpouse a, DAS d where a.automemberID = c.memberid and c.childnumber = d.childnumber and a.automemberID =" + LoginId
        Dim dsdas As DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsdas = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        ViewState("dataset1") = dsdas
        If (dsdas.Tables(0).Rows.Count > 0) Then
            grddsa.DataSource = dsdas.Tables(0)
            grddsa.DataBind()

            Dim Count1 As Integer = 0
            If (dsdas.Tables(0).Rows.Count > 0) Then
                Count1 = Convert.ToInt32(dsdas.Tables(0).Rows.Count.ToString())
                Dim i As Integer
                For i = 0 To Count1 - 1
                    Dim AmountRec As String = dsdas.Tables(0).Rows(i)(10).ToString()
                    Dim DateRec As String = dsdas.Tables(0).Rows(i)(11).ToString()
                    If ((AmountRec <> "") And (DateRec <> "")) Then
                        DirectCast(grddsa.Rows(i).Cells(0).FindControl("btnEdit"), LinkButton).Visible = False
                    End If
                    'Session("i") = i
                Next i
            End If
            Dim pageIndex As Integer = grddsa.PageIndex
            grddsa.PageIndex = pageIndex
            ViewState("dt") = DirectCast(grddsa.DataSource, DataTable)
        Else
            lblerr.Text = "No history exists."
        End If

    End Sub

    Protected Sub GridBind()

        If (txtFname.Text = "" And txtLname.Text = "" And txtemail.Text = "") Then
            lblerr1.Text = "Enter atleast one search box"
        Else

            grddsa.DataSource = Nothing
            grddsa.DataBind()


            Dim FirstName As String = txtFname.Text
            Dim lastname As String = txtLname.Text
            Dim email As String = txtemail.Text
            Dim Chapter As String = ddlchapter.SelectedItem.Text
            If Chapter = "Select chapter" Then
                Chapter = ""
            End If
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim prmArray(4) As SqlParameter

            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@FirstName"
            prmArray(0).Value = FirstName
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@LastName"
            prmArray(1).Value = lastname
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter
            prmArray(2).ParameterName = "@email"
            prmArray(2).Value = email
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter
            prmArray(3).ParameterName = "@Chapter"
            prmArray(3).Value = Chapter
            prmArray(3).Direction = ParameterDirection.Input

            Dim ds As DataSet = Nothing
            Try
                ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "RegDAS", prmArray)
            Catch se As SqlException
                'lblmsg.Text = se.Message
                Return
            End Try
            If (ds.Tables(0).Rows.Count > 0) Then
                grddsa.DataSource = ds.Tables(0)
                'grddsa.AutoGenerateEditButton = True
                'cmd = e.Row.FindControl("YourButtonName")
                grddsa.DataBind()
            Else
                lblerr.Text = "No records exist"
            End If
        End If

    End Sub

    Protected Sub btnsubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsubmit.Click
        lblerr.Text = ""
        lblerr1.Text = ""
        GridBind()
    End Sub

    Protected Sub btnregister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnregister.Click

        If drpchild.SelectedItem.Text = "Select child" Then
            lblerr.Text = "please select a child"
        Else
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim Name As String = drpchild.SelectedItem.Text
            strSql = "select Childnumber, Createdate from child where first_name + ' ' + middle_initial + ' ' + last_name = '" + Name + "'"
            Dim ds As New DataSet
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            Dim Cnum As String = ds.Tables(0).Rows(0)(0)
            Dim CrDate As String = ds.Tables(0).Rows(0)(1)
            ViewState("CRDate") = CrDate
            ViewState("Cnum") = Cnum
            lblerr.Text = ""
            lblerr1.Text = ""
            Dim LoginId As String = Session("LoginID")
            strSql = "Select count(*) from DAS where memberid = " & LoginId & " and childnumber = " & ViewState("Cnum") & " and AmountRec is null and DateReceived is null"
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            Dim Count As Integer = ds.Tables(0).Rows(0)(0)
            If Count > 0 Then
                lblerr.Text = "Since a DAS record is currently open for this child, you cannot open a new one until the current record is completed and processed."
                grddsa.Columns(6).Visible = True
                grddsa.Columns(5).Visible = True
                Bind()
            Else
                BtnAdd.Visible = True

                If grddsa.Rows.Count = 0 Then
                    Dim dt As New DataTable
                    Dim dr As DataRow = Nothing

                    'Dim Text1 As New TextBox
                    'Text1.ID = "mytext"
                    'Text1.Text = DataBinder.Eval(dr, "AmountCol")
                    'btnregister.Controls.Add(Text1)

                    dt.Columns.Add(New DataColumn("ChildName", GetType(String)))
                    dt.Columns.Add(New DataColumn("CreateDate", GetType(String)))
                    dt.Columns.Add(New DataColumn("AmountCol", GetType(String)))
                    dt.Columns.Add(New DataColumn("DateMailed", GetType(String)))
                    dt.Columns.Add(New DataColumn("AmountRec", GetType(String)))
                    dt.Columns.Add(New DataColumn("DateReceived", GetType(String)))
                    dt.Columns.Add(New DataColumn("ParentName", GetType(String)))
                    dt.Columns.Add(New DataColumn("address1", GetType(String)))
                    dt.Columns.Add(New DataColumn("city", GetType(String)))
                    dt.Columns.Add(New DataColumn("state", GetType(String)))
                    dt.Columns.Add(New DataColumn("hphone", GetType(String)))
                    dt.Columns.Add(New DataColumn("DASID", GetType(String)))


                    dr = dt.NewRow()
                    dr("ChildName") = drpchild.SelectedItem.Text
                    dr("CreateDate") = ViewState("CRDate")
                    dr("AmountCol") = ""
                    dr("DateMailed") = ""
                    dr("ParentName") = ViewState("pname")
                    dr("address1") = ViewState("add")
                    dr("city") = ViewState("Cit")
                    dr("state") = ViewState("sta")
                    dr("hphone") = ViewState("ph")
                    dr("DASID") = ""
                    dt.Rows.Add(dr)

                    grddsa.DataSource = dt
                    grddsa.DataBind()
                Else
                    grddsa.FooterRow.Visible = True
                    grddsa.Columns(6).Visible = False
                    grddsa.Columns(5).Visible = False
                    grddsa.FooterRow.Cells(1).Text = drpchild.SelectedItem.Text
                    grddsa.FooterRow.Cells(2).Text = ViewState("CRDate")
                    grddsa.FooterRow.Cells(7).Text = ViewState("pname")
                    grddsa.FooterRow.Cells(8).Text = ViewState("add")
                    grddsa.FooterRow.Cells(9).Text = ViewState("Cit")
                    grddsa.FooterRow.Cells(10).Text = ViewState("sta")
                    grddsa.FooterRow.Cells(11).Text = ViewState("ph")
                End If
            End If
        End If
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click

        Dim sb As StringBuilder = New StringBuilder
        Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
        Dim AmountCol As String = DirectCast(grddsa.FooterRow.FindControl("amountcoll"), TextBox).Text
        Dim DateMail As String = DirectCast(grddsa.FooterRow.FindControl("txtdate1"), TextBox).Text
        Dim AmountRec As String = DirectCast(grddsa.FooterRow.FindControl("txtamountrec1"), TextBox).Text
        Dim Daterec As String = DirectCast(grddsa.FooterRow.FindControl("txtDateRec1"), TextBox).Text
        Dim Modifydate As String = ""
        Dim ModifiedBY As String = ""

        sb.Append(" declare @dasID int ")
        sb.Append(" SELECT @dasID = MAX(DASID) ")
        sb.Append(" FROM DAS ")
        sb.Append(" ")
        sb.Append("  if @dasID is null ")
        sb.Append("	begin ")
        sb.Append(" set @dasID = 1 ")
        sb.Append("	end ")
        sb.Append(" else ")
        sb.Append(" begin ")
        sb.Append(" set @dasID = @dasID + 1  ")
        sb.Append("	end ")
        sb.Append("INSERT INTO DAS([DASID],[ChapterID],[ChapterCode],[EventID],[EventCode],[EventYear],[MemberID],")
        sb.Append("[ChildNumber],[AmountCol],[DateMailed],[AmountRec],[DateReceived],[CreateDate],[CreatedBy],[ModifyDate],[ModifiedBy])")
        sb.Append(" VALUES(@dasID,'<CI>','<CC>','<EI>', '<EC>', '<EY>', '<MI>',")
        sb.Append("'<CN>','<AM>','<DM>', '<AR>', '<DR>', '<CD>', '<CB>', '<MD>', '<MB>')")
        sb.Replace("<CI>", ViewState("chapterid"))
        sb.Replace("<CC>", ViewState("Chapter"))
        sb.Replace("<EI>", Session("EventID"))
        sb.Replace("<EC>", "DAS")
        sb.Replace("<EY>", Year(Now))
        sb.Replace("<MI>", Session("LoginID"))
        'sb.Replace("<CN>", ViewState("Cnumb"))
        sb.Replace("<CN>", ViewState("Cnum"))
        sb.Replace("<AM>", AmountCol)
        sb.Replace("<DM>", DateMail)
        sb.Replace("<AR>", AmountRec)
        sb.Replace("<DR>", Daterec)
        sb.Replace("<CD>", Now())
        sb.Replace("<CB>", Session("LoginID"))
        sb.Replace("<MD>", Modifydate)
        sb.Replace("<MB>", ModifiedBY)

        Dim saved As Boolean = True
        Try
            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sb.ToString)
            Dim dsdas As DataSet
            If (AmountCol = "") Then
                strSql = "Update Das set AmountCol = NULL where Amountcol = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            If (DateMail = "") Then
                strSql = "Update Das set DateMailed = NULL where DateMailed = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            If (AmountRec = "") Then
                strSql = "Update Das set AmountRec = NULL where AmountRec = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            If (Daterec = "") Then
                strSql = "Update Das set DateReceived = NULL where DateReceived = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            If (Modifydate = "") Then
                strSql = "Update Das set ModifyDate = NULL where ModifyDate = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            If (ModifiedBY = "") Then
                strSql = "Update Das set ModifiedBy = NULL where ModifiedBy = 0 and dasid = (Select MAX(DASID) from DAS)"
                Dim conn1 As New SqlConnection(Application("ConnectionString"))
                dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)
            End If
            'Dim dsdas As DataSet
            'Dim conn1 As New SqlConnection(Application("ConnectionString"))
            'dsdas = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strSql)

        Catch se As SqlException
            lblerr.Text = "The following error occured while saving the payment information.Please contact for technical support" & _
            "."
            lblerr.Text = (lblerr.Text + se.Message)
            saved = False
        End Try
        BtnAdd.Visible = False
        grddsa.Columns(6).Visible = True
        grddsa.Columns(5).Visible = True

        Bind()
    End Sub

    Protected Sub lnkback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkback.Click

        If (Session("entryToken") = "Parent") Then

            Server.Transfer("UserFunctions.aspx")
        Else
            Server.Transfer("VolunteerFunctions.aspx")

        End If
    End Sub


End Class
