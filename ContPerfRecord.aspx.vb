﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NativeExcel

Partial Class ContPerfRecord
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1"

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                Loadchild(Session("CustIndID"))
                divparent.Visible = True
            ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
                    hlinkParentRegistration.Text = "Back to Volunteer Functions Page"
                    hlinkParentRegistration.NavigateUrl = "Volunteerfunctions.aspx"
                    divparent.Visible = False
                    divVolunteer.Visible = True
                    LoadStates(ddlState)
                Else
                    Server.Transfer("Volunteerfunctions.aspx")
                End If
            End If
            'pnl.visible = false
        End If
    End Sub

    Private Sub LoadStates(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
        ddlst.Items.Insert(0, New ListItem("Select State", "0"))
        ddlst.SelectedIndex = 0
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        divResult.Visible = False
        Dim FNLN As String = String.Empty
        Dim email As String = String.Empty
        Dim state As String = String.Empty
        Dim strSql As New StringBuilder

        FNLN = txtName.Text.Trim
        email = txtEmail.Text
        Dim length As Integer = 0
        If FNLN.Length > 0 Then
            strSql.Append("  ltrim(rtrim(C.First_Name)) + ltrim(rtrim(C.Last_Name)) like '%" + FNLN.Replace(" ", "") + "%'")
        End If

        If email.Length > 0 Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and (I.email like '%" + email + "%' Or s.Email like '%" + email + "%'")
            Else
                strSql.Append(" I.email like '%" + email + "%' Or s.Email like '%" + email + "%'")
            End If
        End If

        If Not ddlState.SelectedItem.Text = "Select State" Then
            length = strSql.ToString().Length
            state = ddlState.SelectedValue
            If (length > 0) Then
                strSql.Append(" and I.state like '%" + state + "%'")
            Else
                strSql.Append("  I.state like '%" + state + "%'")
            End If
        End If
        If FNLN.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by C.last_name,C.first_name")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Name"
            lblIndSearch.Visible = True
        End If
    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select C.ChildNumber,I.Employer,C.first_name,C.last_name,I.email,I.hphone,I.address1,I.city,I.state,I.zip,Ch.chapterCode from Child C INNER JOIN Indspouse I ON C.MemberID=I.AutomemberID LEFT JOIN IndSpouse S on C.MEMBERID = S.Relationship INNER JOIN Chapter Ch ON Ch.ChapterID = I.ChapterID where  " & sqlSt & "")
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        Panel4.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        hdnChildNumber.Value = GridMemberDt.DataKeys(index).Value
        Dim strSql As String = " select First_name+' '+Last_name as Name, ChildNumber from  Child WHERE Childnumber=" & GridMemberDt.DataKeys(index).Value & ""
        Dim drChild As SqlDataReader
        drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        ddlChild.Enabled = True
        If ddlChild.Items.Count > 0 Then
            loadgrid(ddlChild.SelectedValue)
        Else
            ddlChild.Enabled = False
            lblErr.Text = "There is no child to display."
        End If
        loadgrid(GridMemberDt.DataKeys(index).Value)
        Panel4.Visible = False
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = " select First_name+' '+Last_name as Name, ChildNumber from  Child WHERE MemberID=" & ParentID & " ORDER BY First_name,Last_name "
        Dim drChild As SqlDataReader
        drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlChild.DataSource = drChild
        ddlChild.DataBind()
        ddlChild.Enabled = True
        If ddlChild.Items.Count > 0 Then

            loadgrid(ddlChild.SelectedValue)
        Else
            ddlChild.Enabled = False
            lblErr.Text = "There is no child to display."
        End If
    End Sub

    Private Sub loadgrid(ByVal childnumber As String)
        divResult.Visible = True
        Dim DSProfile As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select C.FIRST_NAME,C.LAST_NAME,C.GRADE,datediff(d,c.DATE_OF_BIRTH,GETDATE ())/365 as  Age,C.SchoolName,I.City,I.State, I.Address1,i.HPhone,I.Email,I.FirstName + ' ' + I.LastName as FatherName, Case when I2.FirstName IS Null then '' Else I2.FirstName + ' ' + I2.LastName END as MotherName,CASE WHEN C.Acheivements ='' then 'Acheivement Not Specified' ELSE C.Acheivements END As Acheivements ,CASE WHEN C.Hobbies ='' then 'Hobby Not Specified' ELSE C.Hobbies END As Hobbies  from Child C INNER JOIN IndSpouse I ON C.MEMBERID = I.AutoMemberID AND I.DonorType = 'IND' Left JOIN IndSpouse I2 ON C.MEMBERID = I2.Relationship AND I2.DonorType = 'SPOUSE' WHERE C.ChildNumber =" & childnumber & "")
        If DSProfile.Tables(0).Rows.Count > 0 Then
            FvwProfile.DataSource = DSProfile
            hdnChildName.Value = DSProfile.Tables(0).Rows(0)("FIRST_NAME").ToString() & " " & DSProfile.Tables(0).Rows(0)("LAST_NAME").ToString()
            FvwProfile.DataBind()
            FVwAchiv.DataSource = DSProfile
            FVwAchiv.DataBind()
        Else
            FvwProfile.DataSource = Nothing
            FvwProfile.DataBind()
            FVwAchiv.DataSource = Nothing
            FVwAchiv.DataBind()
        End If
        Dim DSContests As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select Cnst.ContestYear, Ch.ChapterCode , E.Name , P.Name as ProductName, datediff(d,Cnst.CreateDate,GETDATE ())/365 as  Age,Cnst.Grade,Cnst.[RANK] from Contestant Cnst INNER JOIN Child c ON Cnst.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON Cnst.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = Cnst.EventId Inner Join Product P ON P.ProductId = Cnst.ProductId    where Cnst.PaymentReference is Not NULL AND Cnst.ChildNumber=" & childnumber & " Order by Cnst.ContestYear desc,Cnst.ProductId,Cnst.EventId desc")
        If DSContests.Tables(0).Rows.Count > 0 Then
            GVwContests.DataSource = DSContests
            GVwContests.DataBind()
            lblContests.Text = ""
            lblContests.Visible = False
        Else
            lblContests.Text = "No Contest Detail found for " & ddlChild.SelectedItem.Text
            lblContests.Visible = True
            GVwContests.DataSource = Nothing
            GVwContests.DataBind()
        End If

        ' Online Coaching
        Dim dsCoach As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select REG.EventYear, Ch.ChapterCode , E.Name , P.Name as ProductName,REG.Grade from CoachReg REG INNER JOIN Child c ON REG.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON REG.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = REG.EventId Inner Join Product P ON P.ProductId = REG.ProductId  and REG.ChildNumber=" & childnumber & " where REG.Approved='Y' Order by REG.EventYear desc,REG.ProductId,REG.EventId desc")
        If dsCoach.Tables(0).Rows.Count > 0 Then
            gvCoaching.DataSource = dsCoach
            gvCoaching.DataBind()
            lblCoach.Text = ""
            lblCoach.Visible = False
        Else
            lblCoach.Text = "No Coaching Detail found for " & ddlChild.SelectedItem.Text
            lblCoach.Visible = True
            gvCoaching.DataSource = Nothing
            gvCoaching.DataBind()
        End If

        ' Online Workshop
        Dim dsWkshop As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select REG.EventYear, Ch.ChapterCode , E.Name , P.Name as ProductName,REG.Grade from Registration_OnlineWkShop REG INNER JOIN Child c ON REG.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON REG.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = REG.EventId Inner Join Product P ON P.ProductId = REG.ProductId  and REG.ChildNumber=" & childnumber & " where REG.Approved='Y' Order by REG.EventYear desc,REG.ProductId,REG.EventId desc")
        If dsWkshop.Tables(0).Rows.Count > 0 Then
            gvWkShop.DataSource = dsWkshop
            gvWkShop.DataBind()
            lblWkShop.Text = ""
            lblWkShop.Visible = False
        Else
            lblWkShop.Text = "No Online Workshop Detail found for " & ddlChild.SelectedItem.Text
            lblWkShop.Visible = True
            gvWkShop.DataSource = Nothing
            gvWkShop.DataBind()
        End If

        ' Onsite Workshop
        Dim dsOnsiteWkshop As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select REG.EventYear, Ch.ChapterCode , E.Name , P.Name as ProductName,REG.Grade from Registration REG INNER JOIN Child c ON REG.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON REG.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = REG.EventId Inner Join Product P ON P.ProductId = REG.ProductId  and REG.ChildNumber=" & childnumber & " where REG.PaymentReference is not null and REG.EventId=3  Order by REG.EventYear desc,REG.ProductId,REG.EventId desc")
        If dsOnsiteWkshop.Tables(0).Rows.Count > 0 Then
            gvOnsiteWkShop.DataSource = dsOnsiteWkshop
            gvOnsiteWkShop.DataBind()
            lblOnsiteWkShop.Text = ""
            lblOnsiteWkShop.Visible = False
        Else
            lblOnsiteWkShop.Text = "No Onsite Coaching Detail found for " & ddlChild.SelectedItem.Text
            lblOnsiteWkShop.Visible = True
            gvOnsiteWkShop.DataSource = Nothing
            gvOnsiteWkShop.DataBind()
        End If

        ' PrepClub  
        Dim dsPrepClub As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select Prep.EventYear, Ch.ChapterCode , E.Name , P.Name as ProductName,Prep.Grade from registration_prepclub Prep INNER JOIN Child c ON Prep.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON Prep.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = Prep.EventId Inner Join Product P ON P.ProductId = Prep.ProductId  and Prep.ChildNumber=" & childnumber & "  where Prep.Approved='Y'  Order by Prep.EventYear desc,Prep.ProductId,Prep.EventId desc")
        If dsPrepClub.Tables(0).Rows.Count > 0 Then
            gvPrepClub.DataSource = dsPrepClub
            gvPrepClub.DataBind()
            lblPrepClub.Text = ""
            lblPrepClub.Visible = False
        Else
            lblPrepClub.Text = "No PrepClub Detail found for " & ddlChild.SelectedItem.Text
            lblPrepClub.Visible = True
            gvPrepClub.DataSource = Nothing
            gvPrepClub.DataBind()
        End If

        ' Game
        Dim dsGame As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select G.EventYear, Ch.ChapterCode , E.Name , P.Name as ProductName from Game G INNER JOIN Child c ON G.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON G.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = G.EventId Inner Join Product P ON P.ProductId = G.ProductId  and G.ChildNumber=" & childnumber & "  where G.Approved='Y'  Order by G.EventYear desc,G.ProductId,G.EventId desc")
        If dsGame.Tables(0).Rows.Count > 0 Then
            gvGame.DataSource = dsGame
            gvGame.DataBind()
            lblGame.Text = ""
            lblGame.Visible = False
        Else
            lblGame.Text = "No Game Detail found for " & ddlChild.SelectedItem.Text
            lblGame.Visible = True
            gvGame.DataSource = Nothing
            gvGame.DataBind()
        End If
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChild.SelectedIndexChanged
        loadgrid(ddlChild.SelectedValue)
    End Sub
    Protected Sub BtnExportToExcel_Click(sender As Object, e As EventArgs)
        Dim dsRecords As New DataSet
        Dim CmdText As String = "select Cnst.ContestYear, Ch.ChapterCode , E.Name as Contest , P.Name as ProductName,Cnst.Grade,Cnst.[RANK] from Contestant Cnst INNER JOIN Child c ON Cnst.ChildNumber = c.ChildNumber INNER JOIN Chapter Ch ON Cnst.ChapterID = ch.ChapterID INNER JOIN Event E ON E.EventId = Cnst.EventId Inner Join Product P ON P.ProductId = Cnst.ProductId    where Cnst.PaymentReference is Not NULL AND Cnst.ChildNumber=" & hdnChildNumber.Value & " Order by Cnst.ContestYear desc,Cnst.ProductId,Cnst.EventId desc"

        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dtDate As DateTime = DateTime.Now
        Dim month As String = dtDate.ToString("MMM")
        Dim day As String = dtDate.ToString("dd")
        Dim year As String = dtDate.ToString("yyyy")

        Dim monthDay As String = month & "" & day

        Dim Excelname = "ContestantHistory_" & monthDay & "_" & year & ".xls"
        Dim xlWorkBook As IWorkbook = NativeExcel.Factory.CreateWorkbook
        If dt.Rows.Count > 0 Then
            Dim Sheet1 As IWorksheet = xlWorkBook.Worksheets.Add()

            Sheet1.Name = dt.TableName
            Sheet1.Range("A1:F1").MergeCells = True
            Sheet1.Range("A1").Value = hdnChildName.Value
            Sheet1.Range("A1").Font.Bold = True

            Sheet1.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter

            'Sheet1.Range("A3").Value = "Contest Year"
            'Sheet1.Range("A3").Font.Bold = True

            'Sheet1.Range("B3").Value = "Chapter Code"
            'Sheet1.Range("B3").Font.Bold = True

            'Sheet1.Range("C3").Value = "Contest"
            'Sheet1.Range("C3").Font.Bold = True

            'Sheet1.Range("D3").Value = "Product Name"
            'Sheet1.Range("D3").Font.Bold = True

            'Sheet1.Range("E3").Value = "Grade"
            'Sheet1.Range("E3").Font.Bold = True

            'Sheet1.Range("F3").Value = "Rank"
            'Sheet1.Range("F3").Font.Bold = True


            'Dim iRowIndex As Integer = 4
            'Dim CRange As IRange = Nothing
            '' Dim i As Integer = 0

            'For Each dr As DataRow In dsRecords.Tables(0).Rows

            '    CRange = Sheet1.Range("A" + iRowIndex.ToString())
            '    CRange.Value = dr("ContestYear")

            '    CRange = Sheet1.Range("B" + iRowIndex.ToString())
            '    CRange.Value = dr("ChapterCode")

            '    CRange = Sheet1.Range("C" + iRowIndex.ToString())
            '    CRange.Value = dr("Contest")

            '    CRange = Sheet1.Range("D" + iRowIndex.ToString())
            '    CRange.Value = dr("ProductName")

            '    CRange = Sheet1.Range("E" + iRowIndex.ToString())
            '    CRange.Value = dr("Grade")

            '    CRange = Sheet1.Range("F" + iRowIndex.ToString())
            '    CRange.Value = dr("Rank")

            '    iRowIndex = iRowIndex + 1
            '    ' i = i + 1
            'Next

            Dim colCount As Integer = dt.Columns.Count
            Dim i As Integer = 1
            For Each dc As DataColumn In dt.Columns

                Sheet1.Range(2, i).Value = dc.ColumnName

                Sheet1.Range(2, i).Font.Bold = True
                i = i + 1
            Next

            Dim j As Integer
            i = 3
            For Each dr As DataRow In dt.Rows
                For j = 0 To dt.Columns.Count - 1
                    Sheet1.Range(i, j + 1).Value = dr(j).ToString()
                Next
                i = i + 1
            Next

            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" + Excelname)
            xlWorkBook.SaveAs(HttpContext.Current.Response.OutputStream)
            Response.End()

            'Response.Clear()
            '' Response.ContentType = "application/vnd.ms-excel"
            '' Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            'Response.AddHeader("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")
            'Response.AddHeader("Content-Disposition", "attachment;filename=" & Excelname)
            'xlWorkBook.SaveAs(Response.OutputStream, XlFileFormat.xlOpenXMLWorkbook)
            'Response.End()
        End If

    End Sub
End Class
