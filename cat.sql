USE [nsfprod_copy]
GO
/****** Object:  StoredProcedure [dbo].[USP_GetRoleCodeByCategory]    Script Date: 04/29/2014 15:29:25 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
Create Proc [dbo].[USP_GetRoleCodeByCategory] ( @RoleCategory varchar(30) ) as if @RoleCategory = 'Finals' Begin select RoleCode,RoleID from [Role] where Finals = 'Y' End if @RoleCategory = 'Chapter' Begin select RoleCode,RoleID from [Role] where Chapter = 'Y' End  if @RoleCategory = 'National' Begin select RoleCode,RoleID from [Role] where [National] = 'Y' End  if @RoleCategory = 'Zonal' Begin select RoleCode,RoleID from [Role] where Zonal = 'Y' End  if @RoleCategory = 'Cluster' Begin select RoleCode,RoleID from [Role] where Cluster = 'Y' End  if @RoleCategory = 'IndiaChapter' Begin select RoleCode,RoleID from [Role] where IndiaChapter = 'Y' End
GO
