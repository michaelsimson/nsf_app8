﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ContPerfRecord.aspx.vb" Inherits="ContPerfRecord" Title="Performance Record of Contestants" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left">
        <asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:HyperLink>
    </div>
    <div style="width: 1000px; text-align: center;">

        <div style="height: 25px; vertical-align: middle; text-align: center; font-family: Tahoma; COLOR: #003366; font-size: 1.2em; font-weight: 700; Font-style: normal;">Performance Record of Contestants</div>
        <div style="text-align: center" runat="server" id="divparent" visible="false">
            Select Child :
            <asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField="ChildNumber" Width="150px" runat="server"></asp:DropDownList>
            <asp:Label ID="lblErr" runat="server"></asp:Label>
        </div>
        <div style="text-align: center; width: 1000px" runat="server" id="divVolunteer" visible="false">
            <b>Search Child</b>
            <center>
                <table border="1" width="30%" bgcolor="silver">
                    <tr>
                        <td class="ItemLabel" align="right">&nbsp;Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" align="right">&nbsp;Email:</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                    </tr>

                    <tr>
                        <td class="ItemLabel" align="right">&nbsp;State:</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlState" runat="server">
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" />
                        </td>
                        <td align="left">
                            <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" />
                        </td>
                    </tr>
                </table>
            </center>


            <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false"></asp:Label>
            <br />
            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                <b>Search Result</b>
                <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="ChildNumber" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                    <Columns>
                        <asp:ButtonField DataTextField="ChildNumber" HeaderText="Child Number"></asp:ButtonField>
                        <asp:BoundField DataField="First_Name" HeaderText="FirstName"></asp:BoundField>
                        <asp:BoundField DataField="Last_Name" HeaderText="Last Name"></asp:BoundField>
                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                        <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>

                    </Columns>
                </asp:GridView>
            </asp:Panel>
        </div>
        <div style="text-align: center" runat="server" id="divResult" visible="false">
            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="center">
                        <asp:FormView ID="FvwProfile" runat="server">
                            <ItemTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                        <td width="6px" height="6px" background="images/tl.gif"></td>
                                        <td width="300" height="6px" background="images/t.gif"></td>
                                        <td width="11px" height="6px" background="images/tr.gif"></td>
                                    </tr>

                                    <tr>
                                        <td height="200px" background="images/l.gif"></td>
                                        <td>
                                            <table border="0" cellpadding="2" cellspacing="0">
                                                <tr>
                                                    <td align="left" colspan="2" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal;">Child Profile : </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Name </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "FIRST_NAME")%>  <%#DataBinder.Eval(Container.DataItem, "LAST_NAME")%></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Grade </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "Grade")%> <span style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Age :  </span><%#DataBinder.Eval(Container.DataItem, "Age")%></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">School </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "SchoolName")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">City, State </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "City")%>, <%#DataBinder.Eval(Container.DataItem, "State")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Home Address </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "Address1")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Home Phone </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "HPhone")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Home Email </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "Email")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Father Name </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "FatherName")%> </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Mother Name </td>
                                                    <td align="left">: <%#DataBinder.Eval(Container.DataItem, "MotherName")%> </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="images/r.gif"></td>
                                    </tr>

                                    <tr>
                                        <td width="6px" height="12px" background="images/bl.gif"></td>
                                        <td height="tpx" background="images/b.gif"></td>
                                        <td width="11px" height="12px" background="images/br.gif"></td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:FormView>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table1: Contestant History 
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <div style="clear: both; margin-bottom: 1px;"></div>
                        <div style="float: right;">
                            <asp:Button ID="BtnExportToExcel" runat="server" Text="Export to Excel" OnClick="BtnExportToExcel_Click" Style="margin-left: 45px;" />
                        </div>
                        <div style="clear: both; margin-bottom: 1px;"></div>
                        <asp:GridView ID="GVwContests" runat="server" AutoGenerateColumns="false">
                            <Columns>
                                <asp:BoundField DataField="ContestYear" HeaderText="Contest Year"></asp:BoundField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="Name" HeaderText="Contest"></asp:BoundField>
                                <asp:BoundField ItemStyle-HorizontalAlign="Left" DataField="ProductName" HeaderText="Event Name"></asp:BoundField>
                                <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                                <asp:BoundField DataField="Rank" HeaderText="Rank"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblContests" ForeColor="Red" runat="server" Visible="false"></asp:Label>
                    </td>
                </tr>
            </table>
            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="center">
                        <asp:FormView ID="FVwAchiv" runat="server">
                            <ItemTemplate>
                                <table border="0" cellpadding="0" cellspacing="0" align="center">
                                    <tr>
                                        <td width="6px" height="6px" background="images/tl.gif"></td>
                                        <td width="300" height="6px" background="images/t.gif"></td>
                                        <td width="11px" height="6px" background="images/tr.gif"></td>
                                    </tr>

                                    <tr>
                                        <td background="images/l.gif"></td>
                                        <td align="left" style="font-family: Tahoma; font-size: 12px; font-weight: bold">
                                            <table border="0" cellpadding="2" cellspacing="0">
                                                <tr>
                                                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal;">Acheivements & Hobbies : </td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Acheivements</td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><%#DataBinder.Eval(Container.DataItem, "Acheivements")%></td>
                                                </tr>
                                                <tr>
                                                    <td style="text-align: left; font-family: Tahoma; font-size: 11px; font-weight: bold">Hobbies</td>
                                                </tr>
                                                <tr>
                                                    <td align="left"><%#DataBinder.Eval(Container.DataItem, "Hobbies")%> </td>
                                                </tr>
                                            </table>
                                        </td>
                                        <td background="images/r.gif"></td>
                                    </tr>

                                    <tr>
                                        <td width="6px" height="12px" background="images/bl.gif"></td>
                                        <td height="tpx" background="images/b.gif"></td>
                                        <td width="11px" height="12px" background="images/br.gif"></td>
                                    </tr>
                                </table>
                            </ItemTemplate>
                        </asp:FormView>
                    </td>
                </tr>
            </table>

            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table2: Online Coaching History
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:GridView ID="gvCoaching" runat="server">
                        </asp:GridView>
                        <asp:Label ID="lblCoach" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>

            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table3: Online Workshop History
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:GridView ID="gvWkShop" runat="server">
                        </asp:GridView>
                        <asp:Label ID="lblWkShop" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>


            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table 4: Onsite Workshop History
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:GridView ID="gvOnsiteWkShop" runat="server">
                            <Columns>
                            </Columns>
                        </asp:GridView>
                        <asp:Label ID="lblOnsiteWkShop" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>

            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table 5: PrepClub History
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:GridView ID="gvPrepClub" runat="server">
                        </asp:GridView>
                        <asp:Label ID="lblPrepClub" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>

            <table border="0" cellpadding="3" cellspacing="0" align="center">
                <tr>
                    <td align="left" style="height: 25px; vertical-align: middle; font-family: Verdana; COLOR: #333366; font-size: 0.9em; font-weight: 700; Font-style: normal; text-align: center;">Table 6: Game History
                    </td>
                </tr>

                <tr>
                    <td align="center">
                        <asp:GridView ID="gvGame" runat="server">
                        </asp:GridView>
                        <asp:Label ID="lblGame" ForeColor="Red" runat="server" Visible="False"></asp:Label>
                    </td>
                </tr>
            </table>


        </div>
    </div>
    <asp:HiddenField runat="server" ID="hdnChildNumber" Value="0" />
     <asp:HiddenField runat="server" ID="hdnChildName" Value="0" />
</asp:Content>

