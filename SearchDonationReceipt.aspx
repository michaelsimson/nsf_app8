<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" EnableEventValidation="false" AutoEventWireup="true" CodeFile="SearchDonationReceipt.aspx.vb" Inherits="SearchDonationReceipt" title="Search Donation Receipt" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <table cellspacing="1" cellpadding="2" width="90%"  align="center" border="0" >
        <tr>
            <td colspan="4" >                
                <asp:HyperLink ID="hplback" CssClass="btn_02" runat="server" NavigateUrl="~/VolunteerFunctions.aspx" >Back To Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;&nbsp;                                                    
            </td>
        </tr>
        <tr>
            <td align="center" class="Heading" colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td class="Heading" colspan="4" align="center" >Search for Donation Receipt</td>
        </tr>
        <tr>
            <td align="center" colspan="4">&nbsp;
            </td>
        </tr>
        <tr>
            <td colspan="4" align="center" >Search by one or more attributes given below.  Only enter the data you know for sure.</td>
        </tr>
        <tr>
            <td colspan="4" align="center">
                <asp:RadioButtonList runat="server" RepeatDirection="Horizontal" ID="rblIndividual" AutoPostBack="True">
                    <asp:ListItem Text="One Individual" Value="S" Selected="True"></asp:ListItem>
                    <asp:ListItem Text="Multiple Individuals" Value="M"></asp:ListItem>
                     <asp:ListItem Text="Corporations on demand" Value="C"></asp:ListItem>
                </asp:RadioButtonList>
            </td>
        </tr>
        <tr><td colspan="4">&nbsp;</td></tr>
        <tr>
            <td>
                Email:</td>
            <td>
                <asp:TextBox ID="txtemail" runat="server"></asp:TextBox>    
            </td>
            <td>First Name/Organization:</td>
            <td><asp:TextBox ID="txtfirstname" runat="server"></asp:TextBox>    </td>
        </tr>
        <tr>
            <td>Last Name:</td>
            <td>
                <asp:TextBox ID="txtlastname" runat="server" ></asp:TextBox>            
            </td>
            <td>
                Street:</td>
            <td><asp:TextBox ID="txtstreet" runat="server"></asp:TextBox></td>
        </tr>
        <tr>
            <td>
                City:</td>
            <td><asp:TextBox ID="txtcity" runat="server"></asp:TextBox></td>
            <td>
                State:</td>
            <td><asp:DropDownList ID="ddlstate" runat="server">
                
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td>
                Zip:</td>
            <td><asp:TextBox ID="txtzip" runat="server"></asp:TextBox></td>
             <td>NSF Chapter:</td>
            <td><asp:DropDownList ID="ddlchapter" runat="server" >
        
            </asp:DropDownList></td>
        </tr> 
        <tr>
            <td>Calender Year:</td>
            <td><asp:DropDownList runat="server" ID="ddlYear"></asp:DropDownList></td>
            <td>Having Email Address:</td>
            <td><asp:RadioButtonList RepeatDirection="Horizontal" runat="server" ID="rblEmail" >
                <asp:ListItem Text="Yes" Value="Y" Selected></asp:ListItem>
                <asp:ListItem Text="No" Value="N"></asp:ListItem>
            </asp:RadioButtonList></td>
        </tr>          
        
        <tr align="center">
            <td colspan="4" align="center" style="height: 24px" >
                <asp:Button CssClass="FormButton" ID="btnsearch" runat="server" Text="Search" />
                <asp:Button runat="server" ID="btnViewAll" CssClass="FormButton" Text="View All Receipts" />
                <asp:Button runat="server" ID="btnToExcel" CssClass="FormButton" Text="To Excel" Enabled="false" />
                <asp:Button runat="server" ID="btnGenerate" CssClass="FormButton" Text="Generate Receipts" />
                <asp:HyperLink runat="server" ID="hlnkDownload" Text="Download Receipt" NavigateUrl="~/showalldonationreceipts.aspx"></asp:HyperLink><b>[Right click and Save]</b>
            </td>
        </tr>
        <tr align="left">
            <td colspan="4" align="left" style="height: 24px" >
            <asp:Label ID="lblViewMsg" runat="server" Text="**Note: Click on View All Receipts to send emails." ForeColor="Red" Font-Bold="false"></asp:Label>
            
            </td>
        
        </tr>
        
    </table>
    
    <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
        <tr>
            <td align="center">
            <asp:GridView ID="gvDonationReceipt" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="None"  Caption="Donation Receipts" CssClass="GridStyle" CaptionAlign="Top" PageSize="5" BackColor="White" BorderColor="#CCCCCC" BorderWidth="1px" CellPadding="3">
            <Columns>
                <asp:BoundField DataField ="AutoMemberID" HeaderText="Member ID" />                
                 <asp:TemplateField HeaderText="Donation Receipt">
                    <ItemTemplate>
                        <asp:HyperLink runat="server" ID="hlnkDonationReceipt" Text="View Donation Receipt" NavigateUrl="~/ShowDonationReceipt.aspx?MemberId={0}&DonorType={1}"></asp:HyperLink>
                    </ItemTemplate>
                </asp:TemplateField>  
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />               
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="Address2" HeaderText="Address2" />
                <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
                <asp:BoundField DataField="ReferredBy" HeaderText="ReferredBy" />                
                <asp:BoundField DataField="Chapter" HeaderText="Chapter" />
            </Columns>
            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True"  />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        </asp:GridView>
            </td>
        </tr>
       
        <tr>
            <td  class="Heading" colspan="4"><asp:Label runat="server" ID="lblMessage"></asp:Label></td>
        </tr>
    </table>
</asp:Content>



 
 
 