<%@ Page Language="VB" AutoEventWireup="false" CodeFile="CVV.aspx.vb" Inherits="CVV" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
    <b>Card Verification Number Location:</b><br />
    The card verification number for Discover, Visa, Master Card is a 3-digit number 
    printed on the back of your card. It appears after and to the right of your card number.
    <asp:Image runat="server" ID="Imgcvv" ImageUrl="~/Images/cvv.bmp" />
    <br />
    On an American Express card, the verification number is the 4-digit number 
    printed on the front of your card. It always appears above your card number
    and is either on the left side or on the right side of the card. 
    <br />
    <asp:Image runat="server" ID="Imgcvv1" ImageUrl="~/Images/cvv1.bmp" />
    <br />
    </div>
    </form>
</body>
</html>
