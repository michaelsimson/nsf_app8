﻿<%@ Page Language="VB" AutoEventWireup="false" %>

<%@ Import Namespace="System.Collections.Generic" %>

<%@ Import Namespace="System.IO" %>
<%@ Import Namespace="System.Net.Mail" %>
<%@ Import Namespace="Microsoft.ApplicationBlocks.Data" %>
<%@ Import Namespace="Braintree" %>

<script runat="server" language="vb" >
    
    Dim strLogFile As String '= System.Configuration.ConfigurationManager.AppSettings("logFilePath").ToString()
    
    Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        Try
            
            Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, "jcc9t485jznz4kcm", "mknrwcsqs6xvbzs3", "2897e9457f385c31fc9aa0f302063935")
            If Request.HttpMethod = "GET" Then
                If Not Request.QueryString("bt_challenge") Is Nothing Then
                    Response.Write(gateway.WebhookNotification.Verify(Request.QueryString("bt_challenge")))
                End If
            ElseIf Request.HttpMethod = "POST" Then
                Dim strLogMessage As String = ""
                Try
                    strLogMessage = "================= Log Details ===================\n"
                    Dim cmdText As String, BT_PaymentRef As String, BT_TransId As String, BT_RefundRef As String, BT_ETG_Response As String
                    Dim BT_Token As String, BT_Brand As String, ccF, ccM, ccL As String, MS_TransDate As String, BT_DisbDate As String
                    Dim EMail, CardHName As String, TransType, CreatedBy As String, CreateDate As String, BT_CustomerId As String
                    Dim MS_Amount As Decimal
                    Dim isValid As Boolean = False
                    Dim webhook As WebhookNotification = gateway.WebhookNotification.Parse(Request.Params("bt_signature"), Request.Params("bt_payload"))
                
                    strLogMessage = strLogMessage & "\n Gateway : "
                                        
                    Try
                        strLogMessage = strLogMessage & " Single Transaction : " & webhook.Transaction.Id.ToString() & "\n"
                        Dim transSingle As Transaction = webhook.Transaction
                        MS_Amount = Nothing
                        MS_TransDate = ""
                        ccF = ""
                        ccM = ""
                        ccL = ""
                        BT_Brand = ""
                        BT_ETG_Response = ""
                        BT_PaymentRef = ""
                        BT_TransId = ""
                        BT_CustomerId = ""
                        BT_RefundRef = ""
                        BT_Token = ""
                        EMail = ""
                        CardHName = ""
                        BT_RefundRef = ""
                        TransType = ""
                        CreateDate = ""
                        BT_DisbDate = ""
                        CreatedBy = ""
                    
                        BT_TransId = transSingle.Id
                        BT_CustomerId = transSingle.Customer.Id
                        BT_PaymentRef = transSingle.Id + "-" + transSingle.Customer.Id
                        If Not transSingle.RefundedTransactionId Is Nothing Then
                            BT_RefundRef = transSingle.RefundedTransactionId + "-" + transSingle.Customer.Id
                        End If
                        BT_Brand = transSingle.CreditCard.CardType.ToString
                        BT_Token = transSingle.CreditCard.Token
                        Try
                            MS_Amount = Convert.ToDecimal(transSingle.Amount)
                            If Not transSingle.RefundedTransactionId Is Nothing Then
                                MS_Amount = "-" & MS_Amount
                            End If
                            ccF = Left(transSingle.CreditCard.MaskedNumber.ToString, 4)
                            ccM = Right(Left(transSingle.CreditCard.MaskedNumber.ToString, 6), 2)
                            ccL = Right(transSingle.CreditCard.MaskedNumber.ToString, 4)
                            If transSingle.Customer.Email Is Nothing Then
                                EMail = ""
                            Else
                                EMail = transSingle.Customer.Email.ToString
                            End If
                            If Not transSingle.CreditCard.CardholderName Is Nothing Then
                                CardHName = transSingle.CreditCard.CardholderName.ToString
                            Else
                                CardHName = ""
                            End If
                            MS_TransDate = transSingle.CreatedAt
                            BT_ETG_Response = transSingle.ProcessorAuthorizationCode
                            If Not transSingle.DisbursementDetails.DisbursementDate.ToString Is Nothing Then
                                BT_DisbDate = transSingle.DisbursementDetails.DisbursementDate.ToString
                            End If
                        Catch ex As Exception
                            strLogMessage = strLogMessage & (" \n 0.Single Trans : Error on Brain Tree \n\r\r" & ex.ToString)
                        End Try
                        cmdText = "select count(*) from MSChargeTemp where PaymentReference='" & BT_PaymentRef & "'"
                        strLogMessage = strLogMessage & "\n 1. DUPLICATE CHECKING \n \r\r A. Payment Reference :" & cmdText
                        Dim iCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                        cmdText = "select ETG_Response,TransType, CreateDate,CreatedBy from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'"
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                        strLogMessage = strLogMessage & "\n B.Single Trans Ccsubmitlog :"
                        If ds.Tables(0).Rows.Count > 0 Then
                            TransType = ds.Tables(0).Rows(0)("TransType").ToString
                            CreateDate = ds.Tables(0).Rows(0)("CreateDate").ToString
                            CreatedBy = ds.Tables(0).Rows(0)("CreatedBy").ToString
                        End If
                        If iCnt > 0 Then
                            cmdText = "Update MSChargeTemp Set MS_TransDate=CONVERT(DATETIME,'" & MS_TransDate & "',101),DisbDate=CONVERT(DATETIME,"
                            If BT_DisbDate = "" Then
                                cmdText = cmdText & "NULL"
                            Else
                                cmdText = cmdText & "'" & BT_DisbDate & "'"
                            End If
                            cmdText = cmdText & ",101),ID='" & BT_TransId & "',CustomerId='" & BT_CustomerId & "',RefundReference='" & BT_RefundRef & "', Brand='" & BT_Brand & "' , Token='" & BT_Token & "',TransType='" & TransType & "',Email='" & EMail & "', CHName='" & CardHName & "',MS_CN1='" & ccF & "', MS_CN2='" & ccM & "', MS_CN3='" & ccL & "',CreateDate=CONVERT(DATETIME," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",101) , CreatedBy=" & IIf(CreatedBy = "", "NULL", CreatedBy) & ", ModifiedDate=getdate(), MS_Amount=" & MS_Amount & ", AuthCode=" & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ",DLDate=CONVERT(varchar(10),getdate(),101) where PaymentReference = '" & BT_PaymentRef & "'"
                            strLogMessage = strLogMessage & ("\n 2.Single Trans UPDATE \n\r PaymentReference : " & cmdText)
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                            strLogMessage = strLogMessage & (" \n 2.1. UPDATED ....")
                        Else
                            cmdText = "insert into MSChargeTemp(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,DLDate) values (CONVERT(DATETIME,'" & MS_TransDate & "',101) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",101),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & TransType & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',Convert(Datetime," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",101)," & IIf(CreatedBy = "", "NULL", CreatedBy) & ", " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ",CONVERT(varchar(10),getdate(),101)) "
                            strLogMessage = strLogMessage & (" \n 3. INSERT \n\r PaymentReference: " & cmdText)
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                            strLogMessage = strLogMessage & (" \n 3.1. INSERTED ....")
                        End If
                    Catch ex As Exception
                        strLogMessage = strLogMessage & "\n Single Trans Error : " & ex.ToString
                    End Try
                    
                    Try
                        strLogMessage = strLogMessage & "\n Subscription id : " & webhook.Subscription.Id.ToString()
                    Catch exT As Exception
                        strLogMessage = strLogMessage & "\n Error on Subscription id :" & exT.ToString()
                    End Try
                    Try
                        strLogMessage = strLogMessage & "\n Disbursement Id: " & webhook.Disbursement.Id.ToString
                        strLogMessage = strLogMessage & "\n Disbursement Id: " & webhook.Disbursement.TransactionIds.Count.ToString
                        strLogMessage = strLogMessage & "\n Disbursement Id: " & webhook.Disbursement.Transactions.FirstItem.Id.ToString
                    Catch exT As Exception
                        strLogMessage = strLogMessage & "\n Error on Disbursement Id :" & exT.ToString()
                    End Try
                                
                    Dim t As Transaction
                    Dim b As Subscription = gateway.Subscription.Find(webhook.Subscription.Id)
                    strLogMessage = strLogMessage & "\n Subscription Find : "
                    Dim coll As List(Of Transaction) = b.Transactions
                    strLogMessage = strLogMessage & "\n Subscription List : "
                    For Each t In coll
                        strLogMessage = strLogMessage & "\n Transaction : " & t.Id.ToString
                        Try
                            MS_Amount = Nothing
                            MS_TransDate = ""
                            ccF = ""
                            ccM = ""
                            ccL = ""
                            BT_Brand = ""
                            BT_ETG_Response = ""
                            BT_PaymentRef = ""
                            BT_TransId = ""
                            BT_CustomerId = ""
                            BT_RefundRef = ""
                            BT_Token = ""
                            EMail = ""
                            CardHName = ""
                            BT_RefundRef = ""
                            TransType = ""
                            CreateDate = ""
                            BT_DisbDate = ""
                            CreatedBy = ""
                    
                            BT_TransId = t.Id
                            BT_CustomerId = t.Customer.Id
                            BT_PaymentRef = t.Id + "-" + t.Customer.Id
                            If Not t.RefundedTransactionId Is Nothing Then
                                BT_RefundRef = t.RefundedTransactionId + "-" + t.Customer.Id
                            End If
                            BT_Brand = t.CreditCard.CardType.ToString
                            BT_Token = t.CreditCard.Token
                            Try
                                MS_Amount = Convert.ToDecimal(t.Amount)
                                If Not t.RefundedTransactionId Is Nothing Then
                                    MS_Amount = "-" & MS_Amount
                                End If
                                ccF = Left(t.CreditCard.MaskedNumber.ToString, 4)
                                ccM = Right(Left(t.CreditCard.MaskedNumber.ToString, 6), 2)
                                ccL = Right(t.CreditCard.MaskedNumber.ToString, 4)
                                If t.Customer.Email Is Nothing Then
                                    EMail = ""
                                Else
                                    EMail = t.Customer.Email.ToString
                                End If
                                If Not t.CreditCard.CardholderName Is Nothing Then
                                    CardHName = t.CreditCard.CardholderName.ToString
                                Else
                                    CardHName = ""
                                End If
                                MS_TransDate = t.CreatedAt
                                BT_ETG_Response = t.ProcessorAuthorizationCode
                                If Not t.DisbursementDetails.DisbursementDate.ToString Is Nothing Then
                                    BT_DisbDate = t.DisbursementDetails.DisbursementDate.ToString
                                End If
                            Catch ex As Exception
                                strLogMessage = strLogMessage & (" \n 0. Error on Brain Tree \n\r\r" & ex.ToString)
                            End Try
                            cmdText = "select count(*) from MSChargeTemp where PaymentReference='" & BT_PaymentRef & "'"
                            strLogMessage = strLogMessage & "\n 1. DUPLICATE CHECKING \n \r\r A. Payment Reference :" & cmdText
                            Dim iCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                            cmdText = "select ETG_Response,TransType, CreateDate,CreatedBy from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'"
                            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                            strLogMessage = strLogMessage & "\n B. Ccsubmitlog :"
                            If ds.Tables(0).Rows.Count > 0 Then
                                TransType = ds.Tables(0).Rows(0)("TransType").ToString
                                CreateDate = ds.Tables(0).Rows(0)("CreateDate").ToString
                                CreatedBy = ds.Tables(0).Rows(0)("CreatedBy").ToString
                            End If
                            If iCnt > 0 Then
                                cmdText = "Update MSChargeTemp Set MS_TransDate=CONVERT(DATETIME,'" & MS_TransDate & "',101),DisbDate=CONVERT(DATETIME,"
                                If BT_DisbDate = "" Then
                                    cmdText = cmdText & "NULL"
                                Else
                                    cmdText = cmdText & "'" & BT_DisbDate & "'"
                                End If
                                cmdText = cmdText & ",101),ID='" & BT_TransId & "',CustomerId='" & BT_CustomerId & "',RefundReference='" & BT_RefundRef & "', Brand='" & BT_Brand & "' , Token='" & BT_Token & "',TransType='" & TransType & "',Email='" & EMail & "', CHName='" & CardHName & "',MS_CN1='" & ccF & "', MS_CN2='" & ccM & "', MS_CN3='" & ccL & "',CreateDate=CONVERT(DATETIME," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",101) , CreatedBy=" & IIf(CreatedBy = "", "NULL", CreatedBy) & ", ModifiedDate=getdate(), MS_Amount=" & MS_Amount & ", AuthCode=" & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ",DLDate=CONVERT(varchar(10),getdate(),101) where PaymentReference = '" & BT_PaymentRef & "'"
                                strLogMessage = strLogMessage & ("\n 2. UPDATE \n\r PaymentReference : " & cmdText)
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                            Else
                                cmdText = "insert into MSChargeTemp(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode,DLDate) values (CONVERT(DATETIME,'" & MS_TransDate & "',101) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",101),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & TransType & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',Convert(Datetime," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",101)," & IIf(CreatedBy = "", "NULL", CreatedBy) & ", " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ",CONVERT(varchar(10),getdate(),101)) "
                                strLogMessage = strLogMessage & (" \n 3. INSERT \n\r PaymentReference: " & cmdText)
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                            End If
                        Catch ex As Exception
                            strLogMessage = strLogMessage & "\n Error : " & ex.ToString
                        End Try
                    Next
                    strLogMessage = strLogMessage & "\n 4. Records exists in Brain Tree : " & isValid
                Catch ex1 As Exception
                    strLogMessage = strLogMessage & (" \n 5. Error on Brain Tree Web Service \n\r\r" & ex1.ToString)
                End Try
                writeToLogFile(strLogMessage)
                SendMessage("Brain Tree Webhook Subscription ", "Webhook Subscription Details", "")
                
            End If
        Catch ex As Exception
            Response.Write("Err:" & ex.ToString())
        End Try
    End Sub
    Public Sub writeToLogFile(logMessage As String)
        strLogFile = "~\BTWebhookLog_Subscription.txt"
        Dim strPath As String = Server.MapPath(strLogFile)
        Dim strLogMessage As String = ""
        Try
            Dim swLog As StreamWriter
            strLogMessage = String.Format("{0}: {1}", Date.Now, logMessage)
            If (Not File.Exists(strLogFile)) Then
                swLog = New StreamWriter(Server.MapPath(strLogFile))
            Else
                swLog = File.AppendText(strLogFile)
            End If
            swLog.WriteLine(strLogMessage)
            swLog.WriteLine()
            swLog.Close()
        Catch ex As Exception
        End Try
    End Sub
    Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@northsouth.org")
        email.To.Add("bindhu.rajalakshmi@capestart.com")
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(Server.MapPath(strLogFile))
        email.Attachments.Add(oAttch)

        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Try
            client.Send(email)
        Catch ex As Exception
            'lblMessage.Text = e.Message.ToString
            ok = False
        End Try

    End Sub
     
</script>
