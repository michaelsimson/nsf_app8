﻿using System;
using System.Collections.Generic;

using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Drawing;
using System.Collections;


public partial class AddUpdSpashPageContents : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        lblerr.Text = "";
        if (!IsPostBack)
        {

            PopulateYear(ddlYear);
            LoadEvent(ddEvent);
            loadAllContents();
        }
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;

            ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= MaxYear - 1; i--)
            {
                list.Add(new ListItem(i.ToString() + "-" + (i + 1).ToString().Substring(2, 2), i.ToString()));

            }

            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();

            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));

            ddlObject.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        }
        catch (Exception ex) { }

    }


    public void LoadEvent(DropDownList ddlObject)
    {
        try
        {

            string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID");
            //  Where EF.EventYear ="
            //+ (ddlYear.SelectedValue + ""))
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 0))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                }
                else
                {
                    ddlObject.Enabled = false;

                }

            }
            else
            {

                lblerr.Text = "No Events present for the selected year";
            }
        }
        catch
        {
        }
    }
    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlYear.SelectedValue != "-1")
        {
            LoadEvent(ddEvent);
        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        InsertUpdateSession();

        loadAllContents();
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        btnSubmit.Text = "Save";
        ddlYear.SelectedValue = "-1";
        ddEvent.SelectedValue = "-1";
        txtContentText.Text = "";
    }
    public string validateContents()
    {
        string retval = "1";
        if (ddlYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retval = "-1";
        }
        else if (ddEvent.SelectedValue == "" || ddEvent.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Event";
            retval = "-1";
        }
        else if (ddlContentType.SelectedValue == "" || ddlContentType.SelectedValue == "-1")
        {
            lblerr.Text = "Please select ContentType";
            retval = "-1";
        }
        else if (txtContentText.Text == "")
        {
            lblerr.Text = "Please fill Content Body";
            retval = "-1";
        }
        return retval;
    }

    public void InsertUpdateSession()
    {
        try
        {
            if (validateContents() == "1")
            {
                string CmdText = string.Empty;
                string productGroup = string.Empty;
                string ProductGroupID = string.Empty;
                string Product = string.Empty;
                string ProductID = string.Empty;
                string Msg = string.Empty;

                if (btnSubmit.Text == "Save")
                {
                    Msg = "Inserted Successfully";
                }
                else
                {
                    Msg = "Updated Successfully";
                }
                if (btnSubmit.Text == "Save")
                {
                    CmdText = "Insert into SplashPageContents (EventID,EventYear,ContentType,ContentText,CreatedDate,CreatedBy) values (" + ddEvent.SelectedValue + "," + ddlYear.SelectedValue + ",'" + ddlContentType.SelectedValue + "','" + txtContentText.Text + "',GetDate()," + Session["LoginID"].ToString() + ")";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = Msg;
                    btnSubmit.Text = "Save";
                }
                else
                {
                    CmdText = "update SplashPageContents set EventID=" + ddEvent.SelectedValue + ", EventYear=" + ddlYear.SelectedValue + ", ContentType='" + ddlContentType.SelectedValue + "', ContentText='" + txtContentText.Text + "', ModifiedDate=GetDate(), ModifiedBy=" + Session["LoginID"].ToString() + " where ContentID=" + hdnContentID.Value + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblerr.Text = Msg;
                    btnSubmit.Text = "Save";
                }

            }
        }

        catch (Exception ex)
        {
            // Response.Write(ex.ToString());
        }
    }

    public void loadAllContents()
    {
        string cmdText = string.Empty;
        DataSet ds;
        cmdText = "select cc.ContentID,E.EventID,E.name as EventName, CC.EventYear, CC.ContentType, CC.ContentText from SplashPageContents CC inner join Event E on (CC.EventID=E.EventID)";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0].Rows.Count > 0)
        {
            GrdLevel.DataSource = ds;
            GrdLevel.DataBind();
        }
        else
        {
            //GrdLevel.DataSource = null;
            //GrdLevel.DataBind();
            GrdLevel.Visible = false;
        }
    }
    protected void GrdLevel_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Modify")
            {
                btnSubmit.Text = "Update";
                GridViewRow row = null;
                GrdLevel.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GrdLevel.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");


                ddlYear.SelectedValue = ((Label)GrdLevel.Rows[selIndex].FindControl("lblYear") as Label).Text;
                LoadEvent(ddEvent);
                ddEvent.SelectedValue = ((Label)GrdLevel.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ddlContentType.SelectedValue = ((Label)GrdLevel.Rows[selIndex].FindControl("lblContentType") as Label).Text.ToString().Trim();

                string contentBody = string.Empty;
                contentBody = ((Label)GrdLevel.Rows[selIndex].FindControl("lblContent") as Label).Text;
                txtContentText.Text = contentBody;

                string contentID = ((Label)GrdLevel.Rows[selIndex].FindControl("lblContentID") as Label).Text;
                hdnContentID.Value = contentID;
            }
            else if (e.CommandName == "DeleteContent")
            {
                GridViewRow row = null;
                GrdLevel.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string contentID = ((Label)GrdLevel.Rows[selIndex].FindControl("lblContentID") as Label).Text;
                hdnContentID.Value = contentID;

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteConfirm();", true);
            }
        }
        catch
        {

        }
    }

    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        try
        {
            string CmdText = "delete from SplashPageContents where ContentID=" + hdnContentID.Value + "";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            lblerr.Text = "Level deleted successfully";
            loadAllContents();
        }
        catch (Exception ex)
        {
        }
    }
}