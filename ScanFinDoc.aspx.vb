﻿
Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ScanFinDoc
    Inherits System.Web.UI.Page
    Dim strSql As String
    Dim formatstring As String
    Dim value As Object
    Dim srcfilename As String
    Dim Eventyear As String
    Dim Transcat As String
    Dim BankId As String
    Dim Transdate As String
    Dim chkdepnumber As String
    Dim dblRegFee As Double
    Dim BusType As String
    Dim IRSCat As String
    Dim ChapterCode As String
    Dim Amount As String
    Dim DonationType As String
    Dim DonationID As Integer
    Dim DonorType As String
    Dim setFlag As Boolean = False
    Dim dsDonation As New DataSet

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("Maintest.aspx")
            End If
            If Not IsPostBack Then
                TrDetailView.Visible = False
                Try
                    If Request.QueryString("id") = 1 Then
                        TxtFrom.Text = Session("Frmdate")
                        txtTo.Text = Session("ToDate")
                        ddlCategory.SelectedIndex = ddlCategory.Items.IndexOf(ddlCategory.Items.FindByValue(Session("DCatID")))
                        If ddlCategory.SelectedValue = 3 Then
                           
                        ElseIf ddlCategory.SelectedValue = 4 Then
                            
                            'ElseIf ddlCategory.SelectedValue = 1 Then
                            '    BtnDetRawData.Visible = True
                            '    BtnExportList.Visible = True
                        Else
                           
                        End If
                        GetDonationReceipt(Session("ManageVouchersSQl"))
                        If Session("DCatID") = 4 Then
                            loadbank()
                            trbanks.Visible = True
                        End If
                    End If
                Catch ex As Exception
                End Try
                '' Show GeneralLedger table Data
                'Response.Write("<script language='javascript'>")
                'Response.Write(" window.open('ShowGeneralLedger.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');")
                'Response.Write("</script>")
            End If

        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub


    'lstBank
    Private Sub loadbank()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank WHERE BankID in (1,2,3,4,5,6,7)")
        lstBank.DataSource = ds
        lstBank.DataTextField = "BankCode"
        lstBank.DataValueField = "BankID"
        lstBank.DataBind()
        lstBank.Items.Insert(0, New ListItem("All", 0))
        lstBank.SelectedIndex = 0

    End Sub

    Private Sub GetDonationReceipt(ByVal StrSQL As String)
        'gvDonation

        Dim conn As New SqlConnection(Application("ConnectionString"))
        Session("StrSQL") = StrSQL
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL)


        If dsDonation.Tables(0).Rows.Count > 0 Then
            If ddlCategory.SelectedValue = 3 Then
               
            ElseIf ddlCategory.SelectedValue = 4 Then
               
                'ElseIf ddlCategory.SelectedValue = 1 Then
                '    BtnDetRawData.Visible = True
                '    BtnExportList.Visible = True
                'ElseF()

            End If
        End If


        If ddlCategory.SelectedValue = 1 Or ddlCategory.SelectedValue = 2 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                gvDonation.Visible = True
                GVCCRevenues.Visible = False
                GVTDAIncome.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                gvExp.Visible = False
               
                setFlag = False
                gvDonation.DataSource = dsDonation.Tables(0)
                gvDonation.DataBind()
                If setFlag = True Then

                Else

                End If
            Else
                gvDonation.DataSource = Nothing
                gvDonation.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = 5 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                gvDonation.Visible = False
                GVCCRevenues.Visible = False
                GVTDAIncome.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                gvExp.Visible = True
               

                'setFlag = FalseBtnSubmit_Click
                gvExp.DataSource = dsDonation.Tables(0)
                gvExp.DataBind()
                If gvExp.Rows.Count > 0 Then
                    For i As Integer = 0 To gvExp.Rows.Count
                        gvExp.Columns(5).ItemStyle.ForeColor = Color.Black
                        gvExp.Columns(1).Visible = True
                        gvExp.Columns(2).Visible = True
                    Next
                End If

                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                Dim StrSQLExp As String = "Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber,'Not Available' as DateCashed, E.DatePaid,"
                StrSQLExp = StrSQLExp + "'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,"
                StrSQLExp = StrSQLExp + " E.BankID From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)  WHERE E.CheckNumber is Not null "
                StrSQLExp = StrSQLExp + " AND (E.Account is Not null or E.TransType = 'Transfers') AND E.DatePaid Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' "
                StrSQLExp = StrSQLExp + " Group BY E.DatePaid, E.CheckNumber,E.BankID ORDER BY E.CheckNumber,E.DatePaid "

                Dim dsExp1 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQLExp)
                If dsExp1.Tables(0).Rows.Count > 0 Then
                    TrDonation.Visible = True
                    gvDonation.Visible = False
                    GVCCRevenues.Visible = False
                    GVTDAIncome.Visible = False
                    GVACT_EFTDon.Visible = False
                    GVBnk_CCFees.Visible = False
                    GVTransfers.Visible = False
                    GVBuyTrans.Visible = False
                    gvExp.Visible = True
                   

                    gvExp.DataSource = dsExp1.Tables(0)
                    gvExp.DataBind()
                    For i As Integer = 0 To gvExp.Rows.Count
                        gvExp.Columns(5).ItemStyle.ForeColor = Color.Red
                        gvExp.Columns(1).Visible = False
                        gvExp.Columns(2).Visible = False
                    Next
                    
                Else
                    gvExp.DataSource = Nothing
                    gvExp.DataBind()
                    lblErr.Text = "Sorry No data found"
                    gvExp.Visible = False
                    gvDonation.Visible = False
                    GVCCRevenues.Visible = False
                    GVACT_EFTDon.Visible = False
                    GVBnk_CCFees.Visible = False
                End If
            End If
        ElseIf ddlCategory.SelectedValue = 3 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
               
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVCCRevenues.Visible = True
                ' btnGenAll.Visible = True
                'setFlag = False
                GVCCRevenues.DataSource = dsDonation.Tables(0)
                GVCCRevenues.DataBind()
                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                GVCCRevenues.DataSource = Nothing
                GVCCRevenues.DataBind()
                lblErr.Text = "Sorry No data found"
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = 4 Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
               
                gvDonation.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVTDAIncome.Visible = True
                ' btnGenAll.Visible = True
                'setFlag = False
                'GVTDAIncome.Columns(6).Visible = True
                GVTDAIncome.DataSource = dsDonation.Tables(0)
                GVTDAIncome.DataBind()
                'GVTDAIncome.Columns(6).Visible = False
                'If setFlag = True Then
                '    btnGenAll.Attributes.Add("onclick", "return confirm('Not reconciled for all rows. Do you still want a QB file?');")
                'Else
                '    btnGenAll.Attributes.Remove("onclick")
                'End If
            Else
                GVTDAIncome.DataSource = Nothing
                GVTDAIncome.DataBind()
                lblErr.Text = "Sorry No data found"
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                gvExp.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "6" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
               
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVACT_EFTDon.Visible = True
                GVACT_EFTDon.DataSource = dsDonation.Tables(0)
                GVACT_EFTDon.DataBind()
            Else
                GVACT_EFTDon.DataSource = Nothing
                GVACT_EFTDon.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "7" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
               
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = False
                GVBnk_CCFees.Visible = True
                'GVBnk_CCFees.Columns(2).Visible = True
                'GVBnk_CCFees.Columns(9).Visible = True
                GVBnk_CCFees.DataSource = dsDonation.Tables(0)
                GVBnk_CCFees.DataBind()
                'GVBnk_CCFees.Columns(2).Visible = False
               
            Else
                GVBnk_CCFees.DataSource = Nothing
                GVBnk_CCFees.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "8" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
               
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVBuyTrans.Visible = False
                GVTransfers.Visible = True
                GVTransfers.Columns(14).Visible = True
                GVTransfers.Columns(2).Visible = True
                GVTransfers.Columns(13).Visible = True
                GVTransfers.Columns(6).Visible = True
                GVTransfers.Columns(8).Visible = True
                GVTransfers.Columns(9).Visible = True

                GVTransfers.DataSource = dsDonation.Tables(0)
                GVTransfers.DataBind()
                Dim AvgValue As Double

                For i As Integer = 0 To GVTransfers.Rows.Count - 1
                    If GVTransfers.Rows(i).Cells(10).Text = 0 Then
                        AvgValue = (Convert.ToDouble(GVTransfers.Rows(i).Cells(14).Text.ToString()) * GetAvgPrice(GVTransfers.Rows(i).Cells(2).Text, GVTransfers.Rows(i).Cells(13).Text, GVTransfers.Rows(i).Cells(14).Text, GVTransfers.Rows(i).Cells(6).Text, GVTransfers.Rows(i).Cells(8).Text, GVTransfers.Rows(i).Cells(9).Text))
                        GVTransfers.Rows(i).Cells(10).Text = String.Format("{0:c}", AvgValue)
                        'Format$(GVTransfers.Rows(i).Cells(10).Text, "Currency")
                    End If
                Next

                'For i As Integer = 0 To dsDonation.Tables(0).Rows.Count - 1
                '    If dsDonation.Tables(0).Rows(i)("Amount") = 0 Then
                '        AvgValue = (Convert.ToDouble(dsDonation.Tables(0).Rows(i)("Quantity")) * GetAvgPrice(dsDonation.Tables(0).Rows(i)("BankID"), dsDonation.Tables(0).Rows(i)("Ticker"), dsDonation.Tables(0).Rows(i)("Quantity"), dsDonation.Tables(0).Rows(i)("TransDate"), TxtFrom.Text, txtTo.Text))
                '        dsDonation.Tables(0).Rows(i)("Amount") = AvgValue
                '        'Format$(GVTransfers.Rows(i).Cells(10).Text, "Currency")
                '        Response.Write(AvgValue)
                '    End If
                'Next


                'GVTransfers.Columns(13).Visible = False
                ' GVTransfers.Columns(14).Visible = False

            Else
                GVTransfers.DataSource = Nothing
                GVTransfers.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
            End If
        ElseIf ddlCategory.SelectedValue = "9" Or ddlCategory.SelectedValue = "10" Then
            If dsDonation.Tables(0).Rows.Count > 0 Then
                TrDonation.Visible = True
                
                gvDonation.Visible = False
                GVTDAIncome.Visible = False
                gvExp.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
                GVBuyTrans.Visible = True
                GVBuyTrans.DataSource = dsDonation.Tables(0)
                GVBuyTrans.DataBind()
            Else
                GVBuyTrans.DataSource = Nothing
                GVBuyTrans.DataBind()
                lblErr.Text = "Sorry No data found"
                gvDonation.Visible = False
                gvExp.Visible = False
                GVTDAIncome.Visible = False
                GVCCRevenues.Visible = False
                GVACT_EFTDon.Visible = False
                GVBnk_CCFees.Visible = False
                GVTransfers.Visible = False
            End If
        End If
    End Sub

    Function GetAvgPrice(ByVal BankID As Integer, ByVal CurrTicker As String, ByVal Quantity As Double, ByVal TransDate As String, ByVal sFY_BegDate As String, ByVal sFY_EndDate As String) As Double
        Dim sSql As String

        If BankID = 0 Then
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=(select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID in (select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        Else
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=" & BankID & " and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID =" & BankID & " AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "

        End If

        Dim rsBankTran As SqlDataReader = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Shares", Type.GetType("System.Decimal"))
        dt.Columns.Add("Price", Type.GetType("System.Decimal"))
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"))
        dt.Columns.Add("OutstandingShares", Type.GetType("System.Decimal"))
        dt.Columns.Add("CostBasis", Type.GetType("System.Decimal"))
        dt.Columns.Add("AvgPrice", Type.GetType("System.Decimal"))
        Dim currentIndex As Integer = -1
        While rsBankTran.Read
            If rsBankTran("TransCat").ToString().ToLower.Trim = "begbal" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount")
                dr("OutstandingShares") = rsBankTran("Quantity")
                dr("CostBasis") = rsBankTran("NetAmount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf currentIndex = -1 Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = 0.0
                dr("Price") = 0.0
                dr("Amount") = 0.0
                dr("OutstandingShares") = 0.0
                dr("CostBasis") = 0.0
                dr("AvgPrice") = 0.0
                dt.Rows.Add(dr)
            End If
            If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)

            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity") * -1
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            End If
        End While
        If currentIndex < 0 Then
            Return 0.0
        Else
            Return Math.Round(dt.Rows(currentIndex)("AvgPrice"), 2)
        End If
    End Function
    Protected Sub gvDonation_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvDonation.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            'e.Row.Cells(9).Enabled = e.Row.DataItem("DSlipNoStatus")







            ''** e.Row.Cells(7).Enabled = e.Row.DataItem("ReconcileStatus")
            
        End If
    End Sub

    Protected Sub BtnSubmit1_Click(ByVal sender As Object, ByVal e As System.EventArgs)

        'If FileUpLoad1.HasFile Then
        Try
            Label3.Visible = False
            Label1.Visible = False
            'MsgBox("If critical, you MUST provide a reason.")
            srcfilename = FileUpLoad1.FileName
            Dim orgfilename As String = Txtstring.Text


            Dim checkfile As String() = srcfilename.Split(".")
            Dim orgstring As String() = orgfilename.Split(".")


            If checkfile(1).Trim() <> orgstring(1).Trim() Then

                Label1.Visible = True
                Label1.Text = "Only pdf documents are allowed"
                Label2.Visible = True
                Label2.Text = "File format:" & Txtstring.Text
                Response.Write("<script language='javascript'>alert('Only pdf documents are allowed');</script>")
                Exit Sub





            ElseIf checkfile(0).Trim() <> orgstring(0).Trim() Then
                'Label1.Visible = True
                ' Label1.Text = "Wrong File Format"
                Label2.Visible = True
                Label2.Text = "File format:" & Txtstring.Text
                tblDupRec.Visible = True
                Pnlupload.Visible = True
                Label7.Text = "File format:" & Txtstring.Text

            End If

           
            insertfile()
            If value = "0" Then
                Tbldup.Visible = True
                Pnlupload.Visible = True
                tblDupRec.Visible = False

                Dim uploadFolder As String = Request.PhysicalApplicationPath + "ScanFinDoc/"
                'Dim uploadFolder As String = Request.PhysicalApplicationPath + "../Private/ScanFinDoc/"
                ' If (FileUpLoad1.HasFile) Then
                ' {  
                Dim extension As String = Path.GetExtension(FileUpLoad1.PostedFile.FileName)
                FileUpLoad1.SaveAs(uploadFolder + "Test.pdf")
            Else
                Tbldup.Visible = False
                Dim uploadFolder As String = Request.PhysicalApplicationPath + "ScanFinDoc/"
                'Dim uploadFolder As String = Request.PhysicalApplicationPath + "../Private/ScanFinDoc/"
                ' If (FileUpLoad1.HasFile) Then
                ' {  
                Dim extension As String = Path.GetExtension(FileUpLoad1.PostedFile.FileName)
                FileUpLoad1.SaveAs(uploadFolder + Txtstring.Text)
                ' FileUpLoad1.SaveAs(Server.MapPath("Private/ScanFinDoc/") & FileUpLoad1.FileName)
                If tblDupRec.Visible = True Then
                Else
                    buttoninsert()
                    displayfilename()
                End If

                Dim strmessage As String
               
                Label4.Visible = True
                Label4.Text = "File Uploaded"
                Pnlupload.Visible = True
                If tblDupRec.Visible = True Then
                    Label4.Visible = False
                Else

                    Label4.Visible = True

                    strmessage = "File uploaded successfully as: " + Txtstring.Text
                    Response.Write("<script language='javascript'>alert('" + strmessage + "');</script>")
                End If


            End If

        Catch ex As Exception
            Label3.Visible = True
            Label3.Text = "Technical Issue Contact Admin"
            Tbldup.Visible = False
            tblDupRec.Visible = False
        End Try
        ' Else
        ' Label1.Text = "You have not specified a file."
        'End If
    End Sub
    


    Protected Sub Download()
        Try
            Pnlupload.Visible = False
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("~/ScanFinDoc/" & TextBox8.Text))
            ' If file.Exists Then 'set appropriate headers
            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
            Response.AddHeader("Content-Length", file.Length.ToString())
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(file.FullName)
            Response.End()
            'Else
            ' lblErr.Text = "File Does not Exist"
            'End If
        Catch ex As Exception
        End Try
    End Sub
    Protected Sub insertfile()

        Dim objid As Integer
        objid = Convert.ToInt32(Session("LoginID").ToString())
        Dim objcreatedat As Object
        objcreatedat = System.DateTime.Now
        Dim strfilename As String
        strfilename = Txtstring.Text
        If ddlCategory.SelectedValue = "5" Then
            strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[TransDate] ,[CheckNumber],[DocName],[transid],[CreateDate],[CreatedBy],[DocCat],[DocID],[DepSlipNumber]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "', '" + TextBox5.Text + "', '" + strfilename + "','" + TextBox10.Text + "',getdate(),'" + objid.ToString() + "','" + TextDoccat.Text + "','" + Textdocid.Text + "','" + TextBox10.Text + "')select @@rowcount End  else select 0 "
        ElseIf ddlCategory.SelectedValue = "9" Then
            strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[TransDate] ,[CheckNumber],[DocName],[transid],[CreateDate],[CreatedBy],[DocCat],[DocID],[DepSlipNumber]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "', '" + TextBox5.Text + "', '" + strfilename + "','" + TextBox10.Text + "',getdate(),'" + objid.ToString() + "','" + TextDoccat.Text + "','" + Textdocid.Text + "','" + TextBox10.Text + "')select @@rowcount End  else select 0 "
        Else
            strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[TransDate] ,[DocName],[transid],[CreateDate],[CreatedBy],[DocCat],[DocID],[DepSlipNumber]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "',  '" + strfilename + "','" + TextBox10.Text + "',getdate(),'" + objid.ToString() + "','" + TextDoccat.Text + "','" + Textdocid.Text + "','" + TextBox10.Text + "')select @@rowcount End  else select 0 "
        End If
        'strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[TransDate] ,[CheckNumber],[DocName],[transid],[CreateDate],[CreatedBy],[DocCat],[DocID],[DepSlipNumber]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "', '" + TextBox5.Text + "', '" + strfilename + "','" + TextBox10.Text + "',getdate(),'" + objid.ToString() + "','" + TextDoccat.Text + "','" + Textdocid.Text + "','" + TextBox10.Text + "')select @@rowcount End  else select 0 "
        ' strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[BankTransDate] ,[CheckNumber],[DocName],[banktransid],[createdby],[createdate],[Modifiedby],[ModifyDate]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "', '" + TextBox5.Text + "', '" + strfilename + "','" + TextBox10.Text + "','" + objid + "',getdate(),'" + objid + "',getdate())select @@rowcount End  else select 0 "
        'value = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, sqlstring);
        Dim str As String
        str = strSql

        value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
    End Sub
    Protected Sub insertfilemodification()
        Dim objid As Integer
        objid = Convert.ToInt32(Session("LoginID").ToString())
        Dim objcreatedat As Object
        objcreatedat = System.DateTime.Now
        Dim strfilename As String
        strfilename = Txtstring.Text
        strSql = "update [scanfindoc] set [ModifyDate]=getdate(),[Modifiedby]='" + objid.ToString() + "' where Docname='" + Txtstring.Text + "' "
        ' strSql = "if Not Exists(Select * from scanfindoc where DocName ='" + strfilename + "')Begin INSERT INTO .[dbo].[scanfindoc] ([Eventyear],[Transcat] ,[BankId] ,[BankTransDate] ,[CheckNumber],[DocName],[banktransid],[createdby],[createdate],[Modifiedby],[ModifyDate]) VALUES ('" + TextBox1.Text + "','" + TextBox2.Text + "', '" + TextBox3.Text + "','" + TextBox4.Text + "', '" + TextBox5.Text + "', '" + strfilename + "','" + TextBox10.Text + "','" + objid + "',getdate(),'" + objid + "',getdate())select @@rowcount End  else select 0 "
        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, strSql)
        Dim str As String
        str = strSql
    End Sub
    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSubmit.Click
        displayfilename()
        buttoninsert()
        displayfilename()
    End Sub
   
    Protected Sub buttoninsert()
        TrDetailView.Visible = False
        btnClose.Visible = False
        Pnlupload.Visible = False


        If Not IsDate(TxtFrom.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf Not IsDate(txtTo.Text) Then
            lblErr.Text = "please enter from date in mm/dd/yyyy format"
            Exit Sub
        ElseIf ddlCategory.SelectedValue = "0" Then
            lblErr.Text = "Please Select valid Category"
            Exit Sub
        ElseIf Convert.ToDateTime(TxtFrom.Text) > Convert.ToDateTime(txtTo.Text) Then
            lblErr.Text = "Invalid Dates"
            Exit Sub
        Else
            lblErr.Text = ""
            Dim StrSQL As String
            If ddlCategory.SelectedValue = "1" Then
                StrSQL = " Select Distinct Top 100 sum(D.Amount) as Amount, D.DepositSlip,D.DepositSlip as DoCID, case when  MONTH(d.DepositDate)>=5 then YEAR(d.DepositDate) else YEAR(d.DepositDate)-1 end   as EventYear,D.DepositDate,sc.Docname,'DGN'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlip) as VoucherNo"
                StrSQL = StrSQL & " ,   case when  bs.BankTransID IS null then brs.BrokTransID else bs.BankTransID end   as BankTransID"
                StrSQL = StrSQL & " ,case when Bs.transcat is null then BrS.TransCat else Bs.transcat end as Transcat, CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile,CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0) AND (COUNT(BrS.DepCheckNo)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
                StrSQL = StrSQL & " CASE WHEN (COUNT(BS.DepSlipNumber)>0 OR COUNT(BrS.DepCheckNo) > 0) then CASE WHEN D.BankID in (4,5,6,7) THEN Convert(Varchar,BrS.DepCheckNo) ELSE Convert(Varchar,BS.DepSlipNumber) END ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END END "
                StrSQL = StrSQL & " as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null AND BrS.DepCheckNo is NULL) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = CASE WHEN D.BankID in (4,5,6,7) THEN BrS.NetAmount ELSE  BS.Amount END THEN 'False' ELSE 'True' END as ReconcileStatus,CASE WHEN D.BankID in (4,5,6,7) THEN BrS.DepCheckNo  ElSE BS.DepSlipNumber END as DepSlipNumber "
                'StrSQL = StrSQL & " ,CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END  END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted ,"
                'StrSQL = StrSQL & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE  CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END"
                'StrSQL = StrSQL & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber"
                StrSQL = StrSQL & ",D.BankID from  DonationsInfo  D left join ScanFinDoc sc on sc.docid=d.DepositSlip Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON  B.BankID=D.BankID AND B.TransCat='Deposit'  AND D.DepositDate = B.TransDate "
                StrSQL = StrSQL & " Left Outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND  BS.BankID=D.BankID AND BS.TransCat='Deposit' AND D.DepositSlip = BS.DepSlipNumber"
                StrSQL = StrSQL & " Left Outer Join BrokTrans BrS ON D.DepositDate=BrS.TransDate AND BrS.BankID=D.BankID AND BrS.TransCat='Donation' AND D.DepositSlip = BrS.DepCheckNo AND D.BankID in (4,5,6,7)"
                StrSQL = StrSQL & " where D.METHOD  IN ('Check','CASH') AND D.DepositDate  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59' Group BY BS.banktransid,brs.BrokTransID,sc.Docname,D.DepositSlip,BrS.TransCat,Bs.transcat, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount,BrS.NetAmount,Brs.DepCheckNo order by  D.DepositDate,D.DepositSlip"
                TextDoccat.Text = "DonCheck"
                TextDoccat.Text = "DonCheck"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 1
                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "2" Then
                StrSQL = "Select  Top 100 sum(D.Amount) as Amount,bs.transcat,bs.BankTransID,D.DepositSlipNo as DocID,sc.docname, RTRIM(D.DepositSlipNo) as DepositSlip, D.DepositDate,'OTH'+RIGHT('00'+ CONVERT(VARCHAR,D.BankID),2)+Replace(CONVERT(VARCHAR(10), D.DepositDate, 101),'/','')+CONVERT(VARCHAR,DepositSlipNo) as VoucherNo ,case when  MONTH(d.DepositDate)>=5 then YEAR(d.DepositDate) else YEAR(d.DepositDate)-1 end   as EventYear,"
                StrSQL = StrSQL & " CASE WHEN sum(D.Amount) = BS.Amount THEN 'Matched' ELSE CASE WHEN B.TransDate IS NULL then 'Verify Manually' ELSE 'Not Matched: Verify' END END as Reconcile, CASE WHEN ((B.TransDate IS NULL) AND (COUNT(BS.DepSlipNumber)=0)) then 'Data not posted' ELSE 'Data Posted' END As Dataposted , "
                StrSQL = StrSQL & " CASE WHEN COUNT(BS.DepSlipNumber) > 0 then Convert(Varchar,BS.DepSlipNumber) ELSE CASE WHEN B.TransDate IS NULL then 'Data not posted' ELSE 'N/A: Update' END "
                StrSQL = StrSQL & " END as DSlipNo, CASE WHEN (B.TransDate IS NOT NULL AND BS.DepSlipNumber is Null) then 'True' ELSE 'False' END as DSlipNoStatus,CASE WHEN sum(D.Amount) = BS.Amount THEN 'False' ELSE 'True' END as ReconcileStatus,BS.DepSlipNumber,D.BankID from OtherDeposits D Left outer Join (SELECT BankID, TransCat, TransDate, DepSlipNumber FROM BankTrans WHERE DepSlipNumber is Null Group by BankID, TransCat, TransDate, DepSlipNumber) B ON B.BankID=D.BankID AND B.TransCat='Deposit' AND D.DepositDate = B.TransDate"
                StrSQL = StrSQL & "  Left outer Join BankTrans BS ON D.DepositDate=BS.TransDate AND BS.BankID=D.BankID AND BS.TransCat IN ('Deposit','DepositError') AND D.DepositSlipNo = BS.DepSlipNumber  left join ScanFinDoc sc on sc.TransID=bs.BankTransID where  D.DepositDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "  23:59' Group BY bs.transcat,bs.BankTransID,sc.docname,D.DepositSlipNo, D.DepositDate,BS.DepSlipNumber,D.BankID,B.TransDate,BS.Amount order by D.DepositDate,D.DepositSlipNo "
                'Response.Write(StrSQL)
                TextDoccat.Text = "OthDeposit"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 2
                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "3" Then
                '3 - CC Revenues -
                'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
                ' having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0
                Dim StrSQL1 As String
                Dim valuedoc As String
                StrSQL1 = "select docname from scanfindoc where doccat='CCRevenue'"

                valuedoc = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL1)
                StrSQL = "SELECT SUM(Amount) as Amount,'" + valuedoc + "' as DocName,  case when  MONTH(StartDate)>=5 then YEAR(StartDate) else YEAR(StartDate)-1 end   as EventYear,1 as bankid ,1 as banktransid, 'CCRevenue' as TransCat,'CGN01'+Replace(CONVERT(VARCHAR(10), StartDate, 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), EndDate, 101),'/','') as VoucherNo ,StartDate,EndDate from("
                StrSQL = StrSQL & " Select Sum(NFG.TotalPayment) as Amount "  ' ,'CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " ,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate "
                'StrSQL = StrSQL & "  ,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(NFG.MS_TransDate)-1),NFG.MS_TransDate),101) AS StartDate ,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DATEADD(mm,1,NFG.MS_TransDate))),DATEADD(mm,1,NFG.MS_TransDate)),101) AS EndDate "
                StrSQL = StrSQL & " from NFG_Transactions NFG where NFG.MS_TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by NFG.MS_TransDate"
                'Commented on 21-11-2013 
                'StrSQL = StrSQL & " having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0 or SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) < 0 "

                ''Added to Merge CCRevenueAdjustments
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select (SUM(CreditCC)- SUM(Registrations)) as Amount " ','CGN01'+Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','')+ '_'+Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate from "
                StrSQL = StrSQL & ",'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate "
                'StrSQL = StrSQL & ", CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(TransDate)-1),TransDate),101) AS StartDate,CONVERT(VARCHAR(10),DATEADD(dd,-(DAY(DATEADD(mm,1,TransDate))),DATEADD(mm,1,TransDate)),101) AS EndDate "
                StrSQL = StrSQL & " From (select SUM(Amount) as CreditCC ,0 as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) "
                StrSQL = StrSQL & " Union All "
                StrSQL = StrSQL & " select 0 as CreditCC,SUM(NFG.TotalPayment) as Registrations, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG WHERE NFG.MS_TransDate between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 ' Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) "
                StrSQL = StrSQL & ")T3 Group by TransDate "
                StrSQL = StrSQL & ")T4 group by StartDate,EndDate Order By StartDate"  'VoucherNo,
                'Response.Write(StrSQL)
                TextDoccat.Text = "CCRevenue"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 3
                GetDonationReceipt(StrSQL)

            ElseIf ddlCategory.SelectedValue = "4" Then
                '4 - TDA Accounts -
                Dim i As Integer
                Dim BankIDs As String = "0"
                If lstBank.Items(i).Selected = True Then
                    BankIDs = "1,2,3,4,5,6,7"
                Else
                    For i = 0 To lstBank.Items.Count - 1
                        If lstBank.Items(i).Selected Then
                            BankIDs = BankIDs & "," & lstBank.Items(i).Value
                        End If
                    Next
                End If
                StrSQL = " Select b.transdate, sc.docname,case when  MONTH(b.TransDate)>=5 then YEAR(b.TransDate) else YEAR(b.TransDate)-1 end   as EventYear,b.BrokTransID ,b.TransCat, SUM(Case WHEN B.NetAmount = 0 then B.Quantity Else B.NetAmount End) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,B.BankID,B.BankCode,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " FROM   BrokTrans B Inner Join Bank BB On B.BankID=BB.BankID  left join ScanFinDoc sc on sc.TransID=b.BrokTransID WHERE ((B.AssetClass IN ('MMK','Cash') AND B.TransType='Div' and B.TransCat='Div') OR (B.AssetClass IN ('Stock','MutFund') AND B.TransType='Div' AND B.TransCat in ('STCGDiv','LTCGDiv','OrdDiv'))) AND  B.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By  sc.docname,b.TransDate,b.TransCat, b.BrokTransID, B.BankID,B.BankCode " ' Order By B.BankID CONVERT(VARCHAR(25),DATEADD(dd,-(DAY(B.TransDate)-1),B.TransDate),101),DATEADD(d,-1,DATEADD(mm, DATEDIFF(m,0,B.TransDate)+1,0)) " 
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " Select   b.transdate, sc.docname,case when  MONTH(b.TransDate)>=5 then YEAR(b.TransDate) else YEAR(b.TransDate)-1 end   as EventYear,b.BankTransID,b.TransCat,SUM(B.Amount) as Amount, 'IGN'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2) + Replace(CONVERT(VARCHAR(10), '" & TxtFrom.Text & "', 101),'/','') +'_'+ Replace(CONVERT(VARCHAR(10), '" & txtTo.Text & "', 101),'/','') as VoucherNo,B.BankID,BB.BankCode ,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate "
                StrSQL = StrSQL & " FROM  BankTrans B Inner Join Bank BB On B.BankID=BB.BankID "
                StrSQL = StrSQL & " left join ScanFinDoc sc on sc.TransID=b.BankTransID WHERE B.TransType='Credit' and B.TransCat='Interest' AND  B.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59 '"
                StrSQL = StrSQL & " AND B.BankID in (" & BankIDs & ") Group By    b.TransCat,sc.docname,b.TransDate,b.BankTransID,B.BankID,BB.BankCode Order by  b.TransDate"
                TextDoccat.Text = "InvIncome"
                'Response.Write(StrSQL)
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("BankIDs") = BankIDs
                Session("DCatID") = 4

                GetDonationReceipt(StrSQL)
            ElseIf ddlCategory.SelectedValue = "5" Then
                '5 - Vouchers - Expense checks
                StrSQL = " Select Distinct sum(E.ExpenseAmount) as Amount,E.CheckNumber, case when  MONTH(E.DatePaid)>=5 then YEAR(E.DatePaid) else YEAR(E.DatePaid)-1 end   as EventYear, case when E.BanKID=4 then Br.TransCat else Ba.transcat end as Transcat,Sd.DocName, " 'Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End as DateCashed"
                StrSQL = StrSQL & "  case when  Ba.BankTransID IS null then Br.BrokTransID else ba.BankTransID end   as BankTransID, "
                StrSQL = StrSQL & " Case when E.BanKID in (1,2,3) then Case When Ba.Transdate is not null Then CONVERT(VARCHAR(10), Ba.Transdate, 101) Else 'Not Available' End "
                StrSQL = StrSQL & " Else case when E.BanKID in (4,5,6,7) then Case when Br.TransDate is not null Then CONVERT(VARCHAR(10), Br.Transdate, 101) Else 'Not Available' End"
                StrSQL = StrSQL & " Else 'Not Available' End End as DateCashed"
                StrSQL = StrSQL & ", E.DatePaid,'VPS'+RIGHT('00'+ CONVERT(VARCHAR,E.BankID),2)+Replace(CONVERT(VARCHAR(10), E.DatePaid, 101),'/','')+CONVERT(VARCHAR,E.CheckNumber) as VoucherNo,E.BankID " 'Case When Ba.Transdate is not null Then Ba.Transdate Else '' End as DateCashed,
                StrSQL = StrSQL & " From ExpJournal E Inner Join AcctgTransType ATT ON ATT.Code = E.TransType AND ATT.AcctgTransTypeID IN (1, 2, 3, 4, 5, 7)"
                StrSQL = StrSQL & " LEFT Join BankTrans Ba on CONVERT(Varchar,Ba.CheckNumber)=E.CheckNumber and Ba.BankID=E.BanKID left join scanfindoc sd on  sd.CheckNumber=E.checkNumber" ' AND Ba.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
                StrSQL = StrSQL & " LEFT Join BrokTrans Br on (CONVERT(Varchar,Br.ExpCheckNo)=E.CheckNumber OR CONVERT(Varchar,Br.DepCheckNo)=E.CheckNumber) and Br.BankID=E.BanKID " 'AND Br.TransDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Commented on 29-04-2013
                StrSQL = StrSQL & " WHERE E.CheckNumber is Not null AND (ba.BankTransID IS NOT NULL OR Br.BrokTransID IS NOT Null Or E.TransactionID is not null) AND  (E.Account is Not null or E.TransType = 'Transfers') AND E.DatePaid  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'" 'Modified on 29-04-2013
                StrSQL = StrSQL & " Group BY Br.BrokTransID,Ba.BankTransID,E.DatePaid, E.CheckNumber,Sd.DocName,E.BankID,Ba.Transdate, Br.TransDate,E.Eventyear,Ba.transcat,Br.transcat ORDER BY E.CheckNumber,E.DatePaid"
                'Response.Write(StrSQL)
                TextDoccat.Text = "ExpCheck"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 5
                GetDonationReceipt(StrSQL)

            ElseIf ddlCategory.SelectedValue = "6" Then
                'ACT_EFT Donations
                StrSQL = "SELECT  B.BankID,B.BankTransID, SUM(B.Amount) as Amount,B.TransCat,'DGE' as TransType,B.TransDate,'DGE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+CONVERT(VARCHAR,B.BankTransID) as VoucherNo,sc.Docname,"
                StrSQL = StrSQL & "case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear,B.VendCust, N.Description ,N.DonorType,'" & TxtFrom.Text & "' as StartDate , '" & txtTo.Text & "' as EndDate FROM BankTrans B "
                StrSQL = StrSQL & "left join scanfindoc sc on sc.TransID=b.BankTransID LEFT JOIN ChaseDesc C ON CHARINDEX(C.Token, B.Description) > 0 and B.TransCat = C.Bk_TransCat and B.VendCust = C.Bk_VendorCust and B.Reason = C.Bk_Reason and B.BankID=1"
                StrSQL = StrSQL & "LEFT Join HBTAddInfo H ON CHARINDEX(H.Token, B.AddInfo) > 0 and B.TransCat = H.Bk_TransCat and B.VendCust = H.Bk_VendorCust and ((B.Reason = H.Bk_Reason) Or (B.Reason IS NULL and H.Bk_Reason is Null)) and B.BankID=2"
                StrSQL = StrSQL & "Inner Join NSFAccounts N ON C.Bk_DonorType = N.Description Or H.Bk_DonorType = N.Description "
                StrSQL = StrSQL & "Where (H.ID is not null Or C.ID is not null) and B.TransCat='Donation' and b.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "'"
                StrSQL = StrSQL & "Group By sc.Docname,B.BankID,N.Description ,N.DonorType,B.TransCat,B.TransDate,B.Vendcust,B.BankTransID Order by B.TransDate"
                TextDoccat.Text = "ACH_EFT"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 6
                GetDonationReceipt(StrSQL)

                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "7" Then
                'Vouchers Bank Service Charges
                Dim StrSQL1 As String
                Dim valuedoc As String
                StrSQL1 = "select docname from scanfindoc where doccat='CCRevenue'"

                valuedoc = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL1)

                StrSQL = "SELECT case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear,B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ IsNull(B.VendCust,'') as VoucherNo," ''_'+RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
                StrSQL = StrSQL & " E.AccountName,B.TransDate,b.transcat,b.banktransid,'VPE' as TransType,E.ExpCatCode,  IsNull(B.VendCust,'') as VendCust,B.Reason,B.AddInfo,sc.docname"
                StrSQL = StrSQL & " FROM BankTrans  B "
                StrSQL = StrSQL & " INNER JOIN ExpenseCategory E ON B.TransCat= E.ExpCatCode "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo left join ScanFinDoc sc on sc.banktransid=b.banktransid"
                StrSQL = StrSQL & " Where TransType= 'DEBIT' And B.TransCat = 'CreditCard' and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group By sc.docname,b.transcat,b.banktransid,B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode"
                StrSQL = StrSQL & " UNION ALL "
                StrSQL = StrSQL & " SELECT case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear,B.BankID,SUM(Amount) as Amount,'VPE' as TransType,B.TransDate,'VPE'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ + IsNull(B.VendCust,'') as VoucherNo," '_'+'RIGHT('00'+ Convert(varchar(10),(row_number() OVER (ORDER BY B.TransDate, B.BankID))),2)
                StrSQL = StrSQL & " E.AccountName,B.TransDate,b.transcat,b.banktransid,'VPE' as TransType,E.ExpCatCode,IsNull(B.VendCust,'') as  VendCust,B.Reason,B.AddInfo,sc.docname"
                StrSQL = StrSQL & " FROM BankTrans B "
                StrSQL = StrSQL & " INNER JOIN ExpenseCategory  E ON B.TransCat= E.ExpCatCode "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON E.Account = N.AccNo left join ScanFinDoc sc on sc.banktransid=b.banktransid "
                StrSQL = StrSQL & " where B.TransCat in ('Fee','DepositReturn','Shipping','FundsReturn') and TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group By sc.docname,b.transcat,b.banktransid,B.BankID,B.TransDate,B.VendCust,B.Reason,B.AddInfo,E.AccountName,E.ExpCatCode  order by b.TransDate"
                TextDoccat.Text = "Fees"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 7
                GetDonationReceipt(StrSQL)
                'Response.Write(StrSQL)
            ElseIf ddlCategory.SelectedValue = "8" Then
                ''TransferOut
                StrSQL = "SELECT B.BankID,sc.docname,case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear,B.BrokTransID,B.BankCode,'IGT'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+"
                StrSQL = StrSQL & " Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ ISNULL(B.AssetClass,'')+ RIGHT('00'+ CONVERT(VARCHAR,B1.BankID),2) as VoucherNo,B.TransDate,B.TransCat,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate,'IGT' as TransType,B.AssetClass,B.Ticker,B.Quantity,B.Price,"
                StrSQL = StrSQL & " B.NetAmount as Amount,B.RestType,B1.RestType as RestTypeTo,B1.BankID as ToBankID,N.Description"
                StrSQL = StrSQL & " FROM BrokTrans B "
                StrSQL = StrSQL & " Inner join BrokTrans B1 On B1.TranId=B.TranID and B.TransType = B1.TransType and B.TransDate= B1.TransDAte and B.TransCat<>B1.TransCat "
                StrSQL = StrSQL & " Inner JOin NSFAccounts N On N.BankID=B.BankId and B.RestType=N.RestrictionType and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv')))"
                StrSQL = StrSQL & "left join ScanFinDoc sc on sc.transid=B.BrokTransID Where B1.Transcat='TransferIn' and B.TransCat='TransferOut' and B.Transdate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' Order by B.TransDate"
                TextDoccat.Text = "Transfers"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 8
                'Response.Write(StrSQL)
                GetDonationReceipt(StrSQL)

                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "9" Then
                StrSQL = "SELECT B.BankID,sc.docname, case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear ,b.BrokTransID,B.BankCode,N.AccNo,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount"
                StrSQL = StrSQL & " ,N.Description, 'IGB' as TransType,B.AssetClass,"
                StrSQL = StrSQL & " 'IGB'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,N.Description,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & "left join ScanFinDoc sc on n.AccNo=sc.CheckNumber Where B.Transcat='Buy' and B.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group by sc.docname,b.BrokTransID ,B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass order by transdate,broktransid"
                'Response.Write(StrSQL)
                  TextDoccat.Text = "Transfers"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 9
                GetDonationReceipt(StrSQL)
                'lblErr.Text = "This Category is under Development"
            ElseIf ddlCategory.SelectedValue = "10" Then
                StrSQL = "SELECT B.BankID,sc.docname,B.BankCode,N.AccNo,B.TransDate,B.TransCat,B.AssetClass,SUM(B.NetAmount) as Amount,case when  MONTH(B.TransDate)>=5 then YEAR(B.TransDate) else YEAR(B.TransDate)-1 end   as EventYear,b.BrokTransID"
                StrSQL = StrSQL & " ,N.Description, 'IGS' as TransType,B.AssetClass,"
                StrSQL = StrSQL & " 'IGS'+RIGHT('00'+ CONVERT(VARCHAR,B.BankID),2)+Replace(CONVERT(VARCHAR(10), B.TransDate, 101),'/','')+ "
                StrSQL = StrSQL & " IsNull(B.AssetClass,'')+'_'+ISNULL(B.TransCat,'') as VoucherNo,N.Description,'" & TxtFrom.Text & "' as StartDate,'" & txtTo.Text & "' as EndDate FROM BrokTrans B "
                StrSQL = StrSQL & " INNER JOIN NSFAccounts N ON B.BanKID=N.BankID   and ((B.AssetClass='Cash' and N.InvIncType in('MMK')) Or (B.AssetClass in('Stock','Mutfund') and N.InvIncType in ('Inv'))) " 'and N.Description ='Investment-General Unrestricted'"
                StrSQL = StrSQL & "  left join ScanFinDoc sc on sc.BankTransID=b.BrokTransID Where B.Transcat='Sell' and B.TransDate Between '" & TxtFrom.Text & "' and '" & txtTo.Text & "' "
                StrSQL = StrSQL & " Group by sc.docname,b.BrokTransID,B.TransDate,B.AssetClass,B.BankID,B.BankCode,B.TransCat,N.Description,N.AccNo,B.AssetClass Order by B.TransDate"
                'Response.Write(StrSQL)
                TextDoccat.Text = "Transfers"
                Session("Frmdate") = TxtFrom.Text
                Session("ToDate") = txtTo.Text
                Session("ManageVouchersSQl") = StrSQL
                Session("DCatID") = 10
                GetDonationReceipt(StrSQL)
                'lblErr.Text = "This Category is under Development"
            End If

        End If

    End Sub
    Protected Sub displaypopup()
        lbprevious.Visible = True
        Label5.Visible = False
        Label6.Visible = False
        Dim Qvalcat As String
        Qvalcat = ddlCategory.SelectedValue

        If Qvalcat = "1" Then

            strSql = "select   * from donationsinfo where DepositSlip='" + Textdocid.Text + "' and DepositDate  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'"

        ElseIf Qvalcat = "2" Then
            strSql = "select   * from OtherDeposits where depositslipno='" + Textdocid.Text + "' and DepositDate Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "  23:59'"
        ElseIf Qvalcat = "3" Then
            strSql = "select   * from ChargeRec "
        ElseIf Qvalcat = "5" Then
            strSql = "select   * from ExpJournal  where CheckNumber='" + Textdocid.Text + "' and DatePaid  Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & " 23:59'"

        ElseIf Qvalcat = "8" Then
            strSql = "select   * from broktrans  where broktransid='" + Textdocid.Text + "' "

        Else

            strSql = "select   * from BankTrans  where Banktransid='" + Textdocid.Text + "' "


        End If
        Dim dsnew As DataSet
        Dim dtmerge As DataTable



        Dim conn As New SqlConnection(Application("ConnectionString"))

        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)

        Gridpopup.Visible = True
        dtmerge = dsDonation.Tables(0)
        Gridpopup.DataSource = dtmerge
        Gridpopup.DataBind()

        ListBox1.Visible = False
        gvDonation.Visible = False
        gridsanfindoc.Visible = False

        grdEditVoucher.Visible = False
        GVTDA.Visible = False
        gvExp.Visible = False
        GVCCRevenues.Visible = False
        GVACT_EFTDon.Visible = False
        GVBnk_CCFees.Visible = False
        GVBuyTrans.Visible = False
        GVTransfers.Visible = False
        GVTDAIncome.Visible = False
        GVGLTemp.Visible = False
        'Dim DocID As Integer
        '  DocID = k.Value
        '  Response.Write("<script language='javascript'>window.open('scanfinpopup.aspx?id=" + Textdocid.Text + "& DDc=" + ddlCategory.SelectedValue + "','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1')</script> ")
    End Sub
    Protected Sub displayfilename()
        Label5.Visible = True
        Label6.Visible = True
        Dim tbldisp As New DataTable
        ListBox1.Items.Clear()
        Dim di As New IO.DirectoryInfo(Server.MapPath("~/ScanFinDoc/"))
        Dim diar1 As IO.FileInfo() = di.GetFiles()
        Dim dra As IO.FileInfo

        'list the names of all files in the specified directory
        For Each dra In diar1
            ListBox1.Visible = True
            ListBox1.Height = 300

            ListBox1.Items.Add(dra.Name)
            'ListBox1.Items.Add(dra)
        Next
        strSql = "select top 25 * from ScanFinDoc order by  CreateDate desc,doccat"
        Dim conn As New SqlConnection(Application("ConnectionString"))

        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        gridsanfindoc.Visible = True

        gridsanfindoc.DataSource = dsDonation.Tables(0)
        gridsanfindoc.DataBind()
    End Sub
    Protected Sub gvDonation_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvDonation.RowCommand
        Try
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim row As GridViewRow = gvDonation.Rows(index)
            Eventyear = row.Cells(3).Text
            Transcat = row.Cells(4).Text
            BankId = row.Cells(6).Text
            Transdate = row.Cells(5).Text
            chkdepnumber = row.Cells(0).Text
            TextBox6.Text = row.Cells(8).Text
            Pnlupload.Visible = True

            Dim month As String
            Dim editdate As String

            Dim date1 As String
            Dim year1 As String
            Dim checkfile As String() = Transdate.Split("/")
            If checkfile(0).Length = 1 Then
                month = "0" + checkfile(0)
            Else

                month = checkfile(0)
            End If
            If checkfile(1).Length = 1 Then
                date1 = "0" + checkfile(1)
            Else

                date1 = checkfile(1)
            End If
            year1 = checkfile(2)
            editdate = month + "-" + date1 + "-" + year1
            ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
            ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
            '  date = ""+day+month+date[2]

            Txtstring.Text = row.Cells(3).Text & "_" & row.Cells(4).Text & "_" & row.Cells(6).Text & "_" & editdate & "_" & row.Cells(0).Text & ".pdf"
            TextBox1.Text = row.Cells(3).Text
            TextBox2.Text = row.Cells(4).Text
            TextBox3.Text = row.Cells(6).Text
            TextBox4.Text = row.Cells(5).Text
            TextBox5.Text = row.Cells(0).Text
            TextBox7.Text = row.Cells(0).Text
            TextBox8.Text = row.Cells(8).Text
            TextBox10.Text = row.Cells(9).Text
            Textdocid.Text = row.Cells(10).Text
            Pnlupload.Visible = True
            ' upformat.Visible = False
            BtnSubmit1.Visible = True
            Label2.Visible = True
            Label2.Text = "File format:" & Txtstring.Text
            Label4.Visible = False

            If e.CommandName = "DownLoad" Then
                Pnlupload.Visible = False
                Download()


            End If
            If e.CommandName = "DOCID" Then
                Pnlupload.Visible = False
                displaypopup()


            End If
            'Export
            'Dim gvtemp As New GridView
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub GVBuyTrans_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVBuyTrans.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVBuyTrans.Rows(index)

        ' Dim frmdate As Date = Convert.ToDateTime(row.Cells(5).Text)
        ' Dim todate As DateTime = Convert.ToDateTime(row.Cells(5).Text + " 23:59")
        'Dim BankID As Integer = row.Cells(2).Text
        'Dim VoucherNo As String = row.Cells(4).Text
        'Dim AssetClass As String = row.Cells(11).Text

        Eventyear = row.Cells(6).Text
        Transcat = row.Cells(4).Text
        BankID = row.Cells(2).Text
        Transdate = row.Cells(8).Text
        chkdepnumber = row.Cells(7).Text
        TextBox6.Text = row.Cells(13).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(6).Text & "_" & row.Cells(4).Text & "_" & row.Cells(2).Text & "_" & editdate & "_" & row.Cells(5).Text & ".pdf"
        TextBox1.Text = row.Cells(6).Text
        TextBox2.Text = row.Cells(4).Text
        TextBox3.Text = row.Cells(2).Text
        TextBox4.Text = row.Cells(8).Text
        TextBox5.Text = row.Cells(7).Text
        TextBox7.Text = row.Cells(5).Text
        TextBox8.Text = row.Cells(13).Text
        TextBox10.Text = row.Cells(5).Text
        Textdocid.Text = row.Cells(5).Text
        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If
     
    End Sub
    Protected Sub GVTransfers_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVTransfers.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVTransfers.Rows(index)

        Eventyear = row.Cells(7).Text
        Transcat = row.Cells(11).Text
        BankId = row.Cells(2).Text
        Transdate = row.Cells(6).Text
        chkdepnumber = row.Cells(3).Text
        TextBox6.Text = row.Cells(14).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(7).Text & "_" & row.Cells(11).Text & "_" & row.Cells(2).Text & "_" & editdate & "_" & row.Cells(3).Text & ".pdf"
        TextBox1.Text = row.Cells(7).Text
        TextBox2.Text = row.Cells(11).Text
        TextBox3.Text = row.Cells(2).Text
        TextBox4.Text = row.Cells(6).Text
        TextBox5.Text = row.Cells(3).Text
        TextBox7.Text = row.Cells(3).Text
        TextBox8.Text = row.Cells(14).Text
        TextBox10.Text = row.Cells(3).Text
        Textdocid.Text = row.Cells(3).Text
        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If
        'Export

        

    End Sub
    Protected Sub GVBnk_CCFees_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVBnk_CCFees.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVBnk_CCFees.Rows(index)

        Eventyear = row.Cells(3).Text
        Transcat = row.Cells(9).Text
        BankId = row.Cells(2).Text
        Transdate = row.Cells(6).Text
        chkdepnumber = row.Cells(10).Text
        TextBox6.Text = row.Cells(11).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(3).Text & "_" & row.Cells(9).Text & "_" & row.Cells(2).Text & "_" & editdate & "_" & row.Cells(10).Text & ".pdf"
        TextBox1.Text = row.Cells(3).Text
        TextBox2.Text = row.Cells(9).Text
        TextBox3.Text = row.Cells(2).Text
        TextBox4.Text = row.Cells(6).Text
        TextBox5.Text = row.Cells(10).Text
        TextBox7.Text = row.Cells(10).Text
        TextBox8.Text = row.Cells(11).Text
        TextBox10.Text = row.Cells(10).Text

        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
          
    End Sub

    Protected Sub gvExp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvExp.PageIndexChanging
        Dim dsExp As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsExp = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("ManageVouchersSQl"))
        gvExp.DataSource = dsExp
        gvExp.PageIndex = e.NewPageIndex
        gvExp.DataBind()
    End Sub

    Protected Sub gvExp_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvExp.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = gvExp.Rows(index)


        Eventyear = row.Cells(0).Text
        Transcat = row.Cells(1).Text
        BankId = row.Cells(8).Text
        Transdate = row.Cells(6).Text
        chkdepnumber = row.Cells(2).Text
        TextBox6.Text = row.Cells(10).Text
        TextBox8.Text = row.Cells(10).Text
        TextBox10.Text = row.Cells(5).Text
        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(0).Text & "_" & row.Cells(1).Text & "_" & row.Cells(8).Text & "_" & editdate & "_" & row.Cells(2).Text & ".pdf"
        TextBox1.Text = row.Cells(0).Text
        TextBox2.Text = row.Cells(1).Text
        TextBox3.Text = row.Cells(8).Text
        TextBox4.Text = row.Cells(6).Text
        TextBox5.Text = row.Cells(2).Text
        TextBox7.Text = row.Cells(2).Text
        Textdocid.Text = row.Cells(2).Text
        Pnlupload.Visible = True
        'upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False
        ' Label1.Text = ""


        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If
       
    End Sub

    Protected Sub GVCCRevenues_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVCCRevenues.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVCCRevenues.Rows(index)




        Eventyear = row.Cells(4).Text
        Transcat = row.Cells(3).Text
        BankId = row.Cells(5).Text
        Transdate = row.Cells(6).Text
        chkdepnumber = row.Cells(5).Text
        TextBox6.Text = row.Cells(9).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(4).Text & "_" & row.Cells(3).Text & "_" & row.Cells(5).Text & "_" & editdate & "_" & row.Cells(5).Text & ".pdf"
        TextBox1.Text = row.Cells(4).Text
        TextBox2.Text = row.Cells(3).Text
        TextBox3.Text = row.Cells(5).Text
        TextBox4.Text = row.Cells(6).Text
        TextBox5.Text = row.Cells(5).Text
        TextBox7.Text = row.Cells(5).Text
        TextBox8.Text = row.Cells(9).Text
        TextBox10.Text = row.Cells(5).Text
        Textdocid.Text = row.Cells(10).Text
        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If

        'Export
        'Dim gvtemp As New GridView


        'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
        ' having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0



    End Sub
    Protected Sub GVACT_EFTDon_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVACT_EFTDon.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVACT_EFTDon.Rows(index)
        Dim frmdate As Date = Convert.ToDateTime(row.Cells(7).Text)
        Dim todate As DateTime = Convert.ToDateTime(row.Cells(8).Text)
        Dim BankID As Integer = row.Cells(3).Text
        Dim BankTransID As Integer = row.Cells(16).Text
        Eventyear = row.Cells(0).Text
        Transcat = row.Cells(6).Text
        BankID = row.Cells(3).Text
        Transdate = row.Cells(5).Text
        chkdepnumber = row.Cells(4).Text
        TextBox6.Text = row.Cells(11).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(0).Text & "_" & row.Cells(6).Text & "_" & row.Cells(3).Text & "_" & editdate & "_" & row.Cells(4).Text & ".pdf"
        TextBoxval.Text = row.Cells(0).Text & "_" & row.Cells(6).Text & "_" & row.Cells(3).Text & "_" & editdate & "_" & row.Cells(4).Text
        TextBox1.Text = row.Cells(0).Text
        TextBox2.Text = row.Cells(6).Text
        TextBox3.Text = row.Cells(3).Text
        TextBox4.Text = row.Cells(5).Text
        TextBox5.Text = row.Cells(10).Text
        TextBox7.Text = row.Cells(0).Text
        TextBox8.Text = row.Cells(17).Text
        TextBox10.Text = row.Cells(16).Text
        Textdocid.Text = row.Cells(4).Text
        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If

        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If
       
    End Sub

    Protected Sub GVTDAIncome_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GVTDAIncome.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GVTDAIncome.Rows(index)
        'select SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee),Sum(NFG.TotalPayment) from NFG_Transactions NFG where NFG.[Payment Date] Between '" & TxtFrom.Text & "' AND '" & txtTo.Text & "'
        'having SUM(NFG.[Contribution Amount]+NFG.MealsAmount+NFG.Fee) > 0

        'Export
        'Dim gvtemp As New GridView
        Eventyear = row.Cells(7).Text
        Transcat = row.Cells(6).Text
        BankId = row.Cells(9).Text
        Transdate = row.Cells(3).Text
        chkdepnumber = row.Cells(8).Text
        TextBox6.Text = row.Cells(12).Text
        Pnlupload.Visible = True

        Dim month As String
        Dim editdate As String

        Dim date1 As String
        Dim year1 As String
        Dim checkfile As String() = Transdate.Split("/")
        If checkfile(0).Length = 1 Then
            month = "0" + checkfile(0)
        Else

            month = checkfile(0)
        End If
        If checkfile(1).Length = 1 Then
            date1 = "0" + checkfile(1)
        Else

            date1 = checkfile(1)
        End If
        year1 = checkfile(2)
        editdate = month + "-" + date1 + "-" + year1
        ' day = Transdate[0].length==1 ? "0"+Transdate[0] : Transdate[0]
        ' month = date[1].length==1 ? "0"+frmdate[1] : frmdate[1]
        '  date = ""+day+month+date[2]

        Txtstring.Text = row.Cells(7).Text & "_" & row.Cells(6).Text & "_" & row.Cells(9).Text & "_" & editdate & "_" & row.Cells(8).Text & ".pdf"
        TextBox1.Text = row.Cells(7).Text
        TextBox2.Text = row.Cells(6).Text
        TextBox3.Text = row.Cells(9).Text
        TextBox4.Text = row.Cells(3).Text
        TextBox5.Text = row.Cells(8).Text
        TextBox7.Text = row.Cells(8).Text
        TextBox8.Text = row.Cells(12).Text
        TextBox10.Text = row.Cells(8).Text
        Textdocid.Text = row.Cells(8).Text

        Pnlupload.Visible = True
        ' upformat.Visible = False
        BtnSubmit1.Visible = True
        Label2.Visible = True
        Label2.Text = "File format:" & Txtstring.Text
        Label4.Visible = False

        If e.CommandName = "DownLoad" Then
            Pnlupload.Visible = False
            Download()
        End If
        If e.CommandName = "DOCID" Then
            Pnlupload.Visible = False
            displaypopup()


        End If
        'Export
     
               
    End Sub

    Function GetData(ByVal Grdvw As GridView) As String
        Dim dsTransDonation As New DataSet
        Dim Filename As String = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrTrans"))
        If dsTransDonation.Tables(0).Rows.Count > 0 Then
            dsTransDonation.Tables(0).Columns.Remove("DonationID")
            dsTransDonation.Tables(0).Columns.Remove("ChapterID")
            Filename = "DGN" & dsTransDonation.Tables(0).Rows(0)("DepositDate").ToString().Replace("/", "") & dsTransDonation.Tables(0).Rows(0)("DepositSlip") & "_" & Now.Month & "_" & Now.Day & "_" & Now.Year
            Grdvw.DataSource = dsTransDonation.Tables(0)
            Grdvw.DataBind()
        Else
            Grdvw.DataSource = Nothing
            Grdvw.DataBind()
        End If
        Return Filename
    End Function

    Public Sub downfile(ByVal filename As String, ByVal sb As StringBuilder)
        'Dim file As System.IO.FileInfo = New System.IO.FileInfo(filename) '-- if the file exists on the server
        'If file.Exists Then 'set appropriate headers
        Response.Clear()
        Response.AddHeader("Content-Disposition", "attachment; filename=" & filename)
        Response.AddHeader("Content-Length", sb.Length.ToString())
        Response.ContentType = "application/octet-stream"
        Response.Write(sb.ToString())
        ' Response.WriteFile(file.FullName)
        Response.End() 'if file does not exist
        'Else
        'Response.Write("This file does not exist.")
        'End If 'nothing in the URL as HTTP GET
    End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal dvregistrationchapter As Control)
    End Sub

    Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        TrDetailView.Visible = False
        btnClose.Visible = False
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("Frmdate")
        Session.Remove("ToDate")
        Session.Remove("ManageVouchersSQl")
        Session.Remove("DCatID")
        Session.Remove("StrTrans")
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub
    Public Sub LoadErrorData()
        Dim dsTransDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Try
            dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrTrans"))
            If dsTransDonation.Tables(0).Rows.Count > 0 Then
                'trEdit.Visible = True
                btnClose.Visible = True
                TrDetailView.Visible = True
                lblErr.Text = ""
                grdEditVoucher.DataSource = dsTransDonation.Tables(0)
                grdEditVoucher.DataBind()
            Else
                grdEditVoucher.DataSource = Nothing
                grdEditVoucher.DataBind()
                lblErr.Text = "Sorry No detailed view to show"
                ' Tredit.Visible = False
                TrDetailView.Visible = False
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub grdEditVoucher_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdEditVoucher.CancelCommand
        grdEditVoucher.EditItemIndex = -1
        LoadErrorData()
    End Sub
    Protected Sub grdEditVoucher_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        grdEditVoucher.EditItemIndex = CInt(e.Item.ItemIndex)
        DonationID = CInt(e.Item.Cells(1).Text)
        DonorType = CType(e.Item.Cells(6).FindControl("lblDonorType"), Label).Text
        BusType = CType(e.Item.Cells(7).FindControl("lblBusType"), Label).Text
        IRSCat = CType(e.Item.Cells(8).FindControl("lblIRSCat"), Label).Text
        DonationType = CType(e.Item.Cells(9).FindControl("lblDonationType"), Label).Text
        ChapterCode = CType(e.Item.Cells(10).FindControl("lblChapterCode"), Label).Text
        Amount = CType(e.Item.Cells(11).FindControl("lblAmount"), Label).Text
        Session("DonationID") = DonationID
        Session("DonorType") = DonorType
        Session("BusType") = BusType
        Session("IRSCat") = IRSCat
        Session("DonationType") = DonationType
        Session("ChapterCode") = ChapterCode
        LoadErrorData()

    End Sub
    

    Public Sub SetDropDown_BusType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("BusType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_DonorType(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonorType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_Chapter(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select ChapterID,WebFolderName from Chapter order by State,Chaptercode")
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("ChapterCode")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_donationtype(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("DonationType")))
        Catch ex As Exception

        End Try
    End Sub
    Public Sub SetDropDown_IRSCat(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Session("IRSCat")))
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub GVGLTemp_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs)
        Dim dsDonation As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("ManageVouchersSQl"))
        GVGLTemp.DataSource = dsDonation
        GVGLTemp.PageIndex = e.NewPageIndex
        GVGLTemp.DataBind()
       
    End Sub

    

    Protected Sub ddlCategory_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlCategory.SelectedIndexChanged
        tblDupRec.Visible = False
        TxtFrom.Text = ""
        txtTo.Text = ""
        If ddlCategory.SelectedValue = 4 Then
            trbanks.Visible = True
            loadbank()
           
        Else
            trbanks.Visible = False
        End If


    End Sub


    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        Try
            ' Pnlupload.Visible = True
            ' Label1.Visible = False
            'Label2.Visible = False
            'BtnSubmit1.Visible = False
            'upformat.Visible = True
            Dim strmessage As String
            strmessage = "File uploaded successfully as: " + Txtstring.Text
            Response.Write("<script language='javascript'>alert('" + strmessage + "');</script>")
            tblDupRec.Visible = False
            buttoninsert()
            displayfilename()
        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        Try
            tblDupRec.Visible = False
            Pnlupload.Visible = True

            Dim str As String

            str = Server.MapPath("~/ScanFinDoc/") & Txtstring.Text

            'FileUpLoad1.SaveAs(Server.MapPath("Private/ScanFinDoc/") & FileUpLoad1.FileName)
            File.Delete(str)


            'File.Delete(Server.MapPath("Private/ScanFinDoc/") & str)
            Dim strfilename As String
            strfilename = Txtstring.Text
            strSql = "Delete  from scanfindoc where DocName ='" + Txtstring.Text + "' "
            'value = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, sqlstring);
            value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            buttoninsert()
            displayfilename()
            ' upformat.Visible = True
            Pnlupload.Visible = False
            'Label3.Visible = True
            ' Label3.Text = str
       Catch ex As Exception
            Label3.Visible = True
            Label3.Text = "ERROR: " & ex.Message.ToString()
 End Try
    End Sub
    

    Function GetGridData(ByVal Grdvw As GridView) As String
        Dim dsTransDonation As New DataSet
        Dim Filename As String = ""
        Dim conn As New SqlConnection(Application("ConnectionString"))
        dsTransDonation = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSQL"))
        If dsTransDonation.Tables(0).Rows.Count > 0 Then
            If ddlCategory.SelectedValue = 1 Or ddlCategory.SelectedValue = 2 Then
                dsTransDonation.Tables(0).Columns.RemoveAt(6)
                dsTransDonation.Tables(0).Columns.RemoveAt(8)
                'ElseIf ddlCategory.SelectedValue = 5 Then

            End If
            Filename = ddlCategory.SelectedItem.Text.Replace(" ", "").Trim & "_" & TxtFrom.Text & "_" & txtTo.Text
            Grdvw.DataSource = dsTransDonation.Tables(0)
            Grdvw.DataBind()

        Else
            Grdvw.DataSource = Nothing
            Grdvw.DataBind()
        End If
        Return Filename
    End Function

    Protected Sub gvExp_RowDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewRowEventArgs) Handles gvExp.RowDataBound
       


    End Sub

    'Protected Sub upformat_Click(sender As Object, e As EventArgs) Handles upformat.Click
    '  uploadremanedfile()
    'End Sub

    Protected Sub yesdup_Click(sender As Object, e As EventArgs) Handles yesdup.Click

        insertfilemodification()
        Dim str As String

        str = Server.MapPath("~/ScanFinDoc/" & Txtstring.Text)
        File.Delete(str)
        'FileUpLoad1.SaveAs(Server.MapPath("Private/ScanFinDoc/") & FileUpLoad1.FileName)

        Dim file1 As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("~/ScanFinDoc/Test.pdf"))
      
        file1.MoveTo(Server.MapPath("~/ScanFinDoc/" & Txtstring.Text))
        displayfilename()
        Tbldup.Visible = False
        Dim strmessage As String
        strmessage = "File uploaded successfully as: " + Txtstring.Text
        Response.Write("<script language='javascript'>alert('" + strmessage + "');</script>")
        displayfilename()
        ' Response.Write("<script language='javascript'>alert('Please upload the Files');</script>")
        ' FileUpLoad1.SaveAs(Server.MapPath("Private/ScanFinDoc/") & FileUpLoad1.FileName)
        'File.Delete(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
        ' File.Move(string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value), string.Concat(Server.MapPath(System.Configuration.ConfigurationManager.AppSettings["TestPapersTempPath"].ToString()), hdnTempFileName.Value.Replace("Temp_", "").ToString()));
    End Sub

    

    Protected Sub nodup_Click(sender As Object, e As EventArgs) Handles nodup.Click
        Tbldup.Visible = False
        Dim str As String

        str = Server.MapPath("~/ScanFinDoc/Test.pdf")

        'FileUpLoad1.SaveAs(Server.MapPath("Private/ScanFinDoc/") & FileUpLoad1.FileName)
        File.Delete(str)
        displayfilename()
    End Sub

    Protected Sub lbprevious_Click(sender As Object, e As EventArgs) Handles lbprevious.Click
        lbprevious.Visible = False
        Gridpopup.Visible = False
        buttoninsert()
        displayfilename()
    End Sub
End Class
