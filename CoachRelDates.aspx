<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="CoachRelDates.aspx.cs" Inherits="CoachRelDates" Title="Coach Release Date Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

    <script type="text/javascript">

        $(function () {
            var pickerOpts = {
                dateFormat: $.datepicker.regional['fr']
            };
            $("#<%=tboxQrelease.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=tboxQdeadline.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=tboxArelease.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=tboxSrelease.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtQR.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtQD.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtAD.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });
            $("#<%=txtSD.ClientID %>").datepicker({ dateFormat: 'mm/dd/yy' });

        });

    </script>

    <script type="text/javascript">
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Do you want to Delete the Record?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }
    </script>

    <style type="text/css">
        .ui-datepicker-trigger {
            padding-left: 5px;
            position: relative;
            top: 3px;
        }

        .ui-datepicker {
            font-size: 8pt !important;
        }

        .style8 {
            width: 99px;
        }

        .style10 {
            width: 147px;
        }

        .style12 {
            width: 84px;
            text-align: left;
        }

        .style16 {
            width: 126px;
        }

        .style17 {
            width: 88px;
        }

        .style20 {
            width: 113px;
            text-align: left;
        }

        .style47 {
            width: 100%;
        }

        .style53 {
            width: 179px;
        }

        .style60 {
            width: 68px;
        }

        .style67 {
            width: 15px;
            font-weight: bold;
        }

        .style70 {
            width: 286px;
        }

        .style87 {
        }

        .style90 {
            width: 184px;
        }

        .style95 {
            width: 93px;
        }

        .style97 {
            width: 117px;
        }

        .style98 {
            width: 37px;
        }

        .style99 {
            width: 120px;
            text-align: left;
        }

        .style101 {
            width: 116px;
        }

        .style102 {
            width: 10px;
        }

        .style118 {
            width: 286px;
            text-align: left;
        }

        .style120 {
            width: 15px;
        }

        .style121 {
            width: 63px;
        }

        .style129 {
            width: 133px;
        }

        .style1 {
            width: 100%;
            height: 92px;
        }

        .style158 {
            width: 95px;
        }

        .style160 {
            width: 271px;
            text-align: left;
        }

        .style162 {
            width: 121px;
        }

        .style163 {
            width: 4px;
        }

        .style165 {
        }

        .style167 {
            text-align: left;
        }

        .style176 {
            width: 199px;
            text-align: left;
        }

        .style177 {
            width: 101px;
            text-align: left;
        }

        .style178 {
            width: 95px;
            text-align: left;
        }

        .style185 {
            width: 152px;
        }

        .style186 {
            width: 101px;
        }

        .style187 {
            width: 181px;
            text-align: left;
        }

        .style191 {
            width: 158px;
        }

        .style195 {
            font-weight: bold;
            width: 158px;
        }

        .style197 {
            width: 54px;
            font-weight: bold;
        }

        .style198 {
            width: 54px;
        }

        .style199 {
            text-align: left;
            width: 182px;
        }

        .style200 {
            width: 120px;
        }

        .style201 {
            width: 179px;
            height: 29px;
        }

        .style202 {
            width: 68px;
            height: 29px;
        }

        .style203 {
            width: 15px;
            height: 29px;
        }

        .style204 {
            width: 10px;
            height: 29px;
        }

        .style205 {
            width: 117px;
            height: 29px;
        }

        .style206 {
            height: 29px;
        }

        .style207 {
            width: 286px;
            height: 29px;
        }
    </style>
    <div align="left">

        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="VolunteerFunctions.aspx" runat="server">&nbsp; Back to Volunteer Functions</asp:HyperLink>
    </div>


    <center>
        <asp:DropDownList ID="dllfileChoice" runat="server" AutoPostBack="True"
            OnSelectedIndexChanged="dllfileChoice_SelectedIndexChanged"
            Style="height: 22px">
        </asp:DropDownList>
    </center>


    <asp:Panel ID="Panel6" runat="server" Style="text-align: center"
        Visible="False">
        <br />
        <center>

            <asp:Label ID="Label10" runat="server" Text="Modify Release Dates" Font-Bold="True"
                Font-Size="Large"
                Style="font-family: Arial, Helvetica, sans-serif; font-size: large;"
                ForeColor="#009900"></asp:Label>
        </center>
        <br />
        <br />
        <center>
            <table class="style47">
                <tr>
                    <td class="style53">&nbsp;</td>
                    <td class="style60">&nbsp;</td>
                    <td class="style67">&nbsp;</td>
                    <td bgcolor="Honeydew" style="font-weight: bold;" class="style102">Year</td>
                    <td class="style97" style="font-weight: bold;" bgcolor="Honeydew">Event</td>
                    <td class="style97" style="font-weight: bold;" bgcolor="Honeydew">Semester</td>
                    <td class="style97" style="font-weight: bold;" bgcolor="Honeydew">Coach</td>
                    <td class="style87" style="font-weight: bold;" bgcolor="Honeydew">Product Group Code</td>
                    <td class="style101" style="font-weight: bold;" bgcolor="Honeydew">Product Code</td>
                    <td bgcolor="Honeydew" style="font-weight: bold;" class="style163">Level</td>
                    <td bgcolor="Honeydew" style="font-weight: bold;" class="style163">Session#</td>
                    <td class="style95" style="font-weight: bold;" bgcolor="Honeydew">Paper Type</td>
                    <td class="style98" style="font-weight: bold;" bgcolor="Honeydew">Week#</td>
                    <td bgcolor="Honeydew">Set#</td>

                    <td class="style70">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
                <tr>
                    <td class="style53"></td>
                    <td class="style60"></td>
                    <td class="style120"></td>
                    <td class="style102">
                        <asp:DropDownList ID="ddlFlrvYear" runat="server" Height="20px" Width="89px" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFlrvYear_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style97">
                        <asp:DropDownList ID="ddlFlrvEvent" runat="server" Height="20px" Width="110px">
                        </asp:DropDownList>
                    </td>
                    <td class="style97">
                        <asp:DropDownList ID="ddlSemester" runat="server" Height="22px" Width="145px" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style97">
                        <asp:DropDownList ID="DDLACoach" AutoPostBack="true" Width="110px" runat="server"
                            OnSelectedIndexChanged="DDLACoach_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style87">
                        <asp:DropDownList ID="ddlFlrvProductGroup" runat="server" AutoPostBack="true"
                            Height="20px"
                            Width="148px"
                            OnSelectedIndexChanged="ddlFlrvProductGroup_SelectedIndexChanged1">
                        </asp:DropDownList>
                    </td>
                    <td class="style101">
                        <asp:DropDownList ID="ddlFlrvProduct" runat="server" Height="20px"
                            Width="115px"
                            OnSelectedIndexChanged="ddlFlrvProduct_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td class="style163">
                        <asp:DropDownList ID="ddlFlrvLevel" runat="server" Width="100px">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlSessionM" runat="server" Enabled="False" Height="22px"
                            Width="108px">
                        </asp:DropDownList>
                    </td>
                    <td class="style95">
                        <asp:DropDownList ID="ddlFlrvPaperType" runat="server" Height="20px"
                            Width="117px">
                        </asp:DropDownList>
                    </td>
                    <td class="style98">
                        <asp:DropDownList ID="ddlFlrvWeek" runat="server" Height="20px" Width="90px" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFlrvWeek_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td>
                        <asp:DropDownList ID="ddlFlrvSet" runat="server" Height="20px" Enabled="false">
                        </asp:DropDownList>
                    </td>
                    <td class="style70"></td>
                    <td></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="style201"></td>
                    <td class="style202"></td>
                    <td class="style203"></td>
                    <td class="style204">
                        <asp:Button ID="btnvSerarch" runat="server" Height="25px"
                            OnClick="btnvSerarch_Click" Text="Search" Width="88px" />
                    </td>
                    <td class="style205">
                        <asp:Button ID="btnvReset" runat="server" Text="Reset" Width="83px"
                            OnClick="btnvReset_Click" Height="25px" />
                    </td>
                    <td class="style206" colspan="5">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="style207" colspan="2"></td>
                    <td class="style206"></td>
                    <td class="style206"></td>
                </tr>
                <tr>
                    <td class="style53">&nbsp;</td>
                    <td class="style60">&nbsp;</td>
                    <td class="style120">&nbsp;</td>
                    <td class="style102">&nbsp;</td>
                    <td class="style97">&nbsp;</td>
                    <td class="style87">&nbsp;</td>
                    <td class="style101"></td>
                    <td class="style163">&nbsp;</td>
                    <td class="style90" colspan="2">&nbsp;</td>
                    <td class="style70" colspan="2">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </center>
    </asp:Panel>
    <div style="clear: both; margin-bottom: 5px;"></div>
    <center>
        <asp:Label ID="Label16" runat="server" ForeColor="Red"></asp:Label></center>
    <div>


        <asp:Panel ID="Panel7" runat="server" Visible="False">
            <center>
                <table class="style47">
                    <tr>
                        <td class="style53"></td>
                        <td class="style121"></td>
                        <td class="style197"></td>
                        <td class="style195"></td>
                        <td class="style99" style="display: none;">Coach Name</td>
                        <td class="style176" style="display: none;">
                            <asp:DropDownList ID="ddlNameM" runat="server" AutoPostBack="True"
                                Height="22px" OnSelectedIndexChanged="ddlNameM_SelectedIndexChanged"
                                Width="190px">
                            </asp:DropDownList>
                        </td>

                        <td class="style165">&nbsp;</td>
                        <td class="style70"></td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="style53">&nbsp;&nbsp;</td>
                        <td class="style121"></td>
                        <td class="style197"></td>
                        <td class="style195"></td>
                        <td class="style99">Q-Release Date&nbsp;
                        </td>
                        <td class="style176">
                            <asp:TextBox ID="txtQR" runat="server" AutoPostBack="True" Enabled="False"
                                OnTextChanged="txtQR_TextChanged" Width="106px"></asp:TextBox>
                            <asp:Label ID="Label11" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td class="style99">A-Release Date&nbsp;
                        </td>
                        <td class="style187">
                            <asp:TextBox ID="txtAD" runat="server" AutoPostBack="True" Enabled="False"
                                OnTextChanged="txtAD_TextChanged" Width="106px"></asp:TextBox>
                            <asp:Label ID="Label13" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td class="style165" colspan="2">
                            <asp:Label ID="Label15" runat="server" Font-Bold="True"></asp:Label>
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="style53"></td>
                        <td class="style121"></td>
                        <td class="style197"></td>
                        <td class="style191"></td>
                        <td class="style99">
                            <span style="text-align: left">Q-Deadline Date</span></td>
                        <td class="style176">
                            <b>
                                <asp:TextBox ID="txtQD" runat="server" Width="106px" Enabled="False"
                                    AutoPostBack="True" OnTextChanged="txtQD_TextChanged"></asp:TextBox>
                                <b>
                                    <asp:Label ID="Label12" runat="server" ForeColor="Red" Font-Bold="False"></asp:Label>
                                </b></b>
                        </td>
                        <td class="style99">S-Release Date</td>
                        <td class="style187">
                            <asp:TextBox ID="txtSD" runat="server" Width="106px" Enabled="False"
                                AutoPostBack="True" OnTextChanged="txtSD_TextChanged"></asp:TextBox>
                            <asp:Label ID="Label14" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td class="style165">
                            <asp:Button ID="btnUpdate" runat="server" Enabled="False"
                                OnClick="Button2_Click" Text="Update" Height="25px"
                                Width="88px" />
                        </td>
                        <td class="style118">
                            <asp:Button ID="btnDelete" runat="server" OnClientClick="Confirm()"
                                Text="Delete" Width="88px" OnClick="btnDelete_Click1" Enabled="False"
                                Height="25px" />
                        </td>
                        <td></td>
                        <td></td>
                    </tr>
                    <tr>
                        <td class="style53">&nbsp;</td>
                        <td class="style121">&nbsp;</td>
                        <td class="style198">&nbsp;</td>
                        <td class="style191">&nbsp;</td>
                        <td class="style200"></td>
                        <td class="style176">&nbsp;</td>
                        <td class="style99"></td>
                        <td class="style187">&nbsp;</td>
                        <td class="style70" colspan="2">&nbsp;</td>
                        <td>&nbsp;</td>
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </center>
        </asp:Panel>

    </div>
    <asp:Panel ID="Panel1" runat="server" BorderWidth="1px" BorderColor="White"
        Width="100%" Visible="False" Style="text-align: center"
        BackColor="White">
        <br />
        <center>
            <asp:Label ID="Label8" runat="server" Text="Add Release Dates" Font-Bold="True"
                Font-Size="Large"
                Style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: large;"
                ForeColor="#009900"></asp:Label>
        </center>
        <br />
        <br />
        <table style="width: 100%">
            <tr>
                <td class="style129"></td>
                <td style="font-weight: 700;" bgcolor="Honeydew" class="style12">Year</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style20">Event</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style20">Semester</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style20">Coach</td>

                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style10">&nbsp;Product Group Code</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style129">Product Code</td>
                <td bgcolor="Honeydew" class="style162"
                    style="font-weight: 700; text-align: left;">Level</td>
                <td bgcolor="Honeydew" class="style162"
                    style="font-weight: 700; text-align: left;">Session#</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style16">Paper Type</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style17">Week#</td>
                <td style="font-weight: 700; text-align: left;" bgcolor="Honeydew"
                    class="style8">Set#</td>
                <td></td>
            </tr>
            <tr>
                <td class="style129">&nbsp;</td>
                <td style="width: 84px">
                    <asp:DropDownList ID="ddlFlrYear" runat="server" Height="20px" Width="89px" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlFlrYear_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="style20">
                    <asp:DropDownList ID="ddlFlrEvent" runat="server" Height="20px" Width="110px">
                    </asp:DropDownList>
                </td>
                <td class="style20">
                    <asp:DropDownList ID="ddlFrlSemester" runat="server" Height="20px" Width="110px" OnSelectedIndexChanged="ddlFrlSemester_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td class="style20">
                    <asp:DropDownList ID="DDLMCoach" AutoPostBack="true" runat="server" Height="20px" Width="110px" OnSelectedIndexChanged="DDLMCoach_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="style10">
                    <asp:DropDownList ID="ddlFlrProductGroup" runat="server" AutoPostBack="true"
                        Height="20px" OnSelectedIndexChanged="ddlFlrProductGroup_SelectedIndexChanged"
                        Width="148px">
                    </asp:DropDownList>
                </td>
                <td class="style129">
                    <asp:DropDownList ID="ddlFlrProduct" runat="server" Height="20px" Width="135px"
                        OnSelectedIndexChanged="ddlFlrProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td class="style162">
                    <asp:DropDownList ID="ddlFlrLevel" runat="server" Width="120px">
                    </asp:DropDownList>
                </td>
                <td>
                    <asp:DropDownList ID="ddlSession" runat="server" Width="108px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddlSession_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="width: 126px">
                    <asp:DropDownList ID="ddlFlrPaperType" runat="server" Height="20px"
                        Width="135px">
                    </asp:DropDownList>
                </td>
                <td style="width: 88px">
                    <asp:DropDownList ID="ddlFlrWeek" runat="server" Height="20px" Width="90px" AutoPostBack="true"
                        OnSelectedIndexChanged="ddlFlrWeek_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td class="style8">
                    <asp:DropDownList ID="ddlFlrSet" runat="server" Height="20px" Enabled="false">
                    </asp:DropDownList>
                </td>
                <td>&nbsp;</td>
            </tr>
            <tr>
                <td class="style129">&nbsp;</td>
                <td style="width: 84px">
                    <asp:Button ID="btnSearch" runat="server" Text="Search" Width="88px"
                        OnClick="btnSearch_Click" Height="25px" />
                </td>
                <td class="style20">
                    <asp:Button ID="btnReset" runat="server" Text="Reset" Width="88px"
                        OnClick="btnReset_Click" Height="25px" />
                </td>
                <td colspan="7">
                    <asp:Label ID="lblSearchErr" runat="server" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr>
                <td class="style129">&nbsp;</td>
                <td style="width: 84px">&nbsp;</td>
                <td class="style20">&nbsp;</td>
                <td colspan="7">&nbsp;</td>
            </tr>
        </table>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <center>
            <asp:Label ID="lblsession" runat="server" ForeColor="Red"></asp:Label></center>

        <asp:Panel ID="Panel3" runat="server">
            <table class="style1">
                <tr>
                    <td class="style129"></td>
                    <td class="style185"></td>
                    <td class="style177" style="display: none;">Coach Name </td>
                    <td class="style160" style="display: none;">
                        <asp:DropDownList ID="ddlName" runat="server" AutoPostBack="True"
                            OnSelectedIndexChanged="ddlName_SelectedIndexChanged" Width="190px">
                        </asp:DropDownList>
                    </td>

                    <td class="style167">&nbsp;</td>
                    <td class="style167" colspan="2">&nbsp;</td>
                    <td class="style167" colspan="3">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style129"></td>
                    <td class="style185"></td>
                    <td class="style177">Q-Release Date</td>
                    <td class="style160">
                        <asp:TextBox ID="tboxQrelease" runat="server" AutoPostBack="True"
                            Enabled="False" OnTextChanged="tboxQrelease_TextChanged"
                            Style="text-align: left" Width="108px"></asp:TextBox>
                        <asp:Label ID="lblQR" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="style178">A-Release Date</td>
                    <td class="style199">
                        <asp:TextBox ID="tboxArelease" runat="server" AutoPostBack="True"
                            Enabled="False" OnTextChanged="tboxArelease_TextChanged" Width="108px"></asp:TextBox>
                        <asp:Label ID="lblAR" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td colspan="2" align="left">
                        <asp:Label ID="Label9" runat="server" Font-Bold="True" ForeColor="#33CC33"></asp:Label>
                    </td>
                    <td colspan="2">&nbsp;</td>
                    <td colspan="2">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style129"></td>
                    <td class="style185"></td>
                    <td class="style177">Q-Deadline Date</td>
                    <td class="style160">
                        <asp:TextBox ID="tboxQdeadline" runat="server" AutoPostBack="True"
                            Enabled="False" OnTextChanged="tboxQdeadline_TextChanged" Width="108px"></asp:TextBox>
                        <asp:Label ID="lblQD" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="style178">S-Release Date</td>
                    <td class="style199">
                        <asp:TextBox ID="tboxSrelease" runat="server" AutoPostBack="True"
                            Enabled="False" OnTextChanged="tboxSrelease_TextChanged" Width="108px"></asp:TextBox>
                        <asp:Label ID="lblAD" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td align="left">
                        <asp:Button ID="Button1" runat="server" Enabled="False" Height="25px"
                            OnClick="Button1_Click2" Text="Add" Width="88px" />
                    </td>
                    <td colspan="4"></td>
                    <td></td>
                </tr>
                <tr>
                    <td class="style129">&nbsp;</td>
                    <td class="style185">&nbsp;</td>
                    <td class="style186">&nbsp;</td>
                    <td class="style160">&nbsp;</td>
                    <td class="style158">&nbsp;</td>
                    <td class="style199">&nbsp;</td>
                    <td>&nbsp;</td>
                    <td colspan="4">&nbsp;</td>
                    <td>&nbsp;</td>
                </tr>
            </table>
        </asp:Panel>

    </asp:Panel>
    <div align="center">
        <asp:Label ID="lblNoPermission" runat="server" align="center" ForeColor="Red" Visible="false"></asp:Label>
    </div>

    <asp:GridView ID="gvEdit" runat="server" AutoGenerateColumns="False"
        AutoPostBack="False" CellPadding="4" DataKeyNames="CoachRelID"
        ForeColor="#333333" Height="100px" OnRowDataBound="gvEdit_RowDataBound" OnRowCommand="gvEdit_RowCommand"
        OnSelectedIndexChanged="gvEdit_SelectedIndexChanged"
        Style="text-align: justify" Width="866px">
        <RowStyle BackColor="#F7F6F3" ForeColor="#333333" />
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:CommandField>
            <asp:BoundField DataField="NameOfCoach" HeaderText="CoachName"
                SortExpression="NameOfCoach">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId"
                SortExpression="CoachPaperId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="EventYear" HeaderText="EventYear"
                SortExpression="EventYear">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId"
                SortExpression="ProductId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="EventId" HeaderText="EventId"
                SortExpression="EventId">
                <ControlStyle BackColor="#00CC00" />
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PaperType" HeaderText="PaperType"
                SortExpression="PaperType">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="DocType" HeaderText="DocType"
                SortExpression="DocType">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId"
                SortExpression="ProductGroupId" Visible="False">
                <HeaderStyle BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"
                SortExpression="ProductGroupCode">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"
                SortExpression="ProductCode">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="Semester">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="Session" HeaderText="Session"
                SortExpression="Session">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="Sections" HeaderText="Sec.Num"
                SortExpression="Sections">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="EventCode" HeaderText="EventCode"
                SortExpression="EventCode" Visible="False">
                <HeaderStyle BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName"
                SortExpression="TestFileName" Visible="False">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description"
                SortExpression="Description" Visible="False">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="Password" HeaderText="Password"
                SortExpression="Password" Visible="False">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="QReleaseDate" DataFormatString="{0:MM/dd/yyyy}"
                HeaderText="QReleaseDate" SortExpression="QReleaseDate">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="QDeadlineDate" DataFormatString="{0:MM/dd/yyyy}"
                HeaderText="QDeadlineDate" SortExpression="QDeadlineDate">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="AReleaseDate" DataFormatString="{0:MM/dd/yyyy}"
                HeaderText="AReleaseDate" SortExpression="AReleaseDate">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="SReleaseDate" DataFormatString="{0:MM/dd/yyyy}"
                HeaderText="SReleaseDate" SortExpression="SReleaseDate">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="CoachRelID" HeaderText="CoachRelID"
                SortExpression="CoachRelID" Visible="true">
                <HeaderStyle BorderColor="Black" BackColor="#33CC33" ForeColor="White" />
            </asp:BoundField>
            <asp:TemplateField HeaderStyle-Width="75px" ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">
                <ItemTemplate>
                    <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="MemberID" HeaderText="CoachID"
                SortExpression="MemberID" Visible="false">
                <HeaderStyle BorderColor="Black" />
            </asp:BoundField>--%>
        </Columns>
        <FooterStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <PagerStyle BackColor="#284775" ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#E2DED6" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#5D7B9D" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#999999" />
        <AlternatingRowStyle BackColor="White" ForeColor="#284775" />
    </asp:GridView>

    <asp:GridView ID="gvTestPapers" runat="server" AutoGenerateColumns="False"
        AutoPostBack="False" CellPadding="4" DataKeyNames="CoachPaperId"
        ForeColor="#333333" Height="100px" OnRowDataBound="gvTestPapers_RowDataBound"
        OnRowCommand="gvTestPapers_RowCommand"
        OnSelectedIndexChanged="gvTestPapers_SelectedIndexChanged"
        OnSorting="gvTestPapers_Sorting" Style="text-align: center"
        Width="866px">
        <RowStyle BackColor="#EFF3FB" />
        <Columns>
            <asp:CommandField ButtonType="Button" ShowSelectButton="True">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:CommandField>
            <asp:BoundField DataField="NameOfCoach" HeaderText="CoachName"
                SortExpression="NameOfCoach" Visible="False">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId"
                SortExpression="CoachPaperId">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="EventYear" HeaderText="EventYear"
                SortExpression="EventYear">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductId" HeaderText="ProductId"
                SortExpression="ProductId">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="EventId" HeaderText="EventId"
                SortExpression="EventId">
                <ControlStyle BackColor="#00CC00" />
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PaperType" HeaderText="PaperType"
                SortExpression="PaperType">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="DocType" HeaderText="DocType"
                SortExpression="DocType">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId"
                SortExpression="ProductGroupId" Visible="False">
                <HeaderStyle BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"
                SortExpression="ProductGroupCode">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"
                SortExpression="ProductCode">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="Level">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="Sections" HeaderText="Sec.Num"
                SortExpression="Sections">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="EventCode" HeaderText="EventCode"
                SortExpression="EventCode" Visible="False">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName"
                SortExpression="TestFileName">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description"
                SortExpression="Description">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="Password" HeaderText="Password"
                SortExpression="Password">
                <HeaderStyle BackColor="#33CC33" ForeColor="White" BorderColor="Black" />
                <ItemStyle HorizontalAlign="Left" />
            </asp:BoundField>
        </Columns>
        <FooterStyle Font-Bold="True" ForeColor="White" />
        <PagerStyle ForeColor="White" HorizontalAlign="Center" />
        <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
        <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
        <EditRowStyle BackColor="#2461BF" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>

    <br />

    <br />
    <asp:HiddenField ID="hdntlead" runat="server" />
    <div>

        <div>
        </div>
    </div>

</asp:Content>



