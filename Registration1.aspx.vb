'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/Registration.aspx  
'   Author    : NSF
'   Contact   : NSF
'
'	Desc:	This page enables the user to Register them as Volunteer  
'           to the NSF.
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetVolunteer
'           2) NSF\usp_GetVolunteerSpouse
'           3) NSF\usp_Insert_Volunteer
'           4) NSF\
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Web.SessionState
Imports System.IO
Imports System.Runtime.Serialization.Formatters.Binary
Imports System.Xml.Serialization
Imports System.Runtime.Serialization
Imports System.Xml

Imports nsf.Entities
Imports nsf.Data.SqlClient

Namespace VRegistration


Partial Class Registration
        Inherits System.Web.UI.Page
        'Implements System.Web.UI.ICallbackEventHandler
#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents txtSecEmailSp As System.Web.UI.WebControls.TextBox
    Protected WithEvents ckbAddressSp As System.Web.UI.WebControls.CheckBox


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region
        Dim clientData As String
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
                Server.Transfer("login.aspx")
            End If
            If Session("LoginEmail") Is Nothing Then
                Server.Transfer("login.aspx")
            End If
            lblMsg.Text = String.Empty
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If

            Dim objIndSpouse As New IndSpouse10
            Dim objSpouse As IndSpouse10
            Dim nextPage As Boolean

            'ClientScript.RegisterOnSubmitStatement([GetType], "Form1", "if (this.submitted) return false; this.submitted = true; return true;")
            If Session("nextPageName") Is Nothing Then
                Session("nextPageName") = "MainChild.aspx"
            End If

            hide1.Visible = False
            hide2.Visible = False

            If Page.IsPostBack = False Then

                If Session("entryToken").ToString.ToUpper() = "DONOR" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    rfvHomePhoneInd.Enabled = False
                    hlnkMainMenu.NavigateUrl = "DonorFunctions.aspx"
                ElseIf Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    hlnkMainMenu.NavigateUrl = "VolunteerFunctions.aspx"
                ElseIf Session("entryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Main Page"
                    hlnkMainMenu.NavigateUrl = "UserFunctions.aspx"
                End If
                Page.MaintainScrollPositionOnPostBack = True
                '*** Populate State DropDown
                Dim dsStates As DataSet

                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlStateInd.DataSource = dsStates.Tables(0)
                    ddlStateInd.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlStateInd.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlStateInd.DataBind()
                    ddlStateInd.Items.Insert(0, New ListItem("Select State", String.Empty))

                    ddlStateSp.DataSource = dsStates.Tables(0)
                    ddlStateSp.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlStateSp.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlStateSp.DataBind()
                    ddlStateSp.Items.Insert(0, New ListItem("Select State", String.Empty))

                End If
                ddlStateInd.SelectedIndex = 0
                ddlStateSp.SelectedIndex = 0
                '*** Populate Chapter DropDown
                '*** Populate Chapter Names List
                Dim objChapters As New NorthSouth.BAL.Chapter
                Dim dsChapters As New DataSet
                objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

                If dsChapters.Tables.Count > 0 Then
                    ddlChapterInd.DataSource = dsChapters.Tables(0)
                    ddlChapterInd.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                    ddlChapterInd.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                    ddlChapterInd.DataBind()
                    ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                    If Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                        If Len(Session("LoginChapterID")) > 0 Then
                            ddlChapterInd.SelectedValue = Session("LoginChapterID")
                            ddlChapterInd.Enabled = False
                        End If
                    End If
                    If Session("RoleID") = "5" Then
                        ddlChapterInd.Items.Clear()
                        Dim strSql As String
                        strSql = "Select chapterid, chaptercode, state from chapter "
                        strSql = strSql & " where clusterid in (Select clusterid from "
                        strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                        strSql = strSql & " order by state, chaptercode"
                        Dim con As New SqlConnection(Application("ConnectionString"))
                        Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                        While (drNSFChapters.Read())
                            ddlChapterInd.Items.Add(New ListItem(drNSFChapters(1).ToString(), drNSFChapters(0).ToString()))
                        End While
                        ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                        'ddlChapterInd.Enabled = True
                    End If
                End If
                setSpouseValidators(False)

                If Not Request.QueryString("Type") Is Nothing Then
                    If Request.QueryString("Type") = "Individual" Then
                        If Session("NavPath") = "Sponsor" Then
                            hlnkPrevPage.Visible = True
                            hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        ElseIf Session("NavPath") = "Search" Then
                            hlnkPrevPage.Visible = True
                            hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        Else
                            hlnkPrevPage.Visible = False
                        End If
                    Else
                        Exit Sub
                    End If
                    If Session("NavPath") = "Search" Then
                        rfvEmail.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                    ElseIf Session("NavPath") = "Sponsor" Then
                        rfvEmail.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                    End If
                Else
                    'Get IndID and SpouseID for the givn Logon Person
                    Dim StrIndSpouse As String = ""
                    Dim intIndID As Integer = 0
                    Dim dsIndSpouse As New DataSet
                    If Request.QueryString("Id") Is Nothing Then
                        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
                        hlnkPrevPage.Visible = True

                        If Session("NavPath") = "Sponsor" Then
                            hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        ElseIf Session("NavPath") = "Search" Then
                            hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        Else
                            hlnkPrevPage.Visible = False
                        End If
                    Else
                        StrIndSpouse = "AutoMemberID=" & Request.QueryString("ID")
                    End If

                    If Session("NavPath") = "Search" Then
                        rfvEmail.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        hlnkPrevPage.NavigateUrl = "dbsearchresults.aspx"
                        hlnkPrevPage.Visible = True
                    ElseIf Session("NavPath") = "Sponsor" Then
                        rfvEmail.Enabled = False
                        rfvHomePhoneInd.Enabled = False
                        revPrimaryEmailInd.Enabled = False
                        hlnkPrevPage.NavigateUrl = "search_sponsor.aspx"
                        hlnkPrevPage.Visible = True
                    End If
                    'revSecondaryEMail.Enabled = False                    

                    objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

                    If dsIndSpouse.Tables.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                            If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                                End If
                            Else
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                Else
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                                End If
                            End If
                        Else
                            txtPrimaryEmailInd.Text = Session("LoginEmail")
                            txtPrimaryEmailInd.Enabled = False
                            Try
                                ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                            Catch
                                ddlChapterInd.Items.FindByValue(String.Empty).Selected = True
                            End Try
                        End If
                    Else
                        txtPrimaryEmailInd.Text = Session("LoginEmail")
                        txtPrimaryEmailInd.Enabled = False
                        ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                    End If

                    If intIndID > 0 Then

                        'Start Grade locking code
                        '*******************************************
                        '**** Code Added By FERDINE Jan 05 2010 ****
                        '*******************************************

                        If (Not (Session("RoleId") = Nothing)) Then

                            If (((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5"))) Then
                                MsgBox(intIndID)
                            Else
                                lockchapter(intIndID)

                            End If
                        Else
                            lockchapter(intIndID)
                        End If
                        'End Grade Locking code
                        'check that child has already registered.
                        'Dim objContestant As nsf.Data.SqlClient.SqlContestantProvider
                        'objContestant = New nsf.Data.SqlClient.SqlContestantProvider(Application("ConnectionString"), True, "SqlNetTiersProvider")

                        'Dim regContestants As nsf.Entities.TList(Of Contestant) = objContestant.Find("ContestYear='" + Application("ContestYear") + "' AND ParentID='" + intIndID.ToString + "'")

                        'If regContestants.Count > 0 Then
                        '    ddlChapterInd.Enabled = False
                        'End If
                        'get spouse




                        Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
                        objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), intIndID)
                        If objIndSpouseExt.Id > 0 Then
                            objIndSpouse = objIndSpouseExt
                        End If
                        Me.btnSubmit.Text = "Continue"
                        nextPage = True
                    End If


                    'set spouse validators to false.

                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailSp.ReadOnly = False
                    ddlStateOfOriginInd.Visible = True
                    ddlStateOfOriginSp.Visible = True
                    'txtStateOfOriginInd.Visible = False
                    'txtStateOfOriginSp.Visible = False

                    loadStates(Me.ddlStateOfOriginInd, "IN")
                    loadStates(Me.ddlStateOfOriginSp, "IN")

                    If Not objIndSpouse Is Nothing Then
                        If objIndSpouse.Id > 0 Then
                            Session("IndRegInfo") = objIndSpouse
                            With objIndSpouse
                                Session("CustIndID") = .Id
                                If Trim(.Title.Length) > 0 Then
                                    Try
                                        ddlTitleInd.Items.FindByValue(.Title).Selected = True
                                    Catch
                                    End Try
                                End If
                                txtFirstNameInd.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                'txtLastNameInd.Text = .LastName.Substring(0, 1).ToUpper + .LastName.Substring(1)
                                txtLastNameInd.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                txtAddress1Ind.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                txtAddress2Ind.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                txtCityInd.Text = StrConv(.City, VbStrConv.ProperCase)
                                If Trim(.State.Length) > 0 Then
                                    ddlStateInd.SelectedValue = .State
                                End If
                                txtZipInd.Text = .Zip
                                If Trim(.Country.Length) > 0 Then
                                    ddlCountryInd.SelectedValue = .Country
                                End If
                                If Trim(.Gender.Length) > 0 Then
                                    ddlGenderInd.SelectedValue = .Gender
                                End If
                                txtHomePhoneInd.Text = .HPhone
                                txtWorkPhoneInd.Text = .WPhone
                                txtWorkFaxInd.Text = .WFax
                                txtCellPhoneInd.Text = .CPhone
                                txtPrimaryEmailInd.Text = .Email
                                txtPrimaryEmailInd.Enabled = False
                                txtSecondaryEmailInd.Text = .SecondaryEmail
                                If Trim(.Education.Length) > 0 Then
                                    ddlEducationalInd.SelectedValue = .Education
                                End If
                                If Trim(.Career.Length) > 0 Then
                                    ddlCareerInd.SelectedValue = .Career
                                End If
                                txtEmployerInd.Text = StrConv(.Employer, VbStrConv.ProperCase)
                                If Trim(.CountryOfOrigin.Length) > 0 Then
                                    ddlCountryOfOriginInd.SelectedValue = .CountryOfOrigin
                                End If
                                If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
                                    If Trim(.StateOfOrigin.Length) > 0 Then
                                        ddlStateOfOriginInd.SelectedValue = .StateOfOrigin
                                    End If
                                    'txtStateOfOriginInd.Visible = False
                                Else
                                    txtStateOfOriginInd.Visible = True
                                    txtStateOfOriginInd.Text = .StateOfOrigin
                                    'ddlStateOfOriginInd.Visible = False
                                End If

                                If .VolunteerFlag = "Yes" Or .VolunteerFlag = "No" Then
                                    rbVolunteerInd.SelectedValue = .VolunteerFlag
                                Else
                                    rbVolunteerInd.Items(0).Selected = False
                                    rbVolunteerInd.Items(1).Selected = False
                                End If

                                If .Liaison = "Yes" Or .Liaison = "No" Then
                                    rbLiaisonInd.SelectedValue = .Liaison
                                Else
                                    rbLiaisonInd.Items(0).Selected = False
                                    rbLiaisonInd.Items(1).Selected = False
                                End If

                                If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                    rbSponsorInd.SelectedValue = .Sponsor
                                Else
                                    rbSponsorInd.Items(0).Selected = False
                                    rbSponsorInd.Items(1).Selected = False
                                End If

                                ddlMaritalStatusInd.SelectedValue = .MaritalStatus

                                If Trim(.Chapter.Length) > 0 Then
                                    Try
                                        ddlChapterInd.Items.FindByValue(.ChapterID).Selected = True
                                    Catch
                                    End Try

                                End If

                                If Trim(.ReferredBy.Length) >= 4 And Not IsDBNull(.ReferredBy) Then
                                    ddlReferredByInd.Items.FindByValue(.ReferredBy).Selected = True
                                End If


                                Dim StrSpouse As String = ""
                                Dim intSpouseID As Integer = 0
                                Dim dsSpouse As New DataSet
                                StrSpouse = "Relationship='" & Session("CustIndID") & "'"


                                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                                If dsSpouse.Tables.Count > 0 Then
                                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    End If
                                End If

                                If intSpouseID > 0 Then
                                    'If objSpouse Is Nothing Then
                                    objSpouse = New IndSpouse10
                                    objSpouse.GetIndSpouseByID(Application("ConnectionString"), intSpouseID)
                                    'End If
                                    Session("SpRegInfo") = objSpouse
                                    If Not objSpouse Is Nothing Then
                                        With objSpouse

                                            Session("CustSpouseID") = .Id

                                            If Trim(.Title.Length) > 0 Then
                                                Try
                                                    ddlTitleSp.Items.FindByValue(.Title).Selected = True
                                                Catch
                                                End Try
                                            End If

                                            txtFirstNameSp.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                            txtLastNameSp.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                            txtAddress1Sp.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                            txtAddress2Sp.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                            txtCitySp.Text = StrConv(.City, VbStrConv.ProperCase)

                                            If Trim(.State.Length) > 0 Then
                                                ddlStateSp.SelectedValue = .State
                                            End If

                                            txtZipSp.Text = .Zip

                                            If Trim(.Country.Length) > 0 Then
                                                ddlCountrySp.SelectedValue = .Country
                                            End If

                                            If Trim(.Gender.Length) > 0 Then
                                                ddlGenderSp.SelectedValue = .Gender
                                            End If

                                            txtHomePhoneSp.Text = .HPhone
                                            txtWorkPhoneSp.Text = .WPhone
                                            txtWorkFaxSp.Text = .WFax
                                            txtCellPhoneSp.Text = .CPhone

                                            If Trim(.Email.Length) = 0 Then
                                                'sg 01/26/07 txtPrimaryEmailSp.Text = txtPrimaryEmailInd.Text
                                                txtPrimaryEmailSp.Enabled = True
                                            Else
                                                'sg 01/26/07 If .Email = Session("LoginEmail") Then
                                                txtPrimaryEmailSp.Text = .Email
                                                txtPrimaryEmailSp.Enabled = False
                                                'Else
                                                'txtPrimaryEmailSp.Text = .Email
                                                'txtPrimaryEmailSp.Enabled = False
                                                'End If
                                            End If

                                            txtSecondaryEmailSp.Text = .SecondaryEmail

                                            If Trim(.Education.Length) > 0 Then
                                                ddlEducationSp.SelectedValue = .Education
                                            End If

                                            If Trim(.Career.Length) > 0 Then
                                                ddlCareerSp.SelectedValue = .Career
                                            End If

                                            txtEmployerSp.Text = StrConv(.Employer, VbStrConv.ProperCase)

                                            If Trim(.CountryOfOrigin.Length) > 0 Then
                                                ddlCountryOfOriginSp.SelectedValue = .CountryOfOrigin
                                            End If

                                            If loadStates(Me.ddlStateOfOriginSp, Me.ddlCountryOfOriginSp.SelectedValue) Then
                                                If Trim(.StateOfOrigin.Length) > 0 Then
                                                    ddlStateOfOriginSp.SelectedValue = .StateOfOrigin
                                                End If
                                                'txtStateOfOriginSp.Visible = False
                                            Else
                                                txtStateOfOriginSp.Visible = True
                                                txtStateOfOriginSp.Text = .StateOfOrigin
                                                'ddlStateOfOriginSp.Visible = False
                                            End If

                                            txtStateOfOriginSp.Text = .StateOfOrigin

                                            If .VolunteerFlag = "Yes" Or .VolunteerFlag = "No" Then
                                                rbVolunteerSp.SelectedValue = .VolunteerFlag
                                            Else
                                                rbVolunteerSp.Items(0).Selected = False
                                                rbVolunteerSp.Items(1).Selected = False
                                            End If

                                            If .Liaison = "Yes" Or .Liaison = "No" Then
                                                rbLiaisonSp.SelectedValue = .Liaison
                                            Else
                                                rbLiaisonSp.Items(0).Selected = False
                                                rbLiaisonSp.Items(1).Selected = False
                                            End If

                                            If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                                rbSponsorSp.SelectedValue = .Sponsor
                                            Else
                                                rbSponsorSp.Items(0).Selected = False
                                                rbSponsorSp.Items(1).Selected = False
                                            End If

                                            ddlMaritalStatusSp.SelectedValue = .MaritalStatus

                                            If .PrimaryContact = "Y" Then
                                                Me.ckbPrimaryContact.Checked = True
                                            End If
                                        End With
                                        setSpouseValidators(True)
                                    End If
                                End If
                            End With
                        End If
                        ' Commented by Ferdine
                        'If checkContestantRegistered(Session("CustIndID"), Session("CustSpouseID"), Application("ContestYear")) Then
                        '    Me.ddlChapterInd.Enabled = False
                        'Else
                        '    Me.ddlChapterInd.Enabled = True
                        'End If
                    End If
                End If
            End If
            If Session("RoleID") = "1" Or Session("RoleID") = "2" Or Session("RoleID") = "37" Or Session("RoleID") = "38" Then
                txtPrimaryEmailInd.ReadOnly = False
                txtPrimaryEmailInd.Enabled = True
                txtPrimaryEmailSp.Enabled = True
                txtPrimaryEmailSp.ReadOnly = False
                'setSpouseValidators(False)
                Me.rfvPrimaryEmailSp.Enabled = False
            End If
            If Session("RoleID") = "37" And Session("LoginChapterID") = "48" Then
                If Len(txtPrimaryEmailInd.Text) > 0 Then
                    txtPrimaryEmailInd.ReadOnly = True
                    txtPrimaryEmailInd.Enabled = False
                Else
                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailInd.Enabled = True
                End If
                If Len(txtPrimaryEmailSp.Text) > 0 Then
                    txtPrimaryEmailSp.Enabled = False
                    txtPrimaryEmailSp.ReadOnly = True
                    'setSpouseValidators(False)
                    Me.rfvPrimaryEmailSp.Enabled = False
                Else
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailSp.ReadOnly = False
                    setSpouseValidators(True)
                    Me.rfvPrimaryEmailSp.Enabled = False
                End If
            ElseIf Session("RoleID") = "38" And Session("LoginChapterID") = "48" Then
                If Len(txtPrimaryEmailInd.Text) > 0 Then
                    txtPrimaryEmailInd.ReadOnly = True
                    txtPrimaryEmailInd.Enabled = False
                Else
                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailInd.Enabled = True
                End If
                If Len(txtPrimaryEmailSp.Text) > 0 Then
                    txtPrimaryEmailSp.Enabled = False
                    txtPrimaryEmailSp.ReadOnly = True
                    'setSpouseValidators(False)
                    Me.rfvPrimaryEmailSp.Enabled = False
                Else
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailSp.ReadOnly = False
                    setSpouseValidators(True)
                    Me.rfvPrimaryEmailSp.Enabled = False
                End If
            End If
            'ddlCountryOfOriginInd.Attributes.Add("onchange", "GetStateDetails(this.name+ '-' + this.options[this.selectedIndex].value , 'ddlCountryOfOriginInd');")
            'Dim callBackStateName As String
            'callBackStateName = Page.ClientScript.GetCallbackEventReference(Me, "arg", "StateNameClientCallback", "context")
            'Dim StateNameclientFunction As String
            'StateNameclientFunction = "function GetStateDetails(arg, context){ " + callBackStateName + "; }"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GetStateDetails", StateNameclientFunction, True)

            'ddlCountryOfOriginSp.Attributes.Add("onchange", "GetStateDetails(this.name+ '-' + this.options[this.selectedIndex].value , 'ddlCountryOfOriginSp');")
            'callBackStateName = Page.ClientScript.GetCallbackEventReference(Me, "arg", "StateNameClientCallback", "context")
            'StateNameclientFunction = "function GetStateDetails(arg, context){ " + callBackStateName + "; }"
            'Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "GetStateDetails", StateNameclientFunction, True)

        End Sub
    Private Sub GetSpouseData()

    End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim indId As Integer
            Dim spId As Integer
            Dim strSql As String
            If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                setSpouseValidators(True)
                'If Session("Entrytoken").ToString.ToUpper() <> "VOLUNTEER" Then
                If rbVolunteerSp.SelectedValue = "Yes" And Session("NavPath") <> "Search" Then
                    If Len(txtPrimaryEmailSp.Text) <= 0 Then
                        lblMsg.Visible = True
                        lblMsg.Text = "Primary Email Address Should not be blank."
                        lblMsg.ForeColor = Color.Red
                        Exit Sub
                    End If
                End If
                'End If
            End If
            If Page.IsValid Then
                If ckbPrimaryContact.Checked And txtPrimaryEmailSp.Text.Length < 5 Then
                    lblMsg.Visible = True
                    lblMsg.Text = "Primary Email should not be blank."
                    lblMsg.ForeColor = Color.Red
                    Exit Sub
                End If
                indId = UpdateIndividualInfo()
                If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                    If Session("LoginID") Is Nothing Then
                        Session("LoginID") = indId
                    End If
                End If
                If lblDuplicateMsg.Visible = False Then
                    If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                        If Val(indId) > 0 Then
                            spId = UpdateSpouseInfo(indId)
                        End If
                        'set spouseid for individual.
                        If spId > 0 Then
                            'Dim objIndividual As New IndSpouse10
                            'objIndividual = Session("IndRegInfo")
                            'objIndividual.PrimaryIndSpouseID = spId
                            'objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                            strSql = "UPDATE INDSPOUSE SET PrimaryIndSpouseID=" & spId
                            If Not Session("LoginID") Is Nothing Then
                                strSql = strSql & ",CreatedBy=" & Session("LoginID")
                            Else
                                strSql = strSql & ",CreatedBy=" & indId
                            End If
                            strSql = strSql & " WHERE AutomemberID=" & indId
                            strSql = strSql & " AND DonorType='IND'"

                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                            strSql = "UPDATE INDSPOUSE SET "
                            If Not Session("LoginID") Is Nothing Then
                                strSql = strSql & "CreatedBy=" & Session("LoginID")
                            Else
                                strSql = strSql & "CreatedBy=" & indId
                            End If
                            strSql = strSql & " WHERE AutomemberID=" & spId
                            strSql = strSql & " AND DonorType='SPOUSE'"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

                        End If
                    Else
                        Dim objIndividual As New IndSpouse10
                        objIndividual = Session("IndRegInfo")
                        objIndividual.PrimaryIndSpouseID = Nothing
                        objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                    End If
                End If
                'set session vars for following screens.
                Session("CustIndID") = indId
                Session("CustSpouseID") = spId

                'cleanup...
                Session("IndRegInfo") = Nothing
                Session("SpRegInfo") = Nothing

                If Session("ParentsChapterID") Is Nothing Then
                    Session("ParentsChapterID") = ddlChapterInd.SelectedItem.Value
                End If
                If lblDuplicateMsg.Visible = False Then
                    If rbVolunteerInd.SelectedValue = "Yes" Then
                        Session("VolunteerInd") = True
                    Else
                        Session("VolunteerInd") = False
                    End If
                    If rbVolunteerSp.SelectedValue = "Yes" Then
                        Session("VolunteerSP") = True
                    Else
                        Session("VolunteerSP") = False
                    End If
                    If rbVolunteerInd.SelectedValue = "Yes" Or rbVolunteerSp.SelectedValue = "Yes" Or rbVolunteerSp.SelectedValue Is Nothing Then
                        If Request.QueryString("Id") Is Nothing Then
                            Response.Redirect("VolunteerRole.aspx")
                        Else
                            Response.Redirect("VolunteerRole.aspx?Id=" & Request.QueryString("Id"))
                        End If
                    ElseIf rbVolunteerInd.SelectedValue = "No" Or rbVolunteerSp.SelectedValue = "No" Or rbVolunteerSp.SelectedValue Is Nothing Then
                        If Session("entryToken").ToString.ToUpper = "DONOR" Then
                            Response.Redirect("DonorFunctions.aspx")
                        ElseIf Session("entryToken").ToString.ToUpper = "PARENT" Then
                            Response.Redirect("MainChild.aspx")
                        Else
                            If Session("NavPath") = "Sponsor" Then
                                Response.Redirect("search_sponsor.aspx")
                            ElseIf Session("NavPath") = "Search" Then
                                Response.Redirect("dbSearchResults.aspx")
                            Else
                                btnSubmit.Enabled = False
                                lblDuplicateMsg.Visible = True
                                lblDuplicateMsg.ForeColor = Color.Red
                                lblDuplicateMsg.Text = "Your profile was saved successfully.  Please click on Home to log out."
                            End If
                        End If
                    Else
                        Select Case Session("EventID")
                            Case 1, 2
                                Response.Redirect("MainChild.aspx")
                            Case 3
                                If Request.QueryString("Id") Is Nothing Then
                                    Response.Redirect("VolunteerRole.aspx")
                                Else
                                    Response.Redirect("VolunteerRole.aspx?Id=" & Request.QueryString("Id"))
                                End If
                        End Select
                    End If

                    If Not Request("Action") Is Nothing Then
                        If Request("Action") = "ChapterFunction" Then
                            Response.Redirect("RegistrationList.aspx")
                        End If
                    Else
                        Select Case Session("EventID")
                            Case 1, 2
                                If Request("ParentUpdate") = "True" Then
                                    Response.Redirect("UserFunctions.aspx")
                                End If
                        End Select
                    End If
                End If
            Else

                'If lblDuplicateMsg.Visible = True Then
                'lblDuplicateMsg.Text = "Duplicate Record Found."
                'End If
                lblDuplicateMsg.Visible = False
            End If
        End Sub
        Private Function UpdateIndividualInfo() As Integer
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim objIndividual As New IndSpouse10
            Dim indID As Integer

            If Not Session("IndRegInfo") Is N