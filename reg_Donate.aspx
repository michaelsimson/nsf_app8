<%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false"
    Inherits="VRegistration.Donate" Trace="false" CodeFile="reg_Donate.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <br />
    <br />
    <table width="75%" align="center">
        <tr>
            <td align="center">
                <table border="0" style="border-color: Black; border-style: solid; border-width: 1px"
                    align="cneter">
                    <tr>
                        <td class="ContentSubTitle" valign="top" nowrap align="center" style="color: #0000ff;
                            font-weight: bold; font-family: Arial; font-size: 19pt;height:40px" colspan="2">
                            NSF Donation
                        </td>
                    </tr>
                    <tr>
                        <td style="width: 230px">
                            <asp:Label ID="lblMessage" runat="server" CssClass="largewordingbold"></asp:Label></td>
                        <td>
                        </td>
                    </tr>
                    <tr>
                        <td class="largewordingbold" style="width: 230px; height: 53px;  Font-Size: 14pt;" align="right"  >
                            Optional Donation(US$)&nbsp;&nbsp;
                        </td>
                        <td class="largewordingbold" style="height: 53px" align="left">
                            <table border="0">
                                <tr>
                                    <td>
                                        <asp:TextBox ID="txtDonation" runat="server" Width="80px" Font-Bold="True" Font-Size="Large" Text="250.00"></asp:TextBox>
                                        <a href="reg_donate_tellMeMore.htm" target="_blank">Tell me more </a>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <asp:Label ID="lbltxt" runat="server" Text="Make it Zero if you don't want to donate."></asp:Label>
                                    </td>
                                 </tr>
                                 <tr>
                                    <td colspan="3" align="left">
                                        <asp:RequiredFieldValidator ID="rfvDonationAmount" runat="server" ErrorMessage="Value Required, please enter 0 or amount of your choice"
                                            ControlToValidate="txtDonation" Font-Size="X-Small" Font-Bold="True"></asp:RequiredFieldValidator>
                                        <asp:RangeValidator ID="rngDonationAmount" runat="server" CssClass="SmallFont" ErrorMessage="Donation Amount should be Greater than Zero"
                                            ControlToValidate="txtDonation" Type="Currency" MinimumValue="0" MaximumValue="1000000"></asp:RangeValidator><br />
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td class="largewordingbold" align="right" style="width: 230px; height: 30px">
                            Donation For:&nbsp;&nbsp;</td>
                        <td class="largewordingbold" style="height: 30px" valign="top" align="left">
                            <asp:Panel ID="Panel1" runat="server" CssClass="largewordingbold">
                                <asp:DropDownList ID="ddlDonationFor" runat="server" CssClass="mediumwording">
                                </asp:DropDownList>
                                <asp:Label ID="DonationForLabel" runat="server" CssClass="largewordingbold"></asp:Label>
                                <asp:RequiredFieldValidator ID="rfvDonationFor" runat="server" Font-Bold="True" Font-Size="X-Small"
                                    ControlToValidate="ddlDonationFor" ErrorMessage="Please select an option" CssClass="mediumwording"></asp:RequiredFieldValidator>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr runat="server" id="trv1">
                        <td class="largewordingbold" valign="top" align="left" colspan="2" style="height: 25px">
                            &nbsp;I am interested in supporting the NSF Scholarships in India&nbsp; <font class="mediumwordingbold">
                                (please check one below)</font>
                        </td>
                    </tr>
                    <tr runat="server" id="trv2">
                        <td class="largewordingbold" align="left" valign="top" colspan="2" style="height: 25px">
                            <asp:CheckBox ID="cbSupport1" runat="server" CssClass="mediumwording" Text="I will contribute at least one scholarship to help a poor child go to college in India ($250 per child per year)"
                                Width="700px" AutoPostBack="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr  runat="server" id="trv3">
                        <td class="largewordingbold" align="left" valign="top" colspan="2" style="height: 25px">
                            <asp:CheckBox ID="cbSupport2" runat="server" CssClass="mediumwording" Text="I will make every effort to raise funds for at least one poor child in India ($250 per child per year)"
                                Width="698px" AutoPostBack="True"></asp:CheckBox>
                        </td>
                    </tr>
                    <tr>
                        <td class="largewordingbold" colspan="2" nowrap="noWrap" align="left">
                            &nbsp;I would like to help the following NSF India chapter
                        </td>
                    </tr>
                    <tr style="height:9px">
                    <td colspan="3"></td>
                    </tr>
                    <tr>
                        <td class="largewordingbold" style="width: 230px; height: 53px;" align="right">
                            &nbsp;
                        </td>
                        <td colspan="2" align="left">
                            <asp:DropDownList ID="ddlChapterLiaison" runat="server" CssClass="mediumwording"
                                Width="183px">
                                <asp:ListItem Selected="True">-Select Chapter-</asp:ListItem>
                                <asp:ListItem Value="No Preference">No Preference</asp:ListItem>
                                <asp:ListItem Value="Ahmedabad">Ahmedabad</asp:ListItem>
                                <asp:ListItem Value="Bangalore">Bangalore</asp:ListItem>
                                <asp:ListItem Value="Bhubaneswar">Bhubaneswar</asp:ListItem>
                                <asp:ListItem Value="Chennai">Chennai</asp:ListItem>
                                <asp:ListItem Value="Hyderabad">Hyderabad</asp:ListItem>
                                <asp:ListItem Value="Jamshedpur">Jamshedpur</asp:ListItem>
                                <asp:ListItem Value="Jodhpur">Jodhpur</asp:ListItem>
                                <asp:ListItem Value="Kanpur">Kanpur</asp:ListItem>
                                <asp:ListItem Value="Katihar">Katihar</asp:ListItem>
                                <asp:ListItem Value="Kochi">Kochi</asp:ListItem>
                                <asp:ListItem Value="Kolkata">Kolkata</asp:ListItem>
                                <asp:ListItem Value="Madurai">Madurai</asp:ListItem>
                                <asp:ListItem Value="Pune">Pune</asp:ListItem>
                                <asp:ListItem Value="Sibsagar">Sibsagar</asp:ListItem>
                                <asp:ListItem Value="Trivandrum">Trivandrum</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr><td colspan="2" align="center"><asp:Panel ID="pnlConfirm" runat="server" Visible="false" Font-Bold="true">
                    <span style="color:red;"> You are proceeding with a suggested donation of $250 to help a poor student go to college in India.  Please confirm.</span>  <br />
                    <asp:Button ID="btnYes" runat="server" Text="Yes" /><asp:Button ID="btnNo" Text="No" runat="server" /></asp:Panel></td></tr>
                    <tr>
                        <td>
                        </td>
                        <td class="largewordingbold" align="right">
                            <asp:LinkButton ID="lbContinue" runat="server" Font-Size="X-Large" Font-Bold="True">Continue</asp:LinkButton>
                            &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp; &nbsp;&nbsp;</td>
                    </tr>
                    <!--<asp:ListItem Value="Public health/sanitation in India">Public health/sanitation in India</asp:ListItem>
								<asp:ListItem Value="Mentoring in US">Mentoring in US</asp:ListItem> -->
                </table>
            </td>
        </tr>
    </table>
</asp:Content>
