﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CoachingStatusGUI.aspx.cs" Inherits="CoachingStatusGUI" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link href="css/ClassCal.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="css/HtmlGridTable.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="Scripts/jquery.toastmessage.js"></script>

    <link href="css/jquery.toastmessage.css" rel="stylesheet" />
    <div>
        <script type="text/javascript">

            $(function (e) {

                loadSchedule(0);
            });

            function loadSchedule(coachClassCalID) {
                var jsonData = JSON.stringify({ CoachClassCalID: coachClassCalID });

                $.ajax({
                    type: "POST",
                    url: "CoachingStatusGUI.aspx/ListSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var i = 0;
                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";
                        tblHtml += "<th>Ser#</th>";
                        tblHtml += "<th>Date</th>";
                        tblHtml += "<th>Day</th>";
                        tblHtml += " <th>Time</th>";
                        tblHtml += "<th>Class Stype</th>";
                        tblHtml += "<th>Week#</th>";
                        tblHtml += "<th>Status</th>";
                        tblHtml += "<th>Makeup</th>";
                        tblHtml += "<th>Substitute</th>";
                        tblHtml += "<th>Reason</th>";
                        tblHtml += "<th>HwRelDate</th>";
                        tblHtml += "<th>HWDueDate</th>";
                        tblHtml += "<th>SRelDate</th>";
                        tblHtml += "<th>ARelDate</th>";
                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        var responseCount = 0;
                        var acceptedCount = 0;
                        var i = 0;
                        var surveyTitle = "";
                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {

                                $("#spnCoachname").text(value.CoachName);
                                $("#spnChildName").text(value.ChildName);
                                $("#spnParentname").text(value.ParentName);
                                $("#spnproductGroup").text(value.ProductGroup);
                                $("#spnproduct").text(value.Product);
                                $("#spnLevel").text(value.Level);
                                $("#spnSessionNo").text(value.SessionNo);

                                i++;

                                tblHtml += "<tr>";

                                tblHtml += "<tr>";

                                tblHtml += "<td>" + i + "</td>";
                                tblHtml += "<td>" + value.Date + "</td>";
                                tblHtml += "<td>" + value.Day + "</td>";
                                tblHtml += "<td>" + value.Time + "</td>";
                                tblHtml += "<td>" + value.ClassType + "</td>";
                                tblHtml += "<td>" + value.WeekNo + "</td>";
                                tblHtml += "<td>" + value.Status + "</td>";
                                tblHtml += "<td>" + value.Makeup + "</td>";
                                tblHtml += "<td>" + value.SubstituteCoachName + "</td>";
                                tblHtml += "<td></td>";
                                tblHtml += "<td>" + (value.HWDeadlineDate == null ? "" : value.HWDeadlineDate) + "</td>";
                                tblHtml += "<td>" + (value.HWDueDate == null ? "" : value.HWDueDate) + "</td>";
                                tblHtml += "<td>" + (value.SRelDate == null ? "" : value.SRelDate) + "</td>";
                                tblHtml += "<td>" + (value.ARelDate == null ? "" : value.ARelDate) + "</td>";


                                tblHtml += "</tr>";


                            });


                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";



                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

        </script>
    </div>
    <%--    <link href="css/CoachClassCal.css" rel="stylesheet" />
    <div>
        <center>
            <h2>Coach Class Calendar</h2>
        </center>
    </div>
    <div class="form">

        <div>
            <div style="float: left;">
                <span id="spnEventYear">Event Year</span>
            </div>
            <div style="float: left; margin-left: 10px;">
                <select id="selEventYeat">
                    <option value="0">Seleect</option>
                    <option value="2017-18">2017-18</option>
                    <option value="2016-17" selected="selected">2016-17</option>
                </select>
            </div>
        </div>
        <div>
            <div style="float: left;">
                <span id="Span1">Event</span>
            </div>
            <div style="float: left; margin-left: 10px;">
                <input type="text" id="txtEvent" />
            </div>
        </div>

    </div>--%>

    <div>
        <center>
            <h2>Coach Class Schedule</h2>
        </center>
    </div>


    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        Table 1: Coach Class Calendar
    <div style="clear: both; margin-bottom: 2px;"></div>
        <div style="font-size: 13px;">
            <div style="float: left;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Coach Name: </spa>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnCoachname"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Child Name: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnChildName"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Parent Name: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnParentname"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Product rgroup: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnproductGroup"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Product: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnproduct"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Level: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnLevel"></span>
                </div>
            </div>
            <div style="float: left; margin-left: 10px;">
                <div style="float: left;">
                    <span style="font-weight: bold;">Session No: </span>
                </div>
                <div style="float: left; margin-left: 2px;">
                    <span id="spnSessionNo"></span>
                </div>
            </div>
        </div>
        <div style="clear: both"></div>
        <div>
            <table id="table" style="margin-top: 0px;">
                <%--  <thead>
                <tr>
                    <th>Action</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Class Stype</th>
                    <th>Week#</th>
                    <th>Status</th>
                    <th>Makeup</th>
                    <th>Substitute</th>
                    <th>Reason</th>
                    <th>HwRelDate</th>
                    <th>HWDueDate</th>
                    <th>SRelDate</th>
                    <th>ARelDate</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <button id="Button1" style="padding: 5px; font-size: 16px; cursor: pointer;">Modify</button></td>
                    <td>03/25/2017</td>
                    <td>6:00 PM</td>
                    <td>Regular</td>
                    <td>1</td>
                    <td>On</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>--%>
            </table>
        </div>
</asp:Content>
