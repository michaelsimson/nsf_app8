<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="OnlineWSCal.aspx.vb" EnableSessionState="True" Inherits="OnlineWSCal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.toast.js"></script>
    <link href="css/jquery.toast.css" rel="stylesheet" />
    <script type="text/javaScript">
        var accessToken;
        var orgAnizerKey;
        $(function (e) {
            accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
            orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;
        });
        function DeleteConfirm() {

            if (confirm("Are you sure want to delete?")) {
                document.getElementById("<%=btnDeleteWSCal.ClientID%>").click();
            }
        }
        function ShowAlert() {

            if (confirm("Are you sure want to override the workshop document?")) {
                document.getElementById("<%=btnUpdateDocument.ClientID%>").click();
            }
        }
        function ShowDuplicateAlert() {

            statusMessage("Duplicate exists", "error");

        }

        function createWebinar(utcstartTime, utcendTime) {
            var webinarKey;
            var pgId = document.getElementById("<%=hdnPgId.ClientID%>").value;
            var teacherId = document.getElementById("<%=hdnTeacherId.ClientID%>").value;
            var eventYear = document.getElementById("<%=hdnEventYear.ClientID%>").value;
            var duration = document.getElementById("<%=hdnDuration.ClientID%>").value;
            var startDate = document.getElementById("<%=hdnStartDate.ClientID%>").value;
            var startTime = document.getElementById("<%=hdnStartTime.ClientID%>").value;
            var title = document.getElementById("<%=hdnWebinarTitle.ClientID%>").value;
            var sessionType = "single_session";
            var eventid = document.getElementById("<%=hdnEventId.ClientID%>").value;

            var startUnivTime = utcstartTime;

            var endUnivTime = utcendTime;

            var jsonObject = {
                "subject": title, "description": title, "times": [{ "startTime": startUnivTime, "endTime": endUnivTime }], "timeZone": "America/New_York", "type": "single_session",
                "isPasswordProtected": false
            };

            $.ajax({
                url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", accessToken);
                },
                type: 'POST',
                crossDomain: true,
                dataType: 'json',
                contentType: 'application/json',
                processData: false,
                data: JSON.stringify(jsonObject),
                success: function (data) {

                    var obj = $.parseJSON(JSON.stringify(data));
                    var webinarKey = obj.webinarKey;

                    postWebinarToNSF(webinarKey, "1")
                    //$.each(data, function (index, value) {

                    //    webinarKey = value;

                    //});

                },
                error: function () {
                    // alert("Cannot get data");
                }
            });

        }
        function checkTimeConfictsOfWebinar() {

            var retVal = -1;
            var productId = document.getElementById("<%=hdnPId.ClientID%>").value;

            var teacherId = document.getElementById("<%=hdnTeacherId.ClientID%>").value;
            var eventYear = document.getElementById("<%=hdnEventYear.ClientID%>").value;
            var duration = document.getElementById("<%=hdnDuration.ClientID%>").value;
            var startDate = document.getElementById("<%=hdnStartDate.ClientID%>").value;
            var startTime = document.getElementById("<%=hdnStartTime.ClientID%>").value;
            var jsonData = JSON.stringify({ ObjWebinar: { ProductId: productId, EventYear: eventYear, StartDate: startDate, StartTime: startTime, Duration: duration } });
            $.ajax({
                type: "POST",
                url: "TestGotoWebinar.aspx/ValidateWebinarDateTimeConflicts",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d.length) > 0) {
                        var obj = $.parseJSON(JSON.stringify(data.d));

                        if (obj[0].IsValidation == "true") {

                            $("#hdnUTCStartTime").val(obj[0].UTCStartTime);
                            $("#hdnUTCEndTime").val(obj[0].UTCEndTime);
                            createWebinar(obj[0].UTCStartTime, obj[0].UTCEndTime);
                        } else {
                            var obj = $.parseJSON(JSON.stringify(data.d));
                            var startTime = obj[0].StartTime;
                            var endTime = obj[0].EndTime;
                            statusMessage("There is a time conflict, as there is another Webinar scheduled already. Start time:" + startTime + ", End time: " + endTime + "", "error");
                        }
                    }
                }, error: function () {
                    retVal = -1;
                }
            });


        }
        function statusMessage(message, type) {

            $.toast({
                // heading: 'Can I add <em>icons</em>?',
                text: '<b>' + message + '</b>',
                icon: type,
                position: 'top-center',
                hideAfter: 12000,
                stack: 1

            })


        }
        function postWebinarToNSF(webinarKey, mode) {


            var pgId = document.getElementById("<%=hdnPgId.ClientID%>").value;
            var prdId = document.getElementById("<%=hdnPId.ClientID%>").value;
            var teacherId = document.getElementById("<%=hdnTeacherId.ClientID%>").value;
            var eventYear = document.getElementById("<%=hdnEventYear.ClientID%>").value;
            var duration = document.getElementById("<%=hdnDuration.ClientID%>").value;
            var startDate = document.getElementById("<%=hdnStartDate.ClientID%>").value;
            var startTime = document.getElementById("<%=hdnStartTime.ClientID%>").value;
            var title = document.getElementById("<%=hdnWebinarTitle.ClientID%>").value;
            var sessionType = "single_session";
            var eventid = document.getElementById("<%=hdnEventId.ClientID%>").value;

            var jsonData = JSON.stringify({ ObjWebinar: { WebinarKey: webinarKey, EventYear: eventYear, ProductGroupId: pgId, ProductId: prdId, TeacherId: teacherId, Title: title, StartDate: startDate, StartTime: startTime, SessionType: sessionType, Mode: mode, EventId: eventid, Duration: duration } });


            $.ajax({
                type: "POST",
                url: "TestGotoWebinar.aspx/PostNewWebinar",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) > 0) {

                        if (mode == "1") {
                            statusMessage("Webinar created successfully!", "success");
                            assignCoOrganizer(teacherId, webinarKey);
                        }
                    }
                }, failure: function (e) {

                }
            });

        }

        function assignCoOrganizer(memberId, webinarkey) {
            var jsonData = JSON.stringify({ ObjAttendees: { AutoMemberId: memberId, Webinarkey: webinarkey } });
            $.ajax({
                type: "POST",
                url: "TestGotoWebinar.aspx/ListOrganizer",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $.each(data.d, function (index, value) {
                        var name = value.Name;
                        var email = value.Email;
                        var webinarKey = value.Webinarkey;
                        var memberId = value.AutoMemberId;

                        var jsonObject = [{
                            "external": true, "organizerKey": memberId, "givenName": name, "email": email
                        }];

                        $.ajax({
                            url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/coorganizers",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", accessToken);
                            },
                            type: 'POST',
                            crossDomain: true,
                            dataType: 'json',
                            contentType: 'application/json',
                            processData: false,
                            data: JSON.stringify(jsonObject),
                            success: function (data) {

                                var obj = $.parseJSON(JSON.stringify(data));
                                var joinLink = obj[0].joinLink;
                                var memberKey = obj[0].memberKey
                                var loginID = document.getElementById("<%=hdnLoginId.ClientID%>").value;
                                inserCoorganizer(webinarKey, memberKey, joinLink, loginID, memberId);
                            },
                            error: function () {
                                //  alert("Cannot get data");
                            }
                        });

                    });

                },
                failure: function (response) {

                }
            });
        }
        function inserCoorganizer(webinarKey, memberKey, joinUrl, loginId, memberId) {

            var jsonData = JSON.stringify({ ObjAttendee: { Webinarkey: webinarKey, JoinURL: joinUrl, RegistrantKey: memberKey, LoginId: loginId, AutoMemberId: memberId } });


            $.ajax({
                type: "POST",
                url: "TestGotoWebinar.aspx/InsertCoOrganizers",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) > 0) {

                    }
                }, failure: function (e) {

                }
            });
        }
      
    </script>
    <asp:Button ID="btnDeleteWSCal" runat="server" Style="display: none;" OnClick="btnDeleteWSCal_Click" />
    <asp:Button ID="btnUpdateDocument" runat="server" Text="Update Meeting" Style="display: none;" OnClick="btnUpdateDocument_Click" />

    <div style="text-align: left">
        &nbsp;
     <table width="80%">
         <tr>
             <td>
                 <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton></td>
             <td class="Heading" align="center" style="width: 70%">
                 <strong>
                     <asp:Label ID="lblHeading1" runat="server" Text="Online Workshop Calendar"></asp:Label>
                 </strong>

             </td>

             <td align="right"><a href="javascript:PopupPickerHelp();">Help</a></td>
         </tr>
     </table>

        <script language="javascript" type="text/javascript">
            function PopupPicker(ctl, w, h) {
                var PopupWindow = null;
                settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
                PopupWindow.focus();
            }
            function PopupPickerHelp() {
                var PopupWindow = null;
                settings = 'width=550,height=375,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                PopupWindow = window.open('OnlineWSCalHelp.aspx', 'Online Workshop Calendar Help', settings);
                PopupWindow.focus();
            }

        </script>
    </div>

    <table id="Table1" width="100%" runat="server">
        <%-- <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
        
      </tr>--%>
        <tr>
            <td align="center">

                <table>
                    <tr>
                        <td align="left"><b>Event Year</b></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="120px"></asp:DropDownList>
                        </td>
                        <td align="left"><b>Product Group</b></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList>
                        </td>
                        <td align="left"><b>Product</b></td>
                        <td align="left">
                            <asp:DropDownList ID="ddlProduct" DataTextField="Name"
                                DataValueField="ProductID" AutoPostBack="true"
                                OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false"
                                runat="server" Height="20px" Width="152px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="Tr4" runat="server">
                        <td style="height: 17px;" align="left"><b>Date</b></td>
                        <td id="Td6" style="height: 17px;" runat="server">
                            <asp:TextBox ID="txtStartDate" runat="server" AutoPostBack="true"
                                CssClass="SmallFont" Height="16px" Width="98px"></asp:TextBox>
                            <a href="javascript:PopupPicker('<%=txtStartDate.ClientID%>', 200, 200);">
                                <img src="Images/Calendar.gif" border="0"></a>
                            <asp:Label ID="lbldate" runat="server" Text='mm/dd/yy|TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                        </td>
                        <td align="left" style="height: 17px;"><b>Time</b></td>
                        <td align="left" style="height: 17px;">
                            <asp:DropDownList ID="ddlDisplayTime" runat="server" Height="20px" Width="120px"></asp:DropDownList>
                        </td>
                        <td align="left" style="height: 17px;"><b>Duration</b></td>
                        <td style="height: 17px;">
                            <asp:DropDownList ID="ddlDuration" runat="server" Height="20px" Width="125px">
                                <asp:ListItem Value="0">TBD</asp:ListItem>
                                <asp:ListItem Value="1">1.0</asp:ListItem>
                                <asp:ListItem Value="1.5">1.5</asp:ListItem>
                                <asp:ListItem Value="2">2.0</asp:ListItem>
                                <asp:ListItem Value="2.5">2.5</asp:ListItem>
                                <asp:ListItem Value="3">3.0</asp:ListItem>
                                <asp:ListItem Value="3.5">3.5</asp:ListItem>
                                <asp:ListItem Value="4">4.0</asp:ListItem>
                                <asp:ListItem Value="4.5">4.5</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                                <asp:ListItem Value="5.5">5.5</asp:ListItem>
                                <asp:ListItem Value="6">6.0</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                    </tr>
                    <tr>
                        <td align="left"><b>Reg.Deadline</b> </td>
                        <td>
                            <asp:TextBox ID="txtRegDeadline" runat="server" CssClass="SmallFont"
                                Height="17px" Width="100px"></asp:TextBox>
                            <a href="javascript:PopupPicker('<%=txtRegDeadline.ClientID%>', 200, 200);">
                                <img src="Images/Calendar.gif" border="0">
                            </a>
                            <asp:Label ID="Label11" runat="server" Text='mm/dd/yy|TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                        </td>
                        <td align="left"><b>LateReg.Deadline</b> </td>
                        <td>
                            <asp:TextBox ID="txtLateRegDeadline" runat="server" CssClass="SmallFont"
                                Height="16px" Width="83px"></asp:TextBox>
                            <a href="javascript:PopupPicker('<%=txtLateRegDeadline.ClientID%>', 200, 200);">
                                <img src="Images/Calendar.gif" border="0">
                            </a>
                            <asp:Label ID="Label12" runat="server" Text='mm/dd/yy|TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                        </td>
                        <td align="left"><b>Teacher</b> </td>
                        <td>
                            <asp:DropDownList ID="ddlTeacher" runat="server" Height="20px" Width="145px"></asp:DropDownList>

                        </td>

                    </tr>
                    <tr>
                        <td align="left"><b>Max.Capacity</b> </td>
                        <td>
                            <asp:TextBox ID="txtMaxCap" runat="server" CssClass="SmallFont"
                                Height="21px" Width="109px"></asp:TextBox><asp:CompareValidator ID="CompareValidator1" runat="server" Operator="DataTypeCheck" Type="Integer"
                                    ControlToValidate="txtMaxCap" ErrorMessage="MaxCap must be Integer." /></td>
                        <td>
                            <b>FileName</b></td>
                        <td colspan="3">
                            <asp:FileUpload ID="FileUpLoad1" runat="server" Height="20px" />
                            <asp:Label ID="LabelExampleFormat" runat="server" Font-Size="Small" Text="Ex: 2016_SB_OWorkShopBook_02212016.zip"></asp:Label>

                        </td>



                    </tr>
                    <tr>
                        <td align="left"><b>Webinar Link</b></td>
                        <td>
                            <asp:TextBox ID="txtWebinarLink" runat="server" CssClass="SmallFont"
                                Height="21px" Width="300px"></asp:TextBox></td>

                        <td><b>Recording Link</b>
                        </td>
                        <td colspan="3">
                            <asp:TextBox ID="txtRecordingLink" runat="server" CssClass="SmallFont"
                                Height="21px" Width="400px"></asp:TextBox></td>



                    </tr>
                    <tr>
                        <td align="center" colspan="7">
                            <asp:Button ID="btnAddUpdate" runat="server" Text="Add" />
                            &nbsp;
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="6">
                            <asp:Label ID="lblerr" runat="server" Text="" ForeColor="Red"></asp:Label><asp:Label ID="lblFileErr" runat="server" Text="" ForeColor="Red"></asp:Label>
                            <asp:HiddenField ID="hdnEventFeesID" runat="server" Visible="false" />

                        </td>
                    </tr>
                </table>

            </td>
        </tr>
    </table>
    <asp:Panel ID="PnlAddOWkshops" runat="server" Enabled="true ">
        <asp:HiddenField ID="CalRow" runat="server" />
        <table id="tblAddOWkshops" runat="server" visible="true" width="100%">
            <tr>
                <td>
                    <asp:Label ID="lblGridError" runat="server" Width="100%" ForeColor="Red"></asp:Label>
                </td>
            </tr>
            <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
                <td style="width: 991px" align="center" class="ContentSubTitle">
                    <asp:DataGrid ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="OnlineWSCalID"
                        Height="14px" GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Inset"
                        BorderColor="#336666" Visible="False">
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                        <ItemStyle ForeColor="Black" BackColor="#EEEEEE"></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>

                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                            <asp:EditCommandColumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Modify">
                                <ItemStyle ForeColor="Blue"></ItemStyle>
                            </asp:EditCommandColumn>

                            <asp:BoundColumn DataField="OnlineWSCalID" Visible="True" ReadOnly="true"></asp:BoundColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>
                                    <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteLevel" CommandArgument='<%#DataBinder.Eval(Container, "DataItem.OnlineWSCalID")%>' />
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:TemplateColumn>
                                <ItemTemplate>

                                    <asp:LinkButton runat="server" ID="lnkCreateWebinar" CommandName="CreateW" Text="Create Webinar"></asp:LinkButton>
                                    <div style="display: none;">
                                        <asp:Label ID="lblOnlineWsCalId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.OnlineWSCalID")%>'></asp:Label>
                                        <asp:Label ID="lbleventId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventID")%>'></asp:Label>
                                        <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                        <asp:Label ID="lblPgId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>'></asp:Label>
                                        <asp:Label ID="lblPrdId" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductID")%>'></asp:Label>
                                        <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                        <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date")%>'></asp:Label>
                                        <asp:Label ID="lblDurationWeb" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>'></asp:Label>
                                        <asp:Label ID="lblTeacherID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeacherID")%>'></asp:Label>
                                        <asp:Label ID="lblProductGroupName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupName")%>'></asp:Label>
                                        <asp:Label ID="lblProductName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductName")%>'></asp:Label>
                                        <asp:Label ID="lblteacherName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Teacher")%>'></asp:Label>
                                        <asp:Label ID="lblTeacherEmail" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TeacherEmail")%>'></asp:Label>

                                        <asp:Label ID="lblPGCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                        <asp:Label ID="lblPCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>

                                    </div>
                                </ItemTemplate>
                            </asp:TemplateColumn>

                            <asp:BoundColumn DataField="EventFeesID" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventYear" HeaderText="EventYear" ReadOnly="true" HeaderStyle-ForeColor="White" Visible="True"></asp:BoundColumn>
                            <asp:BoundColumn DataField="EventID" HeaderText="Event" ReadOnly="true" HeaderStyle-ForeColor="White" Visible="True"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductGroupID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="ProductID" HeaderText="ProductGroupID" HeaderStyle-ForeColor="White" Visible="False"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="ProductGroup">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlDGProductGroup_SelectedIndexChanged" OnPreRender="SetDropDown_ProductGroup"></asp:DropDownList>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="ProductCode">
                                <ItemTemplate>
                                    <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" runat="server" OnPreRender="SetDropDown_Product" OnSelectedIndexChanged="ddlDGProduct_SelectedIndexChanged"></asp:DropDownList>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Event Date">
                                <ItemTemplate>
                                    <asp:Label ID="lblEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date", "{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEditEventDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Date", "{0:d}")%>' OnPreRender="SetEventDate_PreRender"></asp:TextBox>
                                    <asp:Button ID="Button1" runat="server" CommandName="ShowHideCalendar1" OnClick="Button1_Click" Text="Calendar" />
                                    <asp:Calendar ID="Calendar1" runat="server" OnSelectionChanged="Calendar1_SelectionChanged" Visible="False"></asp:Calendar>
                                    <br />
                                    <asp:Label ID="lblEvDt" runat="server" Text='mm/dd/yy | TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                                    <br />
                                    <asp:Label ID="lblEt" runat="server" Text='Note**:Type Date and Press Enter' ForeColor="Green" Font-Bold="false"></asp:Label>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Time">
                                <ItemTemplate>
                                    <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlTime" runat="server" OnPreRender="SetDropDown_Time" OnSelectedIndexChanged="ddlDGTime_SelectedIndexChanged"></asp:DropDownList>
                                </EditItemTemplate>
                                <HeaderStyle Width="5%" ForeColor="White" Font-Bold="True" Wrap="True" />
                                <ItemStyle Width="5%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Duration">
                                <ItemTemplate>
                                    <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlDuration" runat="server" OnPreRender="SetDropDown_Duration" OnSelectedIndexChanged="ddlDGDuration_SelectedIndexChanged">
                                        <asp:ListItem Value="0">TBD</asp:ListItem>
                                        <asp:ListItem Value="1">1.0</asp:ListItem>
                                        <asp:ListItem Value="1.5">1.5</asp:ListItem>
                                        <asp:ListItem Value="2">2.0</asp:ListItem>
                                        <asp:ListItem Value="2.5">2.5</asp:ListItem>
                                        <asp:ListItem Value="3">3.0</asp:ListItem>
                                        <asp:ListItem Value="3.5">3.5</asp:ListItem>
                                        <asp:ListItem Value="4">4.0</asp:ListItem>
                                        <asp:ListItem Value="4.5">4.5</asp:ListItem>
                                        <asp:ListItem Value="5">5.0</asp:ListItem>
                                        <asp:ListItem Value="5.5">5.5</asp:ListItem>
                                        <asp:ListItem Value="6">6.0</asp:ListItem>
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <HeaderStyle Width="5%" ForeColor="White" Font-Bold="True" Wrap="True" />
                                <ItemStyle Width="5%" />
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="TeacherID" HeaderText="TeacherID" HeaderStyle-ForeColor="White" Visible="False"></asp:BoundColumn>
                            <asp:TemplateColumn HeaderText="Teacher">
                                <ItemTemplate>
                                    <asp:Label ID="lblTeacher" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Teacher")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:DropDownList ID="ddlTeacher" runat="server" OnPreRender="SetDropDown_Teacher" OnSelectedIndexChanged="ddlDGTeacher_SelectedIndexChanged">
                                    </asp:DropDownList>
                                </EditItemTemplate>
                                <HeaderStyle Width="5%" ForeColor="White" Font-Bold="True" Wrap="True" />
                                <ItemStyle Width="5%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="Registration Deadline">
                                <ItemTemplate>
                                    <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--  <asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label> 
                                    --%>
                                    <asp:TextBox ID="txtEditRegDeadLine" runat="server" OnPreRender="SetRegDeadline_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:TextBox>
                                    <asp:Button ID="Button2" runat="server" CommandName="ShowHideCalendar2" OnClick="Button2_Click" Text="Calendar" />
                                    <asp:Calendar ID="Calendar2" runat="server" OnSelectionChanged="Calendar2_SelectionChanged" Visible="False"></asp:Calendar>
                                    <br />
                                    <asp:Label ID="lblRegDt" runat="server" Text='mm/dd/yy | TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="LateFee Deadline">
                                <ItemTemplate>
                                    <asp:Label ID="lblLateFeeDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <%--<asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegDeadline", "{0:d}")%>'></asp:Label>--%>
                                    <asp:TextBox ID="txtEditLateFeeDeadLine" runat="server" OnPreRender="SetLateRegDLDate_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.LateRegDLDate", "{0:d}")%>'></asp:TextBox>
                                    <asp:Button ID="Button3" runat="server" CommandName="ShowHideCalendar3" OnClick="Button3_Click" Text="Calendar" />
                                    <asp:Calendar ID="Calendar3" runat="server" OnSelectionChanged="Calendar3_SelectionChanged" Visible="False"></asp:Calendar>
                                    <br />
                                    <asp:Label ID="lbllateRegDt" runat="server" Text='mm/dd/yy | TBD' ForeColor="Red" Font-Bold="false"></asp:Label>
                                    <%--   --%>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="MaxCapacity">
                                <ItemTemplate>
                                    <asp:Label ID="lblMaxCap" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCap")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtEditMaxCap" runat="server" Width="30px" OnPreRender="SetMaxCap_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCap")%>'></asp:TextBox><br />
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>
                            <asp:TemplateColumn HeaderText="FileName">
                                <ItemTemplate>
                                    <asp:ImageButton ImageUrl="~/Images/download.png" ID="imgBtnDownload" runat="server" CommandName="Download" />
                                    <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FileName")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:FileUpload ID="fupload" runat="server" />
                                    <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FileName")%>'></asp:Label>
                                </EditItemTemplate>
                                <HeaderStyle Width="10%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="10%" />
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Webinar Link">
                                <ItemTemplate>
                                    <asp:Label ID="lblWebinarLink" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.WebinarLink")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtWebinarLink" runat="server" Width="400px" OnPreRender="txtWebinarLink_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.WebinarLink")%>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle Width="30%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="30%" />
                            </asp:TemplateColumn>

                            <asp:TemplateColumn HeaderText="Rcording Link">
                                <ItemTemplate>
                                    <asp:Label ID="lblRecordingLink" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RecordingLink")%>'></asp:Label>
                                </ItemTemplate>
                                <EditItemTemplate>
                                    <asp:TextBox ID="txtRecordingLink" runat="server" Width="400px" OnPreRender="txtRecordingLink_PreRender" Text='<%#DataBinder.Eval(Container, "DataItem.RecordingLink")%>'></asp:TextBox>
                                </EditItemTemplate>
                                <HeaderStyle Width="30%" ForeColor="White" Font-Bold="True" />
                                <ItemStyle Width="30%" />
                            </asp:TemplateColumn>

                        </Columns>
                    </asp:DataGrid>

                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:HiddenField ID="hdnPgId" runat="server" Value="" />
    <asp:HiddenField ID="hdnPId" runat="server" Value="" />
    <asp:HiddenField ID="hdnisPrdEnable" runat="server" Value="" />
    <asp:HiddenField ID="hdnStartDate" runat="server" Value="" />
    <asp:HiddenField ID="hdnStartTime" runat="server" Value="" />
    <asp:HiddenField ID="hdnTeacherId" runat="server" Value="" />
    <asp:HiddenField ID="hdnEventId" runat="server" Value="" />
    <asp:HiddenField ID="hdnEventYear" runat="server" Value="" />
    <asp:HiddenField ID="hdnProductGroupName" runat="server" Value="" />
    <asp:HiddenField ID="hdnProductName" runat="server" Value="" />
    <asp:HiddenField ID="hdnTeacherEmail" runat="server" Value="" />
    <asp:HiddenField ID="hdnTeacherName" runat="server" Value="" />
    <asp:HiddenField ID="hdnWebinarTitle" runat="server" Value="" />
    <asp:HiddenField ID="hdnDuration" runat="server" Value="" />
    <asp:HiddenField ID="hdnUTCStartTime" runat="server" Value="" />
    <asp:HiddenField ID="hdnUTCEndTime" runat="server" Value="" />
    <input type="hidden" id="hdnAccessToken" value="" runat="server" />
    <input type="hidden" id="hdnOrganizerKey" value="" runat="server" />
    <input type="hidden" id="hdnLoginId" value="" runat="server" />

</asp:Content>

