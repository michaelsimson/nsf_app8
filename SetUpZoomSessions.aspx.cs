﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;
using System.IO;
using System.Data;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using System.Drawing;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Collections;
using VRegistration;

public partial class Zoom_API_SetUpZoomSessions : System.Web.UI.Page
{

    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    //public string apiKey = "qcVCf9Q_SO6nLp6uhDUG_g";
    //public string apiSecret = "UvOiedvRQjEjtC5kdWUK6dZW3z31uNf60f1n";


    protected void Page_Load(object sender, EventArgs e)
    {
       // DeleteBulkZoomSessions();
        //var dt = DateTime.Parse("25/5/2016 12:00:00");
        //string s = TimeZoneInfo.ConvertTimeToUtc(dt, TimeZoneInfo.FindSystemTimeZoneById("central standard time")).ToString();
        spnError.InnerText = "";
        spnStatus.InnerText = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                
                if (Session["RoleID"] != null) { hdnRoleID.Value = Session["RoleID"].ToString(); }

                loadyear();
                loadPhase(ddlPhase);
                LoadEvent(ddlEvent);
                fillMeetingGrid("All");

                fillPgGroupSection();
                fillPrdSection();
                loadLevelFilter();
                fillCoachSection();

            }
        }
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddlYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        ddlYearFilter.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddlYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));

            ddlYearFilter.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));

        }
        ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

        ddlYearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }

    //public void createWebinarMeeting()
    //{
    //    try
    //    {


    //        string URL = string.Empty;
    //        string durationHrs = "0";
    //        string service = "1";
    //        URL = "https://api.zoom.us/v1/webinar/create";
    //        string urlParameter = string.Empty;


    //        string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

    //        string date = txtStartDate.Text;
    //        string time = txtStartTime.Text;
    //        string dateTime = "2016-05-25T14:00:00Z";

    //        urlParameter += "api_key=" + apiKey + "";
    //        urlParameter += "&api_secret=" + apiSecret + "";
    //        urlParameter += "&data_type=JSON";
    //        //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
    //        urlParameter += "&host_id=6WaTiABIRkCz7ZWf5rXSeA";
    //        urlParameter += "&topic=Test Webinar Meeting";

    //        urlParameter += "&type=6";

    //        urlParameter += "&timezone=GMT-5:00";
    //        urlParameter += "&approval_type=1";

    //        makeZoomAPICall(urlParameter, URL, service);
    //    }
    //    catch (Exception ex)
    //    {
    //        CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
    //    }
    //}

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }

    }
    public int validateMeeting()
    {
        int retVal = 1;
        if (ddlEvent.SelectedValue == "")
        {
            retVal = -1;
            spnError.InnerText = "Please select Event";
        }
        if (ddlChapter.SelectedValue == "")
        {
            retVal = -1;
            spnError.InnerText = "Please select Chapter";
        }
        if (ddlPhase.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Semester";
        }
        if (ddlCoach.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Coach";
        }
        if (ddlProduct.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Product";
        }
        if (ddlLevel.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Level";
        }
        if (ddlSessionNo.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Session No";
        }

        if (txtMeetingPwd.Text != "")
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showPwdAlert()", true);

            return 0;
        }

        return retVal;
    }

    public int ValidateMeetingList()
    {
        int retVal = 1;
        if (ddlEvent.SelectedValue == "")
        {
            retVal = -1;
            spnError.InnerText = "Please select Event";
        }
        if (ddlChapter.SelectedValue == "")
        {
            retVal = -1;
            spnError.InnerText = "Please select Chapter";
        }
        if (ddlPhase.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Semester";
        }
        if (ddlCoach.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Coach";
        }
        if (ddlProduct.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Product";
        }
        if (ddlLevel.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Level";
        }
        if (ddlSessionNo.SelectedValue == "0")
        {
            retVal = -1;
            spnError.InnerText = "Please select Session No";
        }



        return retVal;
    }


    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnHostURL.Value = json["start_url"].ToString();
                hdnJoinMeetingUrl.Value = json["join_url"].ToString();
            }
            hdnMeetingStatus.Value = "SUCCESS";

            if (serviceType == "3")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnMeetingKey.Value + "");

                string strQuery;
                SqlCommand cmd;
                strQuery = "update CalSignup set MeetingKey = null, meetingPwd=null, Status='Cancelled', HostjoinURL=null  where MeetingKey=@MeetingKey";
                cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@MeetingKey", hdnSessionKey.Value);

                NSFDBHelper objNSF = new NSFDBHelper();
                objNSF.InsertUpdateData(cmd);

                // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "update CalSignup set MeetingKey = null, meetingPwd=null, Status='Cancelled', HostjoinURL=null where MeetingKey=" + hdnMeetingKey.Value + "");

                // fillMeetingGrid();
                spnStatus.InnerText = "Zoom Session deleted successfully";
            }
            if (serviceType == "2")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "Update WebConfLog set MeetingPWd='" + txtMeetingPwd.Text + "' where SessionKey=" + hdnMeetingKey.Value + "");
                //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);
            }
            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }



    public void createZoomMeeting(string hostID)
    {
        try
        {
            if (hdnIsUpdate.Value == "")
            {



                string URL = string.Empty;
                string durationHrs = "0";
                string service = "1";
                URL = "https://api.zoom.us/v1/meeting/create";
                string urlParameter = string.Empty;
                if (validateMeeting() == 1)
                {
                    //txtTopic.Text = ddlCoach.SelectedItem.Text + "-" + ddlProductGroup.SelectedItem.Text;

                    if (ddlMeetingType.SelectedValue == "2")
                    {

                        string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

                        // string date = txtStartDate.Text;
                        //   string time = txtStartTime.Text;
                        //   string dateTime = "2016-05-25T14:00:00Z";

                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += "&api_secret=" + apiSecret + "";
                        urlParameter += "&data_type=JSON";
                        //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                        urlParameter += "&host_id=6WaTiABIRkCz7ZWf5rXSeA";
                        urlParameter += "&topic=" + txtTopic.Text + "";
                        if (txtMeetingPwd.Text != "")
                        {
                            urlParameter += "&password=" + txtMeetingPwd.Text + "";
                        }
                        urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";
                        urlParameter += "&start_time=" + test + "";
                        urlParameter += "&duration=" + durationHrs + "";
                        urlParameter += "&timezone=GMT-5:00";
                        urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                    }
                    else if (ddlMeetingType.SelectedValue == "1")
                    {
                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += "&api_secret=" + apiSecret + "";
                        urlParameter += "&data_type=JSON";
                        urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                        urlParameter += "&topic=" + txtTopic.Text + "";
                        if (txtMeetingPwd.Text != "")
                        {
                            urlParameter += "&password=" + txtMeetingPwd.Text + "";
                        }
                        urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";


                        urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                    }
                    else if (ddlMeetingType.SelectedValue == "3")
                    {
                        urlParameter += "api_key=" + apiKey + "";
                        urlParameter += "&api_secret=" + apiSecret + "";
                        urlParameter += "&data_type=JSON";
                        //  urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";

                        urlParameter += "&host_id=" + hostID + "";

                        urlParameter += "&topic=" + txtTopic.Text + "";
                        if (txtMeetingPwd.Text != "")
                        {
                            urlParameter += "&password=" + txtMeetingPwd.Text + "";
                        }
                        urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";


                        urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                        urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                        urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";

                    }
                    makeZoomAPICall(urlParameter, URL, service);
                }
                else
                {

                    validateMeeting();
                }
            }
            else
            {
                updateZoomSession();
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validateMeeting() == 1)
        {
            if (btnCreateMeeting.Text == "Create Session")
            {
                ScheduleTrainingCenterAll();
            }
            else
            {
                updateZoomSession();
            }
        }
        else
        {
            validateMeeting();
        }
    }
    protected void btnCreateZoomSession_Click(object sender, EventArgs e)
    {
        if (validateMeeting() == 1)
        {
            ScheduleTrainingCenterAll();
        }
        else
        {
            validateMeeting();
        }
    }
    protected void btnClear_Click(object sender, EventArgs e)
    {
        if (btnClear.Text == "Clear")
        {
            //txtDuration.Text = "";
            //txtMeetingPwd.Text = "";
            //txtTopic.Text = "";
            //txtStartTime.Text = "";
            //ddlMeetingType.SelectedValue = "2";
            //ddlJoinBH.SelectedValue = "true";
            //ddlStartType.SelectedValue = "Video";
            //ddlHostVideo.SelectedValue = "true";
            //ddlAudio.SelectedValue = "voip";

            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
        else
        {
            Page.Response.Redirect(Page.Request.Url.ToString(), true);
        }
    }

    public void listMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "2";
            URL = "https://api.zoom.us/v1/meeting/list";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            urlParameter += "&page_size=30";
            urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void deleteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnhostID.Value + "";
            urlParameter += "&id=" + hdnMeetingKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void updateZoomSession()
    {
        try
        {
            string URL = string.Empty;
            string service = "1";
            URL = "https://api.zoom.us/v1/meeting/update";
            string urlParameter = string.Empty;
            if (validateMeeting() == 1)
            {
                if (ddlMeetingType.SelectedValue == "2")
                {
                    //string date = txtStartTime.Text.Substring(0, 10);
                    //string time = txtStartTime.Text.Substring(10, 5);
                    //string dateTime = date.Trim() + "T" + time.Trim() + ":00Z";
                    //dateTime = "2016-5-20T12:00:00Z";
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += "&api_secret=" + apiSecret + "";
                    urlParameter += "&data_type=JSON";
                    urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    if (txtMeetingPwd.Text != "")
                    {
                        urlParameter += "&password=" + txtMeetingPwd.Text + "";

                    }
                    urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";

                    urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                }
                else if (ddlMeetingType.SelectedValue == "1")
                {
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += "&api_secret=" + apiSecret + "";
                    urlParameter += "&data_type=JSON";
                    urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    if (txtMeetingPwd.Text != "")
                    {
                        urlParameter += "&password=" + txtMeetingPwd.Text + "";
                    }
                    urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";


                    urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                }
                else if (ddlMeetingType.SelectedValue == "3")
                {
                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += "&api_secret=" + apiSecret + "";
                    urlParameter += "&data_type=JSON";
                    urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
                    urlParameter += "&id=" + hdnMeetingKey.Value + "";
                    if (txtMeetingPwd.Text != "")
                    {
                        urlParameter += "&password=" + txtMeetingPwd.Text + "";
                    }
                    urlParameter += "&type=" + ddlMeetingType.SelectedValue + "";


                    urlParameter += "&option_jbh=" + ddlJoinBH.SelectedValue + "";
                    urlParameter += "&option_host_video=" + ddlHostVideo.SelectedValue + "";
                    urlParameter += "&option_audio=" + ddlAudio.SelectedValue + "";
                }
                makeZoomAPICall(urlParameter, URL, service);
            }
            else
            {
                validateMeeting();
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        deleteMeeting();
    }
    protected void btnUpdateMeeting_Click(object sender, EventArgs e)
    {
        updateZoomSession();
    }

    public void LoadEvent(DropDownList ddlObject)
    {
        try
        {

            string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
            "here EF.EventYear ="
                        + (ddlYear.SelectedValue + "  and E.EventId in(13,19)"));
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.DataSource = ds;
                ddlObject.DataTextField = "EventCode";
                ddlObject.DataValueField = "EventId";
                ddlObject.DataBind();
                if ((ds.Tables[0].Rows.Count > 0))
                {
                    ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                    ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                    ddlObject.Enabled = false;

                    ddlChapter.SelectedValue = "112";
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                    {
                        fillCoach();
                    }
                    else
                    {
                        fillCoach();
                        fillProductGroup();
                    }
                }
                else
                {
                    ddlObject.Enabled = false;
                    fillProductGroup();
                }

            }
            else
            {

                spnError.InnerText = "No Events present for the selected year";
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void fillProductGroup()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                if (ddlCoach.SelectedItem.Text == "All")
                {
                    Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "'  and Accepted='Y' and Semester='" + ddlPhase.SelectedItem.Value + "')";
                }
                else
                {
                    Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedItem.Value + "')";
                }
            }
            else
            {
                Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedItem.Value + "')";
            }


            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroup.DataSource = ds;
                ddlProductGroup.DataTextField = "ProductGroupCode";
                ddlProductGroup.DataValueField = "ProductGroupId";
                ddlProductGroup.DataBind();
                ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProductGroup.SelectedIndex = 1;
                    ddlProductGroup.Enabled = false;
                    fillProduct();
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        ddlProductGroup.Items.Insert(1, new ListItem("All", "All"));
                    }
                    ddlProductGroup.Enabled = true;
                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    public void fillProduct()
    {
        try
        {
            if (ddlProductGroup.SelectedValue != "All" && ddlCoach.SelectedValue != "All")
            {
                //  txtTopic.Text = ddlCoach.SelectedItem.Text + "-" + ddlProductGroup.SelectedItem.Text;
            }
            else
            {
                txtTopic.Enabled = false;

            }
            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                if (ddlProductGroup.SelectedItem.Text == "All")
                {
                    Cmdtext = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where EventYear=" + ddlYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and Accepted='Y' and Semester='" + ddlPhase.SelectedItem.Value + "')";
                }
                else
                {
                    Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and Semester='" + ddlPhase.SelectedItem.Value + "' ";
                    if (ddlCoach.SelectedValue != "All")
                    {
                        Cmdtext += " and MemberID=" + ddlCoach.SelectedValue + "";
                    }
                    Cmdtext += " and Accepted='Y')";
                }
            }
            else
            {
                Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlPhase.SelectedItem.Value + "')";
            }



            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                //ddlProduct.Enabled = true;
                ddlProduct.DataSource = ds;
                ddlProduct.DataTextField = "ProductCode";
                ddlProduct.DataValueField = "ProductID";
                ddlProduct.DataBind();
                ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlProduct.SelectedIndex = 1;
                    ddlProduct.Enabled = false;
                    loadLevel();
                    LoadSessionNo();
                    if (Session["RoleID"].ToString() == "88")
                    {
                    }
                    //  txtTopic.Text = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text;
                }
                else
                {
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        ddlProduct.Items.Insert(1, new ListItem("All", "All"));
                        if (ddlProductGroup.SelectedValue == "All")
                        {
                            ddlProduct.SelectedValue = "All";
                            ddlProduct.Enabled = false;
                        }
                        else
                        {
                            ddlProduct.Enabled = true;
                        }
                    }

                }

            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    public void fillCoach()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedItem.Value + "' order by ID.FirstName ASC";


            }
            else
            {
                cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " and Semester='" + ddlPhase.SelectedItem.Value + "' order by I. FirstName";
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoach.DataSource = ds;
                ddlCoach.DataTextField = "Name";
                ddlCoach.DataValueField = "AutoMemberID";
                ddlCoach.DataBind();
                ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {
                    ddlCoach.Enabled = true;
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        ddlCoach.Items.Insert(1, new ListItem("All", "All"));
                    }
                }
                else
                {
                    ddlCoach.SelectedValue = Session["LoginID"].ToString();
                    if (ds.Tables[0].Rows.Count > 1)
                    {
                        ddlCoach.Items.Insert(1, new ListItem("All", "All"));
                    }
                    ddlCoach.Enabled = false;
                }

            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        loadLevel();
        LoadSessionNo();
    }


    public void loadLevel()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedItem.Value + "'";

                if (ddlCoach.SelectedItem.Text != "All")
                {
                    cmdText += " and V.MemberID='" + ddlCoach.SelectedValue + "'";
                }

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "All")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedItem.Text != "All")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            else
            {
                cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + ddlPhase.SelectedItem.Value + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                ddlLevel.DataTextField = "Level";
                ddlLevel.DataValueField = "Level";
                ddlLevel.DataSource = ds;
                ddlLevel.DataBind();
                ddlLevel.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlLevel.SelectedIndex = 1;
                    ddlLevel.Enabled = false;
                }

                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlLevel.Enabled = true;
                    if (ddlProduct.SelectedItem.Text == "All")
                    {
                        ddlLevel.Items.Insert(1, new ListItem("All", "All"));
                        ddlLevel.SelectedValue = "All";
                        ddlLevel.Enabled = false;
                    }
                    else
                    {
                        ddlLevel.Enabled = true;
                    }

                }
                else
                {
                    ddlLevel.Enabled = false;
                }
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void LoadSessionNo()
    {
        try
        {

            string cmdText = "";

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedItem.Value + "'";

                if (ddlCoach.SelectedItem.Text != "All")
                {
                    cmdText += " and V.MemberID='" + ddlCoach.SelectedValue + "'";
                }

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "" && ddlProductGroup.SelectedValue != "All")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "" && ddlProduct.SelectedValue != "All")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            else
            {
                cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=" + ddlEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "' and V.Semester='" + ddlPhase.SelectedItem.Value + "'";

                if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
                {
                    cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
                }

                if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
                {
                    cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
                }
            }
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {

                ddlSessionNo.DataTextField = "SessionNo";
                ddlSessionNo.DataValueField = "SessionNo";
                ddlSessionNo.DataSource = ds;
                ddlSessionNo.DataBind();
                ddlSessionNo.Items.Insert(0, new ListItem("Select", "0"));
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddlSessionNo.SelectedIndex = 1;
                    ddlSessionNo.Enabled = false;
                    if (ddlProductGroup.SelectedValue != "All" && ddlCoach.SelectedValue != "All")
                    {
                        txtTopic.Text = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "-" + ddlLevel.SelectedItem.Text + "-" + ddlSessionNo.SelectedValue;
                    }
                }
                if (ds.Tables[0].Rows.Count > 1)
                {
                    ddlSessionNo.Enabled = true;
                    if (ddlLevel.SelectedValue == "All")
                    {
                        ddlSessionNo.Items.Insert(1, new ListItem("All", "All"));
                        ddlSessionNo.SelectedValue = "All";
                        ddlSessionNo.Enabled = false;
                    }
                    else
                    {
                        ddlSessionNo.Enabled = true;
                    }
                }
                else
                {
                    ddlSessionNo.Enabled = false;
                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }

    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddlEvent);
        fillMeetingGrid("");
    }
    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillProductGroup();
    }


    public void ScheduleTrainingCenterAll()
    {
        try
        {
            string CmdText = string.Empty;
            DataSet ds = new DataSet();
            int count = 0;




            if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All" && ddlProduct.SelectedValue != "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,VL.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term' and cd.Semester=cs.Semester) inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookup VL on (CS.VRoom=VL.VRoom) where Accepted='Y' and cs.Semester='" + ddlPhase.SelectedItem.Value + "' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and (CS.Vroom is not null )";
            }
            else if (ddlCoach.SelectedValue == "All" && ddlProductGroup.SelectedValue == "All" && ddlProduct.SelectedValue == "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,VL.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term' and Cd.Semester=cs.Semester) inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookup VL on (CS.VRoom=VL.VRoom) where Accepted='Y' and cs.Semester='" + ddlPhase.SelectedItem.Value + "' and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and (CS.Vroom is not null)";
            }
            else if (ddlCoach.SelectedValue == "All" && ddlProduct.SelectedValue == "All" && ddlProductGroup.SelectedValue != "All")
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,VL.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term' and Cd.Semester=cs.Semester) inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookup VL on (CS.VRoom=VL.VRoom)  where Accepted='Y' and cs.Semester='" + ddlPhase.SelectedValue + "' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and (CS.Vroom is not null )";
            }
            else
            {
                CmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,VL.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term' and Cd.Semester=cs.Semester) inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID)  inner join VirtualRoomLookup VL on (CS.VRoom=VL.Vroom) where Accepted='Y' and cs.Semester='" + ddlPhase.SelectedValue + "' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddlYear.SelectedValue + " and Approved='Y') and (CS.Vroom is not null)";
                //and CS.UserID is not null
            }



            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);

            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        count++;

                        string WebExID = dr["UserID"].ToString();
                        string Pwd = dr["Pwd"].ToString();
                        int Capacity = Convert.ToInt32(dr["MaxCapacity"].ToString());
                        string ScheduleType = dr["ScheduleType"].ToString();

                        string year = dr["EventYear"].ToString();
                        string eventID = dr["EventID"].ToString();
                        string chapterId = ddlChapter.SelectedValue;
                        string ProductGroupID = dr["ProductGroupID"].ToString();
                        string ProductGroupCode = dr["ProductGroupCode"].ToString();
                        string ProductID = dr["ProductID"].ToString();
                        string ProductCode = dr["ProductCode"].ToString();
                        string Semester = dr["Semester"].ToString();
                        string Level = dr["Level"].ToString();
                        string Sessionno = dr["SessionNo"].ToString();
                        string CoachID = dr["MemberID"].ToString();
                        string CoachName = dr["CoachName"].ToString();

                        string MeetingPwd = "training";


                        string Time = dr["Time"].ToString();
                        string Day = dr["Day"].ToString();
                        string BeginTime = dr["Begin"].ToString();
                        string EndTime = dr["End"].ToString();
                        string startDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        string EndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        if (BeginTime == "")
                        {
                            BeginTime = "20:00:00";
                        }
                        if (EndTime == "")
                        {
                            EndTime = "21:00:00";
                        }

                        if (txtDuration.Text != "")
                        {
                            int Duration = Convert.ToInt32(txtDuration.Text);
                        }
                        string timeZoneID = ddlTimeZone.SelectedValue;

                        string TimeZone = "";
                        string SignUpId = dr["SignupID"].ToString();
                        double Mins = 0.0;
                        DateTime dFrom;
                        DateTime dTo;
                        string sDateFrom = BeginTime;
                        string sDateTo = EndTime;
                        if (DateTime.TryParse(sDateFrom, out dFrom) && DateTime.TryParse(sDateTo, out dTo))
                        {
                            TimeSpan TS = dTo - dFrom;
                            Mins = TS.TotalMinutes;

                        }
                        string durationHrs = Mins.ToString("0");
                        if (durationHrs.IndexOf("-") > -1)
                        {
                            durationHrs = "188";
                        }

                        if (timeZoneID == "11")
                        {
                            TimeZone = "EST/EDT – Eastern";
                        }
                        else if (timeZoneID == "7")
                        {
                            TimeZone = "CST/CDT - Central";
                        }
                        else if (timeZoneID == "6")
                        {
                            TimeZone = "MST/MDT - Mountain";
                        }
                        else
                        {
                            TimeZone = "PST/PDT (Pacific or West Coast Time)";
                        }
                        string userID = Session["LoginID"].ToString();
                        txtTopic.Text = CoachName + "-" + ProductCode + "-" + Level + "-" + Sessionno;
                        string hostID = dr["HostID"].ToString();
                        createZoomMeeting(hostID);

                        if (hdnMeetingStatus.Value == "SUCCESS")
                        {
                            string meetingURL = (hdnHostURL.Value == "0" ? "" : hdnHostURL.Value);

                            string cmdText = "";

                            string meetingType = ddlMeetingType.SelectedItem.Text;

                            if (txtMeetingPwd.Text == "")
                            {
                                MeetingPwd = "null";
                            }
                            else
                            {
                                MeetingPwd = "'" + txtMeetingPwd.Text + "'";
                            }

                            cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," + year + "," + eventID + "," + chapterId + "," + ProductGroupID + ",'" + ProductGroupCode + "'," + ProductID + ",'" + ProductCode + "','" + startDate + "','" + EndDate + "','" + BeginTime + "','" + EndTime + "'," + durationHrs + "," + timeZoneID + ",'" + TimeZone + "','" + SignUpId + "'," + CoachID + ",'" + Semester + "','" + Level + "','" + Sessionno + "','" + meetingURL + "','" + hdnSessionKey.Value + "'," + MeetingPwd + ",'Active','" + Day + "','" + userID + "','" + WebExID + "','" + Pwd + "','" + meetingType + "'";

                            DataSet objDs = new DataSet();
                            objDs = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);

                            if (null != objDs && objDs.Tables.Count > 0)
                            {
                                if (objDs.Tables[0].Rows.Count > 0)
                                {
                                    if (Convert.ToInt32(objDs.Tables[0].Rows[0]["Retval"].ToString()) > 0)
                                    {

                                        spnStatus.InnerText = "Zoom session Created successfully";
                                        string ChidName = string.Empty;
                                        string Email = string.Empty;
                                        string City = string.Empty;
                                        string Country = string.Empty;
                                        string ChildNumber = string.Empty;
                                        string CoachRegID = string.Empty;

                                        DataSet dsChild = new DataSet();

                                        string ChildText = "select case when CR.ChildnUmber is null then CR.AdultId else C1.ChildNumber end as ChildNUmber,CR.CoachRegID,CR.PMemberID as memberId, case when CR.Childnumber is null then IP1.Gender else C1.Gender end as Gender, case when CR.Childnumber is null then IP1.FirstName+ ' ' +IP.LastName else C1.FIRST_NAME +' '+ C1.LAST_NAME end as Name, case when CR.Childnumber is null then IP1.Email else case when C1.Email IS NULL then IP.Email else C1.Email end end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail  from CoachReg CR  inner join IndSpouse IP on(IP.AutoMemberID=CR.PMemberID) left join Indspouse IP1 on (IP1.AutoMemberId=CR.AdultId) left join child C1 on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + CoachID + " and CR.EventYear=" + ddlYear.SelectedValue + " and CR.EventID=" + ddlEvent.SelectedValue + ") where CR.EventYear=" + ddlYear.SelectedValue + " and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + CoachID + " and CR.Approved='Y' and CR.Level='" + Level + "' and CR.Semester='" + ddlPhase.SelectedValue + "'";



                                        dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                                        if (null != dsChild && dsChild.Tables.Count > 0)
                                        {
                                            if (dsChild.Tables[0].Rows.Count > 0)
                                            {
                                                foreach (DataRow drChild in dsChild.Tables[0].Rows)
                                                {
                                                    ChidName = drChild["Name"].ToString();
                                                    Email = drChild["WebExEmail"].ToString();
                                                    if (Email.IndexOf(';') > 0)
                                                    {
                                                        Email = Email.Substring(0, Email.IndexOf(';'));
                                                    }
                                                    City = drChild["City"].ToString();
                                                    Country = drChild["Country"].ToString();
                                                    ChildNumber = drChild["ChildNumber"].ToString();
                                                    CoachRegID = drChild["CoachRegID"].ToString();

                                                    if (hdnMeetingStatus.Value == "SUCCESS")
                                                    {
                                                        string attendeeJoinURl = "https://northsouth.zoom.us/j/" + hdnSessionKey.Value;

                                                        string CmdChildUpdateText = "update CoachReg set AttendeeJoinURL='" + attendeeJoinURl + "',Status='Active',ModifyDate=GetDate(), ModifiedBy='" + userID + "' where CoachRegID='" + CoachRegID + "'";

                                                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);
                                                    }
                                                }
                                            }
                                        }

                                    }
                                    else
                                    {

                                        spnStatus.InnerText = objDs.Tables[0].Rows[0]["Retval"].ToString();
                                    }
                                }
                            }


                            spnStatus.InnerText = "Zoom session Created successfully";
                        }
                        else
                        {
                            //spnError.InnerText = hdnException.Value;
                        }

                    }
                    fillMeetingGrid("false");
                }
                else
                {
                    spnError.InnerText = "No child is assigned for the selected Coach..";
                }


            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void clearoldsessions()
    {
        string cmdText = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time,VC.SessionType,CS.UserID,CS.Pwd,CS.VRoom, Vl.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) inner join VirtualRoomLookup Vl on (Vl.Vroom=CS.Vroom) where VC.EventYear=2016 order by IP.FirstName, IP.LastName ASC";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    hdnhostID.Value = dr["HostID"].ToString();
                    hdnMeetingKey.Value = dr["SessionKey"].ToString();
                    deleteMeeting();
                }
            }
        }
    }

    public void fillMeetingGrid(string IsFilter)
    {


        string cmdtext = "";
        DataSet ds = new DataSet();

        string year = string.Empty;
        string cmdText = "select max(EventYear) from EventFees where EventId=13";
        year = SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText).ToString();
        year = ddlYear.SelectedValue;

        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,CS.SessionNo,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,case when VC.Day is null then CS.Day else VC.Day end as Day,cs.time,VC.SessionType,CS.UserID,CS.Pwd,CS.VRoom, Vl.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID and CS.MeetingKey=VC.SessionKey) inner join VirtualRoomLookup Vl on (Vl.Vroom=CS.Vroom) where VC.EventYear=" + year + "";

        if (IsFilter == "true")
        {

            if (ddlYearFilter.SelectedValue != "0")
            {

                cmdtext += " and CS.EventYear=" + ddlYearFilter.SelectedValue + "";
            }
            if (ddlProductGroupFilter.SelectedValue != "-1")
            {
                cmdtext += " and CS.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if (ddlProductFilter.SelectedValue != "-1")
            {
                cmdtext += " and CS.ProductID=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlPhaseFilter.SelectedValue != "0")
            {
                cmdtext += " and CS.Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }
            if (ddlCoachFilter.SelectedValue != "-1")
            {

                cmdtext += " and CS.MemberID=" + ddlCoachFilter.SelectedValue + "";
            }
            if (DDlLevelFilter.SelectedValue != "-1")
            {

                cmdtext += " and CS.Level=" + DDlLevelFilter.SelectedValue + "";
            }
        }
        else if (IsFilter == "false")
        {
            cmdtext += " and VC.Semester='" + ddlPhase.SelectedValue + "'";
            if (ddlProductGroup.SelectedValue != "All")
            {
                cmdtext += " and VC.ProductGroupID='" + ddlProductGroup.SelectedValue + "'";
            }
            if (ddlProduct.SelectedValue != "All")
            {
                cmdtext += " and VC.ProductId='" + ddlProduct.SelectedValue + "'";
            }
            if (ddlCoach.SelectedValue != "All")
            {

                cmdtext += " and VC.MemberID='" + ddlCoach.SelectedValue + "'";
            }
            if (ddlLevel.SelectedValue != "All")
            {
                cmdtext += " and VC.Level='" + ddlLevel.SelectedValue + "'";
            }
            if (ddlSessionNo.SelectedValue != "All")
            {
                cmdtext += " and VC.Session='" + ddlSessionNo.SelectedValue + "'";
            }
        }
        cmdtext += " order by IP.FirstName, IP.LastName ASC";

        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                    // fillCoachSection();
                    spntable.InnerText = "";
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    spntable.InnerText = "No record exist.";

                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode, string Event, string level, string sessionNO, int year, string Semester)
    {


        DataSet ds = new DataSet();
        string CmdText = string.Empty;

        try
        {
            CmdText = "select case when CR.ChildnUmber is null then CR.AdultId else C1.ChildNumber end as ChildNUmber,CR.CoachRegID,CR.PMemberID as memberId, case when CR.Childnumber is null then IP1.Gender else C1.Gender end as Gender, case when CR.Childnumber is null then IP1.FirstName+ ' ' +IP.LastName else C1.FIRST_NAME +' '+ C1.LAST_NAME end as Name, case when CR.Childnumber is null then IP1.Email else case when C1.Email IS NULL then IP.Email else C1.Email end end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL, CR.AttendeeID,CR.RegisteredID,CR.CMemberID, CR.PMemberId, CR.ProductGroupCode,CR.ProductCode,CR.Level, CR.SessionNo, CR.Semester,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,  case when CR.Childnumber is null then 'Adult' else 'Child' end as Type  from CoachReg CR  inner join IndSpouse IP on(IP.AutoMemberID=CR.PMemberID) left join Indspouse IP1 on (IP1.AutoMemberId=CR.AdultId) left join child C1 on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + year + " and CR.EventID=" + Event + ") where CR.EventYear=" + year + " and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.Approved='Y' and CR.Level='" + level + "' and CR.Semester='" + Semester + "'";

           // CmdText = "select C1.ChildNumber,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level, CR.SessionNo, CR.Semester from CoachReg CR  inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) left join child C1 on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and CR.EventYear=" + year + " and CR.EventID=" + Event + ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" + year + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and CMemberID=" + MemberID + " and Approved='Y') and CR.Level='" + level + "' and CR.SessionNo=" + sessionNO + " and CR.Semester='" + Semester + "' order by C1.Last_Name, C1.First_Name Asc";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    dvStudentList.Visible = true;
                    btnCloseTable2.Visible = true;
                }
                else
                {
                    GrdStudentsList.DataSource = ds;
                    GrdStudentsList.DataBind();
                    dvStudentList.Visible = true;
                    btnCloseTable2.Visible = false;



                }
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = null;
        if (e.CommandName == "Join")
        {

            row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

            int selIndex = row.RowIndex;
            GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
            string meetingLink = ((LinkButton)GrdStudentsList.Rows[selIndex].FindControl("HlAttendeeMeetURL") as LinkButton).Text;
            hdnZoomURL.Value = meetingLink;
            string beginTime = hdnStartTime.Value;
            string day = hdnDay.Value;
            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            if (DateTime.TryParse(beginTime, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;

            }
            string today = DateTime.Now.DayOfWeek.ToString();
            if (mins <= 30 && day == today)
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert();", true);
            }


        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                string Semester = GrdMeeting.Rows[selIndex].Cells[8].Text;
                int year = Convert.ToInt32(GrdMeeting.Rows[selIndex].Cells[3].Text);

                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;

                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;

                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;

                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;


                string SessionNo = GrdMeeting.Rows[selIndex].Cells[10].Text.Trim();
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                hdnStartTime.Value = beginTime;

                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                hdnDay.Value = day;
                fillStudentList(MemberID, ProductGroupCode, ProductCode, "13", Level, SessionNo, year, Semester);
            }
            else if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string Time = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblOrgTime") as Label).Text;
                ddlYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                LoadEvent(ddlEvent);
                ddlEvent.SelectedValue = EventID;
                ddlChapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();
                ddlProduct.SelectedValue = ProductID;

                ddlCoach.SelectedValue = MemberID;


                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                ddlSessionNo.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;
                txtMeetingPwd.Text = GrdMeeting.Rows[selIndex].Cells[13].Text;
                //txtMeetingPwd.Attributes.Add("Value", "training");
                ddlChapter.Enabled = false;
                ddlTimeZone.Enabled = false;
                txtTopic.Enabled = false;
                ddlMeetingType.Enabled = false;
                txtDuration.Text = GrdMeeting.Rows[selIndex].Cells[19].Text;
                string meetingType = GrdMeeting.Rows[selIndex].Cells[22].Text;
                //  ddlMeetingType.SelectedItem.Text = meetingType;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                ddlCoach.Enabled = false;
                btnCreateMeeting.Text = "Update Meeting";
                btnClear.Text = "Cancel";
                hdnSessionKey.Value = sessionKey;

                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;

            }
            else if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string hostID = string.Empty;
                hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnMeetingKey.Value = sessionKey;
                hdnhostID.Value = hostID;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteMeeting();", true);
            }

            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;

                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    getmeetingIfo(sessionKey, hostID);
                    string cmdText = "";
                    cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    string meetingLink = hdnHostURL.Value;



                    hdnZoomURL.Value = meetingLink;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                }
                else
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                }
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    public class SessionInfo
    {
        public string SessionKey { get; set; }
        public string HostURL { get; set; }
        public string JoinURL { get; set; }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid("false");
    }

    public void fillPgGroupSection()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();

            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" + ddlYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and Accepted='Y')";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductGroupFilter.DataSource = ds;
                ddlProductGroupFilter.DataTextField = "ProductGroupCode";
                ddlProductGroupFilter.DataValueField = "ProductGroupId";
                ddlProductGroupFilter.DataBind();

                ddlProductGroupFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlProductGroupFilter.Enabled = true;

            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    public void fillPrdSection()
    {
        try
        {

            string Cmdtext = string.Empty;
            DataSet ds = new DataSet();

            Cmdtext = "select ProductId,ProductCode from Product where EventID=13 and ProductID in (select distinct(ProductId) from EventFees where EventId=13 and EventYear=" + ddlYear.SelectedValue + " ) and ProductID in(select ProductID from CalSignUp where EventYear='" + ddlYear.SelectedValue + "' and Accepted='Y')";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {

                ddlProductFilter.DataSource = ds;
                ddlProductFilter.DataTextField = "ProductCode";
                ddlProductFilter.DataValueField = "ProductId";
                ddlProductFilter.DataBind();

                ddlProductFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlProductFilter.Enabled = true;

            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    public void fillCoachSection()
    {
        try
        {

            string cmdText = "";


            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddlYear.SelectedValue + " and V.EventID=13 and V.Accepted='Y' order by ID.FirstName ASC";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {

                ddlCoachFilter.DataSource = ds;
                ddlCoachFilter.DataTextField = "Name";
                ddlCoachFilter.DataValueField = "AutoMemberID";
                ddlCoachFilter.DataBind();

                ddlCoachFilter.Items.Insert(0, new ListItem("Select", "-1"));
                ddlCoachFilter.Enabled = true;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    protected void ddlFilterBy_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlFilterBy.SelectedValue == "None")
        {
            dvFilterSection.Visible = false;
            fillMeetingGrid("All");
        }
        else if (ddlFilterBy.SelectedValue == "Year")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = true;
        }
        else if (ddlFilterBy.SelectedValue == "ProductGroup")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = true;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
        else if (ddlFilterBy.SelectedValue == "Product")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = true;
            dvCoachFilter.Visible = false;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
        else if (ddlFilterBy.SelectedValue == "Semester")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = false;
            dvPhaseFilter.Visible = true;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
        }
        else if (ddlFilterBy.SelectedValue == "Coach")
        {
            dvFilterSection.Visible = true;
            dvFilterPrdGroupSection.Visible = false;
            dvProductFilter.Visible = false;
            dvCoachFilter.Visible = true;
            dvButton.Visible = true;
            dvYearSection.Visible = false;
            fillCoachSection();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void btnCloseTable2_Click(object sender, EventArgs e)
    {
        dvStudentList.Visible = false;
        btnCloseTable2.Visible = false;
    }

    protected void btnScheduleMeeting_Click(object sender, EventArgs e)
    {

        ScheduleTrainingCenterAll();

    }


    [WebMethod]
    public static List<ParentInfo> ListParentInfo(long PMemberID)
    {

        int retVal = -1;
        List<ParentInfo> objListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = " select distinct IP.FirstName as IndName, IP.LastName as IndLastName, IP1.FirstName as SpFirstName, IP.LastName as spLastName, IP.EMail as IndEMail, IP1.EMail as SpouseEmail, IP.Cphone, IP.WPhone, IP.HPhone, C.FIRST_NAME, C.LAST_NAME, C.EMail, IP.State, IP.City from indspouse IP inner join child C on (C.MemberID=IP.AutoMemberID) inner join Indspouse IP1 on (IP1.Relationship=IP.AutoMemberID) inner join CoachReg CR on (CR.PmemberID=IP.AutomemberID and CR.Approved='Y') where IP.AutoMemberID=" + PMemberID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo parent = new ParentInfo();

                        parent.IndFirstName = dr["IndName"].ToString();
                        parent.IndLastName = dr["IndLastName"].ToString();
                        parent.SpouseFirstName = dr["SpFirstName"].ToString();
                        parent.SpouseLastName = dr["spLastName"].ToString();
                        parent.ChildFirstName = dr["FIRST_NAME"].ToString();
                        parent.ChildLastName = dr["LAST_NAME"].ToString();
                        parent.IndEMail = dr["IndEMail"].ToString();
                        parent.SpouseEmail = dr["SpouseEmail"].ToString();
                        parent.ChildEmail = dr["EMail"].ToString();
                        parent.HPhone = dr["HPhone"].ToString();
                        parent.CPhone = dr["CPhone"].ToString();
                        parent.State = dr["State"].ToString();
                        parent.City = dr["City"].ToString();

                        objListParent.Add(parent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        return objListParent;
    }

    [WebMethod]
    public static List<ParentInfo> ListCoachInfo(long CMemberID)
    {

        int retVal = -1;
        List<ParentInfo> objListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = " select IP.FirstName as CoachFirstName, IP.LastName as CoachLastname, IP.EMail as CoachEmail, IP.Cphone, IP.WPhone, IP.HPhone from indspouse IP where IP.AutoMemberID=" + CMemberID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo parent = new ParentInfo();

                        parent.CoachFirstname = dr["CoachFirstName"].ToString();
                        parent.Coachlastname = dr["CoachLastname"].ToString();

                        parent.CoachEmail = dr["CoachEmail"].ToString();

                        parent.HPhone = dr["HPhone"].ToString();
                        parent.CPhone = dr["CPhone"].ToString();

                        objListParent.Add(parent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        return objListParent;
    }

    [WebMethod]
    public static List<ParentInfo> getEmailBasedOnMemberID(long MemberID)
    {

        int retVal = -1;
        List<ParentInfo> objListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = " select I IP.EMail as ParentEmail from indspouse IP where IP.AutoMemberID=" + MemberID + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo parent = new ParentInfo();


                        parent.IndEMail = dr["EMail"].ToString();



                        objListParent.Add(parent);
                    }
                }
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        return objListParent;
    }

    [WebMethod]
    public static int SendEmailsCoachesAndParents(string FromEmail, string ToEmail, string Subject, string Body, string CC)
    {

        int retVal = -1;



        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(FromEmail);
            mail.Body = Body;
            mail.Subject = Subject;

            //mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

            String[] strEmailAddressList = null;
            String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Match EmailAddressMatch = default(Match);
            strEmailAddressList = ToEmail.Replace(',', ';').Split(';');
            foreach (object item in strEmailAddressList)
            {
                EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);

                if (EmailAddressMatch.Success)
                {

                    mail.Bcc.Add(new MailAddress(item.ToString().Trim()));

                }
            }
            if (CC != "")
            {
                mail.CC.Add(new MailAddress(CC.Trim()));
            }
            SmtpClient client = new SmtpClient();
            // client.Host = host;
            mail.IsBodyHtml = true;
            try
            {
                client.Send(mail);
                retVal = 1;
                //lblError.Text = ex.ToString();
            }
            catch (Exception ex)
            {
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }


        return retVal;

    }

    [WebMethod]
    public static List<ParentInfo> GetCoachSessionInfo(long MemberId)
    {

        int retVal = -1;
        List<ParentInfo> objListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            int EventYear = 2017;
            try
            {
                EventYear = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "select max(EventYear) from EventFees where EventId=13").ToString());
            }
            catch
            {
            }
            string Semester = "Fall";
            try
            {
                Common objCommon = new Common();
                Semester = objCommon.GetDefaultSemester(EventYear.ToString());
            }
            catch
            {
            }
            string cmdText = " Select * from CalSignup where MemberId=" + MemberId + " and Accepted='Y' and Eventyear=" + EventYear + " and Semester='" + Semester + "'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo parent = new ParentInfo();

                        parent.SessionKey = dr["MeetingKey"].ToString();
                        parent.SessionNo = dr["SessionNo"].ToString();
                        parent.ProductName = dr["ProductCode"].ToString();


                        objListParent.Add(parent);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        return objListParent;
    }

    [WebMethod]
    public static List<ParentInfo> GetPreviousClassBasedCoach(long MemberId)
    {

        int retVal = -1;
        List<ParentInfo> objListParent = new List<ParentInfo>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = " Select EventYear, ProductGroupCode, ProductCode,Level, (Select count(*) from CoachReg where EventYear=cs.EventYear and Approved='Y' and CMemberId=cs.memberId and EventYear=cs.EventYear and SessionNo=cs.SessionNo and ProductId=cs.ProductId and Semester=cs.Semester) as ChildrenCount from CalSignup cs where cs.MemberId=" + MemberId + " and cs.Accepted='Y'";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ParentInfo parent = new ParentInfo();

                        parent.ProductName = dr["ProductCode"].ToString();
                        parent.ProductGroupName = dr["ProductGroupCode"].ToString();
                        parent.EventYear = dr["EventYear"].ToString();
                        parent.NoOfCountChildren = dr["ChildrenCount"].ToString();
                        parent.Level= dr["Level"].ToString();

                        objListParent.Add(parent);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        return objListParent;
    }

    public class NSFScholorship
    {
        public string EventYear { get; set; }
        public int Renewal { get; set; }
        public int Fresh { get; set; }
        public int Total { get; set; }
        public string Type { get; set; }
        public string ChapterId { get; set; }
        public List<NSFScholorshipHistory> ListScholor { get; set; }
    }
    public class NSFScholorshipHistory
    {

        public int Renewal { get; set; }
        public int Fresh { get; set; }

    }
    [WebMethod]
    public static List<NSFScholorship> GetScholorshipHistory(NSFScholorship ObjScholr)
    {
        List<NSFScholorship> ObjListScholor = new List<NSFScholorship>();
        List<NSFScholorshipHistory> ListScholor = new List<NSFScholorshipHistory>();
        string CmdText = "";
        CmdText += " (SELECT 'Fresh' AS 'type', [2013],[2014],[2015], [2016],[2017]";
        CmdText += "FROM";
        CmdText += "(SELECT ScholarshipEntryYear, AppID ";
        CmdText += " FROM Applicant_Header where AppStatusID=7 and ChapterID=6) AS SourceTable1 ";
        CmdText += "PIVOT";
        CmdText += "(";
        CmdText += "count(AppID)";
        CmdText += "FOR ScholarshipEntryYear IN ([2013],[2014],[2015],[2016],[2017])";
        CmdText += ") AS P1)union(SELECT 'Renewal' AS ' ', [2013],[2014],[2015], [2016],[2017]";
        CmdText += "FROM";
        CmdText += "(SELECT ScholarshipEntryYear, AppID ";
        CmdText += " FROM Applicant_Header where ChapterID=6 and ";
        CmdText += "(Applicant_Header.RenewYear1=Applicant_Header.ScholarshipEntryYear and Applicant_Header.AppStatusRenew1=7) or";
        CmdText += "(Applicant_Header.RenewYear2=Applicant_Header.ScholarshipEntryYear and Applicant_Header.AppStatusRenew2=7) or";
        CmdText += "(Applicant_Header.RenewYear3=Applicant_Header.ScholarshipEntryYear and Applicant_Header.AppStatusRenew3=7) or";
        CmdText += "(Applicant_Header.RenewYear4=Applicant_Header.ScholarshipEntryYear and Applicant_Header.AppStatusRenew4=7)) AS SourceTable2 ";
        CmdText += "PIVOT";
        CmdText += "(";
        CmdText += "count(AppID)";
        CmdText += "FOR ScholarshipEntryYear IN ([2013],[2014],[2015], [2016],[2017])";
        CmdText += ") AS P2)";

        SqlConnection cn = new SqlConnection("Data Source=sql.northsouth.org,1433;Initial Catalog=IndiaBee_prd;User ID=northsouth;Password=Nhemi$SHemi");
        cn.Open();
        SqlCommand cmd = new SqlCommand(CmdText, cn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        if (null != ds)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    NSFScholorship ObjScholor = new NSFScholorship();

                    ObjScholor.Type = dr["Type"].ToString();
                    int i = 0;
                    foreach (DataColumn dc in ds.Tables[0].Columns)
                    {
                        NSFScholorshipHistory ObjHistory = new NSFScholorshipHistory();
                        i++;
                        if (dc.ColumnName.ToString() != "")
                        {

                            if (dr["Type"].ToString() == "Fresh")
                            {
                                if (i != 1)
                                {
                                    ObjHistory.Fresh = Convert.ToInt32(dr[dc.ColumnName.ToString()].ToString());
                                }
                            }
                            else
                            {
                                if (i != 1)
                                {
                                    ObjHistory.Renewal = Convert.ToInt32(dr[dc.ColumnName.ToString()].ToString());
                                }
                            }
                        }

                        ListScholor.Add(ObjHistory);
                        ObjScholor.ListScholor = ListScholor;
                    }

                    ObjListScholor.Add(ObjScholor);
                }
            }
        }

        return ObjListScholor;
    }

    public class ParentInfo
    {
        public string PMemberID { get; set; }
        public string IndFirstName { get; set; }
        public string IndLastName { get; set; }
        public string SpouseFirstName { get; set; }
        public string SpouseLastName { get; set; }
        public string ChildFirstName { get; set; }
        public string ChildLastName { get; set; }
        public string IndEMail { get; set; }
        public string SpouseEmail { get; set; }
        public string ChildEmail { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string CoachID { get; set; }
        public string ProductName { get; set; }
        public string ProductGroupName { get; set; }
        public string EventYear { get; set; }
        public string SessionKey { get; set; }
        public string SessionNo { get; set; }
        public string CoachFirstname { get; set; }
        public string Coachlastname { get; set; }
        public string CoachEmail { get; set; }
        public string Level { get; set; }
        public string Grade { get; set; }
        public string State { get; set; }
        public string City { get; set; }
        public string NoOfCountChildren { get; set; }
    }


    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
        ddlProductGroup.Items.Clear();
        ddlProduct.Items.Clear();
        ddlLevel.Items.Clear();
        ddlSessionNo.Items.Clear();
        txtTopic.Text = "";
        fillMeetingGrid("");
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue);
        ddlPhaseFilter.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue);
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ValidateMeetingList() == 1)
        {
            fillMeetingGrid("false");
        }
    }

    public void loadLevelFilter()
    {
        try
        {
            string strSql = " select distinct LevelCode from ProdLevel where EventYear=" + ddlYearFilter.SelectedValue + "  and EventID=13";
            if (ddlProductGroupFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "-1")
            {
                strSql = strSql + " and ProductID=" + ddlProductFilter.SelectedValue + "";
            }

            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
            DDlLevelFilter.DataValueField = "LevelCode";
            DDlLevelFilter.DataTextField = "LevelCode";
            DDlLevelFilter.DataSource = drproductgroup;
            DDlLevelFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                DDlLevelFilter.Items.Insert(0, new ListItem("Select", "-1"));
                DDlLevelFilter.Enabled = true;

            }
            else
            {
                // ddlLevelFilter.Enabled = false;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }

    }
    protected void ddlCoachFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");

    }
    protected void DDlLevelFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void ddlProductFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void ddlProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void ddlPhaseFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void ddlYearFilter_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("true");
    }
    protected void btnClearFilter_Click(object sender, EventArgs e)
    {
        Response.Redirect("~/SetupZoomSessions.aspx");
    }

    public void DeleteBulkZoomSessions()
    {
        string Cmdtext = "select Vl.Hostid, VL.Pwd, WC.SessionKey from Webconflog WC inner join VirtualRoomLookup VL on(VL.Userid = Wc.userid) where eventYear = 2017";
        //and productgroupcode not in ('UV')
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0].Rows.Count > 0)
        {
            foreach (DataRow dr in ds.Tables[0].Rows)
            {
                string URL = string.Empty;
                string service = "3";
                URL = "https://api.zoom.us/v1/meeting/delete";
                string urlParameters = string.Empty;

                urlParameters += "api_key=" + apiKey + "";
                urlParameters += "&api_secret=" + apiSecret + "";
                urlParameters += "&data_type=JSON";
                urlParameters += "&host_id=" + dr["Hostid"].ToString() + "";
                urlParameters += "&id=" + dr["SessionKey"].ToString() + "";

             //   makeZoomAPICall(urlParameters, URL, service);

                try
                {


                    HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
                    objRequest.Method = "POST";
                    objRequest.ContentLength = urlParameters.Length;
                    objRequest.ContentType = "application/x-www-form-urlencoded";

                    // post data is sent as a stream
                    StreamWriter myWriter = null;
                    myWriter = new StreamWriter(objRequest.GetRequestStream());
                    myWriter.Write(urlParameters);
                    myWriter.Close();

                    // returned values are returned as a stream, then read into a string
                    string postResponse;
                    HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
                    using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
                    {
                        postResponse = responseStream.ReadToEnd();

                        responseStream.Close();
                        
                    }
                   
                }
                catch (Exception ex)
                {
                    CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
                }

            }
        }
    }

    protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillMeetingGrid("");
    }
}