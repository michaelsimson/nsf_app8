﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="CoachClassCalendar.aspx.cs" Inherits="Vol_Signup_Guide_Task_Default" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server"> 
      <div>

            <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                runat="server">
                <strong>Add/Update Volunteer Signup Guide

                </strong>
            </div>
            <br />

            <table style="margin-left: 0px;" border="0">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <a href="http://localhost:3333/App8Full/Vol_Signup_Guide_Task/VolSignupGuide.aspx">Back to Volunteer Functions</a>
                </tr>
                <tr><font color="red"><asp:Label ID="lbevent" runat="server"   ></asp:Label></font></tr>
                <tr>
                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event </b></td>
                    <td style="width: 141px" align="left">
                        <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Team</b> </td>
                    <td style="width: 141px" align="left">
                        <asp:DropDownList ID="ddTeam" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddTeam_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Global</b> </td>
                    <td style="width: 141px" align="left">
                        <asp:DropDownList
                            ID="ddGlobal"
                            runat="server"
                            AutoPostBack="true"
                            Width="150px"
                            OnSelectedIndexChanged="ddGlobal_SelectedIndexChanged">
                            <asp:ListItem>Yes</asp:ListItem>
                            <asp:ListItem>No</asp:ListItem>
                        </asp:DropDownList>
                    </td>
                </tr>
                <tr>
                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Description</b> </td>

                    <td colspan="5">
                        <asp:TextBox ID="tbdescription" TextMode="MultiLine" runat="server" Height="33px" Width="197px"></asp:TextBox></td>




                </tr>



            </table>
            <asp:Button ID="Button1" runat="server" Text="Add/Update" OnClick="Button1_Click" />

            <a href="http://localhost:3333/App8Full/Vol_Signup_Guide_Task/VolSignupView.aspx">View Event with Teams</a>


        </div>
        <div id="Div4"  align="center"  runat="server" >
   
            </div>
   </asp:Content>