﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="PopupHelpGuide.aspx.cs" Inherits="Vol_Signup_Guide_Task_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
    <div>
                    <asp:Label ID="errlabel" runat="server" ></asp:Label>
        <table>
                     <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event </b></td>
</td>
                    <td style="width: 141px" align="left">
                        <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td></table>
      <asp:GridView BorderWidth="0px"  ID="GridViewTeamDescription" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                <Columns>
                    <asp:TemplateField Visible="True">
                        <ItemTemplate>
                   <asp:HiddenField ID="VolSignupGID" Value='<%# Bind("VolSignupGID")%>' runat="server" />

                            <b><asp:Label ID="Label1" runat="server" Text='<%#Eval("TeamName") %>'></asp:Label></b>
                            <br />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
    </div>
    </form>
</body>
</html>
