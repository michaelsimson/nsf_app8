﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Data.SqlClient;


public partial class Vol_Signup_Guide_Task_Default : System.Web.UI.Page
{
    string ConnectionString = "ConnectionString";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Events();


        }
        
    }
    protected void Events()
    {
        string DDLevent;

        try
        {
            DDLevent = "select EventId,EventCode from Event;";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, DDLevent);
            DataSet myDataSet = new DataSet();
            ddEvent.DataSource = dsstate;
            ddEvent.DataTextField = "EventCode";
            ddEvent.DataValueField = "EventId";
            ddEvent.DataBind();
            ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));

        }
        catch (Exception ex)
        {
            //Label1.Text = ex.Message;
        }
        finally
        {
            // close the Sql Connection 
            //mySqlConnection.Close();
        }

    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        string str;
        try
        {
            str = "select VolSignupGID,TeamName,Description from VolSignupGuide where EventName='" + ddEvent.SelectedItem.Text + "' ;";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, str);

            GridViewTeamDescription.DataSource = dsstate.Tables[0];
            GridViewTeamDescription.DataBind();
        }
        catch (Exception ex) { errlabel.Text = "Exception" + ex; }
    }
}