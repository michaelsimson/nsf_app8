﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VolSignupView.aspx.cs" Inherits="Vol_Signup_Guide_Task_Default" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title></title>
</head>
<body>
    <form id="form1" runat="server">
        <div>

            <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                runat="server">
                <strong>Volunteer Signup Guide


                </strong>
            </div>
            <asp:Label ID="errlabel" runat="server" ></asp:Label>
            <br />

            <table style="margin-left: 0px;" border="0">
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td align="right"></td>
                </tr>
                <tr>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <a href="http://localhost:3333/App8Full/Vol_Signup_Guide_Task/VolSignupGuide.aspx">Back to Volunteer Functions</a>
                </tr>
                <tr> </b></td>
                    <td>                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event </b></td>
</td>
                    <td style="width: 141px" align="left">
                        <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddEvent_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td align="left" nowrap="nowrap">&nbsp;</td>
                    <td style="width: 141px" align="left">
                    </td>
                    <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;</td>
                    <td style="width: 141px" align="left">
                        &nbsp;</td>
                    </tr>
        



            </table>

           


            <br />
            <div id="Div4"  align="center"  runat="server" >
            <asp:GridView BorderWidth="0px"  ID="GridViewTeamDescription" runat="server" AutoGenerateColumns="False" EnableModelValidation="True">
                <Columns>
                    <asp:TemplateField Visible="True">
                        <ItemTemplate>
                   <asp:HiddenField ID="VolSignupGID" Value='<%# Bind("VolSignupGID")%>' runat="server" />

                            <b><asp:Label ID="Label1" runat="server" Text='<%#Eval("TeamName") %>'></asp:Label></b>
                            <br />
                           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:Label ID="Label2" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
            </asp:GridView>
                </div>
            <br />

           


        </div>
        <%--<asp:GridView ID="VolSignupGuideView" DataKeyNames="VolSignupGID" runat="server" AutoGenerateColumns="False" EnableViewState="true" OnRowUpdating="VolSignupGuideView_RowUpdating" OnRowEditing="VolSignupGuideView_RowEditing" OnRowCancelingEdit="VolSignupGuideView_RowCancelingEdit">
            <Columns>
                <asp:TemplateField HeaderText="">
                    <EditItemTemplate>
                        <asp:HiddenField ID="VolSignupGID" Value='<%# Bind("VolSignupGID")%>' runat="server" />
                        <asp:Button ID="btnEdit" Text="Update" runat="server"  CommandName="Update" />
                        <asp:Button ID="Button2" Text="Cancel" runat="server" CommandName="Cancel" />
                    </EditItemTemplate>
                    <ItemTemplate>
                        
                        <asp:Button ID="btnEdit" Text="Edit" runat="server" CommandName="Edit" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Event">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddEvent" runat="server">
                        </asp:DropDownList>
                        <asp:HiddenField ID="HFddevent" Value='<%# Bind("EventName")%>' runat="server" />

                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("EventName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Teams">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddTeam" runat="server">
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("TeamName") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Global">
                    <EditItemTemplate>
                        <asp:DropDownList ID="ddGlobal" runat="server"  >
                        </asp:DropDownList>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%#Eval("Global") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Description">
                    <EditItemTemplate>
                        <asp:TextBox ID="tbdescription" Text='<%#Bind("Description") %>' runat="server"></asp:TextBox>
                    </EditItemTemplate>
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("Description") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>--%>
    </form>
</body>
</html>