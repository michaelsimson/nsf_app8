﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachingDateCal.aspx.vb" Inherits="CoachingDateCal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script language="javascript" src="js/Calendar.js" type="text/javascript"></script>

    <link href="css/Calendar.css" rel="stylesheet" type="text/css" />

    <div>

        <div runat="server" id="div1">
            <asp:HiddenField ID="hfSD0" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfSD1" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfSD2" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfSD3" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfSD4" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfSD5" runat="server"></asp:HiddenField>

            <asp:HiddenField ID="hfED0" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfED1" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfED2" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfED3" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfED4" runat="server"></asp:HiddenField>
            <asp:HiddenField ID="hfED5" runat="server"></asp:HiddenField>
        </div>



        <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
            <tr>

                <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                    <h2>Official Coaching Date Calendar</h2>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                    <br />
                    <br />
                </td>
            </tr>
            <tr>
                <td colspan="2">

                    <table width="95%" style="margin-left: auto; margin-right: auto; font-weight: bold; background-color: #ffffcc;">
                        <tr class="ContentSubTitle">
                            <td>Year</td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>Event</td>
                            <td>
                                <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="True">
                                </asp:DropDownList>
                            </td>
                            <td>Semester</td>
                            <td>
                                <asp:DropDownList ID="ddlPhase" runat="server" AutoPostBack="True" Width="105px" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                                </asp:DropDownList>
                            </td>
                            <td>Product Group</td>
                            <td>
                                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" Width="125px">
                                </asp:DropDownList>
                            </td>
                            <td>Product</td>
                            <td>
                                <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="ProductCode" DataValueField="ProductId" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Width="110px">
                                </asp:DropDownList>
                            </td>

                            <td>
                                <center>
                                    <asp:Button ID="btnContinue" runat="server" Text="Continue" Style="text-align: center" /></center>
                            </td>
                        </tr>
                    </table>
                    <br />
                    <table width="95%" style="margin-left: auto; margin-right: auto">
                        <tr>
                            <td>

                                <asp:GridView ID="grdCoachBySchedule" runat="server" AutoGenerateColumns="False" Visible="false" EnableViewState="true" Width="100%" HeaderStyle-BackColor="#ffffcc">
                                    <Columns>
                                        <asp:TemplateField ItemStyle-Width="70px">
                                            <ItemTemplate>
                                                <asp:Button ID="btnAdd" runat="server" Text="Add" CommandName="Add" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Schedule Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheduleType" runat="server" Width="150px" Text='<%#DataBinder.Eval(Container.DataItem, "ScheduleType")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Width="150px" Text="__/__/____"></asp:Label>

                                                <asp:ImageButton runat="server" ID="imgStartDate" ImageUrl="~/Images/Calendar.gif" />

                                            </ItemTemplate>

                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date" HeaderStyle-Width="100px">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDate" runat="server" Text="__/__/____" EnableViewState="true"></asp:Label>

                                                <asp:ImageButton runat="server" ID="imgEndDate" ImageUrl="~/Images/Calendar.gif" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nature">
                                            <ItemTemplate>
                                                <asp:DropDownList ID="ddlNature" runat="server" Width="100%">
                                                    <asp:ListItem>TBD</asp:ListItem>
                                                    <asp:ListItem>Tentative</asp:ListItem>
                                                    <asp:ListItem>Firm</asp:ListItem>
                                                </asp:DropDownList>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Message">
                                            <ItemTemplate>
                                                <asp:TextBox ID="txtMessage" runat="server" Width="98%"></asp:TextBox>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>

                                </asp:GridView>

                            </td>

                        </tr>
                    </table>


                    <div id="divUpdate" runat="server" style="width: 95%;" visible="false">

                        <table style="margin-left: auto; margin-right: auto; border-style: solid; border-width: 1px;" class="tableclass">
                            <tr>
                                <td style="text-align: center; font-weight: bold; background-color: #ffffcc;" colspan="2" class="ContentSubTitle">
                                    <h3>Update Coaching Date Calendar</h3>
                                </td>
                            </tr>
                            <tr>
                                <td>Year</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdYear" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>

                            <tr>
                                <td>Event</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdEvent" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Product Group Code</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroupCode" DataValueField="ProductGroupId">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Product Code</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdProduct" runat="server" DataTextField="ProductCode" DataValueField="ProductId">
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Semester</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdPhase" runat="server">
                                        <asp:ListItem Value="0">Select</asp:ListItem>
                                        <asp:ListItem Value="Fall">Fall</asp:ListItem>
                                        <asp:ListItem Value="Spring">Spring</asp:ListItem>
                                        <asp:ListItem Value="Summer">Summer</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                            <tr>
                                <td>Schedule Type</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdScheduleType" runat="server">
                                        <asp:ListItem>Term</asp:ListItem>
                                        <asp:ListItem>Thanksgiving</asp:ListItem>
                                        <asp:ListItem>Christmas</asp:ListItem>
                                        <asp:ListItem>Spring Break</asp:ListItem>
                                        <asp:ListItem>Diwali</asp:ListItem>
                                        <asp:ListItem>Halloween</asp:ListItem>
                                        <asp:ListItem>New Year</asp:ListItem>
                                    </asp:DropDownList>
                                    <asp:HiddenField ID="h_UpdScheduleType" runat="server"></asp:HiddenField>
                                </td>
                            </tr>
                            <tr>
                                <td>Start Date</td>
                                <td>
                                    <asp:Label ID="lblUpdStartDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                    <asp:ImageButton ID="imgUpdStartDate" runat="server" ImageUrl="~/Images/Calendar.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td>End Date</td>
                                <td>
                                    <asp:Label ID="lblUpdEndDate" runat="server" EnableViewState="true" Text="__/__/____"></asp:Label>
                                    <asp:ImageButton ID="imgUpdEndDate" runat="server" ImageUrl="~/Images/Calendar.gif" />
                                </td>
                            </tr>
                            <tr>
                                <td>Nature</td>
                                <td>
                                    <asp:DropDownList ID="ddlUpdNature" runat="server">
                                        <asp:ListItem>TBD</asp:ListItem>
                                        <asp:ListItem>Tentative</asp:ListItem>
                                        <asp:ListItem>Firm</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>Message</td>
                                <td>
                                    <asp:TextBox ID="txtUpdMessage" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td colspan="2" align="center">
                                    <asp:Label ID="lblCDateCalId" runat="server" Visible="false"></asp:Label>
                                    <asp:Button ID="btnUpdate" runat="server" Text="Update" />
                                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" />
                                </td>
                            </tr>
                        </table>


                    </div>

                    <div style="width: 100%; height: 25px">
                        <center>
                            <asp:Label ID="lblMsg" runat="server"></asp:Label>
                        </center>
                    </div>

                    <table width="95%" style="margin-left: auto; margin-right: auto">

                        <tr>
                            <td>

                                <asp:HiddenField ID="hfUpdateStartDate" runat="server" />
                                <asp:HiddenField ID="hfUpdateEndDate" runat="server" />

                                <asp:GridView ID="grdCoachCalAll" runat="server" AutoGenerateColumns="False" EnableViewState="true" Width="100%" HeaderStyle-BackColor="#ffffcc">
                                    <Columns>
                                        <asp:TemplateField>
                                            <ItemTemplate>
                                                <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="CDateCalId" Visible="True" HeaderText="Id" HeaderStyle-Width="50px"></asp:BoundField>
                                        <asp:BoundField DataField="EventYear" Visible="true" HeaderText="EventYear"></asp:BoundField>

                                        <asp:TemplateField HeaderText="Event">
                                            <ItemTemplate>
                                                <asp:Label ID="lblEventId" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem, "EventId")%>'></asp:Label>
                                                <asp:Label ID="lblProductGroupId" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem, "ProductGroupId")%>'></asp:Label>
                                                <asp:Label ID="lblProductId" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container.DataItem, "ProductId")%>'></asp:Label>

                                                <asp:Label ID="lblEvent" runat="server" Width="150px" Text='<%#DataBinder.Eval(Container.DataItem, "EventCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="ProductGroup">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "ProductGroupCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Product">

                                            <ItemTemplate>
                                                <asp:Label ID="lblProductCode" runat="server" Width="150px" Text='<%#DataBinder.Eval(Container.DataItem, "ProductCode")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:BoundField DataField="Semester" Visible="true" HeaderText="Semester"></asp:BoundField>

                                        <asp:TemplateField HeaderText="Schedule Type">
                                            <ItemTemplate>
                                                <asp:Label ID="lblScheduleType" runat="server" Width="150px" Text='<%#DataBinder.Eval(Container.DataItem, "ScheduleType")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Date" HeaderStyle-Width="100px">

                                            <ItemTemplate>
                                                <asp:Label ID="lblStartDate" runat="server" Width="150px" Text='<%#DataBinder.Eval(Container.DataItem, "StartDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Date" HeaderStyle-Width="100px">

                                            <ItemTemplate>
                                                <asp:Label ID="lblEndDate" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "EndDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Width="100px"></HeaderStyle>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Nature">

                                            <ItemTemplate>

                                                <asp:Label ID="lblNature" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Nature")%>'></asp:Label>


                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Message">

                                            <ItemTemplate>
                                                <asp:Label ID="lblMessage" runat="server" Text='<%# DataBinder.Eval(Container.DataItem, "Message")%>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                </asp:GridView>

                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>


    </div>

</asp:Content>

