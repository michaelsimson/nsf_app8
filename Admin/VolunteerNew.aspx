<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VolunteerNew.aspx.cs" Inherits="Admin_VolunteerNew" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:GridView ID="gvIndSpouse" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" Caption="Individual / Spouse"
        CaptionAlign="Top" DataKeyNames="MemberID" EnableSortingAndPagingCallbacks="True"
        OnRowCommand="gvIndSpouse_RowCommand" OnSelectedIndexChanged="gvIndSpouse_SelectedIndexChanged"
        PageSize="5" CellPadding="4" ForeColor="#333333" GridLines="None">
        <Columns>
            <asp:ButtonField DataTextField="automemberID" HeaderText="Add Role" Text="Add" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
            <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
            <asp:BoundField DataField="Email" HeaderText="Email" />
            <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
            <asp:BoundField DataField="Address1" HeaderText="Address" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" />
            <asp:BoundField DataField="State" HeaderText="State" />
        </Columns>
        <HeaderStyle BorderStyle="Solid" BorderWidth="1px" BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <RowStyle BackColor="#E3EAEB" />
        <EditRowStyle BackColor="#7C6F57" />
        <SelectedRowStyle BackColor="#C5BBAF" Font-Bold="True" ForeColor="#333333" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <AlternatingRowStyle BackColor="White" />
    </asp:GridView>
    <asp:Label ID="lblMessage" runat="server" Style="z-index: 102; left: 7px; position: absolute;
        top: 151px" Text=""></asp:Label>
    <asp:HyperLink ID="hlinkGoBack" runat="server" NavigateUrl="~/ChapVolunteerRoles.aspx">Go back to Menu</asp:HyperLink>
</asp:Content>


 

 
 
 