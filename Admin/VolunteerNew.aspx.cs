using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

public partial class Admin_VolunteerNew : System.Web.UI.Page
{
    string sSQL;
    //Data.SqlClient.SqlDataAdapter dataAdapterInd, dataAdapterOrg;
    DataSet dsIndSpouse = new DataSet();

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginchapterID"] != null)
            sSQL = " DeletedFlag <> 'Yes' and CHAPTER = '" + Session["LoginChapterID"].ToString() + "'";
        else sSQL = " DeletedFlag <> 'Yes'";
        NorthSouth.DAL.IndSpouseDAL indDal = new NorthSouth.DAL.IndSpouseDAL(WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString());
        indDal.SearchIndSpouseWhere(dsIndSpouse, sSQL);

        if (dsIndSpouse.Tables[0].Rows.Count > 0)
        {
            gvIndSpouse.DataSource = dsIndSpouse;
            gvIndSpouse.DataBind();
            //string[] keyColumn = dsIndSpouse.Tables[0].Columns["MemberID"];
            gvIndSpouse.DataKeyNames = new string[] { "automemberID" };
        }
        else lblMessage.Text = "No results matching your critiria found!";
    }
    protected void gvIndSpouse_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvIndSpouse_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        Session["MemberId"] = gvIndSpouse.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString();
        Response.Redirect("VolunteerAddByChap.aspx?MemberID=" + Session["MemberId"]);
    }
}

 