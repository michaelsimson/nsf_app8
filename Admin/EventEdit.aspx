
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="EventEdit.aspx.cs" Inherits="EventEdit" Title="Event Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<data:MultiFormView ID="FormView1" DataKeyNames="EventId" runat="server" DataSourceID="EventDataSource">
		
			<EditItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/EventFields.ascx" />
			</EditItemTemplatePaths>
		
			<InsertItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/EventFields.ascx" />
			</InsertItemTemplatePaths>
		
			<EmptyDataTemplate>
				<b>Event not found!</b>
			</EmptyDataTemplate>
			
			<FooterTemplate>
				<asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
				<asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
				<asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
			</FooterTemplate>

		</data:MultiFormView>
		
		<data:EventDataSource ID="EventDataSource" runat="server"
			SelectMethod="GetByEventId"
		>
			<Parameters>
				<asp:QueryStringParameter Name="EventId" QueryStringField="EventId" Type="String" />

			</Parameters>
		</data:EventDataSource>
		
		<br />

		<data:EntityGridView ID="GridViewRegistration" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewRegistration_SelectedIndexChanged"			 			 
			DataSourceID="RegistrationDataSource"
			DataKeyNames="RegistrationId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Registration.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" SortExpression="ProductGroupId" />				
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />				
				<asp:BoundField DataField="MemberId" HeaderText="MemberId" SortExpression="MemberId" />				
				<asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />				
				<asp:BoundField DataField="EventYear" HeaderText="EventYear" SortExpression="EventYear" />				
				<asp:BoundField DataField="ParentId" HeaderText="ParentId" SortExpression="ParentId" />				
				<asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" SortExpression="ChildNumber" />				
				<asp:BoundField DataField="Fee" HeaderText="Fee" SortExpression="Fee" />				
				<asp:BoundField DataField="DiscountedAmount" HeaderText="DiscountedAmount" SortExpression="DiscountedAmount" />				
				<asp:BoundField DataField="LateFee" HeaderText="LateFee" SortExpression="LateFee" />				
				<asp:BoundField DataField="PaymentDate" HeaderText="PaymentDate" SortExpression="PaymentDate" />				
				<asp:BoundField DataField="PaymentMode" HeaderText="PaymentMode" SortExpression="PaymentMode" />				
				<asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference" SortExpression="PaymentReference" />				
				<asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes" SortExpression="PaymentNotes" />				
				<asp:BoundField DataField="ChildNumberOld" HeaderText="ChildNumberOld" SortExpression="ChildNumberOld" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No Registration Found! </b>
				<asp:HyperLink runat="server" ID="hypRegistration" NavigateUrl="~/admin/RegistrationEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:RegistrationDataSource ID="RegistrationDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewRegistration" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewRegistration" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:RegistrationDataSource>
		<br />
		<data:EntityGridView ID="GridViewEventCalendar" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewEventCalendar_SelectedIndexChanged"			 			 
			DataSourceID="EventCalendarDataSource"
			DataKeyNames="EventCalendarId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_EventCalendar.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" SortExpression="ProductGroupId" />				
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />				
				<asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId" />				
				<asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />				
				<asp:BoundField DataField="EventYear" HeaderText="EventYear" SortExpression="EventYear" />				
				<asp:BoundField DataField="EventDay1" HeaderText="EventDay1" SortExpression="EventDay1" />				
				<asp:BoundField DataField="EventDay1Desc" HeaderText="EventDay1Desc" SortExpression="EventDay1Desc" />				
				<asp:BoundField DataField="EventDay1Fee" HeaderText="EventDay1Fee" SortExpression="EventDay1Fee" />				
				<asp:BoundField DataField="VenueId1" HeaderText="VenueId1" SortExpression="VenueId1" />				
				<asp:BoundField DataField="CheckinTimeDay1" HeaderText="CheckinTimeDay1" SortExpression="CheckinTimeDay1" />				
				<asp:BoundField DataField="StartTimeDay1" HeaderText="StartTimeDay1" SortExpression="StartTimeDay1" />				
				<asp:BoundField DataField="EndTimeDay1" HeaderText="EndTimeDay1" SortExpression="EndTimeDay1" />				
				<asp:BoundField DataField="BuildingDay1" HeaderText="BuildingDay1" SortExpression="BuildingDay1" />				
				<asp:BoundField DataField="RoomDay1" HeaderText="RoomDay1" SortExpression="RoomDay1" />				
				<asp:BoundField DataField="RegDeadlineDay1" HeaderText="RegDeadlineDay1" SortExpression="RegDeadlineDay1" />				
				<asp:BoundField DataField="LateFee1" HeaderText="LateFee1" SortExpression="LateFee1" />				
				<asp:BoundField DataField="DeadlineLateFee1" HeaderText="DeadlineLateFee1" SortExpression="DeadlineLateFee1" />				
				<asp:BoundField DataField="EventDay2" HeaderText="EventDay2" SortExpression="EventDay2" />				
				<asp:BoundField DataField="EventDay2Desc" HeaderText="EventDay2Desc" SortExpression="EventDay2Desc" />				
				<asp:BoundField DataField="EventDay2Fee" HeaderText="EventDay2Fee" SortExpression="EventDay2Fee" />				
				<asp:BoundField DataField="VenueId2" HeaderText="VenueId2" SortExpression="VenueId2" />				
				<asp:BoundField DataField="CheckinTimeDay2" HeaderText="CheckinTimeDay2" SortExpression="CheckinTimeDay2" />				
				<asp:BoundField DataField="StartTimeDay2" HeaderText="StartTimeDay2" SortExpression="StartTimeDay2" />				
				<asp:BoundField DataField="EndTimeDay2" HeaderText="EndTimeDay2" SortExpression="EndTimeDay2" />				
				<asp:BoundField DataField="BuildingDay2" HeaderText="BuildingDay2" SortExpression="BuildingDay2" />				
				<asp:BoundField DataField="RoomDay2" HeaderText="RoomDay2" SortExpression="RoomDay2" />				
				<asp:BoundField DataField="RegDeadlineDay2" HeaderText="RegDeadlineDay2" SortExpression="RegDeadlineDay2" />				
				<asp:BoundField DataField="LateFee2" HeaderText="LateFee2" SortExpression="LateFee2" />				
				<asp:BoundField DataField="DeadlineLateFee2" HeaderText="DeadlineLateFee2" SortExpression="DeadlineLateFee2" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No EventCalendar Found! </b>
				<asp:HyperLink runat="server" ID="hypEventCalendar" NavigateUrl="~/admin/EventCalendarEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:EventCalendarDataSource ID="EventCalendarDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewEventCalendar" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewEventCalendar" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:EventCalendarDataSource>
		<br />
		<data:EntityGridView ID="GridViewProduct" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewProduct_SelectedIndexChanged"			 			 
			DataSourceID="ProductDataSource"
			DataKeyNames="ProductId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Product.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />				
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No Product Found! </b>
				<asp:HyperLink runat="server" ID="hypProduct" NavigateUrl="~/admin/ProductEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:ProductDataSource ID="ProductDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewProduct" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewProduct" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ProductDataSource>
		<br />
		<data:EntityGridView ID="GridViewProductGroup" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewProductGroup_SelectedIndexChanged"			 			 
			DataSourceID="ProductGroupDataSource"
			DataKeyNames="ProductGroupId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_ProductGroup.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />				
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No ProductGroup Found! </b>
				<asp:HyperLink runat="server" ID="hypProductGroup" NavigateUrl="~/admin/ProductGroupEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:ProductGroupDataSource ID="ProductGroupDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewProductGroup" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewProductGroup" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ProductGroupDataSource>
		<br />
		

</asp:Content>


 

 
 
 