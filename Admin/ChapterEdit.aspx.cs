
#region Imports...
using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Web;

using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using nsf.Web.UI;
#endregion

public partial class ChapterEdit : System.Web.UI.Page
{
    protected void IsDuplicate(object source, ServerValidateEventArgs value)
    {

        // check if update operation
        if (((Button)(Page.Master.FindControl("Content_main").
                     FindControl("FormView1$UpdateButton"))).Visible == true)
        {
            value.IsValid = true;
            return;
        }

        // Declare database objects such as connection,
        // command and transaction
        string chapterCode = value.Value;
        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ChapterCode from Chapter where ChapterCode ='");
        queryStr.Append(chapterCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                /* if there is an entry, then there is a duplicate */
                /* So make the isValid false */
                value.IsValid = (reader.Read()) ? false : true;
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }
    }

    void ClusterCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string clusterCode = ((DropDownList)Page.Master.
            FindControl("Content_main").FindControl("FormView1$dataClusterCode")).SelectedItem.Value;

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ClusterId from Cluster where ClusterCode ='");
        queryStr.Append(clusterCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ((TextBox)(Page.Master.FindControl("Content_main").FindControl("FormView1$dataClusterId"))).Text =
                        reader.GetInt32(0).ToString();
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }

    }

    void ZoneCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string zoneCode = ((DropDownList)Page.Master.
            FindControl("Content_main").FindControl("FormView1$dataZoneCode")).SelectedItem.Value;

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ZoneId from Zone where ZoneCode ='");
        queryStr.Append(zoneCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ((TextBox)(Page.Master.FindControl("Content_main").FindControl("FormView1$dataZoneId"))).Text =
                        reader.GetInt32(0).ToString();
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }

    }

	protected void Page_Load(object sender, EventArgs e)
	{		
		FormUtil.RedirectAfterInsertUpdate(FormView1, "ChapterEdit.aspx?{0}", ChapterDataSource);
		FormUtil.RedirectAfterAddNew(FormView1, "ChapterEdit.aspx");
		FormUtil.RedirectAfterCancel(FormView1, "Chapter.aspx");
		FormUtil.SetDefaultMode(FormView1, "ChapterID");

        CustomValidator cv = Page.Master.FindControl("Content_main").
FindControl("FormView1$ChapterCodeValidator") as CustomValidator;
        cv.ServerValidate +=
                      new ServerValidateEventHandler(this.IsDuplicate);

        DropDownList ddl1 = Page.Master.FindControl("Content_main").
FindControl("FormView1$dataClusterCode") as DropDownList;
        ddl1.SelectedIndexChanged += new EventHandler(ClusterCode_SelectedIndexChanged);
        DropDownList ddl2 = Page.Master.FindControl("Content_main").
FindControl("FormView1$dataZoneCode") as DropDownList;
        ddl2.SelectedIndexChanged += new EventHandler(ZoneCode_SelectedIndexChanged);

        if (!Page.IsPostBack)
        {

            TextBox tbx1 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreateDate") as TextBox;
            TextBox tbx2 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreatedBy") as TextBox;
            TextBox tbx3 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifyDate") as TextBox;
            TextBox tbx4 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifiedBy") as TextBox;

            /* Make the audit trail related fields readonly */
            tbx1.ReadOnly = true;
            tbx2.ReadOnly = true;
            tbx3.ReadOnly = true;
            tbx4.ReadOnly = true;

            (((TextBox)Page.Master.FindControl("Content_main").
   FindControl("FormView1$dataClusterId"))).ReadOnly = true;
            (((TextBox)Page.Master.FindControl("Content_main").
               FindControl("FormView1$dataZoneId"))).ReadOnly = true;

            Button tb5 = Page.Master.FindControl("Content_main").
                   FindControl("FormView1$InsertButton") as Button;

            System.DateTime n = System.DateTime.Now;

            // Insert operation
            if (tb5.Visible == true)
            {
                tbx1.Text = n.ToString();
                tbx2.Text =(Session["LoginID"]!=null)?(String)Session["LoginID"]:"0000";
            }
            else
            {
                tbx3.Text = n.ToString();
                tbx4.Text =(Session["LoginID"]!=null)?(String)Session["LoginID"]:"0000";
            }
        } // Not postback

	}
	protected void GridViewContestant_SelectedIndexChanged(object sender, EventArgs e)
	{
		string urlParams = string.Format("ContestantId={0}", GridViewContestant.SelectedDataKey.Values[0]);
		Response.Redirect("ContestantEdit.aspx?" + urlParams, true);		
	}	
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["ChapterID"] != null)
           {
               return string.Format("ChapterID='{0}'", Request.QueryString["ChapterID"].ToString());
           }
           return string.Empty;
       }
    }

}



 