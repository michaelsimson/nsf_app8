
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="Chapter.aspx.cs" Inherits="Chapter" Title="Chapter List" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
		<data:EntityGridView ID="GridView1" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="ChapterDataSource"
			DataKeyNames="ChapterID"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Chapter.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />				
				<asp:BoundField DataField="ChapterCode" HeaderText="ChapterCode" SortExpression="ChapterCode"  />
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"  />
				<asp:BoundField DataField="State" HeaderText="State" SortExpression="State"  />
				<asp:BoundField DataField="City" HeaderText="City" SortExpression="City"  />
				<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
						<asp:Repeater ID="DataValue5" runat="server" DataSourceID="StatusFilter5">
							<ItemTemplate>
								<%# Eval("DataText") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="StatusFilter5" runat="server"
							DataSourceID="StatusDataSource5"
							Filter='<%# String.Format("DataValue = {0}", Eval("Status")) %>'
						/>
					</ItemTemplate>
				</asp:TemplateField>

				<asp:TemplateField HeaderText="ZoneCode" SortExpression="ZoneCode">
				<ItemTemplate>
						<asp:Repeater ID="ZoneCode2" runat="server" DataSourceID="ZoneFilter2">
							<ItemTemplate>
								<%# Eval("ZoneCode") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="ZoneFilter2" runat="server"
							DataSourceID="ZoneDataSource2"
							Filter='<%# String.Format("ZoneCode = {0}", Eval("ZoneCode")) %>'
						/>
					</ItemTemplate>
				</asp:TemplateField>


				<asp:TemplateField HeaderText="ClusterCode">
				<ItemTemplate>
						<asp:Repeater ID="ClusterCode4" runat="server" DataSourceID="ClusterFilter4">
							<ItemTemplate>
								<%# Eval("ClusterCode") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="ClusterFilter4" runat="server"
							DataSourceID="ClusterDataSource4"
							Filter='<%# String.Format("ClusterCode = {0}", Eval("ClusterCode")) %>'
						/>
					</ItemTemplate>
				</asp:TemplateField>


				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No Chapter Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
		<br />
		<asp:Button runat="server" ID="btnChapter" OnClientClick="javascript:location.href='ChapterEdit.aspx'; return false;" Text="Add New"></asp:Button>
			
		<data:ZoneDataSource ID="ZoneDataSource1" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ZoneDataSource ID="ZoneDataSource2" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ClusterDataSource ID="ClusterDataSource3" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ClusterDataSource ID="ClusterDataSource4" runat="server"
			SelectMethod="GetAll"
		/>

		<data:StatusDataSource ID="StatusDataSource5" runat="server"
			SelectMethod="GetByTableID"
		>
			<Parameters>
 
			<data:DataParameter Name="TableID" Type="int32" DefaultValue='5' DataSourceID="LookUpCodesDataSource1" />
			</Parameters>
       </data:StatusDataSource>

		<data:ChapterDataSource ID="ChapterDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ChapterDataSource>
	    		
</asp:Content>




 

 
 
 