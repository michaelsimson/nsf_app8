﻿<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="TestBulkChangeCoach.aspx.vb" Inherits="Admin_TestBulkChangeCoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <script type="text/javascript">

        function Validate() {

            var err = document.getElementById('_ctl0_Content_main_lblError');
            var yr = document.getElementById('_ctl0_Content_main_ddlYear');
            var prdgrp = document.getElementById('_ctl0_Content_main_ddlProductGroup');
            var prd = document.getElementById('_ctl0_Content_main_ddlProduct');
            var fromCoach = document.getElementById('_ctl0_Content_main_ddlFromCoach');
            var fromPhase = document.getElementById('_ctl0_Content_main_ddlFromPhase');
            var fromLevel = document.getElementById('_ctl0_Content_main_ddlFromLevel');
            var fromSession = document.getElementById('_ctl0_Content_main_ddlFromSession');
            var fromDay = document.getElementById('_ctl0_Content_main_ddlFromDay');
            var fromTime = document.getElementById('_ctl0_Content_main_ddlFromTime');
            var toCoach = document.getElementById('_ctl0_Content_main_ddlToCoach');

            if (prdgrp.value == '') {
                err.innerHTML = 'Select Product Group'; return false;
            }
            else if (prd.value == '') {
                err.innerHTML = 'Select Product'; return false;
            }
            else if (fromCoach.value == '') {
                err.innerHTML = 'Select From Coach'; return false;
            }
            else if (fromPhase.value == '') {
                err.innerHTML = 'Select Phase'; return false;
            }
            else if (fromLevel.value = '') {
                err.innerHTML = 'Select Level'; return false;
            }
            else if (fromSession.value == '') {
                err.innerHTML = 'Select Session'; return false;
            }
            else if (fromDay.value == '') {
                err.innerHTML = 'Select Day'; return false;
            }
            else if (fromTime.value == '') {
                err.innerHTML = 'Select Time'; return false;
            }
            else if (toCoach.value == '') {
                err.innerHTML = 'Select To Coach'; return false;
            }
            else {
                return ConfirmToCoach();
            }
        }
        function ConfirmToCoach() {
            if (confirm('Are you sure the Coach in the TO field is absolutely correct?'))
                return ConfirmSave();
            else
                return false;
        }
        function ConfirmSave() {
            return confirm('Do you want to switch?');
        }

</script>
    
     <div>
			<table id="tblLogin" border="0" cellpadding = "3" cellspacing = "0" width="900px" runat="server">
				<tr>
					<td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h1>Bulk Change for a Coach</h1>
					</td>
				</tr>
				 <tr>
			        <td colspan="2">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td> </td>
                    <td>
                         
                        <table style="width:100%;border-style:solid;border-width:1px;" cellspacing="0" cellpadding="0"  class="tableclass">
                            <tr style="text-align:center;font-weight:bold;background-color:#ffffcc;"><th style="width:25%">Role </th><th style="width:25%">Year</th><th style="width:25%">ProductGroup</th><th style="width:25%">Product</th></tr>
                            <tr style="text-align:center;font-weight:bold"><td><asp:Label ID="lblRoleName" runat="server" Text=""></asp:Label></td>
                                <td><asp:DropDownList ID="ddlYear" runat="server" Width="75px" AutoPostBack="True"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlProductGroup" runat="server" DataTextField="ProductGroupCode" DataValueField ="ProductGroupId" AutoPostBack="True" Width="150px"></asp:DropDownList></td>
                                 <td><asp:DropDownList ID="ddlProduct" runat="server" DataTextField="ProductCode" DataValueField ="ProductId" Width="150px" AutoPostBack="True"></asp:DropDownList></td>
                            </tr>
                            <tr><td></td></tr>
                            </table>

                        <br />
                        <div style="width:100%; text-align:center;height:30px">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                            <asp:Label ID="lblMsg" runat="server" ForeColor="Blue"></asp:Label>
                        </div>
                        
                        <div>
                        <table width="100%" class="tableclass" cellspacing="0" cellpadding="0">
                            <tr style="text-align:center;background-color:#ffffcc;"> <th></th><th>Coach</th><th>Phase</th><th>Level</th><th>Session</th><th>Day</th><th>Time</th></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr style="text-align:center"> <th style="text-align:left">From</th>
                                <td><asp:DropDownList ID="ddlFromCoach" runat="server" AutoPostBack="True" DataTextField="CoachName" DataValueField ="CMemberId" Width="250px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlFromPhase" runat="server" DataTextField="Phase" DataValueField ="Phase" AutoPostBack="True" Width="50px"></asp:DropDownList></td>
                                 <td><asp:DropDownList ID="ddlFromLevel" runat="server" DataTextField="Level" DataValueField ="Level" AutoPostBack="True" Width="150px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlFromSession" runat="server" DataTextField="SessionNo" DataValueField ="SessionNo" AutoPostBack="True" Width="50px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlFromDay" runat="server" DataTextField="Day" DataValueField ="Day" AutoPostBack="True" Width="100px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlFromTime" runat="server" DataTextField="Time" DataValueField ="Time" Width="75px" AutoPostBack="True"></asp:DropDownList></td></tr>
                            <tr><td>&nbsp;</td></tr>
                            <tr style="text-align:center"> <th style="text-align:left">To</th>
                                <td><asp:DropDownList ID="ddlToCoach" runat="server" DataTextField="CoachName" DataValueField ="MemberId" Width="250px"></asp:DropDownList></td>
                                <td><asp:Label ID="lblToPhase" runat="server" Width="50px" style="border:medium"></asp:Label></td>
                                <td><asp:Label ID="lblToLevel" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblToSessionNo" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblToDay" runat="server"></asp:Label></td>
                                <td><asp:Label ID="lblToTime" runat="server"></asp:Label></td></tr>
                               <%-- <td><asp:DropDownList ID="ddlToPhase" runat="server" DataTextField="Phase" DataValueField ="Phase" Width="50px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToLevel" runat="server" DataTextField="Level" DataValueField ="Level" Width="150px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToSession" runat="server" DataTextField="Session" DataValueField ="Session" Width="50px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToDay" runat="server" DataTextField="Day" DataValueField ="MemberId" Width="100px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToTime" runat="server" DataTextField="Time" DataValueField ="MemberId" Width="75px"></asp:DropDownList></td>--%>
                        </table>
                            </div>

                    </td>
			    </tr>
                <tr>
                    
                    <td colspan="2" align="center">
                          <br />
<asp:Button ID="btnUpdate" runat="server" Text="Switch All Students"  OnClientClick="return Validate();" />
                    </td>
                </tr>
						</table>
					
		</div>
</asp:Content>