using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using nsf.Web.UI;

public partial class Admin_VolunteerAdd : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       // FormUtil.SetDefaultMode(FormView1,null);
        if (Convert.ToInt32(Session["RoleId"]) == 4)
        {
            lblMessage.Text = "A cluster coordinator is not allowed to assign any roles!";
            DetailsView1.Enabled = false;
        }
    }
    /*
    protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
            FormViewRow row = FormView1.Row;
            TextBox tbMemberId = (TextBox) row.FindControl("dataMemberId");
        DropDownList ddlRoleCode = (DropDownList) row.FindControl("dataRoleCode");
        DropDownList ddlTeamLead = (DropDownList)row.FindControl("dataTeamLead");
        TextBox tbEventYear = (TextBox) row.FindControl("dataEventYear");
        TextBox tbEventCode = (TextBox) row.FindControl("dataEventCode");
        TextBox tbChapterCode = (TextBox) row.FindControl("dataChapterCode");
        TextBox tbZoneCode = (TextBox) row.FindControl("dataZoneCode");
        TextBox tbClusterCode = (TextBox)row.FindControl("dataClusterCode");
        DropDownList ddlNational = (DropDownList) row.FindControl("dataNational");
        DropDownList ddlFinals = (DropDownList)row.FindControl("dataFinals");
        TextBox tbIndiaChapter = (TextBox)row.FindControl("dataIndiaChapter");
        TextBox tbProductGroupCode = (TextBox)row.FindControl("dataProductGroupCode");
        TextBox tbProductCode = (TextBox)row.FindControl("dataProductCode");
        DataTable dt = new DataTable();
        VolunteerCheckTableAdapters.VolunteerTableAdapter vta = new VolunteerCheckTableAdapters.VolunteerTableAdapter();
        dt = vta.GetData(Convert.ToInt32(tbMemberId.Text), ddlRoleCode.SelectedValue,
            ddlTeamLead.SelectedValue, Convert.ToInt32(tbEventYear.Text), tbEventCode.Text,
            tbChapterCode.Text, tbZoneCode.Text, tbClusterCode.Text, ddlNational.SelectedValue,
            ddlFinals.SelectedValue, tbIndiaChapter.Text, tbProductGroupCode.Text,
            tbProductCode.Text);
        if(dt.Rows.Count>0)
              Response.Redirect("VolunteerEdit.aspx?Id="+tbMemberId.Text.Trim());
        //Response.Redirect("Volunteer.aspx");
    }
     * */
    protected void DetailsView1_PreRender(object sender, EventArgs e)
    {
        if (DetailsView1.CurrentMode == DetailsViewMode.Insert)
        {
            ((TextBox)DetailsView1.FindControl("TextBox1")).Text = Session["MemberId"].ToString();
            if (Session["RoleId"] != null && Convert.ToInt32(Session["RoleId"]) == 1)
            {
                ((DropDownList)DetailsView1.FindControl("ddlRoleId")).Items.Insert(0, new ListItem("1", "1"));
                ((DropDownList)DetailsView1.FindControl("ddlRoleCode")).Items.Insert(0, new ListItem("Admin", "Admin"));
            }
            if (Session["RoleId"] != null && Convert.ToInt32(Session["RoleId"]) != 1)
            {
                ((DropDownList)DetailsView1.FindControl("ddlAgent")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlWrite")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlAuth")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlAgent")).SelectedIndex = 2;
                ((DropDownList)DetailsView1.FindControl("ddlWrite")).SelectedIndex = 2;
                ((DropDownList)DetailsView1.FindControl("ddlAuth")).SelectedIndex = 2;
            }
            if (this.IsPostBack == true)
            {
                DataTable dt = new DataTable();
                RoleGetByCodeTableAdapters.RoleTableAdapter rt = new RoleGetByCodeTableAdapters.RoleTableAdapter();
                dt = rt.GetData(((DropDownList)DetailsView1.FindControl("ddlRoleCode")).Text);
                if (dt.Rows[0].ItemArray[2].ToString().Trim() != "Y")
                {
                    ((DropDownList)DetailsView1.FindControl("ddlNational")).Enabled = false;
                    ((DropDownList)DetailsView1.FindControl("ddlNational")).SelectedIndex = 2;
                }


            }
        }
    }
    protected void CompleteTasks(object sender, DetailsViewInsertEventArgs e)
    {
        if (DetailsView1.CurrentMode == DetailsViewMode.Insert)
        {
            int ddlRoleIndex = ((DropDownList)DetailsView1.FindControl("ddlRoleCode")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlRoleId")).SelectedIndex = ddlRoleIndex;

            int ddlEventIndex = ((DropDownList)DetailsView1.FindControl("ddlEvent")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlEventId")).SelectedIndex = ddlEventIndex;
            ((DropDownList)DetailsView1.FindControl("ddlEventYear")).SelectedIndex = ddlEventIndex;

            int ddlChapterIndex = ((DropDownList)DetailsView1.FindControl("ddlChapter")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlChapterId")).SelectedIndex = ddlChapterIndex;

            int ddlZoneIndex = ((DropDownList)DetailsView1.FindControl("ddlZone")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlZoneId")).SelectedIndex = ddlZoneIndex;

            int ddlClusterIndex = ((DropDownList)DetailsView1.FindControl("ddlCluster")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlClusterId")).SelectedIndex = ddlClusterIndex;

            int ddlProdGroupIndex = ((DropDownList)DetailsView1.FindControl("ddlProdGroup")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlProductGroupID")).SelectedIndex = ddlProdGroupIndex;

            int ddlProdIndex = ((DropDownList)DetailsView1.FindControl("ddlProduct")).SelectedIndex;
            ((DropDownList)DetailsView1.FindControl("ddlProductId")).SelectedIndex = ddlProdIndex;

        }
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        lblMessage.Text = "Volunteer Role Added!";
    }

    void CheckForExistingVolunteer()
    {
        /*
         if (DetailsView1.CurrentMode == DetailsViewMode.Insert)
         {
             TextBox tbMemberId = (TextBox)DetailsView1.FindControl("TextBox1");
             DropDownList ddRoleCode = (DropDownList)DetailsView1.FindControl("ddlRoleCode");
             DropDownList dlTeamLead = (DropDownList)DetailsView1.FindControl("ddlTeamLead");
             DropDownList dlEventYear = (TextBox)DetailsView1.FindControl("ddlEventYear");
             DropDownList dlEventCode = (TextBox)DetailsView1.FindControl("dataEventCode");
             DropDownList dlChapterCode = (TextBox)DetailsView1.FindControl("dataChapterCode");
             DropDownList dlZoneCode = (TextBox)DetailsView1.FindControl("dataZoneCode");
             DropDownList dlClusterCode = (TextBox)DetailsView1.FindControl("dataClusterCode");
             DropDownList dlNational = (DropDownList)DetailsView1.FindControl("dataNational");
             DropDownList dlFinals = (DropDownList)DetailsView1.FindControl("dataFinals");
             DropDownList dlIndiaChapter = (TextBox)DetailsView1.FindControl("dataIndiaChapter");
             DropDownList dlProductGroupCode = (TextBox)DetailsView1.FindControl("dataProductGroupCode");
             DropDownList dlProductCode = (TextBox)DetailsView1.FindControl("dataProductCode");
             DataTable dt = new DataTable();
             VolunteerCheckTableAdapters.VolunteerTableAdapter vta = new VolunteerCheckTableAdapters.VolunteerTableAdapter();
             dt = vta.GetData(Convert.ToInt32(tbMemberId.Text), ddlRoleCode.SelectedValue,
                 ddlTeamLead.SelectedValue, Convert.ToInt32(tbEventYear.Text), tbEventCode.Text,
                 tbChapterCode.Text, tbZoneCode.Text, tbClusterCode.Text, ddlNational.SelectedValue,
                 ddlFinals.SelectedValue, tbIndiaChapter.Text, tbProductGroupCode.Text,
                 tbProductCode.Text);
             if (dt.Rows.Count > 0)
                 Response.Redirect("VolunteerEdit.aspx?Id=" + tbMemberId.Text.Trim());
             //Response.Redirect("Volunteer.aspx");
         }*/
    }
}

 