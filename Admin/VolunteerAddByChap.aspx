<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VolunteerAddByChap.aspx.cs" Inherits="Admin_VolunteerAddByChap" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataKeyNames="VolunteerID"
        DataSourceID="VolAddDS" Height="30%" Width="50%" DefaultMode="Insert" OnPreRender="DetailsView1_PreRender" CellPadding="4" ForeColor="#333333" GridLines="None" OnItemInserted="DetailsView1_ItemInserted" OnItemInserting="DetailsView1_ItemInserting" OnItemUpdated="DetailsView1_ItemUpdated" Caption="Add New Role">
        <Fields>
            <asp:TemplateField HeaderText="Name">
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox23" runat="server" ReadOnly="True" Text="<%# GetName() %>"></asp:TextBox>
                </InsertItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="VolunteerId" HeaderText="VolunteerId" InsertVisible="False"
                ReadOnly="True" SortExpression="VolunteerId" />
            <asp:TemplateField HeaderText="MemberId" SortExpression="MemberId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("MemberId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" ReadOnly="True" Text='<%# Bind("MemberId") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text="Label"></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RoleId" SortExpression="RoleId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox16" runat="server" Text='<%# Bind("RoleId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlRoleId" runat="server" SelectedValue='<%# Bind("RoleId") %>' DataSourceID="RoleDS" DataTextField="RoleId" DataValueField="RoleId" Enabled="false" AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="RoleDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByChap" TypeName="RolesGreaterTableAdapters.RoleTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="RoleId" SessionField="RoleId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Bind("RoleId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RoleCode" SortExpression="RoleCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("RoleCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlRoleCode" runat="server" DataSourceID="RoleT" DataTextField="RoleCode"
                        DataValueField="RoleCode" SelectedValue='<%# Bind("RoleCode") %>' AutoPostBack="True" AppendDataBoundItems="True">
                        <asp:ListItem Selected="True">Select Role</asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="RoleT" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetDataByChap" TypeName="RolesGreaterTableAdapters.RoleTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="RoleId" SessionField="RoleId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("RoleCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TeamLead" SortExpression="TeamLead">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox3" runat="server" Text='<%# Bind("TeamLead") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlTeamLead" runat="server" SelectedValue='<%# Bind("TeamLead") %>'>
                        <asp:ListItem >Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("TeamLead") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventYear" SortExpression="EventYear" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox17" runat="server" Text='<%# Bind("EventYear") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    &nbsp;<asp:TextBox ID="tbYear" runat="server" Text='<%# Bind("EventYear") %>'></asp:TextBox>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label17" runat="server" Text='<%# Bind("EventYear") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventId" SortExpression="EventId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox4" runat="server" Text='<%# Bind("EventId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlEventId" runat="server" SelectedValue='<%# Bind("EventId") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("EventId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventCode" SortExpression="EventCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox5" runat="server" Text='<%# Bind("EventCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlEvent" runat="server" SelectedValue='<%# Bind("EventCode") %>' AppendDataBoundItems="True" AutoPostBack="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>Finals</asp:ListItem>
                        <asp:ListItem>Chapter</asp:ListItem>
                        <asp:ListItem>WkShop</asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("EventCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ChapterId" SortExpression="ChapterId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox18" runat="server" Text='<%# Bind("ChapterId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlChapterId" runat="server" DataSourceID="ChapterDS" DataTextField="ChapterID"
                        DataValueField="ChapterID" SelectedValue='<%# Bind("ChapterId") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ChapterDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label18" runat="server" Text='<%# Bind("ChapterId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ChapterCode" SortExpression="ChapterCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox6" runat="server" Text='<%# Bind("ChapterCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlChapter" runat="server" DataSourceID="Chapter" DataTextField="ChapterCode"
                        DataValueField="ChapterCode" SelectedValue='<%# Bind("ChapterCode") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="Chapter" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("ChapterCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="National" SortExpression="National" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox7" runat="server" Text='<%# Bind("National") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlNational" runat="server">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("National") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZoneId" SortExpression="ZoneId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox19" runat="server" Text='<%# Bind("ZoneId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlZoneId" runat="server" DataSourceID="ZoneDs" DataTextField="ZoneId"
                        DataValueField="ZoneId" SelectedValue='<%# Bind("ZoneId") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ZoneDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label19" runat="server" Text='<%# Bind("ZoneId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZoneCode" SortExpression="ZoneCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox8" runat="server" Text='<%# Bind("ZoneCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlZone" runat="server" DataSourceID="Zoneds2" DataTextField="ZoneCode"
                        DataValueField="ZoneCode" SelectedValue='<%# Bind("ZoneCode") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="Zoneds2" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("ZoneCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ClusterId" SortExpression="ClusterId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox20" runat="server" Text='<%# Bind("ClusterId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlClusterId" runat="server" DataSourceID="ClusterDs" DataTextField="ClusterId"
                        DataValueField="ClusterId" SelectedValue='<%# Bind("ClusterId") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ClusterDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label20" runat="server" Text='<%# Bind("ClusterId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ClusterCode" SortExpression="ClusterCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox9" runat="server" Text='<%# Bind("ClusterCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlCluster" runat="server" DataSourceID="ClusterDs2" DataTextField="ClusterCode"
                        DataValueField="ClusterCode" SelectedValue='<%# Bind("ClusterCode") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ClusterDs2" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("ClusterCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Finals" SortExpression="Finals" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox10" runat="server" Text='<%# Bind("Finals") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlFinals" runat="server">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("Finals") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="IndiaChapter" HeaderText="IndiaChapter" SortExpression="IndiaChapter" Visible="False" />
            <asp:TemplateField HeaderText="ProductGroupID" SortExpression="ProductGroupID" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox21" runat="server" Text='<%# Bind("ProductGroupID") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlProductGroupID" runat="server" SelectedValue='<%# Bind("ProductGroupID") %>' AppendDataBoundItems="True" Enabled="False">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdGroupDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProdgroupTableAdapters.ProductGroupTableAdapter">
                    </asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label21" runat="server" Text='<%# Bind("ProductGroupID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox11" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlProdGroup" runat="server" SelectedValue='<%# Bind("ProductGroupCode") %>' AppendDataBoundItems="True" AutoPostBack="True">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductId" SortExpression="ProductId" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox22" runat="server" Text='<%# Bind("ProductId") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlProductId" runat="server" SelectedValue='<%# Bind("ProductId") %>' AppendDataBoundItems="True" Enabled="False">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProductTableAdapters.ProductTableAdapter"></asp:ObjectDataSource>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label22" runat="server" Text='<%# Bind("ProductId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductCode" SortExpression="ProductCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox12" runat="server" Text='<%# Bind("ProductCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlProduct" runat="server" SelectedValue='<%# Bind("ProductCode") %>' 
                    AppendDataBoundItems="True">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="AgentFlag" SortExpression="AgentFlag" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox13" runat="server" Text='<%# Bind("AgentFlag") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlAgent" runat="server">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("AgentFlag") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="WriteAccess" SortExpression="WriteAccess" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox14" runat="server" Text='<%# Bind("WriteAccess") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlWrite" runat="server">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Bind("WriteAccess") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Authorization" SortExpression="Authorization" Visible="False">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox15" runat="server" Text='<%# Bind("Authorization") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="ddlAuth" runat="server">
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label15" runat="server" Text='<%# Bind("Authorization") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
        <FooterStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <CommandRowStyle BackColor="#C5BBAF" Font-Bold="True" />
        <EditRowStyle BackColor="#7C6F57" />
        <RowStyle BackColor="#E3EAEB" />
        <PagerStyle BackColor="#666666" ForeColor="White" HorizontalAlign="Center" />
        <FieldHeaderStyle BackColor="#D0D0D0" Font-Bold="True" />
        <HeaderStyle BackColor="#1C5E55" Font-Bold="True" ForeColor="White" />
        <AlternatingRowStyle BackColor="White" />
    </asp:DetailsView>
    <asp:ObjectDataSource ID="VolAddDS" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="VolSaveTableAdapters.VolunteerTableAdapter"
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    &nbsp;
    <asp:Label ID="lblMessage" runat="server" Style="z-index: 102; left: 471px; position: absolute;
        top: 156px"></asp:Label>
    <asp:HyperLink ID="hlinkGoBack" runat="server" NavigateUrl="~/ChapVolunteerRoles.aspx"
        Style="z-index: 104; left: 192px; position: absolute; top: 475px">Go back to Menu</asp:HyperLink>
</asp:Content>


 

 
 
 