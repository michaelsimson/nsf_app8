using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;
using Microsoft.ApplicationBlocks.Data;


public partial class Admin_VolunteerResults : System.Web.UI.Page
{
    string sSQL, sSQLORG;
    //Data.SqlClient.SqlDataAdapter dataAdapterInd, dataAdapterOrg;
    DataSet dsIndSpouse = new DataSet();
    
    protected void Page_Load(object sender, EventArgs e)
    {
        sSQL = Session["sSQL"].ToString();
        sSQLORG = Session["sSQLORG"].ToString();
        NorthSouth.DAL.IndSpouseDAL indDal = new NorthSouth.DAL.IndSpouseDAL(WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString());
        indDal.SearchIndSpouseWhere(dsIndSpouse, sSQL);
      
  //      dsIndSpouse = SqlHelper.ExecuteDataset(WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString(), CommandType.StoredProcedure, "IndSpouse_Find");
        
        if (dsIndSpouse.Tables[0].Rows.Count > 0)
        {
            gvIndSpouse.DataSource = dsIndSpouse;
            gvIndSpouse.DataBind();
            //string[] keyColumn = dsIndSpouse.Tables[0].Columns["MemberID"];
            gvIndSpouse.DataKeyNames = new string[] { "automemberID" };
        }
        else lblMessage.Text = "No results matching your critiria found!";
    }
    protected void gvIndSpouse_SelectedIndexChanged(object sender, EventArgs e)
    {
        string memberQuery = gvIndSpouse.DataKeys.ToString();//Request.QueryString["Id"];
        string finalQuery = memberQuery;
        if (memberQuery.IndexOf("IND") != -1)
            finalQuery = finalQuery + " OR SPOUSE" + memberQuery.Substring(3, memberQuery.Length - 4);
        else if (memberQuery.IndexOf("SPOUSE") != -1)
            finalQuery = finalQuery + " OR IND" + memberQuery.Substring(6, memberQuery.Length - 7);
        Session["MemberIdQuery"] = finalQuery;
        Response.Redirect("VolunteerShow.aspx");
    }
    protected void gvIndSpouse_RowCommand(object sender, GridViewCommandEventArgs e)
    {
      //  string memberQuery = gvIndSpouse.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString();
      //  string spouseQuery="";
      //  if (memberQuery.IndexOf("IND") != -1)
       //     memberQuery = memberQuery.Substring(3, memberQuery.Length - 3);
       // else if (memberQuery.IndexOf("SPOUSE") != -1)
       //     memberQuery = memberQuery.Substring(6, memberQuery.Length - 6) ;
       // Session["MemberIdQuery"] = " MemberID Like '%" + memberQuery +"'" ;
       // Session["SpouseIdQuery"] = spouseQuery;
       // Response.Redirect("VolunteerShow.aspx");
        Session["MemberId"] = gvIndSpouse.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString();
        Response.Redirect("VolunteerAddByChap.aspx?MemberID="+Session["MemberId"]);
    }
    protected void ViewData(object sender, GridViewCommandEventArgs e)
    {
        Session["MemberId"] = gvIndSpouse.DataKeys[Convert.ToInt32(e.CommandArgument)].Value.ToString();
        Response.Redirect("VolunteerEdit.aspx");
    }
}

 