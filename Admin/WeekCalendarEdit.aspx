
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="WeekCalendarEdit.aspx.cs" Inherits="WeekCalendarEdit" Title="WeekCalendar Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<data:MultiFormView ID="FormView1" DataKeyNames="WeekId" runat="server" DataSourceID="WeekCalendarDataSource">
		
			<EditItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/WeekCalendarFields.ascx" />
			</EditItemTemplatePaths>
		
			<InsertItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/WeekCalendarFields.ascx" />
			</InsertItemTemplatePaths>
		
			<EmptyDataTemplate>
				<b>WeekCalendar not found!</b>
			</EmptyDataTemplate>
			
			<FooterTemplate>
				<asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
				<asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
				<asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
			</FooterTemplate>

		</data:MultiFormView>
		
		<data:WeekCalendarDataSource ID="WeekCalendarDataSource" runat="server"
			SelectMethod="GetByWeekId"
		>
			<Parameters>
				<asp:QueryStringParameter Name="WeekId" QueryStringField="WeekId" Type="String" />

			</Parameters>
		</data:WeekCalendarDataSource>
		
		<br />

		

</asp:Content>


 

 
 
 