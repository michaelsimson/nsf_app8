
#region Imports...
using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Web;

using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using nsf.Web.UI;
#endregion

public partial class ClusterEdit : System.Web.UI.Page
{

    protected void IsDuplicate(object source, ServerValidateEventArgs value)
    {

        // check if update operation
        if (((Button)(Page.Master.FindControl("Content_main").
                     FindControl("FormView1$UpdateButton"))).Visible == true)
        {
            value.IsValid = true;
            return;
        }

        // Declare database objects such as connection,
        // command and transaction
        string clusterCode = value.Value;
        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ClusterCode from Cluster where ClusterCode ='");
        queryStr.Append(clusterCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        //string connectionString = "Data Source=anu-lt\\northsouth2;Initial Catalog=nsfdev;Integrated Security=true;Connection Timeout=1;";
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                /* if there is an entry, then there is a duplicate */
                /* So make the isValid false */
                value.IsValid = (reader.Read()) ? false : true;
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{		
		FormUtil.RedirectAfterInsertUpdate(FormView1, "ClusterEdit.aspx?{0}", ClusterDataSource);
		FormUtil.RedirectAfterAddNew(FormView1, "ClusterEdit.aspx");
		FormUtil.RedirectAfterCancel(FormView1, "Cluster.aspx");
		FormUtil.SetDefaultMode(FormView1, "ClusterId");

        (((CustomValidator)Page.Master.FindControl("Content_main").
 FindControl("FormView1$ClusterCodeValidator"))).ServerValidate +=
              new ServerValidateEventHandler(this.IsDuplicate);

        if (!Page.IsPostBack)
        {

            TextBox tbx1 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreateDate") as TextBox;
            TextBox tbx2 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreatedBy") as TextBox;
            TextBox tbx3 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifyDate") as TextBox;
            TextBox tbx4 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifiedBy") as TextBox;

            tbx1.ReadOnly = true;
            tbx2.ReadOnly = true;
            tbx3.ReadOnly = true;
            tbx4.ReadOnly = true;

            Button tb5 = Page.Master.FindControl("Content_main").
                   FindControl("FormView1$InsertButton") as Button;


            System.DateTime n = System.DateTime.Now;
            // Insert operation
            if (tb5.Visible == true)
            {
                tbx1.Text = n.ToString();
                tbx2.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
            else
            {
                tbx3.Text = n.ToString();
                tbx4.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }

        } // Not postback


	}
	protected void GridViewChapter_SelectedIndexChanged(object sender, EventArgs e)
	{
		string urlParams = string.Format("ChapterID={0}", GridViewChapter.SelectedDataKey.Values[0]);
		Response.Redirect("ChapterEdit.aspx?" + urlParams, true);		
	}	
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["ClusterId"] != null)
           {
               return string.Format("ClusterId='{0}'", Request.QueryString["ClusterId"].ToString());
           }
           return string.Empty;
       }
    }

}



 