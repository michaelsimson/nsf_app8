﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml
Imports Newtonsoft.Json.Linq
Imports Newtonsoft.Json
Imports System.Web.Script.Serialization
Imports NativeExcel
Imports VRegistration

Partial Class Admin_BulkChangeCoach
    Inherits System.Web.UI.Page
    Public apiKey As String = "6sTMyAgRSpCmTSIJIWFP8w"
    Public apiSecret As String = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1"
        'Session("LoginID") = "4240"

        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)

            ddlYear.Items.Add(New ListItem(Convert.ToString(year - 1) + "-" + Convert.ToString(year).ToString().Substring(2, 2), Convert.ToString(year - 1)))
            ddlYear.Items.Add(New ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).ToString().Substring(2, 2), Convert.ToString(year)))
            ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(eventyear) from EventFees where EventId=13")

            loadSemester()
            ' ddlYear.SelectedValue = "2015"
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                hlnkMainMenu.Text = "Back to Parent Functions"
                hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx"
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("../login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                GetRoleName()
                FillMasterDetails()
            Else
                Response.Redirect("../maintest.aspx")
            End If
            ddlYear.Enabled = True
            ddlProductGroup.Enabled = True



            Dim CoachChange As String = "Y"
            Dim SessionCreationDate As String = Nothing
            Dim cmdText As String = String.Empty
            Dim dsCH As DataSet
            Dim dtSessionCreationDate As New DateTime()
            Dim dtTodayDate As New DateTime()

            'Local
            'Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
            'dtTodayDate = Convert.ToDateTime(CurrDate).ToString("dd/MM/yyyy")

            'Serevr
            'Dim CurrDate As String = DateTime.Today.ToString("MM/dd/yyyy")
            'dtTodayDate = Convert.ToDateTime(CurrDate).ToString("MM/dd/yyyy")
            ''  ddlYear.SelectedValue = "2015"
            'cmdText = "select CoachChange,SessionCreationDate from WebConfControl where EventID=13 and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & ""
            'dsCH = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            'If dsCH.Tables.Count > 0 Then
            '    ' Condition to check if dataset tables contains data or not
            '    If dsCH.Tables(0).Rows.Count > 0 Then
            '        CoachChange = dsCH.Tables(0).Rows(0)("CoachChange").ToString()
            '        SessionCreationDate = dsCH.Tables(0).Rows(0)("SessionCreationDate").ToString()
            '        ' dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("dd/MM/yyyy")
            '        dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("MM/dd/yyyy")
            '    End If
            'Else

            'End If
            'If dsCH.Tables(0).Rows.Count > 0 Then

            '    CoachChange = "Y"
            '    If dtTodayDate >= dtSessionCreationDate And CoachChange.Trim = "N" Then
            '        btnUpdate.Enabled = False
            '        lblError.Text = "System is under maintenance.  Change cannot be executed for next one hour."

            '    Else
            '        btnUpdate.Enabled = True
            '    End If
            'Else
            '    btnUpdate.Enabled = True
            'End If
        End If
    End Sub

    Private Sub FillMasterDetails()

        Try
            ddlProductGroup.Items.Clear()
            ddlProduct.Items.Clear()
            ddlFromCoach.Items.Clear()
            ddlProductGroup.Enabled = False
            ddlProduct.Enabled = False
            ddlFromCoach.Enabled = False
            ClearCoachDetails()
            FillProductGroup()
            FillProduct()
            If ddlProductGroup.Items.Count <> 0 Then
                ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
            End If
            'FillProduct()
            'FillFromDetails()
            'FillFromPhaseDetails()
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Private Sub FillFromDetails()
        Dim CmdText As String = String.Empty
        CmdText = "select distinct CR.CMemberId as CMemberId,I.FirstName + ' ' + I.LastName as CoachName,I.firstname as firstname,I.lastname as lastname from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and C.Accepted='Y' order by firstname,lastname" 'C.Userid is not null and C.pwd is not null
        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)

        If dsCoach.Tables(0).Rows.Count > 0 Then

            ddlFromCoach.DataSource = dsCoach.Tables(0)
            ddlFromCoach.DataBind()
            ddlFromCoach.Items.Insert(0, New ListItem("Select", "0"))
            If ddlFromCoach.Items.Count > 0 Then
                ddlFromCoach.Items(0).Selected = True
                If ddlFromCoach.Items.Count = 1 Then
                    ddlFromCoach.Enabled = False
                Else
                    ddlFromCoach.Enabled = True
                End If

            End If

        End If
    End Sub

    Private Sub FillFromPhaseDetails()
        Dim dsPhase As DataSet
        dsPhase = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.Semester from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.Semester='" & DDlSemester.SelectedValue & "' and CR.CMemberId= " & ddlFromCoach.SelectedValue)
        ddlFromPhase.DataSource = dsPhase.Tables(0)
        ddlFromPhase.DataBind()
        If ddlFromPhase.Items.Count > 0 Then
            ddlFromPhase.Items(0).Selected = True
            ddlFromPhase_SelectedIndexChanged(ddlFromPhase, New EventArgs)
            If ddlFromPhase.Items.Count = 1 Then
                ddlFromPhase.Enabled = False
            Else
                ddlFromPhase.Enabled = True
            End If
        End If

    End Sub
    Private Sub FillFromLevelDetails()
        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.Level from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.CMemberId=" & ddlFromCoach.SelectedValue & " and CR.Semester = '" & ddlFromPhase.SelectedValue & "'")
        ddlFromLevel.DataSource = dsCoach.Tables(0)
        ddlFromLevel.DataBind()
        ddlFromLevel.Enabled = False
        If dsCoach.Tables(0).Rows.Count > 0 Then
            If ddlFromLevel.Items.Count > 0 Then
                ddlFromLevel.Items(0).Selected = True
                If ddlFromLevel.Items.Count > 1 Then
                    ddlFromLevel.Enabled = True
                End If
            End If
        End If

    End Sub

    Private Sub FillFromSessionDetails()
        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.SessionNo  from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.CMemberId=" & ddlFromCoach.SelectedValue)
        ddlFromSession.DataSource = dsCoach.Tables(0)
        ddlFromSession.DataBind()
        ddlFromSession.Enabled = False
        If ddlFromSession.Items.Count > 0 Then
            ddlFromSession.Items(0).Selected = True

            If ddlFromSession.Items.Count > 1 Then
                ddlFromSession.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillFromDay()
        Dim dsDay As DataSet
        dsDay = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct C.Day from calsignup C where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.MemberId=" & ddlFromCoach.SelectedValue & " and C.Semester ='" & ddlFromPhase.SelectedValue & "' and C.Level='" & ddlFromLevel.SelectedValue & "' and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Accepted='Y' ")
        ddlFromDay.DataSource = dsDay.Tables(0)
        ddlFromDay.DataBind()
        ddlFromDay.Enabled = False
        If ddlFromDay.Items.Count > 0 Then
            ddlFromDay.Items(0).Selected = True

            If ddlFromDay.Items.Count > 1 Then
                ddlFromDay.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillFromTime()
        Dim dsTime As DataSet
        dsTime = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct C.Time from calsignup C where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.MemberId=" & ddlFromCoach.SelectedValue & " and C.Semester ='" & ddlFromPhase.SelectedValue & "' and C.Level='" & ddlFromLevel.SelectedValue & "' and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Accepted='Y' and C.Day='" & ddlFromDay.SelectedValue & "' ")
        ddlFromTime.DataSource = dsTime.Tables(0)
        ddlFromTime.DataBind()
        ddlFromTime.Enabled = False
        If ddlFromTime.Items.Count > 0 Then
            ddlFromTime.Items(0).Selected = True
            If ddlFromTime.Items.Count > 1 Then
                ddlFromTime.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillToDetails()

        Dim dsCoach As DataSet
        Dim cmdtext As String = "select distinct C.MemberId,I.FirstName + ' ' + I.LastName as CoachName,I.firstname as firstname,I.lastname as lastname,C.SignUpId as SignUpId, C.SessionNo from CalSignUp C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.Semester='" & ddlFromPhase.SelectedValue & "' and C.Level= '" & ddlFromLevel.SelectedValue & "' and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time = '" & ddlFromTime.SelectedValue & "' and C.Accepted='Y' and C.MemberId not in (" & ddlFromCoach.SelectedValue & ") order by lastname,firstname"

        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdtext)
        ddlToCoach.DataSource = dsCoach.Tables(0)
        ddlToCoach.DataBind()
        ddlToCoach.Enabled = False

        If ddlToCoach.Items.Count > 0 Then
            ddlToCoach.Items(0).Selected = True
            If ddlToCoach.Items.Count > 1 Then
                ddlToCoach.Enabled = True
            Else
                lblToSessionNo.Text = dsCoach.Tables(0).Rows(0)("SessionNo").ToString()
                FillToCoachStudents()
            End If
            lblToPhase.Text = ddlFromPhase.SelectedValue
            lblToLevel.Text = ddlFromLevel.SelectedValue

            lblToDay.Text = ddlFromDay.SelectedValue
            lblToTime.Text = ddlFromTime.SelectedValue

        End If

    End Sub



    Private Sub GetRoleName()
        Try
            Dim RoleCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select RoleCode from Volunteer Where RoleId=" & Session("RoleId") & " and MemberId= " & Session("LoginID"))
            If RoleCode <> "" Then
                lblRoleName.Text = RoleCode
            Else
                lblRoleName.Text = ""
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    'Get Product Group details
    Private Sub FillProductGroup()

        Dim Cmdtext As String = String.Empty

        Dim dsPrdGroup As DataSet
        If Session("RoleId").ToString() = "89" Or (Session("RoleId").ToString() = "88") Then
            Cmdtext = "select distinct V.ProductGroupID, V.ProductGroupCode from volunteer V inner join CalSignup Cs on (V.ProductGroupID=Cs.ProductGroupID) where V.Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and V.ProductGroupID in (select distinct(ProductGroupId) from EventFees where VEventId=13 and EventYear=" & ddlYear.SelectedValue & ") and CS.Semester='" & DDlSemester.SelectedValue & "'"
            dsPrdGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, Cmdtext) ' and EventYear =" & ddlYear.SelectedValue)

        Else
            Cmdtext = "select distinct P.ProductGroupId,P.ProductGroupCode from ProductGroup P inner join calsignup Cs on (P.ProductGroupID=Cs.ProductGroupID) where P.EventID=13 and P.ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=13 and EventYear=" & ddlYear.SelectedValue & ") and Cs.Semester='" & DDlSemester.SelectedValue & "'"
            dsPrdGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, Cmdtext)
            'dsPrdGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductGroupID, ProductGroupCode from volunteer where Eventid=13 and ProductId is not Null and EventYear =" & ddlYear.SelectedValue)
        End If

        ddlProductGroup.DataSource = dsPrdGroup.Tables(0)
        ddlProductGroup.DataBind()

        lblError.Text = ""
        ddlProductGroup.Enabled = False
        ddlProductGroup.Items.Insert(0, "Select")
        If ddlProductGroup.Items.Count > 0 Then
            ddlProductGroup.Items(0).Selected = True
            If ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Enabled = True
            End If
        Else
            lblError.ForeColor = Color.Red
            lblError.Text = "No Eligible Coacher Available"
        End If

    End Sub

    'Get Product Details for the selected product group
    Private Sub FillProduct()
        Try
            Dim CmdText As String = String.Empty
            Dim dsPrd As DataSet
            If Session("RoleId").ToString() = "89" Or (Session("RoleId").ToString() = "88") Then
                CmdText = " select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and EventYear =" & ddlYear.SelectedValue & " and ProductID in (select distinct(ProductId) from EventFees where EventId=13 and EventYear=" & ddlYear.SelectedValue & ""

                If (ddlProductGroup.SelectedValue <> "Select") Then
                    CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                End If

                CmdText = CmdText & ")"

                If (ddlProductGroup.SelectedValue <> "Select") Then
                    CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                End If


                ' Added by Bindhu on Aug26_2016 to allow coach admin to have access with all products of assigned productgroup
                If Session("RoleId").ToString() = "89" Then
                    CmdText = "select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and EventYear =" & ddlYear.SelectedValue & " and ProductId in (select distinct(ProductId) from EventFees where EventYear=" & ddlYear.SelectedValue & " and EventID=13 "

                    If (ddlProductGroup.SelectedValue <> "Select") Then
                        CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                    End If
                    CmdText = CmdText & ")"
                    If (ddlProductGroup.SelectedValue <> "Select") Then
                        CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                    End If

                    CmdText = CmdText & " union all select distinct p.productid, p.productcode from product p inner join Volunteer v on v.ProductGroupId =p.productgroupid where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId")
                    CmdText = CmdText & " and P.ProductId in (select distinct(ProductId) from EventFees where  and EventYear=" & ddlYear.SelectedValue & " and EventID=13   "

                    If (ddlProductGroup.SelectedValue <> "Select") Then
                        CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                    End If
                    CmdText = CmdText & ")"
                    If (ddlProductGroup.SelectedValue <> "Select") Then
                        CmdText = CmdText & "  and P.ProductGroupId=" & ddlProductGroup.SelectedValue & ""
                    End If
                    CmdText = CmdText & "and v.ProductId is null  and v.ProductGroupId is not null"
                    CmdText = CmdText & " Order by ProductId"

                End If

                dsPrd = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)
            Else
                'dsPrd = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductID,ProductCode from volunteer where Eventid=13 and ProductGroupId =" & ddlProductGroup.SelectedValue & " and ProductCode is not null and EventYear =" & ddlYear.SelectedValue)
                CmdText = " select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where EventId=13 and EventYear=" & ddlYear.SelectedValue & ""
                If (ddlProductGroup.SelectedValue <> "Select") Then
                    CmdText = CmdText & " and ProductGroupId = " & ddlProductGroup.SelectedValue & ""
                End If
                CmdText = CmdText & ")"
                If (ddlProductGroup.SelectedValue <> "Select") Then
                    CmdText = CmdText & "  and ProductGroupId=" & ddlProductGroup.SelectedValue & ""
                End If

                dsPrd = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, CmdText)

            End If

            ddlProduct.DataSource = dsPrd.Tables(0)
            ddlProduct.DataBind()
            lblError.Text = ""
            ddlProduct.Enabled = False
            ddlProduct.Items.Insert(0, "Select")
            If ddlProduct.Items.Count > 0 Then
                ddlProduct.Items(0).Selected = True
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Enabled = True
                    divFromStudents.Visible = False
                    divToStudents.Visible = False
                    divStudList.Visible = False
                End If
            Else
                lblError.ForeColor = Color.Red
                lblError.Text = "No Eligible Coacher Available"
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try

    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged

        ddlFromCoach.Items.Clear()
        ddlProduct.Items.Clear()
        ClearCoachDetails()
        Dim CoachChange As String = String.Empty
        Dim SessionCreationDate As String = Nothing
        Dim cmdText As String = String.Empty
        Dim dsCH As DataSet
        Dim dtSessionCreationDate As New DateTime()
        Dim dtTodayDate As New DateTime()

        If ddlProductGroup.Items.Count <> 0 Then

            FillProduct()
            ' ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
        Else
            lblError.ForeColor = Color.Red
            lblError.Text = "No Eligible Product Available"
        End If
        btnUpdate.Enabled = True

        ''Local
        ''Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
        ''dtTodayDate = Convert.ToDateTime(CurrDate).ToString("dd/MM/yyyy")

        ''Serevr
        'Dim CurrDate As String = DateTime.Today.ToString("MM/dd/yyyy")
        'dtTodayDate = Convert.ToDateTime(CurrDate).ToString("MM/dd/yyyy")

        'cmdText = "select CoachChange,SessionCreationDate from WebConfControl where EventID=13 and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & ""
        'dsCH = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        'If dsCH.Tables.Count > 0 Then
        '    ' Condition to check if dataset tables contains data or not
        '    If dsCH.Tables(0).Rows.Count > 0 Then
        '        CoachChange = dsCH.Tables(0).Rows(0)("CoachChange").ToString()
        '        SessionCreationDate = dsCH.Tables(0).Rows(0)("SessionCreationDate").ToString()
        '        'dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("dd/MM/yyyy")
        '        dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("MM/dd/yyyy")
        '    End If
        'Else

        'End If
        'If dsCH.Tables(0).Rows.Count > 0 Then
        '    CoachChange = "Y"

        '    If dtTodayDate >= dtSessionCreationDate And CoachChange.Trim = "N" Then
        '        btnUpdate.Enabled = False
        '        lblError.Text = "System is under maintenance.  Change cannot be executed for next one hour."
        '    Else
        '        btnUpdate.Enabled = True
        '        If ddlProductGroup.Items.Count <> 0 Then

        '            FillProduct()
        '            ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
        '        Else
        '            lblError.ForeColor = Color.Red
        '            lblError.Text = "No Eligible Product Available"
        '        End If


        '    End If
        'Else

        'End If


    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged

        FillMasterDetails()
        Try
            If ddlProductGroup.Items.Count > 0 Then
                ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Protected Sub ddlFromCoach_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromCoach.SelectedIndexChanged
        ClearCoachDetails()
        divToStudents.Visible = False
        divFromStudents.Visible = True
        divStudList.Visible = False
        If ddlFromCoach.SelectedItem.Text.ToLower = "select" Then
            divFromStudents.Visible = False
        End If

        If ddlFromCoach.Items.Count <> 0 Then
            FillFromPhaseDetails()
            ddlFromPhase_SelectedIndexChanged(ddlFromPhase, New EventArgs)

        End If
    End Sub
    Protected Sub ddlToCoach_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlToCoach.SelectedIndexChanged
        divToStudents.Visible = True
        If ddlFromCoach.Items.Count <> 0 Then
            FillToCoachSessionNo()
            FillToCoachStudents()
        End If
    End Sub

    Protected Sub ddlFromPhase_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromPhase.SelectedIndexChanged
        If ddlFromCoach.Items.Count > 0 And ddlFromPhase.Items.Count > 0 Then
            FillFromLevelDetails()
            ddlFromLevel_SelectedIndexChanged(ddlFromLevel, New EventArgs)
        End If
    End Sub

    Protected Sub ddlFromLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromLevel.SelectedIndexChanged
        If ddlFromLevel.Items.Count > 0 Then
            FillFromSessionDetails()
            ddlFromSession_SelectedIndexChanged(ddlFromSession, New EventArgs)
        End If

    End Sub

    Protected Sub ddlFromSession_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromSession.SelectedIndexChanged
        FillFromDay()
        ddlFromDay_SelectedIndexChanged(ddlFromDay, e)
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged

        ddlFromCoach.Items.Clear()
        ClearCoachDetails()
        If ddlProduct.Items.Count <> 0 Then
            Dim PgCode As String = ""
            Dim Cmdtext As String = " Select distinct productgroupid from Product where ProductId=" & ddlProduct.SelectedValue & ""
            PgCode = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, Cmdtext)
            ddlProductGroup.SelectedValue = PgCode

            FillFromDetails()
            ddlFromCoach_SelectedIndexChanged(ddlFromCoach, e)
        End If


    End Sub

    Protected Sub ddlFromDay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromDay.SelectedIndexChanged
        FillFromTime()
        ddlFromTime_SelectedIndexChanged(ddlFromTime, e)
    End Sub


    Private Sub ClearCoachDetails()
        'ddlFromCoach.Items.Clear()
        lblError.Text = ""
        lblMsg.Text = ""
        ddlToCoach.Items.Clear()
        ddlToCoach.Enabled = False

        ddlFromPhase.Items.Clear()
        ddlFromPhase.Enabled = False

        ddlFromLevel.Items.Clear()
        ddlFromLevel.Enabled = False

        ddlFromSession.Items.Clear()
        ddlFromSession.Enabled = False

        ddlFromDay.Items.Clear()
        ddlFromDay.Enabled = False

        ddlFromTime.Items.Clear()
        ddlFromTime.Enabled = False

        lblToPhase.Text = ""
        lblToLevel.Text = ""
        lblToSessionNo.Text = ""
        lblToDay.Text = ""
        lblToTime.Text = ""
    End Sub

    Private Sub ShowMessage(str As String, bErr As Boolean)
        lblError.Text = ""
        lblMsg.Text = ""
        If bErr Then
            lblError.Text = str
        Else
            lblMsg.Text = str
        End If

    End Sub
    Protected Sub BtnBulkTransfer_Click(sender As Object, e As EventArgs) Handles BtnBulkTransfer.Click
        BulkCoachTransfer()
    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        CheckCapacity()
        divFromStudents.Visible = True
        divToStudents.Visible = True
        divStudList.Visible = True
    End Sub

    Protected Sub ddlFromTime_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromTime.SelectedIndexChanged
        FillToDetails()
        ''fill From coach students
        FillFromCoachStudents()
    End Sub

    Private Sub FillFromCoachStudents()

        '   divFromStudents.Visible = True
        lblFromCoachName.Text = ddlFromCoach.SelectedItem.Text

        Dim SQLStr As String = "select ROW_NUMBER() Over (ORDER BY Co.LastName,Co.FirstName,P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, CR.CoachRegID,C.SignUpID, Case when CR.AdultId is null then Ch.FIRST_NAME + ' ' + Ch.LAST_NAME else IP.Firstname +' '+ Ip.LastName end AS StudentName,CR.Grade, Case when CR.AdultId is null then Ch.Email else IP.Email end as StudentEmail,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email as Email ,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip,I1.Career as JobTitle, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,cr.Semester,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity, Case when CR.AdultId is null then CR.ChildNumber else CR.AdultId end as ID,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status, CR.AttendeeJoinURL, C.MeetingKey, C.Vroom "
        ' If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "96" Then 
        SQLStr = SQLStr & ", Co.FirstName + ' ' + Co.LastName CoacherName, Case when CR.AdultId is null then 'Child' else 'Adult' end as Type "
        'Else
        'SQLStr = SQLStr & ", CoacherName "
        'End If

        SQLStr = SQLStr & " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse Ip on (IP.AutomemberId = CR.AdultId) Left Join IndSpouse Co on Co.AutomemberId=CR.CMemberId INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlYear.Text & " where "
        If ddlFromCoach.SelectedValue <> "0" Then
            SQLStr = SQLStr & " CR.CMemberid=" & ddlFromCoach.SelectedValue
        End If
        If ddlFromSession.SelectedValue > 0 Then
            SQLStr = SQLStr & " AND CR.SessionNo= " & ddlFromSession.SelectedValue
        End If
        If ddlFromLevel.SelectedValue <> "Select" And ddlFromLevel.SelectedValue <> "" And ddlFromLevel.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.Level= '" & ddlFromLevel.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue
        ElseIf ddlProduct.SelectedValue <> "0" Then
            SQLStr = SQLStr & "  AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue & " AND CR.ProductID= " & ddlProduct.SelectedValue & ""
        End If
        SQLStr = SQLStr & " AND CR.Semester='" & ddlFromPhase.SelectedValue & "' AND CR.Approved='Y' and C.Accepted='Y' ORDER BY  "

        SQLStr = SQLStr & " C.Level,C.Day,Co.LastName,Co.FirstName,P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"

        Dim dsCoach As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, SQLStr)
        ViewState("FromCoachStud") = dsCoach
        If dsCoach.Tables(0).Rows.Count > 0 Then
            divFrom.Visible = True
            lblFromErr.Visible = False
            gvFrom.DataSource = dsCoach.Tables(0)
            gvFrom.DataBind()
        Else
            divFrom.Visible = False
            lblFromErr.Visible = True
        End If

    End Sub


    Private Sub FillToCoachStudents()
        If ddlToCoach.SelectedItem.Text.ToLower <> "select" Then
            divToStudents.Visible = True
        End If
        lblToCoachName.Text = ddlToCoach.SelectedItem.Text

        Dim SQLStr As String = "select ROW_NUMBER() Over (ORDER BY Co.LastName,Co.FirstName,P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, CR.CoachRegID,C.SignUpID, Case when CR.AdultId is null then Ch.FIRST_NAME + ' ' + Ch.LAST_NAME else IP.FirstName + ' ' + Ip.LastName end AS StudentName,CR.Grade, Case when CR.AdultId is null then Ch.Email else Ip.Email end as StudentEmail,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email as Email ,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip,I1.Career as JobTitle, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,CR.Semester,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity, Case When Cr.AdultId is null then CR.ChildNumber else CR.AdultId end as ID,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status, CR.AttendeeJoinURL, C.MeetingKey, C.Vroom "
        ' If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "96" Then 
        SQLStr = SQLStr & ", Co.FirstName + ' ' + Co.LastName CoacherName, Case when CR.AdultId is null then 'Child' else 'Adult' end as Type "
        'Else
        'SQLStr = SQLStr & ", CoacherName "
        'End If

        SQLStr = SQLStr & " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) Left Join IndSpouse Co on Co.AutomemberId=CR.CMemberId INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlYear.Text & " where "
        If ddlToCoach.SelectedValue <> "0" Then
            SQLStr = SQLStr & " CR.CMemberid=" & ddlToCoach.SelectedValue
        End If
        If lblToSessionNo.Text <> "" Then
            SQLStr = SQLStr & " AND CR.SessionNo= " & lblToSessionNo.Text
        End If
        If ddlFromLevel.SelectedValue <> "Select" And ddlFromLevel.SelectedValue <> "" And ddlFromLevel.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.Level= '" & ddlFromLevel.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue
        ElseIf ddlProduct.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue & " AND CR.ProductID= " & ddlProduct.SelectedValue & ""
        End If

        SQLStr = SQLStr & " AND C.Day ='" & ddlFromDay.SelectedItem.Text & "' AND C.Time ='" & ddlFromTime.SelectedItem.Text & "' and C.Level='" & ddlFromLevel.SelectedValue & "'"

        SQLStr = SQLStr & " AND CR.Semester='" & ddlFromPhase.SelectedValue & "' AND CR.Approved='Y' and C.Accepted='Y' ORDER BY  "

        SQLStr = SQLStr & " C.Level,C.Day,Co.LastName,Co.FirstName,P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"

        Dim dsCoach As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, SQLStr)
        ViewState("ToCoachStud") = dsCoach
        If dsCoach.Tables(0).Rows.Count > 0 Then
            divTo.Visible = True
            lblToErr.Visible = False
            gvTo.DataSource = dsCoach.Tables(0)
            gvTo.DataBind()
        Else
            divTo.Visible = False
            lblToErr.Visible = True
        End If
    End Sub
    Private Sub FillToCoachStudentsALL()
        divStudList.Visible = True
        If ddlToCoach.SelectedItem.Text.ToLower <> "select" Then
            divToStudents.Visible = True
        End If
        lblToCoachName.Text = ddlToCoach.SelectedItem.Text

        Dim SQLStr As String = "select ROW_NUMBER() Over (ORDER BY Co.LastName,Co.FirstName,P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, CR.CoachRegID,C.SignUpID,Case when CR.AdultId is null then Ch.FIRST_NAME + ' ' + Ch.LAST_NAME else IP.FirstName + ' ' + Ip.LastName end AS StudentName,CR.Grade, Case when CR.AdultId is null then Ch.Email else Ip.Email end as StudentEmail,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email as Email ,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip,I1.Career as JobTitle, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,Case When Cr.AdultId is null then CR.ChildNumber else CR.AdultId end as ID,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status, CR.AttendeeJoinURL, C.MeetingKey, C.Vroom "
        ' If Session("RoleId").ToString() = "1" Or Session("RoleId").ToString() = "2" Or Session("RoleId").ToString() = "96" Then 
        SQLStr = SQLStr & ", Co.FirstName + ' ' + Co.LastName CoacherName, Case when CR.AdultId is null then 'Child' else 'Adult' end as Type "
        'Else
        'SQLStr = SQLStr & ", CoacherName "
        'End If

        SQLStr = SQLStr & " from Coachreg CR left Join Child Ch ON CR.ChildNumber=Ch.ChildNumber left Join Indspouse IP on (IP.AutoMemberId = CR.AdultId) Left Join IndSpouse Co on Co.AutomemberId=CR.CMemberId INNER JOIN IndSpouse I1 ON CR.PMEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
        SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlYear.Text & " where "
        If ddlToCoach.SelectedValue <> "0" Then
            SQLStr = SQLStr & " CR.CMemberid=" & ddlToCoach.SelectedValue
        End If
        If lblToSessionNo.Text <> "" Then
            SQLStr = SQLStr & " AND CR.SessionNo= " & lblToSessionNo.Text
        End If
        If ddlFromLevel.SelectedValue <> "Select" And ddlFromLevel.SelectedValue <> "" And ddlFromLevel.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.Level= '" & ddlFromLevel.SelectedValue & "'"
        End If
        If ddlProductGroup.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue
        ElseIf ddlProduct.SelectedValue <> "0" Then
            SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue & " AND CR.ProductID= " & ddlProduct.SelectedValue & ""
        End If

        SQLStr = SQLStr & " AND C.Day ='" & ddlFromDay.SelectedItem.Text & "' AND C.Time ='" & ddlFromTime.SelectedItem.Text & "' and C.Level='" & ddlFromLevel.SelectedValue & "'"

        SQLStr = SQLStr & " AND CR.Semester='" & ddlFromPhase.SelectedValue & "' AND CR.Approved='Y' and C.Accepted='Y' ORDER BY  "

        SQLStr = SQLStr & " C.Level,C.Day,Co.LastName,Co.FirstName,P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"

        Dim dsCoach As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, SQLStr)

        If dsCoach.Tables(0).Rows.Count > 0 Then
            divStudList.Visible = True
            lblStudList.Visible = False
            gvStudList.DataSource = dsCoach.Tables(0)
            gvStudList.DataBind()
        Else
            divStudList.Visible = False
            lblStudList.Visible = True
        End If
    End Sub
    Private Sub SendMailInfo(frmCoachID As Integer, toCoachId As Integer)

        'send emails to coachs & CoachAdmin
        Dim dr As SqlDataReader
        Dim fromCoachMail As String = "", toCoachMail As String = "", fromCoachName As String = "", cmdText, MailBody, MailBody1, MailBody2 As String, cmdParentQuery As String
        Dim CoachAdminEmail, subj As String, ProductCode As String, CoachMailBody As String = "", MailBody4 As String = "", MailBody3 As String = ""

        MailBody = ""
        subj = "A Coach is switching"
        CoachAdminEmail = ""

        'Get from coach mail
        cmdText = "select distinct I.Email as FromCoachmail,I.Firstname + ' ' + I.LastName as CoachName FROM CalSignUp C inner join IndSpouse I on  C.MemberID=I.AutoMemberID  Inner JOIN Product P ON C.ProductID=P.ProductID"
        cmdText = cmdText & "  WHERE C.MemberId = " & frmCoachID & " And C.eventyear = " & ddlYear.SelectedValue & " and C.productgroupid=" & ddlProductGroup.SelectedValue & " and C.productid=" & ddlProduct.SelectedValue & " and C.Semester = '" & ddlFromPhase.SelectedValue & "' and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and Accepted is null"

        dr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)
        If dr.Read Then
            fromCoachMail = dr("FromCoachmail").ToString()
            fromCoachName = dr("CoachName").ToString()
        End If

        ' Getting To Coach Mail
        cmdText = "select distinct I.Email ToCoachMail,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day , C.TIMe,C.level,C.ProductID, Ch.First_Name + ' ' + Ch.Last_Name as ChildName,I2.FirstName + ' ' + I2.LastName as ParentName, I2.Email as ParentEMail,I2.AutoMemberId as ParentId FROM coachreg cr inner join IndSpouse I on cr.CMemberID=I.AutoMemberID INNER JOIN CalSignUp C ON c.memberid = cr.Cmemberid Inner JOIN Product P ON Cr.ProductID=P.ProductID  inner join Child Ch on Ch.Childnumber = cr.ChildNumber "
        cmdText = cmdText & " INNER JOIN IndSpouse I2 ON I2.AutoMemberID=cr.PMemberId WHERE c.MemberId =" & toCoachId & "  And Cr.eventyear = " & ddlYear.SelectedValue & " and cr.productgroupid=" & ddlProductGroup.SelectedValue & " and cr.productid=" & ddlProduct.SelectedValue & " and C.Semester = '" & ddlFromPhase.SelectedValue & "' and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and cr.Approved='y'"

        dr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)
        If dr.Read Then
            toCoachMail = dr("ToCoachMail").ToString()
        End If

        Dim dt As DataTable
        dt = ViewState("StudentList")

        MailBody3 = " Switching from: " & fromCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString() & "<br> To: "
        MailBody4 = toCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & ", Time : " & ddlFromTime.SelectedValue.ToString() & ",Switch Date : " & Now.ToString()

        Dim drStud As DataRow
        For Each drStud In dt.Rows
            If drStud("ChildName").ToString() <> "" Then
                ProductCode = drStud("ProductCode")
                ' toCoachMail = drStud("ToCoachMail")
                MailBody2 = "<br>Student: " & drStud("ChildName") & ", Parent Name: " & drStud("ParentName") & ", Email: " & drStud("ParentEMail") & ", " & ProductCode & ","
                MailBody3 = " <br><br>Switching from: " & fromCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString() & "<br> To: "
                MailBody4 = toCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & ", Time : " & ddlFromTime.SelectedValue.ToString() & ",Switch Date : " & Now.ToString()

                CoachMailBody = CoachMailBody & MailBody2

                ProductCode = drStud("ProductCode")

                ' Get father and mother mail ids
                cmdParentQuery = "select distinct EmailID as EmailID from(SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid =" & drStud("ParentId") & ")) or "
                cmdParentQuery = cmdParentQuery & " (donortype = 'SPOUSE'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship =" & drStud("ParentId") & ") group by Email)as Email where (EmailID is not null and len(EmailID)<>'0' )"

                Dim drParent As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdParentQuery)

                MailBody1 = "Dear Parent, <br><br>Note : Do not reply to the email above.<br>"
                MailBody = MailBody1 & MailBody2 & MailBody3 & MailBody4

                While drParent.Read
                    '' Send Mail to Each Parent (Father and Mother)
                    SendEmail(subj, MailBody, drParent("EmailId"), ProductCode)

                    'Response.Write("<br> Parent :" & drParent("EmailId").ToString() & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
                    'Response.Write("<br>==============================================")
                End While

                'Send Mail to Each Child
                If drStud("ChildEMail").ToString() <> "" Then

                    MailBody1 = "Dear Student, <br><br>Note : Do not reply to the email above.<br> "
                    MailBody = MailBody1 & MailBody2 & MailBody3 & MailBody4
                    SendEmail(subj, MailBody, drStud("ChildEMail"), ProductCode)

                    'Response.Write("<br> Child :" & drStud("ChildEMail").ToString() & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
                    'Response.Write("<br>==============================================")
                End If
            End If
        Next

        MailBody1 = "Dear Coach, <br><br>Note : Do not reply to the email above.<br>"
        Dim mailResign As String
        mailResign = "<br>Coach :" & fromCoachName & " resigned from " & ProductCode & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString()

        MailBody = MailBody1 & mailResign ' & MailBody3 & MailBody4
        ' Response.Write("<br> From coach :" & fromCoachMail & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)

        'Send mail for resigned coach
        SendEmail(subj, MailBody, fromCoachMail, ProductCode)

        MailBody = MailBody1 & CoachMailBody & MailBody3 & MailBody4

        'Response.Write("<br>==============================================")
        'Response.Write("<br> To Coach :" & toCoachMail & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
        'Response.Write("<br>==============================================")

        ' Send mail to ToCoach
        SendEmail(subj, MailBody, toCoachMail, ProductCode)

    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal PCode As String)
        Dim sFrom As String = "nsfcontests@northsouth.org" '"nsfcontests@gmail.com" 'Updated on Jan 30 2015

        Select Case PCode.ToLower
            Case "mb1"
                sFrom = "nsfelementarymath@northSouth.org"
            Case "mb2"
                sFrom = "nsfpremathcounts@northSouth.org"
            Case "mb3"
                sFrom = "nsfmathcounts@northSouth.org"
            Case "jsc"
                sFrom = "nsfjuniorscience@northSouth.org"
            Case "isc"
                sFrom = "nsfintermediatescience@northSouth.org"
            Case "ssc"
                sFrom = "nsfseniorscience@northSouth.org"
            Case Else
                sFrom = "nsfcontests@northsouth.org"
        End Select


        'Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        Dim ok As Boolean = True
        Try
            ' client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub


    ' Log details in BulkChangeCoachLog Table
    Private Sub LogCoachDetails(fromCoachId As Integer, toCoachId As Integer, day As String, tme As String, vroom As Integer, uid As String, pwd As String)
        Dim cmdLog As String
        cmdLog = "INSERT INTO BulkChangeCoachLog (EventYear, FMemeberID, TMemberID, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Semester, [Level], Session, Day, Time, Vroom, UserID, PWD, CreateDate, CreatedBy)"
        cmdLog = cmdLog & " Values( " & ddlYear.SelectedValue & " ," & fromCoachId & " ," & toCoachId & "," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "','" & ddlFromPhase.SelectedValue & "'"
        cmdLog = cmdLog & " , '" & ddlFromLevel.SelectedValue & "'," & ddlFromSession.SelectedValue & ", '" & day & "', '" & tme & "'," & vroom & ",'" & uid & "','" & pwd & "',GetDate()," & Session("LoginId") & ")"
        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdLog)
    End Sub
    Public Sub CreateTrainingSession(WebEXID As String, PWD As String, MeetingTitle As String, Capacity As Integer, StartDate As String, Time As String, _
    Day As String, BeginTime As String, EndTime As String, EndDate As String, Duration As String, CoachName As String, ProductCode As String)
        Dim MTitle As String = CoachName & " - " & ProductCode
        Dim dt As DateTime = DateTime.Now.AddDays(1).ToShortDateString()
        Dim strDate = dt.Month & "/" & dt.Day & "/" & dt.Year
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"

        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf

        strXML &= "<webExID>" & WebEXID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & PWD & "</password>" & vbCr & vbLf
        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.CreateTrainingSession"">" & vbCr & vbLf

        strXML &= "<accessControl>" & vbCr & vbLf
        'strXML &= "  <listing>PUBLIC</listing>\r\n";
        strXML &= "  <sessionPassword>training</sessionPassword>" & vbCr & vbLf
        strXML &= "</accessControl>" & vbCr & vbLf

        strXML &= "<schedule>" & vbCr & vbLf

        strXML &= "<startDate>" & strDate & " " & Time & "</startDate>" & vbCr & vbLf


        strXML &= "<duration>" & Duration & "</duration>" & vbCr & vbLf

        strXML &= "     <timeZoneID>11</timeZoneID>" & vbCr & vbLf
        strXML &= "     <openTime>20</openTime>" & vbCr & vbLf
        strXML &= "</schedule>" & vbCr & vbLf

        strXML &= "<metaData>" & vbCr & vbLf
        strXML &= "<confName>" & MTitle & "</confName>" & vbCr & vbLf
        strXML &= "     <agenda>agenda 1</agenda>" & vbCr & vbLf
        strXML &= "     <description>Training</description>" & vbCr & vbLf
        strXML &= "     <greeting>greeting</greeting>" & vbCr & vbLf
        strXML &= "     <location>location</location>" & vbCr & vbLf
        strXML &= "     <invitation>invitation</invitation>" & vbCr & vbLf
        strXML &= "</metaData>" & vbCr & vbLf

        strXML &= "<repeat>"
        strXML &= "     <repeatType>MULTIPLE_SESSION</repeatType>"
        strXML &= "     <dayInWeek>"
        strXML &= "         <day>THURSDAY</day>"
        strXML &= "     </dayInWeek>"
        strXML &= "     <occurenceType>WEEKLY</occurenceType>"
        strXML &= "     <expirationDate>" & EndDate & " " & EndTime & "</expirationDate>"
        strXML &= "</repeat>"

        strXML &= "<attendeeOptions>" & vbCr & vbLf
        strXML &= "      <request>true</request>" & vbCr & vbLf
        strXML &= "      <registration>true</registration>" & vbCr & vbLf
        strXML &= "      <auto>true</auto>" & vbCr & vbLf
        strXML &= "      <registrationPWD>training</registrationPWD>" & vbCr & vbLf
        strXML &= "      <maxRegistrations>200</maxRegistrations>" & vbCr & vbLf
        strXML &= "      <registrationCloseDate>10/30/2015 12:00:00"
        strXML &= "      </registrationCloseDate>" & vbCr & vbLf
        strXML &= "      <emailInvitations>true</emailInvitations>" & vbCr & vbLf
        strXML &= "</attendeeOptions>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf

        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)
        request.ContentLength = byteArray.Length
        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()
        Dim response As WebResponse = request.GetResponse()
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim Result As String = Nothing
        Result = ProcessCreatedTrainingSessionsResponse(xmlReply)
    End Sub
    Public Sub RegisterMeetingAttendee(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, City As String, Email As String, Country As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.attendee.RegisterMeetingAttendee"">" & vbCr & vbLf

        strXML &= "<attendees>" & vbCr & vbLf
        strXML &= "<person>" & vbCr & vbLf
        strXML &= "<name>" & Name & "</name>" & vbCr & vbLf
        strXML &= "<title>title</title>" & vbCr & vbLf
        strXML &= "<company>microsoft</company>" & vbCr & vbLf
        strXML &= "<address>" & vbCr & vbLf
        strXML &= "<addressType>PERSONAL</addressType>" & vbCr & vbLf
        strXML &= "<city>" & City & "</city>" & vbCr & vbLf
        strXML &= "<country>US</country>" & vbCr & vbLf
        strXML &= "</address>" & vbCr & vbLf

        strXML &= "<email>" & Email & "</email>" & vbCr & vbLf
        strXML &= "<notes>notes</notes>" & vbCr & vbLf
        strXML &= "<url>https://</url>" & vbCr & vbLf
        strXML &= "<type>VISITOR</type>" & vbCr & vbLf
        strXML &= "</person>" & vbCr & vbLf
        strXML &= "<joinStatus>ACCEPT</joinStatus>" & vbCr & vbLf
        strXML &= "<role>ATTENDEE</role>" & vbCr & vbLf

        strXML &= "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML &= "</attendees>" & vbCr & vbLf


        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = RegisterMeetingAttendeeResponse(xmlReply)
    End Sub
    Public Sub GetJoinMeetingURL(sessionKey As String, WebExID As String, Pwd As String, AttendeeID As String, Name As String, Email As String, MeetingPassword As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)

        request.Method = "POST"

        request.ContentType = "application/x-www-form-urlencoded"


        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service\"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd\"">" & vbCr & vbLf

        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GetjoinurlMeeting"">" & vbCr & vbLf

        strXML &= "<sessionKey>" & sessionKey & "</sessionKey>" & vbCr & vbLf
        strXML &= "<attendeeName>" & Name & "</attendeeName>" & vbCr & vbLf
        strXML &= "<attendeeEmail>" & Email & "</attendeeEmail>" & vbCr & vbLf
        strXML &= "<meetingPW>" & MeetingPassword & "</meetingPW>" & vbCr & vbLf
        strXML &= "<RegID>" & hdnAttendeeRegisteredID.Value & "</RegID>" & vbCr & vbLf

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        request.ContentLength = byteArray.Length


        Dim dataStream As Stream = request.GetRequestStream()

        dataStream.Write(byteArray, 0, byteArray.Length)

        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()


        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingAttendeeURLResponse(xmlReply)
    End Sub
    Public Sub CancelMeeting(sessionKey As String, WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"
        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        request.Method = "POST"
        request.ContentType = "application/x-www-form-urlencoded"
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf
        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf
        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.training.DelTrainingSession"">" & vbCr & vbLf
        strXML &= (Convert.ToString("<sessionKey>") & sessionKey) & "</sessionKey>"
        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)
        request.ContentLength = byteArray.Length
        Dim dataStream As Stream = request.GetRequestStream()
        dataStream.Write(byteArray, 0, byteArray.Length)
        dataStream.Close()

        Dim response As WebResponse = request.GetResponse()
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim Result As String = Nothing
        Result = ProcessTrainingSessionsResponse(xmlReply)

    End Sub
    Private Function ProcessMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function
    Private Function RegisterMeetingAttendeeResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:registerID", manager).InnerText
                Dim attendeeID As String

                attendeeID = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/att:register/att:attendeeID", manager).InnerText

                hdnMeetingAttendeeID.Value = attendeeID
                hdnAttendeeRegisteredID.Value = meetingKey
                hdnAttendeeRegistrationKey.Value = meetingKey

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function

    Private Function MeetingAttendeeURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:joinMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function
    Private Function ProcessTrainingSessionsResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim status As String = Nothing
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            status = (xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText)

        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        'Return sb.ToString()
        Return status.ToString()
    End Function
    Public Sub SwitchBulkCoachSituation1(fromCoachId As String, toCoachId As String, ToCoachMeetingKey As String, ToCoachWebExID As String, ToCoachPwd As String, UserID As String, ToCoachSessionNo As Integer)
        'From Coach and To Coach both are registered for a web conf sessions
        Try


            Dim cmdText As String = Nothing
            ' deleteMeeting()

            Dim dsChild As New DataSet()
            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on (CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ddlProductGroup.SelectedValue & "' and CR.ProductID='" & ddlProduct.SelectedValue & "' and CR.CMemberID=" & fromCoachId & " and CR.EventYear=" & ddlYear.SelectedValue & ") where C1.ChildNumber in (select ChildNumber from CoachReg where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and CMemberID=" & fromCoachId & " and Approved='Y' and  Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and semester='" & ddlFromPhase.SelectedValue & "')"
            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)

            Dim dsChildURl As DataSet = New DataSet()
            ChildText = "select distinct AttendeeJoinURL from Coachreg where CMemberID=" & toCoachId & " and Approved='Y' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ToCoachSessionNo & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and Semester='" & ddlFromPhase.SelectedValue & "'"
            dsChildURl = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
            If dsChildURl IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChildURl.Tables(0).Rows.Count > 0 Then
                    hdnMeetingURL.Value = dsChildURl.Tables(0).Rows(0)("AttendeeJoinURL").ToString()
                End If
            End If

            cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & ", SessionNo=" & ToCoachSessionNo & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"

            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "UPDATE WebConfLog SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",MEMBERID=" & toCoachId & " where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and Session=" & ddlFromSession.SelectedValue & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            Dim ChidName As String = String.Empty
            Dim Email As String = String.Empty
            Dim City As String = String.Empty
            Dim Country As String = String.Empty
            Dim ChildNumber As String = String.Empty
            Dim CoachRegID As String = String.Empty


            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChild.Tables(0).Rows.Count > 0 Then
                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                        ChidName = drChild("Name").ToString()
                        Email = drChild("OnlineClassEmail").ToString()
                        City = drChild("City").ToString()
                        Country = drChild("Country").ToString()
                        ChildNumber = drChild("ChildNumber").ToString()
                        CoachRegID = drChild("CoachRegID").ToString()

                        'RegisterMeetingAttendee(ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, "", ChidName, City, Email, Country)
                        'If hdnMeetingStatus.Value = "Success" Then

                        'GetJoinMeetingURL(ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, hdnMeetingAttendeeID.Value, ChidName, Email, hdnToCoachMeetingPwd.Value)

                        Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where EventYear=" & ddlYear.SelectedValue & " and CMemberID=" & toCoachId & " and ChildNumber=" & ChildNumber & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Approved='Y' and CoachRegID='" & CoachRegID & "'"

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                        'End If
                    Next
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub
    Public Sub SwitchBulkCoachSituation2(fromCoachId As String, toCoachId As String, MeetingKey As String, HostMeetingUrl As String, FromCoachWebExID As String, FromCoachPwd As String, UserID As String, BeginTime As TimeSpan, EndTime As TimeSpan, Duration As String, PWD As String, MeetingPwd As String, Status As String, Cycle As String, Vroom As String, ToCoachSessionNo As Integer)
        'From Coach registered for a session but To Coach doesn't registered for a session
        Try


            Dim cmdText As String = Nothing

            Dim dsChild As New DataSet()
            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ddlProductGroup.SelectedValue & "' and CR.ProductID='" & ddlProduct.SelectedValue & "' and CR.CMemberID=" & toCoachId & " and CR.EventYear=" & ddlYear.SelectedValue & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and CMemberID=" & toCoachId & " and Approved='Y' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ToCoachSessionNo & " and semester='" & ddlFromPhase.SelectedValue & "')"
            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)

            Dim dsChildURl As DataSet = New DataSet()
            ChildText = "select distinct AttendeeJoinURL from Coachreg where CMemberID=" & fromCoachId & " and Approved='Y' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ToCoachSessionNo & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and semester='" & ddlFromPhase.SelectedValue & "'"
            dsChildURl = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
            If dsChildURl IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChildURl.Tables(0).Rows.Count > 0 Then
                    hdnMeetingURL.Value = dsChildURl.Tables(0).Rows(0)("AttendeeJoinURL").ToString()
                End If
            End If

            cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & ", SessionNo=" & ToCoachSessionNo & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Update Calsignup --Update Details of ToCoach with From Coach 
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",[Duration] = " & Duration & " , [Begin] ='" & BeginTime.ToString() & "', [End] ='" & EndTime.ToString() & "', Cycle=" & Cycle & ", vroom =" & Vroom & " ,[UserId]='" & FromCoachWebExID & "', Pwd ='" & FromCoachPwd & "',HostJoinURL='" & HostMeetingUrl & "',MeetingKey='" & MeetingKey & "',MeetingPwd='" & MeetingPwd & "', Status='" & Status & "'  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "'  and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Update From coach Accepted =null 
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)


            cmdText = "UPDATE WebConfLog SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",MEMBERID=" & toCoachId & ", Session=" & ToCoachSessionNo & " where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and Session=" & ddlFromSession.SelectedValue & ""
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)



            Dim ChidName As String = String.Empty
            Dim Email As String = String.Empty
            Dim City As String = String.Empty
            Dim Country As String = String.Empty
            Dim ChildNumber As String = String.Empty
            Dim CoachRegID As String = String.Empty

            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChild.Tables(0).Rows.Count > 0 Then
                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                        ChidName = drChild("Name").ToString()
                        Email = drChild("OnlineClassEmail").ToString()
                        City = drChild("City").ToString()
                        Country = drChild("Country").ToString()
                        ChildNumber = drChild("ChildNumber").ToString()
                        CoachRegID = drChild("CoachRegID").ToString()

                        ' RegisterMeetingAttendee(MeetingKey, FromCoachWebExID, FromCoachPwd, "", ChidName, City, Email, Country)
                        ' If hdnMeetingStatus.Value = "Success" Then

                        'GetJoinMeetingURL(MeetingKey, FromCoachWebExID, FromCoachPwd, hdnMeetingAttendeeID.Value, ChidName, Email, hdnFromCoachMeetingKey.Value)
                        Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                        'End If
                    Next
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub
    Public Sub SwitchBulkCoachSituation3(fromCoachId As String, toCoachId As String, ToCoachMeetingKey As String, ToCoachWebExID As String, ToCoachPwd As String, UserID As String, ToCoachSessionNo As Integer)
        'To Coach registered for a session but From coach doesn't registered for a session
        Try


            Dim cmdText As String = Nothing

            Dim dsChild As New DataSet()
            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ddlProductGroup.SelectedValue & "' and CR.ProductID='" & ddlProduct.SelectedValue & "' and CR.CMemberID=" & fromCoachId & " and CR.EventYear=" & ddlYear.SelectedValue & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and CMemberID=" & fromCoachId & " and Approved='Y' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and semester='" & ddlFromPhase.SelectedValue & "')"
            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)

            Dim dsChildURl As DataSet = New DataSet()
            ChildText = "select distinct AttendeeJoinURL from CoachReg where CMemberID=" & toCoachId & " and Approved='Y' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ToCoachSessionNo & " and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID='" & ddlProductGroup.SelectedValue & "' and ProductID='" & ddlProduct.SelectedValue & "' and semester='" & ddlFromPhase.SelectedValue & "'"
            dsChildURl = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
            If dsChildURl IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChildURl.Tables(0).Rows.Count > 0 Then
                    hdnMeetingURL.Value = dsChildURl.Tables(0).Rows(0)("AttendeeJoinURL").ToString()
                End If
            End If

            cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & ", SessionNo=" & ToCoachSessionNo & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"

            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"

            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            Dim ChidName As String = String.Empty
            Dim Email As String = String.Empty
            Dim City As String = String.Empty
            Dim Country As String = String.Empty
            Dim ChildNumber As String = String.Empty
            Dim CoachRegID As String = String.Empty

            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                If dsChild.Tables(0).Rows.Count > 0 Then
                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                        ChidName = drChild("Name").ToString()
                        Email = drChild("OnlineClassEmail").ToString()
                        City = drChild("City").ToString()
                        Country = drChild("Country").ToString()
                        ChildNumber = drChild("ChildNumber").ToString()
                        CoachRegID = drChild("CoachRegID").ToString()

                        ' RegisterMeetingAttendee(ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, "", ChidName, City, Email, Country)
                        'If hdnMeetingStatus.Value = "Success" Then

                        'GetJoinMeetingURL(ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, hdnMeetingAttendeeID.Value, ChidName, Email, hdnToCoachMeetingKey.Value)

                        Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                        'End If
                    Next
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Public Sub SwitchBulkCoachSituation4(fromCoachId As String, toCoachId As String, MeetingKey As String, HostMeetingUrl As String, FromCoachWebExID As String, FromCoachPwd As String, UserID As String, BeginTime As TimeSpan, EndTime As TimeSpan, Duration As String, PWD As String, MeetingPwd As String, Status As String, Cycle As String, Vroom As String, ToCoachSessionNo As Integer)
        'From Coach and To Coach both are not registered for a session.
        Try


            Dim cmdText As String = Nothing

            'Update From Coach's student to To Coach
            cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & ", SessionNo=" & ToCoachSessionNo & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Update Calsignup --Update Details of ToCoach with From Coach 
            'cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",[Duration] = " & Duration & " , [Begin] ='" & BeginTime.ToString() & "', [End] ='" & EndTime.ToString() & "', Cycle=" & Cycle & ", vroom =" & Vroom & " ,[UserId]='" & FromCoachWebExID & "', Pwd ='" & FromCoachPwd & "',HostJoinURL=null,MeetingKey=null,MeetingPwd=null, Status=null  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = " & ddlFromPhase.SelectedValue & " and Level = '" & ddlFromLevel.SelectedValue & "'  and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            'SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)


            'Update From coach Accepted =null 
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day, Vl.UserID, Vl.Pwd,Vl.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddlYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookUp VL on(CS.Vroom=VL.Vroom) where Accepted='Y' and CS.ProductGroupCode in ('" & ddlProductGroup.SelectedItem.Text & "') and CS.ProductCode in ('" & ddlProduct.SelectedItem.Text & "') and CS.EventYear='" + ddlYear.SelectedValue + "' and CS.MemberID=" & toCoachId & " and CS.Semester='" & ddlFromPhase.SelectedValue & "'"

            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)


            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows

                        Dim HostID As String = String.Empty
                        HostID = dr("HostID").ToString()
                        Dim WebExID As String = dr("UserID").ToString()
                        Dim WebExPwd As String = dr("PWD").ToString()
                        Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                        Dim ScheduleType As String = dr("ScheduleType").ToString()

                        Dim year As String = dr("EventYear").ToString()
                        Dim eventID As String = dr("EventID").ToString()

                        Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                        Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                        Dim ProductID As String = dr("ProductID").ToString()
                        Dim ProductCode As String = dr("ProductCode").ToString()
                        Dim Semester As String = dr("Semester").ToString()
                        Dim Level As String = dr("Level").ToString()
                        Dim Sessionno As String = dr("SessionNo").ToString()
                        Dim CoachID As String = dr("MemberID").ToString()
                        MeetingPwd = "training"

                        Dim Time As String = dr("Time").ToString()
                        Dim Day As String = dr("Day").ToString()
                        Dim STime As String = dr("Begin").ToString()
                        Dim ETime As String = dr("End").ToString()

                        Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                        Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                        'string endTime = "01:00 AM";
                        'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                        'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                        'double hours = (time1 - time2).TotalHours;

                        Dim timeZoneID As String = ddlTimeZone.SelectedValue
                        Dim TimeZone As String = ddlTimeZone.SelectedItem.Text
                        Dim SignUpId As String = dr("SignupID").ToString()
                        Dim Mins As Double = 0.0
                        Dim dFrom As DateTime
                        Dim dTo As DateTime

                        Dim sDateFrom As String = STime
                        Dim sDateTo As String = ETime
                        If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                            Dim TS As TimeSpan = dTo - dFrom

                            Mins = TS.TotalMinutes
                        End If
                        Dim durationHrs As String = Mins.ToString("0")
                        If durationHrs.IndexOf("-") > -1 Then
                            durationHrs = "188"
                        End If

                        If timeZoneID = "4" Then
                            TimeZone = "EST/EDT – Eastern"
                        ElseIf timeZoneID = "7" Then
                            TimeZone = "CST/CDT - Central"
                        ElseIf timeZoneID = "6" Then
                            TimeZone = "MST/MDT - Mountain"
                        End If

                        Dim CoachName As String = String.Empty

                        CoachName = dr("CoachName").ToString()
                        Dim meetingTitle As String = String.Empty
                        meetingTitle = CoachName & "-" & ProductCode & "-" & Level & "-" & Sessionno
                        If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then

                            createZoomMeeting(meetingTitle, HostID)
                            If hdnMeetingStatus.Value = "SUCCESS" Then

                                Dim meetingURL As String = hdnHostMeetingURL.Value
                                cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & EndDate & "','" & STime & "','" & ETime & "'," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Semester & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "', 'Recurring Meeting'"

                                Dim objDs As New DataSet()
                                objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                                    If objDs.Tables(0).Rows.Count > 0 Then
                                        If Convert.ToInt32(objDs.Tables(0).Rows(0)("Retval").ToString()) > 0 Then

                                            Dim dsChild As New DataSet()
                                            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ProductGroupID & "' and CR.ProductID='" & ProductID & "' and CR.CMemberID=" & CoachID & " and CR.EventYear=" & year & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & year & " and ProductGroupID='" & ProductGroupID & "' and ProductID='" & ProductID & "' and CMemberID=" & CoachID & " and Approved='Y' and Level = '" & Level & "' and SessionNo=" & Sessionno & ") and CR.Level='" & Level & "' and CR.SessionNo=" & Sessionno & " and CR.semester='" & ddlFromPhase.SelectedValue & "'"
                                            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
                                            Dim ChidName As String = String.Empty
                                            Dim Email As String = String.Empty
                                            Dim City As String = String.Empty
                                            Dim Country As String = String.Empty
                                            Dim ChildNumber As String = String.Empty
                                            Dim CoachRegID As String = String.Empty

                                            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                                                If dsChild.Tables(0).Rows.Count > 0 Then
                                                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                                                        ChidName = drChild("Name").ToString()
                                                        Email = drChild("Email").ToString()
                                                        City = drChild("City").ToString()
                                                        Country = drChild("Country").ToString()
                                                        ChildNumber = drChild("ChildNumber").ToString()
                                                        CoachRegID = drChild("CoachRegID").ToString()


                                                        If hdnMeetingStatus.Value = "SUCCESS" Then


                                                            Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"

                                                            CmdChildUpdateText = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpId & " and CR.CoachRegID = " & CoachRegID & ""

                                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                                                        End If
                                                    Next
                                                End If
                                            End If
                                        Else

                                        End If
                                    End If
                                End If


                            Else
                            End If
                        Else

                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub


    Public Sub GetHostUrlMeeting(WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"


        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf


        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GethosturlMeeting"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & hdnSessionKey.Value & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingHostURLResponse(xmlReply)
        'lblMsg3.Text = result;

    End Sub

    Private Function MeetingHostURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"
                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnHostMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        Return sb.ToString()
    End Function
    Private Function ProcessCreatedTrainingSessionsResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Dim status As String = Nothing
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")
            manager.AddNamespace("train", "http://www.webex.com/schemas/2002/06/service/trainingsession")
            Dim MeetingKey As String = Nothing
            status = (xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText)
            If status = "SUCCESS" Then
                hdnMeetingStatus.Value = "Success"

                hdnSessionKey.Value = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/train:sessionkey", manager).InnerText
            ElseIf status = "FAILURE" Then
                hdnMeetingStatus.Value = "Failure"
                hdnSessionKey.Value = ""
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try

        'Return sb.ToString()
        Return status.ToString()
    End Function

    Public Sub CheckCapacity()
        Try




            Dim fromCoachId As Integer, toCoachId As Integer
            fromCoachId = ddlFromCoach.SelectedValue
            toCoachId = ddlToCoach.SelectedValue
            Dim FromCoachMeetingKey As String = Nothing

            Dim cmdText As String = ""
            Dim CmdTextCount As String = String.Empty
            cmdText = "select MaxCapacity from CalSignUp where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "';  "

            cmdText = cmdText & " select MaxCapacity from CalSignUp where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"

            Dim objDs As New DataSet()
            objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            Dim FromCoachMaxCap As Integer = 0
            Dim ToCoachmaxCap As Integer = 0
            If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                If objDs.Tables(0).Rows.Count > 0 Then

                    FromCoachMaxCap = Convert.ToInt32(objDs.Tables(0).Rows(0)("MaxCapacity").ToString())
                    ToCoachmaxCap = Convert.ToInt32(objDs.Tables(1).Rows(0)("MaxCapacity").ToString())
                End If
            End If

            CmdTextCount = "select count(*) as NStudents from CoachReg where EventYear=" & ddlYear.SelectedValue & " and EventID=13 and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and CMemberID=" & fromCoachId & " and Approved='Y' and Level='" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & ";  "

            CmdTextCount = CmdTextCount & "  select count(*) as NStudents from CoachReg CR  inner join calsignup cs on (cs.MemberID=CR.CMemberID and CR.Approved='Y' and cs.Day='" & ddlFromDay.SelectedValue & "' and cs.SessionNo=CR.SessionNo and cs.EventYear=" & ddlYear.SelectedValue & " and cs.Accepted='Y') where cr.EventYear=" & ddlYear.SelectedValue & " and cr.EventID=13 and cr.ProductGroupID=" & ddlProductGroup.SelectedValue & " and cr.ProductID=" & ddlProduct.SelectedValue & " and cr.CMemberID=" & toCoachId & " and cr.Approved='Y' and cr.Level='" & ddlFromLevel.SelectedValue & "' and cs.Day='" & ddlFromDay.SelectedValue & "';"

            objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, CmdTextCount)
            Dim FromCoacgApprovedCount As Integer = 0
            Dim ToCoachApprovedCount As Integer = 0
            If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                If objDs.Tables(0).Rows.Count > 0 Then
                    FromCoacgApprovedCount = Convert.ToInt32(objDs.Tables(0).Rows(0)("NStudents").ToString())
                    ToCoachApprovedCount = Convert.ToInt32(objDs.Tables(1).Rows(0)("NStudents").ToString())
                End If
            End If
            Dim TotCount As Integer = 0
            TotCount = FromCoacgApprovedCount + ToCoachApprovedCount
            If (ToCoachmaxCap < TotCount) Then
                hdnIsCapacityExceed.Value = "Yes"
            Else
                BulkCoachTransfer()
            End If
            If hdnIsCapacityExceed.Value = "Yes" Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmTransfer(" & TotCount & "," & ToCoachmaxCap & ");", True)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Public Sub BulkCoachTransfer()
        Try

            Dim bIsValid As Boolean = True

            Dim fromCoachId As Integer, toCoachId As Integer
            Dim fromCoachSessionNo As Integer = 1
            Dim ToCoachSessionNo As Integer = 1
            fromCoachId = ddlFromCoach.SelectedValue
            toCoachId = ddlToCoach.SelectedValue
            Dim FromCoachMeetingKey As String = Nothing

            Dim cmdText As String = ""
            cmdText = "select isnull([Duration],'') as Duration,isnull([Begin],'') as BeginTime,isnull([End],'') as EndTime,isnull(Cycle,0) as Cycle,Vl.Vroom,isnull(VL.UserId,'') UserId,isnull(Vl.Pwd,'') Pwd,HostJoinURL,MeetingKey,MeetingPwd,[Status], SessionNo from CalSignUp CS inner join VirtualRoomLookUp VL on(CS.Vroom=VL.VRoom) where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)

            Dim Duration As String = "", BeginTime As TimeSpan, EndTime As TimeSpan, Cycle As Integer = 0, Vroom As Integer = 0, UserID As String = "", PWD As String = "", HostMeetingUrl As String = "", MeetingKey As String = "", MeetingPwd As String = "", Status As String = ""

            While dr.Read()
                Duration = dr("Duration")
                BeginTime = dr("BeginTime")
                EndTime = dr("EndTime")
                Cycle = dr("Cycle")
                If dr("Vroom") Is Nothing Then
                    Vroom = 0
                Else
                    Vroom = Convert.ToInt32(dr("Vroom").ToString())
                End If

                UserID = dr("UserId")
                PWD = dr("pwd")
                fromCoachSessionNo = dr("SessionNo")
                hdnFromCoachWebExID.Value = UserID
                hdnFromCoachWebExPwd.Value = PWD
                Dim index As Integer = 0
                index = dr.GetOrdinal("HostJoinURL")

                If dr.IsDBNull(index) Then
                    HostMeetingUrl = Nothing
                Else
                    HostMeetingUrl = dr("HostJoinURL")
                End If


                index = dr.GetOrdinal("MeetingKey")
                If dr.IsDBNull(index) Then
                    MeetingKey = Nothing
                Else
                    MeetingKey = dr("MeetingKey")
                    FromCoachMeetingKey = dr("MeetingKey").ToString()
                End If

                index = dr.GetOrdinal("MeetingPwd")
                If dr.IsDBNull(index) Then
                    MeetingPwd = Nothing
                Else
                    MeetingPwd = dr("MeetingPwd")
                End If
                index = dr.GetOrdinal("Status")
                If dr.IsDBNull(index) Then
                    Status = Nothing
                Else
                    Status = dr("Status")
                End If



            End While

            '' Get from coach students list
            cmdText = "select case when cr.adultid is null then isnull(Ch.Email,'') else ip.email end as ChildEMail, I.Email ToCoachMail,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day , C.TIMe,C.level,C.ProductID, case when cr.adultid is null then Ch.First_Name + ' ' + Ch.Last_Name else IP.FirstName+' '+ IP.LastName end as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,I2.AutoMemberId as ParentId FROM coachreg cr inner join IndSpouse I on cr.CMemberID=I.AutoMemberID INNER JOIN CalSignUp C ON c.memberid = cr.Cmemberid Inner JOIN Product P ON Cr.ProductID=P.ProductID  left join Child Ch on Ch.Childnumber = cr.ChildNumber left join Indspouse Ip on IP.AutoMemberId = Cr.AdultId "
            cmdText = cmdText & " INNER JOIN IndSpouse I2 ON I2.AutoMemberID=cr.PMemberId WHERE c.MemberId =" & fromCoachId & "  And Cr.eventyear = " & ddlYear.SelectedValue & " and cr.productgroupid=" & ddlProductGroup.SelectedValue & " and cr.productid=" & ddlProduct.SelectedValue & " and C.Semester = '" & ddlFromPhase.SelectedValue & "' and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and cr.Approved='y'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
            ViewState("StudentList") = ds.Tables(0)


            Dim CoachChange As String = "Y"
            Dim SessionCreationDate As String = String.Empty

            Dim dsCH As DataSet
            Dim dtSessionCreationDate As New DateTime()
            Dim dtTodayDate As New DateTime()
            '  Dim CurrDate As String = DateTime.Today.ToString("dd/MM/yyyy")
            Dim CurrDate As String = DateTime.Today.ToString("MM/dd/yyyy")
            dtTodayDate = Convert.ToDateTime(CurrDate)
            cmdText = "select distinct StartDate from CoachingDateCal where EventID=13 and EventYear=" & ddlYear.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ScheduleType='Term'"
            dsCH = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If dsCH.Tables.Count > 0 Then
                ' Condition to check if dataset tables contains data or not
                If dsCH.Tables(0).Rows.Count > 0 Then

                    SessionCreationDate = dsCH.Tables(0).Rows(0)("StartDate").ToString()
                    'dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("dd/MM/yyyy")
                    dtSessionCreationDate = Convert.ToDateTime(SessionCreationDate.ToString()).ToString("MM/dd/yyyy")
                End If
            Else

            End If
            If dsCH.Tables(0).Rows.Count > 0 Then

                CoachChange = "Y"
                If dtTodayDate >= dtSessionCreationDate Then

                    trWebExEntry.Visible = False
                    cmdText = "select MeetingKey,MeetingPwd,Vl.UserID,Vl.PWD, SessionNo from CalSignUp CS inner join VirtualRoomLookUp VL on(CS.Vroom=VL.Vroom) where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "'  and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
                    Dim ToCoachMeetingKey As String = String.Empty
                    Dim ToCoachWebExID As String = String.Empty
                    Dim ToCoachPwd As String = String.Empty
                    Dim ToCoachMeetingPwd As String = String.Empty

                    Dim dsToCoach As DataSet
                    dsToCoach = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                    If (dsToCoach.Tables.Count > 0) Then
                        If dsToCoach.Tables(0).Rows.Count > 0 Then
                            If dsToCoach.Tables(0).Rows(0)("MeetingKey").ToString() <> "" Then
                                ToCoachMeetingKey = dsToCoach.Tables(0).Rows(0)("MeetingKey").ToString()
                                ToCoachWebExID = dsToCoach.Tables(0).Rows(0)("UserID").ToString()
                                ToCoachPwd = dsToCoach.Tables(0).Rows(0)("PWD").ToString()
                                hdnToCoachMeetingKey.Value = ToCoachMeetingKey
                                hdnToCoachWebExID.Value = ToCoachWebExID
                                hdnToCoachWebExPwd.Value = ToCoachPwd
                                hdnToCoachMeetingPwd.Value = ToCoachPwd
                                ToCoachSessionNo = dsToCoach.Tables(0).Rows(0)("SessionNo").ToString()
                            End If

                        End If
                    End If
                    ToCoachSessionNo = Convert.ToInt32(lblToSessionNo.Text)
                    If FromCoachMeetingKey <> "" And ToCoachMeetingKey <> "" Then
                        tblSessionCreation.Visible = False
                        hdnFromCoachMeetingKey.Value = FromCoachMeetingKey
                        'From Coach and To Coach both are registered for a web conf sessions
                        SwitchBulkCoachSituation1(fromCoachId, toCoachId, ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, Session("loginID"), ToCoachSessionNo)
                    ElseIf FromCoachMeetingKey <> "" And ToCoachMeetingKey = "" Then
                        tblSessionCreation.Visible = False
                        'From Coach registered for a session but To Coach doesn't registered for a session
                        SwitchBulkCoachSituation2(fromCoachId, toCoachId, FromCoachMeetingKey, HostMeetingUrl, hdnFromCoachWebExID.Value, hdnFromCoachWebExPwd.Value, Session("loginID"), BeginTime, EndTime, Duration, PWD, MeetingPwd, Status, Cycle, Vroom, ToCoachSessionNo)

                    ElseIf FromCoachMeetingKey = "" And ToCoachMeetingKey <> "" Then
                        tblSessionCreation.Visible = False
                        'To Coach registered for a session but From coach doesn't registered for a session
                        SwitchBulkCoachSituation3(fromCoachId, toCoachId, ToCoachMeetingKey, ToCoachWebExID, ToCoachPwd, Session("loginID"), ToCoachSessionNo)
                    ElseIf FromCoachMeetingKey = "" And ToCoachMeetingKey = "" Then
                        tblSessionCreation.Visible = True
                        'From Coach and To Coach both are not registered for a session.
                        SwitchBulkCoachSituation4(fromCoachId, toCoachId, FromCoachMeetingKey, HostMeetingUrl, hdnFromCoachWebExID.Value, hdnFromCoachWebExPwd.Value, Session("loginID"), BeginTime, EndTime, Duration, PWD, MeetingPwd, Status, Cycle, Vroom, ToCoachSessionNo)

                    End If

                ElseIf dtTodayDate < dtSessionCreationDate Then
                    'Update in Coachreg table to change From memberid with To memberid
                    trWebExEntry.Visible = False
                    cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ", CMEMBERID=" & toCoachId & ", SessionNo=" & lblToSessionNo.Text & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                    'Update Calsignup --Update Details of ToCoach with From Coach 
                    cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",[Duration] = " & Duration & " , [Begin] ='" & BeginTime.ToString() & "', [End] ='" & EndTime.ToString() & "', Cycle=" & Cycle & ", vroom =" & Vroom & " ,[UserId]='" & UserID & "', Pwd ='" & PWD & "',HostJoinURL='" & HostMeetingUrl & "',MeetingKey=null,MeetingPwd=null, Status=null  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                    'Update From coach Accepted =null 
                    cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                    SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                End If

            Else
                'Update in Coachreg table to change From memberid with To memberid
                trWebExEntry.Visible = False
                cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & ", SessionNo=" & lblToSessionNo.Text & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                'Update Calsignup --Update Details of ToCoach with From Coach 
                cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",[Duration] = " & Duration & " , [Begin] ='" & BeginTime.ToString() & "', [End] ='" & EndTime.ToString() & "', Cycle=" & Cycle & ", vroom =" & Vroom & " ,[UserId]='" & UserID & "', Pwd ='" & PWD & "',HostJoinURL='" & HostMeetingUrl & "',MeetingKey='" & FromCoachMeetingKey & "',MeetingPwd=null, Status=null  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                'Update From coach Accepted =null 
                cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null,HostJoinURL=null,MeetingKey=null,MeetingPwd=null,[Status]=null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Semester = '" & ddlFromPhase.SelectedValue & "' and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)


                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            End If
            Try
                'Send mail to parent(Father,Mother)/child/fromcoach/tocoach
                SendMailInfo(fromCoachId, toCoachId)
            Catch ex As Exception
                CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
            End Try
            'FillFromCoachStudents()
            ' FillToCoachStudents()
            FillToCoachStudentsALL()
            'Insert changes on BulkChangeCoachLog Table
            LogCoachDetails(fromCoachId, toCoachId, ddlFromDay.SelectedValue, ddlFromTime.SelectedValue, Vroom, UserID, PWD)

            'Reset Page
            ' commented by Bindhu on Sep16_2016 to hide the page refreshment
            'FillFromDetails()
            'ddlFromCoach_SelectedIndexChanged(ddlFromCoach, New EventArgs)
            'ddlFromCoach.SelectedValue = "0"
            ShowMessage("Successfully completed", False)

            lblMsg.Text = "Successfully completed"
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try

    End Sub

    Private Sub FillToCoachSessionNo()
        Try


            Dim dsCoach As DataSet
            Dim cmdtext As String = "select distinct C.MemberId,I.FirstName + ' ' + I.LastName as CoachName,I.firstname as firstname,I.lastname as lastname,C.SignUpId as SignUpId, C.SessionNo from CalSignUp C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.Semester='" & ddlFromPhase.SelectedValue & "' and C.Level= '" & ddlFromLevel.SelectedValue & "' and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time = '" & ddlFromTime.SelectedValue & "' and C.Accepted='Y' and C.MemberId =" & ddlToCoach.SelectedValue & " order by lastname,firstname"

            dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, cmdtext)

            If (dsCoach.Tables.Count > 0) Then
                If (dsCoach.Tables(0).Rows.Count > 0) Then
                    lblToSessionNo.Text = dsCoach.Tables(0).Rows(0)("SessionNo").ToString()
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try


    End Sub

    Public Sub createZoomMeeting(title As String, hostID As String)
        Try


            Dim URL As String = String.Empty

            Dim service As String = "1"

            URL = "https://api.zoom.us/v1/meeting/create"

            Dim urlParameter As String = String.Empty


            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""

            urlParameter += "&topic=" + title + ""
            urlParameter += "&password=training"
            urlParameter += "&type=3"
            urlParameter += "&option_jbh=true"
            urlParameter += "&option_host_video=false"
            urlParameter += "&option_audio=Both"


            makeZoomAPICall(urlParameter, URL, service)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Public Sub makeZoomAPICall(urlParameters As String, URL As String, serviceType As String)
        Try


            Dim objRequest As HttpWebRequest = DirectCast(WebRequest.Create(URL), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = urlParameters.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"

            ' post data is sent as a stream
            Dim myWriter As StreamWriter = Nothing
            myWriter = New StreamWriter(objRequest.GetRequestStream())
            myWriter.Write(urlParameters)
            myWriter.Close()

            ' returned values are returned as a stream, then read into a string
            Dim postResponse As String
            Dim objResponse As HttpWebResponse = DirectCast(objRequest.GetResponse(), HttpWebResponse)
            Using responseStream As New StreamReader(objResponse.GetResponseStream())
                postResponse = responseStream.ReadToEnd()

                responseStream.Close()
            End Using
            If serviceType = "1" Then

                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                hdnTrainingSessionKey.Value = json("id").ToString()
                hdnHostMeetingURL.Value = json("start_url").ToString()
                hdnMeetingURL.Value = json("join_url").ToString()
                hdnMeetingStatus.Value = "SUCCESS"



                'lblMessage.Text = "Makeup Session deleted successfully"

            ElseIf serviceType = "3" Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnFromCoachMeetingKey.Value + "")

            End If
        Catch ex As Exception
            hdnMeetingStatus.Value = "Failure"
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try

    End Sub

    Public Sub SwitchStudentAndCreateTrainingSession(UserID As String, SignUpId As String)
        Try


            Dim cmdText As String = Nothing
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],Vl.UserID,Vl.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,Vl.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" & ddlYear.SelectedValue & "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookUp VL on(VL.Vroom=CS.VRoom) where Accepted='Y' and SignUpId=" & SignUpId & ""

            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows

                        Dim hostID As String = String.Empty
                        hostID = dr("HostID").ToString()
                        Dim WebExID As String = dr("UserID").ToString()
                        Dim WebExPwd As String = dr("PWD").ToString()
                        Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                        Dim ScheduleType As String = dr("ScheduleType").ToString()

                        Dim year As String = dr("EventYear").ToString()
                        Dim eventID As String = dr("EventID").ToString()

                        Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                        Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                        Dim ProductID As String = dr("ProductID").ToString()
                        Dim ProductCode As String = dr("ProductCode").ToString()
                        Dim Semester As String = dr("Semester").ToString()
                        Dim Level As String = dr("Level").ToString()
                        Dim Sessionno As String = dr("SessionNo").ToString()
                        Dim CoachID As String = dr("MemberID").ToString()
                        Dim MeetingPwd As String = "training"

                        Dim Time As String = dr("Time").ToString()
                        Dim Day As String = dr("Day").ToString()
                        Dim STime As String = dr("Begin").ToString()
                        Dim ETime As String = dr("End").ToString()

                        Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                        Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                        'string endTime = "01:00 AM";
                        'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                        'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                        'double hours = (time1 - time2).TotalHours;

                        Dim timeZoneID As String = ddlTimeZone.SelectedValue
                        Dim TimeZone As String = ddlTimeZone.SelectedItem.Text
                        SignUpId = dr("SignupID").ToString()
                        Dim Mins As Double = 0.0
                        Dim dFrom As DateTime
                        Dim dTo As DateTime

                        Dim sDateFrom As String = STime
                        Dim sDateTo As String = ETime
                        If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                            Dim TS As TimeSpan = dTo - dFrom

                            Mins = TS.TotalMinutes
                        End If
                        Dim durationHrs As String = Mins.ToString("0")
                        If durationHrs.IndexOf("-") > -1 Then
                            durationHrs = "188"
                        End If

                        If timeZoneID = "4" Then
                            TimeZone = "EST/EDT – Eastern"
                        ElseIf timeZoneID = "7" Then
                            TimeZone = "CST/CDT - Central"
                        ElseIf timeZoneID = "6" Then
                            TimeZone = "MST/MDT - Mountain"
                        End If

                        Dim CoachName As String = String.Empty

                        CoachName = dr("CoachName").ToString()
                        Dim meetingTitle As String = String.Empty
                        meetingTitle = CoachName & "-" & ProductCode & "-" & Level & "-" & Sessionno
                        If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then

                            createZoomMeeting(meetingTitle, hostID)
                            If hdnMeetingStatus.Value = "SUCCESS" Then

                                Dim meetingURL As String = hdnHostMeetingURL.Value
                                cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & EndDate & "','" & STime & "','" & ETime & "'," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Semester & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "', 'Recurring Meeting'"

                                Dim objDs As New DataSet()
                                objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                                    If objDs.Tables(0).Rows.Count > 0 Then
                                        If Convert.ToInt32(objDs.Tables(0).Rows(0)("Retval").ToString()) > 0 Then

                                            Dim dsChild As New DataSet()
                                            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ProductGroupID & "' and CR.ProductID='" & ProductID & "' and CR.CMemberID=" & CoachID & " and CR.EventYear=" & year & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & year & " and ProductGroupID='" & ProductGroupID & "' and ProductID='" & ProductID & "' and CMemberID=" & CoachID & " and Approved='Y' and Level = '" & Level & "' and SessionNo=" & Sessionno & ") and CR.Level='" & Level & "' and CR.SessionNo=" & Sessionno & ""
                                            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
                                            Dim ChidName As String = String.Empty
                                            Dim Email As String = String.Empty
                                            Dim City As String = String.Empty
                                            Dim Country As String = String.Empty
                                            Dim ChildNumber As String = String.Empty
                                            Dim CoachRegID As String = String.Empty

                                            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                                                If dsChild.Tables(0).Rows.Count > 0 Then
                                                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                                                        ChidName = drChild("Name").ToString()
                                                        Email = drChild("Email").ToString()
                                                        City = drChild("City").ToString()
                                                        Country = drChild("Country").ToString()
                                                        ChildNumber = drChild("ChildNumber").ToString()
                                                        CoachRegID = drChild("CoachRegID").ToString()

                                                        ' RegisterMeetingAttendee(hdnTrainingSessionKey.Value, WebExID, WebExPwd, "", ChidName, City, Email, Country)
                                                        If hdnMeetingStatus.Value = "SUCCESS" Then

                                                            ' GetJoinMeetingURL(hdnTrainingSessionKey.Value, WebExID, WebExPwd, hdnMeetingAttendeeID.Value, ChidName, Email, MeetingPwd)
                                                            Try


                                                                Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"
                                                                Try
                                                                    Dim strQuery As String = String.Empty
                                                                    strQuery = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL=@AttendeeJoinURL, ,CR.ModifyDate=Getdate(),CR.ModifiedBy=@UserID from CoachReg CR, CalSignUp C where C.SignUpID =@SignupID and CR.CoachRegID = @CoachRegID"
                                                                    Dim cmd As New SqlCommand(strQuery)
                                                                    cmd.Parameters.AddWithValue("@AttendeeJoinURL", hdnMeetingURL.Value)
                                                                    cmd.Parameters.AddWithValue("@UserID", Session("LoginID"))
                                                                    cmd.Parameters.AddWithValue("@SignupID", SignUpId)
                                                                    cmd.Parameters.AddWithValue("@CoachRegID", CoachRegID)
                                                                    Dim objNSF As NSFDBHelper = New NSFDBHelper()
                                                                    ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)

                                                                    If objNSF.InsertUpdateData(cmd) = True Then
                                                                        lblError.Text = "Updated Successfully"
                                                                    End If
                                                                Catch ex As Exception
                                                                    CmdChildUpdateText = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpId & " and CR.CoachRegID = " & CoachRegID & ""

                                                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                                                                End Try


                                                            Catch ex As Exception

                                                            End Try
                                                        End If
                                                    Next
                                                End If
                                            End If
                                        Else

                                        End If
                                    End If
                                End If


                            Else
                            End If
                        Else

                        End If
                    Next

                End If
            End If
            txtMeetingPwd.Text = ""
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Public Sub deleteMeeting()
        Try
            Dim sessionKey As String = hdnFromCoachMeetingKey.Value
            Dim URL As String = String.Empty
            Dim service As String = "3"
            URL = "https://api.zoom.us/v1/meeting/delete"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg"
            urlParameter += "&id=" + sessionKey + ""

            makeZoomAPICall(urlParameter, URL, service)


        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try

    End Sub


    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim sheet As IWorksheet
        Dim sFileName As String = "BulkChangeStudentList_" & Now.Date.ToShortDateString() & ".xls"

        If Not ViewState("FromCoachStud") Is Nothing Then
            sheet = book.Worksheets.Add()
            sheet.Name = "From Coach Students"
            sheet.Range("A1:Q1").MergeCells = True
            sheet.Range("A1").Value = "Table 1: From Coach (" & lblFromCoachName.Text & ") Students List"
            sheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
            sheet.Range("A1").Font.Bold = True
            sheet.Range("A1").Font.Color = Color.Blue
            'SNo	CoachRegID	SignUpID	ChildName	Grade	ChildEmail	FatherName	Email	HPhone	CPhone	City	State	Address1	Zip	JobTitle	ProductName	ProductCode	CoachName	Level	MaxCapacity	ChildNumber	Day	Time	StartDate	Enddate	PaymentReference	PaymentDate	Status	AttendeeJoinURL	MeetingKey	UserID	Pwd	CoacherName
            Dim ds As DataSet = ViewState("FromCoachStud")
            Dim i As Integer, colCount, rowCount As Integer
            Dim arr() As String = {"A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z", "AA", "AB", "AC", "AD", "AE", "AF", "AG", "AH", "AI"}
            colCount = ds.Tables(0).Columns.Count
            rowCount = ds.Tables(0).Rows.Count
            For i = 0 To colCount - 1
                sheet.Range(arr(i) & "2").Value = ds.Tables(0).Columns(i).ColumnName
                sheet.Range(arr(i) & "2").Font.Bold = True
            Next
            For r = 0 To rowCount - 1
                Dim dr As DataRow = ds.Tables(0).Rows(r)
                For i = 0 To colCount - 1
                    sheet.Range(arr(i) & CStr(r + 3)).Value = dr(i)
                Next
            Next




            If Not ViewState("ToCoachStud") Is Nothing Then

                sheet = book.Worksheets.Add()
                sheet.Name = "To Coach Students"
                sheet.Range("A1:Q1").MergeCells = True
                sheet.Range("A1").Value = "Table 2: To Coach (" & lblToCoachName.Text & ") Students List"
                sheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                sheet.Range("A1").Font.Bold = True
                sheet.Range("A1").Font.Color = Color.Blue
                'SNo	CoachRegID	SignUpID	ChildName	Grade	ChildEmail	FatherName	Email	HPhone	CPhone	City	State	Address1	Zip	JobTitle	ProductName	ProductCode	CoachName	Level	MaxCapacity	ChildNumber	Day	Time	StartDate	Enddate	PaymentReference	PaymentDate	Status	AttendeeJoinURL	MeetingKey	UserID	Pwd	CoacherName
                ds = ViewState("ToCoachStud")
                colCount = ds.Tables(0).Columns.Count
                rowCount = ds.Tables(0).Rows.Count
                For i = 0 To colCount - 1
                    sheet.Range(arr(i) & "2").Value = ds.Tables(0).Columns(i).ColumnName
                    sheet.Range(arr(i) & "2").Font.Bold = True
                Next
                For r = 0 To rowCount - 1
                    Dim dr As DataRow = ds.Tables(0).Rows(r)
                    For i = 0 To colCount - 1
                        sheet.Range(arr(i) & CStr(r + 3)).Value = dr(i)
                    Next
                Next
            End If


            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" + sFileName)
            book.SaveAs(Response.OutputStream)
            Response.End()

        End If

        'Response.Clear()
        'Response.AddHeader("content-disposition", "attachment;filename=BulkChangeStudentList_" & Now.Date.ToShortDateString() & ".xls")
        'Response.Charset = ""
        'Response.ContentType = "application/vnd.xls"
        'Dim stringWrite As New System.IO.StringWriter()
        'Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        'table1.RenderControl(htmlWrite)

        'table2.RenderControl(htmlWrite)
        ''Dim DG As New DataGrid
        ''LoadData(DG)
        ''DG.RenderControl(htmlWrite)
        'Response.Write(stringWrite.ToString())
        'Response.[End]()
    End Sub

    Private Sub LoadData(dg As DataGrid)
        Try


            Dim SQLStr As String = "select ROW_NUMBER() Over (ORDER BY Co.LastName,Co.FirstName,P.ProductCode,Ch.LAST_NAME,Ch.FIRST_NAME) as SNo, CR.CoachRegID,C.SignUpID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName,CR.Grade,Ch.Email as ChildEmail,I1.FirstName +' '+ I1.LastName as FatherName,I1.Email as Email ,I1.HPhone,I1.CPhone ,I1.City,I1.State,I1.Address1,I1.Zip,I1.Career as JobTitle, Case when P.CoachName is null then P.ProductCode Else P.CoachName End as ProductName,P.ProductCode,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day as Day , CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,CR.PaymentReference,CR.PaymentDate,Case when CR.PaymentReference IS NULL then 'Pending' Else 'Paid' End as Status, CR.AttendeeJoinURL, C.MeetingKey, C.UserID, C.Pwd "

            SQLStr = SQLStr & ", Co.FirstName + ' ' + Co.LastName CoacherName "
            SQLStr = SQLStr & " from Coachreg CR Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Left Join IndSpouse Co on Co.AutomemberId=CR.CMemberId INNER JOIN IndSpouse I1 ON Ch.MEMBERID = I1.AutoMemberID Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & " CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and  CR.Level=C.Level AND C.EventYear = CR.EventYear AND C.Semester=CR.Semester and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID AND CR.EventYear=" & ddlYear.Text & " where "
            If ddlFromCoach.SelectedValue <> "0" Then
                SQLStr = SQLStr & " CR.CMemberid in (" & ddlFromCoach.SelectedValue
            End If
            If ddlToCoach.Items.Count > 0 And ddlFromCoach.SelectedValue <> "0" Then
                SQLStr = SQLStr & "," & ddlToCoach.SelectedValue & ")"
            Else
                SQLStr = SQLStr & ")"
            End If
            If ddlFromSession.SelectedValue > 0 Then
                SQLStr = SQLStr & " AND CR.SessionNo= " & ddlFromSession.SelectedValue
            End If
            If ddlFromLevel.SelectedValue <> "Select" And ddlFromLevel.SelectedValue <> "" And ddlFromLevel.SelectedValue <> "0" Then
                SQLStr = SQLStr & " AND CR.Level= '" & ddlFromLevel.SelectedValue & "'"
            End If
            If ddlProductGroup.SelectedValue <> "0" Then
                SQLStr = SQLStr & " AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue
            ElseIf ddlProduct.SelectedValue <> "0" Then
                SQLStr = SQLStr & "  AND CR.ProductGroupID= " & ddlProductGroup.SelectedValue & " AND CR.ProductID= " & ddlProduct.SelectedValue & ""
            End If
            SQLStr = SQLStr & " AND CR.Semester='" & ddlFromPhase.SelectedValue & "' AND CR.Approved='Y' and C.Accepted='Y' ORDER BY  "

            SQLStr = SQLStr & " C.Level,C.Day,Co.LastName,Co.FirstName,P.ProductCode,CR.SessionNo,Ch.LAST_NAME,Ch.FIRST_NAME"


            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLStr)
            dg.DataSource = ds.Tables(0)
            dg.DataBind()

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Bulk Change Coaching")
        End Try
    End Sub

    Private Sub loadSemester()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2

            DDlSemester.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        DDlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue)
    End Sub

    Protected Sub DDlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        FillProductGroup()
        FillProduct()
    End Sub
End Class

