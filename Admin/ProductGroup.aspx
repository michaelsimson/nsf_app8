
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="ProductGroup.aspx.cs" Inherits="ProductGroup" Title="ProductGroup List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
		<data:EntityGridView ID="GridView1" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="ProductGroupDataSource"
			DataKeyNames="ProductGroupId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_ProductGroup.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />				
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode"  />
				<asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode"  />
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"  />
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No ProductGroup Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
		<br />
		<asp:Button runat="server" ID="btnProductGroup" OnClientClick="javascript:location.href='ProductGroupEdit.aspx'; return false;" Text="Add New"></asp:Button>
			
		<data:EventDataSource ID="EventDataSource1" runat="server"
			SelectMethod="GetAll"
		/>

		<data:EventDataSource ID="EventDataSource2" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ProductGroupDataSource ID="ProductGroupDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ProductGroupDataSource>
	    		
</asp:Content>




 

 
 
 