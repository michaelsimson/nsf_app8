<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="ChangeWrkshpCenter.aspx.vb" Inherits="VRegistration.ChangeWrkshpCenter"%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div>
			<table id="tblLogin" width="1004" runat="server">
				<tr>
					
					<td class="ContentSubTitle" vAlign="top" noWrap align="center" colspan="2">
						<h1>Change Workshop Chapter</h1>
					</td>
				</tr>
				 <tr>
			        <td colspan="4">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td>&nbsp;</td>
			    </tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
					<td><asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox><asp:button id="btnFindContest" runat="server" CssClass="FormButtonCenter" Text="Find Subject"></asp:button><br>
						&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
							Display="Dynamic"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td colSpan="2" align="center">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text=""></asp:Label></td></tr> 
				<tr>
					<td colSpan="2">
						<table>
							<tr>
								<td><asp:datagrid id="dgSelectedContests" runat="server" CssClass="GridStyle" Width="100%" AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="RegId">
										<FooterStyle CssClass="GridFooter"></FooterStyle>
										<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
										
										<AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
										<ItemStyle  HorizontalAlign="Left" Wrap="true"></ItemStyle>
										<HeaderStyle Wrap="true"  Width="100%"></HeaderStyle>
										<Columns>
											<asp:EditCommandColumn UpdateText="Update /" CancelText="Cancel"
												EditText="Change Center">
                                                <HeaderStyle CssClass="GridHeader" />
                                            </asp:EditCommandColumn>
                                            <asp:TemplateColumn HeaderText="New Center" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
                                                <EditItemTemplate>
                                                    <asp:DropDownList runat="server" ID="ddlNewCenters" CssClass="SmallFont"></asp:DropDownList>
                                                </EditItemTemplate>
                                            </asp:TemplateColumn> 
											<asp:TemplateColumn HeaderText="Child Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
												<ItemTemplate>
													<asp:Label id="lblChildName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ContestantName") %>'>
													</asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblChildID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ChildNumber") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblContestYear" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblContestDate" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"EventDate") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblChapterID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ChapterID") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblRegistrationDeadline" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"RegistrationDeadline") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblECalendarId" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ECalendarId") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductGroupId" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductGroupId") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductGroupCode" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblProductCode" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
													<asp:Label runat="server" Visible="false" ID="lblMemberID" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
												
												</ItemTemplate>												
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Current Center" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
											    <ItemTemplate>
											        <asp:Label ID="lblCurrenctCenter" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ChapterCode") %>'></asp:Label>
											    </ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="WorkShop" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
												<ItemTemplate>
													<asp:Label id="lblContestRegistered" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "EventDesc") & "<BR>" & DataBinder.Eval(Container.DataItem,"EventDate")  %>'>													
													
													</asp:Label>
												</ItemTemplate>
												
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Contact Info" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
												<ItemTemplate>
													<asp:Label ID="lblContactInfo" Runat=server CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ContactInfo")  %>'>
													</asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Parent Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
											    <ItemTemplate>
											        <asp:Label runat="server" ID="lblName" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"VolunteerName") %>'></asp:Label>
											    </ItemTemplate>
											</asp:TemplateColumn>
											<asp:TemplateColumn HeaderText="Contest Info" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
												<ItemTemplate>
													<asp:Label Runat=server ID=lblPaymentInfo CssClass="SmallFont"> </asp:Label>
												</ItemTemplate>
											</asp:TemplateColumn>
										</Columns>
										<PagerStyle HorizontalAlign="Left" ForeColor="#003399" BackColor="#99CCCC" Mode="NumericPages"></PagerStyle>
									</asp:datagrid></td>
							</tr>
							<tr  bgcolor="#FFFFFF">
				    <td colspan="2" class="Heading">
				        <asp:Label runat="server" ID="lblMessage" Visible="false"></asp:Label>
				    </td>
				</tr>
						</table>
					</td>
				</tr>
				
			</table>
		</div>
	</asp:Content>
