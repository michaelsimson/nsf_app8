
#region Imports...
using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Web;

using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using nsf.Web.UI;
#endregion

public partial class ProductEdit : System.Web.UI.Page
{

    protected void IsDuplicate(object source, ServerValidateEventArgs value)
    {

        // check if update operation
        if (((Button)(Page.Master.FindControl("Content_main").
                     FindControl("FormView1$UpdateButton"))).Visible == true)
        {
            value.IsValid = true;
            return;
        }

        // Declare database objects such as connection,
        // command and transaction
        string productCode = value.Value;
        string eventCode = (((DropDownList) Page.Master.FindControl("Content_main").
                            FindControl("FormView1$dataEventCode"))).SelectedItem.Value;
        string productGroupCode = (((DropDownList)Page.Master.FindControl("Content_main").
                            FindControl("FormView1$dataProductGroupCode"))).SelectedItem.Value;       

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ProductCode from Product where ProductCode ='");
        queryStr.Append(productCode);
        queryStr.Append("' AND EventCode = '");
        queryStr.Append(eventCode);
        queryStr.Append("' AND ProductGroupcode = '");
        queryStr.Append(productGroupCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                /* if there is an entry, then there is a duplicate */
                /* So make the isValid false */
                value.IsValid = (reader.Read()) ? false : true;
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }
    }

    void EventCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string eventCode = ((DropDownList)Page.Master.
            FindControl("Content_main").FindControl("FormView1$dataEventCode")).SelectedItem.Value;

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT EventId from Event where EventCode ='");
        queryStr.Append(eventCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ((TextBox)(Page.Master.FindControl("Content_main").FindControl("FormView1$dataEventId"))).Text =
                        reader.GetInt32(0).ToString();
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }

    }

    void ProductGroupCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string productGroupCode = ((DropDownList)Page.Master.
            FindControl("Content_main").FindControl("FormView1$dataProductGroupCode")).SelectedItem.Value;

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ProductGroupId from ProductGroup where ProductGroupCode ='");
        queryStr.Append(productGroupCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ((TextBox)(Page.Master.FindControl("Content_main").FindControl("FormView1$dataProductGroupId"))).Text =
                        reader.GetInt32(0).ToString();
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }

    }

	protected void Page_Load(object sender, EventArgs e)
	{		
		FormUtil.RedirectAfterInsertUpdate(FormView1, "ProductEdit.aspx?{0}", ProductDataSource);
		FormUtil.RedirectAfterAddNew(FormView1, "ProductEdit.aspx");
		FormUtil.RedirectAfterCancel(FormView1, "Product.aspx");
		FormUtil.SetDefaultMode(FormView1, "ProductId");

        CustomValidator cv = Page.Master.FindControl("Content_main").
FindControl("FormView1$ProductCodeValidator") as CustomValidator;
        cv.ServerValidate +=
                      new ServerValidateEventHandler(this.IsDuplicate);

        DropDownList ddl1 = Page.Master.FindControl("Content_main").
FindControl("FormView1$dataEventCode") as DropDownList;
        ddl1.SelectedIndexChanged += new EventHandler(EventCode_SelectedIndexChanged);
        DropDownList ddl2 = Page.Master.FindControl("Content_main").
FindControl("FormView1$dataProductGroupCode") as DropDownList;
        ddl2.SelectedIndexChanged += new EventHandler(ProductGroupCode_SelectedIndexChanged);

        if (!Page.IsPostBack)
        {

            TextBox tbx1 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreateDate") as TextBox;
            TextBox tbx2 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreatedBy") as TextBox;
            TextBox tbx3 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifyDate") as TextBox;
            TextBox tbx4 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifiedBy") as TextBox;

            /* Make the audit trail related fields readonly */
            tbx1.ReadOnly = true;
            tbx2.ReadOnly = true;
            tbx3.ReadOnly = true;
            tbx4.ReadOnly = true;

            (((TextBox)Page.Master.FindControl("Content_main").
   FindControl("FormView1$dataEventId"))).ReadOnly = true;
            (((TextBox)Page.Master.FindControl("Content_main").
               FindControl("FormView1$dataProductGroupId"))).ReadOnly = true;

            Button tb5 = Page.Master.FindControl("Content_main").
                   FindControl("FormView1$InsertButton") as Button;

            System.DateTime n = System.DateTime.Now;

            // Insert operation
            if (tb5.Visible == true)
            {
                tbx1.Text = n.ToString();
                tbx2.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
            else
            {
                tbx3.Text = n.ToString();
                tbx4.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }

        } // Not postback

	}
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["ProductId"] != null)
           {
               return string.Format("ProductId='{0}'", Request.QueryString["ProductId"].ToString());
           }
           return string.Empty;
       }
    }

}



 