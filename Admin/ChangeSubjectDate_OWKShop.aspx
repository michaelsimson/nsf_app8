﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFInnerMasterPage.master" CodeFile="ChangeSubjectDate_OWKShop.aspx.cs" Inherits="Admin_ChangeSubjectDate_OWKShop" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script language="javascript" type="text/javascript">
        var accessToken;
        var orgAnizerKey;
        $(function (e) {
            accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
            orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;
          
        });
        function confirmMsgToUpdate(text) {
            if (confirm("Date has passed, but are sure you want to change the " + text + "?")) {
                document.getElementById('<%= btnConfirmChangeDate.ClientID%>').click();
            }
        }
        function deleteRegistration() {
            var registrantKey = document.getElementById("<%=hdnRegistrantKey.ClientID%>").value;
            var webinarKey = document.getElementById("<%=hdnWebinarkey.ClientID%>").value;
            var regId = document.getElementById("<%=hdnRegId.ClientID%>").value;
            
            accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
            orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;

            $.ajax({
                url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/registrants/" + registrantKey + "",
                beforeSend: function (xhr) {
                    xhr.setRequestHeader("Authorization", accessToken);
                },
                type: 'DELETE',
                crossDomain: true,
                dataType: 'json',
                contentType: 'application/json',
                processData: false,

                success: function (data) {

                    registerAttendees(regId);

                },
                error: function (data) {

                }
            });
        }
        function registerAttendees(regId) {


            var jsonData = JSON.stringify({
                RegID: regId
            });
            accessToken = document.getElementById("<%=hdnAccessToken.ClientID%>").value;
            orgAnizerKey = document.getElementById("<%=hdnOrganizerKey.ClientID%>").value;

            $.ajax({
                type: "POST",
                url: "../TestGotoWebinar.aspx/ListAttendeesFromChangeDate",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    var onlineWsCalId;
                    var i = 0;
                    var firstName;
                    var lastName;
                    var email;
                    var source = "NSF";
                    var address;
                    var city;
                    var state;
                    var zipCode;
                    var country;
                    var phone;
                    var onlineWsCalId;
                    var regId;
                    var webinarKey;
                    var spouseEmail;
                    var secondaryEmail;
                    var parentName;
                    var parentEmail;
                    var parentPhone;
                    var childName;
                    var date;
                    var time;
                    var productName;

                    $.each(data.d, function (index, value) {
                      
                        i++;
                        firstName = value.FirstName
                        lastName = value.LastName;
                        email = value.Email;
                        source = "NSF";
                        address = value.Address1;
                        city = value.City;
                        state = value.State;
                        zipCode = value.Zip;
                        country = value.Country;
                        phone = value.Phone;
                        onlineWsCalId = value.OnlineWsCalID;
                        regId = value.RegId;
                        webinarKey = value.Webinarkey;
                        spouseEmail = value.SpouseEmail;
                        secondaryEmail = value.SecondaryEmail;
                        parentName = value.ParentName;
                        parentEmail = value.Email;
                        parentPhone = value.Phone;
                        date = value.Date;
                        time = value.Time;
                        childName = firstName + " " + lastName;
                        productName = value.ProductName;


                        if (i > 1) {
                            if (value.Email != secondaryEmail) {
                                email = (secondaryEmail == "" ? spouseEmail : secondaryEmail);
                            } else {
                                email = spouseEmail;
                            }
                        } else if (i > 2) {
                            email = spouseEmail;
                        }

                        var jsonObject = { "firstName": firstName, "lastName": lastName, "email": email, "source": source, "address": address, "city": city, "state": state, "zipCode": zipCode, "Country": country, "phone": phone };
                        $.ajax({
                            url: "https://api.getgo.com/G2W/rest/organizers/" + orgAnizerKey + "/webinars/" + webinarKey + "/registrants",
                            beforeSend: function (xhr) {
                                xhr.setRequestHeader("Authorization", accessToken);
                            },
                            type: 'POST',
                            crossDomain: true,
                            dataType: 'json',
                            contentType: 'application/json',
                            processData: false,
                            data: JSON.stringify(jsonObject),
                            success: function (data) {

                                var obj = $.parseJSON(JSON.stringify(data));

                                updateJoinURLToAttendees(obj.joinUrl, obj.registrantKey, regId);

                            },
                            error: function (data) {
                                var obj = $.parseJSON(JSON.stringify(data.responseJSON));
                                var description = obj.description;

                                //sendEmailToAdmin(parentName, parentEmail, parentPhone, productName, date, time, childName, description)


                            }
                        });


                    });



                }, failure: function (e) {

                }
            });
        }
        function updateJoinURLToAttendees(joinURL, registrantKey, regId) {

            //var pgId = $("#selProductGroup").val();
            //var prdId = $("#selProduct").val();
            //var eventyear = $("#selEventyear").val();
            var jsonData = JSON.stringify({ ObjWebinar: { JoinURL: joinURL, RegistrantKey: registrantKey, RegId: regId } });


            $.ajax({
                type: "POST",
                url: "../TestGotoWebinar.aspx/UpdateJoinURLToAttendees",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    if (JSON.stringify(data.d) > 0) {
                        //listWebinars();
                    }
                }, failure: function (e) {

                }
            });
        }
    </script>
    <asp:Button ID="btnConfirmChangeDate" Style="display: none;" runat="server" OnClick="btnConfirmChangeDate_Click" />
    <table id="tblLogin" width="100%" runat="server">
        <tr>
            <td></td>

            <td class="ContentSubTitle" valign="top" nowrap align="left">
                <h1>Change Subject/Date</h1>
            </td>
            <td></td>
        </tr>
        <tr>
            <td colspan="4">
                <asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr runat="server" id="trParentEmail">
            <td class="ItemLabel" valign="top" nowrap align="right">Parent Email ID</td>
            <td>
                <asp:TextBox ID="txtUserId" runat="server" CssClass="SmallFont" Width="300" MaxLength="50"></asp:TextBox><br />
                <br />
                <asp:Button ID="btnFindContest" runat="server" CssClass="FormButtonCenter"
                    Text="Find Records" OnClick="btnFindContest_Click"></asp:Button><br>
                &nbsp;
						<asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
                            Display="Dynamic"></asp:RequiredFieldValidator><asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
                                ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>


        </tr>
    </table>
    <div align="center">
        <asp:Label ID="lblError" runat="server" ForeColor="Red" Text="Email is not available" Visible="false"></asp:Label>
    </div>
    <div>

        <div align="center"><span id="spnTitle" style="font-weight: bold;" runat="server" visible="false">Table 1: Your Registrations</span></div>

        <div style="clear: both;"></div>

        <asp:GridView ID="dgSelectedChild" runat="server" HeaderStyle-BackColor="#ffffcc"
            Style="width: 95%; margin: 0 auto;" AutoGenerateColumns="false"
            OnRowEditing="dgSelectedChild_RowEditing"
            OnRowCancelingEdit="dgSelectedChild_RowCancelingEdit"
            OnRowUpdating="dgSelectedChild_RowUpdating"
            DataKeyNames="childnumber" OnRowCommand="dgSelectedChild_RowCommand" OnRowDataBound="dgSelectedChild_RowDataBound">
            <Columns>

                <asp:TemplateField HeaderText="Action">

                    <ItemTemplate>
                        <asp:Button ID="btnEdit" Text="Change Subject" runat="server" CommandName="Edit Subject" />
                        <asp:Button ID="BtnChangeDate" Text="Change Date" runat="server" CommandName="Edit Date" />
                        <asp:Button ID="BtnUpdateDate" Text="Update" Visible="false" runat="server" CommandName="Update DateSubject" />
                        <asp:Button ID="BtnCancelDate" Text="Cancel" Visible="false" runat="server" CommandName="Cancel DateSubject" />

                        <div style="display: none;">
                            <asp:Label runat="server" ID="lblYear" Text='<%#Eval("EventYear") %>' />
                            <asp:Label runat="server" ID="lblPgID" Text='<%#Eval("ProductGroupID") %>' />
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#Eval("ProductGroupCode") %>' />
                            <asp:Label runat="server" ID="lblPrdCode" Text='<%#Eval("ProductCode") %>' />
                            <asp:Label runat="server" ID="lblPrdID" Text='<%#Eval("ProductID") %>' />

                            <asp:Label runat="server" ID="lblEventDate" Text='<%#Eval("EventDate") %>' />
                            <asp:Label runat="server" ID="lblRegId" Text='<%#Eval("RegId") %>' />

                        </div>
                    </ItemTemplate>

                    <EditItemTemplate>
                        <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" />

                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" />

                    </EditItemTemplate>


                </asp:TemplateField>



                <asp:TemplateField HeaderText="New Subject" Visible="false">
                    <ItemTemplate>
                        <asp:DropDownList ID="DDNewSubjects" Visible="false" runat="server" Width="150px" AutoPostBack="false">
                        </asp:DropDownList>
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:DropDownList ID="DDNewSubject" runat="server" Width="150px" AutoPostBack="false">
                        </asp:DropDownList>
                        <asp:TextBox Visible="false" runat="server" ID="Txtproductcode" Text='<%#Eval("productcode") %>' />
                    </EditItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Child Name">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbname" Text='<%#Eval("Name") %>' />
                        <asp:HiddenField ID="hdchild" Value='<%# Bind("childnumber")%>' runat="server" />

                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="Workshop Date">
                    <ItemTemplate>

                        <asp:Label runat="server" ID="lbOrg" Text='<%#Eval("EventDate","{0:MM/dd/yyyy}") %> ' />
                        <asp:DropDownList ID="DDlEventDates" Width="100px" runat="server" Visible="false">
                        </asp:DropDownList>
                    </ItemTemplate>
                    <EditItemTemplate>

                        <asp:DropDownList ID="DDlEventDate" Width="100px" runat="server">
                        </asp:DropDownList>

                    </EditItemTemplate>
                </asp:TemplateField>



                <asp:TemplateField HeaderText="Subject">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblyears" Text='<%#Eval("ProductName") %>' />
                        <asp:HiddenField ID="hdProductcode" Value='<%# Bind("productcode")%>' runat="server" />
                        <asp:HiddenField ID="Hdgrade" Value='<%# Bind("Grade")%>' runat="server" />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="ParentName">
                    <ItemTemplate>

                        <asp:Label runat="server" ID="lbParent" Text='<%#Eval("ParentName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="Contact Info">
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" CssClass="SmallFont" Text='<%#"PaymentInfo:" %>'>	
                        </asp:Label><br />
                        <asp:HiddenField ID="hdFee" Value='<%# Bind("Fee")%>' runat="server" />
                        <asp:Label ID="lblFee" runat="server" CssClass="SmallFont" Text='<%#"Amount:"+ DataBinder.Eval(Container.DataItem, "Fee") %>'>	
                        </asp:Label><br />
                        <asp:Label ID="lbtdate" runat="server" CssClass="SmallFont" Text='<%#"PaymentDate:"+ DataBinder.Eval(Container.DataItem, "PaymentDate") %>'>																									
                        </asp:Label>

                    </ItemTemplate>

                </asp:TemplateField>














            </Columns>
        </asp:GridView>




        <br />

    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <div>
            <asp:Label ID="lblMsg" runat="server" ForeColor="Red"></asp:Label>
        </div>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <div align="center"><span id="spnTable2Title" style="font-weight: bold;" runat="server" visible="false">Table 2: Change Options</span></div>

        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdChangeOptions" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 900px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdChangeOptions_RowCommand">
            <Columns>

                <asp:TemplateField HeaderText="Action">

                    <ItemTemplate>
                        <asp:Button ID="BtnSelect" Text="Select" runat="server" CommandName="Select" />


                        <div style="display: none;">
                            <asp:Label runat="server" ID="lblYear" Text='<%#Eval("EventYear") %>' />
                            <asp:Label runat="server" ID="lblPgID" Text='<%#Eval("ProductGroupID") %>' />
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#Eval("ProductGroupCode") %>' />
                            <asp:Label runat="server" ID="lblPrdCode" Text='<%#Eval("ProductCode") %>' />
                            <asp:Label runat="server" ID="lblPrdID" Text='<%#Eval("ProductID") %>' />
                            <asp:Label runat="server" ID="lblTeacherID" Text='<%#Eval("TeacherID") %>' />
                            <asp:Label runat="server" ID="lblEventDate" Text='<%#Eval("Date") %>' />
                            <asp:Label runat="server" ID="lblFormatedDate" Text='<%#Eval("Date","{0:MM/dd/yyyy}") %>' />
                            <asp:Label runat="server" ID="lblWSCalId" Text='<%#Eval("OnlineWSCalId") %>' />
                            <asp:Label runat="server" ID="lblLateFeeDeadline" Text='<%#Eval("LateFeeDeadline","{0:MM/dd/yyyy}") %>' />
                        </div>

                    </ItemTemplate>


                </asp:TemplateField>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <%-- <asp:BoundField DataField="AutoMemberID" HeaderText="MemberID"></asp:BoundField>--%>

                <asp:BoundField DataField="EventYear" HeaderText="Event Year"></asp:BoundField>
                <asp:BoundField DataField="name" HeaderText="ProductCode"></asp:BoundField>
                <asp:BoundField DataField="Date" HeaderText="Event Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="Time" HeaderText="TimeEST"></asp:BoundField>
                <asp:BoundField DataField="Duration" HeaderText="Duration (Hours)"></asp:BoundField>
                <asp:BoundField DataField="TeamLead" HeaderText="Team Lead"></asp:BoundField>

                <%--              
                <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>

                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>--%>
            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>
    <input type="hidden" runat="server" id="hdnCommandName" value="0" />
    <input type="hidden" runat="server" id="hdnIndex" value="0" />
    <input type="hidden" runat="server" id="hdnProductCode" value="0" />
    <input type="hidden" runat="server" id="hdnChildnumber" value="0" />
    <input type="hidden" runat="server" id="hdnGrade" value="0" />
    <input type="hidden" runat="server" id="hdnFee" value="0" />
    <input type="hidden" runat="server" id="hdnYear" value="0" />
    <input type="hidden" runat="server" id="hdnRegistrantKey" value="0" />
    <input type="hidden" runat="server" id="hdnWebinarkey" value="0" />
    <input type="hidden" runat="server" id="hdnRegId" value="0" />
    <input type="hidden" id="hdnAccessToken" value="" runat="server" />
    <input type="hidden" id="hdnOrganizerKey" value="" runat="server" />
</asp:Content>
