
#region Imports...
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using nsf.Web.UI;
using System.Web.Configuration;
#endregion

public partial class VolunteerShow : System.Web.UI.Page
{
    string sSQL;
    DataSet dsIndSpouse = new DataSet();
	protected void Page_Load(object sender, EventArgs e)
	{
        sSQL = Session["MemberIdQuery"].ToString();
        NorthSouth.DAL.IndSpouseDAL indDal = new NorthSouth.DAL.IndSpouseDAL(WebConfigurationManager.ConnectionStrings["NSFConnectionString"].ToString());
        indDal.SearchIndSpouseWhere(dsIndSpouse, sSQL);
        if (dsIndSpouse.Tables[0].Rows.Count > 0)
        {
            gvIndSpouse.DataSource = dsIndSpouse;
            gvIndSpouse.DataBind();
            //string[] keyColumn = dsIndSpouse.Tables[0].Columns["MemberID"];
            gvIndSpouse.DataKeyNames = new string[] { "MemberID" };
        }
        else Label1.Text = "No results matching your critiria found!";
        Label1.Text = sSQL;
	}
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["VolunteerId"] != null)
           {
               return string.Format("VolunteerId='{0}'", Request.QueryString["VolunteerId"].ToString());
           }
           return string.Empty;
       }
    }

}



 