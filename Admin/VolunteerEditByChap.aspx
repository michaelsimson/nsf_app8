<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VolunteerEditByChap.aspx.cs" Inherits="Admin_VolunteerEditByChap" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:GridView ID="GridView1" Width="100%" runat="server" AutoGenerateColumns="False" DataSourceID="VoleEditDs" DataKeyNames="VolunteerID" OnPreRender="GridView1_PreRender" OnRowUpdating="GridView1_RowUpdating" OnDataBound="GridView1_DataBound" OnRowCreated="GridView1_RowCreated" OnRowDataBound="GridView1_RowDataBound" OnRowEditing="GridView1_RowEditing1" OnSelectedIndexChanged="GridView1_SelectedIndexChanged">
         <Columns>
             <asp:TemplateField ShowHeader="False">
                 <EditItemTemplate>
                     <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="True" CommandName="Update"
                         Text="Update"></asp:LinkButton>
                     <asp:LinkButton ID="LinkButton2" runat="server" CausesValidation="False" CommandName="Cancel"
                         Text="Cancel"></asp:LinkButton>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False" CommandName="Edit"
                         Text="Edit"></asp:LinkButton>
                 </ItemTemplate>
             </asp:TemplateField>
                <asp:HyperLinkField DataNavigateUrlFields="MemberID" DataNavigateUrlFormatString="~/Admin/VolunteerAddByChap.aspx?MemberID={0}"
                    NavigateUrl="~/Admin/VolunteerAddByChap.aspx" Text="Add_New" HeaderText="AddNewRole" />
             <asp:TemplateField HeaderText="Name" SortExpression="Name">
                 <EditItemTemplate>
                      <asp:TextBox ID="tbName" runat="server" ReadOnly="True" Text='<%# Bind("MemberID") %>'></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblName" runat="server" Text='<%# Bind("MemberID") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
            <asp:BoundField DataField="VolunteerID" HeaderText="VolunteerID" InsertVisible="False"
                ReadOnly="True" SortExpression="VolunteerID" Visible="False" />
             <asp:TemplateField HeaderText="MemberID" SortExpression="MemberID" Visible="False">
                 <EditItemTemplate>
                     <asp:TextBox ID="tbMemberID" runat="server" Text='<%# Bind("MemberID") %>'></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="lblMemberID" runat="server" Text='<%# Bind("MemberID") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
            <asp:TemplateField HeaderText="RoleId" SortExpression="RoleId" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlRoleId" runat="server" AppendDataBoundItems="True"
                        DataSourceID="RoleIdDs" DataTextField="RoleId" DataValueField="RoleId" SelectedValue='<%# Bind("RoleId") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="RoleIdDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="RolesGreaterTableAdapters.RoleTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="Param1" SessionField="RoleId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("RoleId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="RoleCode" SortExpression="RoleCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlRoleCode" runat="server" AppendDataBoundItems="True"
                        DataSourceID="RoleCodeDs" DataTextField="RoleCode" DataValueField="RoleCode" SelectedValue='<%# Bind("RoleCode") %>' AutoPostBack="True" OnSelectedIndexChanged="ddlRoleCode_SelectedIndexChanged">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="RoleCodeDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="RolesGreaterTableAdapters.RoleTableAdapter">
                        <SelectParameters>
                            <asp:SessionParameter Name="Param1" SessionField="RoleId" Type="Int32" />
                        </SelectParameters>
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("RoleCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="TeamLead" SortExpression="TeamLead">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlTeamLead" runat="server" SelectedValue='<%# Bind("TeamLead") %>'>
                        <asp:ListItem>Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label3" runat="server" Text='<%# Bind("TeamLead") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventYear" SortExpression="EventYear">
                <EditItemTemplate>
                    &nbsp;
                    <asp:TextBox ID="tbYear" Width="30" runat="server" Text='<%# Bind("EventYear") %>'></asp:TextBox>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label4" runat="server" Text='<%# Bind("EventYear") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventId" SortExpression="EventId" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlEventId" runat="server" AppendDataBoundItems="True" SelectedValue='<%# Bind("EventId") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>1</asp:ListItem>
                        <asp:ListItem>2</asp:ListItem>
                        <asp:ListItem>3</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    &nbsp; &nbsp;
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label5" runat="server" Text='<%# Bind("EventId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventCode" SortExpression="EventCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlEventCode" runat="server" AppendDataBoundItems="True"
                        SelectedValue='<%# Bind("EventCode") %>' AutoPostBack="True" OnSelectedIndexChanged="ddlEventCode_SelectedIndexChanged">
                        <asp:ListItem Selected="True"></asp:ListItem>
                        <asp:ListItem>Finals</asp:ListItem>
                        <asp:ListItem>Chapter</asp:ListItem>
                        <asp:ListItem>WkShop</asp:ListItem>
                    </asp:DropDownList>
                    &nbsp;
                    &nbsp; &nbsp;
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label6" runat="server" Text='<%# Bind("EventCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ChapterId" SortExpression="ChapterId" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlChapterId" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ChapterIdDs" DataTextField="ChapterID" DataValueField="ChapterID"
                        SelectedValue='<%# Bind("ChapterId") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ChapterIdDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label7" runat="server" Text='<%# Bind("ChapterId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ChapterCode" SortExpression="ChapterCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlChapterCode" runat="server" DataSourceID="ChapterCodeDS"
                        DataTextField="ChapterCode" DataValueField="ChapterCode" SelectedValue='<%# Bind("ChapterCode") %>' AppendDataBoundItems="True">
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ChapterCodeDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label8" runat="server" Text='<%# Bind("ChapterCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="National" SortExpression="National">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlNational" runat="server" SelectedValue='<%# Bind("National") %>'>
                        <asp:ListItem Selected="True">Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label9" runat="server" Text='<%# Bind("National") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZoneId" SortExpression="ZoneId" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlZoneId" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ZoneIdDs" DataTextField="ZoneId" DataValueField="ZoneId" SelectedValue='<%# Bind("ZoneId") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ZoneIdDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label10" runat="server" Text='<%# Bind("ZoneId") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ZoneCode" SortExpression="ZoneCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlZoneCode" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ZoneCodeDs" DataTextField="ZoneCode" DataValueField="ZoneCode"
                        SelectedValue='<%# Bind("ZoneCode") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ZoneCodeDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label11" runat="server" Text='<%# Bind("ZoneCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Finals" SortExpression="Finals">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlFinals" runat="server" SelectedValue='<%# Bind("Finals") %>'>
                        <asp:ListItem Selected="True">Y</asp:ListItem>
                        <asp:ListItem>N</asp:ListItem>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label12" runat="server" Text='<%# Bind("Finals") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ClusterID" SortExpression="ClusterID" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlClusterId" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ClusterIdDS" DataTextField="ClusterId" DataValueField="ClusterId"
                        SelectedValue='<%# Bind("ClusterID") %>'>
                        <asp:ListItem></asp:ListItem><asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ClusterIdDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label13" runat="server" Text='<%# Bind("ClusterID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ClusterCode" SortExpression="ClusterCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlClusterCode" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ClusterCodeDs" DataTextField="ClusterCode" DataValueField="ClusterCode"
                        SelectedValue='<%# Bind("ClusterCode") %>'>
                        <asp:ListItem Selected="True"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ClusterCodeDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label14" runat="server" Text='<%# Bind("ClusterCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductGroupID" SortExpression="ProductGroupID" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlProdGroupId" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ProdGpId" DataTextField="ProductGroupId" DataValueField="ProductGroupId"
                        SelectedValue='<%# Bind("ProductGroupID") %>'>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdGpId" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProdgroupTableAdapters.ProductGroupTableAdapter">
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label16" runat="server" Text='<%# Bind("ProductGroupID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlProdGroupCode" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ProdGrpDS" DataTextField="ProductGroupCode" DataValueField="ProductGroupCode"
                        SelectedValue='<%# Bind("ProductGroupCode") %>' AutoPostBack="True" OnSelectedIndexChanged="ddlProdGroupCode_SelectedIndexChanged">
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdGrpDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProdgroupTableAdapters.ProductGroupTableAdapter">
                    </asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label17" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductID" SortExpression="ProductID" Visible="False">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlProductID" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ProdIdDs" DataTextField="ProductId" DataValueField="ProductId"
                        SelectedValue='<%# Bind("ProductID") %>'>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdIdDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProductTableAdapters.ProductTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label18" runat="server" Text='<%# Bind("ProductID") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="ProductCode" SortExpression="ProductCode">
                <EditItemTemplate>
                    &nbsp;<asp:DropDownList ID="ddlProdCode" runat="server" AppendDataBoundItems="True"
                        DataSourceID="ProdDs" DataTextField="ProductCode" DataValueField="ProductCode"
                        SelectedValue='<%# Bind("ProductCode") %>'>
                        <asp:ListItem></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="ProdDs" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ProductTableAdapters.ProductTableAdapter"></asp:ObjectDataSource>
                </EditItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label19" runat="server" Text='<%# Bind("ProductCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
             <asp:TemplateField HeaderText="MemberID" SortExpression="MemberID">
                 <EditItemTemplate>
                     <asp:TextBox ID="TextBox1" runat="server" ReadOnly="true" Text='<%# Bind("MemberID") %>'></asp:TextBox>
                 </EditItemTemplate>
                 <ItemTemplate>
                     <asp:Label ID="Label15" runat="server" Text='<%# Bind("MemberID") %>'></asp:Label>
                 </ItemTemplate>
             </asp:TemplateField>
             
            
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="VoleEditDs" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetDataByRole" TypeName="VolunteerTableAdapters.VolunteerTableAdapter"
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:SessionParameter Name="ChapID" SessionField="LoginChapterID" Type="Int32" />
            <asp:SessionParameter Name="RoleId" SessionField="RoleId" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    &nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="hlinkMenu" runat="server" OnClick="hlinkMenu_Click">Goback to Menu</asp:LinkButton>
</asp:Content>


 

 
 
 