
#region Imports...
using System;
using System.Configuration;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Globalization;
using System.Text;
using System.Web;

using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using nsf.Web.UI;
#endregion

public partial class WeekCalendarEdit : System.Web.UI.Page
{
    protected void IsValidSatDate(object source, ServerValidateEventArgs value)
    {

        /* check if update operation
        if (((Button)(Page.Master.FindControl("Content_main").
                     FindControl("FormView1$UpdateButton"))).Visible == true)
        {
            value.IsValid = true;
            return;
        }*/

        // Declare database objects such as connection,
        // command and transaction
        DateTime satDay1 = DateTime.Parse(value.Value);
        StringBuilder queryStr = new StringBuilder();

        // Uses the default calendar of the InvariantCulture.
        System.Globalization.Calendar myCal = CultureInfo.InvariantCulture.Calendar;
        if (myCal.GetDayOfWeek(satDay1).Equals("Saturday"))
        {
            value.IsValid = false;
            return;
        }
       DateTime sunDay1 = myCal.AddDays(satDay1,1);

       ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
       string connectionString = connStrSet.ConnectionString;
       //string connectionString = "Data Source=anu-lt\\northsouth2;Initial Catalog=nsfdev;Integrated Security=true;Connection Timeout=1;";
       using (SqlConnection connection = new SqlConnection(connectionString))
       {
           connection.Open();
           int ret;

           for (int i = 2; i <= 9; i++)
           {
               // Adds 7 days to the DateTime.
               satDay1 = myCal.AddDays(satDay1, 7);
               sunDay1 = myCal.AddDays(satDay1, 1);
               queryStr.Append("update WeekCalendar set  SatDay1='");
               queryStr.Append(satDay1.ToString());
               queryStr.Append("', SunDay2='");
               queryStr.Append(sunDay1.ToString());
               queryStr.Append("' where WeekId = ");
               queryStr.Append(i.ToString());

               SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
               ret = command.ExecuteNonQuery();
           }

        }
    }

	protected void Page_Load(object sender, EventArgs e)
	{		
		FormUtil.RedirectAfterInsertUpdate(FormView1, "WeekCalendarEdit.aspx?{0}", WeekCalendarDataSource);
		FormUtil.RedirectAfterAddNew(FormView1, "WeekCalendarEdit.aspx");
		FormUtil.RedirectAfterCancel(FormView1, "WeekCalendar.aspx");
		FormUtil.SetDefaultMode(FormView1, "WeekId");
        CustomValidator cv = Page.Master.FindControl("Content_main").
       FindControl("FormView1$SatDay1Validator") as CustomValidator;
        cv.ServerValidate +=
                      new ServerValidateEventHandler(this.IsValidSatDate);

        if (!Page.IsPostBack)
        {

            TextBox tbx1 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreateDate") as TextBox;
            TextBox tbx2 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreatedBy") as TextBox;
            TextBox tbx3 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifyDate") as TextBox;
            TextBox tbx4 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifiedBy") as TextBox;

            tbx1.ReadOnly = true;
            tbx2.ReadOnly = true;
            tbx3.ReadOnly = true;
            tbx4.ReadOnly = true;

            Button tb5 = Page.Master.FindControl("Content_main").
                   FindControl("FormView1$InsertButton") as Button;


            System.DateTime n = System.DateTime.Now;

            // Insert operation
            if (tb5.Visible == true)
            {
                tbx1.Text = n.ToString();
                tbx2.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
            else
            {
                tbx3.Text = n.ToString();
                tbx4.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
        } // Not postback

	}
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["WeekId"] != null)
           {
               return string.Format("WeekId='{0}'", Request.QueryString["WeekId"].ToString());
           }
           return string.Empty;
       }
    }

}



 