
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="ChapterEdit.aspx.cs" Inherits="ChapterEdit" Title="Chapter Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<data:MultiFormView ID="FormView1" DataKeyNames="ChapterID" runat="server" DataSourceID="ChapterDataSource">
		
			<EditItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/ChapterFields.ascx" />
			</EditItemTemplatePaths>
		
			<InsertItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/ChapterFields.ascx" />
			</InsertItemTemplatePaths>
		
			<EmptyDataTemplate>
				<b>Chapter not found!</b>
			</EmptyDataTemplate>
			
			<FooterTemplate>
				<asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
				<asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
				<asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
			</FooterTemplate>

		</data:MultiFormView>
		
		<data:ChapterDataSource ID="ChapterDataSource" runat="server"
			SelectMethod="GetByChapterID"
		>
			<Parameters>
				<asp:QueryStringParameter Name="ChapterID" QueryStringField="ChapterID" Type="String" />

			</Parameters>
		</data:ChapterDataSource>
		
		<br />

		<data:EntityGridView ID="GridViewContestant" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewContestant_SelectedIndexChanged"			 			 
			DataSourceID="ContestantDataSource"
			DataKeyNames="ContestantId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Contestant.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ContestCode" HeaderText="ContestCode" SortExpression="ContestCode" />				
				<asp:BoundField DataField="ContestYear" HeaderText="ContestYear" SortExpression="ContestYear" />				
				<asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" SortExpression="ChildNumber" />				
				<asp:BoundField DataField="BadgeNumber" HeaderText="BadgeNumber" SortExpression="BadgeNumber" />				
				<asp:BoundField DataField="Score1" HeaderText="Score1" SortExpression="Score1" />				
				<asp:BoundField DataField="Score2" HeaderText="Score2" SortExpression="Score2" />				
				<asp:BoundField DataField="Score3" HeaderText="Score3" SortExpression="Score3" />				
				<asp:BoundField DataField="Rank" HeaderText="Rank" SortExpression="Rank" />				
				<asp:BoundField DataField="NationalInvitee" HeaderText="NationalInvitee" SortExpression="NationalInvitee" />				
				<asp:BoundField DataField="Fee" HeaderText="Fee" SortExpression="Fee" />				
				<asp:BoundField DataField="DiscountedAmount" HeaderText="DiscountedAmount" SortExpression="DiscountedAmount" />				
				<asp:BoundField DataField="PaymentDate" HeaderText="PaymentDate" SortExpression="PaymentDate" />				
				<asp:BoundField DataField="PaymentMode" HeaderText="PaymentMode" SortExpression="PaymentMode" />				
				<asp:BoundField DataField="PaymentReference" HeaderText="PaymentReference" SortExpression="PaymentReference" />				
				<asp:BoundField DataField="PaymentNotes" HeaderText="PaymentNotes" SortExpression="PaymentNotes" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="PhotoImage" HeaderText="PhotoImage" SortExpression="PhotoImage" />				
				<asp:BoundField DataField="FinalsPaymentStatus" HeaderText="FinalsPaymentStatus" SortExpression="FinalsPaymentStatus" />				
				<asp:BoundField DataField="InviteDeclineComments" HeaderText="InviteDeclineComments" SortExpression="InviteDeclineComments" />				
				<asp:BoundField DataField="NationalInviteeStatus" HeaderText="NationalInviteeStatus" SortExpression="NationalInviteeStatus" />				
				<asp:BoundField DataField="InviteUpdatedDate" HeaderText="InviteUpdatedDate" SortExpression="InviteUpdatedDate" />				
				<asp:BoundField DataField="FinalsAmount" HeaderText="FinalsAmount" SortExpression="FinalsAmount" />				
				<asp:BoundField DataField="FinalsPaymentDate" HeaderText="FinalsPaymentDate" SortExpression="FinalsPaymentDate" />				
				<asp:BoundField DataField="FinalsPaymentReference" HeaderText="FinalsPaymentReference" SortExpression="FinalsPaymentReference" />				
				<asp:BoundField DataField="FinalsPaymentMode" HeaderText="FinalsPaymentMode" SortExpression="FinalsPaymentMode" />				
				<asp:BoundField DataField="FinalsBadgeNumber" HeaderText="FinalsBadgeNumber" SortExpression="FinalsBadgeNumber" />				
				<asp:BoundField DataField="FinalsPhase1" HeaderText="FinalsPhase1" SortExpression="FinalsPhase1" />				
				<asp:BoundField DataField="FinalsPhase2" HeaderText="FinalsPhase2" SortExpression="FinalsPhase2" />				
				<asp:BoundField DataField="FinalsRank" HeaderText="FinalsRank" SortExpression="FinalsRank" />				
				<asp:BoundField DataField="FinalsPercentile" HeaderText="FinalsPercentile" SortExpression="FinalsPercentile" />				
				<asp:BoundField DataField="ContestCategoryID" HeaderText="ContestCategoryID" SortExpression="ContestCategoryID" />				
				<asp:BoundField DataField="ContestID" HeaderText="ContestID" SortExpression="ContestID" />				
				<asp:BoundField DataField="ChildNumberOLD" HeaderText="ChildNumberOLD" SortExpression="ChildNumberOLD" />				
				<asp:BoundField DataField="EventId" HeaderText="EventId" SortExpression="EventId" />				
				<asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode" />				
				<asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId" SortExpression="ProductGroupId" />				
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />				
				<asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId" />				
				<asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifiedDate" HeaderText="ModifiedDate" SortExpression="ModifiedDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No Contestant Found! </b>
				<asp:HyperLink runat="server" ID="hypContestant" NavigateUrl="~/admin/ContestantEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:ContestantDataSource ID="ContestantDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewContestant" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewContestant" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ContestantDataSource>
		<br />
		

</asp:Content>


 

 
 
 