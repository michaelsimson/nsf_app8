

#region Imports...
using System;
using System.Data;
using System.Configuration;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using nsf.Web.UI;
#endregion

public partial class WeekCalendar : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
	{
		FormUtil.RedirectAfterUpdate(GridView1, "WeekCalendar.aspx?page={0}");
		FormUtil.SetPageIndex(GridView1, "page");

        /*GridView gv = Page.Master.FindControl("ContentPlaceHolder1").
           FindControl("GridView1") as GridView;
        // Iterates through the rows of the GridView control
        foreach (GridViewRow row in gv.Rows)
        {
            String wid = ((TextBox)(row.FindControl("WeekId"))).Text;
        } */
    }

	protected void GridView_SelectedIndexChanged(object sender, EventArgs e)
	{

		string urlParams = string.Format("WeekId={0}", GridView1.SelectedDataKey.Values[0]);
		Response.Redirect("WeekCalendarEdit.aspx?" + urlParams, true);
    }
}



 