
#region Imports...
using System;
using System.Configuration;
using System.Collections;
using System.Data;
using System.Data.Common;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Text;
using System.Web;

using System.Web.Security;
using System.Web.Configuration;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

using nsf.Web.UI;
#endregion

public partial class ProductGroupEdit : System.Web.UI.Page
{
    protected void IsDuplicate(object source, ServerValidateEventArgs value)
    {

        // check if update operation
        if (((Button)(Page.Master.FindControl("Content_main").
                     FindControl("FormView1$UpdateButton"))).Visible == true)
        {
            value.IsValid = true;
            return;
        }

        // Declare database objects such as connection,
        // command and transaction
        string productGroupCode = value.Value;
        DropDownList dd1 = Page.Master.FindControl("Content_main").FindControl("FormView1$dataEventCode") as DropDownList;
        string eventCode = dd1.SelectedItem.Value;
        
        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT ProductGroupCode from ProductGroup where ProductGroupCode ='");
        queryStr.Append(productGroupCode);
        queryStr.Append("' AND EventCode = '");
        queryStr.Append(eventCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                /* if there is an entry, then there is a duplicate */
                /* So make the isValid false */
                value.IsValid = (reader.Read()) ? false : true;
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }
    }

    void EventCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        string eventCode = ((DropDownList)Page.Master.
            FindControl("Content_main").FindControl("FormView1$dataEventCode")).SelectedItem.Value;

        StringBuilder queryStr = new StringBuilder();
        queryStr.Append("SELECT EventId from Event where EventCode ='");
        queryStr.Append(eventCode);
        queryStr.Append("'");

        ConnectionStringSettings connStrSet = ConfigurationManager.ConnectionStrings["nsf.Data.ConnectionString"];
        string connectionString = connStrSet.ConnectionString;
        using (SqlConnection connection = new SqlConnection(connectionString))
        {
            SqlCommand command = new SqlCommand(queryStr.ToString(), connection);
            connection.Open();
            SqlDataReader reader = command.ExecuteReader();
            try
            {
                if (reader.Read())
                {
                    ((TextBox)(Page.Master.FindControl("Content_main").FindControl("FormView1$dataEventId"))).Text =
                        reader.GetInt32(0).ToString();
                }
            }
            finally
            {
                // Always call Close when done reading.
                reader.Close();
            }

        }

    }

	protected void Page_Load(object sender, EventArgs e)
	{		
		FormUtil.RedirectAfterInsertUpdate(FormView1, "ProductGroupEdit.aspx?{0}", ProductGroupDataSource);
		FormUtil.RedirectAfterAddNew(FormView1, "ProductGroupEdit.aspx");
		FormUtil.RedirectAfterCancel(FormView1, "ProductGroup.aspx");
		FormUtil.SetDefaultMode(FormView1, "ProductGroupId");
        CustomValidator cv = Page.Master.FindControl("Content_main").
        FindControl("FormView1$ProductGroupCodeValidator") as CustomValidator;
        cv.ServerValidate +=
                      new ServerValidateEventHandler(this.IsDuplicate);

        DropDownList ddl1 = Page.Master.FindControl("Content_main").
FindControl("FormView1$dataEventCode") as DropDownList;
        ddl1.SelectedIndexChanged += new EventHandler(EventCode_SelectedIndexChanged);
        if (!Page.IsPostBack)
        {

            TextBox tbx1 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreateDate") as TextBox;
            TextBox tbx2 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataCreatedBy") as TextBox;
            TextBox tbx3 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifyDate") as TextBox;
            TextBox tbx4 = Page.Master.FindControl("Content_main").
                           FindControl("FormView1$dataModifiedBy") as TextBox;

            TextBox tbx5 = Page.Master.FindControl("Content_main").
               FindControl("FormView1$dataEventId") as TextBox;

            tbx1.ReadOnly = true;
            tbx2.ReadOnly = true;
            tbx3.ReadOnly = true;
            tbx4.ReadOnly = true;
            tbx5.ReadOnly = true;

            Button tb5 = Page.Master.FindControl("Content_main").
                   FindControl("FormView1$InsertButton") as Button;


            System.DateTime n = System.DateTime.Now;

            // Insert operation
            if (tb5.Visible == true)
            {
                tbx1.Text = n.ToString();
                tbx2.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
            else
            {
                tbx3.Text = n.ToString();
                tbx4.Text = (Session["LoginID"] != null) ? (String)Session["LoginID"] : "0000";
            }
        } // Not postback

	}

	protected void GridViewProduct_SelectedIndexChanged(object sender, EventArgs e)
	{
		string urlParams = string.Format("ProductId={0}", GridViewProduct.SelectedDataKey.Values[0]);
		Response.Redirect("ProductEdit.aspx?" + urlParams, true);		
	}	
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["ProductGroupId"] != null)
           {
               return string.Format("ProductGroupId='{0}'", Request.QueryString["ProductGroupId"].ToString());
           }
           return string.Empty;
       }
    }

}



 