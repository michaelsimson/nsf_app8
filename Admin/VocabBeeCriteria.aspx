﻿<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="VocabBeeCriteria.aspx.vb" Inherits="Admin_VocabBeeCriteria"%>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left">
<asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink></div>
<div style="width:100%; text-align:center">
<table border="0" cellpadding = "3" cellspacing = "0">
<tr><td class="title02"  align="center" > 
Criteria for Selecting Vocab Bee Published Words
</td>
   </tr>
   <tr><td class="txt01"  align="center" > Contest Year 
       <asp:DropDownList ID="ddlYear" AutoPostBack = "true" OnSelectedIndexChanged = "ddlYear_SelectedIndexChanged" runat="server">
       </asp:DropDownList> </td> </tr> 
<tr><td  align="center" class="txt01" >
<table border="0" cellpadding = "5" cellspacing = "0">
<tr>
<td class="txt01_strong" align="center" >Level</td>
<td class="txt01_strong" align="center">Sub-Level</td>
<td class="txt01_strong" align="center">Total</td>
<td class="txt01_strong" align="center">Regional</td>
<td class="txt01_strong" align="center">Contest</td>
<td class="txt01_strong" align="center">Reg_Reserved</td>
<td class="txt01_strong" align="center">Contest</td>
<td class="txt01_strong" align="center">National</td>
<td class="txt01_strong" align="center">Contest</td>
<td class="txt01_strong" align="center">Nat_Reserved</td>
<td class="txt01_strong" align="center">Contest</td>
</tr>
<tr>
<td class="txt01" align="center">1</td>
<td class="txt01" align="center">1</td>
<td class="txt01" align="center"><asp:Label ID="lbl11" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt11r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl11r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
        <td class="txt01" align="center"><asp:TextBox ID="txt11rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl11rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
        <td class="txt01" align="center"><asp:TextBox ID="txt11n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl11n" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt11nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl11nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
<tr>
<td class="txt01" align="center">1</td>
<td class="txt01" align="center">2&nbsp;</td>
<td class="txt01" align="center"><asp:Label ID="lbl12" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt12r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl12r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt12rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl12rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt12n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl12n" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt12nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl12nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
<tr>
<td class="txt01" align="center">2</td>
<td class="txt01" align="center">1</td>
<td class="txt01" align="center"><asp:Label ID="lbl21" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt21r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl21r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt21rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl21rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt21n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl21n" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt21nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl21nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
<tr>
<td class="txt01" align="center">2</td>
<td class="txt01" align="center">2</td>
<td class="txt01" align="center"><asp:Label ID="lbl22" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt22r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl22r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt22rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl22rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt22n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl22n" runat="server">
       <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt22nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl22nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
<tr>
<td class="txt01" align="center">3</td>
<td class="txt01" align="center">1</td>
<td class="txt01" align="center"><asp:Label ID="lbl31" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt31r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl31r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt31rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl31rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt31n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl31n" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt31nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl31nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
<tr>
<td class="txt01" align="center">3</td>
<td class="txt01" align="center">2</td>
<td class="txt01" align="center"><asp:Label ID="lbl32" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:TextBox ID="txt32r" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl32r" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt32rr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl32rr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
        <td class="txt01" align="center"><asp:TextBox ID="txt32n" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl32n" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
<td class="txt01" align="center"><asp:TextBox ID="txt32nr" Width="50px" runat="server"></asp:TextBox></td>
<td class="txt01" align="center"><asp:DropDownList ID="ddl32nr" runat="server">
        <asp:ListItem>JVB</asp:ListItem><asp:ListItem>IVB</asp:ListItem></asp:DropDownList></td>
</tr>
        
<tr>
<td class="txt01" align="center">&nbsp;</td>
<td class="txt01_strong" align="center">Total</td>
<td class="txt01" align="center"><asp:Label ID="lbltotalSum" runat="server" ></asp:Label></td>
<td class="txt01" align="center"><asp:Label ID="lblrsum" runat="server" ></asp:Label></td>
<td class="txt01" align="center">&nbsp;</td>
<td class="txt01" align="center"><asp:Label ID="lblrrsum" runat="server" ></asp:Label></td>
<td class="txt01" align="center">&nbsp;</td>
<td class="txt01" align="center"><asp:Label ID="lblnsum" runat="server" ></asp:Label></td>
<td class="txt01" align="center">&nbsp;</td>
<td class="txt01" align="center"><asp:Label ID="lblnrsum" runat="server" ></asp:Label></td>
<td class="txt01" align="center">&nbsp;</td>
</tr>
        
</table>
</td></tr>
<tr><td  align="center"> 
    <asp:Button ID="btnSave" runat="server" OnClick="btnsave_Click" Text="Save Criteria" /> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnGenerate" runat="server" Text="Generate Pub Words" OnClick ="btnGenerate_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnExport"  OnClick = "btnExport_Click" Visible="false"  runat="server" Text="Export Published Words" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnPDF"  runat="server" Text="Generate Published PDF Files" Visible="False" />
    </td>
   </tr>
    <tr><td  align="center" > 
        <asp:Label ID="lblerr" runat="server" ForeColor = "Red" ></asp:Label>
        <asp:Label ID="lblAssessExamID" ForeColor="White" runat="server" ></asp:Label>
        <asp:Label ID="lblEditResult" ForeColor="White" runat="server"></asp:Label>
        <asp:Label ID="lblProductID" ForeColor="White" runat="server"></asp:Label>
        <asp:Label ID="lblCount" ForeColor="White" runat="server"></asp:Label>
        
        </td></tr>
    <tr><td  align="left" > 
        Sequence of steps: <br />
        Update the words in both spelling & vocab tables (top half of the screen)  <br />
        (Select Year, Save Criteria, Generate Published Words, Export Published Words)<br />
        Update the Game Flags (bottom of the screen) in both spelling & vocab (only at the time of actually going live with registrations).

        </td></tr>
                  <tr><td  align="center" >
              <asp:DropDownList ID="ddlGame_Flag" runat="server">
                  <asp:ListItem  Value="0">Select</asp:ListItem>
                  <asp:ListItem Value="Regional_Flag">Regional</asp:ListItem>
                  <asp:ListItem Value="Nat_Flag">National</asp:ListItem>
              </asp:DropDownList> &nbsp;&nbsp;<asp:Button Width="150px" ID="btnUpdategameflag" OnClick="btnUpdategameflag_Click" runat="server" Text="Update Game_Flag" />
           </td> </tr>
</table>
</div> 
     <asp:Panel ID="pnlPDF" runat="server">
        <span id="spPDF" runat="server"></span>
    </asp:Panel>
</asp:Content>




