
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="ZoneEdit.aspx.cs" Inherits="ZoneEdit" Title="Zone Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		<data:MultiFormView ID="FormView1" DataKeyNames="ZoneId" runat="server" DataSourceID="ZoneDataSource">
		
			<EditItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/ZoneFields.ascx" />
			</EditItemTemplatePaths>
		
			<InsertItemTemplatePaths>
				<data:TemplatePath Path="~/Admin/UserControls/ZoneFields.ascx" />
			</InsertItemTemplatePaths>
		
			<EmptyDataTemplate>
				<b>Zone not found!</b>
			</EmptyDataTemplate>
			
			<FooterTemplate>
				<asp:Button ID="InsertButton" runat="server" CausesValidation="True" CommandName="Insert" Text="Insert" />
				<asp:Button ID="UpdateButton" runat="server" CausesValidation="True" CommandName="Update" Text="Update" />
				<asp:Button ID="CancelButton" runat="server" CausesValidation="False" CommandName="Cancel" Text="Cancel" />
			</FooterTemplate>

		</data:MultiFormView>
		
		<data:ZoneDataSource ID="ZoneDataSource" runat="server"
			SelectMethod="GetByZoneId"
		>
			<Parameters>
				<asp:QueryStringParameter Name="ZoneId" QueryStringField="ZoneId" Type="String" />

			</Parameters>
		</data:ZoneDataSource>
		
		<br />

		<data:EntityGridView ID="GridViewChapter" runat="server"
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridViewChapter_SelectedIndexChanged"			 			 
			DataSourceID="ChapterDataSource"
			DataKeyNames="ChapterID"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Chapter.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			Visible='<%# (FormView1.DefaultMode == FormViewMode.Insert) ? false : true %>'	
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />
				<asp:BoundField DataField="ChapterCode" HeaderText="ChapterCode" SortExpression="ChapterCode" />				
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />				
				<asp:BoundField DataField="State" HeaderText="State" SortExpression="State" />				
				<asp:BoundField DataField="City" HeaderText="City" SortExpression="City" />				
				<asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />				
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />				
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />				
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />				
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />				
			</Columns>
			<EmptyDataTemplate>
				<b>No Chapter Found! </b>
				<asp:HyperLink runat="server" ID="hypChapter" NavigateUrl="~/admin/ChapterEdit.aspx">Add New</asp:HyperLink>
			</EmptyDataTemplate>
		</data:EntityGridView>					

		<data:ChapterDataSource ID="ChapterDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<asp:ControlParameter Name="WhereClause" ControlID="__Page" PropertyName="WhereClause" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridViewChapter" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridViewChapter" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ChapterDataSource>
		<br />
		

</asp:Content>


 

 
 
 