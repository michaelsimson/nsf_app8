using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class Admin_VolunteerAddByChap : System.Web.UI.Page
{
    string memberId, aValue, pgValue;
    DataSet dsContests = new DataSet();
    static DataTable pdt = new DataTable();
    static DataTable prod = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        memberId = Request.QueryString["MemberID"].ToString();
 
    }
    protected void DetailsView1_PreRender(object sender, EventArgs e)
    {
        if (DetailsView1.CurrentMode == DetailsViewMode.Insert)
        {
            ((TextBox)DetailsView1.FindControl("TextBox1")).Text = memberId;
        

            
            if (Session["RoleId"] != null && Convert.ToInt32(Session["RoleId"]) != 1)
            {
                ((DropDownList)DetailsView1.FindControl("ddlAgent")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlWrite")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlAuth")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlAgent")).SelectedIndex = 2;
                ((DropDownList)DetailsView1.FindControl("ddlWrite")).SelectedIndex = 2;
                ((DropDownList)DetailsView1.FindControl("ddlAuth")).SelectedIndex = 2;
            }
            if(Session["LoginRole"] != null && Session["LoginRole"].ToString() == "ChapterC")
            {
                ((DropDownList)DetailsView1.FindControl("ddlChapterId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlChapter")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlNational")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlZone")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlCluster")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlZoneId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlClusterId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlFinals")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlChapterId")).SelectedValue = Session["LoginChapterID"].ToString();
                ((DropDownList)DetailsView1.FindControl("ddlChapter")).SelectedIndex =
                ((DropDownList)DetailsView1.FindControl("ddlChapterId")).SelectedIndex;
                ((DropDownList)DetailsView1.FindControl("ddlEvent")).Items.Remove("Finals");
                ((DropDownList)DetailsView1.FindControl("ddlEventId")).Items.Remove("1");
            }
            if (this.IsPostBack == true)
            {
                if (((DropDownList)DetailsView1.FindControl("ddlRoleCode")).SelectedValue=="ChapterC")
                {
                     ((DropDownList)DetailsView1.FindControl("ddlChapterId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlChapter")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlNational")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlZone")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlCluster")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlZoneId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlClusterId")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlFinals")).Enabled = false;
                ((DropDownList)DetailsView1.FindControl("ddlEvent")).Enabled=false;
                ((DropDownList)DetailsView1.FindControl("ddlProduct")).Enabled=false;
                ((DropDownList)DetailsView1.FindControl("ddlProdGroup")).Enabled=false;
                ((DropDownList)DetailsView1.FindControl("ddlTeamLead")).Enabled = false;
                }
                DataTable dt = new DataTable();
                RoleGetByCodeTableAdapters.RoleTableAdapter rt = new RoleGetByCodeTableAdapters.RoleTableAdapter();
                dt = rt.GetData(((DropDownList)DetailsView1.FindControl("ddlRoleCode")).Text);
                if(dt.Rows.Count>0)
                if (dt.Rows[0].ItemArray[2].ToString().Trim() != "Y")
                {
                    ((DropDownList)DetailsView1.FindControl("ddlNational")).Enabled = false;
                    ((DropDownList)DetailsView1.FindControl("ddlNational")).SelectedIndex = 2;
                }
                aValue = ((DropDownList)DetailsView1.FindControl("ddlEvent")).SelectedValue;
                GetFromProducts();
                for (int i = 0; i < pdt.Rows.Count; i++)
                {
                    ((DropDownList)DetailsView1.FindControl("ddlProdGroup")).Items.Add(pdt.Rows[i].ItemArray[1].ToString());
                    ((DropDownList)DetailsView1.FindControl("ddlProductGroupID")).Items.Add(pdt.Rows[i].ItemArray[0].ToString());
                }

                pgValue = ((DropDownList)DetailsView1.FindControl("ddlProdGroup")).Text;
                GetFromProductsByGp();
                for (int i = 0; i < prod.Rows.Count; i++)
                {
                    ((DropDownList)DetailsView1.FindControl("ddlProduct")).Items.Add(prod.Rows[i].ItemArray[1].ToString());
                    ((DropDownList)DetailsView1.FindControl("ddlProductId")).Items.Add(prod.Rows[i].ItemArray[0].ToString());
                }
            }
        }
    }
    protected void DetailsView1_ItemInserting(object sender, DetailsViewInsertEventArgs e)
    {

        VolAddDS.InsertParameters[0].DefaultValue = Request.QueryString["MemberId"].ToString();
        //Determine RoleId selection
        ((DropDownList)DetailsView1.FindControl("ddlRoleId")).SelectedIndex =
            ((DropDownList)DetailsView1.FindControl("ddlRoleCode")).SelectedIndex;
        VolAddDS.InsertParameters[1].DefaultValue = ((DropDownList)DetailsView1.FindControl("ddlRoleId")).SelectedValue.ToString();
        
        //detrmine chapid
        if(Session["LoginChapterId"]!=null)
        VolAddDS.InsertParameters[7].DefaultValue = Session["LoginChapterId"].ToString();
       //Determine EventYr, EventId, etc. 

        int ddlEventIndex = ((DropDownList)DetailsView1.FindControl("ddlEvent")).SelectedIndex;
        ((DropDownList)DetailsView1.FindControl("ddlEventId")).SelectedIndex = ddlEventIndex;
        ((TextBox)DetailsView1.FindControl("tbYear")).Text = "2007";// Application["ContestYear"].ToString();
        VolAddDS.InsertParameters[4].DefaultValue = ((TextBox)DetailsView1.FindControl("tbYear")).Text;
        VolAddDS.InsertParameters[5].DefaultValue = ((DropDownList)DetailsView1.FindControl("ddlEventId")).SelectedValue.ToString();


        //Determine ProdCodeID, ProdId, etc.
        if (((DropDownList)DetailsView1.FindControl("ddlProdGroup")).SelectedIndex != -1)
        {
            int ddlProdGroupIndex = ((DropDownList)DetailsView1.FindControl("ddlProdGroup")).SelectedIndex;
            VolAddDS.InsertParameters[16].DefaultValue = pdt.Rows[ddlProdGroupIndex - 1].ItemArray[0].ToString();
            VolAddDS.InsertParameters[17].DefaultValue = pdt.Rows[ddlProdGroupIndex - 1].ItemArray[1].ToString();
        }
        if (((DropDownList)DetailsView1.FindControl("ddlProduct")).SelectedIndex != -1)
        {
            int ddlProdIndex = ((DropDownList)DetailsView1.FindControl("ddlProduct")).SelectedIndex;
            VolAddDS.InsertParameters[18].DefaultValue = prod.Rows[ddlProdIndex - 1].ItemArray[0].ToString();
            VolAddDS.InsertParameters[19].DefaultValue = prod.Rows[ddlProdIndex - 1].ItemArray[1].ToString();
        }
    }
    protected void DetailsView1_ItemUpdated(object sender, DetailsViewUpdatedEventArgs e)
    {
        
    }
    protected void DetailsView1_ItemInserted(object sender, DetailsViewInsertedEventArgs e)
    {
        lblMessage.Text = "Volunteer Role Added!";
    }
    public string GetName()
    {
        string name = "";
        IndSpouseNameTableAdapters.IndSpouseTableAdapter ita = new IndSpouseNameTableAdapters.IndSpouseTableAdapter();
        DataTable dt = new DataTable();
        dt = ita.GetData(Convert.ToInt32(memberId));
        if (dt.Rows.Count > 0)
            name = dt.Rows[0].ItemArray[0].ToString() + " " +
                dt.Rows[0].ItemArray[2].ToString();
        else name = "Not found";
        return name;
    }
    protected void GetFromProducts()
    {
        //ProductTableAdapters.ProductTableAdapter pta = new ProductTableAdapters.ProductTableAdapter();
        ProdgroupTableAdapters.ProductGroupTableAdapter pta = new ProdgroupTableAdapters.ProductGroupTableAdapter();
        pdt.Clear();
        if(aValue != null)
        pdt = pta.GetDataByPG(aValue);
    
    }
    protected void GetFromProductsByGp()
    {
        ProductTableAdapters.ProductTableAdapter ptg = new ProductTableAdapters.ProductTableAdapter();
        prod.Clear();
        if(pgValue != null)
            prod = ptg.GetDataByProdGp(pgValue,aValue);
    }
    
}

 