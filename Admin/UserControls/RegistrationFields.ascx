<%@ Control Language="C#" ClassName="RegistrationFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ChapterId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataChapterId" DataSourceID="ChapterIdChapterDataSource" DataTextField="ChapterCode" DataValueField="ChapterID" SelectedValue='<%# Bind("ChapterId") %>'></asp:DropDownList>
					<data:ChapterDataSource ID="ChapterIdChapterDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ChapterDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChapterCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataChapterCode" DataSourceID="ChapterCodeChapterDataSource" DataTextField="ChapterCode" DataValueField="ChapterCode" SelectedValue='<%# Bind("ChapterCode") %>'></asp:DropDownList>
					<data:ChapterDataSource ID="ChapterCodeChapterDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:ChapterDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventId" DataSourceID="EventIdEventDataSource" DataTextField="EventCode" DataValueField="EventId" SelectedValue='<%# Bind("EventId") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventIdEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataEventCode" DataSourceID="EventCodeEventDataSource" DataTextField="EventCode" DataValueField="EventCode" SelectedValue='<%# Bind("EventCode") %>'></asp:DropDownList>
					<data:EventDataSource ID="EventCodeEventDataSource" runat="server"
 						SelectMethod="GetAll"
 					>
					</data:EventDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupId" Text='<%# Bind("ProductGroupId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">MemberId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataMemberId" Text='<%# Bind("MemberId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataMemberId" runat="server" Display="Dynamic" ControlToValidate="dataMemberId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataMemberId" runat="server" Display="Dynamic" ControlToValidate="dataMemberId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventYear" Text='<%# Bind("EventYear") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventYear" runat="server" Display="Dynamic" ControlToValidate="dataEventYear" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ParentId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataParentId" Text='<%# Bind("ParentId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataParentId" runat="server" Display="Dynamic" ControlToValidate="dataParentId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataParentId" runat="server" Display="Dynamic" ControlToValidate="dataParentId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChildNumber:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChildNumber" Text='<%# Bind("ChildNumber") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataChildNumber" runat="server" Display="Dynamic" ControlToValidate="dataChildNumber" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Fee:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFee" Text='<%# Bind("Fee") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFee" runat="server" Display="Dynamic" ControlToValidate="dataFee" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">DiscountedAmount:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDiscountedAmount" Text='<%# Bind("DiscountedAmount") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataDiscountedAmount" runat="server" Display="Dynamic" ControlToValidate="dataDiscountedAmount" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">LateFee:</td>
				<td>
					<asp:TextBox runat="server" ID="dataLateFee" Text='<%# Bind("LateFee") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataLateFee" runat="server" Display="Dynamic" ControlToValidate="dataLateFee" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentDate" Text='<%# Bind("PaymentDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataPaymentDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentMode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentMode" Text='<%# Bind("PaymentMode") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentReference:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentReference" Text='<%# Bind("PaymentReference") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentNotes:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentNotes" Text='<%# Bind("PaymentNotes") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChildNumberOld:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChildNumberOld" Text='<%# Bind("ChildNumberOld") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataChildNumberOld" runat="server" Display="Dynamic" ControlToValidate="dataChildNumberOld" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


