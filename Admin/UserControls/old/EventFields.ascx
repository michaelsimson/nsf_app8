<%@ Control Language="C#" ClassName="EventFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventCode" Text='<%# Bind("EventCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventCode" runat="server" Display="Dynamic" ControlToValidate="dataEventCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Name:</td>
				<td>
					<asp:TextBox runat="server" ID="dataName" Text='<%# Bind("Name") %>' MaxLength="100"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataName" runat="server" Display="Dynamic" ControlToValidate="dataName" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventYear" Text='<%# Bind("EventYear") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataEventYear" runat="server" Display="Dynamic" ControlToValidate="dataEventYear" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Status:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataStatus" DataSourceID="StatusStatusDataSource" DataTextField="DataText" DataValueField="DataValue" SelectedValue='<%# Bind("Status") %>'></asp:DropDownList>
					<data:StatusDataSource ID="StatusStatusDataSource" runat="server"
 						SelectMethod="GetByTableID" 
 					>
 					
 					<Parameters>
 
 					<data:DataParameter Name="TableID" Type="int32" DefaultValue='1' DataSourceID="LookUpCodesDataSource1" />
 					</Parameters>
					</data:StatusDataSource>
					<data:LookUpCodesDataSource ID="LookUpCodesDataSource1" runat="server"
 						SelectMethod="GetAll" 
 					>
					</data:LookUpCodesDataSource>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>



