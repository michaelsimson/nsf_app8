<%@ Control Language="C#" ClassName="ContestFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ContestCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestCode" Text='<%# Bind("ContestCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataContestCode" runat="server" Display="Dynamic" ControlToValidate="dataContestCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestTypeID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestTypeID" Text='<%# Bind("ContestTypeID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestTypeID" runat="server" Display="Dynamic" ControlToValidate="dataContestTypeID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">NSFChapterID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataNSFChapterID" Text='<%# Bind("NSFChapterID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataNSFChapterID" runat="server" Display="Dynamic" ControlToValidate="dataNSFChapterID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestCategoryID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestCategoryID" Text='<%# Bind("ContestCategoryID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestCategoryID" runat="server" Display="Dynamic" ControlToValidate="dataContestCategoryID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Phase:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPhase" Text='<%# Bind("Phase") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataPhase" runat="server" Display="Dynamic" ControlToValidate="dataPhase" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">GroupNo:</td>
				<td>
					<asp:TextBox runat="server" ID="dataGroupNo" Text='<%# Bind("GroupNo") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataGroupNo" runat="server" Display="Dynamic" ControlToValidate="dataGroupNo" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">BadgeNoBeg:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBadgeNoBeg" Text='<%# Bind("BadgeNoBeg") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataBadgeNoBeg" runat="server" Display="Dynamic" ControlToValidate="dataBadgeNoBeg" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">BadgeNoEnd:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBadgeNoEnd" Text='<%# Bind("BadgeNoEnd") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataBadgeNoEnd" runat="server" Display="Dynamic" ControlToValidate="dataBadgeNoEnd" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestDate" Text='<%# Bind("ContestDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataContestDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">StartTime:</td>
				<td>
					<asp:TextBox runat="server" ID="dataStartTime" Text='<%# Bind("StartTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EndTime:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEndTime" Text='<%# Bind("EndTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Building:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBuilding" Text='<%# Bind("Building") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestYear" Text='<%# Bind("ContestYear") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Room:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRoom" Text='<%# Bind("Room") %>' MaxLength="20"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">RegistrationDeadline:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRegistrationDeadline" Text='<%# Bind("RegistrationDeadline", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataRegistrationDeadline" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">CheckinTime:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCheckinTime" Text='<%# Bind("CheckinTime") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">VenueId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataVenueId" Text='<%# Bind("VenueId") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataVenueId" runat="server" Display="Dynamic" ControlToValidate="dataVenueId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">SponsorId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSponsorId" Text='<%# Bind("SponsorId") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataSponsorId" runat="server" Display="Dynamic" ControlToValidate="dataSponsorId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">SponsorType:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSponsorType" Text='<%# Bind("SponsorType") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventId" Text='<%# Bind("EventId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventCode" Text='<%# Bind("EventCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventCode" runat="server" Display="Dynamic" ControlToValidate="dataEventCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupId" Text='<%# Bind("ProductGroupId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductId" Text='<%# Bind("ProductId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedDate" Text='<%# Bind("ModifiedDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifiedDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


