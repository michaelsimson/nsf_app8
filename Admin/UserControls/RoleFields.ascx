<%@ Control Language="C#" ClassName="RoleFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">RoleCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRoleCode" Text='<%# Bind("RoleCode") %>' MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataRoleCode" runat="server" Display="Dynamic" ControlToValidate="dataRoleCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">National:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataNational" DataValueField='<%# Bind("National") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				    <data:RoleDataSource runat="server" SelectMethod="Get" ></data:RoleDataSource>
				    
				</td>
			</tr>				
			<tr>
				<td class="literal">Zonal:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataZonal" DataValueField='<%# Bind("Zonal") %>' AppendDataBoundItems="true"> 
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Chapter:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataChapter" DataValueField='<%# Bind("Chapter") %>' AppendDataBoundItems="true"> 
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Finals:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataFinals" DataValueField='<%# Bind("Finals") %>' AppendDataBoundItems="true"> 
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
									</td>
			</tr>				
			<tr>
				<td class="literal">Cluster:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataCluster" DataValueField='<%# Bind("Cluster") %>' AppendDataBoundItems="true"> 
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem><asp:ListItem Text="" Value=""></asp:ListItem></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">IndiaChapter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataIndiaChapter" Text='<%# Bind("IndiaChapter") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Name:</td>
				<td>
					<asp:TextBox runat="server" ID="dataName" Text='<%# Bind("Name") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Selection:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSelection" Text='<%# Bind("Selection") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Description:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDescription" Text='<%# Bind("Description") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Restrictions:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRestrictions" Text='<%# Bind("Restrictions") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Resposibilities:</td>
				<td>
					<asp:TextBox runat="server" ID="dataResposibilities" Text='<%# Bind("Resposibilities") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
	<EditItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">RoleCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRoleCode" Text='<%# Bind("RoleCode") %>' MaxLength="20"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataRoleCode" runat="server" Display="Dynamic" ControlToValidate="dataRoleCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">National:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataNational" DataValueField='<%# Bind("National") %>' AppendDataBoundItems="true">
					<asp:ListItem Text="Y" Value="Y"></asp:ListItem><asp:ListItem Text="N" Value="N"></asp:ListItem></asp:DropDownList>
				    <data:RoleDataSource ID="RoleDataSource1" runat="server" SelectMethod="GetByRoleId" ></data:RoleDataSource>
				    
				</td>
			</tr>				
			<tr>
				<td class="literal">Zonal:</td>
				<td>
					<asp:DropDownList runat="server" ID="dataZonal" DataValueField='<%# Bind("Zonal") %>' ></asp:DropDownList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Chapter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChapter" Text='<%# Bind("Chapter") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Finals:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinals" Text='<%# Bind("Finals") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Cluster:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCluster" Text='<%# Bind("Cluster") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">IndiaChapter:</td>
				<td>
					<asp:TextBox runat="server" ID="dataIndiaChapter" Text='<%# Bind("IndiaChapter") %>' MaxLength="10"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Name:</td>
				<td>
					<asp:TextBox runat="server" ID="dataName" Text='<%# Bind("Name") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Selection:</td>
				<td>
					<asp:TextBox runat="server" ID="dataSelection" Text='<%# Bind("Selection") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Description:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDescription" Text='<%# Bind("Description") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Restrictions:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRestrictions" Text='<%# Bind("Restrictions") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Resposibilities:</td>
				<td>
					<asp:TextBox runat="server" ID="dataResposibilities" Text='<%# Bind("Resposibilities") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" /><asp:RequiredFieldValidator ID="ReqVal_dataCreateDate" runat="server" Display="Dynamic" ControlToValidate="dataCreateDate" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifyDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifyDate" Text='<%# Bind("ModifyDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifyDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>
	</EditItemTemplate>
</asp:FormView>


