<%@ Control Language="C#" ClassName="ContestantFields" %>

<asp:FormView ID="FormView1" runat="server">
	<ItemTemplate>
		<table border="0" cellpadding="3" cellspacing="1">
			<tr>
				<td class="literal">ContestCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestCode" Text='<%# Bind("ContestCode") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestCode" runat="server" Display="Dynamic" ControlToValidate="dataContestCode" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestYear:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestYear" Text='<%# Bind("ContestYear") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataContestYear" runat="server" Display="Dynamic" ControlToValidate="dataContestYear" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataContestYear" runat="server" Display="Dynamic" ControlToValidate="dataContestYear" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChildNumber:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChildNumber" Text='<%# Bind("ChildNumber") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataChildNumber" runat="server" Display="Dynamic" ControlToValidate="dataChildNumber" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataChildNumber" runat="server" Display="Dynamic" ControlToValidate="dataChildNumber" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">BadgeNumber:</td>
				<td>
					<asp:TextBox runat="server" ID="dataBadgeNumber" Text='<%# Bind("BadgeNumber") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">Score1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataScore1" Text='<%# Bind("Score1") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataScore1" runat="server" Display="Dynamic" ControlToValidate="dataScore1" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Score2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataScore2" Text='<%# Bind("Score2") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataScore2" runat="server" Display="Dynamic" ControlToValidate="dataScore2" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Score3:</td>
				<td>
					<asp:TextBox runat="server" ID="dataScore3" Text='<%# Bind("Score3") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataScore3" runat="server" Display="Dynamic" ControlToValidate="dataScore3" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">Rank:</td>
				<td>
					<asp:TextBox runat="server" ID="dataRank" Text='<%# Bind("Rank") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataRank" runat="server" Display="Dynamic" ControlToValidate="dataRank" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">NationalInvitee:</td>
				<td>
					<asp:RadioButtonList runat="server" ID="dataNationalInvitee" SelectedValue='<%# Bind("NationalInvitee") %>' RepeatDirection="Horizontal"><asp:ListItem Value="True" Text="Yes" Selected="True"></asp:ListItem><asp:ListItem Value="False" Text="No"></asp:ListItem></asp:RadioButtonList>
				</td>
			</tr>				
			<tr>
				<td class="literal">Fee:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFee" Text='<%# Bind("Fee") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFee" runat="server" Display="Dynamic" ControlToValidate="dataFee" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">DiscountedAmount:</td>
				<td>
					<asp:TextBox runat="server" ID="dataDiscountedAmount" Text='<%# Bind("DiscountedAmount") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataDiscountedAmount" runat="server" Display="Dynamic" ControlToValidate="dataDiscountedAmount" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentDate" Text='<%# Bind("PaymentDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataPaymentDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentMode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentMode" Text='<%# Bind("PaymentMode") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentReference:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentReference" Text='<%# Bind("PaymentReference") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">PaymentNotes:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPaymentNotes" Text='<%# Bind("PaymentNotes") %>'  TextMode="MultiLine"  Width="250px" Rows="5"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreateDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreateDate" Text='<%# Bind("CreateDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataCreateDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestantId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestantId" Text='<%# Bind("ContestantId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataContestantId" runat="server" Display="Dynamic" ControlToValidate="dataContestantId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataContestantId" runat="server" Display="Dynamic" ControlToValidate="dataContestantId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">PhotoImage:</td>
				<td>
					<asp:TextBox runat="server" ID="dataPhotoImage" Text='<%# Bind("PhotoImage") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPaymentStatus:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPaymentStatus" Text='<%# Bind("FinalsPaymentStatus") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">InviteDeclineComments:</td>
				<td>
					<asp:TextBox runat="server" ID="dataInviteDeclineComments" Text='<%# Bind("InviteDeclineComments") %>' MaxLength="100"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">NationalInviteeStatus:</td>
				<td>
					<asp:TextBox runat="server" ID="dataNationalInviteeStatus" Text='<%# Bind("NationalInviteeStatus") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataNationalInviteeStatus" runat="server" Display="Dynamic" ControlToValidate="dataNationalInviteeStatus" ErrorMessage="Invalid value" MaximumValue="255" MinimumValue="0" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">InviteUpdatedDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataInviteUpdatedDate" Text='<%# Bind("InviteUpdatedDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataInviteUpdatedDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsAmount:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsAmount" Text='<%# Bind("FinalsAmount") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFinalsAmount" runat="server" Display="Dynamic" ControlToValidate="dataFinalsAmount" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Currency"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPaymentDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPaymentDate" Text='<%# Bind("FinalsPaymentDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataFinalsPaymentDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPaymentReference:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPaymentReference" Text='<%# Bind("FinalsPaymentReference") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPaymentMode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPaymentMode" Text='<%# Bind("FinalsPaymentMode") %>' MaxLength="30"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsBadgeNumber:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsBadgeNumber" Text='<%# Bind("FinalsBadgeNumber") %>' MaxLength="50"></asp:TextBox>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPhase1:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPhase1" Text='<%# Bind("FinalsPhase1") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFinalsPhase1" runat="server" Display="Dynamic" ControlToValidate="dataFinalsPhase1" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPhase2:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPhase2" Text='<%# Bind("FinalsPhase2") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFinalsPhase2" runat="server" Display="Dynamic" ControlToValidate="dataFinalsPhase2" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsRank:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsRank" Text='<%# Bind("FinalsRank") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFinalsRank" runat="server" Display="Dynamic" ControlToValidate="dataFinalsRank" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">FinalsPercentile:</td>
				<td>
					<asp:TextBox runat="server" ID="dataFinalsPercentile" Text='<%# Bind("FinalsPercentile") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataFinalsPercentile" runat="server" Display="Dynamic" ControlToValidate="dataFinalsPercentile" ErrorMessage="Invalid value" MaximumValue="999999999" MinimumValue="-999999999" Type="Double"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestCategoryID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestCategoryID" Text='<%# Bind("ContestCategoryID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestCategoryID" runat="server" Display="Dynamic" ControlToValidate="dataContestCategoryID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ContestID:</td>
				<td>
					<asp:TextBox runat="server" ID="dataContestID" Text='<%# Bind("ContestID") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataContestID" runat="server" Display="Dynamic" ControlToValidate="dataContestID" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ChildNumberOLD:</td>
				<td>
					<asp:TextBox runat="server" ID="dataChildNumberOLD" Text='<%# Bind("ChildNumberOLD") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataChildNumberOLD" runat="server" Display="Dynamic" ControlToValidate="dataChildNumberOLD" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventId" Text='<%# Bind("EventId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataEventId" runat="server" Display="Dynamic" ControlToValidate="dataEventId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">EventCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataEventCode" Text='<%# Bind("EventCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataEventCode" runat="server" Display="Dynamic" ControlToValidate="dataEventCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupId" Text='<%# Bind("ProductGroupId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductGroupId" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductGroupCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductGroupCode" Text='<%# Bind("ProductGroupCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductGroupCode" runat="server" Display="Dynamic" ControlToValidate="dataProductGroupCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductId:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductId" Text='<%# Bind("ProductId") %>'></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Required"></asp:RequiredFieldValidator><asp:RangeValidator ID="RangeVal_dataProductId" runat="server" Display="Dynamic" ControlToValidate="dataProductId" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ProductCode:</td>
				<td>
					<asp:TextBox runat="server" ID="dataProductCode" Text='<%# Bind("ProductCode") %>' MaxLength="10"></asp:TextBox><asp:RequiredFieldValidator ID="ReqVal_dataProductCode" runat="server" Display="Dynamic" ControlToValidate="dataProductCode" ErrorMessage="Required"></asp:RequiredFieldValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">CreatedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataCreatedBy" Text='<%# Bind("CreatedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataCreatedBy" runat="server" Display="Dynamic" ControlToValidate="dataCreatedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedDate:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedDate" Text='<%# Bind("ModifiedDate", "{0:d}") %>' MaxLength="10"></asp:TextBox><asp:ImageButton ID="cal_dataModifiedDate" runat="server" SkinID="CalendarImageButton" OnClientClick="javascript:showCalendarControl(this.previousSibling);return false;" />
				</td>
			</tr>				
			<tr>
				<td class="literal">ModifiedBy:</td>
				<td>
					<asp:TextBox runat="server" ID="dataModifiedBy" Text='<%# Bind("ModifiedBy") %>'></asp:TextBox><asp:RangeValidator ID="RangeVal_dataModifiedBy" runat="server" Display="Dynamic" ControlToValidate="dataModifiedBy" ErrorMessage="Invalid value" MaximumValue="2147483647" MinimumValue="-2147483648" Type="Integer"></asp:RangeValidator>
				</td>
			</tr>				
			
		</table>

	</ItemTemplate>
</asp:FormView>


