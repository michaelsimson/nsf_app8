
<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true"  CodeFile="VolunteerEdit.aspx.cs" Inherits="VolunteerEdit" Title="Volunteer Edit" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
    Volunteer - Add/Edit
    <asp:Label ID="lblMessage" runat="server" Style="z-index: 100; left: 245px; position: absolute;
        top: 41px"></asp:Label>
    &nbsp;&nbsp;
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataSourceID="VolEditDS" DataKeyNames="VolunteerID">
        <Columns>
            <asp:CommandField ShowDeleteButton="True" ShowEditButton="True" />
            <asp:BoundField DataField="VolunteerID" HeaderText="VolunteerID" InsertVisible="False"
                ReadOnly="True" SortExpression="VolunteerID" />
            <asp:BoundField DataField="MemberID" HeaderText="MemberID" SortExpression="MemberID" />
            <asp:BoundField DataField="RoleId" HeaderText="RoleId" SortExpression="RoleId" />
            <asp:BoundField DataField="RoleCode" HeaderText="RoleCode" SortExpression="RoleCode" />
            <asp:BoundField DataField="TeamLead" HeaderText="TeamLead" SortExpression="TeamLead" />
            <asp:BoundField DataField="EventYear" HeaderText="EventYear" SortExpression="EventYear" />
            <asp:BoundField DataField="EventId" HeaderText="EventId" SortExpression="EventId" />
            <asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode" />
            <asp:BoundField DataField="ChapterId" HeaderText="ChapterId" SortExpression="ChapterId" />
            <asp:BoundField DataField="ChapterCode" HeaderText="ChapterCode" SortExpression="ChapterCode" />
            <asp:BoundField DataField="National" HeaderText="National" SortExpression="National" />
            <asp:BoundField DataField="ZoneId" HeaderText="ZoneId" SortExpression="ZoneId" />
            <asp:BoundField DataField="ZoneCode" HeaderText="ZoneCode" SortExpression="ZoneCode" />
            <asp:BoundField DataField="Finals" HeaderText="Finals" SortExpression="Finals" />
            <asp:BoundField DataField="ClusterID" HeaderText="ClusterID" SortExpression="ClusterID" />
            <asp:BoundField DataField="ClusterCode" HeaderText="ClusterCode" SortExpression="ClusterCode" />
            <asp:BoundField DataField="IndiaChapter" HeaderText="IndiaChapter" SortExpression="IndiaChapter" />
            <asp:BoundField DataField="ProductGroupID" HeaderText="ProductGroupID" SortExpression="ProductGroupID" />
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />
            <asp:BoundField DataField="ProductID" HeaderText="ProductID" SortExpression="ProductID" />
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />
            <asp:BoundField DataField="IndiaChapterName" HeaderText="IndiaChapterName" SortExpression="IndiaChapterName" />
            <asp:BoundField DataField="AgentFlag" HeaderText="AgentFlag" SortExpression="AgentFlag" />
            <asp:BoundField DataField="WriteAccess" HeaderText="WriteAccess" SortExpression="WriteAccess" />
            <asp:BoundField DataField="Authorization" HeaderText="Authorization" SortExpression="Authorization" />
            <asp:BoundField DataField="Yahoogroup" HeaderText="Yahoogroup" SortExpression="Yahoogroup" />
            <asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate" />
            <asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy" />
            <asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate" />
            <asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy" />
        </Columns>
    </asp:GridView>
    <asp:ObjectDataSource ID="VolEditDS" runat="server" DeleteMethod="Delete" InsertMethod="Insert"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetData" TypeName="VolByMemIdTableAdapters.VolunteerTableAdapter"
        UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="Original_VolunteerID" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:QueryStringParameter Name="MemberID" QueryStringField="MemberID" Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="MemberID" Type="Int32" />
            <asp:Parameter Name="RoleId" Type="Int32" />
            <asp:Parameter Name="RoleCode" Type="String" />
            <asp:Parameter Name="TeamLead" Type="String" />
            <asp:Parameter Name="EventYear" Type="Int32" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="ChapterId" Type="Int32" />
            <asp:Parameter Name="ChapterCode" Type="String" />
            <asp:Parameter Name="National" Type="String" />
            <asp:Parameter Name="ZoneId" Type="Int32" />
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Finals" Type="String" />
            <asp:Parameter Name="ClusterID" Type="Int32" />
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="IndiaChapter" Type="String" />
            <asp:Parameter Name="ProductGroupID" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="ProductID" Type="Int32" />
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="IndiaChapterName" Type="String" />
            <asp:Parameter Name="AgentFlag" Type="String" />
            <asp:Parameter Name="WriteAccess" Type="String" />
            <asp:Parameter Name="Authorization" Type="String" />
            <asp:Parameter Name="Yahoogroup" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    &nbsp; &nbsp;&nbsp;
    </div>
    </asp:Content>



 

 
 
 