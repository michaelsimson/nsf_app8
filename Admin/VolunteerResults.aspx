<%@ Page Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VolunteerResults.aspx.cs" Inherits="Admin_VolunteerResults" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:GridView ID="gvIndSpouse" runat="server" AllowPaging="True" AllowSorting="True"
        AutoGenerateColumns="False" BorderStyle="Solid" Caption="All Members"
        CaptionAlign="Top" EnableSortingAndPagingCallbacks="True" PageSize="5" DataKeyNames="MemberID" OnRowCommand="gvIndSpouse_RowCommand" OnSelectedIndexChanged="gvIndSpouse_SelectedIndexChanged">
        <Columns>
            <asp:ButtonField DataTextField="automemberID" HeaderText="Add Role" Text="Add" />
            <asp:BoundField DataField="FirstName" HeaderText="First Name" />
            <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />
            <asp:BoundField DataField="LastName" HeaderText="Last Name" />
            <asp:BoundField DataField="Email" HeaderText="Email" />
            <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
            <asp:BoundField DataField="Address1" HeaderText="Address" />
            <asp:BoundField DataField="City" HeaderText="City" />
            <asp:BoundField DataField="Zip" HeaderText="Zip" />
            <asp:BoundField DataField="State" HeaderText="State" />
        </Columns>
        <HeaderStyle BorderStyle="Solid" BorderWidth="1px" />
    </asp:GridView>
    <asp:Label ID="lblMessage" runat="server" Style="z-index: 101; left: 32px; position: absolute;
        top: 273px"></asp:Label>
    <asp:HyperLink ID="hlnkMenu" runat="server" NavigateUrl="~/VolunteerFunctions.aspx"
        Style="z-index: 104; left: 23px; position: absolute; top: 307px">Go back to Menu</asp:HyperLink>
<hr />
    <asp:GridView ID="gvOrg" runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False"
        BorderWidth="2px" Caption="Organization" CaptionAlign="Top" PageSize="5" Style="z-index: 102;
        left: 285px; position: absolute; top: 371px">
        <Columns>
            <asp:HyperLinkField DataNavigateUrlFields="MEMBERID" DataNavigateUrlFormatString="~/dbdvOrg.aspx?Id={0}"
                DataTextField="MEMBERID" HeaderText="Member ID" />
            <asp:BoundField DataField="ORGANIZATION_NAME" HeaderText="Organization" />
            <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name" />
            <asp:BoundField DataField="MIDDLE_INITIAL" HeaderText="Initial" />
            <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name" />
        </Columns>
    </asp:GridView>
    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="~/Admin/VolunteerSearch.aspx"
        Style="z-index: 104; left: 142px; position: absolute; top: 309px">Go back to search</asp:HyperLink>
</asp:Content>


 

 
 
 