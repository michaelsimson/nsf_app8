
#region Imports...
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using nsf.Web.UI;
#endregion

public partial class VolunteerEdit : System.Web.UI.Page
{
	protected void Page_Load(object sender, EventArgs e)
	{		
		//FormUtil.RedirectAfterInsertUpdate(FormView1, "VolunteerEdit.aspx?{0}", VolunteerDataSource);
		//FormUtil.RedirectAfterAddNew(FormView1, "VolunteerEdit.aspx");
		//FormUtil.RedirectAfterCancel(FormView1, "Volunteer.aspx");
		//FormUtil.SetDefaultMode(FormView1, "MemberId");
        
	}
    public String WhereClause
    {
       get 
       {
           if (Request.QueryString["VolunteerId"] != null)
           {
               return string.Format("VolunteerId='{0}'", Request.QueryString["VolunteerId"].ToString());
           }
           return string.Empty;
       }
    }

    protected void FormView1_ItemInserting(object sender, FormViewInsertEventArgs e)
    {
     //   FormViewRow row = FormView1.Row;
      //  TextBox tb = (TextBox) row.FindControl("dataMemberId");
       
      //  if(tb.Text.Trim() == "1")
      //      Response.Redirect("VolunteerEdit.aspx?Id="+tb.Text.Trim());
               //Response.Redirect("Volunteer.aspx");
    }


    protected void GridView1_PreRender(object sender, EventArgs e)
    {
        if (GridView1.Rows.Count < 1)
            lblMessage.Text = "No records for this volunteer exist! Member ID=" +
                Request.QueryString["MemberId"].ToString();
    }
}



 