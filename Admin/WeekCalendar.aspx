
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="WeekCalendar.aspx.cs" Inherits="WeekCalendar" Title="WeekCalendar List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
		<data:EntityGridView ID="GridView1" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="WeekCalendarDataSource"
			DataKeyNames="WeekId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_WeekCalendar.xls"  
			AllowSorting="true"
			AllowPaging="false"			
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />				
				<asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId" ReadOnly />
				<asp:BoundField DataField="SatDay1" HeaderText="SatDay1" SortExpression="SatDay1"  />
				<asp:BoundField DataField="SunDay2" HeaderText="SunDay2" SortExpression="SunDay2"  />
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No WeekCalendar Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
		<br />
		<br />
			
		<data:WeekCalendarDataSource ID="WeekCalendarDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="WeekId=1" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:WeekCalendarDataSource>
	    
		<data:EntityGridView ID="GridView2" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="WeekCalendarDataSource2"
			DataKeyNames="WeekId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_WeekCalendar.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			>
			<Columns>
				<asp:CommandField />				
				<asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId" ReadOnly />
				<asp:BoundField DataField="SatDay1" HeaderText="SatDay1" SortExpression="SatDay1"  />
				<asp:BoundField DataField="SunDay2" HeaderText="SunDay2" SortExpression="SunDay2"  />
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No WeekCalendar Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
	
	<data:WeekCalendarDataSource ID="WeekCalendarDataSource2" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="WeekId>1" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:WeekCalendarDataSource>    		
</asp:Content>




 

 
 
 