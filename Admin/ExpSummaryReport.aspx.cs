﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Text;
using System.IO;


public partial class ExpSummaryReport : System.Web.UI.Page
{
    //  public DataRow drgrants;
    int i;
    string firstquert;
    DataTable Mergecopy = new DataTable();
    int fiscyear;
    public Int32 Expcategorystr = 0;
    public Int32 Expchapterystr = 0;
    double less;
    double merchless;
    double grantcal;
    double grantmerch;
    string grantval;
    DataSet dsnew;
    DataTable dtnw;
    string clusqry;
    string Zoneqry;
    string Qrycondition;
    string calc;
    int endall;
    int Start;
    string Qrygrants;
    int newvar;
    string str;
    string Qry;
    string StaYear = System.DateTime.Now.Year.ToString();
    DataTable dtnewgrid;
    protected void Page_Load(object sender, EventArgs e)
    {
       
        if (!IsPostBack)
        {
           GRcategory.Visible = false;
        
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (Session["RoleId"].ToString() == "38")
            {
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select count(*) from Volunteer where RoleId=" + Session["RoleId"] + " and [National]='Y' and MemberID = " + Session["LoginID"])) > 0)
                {
                    hdnTechNational.Value = "Y";

                }

                else
                {
                    hdnTechNational.Value = "N";

                }
            }

            if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || ((Session["RoleId"].ToString() == "38") && (hdnTechNational.Value == "Y")) || (Session["RoleID"].ToString() == "3") || (Session["RoleID"].ToString() == "4") || (Session["RoleID"].ToString() == "5")))
            {
                if ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2"))
                {
                    BtnExpcat.Visible = true;
                    if (!IsPostBack)
                    {
                        Expcategory();
                    }
                }

                if (!IsPostBack)
                {
                    Btnsumm.Visible = false;
                    if ((Session["RoleId"].ToString() == "5"))
                    {
                        Chapter();
                        Cluster();
                        Event();
                        Yearscount();
                        years();
                    }
                    else if ((Session["RoleId"].ToString() == "3"))
                    {

                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct zoneid from volunteer where MemberID='" + Session["LoginID"] + "' and TeamLead='" + Session["TL"] + "'  and zoneid is not null");
                        //DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select distinct clusterid,clustercode from volunteer where MemberID='" + Session["LoginID"] + "'");
                        DataTable dt = ds.Tables[0];
                        Txthidden.Text = dt.Rows[0]["zoneid"].ToString();
                        //Response.Write(Txthidden.Text);
                        if (Txthidden.Text != "")
                        {
                            Zone();
                            Cluster();
                            Chapter();
                            Event();
                            Yearscount();
                            years();
                        }
                        else
                        {
                            Pnldisp.Visible = false;
                            lblNoPermission.Text = "Sorry, you don't have ZoneCode ";
                        }

                    }
                    else if ((Session["RoleId"].ToString() == "4"))
                    {

                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct clusterid from volunteer where MemberID='" + Session["LoginID"] + "' and clusterid is not null");
                        DataTable dt = ds.Tables[0];
                        Txthidden.Text = dt.Rows[0]["clusterid"].ToString();
                        if (Txthidden.Text != "")
                        {
                            Cluster();
                            Zone();
                            Chapter();
                            Event();
                            Yearscount();
                            years();
                        }
                        else
                        {
                            Pnldisp.Visible = false;
                            lblNoPermission.Text = "Sorry, you don't have Cluster ";
                        }

                    }

                    else
                    {
                        Zone();
                        Event();
                        Yearscount();
                        years();
                    }
                }
            }
            else
            {
                Pnldisp.Visible = false;
                lblNoPermission.Visible = true;
                lblNoPermission.Text = "Sorry, you are not authorized to access this page ";
            }
        }
            }

    }
    protected void Event()
    {
        ddevent.Items.Clear();

        ddevent.Items.Insert(0, new ListItem("PrepClub", "19"));
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
        {
            //ddevent.Items.Insert(0, new ListItem("Finals", "1"));
            ddevent.Items.Insert(0, new ListItem("Online Coaching", "13"));
        }
        ddevent.Items.Insert(0, new ListItem("Workshop", "3"));
        ddevent.Items.Insert(0, new ListItem("Regionals", "2"));
        if ((Session["RoleId"] != null) && ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85")))
        {
            ddevent.Items.Insert(0, new ListItem("Finals", "1"));
            //ddevent.Items.Insert(0, new ListItem("Online Coaching", "13"));
        }
        ddevent.Items.Insert(0, new ListItem("All", "-1"));
        ddevent.Items.Insert(0, new ListItem("[Select Event]", "0"));




    }
    protected void Expcategory()
    {
        string Expcatqry = "select distinct   ExpCatDesc,ExpCatID from ExpenseCategory order by ExpCatDesc";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expcatqry);
        DDcategory.DataSource = ds;
        DataTable dt = ds.Tables[0];
        DDcategory.DataTextField = "ExpCatDesc";
        DDcategory.DataValueField = "ExpCatID";
        DDcategory.DataBind();
        DDcategory.Items.Insert(0, "[Select Category]");

    }
    protected void Yearscount()
    {
        ddNoyear.Items.Clear();
        ddNoyear.Items.Add("[Select No of Years]");
        ddNoyear.Items.Add("1");
        ddNoyear.Items.Add("2");
        ddNoyear.Items.Add("3");
        ddNoyear.Items.Add("4");
        ddNoyear.Items.Add("5");
        ddNoyear.Items.Add("6");
        ddNoyear.Items.Add("7");
        ddNoyear.Items.Add("8");
        ddNoyear.Items.Add("9");
        ddNoyear.Items.Add("10");

    }
    protected void years()
    {
        DDyear.Items.Clear();
        DDyear.Items.Add("Year");
        DDyear.Items.Add("Calendar Year");
        DDyear.Items.Add("Fiscal Year");


    }
    protected void Zone()
    {
        try
        {
            ddZone.Enabled = true;
            ddZone.Items.Clear();
            ddZone.Items.Insert(0, "[Select Zone]");


            if ((Session["RoleId"].ToString() == "5"))
            {
                Zoneqry = "select ZoneCode,ZoneId from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
            }
            else if ((Session["RoleId"].ToString() == "3"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Zone where  ZoneId='" + Txthidden.Text + "'  and ZoneCode is not null";
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                Zoneqry = "select ZoneCode,ZoneId from Cluster where ClusterId='" + Txthidden.Text + "'";
            }
            else
            {

                if (ddevent.SelectedItem.Text == "All")
                {

                    if ((Session["RoleId"].ToString() == "1") || (Session["RoleID"].ToString() == "2") || (Session["RoleID"].ToString() == "84") || (Session["RoleID"].ToString() == "85") || ((Session["RoleId"].ToString() == "38") && (hdnTechNational.Value == "Y")))
                    {

                        Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                    }

                }
                else if (ddevent.SelectedItem.Text == "All")
                {
                    //ddZone.Items.Clear();
                    //ddZone.Items.Insert(0, "All");
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("All", "-1"));
                    ddCluster.Items.Insert(0, "All");
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Finals")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Finals", "1"));
                    ddZone.Enabled = false;
                    return;
                }
                else if (ddevent.SelectedItem.Text == "Online Coaching")
                {
                    ddZone.Items.Clear();
                    ddZone.Items.Insert(0, new ListItem("Coaching", "13"));
                    ddZone.Enabled = false;
                    return;
                }
                else
                {
                    Zoneqry = "select distinct Name,ZoneId from Zone where Status='A'";
                }

            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Zoneqry);
            ddZone.DataSource = ds;
            if (ds.Tables[0].Rows.Count > 0)
            {
                if (ds.Tables[0].Rows.Count == 1)
                {
                    ddZone.Items.Clear();
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    ddZone.Enabled = false;

                }
                else
                {
                    ddZone.DataTextField = ds.Tables[0].Columns[0].ColumnName;
                    ddZone.DataValueField = "ZoneId";
                    ddZone.DataBind();
                    if (ddevent.SelectedItem.Text == "All")
                    {
                        ddZone.Items.Insert(0, "[Select Zone]");
                        ddZone.Items.Insert(0, "All");

                    }
                    else
                    {
                        ddZone.Items.Insert(0, "All");
                        //ddZone.SelectedIndex = 0;
                        ddZone.Items.Insert(0, "[Select Zone]");
                    }
                }
            }


            else
            {

                ddZone.Items.Insert(0, "[Select Zone]");

            }


        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }

    }

    protected void Cluster()
    {
        try
        {
            ddCluster.Enabled = true;
            ddCluster.Items.Clear();
            ddCluster.Items.Insert(0, "[Select Cluster]");
            if ((Session["RoleId"].ToString() == "5"))
            {
                string clusqry = "select ClusterId,clustercode from chapter where ChapterID='" + ddchapter.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;

                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }
            else if ((Session["RoleId"].ToString() == "4"))
            {
                string clusqry = "select clustercode,ClusterId from Cluster where clusterId='" + Txthidden.Text + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = false;

                }
                else
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "ClusterId";
                    ddCluster.DataBind();
                    Zone();
                    ddCluster.Enabled = true;
                }
            }

            else if ((Session["RoleId"].ToString() == "3"))
            {
                string clusqry = "select clustercode,clusterid from cluster where ZoneId='" + ddZone.SelectedValue + "'";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;

                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");

                }

            }
            else
            {

                if (ddZone.SelectedItem.Text != "[Select Zone]")
                {
                    if (ddZone.SelectedItem.Text == "All")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("All", "-1"));
                        // ddCluster.Items.Insert(0, "All");
                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Finals", "1"));
                        //ddCluster.Items.Insert(0, "Coaching");
                        ddCluster.Enabled = false;


                    }
                    else if (ddevent.SelectedItem.Text == "Online Coaching")
                    {
                        ddCluster.Items.Clear();
                        ddCluster.Items.Insert(0, new ListItem("Coaching", "13"));
                        //ddCluster.Items.Insert(0, "Coaching");
                        ddCluster.Enabled = false;

                    }
                    else
                    {
                        DataSet ds;
                        if (ddZone.SelectedItem.Text == "All")
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A'");
                        }
                        else
                        {
                            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ClusterID from Cluster where Status='A' and ZoneID='" + ddZone.SelectedValue + "'");
                        }
                        ddCluster.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddCluster.DataTextField = "Name";
                            ddCluster.DataValueField = "ClusterID";
                            ddCluster.DataBind();
                            ddCluster.Items.Insert(0, "All");
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                        else
                        {
                            ddCluster.Items.Clear();
                            ddCluster.Items.Insert(0, "[Select Cluster]");
                        }
                    }

                }

                else
                {

                    ddCluster.Items.Clear();
                    ddCluster.Items.Insert(0, "[Select Cluster]");
                }
            }
            if (ddZone.SelectedItem.Text == "All")
            {
                string clusqry = "select  distinct clustercode,clusterid from cluster where Status='A' ";
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, clusqry);
                ddCluster.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Enabled = false;

                }
                else
                {
                    ddCluster.Enabled = true;
                    ddCluster.Items.Clear();
                    ddCluster.DataTextField = "clustercode";
                    ddCluster.DataValueField = "clusterid";
                    ddCluster.DataBind();
                    ddCluster.Items.Insert(0, "All");
                }
            }

        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }

    protected void Chapter()
    {

        try
        {
            ddchapter.Enabled = true;
            if ((Session["RoleId"].ToString() == "5"))
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from volunteer where  MemberID='" + Session["LoginID"] + "' and ChapterId is not null");
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {
                    ddchapter.Items.Clear();
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;


                }
                else
                {
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                }
            }

            else if ((Session["RoleId"].ToString() == "4"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct chapterid,chaptercode from chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "'");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {

                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        Cluster();
                        ddchapter.Enabled = false;


                    }
                    else
                    {
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }


            else if ((Session["RoleId"].ToString() == "3"))
            {
                ddchapter.Items.Clear();
                ddchapter.Items.Insert(0, "All");
                if (ddCluster.SelectedItem.Text != "All")
                {
                    DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select chapterid,chaptercode from chapter where clusterId='" + ddCluster.SelectedValue + "' and ChapterId is not null");
                    ddchapter.DataSource = ds;
                    DataTable dt = ds.Tables[0];
                    if (dt.Rows.Count == 1)
                    {

                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        //Cluster();
                        ddchapter.Enabled = false;


                    }
                    else
                    {
                        ddchapter.Enabled = true;
                        ddchapter.DataTextField = "chaptercode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();
                        ddchapter.Items.Insert(0, "All");
                    }
                }
            }

            else
            {

                if (ddCluster.SelectedItem.Text != "[Select Cluster]")
                {
                    if (ddCluster.SelectedItem.Text == "All")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, "All");

                    }
                    else if (ddevent.SelectedItem.Text == "Finals")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Finals, US", "1"));
                        //ddCluster.Items.Insert(0, "Coaching");
                        ddchapter.Enabled = false;


                    }
                    else if (ddevent.SelectedItem.Text == "Online Coaching")
                    {
                        ddchapter.Items.Clear();
                        ddchapter.Items.Insert(0, new ListItem("Coaching,US", "112"));
                        //ddCluster.Items.Insert(0, "Coaching");
                        ddchapter.Enabled = false;

                    }
                    else
                    {
                        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select distinct Name,ChapterID from Chapter where Status='A' and ClusterId='" + ddCluster.SelectedValue + "'");
                        ddchapter.DataSource = ds;
                        if (ds.Tables[0].Rows.Count > 0)
                        {
                            ddchapter.DataTextField = "Name";
                            ddchapter.DataValueField = "ChapterID";
                            ddchapter.DataBind();
                            ddchapter.Items.Insert(0, "All");
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }
                        else
                        {
                            ddchapter.Items.Insert(0, "[Select Chapter]");
                        }

                    }
                }

                else
                {
                    ddchapter.Items.Clear();
                    ddchapter.Items.Insert(0, "[Select Chapter]");
                }
            }
            if (ddCluster.SelectedItem.Text == "All")
            {
                string ClusterFlqry;
                string WhereCluster;
                ClusterFlqry = "select distinct chapterid,chaptercode from chapter where  ChapterId is not null";
                WhereCluster = ClusterFlqry + Filterdropdown();
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, WhereCluster);
                ddchapter.DataSource = ds;
                DataTable dt = ds.Tables[0];
                if (dt.Rows.Count == 1)
                {

                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    Cluster();
                    ddchapter.Enabled = false;


                }
                else
                {
                    ddchapter.Enabled = true;
                    ddchapter.DataTextField = "chaptercode";
                    ddchapter.DataValueField = "ChapterID";
                    ddchapter.DataBind();
                    ddchapter.Items.Insert(0, "All");
                }
            }
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    protected string Filterdropdown()
    {
        string iCondtions = string.Empty;


        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                iCondtions += " and ZoneId=" + ddZone.SelectedValue;
            }


        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                iCondtions += " and ClusterId=" + ddCluster.SelectedValue;
            }


        }



        return iCondtions;

    }
    protected void ddZone_SelectedIndexChanged(object sender, EventArgs e)
    {

        Cluster();
        Chapter();

    }

    protected void ddCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        Chapter();
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

        button();   
       
    }
    protected void button()
    {
        try
        {
            int count = Griddisp.Columns.Count;

            while (count > 1)
            {
                Griddisp.Columns.RemoveAt(count - 1);
                --count;
            }
        
            Griddisp.DataSource = null;
            Griddisp.DataBind();
            dtnewgrid = new DataTable();
            Mergecopy = new DataTable();
         
    
            DataSet emptds = new DataSet();
            DataTable dtempty = new DataTable();
            //dtempty = emptds.Tables[0];

            DataTable dtgrants = new DataTable();
            DataRow drng = dtgrants.NewRow();

            if ((DDcategory.Visible != true) && (ddevent.SelectedItem.Text != "[Select Event]") && (ddZone.SelectedItem.Text != "[Select Zone]") && (ddCluster.SelectedItem.Text != "[Select Cluster]") && (ddchapter.SelectedItem.Text != "[Select Chapter]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]") || (DDcategory.Visible == true) && (ddevent.SelectedItem.Text != "[Select Event]") && (DDyear.SelectedItem.Text != "Year") && (ddNoyear.SelectedItem.Text != "[Select No of Years]"))
            {
                lblall.Visible = false;
                lbldisp.Visible = false;
                if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
                {
                    calc = ddNoyear.SelectedItem.Text;
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);

                }
                else
                {
                    Start = Convert.ToInt32(StaYear);
                    endall = Convert.ToInt32(StaYear) - 5;

                }
                // DataRow drgrants = dtnewgrid.NewRow();

                //Qrycondition = "select EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount ,year(Ej.DatePaid) as Year from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterIDwhere ej.paid='y' and ej.CheckNumber>0 ";
                bool Isfalse = false;

                //dtgrants[0] = "Grants (Scholarships and Prorgam Related Funding)";
                for (i = endall; i <= Start; i++)
                {
                    // drng[i] = "567";

                    if (DDyear.SelectedItem.Text != "Fiscal Year")
                    {

                        if (DDcategory.Visible == true)
                        {
                            Qrycondition = "with tbl as (select  TOP 100 PERCENT ej.Tochapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                        }
                        else
                        {

                            Qrycondition = "with tbl as (select EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";
                        }


                    }
                    else
                    {
                        fiscyear = i + 1;
                        if (DDcategory.Visible == true)
                        {
                            Qrycondition = "with tbl as (select TOP 100 PERCENT  ej.Tochapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min (CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";

                        }
                        else
                        {
                            Qrycondition = "with tbl as (select  EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,sum(Ej.ExpenseAmount) as ExpenseAmount,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min(CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and  Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";
                        }

                    }

                    if (DDcategory.Visible == true)
                    {
                        Qry = Qrycondition + genwherecategorycondition();
                    }
                    else
                    {
                        Qry = Qrycondition + genWhereConditons();
                    }

                    dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                    DataTable dtmerge = new DataTable();
                    dtmerge = dsnew.Tables[0];
                    if (DDcategory.Visible == true)
                    {
                        firstquert = "with tbl as (select TOP 100 PERCENT ej.Tochapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                        firstquert = firstquert + genwherecategorycondition();

                    }
                    else
                    {
                        firstquert = "with tbl as (select  EC.ExpCatID,EC.ExpCatDesc, EC.AccountName,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                        firstquert = firstquert + genWhereConditonsnew();
                    }

                    DataSet dsnew1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, firstquert);

                    DataTable dtm = new DataTable();
                    dtm = dsnew1.Tables[0];
                    if (Isfalse == false)
                    {
                        Mergecopy = dtm.Copy();
                        if (dtm.Rows.Count > 0)
                        {
                            Isfalse = true;
                            Mergecopy = dtm.Copy();

                        }
                    }

                    if (Isfalse == true)
                    {
                        if (dtmerge.Rows.Count > 0)
                        {
                            for (int j = 0; j <= dtmerge.Rows.Count - 1; j++)
                            {
                                Label lbn = new Label();
                                lbn.Text = dtmerge.Rows[j][0].ToString();
                                for (int h = 0; h <= Mergecopy.Rows.Count - 1; h++)
                                {
                                    Label lb = new Label();
                                    lb.Text = Mergecopy.Rows[h][0].ToString();
                                    if ((dtmerge.Rows[j][0].ToString() == Mergecopy.Rows[h][0].ToString()))
                                    {
                                        string s = dtmerge.Rows[j]["" + i + ""].ToString();
                                        Mergecopy.Rows[h]["" + i + ""] = s;

                                        break;
                                    }



                                }


                            }
                        }


                    }
                }

                dtnewgrid = Mergecopy;


                if (Mergecopy.Rows.Count > 0)
                {
                    dtnewgrid = Mergecopy;
                    DataRow dr = dtnewgrid.NewRow();
                    DataRow drtotal = dtnewgrid.NewRow();
                    DataRow drblank = dtnewgrid.NewRow();
                    DataRow drgrants = dtnewgrid.NewRow();
                    DataRow drlessgrants = dtnewgrid.NewRow();
                    DataRow drmerVr = dtnewgrid.NewRow();
                    drmerVr[1] = "Total less Grants less Merchandise for Sale";
                    //drmerVr[0] = string.Empty;
                    drmerVr[2] = string.Empty;


                    dr[1] = "Total (Sum)";
                    // dr[0] = string.Empty;
                    dr[2] = string.Empty;
                    drgrants[1] = "Grants (Scholarships and Prorgam Related Funding)";
                    //drgrants[0] = "";
                    drgrants[2] = "";
                    drlessgrants[1] = "Total less Grants";
                    // drlessgrants[0] = "";
                    drlessgrants[2] = "";
                    drtotal[1] = "Total (Calculated)";
                    // drtotal[0] = "";
                    drtotal[2] = "";
                    if (DDcategory.Visible == true)
                    {
                        newvar = 3;
                    }
                    else
                    {
                        newvar = 3;
                    }
                    for (int i = newvar; i <= dtnewgrid.Columns.Count - 1; i++)
                    {
                        dr[i] = 0;
                        double sum = 0;
                        string suma = "";
                        foreach (DataRow drnew in dtnewgrid.Rows)
                        {
                            if (!DBNull.Value.Equals(drnew[i]))
                            {
                                suma = drnew[i].ToString();
                                if (suma != "")
                                {
                                    sum += Convert.ToDouble(drnew[i]);
                                }
                            }
                        }
                        dr[i] = sum;
                    }
                    if (DDcategory.Visible == true)
                    {
                        dtnewgrid.Rows.Add(dr);
                    }
                    if (DDcategory.Visible != true)
                    {

                        for (int i = 3; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            //  Object cellValue = dtnewgrid.Rows[0][i];

                            string qrygrant;
                            string totalcalc;
                            string totQry;
                            string Qrymerch;
                            string Qrymerchcond;
                            String colName = dtnewgrid.Columns[i].ColumnName;
                            if (DDyear.SelectedItem.Text != "Fiscal Year")
                            {
                                Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "'  and ej.CheckNumber>0";
                                totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '01/01/" + colName + "' and '12/31/" + colName + "' and  CheckNumber>0   and Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                            }
                            else
                            {
                                int colfisc = Convert.ToInt32(colName) + 1;
                                Qrygrants = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where   Ej.ExpCatID in (29,30,31,32) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'  and ej.CheckNumber>0";
                                Qrymerch = "select sum(Ej.ExpenseAmount) as ExpenseAmount from Expjournal Ej  inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where  Ej.ExpCatID in (15) and Ej.DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "'   and ej.CheckNumber>0";
                                totalcalc = "select ISnull (sum(Ej.ExpenseAmount),0) as expense from ExpJournal Ej inner join ExpenseCategory EC   on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where DatePaid between '05/01/" + colName + "' and '04/30/" + colfisc + "' and  CheckNumber>0   and  Ej.ExpCatID is not null and Ej.ToChapterID is not null";
                            }
                            Qrymerchcond = Qrymerch + gengrantWhereConditons();
                            qrygrant = Qrygrants + gengrantWhereConditons();
                            totQry = totalcalc + gengrantWhereConditons();
                            DataSet dsnewmerge = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrymerchcond);
                            DataTable dtmerch = dsnewmerge.Tables[0];

                            dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, qrygrant);
                            DataTable dtgrantcal = dsnew.Tables[0];
                            DataSet dsnewtot = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, totQry);
                            DataTable dtnewtot = dsnewtot.Tables[0];

                            Label lblgrant = new Label();
                            Label lbltotcalc = new Label();
                            Label lblmerch = new Label();

                            lbltotcalc.Text = dtnewtot.Rows[0]["expense"].ToString();
                            lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                            lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                            if (lblgrant.Text == "")
                            {
                                lblgrant.Text = "0";
                            }
                            else
                            {
                                lblgrant.Text = dtgrantcal.Rows[0]["ExpenseAmount"].ToString();
                            }
                            if (!DBNull.Value.Equals(lblgrant.Text))
                            {
                                grantcal = Convert.ToDouble(lblgrant.Text);

                            }

                            if (lblmerch.Text == "")
                            {
                                lblmerch.Text = "0";
                            }
                            else
                            {
                                lblmerch.Text = dtmerch.Rows[0]["ExpenseAmount"].ToString();
                            }
                            if (!DBNull.Value.Equals(lblmerch.Text))
                            {
                                grantmerch = Convert.ToDouble(lblmerch.Text);

                            }

                            drgrants[i] = grantcal;
                            drmerVr[i] = grantmerch;
                            drtotal[i] = lbltotcalc.Text;

                        }
                        for (int i = 3; i <= dtnewgrid.Columns.Count - 1; i++)
                        {
                            //dr[i] = 0;


                            if (!DBNull.Value.Equals(dr[i]))
                            {
                                double valTot = Convert.ToDouble(dr[i].ToString());
                                // Response.Write(valTot);
                                double grants = Convert.ToDouble(drgrants[i].ToString());
                                double merchval = Convert.ToDouble(drmerVr[i].ToString());
                                less = valTot - grants;
                                merchless = less - merchval;

                            }

                            drlessgrants[i] = less;
                            drmerVr[i] = merchless;

                        }


                        // dtnewgrid.Rows.Add(dr);

                        dtnewgrid.Rows.Add(dr);
                        dtnewgrid.Rows.Add(drtotal);
                        dtnewgrid.Rows.Add(drblank);
                        dtnewgrid.Rows.Add(drgrants);
                        dtnewgrid.Rows.Add(drlessgrants);
                        dtnewgrid.Rows.Add(drmerVr);
                    }
                    // dtnewgrid.Merge(dtgrants);

                    Button2.Enabled = true;
                    GridView1.Visible = false;
                    DataTable dt = new DataTable();
                    dt = dsnew.Tables[0];
                    DataTable dt1 = new DataTable();
                    dt1.Columns.Add("Select Event", typeof(string));
                    if (DDcategory.Visible != true)
                    {

                        dt1.Columns.Add("Select Zone", typeof(string));
                        dt1.Columns.Add("Select Cluster", typeof(string));
                        dt1.Columns.Add("Select Chapter", typeof(string));
                    }
                    dt1.Columns.Add("Select No Of years", typeof(string));
                    dt1.Columns.Add("Select Year", typeof(string));
                    DataRow drow = dt1.NewRow();
                    DataRow drow1 = dt1.NewRow();
                    drow["Select Event"] = ddevent.SelectedItem.Text;
                    if (DDcategory.Visible != true)
                    {
                        drow["Select Zone"] = ddZone.SelectedItem.Text;
                        drow["Select Cluster"] = ddCluster.SelectedItem.Text;
                        drow["Select Chapter"] = ddchapter.SelectedItem.Text;
                    }
                    drow["Select No Of years"] = ddNoyear.SelectedItem.Text;
                    drow["Select Year"] = DDyear.SelectedItem.Text;
                    drow1["Select Event"] = string.Empty;
                    if (DDcategory.Visible != true)
                    {

                        drow1["Select Zone"] = string.Empty;
                        drow1["Select Cluster"] = string.Empty;
                        drow1["Select Chapter"] = string.Empty;
                    }
                    drow1["Select No Of years"] = string.Empty;
                    drow1["Select Year"] = string.Empty;
                    dt1.Rows.InsertAt(drow1, 1);
                    dt1.Rows.InsertAt(drow, 0);

                    // GridView1.DataSource = dt1;
                    // GridView1.DataBind();
                    //foreach (DataColumn col in dtnewgrid.Columns)

                    for (int c = 0; c < dtnewgrid.Columns.Count; c++)
                    {

                        DataColumn col = new DataColumn();
                        BoundField boundField = new BoundField();


                        //Initalize the DataField value.
                        boundField.DataField = dtnewgrid.Columns[c].ColumnName.ToString();
                        boundField.HeaderText = dtnewgrid.Columns[c].ColumnName.ToString();

                        if (c == 0)
                        {
                            //boundField.ItemStyle.CssClass = "Hide";
                            //boundField.HeaderStyle.CssClass = "Hide";

                        }


                        //
                        // this.Griddisp.Rows.Clear();

                       // if (Griddisp.Columns.Count > c + 2)
                        //{
                         //   Griddisp.Columns.RemoveAt(c + 2);
                       // }
                       // Griddisp.Columns.Insert(c, boundField);

                       // if (Griddisp.Columns.Count != dtnewgrid.Columns.Count)
                       // {
                         Griddisp.Columns.Add(boundField);
                       // }




                    }







                    // Griddisp.Visible = true;
                    DataSet emptds1 = new DataSet();
                    DataTable dtempty1 = new DataTable();
                    //dtempty = emptds.Tables[0];
                    Griddisp.AutoGenerateColumns = false;

                    Griddisp.DataSource = dtnewgrid;


                    Griddisp.DataBind();
                    //Griddisp.EnableViewState = true;





                    lbldisp.Visible = false;
                    Session["Sessionno"] = dsnew.Tables[0];
                    dtnw = dsnew.Tables[0];
                }
                else
                {
                    lbldisp.Visible = true;
                    Griddisp.Visible = false;
                }


            }
            else
            {
                lblall.Visible = true;

                lbldisp.Visible = false;
                Griddisp.Visible = false;
            }
        }

       //}

        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }


    protected string genwherecategorycondition()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }


        }
        //if (ddZone.SelectedItem.Text != "[Select Category]")
        //{

        //    iCondtions += " and Ej.ExpCatID=" + Expcategorystr;

        //}

        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);

        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }

        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {

            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";

            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }


        }



        return iCondtions + "group by ej.ExpcatId,ch.[state],ej.Tochapterid,ch.ChapterCode,ch.ClusterCode,ch.ZoneCode,year(Ej.DatePaid) order by ch.[state],ej.Tochapterid) select catid,chapterid,Chapter,Cluster,Zone " + am + " from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";


    }

  
    protected string gengrantWhereConditons()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }


        }
        if (DDcategory.Visible != true)
        {
            if (ddZone.SelectedItem.Text != "[Select Zone]")
            {
                if (ddZone.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {

                        iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                    }
                }


            }
            if (ddCluster.SelectedItem.Text != "[Select Cluster]")
            {
                if (ddCluster.SelectedItem.Text != "All")
                {
                    if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                    {
                        iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                    }
                }


            }
            if (ddchapter.SelectedItem.Text != "[Select Chapter]")
            {
                if (ddchapter.SelectedItem.Text != "All")
                {
                    iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
                }
            }
        }
        else
        {
            if (ddZone.SelectedItem.Text != "[Select Category]")
            {

                iCondtions += " and Ej.ExpCatID=" + DDcategory.SelectedValue;



            }
        }


        return iCondtions + " and ej.paid='y'";
    }

    protected string genWhereConditons()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }



        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }


        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }


        }
        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }

        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);

        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }

        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {

            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";

            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }


        }
        if (DDyear.SelectedItem.Text != "Fiscal Year")
        {

            return iCondtions + "group by EC.ExpCatID,EC.ExpCatDesc,EC.ExpCatDesc,EC.AccountName,year(Ej.DatePaid)) select ExpCatID,ExpCatDesc,AccountName " + am + " from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
        }
        else
        {
            return iCondtions + "group by EC.ExpCatID,EC.ExpCatDesc,EC.AccountName) select ExpCatID,ExpCatDesc,AccountName " + am + " from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";
        }
    }
    protected string genWhereConditonsnew()
    {
        string iCondtions = string.Empty;
        if (ddevent.SelectedItem.Text != "[Select Event]")
        {
            if (ddevent.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.EventID=" + ddevent.SelectedValue;
            }



        }
        if (ddZone.SelectedItem.Text != "[Select Zone]")
        {
            if (ddZone.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ZoneId=" + ddZone.SelectedValue;
                }
            }


        }
        if (ddCluster.SelectedItem.Text != "[Select Cluster]")
        {
            if (ddCluster.SelectedItem.Text != "All")
            {
                if ((ddevent.SelectedItem.Text != "Finals") && (ddevent.SelectedItem.Text != "Online Coaching"))
                {
                    iCondtions += " and ch.ClusterId=" + ddCluster.SelectedValue;
                }
            }


        }

        if (ddchapter.SelectedItem.Text != "[Select Chapter]")
        {
            if (ddchapter.SelectedItem.Text != "All")
            {
                iCondtions += " and ej.ToChapterID=" + ddchapter.SelectedItem.Value.ToString();
            }
        }

        if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
        {
            calc = ddNoyear.SelectedItem.Text;
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);

        }
        else
        {
            Start = Convert.ToInt32(StaYear);
            endall = Convert.ToInt32(StaYear) - 5;
        }

        int sta = Convert.ToInt32(Start);
        string WCNT = string.Empty;
        string am = string.Empty;
        for (int i = endall; i <= sta; i++)
        {

            if (WCNT != string.Empty)
            {
                WCNT = WCNT + ",[" + i.ToString() + "]";
                am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";

            }
            else
            {
                WCNT = "[" + i.ToString() + "]";
                am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
            }


        }


        return iCondtions + "group by EC.ExpcatId,EC.ExpCatDesc,EC.AccountName,year(Ej.DatePaid)) select ExpCatID,ExpCatDesc,AccountName " + am + " from tbl pivot(sum(ExpenseAmount) for [Year] in (" + WCNT + "))As PVTTBL";


    }

    public override void VerifyRenderingInServerForm(Control control)
    {
    }

    protected void UploadDataTableToExcel(DataTable dtRecords)
    {

        Response.Clear();
        String strZoneName = ddZone.SelectedItem.Text;
        strZoneName = strZoneName.Replace(" ", "");
        String strclusterName = ddCluster.SelectedItem.Text;
        strclusterName = strclusterName.Replace(" ", "");
        String strchapterName = ddchapter.SelectedItem.Text;
        strchapterName = strchapterName.Replace(" ", "");
        String strcategoryName = DDcategory.SelectedItem.Text;
        strcategoryName = strcategoryName.Replace(" ", "");
        String strEventName = ddevent.SelectedItem.Text;
        strEventName = strEventName.Replace(" ", "");

        String strYearName = DDyear.SelectedItem.Text;
        strYearName = strYearName.Replace(" ", "");

        DateTime now = DateTime.Now;
        string month = now.ToString("MMM");
        string day = now.ToString("dd");
        string all = string.Concat(month, day);
        // string s=strZoneName.Substring(0,3);
        string attachment;
        if (DDcategory.Visible != true)
        {
            attachment = "attachment; filename=ExpSum_" + strEventName + "_" + strZoneName + "_" + strclusterName + "_" + strchapterName + "_" + ddNoyear.SelectedItem.Text + "_" + strYearName + "_" + all + "_" + StaYear + ".xls";

        }
        else
        {
            attachment = "attachment; filename=ExpSum_" + strEventName + "_" + strcategoryName + "_" + ddNoyear.SelectedItem.Text + "_" + strYearName + "_" + all + "_" + StaYear + ".xls";


        }
        Response.AppendHeader("content-disposition", attachment);
        Response.Charset = "";
        //Response.Cache.SetCacheability(HttpCacheability.NoCache);
        Response.ContentType = "application/vnd.xls";
        System.IO.StringWriter sw = new System.IO.StringWriter();
        System.Web.UI.HtmlTextWriter hw = new HtmlTextWriter(sw);
        GridView1.Visible = true;

        //hw.Write("Expense Summary");
        if (DDcategory.Visible == true)
        {
            Pnlreport.Visible = true;
            Pnlreport.RenderControl(hw);

        }
        else
        {
            Panel1.Visible = true;
            Panel1.RenderControl(hw);
        }
        Panel2.Visible = true;
        Panel2.RenderControl(hw);
        GridView1.RenderControl(hw);
        Griddisp.RenderControl(hw);
        Response.Write(sw.ToString());
        Response.End();


    }

    protected void Button2_Click(object sender, EventArgs e)
    {
        UploadDataTableToExcel(dtnewgrid);

    }

    protected void Griddisp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowIndex == -1) return;
        if (DDcategory.Visible == true)
        {
            newvar = 3;
        }
        else
        {
            newvar = 2;
        }
        for (int i = newvar; i < e.Row.Cells.Count; i++)
        {

            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].Text = String.Format("{0:0.00}", Convert.ToDouble(e.Row.Cells[i].Text)).ToString();
            }

        }
    }
    protected void ddevent_SelectedIndexChanged(object sender, EventArgs e)
    {
        Zone();
        Cluster();
        Chapter();
    }
    protected void ddZone_SelectedIndexChanged1(object sender, EventArgs e)
    {

    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void BtnExpcat_Click(object sender, EventArgs e)
    {

        Pnldisp.Visible = true;
        lblCluster.Visible = false;
        ddCluster.Visible = false;
        ddZone.Visible = false;
        lblZone.Visible = false;
        ddchapter.Visible = false;
        lblchapter.Visible = false;
        DDcategory.Visible = true;
        Lblcat.Visible = true;
        PnlCat.Visible = true;
        Pnlsummary.Visible = false;
        Btnsumm.Visible = true;
        Griddisp.Visible = false;
        ddevent.SelectedIndex = 0;
        ddNoyear.SelectedIndex = 0;
        DDyear.SelectedIndex = 0;
        DDcategory.SelectedIndex = 0;


    }
    protected void Btnsumm_Click(object sender, EventArgs e)
    {
        Pnlsummary.Visible = true;
        lblCluster.Visible = true;
        ddCluster.Visible = true;
        ddZone.Visible = true;
        lblZone.Visible = true;
        ddchapter.Visible = true;
        lblchapter.Visible = true;
        DDcategory.Visible = false;
        Lblcat.Visible = false;
        PnlCat.Visible = false;
        Griddisp.Visible = false;
        Btnsumm.Visible = false;
        ddevent.SelectedIndex = 0;
        ddZone.SelectedIndex = 0;
        ddCluster.SelectedIndex = 0;
        ddchapter.SelectedIndex = 0;
        ddNoyear.SelectedIndex = 0;
        DDyear.SelectedIndex = 0;

    }
   
    protected void Griddisp_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void lnkView_Click(object sender, EventArgs e)
    {
        try
        {
            lbnext.Visible = true;
                lbprevious.Visible=true;
            if (ddNoyear.SelectedItem.Text != "[Select No of Years]")
            {
                calc = ddNoyear.SelectedItem.Text;
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - Convert.ToInt16(calc);

            }
            else
            {
                Start = Convert.ToInt32(StaYear);
                endall = Convert.ToInt32(StaYear) - 5;

            }
            pnlview.Visible = false;
            GRcategory.Visible = true;
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField k = (HiddenField)Griddisp.Rows[index].Cells[0].FindControl("hdnid");


            Expcategorystr = Convert.ToInt32(k.Value);

            bool Isfalse = false;
            for (i = endall; i <= Start; i++)
            {
                // drng[i] = "567";

                if (DDyear.SelectedItem.Text != "Fiscal Year")
                {


                    Qrycondition = "with tbl as (select  TOP 100 PERCENT ej.ExpcatId as catid,ej.Tochapterid as chapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and Ej.DatePaid between '01/01/" + i + "' and '12/31/" + i + "' and  ej.CheckNumber>0";



                }
                else
                {
                    fiscyear = i + 1;

                    Qrycondition = "with tbl as (select TOP 100 PERCENT  ej.ExpcatId as catid,ej.Tochapterid as chapterid,ch.ChapterCode as Chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,sum(Ej.ExpenseAmount) as ExpenseAmount ,CASE when (min(year(Ej.DatePaid))=" + fiscyear + ") then (max(year(Ej.DatePaid)))-1  else min (CONVERT(varchar(50),year(Ej.DatePaid))) end as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and Ej.DatePaid between '05/01/" + i + "' and '04/30/" + fiscyear + "' and  ej.CheckNumber>0";


                }


                Qry = Qrycondition + genwherecategorycondition();

                dsnew = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
                DataTable dtmerge = new DataTable();
                dtmerge = dsnew.Tables[0];



                if (Isfalse == false)
                {
                    firstquert = "with tbl as (select TOP 100 PERCENT ej.ExpcatId as catid,ej.Tochapterid as Chapterid,ch.ChapterCode as chapter,ch.ClusterCode as Cluster,ch.ZoneCode as Zone,0 as ExpenseAmount ,CONVERT(varchar(50),year(Ej.DatePaid))as [Year] from ExpenseCategory EC inner join Expjournal Ej on EC.ExpCatID=Ej.ExpCatID inner join chapter ch on ch.ChapterID=ej.ToChapterID where ej.paid='y' and Ej.ExpCatID=" + Expcategorystr + " and year(Ej.DatePaid) between '" + endall + "' and '" + Start + "' and  ej.CheckNumber>0";
                    firstquert = firstquert + genwherecategorycondition();

                    DataSet dsnew1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, firstquert);

                    DataTable dtm = new DataTable();
                    dtm = dsnew1.Tables[0];
                    Mergecopy = dtm.Copy();
                    if (dtm.Rows.Count > 0)
                    {
                        Isfalse = true;
                        Mergecopy = dtm.Copy();

                    }
                }

                if (Isfalse == true)
                {
                    if (dtmerge.Rows.Count > 0)
                    {
                        for (int j = 0; j <= dtmerge.Rows.Count - 1; j++)
                        {
                            Label lbn = new Label();
                            lbn.Text = dtmerge.Rows[j][0].ToString();
                            for (int h = 0; h <= Mergecopy.Rows.Count - 1; h++)
                            {
                                Label lb = new Label();
                                lb.Text = Mergecopy.Rows[h][0].ToString();
                                if ((dtmerge.Rows[j][0].ToString() == Mergecopy.Rows[h][0].ToString()))
                                {
                                    string s = dtmerge.Rows[j]["" + i + ""].ToString();
                                    Mergecopy.Rows[h]["" + i + ""] = s;

                                    break;
                                }



                            }


                        }
                    }


                }


            }
            GRcategory.DataSource = Mergecopy;
            GRcategory.DataBind();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }

     
    }
    protected void GridEdit_RowEditing(object sender, GridViewEditEventArgs e)
    {
        
       GridEdit.EditIndex = e.NewEditIndex;
        bindgrid();
    }
    protected void GridEdit_RowUpdating(object sender, GridViewUpdateEventArgs e)
  {
      try
      {
          string Qrychapter;

          string s = GridEdit.DataKeys[e.RowIndex].Value.ToString();
          TextBox Eventyear = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("Txtyear");
          TextBox Eventid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxteventId");
          TextBox Chaptercode = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtChaptercode");
          if (Chaptercode.Text == "")
          {

          }
          else
          {
              Qrychapter = "Select ChapterID from chapter where chaptercode='"+ Chaptercode.Text + "'";

             // SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Qrychapter);

              DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrychapter);
              //DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select distinct clusterid,clustercode from volunteer where MemberID='" + Session["LoginID"] + "'");
              DataTable dt = ds.Tables[0];
              Txthidden.Text = dt.Rows[0]["ChapterID"].ToString();
          }
          TextBox Expcatcode = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtExpcatcode");
          TextBox Expenseamount = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtExpamount");
          TextBox Bankid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtBanKID");
          TextBox Tobankid = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtToBankID");
          TextBox Transtype = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtTrans");
          TextBox Account = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtAccount");

          TextBox reportdate = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtReportDate");
          TextBox Resttypefrom = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("TxtRestTypeFrom");
          TextBox RestypeTo = (TextBox)GridEdit.Rows[e.RowIndex].FindControl("Txtresttype");
          if (Eventid.Text == "")
          {
              Eventid.Text = "0";
          }
          if (Bankid.Text == "")
          {
              Bankid.Text = "0";
          }
          if (Tobankid.Text == "")
          {
              Tobankid.Text = "0";
          }
          if (Transtype.Text == "System.Web.UI.WebControls.TextBox")
          {
              Transtype.Text = "NULL";
          }
          if (Account.Text == "")
          {
              Account.Text = "null";
          }
          if ((Resttypefrom.Text == "System.Web.UI.WebControls.TextBox") || (Resttypefrom.Text == null))
          {
              Resttypefrom.Text = "NULL";
          }
          if (RestypeTo.Text == "System.Web.UI.WebControls.TextBox")
          {
              RestypeTo.Text = "NULL";
          }
           string qry = "update Expjournal set EventYear=" + Eventyear.Text + "  , EventId=" + Eventid.Text + " , TochapterId=" + Txthidden.Text + " ,  Expcatcode='" + Expcatcode.Text + "' , ExpenseAmount=" + Expenseamount.Text + " , BankId=" + Bankid.Text + " , ToBankId=" + Tobankid.Text + " , TransType='" + Transtype.Text + "' , Account=" + Account.Text + " ,ReportDate='" + reportdate.Text + "' ,RestTypeFrom='" + Resttypefrom.Text + "' ,RestTypeTo='" + RestypeTo.Text + "' where Transactionid=" + s + "";
          SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qry);

          GridEdit.EditIndex = -1;
          bindgrid();
          GridEdit.Visible = true;
      }
      catch (Exception err)
      {
          lblMessage.Text = err.Message;
      }

   }
    protected void gvDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GridEdit.EditIndex = -1;
        bindgrid();
    }
   
    public void bindgrid()
    {
        string Expjournal = "select Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID,(I.FirstName + ' ' + I.LastName) as MemberName,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,ej.ExpenseAmount,Ej.TreatID,ej.TreatCode,ej.NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,ej.DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,ej.ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=ch.ChapterID  where Ej.Paid='y' and Ej.CheckNumber>0 and Ej.TochapterID=" + Session["chapter"] + " and Ej.expcatID=" + Session["Category"] + " order by EventYear, DatePaid ";
        //string Expjournal = "select TransactionID,ChapterID,EventYear,EventID from ExpJournal where TochapterID=" + Session["chapter"] + " and expcatID=" + Session["Category"] + " order by EventYear, DatePaid ";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
        ddCluster.DataSource = ds;
        DataTable dt = ds.Tables[0];
        GridEdit.DataSource = dt;
        GridEdit.DataBind();
    }
    public void btDetails_Click(object sender, EventArgs e)
    {
        try
        {
            //  Response.Write("grhyh");
            pnlview.Visible = false;
            pnlcategory.Visible = false;
            // GRcategory.Visible = true;
            //pnlEdit.Visible = true;

            GridEdit.Visible = true;


            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            HiddenField hdcatid = (HiddenField)GRcategory.Rows[index].Cells[0].FindControl("hdcatid");
            HiddenField hdchapter = (HiddenField)GRcategory.Rows[index].Cells[1].FindControl("Hdchapter");
            Expcategorystr = Convert.ToInt32(hdcatid.Value);
            Expchapterystr = Convert.ToInt32(hdchapter.Value);
            Session["Category"] = Expcategorystr;

            Session["chapter"] = Expchapterystr;
            string Expjournal = "select Ej.TransactionID,Ej.ChapterID,Ej.EventYear,Ej.EventID,Ej.ReimbMemberID,(I.FirstName + ' ' + I.LastName) as MemberName,Ej.DonorType,ej.ToChapterID,ch.ChapterCode,Ej.ExpCatCode,ej.ExpenseAmount,Ej.TreatID,ej.TreatCode,ej.NationalApprovalDate,ej.Comments,ej.BanKID,ej.ToBankID,ej.CheckNumber,ej.Paid,ej.DatePaid,ej.TransType,ej.Account,ej.PaymentMethod,ej.ReportDate,ej.RestTypeFrom,ej.RestTypeTo from ExpJournal Ej inner join IndSpouse I  on I.AutoMemberID=ej.ReimbMemberID inner join chapter ch on ch.ChapterID=ch.ChapterID  where Ej.Paid='y' and Ej.CheckNumber>0 and Ej.TochapterID=" + Expchapterystr + " and Ej.expcatID=" + Expcategorystr + " order by EventYear, DatePaid ";
            // string Expjournal = "select * from ExpJournal where TochapterID=" + Expchapterystr + " and expcatID=" + Expcategorystr + " order by EventYear, DatePaid ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
            ddCluster.DataSource = ds;
            DataTable dt = ds.Tables[0];
            GridEdit.DataSource = dt;
            GridEdit.DataBind();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }


    }
    protected void btedit_Click(object sender, EventArgs e)
    {
        try
        {
            string Expjournal = "select TransactionID,ChapterID,EventYear from ExpJournal where TochapterID=108 and expcatID=1 order by EventYear, DatePaid ";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Expjournal);
            ddCluster.DataSource = ds;
            DataTable dt = ds.Tables[0];
            GridEdit.DataSource = dt;
            GridEdit.DataBind();
        }
        catch (Exception err)
        {
            lblMessage.Text = err.Message;
        }
    }
    


    protected void gvUserEdit_RowDataBound(object sender, GridViewRowEventArgs e)
    {

       
    }


    
    protected void GridEdit_DataBound(object sender, EventArgs e)
    {
        //try
        //{
        //    for (int i = 0; i < e.Row.Cells.Count; i++)
        //    {
                
        //        e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
        //        if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
        //    }
        //}
        //catch (Exception ex) { }

    }
    protected void Griddisp_RowCommand(object sender, GridViewCommandEventArgs e)
    {

    }
    protected void lbprevious_Click(object sender, EventArgs e)
    {
        pnlview.Visible = true;
        //Griddisp.Visible = true;
        button();
        GRcategory.Visible = false;
    }
}



 
