﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Reflection
Imports NativeExcel
Imports System.IO

Partial Class Admin_SpellingBeeCriteria
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("LoginID") = 4240
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("../maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            loadyear()
            Dim dt As Date = Now.Date
            If dt.Month >= 10 And dt.Month <= 12 Then
                ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(dt.Year)) + 1
                'Else
                '    ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(dt.Year)) + 1
            End If

            LoadCount()
            ddlGame_Flag.SelectedIndex = ddlGame_Flag.Items.IndexOf(ddlGame_Flag.Items.FindByValue(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT ISNULL(Game_Flag,'0') FROM Event WHERE EventID=4")))
        End If
        lblcalc()
    End Sub
    Private Sub loadyear()
        Dim i, j As Integer
        j = 0
        For i = Now.Year To Now.Year + 2
            ddlYear.Items.Insert(j, i.ToString())
            j = j + 1
        Next
        ddlYear.Items(0).Selected = True
    End Sub

    Private Sub lblcalc()
        Try
            lbltotalSum.Text = Val(lbl11.Text) + Val(lbl12.Text) + Val(lbl21.Text) + Val(lbl22.Text) + Val(lbl31.Text) + Val(lbl32.Text)
            lblrsum.Text = Val(txt11r.Text) + Val(txt12r.Text) + Val(txt21r.Text) + Val(txt22r.Text) + Val(txt31r.Text) + Val(txt32r.Text)
            lblnsum.Text = Val(txt11n.Text) + Val(txt12n.Text) + Val(txt21n.Text) + Val(txt22n.Text) + Val(txt31n.Text) + Val(txt32n.Text)

        Catch ex As Exception

        End Try
    End Sub
    Private Sub LoadCount()
        ' display the number of words
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from SBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") > 0 Then
            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 1 and SubLevel=1")
            'lbl11.Text = ds.Tables(0).Rows(0)(0)
            txt11r.Text = ds.Tables(0).Rows(0)(1)
            txt11n.Text = ds.Tables(0).Rows(0)(2)
            ddl11r.SelectedIndex = ddl11r.Items.IndexOf(ddl11r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl11n.SelectedIndex = ddl11n.Items.IndexOf(ddl11n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 1 and SubLevel=2")
            'lbl12.Text = ds.Tables(0).Rows(0)(0)
            txt12r.Text = ds.Tables(0).Rows(0)(1)
            txt12n.Text = ds.Tables(0).Rows(0)(2)
            ddl12r.SelectedIndex = ddl12r.Items.IndexOf(ddl12r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl12n.SelectedIndex = ddl12n.Items.IndexOf(ddl12n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 2 and SubLevel=1")
            ' lbl21.Text = ds.Tables(0).Rows(0)(0)
            txt21r.Text = ds.Tables(0).Rows(0)(1)
            txt21n.Text = ds.Tables(0).Rows(0)(2)
            ddl21r.SelectedIndex = ddl21r.Items.IndexOf(ddl21r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl21n.SelectedIndex = ddl21n.Items.IndexOf(ddl21n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 2 and SubLevel=2")
            ' lbl22.Text = ds.Tables(0).Rows(0)(0)
            txt22r.Text = ds.Tables(0).Rows(0)(1)
            txt22n.Text = ds.Tables(0).Rows(0)(2)
            ddl22r.SelectedIndex = ddl22r.Items.IndexOf(ddl22r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl22n.SelectedIndex = ddl22n.Items.IndexOf(ddl22n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 3 and SubLevel=1")
            'lbl31.Text = ds.Tables(0).Rows(0)(0)
            txt31r.Text = ds.Tables(0).Rows(0)(1)
            txt31n.Text = ds.Tables(0).Rows(0)(2)
            ddl31r.SelectedIndex = ddl31r.Items.IndexOf(ddl31r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl31n.SelectedIndex = ddl31n.Items.IndexOf(ddl31n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select total,Regional,[National],RegContest, NatContest from SBPubCriteria where [level] = 3 and SubLevel=2")
            ' lbl32.Text = ds.Tables(0).Rows(0)(0)
            txt32r.Text = ds.Tables(0).Rows(0)(1)
            txt32n.Text = ds.Tables(0).Rows(0)(2)
            ddl32r.SelectedIndex = ddl32r.Items.IndexOf(ddl32r.Items.FindByText(ds.Tables(0).Rows(0)(3).ToString().Trim()))
            ddl32n.SelectedIndex = ddl32n.Items.IndexOf(ddl32n.Items.FindByText(ds.Tables(0).Rows(0)(4).ToString().Trim()))
            btnExport.Visible = True
            btnPDF.Visible = True
        Else
            btnGenerate.Enabled = False
        End If
        lbl11.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=1 and [sub-Level_NSF] =1")
        lbl12.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=1 and [sub-Level_NSF] =2")

        lbl21.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=2 and [sub-Level_NSF] =1")
        lbl22.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=2 and [sub-Level_NSF] =2")

        lbl31.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=3 and [sub-Level_NSF] =1")
        lbl32.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select COUNT (*) from word_master_new where Level_NSF=3 and [sub-Level_NSF] =2")


    End Sub
    Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Sum of JSB and SSB in reg and Nat must be 1000
        'Sum of Reg & Nat must be less than total
        Dim JSBsumR, SSBSumR, JSBSumN, SSBSumN As Integer
        JSBsumR = 0
        SSBSumR = 0
        JSBSumN = 0
        SSBSumN = 0
        Try

            If ddl11r.SelectedValue = "JSB" Then
                JSBsumR = Val(txt11r.Text)
            Else
                SSBSumR = Val(txt11r.Text)
            End If
            If ddl12r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt12r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt12r.Text)
            End If
            If ddl21r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt21r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt21r.Text)
            End If
            If ddl22r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt22r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt22r.Text)
            End If
            If ddl31r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt31r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt31r.Text)
            End If
            If ddl32r.SelectedValue = "JSB" Then
                JSBsumR = JSBsumR + Val(txt32r.Text)
            Else
                SSBSumR = SSBSumR + Val(txt32r.Text)
            End If


            'Nat
            If ddl11n.SelectedValue = "JSB" Then
                JSBSumN = Val(txt11n.Text)
            Else
                SSBSumN = Val(txt11n.Text)
            End If
            If ddl12n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt12n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt12n.Text)
            End If
            If ddl21n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt21n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt21n.Text)
            End If
            If ddl22n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt22n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt22n.Text)
            End If
            If ddl31n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt31n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt31n.Text)
            End If
            If ddl32n.SelectedValue = "JSB" Then
                JSBSumN = JSBSumN + Val(txt32n.Text)
            Else
                SSBSumN = SSBSumN + Val(txt32n.Text)
            End If
            If JSBSumN < 1000 Then
                lblerr.Text = " JSB National is less than 1000"
                Exit Sub
            End If
            If JSBsumR < 1000 Then
                lblerr.Text = " JSB Regional is less than 1000"
                Exit Sub
            End If
            If SSBSumN < 1000 Then
                lblerr.Text = " SSB National is less than 1000"
                Exit Sub
            End If
            If SSBSumR < 1000 Then
                lblerr.Text = " SSB Regional is less than 1000"
                Exit Sub
            End If

            If Convert.ToInt32(lbl11.Text) < (Val(txt11r.Text) + Val(txt11n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl12.Text) < (Val(txt12r.Text) + Val(txt12n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 1 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl21.Text) < (Val(txt21r.Text) + Val(txt21n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl22.Text) < (Val(txt22r.Text) + Val(txt22n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 2 Sub Level 2  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl31.Text) < (Val(txt31r.Text) + Val(txt31n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 1  "
                Exit Sub
            End If

            If Convert.ToInt32(lbl32.Text) < (Val(txt32r.Text) + Val(txt32n.Text)) Then
                lblerr.Text = "Total Count exceeds the existing count for Level 3 Sub Level 2  "
                Exit Sub
            End If

        Catch ex As Exception
            lblerr.Text = "Please enter all Values & Enter Numeric values only"
            Exit Sub
        End Try
        lblerr.Text = ""
        Dim strSQl As String
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from SBPubCriteria where ContestYear=" & ddlYear.SelectedValue & "") < 1 Then
            strSQl = "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (1,1," & lbl11.Text & ",'" & ddl11r.SelectedValue & "'," & Val(txt11r.Text) & ",'" & ddl11n.SelectedValue & "'," & Val(txt11n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (1,2," & lbl12.Text & ",'" & ddl12r.SelectedValue & "'," & Val(txt12r.Text) & ",'" & ddl12n.SelectedValue & "'," & Val(txt12n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (2,1," & lbl21.Text & ",'" & ddl21r.SelectedValue & "'," & Val(txt21r.Text) & ",'" & ddl21n.SelectedValue & "'," & Val(txt21n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (2,2," & lbl22.Text & ",'" & ddl22r.SelectedValue & "'," & Val(txt22r.Text) & ",'" & ddl22n.SelectedValue & "'," & Val(txt22n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (3,1," & lbl31.Text & ",'" & ddl31r.SelectedValue & "'," & Val(txt31r.Text) & ",'" & ddl31n.SelectedValue & "'," & Val(txt31n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            strSQl = strSQl & "INSERT INTO SBPubCriteria([Level], SubLevel, Total, RegContest, Regional, NatContest, [National], ContestYear, CreateDate, CreatedBy) VALUES (3,2," & lbl32.Text & ",'" & ddl32r.SelectedValue & "'," & Val(txt32r.Text) & ",'" & ddl32n.SelectedValue & "'," & Val(txt32n.Text) & "," & ddlYear.SelectedValue & ",Getdate()," & Session("LoginID") & ");"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values inserted successfully"
        Else
            strSQl = "UPDATE SBPubCriteria SET Total=" & Val(lbl11.Text) & ", Regional=" & Val(txt11r.Text) & ", RegContest='" & ddl11r.SelectedValue & "', [National]=" & Val(txt11n.Text) & ", NatContest='" & ddl11n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl12.Text) & ", Regional=" & Val(txt12r.Text) & ", RegContest='" & ddl12r.SelectedValue & "', [National]=" & Val(txt12n.Text) & ", NatContest='" & ddl12n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 1 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl21.Text) & ", Regional=" & Val(txt21r.Text) & ", RegContest='" & ddl21r.SelectedValue & "', [National]=" & Val(txt21n.Text) & ", NatContest='" & ddl21n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl22.Text) & ", Regional=" & Val(txt22r.Text) & ", RegContest='" & ddl22r.SelectedValue & "', [National]=" & Val(txt22n.Text) & ", NatContest='" & ddl22n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 2 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl31.Text) & ", Regional=" & Val(txt31r.Text) & ", RegContest='" & ddl31r.SelectedValue & "', [National]=" & Val(txt31n.Text) & ", NatContest='" & ddl31n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 1 AND ContestYear=" & ddlYear.SelectedValue & ";"
            strSQl = strSQl & "UPDATE SBPubCriteria SET Total=" & Val(lbl32.Text) & ", Regional=" & Val(txt32r.Text) & ", RegContest='" & ddl32r.SelectedValue & "', [National]=" & Val(txt32n.Text) & ", NatContest='" & ddl32n.SelectedValue & "',ModifyDate=Getdate(),Modifyby=" & Session("LoginID") & " WHERE [Level] = 3 AND  SubLevel = 2 AND ContestYear=" & ddlYear.SelectedValue & ";"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Values updated successfully"
        End If
        btnGenerate.Enabled = True
    End Sub
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, "usp_createTempTable")
        Dim dstemp, dsword As DataSet
        dstemp = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select SBPubTempID from  sbpubtemp  order by Randvalue")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select word from Word_Master_New")
        Dim i As Integer
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE Word_Master_New set [Reg_Rand#] = " & dstemp.Tables(0).Rows(i)(0) & ",Regional_Flag=Null , nat_flag=Null where word='" & dsword.Tables(0).Rows(i)(0) & "'")
        Next
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "drop table SBPubTemp")
        dsword = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select [Regional],[National], [level],sublevel from SBPubCriteria where ContestYeaR=" & ddlYear.SelectedValue & "")
        Dim sqlstr As String
        For i = 0 To dsword.Tables(0).Rows.Count - 1
            sqlstr = "Update Word_Master_New set  Regional_Flag='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("Regional") & " word from Word_Master_New where Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional_Flag  is null and nat_flag is Null order by Reg_Rand#) AND  Regional_Flag  is null and nat_flag is Null and Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & "; "
            sqlstr = sqlstr & "Update Word_Master_New set  nat_flag='Y' WHERE word in (select top " & dsword.Tables(0).Rows(i)("National") & " word from Word_Master_New where Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " and Regional_Flag  is null and nat_flag is Null order by Reg_Rand#) AND  Regional_Flag  is null and nat_flag is Null and Level_NSF= " & dsword.Tables(0).Rows(i)("Level") & " AND [Sub-Level_NSF] =" & dsword.Tables(0).Rows(i)("sublevel") & " "
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlstr)
            ' Response.Write(sqlstr & "<br><br>")
        Next
        lblerr.Text = "Generated Successfully"
        btnExport.Visible = True
        btnPDF.Visible = True
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Create workbook
        Dim book As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim i As Integer
        Dim j As Integer = 0
        Dim ds As DataSet
        Dim sheet As IWorksheet = book.Worksheets.Add()
        sheet.Cells("A2").Value = "JSB_REG_WORDS"
        sheet.Cells("B2").Value = "Level"
        sheet.Cells("C2").Value = "SubLevel"
        sheet.Name = "JSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='JSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")

        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet1 As IWorksheet = book.Worksheets.Add()
        sheet1.Cells("A2").Value = "JSB_NAT_WORDS"
        sheet1.Cells("B2").Value = "Level"
        sheet1.Cells("C2").Value = "SubLevel"
        sheet1.Name = "JSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.NatContest='JSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet1.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet1.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet1.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet2 As IWorksheet = book.Worksheets.Add()
        sheet2.Cells("A2").Value = "SSB_REG_WORDS"
        sheet2.Cells("B2").Value = "Level"
        sheet2.Cells("C2").Value = "SubLevel"
        sheet2.Name = "SSB_Reg"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Regional_Flag='Y' and S.RegContest='SSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet2.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet2.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet2.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        Dim sheet3 As IWorksheet = book.Worksheets.Add()
        sheet3.Cells("A2").Value = "SSB_NAT_WORDS"
        sheet3.Cells("B2").Value = "Level"
        sheet3.Cells("C2").Value = "SubLevel"
        sheet3.Name = "SSB_Nat"
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select w.word,S.Level,S.SubLevel from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where   w.Nat_Flag='Y' and S.NatContest='SSB' and S.ContestYeaR=" & ddlYear.SelectedValue & " order by w.[Reg_Rand#]")
        j = 0
        For i = 3 To ds.Tables(0).Rows.Count + 2
            sheet3.Cells(i, 1).Value = ds.Tables(0).Rows(j)(0)
            sheet3.Cells(i, 2).Value = ds.Tables(0).Rows(j)(1)
            sheet3.Cells(i, 3).Value = ds.Tables(0).Rows(j)(2)
            j = j + 1
        Next
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where w.Regional_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where w.Nat_Flag='Y' and S.RegContest='JSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where w.Regional_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]
        'select w.word from word_master_new w Inner Join SBPubcriteria S ON w.Level_NSF= S.Level AND w.[Sub-Level_NSF] = S.SubLevel where w.Nat_Flag='Y' and S.RegContest='SSB' order by w.[Reg_Rand#]
        'Stream workbook  
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=SBPubWords_" & ddlYear.SelectedValue & ".xls")
        book.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadCount()
        lblcalc()
    End Sub

    Protected Sub btnUpdategameflag_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        If Not ddlGame_Flag.SelectedItem.Value = "0" Then
            Dim strSQl As String = "Update Event Set Game_Flag='" & ddlGame_Flag.SelectedValue & "' WHERE EventID=4;"
            strSQl = strSQl & "update word_master_new SET Game_Flag= CASE WHEN " & ddlGame_Flag.SelectedValue & "='Y' THEN 'Y' ELSE NULL END,ModifiedDate=GETDATE();"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQl)
            lblerr.Text = "Event Flag & Game Flag updated successfully"
        Else
            lblerr.Text = "Please select valid option"
        End If
    End Sub
    Protected Sub btnPDF_Click(sender As Object, e As EventArgs) Handles btnPDF.Click
        GeneratePdf(True, 1, "JSB", "Junior Spelling Bee")
        GeneratePdf(True, 2, "SSB", "Senior Spelling Bee")

        GeneratePdf(False, 1, "JSB", "Junior Spelling Bee")
        GeneratePdf(False, 2, "SSB", "Senior Spelling Bee")
        CompressFiles(True)

    End Sub
    Sub CompressFiles(bIsRegional As Boolean)
        Try
            'Dim ContestFirstChar As Char = "R"

            Dim strFileName As String
            strFileName = "SpellingBeeContest_" & ddlYear.SelectedValue & ".zip"
            'If bIsRegional = True Then
            '    strFileName = "RegionalContest_" & ddlYear.SelectedValue & ".zip"
            'Else
            '    strFileName = "NationalFinalContest_" & ddlYear.SelectedValue & ".zip"
            '    ContestFirstChar = "F"
            'End If

            Dim zFile As Ionic.Zip.ZipFile = New Ionic.Zip.ZipFile
            Dim path As String = Server.MapPath("Reports/SBVB")

            Dim files As String() = Directory.GetFiles(path)
            Dim fname As String

            For Each fname In files
                ' If (fname.Contains(ddlYear.SelectedValue & "R") Or fname.Contains(ddlYear.SelectedValue & "F")) Then
                If (fname.Contains("JSB") Or fname.Contains("SSB")) And fname.Contains(ddlYear.SelectedValue) Then
                    Dim iZEntry As Ionic.Zip.ZipEntry = zFile.AddFile(fname, "")
                End If
            Next
            Dim pt As String = Server.MapPath("Reports/SBVB/" & strFileName)
            zFile.Save(Server.MapPath("Reports/SBVB/" & strFileName))

            Dim FList As System.IO.DirectoryInfo = New DirectoryInfo(path)
            Dim fInfo As FileInfo
            For Each fInfo In FList.GetFiles()
                ' If fInfo.Name.Contains(ddlYear.SelectedValue & "R") Or fInfo.Name.Contains(ddlYear.SelectedValue & "F") Then
                If (fInfo.Name.Contains("JSB") Or fInfo.Name.Contains("SSB")) And fInfo.Name.Contains(ddlYear.SelectedValue) Then
                    fInfo.Delete()
                End If
            Next
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(Server.MapPath("Reports/SBVB/" & strFileName))
            If file.Exists Then
                Response.Clear()
                'Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                Response.AddHeader("Content-Length", file.Length.ToString())
                Response.ContentType = "application/octet-stream"
                Response.WriteFile(file.FullName)
            End If
        Catch ex As Exception
        End Try
    End Sub
    Public Function GetFont(name As String, size As Integer) As iTextSharp.text.Font
        Dim baseFont__1 As iTextSharp.text.pdf.BaseFont = iTextSharp.text.pdf.BaseFont.CreateFont(name, iTextSharp.text.pdf.BaseFont.CP1257, iTextSharp.text.pdf.BaseFont.EMBEDDED)
        Dim f As New iTextSharp.text.Font(baseFont__1, size)
        Return f
    End Function

    Sub GeneratePdf(bIsRegional As Boolean, SetNo As Integer, ProductCode As String, ProductName As String)
        Dim path As String = "Reports/SBVB"
        Dim strFontFamily As String = Server.MapPath("Fonts/Consolab.ttf")
        Dim baseFont As iTextSharp.text.pdf.BaseFont = iTextSharp.text.pdf.BaseFont.CreateFont(strFontFamily, iTextSharp.text.pdf.BaseFont.IDENTITY_H, iTextSharp.text.pdf.BaseFont.NOT_EMBEDDED)

        Try

            If (Not Directory.Exists(Server.MapPath(path))) Then
                Directory.CreateDirectory(Server.MapPath(path))
            End If
            Dim strProductName As String
            strProductName = ddlYear.SelectedValue
            If bIsRegional = True Then
                strProductName = strProductName + "R"
            Else
                strProductName = strProductName + "F"
            End If
            strProductName = strProductName + SetNo.ToString + ProductCode

            Dim ds As DataSet
            Dim strCmdText As String = ""
            Dim prmArray(4) As SqlParameter
            prmArray(0) = New SqlParameter("@Year", DbType.Int32)
            prmArray(0).Value = ddlYear.SelectedValue
            prmArray(1) = New SqlParameter("@ProductCode", DbType.String)
            prmArray(1).Value = ProductCode
            prmArray(2) = New SqlParameter("@EventId", DbType.UInt32)
            If bIsRegional = True Then
                prmArray(2).Value = 2
            Else
                prmArray(2).Value = 1
            End If
            prmArray(3) = New SqlParameter("@IsSpelling", DbType.UInt32)
            prmArray(3).Value = 1
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetSBVBWord", prmArray)

            Dim para1 As iTextSharp.text.Paragraph = New iTextSharp.text.Paragraph
            Dim para2 As iTextSharp.text.Paragraph = New iTextSharp.text.Paragraph
            Dim para3 As iTextSharp.text.Paragraph = New iTextSharp.text.Paragraph


            Dim cFont As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 15.99, iTextSharp.text.Font.NORMAL) 'Times-Roman"
            Dim cFont1 As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 12, iTextSharp.text.Font.NORMAL)

            'Dim cFontTest As iTextSharp.text.Font = GetFont("C:\Windows\Fonts\Consola.ttf", 16)
            Dim cFont2 As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 14, iTextSharp.text.Font.NORMAL)
            Dim mColor As iTextSharp.text.BaseColor = New iTextSharp.text.BaseColor(192, 0, 0) '(136, 0, 21)
            Dim fBlue As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 15.99, FontStyle.Regular, iTextSharp.text.BaseColor.BLUE)
            Dim fRed As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 15.99, FontStyle.Regular, mColor)
            Dim ch1 As iTextSharp.text.Chunk = New iTextSharp.text.Chunk("North South Foundation-", cFont)
            Dim ch2 As iTextSharp.text.Chunk = New iTextSharp.text.Chunk(ddlYear.SelectedValue, fBlue)
            Dim ch3 As iTextSharp.text.Chunk
            Dim ch4 As iTextSharp.text.Chunk
            Dim ch5 As iTextSharp.text.Chunk
            Dim ch6 As iTextSharp.text.Chunk
            Dim ch7 As iTextSharp.text.Chunk
            Dim ch8 As iTextSharp.text.Chunk
            Dim ch9 As iTextSharp.text.Chunk
            If bIsRegional = True Then
                ch3 = New iTextSharp.text.Chunk(" Regional Contests", cFont)
            Else
                ch3 = New iTextSharp.text.Chunk(" National Finals Contests", cFont)
            End If
            Dim ph1 As New iTextSharp.text.Phrase

            Dim pdfDoc As iTextSharp.text.Document = New iTextSharp.text.Document(iTextSharp.text.PageSize.LETTER)
            'Response.ContentType = "application/pdf"
            'Response.AddHeader("content-disposition", "attachment;filename=" + strProductName + ".pdf")
            'Response.Cache.SetCacheability(HttpCacheability.NoCache)
            Dim dirPath As String = "Reports/SBVB/" + strProductName + ".pdf"
            Dim fs As System.IO.FileStream = New FileStream(Server.MapPath(dirPath), FileMode.Create)
            Dim writer As iTextSharp.text.pdf.PdfWriter = iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, fs)
            Dim sw As StringWriter = New StringWriter()
            Dim hw As HtmlTextWriter = New HtmlTextWriter(sw)
            pdfDoc.SetMargins(70, 71, 71, 9.0F)
            Dim htmlparser As iTextSharp.text.html.simpleparser.HTMLWorker = New iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc)

            pdfDoc.Open()
            para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            Dim sHead1ChSpace As Single = 0.025
            pdfDoc.NewPage()
            ch1.SetCharacterSpacing(sHead1ChSpace)
            ch2.SetCharacterSpacing(sHead1ChSpace)
            ch3.SetCharacterSpacing(sHead1ChSpace)
            ph1.Add(ch1)
            ph1.Add(ch2)
            ph1.Add(ch3)
            para1.Add(ph1)
            para1.ExtraParagraphSpace = 3.5
            pdfDoc.Add(para1)
            ch1 = New iTextSharp.text.Chunk(ProductName, fRed)
            ch2 = New iTextSharp.text.Chunk(" (", cFont)
            ch3 = New iTextSharp.text.Chunk(ProductCode, fRed)
            ch4 = New iTextSharp.text.Chunk(")", cFont)

            If bIsRegional = True Then
                ch5 = New iTextSharp.text.Chunk(" 1000 Practice Words", cFont)
            Else
                ch5 = New iTextSharp.text.Chunk(" Practice Words", cFont)
            End If
            Dim ph2 = New iTextSharp.text.Phrase()
            ch1.SetCharacterSpacing(sHead1ChSpace)
            ch2.SetCharacterSpacing(sHead1ChSpace)
            ch3.SetCharacterSpacing(sHead1ChSpace)
            ch4.SetCharacterSpacing(sHead1ChSpace)
            ch5.SetCharacterSpacing(sHead1ChSpace)
            ph2.Add(ch1)
            ph2.Add(ch2)
            ph2.Add(ch3)
            ph2.Add(ch4)
            ph2.Add(ch5)
            para1 = New iTextSharp.text.Paragraph
            para1.SpacingBefore = 3
            para1.Add(ph2)
            Dim sContentParaSpace As Single = 0.025
            para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            para1.SpacingAfter = 13 '11.9
            pdfDoc.Add(para1)

            ch1 = New iTextSharp.text.Chunk("Dear Contestant", cFont2)
            ch1.SetCharacterSpacing(sContentParaSpace)
            ph1 = New iTextSharp.text.Phrase()
            ph1.Add(ch1)
            para2 = New iTextSharp.text.Paragraph()
            para2.Add(ph1)
            ' para2 = New iTextSharp.text.Paragraph("Dear Contestant", cFont2)
            para2.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            para1.SpacingAfter = 2
            pdfDoc.Add(para2)
            If bIsRegional = True Then
                Dim cFontBlue As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE)
                ch1 = New iTextSharp.text.Chunk("Out of the ", cFont1)
                ch2 = New iTextSharp.text.Chunk("25 ", cFontBlue)
                ch3 = New iTextSharp.text.Chunk("Phase I questions, ", cFont1)
                ch4 = New iTextSharp.text.Chunk("10 ", New iTextSharp.text.Font(baseFont, 12, iTextSharp.text.Font.NORMAL, mColor))
                ch5 = New iTextSharp.text.Chunk("will come from the 1000 practice", cFont1)

                ch6 = New iTextSharp.text.Chunk("words given here; All the ", cFont1)
                ch7 = New iTextSharp.text.Chunk("6 ", cFontBlue)
                ch8 = New iTextSharp.text.Chunk("rounds of Phase II questions will come", cFont1)

                ch9 = New iTextSharp.text.Chunk("from the 1000 practice words given here", cFont1)
                'ch1.SetCharacterSpacing(sContentParaSpace)
                'ch2.SetCharacterSpacing(sContentParaSpace)
                'ch3.SetCharacterSpacing(sContentParaSpace)
                'ch4.SetCharacterSpacing(sContentParaSpace)
                'ch5.SetCharacterSpacing(sContentParaSpace)
                'ch6.SetCharacterSpacing(sContentParaSpace)
                'ch7.SetCharacterSpacing(sContentParaSpace)
                'ch8.SetCharacterSpacing(sContentParaSpace)
                'ch9.SetCharacterSpacing(sContentParaSpace)
                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch1)
                'ph2.Add(ch2)
                'ph2.Add(ch3)
                'ph2.Add(ch4)
                'ph2.Add(ch5)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
                'para1.Leading = 15.1
                'pdfDoc.Add(para1)

                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch6)
                'ph2.Add(ch7)
                'ph2.Add(ch8)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
                'para1.Leading = 14.5
                'pdfDoc.Add(para1)

                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch9)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER

                'para1.Leading = 14.1
                'para1.SpacingAfter = 12.5
                'pdfDoc.Add(para1)
            Else
                Dim cFontBlue As iTextSharp.text.Font = New iTextSharp.text.Font(baseFont, 12, iTextSharp.text.Font.NORMAL, iTextSharp.text.BaseColor.BLUE)
                ch1 = New iTextSharp.text.Chunk("Out of the ", cFont1)
                ch2 = New iTextSharp.text.Chunk("30 ", cFontBlue)
                ch3 = New iTextSharp.text.Chunk("Phase I questions, ", cFont1)
                ch4 = New iTextSharp.text.Chunk("10 ", New iTextSharp.text.Font(baseFont, 12, iTextSharp.text.Font.NORMAL, mColor))
                ch5 = New iTextSharp.text.Chunk("will come from these 1000 practice", cFont1)

                ch6 = New iTextSharp.text.Chunk("words; All the ", cFont1)
                ch7 = New iTextSharp.text.Chunk("3 ", cFontBlue)
                ch8 = New iTextSharp.text.Chunk("rounds of Phase II questions will come from these", cFont1)

                ch9 = New iTextSharp.text.Chunk("1000 practice words. All Phase III words are from unpublished sources.", cFont1)

                'ch1.SetCharacterSpacing(sContentParaSpace)
                'ch2.SetCharacterSpacing(sContentParaSpace)
                'ch3.SetCharacterSpacing(sContentParaSpace)
                'ch4.SetCharacterSpacing(sContentParaSpace)
                'ch5.SetCharacterSpacing(sContentParaSpace)
                'ch6.SetCharacterSpacing(sContentParaSpace)
                'ch7.SetCharacterSpacing(sContentParaSpace)
                'ch8.SetCharacterSpacing(sContentParaSpace)
                'ch9.SetCharacterSpacing(sContentParaSpace)
                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch1)
                'ph2.Add(ch2)
                'ph2.Add(ch3)
                'ph2.Add(ch4)
                'ph2.Add(ch5)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
                'para1.Leading = 14.2
                'pdfDoc.Add(para1)

                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch6)
                'ph2.Add(ch7)
                'ph2.Add(ch8)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
                'para1.Leading = 14.5
                'pdfDoc.Add(para1)

                'ph2 = New iTextSharp.text.Phrase()
                'ph2.Add(ch9)
                'para1 = New iTextSharp.text.Paragraph
                'para1.Add(ph2)
                'para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER

                'para1.Leading = 13.5
                'para1.SpacingAfter = 10.6
                'pdfDoc.Add(para1)
            End If

            ch1.SetCharacterSpacing(sContentParaSpace)
            ch2.SetCharacterSpacing(sContentParaSpace)
            ch3.SetCharacterSpacing(sContentParaSpace)
            ch4.SetCharacterSpacing(sContentParaSpace)
            ch5.SetCharacterSpacing(sContentParaSpace)
            ch6.SetCharacterSpacing(sContentParaSpace)
            ch7.SetCharacterSpacing(sContentParaSpace)
            ch8.SetCharacterSpacing(sContentParaSpace)
            ch9.SetCharacterSpacing(sContentParaSpace)
            ph2 = New iTextSharp.text.Phrase()
            ph2.Add(ch1)
            ph2.Add(ch2)
            ph2.Add(ch3)
            ph2.Add(ch4)
            ph2.Add(ch5)
            para1 = New iTextSharp.text.Paragraph
            para1.Add(ph2)
            para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            para1.Leading = 14.2
            pdfDoc.Add(para1)

            ph2 = New iTextSharp.text.Phrase()
            ph2.Add(ch6)
            ph2.Add(ch7)
            ph2.Add(ch8)
            para1 = New iTextSharp.text.Paragraph
            para1.Add(ph2)
            para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER
            para1.Leading = 14.5
            pdfDoc.Add(para1)

            ph2 = New iTextSharp.text.Phrase()
            ph2.Add(ch9)
            para1 = New iTextSharp.text.Paragraph
            para1.Add(ph2)
            para1.Alignment = iTextSharp.text.Element.ALIGN_CENTER

            para1.Leading = 13.5
            para1.SpacingAfter = 10.6
            pdfDoc.Add(para1)


            pdfDoc.SetPageSize(iTextSharp.text.PageSize.LETTER)
            'para1 = New iTextSharp.text.Paragraph
            'para1.Add(" ")
            'pdfDoc.Add(para1)

            FillWords(ds)

            pnlPDF.RenderControl(hw)
            Dim repeaterTable As String = sw.ToString()
            Dim sread As StringReader = New StringReader(repeaterTable)
            htmlparser.Parse(sread)
            pdfDoc.Close()
            'Response.Write(pdfDoc)
            'Response.End()
            writer.Close()
            fs.Close()

        Catch ex As Exception
            '  Response.Write(ex.ToString)
        End Try
    End Sub
    Dim Word As String = ""
    Sub FillWords(ds As DataSet)
        Dim iEndRowCnt As Integer = 132
        Dim iBeginRowCnt As Integer = 1
        Dim iCheckDSRowCnt As Integer = 44
        Dim iWrapLength As Integer = 21

        Dim sbLayoutTable As New StringBuilder
        sbLayoutTable.Append("<table style='font-size:10.5px;font-family:Courier;' width='100%'>")
        Dim iRowCnt As Integer = 0, iColCnt As Integer = 0
        While (1 = 1)
            If iBeginRowCnt = 1 Then
                iEndRowCnt = 132
            Else
                iEndRowCnt = iBeginRowCnt + 162
            End If
            Dim drHead As DataRow() = ds.Tables(0).Select("style='Head' and SL>=" & iBeginRowCnt & " and SL<=" & iEndRowCnt)
            Dim iHeadCnt As Integer = drHead.Length
            If iHeadCnt > 0 Then
                iEndRowCnt = iEndRowCnt - iHeadCnt
            End If
            Dim drHighestWrd As DataRow() = ds.Tables(0).Select("SL>=" & iBeginRowCnt & " and SL<=" & iEndRowCnt & " and LEN(Word)>" & iWrapLength)
            If drHighestWrd.Length > 0 Then
                iEndRowCnt = iEndRowCnt - drHighestWrd.Length
            End If
            Dim drs As DataRow() = ds.Tables(0).Select("SL>=" & iBeginRowCnt & " and SL<=" & iEndRowCnt)
            If drs.Length = 0 Then
                Exit While
            End If
            Dim dr As DataRow
            Dim sb As New StringBuilder()
            iRowCnt = 0
            iColCnt = 0
            If iBeginRowCnt = 1 Then
                iCheckDSRowCnt = 44
            Else
                iCheckDSRowCnt = 54
            End If
            Dim iRIndx As Integer = 0
            Dim iEndDRSRowCnt As Integer = drs.Length() - 1
            Dim iEndValueOfiRIndx As Integer = 0
            For iRIndx = 0 To iEndDRSRowCnt
                dr = drs(iRIndx)
                Word = dr("word").ToString().Trim()
                If Word = "yeoman" Then
                    Word = Word
                End If
                If iRowCnt = 0 Then
                    If iColCnt = 0 Then
                        sb.Append("<table width='87%' cellspacing=0 cellpading=0>")
                    Else
                        sb.Append("<table width='96%' cellspacing=0 cellpading=0>")
                    End If
                End If
                If dr("Style") = "Head" Then
                    If iColCnt = 0 Then
                        sb.Append("<tr><td colspan=4 style='font-size:15pt;'><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><td></td></tr></table><table cellspacing=0 cellpadding=4><tr><td style='vertical-align:middle;color:blue;font-family:Consola;'><b>" & Word & "</b></td></tr></table><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><Td></td></tr></table></td></tr>")
                    Else
                        sb.Append("<tr><td></td><td colspan=10 style='font-size:15pt;'><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><td></td></tr></table><table cellspacing=0 cellpadding=4><tr><td style='vertical-align:middle;color:blue;font-family:Consola;'><b>" & Word & "</b></td></tr></table><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><Td></td></tr></table></td></tr>")
                    End If
                    iRowCnt = iRowCnt + 2
                Else
                    If Word.Length() > iWrapLength Then
                        iRowCnt = iRowCnt + 1
                    End If
                    If iColCnt = 0 Then
                        sb.Append("<tr><td colspan='4'>" & Word & "</td></tr>")
                    Else
                        '---<td></td>
                        sb.Append("<tr><td></td><td colspan='10'>" & Word & "</td></tr>")
                    End If
                    iRowCnt = iRowCnt + 1
                End If

                If iRowCnt > iCheckDSRowCnt Then
                    If dr("Style") = "Head" Then
                        Dim s As String
                        If iColCnt = 0 Then
                            s = "<tr><td colspan=4 style='font-size:15pt;'><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><td></td></tr></table><table cellspacing=0 cellpadding=4><tr><td style='vertical-align:middle;color:blue;font-family:Consola;'><b>" & Word & "</b></td></tr></table><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><Td></td></tr></table></td></tr>"
                        Else
                            s = "<tr><td></td><td colspan=10 style='font-size:15pt;'><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><td></td></tr></table><table cellspacing=0 cellpadding=4><tr><td style='vertical-align:middle;color:blue;font-family:Consola;'><b>" & Word & "</b></td></tr></table><table border=1 cellspacing=0 cellpadding=0 width=100%><tr><Td></td></tr></table></td></tr>"
                        End If

                        Dim iInx As Integer = sb.ToString().IndexOf(s)
                        Dim iLength As Integer = s.Length
                        sb.Remove(iInx, iLength)
                        If iColCnt = 0 Then
                            sb.Append("<tr><td colspan=4></td></tr>")
                        Else
                            sb.Append("<tr><td colspan=11></td></tr>")
                        End If
                        iRIndx = iRIndx - 1
                    Else
                        Dim s As String
                        If iColCnt = 0 Then
                            s = "<tr><td colspan='4'>" & Word & "</td></tr>"
                        Else
                            s = "<tr><td></td><td colspan='10'>" & Word & "</td></tr>"
                        End If

                        Dim iInx As Integer = sb.ToString().IndexOf(s)
                        Dim iLength As Integer = s.Length()
                        sb.Remove(iInx, iLength)
                        If Word.Length() > iWrapLength Then
                            If iColCnt = 0 Then
                                sb.Append("<tr><td></td><td></td><td></td><td></td></tr>")
                            Else
                                sb.Append("<tr><td colspan=11></td></tr>")
                            End If

                        End If
                        iRIndx = iRIndx - 1
                    End If
                End If
                If iRowCnt >= iCheckDSRowCnt Or iEndDRSRowCnt - iRIndx = 0 Then
                    If iRowCnt <> 0 Then
                        Dim i As Integer = 1
                        For i = iRowCnt + 1 To iCheckDSRowCnt
                            If iColCnt = 0 Then
                                sb.Append("<tr><td colspan='4' style='color:white;'> .... </td></tr>")
                            Else
                                sb.Append("<tr><td colspan='11' style='color:white;'> .... </td></tr>")
                            End If
                        Next
                    End If

                    sb.Append("</table>")
                    If iColCnt = 0 Then
                        sbLayoutTable.Append("<tr>")
                    End If
                    If iColCnt = 0 Then
                        sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                    ElseIf iColCnt = 1 Then
                        sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                    Else
                        sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                    End If
                    sb.Remove(0, sb.Length)
                    iColCnt = iColCnt + 1
                    If iColCnt = 3 Then
                        sbLayoutTable.Append("</tr>")
                        If iRowCnt >= iCheckDSRowCnt Then
                            iEndValueOfiRIndx = iRIndx
                            iColCnt = 0
                            iRowCnt = 0
                            Exit For
                        End If
                        iColCnt = 0
                    End If
                    iRowCnt = 0

                End If
                iEndValueOfiRIndx = iRIndx


            Next
            'If iRowCnt <> 0 Then
            '    Dim i As Integer = 1
            '    For i = iRowCnt + 1 To iCheckDSRowCnt
            '        If iColCnt = 0 Then
            '            sb.Append("<tr><td colspan='4' style='color:white;'>TEST</td></tr>")
            '        Else
            '            sb.Append("<tr><td colspan='12' style='color:white;'>TEST</td></tr>")
            '        End If
            '    Next
            'End If
            If iColCnt <> 0 Then
                ''sb.Append("</table>")
                ''If iColCnt = 0 Then
                ''    sbLayoutTable.Append("<tr>")
                ''End If
                ''If iColCnt = 0 Then
                ''    sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                ''ElseIf iColCnt = 1 Then
                ''    sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                ''Else
                ''    sbLayoutTable.Append("<td style='vertical-align:top;' colspan=2>" & sb.ToString() & "</td>")
                ''End If

                ''sb.Remove(0, sb.Length)
                ''iColCnt = iColCnt + 1
                'If iColCnt = 3 Then
                '    sbLayoutTable.Append("</tr>")
                'End If
                Select Case iColCnt
                    Case 1
                        '2,3
                        sbLayoutTable.Append("<td></td><td></td><td></td>")
                    Case 2
                        '3
                        sbLayoutTable.Append("<td></td><td></td>")
                End Select
                sbLayoutTable.Append("</tr>")
                iColCnt = 0
                iRowCnt = 0
            End If

            iEndRowCnt = iEndRowCnt - (iEndDRSRowCnt - iEndValueOfiRIndx)
            'iEndRowCnt = iEndRowCnt - (iEndValueOfiRIndx - iEndDRSRowCnt)
            iBeginRowCnt = iEndRowCnt + 1
        End While
        sbLayoutTable.Append("</table>")
        spPDF.InnerHtml = sbLayoutTable.ToString()
    End Sub
End Class
