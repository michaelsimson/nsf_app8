using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;


public partial class Admin_VolunteerEditByChap : System.Web.UI.Page
{
    static string aValue, pgValue;
    static DataTable pdt = new DataTable();
    static DataTable prod = new DataTable();
    protected void Page_Load(object sender, EventArgs e)
    {
        Session["EventCode"] = null;
        Session["ProdGroup"] = null;       

    }
    protected void GridView1_PreRender(object sender, EventArgs e)
    {
       // if(IsPostBack==false)
        foreach(GridViewRow Row in GridView1.Rows)
            if(((Label)Row.FindControl("lblName")) !=null)
        ((Label)Row.FindControl("lblName")).Text =
               GetName(((Label)Row.FindControl("lblMemberID")).Text);
       
    }
    protected void GridView1_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        
        GridViewRow row = GridView1.Rows[GridView1.EditIndex];
        //if (row.RowState == DataControlRowState.)
        {
           // lblMessage.Text = "Updating";
           // VolEditDS.InsertParameters[0].DefaultValue = ((TextBox)row.FindControl("TextBox1")).Text ;
            //Determine RoleId selection
            e.NewValues["MemberID"] = ((TextBox)row.FindControl("TextBox1")).Text;
            ((DropDownList)row.FindControl("ddlRoleId")).SelectedIndex =
                ((DropDownList)row.FindControl("ddlRoleCode")).SelectedIndex;
          //  VolEditDS.InsertParameters[1].DefaultValue = ((DropDownList)row.FindControl("ddlRoleId")).SelectedValue.ToString();
            e.NewValues["RoleId"] = ((DropDownList)row.FindControl("ddlRoleId")).SelectedValue;
            e.NewValues["RoleCode"] = ((DropDownList)row.FindControl("ddlRoleCode")).SelectedValue.ToString();
            //detrmine chapid
               ((DropDownList)row.FindControl("ddlChapterId")).SelectedIndex =
               ((DropDownList)row.FindControl("ddlChapterCode")).SelectedIndex;
            e.NewValues["ChapterId"] = ((DropDownList)row.FindControl("ddlChapterId")).SelectedValue;

            //Determine EventYr, EventId, etc. 
            int ddlEventIndex = ((DropDownList)row.FindControl("ddlEventCode")).SelectedIndex;
            ((DropDownList)row.FindControl("ddlEventId")).SelectedIndex =
                ((DropDownList)row.FindControl("ddlEventCode")).SelectedIndex;
            e.NewValues["EventCode"] = ((DropDownList)row.FindControl("ddlEventCode")).SelectedValue;
            e.NewValues["EventId"] = ((DropDownList)row.FindControl("ddlEventId")).SelectedValue;
            //
            e.NewValues["EventYear"] = ((TextBox)row.FindControl("tbYear")).Text;
            // = .ToString();
           // VolEditDS.InsertParameters[5].DefaultValue = ((DropDownList)row.FindControl("ddlEventId")).SelectedValue.ToString();


            //Determine ProdCodeID, ProdId, etc.
            if (((DropDownList)row.FindControl("ddlProdGroupCode")).SelectedIndex > -1)
                if(pdt.Rows.Count>0)
            {
                int ddlProdGroupIndex = ((DropDownList)row.FindControl("ddlProdGroupCode")).SelectedIndex;
                e.NewValues["ProductGroupID"] = pdt.Rows[ddlProdGroupIndex-1].ItemArray[0].ToString();
                e.NewValues["ProductGroupCode"] = pdt.Rows[ddlProdGroupIndex-1].ItemArray[1].ToString();
            }
            if (((DropDownList)row.FindControl("ddlProdCode")).SelectedIndex > -1)
                if(prod.Rows.Count>0)
            {
                int ddlProdIndex = ((DropDownList)row.FindControl("ddlProdCode")).SelectedIndex;
                e.NewValues["ProductID"] = prod.Rows[ddlProdIndex-1].ItemArray[0].ToString();
                e.NewValues["ProductCode"] = prod.Rows[ddlProdIndex-1].ItemArray[1].ToString();
            }
        }
    }
   
    protected void GetFromProducts()
    {
        //ProductTableAdapters.ProductTableAdapter pta = new ProductTableAdapters.ProductTableAdapter();
        ProdgroupTableAdapters.ProductGroupTableAdapter pta = new ProdgroupTableAdapters.ProductGroupTableAdapter();
        pdt.Clear();
        if (aValue != null)
            pdt = pta.GetDataByPG(aValue);

    }
    protected void GetFromProductsByGp()
    {
        ProductTableAdapters.ProductTableAdapter ptg = new ProductTableAdapters.ProductTableAdapter();
        prod.Clear();
        if (pgValue != null && aValue != null)
            prod = ptg.GetDataByProdGp(pgValue, aValue);
    }
    protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
    }
    protected void GridView1_DataBound(object sender, EventArgs e)
    {
       
    }
    protected void GridView1_RowCreated(object sender, GridViewRowEventArgs e)
    {
      
    }
    protected void GridView1_RowEditing1(object sender, GridViewEditEventArgs e)
    {
        
        GridViewRow Row = GridView1.Rows[e.NewEditIndex];
        
        if (Row.RowState == DataControlRowState.Edit )
        {
                if (((DropDownList)Row.FindControl("ddlRoleCode")).SelectedValue == "ChapterC")
                {
                    ((DropDownList)Row.FindControl("ddlEventCode")).Enabled = false;
                    ((DropDownList)Row.FindControl("ddlProdGroupCode")).Enabled = false;
                    ((DropDownList)Row.FindControl("ddlProdCode")).Enabled = false;
                }
                else
                {
                    ((DropDownList)Row.FindControl("ddlEventCode")).Enabled = true;
                    ((DropDownList)Row.FindControl("ddlProdGroupCode")).Enabled = true;
                    ((DropDownList)Row.FindControl("ddlProdCode")).Enabled = true;
                }
                ((DropDownList)Row.FindControl("ddlChapterId")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlChapterCode")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlNational")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlZoneCode")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlClusterCode")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlZoneId")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlClusterId")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlFinals")).Enabled = false;
                ((DropDownList)Row.FindControl("ddlEventCode")).Items.Remove("Finals");
                ((DropDownList)Row.FindControl("ddlEventId")).Items.RemoveAt(1);
                
                
            }
        }

    protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
    {

        if( (e.Row.RowState & DataControlRowState.Edit)>0)
        {
            /* if (Session["RoleId"] != null && Convert.ToInt32(Session["RoleId"]) != 1)
             {
                 ((DropDownList)row.FindControl("ddlAgent")).Enabled = false;
                 ((DropDownList)row.FindControl("ddlWrite")).Enabled = false;
                 ((DropDownList)row.FindControl("ddlAuth")).Enabled = false;
                 ((DropDownList)row.FindControl("ddlAgent")).SelectedIndex = 2;
                 ((DropDownList)row.FindControl("ddlWrite")).SelectedIndex = 2;
                 ((DropDownList)row.FindControl("ddlAuth")).SelectedIndex = 2;
             }*/
            if (Session["LoginRole"] != null && Session["LoginRole"].ToString() == "ChapterC")
            {
                ((DropDownList)e.Row.FindControl("ddlChapterId")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlChapterCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlNational")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlZoneCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlClusterCode")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlZoneId")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlClusterId")).Enabled = false;
                ((DropDownList)e.Row.FindControl("ddlFinals")).Enabled = false;
                //   ((DropDownList)e.Row.FindControl("ddlChapterId")).SelectedValue = Session["LoginChapterID"].ToString();
                //   ((DropDownList)e.Row.FindControl("ddlChapter")).SelectedIndex =
                //   ((DropDownList)e.Row.FindControl("ddlChapterId")).SelectedIndex;
                ((DropDownList)e.Row.FindControl("ddlEventId")).Items.RemoveAt(1);
                ((DropDownList)e.Row.FindControl("ddlEventCode")).Items.Remove("Finals");
            }
            if (this.IsPostBack == true)
            {
                
                
            }
            ((TextBox)e.Row.FindControl("tbName")).Text =
                GetName(((TextBox)e.Row.FindControl("tbMemberID")).Text);
        }
    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        
    }

    protected void ddlProdGroupCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        GridViewRow Row = GridView1.Rows[GridView1.EditIndex];
        Session["ProdGroup"] = (((DropDownList)Row.FindControl("ddlProdGroupCode")).SelectedValue.ToString());
        Session["EventCode"] = (((DropDownList)Row.FindControl("ddlEventCode")).SelectedValue.ToString());
        ProductTableAdapters.ProductTableAdapter pt = new ProductTableAdapters.ProductTableAdapter();
        DataTable dt = new DataTable();
        dt = pt.GetDataByProdGp(Session["ProdGroup"].ToString(), Session["EventCode"].ToString());
       ((DropDownList)Row.FindControl("ddlProdCode")).Items.Clear();
       ((DropDownList)Row.FindControl("ddlProductID")).Items.Clear();
       for (int i = 0; i < dt.Rows.Count; i++)
          {
             ((DropDownList)Row.FindControl("ddlProdCode")).Items.Add(dt.Rows[i].ItemArray[1].ToString());
             ((DropDownList)Row.FindControl("ddlProductID")).Items.Add(dt.Rows[i].ItemArray[0].ToString());
          }
      }

    protected void ddlRoleCode_SelectedIndexChanged(object sender, EventArgs e)
    {
        
        GridViewRow Row = GridView1.Rows[GridView1.EditIndex];
        if (((DropDownList)Row.FindControl("ddlRoleCode")).SelectedValue == "ChapterC")
        {

            ((DropDownList)Row.FindControl("ddlEventCode")).Enabled = false;
            ((DropDownList)Row.FindControl("ddlProdGroupCode")).Enabled = false;
            ((DropDownList)Row.FindControl("ddlProdCode")).Enabled = false;
        }
    }
    protected void ddlEventCode_SelectedIndexChanged(object sender, EventArgs e)
    {

        GridViewRow Row = GridView1.Rows[GridView1.EditIndex];
        Session["EventCode"] = (((DropDownList)Row.FindControl("ddlEventCode")).SelectedValue.ToString());
        ProdgroupTableAdapters.ProductGroupTableAdapter pgt = new ProdgroupTableAdapters.ProductGroupTableAdapter();
        DataTable dt = new DataTable();
        dt = pgt.GetDataByPG(Session["EventCode"].ToString());
 //       ((DropDownList)Row.FindControl("ddlProdGroupCode")).DataSource = dt.Columns[1];
  //      ((DropDownList)Row.FindControl("ddlProdGroupId")).DataSource = dt.Columns[0];

        ((DropDownList)Row.FindControl("ddlProdGroupCode")).Items.Clear();
        ((DropDownList)Row.FindControl("ddlProdGroupId")).Items.Clear();
         for (int i = 0; i < dt.Rows.Count; i++)
             {
                ((DropDownList)Row.FindControl("ddlProdGroupCode")).Items.Add(dt.Rows[i].ItemArray[1].ToString());
                ((DropDownList)Row.FindControl("ddlProdGroupId")).Items.Add(dt.Rows[i].ItemArray[0].ToString());
             }
    }
    public string GetName(string memberId)
    {
        string name = "";
        IndSpouseNameTableAdapters.IndSpouseTableAdapter ita = new IndSpouseNameTableAdapters.IndSpouseTableAdapter();
        DataTable dt = new DataTable();
        dt = ita.GetData(Convert.ToInt32(memberId));
        if (dt.Rows.Count > 0)
            name = dt.Rows[0].ItemArray[0].ToString() + " " +
                dt.Rows[0].ItemArray[2].ToString();
        else name = "Not found";
        return name;
    }
    protected void hlinkMenu_Click(object sender, EventArgs e)
    {
        if(Session["LoginRole"].ToString() =="ChapterC")
            Response.Redirect("~/ChapVolunteerRoles.aspx");
        else if(Session["LoginRole"].ToString() =="NationalC")
            Response.Redirect("~/VolunteerFunctions.aspx");
    }
}

 