using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Web.Configuration;

public partial class Admin_VolunteerSearch : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
     

    }
    protected void btnSearch_Click(object sender, EventArgs e)
    {
        string sSQL;
        string sSQLORG;

        sSQL = " DeletedFlag <> 'Yes'";
        sSQLORG = " DeletedFlag <> 'Yes'";
        //sSQL = " WHERE DeletedFlag <> 'Yes'";
        //sSQLORG = " WHERE DeletedFlag <>'Yes'";
        
        bool blnCheck  = false;
        string Email = tbEmail.Text.Trim();
        string FirstName = tbFnameOrg.Text.Trim();
        string LastName = tbLname.Text.Trim();
        string Street = tbStreet.Text.Trim();
        string City = tbCity.Text.Trim();
        string state = ddlState.SelectedValue.ToString();
        string ZipCode  = tbZip.Text.Trim();
        string HomePhone  = tbPhone.Text.Trim();
        string ReferredBy  = tbRef.Text.Trim();
        string LiaisonPerson = tbLiaison.Text.Trim();
        string NSFChapter = ddlChapter.SelectedValue.ToString();
        string otherState = tbOther.Text.Trim();
        

        if (FirstName.Length > 0)
        {
            sSQL = sSQL + " and FIRSTNAME like '%" + FirstName + "%'";
            sSQLORG = sSQLORG + " and ORGANIZATION_NAME like '%" + FirstName + "%'";
            blnCheck = true;
        }

        if (LastName.Length > 0) 
        {
            sSQL = sSQL + " and LASTNAME like '%" + LastName + "%'";
            sSQLORG = sSQLORG + " and ORGANIZATION_NAME like '%" + LastName + "%'";
            blnCheck = true;
        }

        if (Street.Length > 0) 
        {
            sSQL = sSQL + " and ADDRESS1 like '%" + Street + "%'";
            sSQLORG = sSQLORG + " and ADDRESS1 like '%" + Street + "%'";
            blnCheck = true;
        }

        if (City.Length > 0) 
        {
            sSQL = sSQL + " and CITY like '%" + City + "%'";
            sSQLORG = sSQLORG + " and CITY like '%" + City + "%'";
            blnCheck = true;
        }

        if (state.Length > 0 && state != "Select State") 
        {
            sSQL = sSQL + " and STATE like '%" + state + "%'";
            sSQLORG = sSQLORG + " and STATE like '%" + state + "%'";
            blnCheck = true;
        }

        if (otherState.Length > 0 && state == "Other")
        {
            sSQL = sSQL + " and STATE like '%" + otherState + "%'";
            sSQLORG = sSQLORG + " and STATE like '%" + otherState + "%'";
            blnCheck = true;
        }

        if (ZipCode.Length > 0) 
        {
            sSQL = sSQL + " and ZIP like '%" + ZipCode + "%'";
            sSQLORG = sSQLORG + " and ZIP like '%" + ZipCode + "%'";
            blnCheck = true;
        }

        if (Email.Length > 0) 
        {
            sSQL = sSQL + " and EMAIL like '%" + Email + "%'";
            sSQLORG = sSQLORG + " and EMAIL like '%" + Email + "%'";
            blnCheck = true;
        }

        if (HomePhone.Length > 0)
        {
            sSQL = sSQL + " and HPHONE like '%" + HomePhone + "%'";
            sSQLORG = sSQLORG + " and PHONE like '%" + HomePhone + "%'";
            blnCheck = true;
        }

        if (ReferredBy.Length > 0) 
        {
            sSQL = sSQL + " and Referred_By like '%" + ReferredBy + "%'";
            sSQLORG = sSQLORG + " and Referred_By like '%" + ReferredBy + "%'";
            blnCheck = true;
        }

        if (LiaisonPerson.Length > 0) 
        {
            sSQL = sSQL + " and LiasonPerson like '%" + LiaisonPerson + "%'";
            sSQLORG = sSQLORG + " and LiaisonPerson like '%" + LiaisonPerson + "%'";
            blnCheck = true;
        }

        if (NSFChapter.Length > 0 && NSFChapter != "[Select Chapter]")
        {
            sSQL = sSQL + " and CHAPTER like '%" + NSFChapter + "%'";
            sSQLORG = sSQLORG + " and NSF_CHAPTER like '%" + NSFChapter + "%'";
            blnCheck = true;
    }

        if (blnCheck == false)
        {
            sSQL = " DeletedFlag <> 'Yes'";
            sSQLORG = " DeletedFlag <> 'Yes'";
        }

        Session["sSQL"] = sSQL;
        Session["sSQLORG"] = sSQLORG;
        Response.Redirect("VolunteerResults.aspx");
    }
    protected void ddlState_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
    protected void ddlChapter_SelectedIndexChanged(object sender, EventArgs e)
    {
    }
}

 