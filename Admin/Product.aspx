
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="Product.aspx.cs" Inherits="Product" Title="Product List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
		<data:EntityGridView ID="GridView1" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="ProductDataSource"
			DataKeyNames="ProductId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Product.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True" />				
				<asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode"  />
				<asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode"  />
				<asp:TemplateField HeaderText="EventCode" SortExpression="EventCode">
				<ItemTemplate>
						<asp:Repeater ID="EventCode4" runat="server" DataSourceID="EventFilter4">
							<ItemTemplate>
								<%# Eval("EventCode") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="EventFilter4" runat="server"
							DataSourceID="EventDataSource4"
							Filter='<%# String.Format("EventCode = {0}", Eval("EventCode")) %>'
						/>
					</ItemTemplate>
				</asp:TemplateField>


				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"  />
				<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
						<asp:Repeater ID="DataValue5" runat="server" DataSourceID="StatusFilter5">
							<ItemTemplate>
								<%# Eval("DataText") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="StatusFilter5" runat="server"
							DataSourceID="StatusDataSource5"
							Filter='<%# String.Format("DataValue = {0}", Eval("Status")) %>'
						/>
					</ItemTemplate>
				</asp:TemplateField>


				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No Product Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
		<br />
		<asp:Button runat="server" ID="btnProduct" OnClientClick="javascript:location.href='ProductEdit.aspx'; return false;" Text="Add New"></asp:Button>
			
		<data:ProductGroupDataSource ID="ProductGroupDataSource1" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ProductGroupDataSource ID="ProductGroupDataSource2" runat="server"
			SelectMethod="GetAll"
		/>

		<data:EventDataSource ID="EventDataSource3" runat="server"
			SelectMethod="GetAll"
		/>

		<data:EventDataSource ID="EventDataSource4" runat="server"
			SelectMethod="GetAll"
		/>

		<data:StatusDataSource ID="StatusDataSource5" runat="server"
			SelectMethod="GetByTableID"
		>
			<Parameters>
 
			<data:DataParameter Name="TableID" Type="int32" DefaultValue='2' DataSourceID="LookUpCodesDataSource1" />
			</Parameters>
       </data:StatusDataSource>
  	   <data:LookUpCodesDataSource ID="LookUpCodesDataSource1" runat="server"
 				SelectMethod="GetAll" 
 	    >
		</data:LookUpCodesDataSource>
       
		<data:ProductDataSource ID="ProductDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ProductDataSource>
	    		
</asp:Content>




 

 
 
 