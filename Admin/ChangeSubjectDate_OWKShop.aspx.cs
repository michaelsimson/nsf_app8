﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;

public partial class Admin_ChangeSubjectDate_OWKShop : System.Web.UI.Page
{
    #region Variable Declaration
    GlobalVariable gv = new GlobalVariable();
    DataSet DsDetails;
    DropDownList ddlTemp = null;
    DataSet dsDropdown;

    #endregion

    #region Events
    public static string AccessToken = "ZR8zO6GhdSoGiKIXXgd5Ic3z4xQA";
    public static string Organizerkey = "5516732880916201477";
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = 4240;
        //Session["LoggedIn"] = "true";
        lblMsg.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            try
            {
                hdnAccessToken.Value = AccessToken;
                hdnOrganizerKey.Value = Organizerkey;
                if (null == Session["LoggedIn"] || Session["LoggedIn"].ToString().ToLower() != "true")
                {
                    Response.Redirect("..\\maintest.aspx");
                }
                if (Convert.ToString(Session["entryToken"]) == "Parent")
                {
                    txtUserId.Text = Convert.ToString(Session["UserID"]);
                    txtUserId.Enabled = false;
                    trParentEmail.Visible = false;
                    Data();
                    hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx";
                }
            }
            catch
            {
                SessionExp();
            }
        }
    }
    private void SessionExp()
    {
        if (Session["LoggedIn"] == null)
        {
            Response.Redirect("..\\maintest.aspx");
        }
    }

    protected void btnFindContest_Click(object sender, EventArgs e)
    {
        if (txtUserId.Text != "")
        {
            Data();
        }
    }

    protected void dgSelectedChild_RowEditing(object sender, GridViewEditEventArgs e)
    {
        dgSelectedChild.EditIndex = e.NewEditIndex;
        int selIndex = e.NewEditIndex;

        Data();

        //if (hdnCommandName.Value == "Edit")
        //{
        //    ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubject") as DropDownList).Enabled = true;
        //    ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDate") as DropDownList).Enabled = false;

        //}
        //else if (hdnCommandName.Value == "Edit Date")
        //{
        //    ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDate") as DropDownList).Enabled = true;
        //    ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubject") as DropDownList).Enabled = false;
        //}
        HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[selIndex].Cells[0].FindControl("hdchild");
        HiddenField Grade = (HiddenField)dgSelectedChild.Rows[selIndex].Cells[0].FindControl("Hdgrade");
        HiddenField product = (HiddenField)dgSelectedChild.Rows[selIndex].Cells[0].FindControl("hdProductcode");
        HiddenField Fee = (HiddenField)dgSelectedChild.Rows[selIndex].Cells[0].FindControl("hdFee");

        hdnProductCode.Value = product.Value;
        hdnChildnumber.Value = Childnumber.Value;
        hdnGrade.Value = Grade.Value;
        hdnFee.Value = Fee.Value;

    }

    protected void lnkDropdown_Click(object sender, EventArgs e)
    {
        try
        {

            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            hdnIndex.Value = index.ToString();
            HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdchild");
            HiddenField Grade = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("Hdgrade");
            HiddenField product = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdProductcode");
            HiddenField Fee = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdFee");
            ViewState["ChildNumber"] = Childnumber.Value;
            ViewState["Grade"] = Grade.Value;
            ViewState["Product"] = product.Value;
            ViewState["Fee"] = Fee.Value;

            if (hdnCommandName.Value == "Edit Date")
            {
                DropDownList ddlSub = (DropDownList)dgSelectedChild.Rows[index].Cells[1].FindControl("DDNewSubject");
                DropDownList DDlEventDate = (DropDownList)dgSelectedChild.Rows[index].Cells[3].FindControl("DDlEventDate");
                ddlSub.Visible = false;
                DDlEventDate.Visible = true;
            }
        }
        catch
        {
            // Response.Write("Admin Error");
        }
    }

    protected void ChangeDate_Click(object sender, EventArgs e)
    {
        try
        {
            hdnCommandName.Value = "";
            GridViewRow gvrow = (GridViewRow)(sender as Control).Parent.Parent;
            int index = gvrow.RowIndex;
            hdnIndex.Value = index.ToString();
            HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdchild");
            HiddenField Grade = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("Hdgrade");
            HiddenField product = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdProductcode");
            HiddenField Fee = (HiddenField)dgSelectedChild.Rows[index].Cells[0].FindControl("hdFee");
            ViewState["ChildNumber"] = Childnumber.Value;
            ViewState["Grade"] = Grade.Value;
            ViewState["Product"] = product.Value;
            ViewState["Fee"] = Fee.Value;


        }
        catch
        {
            // Response.Write("Admin Error");
        }
    }

    protected void ddProductGroup(object sender, System.EventArgs e)
    {
        try
        {
            ddlTemp = (DropDownList)sender;
            string commandStringDropdown = "select distinct(E.productcode),pc.RegDeadline,pc.StartTime,CAST(pc.venueID AS VARCHAR(10)) + ',' + E.productcode as CodeVSvenue ," +
            "pc.EndTime,pc.venueID,E.productcode+','+O.ORGANIZATION_NAME as ProductVenue" +
            " from EventFees E  left join   PrepClubCal Pc on  E.EventID=pc.EventID and E.ProductID=pc.ProductId left join OrganizationInfo O on O.AutoMemberID=Pc.VenueID  " +
            " where  " + ViewState["Grade"] + " between E.GradeFrom and E.GradeTo and E.ProductCode not in  " +
            " (select productcode from Registration_PrepClub where ChildNumber=" + ViewState["ChildNumber"] + " ) and e.RegFee <=" + ViewState["Fee"] + "" +
            "and (pc.RegDeadline>GETDATE() or ( pc.RegDeadline=GETDATE() and cast(GETDATE() as time) between pc.StartTime and pc.EndTime)) and VenueId is not null;";
            dsDropdown = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandStringDropdown);
            ddlTemp.DataSource = dsDropdown;
            DataTable dt = dsDropdown.Tables[0];
            ddlTemp.DataTextField = "ProductVenue";
            ddlTemp.DataValueField = "CodeVSvenue";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "[Select Subject]");
            ddlTemp.SelectedIndex = 0;

            if (hdnCommandName.Value == "Edit")
            {
                DropDownList ddlSub = (DropDownList)dgSelectedChild.Rows[Convert.ToInt32(hdnIndex.Value)].Cells[1].FindControl("DDNewSubject");
                DropDownList DDlEventDate = (DropDownList)dgSelectedChild.Rows[Convert.ToInt32(hdnIndex.Value)].Cells[3].FindControl
("DDlEventDate");
                Label lbOrg = (Label)dgSelectedChild.Rows[Convert.ToInt32(hdnIndex.Value)].Cells[3].FindControl
("lbOrg");
                ddlSub.Visible = true;
                DDlEventDate.Visible = false;
                lbOrg.Visible = true;
            }
        }
        catch
        {
            //  Response.Write("Admin Error");
        }
    }

    protected void dgSelectedChild_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        dgSelectedChild.EditIndex = -1;
        Data();
    }

    protected void dgSelectedChild_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            DropDownList ddfromchaptercod = (DropDownList)dgSelectedChild.Rows[e.RowIndex].FindControl("DDNewSubject");
            string str = string.Empty;

            if (ddfromchaptercod.SelectedItem.Text != "[Select Subject]")
            {
                str = ddfromchaptercod.SelectedValue;
                string[] splitProduct = str.Split(',');
                string productcodeName = splitProduct[1];
                string ProductVenue = splitProduct[0];
                int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text,
                    " select  prepclubcalid from PrepClubCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, regdeadline))>cast(GETDATE() as date) and  productcode='"
                    + productcodeName + "' and venueid=" + ProductVenue + ") or ((DATEADD(dd, 0, DATEDIFF(dd, 0, regdeadline))=cast(GETDATE() as date)) and (starttime>cast(GETDATE() as time))   and  productcode='"
                    + productcodeName + "' and venueid=" + ProductVenue + ")"));
                if (ObjValiddate != 0)
                {
                    string qryUpdate = "update Registration_PrepClub set venueid=" + ProductVenue + " ,ProductCode='"
                        + productcodeName + "' where ProductCode='" + ViewState["Product"] + "' and ChildNumber="
                        + ViewState["ChildNumber"] + "";
                    //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, qryUpdate);
                    // Response.Write("<script>alert('Updated successfully')</script>");
                }
                else
                {
                    Response.Write("<script>alert('Time Deadline Crossed ')</script>");
                }
                dgSelectedChild.EditIndex = -1;
                Data();
            }
            dgSelectedChild.EditIndex = -1;
            Data();
        }
        catch (Exception ex)
        {
            //  Response.Write("Admin Error");
        }
    }
    #endregion

    #region User Methods
    protected int GetWrkShopYear()
    {
        return gv.GetYear(Application["ConnectionString"].ToString(), 20);

    }

    protected void Data()
    {
        try
        {
            string strCmd = " select automemberid, case when donortype='IND' then (select automemberid from indspouse where relationship = i.automemberid )" +
                " ELSE isnull(relationship,0)  END from indspouse i  where i.email='" + txtUserId.Text + "'";
            DataSet dsInd = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strCmd);

            string strIds = dsInd.Tables[0].Rows[0][0].ToString() + "," + dsInd.Tables[0].Rows[0][1].ToString();

            string commandString = "select C.First_Name+' '+C.Last_Name as Name,I.FirstName+''+I.LastName as ParentName,Rp.Grade," +
            "Rp.childnumber,Rp.productcode,Rp.ProductID,Rp.Fee,Rp.PaymentDate,Rp.EventDate,RP.EventYear,P.Name as ProductName,Rp.ProductGroupID,Rp.ProductGroupCode, Rp.RegId from " +
                " Registration_OnlineWKShop Rp left join " +
            "Child c on Rp.childnumber=c.childnumber left join IndSpouse I on I.AutoMemberID=Rp.MemberId inner join Product P on (P.ProductID=Rp.ProductID)  where I.AutoMemberId in ( " + strIds + ") and EventYear=" + GetWrkShopYear();//Email='" + txtUserId.Text + "'";
            DsDetails = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandString);
            if (null != DsDetails && DsDetails.Tables.Count > 0)
            {
                if (DsDetails.Tables[0].Rows.Count > 0)
                {
                    spnTitle.Visible = true;
                    dgSelectedChild.DataSource = DsDetails;
                    dgSelectedChild.DataBind();
                }
                else
                {
                    spnTitle.Visible = false;
                    dgSelectedChild.DataSource = DsDetails;
                    dgSelectedChild.DataBind();
                }
            }

            if (null != DsDetails && DsDetails.Tables.Count > 0 && DsDetails.Tables[0].Rows.Count <= 0)
            {
                lblError.Text = "You have not registered yet";
                lblError.Visible = true;

            }
            else
            {
                lblError.Visible = false;
            }
        }
        catch
        {
            // Response.Write("Admin Error");
        }
    }

    protected void PopulateSubject(DropDownList ddlTemp)
    {
        try
        {

            string commandStringDropdown = "select distinct E.productcode as ProductVenue,E.ProductID from EventFees E  left join   OnlineWSCal Pc on  E.EventID=pc.EventID and E.ProductID=pc.ProductId   where  E.EventID=20 and E.EventYear=" + hdnYear.Value + " and E.ProductCode not in   (select productcode from Registration_OnlineWKShop where ChildNumber=" + hdnChildnumber.Value + " ) and e.RegFee <=30.0000 and Pc.EventYear=" + hdnYear.Value + " and Pc.Date>=GetDate() ";
            if (Convert.ToString(Session["entryToken"]) == "Parent")
            {
                commandStringDropdown = commandStringDropdown + " and (pc.RegistrationDeadline>=GETDATE()) ";
            }
            // )

            dsDropdown = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandStringDropdown);
            ddlTemp.DataSource = dsDropdown;
            DataTable dt = dsDropdown.Tables[0];
            ddlTemp.DataTextField = "ProductVenue";
            ddlTemp.DataValueField = "ProductID";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "[Select Subject]");
            ddlTemp.SelectedIndex = 0;


        }
        catch
        {
            //  Response.Write("Admin Error");
        }
    }

    protected void PopulateEventDate(DropDownList ddlTemp)
    {
        try
        {

            string commandStringDropdown = "select distinct Convert(Varchar(10),Date,101) as Date from OnlineWSCal where EventYear=" + hdnYear.Value + " and ProductCode='" + hdnProductCode.Value + "'";

            dsDropdown = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, commandStringDropdown);
            ddlTemp.DataSource = dsDropdown;
            DataTable dt = dsDropdown.Tables[0];
            ddlTemp.DataTextField = "Date";
            ddlTemp.DataValueField = "Date";
            ddlTemp.DataBind();
            ddlTemp.Items.Insert(0, "[Select Date]");
            ddlTemp.SelectedIndex = 0;


        }
        catch
        {
            //  Response.Write("Admin Error");
        }
    }

    public void DeleteRegistration(string RegId)
    {
        try
        {
            string cmdtext = string.Empty;
            cmdtext = " select ws.Webinarkey, RW.RegistrantKey from Registration_OnlineWkShop RW inner join Onlinewscal WS on (RW.OnlineWsCalID=ws.OnlineWsCalId) where RW.RegId=" + RegId + "";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            string Registrantkey = string.Empty;
            string WebinarKey = string.Empty;

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Registrantkey = ds.Tables[0].Rows[0]["RegistrantKey"].ToString();
                    WebinarKey = ds.Tables[0].Rows[0]["Webinarkey"].ToString();
                    hdnWebinarkey.Value = WebinarKey;
                    hdnRegistrantKey.Value = Registrantkey;
                }
            }
        }
        catch
        {

        }
    }

    protected void dgSelectedChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            dgSelectedChild.PageIndex = 0;
            row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            int selIndex = row.RowIndex;
            if (e.CommandName == "Edit Subject")
            {
                dgSelectedChild.Rows[selIndex].BackColor = Color.FromName("#D8D8D8");
                hdnCommandName.Value = "Edit Subject";
                DropDownList DDLSubject = ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubjects") as DropDownList);
                ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubjects") as DropDownList).Visible = false;
                ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDates") as DropDownList).Visible = false;
                //((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnUpdateDate") as Button).Visible = true;
                //((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnCancelDate") as Button).Visible = true;

                ((Button)dgSelectedChild.Rows[selIndex].FindControl("btnEdit") as Button).Visible = true;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnChangeDate") as Button).Visible = true;
                ((Label)dgSelectedChild.Rows[selIndex].FindControl("lbOrg") as Label).Visible = true;

                HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdchild");
                HiddenField Grade = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("Hdgrade");
                HiddenField product = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdProductcode");
                HiddenField Fee = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdFee");
                string year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;

                hdnProductCode.Value = product.Value;
                hdnChildnumber.Value = Childnumber.Value;
                hdnGrade.Value = Grade.Value;
                hdnFee.Value = Fee.Value;
                hdnYear.Value = year;

                string teacherID = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string EventDate = string.Empty;
                string Year = string.Empty;

                Year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;
                ProductID = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPgID") as Label).Text;
                ProductGroupID = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;
                ProductCode = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPrdCode") as Label).Text;
                ProductGroupCode = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPgCode") as Label).Text;

                EventDate = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblEventDate") as Label).Text;

                DateTime dtToday = new DateTime();
                dtToday = DateTime.Now;

                DateTime dtWorkshopDate = Convert.ToDateTime(EventDate);

                dtWorkshopDate = dtWorkshopDate.AddDays(-0);
                if (Session["RoleID"] == null)
                {
                    if (dtToday >= dtWorkshopDate)
                    {
                        lblMsg.Text = "Time Deadline Crossed";
                        GrdChangeOptions.Visible = false;
                    }
                    else
                    {
                        PopulateSubject(DDLSubject);
                        populateChangeOptions();

                    }
                }
                else
                {
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "97")
                    {
                        if (dtToday >= dtWorkshopDate)
                        {
                            PopulateSubject(DDLSubject);
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmMsgToUpdate('subject');", true);
                        }
                        else
                        {
                            PopulateSubject(DDLSubject);
                            populateChangeOptions();
                        }
                    }
                    else
                    {
                        if (dtToday >= dtWorkshopDate)
                        {

                            lblMsg.Text = "Time Deadline Crossed";
                            GrdChangeOptions.Visible = false;
                        }
                        else
                        {
                            PopulateSubject(DDLSubject);
                            populateChangeOptions();
                        }
                    }
                }
            }
            else if (e.CommandName == "Edit Date")
            {
                hdnCommandName.Value = "Edit Date";
                dgSelectedChild.Rows[selIndex].BackColor = Color.FromName("#D8D8D8");
                DropDownList DDlEventDate = ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDates") as DropDownList);
                ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubjects") as DropDownList).Visible = false;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("btnEdit") as Button).Visible = true;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnChangeDate") as Button).Visible = true;
                ((Label)dgSelectedChild.Rows[selIndex].FindControl("lbOrg") as Label).Visible = true;

                HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdchild");
                HiddenField Grade = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("Hdgrade");
                HiddenField product = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdProductcode");
                HiddenField Fee = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdFee");
                string year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;

                hdnProductCode.Value = product.Value;
                hdnChildnumber.Value = Childnumber.Value;
                hdnGrade.Value = Grade.Value;
                hdnFee.Value = Fee.Value;
                hdnYear.Value = year;

                string teacherID = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string EventDate = string.Empty;
                string Year = string.Empty;
                string RegId = string.Empty;

                Year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;
                ProductID = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPgID") as Label).Text;
                ProductGroupID = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;
                ProductCode = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPrdCode") as Label).Text;
                ProductGroupCode = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblPgCode") as Label).Text;
                EventDate = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblEventDate") as Label).Text;
                RegId = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblRegId") as Label).Text;
                hdnRegId.Value = RegId;
                DateTime dtToday = new DateTime();
                dtToday = DateTime.Now;
                DateTime dtWorkshopDate = Convert.ToDateTime(EventDate);
                dtWorkshopDate = dtWorkshopDate.AddDays(0);
                if (Session["RoleID"] == null)
                {
                    if (dtToday >= dtWorkshopDate)
                    {
                        lblMsg.Text = "Time Deadline Crossed";
                        GrdChangeOptions.Visible = false;
                    }
                    else
                    {
                        PopulateEventDate(DDlEventDate);
                        populateChangeOptionsDate();
                        DeleteRegistration(hdnRegId.Value);
                    }
                }
                else
                {
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "97")
                    {
                        if (dtToday >= dtWorkshopDate)
                        {
                            PopulateEventDate(DDlEventDate);
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmMsgToUpdate('date');", true);
                        }
                        else
                        {
                            PopulateEventDate(DDlEventDate);
                            populateChangeOptionsDate();
                            DeleteRegistration(hdnRegId.Value);
                        }

                    }
                    else
                    {
                        if (dtToday >= dtWorkshopDate)
                        {
                            lblMsg.Text = "Time Deadline Crossed";
                            GrdChangeOptions.Visible = false;
                        }
                        else
                        {
                            PopulateEventDate(DDlEventDate);
                            populateChangeOptionsDate();
                            DeleteRegistration(hdnRegId.Value);
                        }
                    }
                }
            }
            else if (e.CommandName == "Update DateSubject")
            {

                try
                {
                    if (hdnCommandName.Value == "Edit Subject")
                    {
                        DropDownList ddfromchaptercod = (DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubjects");
                        string str = string.Empty;

                        string productID = ddfromchaptercod.SelectedValue;

                        if (ddfromchaptercod.SelectedItem.Text != "[Select Subject]")
                        {
                            str = ddfromchaptercod.SelectedItem.Text;

                            HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdchild");
                            HiddenField Grade = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("Hdgrade");
                            HiddenField product = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdProductcode");
                            HiddenField Fee = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdFee");

                            string Year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;

                            hdnProductCode.Value = product.Value;
                            hdnChildnumber.Value = Childnumber.Value;
                            hdnGrade.Value = Grade.Value;
                            hdnFee.Value = Fee.Value;

                            string productcodeName = str;
                            string ProductVenue = str;

                            string cmdText = string.Empty;
                            cmdText = "select  OnlineWSCalID from OnlineWSCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))>cast(GETDATE() as date) and  productcode='" + productcodeName + "') or ((DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))=cast(GETDATE() as date)) and (Time>cast(GETDATE() as time))   and  productcode='" + productcodeName + "')";

                            int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            if (ObjValiddate != 0)
                            {
                                cmdText = "Update RWK set RWK.OnlineWSCalId=Owk.OnlineWSCalId, RWK.EventDate=Owk.Date, RWK.ProductGroupId=Owk.ProductGroupId, RWK.ProductGroupCode=Owk.ProductGroupCode, RWK.ProductCode=OWk.ProductCode, RWK.ProductID=Owk.ProductId,RWK.ModifiedBy=" + Session["LoginID"] + " , RWK.ModifyDate=GetDate() from  Registration_OnlineWkShop  RWK ,OnlineWSCal Owk where RWK.ChildNumber=" + hdnChildnumber.Value + " and RWK.ProductCode='" + hdnProductCode.Value + "' and RWK.EventYear=" + Year + " and Owk.OnlineWSCalId=" + ObjValiddate;
                                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                                Response.Write("<script>alert('Updated successfully')</script>");
                            }
                            else
                            {
                                Response.Write("<script>alert('Time Deadline Crossed ')</script>");
                            }
                            dgSelectedChild.EditIndex = -1;
                            Data();
                        }
                        dgSelectedChild.EditIndex = -1;
                        Data();
                    }
                    else if (hdnCommandName.Value == "Edit Date")
                    {
                        DropDownList DDlEventDate = (DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDates");
                        string EventDate = string.Empty;
                        if (DDlEventDate.SelectedItem.Text != "[Select Subject]")
                        {
                            EventDate = DDlEventDate.SelectedValue;
                            HiddenField Childnumber = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdchild");
                            HiddenField Grade = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("Hdgrade");
                            HiddenField product = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdProductcode");
                            HiddenField Fee = (HiddenField)dgSelectedChild.Rows[selIndex].FindControl("hdFee");

                            hdnProductCode.Value = product.Value;
                            hdnChildnumber.Value = Childnumber.Value;
                            hdnGrade.Value = Grade.Value;
                            hdnFee.Value = Fee.Value;
                            string Year = ((Label)dgSelectedChild.Rows[selIndex].FindControl("lblYear") as Label).Text;
                            string cmdText = string.Empty;
                            cmdText = "select  OnlineWSCalID from OnlineWSCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))>cast(GETDATE() as date) and  productcode='" + hdnProductCode.Value + "') or ((DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))=cast(GETDATE() as date)) and (Time>cast(GETDATE() as time))   and  productcode='" + hdnProductCode.Value + "')";

                            int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                            //if (ObjValiddate != 0)
                            //{
                            //Commented by bindhu on Feb20,2016
                            cmdText = "Update Registration_OnlineWkShop set EventDate='" + EventDate + "' where ChildNumber=" + hdnChildnumber.Value + " and ProductCode='" + hdnProductCode.Value + "' and EventYear=" + Year + "";
                            // cmdText = "Update RWK set RWK.OnlineWSCalId=Owk.OnlineWSCalId, RWK.EventDate=Owk.Date, RWK.EventYear=Owk.EventYear, RWK.ProductGroupId=Owk.ProductGroupId, RWK.ProductGroupCode=Owk.ProductGroupCode, RWK.ProductCode=OWk.ProductCode, RWK.ProductID=Owk.ProductId from  Registration_OnlineWkShop  RWK ,OnlineWSCal Owk where RWK.ChildNumber=" + hdnChildnumber.Value + " and RWK.ProductCode='" + hdnProductCode.Value + "' and RWK.EventYear=" + Year + " and Owk.OnlineWSCalId=" + OnlineWSCalId;
                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                            Response.Write("<script>alert('Updated successfully')</script>");
                            //}
                            //else
                            //{
                            Response.Write("<script>alert('Time Deadline Crossed ')</script>");
                            //}
                            dgSelectedChild.EditIndex = -1;
                            Data();
                        }
                        dgSelectedChild.EditIndex = -1;
                        Data();
                    }
                }
                catch (Exception ex)
                {
                    //  Response.Write("Admin Error");
                }
            }
            else if (e.CommandName == "Cancel DateSubject")
            {
                ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDNewSubjects") as DropDownList).Visible = false;
                ((DropDownList)dgSelectedChild.Rows[selIndex].FindControl("DDlEventDates") as DropDownList).Visible = false;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnUpdateDate") as Button).Visible = false;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnCancelDate") as Button).Visible = false;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("btnEdit") as Button).Visible = true;
                ((Button)dgSelectedChild.Rows[selIndex].FindControl("BtnChangeDate") as Button).Visible = true;
                ((Label)dgSelectedChild.Rows[selIndex].FindControl("lbOrg") as Label).Visible = true;
            }
        }
        catch
        {
        }
    }

    protected void dgSelectedChild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && dgSelectedChild.EditIndex == e.Row.RowIndex)
            {
                DropDownList DDLSubject = ((DropDownList)e.Row.FindControl("DDNewSubject") as DropDownList);
                DropDownList DDlEventDate = ((DropDownList)e.Row.FindControl("DDlEventDate") as DropDownList);
                DropDownList DDlEventDates = ((DropDownList)e.Row.FindControl("DDlEventDates") as DropDownList);
                DropDownList DDNewSubjects = ((DropDownList)e.Row.FindControl("DDNewSubjects") as DropDownList);
                HiddenField Childnumber = ((HiddenField)e.Row.FindControl("hdchild") as HiddenField);
                HiddenField product = ((HiddenField)e.Row.FindControl("hdProductcode") as HiddenField);
                hdnChildnumber.Value = Childnumber.Value;
                hdnProductCode.Value = product.Value;
                PopulateSubject(DDLSubject);
                PopulateSubject(DDNewSubjects);
                PopulateEventDate(DDlEventDate);
                PopulateEventDate(DDlEventDates);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void populateChangeOptions()
    {
        string cmdText = string.Empty;
        cmdText = "select distinct E.productcode,E.ProductGroupID,E.ProductGroupCode,PC.EventYear,PC.Date,PC.Time,PC.TeacherID,E.ProductID,PC.Duration,IP.FirstName +' '+IP.LastName as TeamLead,P.name,PC.TeacherID,PC.OnlineWSCalId, PC.LateFeeDeadline from EventFees E  left join   OnlineWSCal Pc on  E.EventID=pc.EventID and E.ProductID=pc.ProductId inner join Indspouse IP on (IP.AutoMemberID=Pc.TeacherID)  inner join Product P on (E.ProductID=P.ProductID)  where  E.EventID=20 and E.EventYear=" + hdnYear.Value + " and E.ProductCode not in   (select productcode from Registration_OnlineWKShop where ChildNumber=" + hdnChildnumber.Value + " and EventYear=" + hdnYear.Value + ") and e.RegFee <=30.0000 and PC.EventYear= " + hdnYear.Value + "  and PC.Date>=GetDate() ";

        if (Convert.ToString(Session["entryToken"]) == "Parent")
        {
            cmdText = cmdText + " and (pc.RegistrationDeadline>=GETDATE())";
        }
        try
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    spnTable2Title.Visible = true;
                    GrdChangeOptions.Visible = true;
                    GrdChangeOptions.DataSource = ds;
                    GrdChangeOptions.DataBind();
                }
                else
                {
                    lblMsg.Text = "No record exists";
                    spnTable2Title.Visible = false;
                    GrdChangeOptions.DataSource = ds;
                    GrdChangeOptions.DataBind();
                }
            }
        }
        catch
        {
        }
    }

    public void populateChangeOptionsDate()
    {
        string cmdText = string.Empty;
        cmdText = "select distinct Convert(Varchar(10),Date,101) as Date,EventYear,Time,Duration,IP.FirstName +' '+IP.LastName as TeamLead,P.Name,OW.ProductGroupId, OW.ProductID,OW.ProductCode,OW.TeacherID,OW.ProductGroupCode,OW.OnlineWSCalId, OW.LateFeeDeadLine from OnlineWSCal OW inner join Product P on(P.ProductId=OW.ProductID) inner join IndSpouse IP on (IP.AutoMemberID=OW.TeacherID) where OW.Date>=GetDate() and EventYear=" + hdnYear.Value + " and OW.ProductCode='" + hdnProductCode.Value + "' and Date not in(select EventDate from Registration_OnlineWkshop where EventYear=" + hdnYear.Value + " and ChildNUmber=" + hdnChildnumber.Value + " and ProductCode='" + hdnProductCode.Value + "')";
        try
        {
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    spnTable2Title.Visible = true;
                    GrdChangeOptions.Visible = true;
                    GrdChangeOptions.DataSource = ds;
                    GrdChangeOptions.DataBind();
                }
                else
                {
                    lblMsg.Text = "No record exists";
                    spnTable2Title.Visible = false;
                    GrdChangeOptions.DataSource = ds;
                    GrdChangeOptions.DataBind();
                }
            }
        }
        catch
        {
        }
    }
    protected void GrdChangeOptions_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        GridViewRow row = null;
        GrdChangeOptions.PageIndex = 0;
        row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
        int selIndex = row.RowIndex;
        try
        {
            if (e.CommandName == "Select")
            {
                string teacherID = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string EventDate = string.Empty;
                string Year = string.Empty;
                string lateFeeDeadline = string.Empty;
                int OnlineWSCalId = 0;

                Year = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblYear") as Label).Text;
                ProductID = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblPrdID") as Label).Text;
                ProductGroupID = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblPgID") as Label).Text;
                ProductCode = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblPrdCode") as Label).Text;
                ProductGroupCode = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblPgCode") as Label).Text;
                teacherID = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblTeacherID") as Label).Text;
                EventDate = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblFormatedDate") as Label).Text;
                lateFeeDeadline = ((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblLateFeeDeadline") as Label).Text;
                OnlineWSCalId = Convert.ToInt32(((Label)GrdChangeOptions.Rows[selIndex].FindControl("lblWSCalId") as Label).Text);
                // Response.Write(OnlineWSCalId);

                DateTime dtToday = new DateTime();
                dtToday = DateTime.Now;

                DateTime dtWorkshopDate = Convert.ToDateTime(lateFeeDeadline);

                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "97")
                {
                    string cmdText = string.Empty;
                    cmdText = "select  OnlineWSCalID from OnlineWSCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))>cast(GETDATE() as date) and  productcode='" + ProductCode + "') or ((DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))=cast(GETDATE() as date)) and (Time>cast(GETDATE() as time))   and  productcode='" + ProductCode + "')";

                    int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                    //if (ObjValiddate > 0)
                    //{
                    // cmdText = "Update Registration_OnlineWkShop set  OnlineWSCalID= " + ObjValiddate + ",EventDate='" + EventDate + "', ProductID=" + ProductID + ", ProductCode='" + ProductCode + "',ProductGroupID=" + ProductGroupID + ",ProductGroupCode='" + ProductGroupCode + "', EventYear=" + Year + ", ModifiedBy=" + Session["LoginID"].ToString() + ", ModifyDate=GetDate() where ChildNumber=" + hdnChildnumber.Value + " and ProductCode='" + hdnProductCode.Value + "' and EventYear=" + Year + "";
                    cmdText = "Update RWK set RWK.OnlineWSCalId=Owk.OnlineWSCalId, RWK.EventDate=Owk.Date, RWK.EventYear=Owk.EventYear, RWK.ProductGroupId=Owk.ProductGroupId, RWK.ProductGroupCode=Owk.ProductGroupCode, RWK.ProductCode=OWk.ProductCode, RWK.ProductID=Owk.ProductId from  Registration_OnlineWkShop  RWK ,OnlineWSCal Owk where RWK.ChildNumber=" + hdnChildnumber.Value + " and RWK.ProductCode='" + hdnProductCode.Value + "' and RWK.EventYear=" + Year + " and Owk.OnlineWSCalId=" + OnlineWSCalId;
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                    lblMsg.Text = "Table 1 Updated successfully";
                    Data();


                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteRegistration();", true);
                }
                else
                {


                    if (dtWorkshopDate > dtToday)
                    {

                        string cmdText = string.Empty;
                        cmdText = "select  OnlineWSCalID from OnlineWSCal where (DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))>cast(GETDATE() as date) and  productcode='" + ProductCode + "') or ((DATEADD(dd, 0, DATEDIFF(dd, 0, RegistrationDeadLine))=cast(GETDATE() as date)) and (Time>cast(GETDATE() as time))   and  productcode='" + ProductCode + "')";

                        int ObjValiddate = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, cmdText));
                        //if (ObjValiddate > 0)
                        //{
                        // cmdText = "Update Registration_OnlineWkShop set  OnlineWSCalID= " + ObjValiddate + ",EventDate='" + EventDate + "', ProductID=" + ProductID + ", ProductCode='" + ProductCode + "',ProductGroupID=" + ProductGroupID + ",ProductGroupCode='" + ProductGroupCode + "', EventYear=" + Year + ", ModifiedBy=" + Session["LoginID"].ToString() + ", ModifyDate=GetDate() where ChildNumber=" + hdnChildnumber.Value + " and ProductCode='" + hdnProductCode.Value + "' and EventYear=" + Year + "";
                        cmdText = "Update RWK set RWK.OnlineWSCalId=Owk.OnlineWSCalId, RWK.EventDate=Owk.Date, RWK.EventYear=Owk.EventYear, RWK.ProductGroupId=Owk.ProductGroupId, RWK.ProductGroupCode=Owk.ProductGroupCode, RWK.ProductCode=OWk.ProductCode, RWK.ProductID=Owk.ProductId from  Registration_OnlineWkShop  RWK ,OnlineWSCal Owk where RWK.ChildNumber=" + hdnChildnumber.Value + " and RWK.ProductCode='" + hdnProductCode.Value + "' and RWK.EventYear=" + Year + " and Owk.OnlineWSCalId=" + OnlineWSCalId;
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                        lblMsg.Text = "Table 1 Updated successfully";
                        Data();


                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteRegistration();", true);

                    }
                    else
                    {
                        lblMsg.Text = "Registration/change deadline has passed for the workshop.";
                    }
                }
                //}
                //else
                //{
                //    lblMsg.Text = "Time Deadline Crossed";
                //}
            }

        }
        catch (Exception ex)
        {
            //  Response.Write("Error: " + ex.ToString());
        }
    }
    protected void btnConfirmChangeDate_Click(object sender, EventArgs e)
    {
        if (hdnCommandName.Value == "Edit Subject")
        {

            populateChangeOptions();
        }
        else if (hdnCommandName.Value == "Edit Date")
        {
            populateChangeOptionsDate();
            DeleteRegistration(hdnRegId.Value);
        }
    }

    #endregion
}