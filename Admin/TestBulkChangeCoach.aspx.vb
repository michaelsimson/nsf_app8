﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Net.Mail

Partial Class Admin_TestBulkChangeCoach
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("..\maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlYear.Items.Insert(0, Convert.ToString(year))
            ddlYear.Items.Insert(1, Convert.ToString(year + 1))
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByText(Convert.ToString(year)))
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then

                hlnkMainMenu.Text = "Back to Parent Functions"
                hlnkMainMenu.NavigateUrl = "../UserFunctions.aspx"
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("../login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "88") Then
                GetRoleName()
                FillMasterDetails()
            Else
                Response.Redirect("../maintest.aspx")
            End If
        End If
    End Sub

    Private Sub FillMasterDetails()

        Try
            ddlProductGroup.Items.Clear()
            ddlProduct.Items.Clear()
            ddlFromCoach.Items.Clear()
            ddlProductGroup.Enabled = False
            ddlProduct.Enabled = False
            ddlFromCoach.Enabled = False
            ClearCoachDetails()
            FillProductGroup()
            If ddlProductGroup.Items.Count <> 0 Then
                ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
            End If
            'FillProduct()
            'FillFromDetails()
            'FillFromPhaseDetails()
        Catch ex As Exception

        End Try
    End Sub

    Private Sub FillFromDetails()

        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.CMemberId as CMemberId,I.FirstName + ' ' + I.LastName as CoachName,I.firstname as firstname,I.lastname as lastname from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and C.Userid is not null and C.pwd is not null  order by lastname,firstname")

        If dsCoach.Tables(0).Rows.Count > 0 Then

            ddlFromCoach.DataSource = dsCoach.Tables(0)
            ddlFromCoach.DataBind()
            If ddlFromCoach.Items.Count > 0 Then
                ddlFromCoach.Items(0).Selected = True
                If ddlFromCoach.Items.Count = 1 Then
                    ddlFromCoach.Enabled = False
                Else
                    ddlFromCoach.Enabled = True
                End If

            End If

        End If
    End Sub

    Private Sub FillFromPhaseDetails()
        Dim dsPhase As DataSet
        dsPhase = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.Phase from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.CMemberId= " & ddlFromCoach.SelectedValue)
        ddlFromPhase.DataSource = dsPhase.Tables(0)
        ddlFromPhase.DataBind()
        If ddlFromPhase.Items.Count > 0 Then
            ddlFromPhase.Items(0).Selected = True
            ddlFromPhase_SelectedIndexChanged(ddlFromPhase, New EventArgs)
            If ddlFromPhase.Items.Count = 1 Then
                ddlFromPhase.Enabled = False
            Else
                ddlFromPhase.Enabled = True
            End If
        End If

    End Sub
    Private Sub FillFromLevelDetails()
        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.Level from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.CMemberId=" & ddlFromCoach.SelectedValue & " and CR.Phase = " & ddlFromPhase.SelectedValue)
        ddlFromLevel.DataSource = dsCoach.Tables(0)
        ddlFromLevel.DataBind()
        ddlFromLevel.Enabled = False
        If dsCoach.Tables(0).Rows.Count > 0 Then
            If ddlFromLevel.Items.Count > 0 Then
                ddlFromLevel.Items(0).Selected = True
                If ddlFromLevel.Items.Count > 1 Then
                    ddlFromLevel.Enabled = True
                End If
            End If
        End If

    End Sub

    Private Sub FillFromSessionDetails()
        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct CR.SessionNo  from coachreg CR inner join indspouse I on CR.CMemberID=I.AutoMemberID Inner join calsignup C on CR.CMemberId = C.MemberId Where CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.EventYear=" & ddlYear.SelectedValue & " and CR.CMemberId=" & ddlFromCoach.SelectedValue)
        ddlFromSession.DataSource = dsCoach.Tables(0)
        ddlFromSession.DataBind()
        ddlFromSession.Enabled = False
        If ddlFromSession.Items.Count > 0 Then
            ddlFromSession.Items(0).Selected = True

            If ddlFromSession.Items.Count > 1 Then
                ddlFromSession.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillFromDay()
        Dim dsDay As DataSet
        dsDay = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct C.Day from calsignup C where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.MemberId=" & ddlFromCoach.SelectedValue & " and C.Phase =" & ddlFromPhase.SelectedValue & " and C.Level='" & ddlFromLevel.SelectedValue & "' and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Accepted='Y' ")
        ddlFromDay.DataSource = dsDay.Tables(0)
        ddlFromDay.DataBind()
        ddlFromDay.Enabled = False
        If ddlFromDay.Items.Count > 0 Then
            ddlFromDay.Items(0).Selected = True

            If ddlFromDay.Items.Count > 1 Then
                ddlFromDay.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillFromTime()
        Dim dsTime As DataSet
        dsTime = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct C.Time from calsignup C where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.MemberId=" & ddlFromCoach.SelectedValue & " and C.Phase =" & ddlFromPhase.SelectedValue & " and C.Level='" & ddlFromLevel.SelectedValue & "' and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Accepted='Y' and C.Day='" & ddlFromDay.SelectedValue & "' ")
        ddlFromTime.DataSource = dsTime.Tables(0)
        ddlFromTime.DataBind()
        ddlFromTime.Enabled = False
        If ddlFromTime.Items.Count > 0 Then
            ddlFromTime.Items(0).Selected = True
            If ddlFromTime.Items.Count > 1 Then
                ddlFromTime.Enabled = True
            End If
        End If
    End Sub

    Private Sub FillToDetails()

        Dim dsCoach As DataSet
        dsCoach = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct C.MemberId,I.FirstName + ' ' + I.LastName as CoachName,I.firstname as firstname,I.lastname as lastname,C.SignUpId as SignUpId from CalSignUp C inner join indspouse I on C.MemberID=I.AutoMemberID Where C.ProductGroupId=" & ddlProductGroup.SelectedValue & " and C.ProductId=" & ddlProduct.SelectedValue & " and C.EventYear=" & ddlYear.SelectedValue & " and C.Phase=" & ddlFromPhase.SelectedValue & " and C.Level= '" & ddlFromLevel.SelectedValue & "' and C.SessionNo= " & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time = '" & ddlFromTime.SelectedValue & "' and C.Accepted='Y' and C.MemberId not in (" & ddlFromCoach.SelectedValue & ") order by lastname,firstname")
        ddlToCoach.DataSource = dsCoach.Tables(0)
        ddlToCoach.DataBind()
        ddlToCoach.Enabled = False

        If ddlToCoach.Items.Count > 0 Then
            ddlToCoach.Items(0).Selected = True
            If ddlToCoach.Items.Count > 1 Then
                ddlToCoach.Enabled = True
            End If
            lblToPhase.Text = ddlFromPhase.SelectedValue
            lblToLevel.Text = ddlFromLevel.SelectedValue
            lblToSessionNo.Text = ddlFromSession.SelectedValue
            lblToDay.Text = ddlFromDay.SelectedValue
            lblToTime.Text = ddlFromTime.SelectedValue
        End If

    End Sub



    Private Sub GetRoleName()
        Try
            Dim RoleCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select RoleCode from Volunteer Where RoleId=" & Session("RoleId") & " and MemberId= " & Session("LoginID"))
            If RoleCode <> "" Then
                lblRoleName.Text = RoleCode
            Else
                lblRoleName.Text = ""
            End If
        Catch ex As Exception

        End Try
    End Sub

    'Get Product Group details
    Private Sub FillProductGroup()

        Dim dsPrdGroup As DataSet
        If Session("RoleId").ToString() = "89" Or (Session("RoleId").ToString() = "88") Then
            dsPrdGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductGroupID, ProductGroupCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ") ' and EventYear =" & ddlYear.SelectedValue)
        Else
            dsPrdGroup = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductGroupID, ProductGroupCode from volunteer where Eventid=13 and ProductId is not Null and EventYear =" & ddlYear.SelectedValue)
        End If

        ddlProductGroup.DataSource = dsPrdGroup.Tables(0)
        ddlProductGroup.DataBind()

        lblError.Text = ""
        ddlProductGroup.Enabled = False

        If ddlProductGroup.Items.Count > 0 Then
            ddlProductGroup.Items(0).Selected = True
            If ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Enabled = True
            End If
        Else
            lblError.ForeColor = Color.Red
            lblError.Text = "No Eligible Coacher Available"
        End If

    End Sub

    'Get Product Details for the selected product group
    Private Sub FillProduct()
        Try

            Dim dsPrd As DataSet
            If Session("RoleId").ToString() = "89" Or (Session("RoleId").ToString() = "88") Then
                dsPrd = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductGroupId =" & ddlProductGroup.SelectedValue) ' & " and EventYear =" & ddlYear.SelectedValue)
            Else
                dsPrd = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select distinct ProductID,ProductCode from volunteer where Eventid=13 and ProductGroupId =" & ddlProductGroup.SelectedValue & " and EventYear =" & ddlYear.SelectedValue)
            End If

            ddlProduct.DataSource = dsPrd.Tables(0)
            ddlProduct.DataBind()
            lblError.Text = ""
            ddlProduct.Enabled = False
            If ddlProduct.Items.Count > 0 Then
                ddlProduct.Items(0).Selected = True
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Enabled = True
                End If
            Else
                lblError.ForeColor = Color.Red
                lblError.Text = "No Eligible Coacher Available"
            End If

        Catch ex As Exception

        End Try

    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged

        ddlFromCoach.Items.Clear()
        ClearCoachDetails()
        If ddlProductGroup.Items.Count <> 0 Then
            FillProduct()
            ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
        Else
            lblError.ForeColor = Color.Red
            lblError.Text = "No Eligible Product Available"
        End If

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged

        FillMasterDetails()
        Try
            If ddlProductGroup.Items.Count > 0 Then
                ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e)
            End If
        Catch ex As Exception
            ' Response.Write("Err :" & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlFromCoach_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromCoach.SelectedIndexChanged
        ClearCoachDetails()
        If ddlFromCoach.Items.Count <> 0 Then
            FillFromPhaseDetails()
            ddlFromPhase_SelectedIndexChanged(ddlFromPhase, New EventArgs)
        End If
    End Sub

    Protected Sub ddlFromPhase_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromPhase.SelectedIndexChanged
        If ddlFromCoach.Items.Count > 0 Then
            FillFromLevelDetails()
            ddlFromLevel_SelectedIndexChanged(ddlFromLevel, New EventArgs)
        End If


    End Sub

    Protected Sub ddlFromLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromLevel.SelectedIndexChanged
        If ddlFromLevel.Items.Count > 0 Then
            FillFromSessionDetails()
            ddlFromSession_SelectedIndexChanged(ddlFromSession, New EventArgs)
        End If

    End Sub

    Protected Sub ddlFromSession_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromSession.SelectedIndexChanged
        FillFromDay()
        ddlFromDay_SelectedIndexChanged(ddlFromDay, e)
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged

        ddlFromCoach.Items.Clear()
        ClearCoachDetails()
        If ddlProduct.Items.Count <> 0 Then
            FillFromDetails()

            ddlFromCoach_SelectedIndexChanged(ddlFromCoach, e)
        End If


    End Sub

    Protected Sub ddlFromDay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromDay.SelectedIndexChanged
        FillFromTime()
        ddlFromTime_SelectedIndexChanged(ddlFromTime, e)
    End Sub


    Private Sub ClearCoachDetails()
        'ddlFromCoach.Items.Clear()
        lblError.Text = ""
        lblMsg.Text = ""
        ddlToCoach.Items.Clear()
        ddlToCoach.Enabled = False

        ddlFromPhase.Items.Clear()
        ddlFromPhase.Enabled = False

        ddlFromLevel.Items.Clear()
        ddlFromLevel.Enabled = False

        ddlFromSession.Items.Clear()
        ddlFromSession.Enabled = False

        ddlFromDay.Items.Clear()
        ddlFromDay.Enabled = False

        ddlFromTime.Items.Clear()
        ddlFromTime.Enabled = False

        lblToPhase.Text = ""
        lblToLevel.Text = ""
        lblToSessionNo.Text = ""
        lblToDay.Text = ""
        lblToTime.Text = ""
    End Sub

    Private Sub ShowMessage(str As String, bErr As Boolean)
        lblError.Text = ""
        lblMsg.Text = ""
        If bErr Then
            lblError.Text = str
        Else
            lblMsg.Text = str
        End If

    End Sub

    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            Dim bIsValid As Boolean = True

            Dim fromCoachId As Integer, toCoachId As Integer
            fromCoachId = ddlFromCoach.SelectedValue
            toCoachId = ddlToCoach.SelectedValue

            Dim cmdText As String = ""
            cmdText = "select isnull([Duration],'') as Duration,isnull([Begin],'') as BeginTime,isnull([End],'') as EndTime,isnull(Cycle,0) as Cycle,Vroom,UserId,Pwd from CalSignUp where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Phase = " & ddlFromPhase.SelectedValue & " and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"

            Dim dr As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)

            Dim Duration As String = "", BeginTime As TimeSpan, EndTime As TimeSpan, Cycle As Integer = 0, Vroom As Integer = 0, UserID As String = "", PWD As String = ""

            While dr.Read()
                Duration = dr("Duration")
                BeginTime = dr("BeginTime")
                EndTime = dr("EndTime")
                Cycle = dr("Cycle")
                Vroom = dr("VRoom")
                UserID = dr("UserId")
                PWD = dr("pwd")
            End While

            '' Get from coach students list
            cmdText = "select distinct isnull(Ch.Email,'') as ChildEMail,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day , C.TIMe,C.level,C.ProductID, Ch.First_Name + ' ' + Ch.Last_Name as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,I2.AutoMemberId as ParentId FROM coachreg cr inner join IndSpouse I on cr.CMemberID=I.AutoMemberID INNER JOIN CalSignUp C ON c.memberid = cr.Cmemberid Inner JOIN Product P ON Cr.ProductID=P.ProductID  inner join Child Ch on Ch.Childnumber = cr.ChildNumber "
            cmdText = cmdText & " INNER JOIN IndSpouse I2 ON I2.AutoMemberID=cr.PMemberId WHERE c.MemberId =" & fromCoachId & "  And Cr.eventyear = " & ddlYear.SelectedValue & " and cr.productgroupid=" & ddlProductGroup.SelectedValue & " and cr.productid=" & ddlProduct.SelectedValue & " and C.Phase = " & ddlFromPhase.SelectedValue & " and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and cr.Approved='y'"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
            ViewState("StudentList") = ds.Tables(0)

            'Update in Coachreg table to change From memberid with To memberid
            cmdText = "UPDATE CoachReg SET ModifyDate=Getdate(), Modifiedby=" & Session("loginID") & ",CMEMBERID=" & toCoachId & " WHERE ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and CMEMBERID= " & fromCoachId & " and Phase = " & ddlFromPhase.SelectedValue & " and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Approved='y'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Update Calsignup --Update Details of ToCoach with From Coach 
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ",[Duration] = " & Duration & " , [Begin] ='" & BeginTime.ToString() & "', [End] ='" & EndTime.ToString() & "', Cycle=" & Cycle & ", vroom =" & Vroom & " ,[UserId]='" & UserID & "', Pwd ='" & PWD & "'  where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & toCoachId & " and Phase = " & ddlFromPhase.SelectedValue & " and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Update From coach Accepted =null 
            cmdText = "UPDATE CalSignUp SET ModifiedDate=Getdate(), Modifiedby= " & Session("loginID") & ", Accepted= null where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId= " & ddlProduct.SelectedValue & " and EventYear = " & ddlYear.SelectedValue & " and MEMBERID= " & fromCoachId & " and Phase = " & ddlFromPhase.SelectedValue & " and Level = '" & ddlFromLevel.SelectedValue & "' and SessionNo=" & ddlFromSession.SelectedValue & " and Day='" & ddlFromDay.SelectedValue & "' and Time='" & ddlFromTime.SelectedValue & "'"
            SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            'Send mail to parent(Father,Mother)/child/fromcoach/tocoach
            SendMailInfo(fromCoachId, toCoachId)

            'Insert changes on BulkChangeCoachLog Table
            LogCoachDetails(fromCoachId, toCoachId, ddlFromDay.SelectedValue, ddlFromTime.SelectedValue, Vroom, UserID, PWD)

            'Reset Page
            FillFromDetails()
            ddlFromCoach_SelectedIndexChanged(ddlFromCoach, New EventArgs)
            ddlFromCoach.SelectedValue = toCoachId
            ShowMessage("Successfully completed", False)

        Catch ex As Exception
            '   Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub ddlFromTime_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlFromTime.SelectedIndexChanged
        FillToDetails()
    End Sub

    Private Sub SendMailInfo(frmCoachID As Integer, toCoachId As Integer)

        'send emails to coachs & CoachAdmin
        Dim dr As SqlDataReader
        Dim fromCoachMail As String = "", toCoachMail As String = "", fromCoachName As String = "", cmdText, MailBody, MailBody1, MailBody2 As String, cmdParentQuery As String
        Dim CoachAdminEmail, subj As String, ProductCode As String, CoachMailBody As String = "", MailBody4 As String = "", MailBody3 As String = ""

        MailBody = ""
        subj = "A Coach is switching"
        CoachAdminEmail = ""

        'Get from coach mail
        cmdText = "select distinct I.Email as FromCoachmail,I.Firstname + ' ' + I.LastName as CoachName FROM CalSignUp C inner join IndSpouse I on  C.MemberID=I.AutoMemberID  Inner JOIN Product P ON C.ProductID=P.ProductID"
        cmdText = cmdText & "  WHERE C.MemberId = " & frmCoachID & " And C.eventyear = " & ddlYear.SelectedValue & " and C.productgroupid=" & ddlProductGroup.SelectedValue & " and C.productid=" & ddlProduct.SelectedValue & " and C.Phase = " & ddlFromPhase.SelectedValue & " and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and Accepted is null"

        dr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)
        If dr.Read Then
            fromCoachMail = dr("FromCoachmail").ToString()
            fromCoachName = dr("CoachName").ToString()
        End If

        ' Getting To Coach Mail
        cmdText = "select distinct I.Email ToCoachMail,C.SignUpID,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,C.Day as Day , C.TIMe,C.level,C.ProductID, Ch.First_Name + ' ' + Ch.Last_Name as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,I2.AutoMemberId as ParentId FROM coachreg cr inner join IndSpouse I on cr.CMemberID=I.AutoMemberID INNER JOIN CalSignUp C ON c.memberid = cr.Cmemberid Inner JOIN Product P ON Cr.ProductID=P.ProductID  inner join Child Ch on Ch.Childnumber = cr.ChildNumber "
        cmdText = cmdText & " INNER JOIN IndSpouse I2 ON I2.AutoMemberID=cr.PMemberId WHERE c.MemberId =" & toCoachId & "  And Cr.eventyear = " & ddlYear.SelectedValue & " and cr.productgroupid=" & ddlProductGroup.SelectedValue & " and cr.productid=" & ddlProduct.SelectedValue & " and C.Phase = " & ddlFromPhase.SelectedValue & " and C.level='" & ddlFromLevel.SelectedValue & "'  and C.SessionNo=" & ddlFromSession.SelectedValue & " and C.Day='" & ddlFromDay.SelectedValue & "' and C.Time='" & ddlFromTime.SelectedValue & "' and cr.Approved='y'"

        dr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdText)
        If dr.Read Then
            toCoachMail = dr("ToCoachMail").ToString()
        End If

        Dim dt As DataTable
        dt = ViewState("StudentList")

        MailBody3 = " Switching from: " & fromCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString() & "<br> To: "
        MailBody4 = toCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & ", Time : " & ddlFromTime.SelectedValue.ToString() & ",Switch Date : " & Now.ToString()

        Dim drStud As DataRow
        For Each drStud In dt.Rows
            If drStud("ChildName").ToString() <> "" Then
                ProductCode = drStud("ProductCode")
                MailBody2 = "<br>Student: " & drStud("ChildName") & ", Parent Name: " & drStud("ParentName") & ", Email: " & drStud("ParentEMail") & ", " & ProductCode & ","
                MailBody3 = " <br><br>Switching from: " & fromCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString() & "<br> To: "
                MailBody4 = toCoachMail & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & ", Time : " & ddlFromTime.SelectedValue.ToString() & ",Switch Date : " & Now.ToString()

                CoachMailBody = CoachMailBody & MailBody2

                ProductCode = drStud("ProductCode")

                ' Get father and mother mail ids
                cmdParentQuery = "select distinct EmailID as EmailID from(SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null)) and automemberid =" & drStud("ParentId") & ")) or "
                cmdParentQuery = cmdParentQuery & " (donortype = 'SPOUSE'  AND ValidEmailFlag is Null AND (newsletter not in ('2','3') OR (Newsletter is null))  and relationship =" & drStud("ParentId") & ") group by Email)as Email where (EmailID is not null and len(EmailID)<>'0' )"

                Dim drParent As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, cmdParentQuery)

                MailBody1 = "Dear Parent, <br><br>Note : Do not reply to the email above.<br>"
                MailBody = MailBody1 & MailBody2 & MailBody3 & MailBody4

                While drParent.Read
                    '' Send Mail to Each Parent (Father and Mother)
                    SendEmail(subj, MailBody, drParent("EmailId"))

                    Response.Write("<br> Parent :" & drParent("EmailId").ToString() & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
                    Response.Write("<br>==============================================")
                End While

                'Send Mail to Each Child
                If drStud("ChildEMail").ToString() <> "" Then

                    MailBody1 = "Dear Student, <br><br>Note : Do not reply to the email above.<br> "
                    MailBody = MailBody1 & MailBody2 & MailBody3 & MailBody4
                    SendEmail(subj, MailBody, drStud("ChildEMail"))

                    Response.Write("<br> Child :" & drStud("ChildEMail").ToString() & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
                    Response.Write("<br>==============================================")
                End If
            End If
        Next

        MailBody1 = "Dear Coach, <br><br>Note : Do not reply to the email above.<br>"
        Dim mailResign As String
        mailResign = "<br>Coach :" & fromCoachName & " resigned from " & ProductCode & ", Level: " & ddlFromLevel.SelectedValue.ToString() & ", CoachDay:" & ddlFromDay.SelectedValue.ToString() & " , Time : " & ddlFromTime.SelectedValue.ToString()

        MailBody = MailBody1 & mailResign ' & MailBody3 & MailBody4
        Response.Write("<br> From coach :" & fromCoachMail & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)

        'Send mail for resigned coach
        SendEmail(subj, MailBody, fromCoachMail)

        MailBody = MailBody1 & CoachMailBody & MailBody3 & MailBody4

        Response.Write("<br>==============================================")
        Response.Write("<br> To Coach :" & toCoachMail & "<br>Subject:" & subj & "<br> Body: <br>" & MailBody)
        Response.Write("<br>==============================================")

        ' Send mail to ToCoach
        SendEmail(subj, MailBody, toCoachMail)

    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Dim sFrom As String = "nsfcontests@gmail.com"
        ''Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        'Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        'Dim client As New SmtpClient()
        ''Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        ''client.Host = host
        'mail.IsBodyHtml = True
        'Dim ok As Boolean = True
        'Try
        '    client.Send(mail)
        'Catch e As Exception
        '    ok = False
        'End Try
    End Sub


    ' Log details in BulkChangeCoachLog Table
    Private Sub LogCoachDetails(fromCoachId As Integer, toCoachId As Integer, day As String, tme As String, vroom As Integer, uid As String, pwd As String)
        Dim cmdLog As String
        cmdLog = "INSERT INTO BulkChangeCoachLog (EventYear, FMemeberID, TMemberID, ProductGroupID, ProductGroupCode, ProductID, ProductCode, Phase, [Level], Session, Day, Time, Vroom, UserID, PWD, CreateDate, CreatedBy)"
        cmdLog = cmdLog & " Values( " & ddlYear.SelectedValue & " ," & fromCoachId & " ," & toCoachId & "," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlFromPhase.SelectedValue
        cmdLog = cmdLog & " , '" & ddlFromLevel.SelectedValue & "'," & ddlFromSession.SelectedValue & ", '" & day & "', '" & tme & "'," & vroom & ",'" & uid & "','" & pwd & "',GetDate()," & Session("LoginId") & ")"
        SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdLog)
    End Sub

End Class

