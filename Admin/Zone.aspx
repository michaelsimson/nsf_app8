
<%@ Page Language="C#" Theme="Default" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"  CodeFile="Zone.aspx.cs" Inherits="Zone" Title="Zone List" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
		
		<data:EntityGridView ID="GridView1" runat="server"			
			AutoGenerateColumns="False"					
			OnSelectedIndexChanged="GridView_SelectedIndexChanged"
			DataSourceID="ZoneDataSource"
			DataKeyNames="ZoneId"
			AllowMultiColumnSorting="false"
			DefaultSortColumnName="" 
			DefaultSortDirection="Ascending"	
			ExcelExportFileName="Export_Zone.xls"  
			AllowSorting="true"
			AllowPaging="true"			
			>
			<Columns>
				<asp:CommandField ShowSelectButton="True"  />				
				<asp:BoundField DataField="ZoneCode" HeaderText="ZoneCode" SortExpression="ZoneCode"  />
				<asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name"  />
				<asp:TemplateField HeaderText="Status" SortExpression="Status">
				<ItemTemplate>
						<asp:Repeater ID="DataValue5" runat="server" DataSourceID="StatusFilter5">
							<ItemTemplate>
								<%# Eval("DataText") %>
							</ItemTemplate>
						</asp:Repeater>

						<data:EntityDataSourceFilter ID="StatusFilter5" runat="server"
							DataSourceID="StatusDataSource5"
							Filter='<%# String.Format("DataValue = {0}", Eval("Status")) %>'
						/>
					</ItemTemplate>

				</asp:TemplateField>


				<asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description"  />
				<asp:BoundField DataField="CreateDate" HeaderText="CreateDate" SortExpression="CreateDate"  />
				<asp:BoundField DataField="CreatedBy" HeaderText="CreatedBy" SortExpression="CreatedBy"  />
				<asp:BoundField DataField="ModifyDate" HeaderText="ModifyDate" SortExpression="ModifyDate"  />
				<asp:BoundField DataField="ModifiedBy" HeaderText="ModifiedBy" SortExpression="ModifiedBy"  />
			</Columns>
			<EmptyDataTemplate>
				<b>No Zone Found!</b>
			</EmptyDataTemplate>
		</data:EntityGridView>
		<br />
		<asp:Button runat="server" ID="btnZone" OnClientClick="javascript:location.href='ZoneEdit.aspx'; return false;" Text="Add New"></asp:Button>
			
		<data:StatusDataSource ID="StatusDataSource1" runat="server"
			SelectMethod="GetAll"
		/>

		<data:ZoneDataSource ID="ZoneDataSource" runat="server"
			SelectMethod="GetPaged"
			EnablePaging="True"
			EnableSorting="True"
		>
			<Parameters>
				<data:CustomParameter Name="WhereClause" Value="" ConvertEmptyStringToNull="false" />
				<data:CustomParameter Name="OrderByClause" Value="" ConvertEmptyStringToNull="false" />
				<asp:ControlParameter Name="PageIndex" ControlID="GridView1" PropertyName="PageIndex" Type="Int32" />
				<asp:ControlParameter Name="PageSize" ControlID="GridView1" PropertyName="PageSize" Type="Int32" />
				<data:CustomParameter Name="RecordCount" Value="0" Type="Int32" />
			</Parameters>
		</data:ZoneDataSource>
	    <data:StatusDataSource ID="StatusDataSource5" runat="server"
			SelectMethod="GetByTableID"
		>
			<Parameters>
 
			<data:DataParameter Name="TableID" Type="int32" DefaultValue='3' DataSourceID="LookUpCodesDataSource1" />
			</Parameters>
       </data:StatusDataSource>
  	   <data:LookUpCodesDataSource ID="LookUpCodesDataSource1" runat="server"
 				SelectMethod="GetAll" 
 	    >
		</data:LookUpCodesDataSource>
	    		
</asp:Content>




 

 
 
 