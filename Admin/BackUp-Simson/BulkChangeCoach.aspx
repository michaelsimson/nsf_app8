﻿<%@ Page Language="VB" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="false" CodeFile="BulkChangeCoach.aspx.vb" Inherits="Admin_BulkChangeCoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript">

        function Validate() {

            var err = document.getElementById('_ctl0_Content_main_lblError');
            var yr = document.getElementById('_ctl0_Content_main_ddlYear');
            var prdgrp = document.getElementById('_ctl0_Content_main_ddlProductGroup');
            var prd = document.getElementById('_ctl0_Content_main_ddlProduct');
            var fromCoach = document.getElementById('_ctl0_Content_main_ddlFromCoach');
            var fromPhase = document.getElementById('_ctl0_Content_main_ddlFromPhase');
            var fromLevel = document.getElementById('_ctl0_Content_main_ddlFromLevel');
            var fromSession = document.getElementById('_ctl0_Content_main_ddlFromSession');
            var fromDay = document.getElementById('_ctl0_Content_main_ddlFromDay');
            var fromTime = document.getElementById('_ctl0_Content_main_ddlFromTime');
            var toCoach = document.getElementById('_ctl0_Content_main_ddlToCoach');

            if (prdgrp.value == '') {
                err.innerHTML = 'Select Product Group'; return false;
            }
            else if (prd.value == '') {
                err.innerHTML = 'Select Product'; return false;
            }
            else if (fromCoach.value == '') {
                err.innerHTML = 'Select From Coach'; return false;
            }
            else if (fromPhase.value == '') {
                err.innerHTML = 'Select Semester'; return false;
            }
            else if (fromLevel.value = '') {
                err.innerHTML = 'Select Level'; return false;
            }
            else if (fromSession.value == '') {
                err.innerHTML = 'Select Session'; return false;
            }
            else if (fromDay.value == '') {
                err.innerHTML = 'Select Day'; return false;
            }
            else if (fromTime.value == '') {
                err.innerHTML = 'Select Time'; return false;
            }
            else if (toCoach.value == '') {
                err.innerHTML = 'Select To Coach'; return false;
            }
            else {
                return ConfirmToCoach();
            }
        }
        function ConfirmToCoach() {
            if (confirm('Are you sure the Coach in the TO field is absolutely correct?'))
                return ConfirmSave();
            else
                return false;
        }
        function ConfirmSave() {
            return confirm('Do you want to switch?');
        }

        function ConfirmTransfer(totCount, ToCount) {
            var Message = "" + totCount + " students exceed the max capacity of " + ToCount + ". Do you still want to transfer?";

            if (confirm(Message)) {
                document.getElementById('<%= BtnBulkTransfer.ClientID%>').click();
            }
        }

    </script>
    <asp:Button ID="BtnBulkTransfer" runat="server" Text="Switch All Students" Style="display: none;" />
    <div>
        <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="900px" runat="server">
            <tr>
                <td></td>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1>Bulk Change for a Coach</h1>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                </td>
            </tr>
            <tr>
                <td></td>
                <td>

                    <table style="width: 100%; border-style: solid; border-width: 1px;" cellspacing="0" cellpadding="0" class="tableclass">
                        <tr style="text-align: center; font-weight: bold; background-color: #ffffcc;">
                            <th style="width: 25%">Role </th>
                            <th style="width: 25%">Year</th>
                            <th style="width: 25%">Semester</th>
                            <th style="width: 25%">ProductGroup</th>
                            <th style="width: 25%">Product</th>
                        </tr>
                        <tr style="text-align: center; font-weight: bold">
                            <td>
                                <asp:Label ID="lblRoleName" runat="server" Text=""></asp:Label></td>
                            <td>
                                <asp:DropDownList ID="ddlYear" runat="server" Width="85px" AutoPostBack="True"></asp:DropDownList></td>
                            <td>
                                <asp:DropDownList ID="DDlSemester" runat="server" Width="85px" AutoPostBack="True" OnSelectedIndexChanged="DDlSemester_SelectedIndexChanged"></asp:DropDownList></td>
                            <td>
                                <asp:DropDownList ID="ddlProductGroup" runat="server" DataTextField="ProductGroupCode" DataValueField="ProductGroupId" AutoPostBack="True" Width="150px"></asp:DropDownList></td>
                            <td>
                                <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="ProductCode" DataValueField="ProductId" Width="150px" AutoPostBack="True"></asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>

                    <br />
                    <div style="width: 100%; text-align: center; height: 30px">
                        <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        <asp:Label ID="lblMsg" runat="server" ForeColor="Blue"></asp:Label>
                    </div>

                    <div>
                        <table width="100%" class="tableclass" cellspacing="0" cellpadding="0">
                            <tr style="text-align: center; background-color: #ffffcc;">
                                <th></th>
                                <th>Coach</th>
                                <th>Semester</th>
                                <th>Level</th>
                                <th>Session</th>
                                <th>Day</th>
                                <th>Time</th>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="text-align: center">
                                <th style="text-align: left">From</th>
                                <td>
                                    <asp:DropDownList ID="ddlFromCoach" runat="server" AutoPostBack="True" DataTextField="CoachName" DataValueField="CMemberId" Width="250px"></asp:DropDownList></td>
                                <td>
                                    <asp:DropDownList ID="ddlFromPhase" runat="server" DataTextField="Semester" DataValueField="Semester" AutoPostBack="True" Width="50px"></asp:DropDownList></td>
                                <td>
                                    <asp:DropDownList ID="ddlFromLevel" runat="server" DataTextField="Level" DataValueField="Level" AutoPostBack="True" Width="150px"></asp:DropDownList></td>
                                <td>
                                    <asp:DropDownList ID="ddlFromSession" runat="server" DataTextField="SessionNo" DataValueField="SessionNo" AutoPostBack="True" Width="50px"></asp:DropDownList></td>
                                <td>
                                    <asp:DropDownList ID="ddlFromDay" runat="server" DataTextField="Day" DataValueField="Day" AutoPostBack="True" Width="100px"></asp:DropDownList></td>
                                <td>
                                    <asp:DropDownList ID="ddlFromTime" runat="server" DataTextField="Time" DataValueField="Time" Width="75px" AutoPostBack="True"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td>&nbsp;</td>
                            </tr>
                            <tr style="text-align: center">
                                <th style="text-align: left">To</th>
                                <td>
                                    <asp:DropDownList ID="ddlToCoach" runat="server" DataTextField="CoachName" DataValueField="MemberId" Width="250px" AutoPostBack="true"></asp:DropDownList></td>
                                <td>
                                    <asp:Label ID="lblToPhase" runat="server" Width="50px" Style="border: medium"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblToLevel" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblToSessionNo" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblToDay" runat="server"></asp:Label></td>
                                <td>
                                    <asp:Label ID="lblToTime" runat="server"></asp:Label></td>
                            </tr>

                            <%-- <td><asp:DropDownList ID="ddlToPhase" runat="server" DataTextField="Semester" DataValueField ="Semester" Width="50px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToLevel" runat="server" DataTextField="Level" DataValueField ="Level" Width="150px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToSession" runat="server" DataTextField="Session" DataValueField ="Session" Width="50px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToDay" runat="server" DataTextField="Day" DataValueField ="MemberId" Width="100px"></asp:DropDownList></td>
                                <td><asp:DropDownList ID="ddlToTime" runat="server" DataTextField="Time" DataValueField ="MemberId" Width="75px"></asp:DropDownList></td>--%>
                        </table>
                        <div style="clear: both;"></div>
                        <table width="50%" class="tableclass" id="tblSessionCreation" visible="true" runat="server" cellspacing="0" cellpadding="0" style="float: right;">
                            <tr id="trWebExEntry" runat="server" visible="false" style="text-align: center; color: green; font-weight: bold;">
                                <td align="left" nowrap="nowrap" id="Td5" runat="server">Meeting Password </td>
                                <td style="width: 100px" runat="server" align="left" id="Td6">
                                    <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server" Width="100" Wrap="False"></asp:TextBox>

                                </td>
                                <td align="left" nowrap="nowrap" id="Td3" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Zone </td>
                                <td style="width: 100px" runat="server" align="left" id="Td4">
                                    <asp:DropDownList ID="ddlTimeZone" runat="server" Width="110px"
                                        AutoPostBack="True">

                                        <asp:ListItem Value="4" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                                        <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                                        <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                                        <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                            </tr>
                        </table>
                    </div>

                </td>

            </tr>

            <tr>


                <td colspan="2" align="center">
                    <br />
                    <asp:Button ID="btnUpdate" runat="server" Text="Switch All Students" OnClientClick="return Validate();" /><br />
                </td>
            </tr>
        </table>

        <div runat="server" id="divFromStudents">

            <center>
                <table width="100%" runat="server" id="table1">
                    <tr>
                        <td width="70%" style="text-align: right">

                            <b>Table 1: From Coach (<asp:Label ID="lblFromCoachName" runat="server"></asp:Label>) Students List</b>

                        </td>
                        <td width="30%" style="text-align: right">
                            <asp:Button ID="btnExport" runat="server" Text="Export All" Style="text-align: right" /></td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <div style="overflow: scroll; width: 900px;" runat="server" id="divFrom">
                                    <asp:GridView ID="gvFrom" runat="server"></asp:GridView>
                                </div>
                                <asp:Label ID="lblFromErr" runat="server" Text="There is no student for this coach." ForeColor="red"></asp:Label></center>
                        </td>
                    </tr>
                </table>
            </center>
        </div>

        <div runat="server" id="divToStudents">
            <center>
                <table width="100%" runat="server" id="table2">
                    <tr>
                        <td width="70%" style="text-align: right">
                            <b>Table 2: To Coach (<asp:Label ID="lblToCoachName" runat="server"></asp:Label>) Students List</b>
                        </td>
                        <td width="30%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <div style="overflow: scroll; width: 900px;" runat="server" id="divTo">
                                    <asp:GridView ID="gvTo" runat="server"></asp:GridView>
                                </div>
                                <asp:Label ID="lblToErr" runat="server" Text="There is no student for this coach." ForeColor="red"></asp:Label></center>
                        </td>
                    </tr>
                </table>
            </center>
        </div>
        <div runat="server" id="divStudList">
            <center>
                <table width="100%">
                    <tr>
                        <td width="70%" style="text-align: right">
                            <b>Table 3: To Coach All Students List</b>
                        </td>
                        <td width="30%">&nbsp;</td>
                    </tr>
                    <tr>
                        <td colspan="2">
                            <center>
                                <div style="overflow: scroll; width: 900px;" runat="server" id="div2">
                                    <asp:GridView ID="gvStudList" runat="server"></asp:GridView>
                                </div>
                                <asp:Label ID="lblStudList" runat="server" Text="There is no student for this coach." ForeColor="red"></asp:Label></center>
                        </td>
                    </tr>
                </table>
            </center>
        </div>

    </div>
    <input type="hidden" value="0" id="hdnMeetingStatus" runat="server" />
    <input type="hidden" value="0" id="hdnMeetingAttendeeID" runat="server" />
    <input type="hidden" value="0" id="hdnAttendeeRegisteredID" runat="server" />
    <input type="hidden" value="0" id="hdnAttendeename" runat="server" />
    <input type="hidden" value="0" id="hdnAttendeeEmail" runat="server" />
    <input type="hidden" value="0" id="hdnCity" runat="server" />
    <input type="hidden" value="0" id="hdnState" runat="server" />
    <input type="hidden" value="0" id="hdnWebExID" runat="server" />
    <input type="hidden" value="0" id="hdnWebExPWD" runat="server" />
    <input type="hidden" value="0" id="hdnMeetingURL" runat="server" />

    <input type="hidden" id="hdnToCoachMeetingKey" runat="server" value="0" />
    <input type="hidden" id="hdnFromCoachMeetingKey" runat="server" value="0" />
    <input type="hidden" id="hdnFromCoachWebExID" runat="server" value="0" />
    <input type="hidden" id="hdnFromCoachWebExPwd" runat="server" value="0" />
    <input type="hidden" id="hdnToCoachWebExID" runat="server" value="0" />
    <input type="hidden" id="hdnToCoachWebExPwd" runat="server" value="0" />
    <input type="hidden" id="hdnAttendeeRegistrationKey" runat="server" value="0" />
    <input type="hidden" id="hdnToCoachMeetingPwd" runat="server" value="0" />
    <input type="hidden" id="hdnSessionKey" runat="server" value="0" />

    <input type="hidden" value="0" id="hdnHostMeetingURL" runat="server" />
    <input type="hidden" value="0" id="hdnIsCapacityExceed" runat="server" />
    <input type="hidden" value="0" id="hdnTrainingSessionKey" runat="server" />


</asp:Content>
