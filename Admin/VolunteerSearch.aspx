<%@ Page Language="C#"  MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="VolunteerSearch.aspx.cs" Inherits="Admin_VolunteerSearch" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server"><div><br /><br />
    <br />
   <asp:Label ID="Label2" runat="server" Font-Bold="True" Font-Size="Medium" Style="z-index: 104;
            left: 263px; position: absolute; top: 136px" Text="Search Records"></asp:Label>
        <br /> <table border="0" style="z-index: 103; left: 265px; width: 627px; position: absolute;
            top: 229px; height: 232px">
            <tr>
                <td style="width: 221px">
                    E-Mail:</td>
                <td>
                    <asp:TextBox ID="tbEmail" runat="server"></asp:TextBox></td>
                <td>
                    First Name/Organization:</td>
                <td style="width: 2px">
                    <asp:TextBox ID="tbFnameOrg" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 221px">
                    Last Name:</td>
                <td>
                    <asp:TextBox ID="tbLname" runat="server"></asp:TextBox></td>
                <td>
                    Street:</td>
                <td style="width: 2px">
                    <asp:TextBox ID="tbStreet" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 38px; width: 221px;">
                    City:</td>
                <td style="height: 38px">
                    <asp:TextBox ID="tbCity" runat="server"></asp:TextBox></td>
                <td style="height: 38px">
                    State:</td>
                <td style="width: 2px; height: 38px">
                    <asp:DropDownList ID="ddlState" runat="server" AppendDataBoundItems="True" DataSourceID="StateDS" DataTextField="Name" DataValueField="StateCode" OnSelectedIndexChanged="ddlState_SelectedIndexChanged">
                        <asp:ListItem>Select State</asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="StateDS" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="StateNamesTableAdapters.StateMasterTableAdapter">
                    </asp:ObjectDataSource>
                    <asp:TextBox ID="tbOther" runat="server" Width="53px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 34px; width: 221px;">
                    Zip Code:</td>
                <td style="height: 34px">
                    <asp:TextBox ID="tbZip" runat="server"></asp:TextBox></td>
                <td style="height: 34px">
                    Home Phone:</td>
                <td style="width: 2px; height: 34px">
                    <asp:TextBox ID="tbPhone" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 34px; width: 221px;">
                    Referred by:</td>
                <td style="height: 34px">
                    <asp:TextBox ID="tbRef" runat="server"></asp:TextBox></td>
                <td style="height: 34px">
                    Liaison Person:
                </td>
                <td style="width: 2px; height: 34px">
                    <asp:TextBox ID="tbLiaison" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="height: 34px; width: 221px;">
                    NSF Chapter</td>
                <td style="height: 34px">
                    <asp:DropDownList ID="ddlChapter" runat="server" DataSourceID="Chapter" DataTextField="ChapterCode" DataValueField="ChapterID" AppendDataBoundItems="True" OnSelectedIndexChanged="ddlChapter_SelectedIndexChanged" >
                    <asp:ListItem Text="[Select Chapter]"></asp:ListItem>
                    </asp:DropDownList><asp:ObjectDataSource ID="Chapter" runat="server" OldValuesParameterFormatString="original_{0}"
                        SelectMethod="GetData" TypeName="ChapterNamesTableAdapters.ChapterTableAdapter">
                    </asp:ObjectDataSource>
                </td>
                <td style="height: 34px">
                </td>
                <td style="width: 2px; height: 34px">
                </td>
            </tr>
        </table><br />
        <asp:Label ID="Label1" runat="server" Text="Search by one or maore attributes given below" style="z-index: 101; left: 266px; position: absolute; top: 171px"></asp:Label>
    
    </div>
        <asp:Button ID="btnSearch" runat="server" Height="55px" Style="z-index: 102; left: 338px;
            position: absolute; top: 479px" Text="Search" Width="196px" OnClick="btnSearch_Click" />
    <asp:Label ID="lblMessage" runat="server"></asp:Label>
    <asp:HyperLink ID="hlnkMenu" runat="server" NavigateUrl="~/VolunteerFunctions.aspx"
        Style="z-index: 104; left: 23px; position: absolute; top: 307px">Go back to Menu</asp:HyperLink>
</asp:Content>


 

 
 
 