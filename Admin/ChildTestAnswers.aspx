<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" ValidateRequest="false" EnableViewStateMac="false"
    CodeFile="ChildTestAnswers.aspx.cs" Inherits="ChildTestAnswers" %>

<%--<asp:Content ID="Content2" ContentPlaceHolderID="Content_Head" runat="server">
 </asp:Content>--%>
<%--<%@ Register Assembly="AjaxControlToolkit" Namespace="AjaxControlToolkit" TagPrefix="asp" %>
--%>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="server">
    <%-- <asp:ScriptManager ID="ScriptManager1" runat="server">
</asp:ScriptManager>--%>

    <table cellspacing="0" cellpadding="0" width="100%" border="0">
        <tr id="trVol" runat="server" visible="false">
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlnkMainMenu" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <tr id="trParent" runat="server" visible="false">
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkParent" runat="server" NavigateUrl="UserFunctions.aspx">Back to Parent Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <tr id="trStudent" runat="server" visible="false">
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkStudent" runat="server" NavigateUrl="StudentFunctions.aspx">Back to Student Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
        <!-- COUNTDOWN TIMER -->
        <!-- This goes in the HEAD of the html file -->
        <script src="Scripts/jquery-1.4.1-vsdoc.js" type="text/javascript"></script>
        <script src="Scripts/jquery-1.4.1.js" type="text/javascript"></script>
        <script type="text/javascript">
        </script>

        <%-- <asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="_ctl0_Content_main" runat="server">
        --%><tr>
            <td valign="top">
                <table class="admin-tan-border" cellpadding="5px" width="100%" border="0">
                    <tr>
                        <td nowrap="nowrap" colspan="16" align="center">
                            <div style="float: inherit; color: Blue">
                                <h3>
                                    <asp:Literal ID="ltlPageHeader" runat="server" Text="Enter Your Answers"></asp:Literal></h3>
                            </div>
                            <div style="float: right;">
                                <table>
                                    <tr>
                                        <td width="100%" align="center">
                                            <span id="theTime" style="font-size: xx-large" runat="server"></span>
                                        </td>
                                    </tr>
                                </table>
                            </div>
                        </td>
                    </tr>
                    <tr runat="server">
                        <td align="center">
                            <table runat="server" width="100%" style="color: Gray; background-color: #f4f4f4">
                                <tr align="center">
                                    <td colspan="2" align="left" style="font-size: large; color: #6B33EE">Instructions to follow
                                        <br />
                                        <br />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">1.Make selections on the top row and press <b><font color="#6B33EE" size="2.5pt">Submit.</font></b></td>
                                    <td align="left">5.Enter your answer for each question.Enter answers with <b>no units</b>. Also no comma separators need to be entered.</td>
                                </tr>
                                <tr>
                                    <td align="left">2.Press <b><font color="#6B33EE" size="2.5pt">Select</font></b> on the appropriate week/row in table.</td>
                                    <td align="left">6.Press <b><font color="#6B33EE" size="2.5pt">Save Answers</font></b> button frequently to avoid losing data.</td>
                                </tr>
                                <tr>
                                    <td align="left">3.Press on <b><font color="#6B33EE" size="2.5pt">Start Exam</font></b> button.</td>
                                    <td align="left">7.Once you are done with all <b><font color="#6B33EE" size="2.5pt">Sections</font></b> and <b><font color="#6B33EE" size="2.5pt">Saved</font></b>, press <b><font color="#6B33EE" size="2.5pt">Submit for Grading.</font></b><br />
                                        Once submitted, you cannot change your answers.</td>
                                </tr>
                                <tr>
                                    <td align="left">4.Press on appropriate <b><font color="#6B33EE" size="2.5pt">Section</font></b> (among highlighted)</td>
                                    <td align="left">8.<b><font color="#6B33EE" size="2.5pt">Regrade</font></b> is only to correct grading errors, <b><font color="#6B33EE" size="2.5pt">not to change answers.</font></b></td>
                                </tr>
                            </table>
                        </td>

                    </tr>
                    <tr>
                        <td>
                            <table align="center" border="1">
                                <tr>
                                    <td width="45px"><b>Event:</b></td>
                                    <td width="95px">
                                        <asp:DropDownList ID="ddlEvent" runat="server">
                                            <asp:ListItem Value="13">Coaching</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td width="50px"><b>EventYear:</b></td>
                                    <td width="25px">
                                        <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server"></asp:DropDownList></td>
                                    <td id="td_ChildPar" runat="server" width="95px">
                                        <b>ChildName:</b></td>
                                    <td id="td_ddlChildPar" runat="server" width="95px">
                                        <asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="ChildName" DataValueField="ChildNumber" runat="server"></asp:DropDownList>
                                        <asp:HiddenField ID="hdnChGrade" runat="server" Visible="false" />
                                    </td>
                                    <td width="50px"><b>PaperType:</b></td>
                                    <td width="50px">
                                        <asp:DropDownList ID="ddlPaperType" runat="server">
                                            <asp:ListItem Value="-1">Select</asp:ListItem>
                                            <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                                            <asp:ListItem Value="PT">Pretest</asp:ListItem>
                                            <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                                            <asp:ListItem Value="FT">Final Test</asp:ListItem>
                                        </asp:DropDownList></td>
                                    <td width="50px"><b>ProductGroup:</b></td>
                                    <td width="75px">
                                        <asp:DropDownList ID="ddlProductGroup"
                                            DataTextField="Name" DataValueField="ProductGroupID"
                                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                                            AutoPostBack="true" Enabled="false" runat="server">
                                        </asp:DropDownList></td>
                                    <td width="50px"><b>Product:</b></td>
                                    <td width="75px">
                                        <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" Enabled="false" runat="server"
                                            OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                                        </asp:DropDownList></td>
                                    <td width="50px"><b>Level:</b></td>
                                    <td width="60px">
                                        <asp:DropDownList ID="ddlLevel" DataTextField="Level" DataValueField="Level" Enabled="false" runat="server"></asp:DropDownList></td>
                                    <%-- <td runat="server" Id="td_childVol" visible="false" width="100px" >
                  <b>ChildName</b></td><td id="td_ddlChildVol" runat="server" width="75px" visible="false">
                                <asp:DropDownList ID="ddlmemberID" runat="server" DataTextField="ChildName" DataValueField="ChildNumber" AppendDataBoundItems="true" AutoPostBack="true" 
                                    onselectedindexchanged="ddlmemberID_SelectedIndexChanged">
                                    <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="StudentIDSource" runat="server"></asp:SqlDataSource>
                            </td>--%>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="Btn_Submit" runat="server" Text="Submit" OnClick="Btn_Click" />&nbsp;
                             <asp:Button ID="BtnGradeCard" runat="server" Text="Grade Card"
                                 OnClick="BtnGradeCard_Click" />
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
                            <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                            <asp:HiddenField ID="hdnCoachPaperID" runat="server" />
                        </td>
                    </tr>

                    <tr>
                        <td align="center" colspan="2">
                            <table>
                                <tr>
                                    <td>
                                        <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                                            BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                                            OnItemCommand="DGCoachPapers_ItemCommand" OnItemDataBound="DGCoachPapers_ItemDataBound">
                                            <FooterStyle BackColor="#CCCCCC" />
                                            <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                                            <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                                            <ItemStyle BackColor="White" />
                                            <Columns>

                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:TemplateColumn>
                                                    <ItemTemplate>
                                                        <asp:Button ID="btnReGrade" runat="server" CommandName="Regrade" Text="Regrade" Enabled="false"></asp:Button>
                                                    </ItemTemplate>
                                                </asp:TemplateColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventCode" HeaderText="EventCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroupCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="ProductCode"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>
                                                <%-- <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QReleaseDate" HeaderText="QReleaseDate" Visible="false"></asp:BoundColumn>
                                                --%>
                                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QDeadlineDate" HeaderText="QDeadlineDate" Visible="false"></asp:BoundColumn>

                                            </Columns>

                                        </asp:DataGrid>
                                        <asp:HiddenField ID="hdnSections" runat="server" />
                                    </td>
                                </tr>


                                <%--<tr><td><b>Test Number</b></td><td><asp:DropDownList ID="ddlTestNumber" runat="server" DataTextField="CoachPaperID" DataValueField="CoachPaperID"
                                    AutoPostBack="true" OnSelectedIndexChanged="ddlTestNumber_SelectedIndexChanged"
                                    Width="80px" Enabled="false">
                                    <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                              
                            </td> </tr>--%>
                                <%--<tr>
                            <td id="tdChild" runat="server" Visible="false"><b>ChildName</b> 
                                <asp:DropDownList ID="ddlmemberID" runat="server" DataTextField="ChildName" DataValueField="ChildNumber"
                                    AppendDataBoundItems="true"  AutoPostBack="true" 
                                    onselectedindexchanged="ddlmemberID_SelectedIndexChanged">
                                    <asp:ListItem Text="-- Select --" Value="-1"></asp:ListItem>
                                </asp:DropDownList>
                                <asp:SqlDataSource ID="StudentIDSource" runat="server"></asp:SqlDataSource>
                            </td></tr>--%>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" visible="false" runat="server">
                            <h3 style="color: Blue; font-size: large; font-family: Sans-Serif">Section Timer:<span id="sectiontimer"></span></h3>
                        </td>
                    </tr>
                    <tr visible="false" runat="server">
                        <td>
                            <asp:Label ID="lblNote" runat="server" Text="Note**: Please select TestNumber and ChildName to Start Test" ForeColor="Green" Font-Bold="true"></asp:Label></td>
                    </tr>
                    <tr>
                        <td runat="server" align="center">
                            <asp:Panel ID="pnlsubmit" runat="server" Visible="false" BorderStyle="Inset" Width="45%" BackColor="White">
                                <table class="announcement_text" border="0" width="100%">
                                    <tr>
                                        <td align="center">
                                            <asp:Label ID="lblTotalCount" runat="server" ForeColor="Red" Font-Size="Large"></asp:Label></td>
                                    </tr>
                                    <tr>
                                        <td align="center" style="background-color: Silver; width: 100%">
                                            <h2 style="padding: 0px; margin-top: 0px;">
                                                <asp:Label ID="Label1" runat="server" Text="Confirm Submit:Are You Sure you want to Submit?"></asp:Label></h2>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                    </tr>
                                    <tr>
                                        <td align="center">
                                            <%-- <ContentTemplate>   
                                <asp:UpdatePanel runat="server" id="UpdatePanel1" > 
                                    <ContentTemplate> --%>
                                            <asp:Button ID="btnConfirmSubmit" runat="server" CausesValidation="False"
                                                CssClass="standard-text" OnClick="btnConfirmSubmit_Click" Text="Submit" />
                                            <%--OnClientClick="$('#MainContent_AllSections :input').removeAttr('disabled');return true;" --%>
                                            <%-- </ContentTemplate>
                               </asp:UpdatePanel> 
                           </ContentTemplate>--%>
                                            <asp:Button ID="BtnCancel" runat="server" CausesValidation="False"
                                                CssClass="standard-text" Text="Cancel" OnClick="BtnCancel_Click" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>&nbsp;
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Label ID="lblSuccess" CssClass="announcement_text" runat="server" Text="" ForeColor="Red" Font-Size="Medium" Font-Bold="false"></asp:Label></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <asp:Button ID="btnstart" Text="Start Exam" runat="server" Enabled="false" OnClick="btnstart_Click" />&nbsp;
                       <asp:Button ID="Btn_Save" runat="server" Text="Save Answers" Enabled="false" OnClick="Btn_Save_Click" />&nbsp;
                       <%--<asp:Button Id="Btn_Load" runat="server" Text="Load" Enabled="false" onclick="Btn_Load_Click" Visible="false" />--%></td>
                    </tr>
                    <tr id="TrTimers" visible="false" runat="server">
                        <td>
                            <table>
                                <tr>
                                    <td>Section1:
                                   
                                        <asp:Label ID="Timer1" runat="server"></asp:Label>
                                    </td>
                                    <td>Section2:
                                  
                                        <asp:Label ID="Timer2" runat="Server"></asp:Label>
                                    </td>
                                    <td>Section3:
                                    
                                        <asp:Label ID="Timer3" runat="Server"></asp:Label>
                                    </td>
                                    <td>Section4:
                                    
                                        <asp:Label ID="Timer4" runat="Server"></asp:Label>
                                    </td>
                                    <td>Section5:
                                    
                                        <asp:Label ID="Timer5" runat="Server"></asp:Label>
                                    </td>


                                </tr>
                            </table>
                        </td>


                    </tr>
                    <tr>
                        <td align="center">
                            <table runat="server" width="600px">
                                <tr>
                                    <td colspan="7" width="600px" align="right">
                                        <asp:Button ID="btnSubmit" Text="Submit for Grading" runat="server" Enabled="false" OnClick="btnSubmit_Click1" CausesValidation="False" />
                                    </td>
                                    <td colspan="2"></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Label ID="lblWrngmsg" runat="server" Text="To enter answers, select Section.  Press Save Answers button frequently to avoid losing data." ForeColor="Red" Visible="false"></asp:Label></td>
                    </tr>
                    <tr>
                        <td>
                            <asp:Button ID="btnsection1" runat="server" Text="Section1" Enabled="false" class="buttonClass" OnClick="btnsection1_click" />
                            <asp:Button ID="btnsection2" runat="server" Text="Section2" Enabled="false" class="buttonClass" OnClick="btnsection2_click" />
                            <asp:Button ID="btnsection3" runat="server" Text="Section3" Enabled="false" class="buttonClass" OnClick="btnsection3_click" />
                            <asp:Button ID="btnsection4" runat="server" Text="Section4" Enabled="false" class="buttonClass" OnClick="btnsection4_click" />
                            <asp:Button ID="btnsection5" runat="server" Text="Section5" Enabled="false" class="buttonClass" OnClick="btnsection5_click" />
                        </td>
                    </tr>
                    <tr valign="top" height="*">
                        <td colspan="2">
                            <asp:Panel ID="AllSections" runat="server">
                                <table>
                                    <tr>
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl1">
                                                <asp:GridView ID="GridView1" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSectionNumber,TestSetUpSectionsID,QuestionType"
                                                    OnRowDataBound="GridView1_RowDataBound"
                                                    OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber" />
                                                        <asp:TemplateField HeaderText="Section 1">

                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" RepeatDirection="Horizontal" runat="server">
                                                                    <%----%>
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />

                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel ID="pnl2" runat="server">
                                                <asp:GridView ID="GridView2" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowPaging="False" AllowSorting="True" BackColor="White"
                                                    BorderColor="#CCCCCC" BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView2_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 2">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl3">
                                                <asp:GridView ID="GridView3" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView3_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 3">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel runat="server" ID="pnl4">
                                                <asp:GridView ID="GridView4" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView4_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="true" />
                                                        <asp:TemplateField HeaderText="Section 4">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                        <td style="vertical-align: top">
                                            <asp:Panel ID="pnl5" runat="server">
                                                <asp:GridView ID="GridView5" runat="server" Width="100%" AutoGenerateColumns="False"
                                                    EmptyDataText="No records found" AllowSorting="True" BackColor="White" BorderColor="#CCCCCC"
                                                    BorderStyle="None" BorderWidth="1px" CellPadding="3" DataKeyNames="TestSetUpSectionsID,TestSectionNumber,QuestionType"
                                                    OnRowDataBound="GridView5_RowDataBound" OnSorting="GridView1_Sorting" Enabled="false">
                                                    <Columns>
                                                        <asp:BoundField DataField="QuestionNumber" HeaderText="QNo" SortExpression="QuestionNumber"
                                                            Visible="True" />
                                                        <asp:TemplateField HeaderText="Section 5">
                                                            <ItemTemplate>
                                                                <asp:TextBox ID="gvtxtCorrectAnswer" runat="server" TextMode="MultiLine" Width="140px"></asp:TextBox>
                                                                <asp:RadioButtonList ID="rblCorrectAnswer" runat="server" RepeatDirection="Horizontal">
                                                                    <asp:ListItem Text="A" Value="A"></asp:ListItem>
                                                                    <asp:ListItem Text="B" Value="B"></asp:ListItem>
                                                                    <asp:ListItem Text="C" Value="C"></asp:ListItem>
                                                                    <asp:ListItem Text="D" Value="D"></asp:ListItem>
                                                                    <asp:ListItem Text="E" Value="E"></asp:ListItem>
                                                                    <asp:ListItem Text="F" Value="F"></asp:ListItem>
                                                                    <asp:ListItem Text="G" Value="G"></asp:ListItem>
                                                                    <asp:ListItem Text="H" Value="H"></asp:ListItem>
                                                                    <asp:ListItem Text="I" Value="I"></asp:ListItem>
                                                                    <asp:ListItem Text="J" Value="J"></asp:ListItem>
                                                                </asp:RadioButtonList>
                                                            </ItemTemplate>
                                                        </asp:TemplateField>
                                                        <asp:BoundField DataField="QuestionType" HeaderText="QType" Visible="false" SortExpression="QuestionType" />
                                                    </Columns>
                                                    <FooterStyle BackColor="White" ForeColor="#000066" />
                                                    <HeaderStyle BackColor="#46A71C" Font-Bold="True" ForeColor="White" />
                                                    <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                                                    <RowStyle ForeColor="#000066" />
                                                    <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                                                </asp:GridView>
                                            </asp:Panel>
                                        </td>
                                    </tr>
                                </table>
                            </asp:Panel>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <asp:Button runat="server" ID="hiddenTargetControlForRequestDetails" Style="display: none" />
    <asp:Button runat="server" ID="hiddenTargetControlForSubmitExam" Style="display: none" />
    <%--  <asp:Panel ID="pnlsubmit" runat="server">
        <table class="announcement_text">
            <tr><td><h2 style="padding: 0px; margin-top: 0px">
                        <asp:Label ID="Label1" runat="server" Text="Confirm Submit(DoubleClickOnSubmitButton)"></asp:Label></h2>
                </td>
            </tr>
            <tr><td>&nbsp;</td></tr>
            <tr><td align="center">
                <ContentTemplate>   
                    <asp:UpdatePanel runat="server" id="UpdatePanel1" > 
                        <ContentTemplate> 
                            <asp:Button ID="Button5" runat="server" CausesValidation="False"  
                               CssClass="announcement_text" OnClick="btnSubmit_Click" Text="Submit" /><%--OnClientClick="$('#MainContent_AllSections :input').removeAttr('disabled');return true;" - -%>
                         </ContentTemplate>
                   </asp:UpdatePanel> 
               </ContentTemplate>
               <asp:Button ID="Button6" runat="server" CausesValidation="False" CssClass="standard-text" Text="Cancel" />
              </td>
            </tr>
            <tr>
                <td>
                    &nbsp;
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:modalpopupextender ID="pnlsubmitE" runat="server" 
        TargetControlID="btnSubmit" CancelControlID="Button6"  
        PopupControlID="pnlsubmit" BackgroundCssClass="modalBackground">
    </asp:modalpopupextender>--%>


    <script type="text/javascript">
        document.getElementById("_ctl0_Content_main_pnl1").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl2").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl3").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl4").style.display = "Block";
        document.getElementById("_ctl0_Content_main_pnl5").style.display = "Block";

    </script>
</asp:Content>
