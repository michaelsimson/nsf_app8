<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="TestSections.aspx.vb" Inherits="TestSections" Title="Test Sections" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="javascript" type="text/javascript">
        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('TestSectionsHelp.aspx?ID=' + ctl, 'Subject Help', settings);
            PopupWindow.focus();
        }

        function redirectToAnserkey() {
            var productGroupId = document.getElementById("<%=ddlProductGroup.ClientID%>").value;
            var productId = document.getElementById("<%=ddlProduct.ClientID%>").value;
            var level = document.getElementById("<%=ddlLevel.ClientID%>").value;
            var papertype = document.getElementById("<%=ddlPaperType.ClientID%>").value;
            var weekId = document.getElementById("<%=ddlWeekID.ClientID%>").value;

            var setnum = document.getElementById("<%=ddlSetNo.ClientID%>").value;
            var sections = document.getElementById("<%=ddlSections.ClientID%>").value;
            var semester = document.getElementById("<%=ddlSemester.ClientID%>").value;

            var url = "TestAnswerKey.aspx?pgId=" + productGroupId + "&pId=" + productId + "&level=" + level + "&pType=" + papertype + "&wkId=" + weekId + "&setNum=" + setnum + "&sec=" + sections + "&Sem=" + semester + "";

            document.getElementById("ancAnswerKey").href = url;

        }
    </script>
    <div style="text-align: left">
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;
        <a id="ancAnswerKey" target="_blank" class="btn_02" style="cursor: pointer;" onclick="redirectToAnserkey()">Answer Key</a>


    </div>
    <table cellpadding="2" cellspacing="0" border="0" width="1250px" style="display: none;">
        <tr>
            <td class="btn_02" align="center">
                <asp:Label ID="lblmsg" runat="server" ForeColor="Green" Visible="false" Text="Test Sections Setup"></asp:Label></td>
        </tr>
        <tr>
            <td></td>
        </tr>

    </table>
    <table border="1" width="100%" style="color: Gray; display: none;" cellpadding="3" cellspacing="0">
        <tr>
            <td width="110px"><b>EventYear:</b>
            </td>
            <td width="110px"><b>Event:</b>
                <asp:DropDownList ID="ddlEvent" runat="server">
                    <asp:ListItem Value="13">Coaching</asp:ListItem>
                </asp:DropDownList></td>
            <td width="110px"><b>Semester:</b>
            </td>
            <td width="160px"><b>ProductGroup:</b></td>
            <td width="180px"><b>Product:</b>
                <b>Level:</b>
            </td>
            <td width="140px"><b>PaperType:</b></td>
            <td width="100px"><b>WeekID:</b>
            </td>
            <td width="100px"><b>SetNum:</b>
            </td>
            <td width="100px"><b>Sections:</b>
            </td>
        </tr>
        <tr>
            <td colspan="10" align="center">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" /></td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvSeacrhCoachpapers" runat="server" style=" margin: auto; width: 1100px;" visible="true">
        <div style="margin-top: 10px; margin-left: 20px;">
            <center>
                <h2><span style="color: green;">Test Sections Setup</span></h2>
            </center>

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Eventyear"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroup"
                            DataTextField="Name" DataValueField="ProductGroupID"
                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                            AutoPostBack="true" Enabled="false" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProduct" DataTextField="Name" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" DataValueField="ProductID" runat="server" AutoPostBack="true" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 30px;">
                    <div style="float: left;">
                        <asp:Label ID="Label1" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlLevel" DataTextField="Name" DataValueField="Level" Enabled="false" runat="server" AutoPostBack="True" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">

                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label19" runat="server" Font-Bold="true" Text="Paper Type"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 3px;">
                        <asp:DropDownList ID="ddlPaperType" runat="server" Width="110">
                            <asp:ListItem Value="">Select</asp:ListItem>
                            <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                            <asp:ListItem Value="PT">Pretest</asp:ListItem>
                            <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                            <asp:ListItem Value="FT">Final Test</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 32px;">
                    <div style="float: left;">
                        <asp:Label ID="Label21" runat="server" Font-Bold="true">Week# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 23px;">
                        <asp:DropDownList ID="ddlWeekID" runat="server" AutoPostBack="True" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 32px; display:none;">
                    <div style="float: left;">
                        <asp:Label ID="Label2" runat="server" Font-Bold="true">Set# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 65px;">
                        <asp:DropDownList ID="ddlSetNo" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div id="Div1" style="float: left; margin-left: 30px;" runat="server">
                    <div style="float: left;">
                        <asp:Label ID="Label20" runat="server" Font-Bold="true" Text="Sections"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 43px;">
                        <asp:DropDownList ID="ddlSections" runat="server" Width="110">
                            <asp:ListItem Value="0">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>


                <div style="float: left; margin-left: 20px;">

                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnClearFilter" runat="server" Text="Submit" OnClick="btnClearFilter_Click" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnRep" runat="server" Text="Replicate" />
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:Label ID="lblValText" runat="server" ForeColor="Red"></asp:Label></center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <table id="tblCopy" runat="server" visible="false">
            <tr>
                <td align="left">Copy From:
                <asp:DropDownList ID="ddlCopyFrom" DataTextField="CoachPaperID" DataValueField="CoachPaperID" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="-1">Select PaperID</asp:ListItem>
                </asp:DropDownList>
                    CopyTo:<asp:DropDownList ID="ddlCopyTo" DataTextField="CoachPaperID" DataValueField="CoachPaperID" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="-1">Select PaperID</asp:ListItem>
                    </asp:DropDownList>
                    <asp:Button ID="btnContinue" runat="server" Text="Continue" />
                    &nbsp;&nbsp; 
                            <asp:Button ID="btnCancelCopy" runat="server" Text="Cancel" />
                    <asp:Label ID="lblCopyErr" runat="server" Text="" ForeColor="Red"></asp:Label><br />

                </td>
            </tr>
        </table>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center" id="tblTestSection" runat="server" visible="false">
        <tr>
            <td align="center" colspan="8">
                <table border="1">
                    <tr>
                        <td align="left">CoachPaperId</td>
                        <td align="left">
                            <asp:TextBox ID="txtTestNumber" runat="server" Enabled="false"></asp:TextBox>
                            <asp:HiddenField ID="hdnTestSetUpSectionsId" runat="server" />
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Level</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlLevel1" runat="server" Enabled="false"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">SectionNumber</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSectionNo" runat="server"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Subject</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlSubject" runat="server">
                                <%--  
                                              <asp:ListItem Value="">Select</asp:ListItem>
                                              <asp:ListItem Value="Math">Math</asp:ListItem>
                                              <asp:ListItem Value="CriticalReading">Critical Reading</asp:ListItem>
                                              <asp:ListItem Value="Writing">Writing</asp:ListItem>
                                              <asp:ListItem Value="Pre-Mathcounts">Pre-Mathcounts</asp:ListItem>
                                              <asp:ListItem Value="Mathcounts">Mathcounts</asp:ListItem>
                                --%>
                            </asp:DropDownList>&nbsp;
                                <a href="javascript:PopupPicker('Subject');">Help</a></td>
                    </tr>
                    <tr>
                        <td align="left">Section Time Limit</td>
                        <td align="left">
                            <asp:TextBox ID="txtTimeLimit" runat="server"></asp:TextBox>
                            minutes
                                               <asp:RangeValidator ControlToValidate="txtTimeLimit" ID="RangeValidator1" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left"># Of Questions</td>
                        <td align="left">
                            <asp:TextBox ID="txtNoOfQues" runat="server"></asp:TextBox>
                            <asp:RangeValidator ControlToValidate="txtNoOfQues" ID="RangeValidator2" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Question Type</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlQuestionType" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="">Select choice</asp:ListItem>
                                <asp:ListItem Value="RadioButton">Multiple choice</asp:ListItem>
                                <asp:ListItem Value="TextArea">Non-multiple choice</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left"># Of Choices</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlNoOfChoices" runat="server" Enabled="false"></asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Penalty</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlPenalty" runat="server" Enabled="false">
                                <asp:ListItem Value="Y">Yes</asp:ListItem>
                                <asp:ListItem Value="N">No</asp:ListItem>
                            </asp:DropDownList></td>
                    </tr>
                    <tr>
                        <td align="left">Question No. From</td>
                        <td align="left">
                            <asp:TextBox ID="txtQuesNoFrom" runat="server"></asp:TextBox>
                            <asp:RangeValidator ControlToValidate="txtQuesNoFrom" ID="RangeValidator3" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="left">Question No. To</td>
                        <td align="left">
                            <asp:TextBox ID="txtQuesNoTo" runat="server"></asp:TextBox>
                            <asp:RangeValidator ControlToValidate="txtQuesNoTo" ID="RangeValidator4" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                        </td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add" />
                            &nbsp;&nbsp;&nbsp; 
                                    <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Clear" /></td>
                    </tr>
                    <tr>
                        <td align="center" colspan="2">
                            <asp:Label ID="lblErr" ForeColor="Red" runat="server"></asp:Label>&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrd" runat="server" Text="" Visible="false"></asp:Label>
                <asp:Label ID="lblPrdGrp" runat="server" Text="" Visible="false"></asp:Label></td>
        </tr>
    </table>


    <table id="tblDGTestSection" runat="server" cellpadding="2" cellspacing="0" align="center" border="0">
        <tr>
            <td align="center">
                <asp:Label ID="lblTestSection" runat="server" Text="" Font-Bold="True" ForeColor="Green"></asp:Label>

                <asp:DataGrid ID="DGTestSection" runat="server" DataKeyField="TestSetUpSectionsID" AutoGenerateColumns="False"
                    CellPadding="4" BorderStyle="None" CellSpacing="2" ForeColor="Black"
                    OnItemCommand="DGTestSection_Itemcommand">
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedItemStyle BackColor="Green" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <HeaderStyle BackColor="#A69B00" ForeColor="Blue" />
                    <ItemStyle BackColor="White" />
                    <EditItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />

                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn DataField="TestSetUpSectionsID" HeaderText="ID" Visible="false" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionNumber" HeaderText="SectionNumber" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Subject" HeaderText="Subject" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionTimelimit" HeaderText="Timelimit" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfQuestions" HeaderText="#OfQuestions" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionType" HeaderText="QuestionType" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Penalty" HeaderText="Penalty" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfChoices" HeaderText="NumberOfChoices" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberFrom" HeaderText="QuestionNoFrom" />
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberTo" HeaderText="QuestionNumberTo" />

                    </Columns>
                </asp:DataGrid>
            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <table id="tblDGCoachPaper" runat="server" align="center" cellpadding="2" cellspacing="0" border="0">
        <tr>
            <td colspan="8" align="center">
                <asp:Label CssClass="btn02" ID="lblCoachPaper" runat="server" Text="" ForeColor="Brown" Font-Bold="true"></asp:Label>

                <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                    BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                    OnItemCommand="DGCoachPapers_ItemCommand">
                    <FooterStyle BackColor="#CCCCCC" />
                    <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                    <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                    <ItemStyle BackColor="White" />
                    <Columns>
                        <asp:TemplateColumn>
                            <ItemTemplate>
                                <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                <div style="display: none;">
                                    <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                    <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                </div>
                            </ItemTemplate>
                        </asp:TemplateColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestSetup" HeaderText="TestSetup"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="PaperID"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                        <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>

                    </Columns>

                </asp:DataGrid></td>
        </tr>
    </table>

</asp:Content>

