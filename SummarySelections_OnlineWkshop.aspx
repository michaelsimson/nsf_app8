<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SummarySelections_OnlineWkshop.aspx.vb" Inherits="SummarySelections_OnlineWkshop"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table width="100%" border="0">
				<tr>
					<td class="Heading"  align="center" colspan="2">
                         Summary of Your Online Workshops
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<%--<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>--%>
							<tr>
								<td><asp:label id="lblStateZip" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">Child/Children Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px"><asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="OnlineWSCalID"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblChildId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildId") %>' CssClass="SmallFont" Visible="false">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Online Workshops Selected" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblOnlineWS" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblProductId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>' CssClass="SmallFont" Visible ="false" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
									<asp:TemplateColumn HeaderText="LateFee" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblLateFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LateFee","{0:N2}") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="OnlineWS Date" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblEventDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.EventDate","{0:d}") %>' CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Time" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblTime" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Time") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Regsitration Deadline" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblRegDeadlineDate" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.RegistrationDeadline","{0:d}") %>' CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>
								
								
								
								<%--<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>' CssClass="SmallFont"></asp:Label>&nbsp;
										<asp:Label id="lblChapterId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterId") %>' CssClass="SmallFont" Visible ="false" ></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Location" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblLocation" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Venue") %>' CssClass="SmallFont"></asp:Label>&nbsp;
									</ItemTemplate>
								</asp:TemplateColumn>--%>
							</Columns>
						</asp:datagrid>
						</td>
                        
				</tr>
				
								<tr><td class="SmallFont" style="width:340px" align="right">
								Total Amount to be paid :
                        <asp:Label ID="lblTotalAmt" style="color:Red" runat="server" Text="Label" Width="118px"></asp:Label>
					</td>
					<td>&nbsp;</td>
				</tr>
				<%--<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>--%>
				<tr>
				<td colspan="2">&nbsp;</td>
				</tr>
				<tr>
					<td   colspan="2" class="SmallFont"  style="color:Red">
					Please verify your selections for each child along with the event date and time. 
					</td>
				</tr>
				<tr>
				<td   colspan="2" class="SmallFont" style="color:Red">
				Please visit www.northsouth.org for the latest information.  Once paid, your payment is non-refundable.
				</td>
				</tr>
				 <tr><td align="right"><asp:Label id="lblDiscount" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
				                             <asp:Label ID="lblTotalFee" runat="server" ForeColor="Green" Font-Bold="True"></asp:Label></td></tr>
						   <tr><td class="SmallFont">Your registration is 2/3rd tax-deductible and helps a poor child go to college in India.</td></tr>
							
				<tr>
				    <td>
				    <table align="center">
				            <tr>
				                <td>
				    	        <asp:CheckBox ID="ChkEmail" runat="server" Visible="true" /> 
                                    You will receive all communications to <asp:Label ID="lblCommuEmail" runat="Server" Text="" Font-Bold="True" ForeColor="Red"></asp:Label>. 
				                 <asp:LinkButton  class="btn_02" ID="hlinkEmail" runat="server">Change Email</asp:LinkButton>.(This will change your login too)
				                </td></tr>
				             <tr><td><asp:CheckBox ID="ChkDetails" runat="server" Visible="true"/> Change of Subject and Date depends on availability, no guarantee. Transfer to another child is not allowed. Unless paid, seat is not guaranteed.</td></tr>
				             <tr><td><asp:CheckBox ID="ChkCommEmail" runat="server" Visible="true"/> I note the Workshop Book can be downloaded from View Status link on the Parent Functions page (A few days before the class)</td></tr>
				     </table> 
				    </td>
				</tr> 
					<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Text ="" Visible="True" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblEmailInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="True" ></asp:Label></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
				<%--<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>--%>
			</table>
			<table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left" colspan="2">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
				</tr>
			</table>
    <input type="hidden" runat="server" id="hdnMemberId" value="0" />
</asp:Content>

