<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="ManageTestPapers.aspx.cs" Inherits="ManageTestPapers" Title="Upload Test Papers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="left" >
     	<asp:HyperLink ID="backToVolunteerFunctions"  CssClass="btn_02"  NavigateUrl="VolunteerFunctions.aspx" runat="server" >  Back to Volunteer Functions</asp:HyperLink>
   </div>   
   <div align="center">      
    <asp:DropDownList ID="dllfileChoice"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllfileChoice_SelectedIndexChanged"
         Visible="False">
    </asp:DropDownList>
     <asp:DropDownList ID="ddlFlrEvent" runat="server" OnSelectedIndexChanged="ddlFlrEvent_SelectedIndexChanged" AutoPostBack="true" Visible="false"></asp:DropDownList> 
     <asp:DropDownList ID="ddlEvent"  runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" Visible="false"></asp:DropDownList>
    </div>
    
    <div align="right" runat="server" id="divRepl" visible="false"><table><tr><td>
        <asp:Label ID="lblSuccessMsg" runat="server" ForeColor="#006600"></asp:Label>
    <asp:Label ID="lblReplErr" runat="server" Text="" ForeColor="Red"></asp:Label>
        <asp:Button ID="btnReplicate" runat="server" Text="Replicate Instruction Files" 
            onclick="btnReplicate_Click" /></td><td><asp:Label ID="lblMsg" runat="server" Text="ToYear"></asp:Label></td><td><asp:DropDownList ID="ddlReplYear" runat="server"></asp:DropDownList></td> </tr></table></div>
    <asp:Label ID="lblNoPermission" runat="server" ForeColor="Red" Visible="false"></asp:Label><br />
    &nbsp
    <asp:Panel ID="Panel1" runat="server" BorderWidth="1px" BorderColor="black" Height="525px" Width="100%" Visible="False">
        <asp:Label ID="Label8" runat="server" Text="Upload Test Papers" Font-Bold="True"
            Font-Size="Large"></asp:Label>        <br />    
        
        <table style="width: 100%;" border="0">
        <tr>
        <td width="40%">
        <table style="width: 50%;" border="0">
             <%--   <tr> <td style="width: 137px; height: 24px;" >
                     <asp:Label ID="Label15" runat="server" Text="EventID"></asp:Label>
                    </td>
                    <td style="width: 526px; height: 24px;">
                     <asp:DropDownList ID="ddlEvent" runat="server"  OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList></td> </tr>--%>
                <tr>
                    <td style="width: 137px; height: 24px;" >
                     <asp:Label ID="Label12" runat="server" Text="ContestYear"></asp:Label>
                    </td>
                    <td style="width: 526px; height: 24px;">
                     <asp:DropDownList ID="ddlContestYear" runat="server">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" runat="server"
                        ErrorMessage="  * Required ContestYear#" ControlToValidate="ddlContestYear" InitialValue="0"></asp:RequiredFieldValidator>

                    </td>
                </tr>
            <tr>
                <td style="width: 137px">
                    <asp:Label ID="Label1" runat="server" Text="Set#"></asp:Label></td>
                <td style="width: 526px">
                    <asp:DropDownList ID="ddlSet" runat="server"  AutoPostBack ="true"
                        onselectedindexchanged="ddlSet_SelectedIndexChanged" >
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" runat="server"
                        ErrorMessage="  * Required Set#" ControlToValidate="ddlSet" InitialValue="-1">
                    </asp:RequiredFieldValidator>
                 </td>
            </tr>        
            <tr>
                <td style="width: 137px">
                  <asp:Label ID="Label11" runat="server" Text="DocType"></asp:Label>
                    
                </td>
                <td style="width: 526px">
                     <asp:DropDownList ID="ddlDocType" runat="server" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged" AutoPostBack="true" >
                         <asp:ListItem Text="[Select Doc Type]" Value="-1"></asp:ListItem>
                         <asp:ListItem Text ="Instructions" Value="Instr" ></asp:ListItem>
                         <asp:ListItem Text ="Test Paper" Value="TestP" ></asp:ListItem>
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" runat="server"
                        ErrorMessage="  * Required Field" ControlToValidate="ddlDocType" InitialValue="-1"></asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 137px" >
                    <asp:Label ID="lblProductGroup" runat="server" Text="Product Group"></asp:Label></td>
                <td style="width: 526px">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                        AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server"
                        ErrorMessage="  * Required Product Group" ControlToValidate="ddlProductGroup" InitialValue="-1">
                    </asp:RequiredFieldValidator>
                 </td>
            </tr>
            <tr>
                <td style="height: 58px; width: 137px;">
                    <asp:Label ID="Label4" runat="server" Text="No. Of Contestants"></asp:Label></td>
                <td style="height: 58px; width: 526px;">
                    <asp:DropDownList ID="ddlNoOfContestants" runat="server" OnSelectedIndexChanged="ddlNoOfContestants_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                 </td>
            </tr>            
            <tr>
                <td style="width: 137px">
                    <asp:Label ID="Label2" runat="server" Text="Product Code" Width="87px"></asp:Label></td>
                <td style="width: 526px">
                    <asp:DropDownList ID="ddlProduct" runat="server" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" runat="server"
                        ErrorMessage="  * Required Product" ControlToValidate="ddlProduct" InitialValue="-1">
                    </asp:RequiredFieldValidator>
                 </td>

            </tr>
            
            
            <tr>
                <td style="height: 21px; width: 137px;">
                    <asp:Label ID="Label3" runat="server" Text="Week Of"></asp:Label></td>
                <td style="height: 21px; width: 526px;">
                    <asp:DropDownList ID="ddlWeek" runat="server"  AutoPostBack ="true"
                        onselectedindexchanged="ddlWeek_SelectedIndexChanged">
                    </asp:DropDownList> 
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" runat="server"
                     ErrorMessage="  * Required Week Of" ControlToValidate="ddlWeek" InitialValue="-1">
                    </asp:RequiredFieldValidator>
                 </td>
            </tr>

            <tr>
                <td style="width: 137px">
                    <asp:Label ID="Label5" runat="server" Text="FileName"></asp:Label></td>
                <td style="width: 326px">
                    <asp:FileUpload ID="FileUpLoad1" runat="server" Height="20px" Width="467px" />&nbsp;
                    <asp:Label ID="LabelExampleFormat" runat="server"   Font-Size=Small Text="Ex: Set2_2008_SB_JSB_Instr_05.zip (For Instructions), Set2_2008_SB_JSB_TestP_05.zip (For Test Paper)" Width="479px"></asp:Label>
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" runat="server"
                    ErrorMessage="  *Required File " ControlToValidate="FileUpLoad1">
                    </asp:RequiredFieldValidator>
                </td>
            </tr>
            <tr>
                <td style="width: 137px; height: 58px;">
                    <asp:Label ID="Label6" runat="server" Text="Description"></asp:Label></td>
                <td style="width: 526px; height: 58px;">
                    <asp:TextBox ID="txtDescription" Text="" runat="server" Height="75px" TextMode="MultiLine"
                    Width="517px"></asp:TextBox></td>
            </tr>
            <tr>
                <td style="width: 137px">
                    <asp:Label ID="Label9" runat="server" Text="Password"></asp:Label></td>
                <td style="width: 526px">
                    <asp:TextBox ID="TxtPassword" Text="" runat="server"></asp:TextBox><asp:Label ID="Label10" runat="server" EnableViewState="false" ForeColor="Green"> (Optional)</asp:Label>
                 </td>
            </tr>               
            <tr>
                <td style="width: 137px">
                </td>
                <td style="width: 526px">
                    <asp:Button ID="UploadBtn" Text="Upload File" OnClick="UploadBtn_Click" runat="server"
                        Width="105px" />
                    <asp:Label ID="lblMessage" runat="server" EnableViewState="false" ForeColor="Red"></asp:Label>&nbsp;
                                            		            <asp:hyperlink id="Hyperlink2" runat="server" Visible="false"  NavigateUrl="../2007_Finals/photo_instructions.asp" Target="_blank">Help</asp:hyperlink>

                    </td>
                   </tr>
        </table>
        </td>
            <td>
                <table style="width: 90%;" border="0">
               
            <tr>
                    <td style="width: 87px">
                    <asp:Label ID="lblReviewedBY" Text="ReviewedBy" runat="server"></asp:Label>
                    </td>
                    <td>
                       <asp:TextBox ID="txtReviewedBy" runat="server" />&nbsp;
                    </td>
             </tr>
                    
             <tr>
                     <td style="width: 87px">
                       <asp:Label ID="lblReviewDate" Text="ReviewedDate" runat="server"></asp:Label>
                    </td>
                    <td>
                        <asp:TextBox ID="txtReviewedDate" runat="server" />&nbsp;
                    </td>
                
             </tr>
             
             <tr>
                     <td style="width: 87px">
                     <asp:Label ID="lblModifiedBy" Text="ModifiedBy" runat="server"></asp:Label>
                    </td>
                    <td>
                      <asp:TextBox ID="txtModifiedBy" runat="server" />&nbsp;
                    </td>
             </tr>
             <tr>
                     <td style="width: 87px">
                     <asp:Label ID="lblModifiedDate" Text="ModifiedDate" runat="server"></asp:Label>
                    </td>
                    <td>
                       <asp:TextBox ID="txtModefiedDate" runat="server" />&nbsp;
                    </td>
             </tr>
             <tr>
             <td style="width: 87px">
                <asp:Label ID="lblStatusFlag" Text="StatusFlag" runat="server"></asp:Label>
             </td>
             <td>
              <asp:TextBox ID="txtStatusFlag" runat="server" />&nbsp;
             </td>
             </tr>
        </table>
        
            </td>
             </tr>
        </table>
    </asp:Panel>
   
    <asp:Panel ID="Panel5"  runat="server" BorderWidth="1px" BorderColor="black"  Width="100%" Visible="False">
       <div align="center" >
        <asp:Label ID="lblReplace" ForeColor="Red"  Text="This is a duplicate.  Do you want to replace the current file?" runat="server"></asp:Label><br />
        <asp:Button ID="btnYes" runat="server" Text="Yes" onclick="btnYes_Click" /> &nbsp;&nbsp;<asp:Button 
               ID="btnNo" runat="server" Text="No" onclick="btnNo_Click" />
        </div>
    </asp:Panel>
  
    <asp:Panel ID="Panel2" runat="server" Visible="False">
        <asp:Label ID="Label7" runat="server" Text="Filter/Download Test Papers" Width="249px" Font-Bold="True"
            Font-Size="Large"></asp:Label>
        <br />
        <table style="width: 100%" border="0" cellpadding="4" cellspacing="0" bordercolor="black">
            <tr>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Year</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Product Group Code</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Product Code</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Week Of</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Set#</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    #Of Children</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Test File Name</td>
                <td nowrap="nowrap" bgcolor="honeydew">
                    Description</td>
            </tr>
            <tr>
             <td style="width: 115px">
                    <asp:DropDownList ID="ddlFlrYear" runat="server"  AutoPostBack="true"
                        onselectedindexchanged="ddlFlrYear_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlFlrProductGroup" runat="server" OnSelectedIndexChanged="ddlFlrProductGroup_SelectedIndexChanged"
                        AutoPostBack="true"></asp:DropDownList></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlFlrProduct" runat="server">
                    </asp:DropDownList></td>
                <td style="width: 100px">
                    <asp:DropDownList ID="ddlFlrWeek" runat="server"  AutoPostBack ="true"
                        onselectedindexchanged="ddlFlrWeek_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td style="width: 88px">
                    <asp:DropDownList ID="ddlFlrSet" runat="server" AutoPostBack ="true"
                        onselectedindexchanged="ddlFlrSet_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td style="width: 115px">
                    <asp:DropDownList ID="ddlFlrNoOfContestants" runat="server">
                    </asp:DropDownList></td>
                <td style="width: 115px">
                    <asp:TextBox ID="tbxFlrTestFileName" runat="server"></asp:TextBox></td>
                <td style="width: 115px">
                    <asp:TextBox ID="tbxFlrDescription" runat="server"></asp:TextBox></td>
            </tr>
            <tr>
                <td colspan="8" style="height: 20px">
                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_Click" Text="Search" />&nbsp;
                    <asp:Button ID="btnReset" runat="server" OnClick="btnReset_Click" Text="Reset" />
                    &nbsp;&nbsp;&nbsp;&nbsp; 
                    <asp:Label ID="lblSearchErr" ForeColor="Red"  runat="server" ></asp:Label>
                    </td>
            </tr>
        </table>
        <br />
        <br />

    </asp:Panel>
    
    <asp:Panel ID="Panel4" runat="server" Visible="False">
        <asp:Label ID="Label13" runat="server" Text="Download Test Papers" Width="249px" Font-Bold="True"
            Font-Size="Large"></asp:Label> &nbsp;&nbsp;&nbsp; 
        
            <br />
            <br />
        <asp:Label ID="Label14" runat="server" Text="Select Week range"> </asp:Label>
            &nbsp;
        <asp:DropDownList ID="ddlFlrWeekForExamReceiver" runat="server" OnSelectedIndexChanged="ddlFlrWeekForExamReceiver_SelectedIndexChanged">
        </asp:DropDownList>
            <asp:Button ID="ButtonForExamReceiver" runat="server" OnClick="ButtonForExamReceiver_Click" Text="Search" /><br />
       
        <br />
    </asp:Panel>
    
    <asp:panel ID="Panel3" runat="server">
    <center><asp:Label  ID="lblChapter" runat="server" Visible="false"  CssClass="title02"></asp:Label><br />
        <asp:Label ID="LblexamRecErr" runat="server" ForeColor="Red" ></asp:Label></center><br />
        <center><asp:Label ID="lblPrdError" runat="server" ForeColor="Red" ></asp:Label>
            <br />
        </center>
            <asp:GridView ID="gvTestPapers" runat="server" AutoGenerateColumns="false" OnRowCommand="gvTestPapers_RowCommand"  o="gvTestPapers_RowDataBound"
            AllowSorting="true" OnSorting="gvTestPapers_Sorting" BorderStyle="Solid" BorderWidth="1" BorderColor="black" CellPadding="4" CellSpacing="0" OnSelectedIndexChanged="gvTestPapers_SelectedIndexChanged"
           >
            <Columns>
                <asp:BoundField HeaderText="Id" DataField="TestPaperId"  SortExpression="TestPaperId" />                
                <asp:BoundField HeaderText="ProdCode" DataField="ProductCode" SortExpression="ProductCode" />
                <asp:BoundField HeaderText="ProdGrpCode" DataField="ProductGroupCode" SortExpression="ProductGroupCode" />
                <asp:BoundField HeaderText="WkId" DataField="WeekId" SortExpression="WeekId" />
                <asp:BoundField HeaderText="Set#" DataField="SetNum" SortExpression="SetNum" />
                <asp:BoundField HeaderText="#OfChildren" DataField="NoOfContestants" SortExpression="NoOfContestants" />
                <asp:BoundField HeaderText="TestFileName" DataField="TestFileName" SortExpression="TestFileName" />
                <asp:BoundField HeaderText="Description" DataField="Description" SortExpression="Description" />
                <asp:BoundField HeaderText="Password" DataField="Password" SortExpression="Password" />
                <asp:ButtonField ButtonType="Button" CommandName="Download" Text="Download" />
                <asp:BoundField HeaderText="DocType" Visible="False"   DataField="DocType" SortExpression="DocType" />

            </Columns>
        </asp:GridView>
        <br />
        <asp:Literal ID="ltrMissingTP" runat="server"></asp:Literal>
     </asp:panel>
    <asp:HiddenField ID="hdnTechNational" runat="server" />
    <asp:HiddenField ID="hdnChapterID" runat="server" />
    <asp:HiddenField ID="hdnTempFileName" runat="server" />
</asp:Content>

 

 
 
 