﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StudentZoomReg.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="StudentWebExReg" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to remove this child from session?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }

            function JoinMeeting() {

                var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

                window.open(url, '_blank');

            }

            function showmsg() {
                alert("Coaches can only join their class up to 30 minutes before class time");
            }
            function showAlert() {
                alert("Meeting attendees can only join their class up to 10 minutes before class time");
            }

        </script>
        <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            Zoom Registration for a Student
                     <br />
            <br />
        </div>
        <div style="clear: both; margin-bottom: 20px;">

            <asp:Button ID="btnRegisterAll"  runat="server" Text="Register All"  OnClick="btnRegisterAll_onClick" />

        </div>
        
        <div style="clear: both; margin-bottom: 10px;"></div>
        <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 80%; background-color: #ffffcc;">
            <tr class="ContentSubTitle" align="center">
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="ddYear" runat="server" Width="75px"
                        AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                       
                    </asp:DropDownList>

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Event </td>
                <td style="width: 439px" align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                        <%--<asp:ListItem Value="13">Coaching</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
                <td style="width: 100px" align="left" nowrap="nowrap" id="Td1" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Chapter </td>
                <td style="width: 100px" runat="server" align="left" id="Td2">
                    <asp:DropDownList ID="ddchapter" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                        <asp:ListItem Value=""></asp:ListItem>
                        <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                    </asp:DropDownList>
                </td>


                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Student
                </td>
                <td style="width: 125px;" align="left">
                    <asp:DropDownList ID="ddlChildrenList" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlChildrenList_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>
                    <div style="margin-bottom: 10px;"></div>
                </td>


            </tr>
            <tr class="ContentSubTitle" align="center">
                <td style="text-align: right; font-weight: bold;">Product Group</td>
                <td style="text-align: left;">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td style="text-align: right; font-weight: bold;">Product</td>
                <td style="text-align: left">
                    <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Level
                </td>
                <td style="width: 125px;" align="left">
                    <asp:DropDownList ID="ddlLevel" Enabled="false" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td id="Td3" style="width: 100px" align="left" nowrap="nowrap" runat="server" visible="true">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Session#
                </td>
                <td id="Td4" style="width: 125px;" align="left" runat="server" visible="true">
                    <asp:DropDownList ID="ddlSession" runat="server" Width="75px" AutoPostBack="True">
                        <%-- <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>--%>
                    </asp:DropDownList>
                </td>
                <td id="tdCoachTitle" runat="server" style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach
                </td>
                <td id="tdCoach" runat="server" style="width: 125px;" align="left">
                    <asp:DropDownList ID="ddlCoach" runat="server" Width="150px" AutoPostBack="True" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>

                </td>


            </tr>
            <tr>
                <td align="center" colspan="10">
                    <asp:Button ID="btnSubmit" runat="server" Visible="false" Text="Submit" Style="height: 26px" />
                    <asp:Button ID="BtnCreateSession" Visible="false" runat="server" Text="Create Meeting" />
                    <asp:Button ID="BtnUpdateRegistration" runat="server" Text="Update Registration" OnClick="BtnUpdateRegistration_Click" />
                    <asp:Button ID="btnCreateAllSessions" runat="server" Text="Create All Sessions" Visible="false" Style="height: 26px" />
                </td>
            </tr>



        </table>

        <br />
        <div align="center">
            <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
        </div>
        <div style="clear: both;"></div>
        <asp:Label ID="lblChildMsg" runat="server" align="center" Style="color: red;"></asp:Label>
        <div style="clear: both;"></div>
        <div align="center">
            <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1200px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdStudentsList_RowCommand" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdStudentsList_PageIndexChanging">
                <Columns>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" HeaderStyle-Width="200">

                        <ItemTemplate>
                            <div style="display: none;">
                                <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeJoinURL") %>'>'></asp:Label>
                                <asp:Label ID="lblUserID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                                <asp:Label ID="LblPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                                <asp:Label ID="LblMeetingKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'>'></asp:Label>
                                <asp:Label ID="LblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                <asp:Label ID="LblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                                <asp:Label ID="lblCoachRegID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CoachRegID") %>'>'></asp:Label>
                                <asp:Label ID="LblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'>'></asp:Label>

                            </div>

                            <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                            <asp:Button ID="BtnReRegister" runat="server" Text="Modify" CommandName="ReRegister" />
                            <asp:Button ID="BtnDelete" runat="server" Text="Delete" CommandName="DeleteAttendee" />
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>
                    <asp:BoundField DataField="ParentName" HeaderText="Parent"></asp:BoundField>
                    <asp:BoundField DataField="CoachName" HeaderText="Coach"></asp:BoundField>
                    <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                    <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                    <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                    <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                    <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                    <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>

                    <asp:TemplateField HeaderText="meeting URL">

                        <ItemTemplate>

                            <div style="display: none;">
                                <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>
                                <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'><%# Eval("Time").ToString() %></asp:Label>
                                <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                            </div>
                            <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                            <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>

            </asp:GridView>
        </div>
        <div style="clear: both;"></div>

        <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
        <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
        <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
        <input type="hidden" id="hdnHostURL" value="0" runat="server" />
        <input type="hidden" id="hdnWebExID" value="0" runat="server" />
        <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
        <input type="hidden" id="hdnCoachID" value="0" runat="server" />
        <input type="hidden" id="hdnChildID" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
        <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
        <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
        <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
        <input type="hidden" id="hdnProductID" value="0" runat="server" />

        <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
        <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
        <input type="hidden" id="HdnChildEmail" value="0" runat="server" />
        <input type="hidden" id="HdnLevel" value="0" runat="server" />
        <input type="hidden" id="hdnCoachname" value="0" runat="server" />
        <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
        <input type="hidden" id="hdnStartMins" value="0" runat="server" />
        <input type="hidden" id="HdnCoachRegID" value="0" runat="server" />
        <input type="hidden" id="hdnZoomURL" value="0" runat="server" />
</asp:Content>

