<%@ Page Language="VB" %>
<%@ Import Namespace="System.Data" %>
<%@ Import Namespace="System.Data.SqlClient" %>
<%@ Import Namespace="System.Data.SqlTypes" %>
<%@ Import Namespace="System.Xml" %>
<%@ Import Namespace="System.Configuration" %>
<script language="VB" runat="server">

	Sub Page_Load(sender as Object, e as EventArgs)

     	Dim UserEmail as string   = trim(Request("UserEmail"))
		Dim ScoreData as string  = trim(Request("scoredata"))
     	Dim Password as string     = trim(Request("Password"))

		Dim myConnection As SqlConnection
			
		' Create connection and set connection string	
		Dim aConfig As ConfigurationSettings
		Dim sConStr As String = aConfig.AppSettings("DBConnection")
		myConnection = New SqlConnection(sConStr)
		


		 Dim sSql as string
		 Dim myDataSet    As DataSet
		 Dim bSuccess as Boolean

		 Try
			Dim m_xmld As XmlDocument
			Dim m_nodelist As XmlNodeList
			Dim m_node As XmlNode
			'Create the XML Document
			m_xmld = New XmlDocument()
			'Load the Xml file
			m_xmld.LoadXml(ScoreData.Replace("[","<").Replace("]",">").Replace("|","/") )
			'Get the list of name nodes 
			m_nodelist = m_xmld.SelectNodes("/Contestants/Contestant")
			'Loop through the nodes
			For Each m_node In m_nodelist
				Dim contestant_id = m_node.Attributes.GetNamedItem("contestant_id").Value
				Dim Score1 = m_node.Attributes.GetNamedItem("Score1").Value
				Dim Score2 = m_node.Attributes.GetNamedItem("Score2").Value
				Dim Score3 = m_node.Attributes.GetNamedItem("Score3").Value
				Dim Rank = m_node.Attributes.GetNamedItem("Rank").Value
		
				sSql = "exec dbo.UpdateScores2009 '" + UserEmail + "', '" + Password + "', " 
				sSql = sSql + contestant_id + ", " + Score1 + ", " + Score2 + ", " + Score3 + ", " + Rank 
				Dim myCommand    As SqlDataAdapter
				myCommand = New SqlDataAdapter(ssql, myConnection)
				
				myDataSet = New DataSet()
					
				' This line changes the name of the outer container
				' tags in the resulting xml file.
				myDataSet.DataSetName = "Results"
					
				' Use the command to fill our DataSet
				myCommand.Fill(myDataSet, "Result")
				if myDataSet.Tables(0).Rows(0)("Success").ToString()="0" then
					'return the data set as xml
					bSuccess = false
					myDataSet.WriteXml(Response.OutputStream)
					exit for
				end if
				bSuccess = true
			Next
		Catch errorVariable As Exception
			'Error trapping
			response.Write(errorVariable.ToString())
		End Try
		if (bSuccess) then
			myDataSet.WriteXml(Response.OutputStream)
		end if		
	End Sub

</script>

 

 
 
 