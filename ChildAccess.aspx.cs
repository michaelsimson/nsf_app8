﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Net;
using System.Data.SqlClient;
using System.Drawing;
using System.Text.RegularExpressions;
using System.Net.Mail;
using System.Web.Services;



public partial class ChildAccess : System.Web.UI.Page
{

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                lblPid.Text = Session["CustIndID"].ToString();
                PopulateData();

                btnContinue.Visible = false;
                if (!string.IsNullOrEmpty(Request.QueryString["id"]))
                {
                    if (Request.QueryString["id"] == "1" || Request.QueryString["id"] == "2")
                    {
                        btnContinue.Visible = true;
                        if (Request.QueryString["id"] == "2")
                        {
                            btnContinue.Text = "Done";
                        }
                    }
                }
            }
        }
    }

    protected void Button1_Click(object sender, EventArgs e)
    {

    }

    private Boolean IsOnlineEmailEmpty()
    {
        string sOnlineEmail;
        foreach (GridViewRow row in gvChild.Rows)
        {
            sOnlineEmail = row.Cells[6].Text.Replace("&nbsp;", "");
            if (sOnlineEmail.Length == 0)
            {
                lblError.Text = "All children in coaching must have email address to join online class";
                return true;
            }
        }

        return false;
    }
    private void ActivateControl(bool b)
    {
        //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
        txtEmailID.Enabled = b;
        txtPassword.Enabled = b;
        txtPasswordConfirm.Enabled = b;
        //txtOnlineEmailID.Enabled = b;
        txtEmailIDConfirm.Enabled = b;
        // txtOnlineEmailIDConfirm.Enabled = b;
        btnUpdate.Enabled = b;
    }

    protected void gvChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        lblResult.Text = "";
        ButtonField buttonClicked = sender as ButtonField;
        String commandName = string.Empty;
        if (buttonClicked != null)
        {
            commandName = buttonClicked.CommandName;
        }

        GridViewRow row = gvChild.SelectedRow;
        if (commandName == "Select")
        {
            hdSelChildName.Value = row.Cells[1].Text.Replace("&nbsp;", "");
            txtEmailID.Text = row.Cells[4].Text.Replace("&nbsp;", "");
            txtEmailIDConfirm.Text = row.Cells[4].Text.Replace("&nbsp;", "");
            txtPassword.Text = row.Cells[5].Text.Replace("&nbsp;", "");
            txtPasswordConfirm.Text = row.Cells[5].Text.Replace("&nbsp;", "");
            //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
            //txtOnlineEmailID.Text = row.Cells[6].Text.Replace("&nbsp;", "");
            //txtOnlineEmailIDConfirm.Text = row.Cells[6].Text.Replace("&nbsp;", "");
            lblchid.Text = row.Cells[0].Text.Trim();
            ActivateControl(true);
        }
        else if (commandName == "ShowPWD")
        {

        }
    }

    protected void PopulateData()
    {
        DataSet ds;
        string strCMD = "";
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        try
        {
            strCMD = "SELECT ChildNumber,FIRST_NAME +' '+ MIDDLE_INITIAL +' ' + LAST_NAME as ChildName ,GRADE , CONVERT(varchar,DATE_OF_BIRTH, 101) AS DateOfBirth,  Email , PwD, OnlineClassEmail from [child] where MEMBERID =" + lblPid.Text;
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                if (Request.QueryString["id"].ToString() == "1" || Request.QueryString["id"] == "2")
                {
                    if (Session["ChildNumbers"] != null)
                    {
                        strCMD = "SELECT ChildNumber,FIRST_NAME +' '+ MIDDLE_INITIAL +' ' + LAST_NAME as ChildName ,GRADE , CONVERT(varchar,DATE_OF_BIRTH, 101) AS DateOfBirth,  Email , PwD, OnlineClassEmail from [child] where MEMBERID =" + lblPid.Text + " and ChildNumber in (" + Session["ChildNumbers"] + ")";
                    }
                }
            }
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strCMD);
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvChild.DataSource = ds;
                gvChild.DataBind();
                Panel2.Visible = true;
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    string Email = gvChild.Rows[i].Cells[4].Text;
                    if (Email.Replace("&nbsp;", "") == "")
                    {
                        ((Button)gvChild.Rows[i].FindControl("BtnShowPwd") as Button).Visible = false;
                    }
                    else
                    {
                        ((Button)gvChild.Rows[i].FindControl("BtnShowPwd") as Button).Visible = true;
                    }
                }
                //  Added by Dino on September 07 2016 , To made default selection and enable all the input controls if one child exists
                if (ds.Tables[0].Rows.Count == 1)
                {

                    lblchid.Text = ds.Tables[0].Rows[0]["ChildNumber"].ToString();
                    gvChild.RowStyle.BackColor = Color.FromName("#FFCC66");
                    gvChild.RowStyle.Font.Bold = true;
                    ActivateControl(true);


                }
            }
        }
        catch (SqlException ex)
        {
            lblResult.ForeColor = Color.Red;
            lblResult.Text = ex.Message.ToString();
        }
    }
    protected void btnAdd_Click(object sender, EventArgs e)
    {
        if (IsValid() == true)
        {
            DataUpdate();
        }
    }
    protected void DataUpdate()
    {
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        lblResult.Text = "";
        int cValue = 0;
        int lValue = 0;
        int iValue = 0;
        if (txtEmailID.Text.Trim().Length > 0)
        {
            cValue = Convert.ToInt16(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT COUNT (Email) from Child where Email ='" + txtEmailID.Text.Trim() + "'"));
            if (cValue > 0)
            {
                int mValue = Convert.ToInt16(SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT COUNT (Email)  from Child where Email = '" + txtEmailID.Text.Trim() + "'" + " and MEMBERID = " + lblPid.Text.Trim() + " and childNumber=" + lblchid.Text.Trim()));
                if (mValue > 0)
                {

                    cValue = 0;
                }
            }
        }
        //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
        // Validate for Join Online Email class
        //if (txtOnlineEmailID.Text.Trim().Length > 0)
        //{
        //    string strCmd = "  Declare @cValue int, @chkValue int; SELECT @cValue=isnull(count(OnlineClassEmail),0) from Child where OnlineClassEmail='" + txtOnlineEmailIDConfirm.Text + "'";
        //    strCmd = strCmd + " IF( @cValue>0) BEGIN ";
        //    strCmd = strCmd + " SELECT @chkValue=isnull(COUNT(OnlineClassEmail),0) from Child where OnlineClassEmail = '" + txtOnlineEmailID.Text.Trim() + "'" + " and MEMBERID = " + lblPid.Text.Trim() + " and childNumber=" + lblchid.Text.Trim();
        //    strCmd = strCmd + " if (@chkValue>0) BEGIN SET @cValue=0 END END SELECT @cValue";
        //    int childOnline = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strCmd));
        //    if (childOnline > 0)
        //    {
        //        strCmd = "SELECT C.First_Name + ' ' + C.Last_Name ChildName from Child C Inner Join IndSpouse I on C.MemberId=I.AutoMemberId where C.OnlineClassEmail='" + txtOnlineEmailIDConfirm.Text + "'";
        //        DataSet dsInd = SqlHelper.ExecuteDataset(conn, CommandType.Text, strCmd);
        //        if (dsInd.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow  dr = dsInd.Tables[0].Rows[0];
        //            lblResult.ForeColor = Color.Red;
        //            //"Online email ID Bukkuri@gmail.com you supplied below has already been used for Kaushik Bukkuri.  Please supply a different unique email ID for Anuraag Bukkuri."
        //            lblResult.Text = "Online Email ID "+  txtOnlineEmailIDConfirm.Text + " you supplied below has already been used for " + dr["ChildName"] + ".<br>Please supply a different unique email ID for "+  hdSelChildName.Value +".";
        //        }
        //        return;
        //    }
        //}

        if (cValue == 0)
        {
            try
            {
                //SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "UPDATE [Child] SET [Email]='" + txtEmailID.Text.Trim() + "', PwD='" + txtPassword.Text.Trim() + "', OnlineClassEmail = '" + txtOnlineEmailID.Text + "'  where MEMBERID = " + lblPid.Text.Trim() + " and childNumber=" + lblchid.Text.Trim());
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, "UPDATE [Child] SET [Email]='" + txtEmailID.Text.Trim() + "', PwD='" + txtPassword.Text.Trim() + "' where MEMBERID = " + lblPid.Text.Trim() + " and childNumber=" + lblchid.Text.Trim());
                lblResult.ForeColor = Color.Green;
                lblResult.Text = "Record Updated.";
                PopulateData();
                Reset();
            }
            catch (Exception ex)
            {
                lblResult.ForeColor = Color.Red;
                lblResult.Text = ex.Message.ToString();
            }
        }
        else
        {
            if (cValue > 0)
            {
                lblResult.ForeColor = Color.Red;
                lblResult.Text = "Email ID already exists in the child database";
            }
        }


    }
    private void Reset()
    {
        txtEmailID.Text = "";
        txtEmailIDConfirm.Text = "";
        txtPassword.Text = "";
        txtPasswordConfirm.Text = "";
        //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
        //txtOnlineEmailID.Text = "";
        //txtOnlineEmailIDConfirm.Text = "";
        ActivateControl(false);
    }

    private Boolean IsValid()
    {
        lblError.Text = "";
        lblResult.Text = "";
        if (txtEmailID.Text.Trim().Length > 0)
        {
            Regex objReg = new Regex("");
            if (!Regex.IsMatch(txtEmailID.Text.Trim(), "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", RegexOptions.IgnoreCase))
            {
                lblError.Text = "EMail id Should be a Valid Email Address";
                return false;
            }
        }
        if (txtEmailIDConfirm.Text.Trim().Length > 0)
        {
            Regex objReg = new Regex("");
            if (!Regex.IsMatch(txtEmailIDConfirm.Text.Trim(), "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", RegexOptions.IgnoreCase))
            {
                lblError.Text = "Confirmation EMail id Should be a Valid Email Address";
                return false;
            }
        }
        if (txtEmailID.Text.Trim().Length > 0)
        {
            if (txtEmailIDConfirm.Text.Trim().Length == 0)
            {
                lblError.Text = "Homework submission confirmation Email Address should not be blank";
                return false;
            }
        }
        if (txtEmailID.Text.Trim() != txtEmailIDConfirm.Text.Trim())
        {
            lblError.Text = "Homework submission Confirmation Email Address is not matching the value on the left side";
            return false;
        }

        if (txtEmailID.Text.Trim().Length > 0 && txtPassword.Text.Trim().Length == 0)
        {
            lblError.Text = "Password should not be blank";
            return false;
        }
        if (txtPassword.Text.Trim() != txtPasswordConfirm.Text.Trim())
        {

            lblError.Text = "Confirmation Password is not matching the value on the left side.";
            return false;
        }
        //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.

        //if (txtOnlineEmailID.Text.Trim().Length == 0)
        //{
        //    lblError.Text = "Online Email address should not be blank";
        //    return false;
        //}
        //else if (txtOnlineEmailID.Text.Trim().Length > 0)
        //{
        //    Regex objReg = new Regex("");
        //    if (!Regex.IsMatch(txtOnlineEmailID.Text.Trim(), "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", RegexOptions.IgnoreCase))
        //    {
        //        lblError.Text = "Confirmation Online EMail Address Should be a Valid Email Address";
        //        return false;
        //    }
        //}

        //if (txtOnlineEmailIDConfirm.Text.Trim().Length == 0)
        //{
        //    lblError.Text = "Confirmation Online Email address should not be blank";
        //    return false;
        //}
        //else if (txtOnlineEmailIDConfirm.Text.Trim().Length > 0)
        //{
        //    Regex objReg = new Regex("");
        //    if (!Regex.IsMatch(txtOnlineEmailIDConfirm.Text.Trim(), "\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*", RegexOptions.IgnoreCase))
        //    {
        //        lblError.Text = "Confirmation Online EMail Address Should be a Valid Email Address";
        //        return false;
        //    }
        //}

        //if (txtOnlineEmailID.Text.Trim() != txtOnlineEmailIDConfirm.Text.Trim())
        //{
        //    lblError.Text = "Confirmation Online Email Address is not matching the value on the left side";
        //    return false;
        //}

        return true;
    }

    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        if (IsValid() == true)
        {
            //SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(),CommandType.Text,"Update Child Set Email='',PwD='' where MEMBERID = " + lblPid.Text.Trim() + " and childNumber=" + lblchid.Text.Trim());
            lblResult.Text = "";
            DataUpdate();
            if (!string.IsNullOrEmpty(Request.QueryString["id"]))
            {
                if (Request.QueryString["id"] == "1")
                {
                    btnContinue.Visible = true;
                    btnContinue.Enabled = true;
                    //if (IsOnlineEmailEmpty() == true)
                    //{
                    //    btnContinue.Enabled = false;
                    //}
                }
                else if (Request.QueryString["id"] == "2")
                {

                    if (Session["UpdateRegInChildAccess"] != null)
                    {
                        //if (IsOnlineEmailEmpty() == false)
                        //{
                        if (Session["UpdateRegInChildAccess"].ToString().ToLower() == "true")
                        {
                            UpdateCoachReg();
                        }
                        //}
                    }
                }
            }
        }

    }
    protected void gvChild_RowCreated(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            GridView HeaderGrid = (GridView)sender;
            GridViewRow HeaderRow = new GridViewRow(0, 0, DataControlRowType.Header, DataControlRowState.Insert);

            TableCell Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "Child Number";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.VerticalAlign = VerticalAlign.Middle;
            Cell_Header.ColumnSpan = 1;
            Cell_Header.RowSpan = 2;
            HeaderRow.Cells.Add(Cell_Header);

            Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "Name Of Child";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.VerticalAlign = VerticalAlign.Middle;
            Cell_Header.ColumnSpan = 1;
            Cell_Header.RowSpan = 2;
            HeaderRow.Cells.Add(Cell_Header);

            Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "Grade";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.VerticalAlign = VerticalAlign.Middle;
            Cell_Header.ColumnSpan = 1;
            Cell_Header.RowSpan = 2;
            HeaderRow.Cells.Add(Cell_Header);

            Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "Date Of Birth";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.VerticalAlign = VerticalAlign.Middle;
            Cell_Header.ColumnSpan = 1;
            Cell_Header.RowSpan = 2;
            HeaderRow.Cells.Add(Cell_Header);

            Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "Homework Submission";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.ColumnSpan = 2;
            HeaderRow.Cells.Add(Cell_Header);

            Cell_Header = new TableCell();
            Cell_Header.ForeColor = Color.White;
            Cell_Header.Text = "";
            Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            Cell_Header.ColumnSpan = 1;
            HeaderRow.Cells.Add(Cell_Header);

            //Cell_Header = new TableCell();
            //Cell_Header.ForeColor = Color.White;
            //Cell_Header.Text = " ";
            //Cell_Header.HorizontalAlign = HorizontalAlign.Center;
            //Cell_Header.ColumnSpan = 1;
            //Cell_Header.RowSpan = 2;
            //HeaderRow.Cells.Add(Cell_Header);

            HeaderRow.BackColor = Color.FromName("#009900");
            HeaderRow.BorderColor = Color.Black;
            gvChild.Controls[0].Controls.AddAt(0, HeaderRow);

        }
    }
    protected void gvChild_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.Header)
        {
            e.Row.Cells[0].Visible = false;
            e.Row.Cells[1].Visible = false;
            e.Row.Cells[2].Visible = false;
            e.Row.Cells[3].Visible = false;

            //e.Row.Cells[7].Visible = false;
        }
    }
    protected void btnContinue_Click(object sender, EventArgs e)
    {
        lblError.Text = "";
        lblResult.Text = "";
        if (Request.QueryString["id"] == "1" || Request.QueryString["id"] == "2")
        {
            //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
            //if (IsOnlineEmailEmpty() == false)
            //{                
            if (Request.QueryString["id"] == "1")
            {
                Response.Redirect("TermsAndConditions.aspx");
            }
            else
            {
                if (Session["UpdateRegInChildAccess"] != null)
                {
                    if (Session["UpdateRegInChildAccess"].ToString().ToLower() == "true")
                    {
                        UpdateCoachReg();
                    }
                }
                Response.Redirect("UserFunctions.aspx");
            }
            //}
        }
    }
    //Update Coachreg details
    private void UpdateCoachReg()
    {
        //Send mail to Coach
        ApprovedChildNotification();
        string strCmd;
        //// update coachreg status and skip payment process
        strCmd = "UPDATE CR SET CR.Approved='Y',CR.ModifyDate=GetDate(),ModifiedBy=" + Session["CustIndID"] + " FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID and CR.Level = C.Level AND C.EventYear = CR.EventYear ";
        strCmd = strCmd + " AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo WHERE CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" + Session["CustIndID"] + " AND CR.EVENTID=" + Session["EventId"] + " And  CR.Eventyear >= Year(GETDATE())";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strCmd);
    }
    private void ApprovedChildNotification()
    {

        try
        {

            string CoachEmail = "";
            if (((Session["CoachEmail"] != null)))
            {
                CoachEmail = Session["CoachEmail"].ToString();
            }
            string eventType = null;
            if (Convert.ToInt16(Session["EventID"]) == 1)
            {
                eventType = "Finals";
            }
            else
            {
                eventType = "Regional";
            }

            SqlDataReader reader = SqlHelper.ExecuteReader(Application["ConnectionString"].ToString(), CommandType.Text, "select I.Email,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,CR.Level,CR.ProductID FROM CoachReg CR INNER JOIN CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.Level =C.Level  AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo   Inner JOIN Product P ON C.ProductID=P.ProductID  Inner JOIN  IndSpouse I ON I.AutoMemberID = C.MemberID INNER JOIN Child Ch ON Ch.ChildNumber=CR.ChildNumber INNER JOIN IndSpouse I2 ON Ch.MEMBERID = I2.AutoMemberID  WHERE  CR.[PaymentReference] is NULL and CR.Approved='N' and CR.[PMemberID]=" + Session["CustIndID"] + " AND CR.EVENTID=" + Session["EventId"] + " And  CR.Eventyear >= Year(GETDATE()) and C.Accepted='Y'");
            string Emailid = null;
            string Mailbody = null;
            while (reader.Read())
            {
                Emailid = reader["Email"].ToString();
                CoachEmail = Emailid;
                Mailbody = "Dear Coach, <br><br>Note : Do not reply to the email above.  ";
                Mailbody = Mailbody + "<br><br>New student: " + reader["ChildName"] + ", Parent Name: " + reader["ParentName"] + ", Email: " + reader["ParentEMail"] + ", " + reader["ProductCode"] + ", Coach: " + Emailid + ", Level: " + reader["Level"] + ".";
                SendEmail("A New student is joining ", Mailbody, Emailid);
            }
        }
        catch (Exception ex)
        {
        }

    }
    private bool SendEmail(string sSubject, string sBody, string sMailTo)
    {
        //sMailTo = "bindhu.rajalakshmi@capestart.com";
        //Build Email Message
        MailMessage email = new MailMessage();
        if (Session["ProductCodeCoach"] == "MB2" | Session["ProductCodeCoach"] == "MB1" | Session["ProductCodeCoach"] == "MB3" | Session["ProductCodeCoach"] == "MB4")
        {
            email.From = new MailAddress("nsfpremathcounts@hotmail.com");
        }
        else if (Session["ProductCodeCoach"] == "SATE" | Session["ProductCodeCoach"] == "SATM")
        {
            email.From = new MailAddress("nsfsat@hotmail.com");
        }
        else if (Session["ProductCodeCoach"] == "JUV" | Session["ProductCodeCoach"] == "IUV" | Session["ProductCodeCoach"] == "SUV")
        {
            email.From = new MailAddress("nsfvalues.parents@gmail.com");
        }
        else
        {
            email.From = new MailAddress("nsfcontests@northsouth.org");
        }

        email.Subject = sSubject;
        email.IsBodyHtml = true;
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure;
        email.Body = sBody;
        //leave blank to use default SMTP server
        SmtpClient client = new SmtpClient();
        //Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        //client.Host = host

        bool ok = true;
        try
        {
            email.To.Add(sMailTo);
            client.Send(email);
        }
        catch (Exception e)
        {
            //   lblMessage.Text = e.Message.ToString
            ok = false;
        }
        return ok;
    }



    protected void gvChild_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "ShowPWD")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                gvChild.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string Pwd = ((Label)gvChild.Rows[selIndex].FindControl("lblChildPwd") as Label).Text;
                string Email = ((Label)gvChild.Rows[selIndex].FindControl("lblEmail") as Label).Text;
                string ChildNumber = ((Label)gvChild.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;

                hdnPwd.Value = Pwd;
                hdnChildNumber.Value = ChildNumber;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "openPopu();", true);
            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                hdSelChildName.Value = row.Cells[1].Text.Replace("&nbsp;", "");
                txtEmailID.Text = row.Cells[4].Text.Replace("&nbsp;", "");
                txtEmailIDConfirm.Text = row.Cells[4].Text.Replace("&nbsp;", "");
                string Pwd = ((Label)gvChild.Rows[selIndex].FindControl("lblChildPwd") as Label).Text;
                txtPassword.Text = Pwd.Replace("&nbsp;", "");
                txtPasswordConfirm.Text = Pwd.Replace("&nbsp;", "");

                //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
                //txtOnlineEmailID.Text = row.Cells[6].Text.Replace("&nbsp;", "");
                //txtOnlineEmailIDConfirm.Text = row.Cells[6].Text.Replace("&nbsp;", "");
                lblchid.Text = row.Cells[0].Text.Trim();
                ActivateControl(true);
            }
        }
        catch
        {

        }
    }

    [WebMethod]
    public static int UpdatePassword(string Password, string ChildNumber)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "UPDATE [Child] SET  PwD='" + Password + "' where  childNumber=" + ChildNumber.Trim() + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;

        }
        catch (Exception ex)
        {
        }
        return retVal;

    }

    [WebMethod]
    public static string GetPassword(string ChildNumber)
    {

        string Password = "";

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = " select pwd from child where  childNumber=" + ChildNumber.Trim() + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    Password = ds.Tables[0].Rows[0]["Pwd"].ToString();
                }
            }

        }
        catch (Exception ex)
        {
        }
        return Password;

    }
}
