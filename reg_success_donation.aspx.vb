Imports System
Imports System.Web
Imports LinkPointTransaction
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports NorthSouth.BAL


Namespace VRegistration
    Partial Class reg_success_donation
        Inherits System.Web.UI.Page


        '    Private us As CultureInfo = New CultureInfo("en-US")
        Protected order As String
        Protected resp As String
        Protected fIE5 As Boolean
        Public nRegFee As Decimal = 0
        Dim sbContests As New StringBuilder


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            ' '' ''debug
            '' ''Session("LoggedIn") = True
            '' ''Session("CustIndID") = 12637
            '' ''Session("EventID") = 1
            '' ''Session("R_APPROVED") = "APPROVED"
            '' ''Session("PaymentReference") = "0C9F4064-466CACA6-599-D985E"
            '' ''Session("RegFee") = 40

            If (Not Session("LoggedIn") Is Nothing) Then
                If Session("LoggedIn") <> "True" Then
                    Dim entryToken As String = Nothing
                    If (Not Session("entryToken") Is Nothing) Then
                        entryToken = Session("entryToken").ToString().Substring(0, 1)
                        Server.Transfer("login.aspx?entry=" + entryToken)
                    Else
                        Server.Transfer(System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL"))
                    End If

                End If
            End If
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("LoginID"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If Len(Trim("" & Session("entryToken"))) = 0 Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim isTestMode As Boolean = False
            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)

            If Not Session("CustIndID") = Nothing Then
                Session("ParentID") = Session("CustIndID")
            End If

            ' Put user code to initialize the page here
            If Not IsPostBack Then

                Dim sb As New StringBuilder
                Dim bc As HttpBrowserCapabilities = Request.Browser
                Dim re As StreamReader
                'Dim reDAS As StreamReader
                Dim nDonationAmt As Decimal = 0
                Dim nTaxDeductibleAmount As Decimal = 0
                Dim nregfee As Decimal = Session("RegFee")
                Dim nTaxDeductibleRegFee = CType(Session("RegFee"), Decimal) * (2 / 3)
                Dim emailBody As String = ""
                Dim screenConfirmText As String = ""
                Dim subMail As String = "Confirmation received for NSF 2007 Contests"
                Dim strDonationMessage As String = ""
                Dim approved As String = ""
                Dim paymentReference As String = ""

                If (isTestMode = True) Then
                    subMail = "Test Email(no real credit card Transactions):" + "Confirmation received for NSF 2007 Contests"
                End If

                If (Not Session("R_APPROVED") Is Nothing) Then
                    approved = Session("R_APPROVED").ToString
                End If

                If (Not Session("PaymentReference") Is Nothing) Then
                    paymentReference = Session("PaymentReference").ToString
                End If

                If (approved = "APPROVED") Then
                    DisplayContests()


                    fIE5 = ((bc.Browser = "IE") _
                                AndAlso (bc.MajorVersion > 4))
                    order = CType(Session("outXml"), String)
                    ' resp = CType(Session("resp"), String)  'commented on 1/15/07
                    ' ParseResponse(resp)                   'commented on 1/15/07

                    If (Not (Session("Donation")) Is Nothing) Then
                        nDonationAmt = CType(Session("Donation"), Decimal)
                    End If
                    '    Session("PaymentReference") = R_OrderNum

                    If (nDonationAmt > 0) Then
                        'strDonationMessage = "Thank you also for your generous donation of " & FormatCurrency(nDonationAmt) & ".<BR> This will help NSF�s goal of providing scholarships ($250 each) to 500 poor but meritorious students in India for the year 2006-2007."
                        strDonationMessage = "We also thank you for your generous donation of " & FormatCurrency(nDonationAmt) & ".This will help in reaching our goal of providing 500 scholarships ($250 each) to those who excel among the poor go to college in India for the upcoming academic year."
                    End If
                    'Your tax-deductible contribution is:  ( donation amount + 2/3 * AMNT)
                    nTaxDeductibleAmount = (nDonationAmt + (nTaxDeductibleRegFee))

                    Dim eventType As String
                    If (Session("EventID") = 1) Then
                        eventType = "Finals"
                    Else
                        eventType = "Regional"
                    End If

                    Dim eventYear As String
                    eventYear = System.Configuration.ConfigurationManager.AppSettings.Get("Contest_Year")

                    If Session("EventID") = "1" Then
                        re = File.OpenText(Server.MapPath("ConfirmingEmailParents.htm"))
                    Else
                        re = File.OpenText(Server.MapPath("success_donate_email.htm"))
                    End If

                    emailBody = re.ReadToEnd
                    re.Close()
                    'emailBody = emailBody.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    emailBody = emailBody.Replace("[PAYMENTREFERENCE]", paymentReference)
                    emailBody = emailBody.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    emailBody = emailBody.Replace("[DATAGRID]", sbContests.ToString)
                    emailBody = emailBody.Replace("[DONATIONTEXT]", strDonationMessage)
                    emailBody = emailBody.Replace("[EVENTYEAR]", eventYear)
                    emailBody = emailBody.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nregfee))
                    emailBody = emailBody.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    ' SendDasMessage("Donation to North South Foundation", emailBody, CType(Session("LoginEmail"), String))

                    ''***Screen Population logic

                    re = File.OpenText(Server.MapPath("success_donate_donor.htm"))
                    screenConfirmText = re.ReadToEnd
                    re.Close()
                    'screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", paymentReference)
                    screenConfirmText = screenConfirmText.Replace("[NAME]", Session("Name"))
                    screenConfirmText = screenConfirmText.Replace("[ADDRESS]", Session("Address"))
                    screenConfirmText = screenConfirmText.Replace("[Date]", Session("Date"))
                    'screenConfirmText = screenConfirmText.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    'screenConfirmText = screenConfirmText.Replace("[DATAGRID]", sbContests.ToString)
                    'screenConfirmText = screenConfirmText.Replace("[DONATIONTEXT]", strDonationMessage)
                    'screenConfirmText = screenConfirmText.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nregfee))
                    screenConfirmText = screenConfirmText.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    'screenConfirmText = screenConfirmText.Replace("[EVENTYEAR]", eventYear)
                    'screenConfirmText = screenConfirmText.Replace("[EVENTTYPE]", eventType)
                    lblDonationMessage.Text = screenConfirmText.ToString
                    screenConfirmText = Nothing

                    'Response.Write(sbContests.ToString())

                    '*************************************************

                    If SendEmail(subMail, emailBody.ToString, CType(Session("LoginEmail"), String)) Then
                        'If SendEmail(subMail, emailBody.ToString, CType("chitturi9@gmail.com", String)) Then
                        lblEmailStatus.Text = "A copy of this communication has been sent to your e-mail address for your permanent records."
                    Else
                        lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
                    End If

                    ''*** DAS Message EMail Communication
                    'Uncomment the following section if DAS email  needs to be sent.
                    'Dim strDASMessage As String
                    'reDAS = File.OpenText(Server.MapPath("DASMessage.htm"))
                    'strDASMessage = reDAS.ReadToEnd
                    'subMail = "DAS Message from North South Foundation"
                    'If (isTestMode = True) Then
                    '    subMail = "Test Email:" + "DAS Message from North South Foundation"
                    'End If
                    'SendDasMessage("DAS Message from North South Foundation", strDASMessage, CType(Session("LoginEmail"), String))
                End If
            End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub

        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
            'Build Email Message
            Dim email As New MailMessage
            'email.From = New MailAddress("contests@northsouth.org")
            email.From = New MailAddress("nagakumar@objectinfo.com")
            email.To.Add("nagakumar@objectinfo.com")
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            client.Host = host

            Dim ok As Boolean = True
            Try
                client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
                client.Timeout = 20000
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
            Return ok
        End Function
        Private Sub SendDasMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)

            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("contests@northsouth.org")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            client.Host = host

            Try
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End Sub


        Private Sub DisplayContests()
            Dim sb As New StringBuilder

            Dim rowcount As Int32 = 0

            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}

            Dim prmArray(4) As SqlParameter
            prmArray(0) = New SqlParameter
            prmArray(0).ParameterName = "@ParentID"
            prmArray(0).Value = Session("CustIndID")
            prmArray(0).Direction = ParameterDirection.Input

            prmArray(1) = New SqlParameter
            prmArray(1).ParameterName = "@ContestYear"
            prmArray(1).Value = Application("ContestYear")
            prmArray(1).Direction = ParameterDirection.Input

            prmArray(2) = New SqlParameter
            prmArray(2).ParameterName = "@paymentreference"
            prmArray(2).Value = Session("PaymentReference")
            prmArray(2).Direction = ParameterDirection.Input

            prmArray(3) = New SqlParameter
            prmArray(3).ParameterName = "@EventID"
            prmArray(3).Value = Session("EventID")
            prmArray(3).Direction = ParameterDirection.Input

            'sandhya - 6//8/2007
            'added this parameter to correct the sql for finals


            'If Session("EventID") = "1" Then   'Regional Contact Information
            '    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)
            'ElseIf Session("EventID") = "2" Then   'Regional Contact Information
            '    SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetRegionalContests", dsContestant, tblConestant, prmArray)
            'End If
            SqlHelper.FillDataset(connContest, CommandType.StoredProcedure, "usp_GetContestsAndPaymentInfo", dsContestant, tblConestant, prmArray)
            'If dsContestant.Tables.Count > 0 Then
            '    dgSelectedContests.DataSource = dsContestant.Tables(0).DefaultView
            '    dgSelectedContests.DataBind()
            'End If

            'sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
            'sb.Append("<tr bgcolor=lightblue forecolor=white>")
            'sb.Append("<td width=20%><b>Contestant Name</b></td>")
            'sb.Append("<td width=20%>Contest Desc</td>")
            'sb.Append("<td width=20%>Contest Location / Contact info</td>")
            'sb.Append("<td width=20%>Contest Date Time</td>")
            'sb.Append("<td width=20%>Payment Info</td>")
            'sb.Append("</tr>")
            'If dsContestant.Tables.Count > 0 Then
            '    Response.Write("<!-- I am here-->")

            '    If dsContestant.Tables(0).Rows.Count > 0 Then
            '        Response.Write("<!-- I am here2-->")

            '        For rowcount = 0 To dsContestant.Tables(0).Rows.Count - 1
            '            Response.Write("<!-- I am here3-->")
            '            sb.Append("<tr>")
            '            sb.Append("<td width=20%>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestantName").ToString() + "</td>")

            '            sb.Append("<td width=20%>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestDesc").ToString() + "</td>")
            '            If Application("EventID") = "1" Then
            '                sb.Append("<td width=20%>")
            '                sb.Append(Application("NationalFinalsCity") + "<BR>")
            '            Else
            '                sb.Append("<td width=20%>")
            '                sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("Building").ToString() + ", ")
            '                sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterCity").ToString() + ", ")
            '                sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChapterState").ToString() + "<BR>")
            '                sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("CoordinatorName").ToString() + "</td>")

            '            End If

            '            sb.Append("<td width=20%>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestDate").ToString() + "<BR>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ContestTime").ToString() + "</td>")

            '            sb.Append("<td width=20%>")
            '            sb.Append(FormatCurrency(dsContestant.Tables(0).Rows(rowcount).Item("Fee")).ToString() + "<BR>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentDate").ToString() + "<BR>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentReference").ToString() + "<BR>")
            '            sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentMode").ToString() + "</td></tr>")
            '        Next
            '    End If
            'End If
            'sb.Append("</table>")
            'sbContests.Append(sb.ToString)
            connContest = Nothing
        End Sub
    End Class

End Namespace

