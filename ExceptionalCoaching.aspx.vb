﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Data
Imports VRegistration
Imports System.Collections
Partial Class ExceptionCoaching
    Inherits System.Web.UI.Page

    Dim cmdText As String
    Dim yr As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 1
        'Session("LoginId") = 4240

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "89") Or (Session("RoleId").ToString() = "96") Then
        Else
            Server.Transfer("maintest.aspx")
        End If

        If Not Page.IsPostBack Then


            'If Convert.ToInt32(Now.Month) > 12 Then
            '    ddlEventYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
            'Else
            '    ddlEventYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year))
            'End If
            'If (yr.ToString().Length = 0 Or yr.ToString() = "0") Then
            '    yr = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
            'End If
            'ddlEventYear.Items.Insert(0, Convert.ToInt32(yr))
            'ddlEventYear.Items(0).Selected = True

            LoadYear()
            '  ddlEventYear.Enabled = False
            LoadEvent()
            loadSemesters()
            LoadProductGroup()
            LoadCoach()
            GetRecords()
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from Product where ProductId=" & ProductID & "")
    End Function

    Function getProductGroupID(ByVal ProductID As Integer) As Integer
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupId from Product where ProductId=" & ProductID & "")
    End Function

    Private Sub LoadProductGroup()

        Try


            Dim dsProductGrp As DataSet

            If Session("RoleId") = 89 Then

                cmdText = " select distinct V.ProductGroupID,V.ProductGroupCode from volunteer V inner join EventFees EF on (V.ProductGroupId=EF.ProductGroupId) where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and EF.Semester ='" & ddlSemester.SelectedValue & "' and V.ProductId is not Null  Order by V.ProductGroupCode"
            Else



                'cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" & ddlEvent.SelectedValue & " and ProductId is not Null Order by ProductGroupCode"
                cmdText = "SELECT  Distinct P.ProductGroupID, P.ProductGroupCode from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & " and EF.Semester='" & ddlSemester.SelectedValue & "'  order by P.ProductGroupID"
            End If

            dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ViewState("PrdGroup") = dsProductGrp.Tables(0)

            ddlProductGroup.DataSource = dsProductGrp.Tables(0)
            ddlProductGroup.DataBind()

            ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)
        Catch ex As Exception

        End Try
    End Sub

    Private Sub LoadProduct()
        If Session("RoleId") = 89 Then
            cmdText = " select distinct V.ProductID,V.ProductCode from volunteer V inner join EventFees EF on (V.ProductID=EF.ProductID) where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and V.ProductGroupId=" & ddlProductGroup.SelectedValue & " and EF.Semester='" & ddlSemester.SelectedValue & "' and V.ProductId is not Null  Order by V.ProductCode"
        Else
            cmdText = " select distinct P.ProductID,P.ProductCode from Product P inner join EventFees EF on (P.ProductID=EF.ProductID) where Status='O' and P.eventid =" & ddlEvent.SelectedValue & " and P.ProductGroupId =" & ddlProductGroup.SelectedValue & " and P.ProductId is not Null and EF.Semester='" & ddlSemester.SelectedValue & "' Order by P.ProductCode"
        End If
        '"SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlProductGroup.SelectedValue & " Order by A.ProductCode"
        Dim dsProduct As DataSet
        dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ddlProduct.DataSource = dsProduct.Tables(0)
        ddlProduct.DataBind()
    End Sub

    Private Sub LoadEvent()

        Session("EventID") = 13
        ddlEvent.Items.Add(New ListItem("Coaching", "13"))

        ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(13))
        ddlEvent.Enabled = False

    End Sub

    Private Sub LoadCoach()
        '' get value from calsignup
        Try
            Dim str As String = "select Distinct I.Firstname + ' ' + I.Lastname as CoachName, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I inner join CalSignup CS on CS.EventYear=" & ddlEventYear.SelectedValue & " and CS.MemberId=I.AutoMemberId and CS.EventId=13 and Accepted='Y' "

            str = str & " and CS.Semester='" + ddlSemester.SelectedValue + "' "
            If ddlProductGroup.SelectedValue > 0 Then
                str = str & " and CS.ProductGroupID=" & ddlProductGroup.SelectedValue
            End If
            If ddlProduct.SelectedValue > 0 Then
                str = str & " and ProductId=" & ddlProduct.SelectedValue
            End If
            str = str & " order by I.LastName,I.FirstName"

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, str)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlCoach.DataSource = ds.Tables(0)
                ddlCoach.DataBind()
            Else
                ddlCoach.DataSource = Nothing
                ddlCoach.DataBind()

            End If

        Catch ex As Exception

        End Try

    End Sub
    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 4
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))


        Next
        ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer, ByVal status As String)
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            'Dim strSql As String = "Select First_name+' '+Last_name as Name, ChildNumber From Child where (memberid=" & ParentID & " Or SpouseID=" & ParentID & " Or MemberID =(Select Relationship from IndSpouse where AutoMemberID =" & ParentID & ")) and MemberID >0"
            Dim strSql As String = "Select First_name+' '+Last_name+' - Child' as Name, ChildNumber From Child where (memberid=" & ParentID & ") union all select FirstName+' '+Lastname+' - Adult' as name, AutomemberId as Childnumber from indspouse where Automemberid=" & ParentID & " union all select FirstName+' '+Lastname+' - Adult' as name, AutomemberId as Childnumber from indspouse where Relationship=" & ParentID & "" ' Or SpouseID=" & ParentID & " Or MemberID =(Select Relationship from IndSpouse where AutoMemberID =" & ParentID & ")) and MemberID >0"

            Dim drChild As SqlDataReader
            drChild = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlChild.DataSource = drChild
            ddlChild.DataBind()
            ddlChild.Enabled = True
            If status = "Y" Then
                ddlChild.Items.Insert(0, "Select Student")
                ddlChild.Items(0).Selected = True
            End If
        Catch ex As Exception
            'Response.Write(ParentID & ex.ToString())
        End Try
    End Sub


    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click

        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        Pnl3Msg.Text = ""

        If firstName.Length > 0 Then
            If (firstName.Contains("_")) Then
                firstName = "'%" + firstName.Replace("_", "/_") + "%' ESCAPE '/' "
            Else
                firstName = "'%" + firstName + "%'"
            End If
            strSql.Append(" ( I.firstName like " + firstName + "  OR I1.firstName like " + firstName + ") ")
        End If

        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (lastName.Contains("_")) Then
                lastName = "'%" + lastName.Replace("_", "/_") + "%' ESCAPE '/' "
            Else
                lastName = "'%" + lastName + "%'"
            End If

            If (length > 0) Then
                strSql.Append(" and (I.lastName like " + lastName + " OR I1.lastName like " + lastName + " )")
            Else
                strSql.Append(" (I.lastName like " + lastName + " OR I1.lastName like " + lastName + ")")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (email.Contains("_")) Then
                email = "'%" + email.Replace("_", "/_") + "%' ESCAPE '/' "
            Else
                email = "'%" + email + "%'"
            End If
            If (length > 0) Then
                strSql.Append(" and (I.Email like " + email + " OR I1.Email like " + email + ")")
            Else
                strSql.Append(" (I.Email like " + email + " OR I1.Email like " + email + " ) ")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" and  (I.DonorType='IND' or I1.Donortype='SPOUSE') order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        'Response.Write(sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse_Exp", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Search.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        txtParent.Text = row.Cells(1).Text + " " + row.Cells(2).Text
        HlblParentID.Text = GridMemberDt.DataKeys(index).Value
        Loadchild(HlblParentID.Text, "Y")
        pIndSearch.Visible = False
    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChild.SelectedIndexChanged
        If Not ddlChild.Items(0).Selected = True Then

            LoadProductGroup()
        End If
    End Sub

    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        Dim strsql As String = "SELECT Ex.ExCoachingID, Convert(nvarchar(50),Ex.EventYear)+'-'+substring(Convert(nvarchar(50),Ex.Eventyear+1),3,2)  as EventYear, Ex.EventID, Ex.EventCode, Ex.CMemberID, Ex.ChildNumber, Ex.ProductGroupID, Ex.ProductGroupCode, Ex.ProductID, Ex.ProductCode, Ex.Semester, Ex.NewDeadline, Ex.CreateDate, Ex.CreatedBy, Ex.ModifyDate, Ex.ModifiedBy,I.FirstName + ' ' + I.LastName as CoachName, I.Email, I2.FirstName+ ' ' + I2.LastName as ParentName, I2.AutomemberId as ParentID, I2.Email as ParentEmail,  case when Ex.AdultId is null then  C.FIRST_NAME + ' ' + C.LAST_NAME else IP.FirstName+' '+IP.LastName end as ChildName, Isnull(CR.Approved,'N') as Paid,  case when Ex.AdultId is null then 'Child' else 'Adult' end as Type  FROM ExCoaching Ex INNER JOIN IndSpouse I ON I.AutoMemberID=Ex.CMemberID left JOIN Child C ON C.ChildNumber = Ex.ChildNumber left join Indspouse IP on (IP.AutoMemberId=Ex.AdultId) Inner Join IndSpouse I2 on Ex.PMemberId=I2.AutoMemberId Left Join CoachReg  CR ON CR.ChildNumber=C.ChildNumber AND CR.ProductId=Ex.ProductId  AND  CR.EventId=Ex.EventID AND CR.EventYear=Ex.EventYear WHERE Ex.EventID=" & ddlEvent.SelectedValue & " and Ex.EventYear=" & ddlEventYear.SelectedValue & " order by  EX.CreateDate DESC, I2.FirstName,I2.LastName ASC "
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        gvExCoaching.DataSource = dt
        gvExCoaching.DataBind()
        gvExCoaching.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
            gvExCoaching.Visible = False
        Else
            Pnl3Msg.Text = ""
            gvExCoaching.Visible = True
        End If
    End Sub

    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String, Type As String)
        clear()
        hlblExContestantID.Text = transID
        Dim strsql As String = "SELECT Ex.ExCoachingID, Ex.CMemberId, Ex.EventYear, Ex.EventID, Ex.PMemberID, case when Ex.AdultId is null then Ex.ChildNumber else Ex.AdultId end as ChildNumber, Ex.ProductGroupID, Ex.ProductGroupCode, Ex.ProductID, Ex.Semester, Ex.ProductCode, Ex.NewDeadline, Ex.CreateDate, Ex.CreatedBy, Ex.ModifyDate, Ex.ModifiedBy,I.FirstName + ' ' + I.LastName as CoachName, "
        If (Type = "Adult") Then
            strsql = strsql + " IP.FirstName +' '+ IP.LastName as ChildName,"
        Else
            strsql = strsql + "  C.FIRST_NAME + ' ' + C.LAST_NAME as ChildName,"
        End If


        strsql = strsql + "  I2.FirstName + ' ' + I2.LastName as ParentName  FROM ExCoaching Ex INNER JOIN IndSpouse I ON I.AutoMemberID=Ex.CMemberID "
        If (Type = "Adult") Then
            strsql = strsql & " INNER JOIN Indspouse IP ON IP.AutoMemberId = Ex.AdultId"
        Else
            strsql = strsql & " INNER JOIN Child C ON C.ChildNumber = Ex.ChildNumber"
        End If
        strsql = strsql & "  inner join IndSpouse I2 on I2.AutomemberId= Ex.PMemberId WHERE ExcoachingID=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        If dsRecords.Tables(0).Rows.Count > 0 Then
            ddlSemester.SelectedValue = dsRecords.Tables(0).Rows(0)("Semester").ToString()
            LoadProductGroup()
            ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ProductGroupID").ToString()))
            LoadProduct()
            ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ProductID").ToString()))
            LoadCoach()
            ddlCoach.SelectedValue = dsRecords.Tables(0).Rows(0)("CMemberId").ToString()
            HlblParentID.Text = dsRecords.Tables(0).Rows(0)("PMemberid").ToString()
            txtParent.Text = dsRecords.Tables(0).Rows(0)("ParentName").ToString() 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select     FirstName + ' ' + LastName as name FROM IndSpouse WHERE AutoMemberID = " & HlblParentID.Text & "")
            Loadchild(HlblParentID.Text, "N")
            ddlChild.SelectedIndex = ddlChild.Items.IndexOf(ddlChild.Items.FindByValue(dsRecords.Tables(0).Rows(0)("ChildNumber").ToString()))
            'ddlChild.Enabled = False

            'ddlProduct.Enabled = False
            txtDeadline.Text = Date.Parse(dsRecords.Tables(0).Rows(0)("NewDeadline").ToString())
        End If
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
        buttonAdd.Text = "Add"
    End Sub
    Private Sub clear()
        lblErr.Text = ""
        Pnl3Msg.Text = ""
        HlblParentID.Text = 0
        hlblExContestantID.Text = 0
        txtParent.Text = String.Empty
        txtDeadline.Text = String.Empty
        ddlChild.Items.Clear()
        ddlProduct.Items.Clear()
        ddlProductGroup.Items.Clear()
        'LoadCoach()

    End Sub

    Protected Sub gvExCoaching_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles gvExCoaching.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim TransID As Integer = e.Item.Cells(2).Text
        Dim Type As String = e.Item.Cells(23).Text
        If (cmdName = "Delete") Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from ExCoaching where ExCoachingID=" & TransID & "")
            lblErr.Text = "Item Deleted"
            clear()
            GetRecords()
        Else
            buttonAdd.Text = "Update"
            If (TransID) And TransID > 0 Then
                GetSelectedRecord(TransID, e.CommandName.ToString(), Type)
            End If
        End If
    End Sub


    Protected Sub gvExCoaching_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles gvExCoaching.ItemDataBound
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            Dim btn1 As Button = Nothing
            btn1 = CType(e.Item.Cells(0).Controls(0), Button)
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            'm sqlstr As String = "select COUNT(Ex.ExContestID) from ExContestant Ex Inner Join Contestant C on C.ChildNumber = ex.ChildNumber and C.PaymentReference is Not Null and c.ParentID = ex.MemberID and c.ContestYear = ex.ContestYear and c.EventId = ex.EventID and c.ProductId = ex.ProductID and Ex.ExContestID=" & e.Item.Cells(2).Text
            'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlstr)

            '\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\

            'If DataBinder.Eval(e.Item.DataItem, "cnt") > 0 Then
            '    'btn.Enabled = False
            '    'btn1.Enabled = False
            'Else
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            '  End If
        End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        If ddlProductGroup.SelectedValue.ToString <> String.Empty Then
            LoadProduct()
            LoadCoach()
        End If

    End Sub
    Protected Sub ddlEventYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlEventYear.SelectedIndexChanged
        If ddlEventYear.SelectedValue.ToString <> String.Empty Then
            GetRecords()
        End If

    End Sub

    Protected Sub buttonAdd_Click(sender As Object, e As EventArgs) Handles buttonAdd.Click
        Try

            Dim cmdText As String

            If buttonAdd.Text.ToString().Equals("Add") Then

                cmdText = "select count(*) from ExCoaching where CMemberId=" & ddlCoach.SelectedValue & " and PMemberId=" & HlblParentID.Text & " and ChildNumber=" & ddlChild.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and  EventYear=" & ddlEventYear.SelectedValue & "  and ProductId = " & ddlProduct.SelectedValue & " and Semester='" & ddlSemester.SelectedValue & "'"

                If HlblParentID.Text = "" Then
                    lblErr.Text = "Please search for Parent Using Search Button"
                ElseIf ddlChild.SelectedValue.ToString = String.Empty Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlChild.Items.Count < 1 Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlProductGroup.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Coach Available"
                ElseIf ddlProduct.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Coach Available"
                    'ElseIf ddlProduct.Items(0).Selected = True Then
                    '    lblErr.Text = "Please select a Product"
                ElseIf txtDeadline.Text = "" Or IsDate(txtDeadline.Text) <> True Then
                    lblErr.Text = "Please enter new deadline date in mm/dd/yyyy format"
                ElseIf Date.Parse(txtDeadline.Text) < Now.Date Then
                    lblErr.Text = "Deadline date Entered was Passed"
                ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) Then
                    lblErr.Text = "Data Already Exists"
                Else
                    Dim ChildNumber As String = String.Empty
                    Dim AdultId As String = String.Empty
                    Dim IsAdult As String = ddlChild.SelectedItem.Text.Split("-")(1).ToString()
                    If (IsAdult.Trim() = "Adult") Then
                        cmdText = "Insert into ExCoaching(CMemberID, PMemberId, AdultId, EventYear, EventID, EventCode, ProductGroupID, ProductGroupCode, ProductID, ProductCode, NewDeadline, CreateDate, CreatedBy, Semester) Values ("
                        cmdText = cmdText & ddlCoach.SelectedValue & "," & HlblParentID.Text & " ," & ddlChild.SelectedValue & " ," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "','" & txtDeadline.Text & "',GetDate()," & Session("LoginID") & ", '" & ddlSemester.SelectedValue & "')"
                    Else
                        cmdText = "Insert into ExCoaching(CMemberID, PMemberId, ChildNumber, EventYear, EventID, EventCode, ProductGroupID, ProductGroupCode, ProductID, ProductCode, NewDeadline, CreateDate, CreatedBy, Semester) Values ("
                        cmdText = cmdText & ddlCoach.SelectedValue & "," & HlblParentID.Text & " ," & ddlChild.SelectedValue & " ," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "','" & txtDeadline.Text & "',GetDate()," & Session("LoginID") & ", '" & ddlSemester.SelectedValue & "')"
                    End If


                    lblErr.Text = ""
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    clear()
                    GetRecords()
                    gvExCoaching.Items(0).BackColor = Color.Aquamarine
                    lblErr.Text = "Inserted Successfully"

                End If

            Else

                cmdText = "select count(*) from ExCoaching where CMemberId <> " & ddlCoach.SelectedValue & " and  PMemberId=" & HlblParentID.Text & " and ChildNumber=" & ddlChild.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and  EventYear=" & ddlEventYear.SelectedValue & "  and ProductId = " & ddlProduct.SelectedValue & " and Semester='" & ddlSemester.SelectedValue & "'"

                If HlblParentID.Text = "" And hlblExContestantID.Text = "" Then
                    lblErr.Text = "Please select once again"
                ElseIf ddlChild.SelectedValue.ToString = String.Empty Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlChild.Items.Count < 1 Then
                    lblErr.Text = "Please select a Valid Child"
                ElseIf ddlProductGroup.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Coach Available"
                ElseIf ddlProduct.Items.Count < 1 Then
                    lblErr.Text = "Sorry, No Eligible Coach Available"
                ElseIf txtDeadline.Text = "" Or IsDate(txtDeadline.Text) <> True Then
                    lblErr.Text = "Please enter new deadline date in mm/dd/yyyy format"
                ElseIf Date.Parse(txtDeadline.Text) < Now.Date Then
                    lblErr.Text = "Deadline date Entered was Passed"
                ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText).ToString() > 0 Then
                    lblErr.Text = "Data already Exist"
                Else

                    cmdText = "UPDATE ExCoaching SET PMemberId=" & HlblParentID.Text & ", ChildNumber=" & ddlChild.SelectedValue & ", EventYear=" & ddlEventYear.SelectedValue & ", EventID=" & ddlEvent.SelectedValue & ", EventCode='" & ddlEvent.SelectedItem.Text & "', ProductGroupID=" & ddlProductGroup.SelectedValue & ", ProductGroupCode='" & ddlProductGroup.SelectedItem.Text & "', ProductID=" & ddlProduct.SelectedValue & ", ProductCode='" & ddlProduct.SelectedItem.Text & "', Semester='" & ddlSemester.SelectedValue & "', NewDeadline='" & txtDeadline.Text & "',ModifyDate = GetDate(),CreatedBy=" & Session("LoginID")
                    cmdText = cmdText & " where ExCoachingId= " & hlblExContestantID.Text
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                    clear()
                    GetRecords()
                    gvExCoaching.Items(0).BackColor = Color.Aquamarine
                    buttonAdd.Text = "Add"
                    lblErr.Text = "Updated Successfully"
                End If
            End If
        Catch ex As Exception
            ' lblErr.Text = lblErr.Text & " <br> " & ex.ToString()
        End Try

    End Sub

    Private Sub loadSemesters()
        ddlSemester.Items.Clear()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2
            ddlSemester.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlSemester.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadProductGroup()
        LoadCoach()
        GetRecords()
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProduct.SelectedIndexChanged
        LoadCoach()
    End Sub
End Class
