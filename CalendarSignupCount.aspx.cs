﻿using System;
using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;
using System.Drawing;
using NativeExcel;
using System.Threading;

public partial class CalendarSignupCount : System.Web.UI.Page
{
    int StartYear;
    int EndYear;
    bool IsFlag = false;
    int MaxYear = DateTime.Now.Year;
    string retFlag = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = "4240";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            Session["CalendarSignupCount"] = null;
            lblerr.Text = string.Empty;
            YearConditions();
            PivotTable();
            PopulateYear(ddlFromYear);
            PopulateYear(ddlToYear);

        }
        else
        {
            PivotTable();
        }
    }
    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            ArrayList list = new ArrayList();
            for (int i = MaxYear; i >= 2010; i--)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));

            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("All", "0"));
            ddlObject.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }
    }

    protected void PopulateToYear()
    {
        try
        {
            ArrayList list = new ArrayList();

            for (int i = 2011; i <= MaxYear; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlToYear.Items.Clear();
            ddlToYear.DataSource = list;
            ddlToYear.DataTextField = "Text";
            ddlToYear.DataValueField = "Value";
            ddlToYear.DataBind();
            ddlToYear.Items.Insert(0, new ListItem("All", "0"));
            ddlToYear.Items.Insert(0, new ListItem("Select Year", "-1"));
        }
        catch (Exception ex) { }
    }

    protected void btnsubmit_Click(object sender, System.EventArgs e)
    {
        try
        {
            dropdownCondition();
        }
        catch (Exception err)
        {
        }
    }
    protected void PivotTable()
    {
        try
        {
            BtnExpo.Visible = false;
            string WCNT = string.Empty;
            string am = string.Empty;
            string Tot = string.Empty;
            lblerr.Text = string.Empty;

            string coachingYear = string.Empty;
            coachingYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            string previousYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CalSignup where EventId=13").ToString();
            previousYear = (Convert.ToInt32(previousYear) - 1).ToString();

            YearConditions();


            for (int i = StartYear; i <= EndYear; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                    Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                    am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                }
            }
            string Qrycondition = "with tbl as(select c.EventYear as Year, c.productgroupcode as ProductGroup,c.ProductCode as Product, c.Level, c.Semester,c.ProductGroupID,c.ProductID,"
         + " COUNT(Distinct(MemberID)) as Signups,'' as Accepted from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 and e.Eventyear=c.eventyear "
           + " and e.productGroupid=c.productgroupid and e.productid=c.productid where c.Eventyear in (" + WCNT.Replace("[", "").Replace("]", "") + ") and C.Level is not null Group by c.EventYear, c.productgroupcode, c.ProductCode, c.Level, c.Semester,c.ProductGroupID,c.ProductID union (select p.EventYear as Year, p.productgroupcode as ProductGroup,p.ProductCode as Product, p.Levelcode as Level, '' as Semester,p.ProductGroupID,p.ProductID,'' as Signups,'' as Accepted from prodlevel p where p.levelcode not in (select  c.level from Calsignup c inner join eventfees e on c.eventid=e.eventid and c.eventid=13 and  p.productid=c.productid))) "
        + " select ProductGroup,Product,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'"
         + "when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' when Level='One Level'then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,ProductGroupID,ProductID, Semester " + am + " ,'Accepted'=Accepted, 'Not Accepted'=Accepted, '" + previousYear + " Accepted'=Accepted, '" + previousYear + " Registrations'=Accepted"
         + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by productgroupId,ProductId,level  ";

            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            DataTable dtCount = dsCount.Tables[0];

            string QryAccepted = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + coachingYear + "  and Accepted='Y'  Group by EventYear, productgroupcode,ProductCode,ProductGroupID,ProductID,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";

            DataSet dsCountProdAccep = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, QryAccepted);
            DataTable dtCountProdAcce = dsCountProdAccep.Tables[0];
            for (int i = 0; i < dtCountProdAcce.Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dtCountProdAcce.Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dtCountProdAcce.Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dtCountProdAcce.Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dtCountProdAcce.Rows[i]["Semester"].ToString().Trim() == dtCount.Rows[j]["Semester"].ToString().Trim()))
                    {

                        dtCount.Rows[j]["Accepted"] = dtCountProdAcce.Rows[i]["Signup"];
                    }

                }
            }

            string PrevyrAcceptedCount = string.Empty;
            PrevyrAcceptedCount = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + previousYear + "  and Accepted='Y'  Group by EventYear, productgroupcode,ProductCode,ProductGroupID,ProductID,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";
            DataSet dsPrevAccCount = new DataSet();
            dsPrevAccCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, PrevyrAcceptedCount);
            for (int i = 0; i < dsPrevAccCount.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dsPrevAccCount.Tables[0].Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dsPrevAccCount.Tables[0].Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dsPrevAccCount.Tables[0].Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dsPrevAccCount.Tables[0].Rows[i]["Semester"].ToString().Trim() == dtCount.Rows[j]["Semester"].ToString().Trim()))
                    {

                        dtCount.Rows[j][previousYear + " Accepted"] = dsPrevAccCount.Tables[0].Rows[i]["Signup"];
                    }

                }
            }

            string PrevyrReg = string.Empty;
            PrevyrAcceptedCount = "select ProductGroupCode, ProductCode,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester ,count(*) Reg  from coachreg where eventyear=2016 and paymentReference is not null Group by ProductGroupID, ProductGroupCode, ProductID, ProductCode,Level,Semester order by ProductGroupID, ProductID,Level";
            DataSet dsPrevYrReg = new DataSet();
            dsPrevYrReg = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, PrevyrAcceptedCount);
            for (int i = 0; i < dsPrevYrReg.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dsPrevYrReg.Tables[0].Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dsPrevYrReg.Tables[0].Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dsPrevYrReg.Tables[0].Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dsPrevYrReg.Tables[0].Rows[i]["Semester"].ToString().Trim() == dtCount.Rows[j]["Semester"].ToString().Trim()))
                    {

                        dtCount.Rows[j][previousYear + " Registrations"] = dsPrevYrReg.Tables[0].Rows[i]["Reg"];
                    }

                }
            }


            QryAccepted = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode ,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + coachingYear + "  and Accepted is null  Group By EventYear, productgroupcode,ProductCode,ProductGroupID,ProductID,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";
            dsCountProdAccep = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, QryAccepted);
            dtCountProdAcce = dsCountProdAccep.Tables[0];
            for (int i = 0; i < dtCountProdAcce.Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {
                    // Temporary hidden by Sims

                    if ((dtCountProdAcce.Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dtCountProdAcce.Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dtCountProdAcce.Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dtCountProdAcce.Rows[i]["Semester"].ToString() == dtCount.Rows[j]["Semester"].ToString()))
                    {
                        string level = dtCount.Rows[j]["Level"].ToString();

                        if (level != "")
                        {
                            level = level.Remove(0, 2);
                        }
                        else
                        {
                            //hdnError.Value = dtCountProdAcce.Rows[i]["Productcode"].ToString();
                        }

                        string cmdCount = "select count(distinct(MemberID)) as MemberCount from CalSignUp cs where Level='" + level + "' and ProductGroupCode='" + dtCountProdAcce.Rows[i]["productgroupcode"].ToString() + "' and ProductCode='" + dtCountProdAcce.Rows[i]["Productcode"].ToString() + "' and Eventyear=" + coachingYear + " and cs.Semester='" + dtCount.Rows[j]["Semester"].ToString() + "' and  Accepted is null and MemberID not in (select distinct(MemberID) from CalSignUp where Level='" + level.Trim() + "' and ProductGroupCode='" + dtCountProdAcce.Rows[i]["productgroupcode"].ToString() + "' and ProductCode='" + dtCountProdAcce.Rows[i]["Productcode"].ToString() + "' and Eventyear=" + coachingYear + " and Semester='" + dtCount.Rows[j]["Semester"].ToString() + "' and  Accepted='Y')";

                        int count = 0;
                        DataSet dsCnt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdCount);
                        if (null != dsCnt && dsCnt.Tables != null)
                        {
                            if (dsCnt.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(dsCnt.Tables[0].Rows[0]["MemberCount"].ToString());
                            }
                        }
                        if (count > 0)
                        {
                            dtCount.Rows[j]["Not Accepted"] = count;
                        }
                    }


                }
            }
            GVcount.Columns.Clear();
            // bound grid dynamically

            BoundField bfield = new BoundField();
            bfield.HeaderText = "ProductGroup";
            bfield.DataField = "ProductGroup";
            GVcount.Columns.Add(bfield);

            bfield = new BoundField();
            bfield.HeaderText = "Product";
            bfield.DataField = "Product";
            GVcount.Columns.Add(bfield);

            bfield = new BoundField();
            bfield.HeaderText = "Level";
            bfield.DataField = "Level";
            GVcount.Columns.Add(bfield);

            bfield = new BoundField();
            bfield.HeaderText = "Semester";
            bfield.DataField = "Semester";
            GVcount.Columns.Add(bfield);

            for (int i = StartYear; i <= EndYear; i++)
            {

                bfield = new BoundField();
                bfield.HeaderText = i.ToString();
                bfield.DataField = i.ToString();
                GVcount.Columns.Add(bfield);
            }


            TemplateField tfield = new TemplateField();
            tfield.HeaderText = "Accepted";
            GVcount.Columns.Add(tfield);

            tfield = new TemplateField();
            tfield.HeaderText = "Not Accepted";
            GVcount.Columns.Add(tfield);


            tfield = new TemplateField();
            tfield.HeaderText = previousYear + " Accepted";
            GVcount.Columns.Add(tfield);


            tfield = new TemplateField();
            tfield.HeaderText = previousYear + " Registrations";
            GVcount.Columns.Add(tfield);



            string Qry = " select distinct c.productcode,c.productgroupcode,c.productgroupID,c.ProductID from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 order by ProductGroupID,ProductID ASC";
            DataSet dsCountProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtCountProduct = dsCountProduct.Tables[0];
            DataTable dtnew = dtCount.Clone();
            for (int i = 0; i <= dtCountProduct.Rows.Count - 1; i++)
            {
                DataRow dr = null;
                string StrProdut2 = dtCountProduct.Rows[i]["productcode"].ToString();
                DataRow[] drs = dtCount.Select("product='" + StrProdut2 + "'");

                if (drs.Length > 0)
                {
                    dr = dtnew.NewRow();
                    dr[0] = "";
                    foreach (DataRow drnew in drs)
                    {
                        dtnew.ImportRow(drnew);
                    }
                }
                else
                {
                    continue;
                }
                for (int j = 6; j <= dtCount.Columns.Count - 1; j++)
                {
                    int sum = 0;
                    foreach (DataRow drnew in drs)
                    {
                        if (!DBNull.Value.Equals(drnew[j]))
                        {
                            string st = drnew[j].ToString();
                            if (st != "")
                            {
                                sum += Convert.ToInt32(drnew[j]);
                            }

                        }
                    }
                    dr[j] = sum;
                }
                dtnew.Rows.Add(dr);
            }
            DataRow drcont = dtnew.NewRow();
            for (int i = 6; i <= dtCount.Columns.Count - 1; i++)
            {
                drcont[i] = 0;
                int sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtCount.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToInt32(drnew[i]);
                        }
                    }
                }
                drcont[i] = sum;
            }
            drcont[0] = "Total";
            dtnew.Rows.Add(drcont);
            if (dtCount.Rows.Count > 0)
            {
                BtnExpo.Visible = true;
                Session["CalendarSignupCount"] = dtnew;
                GVcount.Visible = true;
                GVcount.DataSource = dtnew;
                GVcount.DataBind();
                BindAccepted(WCNT, am, "true");
                showhideGridHeader();
            }
            else
            {
                BtnExpo.Visible = false; ;
                lblerr.Text = "No records found";
            }
        }
        catch (Exception err)
        {

            throw new Exception(err.Message);
        }
    }

    void BindAccepted(string WCNT1, string am1, string IsBind)
    {
        string Qrycondition = "with tbl as(select c.EventYear as Year, c.productgroupcode as ProductGroup,c.ProductCode as Product, c.Level, c.Semester,"
       + " COUNT(Distinct(MemberID)) Signups ,c.ProductGroupID,c.ProductID from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 and e.Eventyear=c.eventyear and e.productGroupid=c.productgroupid and e.productid=c.productid where C.Eventyear in (" + WCNT1.Replace("[", "").Replace("]", "") + ") and Accepted='Y' Group by c.EventYear, c.ProductGroupID,c.ProductID,c.productgroupcode,c.productcode,  Level, c.Semester  union (select p.EventYear as Year, p.productgroupcode as ProductGroup, p.ProductCode as Product, p.Levelcode as Level, '' as Semester, '' as Signups,p.ProductGroupID,p.ProductID  from prodlevel p where p.levelcode not in (select c.level from Calsignup c inner join eventfees e on c.eventid=e.eventid and c.eventid=13 and  p.productid=c.productid))) "
      + " select ProductGroup,Product,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'"
       + "when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' when Level='One Level'then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level, Semester " + am1
       + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT1 + "))As PVTTBL order by ProductGroupID,ProductID,level  ";

        DataSet dsAccept = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
        DataTable dtAccepted = dsAccept.Tables[0];
        gvAccepted.Columns.Clear();
        // bound grid dynamically

        //BoundField bfield = new BoundField();
        //bfield.HeaderText = "ProductGroup";
        //bfield.DataField = "ProductGroup";
        //gvAccepted.Columns.Add(bfield);

        //bfield = new BoundField();
        //bfield.HeaderText = "Product";
        //bfield.DataField = "Product";
        //gvAccepted.Columns.Add(bfield);

        //bfield = new BoundField();
        //bfield.HeaderText = "Level";
        //bfield.DataField = "Level";
        //gvAccepted.Columns.Add(bfield);

        //bfield = new BoundField();
        //bfield.HeaderText = "Semester";
        //bfield.DataField = "Semester";
        //gvAccepted.Columns.Add(bfield);

        //for (int i = StartYear; i <= EndYear; i++)
        //{

        //    bfield = new BoundField();
        //    bfield.HeaderText = i.ToString();
        //    bfield.DataField = i.ToString();
        //    gvAccepted.Columns.Add(bfield);
        //}
        string Qry = " select distinct c.productcode,c.productgroupcode,c.productgroupID,c.ProductID from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 order by ProductGroupID,ProductID ASC";
        DataSet dsCountProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
        DataTable dtCountProduct = dsCountProduct.Tables[0];

        DataTable dtnew = dtAccepted.Clone();
        for (int i = 0; i <= dtCountProduct.Rows.Count - 1; i++)
        {
            DataRow dr = null;
            string StrProdut2 = dtCountProduct.Rows[i]["productcode"].ToString();
            DataRow[] drs = dtAccepted.Select("product='" + StrProdut2 + "'");

            if (drs.Length > 0)
            {
                dr = dtnew.NewRow();
                dr[0] = "";
                foreach (DataRow drnew in drs)
                {
                    dtnew.ImportRow(drnew);
                }
            }
            else
            {
                continue;
            }
            for (int j = 4; j <= dtAccepted.Columns.Count - 1; j++)
            {
                int sum = 0;
                foreach (DataRow drnew in drs)
                {
                    if (!DBNull.Value.Equals(drnew[j]))
                    {
                        string st = drnew[j].ToString();
                        if (st != "")
                        {
                            sum += Convert.ToInt32(drnew[j]);
                        }

                    }
                }
                dr[j] = sum;
            }
            dtnew.Rows.Add(dr);
        }
        DataRow drcont = dtnew.NewRow();
        for (int i = 4; i <= dtAccepted.Columns.Count - 1; i++)
        {
            drcont[i] = 0;
            int sum = 0;
            string suma = "";
            foreach (DataRow drnew in dtAccepted.Rows)
            {
                if (!DBNull.Value.Equals(drnew[i]))
                {
                    suma = drnew[i].ToString();
                    if (suma != "")
                    {
                        sum += Convert.ToInt32(drnew[i]);
                    }
                }
            }
            drcont[i] = sum;
        }
        drcont[0] = "Total";
        dtnew.Rows.Add(drcont);
        if (dtnew.Rows.Count > 0)
        {
            Session["CalendarAcceptedCount"] = dtnew;
            if (IsBind == "true")
            {
                gvAccepted.DataSource = dtnew;
                gvAccepted.DataBind();
            }
        }
    }
    protected void YearConditions()
    {
        string SqlStr = "select Distinct min(eventyear) as MInYear,MAX(eventyear) as MaxYear from calsignup";
        DataSet dsRecordsYear = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlStr);
        DataTable dt = dsRecordsYear.Tables[0];
        // Condition given by Bindhu on Aug27_2016 
        // To set limitation when the From /To year is selected
        if (!isYearSelected)
        {
            StartYear = Convert.ToInt16(dt.Rows[0]["MInYear"].ToString());
            EndYear = Convert.ToInt16(dt.Rows[0]["MaxYear"].ToString());
        }
    }
    bool isYearSelected;
    protected void dropdownCondition()
    {
        isYearSelected = false;
        if (((ddlFromYear.SelectedItem.Text == "Select Year") && (ddlToYear.SelectedItem.Text == "Select Year")) || ((ddlFromYear.SelectedItem.Text == "All") && (ddlToYear.SelectedItem.Text == "All")))
        {
            YearConditions();
            PivotTable();
        }
        else
        {
            if (((ddlFromYear.SelectedItem.Text == "Select Year") || (ddlToYear.SelectedItem.Text == "Select Year")) || ((ddlFromYear.SelectedItem.Text == "All") || (ddlToYear.SelectedItem.Text == "All")))
            {
                if ((ddlFromYear.SelectedItem.Text == "All") || (ddlToYear.SelectedItem.Text == "All"))
                {
                    ddlFromYear.SelectedValue = "0";
                    ddlToYear.SelectedValue = "0";
                    YearConditions();
                    PivotTable();
                }
                else
                {
                    lblerr.Text = "Please Select both years";
                    GVcount.Visible = false;
                    BtnExpo.Visible = false;
                }
            }
            else
            {
                lblerr.Text = string.Empty;
                if (Convert.ToInt16(ddlFromYear.SelectedValue) > Convert.ToInt16(ddlToYear.SelectedValue))
                {
                    lblerr.Text = "Please select less From year";
                    GVcount.Visible = false;
                    BtnExpo.Visible = false;
                }
                else
                {
                    lblerr.Text = "";
                    StartYear = Convert.ToInt16(ddlFromYear.SelectedValue);
                    EndYear = Convert.ToInt16(ddlToYear.SelectedValue);
                    // Added by Bindhu on Aug27_2016 To set limitation when the From /To year is selected
                    isYearSelected = true;
                    PivotTable();
                    IsFlag = true;

                }
            }
        }

    }
    protected void BtnExpo_Click(object sender, System.EventArgs e)
    {
        try
        {
            BtnExpo.Visible = false;
            string WCNT = string.Empty;
            string am = string.Empty;
            string Tot = string.Empty;
            lblerr.Text = string.Empty;

            int iStartYear = 0;
            int iEndYear = 0;
            isYearSelected = false;
            if (ddlFromYear.SelectedValue == "0" || ddlFromYear.SelectedValue == "-1")
            {
                YearConditions();
                iStartYear = StartYear;
                iEndYear = EndYear;
            }
            else
            {
                isYearSelected = true;
                iStartYear = Convert.ToInt32(ddlFromYear.SelectedValue);
                iEndYear = Convert.ToInt32(ddlToYear.SelectedValue);
            }

            for (int i = iStartYear; i <= iEndYear; i++)
            {
                if (WCNT != string.Empty)
                {
                    WCNT = WCNT + ",[" + i.ToString() + "]";
                    am = am + ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                    Tot = Tot + "+isnull(PVTTBL.[" + i.ToString() + "],0)";
                }
                else
                {
                    WCNT = "[" + i.ToString() + "]";
                    Tot = "isnull(PVTTBL.[" + i.ToString() + "],0)";
                    am = ",CONVERT(varchar(50),PVTTBL.[" + i.ToString() + "]) AS [" + i.ToString() + "]";
                }
            }

            string previousYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CalSignup where EventId=13").ToString();
            previousYear = (Convert.ToInt32(previousYear) - 1).ToString();

            // string Qrycondition = "with tbl as(select EventYear as Year, productgroupcode as ProductGroup,ProductCode as Product, Level, Semester,"
            // + "COUNT(Distinct(MemberID))Signups,'' as Accepted from calsignup   Group by EventYear, productgroupcode, ProductCode, Level, Semester )"
            //+ " select ProductGroup,Product,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'"
            // + "when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' else level end as Level, Semester " + am + " ,'Accepted'=Accepted, 'Not Accepted'=Accepted"
            // + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by ProductGroup,product,level ";
            string Qrycondition = "with tbl as(select c.EventYear as Year, c.productgroupcode as ProductGroup,c.ProductCode as Product, c.Level, c.Semester,c.ProductGroupID,c.ProductID,"
        + " COUNT(Distinct(MemberID)) as Signups,'' as Accepted from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 and e.Eventyear=c.eventyear "
          + " and e.productGroupid=c.productgroupid and e.productid=c.productid where c.Eventyear in (" + WCNT.Replace("[", "").Replace("]", "") + ") and C.Level is not null Group by c.EventYear, c.productgroupcode, c.ProductCode, c.Level, c.Semester,c.ProductGroupID,c.ProductID union (select p.EventYear as Year, p.productgroupcode as ProductGroup,p.ProductCode as Product, p.Levelcode as Level, '' as Semester,p.ProductGroupID,p.ProductID,'' as Signups,'' as Accepted from prodlevel p where p.levelcode not in (select  c.level from Calsignup c inner join eventfees e on c.eventid=e.eventid and c.eventid=13 and  p.productid=c.productid))) "
       + " select ProductGroup,Product,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'"
        + "when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,ProductGroupID,ProductID, Semester " + am + " ,'Accepted'=Accepted, 'Not Accepted'=Accepted, '" + previousYear + " Accepted'=Accepted, '" + previousYear + " Registrations'=Accepted"
        + " from tbl pivot(sum(Signups) for [Year] in (" + WCNT + "))As PVTTBL order by ProductGroupID,ProductID,level  ";
            DataSet dsCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qrycondition);
            DataTable dtCount = dsCount.Tables[0];
            string coachingYear = string.Empty;
            coachingYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            string QryAccepted = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + coachingYear + "  and Accepted='Y'  Group by EventYear, productgroupcode,ProductCode,ProductGroupID,ProductID,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";

            DataSet dsCountProdAccep = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, QryAccepted);
            DataTable dtCountProdAcce = dsCountProdAccep.Tables[0];
            for (int i = 0; i < dtCountProdAcce.Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dtCountProdAcce.Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dtCountProdAcce.Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dtCountProdAcce.Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dtCountProdAcce.Rows[i]["Semester"].ToString() == dtCount.Rows[j]["Semester"].ToString()))
                    {

                        dtCount.Rows[j]["Accepted"] = dtCountProdAcce.Rows[i]["Signup"];
                    }

                }
            }

            string PrevyrAcceptedCount = string.Empty;
            PrevyrAcceptedCount = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + previousYear + "  and Accepted='Y'  Group by EventYear, productgroupcode,ProductCode,ProductGroupID,ProductID,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";
            DataSet dsPrevAccCount = new DataSet();
            dsPrevAccCount = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, PrevyrAcceptedCount);
            for (int i = 0; i < dsPrevAccCount.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dsPrevAccCount.Tables[0].Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dsPrevAccCount.Tables[0].Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dsPrevAccCount.Tables[0].Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dsPrevAccCount.Tables[0].Rows[i]["Semester"].ToString().Trim() == dtCount.Rows[j]["Semester"].ToString().Trim()))
                    {

                        dtCount.Rows[j][previousYear + " Accepted"] = dsPrevAccCount.Tables[0].Rows[i]["Signup"];
                    }

                }
            }

            string PrevyrReg = string.Empty;
            PrevyrAcceptedCount = "select ProductGroupCode, ProductCode,case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester ,count(*) Reg  from coachreg where eventyear=2016 and paymentReference is not null Group by ProductGroupID, ProductGroupCode, ProductID, ProductCode,Level,Semester order by ProductGroupID, ProductID,Level";
            DataSet dsPrevYrReg = new DataSet();
            dsPrevYrReg = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, PrevyrAcceptedCount);
            for (int i = 0; i < dsPrevYrReg.Tables[0].Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {

                    if ((dsPrevYrReg.Tables[0].Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dsPrevYrReg.Tables[0].Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dsPrevYrReg.Tables[0].Rows[i]["Level"].ToString().Trim() == dtCount.Rows[j]["Level"].ToString().Trim()) && (dsPrevYrReg.Tables[0].Rows[i]["Semester"].ToString().Trim() == dtCount.Rows[j]["Semester"].ToString().Trim()))
                    {

                        dtCount.Rows[j][previousYear + " Registrations"] = dsPrevYrReg.Tables[0].Rows[i]["Reg"];
                    }

                }
            }

            // QryAccepted = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior' end as Level,Semester,EventYear from calsignup where Eventyear=" + MaxYear + "  and Accepted is null  Group by EventYear, productgroupcode,ProductCode,Accepted, Level, Semester  order by ProductCode";
            QryAccepted = "select COUNT(Distinct(MemberID)) as Signup,ProductGroupCode,Productcode ,ProductGroupID,ProductID , case when Level='Beginner' then '1.Beginner' when Level='Junior' then '1.Junior' when Level='Intermediate' then '2.Intermediate'when Level='Advanced' then '3.Advanced'  when Level='Senior' then '3.Senior'  when Level='One Level' then '1.One Level' when Level='One Level only' then '1.One Level only' end as Level,Semester,EventYear from calsignup where Eventyear=" + coachingYear + "  and Accepted is null  Group by EventYear, productgroupcode,ProductGroupID,ProductID,ProductCode,Accepted, Level, Semester  order by ProductGroupID,ProductID,level ASC";

            dsCountProdAccep = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, QryAccepted);
            dtCountProdAcce = dsCountProdAccep.Tables[0];
            for (int i = 0; i < dtCountProdAcce.Rows.Count; i++)
            {
                for (int j = 0; j < dtCount.Rows.Count; j++)
                {
                    if ((dtCountProdAcce.Rows[i]["productgroupcode"].ToString() == dtCount.Rows[j]["ProductGroup"].ToString()) && (dtCountProdAcce.Rows[i]["Productcode"].ToString() == dtCount.Rows[j]["Product"].ToString()) && (dtCountProdAcce.Rows[i]["Level"].ToString() == dtCount.Rows[j]["Level"].ToString()) && (dtCountProdAcce.Rows[i]["Semester"].ToString() == dtCount.Rows[j]["Semester"].ToString()))
                    {
                        string level = dtCount.Rows[j]["Level"].ToString();
                        if (level != "")
                        {
                            level = level.Remove(0, 2);
                        }
                        else
                        {

                        }
                        string cmdCount = "select count(distinct(MemberID)) as MemberCount from CalSignUp where Level='" + level + "' and ProductGroupCode='" + dtCountProdAcce.Rows[i]["productgroupcode"].ToString() + "' and ProductCode='" + dtCountProdAcce.Rows[i]["Productcode"].ToString() + "' and Eventyear=" + coachingYear + " and Semester='" + dtCount.Rows[j]["Semester"].ToString() + "' and  Accepted is null and MemberID not in (select distinct(MemberID) from CalSignUp where Level='" + level.Trim() + "' and ProductGroupCode='" + dtCountProdAcce.Rows[i]["productgroupcode"].ToString() + "' and ProductCode='" + dtCountProdAcce.Rows[i]["Productcode"].ToString() + "' and Eventyear=" + coachingYear + " and Semester='" + dtCount.Rows[j]["Semester"].ToString() + "' and  Accepted='Y')";
                        int count = 0;
                        DataSet dsCnt = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdCount);
                        if (null != dsCnt && dsCnt.Tables != null)
                        {
                            if (dsCnt.Tables[0].Rows.Count > 0)
                            {
                                count = Convert.ToInt32(dsCnt.Tables[0].Rows[0]["MemberCount"].ToString());
                            }
                        }
                        if (count > 0)
                        {
                            dtCount.Rows[j]["Not Accepted"] = count;
                        }
                    }

                }
            }

            //string Qry = " select distinct productcode,productgroupcode from calsignup order by productgroupcode";
            string Qry = " select distinct c.productcode,c.productgroupcode,c.productgroupID,c.ProductID from calsignup c left join EventFees e on e.eventid=c.eventid and c.eventid=13 order by ProductGroupID,ProductID";
            DataSet dsCountProduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Qry);
            DataTable dtCountProduct = dsCountProduct.Tables[0];
            DataTable dtnew = dtCount.Clone();
            for (int i = 0; i <= dtCountProduct.Rows.Count - 1; i++)
            {
                DataRow dr = null;
                string StrProdut2 = dtCountProduct.Rows[i]["productcode"].ToString();
                DataRow[] drs = dtCount.Select("product='" + StrProdut2 + "'");

                if (drs.Length > 0)
                {
                    dr = dtnew.NewRow();
                    dr[0] = "";
                    foreach (DataRow drnew in drs)
                    {
                        dtnew.ImportRow(drnew);
                    }
                }
                else
                {
                    continue;
                }
                for (int j = 6; j <= dtCount.Columns.Count - 1; j++)
                {
                    int sum = 0;
                    foreach (DataRow drnew in drs)
                    {
                        if (!DBNull.Value.Equals(drnew[j]))
                        {
                            string st = drnew[j].ToString();
                            if (st != "")
                            {
                                sum += Convert.ToInt32(drnew[j]);
                            }

                        }
                    }
                    dr[j] = sum;
                }
                dtnew.Rows.Add(dr);
            }
            DataRow drcont = dtnew.NewRow();
            for (int i = 6; i <= dtCount.Columns.Count - 1; i++)
            {
                drcont[i] = 0;
                int sum = 0;
                string suma = "";
                foreach (DataRow drnew in dtCount.Rows)
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToInt32(drnew[i]);
                        }
                    }
                }
                drcont[i] = sum;
            }
            drcont[0] = "Total";
            dtnew.Rows.Add(drcont);
            if (dtCount.Rows.Count > 0)
            {

                BtnExpo.Visible = true;
                Session["CalendarSignupCount"] = dtnew;
                BindAccepted(WCNT, am, "false");
            }
            else
            {
                BtnExpo.Visible = false; ;
                lblerr.Text = "No records found";
            }



        }

        catch (Exception err)
        {
            throw new Exception(err.Message);
        }

        // GeneralExport((DataTable)Session["CalendarSignupCount"], "CalendarSignupCount.xls");

        ExportToExcel((DataTable)Session["CalendarSignupCount"], (DataTable)Session["CalendarAcceptedCount"]);
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        string attach = string.Empty;
        attach = "attachment;filename=" + fname;
        Response.ClearContent();
        Response.AddHeader("content-disposition", attach);
        Response.ContentType = "application/vnd.xls";
        if (dtdata != null)
        {
            foreach (DataColumn dc in dtdata.Columns)
            {
                Response.Write(dc.ColumnName + "\t");
                //sep = ";";
            }
            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtdata.Rows)
            {
                for (int i = 0; i < dtdata.Columns.Count; i++)
                {

                    Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                }
                Response.Write("\n");
            }

            // Response.Write(sw.ToString());
            //response.End()                           //       Thread was being aborted.
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }


    public void ExportToExcel(DataTable dtdata, DataTable dtAcceptedData)
    {
        try
        {

            if (dtdata != null)
            {
                IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                IWorksheet oSheet = default(IWorksheet);
                IWorksheet oSheet1 = default(IWorksheet);

                oSheet = oWorkbooks.Worksheets.Add();
                oSheet1 = oWorkbooks.Worksheets.Add();

                oSheet.Name = "Calender Signup Count";
                oSheet.Range["A1:R1"].MergeCells = true;
                oSheet.Range["A1"].Value = "Calendar Signup Count";
                oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet.Range["A1"].Font.Bold = true;
                oSheet.Range["A1"].Font.Color = Color.Black;

                oSheet1.Name = "Accepted Count";
                oSheet1.Range["A1:Q1"].MergeCells = true;
                oSheet1.Range["A1"].Value = "Calender Signup - Accepted Count";
                oSheet1.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                oSheet1.Range["A1"].Font.Bold = true;
                oSheet1.Range["A1"].Font.Color = Color.Black;

                ArrayList ArrAlphapets = new ArrayList();
                ArrAlphapets.Add("A");
                ArrAlphapets.Add("B");
                ArrAlphapets.Add("C");
                ArrAlphapets.Add("D");
                ArrAlphapets.Add("E");
                ArrAlphapets.Add("F");
                ArrAlphapets.Add("G");
                ArrAlphapets.Add("H");
                ArrAlphapets.Add("I");
                ArrAlphapets.Add("J");
                ArrAlphapets.Add("K");
                ArrAlphapets.Add("L");
                ArrAlphapets.Add("M");
                ArrAlphapets.Add("N");
                ArrAlphapets.Add("O");
                ArrAlphapets.Add("P");
                ArrAlphapets.Add("Q");
                ArrAlphapets.Add("R");
                ArrAlphapets.Add("S");
                ArrAlphapets.Add("T");
                ArrAlphapets.Add("U");
                ArrAlphapets.Add("V");
                ArrAlphapets.Add("W");
                ArrAlphapets.Add("X");
                ArrAlphapets.Add("Y");
                ArrAlphapets.Add("Z");
                ArrAlphapets.Add("A1");
                ArrAlphapets.Add("B1");
                ArrAlphapets.Add("C1");
                ArrAlphapets.Add("D1");
                ArrAlphapets.Add("E1");
                ArrAlphapets.Add("F1");
                ArrAlphapets.Add("G1");



                int i = 0;
                foreach (DataColumn dc in dtdata.Columns)
                {
                    oSheet.Range[ArrAlphapets[i].ToString() + "3"].Value = dc.ColumnName + "\t";
                    oSheet.Range[ArrAlphapets[i].ToString() + "3"].Font.Bold = true;

                    i++;

                }
                Response.Write(System.Environment.NewLine);
                int iRowIndex = 4;
                IRange CRange = default(IRange);
                foreach (DataRow dr in dtdata.Rows)
                {
                    for (int j = 0; j < dtdata.Columns.Count; j++)
                    {

                        // Response.Write(dr[j].ToString().Replace("\t", " ") + "\t");


                        CRange = oSheet.Range[ArrAlphapets[j].ToString() + iRowIndex.ToString()];
                        try
                        {
                            //Convert.ToInt32(dr[j].ToString());
                            CRange.Value = Convert.ToInt32(dr[j].ToString());
                        }
                        catch
                        {
                            CRange.Value = dr[j].ToString();
                        }


                    }
                    iRowIndex++;
                }

                if (dtAcceptedData != null)
                {
                    i = 0;
                    foreach (DataColumn dc in dtAcceptedData.Columns)
                    {
                        oSheet1.Range[ArrAlphapets[i].ToString() + "3"].Value = dc.ColumnName + "\t";
                        oSheet1.Range[ArrAlphapets[i].ToString() + "3"].Font.Bold = true;

                        i++;

                    }
                    Response.Write(System.Environment.NewLine);
                    iRowIndex = 4;

                    foreach (DataRow dr in dtAcceptedData.Rows)
                    {
                        for (int j = 0; j < dtAcceptedData.Columns.Count; j++)
                        {

                            // Response.Write(dr[j].ToString().Replace("\t", " ") + "\t");

                            CRange = oSheet1.Range[ArrAlphapets[j].ToString() + iRowIndex.ToString()];
                            try
                            {
                                //Convert.ToInt32(dr[j].ToString());
                                CRange.Value = Convert.ToInt32(dr[j].ToString());
                            }
                            catch
                            {
                                CRange.Value = dr[j].ToString();
                            }

                        }
                        iRowIndex++;
                    }
                }



                DateTime dt = DateTime.Now;
                string month = dt.ToString("MMM");
                string day = dt.ToString("dd");
                string year = dt.ToString("yyyy");
                string monthDay = month + "" + day;

                string filename = "CalendarSignupCount_" + "_" + monthDay + "_" + year + ".xls";

                Response.Clear();
                Response.ContentType = "application/vnd.ms-excel";
                Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                oWorkbooks.SaveAs(HttpContext.Current.Response.OutputStream);

                Response.End();


            }
        }
        catch
        {
        }

    }

    protected void gvAccepted_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            DataRowView drv = e.Row.DataItem as DataRowView;
            if (drv["productgroup"].ToString().Equals(""))
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(194, 240, 191);
            }
        }
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;

            }
        }
    }
    protected void GVcount_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            LinkButton lnkView = new LinkButton();
            lnkView.ID = "lblAccepted";
            lnkView.Text = (e.Row.DataItem as DataRowView).Row["Accepted"].ToString();
            lnkView.CommandName = "SelectAccepted";
            lnkView.Click += lnkClick;
            e.Row.Cells[GVcount.Columns.Count - 4].Controls.Add(lnkView);

            lnkView = new LinkButton();
            lnkView.ID = "lblNotAccepted";
            lnkView.Click += lnkClick;
            lnkView.Text = (e.Row.DataItem as DataRowView).Row["Not Accepted"].ToString();
            lnkView.CommandName = "SelectNotAccepted";
            e.Row.Cells[GVcount.Columns.Count - 3].Controls.Add(lnkView);

            lnkView = new LinkButton();
            lnkView.ID = "lbl2016Accepted";
            lnkView.Click += lnkClick;
            string previousYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CalSignup where EventId=13").ToString();
            previousYear = (Convert.ToInt32(previousYear) - 1).ToString();

            lnkView.Text = (e.Row.DataItem as DataRowView).Row[previousYear + " Accepted"].ToString();
            lnkView.CommandName = "Select2016Accepted";
            e.Row.Cells[GVcount.Columns.Count - 2].Controls.Add(lnkView);

            lnkView = new LinkButton();
            lnkView.ID = "lbl2016Reg";
            lnkView.Click += lnkClick;
            lnkView.Text = (e.Row.DataItem as DataRowView).Row[previousYear + " Registrations"].ToString();
            lnkView.CommandName = "Select2016Reg";
            e.Row.Cells[GVcount.Columns.Count - 1].Controls.Add(lnkView);
            lnkView.ForeColor = Color.Black;
            lnkView.Style.Add("cursor", "default;");
            lnkView.Style.Add("text-decoration", "none;");

            //Label lblView = new Label();
            //lblView.ID = "lbl2016Reg";
            //lblView.Text = (e.Row.DataItem as DataRowView).Row["2016 Accepted"].ToString();
            //e.Row.Cells[GVcount.Columns.Count - 1].Controls.Add(lnkView);

            DataRowView drv = e.Row.DataItem as DataRowView;
            if (drv["productgroup"].ToString().Equals(""))
            {
                e.Row.BackColor = System.Drawing.Color.FromArgb(194, 240, 191);
            }
            if (drv["productgroup"].ToString().Equals("Total"))
            {
                //  e.Row.BackColor = System.Drawing.Color.FromArgb(235, 235, 235);
            }
        }
        for (int i = 3; i < e.Row.Cells.Count; i++)
        {
            if ((!string.IsNullOrEmpty(e.Row.Cells[i].Text)) && (e.Row.Cells[i].Text != "&nbsp;"))
            {
                e.Row.Cells[i].HorizontalAlign = HorizontalAlign.Right;

            }
        }
    }
    protected void Page_Init(object sender, EventArgs e)
    {
        //  GVcount.DataBind();
    }



    void lnkClick(object sender, EventArgs e)
    {
        try
        {

            LinkButton lnk = (LinkButton)sender;

            if (lnk.CommandName == "SelectAccepted")
            {
                Span1.InnerText = "Table2 : Coach List - Accepted";
                hdnStatus.Value = "Accepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                GridCoachList.PageIndex = 0;
                row = (GridViewRow)(((LinkButton)sender).NamingContainer);
                int selIndex = row.RowIndex;

                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }
                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;
                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;
                hdnCoachingYear.Value = CurrentYear;
                loadCoachListAccepted(ProductGroupCode, ProductCode, Level, Semester, CurrentYear);
                dvMemberInfo.Visible = false;
            }
            else if (lnk.CommandName == "SelectNotAccepted")
            {
                dvMemberInfo.Visible = false;
                Span1.InnerText = "Table2 : Coach List - Not Yet Accepted";
                hdnStatus.Value = "NotAccepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                row = (GridViewRow)(((LinkButton)sender).NamingContainer);
                int selIndex = row.RowIndex;
                GridCoachList.PageIndex = 0;
                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }
                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;
                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;
                loadCoachListNotAccepted(ProductGroupCode, ProductCode, Level, Semester);
            }
            else if (lnk.CommandName == "Select2016Accepted")
            {
                Span1.InnerText = "Table2 : Coach List - Accepted";
                hdnStatus.Value = "Accepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                GridCoachList.PageIndex = 0;
                row = (GridViewRow)(((LinkButton)sender).NamingContainer);
                int selIndex = row.RowIndex;

                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }

                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;

                string previousYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CalSignup where EventId=13").ToString();
                previousYear = (Convert.ToInt32(previousYear) - 1).ToString();

                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;
                hdnCoachingYear.Value = previousYear;
                loadCoachListAccepted(ProductGroupCode, ProductCode, Level, Semester, previousYear);
                dvMemberInfo.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GVcount_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "SelectAccepted")
            {
                Span1.InnerText = "Table2 : Coach List - Accepted";
                hdnStatus.Value = "Accepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                GridCoachList.PageIndex = 0;
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }

                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;

                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;
                hdnCoachingYear.Value = CurrentYear;
                loadCoachListAccepted(ProductGroupCode, ProductCode, Level, Semester, CurrentYear);
                dvMemberInfo.Visible = false;

            }
            else if (e.CommandName == "SelectNotAccepted")
            {
                dvMemberInfo.Visible = false;
                Span1.InnerText = "Table2 : Coach List - Not Yet Accepted";
                hdnStatus.Value = "NotAccepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                GridCoachList.PageIndex = 0;
                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }
                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;
                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;

                loadCoachListNotAccepted(ProductGroupCode, ProductCode, Level, Semester);

            }
            else if (e.CommandName == "Select2016Accepted")
            {
                Span1.InnerText = "Table2 : Coach List - Accepted";
                hdnStatus.Value = "Accepted";
                dvCoachList.Visible = true;
                GridViewRow row = null;
                GridCoachList.PageIndex = 0;
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GVcount.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                string ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                string Level = GVcount.Rows[selIndex].Cells[2].Text;

                string Semester = GVcount.Rows[selIndex].Cells[3].Text;
                if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;")
                {
                    ProductGroupCode = GVcount.Rows[selIndex].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex].Cells[1].Text;
                }
                else if (ProductGroupCode == "Total")
                {
                    ProductGroupCode = "Total";
                }
                else
                {
                    ProductGroupCode = GVcount.Rows[selIndex - 1].Cells[0].Text;
                    ProductCode = GVcount.Rows[selIndex - 1].Cells[1].Text;
                }

                hdnProductGroupCode.Value = ProductGroupCode;
                hdnProductCode.Value = ProductCode;

                string previousYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CalSignup where EventId=13").ToString();
                previousYear = (Convert.ToInt32(previousYear) - 1).ToString();

                hdnLevel.Value = Level;
                hdnPhase.Value = Semester;
                hdnCoachingYear.Value = previousYear;
                loadCoachListAccepted(ProductGroupCode, ProductCode, Level, Semester, previousYear);
                dvMemberInfo.Visible = false;
            }
        }
        catch (Exception ex)
        {
        }

    }
    public void loadCoachListAccepted(string ProductGroupCode, string ProductCode, string Level, string Semester, string CurrentYear)
    {
        string CmdText = string.Empty;
        // string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

        if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;" && Level != "&nbsp;" && Semester != "&nbsp;")
        {
            Level = Level.Remove(0, 2);
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select Distinct(MemberID) from calsignup where Eventyear=" + CurrentYear + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "'  and Accepted='Y' and Level='" + Level + "' and Semester='" + Semester + "')";
        }
        else
        {

            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select Distinct(MemberID) from calsignup where Eventyear=" + CurrentYear + " and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "'  and Accepted='Y' )";
        }

        if (ProductGroupCode == "Total")
        {
            ArrayList ArrProductGroup = new ArrayList();
            ArrayList ArrProduct = new ArrayList();
            string ProductGroup = string.Empty;
            string Product = string.Empty;
            for (int i = 0; i < GVcount.Rows.Count; i++)
            {
                if (GVcount.Rows[i].Cells[0].Text != "&nbsp;" && GVcount.Rows[i].Cells[0].Text != "" && GVcount.Rows[i].Cells[0].Text != "Total")
                {
                    if (ProductGroup != GVcount.Rows[i].Cells[0].Text)
                    {
                        ArrProductGroup.Add(GVcount.Rows[i].Cells[0].Text);
                    }
                    ProductGroup = GVcount.Rows[i].Cells[0].Text;
                }
            }
            for (int i = 0; i < GVcount.Rows.Count; i++)
            {
                if (GVcount.Rows[i].Cells[1].Text != "&nbsp;" && GVcount.Rows[i].Cells[1].Text != "" && GVcount.Rows[i].Cells[1].Text != "Total")
                {
                    if (ProductGroup != GVcount.Rows[i].Cells[1].Text)
                    {
                        ArrProduct.Add(GVcount.Rows[i].Cells[1].Text);
                    }
                    Product = GVcount.Rows[i].Cells[1].Text;
                }
            }
            string pgCode = string.Empty;
            string PCode = string.Empty;
            for (int i = 0; i < ArrProductGroup.Count; i++)
            {
                pgCode += ArrProductGroup[i].ToString() + ",";
            }
            for (int i = 0; i < ArrProduct.Count; i++)
            {
                PCode += ArrProduct[i].ToString() + ",";
            }
            pgCode = pgCode.Remove(pgCode.Length - 1);
            PCode = PCode.Remove(PCode.Length - 1);
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select Distinct(MemberID) from calsignup where Eventyear=" + CurrentYear + "   and Accepted='Y' )";
        }

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            GridCoachList.DataSource = ds;
            GridCoachList.DataBind();
            Session["CoachList"] = ds.Tables[0];
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
    }
    public void loadCoachListNotAccepted(string ProductGroupCode, string ProductCode, string Level, string Semester)
    {
        string CmdText = string.Empty;
        string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        if (ProductGroupCode != "&nbsp;" && ProductCode != "&nbsp;" && Level != "&nbsp;" && Semester != "&nbsp;")
        {
            Level = Level.Remove(0, 2);
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select distinct(MemberID) from CalSignUp where Level='" + Level + "' and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and Eventyear=" + CurrentYear + " and Semester='" + Semester + "' and  Accepted is null and MemberID not in (select distinct(MemberID) from CalSignUp where Level='" + Level + "' and ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and Eventyear=" + CurrentYear + " and Semester='" + Semester + "' and  Accepted='Y'))";
        }
        else
        {

            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select distinct(MemberID) from CalSignUp where  ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and Eventyear=" + CurrentYear + " and  Accepted is null and MemberID not in (select distinct(MemberID) from CalSignUp where  ProductGroupCode='" + ProductGroupCode + "' and ProductCode='" + ProductCode + "' and Eventyear=" + CurrentYear + " and  Accepted='Y'))";
        }
        if (ProductGroupCode == "Total")
        {
            CmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where B.AutoMemberID in (select distinct(MemberID) from CalSignUp where  Eventyear=" + CurrentYear + " and  Accepted is null and MemberID not in (select distinct(MemberID) from CalSignUp where Eventyear=" + CurrentYear + " and  Accepted='Y'))";
        }

        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            GridCoachList.DataSource = ds;
            GridCoachList.DataBind();
            Session["CoachList"] = ds.Tables[0];
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
    }

    protected void GridCoachList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {

        try
        {
            GridCoachList.PageIndex = e.NewPageIndex;
            DataTable dtex = new DataTable();
            dtex = (DataTable)Session["CoachList"];

            GridCoachList.DataSource = (DataTable)Session["CoachList"];
            GridCoachList.DataBind();
            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "OpenConfirmationBox();", true);
        }
        catch (Exception ex) { }
    }

    public void showhideGridHeader()
    {
        try
        {
            if (ddlFromYear.SelectedValue == "-1" && ddlToYear.SelectedValue == "-1")
            {
                GVcount.HeaderRow.Cells[4].Visible = true;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = true;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = true;
            }
            else if (ddlFromYear.SelectedValue == "2011" && ddlToYear.SelectedValue == "2012")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2011" && ddlToYear.SelectedValue == "2011")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2011" && ddlToYear.SelectedValue == "2013")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2011" && ddlToYear.SelectedValue == "2014")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2011" && ddlToYear.SelectedValue == "2015")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = true;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = true;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = true;
            }
            else if (ddlFromYear.SelectedValue == "2012" && ddlToYear.SelectedValue == "2013")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2012" && ddlToYear.SelectedValue == "2012")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2012" && ddlToYear.SelectedValue == "2014")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = false;


                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2012" && ddlToYear.SelectedValue == "2015")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = true;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = true;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = true;
            }
            else if (ddlFromYear.SelectedValue == "2013" && ddlToYear.SelectedValue == "2014")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2013" && ddlToYear.SelectedValue == "2013")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2013" && ddlToYear.SelectedValue == "2015")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = true;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = true;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = true;
            }
            else if (ddlFromYear.SelectedValue == "2014" && ddlToYear.SelectedValue == "2015")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = true;
            }
            else if (ddlFromYear.SelectedValue == "2014" && ddlToYear.SelectedValue == "2014")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = true;
                GVcount.HeaderRow.Cells[9].Visible = false;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = true;
                GVcount.Columns[9].Visible = false;
            }
            else if (ddlFromYear.SelectedValue == "2015" && ddlToYear.SelectedValue == "2015")
            {
                GVcount.HeaderRow.Cells[4].Visible = false;
                GVcount.HeaderRow.Cells[5].Visible = false;
                GVcount.HeaderRow.Cells[6].Visible = false;
                GVcount.HeaderRow.Cells[7].Visible = false;
                GVcount.HeaderRow.Cells[8].Visible = false;
                GVcount.HeaderRow.Cells[9].Visible = true;

                GVcount.Columns[4].Visible = false;
                GVcount.Columns[5].Visible = false;
                GVcount.Columns[6].Visible = false;
                GVcount.Columns[7].Visible = false;
                GVcount.Columns[8].Visible = false;
                GVcount.Columns[9].Visible = true;
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void btnColseTable2_Click(object sender, System.EventArgs e)
    {
        dvCoachList.Visible = false;
        dvMemberInfo.Visible = false;
    }
    protected void btnCloseTable3_Click(object sender, System.EventArgs e)
    {

        dvMemberInfo.Visible = false;
    }
    protected void GridCoachList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {

                GridViewRow row = null;
                GridCoachList.PageIndex = 0;
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                GridCoachList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string MemberID = string.Empty;
                MemberID = ((Label)GridCoachList.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnMemberID.Value = MemberID;


                LoadCoachCalendarSignup(MemberID);
            }
        }
        catch (Exception ex)
        {
        }
    }

    public void LoadCoachCalendarSignup(string MemberID)
    {
        try
        {
            string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            CurrentYear = hdnCoachingYear.Value;

            string cmdText = string.Empty;
            DataSet ds = new DataSet();

            cmdText = "SELECT C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester as PhaseEx,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,count(distinct(CS.EventYear)) as Years,count(CS.MemberID) as Sessions  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + CurrentYear + " AND C.EventID=13  and C.MemberID=" + MemberID + "  group by C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD";
            //}
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();
                    dvMemberInfo.Visible = true;

                }
                else
                {
                    GrdCoachSignUp.DataSource = ds;
                    GrdCoachSignUp.DataBind();

                }
            }
        }
        catch (Exception ex)
        {
        }
    }


    protected void GrdCoachSignUp_RowEditing(object sender, GridViewEditEventArgs e)
    {

        GrdCoachSignUp.EditIndex = e.NewEditIndex;
        int rowIndex = e.NewEditIndex;
        LoadCoachCalendarSignup(hdnMemberID.Value);
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = true;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = true;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = false;

    }
    protected void GrdCoachSignUp_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdCoachSignUp.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdCoachSignUp.EditIndex = -1;
        LoadCoachCalendarSignup(hdnMemberID.Value);
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
        ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;

    }
    protected void GrdCoachSignUp_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {
        try
        {
            int rowIndex = e.RowIndex;

            string ProductGroup = string.Empty;
            string Day = string.Empty;
            string Year = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlEventYear") as DropDownList).SelectedValue;
            Day = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlDay") as DropDownList).SelectedValue;
            ProductGroup = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblPgCode") as Label).Text;
            string ProductGroupID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblProductGroupID") as Label).Text;
            string EventID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEventID") as Label).Text;
            string MemberID = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlVolunteerList") as DropDownList).SelectedValue;
            string ProductID = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedValue;
            string ProductCode = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlProduct") as DropDownList).SelectedItem.Text;
            string Semester = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblHdnPhase") as Label).Text;
            string Level = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlLevel") as DropDownList).SelectedValue;
            string SessionNo = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlSessionNo") as DropDownList).SelectedValue;
            string Time = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlTime") as DropDownList).SelectedValue;
            string Accepted = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlAccepted") as DropDownList).SelectedValue;
            string Preference = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlPreference") as DropDownList).SelectedValue;
            string MaxCap = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlMaxCap") as DropDownList).SelectedValue;
            string VRoom = ((DropDownList)GrdCoachSignUp.Rows[rowIndex].FindControl("ddlVRoom") as DropDownList).SelectedValue;
            string UserID = ((TextBox)GrdCoachSignUp.Rows[rowIndex].FindControl("txtUserID") as TextBox).Text;
            string PWD = ((TextBox)GrdCoachSignUp.Rows[rowIndex].FindControl("txtPWD") as TextBox).Text;
            string SignUpID = ((Label)GrdCoachSignUp.Rows[rowIndex].FindControl("lblSignUpID") as Label).Text;
            string sLevel = string.Empty;
            sLevel = (Level == "" ? "is null" : "'" + Level + "'");

            string cmdText = string.Empty;
            cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and Eventyear=" + Year + " and EventID=" + EventID + " and Day='" + Day + "' and Time='" + Time + "' and  SignUpID not in (" + SignUpID + ")";
            int count = 0;
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            int countset = 0;
            cmdText = "select count(*) as CountSet from CalSignUp where Memberid=" + MemberID + " and eventyear=" + Year + " and Semester='" + Semester + "' and ProductID=" + ProductID + " and Level =" + sLevel + " AND SessionNo=" + SessionNo + " and  SignUpID not in (" + SignUpID + ")";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    countset = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"].ToString());
                }
            }
            if (count > 0)
            {
                lblerr.Text = "Coach is already scheduled for this Period.Please modify the Day or Time.";
            }
            else if (countset < 1)
            {
                Level = (Level == "" ? "is null" : "'" + Level + "'");
                string sSessionNo = (SessionNo == "" ? "null" : "'" + SessionNo + "'");
                string sAccepted = (Accepted == "" ? "null" : "'" + Accepted + "'");
                string sUserID = (UserID == "" ? "null" : "'" + UserID + "'");
                string sPWD = (PWD == "" ? "null" : "'" + PWD + "'");
                string sVRoom = (VRoom == "0" ? "null" : "'" + VRoom + "'");
                cmdText = "UPDATE CalSignUp SET MemberID = " + MemberID + ",EventYear = " + Year + ",ProductID = " + ProductID + ",ProductCode = '" + ProductCode + "',[Level] =" + Level + ",SessionNo=" + sSessionNo + ",Day='" + Day + "',Time = '" + Time + "',Accepted=" + sAccepted + ",Preference=" + Preference + ",MaxCapacity =" + MaxCap + ",UserID=" + sUserID + ",PWD=" + sPWD + ",Vroom=" + VRoom + ",ModifiedDate = Getdate(),ModifiedBy = " + Session["loginID"].ToString() + " WHERE SignUpID=" + SignUpID + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                LoadCoachCalendarSignup(hdnMemberID.Value);
                GrdCoachSignUp.EditIndex = -1;
                LoadCoachCalendarSignup(hdnMemberID.Value);
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkUpdate") as LinkButton).Visible = false;
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lnkCancel") as LinkButton).Visible = false;
                ((LinkButton)GrdCoachSignUp.Rows[rowIndex].FindControl("lblEdit") as LinkButton).Visible = true;
                if (hdnStatus.Value == "NotAccepted")
                {
                    loadCoachListNotAccepted(hdnProductGroupCode.Value, hdnProductCode.Value, hdnLevel.Value, hdnPhase.Value);
                }
                else
                {
                    string CurrentYear = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
                    hdnCoachingYear.Value = CurrentYear;
                    loadCoachListAccepted(hdnProductGroupCode.Value, hdnProductCode.Value, hdnLevel.Value, hdnPhase.Value, CurrentYear);
                }
                PivotTable();
                lblerr.Text = "Updated Successfully";
            }
            else
            {
                lblerr.Text = "Coach with same product, Semester, Level and Session are already found.";
            }
        }
        catch (Exception ex)
        {
        }


    }
    protected void GrdCoachSignUp_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        if (e.CommandName == "Edit")
        {
            retFlag = "Edit";
        }
        else if (e.CommandName == "Update")
        {
            retFlag = "Update";
        }
        else
        {
            retFlag = "Cancel";
        }
    }

    protected void GrdCoachSignUp_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            if (e.Row.RowType == DataControlRowType.DataRow && GrdCoachSignUp.EditIndex == e.Row.RowIndex)
            {
                DropDownList ddlLevel = ((DropDownList)e.Row.FindControl("ddlLevel") as DropDownList);
                string ProductGroup = string.Empty;
                string Day = string.Empty;
                string Year = ((Label)e.Row.FindControl("lblHdnEventYear") as Label).Text;
                Day = ((Label)e.Row.FindControl("lblHdnDay") as Label).Text;
                ProductGroup = ((Label)e.Row.FindControl("lblPgCode") as Label).Text;
                string ProductGroupID = ((Label)e.Row.FindControl("lblProductGroupID") as Label).Text;
                string EventID = ((Label)e.Row.FindControl("lblEventID") as Label).Text;
                string MemberID = ((Label)e.Row.FindControl("lblhdnAutoMemberID") as Label).Text;
                string ProductID = ((Label)e.Row.FindControl("lblProductID") as Label).Text;
                string Semester = ((Label)e.Row.FindControl("lblHdnPhase") as Label).Text;
                string Level = ((Label)e.Row.FindControl("lblHdnLevel") as Label).Text;
                string SessionNo = ((Label)e.Row.FindControl("lblHdnSession") as Label).Text;
                string Time = ((Label)e.Row.FindControl("lblHdnTime") as Label).Text;
                string Accepted = ((Label)e.Row.FindControl("lblHdnAccepted") as Label).Text;
                string Preference = ((Label)e.Row.FindControl("lblHdnPreference") as Label).Text;
                string MaxCap = ((Label)e.Row.FindControl("lblHdnMaxCap") as Label).Text;
                string VRoom = ((Label)e.Row.FindControl("lblHdnVRoom") as Label).Text;
                string UserID = ((Label)e.Row.FindControl("lblHdnUserID") as Label).Text;
                string PWD = ((Label)e.Row.FindControl("lblHdnPWD") as Label).Text;

                DropDownList ddlAccepted = (DropDownList)e.Row.FindControl("ddlAccepted");
                DropDownList ddlDay = (DropDownList)e.Row.FindControl("ddlDay");
                TextBox txtUserID = (TextBox)e.Row.FindControl("txtUserID");
                TextBox txtPWD = (TextBox)e.Row.FindControl("txtPWD");
                if (UserID != "")
                {
                    txtUserID.Text = UserID;
                }
                if (PWD != "")
                {
                    txtPWD.Text = PWD;
                }

                if (Accepted != "")
                {
                    ddlAccepted.SelectedValue = Accepted;
                }
                if (Day != "")
                {
                    ddlDay.SelectedValue = Day;
                }

                LoadLevel(ddlLevel, ProductGroupID, Year, EventID, ProductID);
                if (Level != "")
                {
                    ddlLevel.SelectedValue = Level;
                }

                DropDownList ddlMaxCap = (DropDownList)e.Row.FindControl("ddlMaxCap");
                LoadCapacity(ddlMaxCap);
                if (MaxCap != "")
                {
                    ddlMaxCap.SelectedValue = MaxCap;
                }

                DropDownList ddlVRoom = (DropDownList)e.Row.FindControl("ddlVRoom");
                LoadVRoom(ddlVRoom);
                if (VRoom != "")
                {
                    ddlVRoom.SelectedValue = VRoom;
                }

                DropDownList ddlYear = (DropDownList)e.Row.FindControl("ddlEventYear");
                loadYear(ddlYear);
                if (Year != "")
                {
                    ddlYear.SelectedValue = Year;
                }

                DropDownList ddlTime = (DropDownList)e.Row.FindControl("ddlTime");
                if (Day == "Saturday" || Day == "Sunday")
                {
                    LoadDisplayTime(ddlTime);
                }
                else
                {
                    LoadWeekDisplayTime(ddlTime);
                }
                if (Time != "")
                {
                    ddlTime.SelectedValue = Time;
                }

                DropDownList ddlVolunteer = (DropDownList)e.Row.FindControl("ddlVolunteerList");
                loadVolunteer(ddlVolunteer);
                if (MemberID != "")
                {
                    ddlVolunteer.SelectedValue = MemberID;
                }

                DropDownList ddlProduct = (DropDownList)e.Row.FindControl("ddlProduct");
                LoadProduct(ddlProduct, Year, EventID, ProductGroupID);
                if (ProductID != "")
                {
                    ddlProduct.SelectedValue = ProductID;
                }

            }
        }
        catch (Exception ex)
        {

        }
    }

    private void LoadLevel(DropDownList ddlObject, string ProductGroup, string eventYear, string eventID, string ProductID)
    {
        try
        {

            ddlObject.Items.Clear();
            ddlObject.Enabled = true;
            string cmdText = string.Empty;
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" + eventYear + " and ProductGroupID=" + ProductGroup + " and ProductID=" + ProductID + " and EventID=" + eventID + "";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            ddlObject.DataValueField = "LevelCode";
            ddlObject.DataTextField = "LevelCode";

            ddlObject.DataSource = ds;
            ddlObject.DataBind();
        }
        catch
        {
        }
        // ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
        //if (ProductGroup == "SAT")
        //{
        //    ddlObject.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlObject.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlObject.Items.Insert(2, new ListItem("Senior", "Senior"));
        //}
        //else if (ProductGroup == "Universal Values")
        //{
        //    //'ddlObject.Enabled = False
        //    ddlObject.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlObject.Items.Insert(1, new ListItem("Junior", "Junior"));
        //    ddlObject.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlObject.Items.Insert(3, new ListItem("Senior", "Senior"));
        //}
        //else if (ProductGroup == "Science")
        //{
        //    ddlObject.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlObject.Items.Insert(1, new ListItem("One level only", "One level only"));
        //}


        //else
        //{
        //    ddlObject.Items.Insert(0, new ListItem("Select", "0"));
        //    ddlObject.Items.Insert(1, new ListItem("Beginner", "Beginner"));
        //    ddlObject.Items.Insert(2, new ListItem("Intermediate", "Intermediate"));
        //    ddlObject.Items.Insert(3, new ListItem("Advanced", "Advanced"));
        //}

    }

    private void LoadCapacity(DropDownList ddltemp)
    {
        ddltemp.Items.Clear();
        ListItem li = default(ListItem);
        int i = 0;
        //10 To 100 Step 5
        for (i = 0; i <= 30; i += 1)
        {
            li = new ListItem(i.ToString());
            ddltemp.Items.Add(li);
        }

    }
    public void LoadVRoom(DropDownList ddltemp)
    {
        ddltemp.Items.Clear();
        ListItem li = default(ListItem);
        int i = 0;
        for (i = 1; i <= 100; i++)
        {
            li = new ListItem(i.ToString());
            ddltemp.Items.Add(li);
        }
        ddltemp.Items.Insert(0, new ListItem("Select", "0"));
    }

    public void loadYear(DropDownList dropDown)
    {
        dropDown.Items.Clear();
        dropDown.Items.Insert(0, Convert.ToString(DateTime.Now.Year + 1));
        dropDown.Items.Insert(1, Convert.ToString(DateTime.Now.Year));
        dropDown.Items.Insert(2, Convert.ToString(DateTime.Now.Year - 1));

        dropDown.SelectedValue = Convert.ToString(DateTime.Now.Year);
    }

    public void LoadDisplayTime(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Insert(0, "Select Time");
        ddlObject.Items.Insert(1, "8:00AM");
        ddlObject.Items.Insert(2, "9:00AM");
        ddlObject.Items.Insert(3, "10:00AM");
        ddlObject.Items.Insert(4, "11:00AM");
        ddlObject.Items.Insert(5, "12:00PM");
        ddlObject.Items.Insert(6, "1:00PM");
        ddlObject.Items.Insert(7, "2:00PM");
        ddlObject.Items.Insert(8, "3:00PM");
        ddlObject.Items.Insert(9, "4:00PM");
        ddlObject.Items.Insert(10, "5:00PM");
        ddlObject.Items.Insert(11, "6:00PM");
        ddlObject.Items.Insert(12, "7:00PM");
        ddlObject.Items.Insert(13, "8:00PM");
        ddlObject.Items.Insert(14, "9:00PM");
        ddlObject.Items.Insert(15, "10:00PM");
        ddlObject.Items.Insert(16, "11:00PM");
        ddlObject.Items.Insert(17, "12:00AM");
    }

    public void LoadWeekDisplayTime(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ddlObject.Items.Insert(0, "Select Time");
        ddlObject.Items.Insert(1, "6:00PM");
        ddlObject.Items.Insert(2, "7:00PM");
        ddlObject.Items.Insert(3, "8:00PM");
        ddlObject.Items.Insert(4, "9:00PM");
        ddlObject.Items.Insert(5, "10:00PM");
        ddlObject.Items.Insert(6, "11:00PM");
        ddlObject.Items.Insert(7, "12:00AM");
    }

    public void loadVolunteer(DropDownList ddlObject)
    {
        string CmdText = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlObject.DataValueField = "MemberID";
                ddlObject.DataTextField = "Name";
                ddlObject.DataSource = ds;
                ddlObject.DataBind();

                ddlObject.Items.Insert(0, "Select");
            }
            else
            {
                ddlObject.DataValueField = "MemberID";
                ddlObject.DataTextField = "Name";
                ddlObject.DataSource = ds;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, "Select");

            }
        }
    }

    public void LoadProduct(DropDownList dropdown, string Year, string EventID, string ProductGroupID)
    {
        string CmdText = "Select P.ProductID, P.Name,P.ProductCode from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" + Year + " AND P.EventID=" + EventID + " and P.ProductGroupID =" + ProductGroupID + "  order by P.ProductID ";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                dropdown.DataValueField = "ProductID";
                dropdown.DataTextField = "ProductCode";
                dropdown.DataSource = ds;
                dropdown.DataBind();

                dropdown.Items.Insert(0, "Select");
            }
            else
            {
                dropdown.DataValueField = "ProductID";
                dropdown.DataTextField = "ProductCode";
                dropdown.DataSource = ds;
                dropdown.DataBind();
                dropdown.Items.Insert(0, "Select");

            }
        }
    }

}