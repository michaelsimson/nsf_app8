﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyResponse.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="SurveyResponse" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="js/jquery.table2excel.js"></script>
        <%-- <script src="js/tableExport.js"></script>
        <script src="js/jquery.base64.js"></script>--%>
        <script language="javascript" type="text/javascript">
            var templateJSON = "";
            $(function (e) {
                GetSurveyTitle();
                LoadSurveysDetails();

            });
            function getQuestionRows() {
                var SurveyID = 1;
                var surveyHtml = "";
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetSurveyLayOut",
                    data: JSON.stringify({ objSurvey: { SurveyID: SurveyID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        //BindSurveyLayout();
                        var questionID;
                        if (JSON.stringify(data.d.length) > 0) {
                            surveyHtml += "<div>";
                            $.each(data.d, function (index, value) {
                                questionID = value.QuestionID;
                                surveyHtml += "<div><span style='font-weight:bold;'>" + (index + 1) + ". " + value.QuestionTitle + "</span></div>";
                                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";

                                if (value.ColCount <= 0) {

                                    if (value.RowCount <= 0) {

                                        surveyHtml += "<div class='dvLeftSection' style='width:800px;'>";

                                        surveyHtml += "<div style='font-weight:bold;'><input type='text' style='width:700px; height:100px;' /></div>";
                                        surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";


                                        surveyHtml += "</div>";

                                    } else {



                                        surveyHtml += "<div style='float:left; width:200px;' class='dvLeftSection'>";
                                        $.each(value.lstSurveyRows, function (index1, value1) {
                                            if (questionID == value1.QuestionID) {
                                                surveyHtml += "<span style='font-weight:bold;'>" + value1.RowTitle + "</span>";
                                                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                                            }
                                        });
                                        surveyHtml += "</div>";
                                        surveyHtml += "<div style='float:left; width:800px;' class='dvRightSection'>";

                                        if (value.ColCount > 0) {
                                            $.each(value.lstSurveyCols, function (index2, value2) {
                                                if (questionID == value2.QuestionID) {
                                                    if (value.ChoiceFormat == "Text") {

                                                        surveyHtml += "<div style='font-weight:bold;'><input type='text' style='width:700px;' /></div>";
                                                    } else if (value.ChoiceFormat == "Radio") {

                                                    }

                                                    surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                                                }
                                            });
                                        } else {
                                            $.each(value.lstSurveyRows, function (index2, value2) {
                                                if (questionID == value2.QuestionID) {
                                                    if (value.ChoiceFormat == "Text") {

                                                        surveyHtml += "<div style='font-weight:bold;'><input type='text' style='width:700px;' /></div>";

                                                    }
                                                    surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                                                }
                                            });


                                        }

                                        surveyHtml += "</div>";
                                    }

                                } else {

                                    surveyHtml += "<div style='float:left; width:1000px;' class='dvLeftSection'>";
                                    surveyHtml += "<div style='float:left; width:800px; margin-left:200px;' class='dvLeftSection'>";
                                    $.each(value.lstSurveyCols, function (index3, value3) {
                                        if (questionID == value3.QuestionID) {
                                            surveyHtml += "<div style='float:left; width:110px;'>";
                                            surveyHtml += "<span style='font-weight:bold;'>" + value3.ColumnTitle + "</span>";
                                            surveyHtml += "</div>";
                                        }
                                    });
                                    surveyHtml += "</div>";
                                    surveyHtml += "</div>";
                                    surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";

                                    surveyHtml += "<div style='float:left; width:1000px;' class='dvLeftSection'>";
                                    $.each(value.lstSurveyRows, function (index1, value1) {
                                        if (questionID == value1.QuestionID) {
                                            surveyHtml += "<div style='float:left; width:200px;'>";
                                            surveyHtml += "<span style='font-weight:bold;'>" + value1.RowTitle + "</span>";
                                            surveyHtml += "</div>";

                                            surveyHtml += "<div style='float:left; width:800px;'>";
                                            $.each(value.lstSurveyCols, function (index2, value2) {
                                                if (questionID == value2.QuestionID) {
                                                    surveyHtml += "<div style='float:left; width:110px;'>";
                                                    surveyHtml += "<input type='radio' />";
                                                    surveyHtml += "</div>";
                                                }
                                            });
                                            surveyHtml += "</div>";

                                            surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                                        }
                                    });
                                    surveyHtml += "</div>";
                                }


                                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                            });
                            surveyHtml += "</div>";
                        }

                        $("#dvRespondContent").html(surveyHtml);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function BindSurveyLayout() {

                var surveyHtml = "";
                surveyHtml += "<div>";

                surveyHtml += "<div><span style='font-weight:bold;'>1. Please tell ur Name</span></div>";
                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                surveyHtml += "<div style='float:left; width:200px;' class='dvLeftSection'>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "</div>";

                surveyHtml += "<div style='float:left; width:800px;' class='dvRightSection'>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                surveyHtml += "<span style='font-weight:bold;'>First Name</span>";
                surveyHtml += "</div>";

                surveyHtml += "</div>";

                $("#dvRespondContent").html(surveyHtml);
            }

            function GetRespondantList() {

                var surveyID = getParameterByName("SurveyID");
                var tblHtml = "";
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetRespondantList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            //    tblHtml += " <thead>";
                            //    tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";
                            //    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Action</td>";
                            //    //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Srvey Title</td>";
                            //    //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>#Of Responses</td>";
                            //    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Collector</td>";
                            //    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>RespondantID</td>";
                            //    tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Started</td>";
                            //    tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Last Modified</td>"

                            //    tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;'>Result URL</td>"

                            //    tblHtml += "</tr>";
                            //    tblHtml += " </thead>";
                            //    var count = $("#hdnResponseCount").val();
                            //    $.each(data.d, function (index, value) {
                            //        tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'><input type='button' class='btnSelect' value='Select' attr-ResID=" + value.RespondentId + " />"
                            //        tblHtml += "</td>";
                            //        //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>2015 - Need Coaches for Online Coaching";
                            //        //tblHtml += "</td>";
                            //        //tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + $("#hdnResponseCount").val() + "";
                            //        //tblHtml += "</td>";
                            //        count =
                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>Web Link 1 (Web Link)";
                            //        tblHtml += "</td>";




                            //        var modifiedDate = convertDate(value.DateModified);
                            //        var createdDate = convertDate(value.DateStart);

                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + value.RespondentId + "";
                            //        tblHtml += "</td>";

                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + createdDate + "";
                            //        tblHtml += "</td>";

                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'>" + modifiedDate + "";
                            //        tblHtml += "</td>";

                            //        tblHtml += "<td style='border: 1px solid black; border-collapse: collapse;'><a href='" + value.AnalysisUrl + "' target='blank'>" + (value.AnalysisUrl).substr(0, 30) + "...</a>";
                            //        tblHtml += "</td>";
                            //        tblHtml += "</tr>";

                            //    });
                            //    $("#spnRespondentCount").text($("#hdnResponseCount").val());

                            //    $("#tblSurveyCollectorList").html(tblHtml);
                            //    $("#btnExportToExcel").css("display", "none");
                            //} else {
                            //    $("#spnErrMsg").text("No record exists");
                            //    $("#btnExportToExcel").css("display", "none");
                            //}
                        }
                    },
                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function convertDate(date) {

                var MyDate_String_Value = date;
                var dateVal = new Date
                            (
                                 parseInt(MyDate_String_Value.replace(/(^.*\()|([+-].*$)/g, ''))
                            );
                var dat = dateVal.getMonth() +
                                         1 +
                                       "/" +
                           dateVal.getDate() +
                                       "/" +
                       dateVal.getFullYear();

                return dat;
            }
            $(document).on("click", ".btnSelect", function (e) {
                $("#dvSurveyResponse").css("display", "block");
                var respondantID = $(this).attr("attr-ResID");
                $("#hdnRespondantID").val(respondantID);
                GetSurveyDetails();
                GetSurveyDetailsForExcel();
                //window.location.href = "SurveyResponse.aspx?SurID=" + surveyID + "";
            });

            function getParameterByName(name) {
                name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
                var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
                    results = regex.exec(location.search);
                return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
            }
            function LoadSurveysDetails() {
                var surveyID = getParameterByName("SurveyID");

                var jsonData = JSON.stringify({ SurveyID: surveyID });

                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        $.each(data.d, function (index, value) {

                            $("#hdnResponseCount").val(value.NumResponses);
                            $("#hdnSurveyTitle").val(value.Nickname);

                        });
                        GetRespondantList();
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });

            }

            function GetSurveyDetails() {
                var dvHtml = "";
                var surveyID = getParameterByName("SurveyID");
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        var i = 0;
                        templateJSON = data.d;
                        //dvHtml += "<tr class='noExl'>";
                        $.each(data.d, function (index, value) {
                            $.each(value.Questions, function (index1, value1) {

                                dvHtml += "<div style='font-weight:Bold;'>Q" + (index1 + 1) + ". " + value1.Heading + "</div>";
                                dvHtml += "<div style='clear:both; margin-bottom:10px;'></div>";

                                if (value1.Answers.length > 0) {

                                    $.each(value1.Answers, function (index2, value2) {

                                        if (value2.Type == 1) {

                                            dvHtml += "<div style='float:left; width:300px; margin-left:25px; font-weight:bold;'>" + value2.Text + "</div><div style='float:left; width:5px;'>:</div>";

                                            dvHtml += "<div style='float:left;'><span style='font-weight:normal;' id=" + value2.AnswerId + " ></span></div>";

                                            dvHtml += "<div style='clear:both; margin-bottom:10px;'></div>";
                                        }

                                    });
                                } else {
                                    i++;
                                    dvHtml += "<div style='margin-left:25px; font-weight:bold; text-align:justify;'><span style='font-weight:normal;' id='spnText" + value1.QuestionId + "'>N/A</span></div>";
                                }

                                dvHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                            });
                        });
                        //dvHtml += "</tr>";
                        //$("#table2excel").html(dvHtml);


                        $("#dvResponseLayout").html(dvHtml);
                        GetResponseFromSMLayout();
                    },
                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            $(document).on("click", "#btnExportToExcel", function (e) {
                //window.open('data:application/vnd.ms-excel,' + $('#dvData').html());

                alert($("#table2excel").html());
                $("#table2excel").table2excel({
                    // exclude CSS class
                    exclude: ".noExl",
                    name: "Survey Repsponse"
                });

                e.preventDefault();

            });


            function GetSurveyDetailsForExcel() {
                var dvHtml = "";

                $.ajax({
                    type: "POST",
                    url: "TestSurveyDesign.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {

                        dvHtml += "<tr class='noExl'>";
                        dvHtml += "<th>SurveyID</th>";
                        dvHtml += "<th>SurveyTitle</th>";
                        dvHtml += "<th>SatrtDate</th>";
                        dvHtml += "<th>EndDate</th>";
                        dvHtml += "</tr>";
                        dvHtml += "<tr>";
                        dvHtml += "<td style='font-weight:bold;'>SurveyID</td>";
                        dvHtml += "<td style='font-weight:bold;'>SurveyTitle</td>";
                        dvHtml += "<td style='font-weight:bold;'>RespondantID</td>";
                        dvHtml += "<td style='font-weight:bold;'>CollectorID</td>";
                        dvHtml += "<td style='font-weight:bold;'>SatrtDate</td>";
                        dvHtml += "<td style='font-weight:bold;'>EndDate</td>";
                        var i = 0;
                        $.each(data.d, function (index, value) {
                            $.each(value.Questions, function (index1, value1) {
                                i++;
                                var colspan = 0;
                                if (i == 1) {
                                    colspan = 3;
                                } else if (i == 2) {
                                    colspan = 0;
                                } else if (i == 3) {
                                    colspan = 6;
                                }
                                dvHtml += "<td colspan='" + colspan + "' style='font-weight:bold;'>" + value1.Heading + "</td>";
                            });
                        });
                        dvHtml += "</tr>";
                        dvHtml += "<tr>";
                        dvHtml += "<td></td>";
                        dvHtml += "<td></td>";
                        dvHtml += "<td></td>";
                        dvHtml += "<td></td>";
                        dvHtml += "<td></td>";
                        dvHtml += "<td></td>";
                        $.each(data.d, function (index, value) {
                            $.each(value.Questions, function (index1, value1) {
                                if (value1.Answers.length > 0) {

                                    $.each(value1.Answers, function (index2, value2) {

                                        if (value2.Type == 1) {
                                            dvHtml += "<td>" + value2.Text + "</td>";
                                        }

                                    });
                                } else {
                                    dvHtml += "<td></td>";
                                }
                            });
                        });

                        dvHtml += "</tr>";
                        dvHtml += "<tr>";
                        $.each(data.d, function (index, value) {
                            dvHtml += "<td>66728431</td>";
                            dvHtml += "<td>2015 - Need Coaches for Online Coaching</td>";
                            dvHtml += "<td>4214185714</td>";
                            dvHtml += "<td>4456019411</td>";
                            dvHtml += "<td>01/01/2016</td>";
                            dvHtml += "<td>02/25/2016</td>";
                            $.each(value.Questions, function (index1, value1) {
                                //dvHtml += "<td>" + value1.Heading + "</td>";

                                // dvHtml += "<div>" + value1.Heading + "</div>";

                                //$.each(value1.Answers, function (index2, value2) {
                                //    dvHtml += "<div style='clear:both; margin-bottom:10px;'></div>";
                                //    dvHtml += "<div style='float:left; width:200px;'>" + value2.Text + "</div>";

                                //});

                                //dvHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                            });
                        });
                        //dvHtml += "</tr>";
                        GetResponseFromSM(dvHtml);



                        //$("#dvSurveyResponse").html(dvHtml);
                    },
                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }


            function GetResponseFromSM(dvHtml) {
                var tblHtml = "";
                var respondantID = $("#hdnRespondantID").val();

                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetResponseFromSM",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        //alert(JSON.stringify(data.d));
                        $.each(data.d, function (index, value) {
                            $.each(value.Questions, function (index, value1) {
                                $.each(value1.Answers, function (index, value2) {
                                    if (value2.Text != null) {

                                        dvHtml += "<td>" + value2.Text + "</td>";
                                    }
                                });
                            });
                        });
                        dvHtml += "</tr>";
                        $("#table2excel").html(dvHtml);
                    },

                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function GetResponseFromSMLayout() {
                var tblHtml = "";
                var surveyID = getParameterByName("SurveyID");
                var respondantID = $("#hdnRespondantID").val();
                var jsonData = JSON.stringify({ SurveyID: surveyID, RespondantID: respondantID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetResponseFromSM",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        $.each(data.d, function (index, value) {
                            $.each(value.Questions, function (index, value1) {

                                $.each(value1.Answers, function (index, value2) {

                                    if (value2.Text != null) {
                                        if (value2.Row != 0) {
                                            if (value2.Text != "") {
                                                $("#" + value2.Row + "").text(value2.Text);
                                            } else {
                                                $("#" + value2.Row + "").text("N/A");
                                            }
                                        } else {
                                            if (value2.Text != "") {
                                                $("#spnText" + value1.QuestionId + "").text(value2.Text);
                                            } else {
                                                $("#spnText" + value1.QuestionId + "").text("N/A");
                                            }
                                        }
                                    } else {
                                        $.each(templateJSON, function (index3, value3) {
                                            $.each(value3.Questions, function (index4, value4) {

                                                //alert(JSON.stringify(value4.Answers));
                                                $.each(value4.Answers, function (index5, value5) {


                                                    if (value5.AnswerId == value2.Col) {

                                                        if (value5.Text != "") {
                                                            $("#" + value2.Row + "").text(value5.Text);
                                                        } else {
                                                            if (respondantID == "4483205334") {
                                                                $("#" + value2.Row + "").text("Sudha Pallapothu");
                                                            } else if (respondantID == "4478103242") {
                                                                $("#" + value2.Row + "").text("Shyam Vaddadi");
                                                            }
                                                            else if (respondantID == "4476997622") {
                                                                $("#" + value2.Row + "").text("Sandeep Tamboli ");
                                                            }
                                                            else if (respondantID == "4472580468") {
                                                                $("#" + value2.Row + "").text("	Naveen Hanumansetty  ");
                                                            }
                                                            else if (respondantID == "4467345942") {
                                                                $("#" + value2.Row + "").text("Pragyna Naik");
                                                            }
                                                            else if (respondantID == "4461958533") {
                                                                $("#" + value2.Row + "").text("Sravya Kuchibhotla");
                                                            }
                                                            else {
                                                                $("#" + value2.Row + "").text("N/A");
                                                            }
                                                        }

                                                        //}
                                                    }

                                                    //}
                                                });
                                            });
                                        });
                                    }
                                });
                            });
                        });

                    },

                    //$("#tblSurveyList").html(tblHtml);

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function GetSurveyTitle() {
                var dvHtml = "";
                var surveyID = getParameterByName("SurveyID");
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",

                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        $.each(data.d, function (index, value) {
                            $("#hdnSurveyTitle").val(value.TitleText);
                            $("#spnSurveyTitle").text(value.TitleText);
                            $("#spnRespondentCount").text($("#hdnResponseCount").val());
                        });
                    }
                });
            }

            function getResponsesDetails() {
                var surveyID = 66728431;
                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
                var jsonData = JSON.stringify({ SurveyID: surveyID });
                var arrQuestionID = new Array();
                $.ajax({
                    type: "POST",
                    url: "SurveyList.aspx/ListSurveyDetails",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        var tblHtml = "";
                        var answerJosn = data.d;
                        tblHtml += "<table>";
                        tblHtml += "<tr>";
                        tblHtml += "<td style='font-weight:bold;'>SurveyID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>RespondentID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>CollectorID";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Start Date";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>End Date";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>IP Address";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Email Address";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>First Name";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Last Name";
                        tblHtml += "</td>";
                        tblHtml += "<td style='font-weight:bold;'>Custom Data";
                        tblHtml += "</td>";
                        var colspan = 0;

                        var i = 0;
                        $.each(data.d, function (index, value) {

                            $.each(value.Questions, function (index1, value1) {
                                arrQuestionID.push(value1.QuestionId);
                                if (value1.Answers.length > 0) {
                                    $.each(value1.Answers, function (index5, value5) {

                                        if (value5.Type == "1") {
                                            i++;
                                        }
                                    });
                                    colspan = i;
                                } else {
                                    colspan = 0;
                                }
                                tblHtml += "<td style='font-weight:bold;' colspan='" + colspan + "'>" + value1.Heading + "</td>";
                                i = 0;
                            });
                            tblHtml += "</tr>";
                            tblHtml += "<tr>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";
                            tblHtml += "<td>";
                            tblHtml += "</td>";

                            $.each(value.Pages, function (index2, value2) {

                                $.each(value2.Questions, function (index3, value3) {
                                    if (value3.Answers.length > 0) {
                                        var answersData = value3.Answers;
                                        var asc = true;
                                        var prop = "Position";
                                        //answersData = answersData.sort(function (a, b) {
                                        //    if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                                        //    else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                                        //});
                                        // alert(JSON.stringify(answersData));
                                        $.each(answersData, function (index4, value4) {
                                            if (value4.Type == "1") {
                                                tblHtml += "<td style='font-weight:bold;'>" + value4.Text + "</td>";
                                            }
                                        });
                                    } else {

                                        tblHtml += "<td style='font-weight:bold;'>&nbsp;</td>";
                                    }
                                });

                            });
                            tblHtml += "</tr>";

                        });
                        // $("#tblSurveyResponseExcel").html(tblHtml);

                        GetRespondantListExcel(surveyID, tblHtml, answerJosn, arrQuestionID);
                        //tblHtml += "</thead>";

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function GetRespondantListExcel(surveyID, tblHtml, answerJosn, arrQuestionID) {


                var jsonData = JSON.stringify({ SurveyID: surveyID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetRespondantList",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {

                        var respondentID = "";
                        var json = data;
                        var arrRespondentID = new Array();
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                respondentID += value.RespondentId + ",";
                                arrRespondentID.push(value.RespondentId);
                            });

                            var respondantID = respondentID.slice(0, -1);

                            GetResponseExcel(surveyID, arrRespondentID, json, tblHtml, answerJosn, arrQuestionID);
                        }
                        else {
                            var d = new Date();
                            var currentDate = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();

                            $("#tblSurveyResponseExcel").html(tblHtml);

                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                            $("#tblSurveyResponseExcel").table2excel({
                                // exclude CSS class
                                exclude: ".noExl",
                                name: "Survey Repsponse",
                                filename: $("#hdnSurveyTite").val() + "_Responses_" + currentDate + ""
                            });
                        }

                    },

                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            function GetResponseExcel(surveyID, respondentID, json, tblHtml, answerJosn, arrQuestionID) {
                var arrResQuesID = new Array();
                var jsonData = JSON.stringify({ SurveyID: surveyID, RespondantID: respondentID });
                $.ajax({
                    type: "POST",
                    url: "SurveyResponse.aspx/GetResponseExcel",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: jsonData,
                    async: true,
                    cache: false,
                    success: function (data) {
                        var i = 0;
                        var ansJson = json.d;
                        var asc = true;
                        var prop = "RespondentId";
                        ansJson = ansJson.sort(function (a, b) {
                            if (asc) return (a[prop] > b[prop]) ? 1 : ((a[prop] < b[prop]) ? -1 : 0);
                            else return (b[prop] > a[prop]) ? 1 : ((b[prop] < a[prop]) ? -1 : 0);
                        });

                        $.each(ansJson, function (index, value) {
                            tblHtml += "<tr>";
                            arrResQuesID = [];
                            $.each(data.d, function (index1, value1) {

                                if (value.RespondentId == value1.RespondentId) {
                                    tblHtml += "<td>" + surveyID + "</td>";
                                    tblHtml += "<td>" + value.RespondentId + "</td>";
                                    tblHtml += "<td>" + value.CollectorId + "</td>";
                                    tblHtml += "<td>" + convertJsonDate(value.DateStart) + "</td>";
                                    tblHtml += "<td>" + convertJsonDate(value.DateModified) + "</td>";
                                    tblHtml += "<td>" + value.IpAddress + "</td>";
                                    tblHtml += "<td>" + value.Email + "</td>";
                                    tblHtml += "<td>" + value.FirstName + "</td>";
                                    tblHtml += "<td>" + value.LastName + "</td>";
                                    tblHtml += "<td>&nbsp;</td>";

                                    $.each(value1.Questions, function (index9, value9) {
                                        if (value.RespondentId == value1.RespondentId) {
                                            arrResQuesID.push(value9.QuestionId);
                                        }
                                    });

                                    $.each(answerJosn, function (index2, value2) {
                                        //alert(JSON.stringify(value2.Questions));

                                        $.each(value2.Questions, function (index3, value3) {

                                            $.each(value1.Questions, function (index4, value4) {
                                                //alert(JSON.stringify(value4.Answers));
                                                // 

                                                if (value3.QuestionId == value4.QuestionId) {
                                                    if ($.inArray(arrQuestionID[i], arrResQuesID) > -1) {
                                                    } else {
                                                        tblHtml += "<td>&nbsp;</td>";
                                                    }
                                                    if (value3.Answers.length > 0) {
                                                        $.each(value3.Answers, function (index5, value5) {
                                                            if (value4.Answers.length > 0) {
                                                                $.each(value4.Answers, function (index6, value6) {


                                                                    // if (value6.Row != "0") {
                                                                    // alert(value6.Text);
                                                                    if (value5.AnswerId == value6.Row) {

                                                                        if (value6.Col == "0") {
                                                                            tblHtml += "<td>" + value6.Text + "</td>";
                                                                        } else {
                                                                            $.each(value3.Answers, function (index7, value7) {
                                                                                if (value6.Col == value7.AnswerId) {
                                                                                    tblHtml += "<td>" + value7.Text + "</td>";
                                                                                }
                                                                            });
                                                                        }
                                                                    }
                                                                    //} else {
                                                                    //    tblHtml += "<td>" + value6.Text + "</td>";
                                                                    //}
                                                                });
                                                            }
                                                        });
                                                    } else {
                                                        //alert(value3.QuestionId);
                                                        if (value3.QuestionId == value4.QuestionId) {

                                                            $.each(value4.Answers, function (index6, value6) {
                                                                if (value6.Row == "0") {
                                                                    tblHtml += "<td>" + value6.Text + "</td>";
                                                                }
                                                            });

                                                        }
                                                    }
                                                }
                                                //} else {
                                                //    tblHtml += "<td>&nbsp;</td>";
                                                //}
                                                i++;
                                            });
                                            i = 0;
                                        });

                                    });


                                    i = 0;
                                }
                            });
                            tblHtml += "</tr>";
                        });
                        var d = new Date();
                        var currentDate = d.getMonth() + "/" + d.getDate() + "/" + d.getFullYear();
                        tblHtml += "</table>";

                        $("#fountainTextG").css("display", "none");
                        $("#overlay").css("display", "none");
                        $("#spnJsonText").text(tblHtml);
                        $("#tblSurveyResponseExcel").html(tblHtml);
                     
                        var jsonData = JSON.stringify({ ResponseHTML: tblHtml, FileName: $("#hdnSurveyTite").val() + "_Responses_" + currentDate + "" });
                        $.ajax({
                            type: "POST",
                            url: "SurveyResponse.aspx/GetResponseHTML",
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: jsonData,
                            async: true,
                            cache: false,
                            success: function (data) {

                            }
                        });
                    
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });


            }

            $(function (e) {
                getResponsesDetails();
            });

            $(document).on("click", "#btnExport", function (e) {
                //window.open('data:application/vnd.ms-excel,' + $('#dvData').html());

                //$("#table2excel").table2excel({
                //    // exclude CSS class
                //    exclude: ".noExl",
                //    name: "Survey Repsponse",
                //    filename: "Survey Repsponse"
                //});
                document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
            });





            function convertJsonDate(jsonDate) {
                var dateString = jsonDate.substr(6);
                var currentTime = new Date(parseInt(dateString));
                var month = currentTime.getMonth() + 1;
                var day = currentTime.getDate();
                var year = currentTime.getFullYear();
                var date = month + "/" + day + "/" + year;
                return date;
            }



            function fnExcelReport() {
                var tab_text = "<table border='2px'><tr bgcolor='#87AFC6'>";
                var textRange; var j = 0;
                tab = document.getElementById('headerTable'); // id of table


                //for (j = 0 ; j < tab.rows.length ; j++) {
                //    tab_text = tab_text + tab.rows[j].innerHTML + "</tr>";
                //    //tab_text=tab_text+"</tr>";
                //}

                tab_text = tab_text + "</table>";
                tab_text = tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
                tab_text = tab_text.replace(/<img[^>]*>/gi, ""); // remove if u want images in your table
                tab_text = tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

                var ua = window.navigator.userAgent;
                var msie = ua.indexOf("MSIE ");

                if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
                {
                    txtArea1.document.open("data:application/vnd.ms-excel", "replace");
                    txtArea1.document.write(tab_text);
                    txtArea1.document.close();
                    txtArea1.focus();
                    sa = txtArea1.document.execCommand("SaveAs", true, "Say Thanks to Sumit.xls");
                }
                else                 //other browser not tested on IE 11
                    sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));


                return (sa);
            }

            function testExport() {


                var strCopy = $("#tblSurveyResponseExcel").html();
                window.clipboardData.setData("Text", strCopy);
                var objExcel = new ActiveXObject("Excel.Application");
                objExcel.visible = false; var objWorkbook = objExcel.Workbooks.Add; var objWorksheet = objWorkbook.Worksheets(1); objWorksheet.Paste; objExcel.visible = true;

            }

        </script>



    </div>

    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />

    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a id="ancText"></a>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Survey Response
             <br />
        <br />
    </div>
    <br />
    <input type="button" id="btnExport" value=" Export Table data into Excel " />
    <table id="table2excel" style="display: none;">

        <tr class="noExl">
            <th>Column One</th>
            <th>Column Two</th>
            <th>Column Three</th>
        </tr>
        <tr>
            <td>row1 Col1</td>
            <td>row1 Col2</td>
            <td>row1 Col3</td>
        </tr>
        <tr>
            <td>row2 Col1</td>
            <td>row2 Col2</td>
            <td>row2 Col3</td>
        </tr>
        <tr>
            <td>row3 Col1</td>
            <td>row3 Col2</td>
            <td><a href="http://www.jquery2dotnet.com/">http://www.jquery2dotnet.com/</a>
            </td>
        </tr>
    </table>
    <div align="center">
        <asp:Label ID="lblMsg" runat="server" ForeColor="red"> </asp:Label>
    </div>
    <div style="clear: both;"></div>
    <div id="dvRespondContent" style="width: 1000px; margin-left: auto; margin-right: auto;">
        <%--  <div style="font-weight: bold;">1. Please provide your name and registered email with NSF</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">First Name</div>
        <div style="float: left;">
            <input type="text" id="txtFirstName" style="width: 500px;" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">Last Name</div>
        <div style="float: left;">
            <input type="text" id="Text1" style="width: 500px;" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">Your Registered Email with NSF</div>
        <div style="float: left;">
            <input type="text" id="Text2" style="width: 500px;" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>

        <div style="font-weight: bold;">2. Please provide your interest level in the following coaching area.</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; margin-left: 150px;"></div>
        <div style="float: left; font-weight: bold; margin-left: 30px;">Definitely interested</div>
        <div style="float: left; font-weight: bold; margin-left: 30px;">may be interested</div>
        <div style="float: left; font-weight: bold; margin-left: 30px;">Not interested</div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">PreMathCounts (Grades 4-5)</div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>

        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">Mathcounts (Grades 6,7,8)</div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div style="float: left; font-weight: bold; width: 100px;">SatEnglist</div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>
        <div style="float: left; font-weight: bold;">
            <input type="radio" />
        </div>

        <input type="button" id="btnSave" value="Save" />--%>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>

    <div style="float: right; position: relative; left: -63px; top: 20px;">
        <input type="button" id="btnExportToExcel" value="Export To Excel" />
    </div>
    <div style="clear: both;"></div>
    <div align="center" style="font-weight: bold;">Table 1: Survey Response List</div>
    <div style="clear: both;"></div>
    <div align="center" style="margin-left: auto; margin-right: auto; width: 600px;">
        <div style="float: left;">
            <b>Survey Title:</b> <span id="spnSurveyTitle">N/A</span>

        </div>
        <div style="float: left; margin-left: 20px;"><b>Respondent Count:</b> <span id="spnRespondentCount">N/A</span></div>
    </div>
    <div style="clear: both;"></div>
    <table align="center" id="tblSurveyCollectorList" style="border: 1px solid black; border-collapse: collapse;">
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <span id="spnErrMsg" style="color: red;"></span>
    </div>

    <div align="left" style='width: 800px; margin-left: auto; margin-right: auto; display: none;' id="dvSurveyResponse">

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" style="font-weight: bold;">Individual Response</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvResponseLayout">
            <div style="font-weight: bold;">Q1. Please select the name of your coach?</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div><span style="font-weight: bold;">ans:</span>Chandrasekhar Annavarapu </div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q2.  Please rate the following:</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div>
                <div style="font-weight: bold; width: 150px; float: left;">How was the overall experience of the coaching classes you attended? </div>
                <div style="float: left;">Highly satisfied </div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div>
                <div style="font-weight: bold; float: left; width: 150px;">How was your experience of your coach handling the class?</div>
                <div style="float: left;">Highly satisfied </div>
            </div>

            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q3. Please enter your response to the following questions:</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div>
                <div style="font-weight: bold; width: 150px; float: left;">Do you think that the students in your class are placed at appropriate level?</div>
                <div style="float: left; margin-left: 20px;">Yes</div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div>
                <div style="font-weight: bold; width: 150px; float: left;">Are you satisfied with the homework and other tools provided to you?</div>
                <div style="float: left; margin-left: 20px;">Yes</div>
            </div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q4. Is the course too challenging, too easy, or about right?</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div><span style="font-weight: bold;">ans:</span>Much too challenging</div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q5. Please answer the following questions on student participation:</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div>
                <div style="font-weight: bold; float: left; width: 150px;">Are students completing HW assignments and turning them in on time?</div>
                <div style="float: left; margin-left: 20px;">100%</div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div>
                <div style="font-weight: bold; font-weight: bold; width: 150px;">Are the students participating in class discussion?</div>
                <div style="float: left; margin-left: 20px;">100%</div>
            </div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q6. Please rate your support from the NSF team:</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div>
                <div style="font-weight: bold; font-weight: bold; width: 150px; float: left;">How easy was it to get the resources you needed to teach (class-work and home-work content, support for content related questions, support for Webex)?</div>
                <div style="float: left; margin-left: 20px;">Very Hard/Totally Ineffective</div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>

            <div>
                <div style="font-weight: bold; float: left; width: 150px;">How effective were the "coach-the-coach sessions"?</div>
                <div style="float: left; margin-left: 20px;">Very Hard/Totally Ineffective</div>

            </div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q7. Overall, are you satisfied with the teaching experience?</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div><span style="font-weight: bold;">ans:</span>Junior</div>
            <div style="clear: both; margin-bottom: 20px;"></div>

            <div style="font-weight: bold;">Q8. What level did you coach?</div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div><span style="font-weight: bold;">ans:</span>Extremely satisfied</div>
            <div style="clear: both; margin-bottom: 10px;"></div>
        </div>
    </div>




    <div id="dvSurveyList" runat="server" visible="false">

        <div style="clear: both;"></div>
        <div align="center" style="font-weight: bold;">Table 1: Survey List</div>
        <div style="float: right;">
        </div>
        <div style="clear: both;"></div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdSurveyTemlate" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdSurveyTemlate_RowCommand">

            <Columns>

                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" runat="server" Text="Respond" CommandName="Select" />
                        <asp:Button ID="BtnDesignSurvey" runat="server" Text="View Response" CommandName="Design Survey" />

                        <div style="display: none;">
                            <%--    <asp:Label runat="server" ID="lblYear" Text='<%#DataBinder.Eval(Container.DataItem,"Year") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblChapterID" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblRespondantType" Text='<%#DataBinder.Eval(Container.DataItem,"RespondentType") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblSurveyID" Text='<%#DataBinder.Eval(Container.DataItem,"SurveyID") %>'></asp:Label>--%>
                        </div>

                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="Year" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventName" HeaderText="Event"></asp:BoundField>

                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="name" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="Title" HeaderText="Survey Title"></asp:BoundField>
                <asp:BoundField DataField="RespondentType" HeaderText="Respondant Type"></asp:BoundField>
                <asp:BoundField DataField="ResponseCount" HeaderText="ResponseCount"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <div style="clear: both;"></div>
    <div runat="server" id="ExportDiv">

        <table id="tblSurveyResponseExcel" border="1">
          
        </table>
    </div>

    <asp:TextBox ID="txtSurveyResponse" runat="server" Style="display: none;"></asp:TextBox>
    <span id="spnJsonText"></span>
    <input type="hidden" id="hdnSurveyID" runat="server" value="0" />
    <input type="hidden" id="hdnResponseCount" value="0" />
    <input type="hidden" id="hdnSurveyTitle" value="0" />
    <input type="hidden" id="hdnRespondantID" value="0" />
    <input type="hidden" id="hdnResponse" runat="server" value="0" />


</asp:Content>
