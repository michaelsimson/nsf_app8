﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
//using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;

public partial class CoachClassCalNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["RoleID"] = "1";
        //Session["LoginID"] = "4240";

        if (Session["LoginID"] == null)
        {
            Server.Transfer("maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {
                hdnLoginID.Value = Session["LoginID"].ToString();
                hdnRoleID.Value = Session["RoleID"].ToString(); ;
            }
        }
    }



    public static bool IsRefresh = false;
    public class CoachClassCal
    {

        public string CoachClassCalID { get; set; }
        public string MemberId { get; set; }
        public string EventId { get; set; }
        public string EventYear { get; set; }
        public string EventCode { get; set; }
        public string ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductId { get; set; }
        public string Product { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public string SessionNo { get; set; }
        public string EndDate { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Time { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string SerNo { get; set; }
        public string WeekNo { get; set; }
        public string Status { get; set; }
        public string Substitute { get; set; }
        public string Makeup { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Reason { get; set; }
        public string ClassType { get; set; }
        public string SubstituteCoachName { get; set; }
        public string ClassStartDate { get; set; }
        public string ClassEndDate { get; set; }
        public string CoachpaperID { get; set; }
        public string HWDeadlineDate { get; set; }
        public string HWDueDate { get; set; }
        public string ARelDate { get; set; }
        public string SRelDate { get; set; }
        public string LoginID { get; set; }
        public string RetVal { get; set; }
        public string CoachRelID { get; set; }
        public string Mode { get; set; }
        public string MakeupCoachName { get; set; }
        public string IsDisabled { get; set; }

        public string URl { get; set; }
        public string Service { get; set; }
        public string APIKey { get; set; }
        public string APISecret { get; set; }
        public string HostID { get; set; }
        public string Topic { get; set; }
        public string MeetingType { get; set; }
        public string StartTime { get; set; }
        public string StartDate { get; set; }
        //  public int Duration { get; set; }
        public string SessionKey { get; set; }
        public string HostURL { get; set; }
        public string JoinURl { get; set; }
        public int Retval { get; set; }
        public string UserID { get; set; }
        public string Pwd { get; set; }

        public string StrStartTime { get; set; }
        public string StrEndTime { get; set; }
        public string ExtraSessionKey { get; set; }
        public string ExtraSessionURL { get; set; }
        public string WeekNoReset { get; set; }
        public string WeekNoStimulation { get; set; }
        public string Action { get; set; }
    }

    public class ProductGroups
    {
        public string ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductId { get; set; }
        public string Product { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public string SessionNo { get; set; }
        public int MemberID { get; set; }
        public string Coachname { get; set; }
        public string Eventyear { get; set; }
        public string WeekNo { get; set; }
        public string ClassType { get; set; }
        public string Status { get; set; }
        public string ClassDate { get; set; }
    }
    public class ZoomSessions
    {
        public string URl { get; set; }
        public string Service { get; set; }
        public string APIKey { get; set; }
        public string APISecret { get; set; }
        public string HostID { get; set; }
        public string Topic { get; set; }
        public string MeetingType { get; set; }
        public string StartTime { get; set; }
        public string StartDate { get; set; }
        public int Duration { get; set; }
        public string SessionKey { get; set; }
        public string HostURL { get; set; }
        public string JoinURl { get; set; }
        public int Retval { get; set; }
    }
    public class CoachingDateCal
    {
        public string StartDate { get; set; }
        public string EndDate { get; set; }
        public string HolidayName { get; set; }
        public string Time { get; set; }
        public string RetVal { get; set; }
    }

    [WebMethod]
    public static List<CoachClassCal> ListSchedule(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        List<CoachClassCal> ObjListCoachClassCal = new List<CoachClassCal>();
        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "  select CoachClassCalID,CL.[MemberId],CL.[EventId],CL.[EventYear] ,CL.[ProductGroupId] ,CL.[ProductGroup],CL.[ProductId] ,CL.[Product],CL.[Semester],CL.[Level],CL.[SessionNo] ,CL.[Date] ,CL.[Day] ,CL.[Time],CL.[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],CL.[CreateDate],CL.[CreatedBy],CL.[ModifyDate],CL.[ModifiedBy], Makeup, ClassType, IP.FirstName+' '+IP.lastname as name,IP1.FirstName+' '+IP1.lastname as MakeupName, CD.QReleaseDate, CD.QDeadLineDate, CD.AReleaseDate, CD.SReleaseDate from CoachClassCal CL left join Indspouse Ip on (Ip.AutoMemberID=Cl.Substitute) left join Indspouse IP1 on(IP1.AutoMemberID=Cl.Makeup) left join Coachpapers CP on (CP.WeekId=Cl.WeekNo and CP.Semester=CL.Semester and CP.Eventyear=Cl.Eventyear and CP.ProductgroupID=CL.ProductGroupID and CP.ProductID=CL.ProductID and CP.Level=CL.Level and CP.DocType='Q') left join CoachRelDates CD on (CD.CoachPaperID=CP.CoachpaperID and CD.memberID=Cl.MemberID and CD.Semester=CL.Semester and CD.Eventyear=Cl.Eventyear and CD.ProductgroupID=CL.ProductGroupID and CD.ProductID=CL.ProductID and CD.Level=CL.Level and CD.Session=CL.SessionNo) where CL.EventYear=" + CoachClassCal.EventYear + " and CL.Semester='" + CoachClassCal.Semester + "' and CL.EventID=" + CoachClassCal.EventId + " and CL.ProductGroupID=" + CoachClassCal.ProductGroupId + " and CL.ProductId=" + CoachClassCal.ProductId + " and CL.Level='" + CoachClassCal.Level + "' and CL.SessionNo=" + CoachClassCal.SessionNo + " and CL.MemberId=" + CoachClassCal.MemberId + "";

            if (Convert.ToInt32(CoachClassCal.CoachClassCalID) > 0)
            {
                cmdText += " and CoachClassCalID=" + CoachClassCal.CoachClassCalID + "";
            }

            cmdText += " order by Cl.WeekNo, Date";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            List<CoachingDateCal> ObjHolidayList = new List<CoachingDateCal>();
            ObjHolidayList = ListHolidays(CoachClassCal);
            DateTime dtPrevClassDate = new DateTime();
            int j = 0;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachSchedule = new CoachClassCal();

                        objCoachSchedule.CoachClassCalID = dr["CoachClassCalID"].ToString();
                        objCoachSchedule.MemberId = dr["MemberId"].ToString();
                        objCoachSchedule.EventId = dr["EventId"].ToString();
                        objCoachSchedule.EventYear = dr["EventYear"].ToString();
                        objCoachSchedule.ProductGroupId = dr["ProductGroupId"].ToString();
                        objCoachSchedule.ProductGroup = dr["ProductGroup"].ToString();
                        objCoachSchedule.ProductId = dr["ProductId"].ToString();
                        objCoachSchedule.Product = dr["Product"].ToString();
                        objCoachSchedule.Semester = dr["Semester"].ToString();
                        objCoachSchedule.Level = dr["Level"].ToString();
                        objCoachSchedule.SessionNo = dr["SessionNo"].ToString();
                        objCoachSchedule.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        objCoachSchedule.Day = dr["Day"].ToString();
                        objCoachSchedule.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        if (dr["Duration"].ToString() != "")
                        {
                            objCoachSchedule.Duration = Convert.ToInt32(dr["Duration"].ToString());
                        }
                        else
                        {
                            objCoachSchedule.Duration = 0;
                        }
                        objCoachSchedule.WeekNo = dr["WeekNo"].ToString();
                        objCoachSchedule.Status = dr["Status"].ToString();
                        objCoachSchedule.Reason = dr["Reason"].ToString();
                        objCoachSchedule.Substitute = dr["Substitute"].ToString().Trim();
                        objCoachSchedule.Makeup = dr["Makeup"].ToString();
                        objCoachSchedule.ClassType = dr["ClassType"].ToString();
                        objCoachSchedule.SubstituteCoachName = dr["name"].ToString();
                        objCoachSchedule.MakeupCoachName = dr["MakeupName"].ToString();
                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "" && dr["QReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachSchedule.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "" && dr["QDeadlineDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachSchedule.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "" && dr["SReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachSchedule.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "" && dr["AReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachSchedule.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (Convert.ToDateTime(dr["Date"].ToString()) < dtCurrentDate.AddDays(-14))
                        {
                            objCoachSchedule.IsDisabled = "Yes";
                        }
                        else
                        {
                            objCoachSchedule.IsDisabled = "No";
                        }
                        ObjListCoachClassCal.Add(objCoachSchedule);


                        CoachClassCal objCoachClassCal = new CoachClassCal();
                        DateTime dtholidayDate = new DateTime();
                        try
                        {
                            dtholidayDate = Convert.ToDateTime(ObjHolidayList[j].StartDate.ToString());
                        }
                        catch
                        {
                        }
                        DateTime dtClassDate = Convert.ToDateTime(dr["Date"].ToString());

                        DateTime dtClassScheduled = Convert.ToDateTime(dr["Date"].ToString());
                        if (j == 0)
                        {
                            dtPrevClassDate = Convert.ToDateTime(dr["Date"].ToString());
                            dtClassScheduled = Convert.ToDateTime(dr["Date"].ToString());
                        }
                        else
                        {
                            dtClassScheduled = Convert.ToDateTime(dr["Date"].ToString()).AddDays(-7);
                        }

                        if (dtClassDate > dtholidayDate && dtClassDate.AddDays(-7) == dtholidayDate && dtPrevClassDate != dtClassScheduled)
                        {
                            CoachClassCal objCoachClassCalHoliday = new CoachClassCal();
                            objCoachClassCalHoliday.CoachClassCalID = "";
                            objCoachClassCalHoliday.MemberId = "";
                            objCoachClassCalHoliday.EventId = "13";
                            objCoachClassCalHoliday.EventYear = CoachClassCal.EventYear;
                            objCoachClassCalHoliday.ProductGroupId = CoachClassCal.ProductGroupId;
                            objCoachClassCalHoliday.ProductGroup = CoachClassCal.ProductGroup;
                            objCoachClassCalHoliday.ProductId = CoachClassCal.ProductId;
                            objCoachClassCalHoliday.Product = CoachClassCal.Product;
                            objCoachClassCalHoliday.Semester = CoachClassCal.Semester;
                            objCoachClassCalHoliday.Level = CoachClassCal.Level;
                            objCoachClassCalHoliday.SessionNo = CoachClassCal.SessionNo;
                            objCoachClassCalHoliday.Date = Convert.ToDateTime(ObjHolidayList[j].StartDate.ToString()).ToString("MM/dd/yyyy");
                            objCoachClassCalHoliday.Time = Convert.ToDateTime(ObjHolidayList[j].Time.ToString()).ToString("HH:mm");
                            objCoachClassCalHoliday.Day = Convert.ToDateTime(ObjHolidayList[j].StartDate.ToString()).ToString("dddd");

                            objCoachClassCalHoliday.WeekNo = "";
                            objCoachClassCalHoliday.Status = "Cancelled";
                            objCoachClassCalHoliday.Reason = "";
                            objCoachClassCalHoliday.Substitute = "";
                            objCoachClassCalHoliday.Makeup = "";
                            objCoachClassCalHoliday.ClassType = "Holiday";
                            objCoachClassCalHoliday.SubstituteCoachName = "";
                            objCoachClassCalHoliday.MakeupCoachName = "";

                            objCoachClassCalHoliday.HWDeadlineDate = "";

                            objCoachClassCalHoliday.HWDueDate = "";

                            objCoachClassCalHoliday.SRelDate = "";

                            objCoachClassCalHoliday.ARelDate = "";

                            objCoachClassCalHoliday.IsDisabled = "Yes";
                            objListSurvey.Add(objCoachClassCalHoliday);
                            j++;
                        }

                        if (dtClassDate == dtholidayDate)
                        {
                            j++;
                        }

                        objCoachClassCal.CoachClassCalID = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.MemberId = dr["MemberId"].ToString();
                        objCoachClassCal.EventId = dr["EventId"].ToString();
                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
                        objCoachClassCal.Product = dr["Product"].ToString();
                        objCoachClassCal.Semester = dr["Semester"].ToString();
                        objCoachClassCal.Level = dr["Level"].ToString();
                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = dr["Day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        if (dr["Duration"].ToString() != "")
                        {
                            objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
                        }
                        else
                        {
                            objCoachClassCal.Duration = 0;
                        }
                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
                        objCoachClassCal.Status = dr["Status"].ToString();
                        objCoachClassCal.Reason = dr["Reason"].ToString();
                        objCoachClassCal.Substitute = dr["Substitute"].ToString().Trim();
                        objCoachClassCal.Makeup = dr["Makeup"].ToString();
                        objCoachClassCal.ClassType = dr["ClassType"].ToString();
                        objCoachClassCal.SubstituteCoachName = dr["name"].ToString();
                        objCoachClassCal.MakeupCoachName = dr["MakeupName"].ToString();
                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "" && dr["QReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "" && dr["QDeadlineDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "" && dr["SReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "" && dr["AReleaseDate"].ToString() != "1/1/1900 12:00:00 AM")
                        {
                            objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (Convert.ToDateTime(dr["Date"].ToString()) < dtCurrentDate.AddDays(-14))
                        {
                            objCoachClassCal.IsDisabled = "Yes";
                        }
                        else
                        {
                            objCoachClassCal.IsDisabled = "No";
                        }
                        objListSurvey.Add(objCoachClassCal);

                        dtPrevClassDate = Convert.ToDateTime(dr["Date"].ToString());


                    }
                    if (j == ObjHolidayList.Count)
                    {

                    }
                    else
                    {
                        for (int i = j; i < ObjHolidayList.Count; i++)
                        {
                            CoachClassCal objCoachClassCal = new CoachClassCal();
                            objCoachClassCal.CoachClassCalID = "";
                            objCoachClassCal.MemberId = "";
                            objCoachClassCal.EventId = "13";
                            objCoachClassCal.EventYear = CoachClassCal.EventYear;
                            objCoachClassCal.ProductGroupId = CoachClassCal.ProductGroupId;
                            objCoachClassCal.ProductGroup = CoachClassCal.ProductGroup;
                            objCoachClassCal.ProductId = CoachClassCal.ProductId;
                            objCoachClassCal.Product = CoachClassCal.Product;
                            objCoachClassCal.Semester = CoachClassCal.Semester;
                            objCoachClassCal.Level = CoachClassCal.Level;
                            objCoachClassCal.SessionNo = CoachClassCal.SessionNo;
                            objCoachClassCal.Date = Convert.ToDateTime(ObjHolidayList[i].StartDate.ToString()).ToString("MM/dd/yyyy");
                            objCoachClassCal.Day = Convert.ToDateTime(ObjHolidayList[i].StartDate.ToString()).ToString("dddd");
                            objCoachClassCal.Time = Convert.ToDateTime(ObjHolidayList[j].Time.ToString()).ToString("HH:mm");
                            objCoachClassCal.WeekNo = "";
                            objCoachClassCal.Status = "Cancelled";
                            objCoachClassCal.Reason = "";
                            objCoachClassCal.Substitute = "";
                            objCoachClassCal.Makeup = "";
                            objCoachClassCal.ClassType = "Holiday";
                            objCoachClassCal.SubstituteCoachName = "";
                            objCoachClassCal.MakeupCoachName = "";

                            objCoachClassCal.HWDeadlineDate = "";

                            objCoachClassCal.HWDueDate = "";

                            objCoachClassCal.SRelDate = "";

                            objCoachClassCal.ARelDate = "";

                            objCoachClassCal.IsDisabled = "Yes";
                            objListSurvey.Add(objCoachClassCal);
                        }
                    }
                    try
                    {
                        UpdateHolidayClass(ObjListCoachClassCal, ObjHolidayList);
                    }
                    catch
                    {
                    }
                    //if (IsRefresh == false)
                    //{

                    //}

                }
                else
                {
                    for (int i = 0; i < ObjHolidayList.Count; i++)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();
                        objCoachClassCal.CoachClassCalID = "";
                        objCoachClassCal.MemberId = "";
                        objCoachClassCal.EventId = "13";
                        objCoachClassCal.EventYear = CoachClassCal.EventYear;
                        objCoachClassCal.ProductGroupId = CoachClassCal.ProductGroupId;
                        objCoachClassCal.ProductGroup = CoachClassCal.ProductGroup;
                        objCoachClassCal.ProductId = CoachClassCal.ProductId;
                        objCoachClassCal.Product = CoachClassCal.Product;
                        objCoachClassCal.Semester = CoachClassCal.Semester;
                        objCoachClassCal.Level = CoachClassCal.Level;
                        objCoachClassCal.SessionNo = CoachClassCal.SessionNo;
                        objCoachClassCal.Date = Convert.ToDateTime(ObjHolidayList[i].StartDate.ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = Convert.ToDateTime(ObjHolidayList[i].StartDate.ToString()).ToString("dddd");
                        objCoachClassCal.Time = Convert.ToDateTime(ObjHolidayList[j].Time.ToString()).ToString("HH:mm");
                        objCoachClassCal.WeekNo = "";
                        objCoachClassCal.Status = "Cancelled";
                        objCoachClassCal.Reason = "";
                        objCoachClassCal.Substitute = "";
                        objCoachClassCal.Makeup = "";
                        objCoachClassCal.ClassType = "Holiday";
                        objCoachClassCal.SubstituteCoachName = "";
                        objCoachClassCal.MakeupCoachName = "";

                        objCoachClassCal.HWDeadlineDate = "";

                        objCoachClassCal.HWDueDate = "";

                        objCoachClassCal.SRelDate = "";

                        objCoachClassCal.ARelDate = "";

                        objCoachClassCal.IsDisabled = "Yes";
                        objListSurvey.Add(objCoachClassCal);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<CoachClassCal> ListScheduleBasedOnId(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSchedule = new List<CoachClassCal>();
        DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "  select CoachClassCalID,CL.[MemberId],CL.[EventId],CL.[EventYear] ,CL.[ProductGroupId] ,CL.[ProductGroup],CL.[ProductId] ,CL.[Product],CL.[Semester],CL.[Level],CL.[SessionNo] ,CL.[Date] ,CL.[Day] ,CL.[Time],CL.[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],CL.[CreateDate],CL.[CreatedBy],CL.[ModifyDate],CL.[ModifiedBy], Makeup, ClassType, IP.FirstName+' '+IP.lastname as name,IP1.FirstName+' '+IP1.lastname as MakeupName, CD.QReleaseDate, CD.QDeadLineDate, CD.AReleaseDate, CD.SReleaseDate from CoachClassCal CL left join Indspouse Ip on (Ip.AutoMemberID=Cl.Substitute) left join Indspouse IP1 on(IP1.AutoMemberID=Cl.Makeup) left join Coachpapers CP on (CP.WeekId=Cl.WeekNo and CP.Semester=CL.Semester and CP.Eventyear=Cl.Eventyear and CP.ProductgroupID=CL.ProductGroupID and CP.ProductID=CL.ProductID and CP.Level=CL.Level and CP.DocType='Q') left join CoachRelDates CD on (CD.CoachPaperID=CP.CoachpaperID and CD.memberID=Cl.MemberID and CD.Semester=CL.Semester and CD.Eventyear=Cl.Eventyear and CD.ProductgroupID=CL.ProductGroupID and CD.ProductID=CL.ProductID and CD.Level=CL.Level and CD.Session=CL.SessionNo) where CL.EventYear=" + CoachClassCal.EventYear + " and CL.Semester='" + CoachClassCal.Semester + "' and CL.EventID=" + CoachClassCal.EventId + " and CL.ProductGroupID=" + CoachClassCal.ProductGroupId + " and CL.ProductId=" + CoachClassCal.ProductId + " and CL.Level='" + CoachClassCal.Level + "' and CL.SessionNo=" + CoachClassCal.SessionNo + " and CL.MemberId=" + CoachClassCal.MemberId + "";

            if (Convert.ToInt32(CoachClassCal.CoachClassCalID) > 0)
            {
                cmdText += " and CoachClassCalID=" + CoachClassCal.CoachClassCalID + "";
            }

            cmdText += " order by Cl.WeekNo";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            List<CoachingDateCal> ObjHolidayList = new List<CoachingDateCal>();
            ObjHolidayList = ListHolidays(CoachClassCal);
            DateTime dtPrevClassDate = new DateTime();
            int j = 0;
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();
                        objCoachClassCal.CoachClassCalID = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.MemberId = dr["MemberId"].ToString();
                        objCoachClassCal.EventId = dr["EventId"].ToString();
                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
                        objCoachClassCal.Product = dr["Product"].ToString();
                        objCoachClassCal.Semester = dr["Semester"].ToString();
                        objCoachClassCal.Level = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = dr["Day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        if (dr["Duration"].ToString() != "")
                        {
                            objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
                        }
                        else
                        {
                            objCoachClassCal.Duration = 0;
                        }
                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
                        objCoachClassCal.Status = dr["Status"].ToString();
                        objCoachClassCal.Reason = dr["Reason"].ToString();
                        objCoachClassCal.Substitute = dr["Substitute"].ToString().Trim();
                        objCoachClassCal.Makeup = dr["Makeup"].ToString();
                        objCoachClassCal.ClassType = dr["ClassType"].ToString();
                        objCoachClassCal.SubstituteCoachName = dr["name"].ToString();
                        objCoachClassCal.MakeupCoachName = dr["MakeupName"].ToString();
                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "" && Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy") != "01/01/1900")
                        {
                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "" && Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy") != "01/01/1900")
                        {
                            objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "" && Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy") != "01/01/1900")
                        {
                            objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "" && Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy") != "01/01/1900")
                        {
                            objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (Convert.ToDateTime(dr["Date"].ToString()) < dtCurrentDate.AddDays(-14))
                        {
                            objCoachClassCal.IsDisabled = "Yes";
                        }
                        else
                        {
                            objCoachClassCal.IsDisabled = "No";
                        }
                        objListSchedule.Add(objCoachClassCal);
                    }
                }
            }
        }
        catch
        {
        }
        return objListSchedule;
    }

    [WebMethod]
    public static List<CoachingDateCal> ListHolidays(CoachClassCal CoachClsCal)
    {
        string CmdHoliday = string.Empty;
        List<CoachingDateCal> objListholiday = new List<CoachingDateCal>();
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        CmdHoliday = " select * from coachingDateCal where Eventyear=" + CoachClsCal.EventYear + " and ScheduleType<>'Term' and ProductGroupId=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        SqlCommand cmd = new SqlCommand(CmdHoliday, cn);
        SqlDataAdapter daHoliday = new SqlDataAdapter(cmd);
        DataSet dsHoliday = new DataSet();
        daHoliday.Fill(dsHoliday);

        string cmdClassText = string.Empty;
        cmdClassText = "  select distinct StartDate, EndDate, (Select distinct Day from CalSignup where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + " and Level='" + CoachClsCal.Level + "' and SessionNo='" + CoachClsCal.SessionNo + "' and MemberID=" + CoachClsCal.MemberId + " and Accepted='Y') as day, (Select distinct Time from CalSignup where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + " and Level='" + CoachClsCal.Level + "'  and SessionNo='" + CoachClsCal.SessionNo + "' and MemberID=" + CoachClsCal.MemberId + " and Accepted='Y') as Time from CoachingDateCal where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and ScheduleType='Term' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        cmd = new SqlCommand(cmdClassText, cn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        DateTime dtClassStartDate = new DateTime();
        string ClassStartDate = string.Empty;
        string classDay = string.Empty;
        string ClassEndDate = string.Empty;
        DateTime dtClassEndDate = new DateTime();
        String ClassTimne = string.Empty;
        if (null != ds)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ClassTimne = dr["Time"].ToString();
                    classDay = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd");
                    if (classDay == dr["day"].ToString())
                    {
                        dtClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                        ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");

                        ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    }
                    else
                    {

                        DateTime dtDate = new DateTime();
                        DateTime dtCoachDate = new DateTime();
                        ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        dtCoachDate = Convert.ToDateTime(dr["StartDate"].ToString());
                        for (int i = 1; i <= 7; i++)
                        {


                            dtDate = dtCoachDate.AddDays(i);
                            if (Convert.ToDateTime(dtDate.ToShortDateString()).ToString("dddd") == dr["day"].ToString())
                            {
                                ClassStartDate = Convert.ToDateTime(dtDate.ToString()).ToString("MM/dd/yyyy");
                                dtClassStartDate = Convert.ToDateTime(dtDate.ToString());
                            }
                        }


                    }
                }
            }
        }

        ArrayList ArrClasDates = new ArrayList();
        dtClassEndDate = Convert.ToDateTime(ClassEndDate.ToString());
        int classcount = 1;
        for (int i = 0; i < 40; i++)
        {
            if (dtClassStartDate <= dtClassEndDate)
            {
                ArrClasDates.Add(dtClassStartDate.ToShortDateString());
                dtClassStartDate = dtClassStartDate.AddDays(7);
            }
        }
        string IsHolidayExists = "";
        if (null != dsHoliday)
        {
            if (dsHoliday.Tables[0].Rows.Count > 0)
            {
                IsHolidayExists = "Yes";
                foreach (DataRow dr in dsHoliday.Tables[0].Rows)
                {
                    DateTime dtHolidayStartDate = new DateTime();
                    DateTime dtHolidayEndDate = new DateTime();
                    DateTime dtTermDate = new DateTime();
                    for (int j = 0; j < ArrClasDates.Count; j++)
                    {
                        dtTermDate = Convert.ToDateTime(ArrClasDates[j].ToString());
                        dtHolidayStartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                        dtHolidayEndDate = Convert.ToDateTime(dr["EndDate"].ToString());
                        if (dtTermDate >= dtHolidayStartDate && dtTermDate <= dtHolidayEndDate)
                        {
                            CoachingDateCal ObjDateCal = new CoachingDateCal();

                            if (Convert.ToString(dr["StartDate"]) != "")
                            {
                                ObjDateCal.StartDate = dtTermDate.ToShortDateString();
                                ObjDateCal.HolidayName = dr["ScheduleType"].ToString();
                                ObjDateCal.Time = ClassTimne;
                            }
                            string IsBind = "";
                            for (int k = 0; k < objListholiday.Count; k++)
                            {
                                if (ObjDateCal.StartDate == objListholiday[k].StartDate.ToString())
                                {
                                    IsBind = "No";
                                }
                            }
                            if (IsBind == "")
                            {
                                objListholiday.Add(ObjDateCal);
                            }
                        }
                    }

                }
            }

        }
        return objListholiday;
    }

    //[WebMethod]
    //public static List<CoachClassCal> ListSchedule(CoachClassCal CoachClassCal)
    //{

    //    int retVal = -1;
    //    List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
    //    DateTime dtCurrentDate = Convert.ToDateTime(DateTime.Now.ToShortDateString());
    //    try
    //    {

    //        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
    //        cn.Open();

    //        string cmdText = string.Empty;
    //        cmdText = "  select CoachClassCalID,CL.[MemberId],CL.[EventId],CL.[EventYear] ,CL.[ProductGroupId] ,CL.[ProductGroup],CL.[ProductId] ,CL.[Product],CL.[Semester],CL.[Level],CL.[SessionNo] ,CL.[Date] ,CL.[Day] ,CL.[Time],CL.[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],CL.[CreateDate],CL.[CreatedBy],CL.[ModifyDate],CL.[ModifiedBy], Makeup, ClassType, IP.FirstName+' '+IP.lastname as name,IP1.FirstName+' '+IP1.lastname as MakeupName, CD.QReleaseDate, CD.QDeadLineDate, CD.AReleaseDate, CD.SReleaseDate from CoachClassCal CL left join Indspouse Ip on (Ip.AutoMemberID=Cl.Substitute) left join Indspouse IP1 on(IP1.AutoMemberID=Cl.Makeup) left join Coachpapers CP on (CP.WeekId=Cl.WeekNo and CP.Semester=CL.Semester and CP.Eventyear=Cl.Eventyear and CP.ProductgroupID=CL.ProductGroupID and CP.ProductID=CL.ProductID and CP.Level=CL.Level and CP.DocType='Q') left join CoachRelDates CD on (CD.CoachPaperID=CP.CoachpaperID and CD.memberID=Cl.MemberID and CD.Semester=CL.Semester and CD.Eventyear=Cl.Eventyear and CD.ProductgroupID=CL.ProductGroupID and CD.ProductID=CL.ProductID and CD.Level=CL.Level and CD.Session=CL.SessionNo) where CL.EventYear=" + CoachClassCal.EventYear + " and CL.Semester='" + CoachClassCal.Semester + "' and CL.EventID=" + CoachClassCal.EventId + " and CL.ProductGroupID=" + CoachClassCal.ProductGroupId + " and CL.ProductId=" + CoachClassCal.ProductId + " and CL.Level='" + CoachClassCal.Level + "' and CL.SessionNo=" + CoachClassCal.SessionNo + " and CL.MemberId=" + CoachClassCal.MemberId + "";

    //        if (Convert.ToInt32(CoachClassCal.CoachClassCalID) > 0)
    //        {
    //            cmdText += " and CoachClassCalID=" + CoachClassCal.CoachClassCalID + "";
    //        }

    //        cmdText += " order by Cl.WeekNo";

    //        SqlCommand cmd = new SqlCommand(cmdText, cn);
    //        SqlDataAdapter da = new SqlDataAdapter(cmd);
    //        DataSet ds = new DataSet();
    //        da.Fill(ds);

    //        //Getting holiday list
    //        string CmdHoliday = string.Empty;
    //        CmdHoliday = " select * from coachingDateCal where Eventyear=" + CoachClassCal.EventYear + " and ScheduleType<>'Term'";
    //        cmd = new SqlCommand(CmdHoliday, cn);
    //        SqlDataAdapter daHoliday = new SqlDataAdapter(cmd);
    //        DataSet dsHoliday = new DataSet();
    //        daHoliday.Fill(dsHoliday);

    //        string IsHolidayExists = "";
    //        if (null != dsHoliday)
    //        {
    //            if (dsHoliday.Tables[0].Rows.Count > 0)
    //            {
    //                IsHolidayExists = "Yes";
    //            }

    //        }
    //        int i = 0;
    //        int iHolidayCount = dsHoliday.Tables[0].Rows.Count;
    //        if (null != ds)
    //        {
    //            if (ds.Tables[0].Rows.Count > 0)
    //            {
    //                foreach (DataRow dr in ds.Tables[0].Rows)
    //                {
    //                    CoachClassCal objCoachClassCal = new CoachClassCal();
    //                    string HolStartDate = string.Empty;
    //                    string HolEndDate = string.Empty;
    //                    string IsHolidayScheduled = string.Empty;
    //                    DateTime dtHolStartDate = new DateTime();
    //                    DateTime dtHolEndDate = new DateTime();
    //                    DateTime dtClassDate = new DateTime();
    //                    if (IsHolidayExists == "Yes")
    //                    {
    //                        if (i < iHolidayCount)
    //                        {

    //                            if (dsHoliday.Tables[0].Rows[i]["StartDate"].ToString() != "")
    //                            {
    //                                HolStartDate = Convert.ToDateTime(dsHoliday.Tables[0].Rows[i]["StartDate"].ToString()).ToShortDateString();
    //                            }
    //                            if (dsHoliday.Tables[0].Rows[i]["EndDate"].ToString() != "")
    //                            {
    //                                HolEndDate = Convert.ToDateTime(dsHoliday.Tables[0].Rows[i]["EndDate"].ToString()).ToShortDateString();
    //                            }
    //                            dtHolStartDate = Convert.ToDateTime(HolStartDate);
    //                            dtHolEndDate = Convert.ToDateTime(HolEndDate);
    //                            dtClassDate = Convert.ToDateTime(dr["Date"]);
    //                            if (dtClassDate >= dtHolStartDate || dtClassDate <= dtHolEndDate)
    //                            {
    //                                string CmdScheduledText = " select count(*) from coachClassCal where Date='" + dr["Date"].ToString() + "' and MemberId=" + dr["memberId"].ToString() + " and ProductGroupId=" + dr["ProductGroupId"].ToString() + " and ProductId=" + dr["ProductId"].ToString() + " and Semester='" + dr["Semester"].ToString() + "' and level='" + dr["Level"].ToString() + "' and SessionNo=" + dr["SessionNo"].ToString() + "";
    //                                try
    //                                {
    //                                    int schedulecount = 0;
    //                                    schedulecount = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, CmdScheduledText).ToString());
    //                                    if (schedulecount > 0)
    //                                    {
    //                                        IsHolidayScheduled = "Yes";
    //                                    }
    //                                }
    //                                catch
    //                                {

    //                                }

    //                            }

    //                        }
    //                    }
    //                    i++;
    //                    dtHolStartDate = Convert.ToDateTime(HolStartDate);
    //                    dtHolEndDate = Convert.ToDateTime(HolEndDate);
    //                    dtClassDate = Convert.ToDateTime(dr["Date"]);

    //                    if ((dtClassDate >= dtHolStartDate || dtClassDate <= dtHolEndDate) && IsHolidayScheduled == "")
    //                    {
    //                        objCoachClassCal.CoachClassCalID = "";
    //                        objCoachClassCal.MemberId = "";
    //                        objCoachClassCal.EventId = dr["EventId"].ToString();
    //                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
    //                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
    //                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
    //                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
    //                        objCoachClassCal.Product = dr["Product"].ToString();
    //                        objCoachClassCal.Semester = dr["Semester"].ToString();
    //                        objCoachClassCal.Level = dr["CoachClassCalID"].ToString();
    //                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
    //                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
    //                        objCoachClassCal.Day = dr["Day"].ToString();
    //                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
    //                        if (dr["Duration"].ToString() != "")
    //                        {
    //                            objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
    //                        }
    //                        else
    //                        {
    //                            objCoachClassCal.Duration = 0;
    //                        }
    //                        objCoachClassCal.WeekNo = "";
    //                        objCoachClassCal.Status = "Cancelled";
    //                        objCoachClassCal.Reason = "";
    //                        objCoachClassCal.Substitute = "";
    //                        objCoachClassCal.Makeup = "";
    //                        objCoachClassCal.ClassType = "holiday";
    //                        objCoachClassCal.SubstituteCoachName = "";
    //                        objCoachClassCal.MakeupCoachName = "";

    //                        objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");

    //                        objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");

    //                        objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");

    //                        objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");

    //                        if (Convert.ToDateTime(dr["Date"].ToString()) < dtCurrentDate.AddDays(-14))
    //                        {
    //                            objCoachClassCal.IsDisabled = "Yes";
    //                        }
    //                        else
    //                        {
    //                            objCoachClassCal.IsDisabled = "No";
    //                        }
    //                    }
    //                    else
    //                    {

    //                        objCoachClassCal.CoachClassCalID = dr["CoachClassCalID"].ToString();
    //                        objCoachClassCal.MemberId = dr["MemberId"].ToString();
    //                        objCoachClassCal.EventId = dr["EventId"].ToString();
    //                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
    //                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
    //                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
    //                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
    //                        objCoachClassCal.Product = dr["Product"].ToString();
    //                        objCoachClassCal.Semester = dr["Semester"].ToString();
    //                        objCoachClassCal.Level = dr["CoachClassCalID"].ToString();
    //                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
    //                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
    //                        objCoachClassCal.Day = dr["Day"].ToString();
    //                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
    //                        if (dr["Duration"].ToString() != "")
    //                        {
    //                            objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
    //                        }
    //                        else
    //                        {
    //                            objCoachClassCal.Duration = 0;
    //                        }
    //                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
    //                        objCoachClassCal.Status = dr["Status"].ToString();
    //                        objCoachClassCal.Reason = dr["Reason"].ToString();
    //                        objCoachClassCal.Substitute = dr["Substitute"].ToString().Trim();
    //                        objCoachClassCal.Makeup = dr["Makeup"].ToString();
    //                        objCoachClassCal.ClassType = dr["ClassType"].ToString();
    //                        objCoachClassCal.SubstituteCoachName = dr["name"].ToString();
    //                        objCoachClassCal.MakeupCoachName = dr["MakeupName"].ToString();
    //                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "")
    //                        {
    //                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
    //                        }
    //                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "")
    //                        {
    //                            objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
    //                        }
    //                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "")
    //                        {
    //                            objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
    //                        }
    //                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "")
    //                        {
    //                            objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
    //                        }
    //                        if (Convert.ToDateTime(dr["Date"].ToString()) < dtCurrentDate.AddDays(-14))
    //                        {
    //                            objCoachClassCal.IsDisabled = "Yes";
    //                        }
    //                        else
    //                        {
    //                            objCoachClassCal.IsDisabled = "No";
    //                        }
    //                    }
    //                    objListSurvey.Add(objCoachClassCal);
    //                }
    //            }
    //            else if (dsHoliday.Tables[0].Rows.Count > 0)
    //            {
    //                foreach (DataRow dr in dsHoliday.Tables[0].Rows)
    //                {
    //                    if (Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd") == "Sunday")
    //                    {
    //                        CoachClassCal objCoachClassCal = new CoachClassCal();
    //                        objCoachClassCal.CoachClassCalID = "";
    //                        objCoachClassCal.MemberId = "";
    //                        objCoachClassCal.EventId = "13";
    //                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
    //                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
    //                        objCoachClassCal.ProductGroup = dr["ProductGroupCode"].ToString();
    //                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
    //                        objCoachClassCal.Product = dr["ProductCode"].ToString();
    //                        objCoachClassCal.Semester = dr["Semester"].ToString();
    //                        objCoachClassCal.Level = "";
    //                        objCoachClassCal.SessionNo = "";
    //                        objCoachClassCal.Date = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
    //                        objCoachClassCal.Day = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd");
    //                        objCoachClassCal.Time = "";
    //                        //if (dr["Duration"].ToString() != "")
    //                        //{
    //                        objCoachClassCal.Duration = 0;
    //                        //}
    //                        //else
    //                        //{
    //                        //    objCoachClassCal.Duration = 0;
    //                        //}
    //                        objCoachClassCal.WeekNo = "";
    //                        objCoachClassCal.Status = "Cancelled";
    //                        objCoachClassCal.Reason = "";
    //                        objCoachClassCal.Substitute = "";
    //                        objCoachClassCal.Makeup = "";
    //                        objCoachClassCal.ClassType = "holiday";
    //                        objCoachClassCal.SubstituteCoachName = "";
    //                        objCoachClassCal.MakeupCoachName = "";

    //                        objCoachClassCal.HWDeadlineDate = "";

    //                        objCoachClassCal.HWDueDate = "";

    //                        objCoachClassCal.SRelDate = "";

    //                        objCoachClassCal.ARelDate = "";

    //                        objCoachClassCal.IsDisabled = "Yes";
    //                        objListSurvey.Add(objCoachClassCal);
    //                    }
    //                }
    //            }
    //        }


    //    }
    //    catch (Exception ex)
    //    {
    //    }
    //    return objListSurvey;

    //}


    [WebMethod]
    public static List<CoachClassCal> PostNewClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string Day = Convert.ToDateTime(objCoachClass.Date).ToString("dddd");
            string substitute = objCoachClass.Substitute == "0" ? "null" : objCoachClass.Substitute;
            string CmdSerNo = string.Empty;
            CmdSerNo = " select max(SerNo) from CoachClassCal where EventYear=" + objCoachClass.EventYear + " and Semester='" + objCoachClass.Semester + "' and EventID=" + objCoachClass.EventId + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + "";
            int SerNo = 0;
            try
            {
                SerNo = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, CmdSerNo).ToString());
            }
            catch
            {
                SerNo = 0;
            }
            SerNo = SerNo + 1;

            if (objCoachClass.ClassType == "Holiday" && objCoachClass.Status == "On")
            {
                objCoachClass.ClassType = "Regular";
            }

            string cmdText = string.Empty;
            cmdText = "insert into coachclasscal ([MemberId],[EventId],[EventYear] ,[ProductGroupId] ,[ProductGroup],[ProductId] ,[Product],[Semester],[Level],[SessionNo] ,[Date] ,[Day] ,[Time],[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],[CreateDate],[CreatedBy], Makeup, ClassType) values(" + objCoachClass.MemberId + "," + objCoachClass.EventId + "," + objCoachClass.EventYear + "," + objCoachClass.ProductGroupId + ",'" + objCoachClass.ProductGroup + "', " + objCoachClass.ProductId + ", '" + objCoachClass.Product + "', '" + objCoachClass.Semester + "', '" + objCoachClass.Level + "', " + objCoachClass.SessionNo + ", '" + objCoachClass.Date + "', '" + Day + "','" + objCoachClass.Time + "', " + objCoachClass.Duration + ", " + SerNo + ", " + objCoachClass.WeekNo + ", '" + objCoachClass.Status + "', " + substitute + ", null, GetDate(), " + objCoachClass.CreatedBy + ", " + objCoachClass.Makeup + ", '" + objCoachClass.ClassType + "')";

            SqlCommand cmd = new SqlCommand(cmdText, cn);

            cmd.ExecuteNonQuery();
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "1";
            if (objCoachClass.Substitute != "0" && objCoachClass.ClassType == "Holiday")
            {
                if (objCoachClass.ClassType == "Holiday")
                {
                    cmdText = " update WebConfLog set SubstituteCoachId=" + objCoachClass.Substitute + " where SessionType='Recurring Meeting'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + "";
                }


                cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();
                retVal = 1;



            }
            if (objCoachClass.ClassType == "Holiday")
            {
                int Retval = WeekNoResetting(objCoachClass);
            }

            if (objCoachClass.ClassType == "Makeup" && objCoachClass.CoachClassCalID != "0")
            {
                int Retval = WeekNoResettingAlone(objCoachClass);
            }

            cmdText = "select ISNULL(CoachPaperID, 0) as CoachpaperID from CoachPapers where EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and WeekId=" + objCoachClass.WeekNo + " and Level='" + objCoachClass.Level + "' and DocType='Q' and Semester='" + objCoachClass.Semester + "'";

            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    objCP.CoachpaperID = ds.Tables[0].Rows[0]["CoachpaperID"].ToString();

                    cmdText = "select ISNULL(CoachRelID,0) as CoachRelID from CoachRelDates where CoachpaperID='" + objCP.CoachpaperID + "' and EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and memberID=" + objCoachClass.MemberId + "";
                    cmd = new SqlCommand(cmdText, cn);
                    da = new SqlDataAdapter(cmd);
                    DataSet dsReldate = new DataSet();
                    da.Fill(dsReldate);
                    if (dsReldate.Tables[0].Rows.Count > 0)
                    {
                        objCP.CoachRelID = dsReldate.Tables[0].Rows[0]["CoachRelID"].ToString();
                    }
                    else
                    {
                        objCP.CoachRelID = "0";
                    }
                }
            }
            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> VerifyRegularClassStatus(CoachClassCal CoachClassCal)
    {
        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            cmdText = "  select Status from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and WeekNo=" + CoachClassCal.WeekNo + " and MemberId=" + CoachClassCal.MemberId + "";//  and Status not in ('On')";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    string Status = "Cancelled";
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        if (dr["Status"].ToString() == "On")
                        {
                            Status = "On";
                        }
                    }
                    CoachClassCal objCoachClassCal = new CoachClassCal();
                    objCoachClassCal.Status = Status;

                    objListSurvey.Add(objCoachClassCal);
                }
                else
                {
                    CoachClassCal objCoachClassCal = new CoachClassCal();
                    objCoachClassCal.Status = "";

                    objListSurvey.Add(objCoachClassCal);
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<CoachClassCal> UpdateCoachClassSchedule(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;

            if (objCoachClass.Substitute == "0" && objCoachClass.Makeup == "0" && objCoachClass.ClassType != "Extra")
            {
                cmdText = "Update CoachClassCal set Status='" + objCoachClass.Status + "', Substitute=null, ModifyDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
            }
            else
            {
                if (objCoachClass.Substitute != "0")
                {
                    cmdText = "Update CoachClassCal set Status='" + objCoachClass.Status + "', Substitute=" + objCoachClass.Substitute + ", Date='" + objCoachClass.Date + "', Time='" + objCoachClass.Time + "', ModifyDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
                }
                else if (objCoachClass.Makeup != "0")
                {
                    String Day = Convert.ToDateTime(objCoachClass.Date).ToString("dddd");
                    cmdText = "Update CoachClassCal set Status='" + objCoachClass.Status + "', Date='" + objCoachClass.Date + "', Time='" + objCoachClass.Time + "', Makeup=" + objCoachClass.Makeup + ", Day='" + Day + "', ModifyDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
                }
                else if (objCoachClass.ClassType == "Extra")
                {
                    String Day = Convert.ToDateTime(objCoachClass.Date).ToString("dddd");
                    cmdText = "Update CoachClassCal set Status='" + objCoachClass.Status + "', Date='" + objCoachClass.Date + "', Time='" + objCoachClass.Time + "',Day='" + Day + "', ModifyDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
                }
            }


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;
            CoachClassCal objCP = new CoachClassCal();

            objCP.RetVal = "1";

            if (objCoachClass.Substitute != "0")
            {
                if (objCoachClass.ClassType == "Regular" || objCoachClass.ClassType == "Holiday")
                {
                    cmdText = " update WebConfLog set SubstituteCoachId=" + objCoachClass.Substitute + ", ModifiedDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where SessionType='Recurring Meeting'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + "";
                }
                //else if (objCoachClass.ClassType == "Makeup")
                //{
                //    cmdText = " update WebConfLog set SubstituteCoachId=" + objCoachClass.Substitute + " where SessionType='Scheduled Meeting'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and StartDate='" + objCoachClass.Date + "'";
                //}
                //else if (objCoachClass.ClassType == "Extra")
                //{
                //    cmdText = " update WebConfLog set SubstituteCoachId=" + objCoachClass.Substitute + " where SessionType='Extra'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and StartDate='" + objCoachClass.Date + "'";
                //}


                cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();
                retVal = 1;
            }
            else if (objCoachClass.Substitute == "0")
            {
                if (objCoachClass.ClassType == "Regular" || objCoachClass.ClassType == "Holiday")
                {
                    cmdText = " update WebConfLog set SubstituteCoachId=null,ModifiedDate=getdate(), ModifiedBy=" + objCoachClass.CreatedBy + " where SessionType='Recurring Meeting'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + "";
                }
                //else if (objCoachClass.ClassType == "Makeup")
                //{
                //    cmdText = " update WebConfLog set SubstituteCoachId=null where SessionType='Scheduled Meeting'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and StartDate='" + objCoachClass.Date + "'";
                //}
                //else if (objCoachClass.ClassType == "Extra")
                //{
                //    cmdText = " update WebConfLog set SubstituteCoachId=null where SessionType='Extra'and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and StartDate='" + objCoachClass.Date + "'";
                //}


                cmd = new SqlCommand(cmdText, cn);
                cmd.ExecuteNonQuery();
                retVal = 1;
            }

            cmdText = "select ISNULL(CoachPaperID, 0) as CoachpaperID from CoachPapers where EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and WeekId=" + objCoachClass.WeekNo + " and Level='" + objCoachClass.Level + "' and DocType='Q' and Semester='" + objCoachClass.Semester + "'";

            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    objCP.CoachpaperID = ds.Tables[0].Rows[0]["CoachpaperID"].ToString();

                    cmdText = "select ISNULL(CoachRelID,0) as CoachRelID from CoachRelDates where CoachpaperID='" + objCP.CoachpaperID + "' and  EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and memberID=" + objCoachClass.MemberId + "";
                    cmd = new SqlCommand(cmdText, cn);
                    da = new SqlDataAdapter(cmd);
                    DataSet dsCoachRelID = new DataSet();
                    da.Fill(dsCoachRelID);
                    if (dsCoachRelID.Tables[0].Rows.Count > 0)
                    {
                        objCP.CoachRelID = dsCoachRelID.Tables[0].Rows[0]["CoachRelID"].ToString();
                    }
                    else
                    {
                        objCP.CoachRelID = "0";
                    }
                }
                else
                {
                    objCP.CoachpaperID = "0";
                }
            }
            // if (objCoachClass.WeekNoReset == "Yes" && objCoachClass.ClassType != "Extra")
            //{
            // retVal = ResettingWeeKNo(objCoachClass);
            retVal = WeekNoResetting(objCoachClass);

            objCP.Retval = retVal;
            // }
            //else
            //{
            //    retVal = -10;
            //    objCP.Retval = retVal;
            //}


            objListCP.Add(objCP);


        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();

            objCP.RetVal = "-1";
        }
        return objListCP;

    }

    [WebMethod]
    public static int ResettingWeeKNo(CoachClassCal objCoachClass)
    {
        int Retval = -1;

        string cmdText = string.Empty;
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string CoachClassId = string.Empty;

        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "'";
        if (objCoachClass.Action == "Delete")
        {
            cmdText += " and WeekNo >= " + objCoachClass.WeekNo + "";
        }
        else
        {
            cmdText += " and WeekNo = " + objCoachClass.WeekNo + "";
        }
        cmdText += " and Eventyear=" + objCoachClass.EventYear + " and (ClassType='Regular' or classtype='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }


        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo>=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + "";
        if (objCoachClass.Action == "Delete")
        {
            cmdText += " and CoachClassCalID>=" + CoachClassId + " ";
        }
        else
        {
            cmdText += " and CoachClassCalID>" + CoachClassId + " ";
        }
        cmdText += " order by WeekNo ASC";
        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeek = new SqlDataAdapter(cmd);
        DataSet dsWeek = new DataSet();
        daWeek.Fill(dsWeek);
        if (null != dsWeek)
        {
            if (dsWeek.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsWeek.Tables[0].Rows)
                {
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    //Getting coach paper of the week#

                    if (objCoachClass.WeekNoStimulation == "decrement")
                    {
                        if (dr["WeekNo"].ToString() == objCoachClass.WeekNo && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                        {

                        }
                        else
                        {
                            weekNo = weekNo - 1;
                        }
                    }
                    else if (objCoachClass.WeekNoStimulation == "increment")
                    {
                        if (dr["WeekNo"].ToString() == objCoachClass.WeekNo && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                        {
                        }
                        else
                        {
                            weekNo = weekNo + 1;
                        }
                    }
                    string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                    //  cmdUpdateText += UpdateRelDates;
                    cmd = new SqlCommand(cmdUpdateText, cn);
                    cmd.ExecuteNonQuery();

                }
                if (objCoachClass.WeekNoStimulation == "decrement")
                {
                    Retval = ResettingReleaseDatesDecrement(objCoachClass);
                }
                else if (objCoachClass.WeekNoStimulation == "increment")
                {
                    Retval = ResettingReleaseDatesIncrement(objCoachClass);
                }

            }
        }

        return Retval;


    }

    [WebMethod]
    public static int ResettingReleaseDatesDecrement(CoachClassCal objCoachClass)
    {
        int Retval = -1;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo>=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + "";

        if (objCoachClass.Action == "Delete")
        {
            cmdText += " and CoachClassCalID>=" + CoachClassId + " ";
        }
        else
        {
            cmdText += " and CoachClassCalID>" + CoachClassId + " ";
        }
        cmdText += " order by WeekNo ASC";

        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    string dtClassDate = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                    string PreviousWeekNo = string.Empty;

                    if (objCoachClass.WeekNoStimulation == "decrement")
                    {
                        //if (dr["WeekNo"].ToString() != PreviousWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == weekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                        {

                        }
                        else
                        {
                            string coachPaperId = "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "'  and level ='" + objCoachClass.Level + "' and EventId=13 and EventYear=" + objCoachClass.EventYear + " and DocType='Q'";
                            try
                            {
                                coachPaperIdBeforRst = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, coachPaperId).ToString();

                                dtHwDate = Convert.ToDateTime(dtClassDate).AddDays(0).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + coachPaperIdBeforRst + " and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and EventYear=" + objCoachClass.EventYear + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);


                            }
                            catch
                            {
                            }

                        }
                        PreviousWeekNo = dr["WeekNo"].ToString();
                        //}
                    }
                }
            }
        }
        return Retval;
    }
    [WebMethod]
    public static int ResettingReleaseDatesIncrement(CoachClassCal objCoachClass)
    {
        int Retval = -1;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo>=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + " order by WeekNo ASC;";
        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    string previouWeekNo = string.Empty;

                    if (objCoachClass.WeekNoStimulation == "increment")
                    {
                        //if (dr["WeekNo"].ToString() != previouWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == objCoachClass.WeekNo && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra" || dr["ClassType"].ToString() == "Regular"))
                        {

                            string ClassDate = string.Empty;
                            ClassDate = Convert.ToDateTime(objCoachClass.Date).ToShortDateString();

                            dtHwDate = Convert.ToDateTime(ClassDate).ToShortDateString();
                            dtHwDueDate = Convert.ToDateTime(ClassDate).AddDays(5).ToShortDateString();
                            dtARelDate = Convert.ToDateTime(ClassDate).AddDays(7).ToShortDateString();
                            dtSRelDate = Convert.ToDateTime(ClassDate).AddDays(-1).ToShortDateString();

                            string CoachPaperIdIncAft = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + weekNo + "  and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "'  and level ='" + objCoachClass.Level + "' and EventId=13 and EventYear=" + objCoachClass.EventYear + "").ToString();

                            string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdIncAft + " and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and EventYear=" + objCoachClass.EventYear + "";
                            SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                        }
                        else
                        {



                            try
                            {
                                string CoachPaperIdInc = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "'  and level ='" + objCoachClass.Level + "' and EventId=13 and EventYear=" + objCoachClass.EventYear + "").ToString();

                                string dtClassDate = string.Empty;
                                dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                                dtHwDate = Convert.ToDateTime(dtClassDate).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdInc + " and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and Session=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and EventYear=" + objCoachClass.EventYear + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                            }
                            catch
                            {
                            }

                        }
                        //}
                        previouWeekNo = dr["WeekNo"].ToString();
                    }
                }
            }
        }
        return Retval;
    }



    [WebMethod]
    public static List<CoachClassCal> GetClassStartDateAndEndDate(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            DateTime dtClassDate = new DateTime();

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            string ClassType = CoachClassCal.ClassType;

            string cmdText = string.Empty;
            cmdText = "  select distinct StartDate, EndDate, (Select distinct Day from CalSignup where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and Accepted='Y') as day,  (Select distinct Time from CalSignup where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "'  and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and Accepted='Y') as Time from CoachingDateCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and ScheduleType='Term' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            int WeekNo = Convert.ToInt32(CoachClassCal.WeekNo);
            int recordCount = 0;
            string recordText = "select count(*) from CoachClasscal where Eventyear=" + CoachClassCal.EventYear + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and WeekNo=" + WeekNo + " and (ClassType='Regular' or ClassType='Holiday' )";

            try
            {
                recordCount = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, recordText).ToString());

            }
            catch
            {
            }
            if (CoachClassCal.ClassType == "Regular")
            {
                if (recordCount > 0)
                {
                    WeekNo = Convert.ToInt32(CoachClassCal.WeekNo);
                }
                else
                {
                    WeekNo = Convert.ToInt32(CoachClassCal.WeekNo) - 1;
                }
            }
            else if (CoachClassCal.ClassType == "Substitute")
            {
                WeekNo = Convert.ToInt32(CoachClassCal.WeekNo);
            }
            else if (CoachClassCal.ClassType == "Makeup Substitute")
            {
                WeekNo = Convert.ToInt32(CoachClassCal.WeekNo);
            }
            cmdText = "select max(Date) as Date from CoachClasscal where Eventyear=" + CoachClassCal.EventYear + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and WeekNo=" + WeekNo + " and (ClassType='Regular' or ClassType='Holiday')";


            string Date = string.Empty;
            string RegularClsDate = string.Empty;
            DateTime dtRegularClassDate = new DateTime();
            try
            {
                cmd = new SqlCommand(cmdText, cn);
                da = new SqlDataAdapter(cmd);
                DataSet dsDate = new DataSet();
                da.Fill(dsDate);

                if (null != dsDate && dsDate.Tables.Count > 0)
                {
                    if (dsDate.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsDate.Tables[0].Rows)
                        {
                            var NewMeetingDay = ds.Tables[0].Rows[0]["Day"].ToString();
                            var ExistingMeetingDSay = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                            if (dr["Date"].ToString() != "" && NewMeetingDay == ExistingMeetingDSay)
                            {

                                Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                                RegularClsDate = Convert.ToDateTime(Date).ToString("MM/dd/yyyy");
                                dtRegularClassDate = Convert.ToDateTime(RegularClsDate);
                            }
                            else if (dr["Date"].ToString() != "" && NewMeetingDay != ExistingMeetingDSay)
                            {
                                Date = Convert.ToDateTime(dr["Date"].ToString()).AddDays(-7).ToString("MM/dd/yyyy");
                                for (int i = 0; i <= 6; i++)
                                {
                                    var day = Convert.ToDateTime(dr["Date"].ToString()).AddDays(i).ToString("dddd");
                                    if (day == NewMeetingDay)
                                    {
                                        var DateR = Convert.ToDateTime(dr["Date"].ToString()).AddDays(-7).ToString("MM/dd/yyyy");
                                        //if (Convert.ToDateTime(dr["Date"].ToString()) < Convert.ToDateTime(DateTime.Now))
                                        //{
                                        //    DateR = Convert.ToDateTime(dr["Date"].ToString()).AddDays(-7).ToString("MM/dd/yyyy");
                                        //}
                                        //else
                                        //{
                                        DateR = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                                        // }
                                        Date = Convert.ToDateTime(DateR).AddDays(i).ToString("MM/dd/yyyy");
                                        RegularClsDate = Convert.ToDateTime(Date).ToString("MM/dd/yyyy");
                                        dtRegularClassDate = Convert.ToDateTime(RegularClsDate);
                                    }
                                }
                            }
                        }
                    }
                }



            }
            catch
            {
            }


            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();


                        objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");

                        string classDay = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd");

                        if (RegularClsDate == "")
                        {
                            if (classDay == dr["day"].ToString())
                            {
                                dtClassDate = Convert.ToDateTime(dr["StartDate"].ToString());
                                objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                if (Convert.ToInt32(CoachClassCal.WeekNo) > 1)
                                {
                                    //objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).AddDays(((Convert.ToInt32(CoachClassCal.WeekNo) - 1) * 7)).ToString("MM/dd/yyyy");
                                    objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).AddDays(((Convert.ToInt32(CoachClassCal.WeekNo)) * 7)).ToString("MM/dd/yyyy");

                                }
                            }
                            else
                            {
                                dtClassDate = Convert.ToDateTime(dr["StartDate"].ToString());
                                DateTime dtDate = new DateTime();
                                DateTime dtCoachDate = new DateTime();
                                for (int i = 1; i <= 7; i++)
                                {

                                    dtDate = dtClassDate.AddDays(i);
                                    if (Convert.ToDateTime(dtDate.ToShortDateString()).ToString("dddd") == dr["day"].ToString())
                                    {
                                        objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtDate.ToString()).ToString("MM/dd/yyyy");
                                        dtCoachDate = Convert.ToDateTime(dtDate.ToString());
                                    }
                                }
                                if (Convert.ToInt32(CoachClassCal.WeekNo) > 1)
                                {
                                    objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtCoachDate.ToString()).AddDays((Convert.ToInt32(CoachClassCal.WeekNo) - 1) * 7).ToString("MM/dd/yyyy");
                                }
                            }
                        }
                        else
                        {
                            if (ClassType == "Regular")
                            {
                                objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).AddDays(7).ToString("MM/dd/yyyy");
                                int RetVal = IsHolidayDate(CoachClassCal, objCoachClassCal.ClassStartDate);

                                if (RetVal > 0)
                                {
                                    int SerNo = 0;
                                    try
                                    {

                                        //Insert holiday class automatically Simson
                                        string Day = Convert.ToDateTime(CoachClassCal.Date).ToString("dddd");

                                        string CmdSerNo = string.Empty;
                                        CmdSerNo = " select max(SerNo) from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and MemberId=" + CoachClassCal.MemberId + "";

                                        try
                                        {
                                            SerNo = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, CmdSerNo).ToString());
                                        }
                                        catch
                                        {
                                            SerNo = 0;
                                        }
                                        SerNo = SerNo + 1;

                                        cmdText = "insert into coachclasscal ([MemberId],[EventId],[EventYear] ,[ProductGroupId] ,[ProductGroup],[ProductId] ,[Product],[Semester],[Level],[SessionNo] ,[Date] ,[Day] ,[Time],[Duration],[SerNo],[WeekNo],[Status],[CreateDate],[CreatedBy], ClassType) values(" + CoachClassCal.MemberId + ",13," + CoachClassCal.EventYear + "," + CoachClassCal.ProductGroupId + ",'" + CoachClassCal.ProductGroup + "', " + CoachClassCal.ProductId + ", '" + CoachClassCal.Product + "', '" + CoachClassCal.Semester + "', '" + CoachClassCal.Level + "', " + CoachClassCal.SessionNo + ", '" + objCoachClassCal.ClassStartDate + "', '" + dr["day"].ToString() + "','" + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm") + "', " + CoachClassCal.Duration + ", " + SerNo + ", " + CoachClassCal.WeekNo + ", 'Cancelled', GetDate(), " + CoachClassCal.CreatedBy + ", 'Holiday')";

                                        cmd = new SqlCommand(cmdText, cn);

                                        cmd.ExecuteNonQuery();
                                    }
                                    catch
                                    {
                                    }
                                    objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).AddDays(14).ToString("MM/dd/yyyy");

                                    RetVal = IsHolidayDate(CoachClassCal, objCoachClassCal.ClassStartDate);
                                    if (RetVal > 0)
                                    {
                                        try
                                        {


                                            SerNo = SerNo + 1;

                                            cmdText = "insert into coachclasscal ([MemberId],[EventId],[EventYear] ,[ProductGroupId] ,[ProductGroup],[ProductId] ,[Product],[Semester],[Level],[SessionNo] ,[Date] ,[Day] ,[Time],[Duration],[SerNo],[WeekNo],[Status],[CreateDate],[CreatedBy], ClassType) values(" + CoachClassCal.MemberId + ",13," + CoachClassCal.EventYear + "," + CoachClassCal.ProductGroupId + ",'" + CoachClassCal.ProductGroup + "', " + CoachClassCal.ProductId + ", '" + CoachClassCal.Product + "', '" + CoachClassCal.Semester + "', '" + CoachClassCal.Level + "', " + CoachClassCal.SessionNo + ", '" + objCoachClassCal.ClassStartDate + "', '" + dr["day"].ToString() + "','" + Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm") + "', " + CoachClassCal.Duration + ", " + SerNo + ", " + CoachClassCal.WeekNo + ", 'Cancelled', GetDate(), " + CoachClassCal.CreatedBy + ", 'Holiday')";

                                            cmd = new SqlCommand(cmdText, cn);

                                            cmd.ExecuteNonQuery();
                                        }
                                        catch
                                        {

                                        }
                                        objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).AddDays(21).ToString("MM/dd/yyyy");
                                    }
                                }

                            }
                            else
                            {
                                // By Bindhu ( swapped the comment line ) to display the relevant class startdate based on the weekno typed in textbox
                                // objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).AddDays(7).ToString("MM/dd/yyyy");
                                objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).ToString("MM/dd/yyyy");
                            }
                        }

                        int HwDAteNo = 5;
                        int AnsRelDate = 8;
                        string CmdHwText = "select distinct HwDueDateNo, AnsRelDateNo from EventFees where EventYear=" + CoachClassCal.EventYear + " and ProductId="+ CoachClassCal.ProductId + "";
                        try
                        {
                            cmd = new SqlCommand(CmdHwText, cn);
                            da = new SqlDataAdapter(cmd);
                            DataSet dsNo = new DataSet();
                            da.Fill(dsNo);
                            if (null != ds)
                            {
                                if (ds.Tables[0].Rows.Count > 0)
                                {
                                    if (ds.Tables[0].Rows[0]["HwDueDateNo"] != null)
                                    {
                                        HwDAteNo = Convert.ToInt32(ds.Tables[0].Rows[0]["HwDueDateNo"].ToString());
                                        
                                    }
                                    if (ds.Tables[0].Rows[0]["AnsRelDateNo"] != null)
                                    {
                                        AnsRelDate = Convert.ToInt32(ds.Tables[0].Rows[0]["AnsRelDateNo"].ToString());
                                    }
                                }
                            }
                        }
                        catch
                        {

                        }
                        objCoachClassCal.HWDueDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(HwDAteNo).ToString("MM/dd/yyyy");

                        //Old Paper Release Dates
                        //objCoachClassCal.ARelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(7).ToString("MM/dd/yyyy");
                        //objCoachClassCal.SRelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(-1).ToString("MM/dd/yyyy");

                        objCoachClassCal.ARelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(AnsRelDate).ToString("MM/dd/yyyy");
                        objCoachClassCal.SRelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(0).ToString("MM/dd/yyyy");

                        objCoachClassCal.Day = dr["day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        objListSurvey.Add(objCoachClassCal);


                    }




                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<CoachClassCal> GetWeekNo(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            DateTime dtClassDate = new DateTime();

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = " select Isnull(Max(WeekNo),0) as WeekNo from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Makeup' or ClassType='Makeup Substitute' or ClassType='Holiday' ) and Status='On' and MemberId=" + CoachClassCal.MemberId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();


                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();

                        objListSurvey.Add(objCoachClassCal);


                    }


                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static int InsertCoachRelDate(CoachClassCal objCoachClass)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string sqlCommand = "usp_Insert_CoachRelDate";
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CoachPaperId", objCoachClass.CoachpaperID);
            param[1] = new SqlParameter("@Semester", objCoachClass.Semester);
            param[2] = new SqlParameter("@SessionNo", objCoachClass.SessionNo);
            param[3] = new SqlParameter("@QReleaseDate", objCoachClass.HWDeadlineDate);
            param[4] = new SqlParameter("@QDeadlineDate", objCoachClass.HWDueDate);
            param[5] = new SqlParameter("@AReleaseDate", objCoachClass.ARelDate);
            param[6] = new SqlParameter("@SReleaseDate", objCoachClass.SRelDate);
            param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
            param[8] = new SqlParameter("@CreatedBy", Int32.Parse(objCoachClass.LoginID));
            param[9] = new SqlParameter("@MemberId", Int32.Parse(objCoachClass.MemberId));

            SqlHelper.ExecuteDataset(cn, CommandType.StoredProcedure, sqlCommand, param);
            retVal = 1;
        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return retVal;

    }

    [WebMethod]
    public static List<ProductGroups> ListproductGroups(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select  distinct ProductGroupID, ProductGroupCode from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.ProductGroupId = dr["ProductGroupID"].ToString();
                        objProductGroup.ProductGroup = dr["ProductGroupCode"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }


    [WebMethod]
    public static List<ProductGroups> ListCoaches(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct CS.MemberID, IP.Firstname +' ' + IP.lastname as Name, IP.LastName, IP.FirstName from CalSignup CS inner join Indspouse IP on (CS.memberID=IP.AutoMemberID) where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "' and Accepted='Y'";

            if (objProdGrp.MemberID > 0)
            {
                cmdText += " and CS.memberID=" + objProdGrp.MemberID + "";
            }

            cmdText += " order by IP.FirstName, IP.LastName ASC";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                        objProductGroup.Coachname = dr["Name"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListLevel(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct Level from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + " and ProductID=" + objProdGrp.ProductId + " and ProductGroupID=" + objProdGrp.ProductGroupId + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.Level = dr["Level"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListProducts(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct productId, productCode from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.ProductId = dr["productId"].ToString();
                        objProductGroup.Product = dr["productCode"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListSessionNo(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct SessionNo from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and ProductID=" + objProdGrp.ProductId + " and Level ='" + objProdGrp.Level + "' and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.SessionNo = dr["SessionNo"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }
    [WebMethod]
    public static int UpdateCoachRelDate(CoachClassCal objCoachClass)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();


            string sqlCommand = "usp_Update_CoachRelDate";
            SqlParameter[] param = new SqlParameter[8];
            param[0] = new SqlParameter("@CoachRelID", objCoachClass.CoachRelID);
            param[1] = new SqlParameter("@QReleaseDate", objCoachClass.HWDeadlineDate);
            param[2] = new SqlParameter("@QDeadlineDate", objCoachClass.HWDueDate);
            param[3] = new SqlParameter("@AReleaseDate", objCoachClass.ARelDate);
            param[4] = new SqlParameter("@SReleaseDate", objCoachClass.SRelDate);
            param[5] = new SqlParameter("@SessionNO", objCoachClass.SessionNo);
            param[6] = new SqlParameter("@ModifiedBy", Int32.Parse(objCoachClass.LoginID));
            param[7] = new SqlParameter("@ModifyDate", System.DateTime.Now);


            SqlHelper.ExecuteDataset(cn, CommandType.StoredProcedure, sqlCommand, param);
            retVal = 1;
        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return retVal;

    }

    [WebMethod]
    public static int CheckWeekNoExists(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            if (objProdGrp.ClassType == "Makeup Substitute")
            {
                cmdText = "select count(*) as RegulatCount from coachClasscal where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and ProductID=" + objProdGrp.ProductId + " and Level ='" + objProdGrp.Level + "' and sessionNo=" + objProdGrp.SessionNo + " and WeekNo=" + objProdGrp.WeekNo + " and ClassType='Makeup'";

            }
            else
            {
                cmdText = "select count(*) as RegulatCount from coachClasscal where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and ProductID=" + objProdGrp.ProductId + " and Level ='" + objProdGrp.Level + "' and sessionNo=" + objProdGrp.SessionNo + " and WeekNo=" + objProdGrp.WeekNo + " and (ClassType='Regular' or ClassType='Holiday')";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["RegulatCount"].ToString());
                }
            }


        }
        catch (Exception ex)
        {
        }
        return retVal;

    }

    [WebMethod]
    public static int CheckDuplicateClass(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select count(*) as classCount from coachClasscal where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and ProductID=" + objProdGrp.ProductId + " and Level ='" + objProdGrp.Level + "' and sessionNo=" + objProdGrp.SessionNo + " and WeekNo=" + objProdGrp.WeekNo + " and ClassType='" + objProdGrp.ClassType + "' and Status='" + objProdGrp.Status + "'";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    retVal = Convert.ToInt32(ds.Tables[0].Rows[0]["classCount"].ToString());
                }
            }

            string classDate = Convert.ToDateTime(objProdGrp.ClassDate).ToShortDateString();

            DateTime dtDate = Convert.ToDateTime(classDate.ToString());

            DateTime dtToday = new DateTime();
            string todayDate = Convert.ToDateTime(DateTime.Now.ToString()).ToShortDateString();

            dtToday = Convert.ToDateTime(todayDate);
            if (retVal < 1)
            {
                if (dtToday < dtDate)
                {
                    retVal = -1;
                }
                else
                {
                    retVal = -2;
                }
            }
        }
        catch (Exception ex)
        {
        }
        return retVal;

    }

    [WebMethod]
    public static List<CoachClassCal> DeleteClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            string Date = "";
            string cmdWeekNo = " select Date from CoachClassCal where CoachCLassCalId=" + objCoachClass.CoachClassCalID + "";
            Date = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, cmdWeekNo).ToString();

            string cmdText = string.Empty;
            cmdText = "Delete from CoachClassCal where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();

            cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and Date >'" + Date + "' and Eventyear=" + objCoachClass.EventYear + "";
            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter daWeek = new SqlDataAdapter(cmd);
            DataSet dsWeek = new DataSet();
            daWeek.Fill(dsWeek);

            int ClassCount = 0;
            try
            {
                ClassCount = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select count(*)  from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo>" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + "").ToString());
            }
            catch
            {
            }
            string IsWeekReset = "";
            if (ClassCount > 0)
            {
                IsWeekReset = "Yes";
            }


            if (IsWeekReset == "Yes" && objCoachClass.ClassType != "Extra")
            {
                objCoachClass.WeekNoStimulation = "decrement";
                objCoachClass.Action = "Delete";
                retVal = WeekNoResetting(objCoachClass);
            }

            CoachClassCal objCP = new CoachClassCal();
            retVal = 1;



            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            retVal = -1;
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> ValidatemakeupExtraClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {
            string RegulaClassDay = string.Empty;
            string RegularTime = string.Empty;

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string Cmdtext = string.Empty;
            Cmdtext = "select distinct Day, Time, Status from CoachClasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and MemberID=" + objCoachClass.MemberId + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Holiday')";

            SqlCommand cmd = new SqlCommand(Cmdtext, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    RegulaClassDay = ds.Tables[0].Rows[0]["Day"].ToString();
                    RegularTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["Time"].ToString()).ToString("HH:mm");
                }
            }

            //validate the regular or substitute class cancelled
            string CmdCancelText = string.Empty;
            CmdCancelText = " Select Status from coachclasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and WeekNo=" + objCoachClass.WeekNo + " and MemberID=" + objCoachClass.MemberId + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Holiday')";
            string IsOnClass = string.Empty;
            DataSet dsCancel = new DataSet();
            cmd = new SqlCommand(CmdCancelText, cn);
            da = new SqlDataAdapter(cmd);
            da.Fill(dsCancel);
            if (null != dsCancel)
            {
                if (dsCancel.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsCancel.Tables[0].Rows)
                    {
                        if (dr["Status"].ToString() == "On")
                        {
                            IsOnClass = "Yes";
                        }
                    }
                }
            }

            CoachClassCal objCP = new CoachClassCal();
            string ToDay = DateTime.Now.ToShortDateString();
            DateTime dtDate = Convert.ToDateTime(ToDay);
            string ClassDate = Convert.ToDateTime(objCoachClass.Date).ToString("MM/dd/yyyy");

            DateTime dtCoachDate = Convert.ToDateTime(ClassDate);
            if (IsOnClass == "")
            {
                if (dtCoachDate > dtDate)
                {
                    objCP.RetVal = "1";

                    string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                    if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                    {
                        objCP.RetVal = "-2";
                    }
                }
                else if (dtCoachDate == dtDate)
                {
                    ToDay = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
                    dtDate = Convert.ToDateTime(ToDay);
                    ClassDate = objCoachClass.Date + " " + objCoachClass.Time;
                    ClassDate = Convert.ToDateTime(ClassDate).ToString("MM/dd/yyyy HH:mm");
                    dtCoachDate = Convert.ToDateTime(ClassDate);
                    if (dtCoachDate > dtDate)
                    {
                        objCP.RetVal = "1";
                    }
                    else
                    {
                        objCP.RetVal = "-1";
                    }

                    string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                    if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                    {
                        objCP.RetVal = "-2";
                    }
                }
                else
                {
                    objCP.RetVal = "-1";
                }

                if (objCoachClass.ClassType == "Makeup" && objCoachClass.Mode == "1")
                {
                    Cmdtext = "select Status from CoachClasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and MemberID=" + objCoachClass.MemberId + " and WeekNo=" + (Convert.ToInt32(objCoachClass.WeekNo)) + " and ClassType='Makeup'";

                    string Makeup = string.Empty;

                    try
                    {
                        Makeup = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, Cmdtext).ToString();

                    }
                    catch
                    {
                    }

                    if (Makeup != "")
                    {
                        if (Makeup != "Cancelled")
                        {
                            objCP.RetVal = "-3";
                        }
                    }
                    //else if (Makeup == "")
                    //{
                    //    objCP.RetVal = "";
                    //}
                }
            }
            else
            {
                objCP.RetVal = "-5";
            }
            objListCP.Add(objCP);
        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
            objListCP.Add(objCP);
        }
        return objListCP;

    }

    [WebMethod]
    public static List<ProductGroups> ListMakeupSubstituteCoaches(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct CS.MemberID, IP.Firstname +' ' + IP.lastname as Name, IP.FirstName, IP.LastName from CalSignup CS inner join Indspouse IP on (CS.memberID=IP.AutoMemberID) where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "' and Accepted='Y' and CS.MemberID<>" + objProdGrp.MemberID + " order by IP.FirstName, IP.LastName ASC;";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                        objProductGroup.Coachname = dr["Name"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<CoachClassCal> ValidateExtraClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {
            string RegulaClassDay = string.Empty;
            string RegularTime = string.Empty;

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string Cmdtext = string.Empty;



            Cmdtext = "select distinct Day, Time from CoachClasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and MemberID=" + objCoachClass.MemberId + " and (ClassType='Regular' or ClassType='Makeup' or ClassType='Holiday')";

            SqlCommand cmd = new SqlCommand(Cmdtext, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RegulaClassDay = ds.Tables[0].Rows[0]["Day"].ToString();
                    RegularTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["Time"].ToString()).ToString("HH:mm");
                }
            }

            DataSet dsCount = new DataSet();
            Cmdtext = "select count(*) as CountSet from CoachClassCal where EventYear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo=" + objCoachClass.SessionNo + " and MemberID=" + objCoachClass.MemberId + " and WeekNo=" + objCoachClass.WeekNo + " and (ClassType='Regular' or ClassType='Makup' or ClassType='Substitute' or ClassType='Holiday')";
            cmd = new SqlCommand(Cmdtext, cn);
            da = new SqlDataAdapter(cmd);

            da.Fill(dsCount);
            int isRegularMakeupSub = 0;
            if (null != dsCount)
            {
                if (dsCount.Tables[0].Rows.Count > 0)
                {
                    isRegularMakeupSub = Convert.ToInt32(dsCount.Tables[0].Rows[0]["CountSet"].ToString());

                }
            }

            DataSet dsDuplicate = new DataSet();
            Cmdtext = "select count(*) as CountSet from CoachClassCal where EventYear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductID=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo=" + objCoachClass.SessionNo + " and MemberID=" + objCoachClass.MemberId + " and WeekNo=" + objCoachClass.WeekNo + " and ClassType='Extra' and Date='" + objCoachClass.Date + "' and Time ='" + objCoachClass.Time + "'";
            cmd = new SqlCommand(Cmdtext, cn);
            da = new SqlDataAdapter(cmd);

            da.Fill(dsDuplicate);
            int isDuplicate = 0;
            if (null != dsDuplicate)
            {
                if (dsDuplicate.Tables[0].Rows.Count > 0)
                {
                    isDuplicate = Convert.ToInt32(dsDuplicate.Tables[0].Rows[0]["CountSet"].ToString());

                }
            }

            CoachClassCal objCP = new CoachClassCal();
            string ToDay = DateTime.Now.ToShortDateString();
            DateTime dtDate = Convert.ToDateTime(ToDay);
            string ClassDate = Convert.ToDateTime(objCoachClass.Date).ToString("MM/dd/yyyy");

            DateTime dtCoachDate = Convert.ToDateTime(ClassDate);
            if (isRegularMakeupSub == 0)
            {
                objCP.RetVal = "-4";
            }
            else if (isDuplicate > 0)
            {
                objCP.RetVal = "-5";
            }
            else if (dtCoachDate > dtDate)
            {
                objCP.RetVal = "1";

                string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                {
                    objCP.RetVal = "-2";
                }
            }
            else if (dtCoachDate == dtDate)
            {
                ToDay = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
                dtDate = Convert.ToDateTime(ToDay);
                ClassDate = objCoachClass.Date + " " + objCoachClass.Time;
                ClassDate = Convert.ToDateTime(ClassDate).ToString("MM/dd/yyyy HH:mm");
                dtCoachDate = Convert.ToDateTime(ClassDate);
                if (dtCoachDate > dtDate)
                {
                    objCP.RetVal = "1";
                }
                else
                {
                    objCP.RetVal = "-1";
                }

                string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                {
                    objCP.RetVal = "-2";
                }
            }
            else
            {
                objCP.RetVal = "-1";
            }



            objListCP.Add(objCP);
        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
            objListCP.Add(objCP);
        }
        return objListCP;

    }
    public static string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public static string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    [WebMethod]
    public static List<ZoomSessions> makeZoomSessions(ZoomSessions objZoomSess)
    {
        List<ZoomSessions> ObjListZoom = new List<ZoomSessions>();
        try
        {

            string URL = string.Empty;
            int durationHrs = objZoomSess.Duration;
            string service = "1";

            URL = "https://api.zoom.us/v1/meeting/create";

            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = objZoomSess.StartDate;
            string strMonth = date.Substring(0, date.IndexOf('/'));
            string strDay = date.Substring(date.IndexOf('/') + 1, 2);
            string year = date.Substring(date.LastIndexOf('/') + 1, 4);
            date = year + "-" + strMonth + "-" + strDay;

            string time = objZoomSess.StartTime;
            string dateTime = date + "T" + time + "Z";

            string MakeupDateTime = date + " " + time;
            DateTime dtMakeupDate = Convert.ToDateTime(MakeupDateTime).AddHours(4);

            string StrMakeupDate = dtMakeupDate.ToString("yyyy-MM-dd");
            string StrMakeupTime = dtMakeupDate.ToString("HH:mm:ss");

            string StrMakeupDateTime = StrMakeupDate + "T" + StrMakeupTime + "Z";

            string dateTime__1 = (Convert.ToString(date + Convert.ToString("T")) + time) + "Z";
            string topic = objZoomSess.Topic;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            //if (DateTime.TryParse(time, dtFromS))
            //{
            //    TimeSpan TS = dtFromS - dtEnds;
            //    mins = TS.TotalMinutes;
            //    dtTo = dtFromS.AddHours(4);
            //}
            string timeFormat = dtTo.Hour.ToString() + ":" + dtTo.Minute.ToString() + ":" + dtTo.Second.ToString();

            dateTime__1 = (Convert.ToString(date + Convert.ToString("T")) + timeFormat) + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + objZoomSess.HostID + "";

            urlParameter += "&topic=" + topic + "";
            // urlParameter += "&password=training";
            urlParameter += "&type=2";
            urlParameter += "&start_time=" + StrMakeupDateTime + "";
            urlParameter += "&duration=" + durationHrs + "";
            urlParameter += "&timezone=GMT-5:00";
            urlParameter += "&option_jbh=true";
            urlParameter += "&option_host_video=true";
            urlParameter += "&option_audio=both";

            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameter.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameter);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();

            }
            var parser = new JavaScriptSerializer();

            dynamic obj = parser.Deserialize<dynamic>(postResponse);



            JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);
            ZoomSessions objZoom = new ZoomSessions();

            var serializer = new JavaScriptSerializer();
            objZoom.SessionKey = json["id"].ToString();
            objZoom.HostURL = json["start_url"].ToString();
            objZoom.JoinURl = json["join_url"].ToString();
            objZoom.Retval = 1;
            ObjListZoom.Add(objZoom);
        }
        catch
        {
            ZoomSessions objZoom = new ZoomSessions();
            objZoom.Retval = -1;
        }
        return ObjListZoom;
    }


    [WebMethod]
    public static List<CoachClassCal> SaveMakeupSessions(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string time = objCoachClass.StartDate + " " + objCoachClass.StartTime;

            string EndTime = Convert.ToDateTime(time).AddMinutes(objCoachClass.Duration).ToShortTimeString();

            string Day = Convert.ToDateTime(objCoachClass.StartDate).ToString("dddd");
            string substitute = objCoachClass.Substitute == "0" ? "null" : objCoachClass.Substitute;

            string cmdText = string.Empty;
            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SessionType, Status)values(" + objCoachClass.EventYear + "," + objCoachClass.EventId + ",13," + objCoachClass.ProductGroupId + ", '" + objCoachClass.ProductGroup + "', " + objCoachClass.ProductId + ", '" + objCoachClass.Product + "', " + objCoachClass.MemberId + ", '" + objCoachClass.SessionKey + "','" + objCoachClass.StartDate + "','" + objCoachClass.EndDate + "', '" + objCoachClass.StartTime + "', '" + EndTime + "', '" + objCoachClass.Semester + "', '" + objCoachClass.Level + "', " + objCoachClass.SessionNo + ", 'Training'," + objCoachClass.Duration + ", 11, 'EST/EDT (Eastern or New York Time)', GetDate(), 4240, '" + objCoachClass.HostURL + "', '" + Day + "','" + objCoachClass.UserID + "', '" + objCoachClass.Pwd + "', 'Scheduled Meeting', 'Active')";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "1";



            cmdText = "Update CalSignup set MakeUpURL='" + objCoachClass.HostURL + "', MakeupMeetKey='" + objCoachClass.SessionKey + "', MakeUpPwd='training',MakeUpDate='" + objCoachClass.Date + "',MakeUpTime='" + objCoachClass.StartTime + "',MakeUpDuration='" + objCoachClass.Duration + "' where EventYear=" + objCoachClass.EventYear + " and Semester='" + objCoachClass.Semester + "' and EventID=" + objCoachClass.EventId + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and Accepted='Y'";

            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            objCP.RetVal = "1";

            string ChidName = string.Empty;
            string Email = string.Empty;
            string City = string.Empty;
            string Country = string.Empty;
            string ChildNumber = string.Empty;
            string CoachRegID = string.Empty;

            DataSet dsChild = new DataSet();

            string ChildText = "select case when CR.Childnumber is null then CR.AdultId else C1.ChildNumber end as ChildNumber,CR.CoachRegID,CR.PMemberID, case when CR.Childnumber is null then IP1.gender else C1.Gender end as Gender, case when CR.Childnumber is null then IP1.FirstName + ' '+ IP1.LastName else  C1.FIRST_NAME +' '+ C1.LAST_NAME end as Name, case when CR.Childnumber is null then IP1.Email else case when C1.Email IS NULL then IP.Email else C1.Email end end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail, case when CR.Childnumber is null then 'Adult' else 'Child' end as Type from CoachReg CR  inner join IndSpouse IP on(IP.AutoMemberID=CR.PMemberID) left join child C1 on(CR.ChildNumber=C1.ChildNumber) left join Indspouse IP1 on (IP1.AutoMemberid=CR.AdultId) where CR.Level='" + objCoachClass.Level + "' and CR.Semester='" + objCoachClass.Semester + "' and CR.ProductGroupCode='" + objCoachClass.ProductGroup + "' and CR.ProductCode='" + objCoachClass.Product + "' and CR.CMemberID=" + objCoachClass.MemberId + " and CR.EventYear=" + objCoachClass.EventYear + " and CR.EventID=" + objCoachClass.EventId + " and CR.approved='Y' and CR.SessionNo=" + objCoachClass.SessionNo + "";

            cmd = new SqlCommand(ChildText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(dsChild);


            if (null != dsChild && dsChild.Tables.Count > 0)
            {
                if (dsChild.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drChild in dsChild.Tables[0].Rows)
                    {
                        ChidName = drChild["Name"].ToString();
                        Email = drChild["WebExEmail"].ToString();
                        if (Email.IndexOf(';') > 0)
                        {
                            Email = Email.Substring(0, Email.IndexOf(';'));
                        }
                        City = drChild["City"].ToString();
                        Country = drChild["Country"].ToString();
                        ChildNumber = drChild["ChildNumber"].ToString();
                        CoachRegID = drChild["CoachRegID"].ToString();
                        //registerMeetingsAttendees(ChidName, Email, City, Country);
                        // if (hdnMeetingStatus.Value == "SUCCESS")
                        //{

                        string CmdChildUpdateText = "update CoachReg set MakeUpURL='" + objCoachClass.JoinURl + "',ModifyDate=GetDate(), ModifiedBy=4240,MakeUpDate='" + objCoachClass.StartDate + "',MakeUpTime='" + objCoachClass.StartTime + "',MakeUpDuration='" + objCoachClass.Duration + "' where CoachRegID=" + CoachRegID + "";

                        cmd = new SqlCommand(cmdText, cn);
                        cmd.ExecuteNonQuery();
                        objCP.RetVal = "1";
                    }

                }
            }

            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
        }
        return objListCP;

    }

    [WebMethod]
    public static List<ZoomSessions> DeleteZoomSessions(ZoomSessions objZoomSess)
    {
        List<ZoomSessions> ObjListZoom = new List<ZoomSessions>();
        try
        {

            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + objZoomSess.HostID + "";
            urlParameter += "&id=" + objZoomSess.SessionKey + "";


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameter.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";


            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameter);
            myWriter.Close();


            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }





            //var parser = new JavaScriptSerializer();

            //dynamic obj = parser.Deserialize<dynamic>(postResponse);



            //JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);
            ZoomSessions objZoom = new ZoomSessions();

            //var serializer = new JavaScriptSerializer();
            //objZoom.SessionKey = json["id"].ToString();
            //objZoom.HostURL = json["start_url"].ToString();
            //objZoom.JoinURl = json["join_url"].ToString();
            objZoom.Retval = 1;

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            string DeleteZoomClass = " Delete from WebCOnflog where sessionKey=" + objZoomSess.SessionKey + "";
            SqlCommand cmd = new SqlCommand(DeleteZoomClass, cn);
            cmd = new SqlCommand(DeleteZoomClass, cn);
            cmd.ExecuteNonQuery();

            ObjListZoom.Add(objZoom);
        }
        catch
        {
            ZoomSessions objZoom = new ZoomSessions();
            objZoom.Retval = -1;
        }
        return ObjListZoom;
    }

    [WebMethod]
    public static List<CoachClassCal> ValidateVroomValidation(CoachClassCal objCoachClass)
    {
        List<CoachClassCal> ObjList = new List<CoachClassCal>();
        SqlCommand cmd;
        SqlDataAdapter da;
        DataSet dsDuration = new DataSet();
        string cmdDurText = "";
        int Duration = 120;


        string Day = Convert.ToDateTime(objCoachClass.Date).ToString("dddd");
        string strStartTime = objCoachClass.StrStartTime;
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        DateTime DtStartTime = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + objCoachClass.StrStartTime);
        strStartTime = Convert.ToDateTime(DtStartTime).AddMinutes(-30).ToString("HH:mm:ss");
        DateTime dtStarttime = new DateTime();
        DateTime dtEndTime = new DateTime();


        DateTime dtPrStartTime = new DateTime();
        DateTime dtPrEndTime = new DateTime();

        cn.Open();
        cmdDurText = "select distinct duration from WebConfLog where EventYear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductID=" + objCoachClass.ProductId + " and SessionType='Recurring Meeting'";


        cmd = new SqlCommand(cmdDurText, cn);
        da = new SqlDataAdapter(cmd);
        dsDuration = new DataSet();
        da.Fill(dsDuration);


        if (null != dsDuration && dsDuration.Tables.Count > 0)
        {
            if (dsDuration.Tables[0].Rows.Count > 0)
            {
                if (dsDuration.Tables[0].Rows[0]["duration"].ToString() != "")
                {
                    Duration = Convert.ToInt32(dsDuration.Tables[0].Rows[0]["duration"]);
                }
            }
        }

        DateTime DtEndTime = Convert.ToDateTime(DateTime.Now.ToShortDateString() + " " + objCoachClass.StrStartTime).AddMinutes(Duration);
        string startTime = Convert.ToDateTime(DtStartTime.AddMinutes(-30)).ToShortTimeString();
        string endTime = Convert.ToDateTime(objCoachClass.StrStartTime).ToString();


        dtPrStartTime = Convert.ToDateTime(startTime);
        dtPrEndTime = Convert.ToDateTime(endTime).AddMinutes(Duration);

        string StrSHr = strStartTime.Substring(0, 2);
        int iShr = Convert.ToInt32(StrSHr);
        string StrEhr = Convert.ToDateTime(endTime).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
        int iEhr = Convert.ToInt32(StrEhr);
        if ((iShr > 7 & iEhr < 8))
        {
            dtPrEndTime = Convert.ToDateTime("23:59:00");
        }
        else
        {
            dtPrEndTime = Convert.ToDateTime(endTime).AddMinutes(Duration);
        }


        try
        {
            string CmdText = string.Empty;
            string cmdText = string.Empty;
            string Vrooms = string.Empty;
            //   cmdText = "  select VR.UserID,VC.BeginTime, VC.EndTime, VR.Vroom from WebCOnfLOg VC inner join VirtualRoomLookUp VR on (VC.userID=VR.UserID)  where Eventyear=" + objCoachClass.EventYear + " and ('" + startTime + "' between BeginTime and EndTime) and ('" + endTime + "' between  BeginTime and EndTime ) order by Vroom";

            cmdText = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day,cs.Productgroupcode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" + objCoachClass.EventYear + " and cs.Accepted='Y' order by cs.Time";

            cmd = new SqlCommand(cmdText, cn);
            da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            Double iDurationBasedOnPG = 0;
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        dtStarttime = Convert.ToDateTime(dr["Begin"].ToString());
                        if (dr["ProductGroupCode"].ToString() == "COMP")
                        {
                            iDurationBasedOnPG = 2;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "GB")
                        {
                            iDurationBasedOnPG = 2;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "LSP")
                        {
                            iDurationBasedOnPG = 1;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "MB")
                        {
                            iDurationBasedOnPG = 2;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "SAT")
                        {
                            iDurationBasedOnPG = 1.5;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "SC")
                        {
                            iDurationBasedOnPG = 2;
                        }
                        else if (dr["ProductGroupCode"].ToString() == "UV")
                        {
                            iDurationBasedOnPG = 1;
                        }
                        string StrSHr1 = Convert.ToDateTime(dr["Begin"].ToString()).ToString("HH:mm:ss").Substring(0, 2);
                        int iShr1 = Convert.ToInt32(StrSHr1);
                        string StrEhr1 = Convert.ToDateTime(dr["Begin"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                        int iEhr1 = Convert.ToInt32(StrEhr1);
                        if ((iShr1 > 7 & iEhr1 < 8))
                        {
                            dtEndTime = Convert.ToDateTime("23:59:00");
                        }
                        else
                        {
                            dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                        }
                        string Meetingday = dr["Day"].ToString();
                        string meetVroom = dr["Vroom"].ToString();
                        if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && Day == dr["Day"].ToString())
                        {

                            Vrooms += dr["VRoom"].ToString() + ",";
                        }
                    }
                }
            }


            cmdText = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" + objCoachClass.EventYear + " and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by vc.BeginTime";
            try
            {
                cmd = new SqlCommand(cmdText, cn);
                da = new SqlDataAdapter(cmd);
                DataSet dsSingle = new DataSet();
                da.Fill(dsSingle);


                if (null != dsSingle && dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            dtStarttime = Convert.ToDateTime(dr["BeginTime"].ToString());
                            if (dr["ProductGroupCode"].ToString() == "COMP")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "GB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "LSP")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "MB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SAT")
                            {
                                iDurationBasedOnPG = 1.5;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SC")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "UV")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            string StrSHr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).ToString("HH:mm:ss").Substring(0, 2);
                            int iShr1 = Convert.ToInt32(StrSHr1);
                            string StrEhr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                            int iEhr1 = Convert.ToInt32(StrEhr1);
                            if ((iShr1 > 7 & iEhr1 < 8))
                            {
                                dtEndTime = Convert.ToDateTime("23:59:00");
                            }
                            else
                            {
                                dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);
                            }
                            //dtEndTime = Convert.ToDateTime(dr["End"].ToString());
                            string Meetingday = dr["Day"].ToString();
                            string meetVroom = dr["Vroom"].ToString();
                            if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && Day == dr["Day"].ToString())
                            {

                                Vrooms += dr["VRoom"].ToString() + ",";
                            }
                        }
                    }
                }
            }
            catch
            {
            }

            if (!string.IsNullOrEmpty(Vrooms))
            {
                Vrooms = Vrooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(Vrooms))
            {
                cmdText = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom not in (" + Vrooms + ") order by vroom desc;";
            }
            else
            {
                cmdText = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp order by vroom desc;";
            }

            cmd = new SqlCommand(cmdText, cn);
            da = new SqlDataAdapter(cmd);
            DataSet dsRoom = new DataSet();
            da.Fill(dsRoom);

            if (dsRoom.Tables.Count > 0)
            {
                if (dsRoom.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsRoom.Tables[0].Rows)
                    {
                        CoachClassCal obCoachClass = new CoachClassCal();
                        objCoachClass.HostID = dr["HostID"].ToString();
                        objCoachClass.Duration = Duration;
                        objCoachClass.UserID = dr["UserId"].ToString();
                        objCoachClass.Pwd = dr["PWD"].ToString();

                        ObjList.Add(objCoachClass);
                    }
                }
            }
        }
        catch
        {
        }
        return ObjList;
    }
    public class Vroom
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string HostId { get; set; }
        public string Day { get; set; }
    }

    [WebMethod]
    public static List<CoachClassCal> GetyearAndSemester()
    {
        List<CoachClassCal> objCoachClass = new List<CoachClassCal>();
        string cmdDurText = "";
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();

        cmdDurText = "select max(EventYear) as EventYear from EventFees where EventId=13; select max( Semester) as Semester from EventFees where Eventyear=(select max(EventYear) from EventFees where EventId=13) and EventId=13";


        SqlCommand cmd = new SqlCommand(cmdDurText, cn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);


        if (null != ds && ds.Tables.Count > 0)
        {
            CoachClassCal ObjCoach = new CoachClassCal();
            ObjCoach.EventYear = ds.Tables[0].Rows[0]["EventYear"].ToString();
            ObjCoach.Semester = ds.Tables[1].Rows[0]["Semester"].ToString();
            objCoachClass.Add(ObjCoach);
        }
        return objCoachClass;

    }

    [WebMethod]
    public static List<CoachClassCal> SaveExtraSessions(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string time = objCoachClass.StartDate + " " + objCoachClass.StartTime;

            string EndTime = Convert.ToDateTime(time).AddMinutes(objCoachClass.Duration).ToShortTimeString();

            string Day = Convert.ToDateTime(objCoachClass.Date).ToString("dddd");

            string cmdText = string.Empty;
            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SessionType, Status)values(" + objCoachClass.EventYear + "," + objCoachClass.EventId + ",112," + objCoachClass.ProductGroupId + ", '" + objCoachClass.ProductGroup + "', " + objCoachClass.ProductId + ", '" + objCoachClass.Product + "', " + objCoachClass.MemberId + ", '" + objCoachClass.SessionKey + "','" + objCoachClass.StartDate + "','" + objCoachClass.EndDate + "', '" + objCoachClass.StartTime + "', '" + EndTime + "', '" + objCoachClass.Semester + "', '" + objCoachClass.Level + "', " + objCoachClass.SessionNo + ", 'Training'," + objCoachClass.Duration + ", 11, 'EST/EDT (Eastern or New York Time)', GetDate(), 4240, '" + objCoachClass.HostURL + "', '" + Day + "','" + objCoachClass.UserID + "', '" + objCoachClass.Pwd + "', 'Extra', 'Active')";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "1";



            cmdText = "Update CalSignup set ExtraSessionKey=" + objCoachClass.SessionKey + ", ExtraSessionURL='" + objCoachClass.HostURL + "' where EventYear=" + objCoachClass.EventYear + " and Semester='" + objCoachClass.Semester + "' and EventID=" + objCoachClass.EventId + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo=" + objCoachClass.SessionNo + " and MemberId=" + objCoachClass.MemberId + " and Accepted='Y'";

            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            objCP.RetVal = "1";

            string ChidName = string.Empty;
            string Email = string.Empty;
            string City = string.Empty;
            string Country = string.Empty;
            string ChildNumber = string.Empty;
            string CoachRegID = string.Empty;

            DataSet dsChild = new DataSet();

            string ChildText = "select case when CR.Childnumber is null then CR.AdultId else C1.ChildNumber end as Childnumber,CR.CoachRegID,CR.PMemberID, case when CR.Childnumber is null then IP1.Gender else C1.Gender end as Gender, case when CR.Childnumber is null then IP1.FirstName + ' ' + IP1.LastName else C1.FIRST_NAME +' '+ C1.LAST_NAME end as Name, case when CR.Childnumber is null then IP1.EMail else case when C1.Email IS NULL then IP.Email else C1.Email end end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from CoachReg CR  inner join IndSpouse IP on(IP.AutoMemberID=CR.PMemberID) left join child C1 on(CR.ChildNumber=C1.ChildNumber) left join Indspouse IP1 on (IP1.AutoMemberId=CR.AdultId) where CR.Level='" + objCoachClass.Level + "' and CR.Semester='" + objCoachClass.Semester + "' and CR.ProductGroupCode='" + objCoachClass.ProductGroup + "' and CR.ProductCode='" + objCoachClass.Product + "' and CR.CMemberID=" + objCoachClass.MemberId + " and CR.EventYear=" + objCoachClass.EventYear + " and CR.EventID=" + objCoachClass.EventId + " and CR.approved='Y' and CR.SessionNo=" + objCoachClass.SessionNo + "";

            cmd = new SqlCommand(ChildText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(dsChild);


            if (null != dsChild && dsChild.Tables.Count > 0)
            {
                if (dsChild.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow drChild in dsChild.Tables[0].Rows)
                    {
                        ChidName = drChild["Name"].ToString();
                        Email = drChild["WebExEmail"].ToString();
                        if (Email.IndexOf(';') > 0)
                        {
                            Email = Email.Substring(0, Email.IndexOf(';'));
                        }
                        City = drChild["City"].ToString();
                        Country = drChild["Country"].ToString();
                        ChildNumber = drChild["ChildNumber"].ToString();
                        CoachRegID = drChild["CoachRegID"].ToString();
                        //registerMeetingsAttendees(ChidName, Email, City, Country);
                        // if (hdnMeetingStatus.Value == "SUCCESS")
                        //{

                        string CmdChildUpdateText = "update CoachReg set ExtraSessionURL='" + objCoachClass.JoinURl + "',ModifyDate=GetDate(), ModifiedBy=4240 where CoachRegID=" + CoachRegID + "";

                        cmd = new SqlCommand(CmdChildUpdateText, cn);
                        cmd.ExecuteNonQuery();
                        objCP.RetVal = "1";
                    }

                }
            }

            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> GetHostIdAndSessionKey(string CoachId, string PgId, string PId, string Semester, string Session, string ClassDate, string EventYear, string Level, string ClassType)
    {
        List<CoachClassCal> objCoachClass = new List<CoachClassCal>();
        string cmdDurText = "";
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();

        cmdDurText = "select Vl.UserID, Vl.Pwd, VL.HostId, WC.Sessionkey from WebConfLog WC inner join VirtualRoomLookup VL on (WC.UserID=VL.userID) where EventId=13 and WC.memberId=" + CoachId + " and ProductGroupid=" + PgId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + Session + " and WC.StartDate='" + ClassDate + "' and Level='" + Level + "' and SessionType='" + ClassType + "'";


        SqlCommand cmd = new SqlCommand(cmdDurText, cn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);

        try
        {
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CoachClassCal ObjCoach = new CoachClassCal();
                    ObjCoach.HostID = ds.Tables[0].Rows[0]["HostId"].ToString();
                    ObjCoach.SessionKey = ds.Tables[0].Rows[0]["sessionKey"].ToString();
                    objCoachClass.Add(ObjCoach);
                }
            }
        }
        catch
        {
        }
        return objCoachClass;

    }

    [WebMethod]
    public static int GetWeekNoConfirmation(CoachClassCal objCoachClass)
    {

        int RetVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = " select count(*) as Countset from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo>" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + " and (Status='On' )";

            SqlCommand cmd = new SqlCommand(cmdText, cn);

            CoachClassCal objCP = new CoachClassCal();

            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {


                    RetVal = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                }
            }

            cmdText = " select count(*) as Countset from CoachclassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + "";
            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter daWeek = new SqlDataAdapter(cmd);
            DataSet dsWeek = new DataSet();
            daWeek.Fill(dsWeek);
            if (null != dsWeek)
            {
                if (dsWeek.Tables[0].Rows.Count > 0)
                {
                    int count = Convert.ToInt32(dsWeek.Tables[0].Rows[0]["Countset"].ToString());
                    if (count > 1)
                    {
                        RetVal = Convert.ToInt32(dsWeek.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
            }

            if (objCoachClass.Status == "On")
            {
                cmdText = " select status, ClassType from CoachClassCal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "' and WeekNo=" + objCoachClass.WeekNo + " and Eventyear=" + objCoachClass.EventYear + " and Status='On' and (ClassType ='Substitute' or ClassType='Makeup' or classtype='Makeup Substitute') and Status='On' ";
                cmd = new SqlCommand(cmdText, cn);
                SqlDataAdapter daOn = new SqlDataAdapter(cmd);
                DataSet dsOn = new DataSet();
                daOn.Fill(dsOn);
                if (null != dsOn)
                {
                    if (dsOn.Tables[0].Rows.Count > 0)
                    {
                        int count = 0;
                        foreach (DataRow dr in dsOn.Tables[0].Rows)
                        {
                            if (dr["Status"].ToString() == "On" && (dr["ClassType"].ToString() == "Regular" || dr["ClassType"].ToString() == "Holiday"))
                            {
                                count = 1;
                            }
                        }

                        if (count > 0)
                        {
                            RetVal = -2;
                        }
                    }
                }
            }

        }
        catch (Exception ex)
        {

            RetVal = -1;
        }
        return RetVal;

    }

    [WebMethod]
    public static List<CoachClassCal> ValidateMakeupSubstitute(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        CoachClassCal objCP = new CoachClassCal();
        try
        {
            string Cmdtext = string.Empty;


            if (objCoachClass.ClassType == "Makeup Substitute")
            {
                Cmdtext = "select Status from CoachClasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and MemberID=" + objCoachClass.MemberId + " and WeekNo=" + (Convert.ToInt32(objCoachClass.WeekNo)) + " and ClassType='Makeup'";

                string Makeup = string.Empty;

                try
                {
                    Makeup = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, Cmdtext).ToString();

                }
                catch
                {
                }

                if (Makeup != "")
                {
                    if (Makeup != "Cancelled")
                    {
                        objCP.RetVal = "-2";
                    }
                    else
                    {
                        objCP.RetVal = "1";
                    }
                }
                //else if (Makeup == "")
                //{
                //    objCP.RetVal = "";
                //}
            }
            objListCP.Add(objCP);
        }



        catch (Exception ex)
        {

            objCP.RetVal = "-1";
            objListCP.Add(objCP);
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> GetWeekNoForHolidayCLass(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            DateTime dtClassDate = new DateTime();

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            List<CoachingDateCal> ObjDateCal = new List<CoachingDateCal>();
            ObjDateCal = GetClassStartDateBasedOnCoach(CoachClassCal);
            string PriorDateToHoliday = Convert.ToDateTime(CoachClassCal.Date).AddDays(-7).ToShortDateString();
            int PriorCLassCount = 0;
            string cmdClassCount = " Select count(*) from CoachClassCal where  EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Makeup' or ClassType='Makeup Substitute' or ClassType='Holiday') and MemberId=" + CoachClassCal.MemberId + " and Date='" + PriorDateToHoliday + "'";
            try
            {
                PriorCLassCount = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, cmdClassCount).ToString());
            }
            catch
            {
            }
            string ClassSTartDate = ObjDateCal[0].StartDate.ToString();
            string HolidayDate = Convert.ToDateTime(CoachClassCal.Date).ToShortDateString();
            if (PriorCLassCount > 0 || ClassSTartDate == HolidayDate)
            {

                string cmdText = string.Empty;
                cmdText = " select Isnull(Max(WeekNo),0) as WeekNo  from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Makeup' or ClassType='Makeup Substitute' or ClassType='Holiday') and Status='On' and MemberId=" + CoachClassCal.MemberId + " and Date<'" + HolidayDate + "' ";

                SqlCommand cmd = new SqlCommand(cmdText, cn);
                SqlDataAdapter da = new SqlDataAdapter(cmd);
                DataSet ds = new DataSet();
                da.Fill(ds);
                if (null != ds)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            CoachClassCal objCoachClassCal = new CoachClassCal();


                            objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
                            objCoachClassCal.Retval = 1;
                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(CoachClassCal.Date).AddDays(0).ToShortDateString();
                            objCoachClassCal.HWDueDate = Convert.ToDateTime(CoachClassCal.Date).AddDays(5).ToShortDateString();
                            objCoachClassCal.ARelDate = Convert.ToDateTime(CoachClassCal.Date).AddDays(7).ToShortDateString();
                            objCoachClassCal.SRelDate = Convert.ToDateTime(CoachClassCal.Date).AddDays(-1).ToShortDateString();

                            objListSurvey.Add(objCoachClassCal);


                        }


                    }
                }
            }
            else
            {
                CoachClassCal objCoachClassCal = new CoachClassCal();
                objCoachClassCal.Retval = -2;
                objListSurvey.Add(objCoachClassCal);
            }

        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<CoachingDateCal> GetClassStartDateBasedOnCoach(CoachClassCal CoachClsCal)
    {
        string CmdHoliday = string.Empty;
        List<CoachingDateCal> objListholiday = new List<CoachingDateCal>();
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        CmdHoliday = " select * from coachingDateCal where Eventyear=" + CoachClsCal.EventYear + " and ScheduleType<>'Term' and ProductGroupId=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        SqlCommand cmd = new SqlCommand(CmdHoliday, cn);
        SqlDataAdapter daHoliday = new SqlDataAdapter(cmd);
        DataSet dsHoliday = new DataSet();
        daHoliday.Fill(dsHoliday);

        string cmdClassText = string.Empty;
        cmdClassText = "  select distinct StartDate, EndDate, (Select distinct Day from CalSignup where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + " and Level='" + CoachClsCal.Level + "' and SessionNo='" + CoachClsCal.SessionNo + "' and MemberID=" + CoachClsCal.MemberId + " and Accepted='Y') as day, (Select distinct Time from CalSignup where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + " and Level='" + CoachClsCal.Level + "'  and SessionNo='" + CoachClsCal.SessionNo + "' and MemberID=" + CoachClsCal.MemberId + " and Accepted='Y') as Time from CoachingDateCal where EventYear=" + CoachClsCal.EventYear + " and Semester='" + CoachClsCal.Semester + "' and ScheduleType='Term' and EventID=" + CoachClsCal.EventId + " and ProductGroupID=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        cmd = new SqlCommand(cmdClassText, cn);
        SqlDataAdapter da = new SqlDataAdapter(cmd);
        DataSet ds = new DataSet();
        da.Fill(ds);
        DateTime dtClassStartDate = new DateTime();
        string ClassStartDate = string.Empty;
        string classDay = string.Empty;
        string ClassEndDate = string.Empty;
        DateTime dtClassEndDate = new DateTime();
        String ClassTimne = string.Empty;
        if (null != ds)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ClassTimne = dr["Time"].ToString();
                    classDay = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd");
                    if (classDay == dr["day"].ToString())
                    {
                        dtClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                        ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");

                        ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                    }
                    else
                    {

                        DateTime dtDate = new DateTime();
                        DateTime dtCoachDate = new DateTime();
                        ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        dtCoachDate = Convert.ToDateTime(dr["StartDate"].ToString());
                        for (int i = 1; i <= 7; i++)
                        {


                            dtDate = dtCoachDate.AddDays(i);
                            if (Convert.ToDateTime(dtDate.ToShortDateString()).ToString("dddd") == dr["day"].ToString())
                            {
                                ClassStartDate = Convert.ToDateTime(dtDate.ToString()).ToString("MM/dd/yyyy");
                                dtClassStartDate = Convert.ToDateTime(dtDate.ToString());
                            }
                        }


                    }
                }
                CoachingDateCal ObjDateCal = new CoachingDateCal();
                ObjDateCal.StartDate = Convert.ToDateTime(ClassStartDate).ToShortDateString();
                objListholiday.Add(ObjDateCal);
            }
        }
        return objListholiday;
    }

    [WebMethod]
    public static List<CoachingDateCal> VerifyHolidayClass(CoachClassCal CoachClsCal)
    {
        string CmdHoliday = string.Empty;
        List<CoachingDateCal> objListholiday = new List<CoachingDateCal>();
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        CmdHoliday = " select * from coachingDateCal where Eventyear=" + CoachClsCal.EventYear + " and ScheduleType<>'Term' and ProductGroupId=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        SqlCommand cmd = new SqlCommand(CmdHoliday, cn);
        SqlDataAdapter daHoliday = new SqlDataAdapter(cmd);
        DataSet dsHoliday = new DataSet();
        daHoliday.Fill(dsHoliday);


        DateTime dtClassStartDate = new DateTime();
        string ClassStartDate = string.Empty;
        string classDay = string.Empty;
        string ClassEndDate = string.Empty;
        DateTime dtClassEndDate = new DateTime();
        DateTime dtClassDate = new DateTime();
        String ClassTimne = string.Empty;
        string RetVal = "-2";
        if (null != dsHoliday)
        {
            if (dsHoliday.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsHoliday.Tables[0].Rows)
                {
                    dtClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                    dtClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString());
                    dtClassDate = Convert.ToDateTime(CoachClsCal.Date);
                    if (dtClassDate >= dtClassStartDate && dtClassDate <= dtClassEndDate)
                    {
                        RetVal = "1";

                    }
                    else
                    {

                    }
                }
            }
            else
            {

                RetVal = "-1";
            }
            CoachingDateCal ObjDateCal = new CoachingDateCal();
            ObjDateCal.RetVal = RetVal;
            objListholiday.Add(ObjDateCal);
        }
        return objListholiday;
    }
    [WebMethod]
    public static List<CoachClassCal> GetReleaseDatesForMakeup(String Date)
    {
        List<CoachClassCal> ObjListCal = new List<CoachClassCal>();
        CoachClassCal objCoachClassCal = new CoachClassCal();
        objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(Date).ToString("MM/dd/yyyy");
        objCoachClassCal.HWDueDate = Convert.ToDateTime(Date).AddDays(5).ToString("MM/dd/yyyy");
        objCoachClassCal.ARelDate = Convert.ToDateTime(Date).AddDays(7).ToString("MM/dd/yyyy");
        objCoachClassCal.SRelDate = Convert.ToDateTime(Date).AddDays(-1).ToString("MM/dd/yyyy");
        ObjListCal.Add(objCoachClassCal);
        return ObjListCal;

    }


    [WebMethod]
    public static int WeekNoResetting(CoachClassCal objCoachClass)
    {
        int Retval = -1;

        string cmdText = string.Empty;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string CoachClassId = string.Empty;

        cmdText = " select * from coachclasscal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "'";

        cmdText += " and Eventyear=" + objCoachClass.EventYear + " order by WeekNo, Date ASC";

        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        string Status = "On";
        string PrevStatus = "On";
        int PrevWeekNo = 0;
        string Classtype = "";
        int weekNo = PrevWeekNo;
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsUniqueDate.Tables[0].Rows)
                {
                    CoachClassId = dr["CoachClassCalId"].ToString();


                    Classtype = dr["ClassType"].ToString();
                    if (PrevStatus == "On" && Classtype != "Extra")
                    {
                        weekNo = weekNo + 1;
                    }

                    string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                    //  cmdUpdateText += UpdateRelDates;
                    cmd = new SqlCommand(cmdUpdateText, cn);
                    cmd.ExecuteNonQuery();

                    PrevWeekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    PrevStatus = dr["Status"].ToString();
                    Retval = 1;
                }

                if (objCoachClass.WeekNoStimulation == "decrement")
                {
                    Retval = ResettingReleaseDatesDecrement(objCoachClass);
                }
                else if (objCoachClass.WeekNoStimulation == "increment")
                {
                    Retval = ResettingReleaseDatesIncrement(objCoachClass);
                }
            }
        }

        return Retval;
    }


    [WebMethod]
    public static int IsHolidayDate(CoachClassCal CoachClsCal, string Date)
    {
        string CmdHoliday = string.Empty;
        List<CoachingDateCal> objListholiday = new List<CoachingDateCal>();
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        CmdHoliday = " select * from coachingDateCal where Eventyear=" + CoachClsCal.EventYear + " and ScheduleType<>'Term' and ProductGroupId=" + CoachClsCal.ProductGroupId + " and ProductId=" + CoachClsCal.ProductId + "";

        SqlCommand cmd = new SqlCommand(CmdHoliday, cn);
        SqlDataAdapter daHoliday = new SqlDataAdapter(cmd);
        DataSet dsHoliday = new DataSet();
        daHoliday.Fill(dsHoliday);


        DateTime dtClassStartDate = new DateTime();
        string ClassStartDate = string.Empty;
        string classDay = string.Empty;
        string ClassEndDate = string.Empty;
        DateTime dtClassEndDate = new DateTime();
        DateTime dtClassDate = new DateTime();
        String ClassTimne = string.Empty;
        int RetVal = -2;
        if (null != dsHoliday)
        {
            if (dsHoliday.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsHoliday.Tables[0].Rows)
                {
                    dtClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString());
                    dtClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString());
                    dtClassDate = Convert.ToDateTime(Date);
                    if (dtClassDate >= dtClassStartDate && dtClassDate <= dtClassEndDate)
                    {
                        RetVal = 1;

                    }
                    else
                    {

                    }
                }
            }
            else
            {

                RetVal = -1;
            }

        }
        return RetVal;
    }

    [WebMethod]
    public static int DeletemakeupFromNSF(string SessionKey, string CoachClassCalID)
    {
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        int RetVal = 1;
        string CmdText = " delete from CoachClassCal where CoachCLassCalId=" + CoachClassCalID + "";
        SqlCommand cmd = new SqlCommand(CmdText, cn);
        cmd.ExecuteNonQuery();
        CmdText = " delete from webconflog where sessionkey=" + SessionKey + "";
        cmd = new SqlCommand(CmdText, cn);
        cmd.ExecuteNonQuery();

        return RetVal;

    }

    [WebMethod]
    public static int WeekNoResettingAlone(CoachClassCal objCoachClass)
    {
        int Retval = -1;

        string cmdText = string.Empty;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string CoachClassId = string.Empty;

        cmdText = " select * from coachclasscal where EventId=13 and memberId=" + objCoachClass.MemberId + " and ProductGroupid=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Semester='" + objCoachClass.Semester + "' and SessionNo=" + objCoachClass.SessionNo + " and level ='" + objCoachClass.Level + "'";

        cmdText += " and Eventyear=" + objCoachClass.EventYear + " order by WeekNo, Date ASC";

        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        string Status = "On";
        string PrevStatus = "On";
        int PrevWeekNo = 0;
        string Classtype = "";
        int weekNo = PrevWeekNo;
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in dsUniqueDate.Tables[0].Rows)
                {
                    CoachClassId = dr["CoachClassCalId"].ToString();


                    Classtype = dr["ClassType"].ToString();
                    if (PrevStatus == "On" && Classtype != "Extra")
                    {
                        weekNo = weekNo + 1;
                    }

                    string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                    //  cmdUpdateText += UpdateRelDates;
                    cmd = new SqlCommand(cmdUpdateText, cn);
                    cmd.ExecuteNonQuery();

                    PrevWeekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    PrevStatus = dr["Status"].ToString();
                    Retval = 1;
                }


            }
        }

        return Retval;
    }

    [WebMethod]
    public static int UpdateHolidayClass(List<CoachClassCal> ListCoachClassCal, List<CoachingDateCal> ListHoliday)
    {

        int Retval = -1;
        try
        {


            if (ListHoliday.Count > 0 && ListCoachClassCal.Count > 0)
            {
                for (int i = 0; i < ListHoliday.Count; i++)
                {
                    Boolean IsExists = false;
                    for (int j = 0; j < ListCoachClassCal.Count; j++)
                    {
                        var holidayDate = Convert.ToDateTime(ListHoliday[i].StartDate).ToString("MM/dd/yyyy");
                        var classDate = Convert.ToDateTime(ListCoachClassCal[j].Date).ToString("MM/dd/yyyy");
                        if (holidayDate.Trim() == classDate.Trim())
                        {
                            IsExists = true;
                        }
                    }
                    //if (IsExists == false)
                    //{
                    //    for (int j = 0; j < ListCoachClassCal.Count; j++)
                    //    {
                    //        var holidayDate = Convert.ToDateTime(ListHoliday[i].StartDate).ToString("MM/dd/yyyy");
                    //        var classDate = Convert.ToDateTime(ListCoachClassCal[j].Date).ToString("MM/dd/yyyy");
                    //        if (Convert.ToDateTime(holidayDate).AddDays(7).ToString("MM/dd/yyyy") == classDate.Trim())
                    //        {
                    //            IsExists = true;
                    //        }
                    //    }
                    //}
                    if (IsExists == false)
                    {
                        var holidayDate = Convert.ToDateTime(ListHoliday[i].StartDate).AddDays(-7).ToString("MM/dd/yyyy");
                        string CmdText = " select * from coachClasscal CL where  CL.EventYear=" + ListCoachClassCal[i].EventYear + " and CL.Semester='" + ListCoachClassCal[i].Semester + "' and CL.EventID=" + ListCoachClassCal[i].EventId + " and CL.ProductGroupID=" + ListCoachClassCal[i].ProductGroupId + " and CL.ProductId=" + ListCoachClassCal[i].ProductId + " and CL.Level='" + ListCoachClassCal[i].Level + "' and CL.SessionNo=" + ListCoachClassCal[i].SessionNo + " and CL.MemberId=" + ListCoachClassCal[i].MemberId + " and CL.Date='" + holidayDate + "'";
                        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
                        cn.Open();
                        SqlCommand cmd = new SqlCommand(CmdText, cn);
                        SqlDataAdapter daWeekNo = new SqlDataAdapter(cmd);
                        DataSet dsWeekNo = new DataSet();

                        daWeekNo.Fill(dsWeekNo);
                        if (null != dsWeekNo)
                        {
                            if (dsWeekNo.Tables[0].Rows.Count > 0)
                            {

                                string CmdNextText = " select count( *) from coachClasscal CL where  CL.EventYear=" + ListCoachClassCal[i].EventYear + " and CL.Semester='" + ListCoachClassCal[i].Semester + "' and CL.EventID=" + ListCoachClassCal[i].EventId + " and CL.ProductGroupID=" + ListCoachClassCal[i].ProductGroupId + " and CL.ProductId=" + ListCoachClassCal[i].ProductId + " and CL.Level='" + ListCoachClassCal[i].Level + "' and CL.SessionNo=" + ListCoachClassCal[i].SessionNo + " and CL.MemberId=" + ListCoachClassCal[i].MemberId + " and (CL.Date='" + Convert.ToDateTime(holidayDate).AddDays(14).ToString("MM/dd/yyyy") + "' or CL.Date='" + Convert.ToDateTime(holidayDate).AddDays(21).ToString("MM/dd/yyyy") + "')";
                                int IsNextClass = 0;
                                try
                                {
                                    IsNextClass = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, CmdNextText).ToString());
                                }
                                catch
                                {
                                    IsNextClass = 0;
                                }
                                if (IsNextClass > 0)
                                {
                                    var WeekNo = dsWeekNo.Tables[0].Rows[0]["WeekNo"].ToString();
                                    var status = dsWeekNo.Tables[0].Rows[0]["Status"].ToString();
                                    if (status.ToLower() == "on")
                                    {
                                        WeekNo = (Convert.ToInt32(WeekNo) + 1).ToString();
                                    }

                                    string CmdInsertion = string.Empty;
                                    int SerNo = 0;
                                    string CmdSerNo = " select max(SerNo) from CoachClassCal where EventYear=" + ListCoachClassCal[i].EventYear + " and Semester='" + ListCoachClassCal[i].Semester + "' and EventID=" + ListCoachClassCal[i].EventId + " and ProductGroupID=" + ListCoachClassCal[i].ProductGroupId + " and ProductId=" + ListCoachClassCal[i].ProductId + " and Level='" + ListCoachClassCal[i].Level + "' and SessionNo=" + ListCoachClassCal[i].SessionNo + " and MemberId=" + ListCoachClassCal[i].MemberId + "";

                                    try
                                    {
                                        SerNo = Convert.ToInt32(SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, CmdSerNo).ToString());
                                    }
                                    catch
                                    {
                                        SerNo = 0;
                                    }
                                    SerNo = SerNo + 1;

                                    CmdInsertion = "insert into coachclasscal ([MemberId],[EventId],[EventYear] ,[ProductGroupId] ,[ProductGroup],[ProductId] ,[Product],[Semester],[Level],[SessionNo] ,[Date] ,[Day] ,[Time],[Duration],[SerNo],[WeekNo],[Status],[CreateDate],[CreatedBy], ClassType) values(" + ListCoachClassCal[i].MemberId + ",13," + ListCoachClassCal[i].EventYear + "," + ListCoachClassCal[i].ProductGroupId + ",'" + ListCoachClassCal[i].ProductGroup + "', " + ListCoachClassCal[i].ProductId + ", '" + ListCoachClassCal[i].Product + "', '" + ListCoachClassCal[i].Semester + "', '" + ListCoachClassCal[i].Level + "', " + ListCoachClassCal[i].SessionNo + ", '" + Convert.ToDateTime(ListHoliday[i].StartDate).ToString("MM/dd/yyyy") + "', '" + ListCoachClassCal[i].Day + "','" + Convert.ToDateTime(ListCoachClassCal[i].Time).ToString("HH:mm") + "', " + ListCoachClassCal[i].Duration + ", " + SerNo + ", " + WeekNo + ", 'Cancelled', GetDate(), 4240, 'Holiday')";

                                    cmd = new SqlCommand(CmdInsertion, cn);

                                    cmd.ExecuteNonQuery();
                                    IsRefresh = true;
                                }
                            }
                        }


                    }
                }
            }
        }
        catch
        {

        }
        return Retval;
    }

    [WebMethod]
    public static List<CoachClassCal> GetCurrentyearAndSemester()
    {
        List<CoachClassCal> ObjListCal = new List<CoachClassCal>();
        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select MAX(eventyear) as Eventyear, Max(Semester) as Semester from EventFees where EventId=13; ";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CoachClassCal ObjCoachClass = new CoachClassCal();
                    ObjCoachClass.EventYear = ds.Tables[0].Rows[0]["Eventyear"].ToString();
                    ObjCoachClass.Semester = ds.Tables[0].Rows[0]["Semester"].ToString();
                    ObjListCal.Add(ObjCoachClass);
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListCal;

    }

    [WebMethod]
    public static string GetSupportMail(string Name)
    {
        return "System.Configuration.ConfigurationManager.AppSettings[Name].ToString()";
    }
    [WebMethod]
    public static List<CoachClassCal> GetCoachEventYearAndSemester(string MemberId)
    {
        List<CoachClassCal> ObjListCal = new List<CoachClassCal>();
        try
        {


            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select MAX(eventyear) as Eventyear, Max(Semester) as Semester from CalSignup where EventId=13 and Memberid=" + MemberId + " ";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    CoachClassCal ObjCoachClass = new CoachClassCal();
                    ObjCoachClass.EventYear = ds.Tables[0].Rows[0]["Eventyear"].ToString();
                    ObjCoachClass.Semester = ds.Tables[0].Rows[0]["Semester"].ToString();
                    ObjListCal.Add(ObjCoachClass);
                }
            }


        }
        catch (Exception ex)
        {
        }
        return ObjListCal;

    }
}