using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class ChapVolunteerRoles : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

    }
    protected void lnkBtn_Click(object sender, EventArgs e)
    {
        if (Session["LoginRole"].ToString() == "ChapterC")
            Response.Redirect("ChapterCoordinator/ChapterFunctions.aspx");
        else if (Session["LoginRole"].ToString() == "NationalC")
            Response.Redirect("VolunteerFunctions.aspx");
    }
}

 