﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class GradeCard
    Inherits System.Web.UI.Page
    Dim dblMaxQ As Integer
    Dim dblCorrect As Decimal
    Dim dblPercent As Decimal
    Dim dblGrade As Decimal
    Dim MaxPap As String
    Dim ChildCount As String
    Dim TopPaperCount As String
    Dim StudType As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            Try
                If Request.QueryString(0) > 0 And Request.QueryString(1) > 0 Then
                    StudType = Request.QueryString("StudType")
                    LoadGrid_GradeCard(Request.QueryString(0), Request.QueryString(1))
                Else
                    lblmsg.Text = "Coach Papers are not valid."
                    tblDGCoachPaper.Visible = False
                End If
            Catch ex As Exception
                lblmsg.Text = ex.ToString()
            End Try
        End If
    End Sub

    Public Sub LoadGrid_GradeCard(ByVal ChildNumber As Integer, ByVal EventYear As Integer)
        Try
            Dim StrGradeCard As String = ""
            Dim StrExt As String = ""
            Dim TotCount, MaxCount As Integer
            Dim ds As DataSet

            If (StudType = "Child") Then



                MaxCount = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(*) from CoachPapers CP where EventYear=" & Request.QueryString(1) & "  and ProductGroupId=" & Request.QueryString(2) & " and ProductID=" & Request.QueryString(3) & " and Level='" & Request.QueryString(4) & "' and DocType='Q' and exists (select CoachPaperID from ChildTestSummary where EventYear=" & Request.QueryString(1) & "  and CP.CoachPaperId=CoachPaperId)")
                TotCount = Math.Round(0.7 * MaxCount)


                StrGradeCard = StrGradeCard & " Select top " & TotCount & " 'Paper' + cast(ROW_NUMBER() OVER(ORDER BY  EventYear) as nvarchar(10)) as RowNo,*,Round((Correct/MaxQ) * 100,2) as [Percent],"
                StrGradeCard = StrGradeCard & " Case When Round((Correct/MaxQ) * 100,2) >= 90 then 'A' Else Case When Round((Correct/MaxQ) * 100,2) Between 80 and 90 then 'B' Else Case When Round((Correct/MaxQ) * 100,2) Between 70 and 80 then 'C' Else Case When Round((Correct/MaxQ) * 100,2) Between 60 and 70 then 'D' Else Case When Round((Correct/MaxQ) * 100,2) <60 then 'E' End End End End End as Grade,"
                'StrGradeCard = StrGradeCard & " Case When Round((Correct/MaxQ) * 100,2) >= 90 then 1 Else Case When Round((Correct/MaxQ) * 100,2) Between 80 and 90 then 2 Else Case When Round((Correct/MaxQ) * 100,2) Between 70 and 80 then 3 Else Case When Round((Correct/MaxQ) * 100,2) Between 60 and 70 then 4 Else Case When Round((Correct/MaxQ) * 100,2) <60 then 5 End End End End End as GradeInt,"
                StrGradeCard = StrGradeCard & " 1 as RowVal   from "
                StrGradeCard = StrGradeCard & " (Select Distinct C.First_Name +' ' + C.Last_Name as ChildName,CR.Childnumber,CR.EventYear,(Select top 1 EventCode from Event where EventID=CR.EventID) as EventCode,(Select Top 1 Name from ProductGroup where ProductGroupId =CR.ProductGroupID and EventID=CR.EventID) as ProductGroup,(Select Top 1 Name from Product where ProductGroupID=CR.ProductGroupID and ProductID=CR.ProductID) as Product,CR.ProductGroupCode,CR.ProductCode,Cr.Phase,CR.SessionNo,CR.Level,Cs.StartDate,CS.Day,CONVERT(varchar(15),CAST(CS.Time AS TIME),100) as Time ,CP.WeekId,CP.CoachPaperId,"
                StrGradeCard = StrGradeCard & " (Select Count(*) from TestAnswerKey where CoachPaperID = CP.CoachPaperId) as MaxQ,"
                StrGradeCard = StrGradeCard & " (Select TotalRawScore from ChildTestSummary where CoachPaperID = CP.CoachPaperId and ChildNumber=" & ChildNumber & " ) as Correct,'' as Max_Count,'' as Count,'' as Top_Count "
                StrGradeCard = StrGradeCard & " from Child C "
                StrGradeCard = StrGradeCard & " Inner Join CoachReg CR on CR.ChildNumber =C.ChildNumber and CR.PMemberID =C.MEMBERID "
                StrGradeCard = StrGradeCard & " Inner Join CalSignUp CS on CS.EventYear =CR.EventYear and CR.CMemberID =CS.MemberID and CR.ProductGroupID=CS.ProductGroupID and CR.ProductID =CS.ProductID and CR.SessionNo =CS.SessionNo and CR.Level =CS.Level "
                StrGradeCard = StrGradeCard & " Inner Join CoachPapers CP on CP.EventYear =CR.EventYear and CP.ProductGroupID=CS.ProductGroupID and CP.ProductID =CS.ProductID and CP.Level =CR.Level "
                StrGradeCard = StrGradeCard & " Inner Join ChildTestAnswers CTA on CTA.EventYear =CR.EventYear and CP.CoachPaperId =CTA.CoachPaperID  and C.ChildNumber =CTA.ChildNumber "
                StrGradeCard = StrGradeCard & " Inner Join ChildTestSummary CTS on  CTS.EventYear =CR.EventYear and CP.CoachPaperId =CTS.CoachPaperID  and C.ChildNumber =CTS.ChildNumber"
                StrGradeCard = StrGradeCard & " Where CR.Approved='Y' and CS.Accepted='Y' and CTS.Completed='Y' and CR.ChildNumber = " & ChildNumber & " and CR.EventYear = " & EventYear & " and CR.ProductGroupId=" & Request.QueryString(2) & " and CR.ProductID=" & Request.QueryString(3) & " and CR.Level='" & Request.QueryString(4) & "' "

                'StrExt = " ) T"
                'ds = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, StrGradeCard & StrExt)


                'If ds.Tables(0).Rows.Count < TotCount Then ''           top " & (TotCount - ds.Tables(0).Rows.Count) + 1 & "
                StrGradeCard = StrGradeCard & " Union All "

                'StrGradeCard = StrGradeCard & " Select 'Paper' + cast(ROW_NUMBER() OVER(ORDER BY Childnumber) as nvarchar(10)) as RowNo,*,'' as [Percent], '' as Grade, 1 as RowVal from( "
                StrGradeCard = StrGradeCard & "Select Distinct  C.First_Name +' ' + C.Last_Name as ChildName,CR.Childnumber,CR.EventYear,(Select top 1 EventCode from Event where EventID=CR.EventID) as EventCode,"
                StrGradeCard = StrGradeCard & "(Select Top 1 Name from ProductGroup where ProductGroupId =CR.ProductGroupID and EventID=CR.EventID) as ProductGroup,"
                StrGradeCard = StrGradeCard & "(Select Top 1 Name from Product where ProductGroupID=CR.ProductGroupID and ProductID=CR.ProductID) as Product,CR.ProductGroupCode,CR.ProductCode,Cr.Phase,CR.SessionNo,CR.Level,Cs.StartDate,"
                StrGradeCard = StrGradeCard & " CS.Day,CONVERT(varchar(15),CAST(CS.Time AS TIME),100) as Time ,CP.WeekId,CP.CoachPaperId, (Select Count(*) from TestAnswerKey where CoachPaperID = CP.CoachPaperId) as MaxQ,"
                StrGradeCard = StrGradeCard & "  (Select TotalRawScore from ChildTestSummary where CoachPaperID = CP.CoachPaperId and ChildNumber=" & ChildNumber & " ) as Correct,'' as Max_Count,'' as Count,'' as Top_Count from Child C"
                StrGradeCard = StrGradeCard & " Inner Join CoachReg CR on CR.ChildNumber =C.ChildNumber and CR.PMemberID =C.MEMBERID Inner Join CalSignUp CS on CS.EventYear =CR.EventYear and CR.CMemberID =CS.MemberID"
                StrGradeCard = StrGradeCard & " and CR.ProductGroupID=CS.ProductGroupID and CR.ProductID =CS.ProductID and CR.SessionNo =CS.SessionNo and CR.Level =CS.Level Inner Join CoachPapers CP "
                StrGradeCard = StrGradeCard & " on CP.EventYear =CR.EventYear and CP.ProductGroupID=CS.ProductGroupID and CP.ProductID =CS.ProductID and CP.Level =CR.Level Inner Join "
                StrGradeCard = StrGradeCard & " ChildTestAnswers CTA on CTA.EventYear = CR.EventYear and CP.CoachPaperId =CTA.CoachPaperID and C.ChildNumber =CTA.ChildNumber Inner Join"
                StrGradeCard = StrGradeCard & " ChildTestSummary CTS on CTS.EventYear = CR.EventYear and CP.CoachPaperId =CTS.CoachPaperID and C.ChildNumber =CTS.ChildNumber "
                StrGradeCard = StrGradeCard & " Where CR.Approved='Y' and CS.Accepted='Y' and CTS.Completed='Y' and CR.ChildNumber not in (" & ChildNumber & ") and CR.EventYear = " & EventYear & " and CR.ProductGroupId=" & Request.QueryString(2) & " and CR.ProductID=" & Request.QueryString(3) & " and CR.Level='" & Request.QueryString(4) & "'" ') T1 "
                'Else
                StrGradeCard = StrGradeCard & ") T"
                'End If
                StrGradeCard = StrGradeCard & " Union all Select '','' as ChildName,Null,Null,Null,'','','','',Null,Null,'',Null,Null,Null,Null,Null,0,0,'','','',0,'' as Grade,0 as RowVal "
                StrGradeCard = StrGradeCard & " Order by RowVal desc,WeekID,RowNo " ',Grade desc "
                'Response.Write(StrGradeCard & "<br /><br /><br />")
            ElseIf (StudType = "Adult") Then
                MaxCount = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(*) from CoachPapers CP where EventYear=" & Request.QueryString(1) & "  and ProductGroupId=" & Request.QueryString(2) & " and ProductID=" & Request.QueryString(3) & " and Level='" & Request.QueryString(4) & "' and DocType='Q' and exists (select CoachPaperID from ChildTestSummary where EventYear=" & Request.QueryString(1) & "  and CP.CoachPaperId=CoachPaperId)")
                TotCount = Math.Round(0.7 * MaxCount)


                StrGradeCard = StrGradeCard & " Select top " & TotCount & " 'Paper' + cast(ROW_NUMBER() OVER(ORDER BY  EventYear) as nvarchar(10)) as RowNo,*,Round((Correct/MaxQ) * 100,2) as [Percent],"
                StrGradeCard = StrGradeCard & " Case When Round((Correct/MaxQ) * 100,2) >= 90 then 'A' Else Case When Round((Correct/MaxQ) * 100,2) Between 80 and 90 then 'B' Else Case When Round((Correct/MaxQ) * 100,2) Between 70 and 80 then 'C' Else Case When Round((Correct/MaxQ) * 100,2) Between 60 and 70 then 'D' Else Case When Round((Correct/MaxQ) * 100,2) <60 then 'E' End End End End End as Grade,"
                'StrGradeCard = StrGradeCard & " Case When Round((Correct/MaxQ) * 100,2) >= 90 then 1 Else Case When Round((Correct/MaxQ) * 100,2) Between 80 and 90 then 2 Else Case When Round((Correct/MaxQ) * 100,2) Between 70 and 80 then 3 Else Case When Round((Correct/MaxQ) * 100,2) Between 60 and 70 then 4 Else Case When Round((Correct/MaxQ) * 100,2) <60 then 5 End End End End End as GradeInt,"
                StrGradeCard = StrGradeCard & " 1 as RowVal   from "
                StrGradeCard = StrGradeCard & " (Select Distinct IP.FirstName +' ' + IP.LastName as ChildName,CR.AdultId as ChildNumber,CR.EventYear,(Select top 1 EventCode from Event where EventID=CR.EventID) as EventCode,(Select Top 1 Name from ProductGroup where ProductGroupId =CR.ProductGroupID and EventID=CR.EventID) as ProductGroup,(Select Top 1 Name from Product where ProductGroupID=CR.ProductGroupID and ProductID=CR.ProductID) as Product,CR.ProductGroupCode,CR.ProductCode,Cr.Phase,CR.SessionNo,CR.Level,Cs.StartDate,CS.Day,CONVERT(varchar(15),CAST(CS.Time AS TIME),100) as Time ,CP.WeekId,CP.CoachPaperId,"
                StrGradeCard = StrGradeCard & " (Select Count(*) from TestAnswerKey where CoachPaperID = CP.CoachPaperId) as MaxQ,"
                StrGradeCard = StrGradeCard & " (Select TotalRawScore from ChildTestSummary where CoachPaperID = CP.CoachPaperId and AdultId=" & ChildNumber & " ) as Correct,'' as Max_Count,'' as Count,'' as Top_Count "
                StrGradeCard = StrGradeCard & " from Indspouse IP "
                StrGradeCard = StrGradeCard & " Inner Join CoachReg CR on CR.AdultId =IP.AutoMemberId  "
                StrGradeCard = StrGradeCard & " Inner Join CalSignUp CS on CS.EventYear =CR.EventYear and CR.CMemberID =CS.MemberID and CR.ProductGroupID=CS.ProductGroupID and CR.ProductID =CS.ProductID and CR.SessionNo =CS.SessionNo and CR.Level =CS.Level "
                StrGradeCard = StrGradeCard & " Inner Join CoachPapers CP on CP.EventYear =CR.EventYear and CP.ProductGroupID=CS.ProductGroupID and CP.ProductID =CS.ProductID and CP.Level =CR.Level "
                StrGradeCard = StrGradeCard & " Inner Join ChildTestAnswers CTA on CTA.EventYear =CR.EventYear and CP.CoachPaperId =CTA.CoachPaperID  and CR.AdultId =CTA.AdultId "
                StrGradeCard = StrGradeCard & " Inner Join ChildTestSummary CTS on  CTS.EventYear =CR.EventYear and CP.CoachPaperId =CTS.CoachPaperID  and CR.AdultId =CTS.AdultId"
                StrGradeCard = StrGradeCard & " Where CR.Approved='Y' and CS.Accepted='Y' and CTS.Completed='Y' and CR.AdultId = " & ChildNumber & " and CR.EventYear = " & EventYear & " and CR.ProductGroupId=" & Request.QueryString(2) & " and CR.ProductID=" & Request.QueryString(3) & " and CR.Level='" & Request.QueryString(4) & "' "

                'StrExt = " ) T"
                'ds = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, StrGradeCard & StrExt)


                'If ds.Tables(0).Rows.Count < TotCount Then ''           top " & (TotCount - ds.Tables(0).Rows.Count) + 1 & "
                StrGradeCard = StrGradeCard & " Union All "

                'StrGradeCard = StrGradeCard & " Select 'Paper' + cast(ROW_NUMBER() OVER(ORDER BY Childnumber) as nvarchar(10)) as RowNo,*,'' as [Percent], '' as Grade, 1 as RowVal from( "
                StrGradeCard = StrGradeCard & "Select Distinct  IP.FirstName +' ' + IP.LastName as ChildName,CR.AdultId as ChildNUmber,CR.EventYear,(Select top 1 EventCode from Event where EventID=CR.EventID) as EventCode,"
                StrGradeCard = StrGradeCard & "(Select Top 1 Name from ProductGroup where ProductGroupId =CR.ProductGroupID and EventID=CR.EventID) as ProductGroup,"
                StrGradeCard = StrGradeCard & "(Select Top 1 Name from Product where ProductGroupID=CR.ProductGroupID and ProductID=CR.ProductID) as Product,CR.ProductGroupCode,CR.ProductCode,Cr.Phase,CR.SessionNo,CR.Level,Cs.StartDate,"
                StrGradeCard = StrGradeCard & " CS.Day,CONVERT(varchar(15),CAST(CS.Time AS TIME),100) as Time ,CP.WeekId,CP.CoachPaperId, (Select Count(*) from TestAnswerKey where CoachPaperID = CP.CoachPaperId) as MaxQ,"
                StrGradeCard = StrGradeCard & "  (Select TotalRawScore from ChildTestSummary where CoachPaperID = CP.CoachPaperId and ChildNumber=" & ChildNumber & " ) as Correct,'' as Max_Count,'' as Count,'' as Top_Count from Indspouse IP"
                StrGradeCard = StrGradeCard & " Inner Join CoachReg CR on CR.AdultId =IP.AutoMemberId  Inner Join CalSignUp CS on CS.EventYear =CR.EventYear and CR.CMemberID =CS.MemberID"
                StrGradeCard = StrGradeCard & " and CR.ProductGroupID=CS.ProductGroupID and CR.ProductID =CS.ProductID and CR.SessionNo =CS.SessionNo and CR.Level =CS.Level Inner Join CoachPapers CP "
                StrGradeCard = StrGradeCard & " on CP.EventYear =CR.EventYear and CP.ProductGroupID=CS.ProductGroupID and CP.ProductID =CS.ProductID and CP.Level =CR.Level Inner Join "
                StrGradeCard = StrGradeCard & " ChildTestAnswers CTA on CTA.EventYear = CR.EventYear and CP.CoachPaperId =CTA.CoachPaperID and CR.AdultId =CTA.AdultId Inner Join"
                StrGradeCard = StrGradeCard & " ChildTestSummary CTS on CTS.EventYear = CR.EventYear and CP.CoachPaperId =CTS.CoachPaperID and CR.AdultId =CTS.AdultId "
                StrGradeCard = StrGradeCard & " Where CR.Approved='Y' and CS.Accepted='Y' and CTS.Completed='Y' and CR.AdultId not in (" & ChildNumber & ") and CR.EventYear = " & EventYear & " and CR.ProductGroupId=" & Request.QueryString(2) & " and CR.ProductID=" & Request.QueryString(3) & " and CR.Level='" & Request.QueryString(4) & "'" ') T1 "
                'Else
                StrGradeCard = StrGradeCard & ") T"
                'End If
                StrGradeCard = StrGradeCard & " Union all Select '','' as ChildName,Null,Null,Null,'','','','',Null,Null,'',Null,Null,Null,Null,Null,0,0,'','','',0,'' as Grade,0 as RowVal "
                StrGradeCard = StrGradeCard & " Order by RowVal desc,WeekID,RowNo " ',Grade desc "
            End If

            ds = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, StrGradeCard)

            If ds.Tables(0).Rows.Count > 1 Then
                tblDGCoachPaper.Visible = True
                'DGGradeCard.DataSource = ds
                'DGGradeCard.DataBind()
                If ds.Tables(0).Rows.Count > 0 Then
                    DGGradeCard.Visible = True
                    Dim row As DataRow = ds.Tables(0).NewRow

                    row("MaxQ") = 0
                    row("Correct") = 0
                    row("Percent") = 0
                    'row("GradeInt") = 0
                    row("Grade") = ""
                    row("RowNo") = "Total"
                    Dim cnt As Integer
                    For cnt = 0 To ds.Tables(0).Rows.Count - 1
                        If ds.Tables(0).Rows(cnt)("RowVal") = 0 Then
                            ds.Tables(0).Rows.RemoveAt(cnt)
                            ds.Tables(0).Rows.InsertAt(row, cnt)
                            Exit For
                        End If
                    Next
                    DGGradeCard.DataSource = ds.Tables(0)
                    DGGradeCard.DataBind()
                Else
                    'Response.Write(StrSQL)
                    lblCoachPaper.Text = "No Data present."

                    DGGradeCard.DataSource = Nothing
                    DGGradeCard.DataBind()
                    DGGradeCard.Visible = False
                End If
            Else
                'Response.Write(StrSQL)
                lblCoachPaper.Text = "No Data present."
                lblCoachPaper.Visible = True
                DGGradeCard.Visible = False
                DGGradeCard.DataSource = Nothing
                DGGradeCard.DataBind()
                DGGradeCard.Visible = False
            End If
        Catch ex As Exception
            lblCoachPaper.Text = ex.ToString() '"Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub DGGradeCard_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGGradeCard.ItemDataBound
        Try
            Dim ProductGroup As String = "" '= DataBinder.Eval(e.Item.DataItem, "ProductGroupCode")
            Dim Product As String = "" ' = DataBinder.Eval(e.Item.DataItem, "ProductCode")
            Dim Level As String = "" '= DataBinder.Eval(e.Item.DataItem, "Level")
            Dim StrUpdate As String = ""

            If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
                Dim MaxQ As Integer = DataBinder.Eval(e.Item.DataItem, "MaxQ")
                Dim Correct As Decimal = DataBinder.Eval(e.Item.DataItem, "Correct")
                Dim Percent As Decimal = DataBinder.Eval(e.Item.DataItem, "Percent")
                'Dim GradeInt As Decimal = DataBinder.Eval(e.Item.DataItem, "GradeInt")
                Dim Grade As String = DataBinder.Eval(e.Item.DataItem, "Grade")
                Dim ChildNumber_Grid As Integer = IIf(DataBinder.Eval(e.Item.DataItem, "ChildNumber") Is DBNull.Value, 0, DataBinder.Eval(e.Item.DataItem, "ChildNumber"))

                'Dim MaxPap As Integer = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select COUNT(*) as Max_Count from ChildTestSummary where  CoachPaperID in (Select Distinct CoachPaperID from CoachPapers where EventYear = " & Request.QueryString(1) & " and ProductGroupCode='" & ProductGroup & "' and ProductCode = '" & Product & "' and Level = '" & Level & "'")
                'Dim ChildCount As Integer = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select COUNT(*)  as count from ChildTestSummary where  CoachPaperID in (Select Distinct CoachPaperID from CoachPapers where EventYear = " & Request.QueryString(1) & " and ProductGroupCode=" & ProductGroup & " and ProductCode='" & Product & "' and Level ='" & Level & "'  and ChildNumber=" & Request.QueryString(0))
                'Dim TopPaperCount As Integer = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "cast(Round((Select COUNT(*) from ChildTestSummary where  CoachPaperID in (Select Distinct CoachPaperID from CoachPapers where EventYear = " & Request.QueryString(1) & " and ProductGroupCode= " & ProductGroup & " and ProductCode = '" & Product & "' and Level ='" & Level & "')) *0.7,0) as int)  ")

                'If DGGradeCard.Items.Count > 0 Then
                If DataBinder.Eval(e.Item.DataItem, "RowNo") = "Total" Then
                    CType(e.Item.FindControl("lblMaxQ"), Label).Text = dblMaxQ 'FormatNumber(IIf(dblMaxQ = 0.0, 0, dblMaxQ), 2)
                    CType(e.Item.FindControl("lblCorrect"), Label).Text = FormatNumber(IIf(dblCorrect = 0.0, 0, dblCorrect), 2)
                    dblPercent = (dblCorrect / dblMaxQ) * 100 'Percent
                    CType(e.Item.FindControl("lblPercent"), Label).Text = FormatNumber(IIf(dblPercent = 0.0, 0, dblPercent), 2)

                    If dblPercent >= 90 Then ' And dblPercent < 2 Then
                        Grade = "A"
                    ElseIf dblPercent >= 80 And dblPercent < 90 Then
                        Grade = "B"
                    ElseIf dblPercent >= 70 And dblPercent < 80 Then
                        Grade = "C"
                    ElseIf dblPercent >= 60 And dblPercent < 70 Then
                        Grade = "D"
                    ElseIf dblPercent < 60 Then
                        Grade = "E"
                    End If
                    CType(e.Item.FindControl("lblGrade"), Label).Text = Grade.ToString()
                    CType(e.Item.FindControl("lblMax_Count"), Label).Text = MaxPap.ToString()
                    'CType(e.Item.FindControl("lblCount"), Label).Text = ChildCount.ToString()
                    CType(e.Item.FindControl("lblTop_Count"), Label).Text = TopPaperCount.ToString()

                    StrUpdate = "Update CoachReg set MaxQ =" & dblMaxQ & ",Correct=" & FormatNumber(IIf(dblCorrect = 0.0, 0, dblCorrect), 2) & ",Percentage=" & FormatNumber(IIf(dblPercent = 0.0, 0, dblPercent), 2) & ",GradeScore='" & Grade & "' where ChildNumber =" & Request.QueryString(0) & " and EventYear=" & Request.QueryString(1) & " and ProductGroupId=" & Request.QueryString(2) & " and ProductID=" & Request.QueryString(3) & " and Level='" & Request.QueryString(4) & "'"
                    SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, StrUpdate)

                Else
                    'If ChildNumber_Grid = Request.QueryString(0) Then
                    dblMaxQ += MaxQ
                    'End If
                    dblCorrect += Correct
                    ' dblPercent += (Correct / MaxQ) * 100 'Percent
                    ' dblGrade += GradeInt

                    ProductGroup = DataBinder.Eval(e.Item.DataItem, "ProductGroupCode")
                    Product = DataBinder.Eval(e.Item.DataItem, "ProductCode")
                    Level = DataBinder.Eval(e.Item.DataItem, "Level")

                    'Response.Write("select  COUNT(*)  from CoachPapers CP where EventYear=" & Request.QueryString(1) & " and ProductGroupCode='" & ProductGroup & "' and ProductCode='" & Product & "' and Level='" & Level & "' and DocType='Q' and exists (select CoachPaperID from ChildTestSummary where EventYear=" & Request.QueryString(1) & "  and CP.CoachPaperId=CoachPaperId)")
                    'Response.Write("<br /><br /><br />")
                    MaxPap = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(*) from CoachPapers CP where EventYear=" & Request.QueryString(1) & "  and ProductGroupCode='" & ProductGroup & "'  and ProductCode='" & Product & "' and Level='" & Level & "' and DocType='Q' and exists (select CoachPaperID from ChildTestSummary where EventYear=" & Request.QueryString(1) & "  and CP.CoachPaperId=CoachPaperId)")
                    TopPaperCount = (Math.Round(MaxPap * 0.7, 0)).ToString() 'MaxPap
                    'Commented and updated on Jan 05 2015

                    'MaxPap = (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select count(*)  from  CoachPapers where EventYear = " & Request.QueryString(1) & " and ProductGroupCode='" & ProductGroup & "' and ProductCode = '" & Product & "' and Level = '" & Level & "' and DocType='Q'   ")).ToString()
                    'ChildCount = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select  COUNT(*)  from CoachPapers CP where EventYear=" & Request.QueryString(1) & "  and ProductGroupCode='" & ProductGroup & "'  and ProductCode='" & Product & "' and Level='" & Level & "' and DocType='Q' and exists (select CoachPaperID from ChildTestSummary where EventYear=" & Request.QueryString(1) & "  and CP.CoachPaperId=CoachPaperId)")
                    'Select COUNT(*)  as count from ChildTestSummary where  CoachPaperID in (Select Distinct CoachPaperID from CoachPapers where EventYear = " & Request.QueryString(1) & " and ProductGroupCode='" & ProductGroup & "' and ProductCode='" & Product & "' and Level ='" & Level & "' and DocType='Q')  and ChildNumber=" & Request.QueryString(0))).ToString()
                    'TopPaperCount = (Math.Round(ChildCount * 0.7, 0)).ToString() 'MaxPap
                End If

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
End Class

