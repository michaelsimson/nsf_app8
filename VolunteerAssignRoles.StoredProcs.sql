if exists( select * from sysobjects where name = 'usp_GetEvents' and xtype='p' )
	drop procedure usp_GetEvents
go

create procedure usp_GetEvents
as
begin
	select EventID,EventCode from Event order by EventCode
end
go


if exists( select * from sysobjects where name = 'usp_GetRoles' and xtype='p' )
	drop procedure usp_GetRoles
go

create procedure usp_GetRoles
as
begin
	select RoleID,selection from Role order by selection
end
go




if exists( select * from sysobjects where name = 'usp_GetZones' and xtype='p' )
	drop procedure usp_GetZones
go

create procedure usp_Getzones
as
begin
	select ZoneID ,ZoneCode+' - ['+Description +']' as ZoneCode 
	from Zone
	where status like 'A%'
	order by ZoneCode
end
go


if exists( select * from sysobjects where name = 'usp_GetClusters' and xtype='p' )
	drop procedure usp_GetClusters
go

create procedure usp_GetClusters
as
begin
	select ClusterID, ClusterCode+' - ['+Description +']'as ClusterCode 
	from Cluster 
	where status like 'A%'
	order by ClusterCode
end
go

if exists( select * from sysobjects where name = 'ufn_GetMemberNameByID' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function ufn_GetMemberNameByID
go

create function dbo.ufn_GetMemberNameByID(@memberid int) returns varchar(60)
as
begin
	declare @name varchar(60)
	select @name = firstName + ' '+ LastName
	from IndSpouse i
	where i.automemberid = @memberid

	return @name
end
go

if exists( select * from sysobjects where name = 'ufn_GetMemberLastNameByID' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function ufn_GetMemberLastNameByID
go

create function dbo.ufn_GetMemberLastNameByID(@memberid int) returns varchar(60)
as
begin
	declare @name varchar(60)
	select @name =  LastName
	from IndSpouse i
	where i.automemberid = @memberid

	return @name
end
go

if exists( select * from sysobjects where name = 'usp_SelectWhereIndSpouse2' and xtype='p' )
	drop procedure usp_SelectWhereIndspouse2
go
 
create PROCEDURE usp_SelectWhereIndspouse2
	@WhereCondition nvarchar(500)
AS
begin
SET NOCOUNT ON
DECLARE @SQL nvarchar(3250)
SET @SQL = 'select automemberid,firstname,lastname,email,hphone,address1,city,state,zip,'
		+ 'dbo.ufn_getChapterCode( chapterid) as chapterCode ,'+
		+ ' referredby,liasonperson from indspouse where '+ @whereCondition

EXEC sp_executesql @SQL
end
go




if exists( select * from sysobjects where name = 'usp_SelectWhereVolunteer2' and xtype='p' )
	drop procedure dbo.usp_SelectWhereVolunteer2
go
 
create PROCEDURE dbo.usp_SelectWhereVolunteer2
	@WhereCondition nvarchar(500)
AS
begin
SET NOCOUNT ON
DECLARE @SQL nvarchar(3250)
SET @SQL = '
SELECT  dbo.ufn_getMemberNameByID( v.memberid) as Name
	  ,[VolunteerId]
      ,v.[MemberId]
      ,v.[RoleId]
      ,dbo.ufn_getRoleCode(v.[RoleId]) as ''Selection''
      ,case v.TeamLead when null then ''N'' else  v.TeamLead end as TeamLead
      ,v.[EventYear]
      ,v.[EventId]
      ,v.[EventCode]
      ,v.[ChapterId]
      ,v.[ChapterCode]
      ,v.[National]
      ,v.[ZoneId]
      ,v.[ZoneCode]
      ,v.[ClusterId]
      ,v.[ClusterCode]
      ,v.[Finals]
      ,v.[IndiaChapter]
      ,v.[ProductGroupID]
      ,dbo.ufn_getProductGroupName(v.[ProductGroupId]) as productGroupCode
      ,v.[ProductId]
      ,dbo.ufn_getProductName(v.[ProductId]) as productCode
      ,isnull(v.AgentFlag,''N'') as AgentFlag
      ,isnull(v.WriteAccess,''N'') as WriteAccess
      ,isnull(v.[Authorization],''N'') as ''Authorization''
      ,v.[Yahoogroup]
      ,v.[IndiaChapterName]
      ,v.[CreateDate]
      ,case len(v.createDate) when 0 then '''' else dbo.ufn_getMemberNameByID(v.[CreatedBy]) end as CreatedBy
      ,v.[ModifyDate]
	  ,case len(v.ModifyDate) when 0 then '''' else dbo.ufn_getMemberNameByID(v.[ModifiedBy]) end as ModifiedBy
    FROM [Volunteer] v
	where ' + @WhereCondition +
' order by roleid , dbo.ufn_getMemberLastNameByID(memberid)'

 EXEC sp_executesql @SQL

end 
go


if exists( select * from sysobjects where name = 'usp_GetRolesWhere' and xtype='p' )
	drop procedure dbo.usp_GetRolesWhere
go

create procedure dbo.usp_GetRolesWhere 
	@wherecondition nvarchar(300)
as
begin
	declare @sql varchar(3000)
set @sql ='select roleid,selection from role ' + @whereCondition +' order by selection '

 EXEC(@sql)

end
go




if exists( select * from sysobjects where name = 'usp_GetProductGroupByEvent' and xtype='p' )
	drop procedure usp_GetProductGroupByEvent
go

create procedure dbo.usp_GetProductGroupByEvent
	@eventId int
as
begin
	select productGroupId, productGroupCode,name
	from productGroup
	where eventId = @eventId	
end
go

if exists( select * from sysobjects where name = 'usp_GetProductByGroupAndEvent' and xtype='p' )
	drop procedure dbo.usp_GetProductByGroupAndEvent
go

create procedure dbo.usp_GetProductByGroupAndEvent
	@productGroupId int,@eventId int
as
begin
	select productId, productCode,name
	from product
	where eventId = @eventId
	and productGroupId = @productGroupId
	and status='o'
end
go



if exists( select * from sysobjects where name = 'usp_UpdateVolunteer' and xtype='p' )
	drop procedure dbo.usp_UpdateVolunteer
go
   
create PROCEDURE dbo.usp_UpdateVolunteer
	@volId int,
	@roleId int,
	@eventYear int,
	@eventId int,
	@productGroupId int =NULL,
	@productId int = NULL,
	@agentFlag varchar(10) =NULL,
	@writeAccess varchar(10)=NULL,
	@yahooGroup varchar(100) =NULL,
	@authorization varchar(10) =NULL,
	@teamLead varchar(10)=NULL,
	@indiaChapterName varchar(500)=NULL,
	@modifyDate datetime = GETDATE,
	@modifiedBy int,
	@memberId int,
	@national varchar(10) =NULL,
	@finals varchar(10) =NULL,
	@clusterId int,
	@chapterId int,
	@zoneId int,
	@indiaChapter varchar(100)
AS
begin
SET NOCOUNT ON
		declare @eventcode varchar(40)
		select @eventcode = eventcode
		from event
		where eventid = @eventid

		update volunteer
		set roleId = @roleId,
			roleCode =(select rolecode from role r where r.roleid=@roleId),
			eventId = @eventId,
			eventCode=@eventCode,
			eventYear = @eventYear,
			productGroupId = @productGroupId,
			productGroupCode = dbo.ufn_getProductGroupCode(@productGroupId),
			productId = @productId,
			productCode = dbo.ufn_getProductCode(@productId),
			agentFlag = @agentFlag,
			writeAccess = @writeAccess,
			yahooGroup = @yahooGroup,
			[authorization] = @authorization,
			teamlead = @teamlead,
			indiaChapterName = @indiaChapterName,
			modifyDate = @modifyDate,
			modifiedBy = @modifiedBy
		where volunteerid = @volId		
end
go



if exists( select * from sysobjects where name = 'ufn_getProductGroupCode' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.ufn_getProductGroupCode
go

create function dbo.ufn_getProductGroupCode(@prodGroupId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = productGroupCode
	from productGroup
	where productGroupId = @prodGroupId

	return @code
end
go



if exists( select * from sysobjects where name = 'ufn_getProductCode' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function ufn_getProductCode
go

create function dbo.ufn_getProductCode(@prodId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = productCode
	from product
	where productId = @prodId

	return @code
end
go



if exists( select * from sysobjects where name = 'ufn_getChapterCode' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.ufn_getChapterCode
go

create function dbo.ufn_getChapterCode(@chapId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = name
	from chapter
	where chapterId = @chapId

	return @code
end
go



if exists( select * from sysobjects where name = 'ufn_getRoleCode' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.ufn_getRoleCode
go

create function dbo.ufn_getRoleCode(@roleId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = selection
	from role
	where roleId = @roleId

	return @code
end
go


if exists( select * from sysobjects where name = 'ufn_getProductName' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.ufn_getProductName
go

create function dbo.ufn_getProductName(@prodId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = name
	from product
	where productId = @prodId

	return @code
end
go



if exists( select * from sysobjects where name = 'ufn_getProductGroupName' and  xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.ufn_getProductGroupName
go

create function dbo.ufn_getProductGroupName(@prodGroupId int) returns varchar(60)
as
begin
	declare @code varchar(60)
	select @code = name
	from productGroup
	where productGroupId = @prodGroupId

	return @code
end
go
if exists( select * from sysobjects where name = 'usp_getCurrentRoles' and xtype='p' )
	drop procedure dbo.usp_getCurrentRoles
go

create procedure dbo.usp_getCurrentRoles 
			@memberId int
as
begin
		select v.volunteerid,v.memberid,firstname,lastname,selection,v.eventcode,
			teamlead,v.[national],zonecode,chaptercode,clustercode,
			pg.name as productGroup ,p.name as product,v.createdate
		from volunteer v 
		inner join indspouse i
		on v.memberid = i.automemberid
		and v.memberid = @memberid
		left outer join productGroup pg
		on pg.productGroupId = v.productGroupId
		left outer join product p
		on p.productId = v.productId
		inner join role r
		on v.roleid=r.roleid
		order by v.createdate desc
		
end
go



if exists( select * from sysobjects where name = 'usp_insertVolunteer' and xtype='p' )
	drop procedure dbo.usp_insertVolunteer
go

create procedure dbo.usp_insertVolunteer 
			@memberId int,
			@roleid int,			
			@teamlead varchar(10) ='N',
			@eventyear int,
			@eventid int,
			@eventcode varchar(10),
			@chapterid int = NULL ,
			@chaptercode varchar(35) = NULL,
			@national varchar(10) = NULL,
			@zoneid int =NULL,
			@zonecode varchar(10) =NULL,
			@clusterid int =NULL,
			@clustercode varchar(10) =NULL,
			@finals varchar(10) = NULL,
			@indiaChapter varchar(35),
			@productGroupId int =NULL,
			@productGroupCode varchar(10) = NULL,
			@productid int = NULL,
			@productCode varchar(10) = NULL,
			@agentFlag varchar(10) =NULL,
			@writeAccess varchar(10) = NULL,
			@authorization varchar(10) = NULL,
			@yahoogroup varchar(100) =NULL,
			@indiachapterName varchar(35) =NULL,
			@createdBy int 
as
begin
		declare @duplicate as varchar(10)
		declare @ret as int

		set @ret = -1
		set @duplicate = 'false'
		declare @roleCode varchar(50)
		select @roleCode=rolecode from role where roleid=@roleid  
		

		INSERT INTO [dbo].[Volunteer]
			   ([MemberId]
			   ,[RoleId]
			   ,[RoleCode]
			   ,[TeamLead]
			   ,[EventYear]
			   ,[EventId]
			   ,[EventCode]
			   ,[ChapterId]
			   ,[ChapterCode]
			   ,[National]
			   ,[ZoneId]
			   ,[ZoneCode]
			   ,[ClusterId]
			   ,[ClusterCode]
			   ,[Finals]
			   ,[IndiaChapter]
			   ,[ProductGroupID]
			   ,[ProductGroupCode]
			   ,[ProductId]
			   ,[ProductCode]
			   ,[AgentFlag]
			   ,[WriteAccess]
			   ,[Authorization]
			   ,[Yahoogroup]
			   ,[IndiaChapterName]
			   ,[CreateDate]
			   ,[CreatedBy])
	         
		 VALUES(
				@memberId  ,
				@roleid  ,
				@rolecode,
				@teamlead  ,
				@eventyear  ,
				@eventid  ,
				@eventcode  ,
				@chapterid  ,
				@chaptercode  ,
				@national  ,
				@zoneid  ,
				@zonecode  ,
				@clusterid  ,
				@clustercode  ,
				@finals  ,
				@indiaChapter  ,
				@productGroupId  ,
				@productGroupCode  ,
				@productid  ,
				@productCode  ,
				@agentFlag  ,
				@writeAccess  ,
				@authorization  ,
				@yahoogroup  ,
				@indiachapterName  ,
				getDate(),
				@createdBy  )

			select @ret =@@identity
end 
go





if exists( select * from sysobjects where name = 'usf_isDuplicateRole' and xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.usf_isDuplicateRole
go
  
create function dbo.usf_isDuplicateRole(
	@memberid int,
	@roleId int,
	@eventYear int,
	@eventId int,
	@national varchar(10),
	@finals varchar(10),
	@indiaChapter varchar(10),
	@zoneId int,
	@clusterId int,
	@chapterId int,
	@productGroupId int,
	@productId int) returns varchar(10)
AS
begin

	declare @ret varchar(10)
	set @ret = 'false'

	if exists ( select * from volunteer
				where memberid = @memberid
				and roleId = @roleId
				and eventYear = @eventYear
				and eventId = @eventId
				and [national] = @national
				and finals = @finals
				and indiaChapter = @indiaChapter
				and zoneId = @zoneId
				and clusterId = @clusterId
				and chapterId = @chapterId
				and productGroupId = @productGroupId
				and productId = @productId
	)
	begin
		set @ret='true'
	end
	return @ret
end
go




if exists( select * from sysobjects where name = 'usf_isDuplicateTeamLead' and xtype in ('p',N'FN', N'IF', N'TF')  )
	drop function dbo.usf_isDuplicateTeamLead
go
  
create function dbo.usf_isDuplicateTeamLead(
	@teamLead varchar(10),
	@roleId int,
	@eventYear int,
	@eventId int,
	@national varchar(10),
	@finals varchar(10),
	@indiaChapter varchar(10),
	@zoneId int,
	@clusterId int,
	@chapterId int,
	@productGroupId int,
	@productId int) returns varchar(10)
AS
begin

	declare @ret varchar(10)
	set @ret ='false'

	if exists ( select * from volunteer
				where teamLead = @teamLead
				and roleId = @roleId
				and eventYear = @eventYear
				and eventId = @eventId
				and [national] = @national
				and finals = @finals
				and indiaChapter = @indiaChapter
				and zoneId = @zoneId
				and clusterId = @clusterId
				and chapterId = @chapterId
				and productGroupId = @productGroupId
				and productId = @productId
	)
	begin
		set @ret='true'
	end

	return @ret
end
go




if exists( select * from sysobjects where name = 'usp_getRoleCategories' and xtype='p' )
	drop procedure dbo.usp_getRoleCategories
go

create procedure dbo.usp_getRoleCategories
				@roleId as int
as
begin

		declare @s  table
		(
			category varchar(20)
		)
		if exists (select * from role where [national]='y' and roleid= @roleId ) 
			insert into @s values ('National')
		if exists (select * from role where [Zonal]='y' and roleid=@roleId ) 
			insert into @s values ('Zonal')

		if exists (select * from role where [Cluster]='y' and roleid=@roleId ) 
			insert into @s values ( 'Cluster')

		if exists (select * from role where [Chapter]='y' and roleid=@roleId) 
			insert into @s values ( 'Chapter')

		if exists (select * from role where [Finals]='y' and roleid=@roleId ) 
			insert into @s values ('Finals')
		if exists (select * from role where [IndiaChapter]='y' and roleid=@roleId ) 
			insert into @s values ('IndiaChapter')

		select * from @s
end
go



if exists( select * from sysobjects where name = 'usp_getVolunteerRoleCategory' and xtype='p' )
	drop procedure dbo.usp_getVolunteerRoleCategory
go

create procedure dbo.usp_getVolunteerRoleCategory
					@volunteerId int
as
begin

		declare @n varchar(10)
		declare @z varchar(40)
		declare @cl varchar(40)
		declare @c varchar(40)
		declare @f varchar(40)
		declare @ic varchar(40)

		select @n=isnull([national],'0'),
				@z=isnull(zoneid,'0'),
				@cl=isnull(clusterid,'0'),
				@c=isnull(chapterid,'0'),
				@f=isnull(finals,'0'),
				@ic=isnull(indiachapter,'0')
		from volunteer
		where volunteerid=@volunteerId

		if( @n <> '0') 
			select 'National'
		else if( @z <> '0') 
			select 'Zonal'
		else if( @cl <> '0') 
			select 'Cluster'
		else if ( @c <> '0')
			select 'Chapter'
		else if (@f <> '0') 
			select 'Finals'
		else if( @ic <> '0') 
			select 'India Chapter'
		else 
			select ''
	end
go