<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdCluster.aspx.vb" Inherits="AddUpdCluster" title="Add/Update Cluster" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <asp:hyperlink id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink><br />
    
  <table cellpadding="0" cellspacing="0" border="0" align="left" width = "1004"><tr><td align = "center"> 
     <center>
    <table cellpadding = "4" cellspacing = "0"  border="0" width="400">
        <tr>
            <td align="center" colspan="3">
                <h2>
                ADD/UPDATE CLUSTER</h2>
            </td>
        </tr>
         <tr>
            <td align="center" colspan="3">
                </td>
        </tr>
    <tr><td align = "left">
        Cluster Code</td><td></td><td align="left"><asp:TextBox ID="txtClusterCode" runat="server" Width="150px" Height="18px"></asp:TextBox> <asp:Label ID="Label1" runat="server" Font-Size="Small" ForeColor="Red" Text="* Use Unique Code"></asp:Label></td>
    </tr>    
        <tr>
            <td align="left">
            Name </td>
            <td>            
            </td>
            <td align="left">
                <asp:TextBox ID="txtClusterName" runat="server" Width="150px" Height="18px"></asp:TextBox>
                 
                </td>
        </tr>
         <tr>
            <td align="left">
                Zone</td>
            <td>
            </td>
            <td align="left"><asp:DropDownList ID="ddlZone" runat="server" Width="155px" Height="23px">
                <asp:ListItem Selected="True" Value="0">Select Zone</asp:ListItem>
    </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                Status</td>
            <td>
            </td>
            <td align="left"> <asp:DropDownList ID="ddlStatus" runat="server" Width="155px" Height="23px">
                    <asp:ListItem Value="A">Active</asp:ListItem>
                    <asp:ListItem Value="I">Inactive</asp:ListItem>
                    <asp:ListItem Selected="True">Select Status</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
        <tr>
            <td align="left">
                Description</td>
            <td>
            </td>
            <td align="left">
                <asp:TextBox ID="txtDescription" runat="server" Width="150px" Height="30px" TextMode="MultiLine"></asp:TextBox></td>
        </tr>
        <tr>
         <td align="center"></td>
            <td align="left" colspan="2">
                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" /> 
                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width = "60" />
            </td>
        </tr>
    </table>
    <asp:Panel runat="server" ID="panel3"   Visible="False">

    <center> <asp:Label ID="Pnl3Msg"  ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>    </center>
     <br />
     <br />

    <asp:GridView ID="GridView1" runat="server" DataKeyNames="ClusterID" AutoGenerateColumns="False"  OnRowCommand="GridView1_RowCommand" >
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
            <asp:BoundField DataField="ClusterID"  HeaderText="Cluster Id" />
            <asp:BoundField DataField="ClusterCode" HeaderText="Cluster Code" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="ZoneCode" HeaderText="Zone Code"/>
            <asp:BoundField DataField="Name" HeaderText="Cluster Name" ItemStyle-HorizontalAlign="Left" />
            <asp:BoundField DataField="Status" HeaderText="Status" />
            <asp:BoundField DataField="Description" HeaderText="Description" ItemStyle-HorizontalAlign="Left" />
         
                 
        </Columns>
    
    </asp:GridView>


</asp:Panel>
    </center>
    </td></tr></table>
</asp:Content>
