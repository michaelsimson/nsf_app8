<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdateRoles.aspx.vb" EnableEventValidation="false" Inherits="AddUpdateRoles" Title="Add/Update Roles" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:HyperLink ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink><br />
    <table cellpadding="0" cellspacing="0" border="0" align="left" width="1100">
        <tr>
            <td align="center">
                <center>
                    <table cellpadding="4" cellspacing="0" border="0" width="1000px">
                        <tr>
                            <td align="center" colspan="6">
                                <h2>ADD/UPDATE ROLES</h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="left">Role Code</td>
                            <td align="left">
                                <asp:TextBox ID="txtRoleCode" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                            <td align="left">Finals</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlFinals" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">Selection</td>
                            <td align="left">
                                <asp:TextBox ID="txtSelection" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left">National</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlNational" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">Cluster</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCluster" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">Description</td>
                            <td align="left">
                                <asp:TextBox ID="txtDescription" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left">Zonal</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlZonal" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">IndiaChapter</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlIndiaChapter" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">Restrictions</td>
                            <td align="left">
                                <asp:TextBox ID="txtRestrictions" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left">Chapter</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlChapter" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Y</asp:ListItem>
                                    <asp:ListItem Value="" Text=""></asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">Name</td>
                            <td align="left">
                                <asp:TextBox ID="txtName" runat="server" Width="150px" Height="18px"></asp:TextBox>&nbsp;</td>
                            <td align="left">Responsibilities</td>
                            <td align="left">
                                <asp:TextBox ID="txtResponsibilities" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left"></td>
                            <td align="left">Team</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlTeam" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="null" Selected="True">Select</asp:ListItem>

                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="center"></td>
                            <td align="center"></td>
                            <td align="center"></td>
                            <td align="left">
                                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" />
                                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width="60" />
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                    <div runat="server" id="DivExcel" align="left">
                        <asp:Button ID="ExpExcel1" runat="server" Text="Export to Excel" />
                    </div>
                    <br />
                    <asp:Panel runat="server" ID="panel3" Visible="False">
                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="RoleId" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand">
                            <Columns>
                                <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                <asp:BoundField DataField="RoleId" HeaderText="RoleId" />
                                <asp:BoundField DataField="RoleCode" HeaderText="RoleCode" />
                                <asp:BoundField DataField="National" HeaderText="National" />
                                <asp:BoundField DataField="Zonal" HeaderText="Zonal" />
                                <asp:BoundField DataField="Chapter" HeaderText="Chapter" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="Finals" HeaderText="Finals" />
                                <asp:BoundField DataField="Cluster" HeaderText="Cluster" />
                                <asp:BoundField DataField="IndiaChapter" HeaderText="IndiaChapter" />
                                <asp:BoundField HeaderStyle-Width="100px" DataField="TeamName" HeaderText="Team" />
                                <asp:BoundField DataField="Name" HeaderText="Name" />
                                <asp:BoundField DataField="Selection" HeaderText="Selection" />
                                <asp:BoundField DataField="Description" HeaderText="Description" />
                                <asp:BoundField DataField="Restrictions" HeaderText="Restrictions" />
                                <asp:BoundField DataField="Responsibilities" HeaderText="Responsibilities" />
                                <%-- <asp:BoundField DataField="CreateDate" HeaderText="Date" DataFormatString="{0:d}"/>
                <asp:BoundField DataField="CreatedBy" HeaderText="Created By"/>
                <asp:BoundField DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:d}"/>
                <asp:BoundField DataField="ModifiedBy" HeaderText="Modified By"/> --%>
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </center>
            </td>
        </tr>
    </table>
</asp:Content>
