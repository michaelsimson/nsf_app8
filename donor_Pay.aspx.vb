Imports System
Imports System.Text
Imports System.Configuration
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections.Specialized
Imports System.Globalization
Imports LinkPointTransaction
Imports System.Text.RegularExpressions
Imports NorthSouth.BAL
Imports nsf.Entities
'Imports GDE4
'Imports GDE4.Service
'Imports GDE4.Transaction
Imports Braintree
Imports System.Net.Mail
Imports System.IO

Namespace VRegistration
    ' <summary>
    ' Summary description for regnlregnet_Pay.
    ' </summary>
    Partial Class Donor_Pay

        Inherits LinkPointAPI_cs.LinkPointTxn_Page
        'Private us As CultureInfo = New CultureInfo("en-US")
        'http://regxlib.com/REDetails.aspx?regexp_id=540
        Private nRegFee As Decimal = 0
        Private nDonationAmt As Decimal = 0
        Private nTotalAmt As Decimal = 0
        Private nMealsAmt As Decimal = 0.0
        Private nLateFee As Decimal = 0
        Dim isTestMode As Boolean = False
        Dim Cust_id As String = String.Empty
        Protected EmailID As String
        Protected RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
        Dim txnStatus As String = String.Empty
        Dim txtAuth As String = String.Empty
        Dim custIndId As Integer = 0
        Dim BT_Auth As String = String.Empty
        Dim BT_Tag As String = String.Empty
        Dim BT_BankMessage As String = String.Empty
        Dim BT_result As String = String.Empty
        Dim BT_Id As String = String.Empty
        Dim BT_Pass As String = String.Empty
        Dim BT_Platform As String
        Dim BT_ETG_Response As String = String.Empty
        Dim BT_Brand As String = String.Empty
        Dim BT_Token As String = String.Empty
        Dim BT_Error As String = String.Empty
        Dim EventId As String = String.Empty

        ' Brain Tree Gateway

        Dim gateway As BraintreeGateway
        Dim ccReq As TransactionCreditCardRequest = New TransactionCreditCardRequest()
        Dim ccAddrReq As CreditCardAddressRequest = New CreditCardAddressRequest()
        Dim tReq As TransactionRequest = New TransactionRequest()


        ' Dim sQLH As New SQLHelper
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            EventId = Request.QueryString("id")
            If (Session("EventId") = 22) Then
                lnkback.Text = ""
                hlnkMainPage.Text = ""
            Else

                If Session("entryToken") = "Parent" Then
                    lnkback.Text = "Back to Parent Functions Page"
                ElseIf Session("entryToken") = "Donor" Then
                    lnkback.Text = "Back to Donor Functions Page"
                ElseIf Session("entryToken") = "Volunteer" Then
                    Requiredfieldvalidatorcvv.EnableClientScript = False
                    Requiredfieldvalidatorcvv.Enabled = False
                    lnkback.Text = "Back to Volunteer Functions Page"
                End If
            End If

            If Session("LoggedIn") <> "True" And EventId = 22 Then
                Response.Redirect("Event_RegDetails.aspx")
            End If

            If Session("LoggedIn") <> "True" Then
                Response.Redirect("login.aspx")
            End If
            If Session("DoPay") Is Nothing Then
                If Session("entryToken") = "Parent" Then
                    Response.Redirect("UserFunctions.aspx")
                ElseIf Session("entryToken") = "Volunteer" Then
                    Response.Redirect("VolunteerFunctions.aspx")
                Else
                    Response.Redirect("DonorFunctions.aspx")
                End If
            End If
            If (Not Request.QueryString("id") Is Nothing) Then
                Session("EventID") = Request.QueryString("id")
            End If
            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)
            lblDonation.Text = ""
            lblTotalAmount.Text = ""
            'If (Not (Session("LATEFEE")) Is Nothing) Then
            '    nLateFee = CType(Session("LATEFEE"), Integer)
            '    nTotalAmt += nLateFee
            'End If
            If (Not (Session("RegFee")) Is Nothing) Then
                'Response.Write("Registration fee:" + Session("RegFee").ToString + "<br>")
                nRegFee = CType(Session("RegFee"), Integer)
                lblRegFee.Text = nRegFee.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nRegFee
            Else
                'Response.Write("Session(RegFee) value not set" + "<br>")

            End If
            If (Not (Session("Donation")) Is Nothing) Then
                'Response.Write("Donation:" + Session("Donation").ToString + "<br>")
                nDonationAmt = CType(Session("Donation"), Integer)
                lblDonation.Text = nDonationAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt = (nTotalAmt + nDonationAmt)
            End If
            If (nTotalAmt = 0) Then
                Response.Redirect("donor_donate.aspx")
            End If
            lblTotalAmount.Text = nTotalAmt.ToString("c", New CultureInfo("en-US"))
            If Not Page.IsPostBack Then
                If Not Session("FinalsDonation") Is Nothing Then
                    hlnkMainPage.NavigateUrl = "~/Don_athonDonorDetails.aspx?id=5"
                End If
                Dim countries As DataSet = New DataSet
                countries.ReadXml(MapPath("countries.xml"))
                ddlCountry.DataSource = countries
                ddlCountry.DataValueField = "value"
                ddlCountry.DataTextField = "text"
                ddlCountry.DataBind()
                Dim dsStates As DataSet
                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlState.DataSource = dsStates.Tables(0)
                    ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlState.DataBind()
                End If
                GetIPAddress(lblIP)
                PrePopulate()
                ' PrepareFormData() 'comment
                ddlYear.Items.Clear()
                Dim li As ListItem = New ListItem(" ", "")
                ddlYear.Items.Add(li)
                Dim i As Integer = System.DateTime.Today.Year
                Do While (i _
                            <= (System.DateTime.Today.Year + 20))
                    Dim j As Integer = (i - 2000)
                    li = New ListItem(i.ToString, j.ToString)
                    ddlYear.Items.Add(li)
                    i = (i + 1)
                Loop
                'Dim nsfMaster As NSFMasterPage = Me.Master
                'nsfMaster.addBackMenuItem("reg_donate.aspx")
            End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub
        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()
        End Sub

        Private Sub PrePopulate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim drIndSpouse As SqlDataReader
            Try
                drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndSpouseByID", New SqlParameter("@autoMemberID", Session("CustIndID")))
                ' Iterate through DataReader, should only be one 
                While (drIndSpouse.Read())
                    If Not drIndSpouse.IsDBNull(0) Then
                        nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                        txtAddress1.Text = drIndSpouse("Address1").ToString()
                        txtAddress2.Text = drIndSpouse("Address2").ToString()
                        txtCity.Text = drIndSpouse("City").ToString()
                        lblMail.Text = drIndSpouse("Email").ToString() 'Added 24/07/2013                       
                        ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                        If ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US" Then
                            Requiredfieldvalidator3.Enabled = True
                            ddlState.Visible = True
                            txtState.Visible = False
                            RFVTxtState.Enabled = False
                            Dim strCountry As String = IIf(ddlCountry.SelectedValue = "IN", "usp_GetIndiaStates", "usp_GetStates")
                            Dim dsStates As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, strCountry)
                            If dsStates.Tables.Count > 0 Then
                                ddlState.DataSource = dsStates.Tables(0)
                                ddlState.DataTextField = dsStates.Tables(0).Columns(2).ToString
                                ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                                ddlState.DataBind()
                                'ddlCountry.Enabled = False
                                'ddlState.Enabled = False
                            End If
                            ddlState.Items.FindByValue(drIndSpouse("State").ToString()).Selected = True
                        Else
                            Requiredfieldvalidator3.Enabled = False
                            ddlState.Visible = False
                            txtState.Visible = True
                            RFVTxtState.Enabled = True
                            txtState.Text = drIndSpouse("State").ToString()
                        End If
                        txtZip.Text = drIndSpouse("Zip").ToString()

                    End If
                End While
                If Not drIndSpouse Is Nothing Then drIndSpouse.Close()
            Finally
                drIndSpouse = Nothing
            End Try

        End Sub

        Protected Sub PrepareFormData()
            bname = txtCardHolderName.Text
            baddr1 = txtAddress1.Text
            baddr2 = txtAddress2.Text
            bstate = ddlState.SelectedValue.ToString
            bcity = txtCity.Text
            bcountry = ddlCountry.SelectedValue.ToString
            bzip = txtZip.Text
            cardnumber = txtCardNumber.Text
            expmonth = ddlMonth.SelectedValue.ToString
            expyear = ddlYear.SelectedValue.ToString
            cvmindicator = "not_provided"

            cvmvalue = Request.Form("cvmvalue")
            'subtotal = Request.Form("subtotal")
            'tax = Request.Form("tax")
            'shipping = nDonationAmt.ToString
            'total = nTotalAmt.ToString

            '***Testing Purposes only
            subtotal = Request.Form("subtotal")
            tax = Request.Form("tax")
            shipping = nDonationAmt.ToString
            total = nTotalAmt.ToString

            refnumber = Request.Form("refnumber")

            'If (Not (Session("ContestsSelected")) Is Nothing) Then
            '** Ferdine Silva 06/07/2010
            ''comments = CType(Session("ContestsSelected"), String)
            'Else
            'comments = ""
            'End If
            If (Session("EventId")) = 22 Then
                Dim ChapterId As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 ChapterId from FreeEvent where FreeEventId=" & Session("FreeEventid").ToString() & "")
                Dim ProductCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 ProductCode from Product where Eventid=" & Session("EventId").ToString() & "")

                comments = CType(ProductCode, String)
                comments = (comments + ("(" & Session("CustIndId").ToString & ")"))
                comments = (comments + ("(" & ChapterId & ")"))
                comments = (comments + ("(" & nRegFee.ToString & ")"))
                comments = (comments + (" RegFee:" + nRegFee.ToString))

                comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            Else
                comments = (" Donation:" + nDonationAmt.ToString)
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            End If





            referred = ("Donated on : " + System.DateTime.Now.ToLongTimeString)
            result = Request.Form("result")
            origin = Request.Form("origin")

            ' GetConfigParams()
            ' configCDE4()
        End Sub

        Private Sub TrimTextBoxEntries(ByRef txtbox As TextBox)
            txtbox.Text = txtbox.Text.Trim
        End Sub

        ' Function to Check for CreditCard.
        Public Function IsCreditCard(ByVal strToCheck As String) As Boolean
            Dim objAlphaNumericPattern As Regex = New Regex("^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$")
            Return objAlphaNumericPattern.IsMatch(strToCheck)
        End Function
        Public Enum CreditCardTypeType
            Visa
            MasterCard
            Discover
            Amex
            Switch
            Solo
        End Enum
        'Private Const cardRegex As String = "^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$"
        Private Const cardRegex As String = "^(?:(?<Visa>4\d{3})|(?<MasterCard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<Amex>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(Amex)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$"

        Private Function GetCardTypeFromNumber(ByVal cardNum As String) As String
            'Create new instance of Regex comparer with our
            'credit card regex patter
            Dim cardTest As New Regex(cardRegex)

            'Compare the supplied card number with the regex
            'pattern and get reference regex named groups
            Dim gc As GroupCollection = cardTest.Match(cardNum).Groups

            'Compare each card type to the named groups to 
            'determine which card type the number matches
            If gc(CreditCardTypeType.Amex.ToString()).Success Then
                'Return CreditCardTypeType.Amex
                Return "Amex"
            ElseIf gc(CreditCardTypeType.MasterCard.ToString()).Success Then
                'Return CreditCardTypeType.MasterCard
                Return "MasterCard"
            ElseIf gc(CreditCardTypeType.Visa.ToString()).Success Then
                '  Return CreditCardTypeType.Visa
                Return "Visa"
            ElseIf gc(CreditCardTypeType.Discover.ToString()).Success Then
                'Return CreditCardTypeType.Discover
                Return "Discover"
            Else
                'Card type is not supported by our system, return null
                '(You can modify this code to support more (or less)
                ' card types as it pertains to your application)
                Return Nothing
            End If
        End Function

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
            lblCardError.Text = ""
            lblMessage.Text = ""
            TrimTextBoxEntries(txtCardNumber)
            If Session("LoggedIn") Is Nothing Then
                Server.Transfer("Maintest.aspx")
                Exit Sub
            End If
            ' ''If (IsCreditCard(txtCardNumber.Text) = False) Then
            ' ''    lblCardError.Text = "Invalid card number"
            ' ''    Return
            ' ''End If
            If ((Convert.ToInt32(ddlMonth.SelectedValue.ToString) < System.DateTime.Today.Month) _
                        AndAlso (Convert.ToInt32(ddlYear.SelectedValue.ToString) _
                        <= (System.DateTime.Today.Year - 2000))) Then
                lblMessage.Text = "For a valid card the Card Expiration must be in future."
                Return
            End If
            lblErrCvv.Text = ""
            If Len(txtcvv.Text.Trim) = 0 Then
                lblErrCvv.Text = "CVV should not be blank."
                Exit Sub
            Else
                Dim sCardType As String = GetCardTypeFromNumber(txtCardNumber.Text)
                If sCardType Is Nothing Then
                    lblErrCvv.Text = "Enter valid credit card number"
                    Exit Sub
                End If
                If sCardType = "Amex" Or sCardType = "MasterCard" Or sCardType = "Visa" Or sCardType = "Discover" Then
                    Dim iDigit As Integer = 3
                    If sCardType = "Amex" Then
                        iDigit = 4
                    End If
                    If (Len(txtcvv.Text.Trim)) <> iDigit Then
                        lblErrCvv.Text = "Given Card card number does not have a " & Len(txtcvv.Text) & " digit CVV code"
                        Exit Sub
                    End If
                End If
            End If


            If Page.IsValid Then
                TrimTextBoxEntries(txtCardHolderName)
                TrimTextBoxEntries(txtAddress1)
                TrimTextBoxEntries(txtAddress2)
                TrimTextBoxEntries(txtCity)
                TrimTextBoxEntries(txtZip)
                lblMessage.Text = ""
                tblError.Visible = True
                Dim comment As String = comments.ToString

                Dim SQLStrS As String = "SELECT COUNT(*) FROM CCSubmitLog C where  MemberID =" & Session("LoginId").ToString() & "  and CreateDate between dateadd(minute,-60,getdate()) and getdate()"
                Dim PayR As Integer = Convert.ToInt16(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLStrS))
                '  If PayR > 0 Then
                'SQLStrS = "select count(Distinct(CCF4+CCL4)) from CCSubmitLog where MemberID =" & Session("LoginId").ToString()
                'PayR = Convert.ToInt16(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLStrS))
                If PayR >= 4 Then
                    lblMessage.Visible = True
                    lblMessage.Text = "Too many transactions in the last 60 minutes."
                    Exit Sub
                End If
                'End If
                ' Parse form data           
                lbContinue.Enabled = False
                '    PrePopulate()
                PrepareFormData()
                ProcessOrder()
                Session.Remove("FinalsDonation")
            End If
        End Sub
        Private Sub ProcessOrder()
            Dim chkErrWithCustomerCreation As Boolean = False, statusReturnByCust As String = ""
            Dim NoCardIssue As Boolean = True
            Dim logData As String = ""
            Try
                Dim IsValidCustomer As Boolean = True
                Dim cAddress As String
                InitializeBT()
                logData = "Initialized" & vbCrLf & "Member Id :"
                If Not Session("CustIndId") Is Nothing Then
                    logData = logData & Session("CustIndId")
                ElseIf Not Session("LoginId") Is Nothing Then
                    logData = logData & Session("LoginId")
                End If

                PreUpdate()
                logData = logData & " - Pre Updated" & vbCrLf

                cAddress = txtAddress1.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + txtZip.Text + "|" + ddlCountry.SelectedValue.ToString
                'Billing Address
                ccAddrReq.StreetAddress = cAddress.Trim().Replace("'", "''")
                ccAddrReq.Locality = txtCity.Text
                ccAddrReq.Region = ddlState.SelectedValue.ToString
                ccAddrReq.CountryName = ddlCountry.SelectedValue.ToString
                ccAddrReq.PostalCode = txtZip.Text

                ' Credit Card Details
                ccReq.CardholderName = txtCardHolderName.Text.Trim()
                ccReq.ExpirationMonth = ddlMonth.SelectedValue
                ccReq.ExpirationYear = ddlYear.SelectedValue
                ccReq.Number = txtCardNumber.Text.Trim()
                ccReq.CVV = txtcvv.Text.Trim()

                ' Search Customer
                Dim req = New CustomerSearchRequest().FirstName.Is(nameLabel.Text.Trim).Email.Is(lblMail.Text).CreditCardNumber.Is(txtCardNumber.Text)
                logData = logData & " - CustomerSearchReq " & vbCrLf

                Dim customerId As String = ""
                Dim c As Customer
                Try
                    Dim coll As Braintree.ResourceCollection(Of Customer) = gateway.Customer.Search(req)
                    For Each c In coll
                        customerId = c.Id
                    Next
                Catch ex As Exception
                    logData = logData & " - CustomerSearchReq : throws Error: " & ex.ToString & vbCrLf
                    BT_BankMessage = "gateway_rejected"
                    lblMessage.Text = "Error while doing transaction with Gateway "
                    lbContinue.Enabled = True
                End Try

                Dim customer As New CustomerRequest
                customer.FirstName = nameLabel.Text
                customer.Email = lblMail.Text
                ' customer.CreditCard = ccReq
                Dim verification As CreditCardVerification
                If customerId = "" Then
                    ' Create New Customer
                    logData = logData & " - Create New Customer" & vbCrLf
                    Dim newCustomer As Braintree.Result(Of Customer)
                    Try
                        newCustomer = gateway.Customer.Create(customer)
                        If newCustomer.IsSuccess Then
                            customerId = newCustomer.Target.Id
                            logData = logData & " - Created New Customer : Success" & vbCrLf
                        Else
                            logData = logData & " - Create New Customer : Failed" & vbCrLf
                            IsValidCustomer = False
                            GoTo lblInValidCustomer
                        End If
                    Catch ex As Exception
                        logData = logData & " - Create New Customer: throws error : " & ex.ToString & vbCrLf
                        If Not newCustomer Is Nothing Then
                            statusReturnByCust = newCustomer.Message.ToString()
                        End If
                        chkErrWithCustomerCreation = True
                    End Try
                Else
                    logData = logData & " - Update New Customer " & vbCrLf
                    Dim updateCustomer As Braintree.Result(Of Customer)
                    Try
                        updateCustomer = gateway.Customer.Update(customerId, customer)
                        If updateCustomer.IsSuccess Then
                            customerId = updateCustomer.Target.Id
                            logData = logData & " - Update New Customer : Success" & vbCrLf
                        Else
                            logData = logData & " - Update New Customer : Failure" & vbCrLf
                            IsValidCustomer = False
                            GoTo lblInValidCustomer
                        End If
                    Catch ex As Exception
                        logData = logData & " - Update New Customer:Throw exception1 :" & ex.ToString
                        If Not updateCustomer Is Nothing Then
                            statusReturnByCust = updateCustomer.Message.ToString()
                        End If
                        logData = logData & " - Update New Customer:Throw exception2 :" & statusReturnByCust & vbCrLf
                        BT_Error = ex.ToString
                        chkErrWithCustomerCreation = True
                    End Try
                End If
                tReq.Amount = nTotalAmt.ToString("f2")
                tReq.CustomerId = customerId
                tReq.BillingAddress = ccAddrReq
                tReq.CreditCard = ccReq
                ''''' Device Data
                tReq.DeviceData = Request.Form("device_data")
                If Session("LoggedIn") Is Nothing And EventId = 22 Then

                    Server.Transfer("Event_RegDetails.aspx")
                    Exit Sub
                End If

                If Session("LoggedIn") Is Nothing Then
                    Server.Transfer("Maintest.aspx")
                    Exit Sub
                End If
                Dim result As Braintree.Result(Of Transaction)
                Try
                    logData = logData & " - BT Transaction Initialized :" & vbCrLf
                    result = gateway.Transaction.Sale(tReq)
                    If (result.IsSuccess) Then
                        logData = logData & " - BT Transaction : Success" & vbCrLf
                        Dim tran As Transaction = result.Target
                        Dim re As Braintree.Result(Of Transaction) = gateway.Transaction.SubmitForSettlement(tran.Id)
                        Try
                            'PaymentReference
                            BT_ETG_Response = tran.ProcessorAuthorizationCode.ToString
                            BT_Auth = tran.Id + "-" + tran.Customer.Id
                            'Status Return
                            BT_BankMessage = tran.ProcessorResponseText.ToString()
                            BT_Brand = tran.CreditCard.CardType.ToString
                            BT_Token = tran.CreditCard.Token.ToString()


                        Catch ex As Exception
                            logData = logData & " - BT Transaction : throws exception:" & ex.ToString & vbCrLf
                            BT_Error = ex.ToString
                        End Try
                        lblMessage.ForeColor = Color.Blue
                        lblMessage.Text = "Success Details"
                        lblMessage.Text = (lblMessage.Text + ("<BR>Authorization Code : " + tran.ProcessorAuthorizationCode.ToString))
                    Else
                        logData = logData & " - BT Transaction : Failure " & vbCrLf
                        lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in, and use a different credit card for payment."
                        lbContinue.Enabled = True
                        If Not result Is Nothing Then
                            BT_result = result.Message.ToString
                            BT_Error = result.Message.ToString
                            logData = logData & " - BT Transaction : Failure(BT_Error): " & BT_Error & vbCrLf
                            If result.Target Is Nothing Then
                                BT_BankMessage = result.Message.ToString
                                logData = logData & " - BT Transaction : Failure(Target Err): " & result.Message.ToString & vbCrLf
                            Else
                                BT_BankMessage = result.Target.ProcessorResponseText.ToString()
                                BT_ETG_Response = result.Target.ProcessorAuthorizationCode.ToString()
                                logData = logData & " - BT Transaction : Failure(Target Res): " & BT_BankMessage & vbCrLf
                            End If
                            lblMessage.Text = lblMessage.Text & result.Message.ToString()
                            Dim cvvCode As String = ""
                            Dim avsCode As String = ""
                            If Not result.Transaction Is Nothing Then
                                If Not result.Transaction.CvvResponseCode Is Nothing Then
                                    cvvCode = result.Transaction.CvvResponseCode.ToString()
                                    avsCode = result.Transaction.AvsPostalCodeResponseCode.ToString()
                                End If
                            End If
                            If cvvCode.Length > 0 Then
                                If cvvCode <> "M" Then
                                    NoCardIssue = False
                                    If avsCode <> "M" Then
                                        lblMessage.Text = "CVV Code and Postal code do not match. Please enter the correct codes."
                                    Else
                                        lblMessage.Text = "CVV Code does not match. Please enter the correct code."
                                    End If
                                ElseIf cvvCode = "M" And avsCode <> "M" Then
                                    NoCardIssue = False
                                    lblMessage.Text = "Postal Code does not match. Please enter the correct code."
                                End If
                            End If
                        Else
                            logData = logData & " - BT Transaction : Failure(Result object Err)" & vbCrLf
                        End If
                    End If

                Catch ex As Exception
                    BT_Error = ex.ToString
                    logData = logData & " - BT Transaction : Exception : " & ex.ToString & vbCrLf
                    lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in, and use a different credit card for payment."
                    lblMessage.Text = lblMessage.Text & "<br>" & BT_BankMessage
                    lbContinue.Enabled = True
                End Try
lblInValidCustomer:
                If chkErrWithCustomerCreation = True Then
                    BT_BankMessage = statusReturnByCust
                End If
                If IsValidCustomer = False Or chkErrWithCustomerCreation = True Then
                    lblMessage.Text = "The credit card issuer has declined to process this transaction.  Please log out and log back in, and use a different credit card for payment."
                    lblMessage.Text = lblMessage.Text & "<br>" & BT_BankMessage
                    lbContinue.Enabled = True
                End If
                PostUpdate()
                If (BT_BankMessage = "Approved") Or (Not result Is Nothing And Not result.Target Is Nothing) Then
                    If BT_BankMessage = "Approved" Or result.Target.ProcessorResponseCode.ToString() = "1000" Then
                        logData = logData & " - BT Transaction : Approved"
                        'Store transaction data on Session and redirect
                        ' ''Session("outXml") = outXml
                        ' ''Session("resp") = resp
                        lblMessage.Text = ""
                        If (SavePaymentInfo() = True) Then
                            logData = logData & " - BT Transaction : Save payment"
                            WriteToErrorLog(logData, "", "Payment Success")
                            'Update Access value with NULL for user memeberid
                            Dim custIndId As String = DBNull.Value.ToString
                            custIndId = Session("CustIndId").ToString
                            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
                            If (Session("EventId") = 22) Then
                                Response.Redirect("freeEvent/FreeEventRegistrationThankU.aspx?Id=" & Session("FreeEventId").ToString() & "&MId=" & Session("FrMemberId").ToString() & "")
                            Else
                                Dim StrSql3 As String
                                StrSql3 = "Update DonationsInfo Set Access = NULL Where DonationNumber = (Select MAX(DonationNumber) from DonationsInfo where MEMBERID = " & custIndId & " and Access = 0)"
                                Dim ds1 As DataSet
                                ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSql3)
                                'Session("PaymentReference") = R_OrderNum  'added - 1/15/07
                                Session("PaymentReference") = BT_Auth
                                Session("R_Approved") = "APPROVED"
                                Session("DoPay") = Nothing
                                Response.Redirect("Donor_pay_success.aspx")
                            End If

                        Else
                            logData = logData & " - BT Transaction : Err with NFG_Transactions"
                            lblMessage.Text = "Error while saving the payment information to NFG_TRANSACTIONS table, but the Credit Card Transaction is successful."
                            lbContinue.Enabled = True
                        End If
                    End If
                Else
                    logData = logData & " - BT Transaction : Not Approved"
                    lblMessage.ForeColor = Color.Red
                    If NoCardIssue = True Then
                        lblMessage.Text = BT_BankMessage.ToString + "We have encountered the following error while processing your credit card"
                        lblMessage.Text = (lblMessage.Text + ("<BR>Card Status:" + BT_BankMessage)) 'Approved
                        'lblMessage.Text = (lblMessage.Text + ("<BR>Card Status:" + R_Approved))
                        'lblMessage.Text = (lblMessage.Text + ("<BR>Error:" + R_Error))
                        'lblMessage.Text = (lblMessage.Text + ("<BR>Message:" + R_Message))
                        'lblMessage.Text = (lblMessage.Text + ("<BR>FraudCode:" + R_FraudCode))
                        lblMessage.Text = (lblMessage.Text + "<BR>Please check card information and try again or try a different card")
                    End If
                    lbContinue.Enabled = True
                End If
                lbContinue.Enabled = True
            Catch ex As Exception
                lblMessage.Text = "The credit card issuer has declined to process this transaction. Please log out and log back in, and use a different credit card for payment."
                lbContinue.Enabled = True
            End Try
            WriteToErrorLog(logData, "", "Donation Page")
        End Sub
        Public Sub WriteToErrorLog(ByVal msg As String, ByVal stkTrace As String, ByVal title As String)
            Try
                'check and make the directory if necessary; this is set to look in the application folder, you may wish to place the error log in 
                'another location depending upon the user's role and write access to different areas of the file system
                Dim path As String = Server.MapPath("~/Errors/")
                If Not System.IO.Directory.Exists(path) Then
                    System.IO.Directory.CreateDirectory(path)
                End If
                Dim fileName As String = Server.MapPath("~\Errors\Donor_Pay_log.txt")
                'check the file

                Dim sw As StreamWriter
                If (Not File.Exists(fileName)) Then
                    sw = New StreamWriter(fileName, True)
                Else
                    File.WriteAllText(fileName, "")
                    sw = File.AppendText(fileName)
                End If
                sw.Write("Title: " & title & vbCrLf)
                sw.Write("Message: " & msg & vbCrLf)
                sw.Write("StackTrace: " & stkTrace & vbCrLf)
                sw.Write("Date/Time: " & DateTime.Now.ToString() & vbCrLf)
                sw.Write("================================================" & vbCrLf)
                sw.Close()
                SendMessage(title, msg, fileName)
            Catch ex As Exception
                '   Response.Write(ex.ToString)
            End Try
        End Sub
        Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal strLogFile As String)
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress(System.Configuration.ConfigurationManager.AppSettings("ContestSupport").ToString()) '"nsfcontests@northsouth.org")
            email.To.Add("bindhu.rajalakshmi@capestart.com")
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(strLogFile)
            email.Attachments.Add(oAttch)
            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            Try
                client.Send(email)
            Catch ex As Exception
                ok = False
            End Try
        End Sub
        Private Function SavePaymentInfo() As Boolean
            'save to NFG transactions
            Dim sb As StringBuilder = New StringBuilder
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim eventId As Integer = 0
            Dim eventCode As String
            Dim parentLName As String = ""
            Dim parentFName As String = ""
            Dim parentEmail As String = ""
            Dim CustIndChapterID As String = ""
            'Dim loginEmail As String = ""
            Dim custIndId As String = DBNull.Value.ToString

            Dim ds As DataSet
            If (Not Session("EventID") Is Nothing) Then
                eventId = CInt(Session("EventID").ToString)
            End If
            eventCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", eventId))
            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If
            Try
                ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstName,lastName,email,ChapterID from indspouse where automemberId = " + custIndId)
                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then
                        parentLName = ds.Tables(0).Rows(0)("lastname")
                        parentFName = ds.Tables(0).Rows(0)("firstname")
                        parentEmail = ds.Tables(0).Rows(0)("email")
                        CustIndChapterID = ds.Tables(0).Rows(0)("ChapterID")
                    End If
                End If
            Catch ex As Exception
                ' Response.Write("Error1 :" & ex.ToString)
            End Try
            If Session("CustIndChapterID") Is Nothing Then
                Session("CustIndChapterID") = CustIndChapterID
            End If
            If Session("@EVE") Is Nothing Then
                'Session("@EVE") = eventCode
            End If
            If Session("@EID") Is Nothing Then
                Session("@EID") = eventId
            End If
            sb.Append(" INSERT INTO NFG_TRANSACTIONS([Last Name], ")
            sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
            sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
            sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
            sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear, [Donation Type],PaymentNotes,Brand")
            sb.Append(" )  VALUES ( '<LASTNAME>', ")
            sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
            sb.Append(" GETDATE(), 'NSF', '<DONATIONAMOUNT>', GETDATE(), ")
            sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', '<MEALSAMOUNT>', '<LATEFEE>', ")
            sb.Append(" '<EVENTID>', '<EVENTFOR>', '<DONATIONPURPOSE>', '<PARENTID>', '<CHAPTERID>', '<REGFEE>', '<TOTALAMOUNT>', '<EVENTYEAR>', 'Credit Card','<COMMENTS>','<Brand>')")
            If (nDonationAmt > 0) Then
                'save to donations info
                sb.Append(" declare @donationnumber int ")
                sb.Append(" SELECT @donationnumber = MAX(DonationNumber) ")
                sb.Append(" FROM DonationsInfo WHERE [MEMBERID]=<PARENTID> ")
                sb.Append(" ")
                sb.Append("  if @donationnumber is null ")
                sb.Append("	begin ")
                sb.Append(" set @donationnumber = 1 ")
                sb.Append("	end ")
                sb.Append(" else ")
                sb.Append(" begin ")
                sb.Append(" set @donationnumber = @donationnumber + 1  ")
                sb.Append("	end ")

                sb.Append(" INSERT INTO DonationsInfo([MEMBERID], [DonationNumber], [DonorType],  ")
                sb.Append(" [AMOUNT], [TRANSACTION_NUMBER], [DonationDate], [METHOD],  ")
                sb.Append(" [PURPOSE], [EVENT], [STATUS], [CreateDate], [DeletedFlag], ")
                sb.Append(" [EVENTID],[EVENTYEAR],[CHAPTERID],[TAXDEDUCTION],[CreatedBy],")
                sb.Append("[Anonymous],[Access],[INMEMORYOF],[MATCHINGFLAG],[BankID],[DonationType],Campaign)")
                sb.Append(" VALUES(<PARENTID>, @donationnumber, 'IND',  ")
                sb.Append(" <DONATIONAMOUNT>, '<PAYMENTREFERENCE>', getdate(), 'Credit Card',  ")
                sb.Append(" '<DONATIONPURPOSE>', '<EVENT>', 'Completed', getdate(), 'No', ")
                sb.Append(" '<EVENTID>', '<EVENTYEAR>', '<CHAPTERID>', '<TAXDEDUCTION>', '<LOGINID>',")
                sb.Append("'<ANONYMOUS>','<ACCESS>','<INMEMORYOF>','<MATCHINGFLAG>',1,'Unrestricted',<CAMPAIGN>)")
            End If
            If ((Session("EventId") = 5 Or Session("EventId") = 12 Or Session("EventId") = 18)) And Not Session("WMSponsorID") Is Nothing Then
                If Session("WMSponsorID").ToString() <> "" Then
                    sb.Append(" ")
                    sb.Append(" UPDATE WMSponsor ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>', ")
                    sb.Append(" [PaidAmount]=<TOTALAMOUNT> ")
                    sb.Append(" WHERE [WMSponsorID]=" & Session("WMSponsorID") & "")
                End If
            End If

            If (Session("EventId") = 22) Then
                sb.Append(" ")
                sb.Append(" UPDATE FreeEventReg ")
                sb.Append(" SET [PaymentDate]=getdate(),  ")
                sb.Append(" [PaymentMode]='Credit Card',  ")
                sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                sb.Append(" [Status]='Confirmed', ")
                sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                sb.Append(" WHERE [PaymentReference] is NULL and [FrMemberID]=" & Session("FrMemberId") & " ")
                sb.Append(" AND FreeEVENTID=" & Session("FreeEventId"))

                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From FreeEventReg WHERE [PaymentReference] is NULL and [FrMemberID]=" & Session("FrMemberId") & " AND FreeEVENTID=" & Session("FreeEventId") & " ") > 0 Then

                End If
            End If
            If (Session("EventId") <> 22) Then


                'save to contest charity
                sb.Append(" ")
                sb.Append(" UPDATE Contest_Charity ")
                sb.Append(" SET [PaymentDate]=getdate(),  ")
                sb.Append(" [PaymentMode]='Credit Card', ")
                sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                sb.Append(" WHERE [PaymentReference] is NULL and [MEMBERID]=<PARENTID>  ")
            End If
            'now replace all the parameters with actual values
            sb.Replace("<PARENTID>", custIndId) 'ok
            sb.Replace("<LASTNAME>", parentLName) 'ok
            sb.Replace("<FIRSTNAME>", parentFName) 'ok
            'sb.Replace("<ADDRESS>", (baddr1.Trim + (" " + baddr2.Trim))) ' to be modified
            sb.Replace("<ADDRESS>", (txtAddress1.Text.Trim + (" " + txtAddress2.Text.Trim))) ' to be modified
            ' sb.Replace("<CITY>", bcity.Trim) ' to be modified
            sb.Replace("<CITY>", txtCity.Text)
            'sb.Replace("<STATE>", bstate) ' to be modified
            sb.Replace("<STATE>", ddlState.SelectedValue.ToString)
            'sb.Replace("<ZIP>", bzip) ' to be modified
            sb.Replace("<ZIP>", txtZip.Text)
            sb.Replace("<PARENTEMAIL>", parentEmail) 'ok
            sb.Replace("<REGFEE>", nRegFee.ToString)
            sb.Replace("<DONATIONAMOUNT>", nDonationAmt.ToString)
            ''sb.Replace("<PAYMENTREFERENCE>", R_OrderNum) ' Auth #to be modified with Payment Reference(Cust_ID)
            ''sb.Replace("<APPROVAL_CODE>", R_Code) '  to be modified
            ''sb.Replace("<APPROVAL_STATUS>", R_Approved) 'to be modified
            sb.Replace("<PAYMENTREFERENCE>", BT_Auth) ' Auth #to be modified with Payment Reference(Cust_ID)
            sb.Replace("<APPROVAL_CODE>", Cust_id) '  to be modified
            sb.Replace("<APPROVAL_STATUS>", BT_BankMessage) 'to be modified
            sb.Replace("<STATUS>", "Complete")
            sb.Replace("<COMMENTS>", comments)
            sb.Replace("<Brand>", BT_Brand)
            sb.Replace("<DONATIONPURPOSE>", Session("DONATIONFOR").ToString)
            sb.Replace("<MEALSAMOUNT>", nMealsAmt.ToString)
            sb.Replace("<LATEFEE>", nLateFee.ToString)
            sb.Replace("<EVENT>", Session("@EVE"))
            'sb.Replace("<EVENTID>", CType(11, String))
            sb.Replace("<EVENTID>", Session("@EID"))
            sb.Replace("<EVENTFOR>", Session("@EVE"))
            'sb.Replace("<EVENTFOR>", eventCode)
            sb.Replace("<EVENTYEAR>", Now.Year.ToString)
            sb.Replace("<CHAPTERID>", Session("CustIndChapterID").ToString) 'coming from reg_donate page.
            sb.Replace("<TAXDEDUCTION>", CType(100, String))
            sb.Replace("<LOGINID>", Session("LoginID"))
            sb.Replace("<TOTALAMOUNT>", nTotalAmt.ToString)
            sb.Replace("<ANONYMOUS>", Session("@Anonymous1")) 'coming from Donor_donate page
            sb.Replace("<ACCESS>", Session("@Access1"))
            sb.Replace("<INMEMORYOF>", Session("@inmemory"))
            sb.Replace("<MATCHINGFLAG>", Session("@Matching"))
            If Not Session("Campaign") Is Nothing Then
                sb.Replace("<CAMPAIGN>", "'" & Session("Campaign") & "'")
            Else
                sb.Replace("<CAMPAIGN>", "NULL")
            End If
            Dim saved As Boolean = True
            Dim test As String = sb.ToString
            Session("Name") = parentFName & " " & parentLName
            Session("Address") = txtAddress1.Text.Trim + (" " + txtAddress2.Text.Trim) & vbCrLf
            Session("Address") = Session("Address") & txtCity.Text.Trim & vbCrLf
            Session("Address") = Session("Address") & ddlState.SelectedValue.ToString & vbCrLf
            Session("Address") = Session("Address") & txtZip.Text & vbCrLf
            Session("Date") = Convert.ToDateTime(Now).ToShortDateString
            Try

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sb.ToString)
            Catch se As SqlException
                lblMessage.Text = "The following error occured while saving the payment information.Please contact for technical support" &
                "."
                lblMessage.Text = (lblMessage.Text + se.Message)
                tblError.Visible = True
                saved = False
            End Try
            Return saved

        End Function

        Private Sub lbRegistration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbRegistration.Click
            If Session("entryToken") = "Volunteer" Then
                Response.Redirect("VolunteerFunctions.aspx")
            Else
                Response.Redirect("DonorFunctions.aspx")
            End If
        End Sub

        Protected Sub lnkback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkback.Click
            If (Session("EventId") = 22) Then
                '' Response.Redirect("FreeEvent/FreeEventReg.aspx")
            Else
                If Session("entryToken") = "Parent" Then
                    Response.Redirect("UserFunctions.aspx")
                ElseIf Session("entryToken") = "Donor" Then
                    Response.Redirect("DonorFunctions.aspx")
                ElseIf Session("entryToken") = "Volunteer" Then
                    Response.Redirect("VolunteerFunctions.aspx")
                End If
            End If


        End Sub

        Private Sub GetIPAddress(ByRef ilbl As Label)
            Dim IPAdd As String = String.Empty
            Try
                IPAdd = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                If String.IsNullOrEmpty(IPAdd) Then
                    IPAdd = Request.ServerVariables("REMOTE_ADDR")
                End If

                ilbl.Text = IPAdd.ToString
            Catch ex As Exception
                Response.Write("Error getting IP Address : " + ex.Message.ToString)
            End Try

        End Sub
        Private Function gDate() As String
            Dim dt As String
            dt = ddlMonth.SelectedValue.ToString() + Right(ddlYear.Text, 2)
            Return dt
        End Function
        Private Sub PreUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim cAddress As String
            Dim eventId As Integer = 0
            Dim ccL As String
            Dim ccF As String
            Dim pName As String = System.IO.Path.GetFileNameWithoutExtension(Request.Path)
            Dim sqlparams(28) As SqlClient.SqlParameter
            Dim nDisAmt As Decimal = 0
            ccL = Right(txtCardNumber.Text, 4)
            ccF = Left(txtCardNumber.Text, 4)

            If (Not Session("@EID") Is Nothing) Then
                eventId = CInt(Session("@EID").ToString)
            End If
            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If
            cAddress = txtAddress1.Text + "|" + txtZip.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + ddlCountry.SelectedValue.ToString
            sqlparams(0) = New SqlParameter("@MemberID", SqlDbType.Int)
            sqlparams(0).Value = CInt(custIndId.ToString)
            sqlparams(1) = New SqlParameter("@EventYear", SqlDbType.Int)
            sqlparams(1).Value = CInt(Now.Year.ToString)
            sqlparams(2) = New SqlParameter("@EventID", SqlDbType.Int)
            sqlparams(2).Value = CInt(eventId.ToString)
            sqlparams(3) = New SqlParameter("@AspxPage", SqlDbType.VarChar)
            sqlparams(3).Value = pName.ToString
            sqlparams(4) = New SqlParameter("@PaymentDate", SqlDbType.SmallDateTime)
            sqlparams(4).Value = System.DateTime.Now.ToString
            sqlparams(5) = New SqlParameter("@IPAddress", SqlDbType.VarChar)
            sqlparams(5).Value = lblIP.Text
            sqlparams(6) = New SqlParameter("@TransType", SqlDbType.VarChar)
            sqlparams(6).Value = "00"
            sqlparams(7) = New SqlParameter("@Platform", SqlDbType.VarChar)
            sqlparams(7).Value = BT_Platform
            sqlparams(8) = New SqlParameter("@StatusSend", SqlDbType.VarChar)
            sqlparams(8).Value = "Sent"
            sqlparams(9) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(9).Value = DBNull.Value
            sqlparams(10) = New SqlParameter("@Amount", SqlDbType.Float)
            sqlparams(10).Value = nTotalAmt.ToString("f2")
            sqlparams(11) = New SqlParameter("@Fee", SqlDbType.Float)
            sqlparams(11).Value = System.DBNull.Value
            sqlparams(12) = New SqlParameter("@LateFee", SqlDbType.Float)
            sqlparams(12).Value = System.DBNull.Value
            sqlparams(13) = New SqlParameter("@Meal", SqlDbType.Float)
            sqlparams(13).Value = System.DBNull.Value
            sqlparams(14) = New SqlParameter("@Donation", SqlDbType.Float)
            sqlparams(14).Value = nTotalAmt.ToString("f2")
            sqlparams(15) = New SqlParameter("@CCF4", SqlDbType.VarChar)
            sqlparams(15).Value = ccF.ToString
            sqlparams(16) = New SqlParameter("@CCL4", SqlDbType.VarChar)
            sqlparams(16).Value = ccL.ToString
            sqlparams(17) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(17).Value = System.DBNull.Value
            sqlparams(18) = New SqlParameter("@CCName", SqlDbType.VarChar)
            sqlparams(18).Value = txtCardHolderName.Text
            sqlparams(19) = New SqlParameter("@CCZip", SqlDbType.VarChar)
            sqlparams(19).Value = txtZip.Text
            sqlparams(20) = New SqlParameter("@ClientEmail", SqlDbType.VarChar)
            sqlparams(20).Value = lblMail.Text
            sqlparams(21) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar)
            sqlparams(21).Value = comments.ToString()
            sqlparams(22) = New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)
            sqlparams(22).Value = System.DateTime.Now.ToString
            sqlparams(23) = New SqlParameter("@CreatedBy", SqlDbType.Int)
            sqlparams(23).Value = CInt(custIndId.ToString)
            sqlparams(24) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(24).Value = System.DateTime.Now.ToString
            sqlparams(25) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(25).Value = custIndId
            sqlparams(26) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(26).Direction = ParameterDirection.Output
            sqlparams(27) = New SqlParameter("@Discount", SqlDbType.Float)
            sqlparams(27).Value = nDisAmt.ToString("f2")
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertCCSubmitLog", sqlparams)
                Cust_id = sqlparams(26).Value.ToString()
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support" &
              "."
                lblMessage.Text = (lblMessage.Text + ex.Message)
                tblError.Visible = True
            End Try
        End Sub

        Private Sub PostUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim sqlparams(8) As SqlClient.SqlParameter
            sqlparams(0) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(0).Value = BT_BankMessage
            sqlparams(1) = New SqlParameter("@ETG_Response", SqlDbType.VarChar)
            sqlparams(1).Value = BT_ETG_Response
            sqlparams(2) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(2).Value = BT_Auth
            sqlparams(3) = New SqlParameter("@Token", SqlDbType.VarChar)
            sqlparams(3).Value = BT_Token
            sqlparams(4) = New SqlParameter("@Brand", SqlDbType.VarChar)
            sqlparams(4).Value = BT_Brand
            sqlparams(5) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(5).Value = System.DateTime.Now
            sqlparams(6) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(6).Value = custIndId
            sqlparams(7) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(7).Value = Cust_id.ToString
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_UpdateCCSubmitLog", sqlparams)
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support."
                lblMessage.Text = (lblMessage.Text + ex.Message)
                tblError.Visible = True
            End Try
        End Sub

        Sub InitializeBT()
            Dim BT As NameValueCollection = CType(ConfigurationManager.GetSection("BT/crd"), NameValueCollection)
            Dim domainDir As String = Request.Url.AbsolutePath.ToString().ToLower
            If domainDir.Contains("app9/") = True Then
                gateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, BT("MerchantId"), BT("PublicKey"), BT("PrivateKey"))
            Else
                gateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
            End If
            BT_Platform = BT("Platform")
        End Sub

    End Class

End Namespace


