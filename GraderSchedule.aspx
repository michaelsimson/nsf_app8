﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="GraderSchedule.aspx.vb" Inherits="GraderSchedule" title="Grader Schedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">


<div align="left">
        <asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>

<table id="MainTable" runat="server" align="left" border="0">
      <tr><td align="center">Grader Schedule </td></tr> 
      <tr><td>  
            <table id="Table1" border="1" runat="server" align="left"><tr>  
                      <td align="left" width="100px">ContestYear </td>
                          <td> <asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px"> Event</td>
                          <td><asp:DropDownList ID="ddlEvent" DataTextField="Name" DataValueField="EventId" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px">Chapter</td>
                          <td><asp:DropDownList ID="ddlChapter" DataTextField="ChapterCode" DataValueField="ChapterId" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px">Purpose</td>
                          <td><asp:DropDownList ID="ddlPurpose" DataTextField="Purpose" DataValueField="Purpose" AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 10px"></td>
                      <td width="100px"> Date </td> 
                          <td><asp:DropDownList ID="ddlDate" DataTextField="Date" DataValueField="ContestDate" runat="server" Width="125px" visible="true" AutoPostBack="true"> </asp:DropDownList></td>
                      </tr>
            </table> 
      </td></tr>
      <tr><td> 
            <table id="Table2" border="0" runat="server" align="center"><tr>
               <td>
                 <table id="Table3" border="1" runat="server">
                        <tr id="TrProductGroup" runat="server">
                                   <td align="left" width="100px">ProductGroup </td><td>
                                   <asp:DropDownList ID="ddlProductGroup" DataTextField="ProductGroupCode" DataValueField="ProductGroupId"  AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td> 
                            </tr><tr id="TrProduct" runat="server"> 
                                   <td width="100px"> Product</td><td> 
                                   <asp:DropDownList ID="ddlProduct"  DataTextField="ProductCode" DataValueField="ProductId" AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td>
                            </tr><tr id ="TrPhase" runat="server"> 
                                    <td width="100px">Phase</td><td> 
                                    <asp:DropDownList ID="ddlPhase"  AutoPostBack="true" DataTextField="Phase" DataValueField="Phase" runat="server" width="125px">
                                          <%--  <asp:ListItem Text="0" Value="0">Select Phase</asp:ListItem>
                                            <asp:ListItem Text="1" Value="1">1</asp:ListItem>
                                            <asp:ListItem Text="2" Value="2">2</asp:ListItem>
                                            <asp:ListItem Text="3" Value="3">3</asp:ListItem>--%>
                                    </asp:DropDownList> </td> 
                            </tr><tr id="TrBldg" runat="server"><td>BldgName</td>
                                   <td><asp:DropDownList ID="ddlBldgID" runat="server" AutoPostBack="true" DataTextField="BldgID" DataValueField="BldgID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr id="TrSeqNo" runat="server">
                                   <td align="left" width="100px">SeqNo</td><td>
                                   <asp:DropDownList ID="ddlSeqNo" runat="server" AutoPostBack="true" DataTextField="SeqNo" DataValueField="SeqNo" Enabled="true" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr id="TrRoomNo" runat="server">
                                   <td align="left" width="100px">RoomNumber</td><td>
                                   <asp:DropDownList ID="ddlRoomNo" runat="server" AutoPostBack="true" DataTextField="RoomNumber" DataValueField="RoomNumber" Enabled="false" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr id="TrStarttime" runat="server"> 
                                    <td width="100px">StartTime</td><td> 
                                    <asp:DropDownList ID="ddlStartTime" runat="server" DataTextField="StartTime" DataValueField="StartTime" width="125px" Enabled="false"></asp:DropDownList> </td> 
                            </tr><tr id="TrEndtime" runat="server"> 
                                    <td width="100px"> EndTime </td><td> 
                                    <asp:DropDownList ID="ddlEndTime" runat="server" DataTextField="EndTime" DataValueField="EndTime" width="125px" Enabled="false"></asp:DropDownList> </td> 
                            </tr>
                 </table>
              </td>
              <td id="Td1" align="center" runat="server">
                 <table id="TableGrader" border="1"  runat="server" align="center">
                            <tr><td>Grader 1</td><td>
                                   <asp:DropDownList ID="ddlGrader_1" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 2</td>
                               <td><asp:DropDownList ID="ddlGrader_2" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr ><td>Grader 3</td>
                               <td><asp:DropDownList ID="ddlGrader_3" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 4</td>
                               <td><asp:DropDownList ID="ddlGrader_4" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 5</td>
                               <td><asp:DropDownList ID="ddlGrader_5" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr ><td>Grader 6</td>
                               <td><asp:DropDownList ID="ddlGrader_6" runat="server" DataTextField="Name" DataValueField="AutoMemberID" visible="true" Width="125px"></asp:DropDownList></td>
                            </tr><tr ><td>Grader 7</td>
                               <td><asp:DropDownList ID="ddlGrader_7" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 8</td>
                               <td><asp:DropDownList ID="ddlGrader_8" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr><tr ><td>Grader 9</td>
                               <td><asp:DropDownList ID="ddlGrader_9" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 10</td>
                              <td><asp:DropDownList ID="ddlGrader_10" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 11</td>
                              <td><asp:DropDownList ID="ddlGrader_11" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr><tr><td>Grader 12</td>
                              <td><asp:DropDownList ID="ddlGrader_12" runat="server" DataTextField="Name" DataValueField="AutoMemberID" Width="125px"></asp:DropDownList></td>
                            </tr>   
                            
                 </table>
              </td></tr>
            </table> 
      </td></tr>
      <tr id="TrCopy" runat="server" align="center" visible="false"><td>
             <table>
                    <tr>
                        <td width="120px" align="right">To(ProductGroup)</td>
                        <td><asp:DropDownList ID="ddlToPG" AutoPostBack="true" runat="server" DataTextField ="ProductGroupCode" DataValueField="ProductGroupID" width="125px" Enabled="True"></asp:DropDownList>
                        <asp:DropDownList ID="ddlToProduct" AutoPostBack="true" runat="server" DataTextField="Productcode" DataValueField="ProductID" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
                        </td><td>StartTime</td><td><asp:DropDownList ID="ddlStartCpTime" DataTextField="StartTime" DataValueField="StartTime" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
                        </td><td> EndTime</td><td><asp:DropDownList ID="ddlEndCpTime" DataTextField="EndTime" DataValueField="EndTime" runat="server" width="125px" Enabled="True"></asp:DropDownList> &nbsp;&nbsp;
                        </td>           
                        <td><asp:Button ID="BtnCopySchedule" runat="server" Text="CopySchedule"> </asp:Button></td>
                    </tr>
             </table></td>
      </tr>
      <tr id="TrAddUpdate" runat="server"><td align="center">
                <asp:Button ID="BtnCopy" runat="server" Width="100px" Text="Copy"> </asp:Button>
                <asp:Button ID="btnAddUpdate" runat="server" Width="100px" Text="Add/Update"> </asp:Button>
                <asp:Button ID="BtnCancel"  OnClick="BtnCancel_Click" runat="server"  Text="Cancel" /></td></tr>
      <tr><td align="center"><asp:Label ID="lblAddUPdate" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
      <tr><td align="center"><asp:Label ID="lblErr" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
      <tr><td align="center"><asp:Label ID="lblRoomSchID" runat="server" Text="" ForeColor="Red" Visible="false"></asp:Label></td></tr>
</table>

<asp:DataGrid ID="DGRoomSchedule" runat="server" DataKeyField="RoomSchID" AutoGenerateColumns="False" OnItemCommand="DGRoomSchedule_ItemCommand" CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
                 <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
                    <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                        <ItemStyle BackColor="White" />
     <COLUMNS>
       <asp:TemplateColumn>
                    <ItemTemplate>
		                <asp:LinkButton id="lbtnRemove" runat="server" CommandName="Delete" Text="Delete" Enabled="False"></asp:LinkButton>
		            </ItemTemplate>
		    </asp:TemplateColumn>
            <asp:TemplateColumn>
                    <ItemTemplate>          
		               <asp:LinkButton id="lbtnEdit" runat="server" CommandName="Edit" Text="Edit" Enabled="false"></asp:LinkButton>
		            </ItemTemplate>           
            </asp:TemplateColumn>   
         <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Chapter"  HeaderText="Chapter" />
           <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear"/>
             <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Event" HeaderText="Event"/>
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID"/>
                <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="SeqNo" HeaderText="SequenceNo"/>
                  <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="RoomNumber" HeaderText="RoomNumber"/>
            <asp:TemplateColumn HeaderText="Capacity"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblCapacity" runat="server" Text='<%# DataBinder.Eval(Container,"DataItem.Capacity") %>'></asp:Label>
		            </ItemTemplate>           
            </asp:TemplateColumn>        
         <%--<asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>--%>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Date" DataFormatString="{0:d}" HeaderText="Date"/>
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"/>
                <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"/>
                  <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Phase" HeaderText="Phase"/>
           <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="StartTime" HeaderText="StartTime" />
             <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndTime" HeaderText="EndTime"/>
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="StartBadgeNo" HeaderText="StartBadgeNo"/>
                 <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="EndBadgeNo" HeaderText="EndBadgeNo"/>
                                 
                <asp:TemplateColumn HeaderText="Grader_1"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_1" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                <asp:TemplateColumn HeaderText="Grader_2"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_2" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Grader_3"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_3" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_4"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_4" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_5"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_5" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_6"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_6" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_7"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_7" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_8"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_8" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_9"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_9" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                 
               <asp:TemplateColumn HeaderText="Grader_10"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_10" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn> 
                    <asp:TemplateColumn HeaderText="Grader_11"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_11" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                    <asp:TemplateColumn HeaderText="Grader_12"  HeaderStyle-Font-Bold="true">
                    <ItemTemplate>
		               <asp:Label id="lblGrader_12" runat="server" Text=""></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                 <asp:TemplateColumn HeaderText="GradSchID"  HeaderStyle-Font-Bold="true" visible="false">
                    <ItemTemplate>
		               <asp:Label id="lblGradSchID" runat="server"  Visible="false"></asp:Label>
		            </ItemTemplate>           
                 </asp:TemplateColumn>
                                     
     </COLUMNS>           
               <HeaderStyle BackColor="White" />
 </asp:DataGrid>


</asp:Content>

