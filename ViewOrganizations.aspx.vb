Imports NorthSouth.BAL
Imports NorthSouth.DAL
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class ViewOrganizations
    Inherits System.Web.UI.Page
    Dim strSql As String

    Protected Sub dgOrgList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgOrgList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                If Not Request.QueryString("MemberiD") Is Nothing Then
                    CType(e.Item.FindControl("hlUpdateOrganization"), LinkButton).PostBackUrl = "UpdateOrganization.aspx?MemberID=" & e.Item.DataItem("AutoMemberID").ToString
                Else
                    CType(e.Item.FindControl("hlUpdateOrganization"), LinkButton).Enabled = False
                End If
        End Select
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not IsPostBack Then
            If Not Request.QueryString("Id") Is Nothing Then
                Dim dsOrganizations As New DataSet
                dsOrganizations = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT * FROM OrganizationInfo WHERE AutoMemberID=" & Request.QueryString("Id") & "")
                dgOrgList.DataSource = dsOrganizations
                dgOrgList.DataBind()
                Exit Sub
            End If
            Dim LoginChapterID As String
            Dim strChapterID As String
            strChapterID = ""
            If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
                strSql = "Select chapterid, chaptercode, state from chapter where "
                If Session("RoleID") = "4" Then
                    strSql = strSql & " clusterid in (Select clusterid from chapter where "
                    strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") "
                ElseIf Session("RoleID") = "5" Then
                    strSql = strSql & " chapterid = " & Session("LoginChapterID") & ""
                End If
                strSql = strSql & "order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))

                Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drNSFChapters.Read())
                    If Len(strChapterID) > 0 Then
                        strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                    Else
                        strChapterID = drNSFChapters(0).ToString()
                    End If
                End While
            End If
            'If Session("RoleID") = "5" And Session("SelChapterID") = 1 Then
            '    LoginChapterID = Session("SelChapterID")
            ' Else
            If Session("RoleID") = "5" Then
                LoginChapterID = strChapterID
            Else
                If Len(Session("LoginChapterID")) > 0 Then
                    LoginChapterID = Session("LoginChapterID")
                Else
                    LoginChapterID = 0
                End If
            End If
            If Globals.SortExpression Is Nothing Then
                LoadGrid(LoginChapterID, "ORGANIZATION_NAME")
            Else
                LoadGrid(LoginChapterID, Globals.SortExpression)
            End If

            If Request.QueryString("MemberiD") Is Nothing Then
                lnkAdd.Enabled = False
            End If
        End If
    End Sub

    Protected Sub dgOrgList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgOrgList.PageIndexChanged
        Try
            dgOrgList.CurrentPageIndex = e.NewPageIndex
            Dim LoginChapterID As Integer
            Dim strChapterID As String
            strChapterID = ""
            If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
                strSql = "Select chapterid, chaptercode, state from chapter where "
                If Session("RoleID") = "4" Then
                    strSql = strSql & " clusterid in (Select clusterid from chapter where "
                    strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") "
                ElseIf Session("RoleID") = "5" Then
                    strSql = strSql & "chapterid = " & Session("LoginChapterID") & ""
                End If
                strSql = strSql & "order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))

                Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drNSFChapters.Read())
                    If Len(strChapterID) > 0 Then
                        strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                    Else
                        strChapterID = drNSFChapters(0).ToString()
                    End If
                End While
            End If
            'If Session("RoleID") = "5" And Session("SelChapterID") = 1 Then
            'LoginChapterID = Session("SelChapterID")
            'Else
            If Session("RoleID") = "5" Then
                LoginChapterID = strChapterID
            Else
                If Len(Session("LoginChapterID")) > 0 Then
                    LoginChapterID = Session("LoginChapterID")
                Else
                    LoginChapterID = 0
                End If
            End If
            If Globals.SortExpression Is Nothing Then
                LoadGrid(LoginChapterID, "ORGANIZATION_NAME")
            Else
                LoadGrid(LoginChapterID, Globals.SortExpression)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub dgOrgList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgOrgList.SelectedIndexChanged
        
    End Sub
    Private Sub LoadGrid(ByVal chapterID As String, Optional ByVal SortExpresssion As String = "")
        Dim organization As New Organization
        Dim dsOrganizations As New DataSet
        dsOrganizations = organization.GetOrganizationList(Application("ConnectionString"), SortExpresssion, chapterID)
        dgOrgList.DataSource = dsOrganizations
        dgOrgList.DataBind()
    End Sub

    Protected Sub dgOrgList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgOrgList.SortCommand
        If Not Request.QueryString("Id") Is Nothing Then
            Dim dsOrganizations As New DataSet
            dsOrganizations = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "SELECT * FROM OrganizationInfo WHERE AutoMemberID=" & Request.QueryString("Id") & "")
            dgOrgList.DataSource = dsOrganizations
            dgOrgList.DataBind()
            Exit Sub
        End If
        If e.SortExpression = "" Then Exit Sub
        Globals.SortExpression = e.SortExpression
        Dim LoginChapterID As Integer
        Dim strChapterID As String
        strChapterID = ""
        If Session("RoleID") = "4" Or Session("RoleID") = "5 Then" Then
            strSql = "Select chapterid, chaptercode, state from chapter where "
            If Session("RoleID") = "4" Then
                strSql = strSql & " clusterid in (Select clusterid from chapter where "
                strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") "
            ElseIf Session("RoleID") = "5" Then
                strSql = strSql & " chapterid = " & Session("LoginChapterID") & ""
            End If
            strSql = strSql & "order by state, chaptercode"
            Dim con As New SqlConnection(Application("ConnectionString"))

            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
        End If
        'If Session("RoleID") = "5" And Session("SelChapterID") = 1 Then
        'LoginChapterID = Session("SelChapterID")
        'Else
            If Session("RoleID") = "5" Then
                LoginChapterID = strChapterID
            Else
                If Len(Session("LoginChapterID")) > 0 Then
                    LoginChapterID = Session("LoginChapterID")
                Else
                    LoginChapterID = 0
                End If
            End If
            LoadGrid(LoginChapterID, e.SortExpression)
    End Sub
End Class
