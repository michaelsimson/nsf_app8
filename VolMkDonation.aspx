﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="VolMkDonation.aspx.vb" Inherits="VolMkDonation" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
  <LINK href="Styles.css" type="text/css" rel="stylesheet">
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
}
    
-->
</style>
    <title>Donate</title>
</head>
<body>
    <form id="form1" runat="server">
    <div align="center">
    <table border="0" cellpadding ="2" cellspacing = "0" width ="1000">
    <tr><td align="center">
   <img src="images/trilogo.gif" width="980px" alt ="" />
	</td></tr>
	<tr>
  <td align="left"> &nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink CssClass="SmallFont" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Button ID="btnSrchParent" OnClick="btnSrchParent_Click" runat="server" Text="Search Parent" Visible = "false"  />
  &nbsp;&nbsp;&nbsp;&nbsp;<asp:Literal ID="ltl1" runat="server"></asp:Literal>
  </td> </tr>
  
  <tr><td align = "center" >
    <asp:Panel ID="pIndSearch" runat="server" Width="950px"  Visible="False">
<b> Search NSF Parent</b>   
<div align = "center">       
    <table  border="1" runat="server" id="tblIndSearch" style="text-align:center"  width="30%" visible="true" bgcolor="silver" >	
        <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;Last Name:</td>
			<td align="left" ><asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
    	</tr>
    	<tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;First Name:</td>
			<td  align ="left" ><asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
    	</tr>
    	 <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;E-Mail:</td>
			<td align="left"><asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
    	</tr>
    	<%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
    	<tr>
    	    <td align="right">
    	        <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
    	    </td>
    	    <td  align="left">					
		        <asp:Button ID="btnIndClose" runat="server"  Text="Close" onclick="btnIndClose_onclick" CausesValidation="False"/>						
		    </td>
    	</tr>		
	</table>
	<asp:Label ID="lblIndSearch"  ForeColor="red" runat="server" Visible = "false" Text="Select the Chapter from the DropDown"></asp:Label>
</div> 
<br />
<asp:Panel ID="Panel4" runat="server"  Visible="False" HorizontalAlign="Center">
 <b> Search Result</b>
         <asp:GridView   HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
         <Columns>
                                <asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
                                <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
                                <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
                                <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
                                <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
                                <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
                                <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
                                <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
                                <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
                                <asp:BoundField DataField="chapterCode" headerText="Chapter" ></asp:BoundField>
                               
         </Columns> </asp:GridView>    
 </asp:Panel>
 </asp:Panel>
   <asp:Label ID="lblCustIndID" ForeColor="White" runat="server"></asp:Label>
  </td></tr>
  
     </table> 
     <div align="left" >
   <table border="0" cellpadding = "2" cellspacing = "0" >
            <tr><td width="10px" >&nbsp;
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink1" runat="server" NavigateUrl="http://www.northsouth.org/">[Home]</asp:HyperLink>
                </td>
                <td width="10px">
                </td>
                <td >
                    <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="~/Logout.aspx">[Logout]</asp:HyperLink>
                </td>
            </tr>
        </table> 
        </div>      
       
    </div>
    </form>
</body>
</html>
