﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ExceptionalCoaching.aspx.vb" Inherits="ExceptionCoaching" Title="Exceptional  Coaching" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>
        <script src="js/ezmodal.js"></script>
        <link href="css/Hover.css" rel="stylesheet" />
        <script src="js/Hover.js"></script>
        <script type="text/javascript">

</script>
        <br />
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;&nbsp;<br />
    </div>

    <table width="100%">
        <tr>
            <td align="center" style="margin-left: auto; margin-right: auto;">
                <table border="0" cellpadding="3" cellspacing="0" style="border-style: solid; border-width: 1px" class="tableclass">
                    <tr>
                        <td colspan="8" style="font-weight: bold; background-color: #ffffcc;" class="ContentSubTitle">
                            <center>
                                <h3>Exception List for Coaching Registration </h3>
                            </center>

                        </td>
                    </tr>
                    <tr>
                        <td align="left">Event Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlEventYear" runat="server" AutoPostBack="true">
                            </asp:DropDownList>
                        </td>
                        <td align="left"></td>
                        <td align="left">Event</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlEvent" Width="150px" DataTextField="Name" DataValueField="EventID" runat="server"></asp:DropDownList>
                        </td>
                        <td align="left"></td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>



                    <tr>
                        <td align="left">Semester</td>
                        <td align="left">
                            <asp:DropDownList Width="150px" ID="ddlSemester" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                        <td align="left">&nbsp;</td>

                        <td align="left">Product Group</td>
                        <td align="left">
                            <asp:DropDownList Width="150px" ID="ddlProductGroup" DataTextField="ProductGroupCode" DataValueField="ProductGroupID" runat="server" AutoPostBack="True"></asp:DropDownList></td>
                        <td align="left">&nbsp;</td>



                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>

                    </tr>

                    <tr>

                        <td align="left">Product </td>
                        <td align="left">
                            <asp:DropDownList Width="150px" ID="ddlProduct" DataTextField="ProductCode" DataValueField="ProductID" runat="server" AutoPostBack="true"></asp:DropDownList></td>
                        <td align="left"></td>

                        <td align="left">Coach Name</td>
                        <td align="left">
                            <asp:DropDownList Width="150px" ID="ddlCoach" runat="server" DataTextField="CoachName" DataValueField="MemberId">
                            </asp:DropDownList></td>
                        <td align="left">&nbsp;</td>



                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>


                    <tr>
                        <td align="left">Parent</td>
                        <td align="left">
                            <asp:TextBox Width="145px" ID="txtParent" runat="server" Enabled="false"></asp:TextBox></td>
                        <td align="left">
                            <asp:Button ID="Search" runat="server" Text="Search" /></td>
                        <td align="left">Student </td>
                        <td align="left">
                            <asp:DropDownList Enabled="false" ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField="ChildNumber" Width="150px" runat="server"></asp:DropDownList></td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>


                    <tr>
                        <td align="left">New Deadline</td>
                        <td align="left">
                            <asp:TextBox ID="txtDeadline" Width="145" runat="server"></asp:TextBox></td>
                        <td align="left" class="SmallFont">MM/DD/YYYY</td>
                        <td align="left"></td>
                        <td align="left">
                            <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
                        </td>
                        <td align="left"></td>
                        <td align="left"></td>
                        <td align="left">
                            <asp:Label ID="hlblExContestantID" runat="server" Visible="false"></asp:Label>
                        </td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Button ID="buttonAdd" runat="server" Text="Add" />
                            &nbsp;
                            <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width="60" /></td>
                        <td align="left"></td>
                        <td align="left"></td>
                    </tr>
                    <tr>
                        <td colspan="6" align="center">
                            <asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                        </td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel ID="pIndSearch" runat="server" Width="950px">

                    <div align="center">
                        <table border="1" runat="server" id="tblIndSearch" style="text-align: center; border-style: solid; border-width: 1px;" width="30%" visible="true">
                            <tr bgcolor="silver">
                                <td colspan="2">
                                    <h3>Search NSF member</h3>
                                </td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                            </tr>
                            <tr>
                                <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                                <td align="left">
                                    <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                            </tr>
                            <%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
                            <tr>
                                <td align="right">
                                    <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                                </td>
                                <td align="left">
                                    <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                                </td>
                            </tr>
                        </table>
                        <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
                    </div>
                    <br />
                    <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                        <b>Search Result</b>
                        <div style="width: 100%; overflow: scroll">
                            <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                                <Columns>
                                    <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                                    <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                                    <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                                    <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                                    <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                                    <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                                    <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                                    <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                                    <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                                    <asp:BoundField DataField="DonorType" HeaderText="DonorType"></asp:BoundField>
                                    <asp:BoundField DataField="SpouseFName" HeaderText="Spouse FirstName"></asp:BoundField>
                                    <asp:BoundField DataField="SpouseLName" HeaderText="Spouse LastName"></asp:BoundField>
                                    <asp:BoundField DataField="SpouseEmail" HeaderText="Spouse Email"></asp:BoundField>
                                    <asp:BoundField DataField="SpouseDonorType" HeaderText="Spouse DonorType"></asp:BoundField>
                                </Columns>
                                <HeaderStyle BackColor="#FFFFCC" />
                                <RowStyle CssClass="SmallFont" HorizontalAlign="Left" />
                            </asp:GridView>
                        </div>
                    </asp:Panel>
                </asp:Panel>
            </td>
        </tr>
        <tr>
            <td align="center">
                <asp:Panel runat="server" ID="panel3" Visible="False">
                    <br />
                    <center>
                        <asp:Label ID="Pnl3Msg" ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>
                    </center>

                    <br />
                    <asp:DataGrid ID="gvExCoaching" runat="server" DataKeyField="ExCoachingID" AutoGenerateColumns="False" OnRowCommand="gvExCoaching_RowCommand" RowStyle-CssClass="SmallFont" HeaderStyle-BackColor="#ffffcc" HeaderStyle-CssClass="ContentSubTitle" HeaderStyle-ForeColor="#008000">
                        <Columns>
                            <asp:ButtonColumn ButtonType="PushButton" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                            <asp:ButtonColumn ButtonType="LinkButton" CommandName="Delete" Text="Delete" HeaderText="Delete" />
                            <asp:BoundColumn DataField="ExCoachingID" HeaderText="ExCoachingID" Visible="false" />

                            <asp:BoundColumn DataField="EventYear" HeaderText="EventYear" />
                            <asp:BoundColumn DataField="EventCode" HeaderText="EventCode" ItemStyle-HorizontalAlign="Left" Visible="false">
                                <ItemStyle HorizontalAlign="Left" />
                            </asp:BoundColumn>
                            <asp:BoundColumn DataField="CMemberID" HeaderText="CMemberID" Visible="false" />

                            <asp:BoundColumn DataField="CoachName" HeaderText="Coach Name" />
                            <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Parent Name">

                                <ItemTemplate>
                                    <asp:Label ID="lblParentID" runat="server" CssClass="lnkParentNameEx" attr-parentID='<%#DataBinder.Eval(Container.DataItem,"ParentID") %>' Style="color: blue; cursor: pointer;" attr-name='<%#DataBinder.Eval(Container, "DataItem.ParentName")%>' attr-email='<%#DataBinder.Eval(Container, "DataItem.ParentEmail")%>' Text='<%#DataBinder.Eval(Container, "DataItem.ParentName")%>'></asp:Label>



                                </ItemTemplate>

                            </asp:TemplateColumn>

                            <asp:BoundColumn DataField="ParentName" HeaderText="Parent Name" Visible="false" />
                            <asp:BoundColumn DataField="ChildNumber" HeaderText="ChildNumber" Visible="false" />
                            <asp:BoundColumn DataField="ChildName" HeaderText="Student Name" />
                            <asp:BoundColumn DataField="email" Visible="false" HeaderText="Email" />
                            <asp:BoundColumn HeaderText="Paid/Adjusted" DataField="Paid" />
                            <asp:BoundColumn DataField="ProductID" HeaderText="ProductID" Visible="false" />
                            <asp:BoundColumn DataField="ProductGroupCode" HeaderText="ProductGroupCode" />
                            <asp:BoundColumn DataField="ProductCode" HeaderText="ProductCode" />
                            <asp:BoundColumn DataField="Semester" HeaderText="Semester" />
                            <asp:BoundColumn DataField="NewDeadline" HeaderText="NewDeadline" DataFormatString="{0:MM/dd/yyyy}" />
                            <asp:BoundColumn DataField="ProductGroupID" HeaderText="ProductGroupID" Visible="false" />


                            <asp:BoundColumn DataField="CreateDate" HeaderText="Date" DataFormatString="{0:MM/dd/yyyy}" />
                            <asp:BoundColumn DataField="CreatedBy" HeaderText="Created By" />
                            <asp:BoundColumn DataField="ModifyDate" HeaderText="Modified Date" DataFormatString="{0:MM/dd/yyyy}" />
                            <asp:BoundColumn DataField="ModifiedBy" HeaderText="Modified By" />
                            <asp:BoundColumn DataField="Type" HeaderText="Type" />
                        </Columns>

                        <HeaderStyle BackColor="#FFFFCC" Font-Bold="True" ForeColor="Green" />

                    </asp:DataGrid>
                </asp:Panel>
            </td>
        </tr>
    </table>

    <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>
    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Parent Information</span>
            </div>

            <div class="ezmodal-content">

                <div class="dvSendEMailContent" style="display; none;">
                    <div align="center"><span class="spnErrMsg" style="color: red;"></span></div>
                    <div style="clear: both; margin-bottom: 5px;"></div>
                    <div>
                        <div class="from">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">From </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txFrom" value="nsfprogramleads@northsouth.org" style="width: 400px;" />
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="To">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">To </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txtTo" style="width: 400px;" />
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="CC">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">CC </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtCC" style="width: 600px; height: 18px;"></textarea>

                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="subjext">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Subject </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtSubject" style="width: 600px; height: 18px;"></textarea>
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="body">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Body </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <div>
                                <asp:TextBox ID="txtPP" runat="server" Style="display: none;"></asp:TextBox>
                                <textarea class="mailBody" style="width: 800px; height: 150px;"></textarea>

                            </div>
                        </div>

                    </div>
                </div>

                <div class="dvgridContent" style="display: none;">
                    <div>
                        <table align="center" class="tblParentInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                        </table>
                    </div>

                    <div>
                        <table align="center" class="tblCoacnInfo" style="border: 1px solid black; border-collapse: collapse; width: 700px;">
                        </table>
                    </div>
                </div>
            </div>



            <div class="ezmodal-footer">
                <button id="Button2" type="button" class="btnSenEmail">SenEmail</button>
                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

</asp:Content>

