<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SVProduct.aspx.vb" Inherits="SVProduct" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    &nbsp; &nbsp; &nbsp;<asp:HyperLink ID="backHyperLink" runat="server" NavigateUrl="~/VolunteerFunctions.aspx"
        Width="93px">Back</asp:HyperLink>
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
    &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;<asp:Label
        ID="Label1" runat="server" Font-Bold="True" Font-Size="Larger" Text="Add / Update Product"></asp:Label>&nbsp; &nbsp; &nbsp; &nbsp;&nbsp;<br />
    <br />
    <asp:DetailsView ID="DetailsView1" runat="server" AutoGenerateRows="False" DataSourceID="ProdObjectDataSource"
        DefaultMode="Insert" Height="50px" Width="344px">
        <Fields>
            <asp:TemplateField HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox1" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    &nbsp;<asp:DropDownList ID="DropDownList1" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource2"
                        DataTextField="ProductGroupCode" DataValueField="ProductGroupCode" SelectedValue='<%# Bind("ProductGroupCode") %>'>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label1" runat="server" Text='<%# Bind("ProductGroupCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="EventCode" SortExpression="EventCode">
                <EditItemTemplate>
                    <asp:TextBox ID="TextBox2" runat="server" Text='<%# Bind("EventCode") %>'></asp:TextBox>
                </EditItemTemplate>
                <InsertItemTemplate>
                    <asp:DropDownList ID="DropDownList2" runat="server" AutoPostBack="True" DataSourceID="SqlDataSource3"
                        DataTextField="EventCode" DataValueField="EventCode" SelectedValue='<%# Bind("EventCode") %>'>
                    </asp:DropDownList>
                </InsertItemTemplate>
                <ItemTemplate>
                    <asp:Label ID="Label2" runat="server" Text='<%# Bind("EventCode") %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
            <asp:CommandField ShowInsertButton="True" />
        </Fields>
    </asp:DetailsView>
    &nbsp;&nbsp;<br />
    <asp:GridView ID="GridView1" runat="server" AllowPaging="True" AutoGenerateColumns="False"
        DataKeyNames="ProductId" DataSourceID="ProdObjectDataSource" Width="415px">
        <Columns>
            <asp:CommandField ShowEditButton="True" />
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode" />
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode" />
            <asp:BoundField DataField="EventCode" HeaderText="EventCode" SortExpression="EventCode" />
            <asp:BoundField DataField="Name" HeaderText="Name" SortExpression="Name" />
            <asp:BoundField DataField="Status" HeaderText="Status" SortExpression="Status" />
        </Columns>
    </asp:GridView>
    <br />
    <asp:ObjectDataSource ID="ProdObjectDataSource" runat="server" InsertMethod="InsertProd"
        OldValuesParameterFormatString="original_{0}" SelectMethod="GetProd" TypeName="ProductDataAccess"
        UpdateMethod="UpdateProd">
        <UpdateParameters>
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="ProductGroupId" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
            <asp:Parameter Name="original_ProductId" Type="Int32" />
            <asp:Parameter Name="ProductId" Type="Int32" />
        </UpdateParameters>
        <InsertParameters>
            <asp:Parameter Name="ProductCode" Type="String" />
            <asp:Parameter Name="ProductGroupId" Type="Int32" />
            <asp:Parameter Name="ProductGroupCode" Type="String" />
            <asp:Parameter Name="EventId" Type="Int32" />
            <asp:Parameter Name="EventCode" Type="String" />
            <asp:Parameter Name="Name" Type="String" />
            <asp:Parameter Name="Status" Type="String" />
            <asp:Parameter Name="CreateDate" Type="DateTime" />
            <asp:Parameter Name="CreatedBy" Type="Int32" />
            <asp:Parameter Name="ModifyDate" Type="DateTime" />
            <asp:Parameter Name="ModifiedBy" Type="Int32" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:SqlDataSource ID="SqlDataSource2" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="SELECT DISTINCT [ProductGroupCode] FROM [ProductGroup]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="SqlDataSource3" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="SELECT DISTINCT [EventCode] FROM [Event]"></asp:SqlDataSource>
    <asp:SqlDataSource ID="ProductSqlDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        ProviderName="<%$ ConnectionStrings:NSFConnectionString.ProviderName %>" SelectCommand="SELECT [ProductCode], [ProductGroupCode], [EventCode], [Name], [Status] FROM [Product]">
    </asp:SqlDataSource>
</asp:Content>



 
 
 