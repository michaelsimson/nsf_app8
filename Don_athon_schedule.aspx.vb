﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.IO

Partial Class Don_athon_schedule
    Inherits System.Web.UI.Page
    Public i As Integer = 0
    'usp_SelectWalkMarathon
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack = True Then
            ' ltlTitle.Text = "North South Foundation - Excel-a-Thon : Noble Cause Through Brilliant Minds!"
            Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            addMenuItemAt(0, "Home", homeURL)
            If (Session("LoggedIn") = "True") Then
                hlinkParentRegistration.Visible = True
                addLogoutMenuItem()
            Else
                If (loginURL <> Nothing And Session("entryToken") <> Nothing) Then
                    loginURL = loginURL + Session("entryToken").ToString().Substring(0, 1)
                    addMenuItem("Login", loginURL)
                End If
            End If
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Insert(0, Convert.ToString(year + 1))
            ddlEventYear.Items.Insert(1, Convert.ToString(year))
            ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            GetEvent()
            GetCluster()
            getDoanteAThon()
        End If
    End Sub

    Public Sub addMenuItem(ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addMenuItem(ByVal itemText As String, ByVal href As String)
        addMenuItem(New MenuItem("&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal itemText As String, ByVal href As String)
        addMenuItemAt(index, New MenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addLogoutMenuItem()
        addMenuItemAt(NavLinks.Items.Count, "Logout", "logout.aspx") 'add to the end
    End Sub
   
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select EventId,Name from Event  where EventId in (5,12,18) order by   EventId")
        ddlEvent.DataSource = ds
        ddlEvent.DataTextField = "Name"
        ddlEvent.DataValueField = "EventId"
        ddlEvent.DataBind()
    End Sub

    Private Sub GetCluster()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select ClusterId,Clustercode from Cluster order by Clustercode")
        ddlCluster.DataSource = ds
        ddlCluster.DataTextField = "Clustercode"
        ddlCluster.DataValueField = "ClusterId"
        ddlCluster.DataBind()
    End Sub
   
    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsValid() Then
            lblErr.Text = "'"
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from DonateAThonCal where EventYear = " & ddlEventYear.SelectedValue & " and  EventID=" & ddlEvent.SelectedValue & " and ClusterID=" & ddlCluster.SelectedValue & " And EndDate >'" & txtStrtDate.Text & "'") = 0 Then
                SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, " Insert into DonateAThonCal(EventID, EventYear, ClusterID, VenueAndTime, Description, StartDate, EventDate, EndDate, CreateDate, CreatedBy) values (" & ddlEvent.SelectedValue & "," & ddlEventYear.SelectedValue & "," & ddlCluster.SelectedValue & ",'" & txtVenue.Text & "','" & txtDescription.Text & "','" & txtStrtDate.Text & "','" & txtEventDate.Text & "','" & txtEndDate.Text & "',GetDate()," & Session("LoginID") & ")")
                getDoanteAThon()
            Else
                lblErr.Text = "Event Already Exist on same dates"
            End If
        Else
            lblErr.Text = "Please fill all input"

        End If
    End Sub

    Public Sub getDoanteAThon()
        Dim ds_WalkaThon As New DataSet
        Try
            ds_WalkaThon = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select D.DonateAThonID, D.EventID, D.EventYear, D.ClusterID, D.VenueAndTime, D.Description, D.StartDate, D.EventDate,D.EndDate,C.ClusterCode,E.Name from  DonateAThonCal D Inner Join Event E On E.EventID=D.EventID Inner Join Cluster C ON C.ClusterID= D.ClusterID")
            DGAthon.DataSource = ds_WalkaThon
            DGAthon.DataBind()
        Catch ex As Exception
        End Try

    End Sub
End Class
