﻿using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

public partial class LiveWebConf : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            if (!IsPostBack)
            {
                listLiveMeetings();
                if (GrdMeeting.Rows.Count > 0)
                {
                    //fillFollowupMeetingGrid();
                }


            }
        }
    }
    public void fillMeetingGrid(string SessionKey)
    {

        string cmdtext = "";
        DataSet ds = new DataSet();
        SessionKey = SessionKey.TrimEnd(',');
        string year = "0";
        year = DateTime.Now.Year.ToString();
        year = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
        if (SessionKey != "")
        {
            cmdtext = "select CS.UserID,CS.PWD,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,VC.Day,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID) inner join VirtualRoomlookUp VL on (VL.Vroom=CS.Vroom) where VC.EventYear=" + year + " and SessionKey in (" + SessionKey + ") ";


            if (Session["RoleID"].ToString() == "88")
            {
                cmdtext += " and VC.MemberID='" + Session["LoginID"].ToString() + "'";

            }
            cmdtext += " order by VC.BeginTime ASC";

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();

                }
            }
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    Console.WriteLine("id: " + m["id"]);
                    SessionKey += m["id"].ToString() + ",";
                }
                fillMeetingGrid(SessionKey);
                //hdnSessionKeys.Value = SessionKey;
                //hdnHostURL.Value = json["start_url"].ToString();
                //hdnJoinMeetingUrl.Value = json["join_url"].ToString();
            }

            // hdnMeetingStatus.Value = "SUCCESS";



        }
        catch
        {
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }
    protected void btnTerminateMeeting_Click(object sender, EventArgs e)
    {
        termianteMeeting();

    }
    public void termianteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            urlParameter += "&id=" + hdnSessionKey.Value + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }
    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Terminate")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductCode = string.Empty;
                string LBeginDate = string.Empty;
                string Year = "";
                string LDay = string.Empty;

                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                string HostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[10].Text;
                hdnHostID.Value = HostID;
                hdnWebExID.Value = WebExID;
                hdnSessionKey.Value = sessionKey;
                hdnPwd.Value = WebExPwd;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[4].Text;
                LBeginDate = GrdMeeting.Rows[selIndex].Cells[13].Text;
                LDay = GrdMeeting.Rows[selIndex].Cells[12].Text;
                Year = GrdMeeting.Rows[selIndex].Cells[1].Text;

                string sEndTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlEndTime") as Label).Text;
                string sBeginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;
                DateTime dt = DateTime.Now;
                DateTime dtEnd = (DateTime.Now);
                DateTime.TryParse(sEndTime, out dtEnd);
                //dtEnd = dtEnd.AddMinutes(5);
                string Day = "";
                string status = "";
                string StartDate = "";
                string BeginTime = "";


                string cmdText = "";
                cmdText = "update WebConfLog set Status='Finished' where SessionKey=" + sessionKey + "";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmKillMeeting();", true);


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid(hdnSessionKeys.Value);
    }




    public void fillFollowupMeetingGrid()
    {
        try
        {
            string cmdtext = "";
            DataSet ds = new DataSet();

            string Year = "0";
            Year = DateTime.Now.Year.ToString();
            Year = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();

            string RoleID = Session["RoleID"].ToString();
            string MemberID = Session["LoginID"].ToString();
            string StartDate = "";
            string TeamLead = "";
            string LeadText = "";
            LeadText = "select TeamLead from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, LeadText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    TeamLead = ds.Tables[0].Rows[0]["TeamLead"].ToString();
                }
            }

            cmdtext = "select CS.UserID,CS.PWD,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Phase,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,CS.UserId,CS.PWD,VC.Day from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join CalSignup CS on(CS.ProductGroupID=VC.ProductGroupID and CS.ProductID=VC.ProductID and CS.EventID=VC.EventId and CS.EventYear=VC.EventYear and CS.Accepted='Y' and CS.MemberID=VC.MemberID)";

            for (int i = 0; i < GrdMeeting.Rows.Count; i++)
            {
                string UserID = GrdMeeting.Rows[i].Cells[17].Text;
                string Pwd = GrdMeeting.Rows[i].Cells[18].Text;

                StartDate = GrdMeeting.Rows[i].Cells[13].Text;

            }

            if ((RoleID == "88" && TeamLead == "") || (RoleID == "88" && TeamLead == "N"))
            {
                cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.MemberID=" + MemberID + " and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ")  order by VC.ProductGroupCode";
            }

            else if (RoleID == "1" || RoleID == "2" || RoleID == "96")
            {
                cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ")  order by VC.ProductGroupCode";
            }
            else if (RoleID == "89" || (RoleID == "88" && TeamLead == "Y"))
            {
                string ProductGroupID = "";
                string ProductID = "";
                string cmdText = "";
                cmdText = "select ProductGroupID,ProductID from Volunteer where RoleID=" + RoleID + " and MemberID=" + MemberID + "";
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        ProductGroupID = ds.Tables[0].Rows[0]["ProductGroupID"].ToString();
                        ProductID = ds.Tables[0].Rows[0]["ProductID"].ToString();
                    }
                }

                cmdtext += " where VC.EventYear=" + Year + " and VC.StartDate='" + StartDate + "' and VC.ProductGroupID=" + ProductGroupID + " and ProductID=" + ProductID + " and VC.SessionKey not in(" + hdnSessionKeys.Value.TrimEnd(',') + ") order by VC.ProductGroupCode,VC.StartDate,VC.BeginTime,VC.EndTime";
            }




            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdFollowUpSessions.DataSource = ds;
                    GrdFollowUpSessions.DataBind();

                }
                else
                {
                    GrdFollowUpSessions.DataSource = ds;
                    GrdFollowUpSessions.DataBind();

                }
            }
        }
        catch
        {
        }

    }
}