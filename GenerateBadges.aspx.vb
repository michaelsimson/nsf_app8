Imports NorthSouth.BAL
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class GenerateBadges
    Inherits System.Web.UI.Page
    Dim strSql As String
    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGenerate.Click
        If ddlBadgeType.SelectedValue = "Participant" Then
            If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
            If ddlPaperSize.SelectedValue = "6" Then
                Server.Transfer("NewShowBadges.aspx")
            Else
                Server.Transfer("ShowBadges.aspx")
            End If
        ElseIf ddlBadgeType.SelectedValue = "Volunteer" Then
            Server.Transfer("ShowVolunteerBadges.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Volunteer" Then
            Server.Transfer("ShowBlankBadges.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Guest" Then
            Server.Transfer("ShowBlankBadges.aspx")
        End If
    End Sub

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim badge As New Badge()
            Dim dsContestDates As New DataSet
            If Not badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID"))) Is Nothing Then
                lblChapter.Text = badge.GetChapterName(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")))
            End If
            lblChapter.Font.Bold = True
            If Convert.ToInt32(Session("SelChapterID")) = 1 Then
                ddlPaperSize.Items(0).Enabled = False
            End If
            dsContestDates = badge.GetContestDates(Application("ConnectionString"), Convert.ToInt32(Session("SelChapterID")), Convert.ToInt32(Now.Year))
            If dsContestDates.Tables(0).Rows.Count > 0 Then
                lstContestDates.DataTextField = "ContestDate"
                lstContestDates.DataValueField = "ContestDate"
                lstContestDates.DataSource = dsContestDates
                lstContestDates.DataBind()
                pnlMessage.Visible = False

            Else
                pnlMessage.Visible = True
                lblMessage.Text = "No Contest Dates found."
                lstContestDates.Visible = False
            End If
        End If
    End Sub
    Public ReadOnly Property ContestDates() As String
        Get
            Return GetSelectedContestDates()
        End Get
    End Property
    Public ReadOnly Property PerPageCount() As Integer
        Get
            Return ddlPaperSize.SelectedValue
        End Get
    End Property
    Public ReadOnly Property PageCount() As Integer
        Get
            Return Convert.ToInt32(txtPageCount.Text)
        End Get
    End Property
    Public ReadOnly Property BlankBadgeType() As String
        Get
            Return ddlBadgeType.SelectedValue
        End Get
    End Property
    Private Function GetSelectedContestDates() As String
        Dim strSelectedContestDates As String
        Dim i As Integer = 0
        For i = 0 To lstContestDates.Items.Count - 1
            If lstContestDates.Items(i).Selected = True Then
                If Len(strSelectedContestDates) > 0 Then
                    strSelectedContestDates = strSelectedContestDates & ",'" & lstContestDates.Items(i).Text & "'"
                Else
                    strSelectedContestDates = "'" & lstContestDates.Items(i).Text & "'"
                End If
            End If
        Next
        Return strSelectedContestDates
    End Function

    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Me.Export = True
        If ddlBadgeType.SelectedValue = "Participant" Then
            If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
            If ddlPaperSize.SelectedValue = "6" Then
                Server.Transfer("NewShowBadges.aspx")
            Else
                Server.Transfer("ShowBadges.aspx")
            End If
        ElseIf ddlBadgeType.SelectedValue = "Volunteer" Then
            Server.Transfer("ShowVolunteerBadges.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Volunteer" Then
            Server.Transfer("ShowBlankBadges.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Guest" Then
            Server.Transfer("ShowBlankBadges.aspx")
        End If
    End Sub
    Protected Sub btnExportPDF_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExportPDF.Click
        Me.Export = True
        If ddlBadgeType.SelectedValue = "Participant" Then
            If Session("SelChapterID") = "1" Then Session("PageRange") = ddlRange.SelectedValue
            If ddlPaperSize.SelectedValue = "6" Then
                Server.Transfer("NewShowBadgesPdf.aspx")
            Else
                Server.Transfer("ShowBadgesPdf.aspx")
            End If
        ElseIf ddlBadgeType.SelectedValue = "Volunteer" Then
            Server.Transfer("ShowVolunteerBadgesPdf.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Volunteer" Then
            Server.Transfer("ShowBlankBadgesPdf.aspx")
        ElseIf ddlBadgeType.SelectedValue = "Blank Guest" Then
            Server.Transfer("ShowBlankBadgesPdf.aspx")
        End If
    End Sub
    Private _export As Boolean
    Public Property Export() As Boolean
        Get
            Return _export
        End Get
        Set(ByVal value As Boolean)
            _export = value
        End Set
    End Property

    Protected Sub ddlBadgeType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBadgeType.SelectedIndexChanged
        trrange.Visible = False
        If ddlBadgeType.SelectedValue = "Participant" Then
            pnlPaperSize.Visible = True
            ddlPaperSize.Enabled = True
            pnlPages.Visible = False

            If Session("SelChapterID") = "1" Then
                Dim Strdates As String = String.Empty
                Dim i As Integer
                'If lstContestDates.Items.Count = 0 Then Exit Sub
                For i = 0 To lstContestDates.Items.Count - 1
                    If Strdates.Length = 0 Then
                        Strdates = "'" & lstContestDates.Items(i).Text & "'"
                    Else
                        Strdates = Strdates & ",'" & lstContestDates.Items(i).Text & "'"
                    End If
                Next
                'Range for participants
                trrange.Visible = True
                ''Dim childcnt As Integer
                ''Try
                ''    childcnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(Now.Year) & "")
                ''Catch ex As Exception
                ''    Response.Write("select count(distinct a.childnumber) from  contestant a, child b, contest c, chapter d,indspouse e where a.badgenumber <>'' and a.childnumber = b.childnumber and a.contestid = c.contestid and d.chapterid = a.chapterid and e.automemberid = a.parentid and c.contestdate in (" & Strdates & ") and a.chapterid =" & Session("SelChapterID") & " and a.contestyear = " & Convert.ToInt32(Now.Year) & "")
                ''    Exit Sub
                ''End Try
                ''Dim Itemcount As Integer
                ''Itemcount = childcnt / 200
                ''If Itemcount * 200 < childcnt Then
                ''    Itemcount = Itemcount + 1
                ''End If
                ''For i = 1 To Itemcount
                ''    Dim li As ListItem = New ListItem()
                ''    li.Text = (((i - 1) * 200) + 1) & "  To  " & (i * 200)
                ''    li.Value = (((i - 1) * 200) + 1) & "_" & (i * 200)
                ''    ddlRange.Items.Insert(i - 1, li)
                ''Next

                'test

                ''Commented donly for testing purpose on 07-04-2013
                ' ''ddlRange.Items.Insert(0, New ListItem("1 to 240", "1_240"))
                ' ''ddlRange.Items.Insert(1, New ListItem("241 to 480", "241_480"))
                ' ''ddlRange.Items.Insert(2, New ListItem("481 to 720", "481_720"))
                ' ''ddlRange.Items.Insert(3, New ListItem("721 to 960", "721_960"))
                ' ''ddlRange.SelectedIndex = 0
                '************************* 07-04-2013 *********************'

                ddlRange.Items.Insert(0, New ListItem("1 to 24", "1_24"))
                ddlRange.Items.Insert(1, New ListItem("25 to 48", "25_48"))
                ddlRange.Items.Insert(2, New ListItem("49 to 72", "49_72"))
                ddlRange.Items.Insert(3, New ListItem("1 to 240", "1_240"))
                ddlRange.Items.Insert(4, New ListItem("241 to 480", "241_480"))
                ddlRange.Items.Insert(5, New ListItem("481 to 720", "481_720"))
                ddlRange.Items.Insert(6, New ListItem("721 to 960", "721_960"))
                ddlRange.Items.Insert(7, New ListItem("961 to 1200", "961_1200"))

                'ddlRange.Items.Insert(3, New ListItem("73 to 96", "73_96"))
                'ddlRange.Items.Insert(4, New ListItem("97 to 120", "97_120"))
                'ddlRange.Items.Insert(5, New ListItem("121 to 144", "121_144"))
                'ddlRange.Items.Insert(6, New ListItem("145 to 168", "145_168"))
                'ddlRange.Items.Insert(7, New ListItem("169 to 192", "169_192"))
                'ddlRange.Items.Insert(8, New ListItem("193 to 216", "193_216"))
                'ddlRange.Items.Insert(9, New ListItem("217 to 240", "217_240"))
                'ddlRange.Items.Insert(10, New ListItem("241 to 264", "241_264"))
                'ddlRange.Items.Insert(11, New ListItem("265 to 288", "265_288"))
                'ddlRange.Items.Insert(12, New ListItem("289 to 312", "289_312"))
                'ddlRange.Items.Insert(13, New ListItem("313 to 336", "313_336"))
                'ddlRange.Items.Insert(14, New ListItem("337 to 360", "337_360"))
                'ddlRange.Items.Insert(15, New ListItem("361 to 384", "361_384"))
                'ddlRange.Items.Insert(16, New ListItem("385 to 408", "385_408"))
                'ddlRange.Items.Insert(17, New ListItem("409 to 432", "409_432"))
                'ddlRange.Items.Insert(18, New ListItem("433 to 456", "433_456"))
                'ddlRange.Items.Insert(19, New ListItem("457 to 480", "457_480"))
                'ddlRange.Items.Insert(20, New ListItem("481 to 504", "481_504"))
                'ddlRange.Items.Insert(21, New ListItem("505 to 528", "505_528"))
                'ddlRange.Items.Insert(22, New ListItem("529 to 552", "529_552"))
                'ddlRange.Items.Insert(23, New ListItem("553 to 576", "553_576"))
                'ddlRange.Items.Insert(24, New ListItem("577 to 600", "577_600"))
                'ddlRange.Items.Insert(25, New ListItem("601 to 624", "601_624"))
                'ddlRange.Items.Insert(26, New ListItem("625 to 648", "625_648"))
                'ddlRange.Items.Insert(27, New ListItem("649 to 672", "649_672"))
                'ddlRange.Items.Insert(28, New ListItem("673 to 696", "673_696"))
                'ddlRange.Items.Insert(29, New ListItem("697 to 720", "697_720"))
                'ddlRange.Items.Insert(30, New ListItem("721 to 744", "721_744"))
                'ddlRange.Items.Insert(31, New ListItem("745 to 768", "745_768"))
                'ddlRange.Items.Insert(32, New ListItem("769 to 792", "769_792"))
                'ddlRange.Items.Insert(33, New ListItem("793 to 816", "793_816"))
                'ddlRange.Items.Insert(34, New ListItem("817 to 840", "817_840"))
                'ddlRange.Items.Insert(35, New ListItem("841 to 864", "841_864"))
                'ddlRange.Items.Insert(36, New ListItem("865 to 888", "865_888"))

                'ddlRange.Items.Insert(0, New ListItem("1 to 60 ", "1_60"))
                'ddlRange.Items.Insert(1, New ListItem("61 to 120 ", "61_120"))
                'ddlRange.Items.Insert(2, New ListItem("121 to 180 ", "121_180"))
                'ddlRange.Items.Insert(3, New ListItem("181 to 240 ", "181_240"))
                'ddlRange.Items.Insert(4, New ListItem("241 to 300 ", "241_300"))
                'ddlRange.Items.Insert(5, New ListItem("301 to 360 ", "301_360"))
                'ddlRange.Items.Insert(6, New ListItem("361 to 420 ", "361_420"))
                'ddlRange.Items.Insert(7, New ListItem("421 to 480 ", "421_480"))
                'ddlRange.Items.Insert(8, New ListItem("481 to 540 ", "481_540"))
                'ddlRange.Items.Insert(9, New ListItem("541 to 600 ", "541_600"))
                'ddlRange.Items.Insert(10, New ListItem("661 to 720 ", "661_720"))
                'ddlRange.Items.Insert(11, New ListItem("721 to 780 ", "721_780"))
                'ddlRange.Items.Insert(12, New ListItem("781 to 840 ", "781_840"))
                'ddlRange.Items.Insert(13, New ListItem("841 to 900 ", "841_900"))

                'ddlRange.Items.Insert(0, New ListItem("1 to 25 ", "1_25"))
                'ddlRange.Items.Insert(1, New ListItem("501 to 525 ", "501_525"))
                'ddlRange.Items.Insert(2, New ListItem("901 to 925 ", "901_925"))
                'ddlRange.SelectedIndex = 0
            End If
        ElseIf ddlBadgeType.SelectedValue = "Volunteer" Then
            pnlPaperSize.Visible = True
            ddlPaperSize.SelectedValue = "6"
            ddlPaperSize.Enabled = False
            pnlPages.Visible = False
        ElseIf ddlBadgeType.SelectedValue = "Blank Volunteer" Then
            pnlPaperSize.Visible = True
            ddlPaperSize.SelectedValue = "6"
            ddlPaperSize.Enabled = False
            pnlPages.Visible = True
        ElseIf ddlBadgeType.SelectedValue = "Blank Guest" Then
            pnlPaperSize.Visible = True
            ddlPaperSize.SelectedValue = "6"
            ddlPaperSize.Enabled = False
            pnlPages.Visible = True
        End If
        If ddlBadgeType.SelectedIndex = 0 Then
            pnlPaperSize.Visible = False
        End If
    End Sub
End Class
