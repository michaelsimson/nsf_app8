﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="IRS1099KDataReconciliation.aspx.cs" Inherits="IRS1099KDataReconciliation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    
       <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>

    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri;
        color: rgb(73, 177, 23);"
        runat="server">
        <strong> 
      IRS 1099-K Data Reconciliation</strong>
             <br />
       
    </div>


    <br />
   
<div runat="server" align="center">
<table  margin-left: 0px;">
    
        <tr>
            
      <td align="left" nowrap="nowrap" style="width: 46px" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Frequency  </b></td>
            
            <td align="left" nowrap="nowrap" >
                 <asp:DropDownList ID="ddlFrequency" runat="server" Width="80px" 
                      AutoPostBack="True" OnSelectedIndexChanged="ddlFrequency_SelectedIndexChanged"  
                   >
                       
                   <asp:ListItem Value="monthly">Monthly</asp:ListItem>
                   <asp:ListItem Value="annual">Annual</asp:ListItem>
                </asp:DropDownList>
              
            </td>
        
            <td align="right" nowrap="nowrap" >&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year</b></td>
            <td style="width: 70px" align="left">
                <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="true">
                </asp:DropDownList>
            </td>
            <td style="width: 85px" align="right">
                <b>Type of Report</b> </td><td>  <asp:DropDownList ID="ddlReportType" runat="server" Width="137px">
                     <asp:ListItem Value="-1">Select Report Type</asp:ListItem>
                   <asp:ListItem Value="CalendarYear">Calendar Year</asp:ListItem>
                   <asp:ListItem Value="FiscalYear">Fiscal Year</asp:ListItem>
                </asp:DropDownList></td><td>
                &nbsp;</td>
          
            
           
        </tr>
    <tr align="center">
         <td colspan="7">
         <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click"  />
            </td>
           </tr>
     </table>
    </div>

    
  
   


     
 <div  align="center">

          <asp:Label ID="lblError" runat="server" ForeColor="red" style="font-weight: 700" Visible="false"></asp:Label></div>
  
    <div id="divTableView" align="center" runat="server">

        
        
        <table>
            <tr>
                <td >
                    <div align="left" runat="server"><asp:Button ID="excel" runat="server"  Text="Export To Excel" 
                onclick="excel_Click" enabled="false" />

                    </div>
                   
                </td>
                <td > <div align="right" id="divShowTrans" runat="server" >

                        <b><asp:Label ID="lblYearTrans" runat="server" Text="Label"></asp:Label></b>&nbsp;&nbsp;&nbsp;<asp:TextBox Enabled="false" ID="txtBoxTransCount" runat="server" Width="63px"></asp:TextBox>

 </div></td>

            </tr>
                <tr>
                <td colspan="2">
<div id="divTableview1" runat="server">
 <table width="100%" border="1" cellpadding="0" cellspacing="0">
           <tr align="center">
               <td colspan="7"><b>
                   <asp:Label ID="lblTableHeader" Font-Size="Medium" runat="server" Height="30px" Text="Label"></asp:Label></b></td>

           </tr>
           <tr style="border-bottom: 0;">
               <td style="width: 15% !important; max-width: 15%; min-width: 15%;"></td>
                <td style="width: 25% !important; max-width: 25%; min-width: 25%;" colspan="2" align="center"><b>VisaMCDisc</b></td>
               <td style="width: 25% !important; max-width: 25%; min-width: 25%;" colspan="2" align="center"><b>AmEx</b></td>
               <td style="width: 35% !important; max-width: 35%; min-width: 35%;" colspan="2" align="center"><b>Total</b></td>
           </tr>
           <tr width="100%"><td colspan="7">
           
        
        <asp:GridView ID="gvIRSReport" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" OnRowDataBound="gvIRSReport_RowDataBound"  Style="table-layout: fixed; border-collapse:collapse; width:100%;">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%# YearFormat(Eval("Month").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                   

                    <ItemStyle Font-Bold="True" BackColor="#FFFFCC" />
                   

                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%# CurFormat(Eval("VMDCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label3" runat="server" Text='<%#CurFormat(Eval("VMDNSFCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="30%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Refunds">

                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#CurFormat(Eval("VMDNSFRefunds").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="30%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label44" runat="server" Text='<%#CurFormat(Eval("AmExCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%#CurFormat(Eval("AmExNSFCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Refunds">
                    <ItemTemplate>
                        <asp:Label ID="Label563" runat="server" Text='<%#CurFormat(Eval("AmExNSFRefunds").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%#CurFormat(Eval("TotalCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%#CurFormat(Eval("TotalNSFCount").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                  <asp:TemplateField HeaderText="NSF Refunds">
                      <ItemTemplate>
                        <asp:Label ID="Label57" runat="server" Text='<%#CurFormat(Eval("TotalNSFRefunds").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                  </asp:TemplateField>
                  <asp:TemplateField HeaderText="Diff">
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%#CurFormat(Eval("Diff").ToString()) %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#FFFFCC" />
        </asp:GridView>

               
           </td>


           </tr>


 </table></div>
                    <div id="divTableview2" runat="server">
                        </br>
                     <table width="100%" border="1" cellpadding="0" cellspacing="0">
           <tr align="center">
               <td colspan="7"><b>
                   <asp:Label ID="lblHeaderTable2" Font-Size="Medium" runat="server" Height="30px" Text="Label"></asp:Label></b></td>

           </tr>
           <tr style="border-bottom: 0;">
               <td style="width: 15% !important; max-width: 15%; min-width: 15%;"></td>
                <td style="width: 25% !important; max-width: 15%; min-width: 15%;" colspan="2" align="center"><b>VisaMCDisc</b></td>
               <td style="width: 25% !important; max-width: 15%; min-width: 15%;" colspan="2" align="center"><b>AmEx</b></td>
               <td style="width: 35% !important; max-width: 15%; min-width: 15%;" colspan="2" align="center"><b>Total</b></td>
           </tr>
           <tr width="100%"><td colspan="7">
           
        
        <asp:GridView ID="gvAnnualReportTrans" runat="server" AutoGenerateColumns="False" EnableModelValidation="True" OnRowDataBound="gvIRSReport_RowDataBound" Style="table-layout: fixed; border-collapse:collapse; width:100%;">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Label ID="Label1" runat="server" Text='<%#Eval("Month") %>'></asp:Label>
                    </ItemTemplate>
                   

                    <ItemStyle Font-Bold="True" BackColor="#FFFFCC" />
                   

                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label2" runat="server" Text='<%#Eval("VMDCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label38" runat="server" Text='<%#Eval("VMDNSFCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="30%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Refunds">
                     <ItemTemplate>
                        <asp:Label ID="Label56" runat="server" Text='<%#Eval("VMDNSFRefunds") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="30%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label4" runat="server" Text='<%#Eval("AmExCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label5" runat="server" Text='<%#Eval("AmExNSFCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Refunds">
                    <ItemTemplate>
                        <asp:Label ID="Label55" runat="server" Text='<%#Eval("AmExNSFRefunds") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="1099-K">
                    <ItemTemplate>
                        <asp:Label ID="Label6" runat="server" Text='<%#Eval("TotalCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="20%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Charges">
                    <ItemTemplate>
                        <asp:Label ID="Label7" runat="server" Text='<%#Eval("TotalNSFCount") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="NSF Refunds">
                      <ItemTemplate>
                        <asp:Label ID="Label59" runat="server" Text='<%#Eval("TotalNSFRefunds") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Diff">
                    <ItemTemplate>
                        <asp:Label ID="Label8" runat="server" Text='<%#Eval("Diff") %>'></asp:Label>
                    </ItemTemplate>
                    <ItemStyle HorizontalAlign="Right" />
                    <%--<HeaderStyle Width="40%" />--%>
                </asp:TemplateField>
            </Columns>
            <HeaderStyle BackColor="#FFFFCC" />
        </asp:GridView>

               
           </td>


           </tr>


 </table>
                         </div>
                </td>
            </tr>
            <tr>
                <td>
                    <b><asp:Label ID="Label8" runat="server" Text="Note: "></asp:Label></b><asp:Label ID="Label9" runat="server" Text="  NSF Data comes from ChargeRec SQL Table.  MS_Trans date is used to aggregate for the given month."></asp:Label>
                       </td>
            </tr>

        </table>

      
        
         
        
      
             
   </div>
   <br />
    
    
  <br />
   
    
          

</asp:Content>

