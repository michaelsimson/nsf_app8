﻿<!--#include file="../uscontests_header.aspx"-->
<script type="text/javascript">
    document.getElementById("dvMathsCoaching").style.display = "block";

</script>
<div class="title02">NSF Pre-Mathcounts Coaching  - Beginner Level</div>

<div align="justify" class="txt01">

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvBeginner">

        <b>Goal: </b>Introduction to basic MATHCOUNTS topics, teach fundamental concepts.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet the following criteria.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Students must be in 4th or 5th  grade.</li>

        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Classwork: </b>New topic with 20 example problems per class.
        <div style="clear: both; margin-bottom: 5px;"></div>
        There will be Midterm Review and Final Review
        <div style="clear: both; margin-bottom: 5px;"></div>
        Coaching material will be based on (but not limited to) the following broad topics:
         <div style="clear: both; margin-bottom: 5px;"></div>
        <ul style="list-style-type: none;">
            <li>(a)	Number theory (b) Algebra (c) Geometry (d) Probability </li>
        </ul>

        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>The topics and lesson plans will be the same for Beginner and Intermediate.  </b>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Note</b>: Please find the sample problems for Beginner Level at below. This is the level of problems students will be working.
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div class="title02">Beginner	</div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div class="title02" style="color:black;">Least Common Multiple and Greatest Common Factor</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <ul style="list-style-type: decimal;">
            <li>Find the largest number which can exactly divide 513, 783 and 1107.</li>
            <li>Find the smallest number exactly divisible by 12, 15, 20, 27.</li>
            <li>The GCF of two numbers is 14 and the LCM is 11592.  If one of the numbers is 504, find the other.</li>
            <li>The product of two numbers is 16184 and their LCM is 952.  Find their GCF</li>
            <li>Find the largest number which can divide 1354, 1866 and 2762 leaving the same remainder of 10.</li>
            <li>Find the largest number which can divide 290, 460 and 552 leaving the remainders of 4, 5, and 6 respectively.</li>
            <li>Find the greatest possible length of a rope which can be used to measure exactly the lengths 7m, 3m 85cm and 12m 95cm.</li>
            <li>Three different containers contain different qualities of mixtures of milk and water whose measurements are 403 kg, 434 kg and 465 kg.  What biggest measure must be there to measure all different qualities an exact number of times?</li>
            <li>Find the least number which when divided by 6, 9, 12, 15 and 18 leaves the same reminder 2 in each case.</li>
            <li>Find the greatest number of four digits which is exactly divisible by each one of the numbers 12, 18, 21 and 28?</li>

            <li>Find the least number of square tiles required to pave the ceiling of a room 15 m 17 cm long and 9 m 2 cm broad.</li>
            <li>Five bells toll at the station together and arrive respectively at intervals of 6, 7, 8, 9, 12 seconds.  </li>

        </ul>
    </div>
    <div style="clear: both; margin-bottom: 20px; border: 1px solid; width: 100%;"></div>


    <div id="dvIntermediate">
        <div class="title02">NSF Pre-Mathcounts Coaching - Intermediate Level</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <b>Goal: </b>Introduction to basic MATHCOUNTS topics, teach fundamental concepts.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet one of the first two criteria and the third one. 
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Students must be in 4th or 5th  grade</li>

        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Classwork: </b>New topic with 20 example problems per class + Additional 10 problems for homework.
        <div style="clear: both; margin-bottom: 5px;"></div>
        There will be Midterm Review and Final Review
         <div style="clear: both; margin-bottom: 5px;"></div>
        <b>The topics and lesson plans will be the same for Beginner and Intermediate.  </b>
        <div style="clear: both; margin-bottom: 5px;"></div>
        It is expected that Intermediate students will receive additional extension problems.  Coaches may want to supplement these with additional material from other sources or developed by them.
         <div style="clear: both; margin-bottom: 5px;"></div>
        Coaching material will be based on (but not limited to) the following broad topics:
         <div style="clear: both; margin-bottom: 5px;"></div>
        <ul style="list-style-type: none;">
            <li>(a)	Number theory (b) Algebra (c) Geometry (d) Probability </li>
        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Note</b>: Please find the sample problems for Intermediate Level in next page. This is the level of problems students will be working.

        
          <div style="clear: both; margin-bottom: 30px;"></div>
        <img src="/public/images/PreMathWarmup.png" />
        <div style="clear: both; margin-bottom: 10px; border: 1px solid Green; width: 100%;"></div>

    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>

    <div id="dvAdvanced">
        <div class="title02">NSF Pre-Mathcounts Coaching - Advanced Level</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <b>Goal: </b>Introduce more topics and then review on all topics from all levels. Primary goal is to help them to be ready for MATHCOUNTS preparation next year.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Recommended Criteria: </b>Should meet the first criteria and one of the last two.
     <div style="clear: both; margin-bottom: 5px;"></div>
        <ul>
            <li>Student must be in 5th grade or above</li>
            <li>Returning student from last year Pre-Mathcounts coaching</li>
            <li>New student (5th grade or above) who can handle the level of complexity of problems in next page. Student must know to solve single variable equations</li>
        </ul>
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>b>Classwork: </b>New topic with 20 example problems per class.
        <div style="clear: both; margin-bottom: 5px;"></div>
        There will be Midterm Review and Final Review
         <div style="clear: both; margin-bottom: 5px;"></div>
        New topics and focus on actual MATHCOUNTS competition problems taken from previous year handbooks, School/Chapter round competition.  
        <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Coaching material will cover more topics such as (a) Simultaneous Equations (b) Permutations and Combinations.</b>
        <div style="clear: both; margin-bottom: 5px;"></div>

        <b>Note</b>: Please find the sample problems for Advanced Level in next page. This is the level of problems students will be working.
         <div style="clear: both; margin-bottom: 5px;"></div>
        <b>Parents</b>: You are the best judge for your child. If your child is not ready to learn the advanced topics that are taught in the level, please do not register them in this level. Coaches will not be teaching the basics as they are expected to know. Secondly and most important, if your child struggles that will severely hurt their self-esteem. Please keep this in mind when you register.

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div class="title02">Advanced	</div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div class="title02" style="color:black;">I.	Solve the following systems of equations using Elimination or Substitution Method</div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <ul style="list-style-type: decimal;">
            <li>3x + 2y = 11
                <br />
                x  -  y =  7
            </li>
            <li>3p + 2q = 6
                <br />
                2p + 5q = -7
            </li>
            <li>3x +  y = 13
                <br />
                2x – 3y = 16
            </li>
            <li>4x – 2y = 16
                <br />
                3x + 4y =  4
            </li>
            <li>2x + 6y = 17
                <br />
                3x – 2y  = 20
            </li>
            <li>The equations 5x + 2y = 48 and 3x + 2y = 32 represent the money collected from school concert tickets sales during two class periods.  If x represents the cost for each adult ticket and y represents the cost for each student ticket, what is the cost for each adult ticket?</li>

            <li>Margie is responsible for buying a week's supply of food and medication for the dogs and cats at a local shelter.  The food and medication for each dog costs twice as much as those supplies for a cat.  She needs to feed 164 cats and 24 dogs.  Her budget is $4240.  How much can Margie spend on each dog for food and medication?</li>

            <li>Two small pitchers and one large pitcher can hold 8 cups of water.  One large pitcher minus one small pitcher constitutes 2 cups of water.  How many cups of  water can each pitcher hold? </li>

            <li>A test has twenty questions worth 100 points.  The test consists of True/False questions worth 3 points each and multiple choice questions worth 11 points each.  How many multiple choice questions are on the test?</li>
            <li>A landscaping company placed two orders with a nursery. The first order was for 13 bushes and 4 trees, and totalled $487. The second order was for 6 bushes and 2 trees, and totalled $232. The bills do not list the per-item price. What were the costs of one bush and of one tree?
 
            </li>


        </ul>


    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div class="title02">Reference Materials used	</div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    Problems are taken from various sources.
    <div style="clear: both; margin-bottom: 10px;"></div>
    <ul style="list-style-type: decimal;">
        <li>Singapore Math</li>
        <li>Math Olympiad</li>
        <li>Continental Math League</li>
        <li>Challenge Math</li>
        <li>MATHCOUNTS</li>
    </ul>
</div>

<!--#include file="../uscontests_footer.aspx"-->
