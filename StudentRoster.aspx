﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="StudentRoster.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="StudentRoster" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Student Roster
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblErrMsg" ForeColor="Red" runat="server"></asp:Label>
    </div>
    <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 50%; background-color: #ffffcc;">
        <tr class="ContentSubTitle" align="center">
            <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:DropDownList ID="ddYear" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                </asp:DropDownList>

            </td>
            <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Semester</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:DropDownList ID="DDlSemester" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="DDlSemester_SelectedIndexChanged">
                </asp:DropDownList>

            </td>
            <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Students</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:DropDownList ID="ddlChild" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged">
                </asp:DropDownList>
                <br />
                <br />
            </td>
            <td align="left" nowrap="nowrap">
                <asp:Button ID="btnSubmit" runat="server" Visible="false" Text="Submit" OnClick="btnSubmit_Click" />
                <asp:Button ID="btnExportToExcel" runat="server" Text="Export" OnClick="btnExportToExcel_Click" />
                <asp:Button ID="btnExportToExcelAll" runat="server" Text="Export All" OnClick="btnExportToExcelAll_Click" />
            </td>

        </tr>
    </table>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="width: 1100px; overflow: scroll;">
        <center><b>Table 1: Student Roster</b></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="grdStudentRoster" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1100px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc">
            <Columns>

                <asp:BoundField DataField="ChildName" HeaderText="Child Name"></asp:BoundField>
                <asp:BoundField DataField="Email" HeaderText="Child Email"></asp:BoundField>
                <asp:BoundField DataField="PwD" HeaderText="Pwd"></asp:BoundField>
                <asp:BoundField DataField="Parent1Name" HeaderText="Parent1Name"></asp:BoundField>
                <asp:BoundField DataField="Parent1Email" HeaderText="Parent1Email"></asp:BoundField>
                <asp:BoundField DataField="Parent1Pwd" HeaderText="Parent1Pwd"></asp:BoundField>
                <asp:BoundField DataField="Parent1Phone" HeaderText="Parent1Phone"></asp:BoundField>
                <asp:BoundField DataField="Parent2Name" HeaderText="Parent2Name"></asp:BoundField>


                <asp:BoundField DataField="Parent2Email" HeaderText="Parent2Email"></asp:BoundField>
                <asp:BoundField DataField="Parent2Pwd" HeaderText="Parent2Pwd"></asp:BoundField>


                <asp:BoundField DataField="Parent2Phone" HeaderText="Parent2Phone"></asp:BoundField>

                <asp:BoundField DataField="CoachName" HeaderText="CoachName"></asp:BoundField>
                <asp:BoundField DataField="CoachEmail" HeaderText="CoachEmail"></asp:BoundField>
                <asp:BoundField DataField="CoachPwd" HeaderText="CoachPwd"></asp:BoundField>
                <asp:BoundField DataField="CoachPhone" HeaderText="CoachPhone"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="CoachHPhone"></asp:BoundField>
                <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"></asp:BoundField>

                <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"></asp:BoundField>
                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                <asp:BoundField DataField="Vroom" HeaderText="Vroom"></asp:BoundField>



            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>
</asp:Content>
