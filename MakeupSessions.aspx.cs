﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;
using System.Collections;
using VRegistration;


public partial class MakeupSessions : System.Web.UI.Page
{
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        lblMakeupSessStatus.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {

            if (!IsPostBack)
            {
                loadyear();
                loadPhase(ddlPhase);
                if (Session["RoleID"] != null)
                {
                    if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96")
                    {
                        btnDowloadFIle.Visible = true;
                    }
                }

                LoadEvent(ddEvent);
                if (Session["RoleId"].ToString() != "88")
                {
                    fillYearFiletr();
                    loadProductGroupFilter();
                    loadProductFilter();

                    loadLevelFilter();
                    loadCoachFilter();
                }
                else
                {
                    dvSearchMakeupSessions.Visible = false;
                }
                fillMeetingGrid();
                // TestCreateTrainingSession();
            }
        }
    }

    private void loadPhase(DropDownList ddlObject)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }

        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddYear.SelectedValue);
    }
    public void loadyear()
    {
        int year = 0;
        year = Convert.ToInt32(DateTime.Now.Year);
        ddYear.Items.Add(new ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2), Convert.ToString(year)));
        for (int i = 0; i < 3; i++)
        {

            ddYear.Items.Add(new ListItem((Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2)), Convert.ToString(year - (i + 1))));


        }
        ddYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString();
    }
    public void fillProductGroup()
    {

        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddEvent.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + " ) and ProductGroupID in(select ProductGroupID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }

        //Cmdtext = "select distinct PG.ProductGroupId,PG.ProductGroupCode from ProductGroup PG inner join CalSignUP CP on CP.ProductGroupID=PG.ProductGroupID where CP.EventID=" + ddEvent.SelectedValue + " and CP.EventYear=" + ddYear.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {

            ddlProductGroup.DataSource = ds;
            ddlProductGroup.DataTextField = "ProductGroupCode";
            ddlProductGroup.DataValueField = "ProductGroupId";
            ddlProductGroup.DataBind();
            ddlProductGroup.Items.Insert(0, new ListItem("Select", "0"));
            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProductGroup.SelectedIndex = 1;
                ddlProductGroup.Enabled = false;
                fillProduct();
            }
            else
            {

                ddlProductGroup.Enabled = true;
            }
        }
    }
    public void fillProduct()
    {
        string Cmdtext = string.Empty;
        DataSet ds = new DataSet();
        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + ddlCoach.SelectedValue + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }
        else
        {
            Cmdtext = "select ProductId,ProductCode from Product where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + ddYear.SelectedValue + ") and ProductID in(select ProductID from CalSignUp where EventYear='" + ddYear.SelectedValue + "' and MemberID=" + Session["LoginID"].ToString() + " and Accepted='Y' and Semester='" + ddlPhase.SelectedValue + "')";
        }


        // Cmdtext = "select ProductId,ProductCode from Product where EventID=" + ddEvent.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + "";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables[0] != null)
        {
            ddlProduct.Enabled = true;
            ddlProduct.DataSource = ds;
            ddlProduct.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "ProductID";
            ddlProduct.DataBind();
            // ddlProduct.Items.Insert(0, new ListItem("Select", "0"));

            if (ds.Tables[0].Rows.Count == 1)
            {
                ddlProduct.SelectedIndex = 0;
                ddlProduct.Enabled = false;
                loadLevel();
                LoadSessionNo();
                //  populateRecClassDate();
                if (Session["RoleID"].ToString() == "88")
                {
                }

            }
            else
            {
                loadLevel();
                LoadSessionNo();
                // populateRecClassDate();
                ddlProduct.Enabled = true;
            }

        }

    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProduct();
        loadLevel();
        LoadSessionNo();
        //fillCoach();
    }

    public void fillCoach()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct ID.AutoMemberID, (ID.FirstName+' '+ ID.LastName) as Name, ID.LastName, ID.FirstName from CalSignUP V inner join IndSpouse ID on (V.MemberID=ID.AutoMemberID)where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.Semester='" + ddlPhase.SelectedItem.Value + "' order by ID.FirstName ASC";


        }
        else
        {
            cmdText = "select distinct I.AutoMemberID, I.FirstName + ' ' + I.LastName as Name, I.firstname as firstname,I.lastname as lastname from  indspouse I  Where I.AutoMemberID=" + Session["LoginID"].ToString() + " order by I. FirstName";
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {

            ddlCoach.DataSource = ds;
            ddlCoach.DataTextField = "Name";
            ddlCoach.DataValueField = "AutoMemberID";
            ddlCoach.DataBind();
            ddlCoach.Items.Insert(0, new ListItem("Select", "0"));
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
            {
                ddlCoach.Enabled = true;
            }
            else
            {
                ddlCoach.SelectedValue = Session["LoginID"].ToString();
                ddlCoach.Enabled = false;
            }

        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        //populateRecClassDate();
        loadLevel();
    }

    public void loadLevel()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "' and Semester='" + ddlPhase.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.Level from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            ddlLevel.DataTextField = "Level";
            ddlLevel.DataValueField = "Level";
            ddlLevel.DataSource = ds;
            ddlLevel.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlLevel.Enabled = true;
            }
            else
            {
                ddlLevel.Enabled = false;
            }
        }

    }
    public void LoadEvent(DropDownList ddlObject)
    {
        string Strsql = ("Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  W" +
        "here EF.EventYear ="
                    + (ddYear.SelectedValue + "  and E.EventId in(13,19)"));
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Strsql);
        if ((ds.Tables[0].Rows.Count > 0))
        {
            ddlObject.DataSource = ds;
            ddlObject.DataTextField = "EventCode";
            ddlObject.DataValueField = "EventId";
            ddlObject.DataBind();
            if ((ds.Tables[0].Rows.Count > 0))
            {
                ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));

                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"));
                ddlObject.Enabled = false;

                ddchapter.SelectedValue = "112";
                if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
                {

                    fillCoach();
                }
                else
                {
                    fillCoach();
                    fillProductGroup();
                }
            }
            else
            {
                ddlObject.Enabled = false;
                fillProductGroup();
            }

        }
        else
        {

            lblerr.Text = "No Events present for the selected year";
        }
    }
    protected void ddYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadEvent(ddEvent);

    }
    protected void btnCreateMeeting_Click(object sender, EventArgs e)
    {
        if (validatemeeting() == "1")
        {
            if (btnCreateMeeting.Text == "Create Makeup Session")
            {
                if (CheckWeekNoExists() > 0)
                {
                    if (VerifyRegularClassCancelled() > 0)
                    {
                        if (CheckDuplicateMakeupClass() > 0)
                        {
                            if (ValdiateMakeupCLassDateTime() > 0)
                            {
                                timeOverLapValdiation();
                            }
                            else
                            {
                                if (ValdiateMakeupCLassDateTime() == -1)
                                {
                                    lblerr.Text = "Makeup class date should be greater than or equal to current date and Makeup class time should be greater than current time.";
                                }
                                else if (ValdiateMakeupCLassDateTime() == -2)
                                {
                                    lblerr.Text = "Makeup class time and Regular class time should not be same.";
                                }
                                else if (ValdiateMakeupCLassDateTime() == -3)
                                {
                                    lblerr.Text = "Makeup class date should be greater than or equal to current date and Makeup class time should be greater than current time.";
                                }
                            }
                        }
                        else
                        {
                            lblerr.Text = "There cannot be two makeups for the same week.";
                        }
                    }
                    else
                    {
                        lblerr.Text = "Makeup class can only be created if the regular class with the regular coach is cancelled or the subtitute class is cancelled.";
                    }
                }
                else
                {
                    lblerr.Text = "Regular class not yet added for the week# " + TxtWeekNo.Text + "";
                }

            }
            else
            {
                if (ValdiateMakeupCLassDateTime() > 0)
                {
                    deleteMeeting();
                    timeOverLapValdiation();

                }
                else
                {
                    if (ValdiateMakeupCLassDateTime() == -1)
                    {
                        lblerr.Text = "There cannot be two makeups for the same week.";
                    }
                    else if (ValdiateMakeupCLassDateTime() == -2)
                    {
                        lblerr.Text = "There cannot be two makeups for the same week.";
                    }
                }
            }
        }
    }
    protected void btnCancelMeeting_Click(object sender, EventArgs e)
    {
        clear();
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        fillMeetingGrid();
        btnAddNewMeeting.Visible = false;
        tblAddNewMeeting.Visible = true;
    }
    protected void btnAddNewMeeting_Click(object sender, EventArgs e)
    {
        tblAddNewMeeting.Visible = true;
    }


    public void fillMeetingGrid()
    {
        btnAddNewMeeting.Visible = false;
        string cmdtext = "";
        DataSet ds = new DataSet();
        spnTable1Title.Visible = true;
        GrdMeeting.Visible = true;

        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,vl.vroom,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SignupID,vl.hostid, cl.WeekNo from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (VC.MemberID=cs.MemberID and VC.ProductGroupId=cs.ProductGroupId and cs.ProductId=Vc.ProductId and cs.Level=vc.Level and cs.SessionNo=vc.Session and cs.Semester=vc.Semester and cs.Accepted='Y' and Cs.eventyear=vc.eventyear) inner join virtualroomlookup vl on (vl.userid=vc.userid) left join CoachClassCal CL on (cl.EventYear=Vc.EventYear and Cl.ProductGroupId=vc.ProductGroupId and cl.ProductId=vc.ProductId and cl.Level=vc.level and cl.SessionNo=vc.Session and cl.Semester=vc.Semester and cl.memberId=vc.memberId and cl.ClassType='Makeup' and cl.Status='On') where VC.EventID=13 and VC.SessionType='Scheduled Meeting' and cl.Status='On'";

        if (Session["RoleId"].ToString() != "88")
        {

            if (ddlEventyearFilter.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.EventYear=" + ddlEventyearFilter.SelectedValue + "";
            }

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                cmdtext = cmdtext + "  and VC.ProductGroupID='" + ddlProductGroupFilter.SelectedValue + "'";
            }

            if ((ddlProductFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.ProductID='" + ddlProductFilter.SelectedValue + "'";
            }

            if ((ddlLevelFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Level='" + ddlLevelFilter.SelectedValue + "'";
            }

            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            if ((ddlCoachFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.memberID='" + ddlCoachFilter.SelectedValue + "'";
            }

            if ((ddlSessionFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Session='" + ddlSessionFilter.SelectedValue + "'";
            }

            if ((ddlDayFilter.SelectedValue != "0"))
            {
                cmdtext = cmdtext + "  and VC.Day='" + ddlDayFilter.SelectedValue + "'";
            }
        }
        else
        {
            cmdtext = cmdtext + " and VC.MemberId=" + ddlCoach.SelectedItem.Value + ""; 
            if (ddYear.SelectedValue!="0")
            {
                cmdtext = cmdtext + " and VC.EventYear=" + ddYear.SelectedValue + "";
            }
            if (ddlPhase.SelectedValue!="0")
            {
                cmdtext = cmdtext + " and VC.Semester='" + ddlPhase.SelectedValue + "'";
            }
            if (ddlProductGroup.SelectedValue!="0")
            {
                cmdtext = cmdtext + " and VC.ProductGroupId=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.ProductId=" + ddlProduct.SelectedValue + "";
            }

            if (ddlLevel.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.Level='" + ddlLevel.SelectedValue + "'";
            }

            if (ddlSession.SelectedValue != "0")
            {
                cmdtext = cmdtext + " and VC.Session=" + ddlSession.SelectedValue + "";
            }
        }
        cmdtext += "order by Coach";


        try
        {

            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    for (int i = 0; i < GrdMeeting.Rows.Count; i++)
                    {
                        if (GrdMeeting.Rows[i].Cells[21].Text == "Cancelled")
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }
                        if (Session["RoleID"].ToString() == "1")
                        {
                            if (GrdMeeting.Rows[i].Cells[21].Text != "Cancelled")
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                            }
                            else
                            {
                                ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = false;
                                ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = false;
                            }
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnModifyMeeting") as Button).Enabled = true;
                            ((Button)GrdMeeting.Rows[i].FindControl("btnCancelMeeting") as Button).Enabled = true;
                        }

                        // enable/disable join meeting button

                        string startDate = GrdMeeting.Rows[i].Cells[15].Text;
                        DateTime dtStartDate = Convert.ToDateTime(startDate);
                        string currentDate = DateTime.Now.ToShortDateString();
                        DateTime dtCurrentDate = Convert.ToDateTime(currentDate);
                        if (dtStartDate >= dtCurrentDate)
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = true;
                        }
                        else
                        {
                            ((Button)GrdMeeting.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = false;
                        }


                    }
                }
                else
                {
                    GrdMeeting.DataSource = ds;
                    GrdMeeting.DataBind();
                    lblMakeupSessStatus.Text = "No record exists.";
                }
            }
            else
            {
                GrdMeeting.DataSource = ds;
                GrdMeeting.DataBind();
                lblMakeupSessStatus.Text = "No record exists.";
            }
        }
        catch (Exception ex)
        {
        }
    }


    public string validatemeeting()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Year";
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "-1" || ddEvent.SelectedValue == "")
        {
            lblerr.Text = "Please select Event";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "0" || ddlCoach.SelectedValue == "")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (ddlProductGroup.SelectedValue == "0" || ddlProductGroup.SelectedValue == "")
        {
            lblerr.Text = "Please select Product Group";
            retVal = "-1";
        }
        else if (ddlProduct.SelectedValue == "0" || ddlProduct.SelectedValue == "")
        {
            lblerr.Text = "Please select Product";
            retVal = "-1";
        }


        else if (ddlPhase.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Semester";
            retVal = "-1";
        }
        else if (ddlLevel.SelectedValue == "-1" || ddlLevel.SelectedValue == "")
        {
            lblerr.Text = "Please select Level";
            retVal = "-1";
        }

        else if (ddlCoach.SelectedValue == "-1")
        {
            lblerr.Text = "Please select Coach";
            retVal = "-1";
        }
        else if (TxtWeekNo.Text == "")
        {
            lblerr.Text = "Please enter regular class week#";
            retVal = "-1";
        }
        else if (Convert.ToInt32(TxtWeekNo.Text) <= 0)
        {
            lblerr.Text = "Week# value should be greater than 0.";
            retVal = "-1";
        }
        else if (txtDate.Text == "")
        {
            lblerr.Text = "Please fill Date";
            retVal = "-1";
        }

        else if (ddlMakeupTime.SelectedValue == "")
        {
            lblerr.Text = "Please fill Time";
            retVal = "-1";
        }


        return retVal;
    }








    protected void GrdStudentsList_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Register")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                string ChidName = GrdStudentsList.Rows[selIndex].Cells[2].Text;
                string Email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                string City = GrdStudentsList.Rows[selIndex].Cells[6].Text;
                string Country = GrdStudentsList.Rows[selIndex].Cells[7].Text;
                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;
                string Level = GrdStudentsList.Rows[selIndex].Cells[11].Text;
                HdnLevel.Value = Level.Trim();
                if (Email.IndexOf(';') > 0)
                {
                    Email = Email.Substring(0, Email.IndexOf(';'));
                }
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                if (hdnMeetingStatus.Value == "SUCCESS")
                {



                    string CmdText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "', MakeUpRegID=" + hdsnRegistrationKey.Value + ", makeUpAttendeeID=" + hdnMeetingAttendeeID.Value + " where EventYear=" + ddYear.SelectedValue + " and CMemberID=" + MemberID + " and ChildNumber=" + ChildNumber + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and Approved='Y' and Level='" + Level + "'";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblSuccess.Text = "Selected child registered successfully for the selected meeting " + hdnSessionKey.Value + "";
                    fillStudentList(MemberID, ddlProductGroup.SelectedItem.Text, ddlProduct.SelectedItem.Text);
                }

            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;

                string ChildNumber = string.Empty;
                string MemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string PMemberID = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblPMemberID") as Label).Text;
                ChildNumber = ((Label)GrdStudentsList.Rows[selIndex].FindControl("lblChildNumber") as Label).Text;

                string LoginSessionID = string.Empty;
                string LoginSessionChildID = string.Empty;
                hdnChildMeetingURL.Value = ((Label)GrdStudentsList.Rows[selIndex].FindControl("LblChildURL") as Label).Text;
                DataSet ds = new DataSet();
                string email = GrdStudentsList.Rows[selIndex].Cells[5].Text;
                int countEmail = 0;
                string ProductCode = string.Empty;
                ProductCode = GrdStudentsList.Rows[selIndex].Cells[10].Text;
                string CmdText = "select count(*) as Countset from child where onlineClassEmail='" + email.Trim() + "'";
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

                if (null != ds && ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        countEmail = Convert.ToInt32(ds.Tables[0].Rows[0]["Countset"].ToString());
                    }
                }
                //if (Session["LoginID"] != null)
                //{
                //    LoginSessionID = Session["LoginID"].ToString();
                //}
                //if (Session["StudentID"] != null)
                //{
                //    LoginSessionChildID = Session["StudentID"].ToString();
                //}
                //if (LoginSessionID == PMemberID)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                //else if (LoginSessionChildID == ChildNumber)
                //{
                //    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "joinChildMeeting();", true);
                //}
                if (countEmail > 0)
                {

                }
                else
                {
                    lblerr.Text = "Only authorized child can join ito the training session.";
                }
            }
            else if (e.CommandName == "Join")
            {

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdStudentsList.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string meetingLink = ((LinkButton)GrdStudentsList.Rows[selIndex].FindControl("HlAttendeeMeetURL") as LinkButton).Text;
                hdnZoomURL.Value = meetingLink;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                hdnStartTime.Value = beginTime;

                string coachname = ((Label)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as Label).Text;

                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                hdnDay.Value = day;


                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {

                    //   logerrors(coachname, "Success", "");
                    CoachingExceptionLog.createExceptionLog(coachname, "Success", "", "Makeup Sessions");
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                }
                else
                {
                    //   logerrors(coachname, "Failure", "");
                    CoachingExceptionLog.createExceptionLog(coachname, "Success", "", "Makeup Sessions");
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert()", true);
                }


            }
        }
        catch (Exception ex)
        {
        }
    }

    protected void GrdMeeting_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);


                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                string ProductID = string.Empty;
                string ProductGroupID = string.Empty;
                string EventID = string.Empty;
                string ChapterID = string.Empty;
                string TimeZoneID = string.Empty;
                string MemberID = string.Empty;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string WeekNo = string.Empty;
                string signupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSignupID") as Label).Text;
                hdnSignupID.Value = signupID;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                EventID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblEventID") as Label).Text;
                ChapterID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
                TimeZoneID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblTimeZoneID") as Label).Text;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                WeekNo = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWeekNo") as Label).Text;
                TxtWeekNo.Text = WeekNo;
                TxtWeekNo.Enabled = false;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnSessionKey.Value = sessionKey;
                hdnHostID.Value = hostID;

                ddEvent.SelectedValue = EventID;
                ddchapter.SelectedValue = ChapterID;
                fillCoach();
                ddlCoach.SelectedValue = MemberID;
                ddlCoach.Enabled = false;
                fillProductGroup();
                ddlProductGroup.SelectedValue = ProductGroupID;
                fillProduct();

                ddlProduct.SelectedValue = ProductID;
                ddlTimeZone.SelectedValue = TimeZoneID;


                ddYear.SelectedValue = GrdMeeting.Rows[selIndex].Cells[3].Text;
                ddlPhase.SelectedValue = GrdMeeting.Rows[selIndex].Cells[8].Text;
                loadLevel();
                ddlLevel.SelectedValue = GrdMeeting.Rows[selIndex].Cells[9].Text;
                LoadSessionNo();
                ddlSession.SelectedValue = GrdMeeting.Rows[selIndex].Cells[10].Text;

                txtDate.Text = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartDate") as Label).Text;


                hdnDuration.Value = GrdMeeting.Rows[selIndex].Cells[19].Text;

                ddlMakeupTime.SelectedValue = ((Label)GrdMeeting.Rows[selIndex].FindControl("hlTime") as Label).Text;

                sessionKey = GrdMeeting.Rows[selIndex].Cells[11].Text;
                hdnSessionKey.Value = sessionKey;
                tblAddNewMeeting.Visible = true;

                btnCreateMeeting.Text = "Update Makeup Session";
                hdnSessionKey.Value = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;




                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

            }
            else if (e.CommandName == "Select")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();

                //fillStudentList(MemberID);
            }
            else if (e.CommandName == "SelectLink")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnSessionNo.Value = GrdMeeting.Rows[selIndex].Cells[10].Text;

                fillStudentList(MemberID, ProductGroupCode, ProductCode);
            }
            else if (e.CommandName == "SelectMeetingURL")
            {
                row = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                //string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                //hdnWebExMeetURL.Value = MeetingURl;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;
                string sessionKey = string.Empty;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;

                if (hdnMeetingStatus.Value == "SUCCESS")
                {
                    string MeetingURl = hdnHostURL.Value;
                    string URL = MeetingURl.Replace("&amp;", "&");
                    hdnWebExMeetURL.Value = URL;
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", true);
                }
                else
                {
                    //lblerr.Text = hdnMeetingStatus.Value;
                }
            }
            else if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                string WebExID = string.Empty;
                string WebExPwd = string.Empty;

                string MemberID = string.Empty;
                string ProductCode = string.Empty;
                string ProductGroupCode = string.Empty;
                string ProductGroupID = string.Empty;
                string ProductID = string.Empty;
                string MeetingPwd = GrdMeeting.Rows[selIndex].Cells[13].Text;
                hdnMeetingPwd.Value = MeetingPwd;
                sessionKey = GrdMeeting.Rows[selIndex].Cells[12].Text;
                hdnSessionKey.Value = sessionKey;
                WebExID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExID") as Label).Text;
                WebExPwd = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWebExPwd") as Label).Text;
                hdnWebExID.Value = WebExID;
                hdnWebExPwd.Value = WebExPwd;
                MemberID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
                hdnCoachID.Value = MemberID;
                ProductGroupCode = GrdMeeting.Rows[selIndex].Cells[6].Text;
                ProductCode = GrdMeeting.Rows[selIndex].Cells[7].Text;
                ProductGroupID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductGroupID") as Label).Text;
                ProductID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblProductID") as Label).Text;
                hdnProductID.Value = ProductID;
                hdnProductGroupID.Value = ProductGroupID;
                string Level = GrdMeeting.Rows[selIndex].Cells[9].Text;
                HdnLevel.Value = Level.Trim();
                hdnSessionNo.Value = GrdMeeting.Rows[selIndex].Cells[10].Text;
                hdnDate.Value = GrdMeeting.Rows[selIndex].Cells[15].Text.Trim();

                string sessionStartDate = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStartDate") as Label).Text;
                hdnStartDate.Value = sessionStartDate;

                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblHostID") as Label).Text;
                hdnHostID.Value = hostID;
                string WeekNo = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblWeekNo") as Label).Text;
                hdnWeekNo.Value = WeekNo;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "deleteMeeting();", true);
            }

            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdMeeting.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string coachName = ((LinkButton)GrdMeeting.Rows[selIndex].FindControl("lnkCoach") as LinkButton).Text;
                string hostID = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                string beginTime = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblBegTime") as Label).Text;
                string day = ((Label)GrdMeeting.Rows[selIndex].FindControl("lblMeetDay") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now.AddMinutes(60);
                double mins = 40.0;
                string duration = "-" + ((Label)GrdMeeting.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                int iduration = 0;
                try
                {
                    iduration = Convert.ToInt32(duration);
                }
                catch
                {
                    iduration = 120;
                }
                if (duration == "-")
                {
                    iduration = 120;
                }
                else
                {
                    iduration = Convert.ToInt32(duration);
                }

                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    if (mins < iduration)
                    {
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                    }
                    else
                    {
                        hdnHostID.Value = hostID;
                        try
                        {
                            listLiveMeetings();
                        }
                        catch
                        {
                        }
                        getmeetingIfo(sessionKey, hostID);
                        string cmdText = "";
                        cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                        string meetingLink = hdnHostURL.Value;

                        try
                        {
                            // logerrors(coachName, "Success", "");
                            CoachingExceptionLog.createExceptionLog(coachName, "Success", "", "Makeup Sessions");
                        }
                        catch
                        {
                        }

                        hdnZoomURL.Value = meetingLink;
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                    }

                }
                else
                {
                    try
                    {
                        CoachingExceptionLog.createExceptionLog(coachName, "Failed", "", "Makeup Sessions");
                        // logerrors(coachName, "Failed", "");
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                    }
                    catch
                    {
                    }

                }
            }
        }
        catch (Exception ex)
        {
            //  logerrors("", "Exception", ex.Message);
            CoachingExceptionLog.createExceptionLog("", "Exception", "", "Makeup Sessions");
        }
    }
    protected void GrdMeeting_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdMeeting.PageIndex = e.NewPageIndex;
        fillMeetingGrid();
    }

    protected void GrdMeeting_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
    {
        GrdMeeting.EditIndex = e.RowIndex;
        int rowIndex = e.RowIndex;
        GrdMeeting.EditIndex = -1;
        string sessionKey = string.Empty;
        sessionKey = GrdMeeting.Rows[rowIndex].Cells[12].Text;
        string WebExID = string.Empty;
        string WebExPwd = string.Empty;
        WebExID = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExID") as Label).Text;
        WebExPwd = ((Label)GrdMeeting.Rows[rowIndex].FindControl("lblWebExPwd") as Label).Text;
        hdnSessionKey.Value = sessionKey;
        hdnWebExID.Value = WebExID;
        hdnWebExPwd.Value = WebExPwd;
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmmeetingCancel();", true);


    }


    public void fillStudentList(string MemberID, string ProductGroupCode, string ProductCode)
    {

        trChildList.Visible = true;
        DataSet ds = new DataSet();
        string CmdText = "";
        CmdText = "select C1.ChildNumber,CS.Time,CS.Day,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.AttendeeID,CR.RegisteredID,CR.CMemberID,CR.PMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail,CR.ProductGroupCode,CR.ProductCode,CR.Level from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber) inner join CalSignup CS on (CS.MemberID=CR.CmemberID and CS.ProductGroupID=CR.productGroupID and CS.ProductID=CR.ProductID and CS.Level=CR.Level and CS.SessionNo=CR.SessionNo and CS.EventYear=CR.EventYear and CS.Semester=CR.Semester) where CR.Level='" + HdnLevel.Value + "' and CR.Semester='" + ddlPhase.SelectedValue + "' and CR.EventYear='" + ddYear.SelectedValue + "' and CR.ProductGroupCode='" + ProductGroupCode + "' and CR.ProductCode='" + ProductCode + "' and CR.CMemberID=" + MemberID + " and  CR.EventID=" + ddEvent.SelectedValue + " and CR.SessionNo='" + ddlSession.SelectedValue + "' and Approved='Y' order by C1.Last_Name, C1.First_Name Asc";




        ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "";
                btnClosetable2.Visible = true;

                for (int i = 0; i < GrdStudentsList.Rows.Count; i++)
                {
                    if (((LinkButton)GrdStudentsList.Rows[i].FindControl("HlAttendeeMeetURL") as LinkButton).Text != "")
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = false;
                    }
                    else
                    {
                        ((Button)GrdStudentsList.Rows[i].FindControl("btnSelect") as Button).Enabled = true;
                    }
                }
            }
            else
            {
                GrdStudentsList.DataSource = ds;
                GrdStudentsList.DataBind();
                lblChildMsg.Text = "No record found";
                btnClosetable2.Visible = false;

            }
        }
    }





    protected void btnClosetable2_Click(object sender, EventArgs e)
    {
        trChildList.Visible = false;
    }

    private TimeSpan GetTimeFromString(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue).AddMinutes(addMinute);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    private TimeSpan GetTimeFromStringSubtract(string timeString, int addMinute)
    {
        DateTime dateWithTime = (DateTime.MinValue);
        DateTime.TryParse(timeString, out dateWithTime);
        return dateWithTime.AddMinutes(addMinute).TimeOfDay;
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {

    }

    protected void ddlCoach_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillProductGroup();
    }

    public void LoadSessionNo()
    {
        string cmdText = "";

        if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "89")
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + ddlCoach.SelectedValue + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        else
        {
            cmdText = "select distinct V.SessionNo from CalSignUP V where V.EventYear=" + ddYear.SelectedValue + " and V.EventID=" + ddEvent.SelectedValue + " and V.Accepted='Y' and V.MemberID='" + Session["LoginID"].ToString() + "'";

            if (ddlProductGroup.SelectedValue != "0" && ddlProductGroup.SelectedValue != "")
            {
                cmdText += " and V.ProductGroupID=" + ddlProductGroup.SelectedValue + "";
            }

            if (ddlProduct.SelectedValue != "0" && ddlProduct.SelectedValue != "")
            {
                cmdText += " and V.ProductID=" + ddlProduct.SelectedValue + "";
            }
        }
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (null != ds && ds.Tables.Count > 0)
        {

            ddlSession.DataTextField = "SessionNo";
            ddlSession.DataValueField = "SessionNo";
            ddlSession.DataSource = ds;
            ddlSession.DataBind();
            if (ds.Tables[0].Rows.Count > 1)
            {
                ddlSession.Enabled = true;
            }
            else
            {
                ddlSession.Enabled = false;
            }
        }

    }

    public void createZoomMeeting(string mode)
    {
        try
        {


            string URL = string.Empty;

            string service = "1";
            if (mode == "1")
            {
                URL = "https://api.zoom.us/v1/meeting/create";
            }
            else if (mode == "2")
            {
                URL = "https://api.zoom.us/v1/meeting/update";
            }
            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = txtDate.Text;
            string strMonth = date.Substring(0, date.IndexOf('-'));
            string strDay = date.Substring(date.IndexOf('-') + 1, 2);
            string year = date.Substring(date.LastIndexOf('-') + 1, 4);
            date = year + "-" + strMonth + "-" + strDay;

            string time = ddlMakeupTime.SelectedValue;
            string dateTime = date + "T" + time + "Z";
            string topic = ddlCoach.SelectedItem.Text + "-" + ddlProduct.SelectedItem.Text + "-" + ddlSession.SelectedValue;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            if (DateTime.TryParse(time, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;
                dtTo = dtFromS.AddHours(4);
            }
            string timeFormat = dtTo.Hour + ":" + dtTo.Minute + ":" + dtTo.Second;

            string MakeupDateTime = date + " " + time;
            DateTime dtMakeupDate = Convert.ToDateTime(MakeupDateTime).AddHours(4);

            string StrMakeupDate = dtMakeupDate.ToString("yyyy-MM-dd");
            string StrMakeupTime = dtMakeupDate.ToString("HH:mm:ss");

            string StrMakeupDateTime = StrMakeupDate + "T" + StrMakeupTime + "Z";

            dateTime = date + "T" + timeFormat + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            if (mode == "1")
            {
                urlParameter += "&topic=" + topic + "";
               // urlParameter += "&password=training";
                urlParameter += "&type=2";
                urlParameter += "&start_time=" + StrMakeupDateTime + "";
                urlParameter += "&duration=" + hdnDuration.Value + "";
                urlParameter += "&timezone=America/Chicago";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }
            else if (mode == "2")
            {
                service = "2";
                urlParameter += "&id=" + hdnSessionKey.Value + "";
                urlParameter += "&start_time=" + StrMakeupDateTime + "";
                urlParameter += "&duration=" + hdnDuration.Value + "";
                urlParameter += "&timezone=America/Chicago";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
            }


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch
        {
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnHostURL.Value = json["start_url"].ToString();

                hdnMeetingUrl.Value = json["join_url"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "2")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = json["id"].ToString();
                hdnMeetingStatus.Value = "SUCCESS";
            }
            else if (serviceType == "3")
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnSessionKey.Value + "");

                string cmdText = string.Empty;

                string strQuery;
                SqlCommand cmd;
                strQuery = "update CalSignUp set status=null, MakeUpURL=null,MakeupMeetKey=null,MakeUpPwd=null,makeUpDate=null,MakeUpTime=null, MakeUpDuration=null where MakeupMeetKey=@MeetingKey";
                cmd = new SqlCommand(strQuery);
                cmd.Parameters.AddWithValue("@MeetingKey", hdnSessionKey.Value);

                InsertUpdateData(cmd);

                //cmdText = "update CalSignUp set status=null, MakeUpURL=null,MakeupMeetKey=null,MakeUpPwd=null,makeUpDate=null,MakeUpTime=null, MakeUpDuration=null where MakeupMeetKey='" + hdnSessionKey.Value + "'";


                //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);



                cmdText = "update CoachReg set status=null, MakeUpURL=null,MakeUpAttendeeID=null,MakeUpRegID=null,MakeUpDate=null,MakeUpTime=null,MakeUpDuration=null where CMemberID='" + hdnCoachID.Value + "' and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and Approved='Y' and SessionNo=" + hdnSessionNo.Value + "";

                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                string cmdCoachRegText = string.Empty;

                DateTime dtClassDate = Convert.ToDateTime(hdnStartDate.Value);
                DateTime dtToday = DateTime.Now;
                if (dtToday < dtClassDate)
                {
                    cmdCoachRegText = "Update CoachClassCal set Status='Cancelled' where MemberID=" + hdnCoachID.Value + " and ProductGroupID=" + hdnProductGroupID.Value + " and ProductID=" + hdnProductID.Value + " and EventYear=" + ddYear.SelectedValue + " and Level='" + HdnLevel.Value + "' and SessionNo=" + hdnSessionNo.Value + " and [Date]='" + hdnStartDate.Value + "'";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdCoachRegText);
                }

                ResettingWeekNo("decrement");

                // WeekNoReset("", "decrement");

                fillMeetingGrid();
                lblSuccess.Text = "Makeup Session deleted successfully";
            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

            }

            if (serviceType == "6")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                JToken token = JToken.Parse(postResponse);
                JArray men = (JArray)token.SelectToken("meetings");
                string SessionKey = string.Empty;
                foreach (JToken m in men)
                {
                    if (m["host_id"].ToString() == hdnHostID.Value)
                    {
                        termianteMeeting(m["id"].ToString(), hdnHostID.Value);
                    }

                }

            }
            if (serviceType == "7")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);

                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            }

            //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + "," + serviceType + ");", true);


        }
        catch
        {
            hdnMeetingStatus.Value = "Failure";
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public void makeZoomSession(string StartDate, string Time, string SignupID, string CoachID, string Day, string EndTime, int duration)
    {
        hdnDuration.Value = duration.ToString();
        createZoomMeeting("1");
        hdnMeetingStatus.Value = "SUCCESS";
        if (hdnMeetingStatus.Value == "SUCCESS")
        {
            string cmdText = "";

            string meetingURL = hdnHostURL.Value;
            string cancelledDate = DDlSubDate.SelectedValue;

            string makeUpDate = txtDate.Text;

            cmdText = "Insert into WebConfLog(EventYear,EventID,ChapterID,ProductGroupID,ProductGroupCode,ProductID,ProductCode,MemberID,SessionKey,StartDate,EndDate,BeginTime,EndTime,Semester,[Level],Session,MeetingPwd,Duration,TimeZoneID,TimeZone,CreatedDate,CreatedBy,Meetingurl,Day,UserID,PWD,SessionType, Status)values(" + ddYear.SelectedValue + "," + ddEvent.SelectedValue + "," + ddchapter.SelectedValue + "," + ddlProductGroup.SelectedValue + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedValue + ",'" + ddlProduct.SelectedItem.Text + "'," + CoachID + ",'" + hdnSessionKey.Value + "','" + makeUpDate + "','" + makeUpDate + "','" + ddlMakeupTime.SelectedValue + "','" + EndTime + "','" + ddlPhase.SelectedValue + "','" + ddlLevel.SelectedValue + "',1,'training'," + duration + "," + ddlTimeZone.SelectedValue + ",'" + ddlTimeZone.SelectedItem.Text + "',getDate()," + Session["LoginID"].ToString() + ",'" + meetingURL + "','" + Day + "','" + hdnZoomUserID.Value + "','" + hdnZoomPwd.Value + "','Scheduled Meeting', 'Active')";
            try
            {
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                cmdText = "Update CalSignup set MakeUpURL='" + meetingURL + "', MakeupMeetKey='" + hdnSessionKey.Value + "', MakeUpPwd='training',MakeUpDate='" + makeUpDate + "',MakeUpTime='" + ddlMakeupTime.SelectedValue + "',MakeUpDuration='" + hdnDuration.Value + "' where SignupID=" + SignupID + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                string ChidName = string.Empty;
                string Email = string.Empty;
                string City = string.Empty;
                string Country = string.Empty;
                string ChildNumber = string.Empty;
                string CoachRegID = string.Empty;

                DataSet dsChild = new DataSet();

                string ChildText = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.Email IS NULL then IP.Email else C1.Email end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as WebExEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber) where CR.Level='" + ddlLevel.SelectedValue + "' and CR.Semester='" + ddlPhase.SelectedValue + "' and CR.ProductGroupCode='" + ddlProductGroup.SelectedItem.Text + "' and CR.ProductCode='" + ddlProduct.SelectedItem.Text + "' and CR.CMemberID=" + ddlCoach.SelectedValue + " and CR.EventYear=" + ddYear.SelectedValue + " and CR.EventID=" + ddEvent.SelectedValue + " and CR.approved='Y' and CR.SessionNo=" + ddlSession.SelectedValue + "";



                dsChild = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ChildText);
                if (null != dsChild && dsChild.Tables.Count > 0)
                {
                    if (dsChild.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow drChild in dsChild.Tables[0].Rows)
                        {
                            ChidName = drChild["Name"].ToString();
                            Email = drChild["WebExEmail"].ToString();
                            if (Email.IndexOf(';') > 0)
                            {
                                Email = Email.Substring(0, Email.IndexOf(';'));
                            }
                            City = drChild["City"].ToString();
                            Country = drChild["Country"].ToString();
                            ChildNumber = drChild["ChildNumber"].ToString();
                            CoachRegID = drChild["CoachRegID"].ToString();

                            string CmdChildUpdateText = "update CoachReg set MakeUpURL='" + hdnMeetingUrl.Value + "',ModifyDate=GetDate(), ModifiedBy='" + Session["LoginID"].ToString() + "',MakeUpDate='" + makeUpDate + "',MakeUpTime='" + ddlMakeupTime.SelectedValue + "',MakeUpDuration='" + duration + "' where CoachRegID=" + CoachRegID + "";

                            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdChildUpdateText);

                        }
                    }
                }


                InsertMakeupClassInCoachClassCal(Day, duration);

            }
            catch (Exception ex)
            {
                lblerr.Text = "Operation failed.";
            }

        }
    }

    public void InsertMakeupClassInCoachClassCal(string Day, int Duration)
    {
        try
        {

            int MakeupClassCount = 0;
            int SerNo = 0;
            string MakeupClassText = " select count(*) from CoachClassCal where MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and EventYear=" + ddYear.SelectedValue + " and ClassType='Makeup' and WeekNo=" + TxtWeekNo.Text + " and Semester='" + ddlPhase.SelectedValue + "'";
            string SerNoText = " select max(SerNo) from CoachClassCal where EventYear=" + ddYear.SelectedValue + " and Semester='" + ddlPhase.SelectedValue + "' and EventID=13 and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and MemberId=" + ddlCoach.SelectedValue + "";
            try
            {
                MakeupClassCount = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, MakeupClassText).ToString());
                SerNo = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, SerNoText).ToString());
            }
            catch
            {
                MakeupClassCount = 0;
                SerNo = 0;
            }
            SerNo = SerNo + 1;
            string MakeupInsertUpdate = "INSERT INTO CoachClassCal (MemberID, EventID,EventYear, ProductGroupID, ProductGroup, ProductID, Product, Semester,[Level],Sessionno, Date, Day, Time, Duration, SerNo, WeekNo, Status, CreateDate, CreatedBy, ClassType)";
            MakeupInsertUpdate += " values(" + ddlCoach.SelectedValue + ", 13," + ddYear.SelectedValue + ", " + ddlProductGroup.SelectedValue + ", '" + ddlProductGroup.SelectedItem.Text + "', " + ddlProduct.SelectedValue + ", '" + ddlProduct.SelectedItem.Text + "','" + ddlPhase.SelectedValue + "','" + ddlLevel.SelectedValue + "'," + ddlSession.SelectedValue + ", '" + txtDate.Text + "','" + Day + "', '" + ddlMakeupTime.SelectedValue + "'," + Duration + "," + SerNo + "," + TxtWeekNo.Text + ",'On',getDate()," + Session["LoginId"].ToString() + ", 'Makeup' )";
            if (MakeupClassCount > 0)
            {
                MakeupInsertUpdate = "Update CoachClassCal set Date='" + txtDate.Text + "', Time='" + ddlMakeupTime.SelectedValue + "', Status='On', Day='" + Day + "' where EventYear=" + ddYear.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and ProductGroupID=" + ddlProductGroup.SelectedValue + " and ProductID=" + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedItem.Text + "' and SessionNo=" + ddlSession.SelectedValue + " and WeekNo=" + TxtWeekNo.Text + " and Semester='" + ddlPhase.SelectedValue + "' and ClassType='Makeup'";
            }
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, MakeupInsertUpdate);
            InsertUpdateReleaseDates();


        }
        catch
        {
            lblerr.Text = "Operation failed.";
        }
    }
    public void InsertUpdateReleaseDates()
    {
        try
        {
            if (ddlProductGroup.SelectedValue != "42")
            {

                string cmdText = "select CoachPaperId from CoachPapers where EventYear=" + ddYear.SelectedValue + " and EventId=13 and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and WeekId=" + TxtWeekNo.Text + " and DocType='Q' and Semester='" + ddlPhase.SelectedValue + "'";
                int iCoachPaperId = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                if (iCoachPaperId > 0)
                {

                    cmdText = "select coachRelID from coachreldates where EventYear=" + ddYear.SelectedValue + "  and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId= " + ddlProduct.SelectedValue + " and Level='" + ddlLevel.SelectedValue + "' and Session=" + ddlSession.SelectedValue + " and MemberID=" + ddlCoach.SelectedValue + " and CoachPaperID=" + iCoachPaperId + " and Semester='" + ddlPhase.SelectedValue + "'";
                    int coachRelID = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, cmdText));
                    if (coachRelID > 0)
                    {
                        DateTime QRDate = Convert.ToDateTime(txtDate.Text);
                        DateTime QDDate = Convert.ToDateTime(txtDate.Text).AddDays(5);

                        DateTime ARDate = Convert.ToDateTime(txtDate.Text).AddDays(7);
                        DateTime SRDate = Convert.ToDateTime(txtDate.Text).AddDays(-1);
                        string Memberid = ddlCoach.SelectedValue;
                        string PGId = ddlProductGroup.SelectedValue;
                        string PId = ddlProduct.SelectedValue;
                        string Level = ddlLevel.SelectedValue;
                        string SessionNo = ddlSession.SelectedValue;
                        string Semester = ddlPhase.SelectedValue;

                        string UpdateReleasedates = " update coachreldates set QReleaseDate='" + QRDate.ToShortDateString() + "', QDeadlineDate='" + QDDate.ToShortDateString() + "', AReleaseDate='" + ARDate.ToShortDateString() + "', SReleasedate='" + SRDate.ToShortDateString() + "',ModifiedBy=" + Int32.Parse(Session["LoginID"].ToString()) + ", ModifyDate=Getdate() where CoachPaperId=" + iCoachPaperId + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";

                        SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateReleasedates);

                        //SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                        //string sqlCommand = "usp_Update_CoachRelDate";
                        //SqlParameter[] param = new SqlParameter[8];
                        //param[0] = new SqlParameter("@CoachRelID", iCoachPaperId);
                        //param[1] = new SqlParameter("@QReleaseDate", QRDate);
                        //param[2] = new SqlParameter("@QDeadlineDate", QDDate);
                        //param[3] = new SqlParameter("@AReleaseDate", ARDate);
                        //param[4] = new SqlParameter("@SReleaseDate", SRDate);
                        //param[5] = new SqlParameter("@SessionNO", ddlSession.SelectedValue);
                        //param[6] = new SqlParameter("@ModifiedBy", Int32.Parse(Session["LoginID"].ToString()));
                        //param[7] = new SqlParameter("@ModifyDate", System.DateTime.Now);
                        //SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                    }
                    else
                    {
                        DateTime QRDate = Convert.ToDateTime(txtDate.Text);
                        DateTime QDDate = Convert.ToDateTime(txtDate.Text).AddDays(5);

                        DateTime ARDate = Convert.ToDateTime(txtDate.Text).AddDays(7);
                        DateTime SRDate = Convert.ToDateTime(txtDate.Text).AddDays(-1);
                        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

                        string sqlCommand = "usp_Insert_CoachRelDate";
                        SqlParameter[] param = new SqlParameter[10];
                        param[0] = new SqlParameter("@CoachPaperId", iCoachPaperId);
                        param[1] = new SqlParameter("@Semester", "1");
                        param[2] = new SqlParameter("@SessionNo", ddlSession.SelectedValue);
                        param[3] = new SqlParameter("@QReleaseDate", QRDate.ToShortDateString());
                        param[4] = new SqlParameter("@QDeadlineDate", QDDate.ToShortDateString());
                        param[5] = new SqlParameter("@AReleaseDate", ARDate.ToShortDateString());
                        param[6] = new SqlParameter("@SReleaseDate", SRDate.ToShortDateString());
                        param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
                        param[8] = new SqlParameter("@CreatedBy", Int32.Parse(Session["LoginID"].ToString()));
                        param[9] = new SqlParameter("@MemberId", Int32.Parse(ddlCoach.SelectedItem.Value));

                        SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
                    }
                }
                else
                {
                }
            }
            string WeekNoStimulation = "";
            if (btnCreateMeeting.Text == "Update Makeup Session")
            {
                WeekNoStimulation = "";
                lblSuccess.Text = "Makeup session updated successfully";
                clear();
            }
            else
            {
                lblSuccess.Text = "Makeup session Created successfully";
                clear();
            }

            fillMeetingGrid();
        }
        catch
        {
            lblerr.Text = "Operation failed.";
        }
    }

    public void WeekNoReset(string Action, string WeekNoStimulation)
    {
        string Cmdtext = string.Empty;
        int iweekNo = 0;

        string WeekNoText = "  select WeekNo from CoachclassCal where EventId=13 and memberId=" + hdnCoachID.Value + " and ProductGroupid=" + hdnProductGroupID.Value + " and ProductId=" + hdnProductID.Value + " and Semester='" + ddlPhase.SelectedValue + "' and SessionNo=" + hdnSessionNo.Value + " and level ='" + HdnLevel.Value + "' and Date='" + hdnStartDate.Value + "' and ClassType='Makeup' and Eventyear=" + ddYear.SelectedValue + "";
        try
        {
            iweekNo = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, WeekNoText).ToString());
        }
        catch
        {
            iweekNo = 0;
        }

        Cmdtext = " select count(*) as Countset from CoachclassCal where EventId=13 and memberId=" + hdnCoachID.Value + " and ProductGroupid=" + hdnProductGroupID.Value + " and ProductId=" + hdnProductID.Value + " and Semester='" + ddlPhase.SelectedValue + "' and SessionNo=" + hdnSessionNo.Value + " and level ='" + HdnLevel.Value + "' and WeekNo>" + iweekNo + " and Eventyear=" + ddYear.SelectedValue + "";
        int ClassSequence = 0;
        try
        {
            ClassSequence = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext).ToString());
        }
        catch
        {
            ClassSequence = 0;
        }
        if (ClassSequence > 0)
        {

            string cmdText = string.Empty;
            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            string CoachClassId = string.Empty;

            cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + hdnCoachID.Value + " and ProductGroupid=" + hdnProductGroupID.Value + " and ProductId=" + hdnProductID.Value + " and Semester='" + ddlPhase.SelectedValue + "' and SessionNo=" + hdnSessionNo.Value + " and level ='" + HdnLevel.Value + "'";
            if (Action == "Cancel")
            {
                cmdText += " and WeekNo >= " + iweekNo + "";
            }
            else
            {
                cmdText += " and WeekNo = " + iweekNo + "";
            }
            cmdText += " and Eventyear=" + ddYear.SelectedValue + " and (ClassType='Regular' or classtype='Holiday') order by Date ASC";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
            DataSet dsUniqueDate = new DataSet();
            daUniqueDate.Fill(dsUniqueDate);
            if (null != dsUniqueDate)
            {
                if (dsUniqueDate.Tables[0].Rows.Count > 0)
                {
                    CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
                }
            }


            cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + hdnCoachID.Value + " and ProductGroupid=" + hdnProductGroupID.Value + " and ProductId=" + hdnProductID.Value + " and Semester='" + ddlPhase.SelectedValue + "' and SessionNo=" + hdnSessionNo.Value + " and level ='" + HdnLevel.Value + "' and WeekNo>=" + iweekNo + " and Eventyear=" + ddYear.SelectedValue + "";
            if (Action == "Cancel")
            {
                cmdText += " and CoachClassCalID>=" + CoachClassId + " ";
            }
            else
            {
                cmdText += " and CoachClassCalID>" + CoachClassId + " ";
            }
            cmdText += " order by WeekNo ASC";
            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter daWeek = new SqlDataAdapter(cmd);
            DataSet dsWeek = new DataSet();
            daWeek.Fill(dsWeek);
            if (null != dsWeek)
            {
                if (dsWeek.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsWeek.Tables[0].Rows)
                    {
                        int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                        //Getting coach paper of the week#

                        if (WeekNoStimulation == "decrement")
                        {
                            if (dr["WeekNo"].ToString() == iweekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                            {

                            }
                            else
                            {
                                weekNo = weekNo - 1;
                            }
                        }
                        else if (WeekNoStimulation == "increment")
                        {
                            if (dr["WeekNo"].ToString() == iweekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                            {
                            }
                            else
                            {
                                weekNo = weekNo + 1;
                            }
                        }
                        string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                        //  cmdUpdateText += UpdateRelDates;
                        cmd = new SqlCommand(cmdUpdateText, cn);
                        cmd.ExecuteNonQuery();

                    }
                    if (WeekNoStimulation == "decrement")
                    {
                        ResettingReleaseDatesDecrement("", WeekNoStimulation, iweekNo);
                    }
                    else if (WeekNoStimulation == "increment")
                    {
                        ResettingReleaseDatesIncrement("", WeekNoStimulation, iweekNo);
                    }

                }
            }
        }
    }

    public void ResettingReleaseDatesDecrement(string Action, string WeekNoStimulation, int iWeekNo)
    {
        string Memberid = hdnCoachID.Value;
        string PGId = hdnProductGroupID.Value;
        string PId = hdnProductID.Value;
        string Level = HdnLevel.Value;
        string SessionNo = hdnSessionNo.Value;
        string Semester = ddlPhase.SelectedValue;
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo>=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + "";

        if (Action == "Delete")
        {
            cmdText += " and CoachClassCalID>=" + CoachClassId + " ";
        }
        else
        {
            cmdText += " and CoachClassCalID>" + CoachClassId + " ";
        }
        cmdText += " order by WeekNo ASC";

        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    string dtClassDate = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                    string PreviousWeekNo = string.Empty;

                    if (WeekNoStimulation == "decrement")
                    {
                        //if (dr["WeekNo"].ToString() != PreviousWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == iWeekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra"))
                        {

                        }
                        else
                        {
                            string coachPaperId = "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + " and DocType='Q'";
                            try
                            {
                                coachPaperIdBeforRst = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, coachPaperId).ToString();

                                dtHwDate = Convert.ToDateTime(dtClassDate).AddDays(0).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + coachPaperIdBeforRst + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);


                            }
                            catch
                            {
                            }

                        }
                        PreviousWeekNo = dr["WeekNo"].ToString();
                        //}
                    }
                }
            }
        }
    }
    public void ResettingReleaseDatesIncrement(string Action, string WeekNoStimulation, int iWeekNo)
    {
        string Memberid = hdnCoachID.Value;
        string PGId = hdnProductGroupID.Value;
        string PId = hdnProductID.Value;
        string Level = HdnLevel.Value;
        string SessionNo = hdnSessionNo.Value;
        string Semester = ddlPhase.SelectedValue;

        SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
        cn.Open();
        string cmdText = string.Empty;
        string CoachClassId = string.Empty;
        cmdText = " select top 1* from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " and (ClassType='Regular' or ClassType='Holiday') order by Date ASC";
        SqlCommand cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daUniqueDate = new SqlDataAdapter(cmd);
        DataSet dsUniqueDate = new DataSet();
        daUniqueDate.Fill(dsUniqueDate);
        if (null != dsUniqueDate)
        {
            if (dsUniqueDate.Tables[0].Rows.Count > 0)
            {
                CoachClassId = dsUniqueDate.Tables[0].Rows[0]["CoachClassCalId"].ToString();
            }
        }

        cmdText = " select *  from CoachclassCal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "' and WeekNo>=" + iWeekNo + " and Eventyear=" + ddYear.SelectedValue + " order by WeekNo ASC;";
        cmd = new SqlCommand(cmdText, cn);
        SqlDataAdapter daWeekAft = new SqlDataAdapter(cmd);
        DataSet dsWeekAft = new DataSet();
        daWeekAft.Fill(dsWeekAft);

        if (null != dsWeekAft)
        {
            if (dsWeekAft.Tables[0].Rows.Count > 0)
            {
                //Updating Release dates after resetting release dates

                foreach (DataRow dr in dsWeekAft.Tables[0].Rows)
                {
                    string dtHwDate = string.Empty;
                    string dtHwDueDate = string.Empty;
                    string dtARelDate = string.Empty;
                    string dtSRelDate = string.Empty;
                    string CoachRelId = string.Empty;
                    string coachPaperIdBeforRst = string.Empty;
                    string coachPaperIdAftrRst = string.Empty;
                    int weekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                    string previouWeekNo = string.Empty;

                    if (WeekNoStimulation == "increment")
                    {
                        //if (dr["WeekNo"].ToString() != previouWeekNo)
                        //{
                        if (dr["WeekNo"].ToString() == iWeekNo.ToString() && (dr["ClassType"].ToString() == "Makeup" || dr["ClassType"].ToString() == "Substitute" || dr["ClassType"].ToString() == "Makeup Substitute" || dr["ClassType"].ToString() == "Extra" || dr["ClassType"].ToString() == "Regular"))
                        {

                            string ClassDate = string.Empty;
                            ClassDate = Convert.ToDateTime(txtDate.Text).ToShortDateString();

                            dtHwDate = Convert.ToDateTime(ClassDate).ToShortDateString();
                            dtHwDueDate = Convert.ToDateTime(ClassDate).AddDays(5).ToShortDateString();
                            dtARelDate = Convert.ToDateTime(ClassDate).AddDays(7).ToShortDateString();
                            dtSRelDate = Convert.ToDateTime(ClassDate).AddDays(-1).ToShortDateString();

                            string CoachPaperIdIncAft = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + weekNo + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + "").ToString();

                            string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdIncAft + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                            SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                        }
                        else
                        {



                            try
                            {
                                string CoachPaperIdInc = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text, "select CoachpaperId from coachpapers where WeekId=" + (Convert.ToInt32(weekNo)) + "  and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "'  and level ='" + Level + "' and EventId=13 and EventYear=" + ddYear.SelectedValue + "").ToString();

                                string dtClassDate = string.Empty;
                                dtClassDate = Convert.ToDateTime(dr["Date"].ToString()).ToShortDateString();
                                dtHwDate = Convert.ToDateTime(dtClassDate).ToShortDateString();
                                dtHwDueDate = Convert.ToDateTime(dtClassDate).AddDays(5).ToShortDateString();
                                dtARelDate = Convert.ToDateTime(dtClassDate).AddDays(7).ToShortDateString();
                                dtSRelDate = Convert.ToDateTime(dtClassDate).AddDays(-1).ToShortDateString();

                                string UpdateRelDates = " update coachreldates set QReleaseDate='" + dtHwDate + "', QDeadlineDate='" + dtHwDueDate + "', AReleaseDate='" + dtARelDate + "', SReleasedate='" + dtSRelDate + "' where CoachPaperId=" + CoachPaperIdInc + " and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and Session=" + SessionNo + " and level ='" + Level + "' and EventYear=" + ddYear.SelectedValue + "";
                                SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, UpdateRelDates);

                            }
                            catch
                            {
                            }

                        }
                        //}
                        previouWeekNo = dr["WeekNo"].ToString();
                    }
                }
            }
        }
    }


    protected void btnDeleteMeeting_Click(object sender, EventArgs e)
    {
        deleteMeeting();
    }
    public void deleteMeeting()
    {
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostID.Value + "";
            urlParameter += "&id=" + hdnSessionKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }


    public void timeOverLapValdiation()
    {
        try
        {

            string cmdText = string.Empty;
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],CS.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day,CS.MakeupMeetKey,VC.Duration from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='" + ddYear.SelectedValue + "' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join WebConfLog VC on(VC.MemberID=CS.MemberID and VC.ProductGroupID=CS.ProductGroupID and VC.ProductID=CS.ProductID and VC.Session=CS.SessionNo and VC.Level=CS.Level and VC.SessionKey=CS.MeetingKey) where Accepted='Y' and CS.ProductGroupCode in ('" + ddlProductGroup.SelectedItem.Text + "') and CS.ProductCode in ('" + ddlProduct.SelectedItem.Text + "') and CS.EventYear='" + ddYear.SelectedValue + "' and CS.MemberID=" + ddlCoach.SelectedValue + " and CS.MemberID in(select CMemberID from CoachReg where EventYear=" + ddYear.SelectedValue + " and Approved='Y') and CS.Level='" + ddlLevel.SelectedValue + "' and CS.Semester='" + ddlPhase.SelectedValue + "'";

            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            //string cmdDuration = "";
            //int iYr = SqlHelper.ExecuteScalar(Application["ConnectionString"], CommandType.Text, cmdText);
            string day = Convert.ToDateTime(txtDate.Text).ToString("dddd");
            DataSet dsTime = new DataSet();
            string strStartTime = string.Empty;
            string strEndTime = string.Empty;
            int duration = 0;
            bool isScheduled = false;

            string vcUserID = string.Empty;
            string vcPwd = string.Empty;
            string userID = string.Empty;
            string pwd = string.Empty;
            string signupId = string.Empty;
            string coachId = string.Empty;


            if (null != ds && ds.Tables != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    userID = ds.Tables[0].Rows[0]["UserID"].ToString();
                    pwd = ds.Tables[0].Rows[0]["Pwd"].ToString();
                    duration = Convert.ToInt32(ds.Tables[0].Rows[0]["Duration"].ToString());
                    signupId = ds.Tables[0].Rows[0]["SignupID"].ToString();
                    coachId = ds.Tables[0].Rows[0]["MemberID"].ToString();
                    strStartTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                    DateTime dtStartTime = Convert.ToDateTime(strStartTime);
                    dtStartTime = dtStartTime.AddMinutes(-30);
                    strStartTime = dtStartTime.ToShortTimeString();
                    strEndTime = txtDate.Text + " " + ddlMakeupTime.SelectedValue;
                    DateTime dtEndTime = Convert.ToDateTime(strEndTime);
                    dtEndTime = dtEndTime.AddMinutes(duration);
                    dtEndTime = dtEndTime.AddMinutes(30);
                    strEndTime = dtEndTime.ToShortTimeString();

                    string hostID = string.Empty;

                    hostID = checkAvailableVrooms(duration);
                    if (hostID != "")
                    {
                        string zoomUserID = hdnZoomUserID.Value;
                        string zoomPwd = hdnZoomPwd.Value;

                        hdnHostID.Value = hostID;

                        makeZoomSession(txtDate.Text, ddlMakeupTime.SelectedValue, signupId, coachId, day, strEndTime, duration);
                    }
                    else
                    {
                        lblerr.Text = "No Vroom is available at this time. Please choose different time.";
                    }
                }
                else
                {
                    lblerr.Text = "Training sessions not created yet for the selected coach.";
                }
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }

    public string checkAvailableVrooms(int Duration)
    {

        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        string hostID = string.Empty;

        string strStartTime = ddlMakeupTime.SelectedValue;
        string strStartDay = Convert.ToDateTime(txtDate.Text).ToString("dddd");
        Duration = Duration + 30;
        string strEndTime = Convert.ToDateTime(strStartTime).ToString();
        strStartTime = Convert.ToDateTime(strStartTime).ToString();

        DateTime dtStarttime = new DateTime();
        DateTime dtEndTime = new DateTime();
        DateTime dtPrStartTime = Convert.ToDateTime(strStartTime).AddMinutes(-30);
        DateTime dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);

        string StrSHr = strStartTime.Substring(0, 2);
        int iShr = Convert.ToInt32(StrSHr);
        string StrEhr = Convert.ToDateTime(strEndTime).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
        int iEhr = Convert.ToInt32(StrEhr);
        if ((iShr > 7 & iEhr < 8))
        {
            dtPrEndTime = Convert.ToDateTime("23:59:00");
        }
        else
        {
            dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);
        }

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day, cs.Productgroupcode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" + ddYear.SelectedValue + " and cs.Accepted='Y' order by Time";

        Double iDurationBasedOnPG = 0;


        try
        {
            string vRooms = string.Empty;

            List<Vroom> ObjVroomList = new List<Vroom>();
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            dtStarttime = Convert.ToDateTime(dr["Begin"].ToString());
                            if (dr["ProductGroupCode"].ToString() == "COMP")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "GB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "LSP")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "MB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SAT")
                            {
                                iDurationBasedOnPG = 1.5;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SC")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "UV")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            string StrSHr1 = Convert.ToDateTime(dr["Begin"].ToString()).ToString().Substring(0, 2);
                            int iShr1 = Convert.ToInt32(StrSHr1);
                            string StrEhr1 = Convert.ToDateTime(dr["Begin"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                            int iEhr1 = Convert.ToInt32(StrEhr1);
                            if ((iShr1 > 7 & iEhr1 < 8))
                            {
                                dtEndTime = Convert.ToDateTime("23:59:00");
                            }
                            else
                            {
                                dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                            }


                            string Meetingday = dr["Day"].ToString();
                            string meetVroom = dr["Vroom"].ToString();




                            if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                            {
                                Vroom objVroom = new Vroom();
                                objVroom.Day = dr["Day"].ToString();
                                objVroom.HostId = dr["HostId"].ToString();
                                ObjVroomList.Add(objVroom);

                                vRooms += dr["VRoom"].ToString() + ",";
                            }
                        }
                        catch
                        {
                        }


                    }
                }
            }



            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" + ddYear.SelectedValue + " and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by VC.BeginTime";
            try
            {
                DataSet dsSingle = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            try
                            {
                                dtStarttime = Convert.ToDateTime(dr["BeginTime"].ToString());
                                if (dr["ProductGroupCode"].ToString() == "COMP")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "GB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "LSP")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "MB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SAT")
                                {
                                    iDurationBasedOnPG = 1.5;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SC")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "UV")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                string StrSHr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).ToString().Substring(0, 2);
                                int iShr1 = Convert.ToInt32(StrSHr1);
                                string StrEhr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                                int iEhr1 = Convert.ToInt32(StrEhr1);
                                if ((iShr1 > 7 & iEhr1 < 8))
                                {
                                    dtEndTime = Convert.ToDateTime("23:59:00");
                                }
                                else
                                {
                                    dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);
                                }

                                string Meetingday = dr["Day"].ToString();
                                string meetVroom = dr["Vroom"].ToString();

                                if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                                {
                                    Vroom objVroom = new Vroom();
                                    objVroom.Day = dr["Day"].ToString();
                                    objVroom.HostId = dr["HostId"].ToString();
                                    ObjVroomList.Add(objVroom);

                                    vRooms += dr["VRoom"].ToString() + ",";
                                }


                            }
                            catch
                            {
                            }


                        }
                    }
                }
            }
            catch
            {
            }


            if (!string.IsNullOrEmpty(vRooms))
            {
                vRooms = vRooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(vRooms))
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom not in (" + vRooms + ") order by vroom desc";
            }
            else
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp order by vroom desc";
            }

            DataSet dsVroom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (dsVroom.Tables.Count > 0)
            {
                if (dsVroom.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsVroom.Tables[0].Rows)
                    {
                        hostID = dr["HostID"].ToString();
                        hdnWebExID.Value = dr["UserID"].ToString();
                        hdnWebExPwd.Value = dr["PWD"].ToString();
                        hdnZoomUserID.Value = dr["UserID"].ToString();
                        hdnZoomPwd.Value = dr["PWD"].ToString();
                    }
                }
                else
                {
                    hostID = "";
                }
            }

            // hostID = "";
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }
        return hostID;
    }

    public class Vroom
    {
        public string StartTime { get; set; }
        public string EndTime { get; set; }
        public string HostId { get; set; }
        public string Day { get; set; }
    }

    public void logerrors(string coachName, string status, string exception)
    {
        string filepath = string.Empty;
        try
        {

            // string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));

            string pageName = Path.GetFileName(Request.Path);
            string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";

            string folderPath = Server.MapPath("~/CoachingLog");
            if (!Directory.Exists(folderPath))
            {
                Directory.CreateDirectory(folderPath);
            }

            filepath = Server.MapPath("~/CoachingLog/" + filename);

            if (File.Exists(filepath))
            {
                using (StreamWriter stwriter = new StreamWriter(filepath, true))
                {
                    string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                    message += Environment.NewLine;
                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;
                    message += string.Format("Date & Time: {0}", DateTime.Now);
                    message += Environment.NewLine;
                    message += string.Format("Coach Name: {0}", coachName);
                    message += Environment.NewLine;
                    message += string.Format("Validation Status: {0}", status);
                    message += Environment.NewLine;

                    message += string.Format("Source: {0}", "Makeup Sessions");
                    message += Environment.NewLine;

                    message += Environment.NewLine;
                    message += string.Format("Exception: {0}", exception);
                    message += Environment.NewLine;

                    message += "-----------------------------------------------------------";
                    message += Environment.NewLine;

                    stwriter.WriteLine(message);
                }
            }
            else
            {
                StreamWriter stwriter = File.CreateText(filepath);
                string message = string.Format("Time: {0}", DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss tt"));
                message += Environment.NewLine;
                message += "-----------------------------------------------------------";
                message += Environment.NewLine;
                message += string.Format("Date & Time: {0}", DateTime.Now);
                message += Environment.NewLine;
                message += string.Format("Coach Name: {0}", coachName);
                message += Environment.NewLine;
                message += string.Format("Validation Status: {0}", status);
                message += Environment.NewLine;

                message += string.Format("Source: {0}", "Makeup Sessions");
                message += Environment.NewLine;

                message += Environment.NewLine;
                message += string.Format("Exception: {0}", exception);
                message += Environment.NewLine;

                message += "-----------------------------------------------------------";
                message += Environment.NewLine;

                stwriter.WriteLine(message);
                stwriter.Close();
            }
        }
        catch (Exception ex)
        {
            // lblerr.Text = ex.Message;
        }
    }

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }

    }

    public void clear()
    {
        btnCreateMeeting.Text = "Create Makeup Session";
        txtDate.Text = "";
        ddlMakeupTime.SelectedValue = "";
        ddlReason.SelectedValue = "0";
        DDlSubDate.SelectedValue = "0";
        TxtWeekNo.Enabled = true;
    }

    public void listLiveMeetings()
    {
        try
        {
            string URL = string.Empty;
            string service = "6";
            URL = "https://api.zoom.us/v1/meeting/live";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            //urlParameter += "&host_id=1BYjtcBlQKSXzwHzRqPaxg";
            //urlParameter += "&page_size=30";
            //urlParameter += "&page_number=1";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    public void termianteMeeting(string sessionkey, string hostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "7";
            URL = "https://api.zoom.us/v1/meeting/end";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hostID + "";
            urlParameter += "&id=" + sessionkey + "";


            makeZoomAPICall(urlParameter, URL, service);
        }

        catch
        {
        }
    }

    protected void ddlLevel_SelectedIndexChanged(object sender, EventArgs e)
    {
        LoadSessionNo();
    }

    public void downloadTextFile()
    {
        string pageName = Path.GetFileName(Request.Path);
        string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";
        string filepath = Server.MapPath("~/CoachingLog/" + filename);
        FileInfo file = new FileInfo(filepath);

        // Checking if file exists
        if (file.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();

            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);

            // Add the file size into the response header
            Response.AddHeader("Content-Length", file.Length.ToString());

            // Set the ContentType
            Response.ContentType = ReturnExtension(file.Extension.ToLower());

            // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
            Response.TransmitFile(file.FullName);

            // End the response
            Response.End();
        }
    }
    private string ReturnExtension(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }

    protected void btnDowloadFIle_Click(object sender, EventArgs e)
    {
        downloadTextFile();
    }

    private Boolean InsertUpdateData(SqlCommand cmd)
    {
        String strConnString = System.Configuration.ConfigurationManager.AppSettings["DBConnection"];

        SqlConnection con = new SqlConnection(strConnString);
        cmd.CommandType = CommandType.Text;
        cmd.Connection = con;
        try
        {
            con.Open();
            cmd.ExecuteNonQuery();
            return true;
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message);
            return false;
        }
        finally
        {
            con.Close();
            con.Dispose();
        }
    }
    protected void ddlPhase_SelectedIndexChanged(object sender, EventArgs e)
    {
        fillCoach();
    }

    public int CheckWeekNoExists()
    {
        int Retval = -1;
        string Cmdtext = string.Empty;
        string Memberid = ddlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = ddlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string SessionNo = ddlSession.SelectedValue;
        string Semester = ddlPhase.SelectedValue;
        int RegularClassCount = 0;

        Cmdtext = "select count(*) as RegulatCount from coachClasscal where Eventyear=" + ddYear.SelectedValue + " and Semester='" + Semester + "'  and memberID=" + Memberid + "  and ProductGroupID=" + PGId + " and ProductID=" + PId + " and Level ='" + Level + "' and sessionNo=" + SessionNo + " and WeekNo=" + TxtWeekNo.Text + " and (ClassType='Regular' or ClassType='Holiday')";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                RegularClassCount = Convert.ToInt32(ds.Tables[0].Rows[0]["RegulatCount"].ToString());
                Retval = RegularClassCount;
            }
        }
        return Retval;
    }

    public int VerifyRegularClassCancelled()
    {
        int Retval = 1;
        string Cmdtext = string.Empty;

        string Memberid = ddlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = ddlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string SessionNo = ddlSession.SelectedValue;
        string Semester = ddlPhase.SelectedValue;
        string IsOnClass = "";

        Cmdtext = "Select Status from coachclasscal where Eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + PGId + " and ProductId=" + PId + " and Level='" + Level + "' and SessionNo='" + SessionNo + "' and WeekNo=" + TxtWeekNo.Text + " and MemberID=" + Memberid + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Holiday')";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    if (dr["Status"].ToString() == "On")
                    {
                        IsOnClass = "Yes";
                    }
                }
            }
        }
        if (IsOnClass == "Yes")
        {
            Retval = -2;
        }

        return Retval;
    }

    public int CheckDuplicateMakeupClass()
    {
        int Retval = 1;
        string Cmdtext = string.Empty;

        string Memberid = ddlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = ddlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string SessionNo = ddlSession.SelectedValue;
        string Semester = ddlPhase.SelectedValue;
        string Makeup = "";

        Cmdtext = "select Status from CoachClasscal where Eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + PGId + " and ProductId=" + PId + " and Level='" + Level + "' and SessionNo='" + SessionNo + "' and MemberID=" + Memberid + " and WeekNo=" + (Convert.ToInt32(TxtWeekNo.Text)) + " and ClassType='Makeup'";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                Makeup = ds.Tables[0].Rows[0]["Status"].ToString();
            }
        }
        if (Makeup != "")
        {
            if (Makeup != "Cancelled")
            {
                Retval = -2;
            }
        }

        return Retval;
    }

    public int ValdiateMakeupCLassDateTime()
    {
        int Retval = 1;
        string Cmdtext = string.Empty;

        string Memberid = ddlCoach.SelectedValue;
        string PGId = ddlProductGroup.SelectedValue;
        string PId = ddlProduct.SelectedValue;
        string Level = ddlLevel.SelectedValue;
        string SessionNo = ddlSession.SelectedValue;
        string Semester = ddlPhase.SelectedValue;
        string RegulaClassDay = "";
        string RegularTime = "";
        Cmdtext = "select distinct Day, Time, Status from CoachClasscal where Eventyear=" + ddYear.SelectedValue + " and ProductGroupID=" + PGId + " and ProductId=" + PId + " and Level='" + Level + "' and SessionNo='" + SessionNo + "' and MemberID=" + Memberid + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Holiday')";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                RegulaClassDay = ds.Tables[0].Rows[0]["Day"].ToString();
                RegularTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["Time"].ToString()).ToString("HH:mm");
            }
        }
        string ToDay = DateTime.Now.ToShortDateString();
        DateTime dtDate = Convert.ToDateTime(ToDay);
        string ClassDate = Convert.ToDateTime(txtDate.Text).ToString("MM/dd/yyyy");

        DateTime dtCoachDate = Convert.ToDateTime(ClassDate);
        if (dtCoachDate > dtDate)
        {

            string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

            if (MakeupDay == RegulaClassDay && RegularTime == ddlMakeupTime.SelectedValue)
            {
                Retval = -2;
            }

        }
        else if (dtCoachDate == dtDate)
        {
            ToDay = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
            dtDate = Convert.ToDateTime(ToDay);
            ClassDate = ClassDate + " " + ddlMakeupTime.SelectedValue;
            ClassDate = Convert.ToDateTime(ClassDate).ToString("MM/dd/yyyy HH:mm");
            dtCoachDate = Convert.ToDateTime(ClassDate);
            if (dtCoachDate > dtDate)
            {
                Retval = 1;
            }
            else
            {
                Retval = -1;
            }

            string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

            if (MakeupDay == RegulaClassDay && RegularTime == ddlMakeupTime.SelectedValue)
            {
                Retval = -2;
            }
        }
        else if (dtCoachDate < dtDate)
        {
            Retval = -3;
        }

        return Retval;
    }

    public void ResettingWeekNo(string WeekNoStimulation)
    {
        int Retval = -1;
        try
        {
            string Memberid = hdnCoachID.Value;
            string PGId = hdnProductGroupID.Value;
            string PId = hdnProductID.Value;
            string Level = HdnLevel.Value;
            string SessionNo = hdnSessionNo.Value;
            string Semester = ddlPhase.SelectedValue;
            string cmdText = string.Empty;
            string CoachClassId = string.Empty;

            cmdText = " select * from coachclasscal where EventId=13 and memberId=" + Memberid + " and ProductGroupid=" + PGId + " and ProductId=" + PId + " and Semester='" + Semester + "' and SessionNo=" + SessionNo + " and level ='" + Level + "'";

            cmdText += " and Eventyear=" + ddYear.SelectedValue + " order by WeekNo, Date ASC";
            DataSet dsUniqueDate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdText);
            string Status = "On";
            string PrevStatus = "On";
            int PrevWeekNo = 0;
            string Classtype = "";
            int weekNo = PrevWeekNo;
            if (dsUniqueDate.Tables.Count > 0)
            {
                if (dsUniqueDate.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsUniqueDate.Tables[0].Rows)
                    {
                        CoachClassId = dr["CoachClassCalId"].ToString();


                        Classtype = dr["ClassType"].ToString();
                        if (PrevStatus == "On" && Classtype != "Extra")
                        {
                            weekNo = weekNo + 1;
                        }

                        string cmdUpdateText = " update CoachClassCal set WeekNo=" + weekNo + " where coachClassCalId=" + dr["CoachClassCalId"].ToString() + "; ";
                        //  cmdUpdateText += UpdateRelDates;
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdUpdateText);

                        PrevWeekNo = Convert.ToInt32(dr["WeekNo"].ToString());
                        PrevStatus = dr["Status"].ToString();
                        Retval = 1;
                    }

                    int iweekNo;
                    string WeekNoText = "  select WeekNo from CoachclassCal where EventId=13 and memberId=" + hdnCoachID.Value + " and ProductGroupid=" + hdnProductGroupID.Value + " and ProductId=" + hdnProductID.Value + " and Semester='" + ddlPhase.SelectedValue + "' and SessionNo=" + hdnSessionNo.Value + " and level ='" + HdnLevel.Value + "' and Date='" + hdnStartDate.Value + "' and ClassType='Makeup' and Eventyear=" + ddYear.SelectedValue + "";
                    try
                    {
                        iweekNo = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, WeekNoText).ToString());
                    }
                    catch
                    {
                        iweekNo = 0;
                    }

                    if (WeekNoStimulation == "decrement")
                    {
                        ResettingReleaseDatesDecrement("", WeekNoStimulation, iweekNo);
                    }
                    else if (WeekNoStimulation == "increment")
                    {
                        ResettingReleaseDatesIncrement("", WeekNoStimulation, iweekNo);
                    }
                }
            }
        }
        catch
        {
        }

    }

    public void fillYearFiletr()
    {
        int Year = Convert.ToInt32(DateTime.Now.Year);

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)));

        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)));
        ddlEventyearFilter.Items.Add(new ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)));

        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select max(eventyear) from EventFees where EventId=13").ToString();

    }

    public void loadProductGroupFilter()
    {
        try
        {
            string strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND  P.EventId=13  ";

            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + " order by P.ProductGroupID";
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductGroupFilter.DataValueField = "ProductGroupID";
            ddlProductGroupFilter.DataTextField = "Name";

            ddlProductGroupFilter.DataSource = drproductgroup;
            ddlProductGroupFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {
                ddlProductGroupFilter.Items.Insert(0, new ListItem("Select", "0"));

                ddlProductGroupFilter.Enabled = true;
            }
            else
            {
                ddlProductGroupFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadProductFilter()
    {
        try
        {
            string strSql = " Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" + ddlEventyearFilter.SelectedValue + " AND P.EventID=13";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and P.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if ((ddlPhaseFilter.SelectedValue != "0"))
            {
                strSql = strSql + " and Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }

            strSql = strSql + "order by P.ProductID";


            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());


            DataSet drproductgroup = new DataSet();
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlProductFilter.DataValueField = "ProductID";
            ddlProductFilter.DataTextField = "Name";

            ddlProductFilter.DataSource = drproductgroup;
            ddlProductFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlProductFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlProductFilter.Enabled = true;
            }
            else
            {
                ddlProductFilter.Enabled = false;
            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadLevelFilter()
    {
        try
        {
            string strSql = " select distinct LevelCode from ProdLevel where EventYear=" + ddlEventyearFilter.SelectedValue + "  and EventID=13";
            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }

            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and ProductID=" + ddlProductFilter.SelectedValue + "";
            }

            DataSet drproductgroup = new DataSet();

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);
            ddlLevelFilter.DataValueField = "LevelCode";
            ddlLevelFilter.DataTextField = "LevelCode";
            ddlLevelFilter.DataSource = drproductgroup;
            ddlLevelFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlLevelFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlLevelFilter.Enabled = true;
            }
            else
            {
                ddlLevelFilter.Enabled = false;
            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    public void loadCoachFilter()
    {
        try
        {
            string strSql = " select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignup V On (V.MemberID = I.AutoMemberID and V.EventYear=" + ddlEventyearFilter.SelectedValue + ")  Where V.EventYear= " + ddlEventyearFilter.SelectedValue + "  ";

            if (ddlProductGroupFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductGroupID=" + ddlProductGroupFilter.SelectedValue + "";
            }
            if (ddlProductFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.ProductID=" + ddlProductFilter.SelectedValue + "";
            }
            if (ddlLevelFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Level=" + ddlLevelFilter.SelectedValue + "";
            }
            if (ddlPhaseFilter.SelectedValue != "0")
            {
                strSql = strSql + " and V.Semester='" + ddlPhaseFilter.SelectedValue + "'";
            }


            strSql = strSql + " order by I.FirstName,I.LastName";

            DataSet drproductgroup = new DataSet();
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            drproductgroup = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, strSql);

            ddlCoachFilter.DataValueField = "MemberID";
            ddlCoachFilter.DataTextField = "Name";

            ddlCoachFilter.DataSource = drproductgroup;
            ddlCoachFilter.DataBind();

            if ((drproductgroup.Tables[0].Rows.Count > 1))
            {

                ddlCoachFilter.Items.Insert(0, new ListItem("Select", "0"));
                ddlCoachFilter.Enabled = true;
            }
            else
            {
                ddlCoachFilter.Enabled = false;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup");
        }
    }

    protected void ddlProductGroupFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }

        fillMeetingGrid();

    }

    protected void ddlProductFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadLevelFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }

        fillMeetingGrid();
    }

    protected void btnClearFilter_Click(object sender, EventArgs e)
    {

        //ddlProductGroupFilter.SelectedValue = "0";
        //ddlProductFilter.SelectedValue = "0";
        //ddlLevelFilter.SelectedValue = "0";
        //ddlCoachFilter.SelectedValue = "0";

        ddlPhaseFilter.SelectedValue = "0";
        ddlSessionFilter.SelectedValue = "0";
        ddlDayFilter.SelectedValue = "0";

        fillYearFiletr();
        loadProductGroupFilter();
        loadProductFilter();

        loadLevelFilter();
        loadCoachFilter();

        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid();
    }

    protected void ddlLevelFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;


        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid();
    }

    protected void ddlSessionFilter_SelectedIndexChanged(object sender, EventArgs e)
    {


        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid();

    }
    protected void ddlDayFilter_SelectedIndexChanged1(object sender, EventArgs e)
    {


        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;

        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid();
    }
    protected void ddlPhaseFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        loadProductGroupFilter();
        loadProductFilter();
        loadLevelFilter();
        loadCoachFilter();
        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }
        fillMeetingGrid();

    }

    protected void ddlCoachFilter_SelectedIndexChanged(object sender, EventArgs e)
    {

        string prdGroup = string.Empty;
        string prdID = string.Empty;
        string strlevel = string.Empty;
        string coach = string.Empty;
        if ((ddlProductGroupFilter.Items.Count > 0 & ddlProductGroupFilter.SelectedValue != "0"))
        {
            prdGroup = ddlProductGroupFilter.SelectedValue;
        }
        if ((ddlProductFilter.Items.Count > 0 & ddlProductFilter.SelectedValue != "0"))
        {
            prdID = ddlProductFilter.SelectedValue;
        }
        if ((ddlLevelFilter.Items.Count > 0 & ddlLevelFilter.SelectedValue != "0"))
        {
            strlevel = ddlLevelFilter.SelectedValue;
        }
        if ((ddlCoachFilter.Items.Count > 0 & ddlCoachFilter.SelectedValue != "0"))
        {
            coach = ddlCoachFilter.SelectedValue;
        }

        fillMeetingGrid();
    }
}