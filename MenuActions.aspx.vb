'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/MenuActions.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page provides the links to various options selected by 
'           the user. Options basing the role thy signed on.
'
'           Will utilize the following stored procedures
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Namespace VRegistration

Partial Class MenuActions
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If Not IsPostBack Then
            hlAddEvent.Attributes.Add("onclick", "javascript:parent.contents.document.URL='AddEvent.aspx" + "';")

            hlEditEvent.Attributes.Add("onclick", "javascript:parent.contents.document.URL='EventsList.aspx" + "';")
            hlRegisterHere.Attributes.Add("onclick", "javascript:parent.contents.document.URL='Registration.aspx" + "';")
            hlEditRegistration.Attributes.Add("onclick", "javascript:parent.contents.document.URL='VolunteerList.aspx" + "';")
            hlEventSignUp.Attributes.Add("onclick", "javascript:parent.contents.document.URL='VolunteerSignUp.aspx" + "';")
            hlViewEvents.Attributes.Add("onclick", "javascript:parent.contents.document.URL='ViewEvents.aspx" + "';")
            hlContactInfo.Attributes.Add("onclick", "javascript:parent.contents.document.URL='ContactInfo.aspx" + "';")
            hlAspiringVolunteers.Attributes.Add("onclick", "javascript:parent.contents.document.URL='AspiringVolunteers.aspx" + "';")

            hlAddEvent.Enabled = True
            hlEditEvent.Enabled = True
            hlRegisterHere.Enabled = True
            hlEditRegistration.Enabled = True
            hlEventSignUp.Enabled = True
            hlViewEvents.Enabled = True
            hlContactInfo.Enabled = True
        End If
    End Sub
End Class

End Namespace

