Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization


Partial Class ContestantRegistration
    Inherits System.Web.UI.Page

    Public nRegFee As Decimal = 0
    Public cnTemp As SqlConnection


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Debug 
        'Session("CustIndID") = 9210  '4
        'Session("LoggedIn") = "True"
        'Session("CustSpouseID") = 9210
        nRegFee = 0
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            'Dim nsfMaster As NSFMasterPage = Me.Master
            'nsfMaster.addBackMenuItem("UserFunctions.aspx")

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If

            LoadChildren()
        End If
        DisplaySelectedContests()
        DisplayPaidContests()
        lblWarning1.Visible = True
        lblWarning2.Visible = True

        If Session("EventID") = 2 Then 'Regionals
            If Not IsThereContestsForLoginChapterID() = True Then
                lblNoContestMsg.Visible = False
            Else
                'Gets Volunteer Information
                Dim dsVolunteer As New DataSet
                Dim strContactInfo As String, strFirstName As String, strLastName As String, strHPhone As String, strCPhone As String, strEmail As String


                strContactInfo = ""
                strFirstName = ""
                strLastName = ""
                strEmail = ""
                strCPhone = ""
                strHPhone = ""
               
                If Not dsVolunteer Is Nothing Then
                    If dsVolunteer.Tables(0).Rows.Count > 0 Then
                        Dim dr As DataRow
                        For Each dr In dsVolunteer.Tables(0).Rows
                            strFirstName = dr("FirstName")
                            strLastName = dr("LastName")
                            strEmail = dr("Email")
                            strCPhone = dr("CPhone")
                            strHPhone = dr("HPhone")
                        Next
                    End If
                End If

                If strFirstName <> "" Then
                    strContactInfo = strFirstName & " " & strLastName & ";  Phone:" & strHPhone & "; Email:" & strEmail
                End If



                If dgEligibleContests.Items.Count > 0 Then
                    If strContactInfo = "" Then
                        lblNoContestMsg.Text = "If you do not find any contests for your chapter, unless your child is not eligible, it may mean that contests dates were not yet determined. Please consult your chapter coordinator before you select any contests."
                    Else
                        lblNoContestMsg.Text = "If you do not find any contests for your chapter, unless your child is not eligible, it may mean that contests dates were not yet determined. Please consult " & strContactInfo & " before you select any contests."
                    End If
                Else
                    If strContactInfo = "" Then
                        lblNoContestMsg.Text = "The contest dates were not yet determined for your area. Please contact your local coordinator regarding this matter. You cannot select contests and register unitl the contest calendar is set for your location."
                    Else
                        lblNoContestMsg.Text = "The contest dates were not yet determined for your area. Please contact " & strContactInfo & " regarding this matter. You cannot select contests and register unitl the contest calendar is set for your location."
                    End If
                End If
                lblNoContestMsg.Visible = True
            End If
        End If

    End Sub

    Private Function IsThereContestsForLoginChapterID() As Boolean
        Dim dsContests As New DataSet
        Dim param(3) As SqlParameter

        IsThereContestsForLoginChapterID = False
        param(0) = New SqlParameter("@ChapterID", Session("LoginChapterID"))
        param(1) = New SqlParameter("@ContestYear", Application("ContestYear"))
        param(2) = New SqlParameter("@EventID", Session("EventID"))
        dsContests = SqlHelper.ExecuteDataset(cnTemp, CommandType.StoredProcedure, "usp_CheckContestForGivenChapterID", param)

        If Not dsContests Is Nothing Then
            If dsContests.Tables(0).Rows.Count > 0 Then
                IsThereContestsForLoginChapterID = True
            End If
        End If

    End Function

    Private Sub DisplaySelectedContests()
        Select Case Session("EventID")
            Case 1
                DisplayNationalSelectedContests()
            Case 2
                DisplayRegionalSelectedContests()
            Case Else
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End Select
    End Sub

    Private Sub DisplayNationalSelectedContests()
        Session("ContestsSelected") = ""

        Dim dsContestant As New DataSet, strSQL As String
        Dim tblConestant() As String = {"Contestant"}

        strSQL = " SELECT a.ChapterID, a.ContestCode as ContestID, a.ContestYear, a.ParentID, a.ChildNumber," & _
                 " a.contestant_id, a.NationalInvitee, a.PaymentDate, a.PaymentReference, " & _
                " b.ContestCategoryID, b.ProductCode as ContestAbbr, " & _
                " b.ContestDate, b.StartTime as ContestTime, Case when Ex.NewDeadline is Null then b.RegistrationDeadline Else Ex.NewDeadline end as RegistrationDeadline, b.NSFChapterID, " & _
                " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, " & _
                " c.NationalFinalsFee as Fee, c.ContestDesc, " & _
                " REPLACE(e.FIRST_NAME + ' ' + COALESCE (e.MIDDLE_INITIAL, '') + ' ' + e.LAST_NAME, ' ', ' ') AS ContestantName, " & _
                " '' as State,'' as City, '' as VolunteerName, '' as VolunteerPhone  " & _
                " FROM Contestant a " & _
                " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber  AND a.ParentID = e.MEMBERID " & _
                " INNER JOIN Contest b On b.ContestID = a.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=e.ChildNumber " & _
                " INNER JOIN ContestCategory c " & _
                " ON b.ContestCategoryID = c.ContestCategoryID " & _
                " WHERE (a.ParentID =  " & Session("CustIndID") & " ) " & _
                " and a.ContestYear =    " & Application("ContestYear") & "   and b.EventID = 1 " & _
                " and Case when Ex.NewDeadline is Null then Datediff(d,b.RegistrationDeadline,Getdate()) Else Datediff(d,Ex.NewDeadline,Getdate()) END <=0 ORDER BY ContestId  "
        ' lblWarning1.Text = strSQL
        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        ' SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "usp_DeletePendingNationalContests", prmNArray)
        SqlHelper.FillDataset(cnTemp, CommandType.Text, strSQL, dsContestant, tblConestant)

        If Not ViewState("SelectedContests") Is Nothing Then
            CType(ViewState("SelectedContests"), ArrayList).Clear()
        End If

        nRegFee = 0
        Dim dvSelectedContests As DataView
        Dim strToday As Date = Date.Now
        dvSelectedContests = dsContestant.Tables(0).DefaultView
        Dim strWhere As String
        strWhere = " PaymentDate is NULL "
        dvSelectedContests.RowFilter = strWhere

        If dvSelectedContests.Count > 0 Then
            dgSelectedContests.DataSource = dvSelectedContests
            dgSelectedContests.DataBind()
            dgSelectedContests.Visible = True
        Else
            dgSelectedContests.Visible = False
        End If

        btnPayNow.Visible = True
        RegFee.Text = "None"
        Session("RegFee") = CType(0, Decimal)
        Session("Donation") = CType(0, Decimal)
        Dim US As New CultureInfo("en-US")
        If (nRegFee > 0) Then
            RegFee.Text = nRegFee.ToString("c", US) '"$" & nRegFee '
            Session("RegFee") = CType(nRegFee, Decimal)
        End If
        btnPayNow.Text = "Add Meals and Pay"
        lblPaynow.Text = "After selecting contests and  pressing on submit , click on Add Meals and Pay to go to the next page."
        lblPaynow.Visible = True
        btnPayNow.Visible = True
    End Sub

    Private Sub DisplayRegionalSelectedContests()
        Session("ContestsSelected") = ""

        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(3) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@LateFeeDate"
        prmArray(2).Value = IIf(Application("ContestType") = "1", Application("LateRegistrationDate"), DBNull.Value)
        prmArray(2).Direction = ParameterDirection.Input

        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        'SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "usp_DeletePendingContests", prmArray)
        SqlHelper.FillDataset(cnTemp, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)
        If Not ViewState("SelectedContests") Is Nothing Then
            CType(ViewState("SelectedContests"), ArrayList).Clear()
        End If

        nRegFee = 0
        Dim dvSelectedContests As DataView
        Dim strToday As Date = Date.Now
        dvSelectedContests = dsContestant.Tables(0).DefaultView
        Dim strWhere As String
        strWhere = " PaymentDate is NULL "
        dvSelectedContests.RowFilter = strWhere

        If dvSelectedContests.Count > 0 Then
            dgSelectedContests.DataSource = dvSelectedContests
            dgSelectedContests.DataBind()
            pnlSelectedContests.Visible = True
        Else
            pnlSelectedContests.Visible = False
        End If

        RegFee.Text = "None"
        Session("RegFee") = CType(0, Decimal)
        Session("Donation") = CType(0, Decimal)
        If (nRegFee > 0) Then
            RegFee.Text = "$" & nRegFee 'nRegFee.ToString("c", "us")
            Session("RegFee") = CType(nRegFee, Decimal)
            btnPayNow.Visible = True
            lblPaynow.Visible = True
        Else
            btnPayNow.Visible = False
        End If
    End Sub

    Private Sub DisplayEligibleContests(ByVal ChildID As Integer)
        If Session("EventID") = 1 Then
            DisplayNationalEligibility(ChildID)
        Else
            DisplayRegionalEligibility(ChildID)
        End If
    End Sub

    Private Sub DisplayNationalEligibility(ByVal ChildID As Integer)
        Dim strSQL As String
        Dim dsEligibleContests As DataSet = New DataSet
        Dim tblEligibleContests() As String = {"EligibleContests"}
        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        strSQL = " SELECT b.ContestDate, b.StartTime as ContestTime, Case when Ex.NewDeadline is Null then b.RegistrationDeadline Else Ex.NewDeadline end as RegistrationDeadline, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, " & _
                 " c.NationalFinalsFee as Fee, b.ContestID, " & _
                 " c.ContestCategoryID, c.ContestCode as ContestAbbr, " & _
                 " c.ContestDesc, a.ContestYear, a.ParentID, a.ChildNumber, " & _
                 " REPLACE(e.FIRST_NAME + ' ' + COALESCE (e.MIDDLE_INITIAL, '') + ' ' + e.LAST_NAME, ' ', ' ') AS ContestantName, " & _
                 " a.contestant_id, a.NationalInvitee, b.NSFChapterID,'' as State,'' as City " & _
                 " FROM Contestant a INNER JOIN ContestCategory c " & _
                 " ON a.ContestCategoryID = c.ContestCategoryID " & _
                 " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber AND a.ParentID = e.MEMBERID " & _
                 " INNER JOIN Contest B on B.ContestCategoryID =  c.ContestCategoryID " & _
                 " LEFT JOIN ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " WHERE (a.ParentID = " & Session("CustIndID") & " )" & _
                 " and a.ContestYear = " & Now.Year() & " and a.NationalInvitee = 1 and  " & _
                 " a.ChildNumber = " & ChildID & " And b.NSFChapterID = 1 " & _
                 " UNION Select b.ContestDate, b.StartTime as ContestTime, Case when Ex.NewDeadline is Null then b.RegistrationDeadline Else Ex.NewDeadline end as RegistrationDeadline, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, " & _
                 " a.NationalFinalsFee as Fee, b.ContestID, " & _
                 " a.ContestCategoryID, a.ContestCode as ContestAbbr, " & _
                 " a.ContestDesc, b.Contest_Year, " & Session("CustIndID") & " as ParentID, " & ChildID & " as ChildNumber, " & _
                 " '' as ContestantName, '' as Contestant_ID, '1' as NationalInvitee,  b.NSFChapterID,'' as State,'' as City " & _
                 " from Contest b   Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID " & _
                 " and b.Contest_Year =  " & Now.Year() & _
                 " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' and  a.GradeBased=1 " & _
                 " and (Select Grade from Child where childnumber = " & ChildID & " ) " & _
                 " between GradeFrom and GradeTo " & _
                 " UNION Select b.ContestDate, b.StartTime as ContestTime, Case when Ex.NewDeadline is Null then b.RegistrationDeadline Else Ex.NewDeadline end as RegistrationDeadline, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, " & _
                 " a.NationalFinalsFee as Fee, b.ContestID, " & _
                 " a.ContestCategoryID, a.ContestCode as ContestAbbr, " & _
                 " a.ContestDesc, b.Contest_Year, " & Session("CustIndID") & " as ParentID, " & ChildID & " as ChildNumber, " & _
                 " '' as ContestantName, '' as Contestant_ID, '1' as NationalInvitee,  b.NSFChapterID,'' as State,'' as City " & _
                 " from Contest b   Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=B.Contest_Year and Ex.EventID=B.EventId and Ex.ChildNumber=" & ChildID & " " & _
                 " INNER JOIN ContestCategory a on b.ContestCategoryID = a.ContestCategoryID " & _
                 " and b.Contest_Year =  " & Now.Year() & _
                 " and b.nsfchapterid = 1 and a.NationalSelectionCriteria = 'O' and a.GradeBased=0  " & _
                 " and (select DATEDIFF(YY,Date_OF_Birth,GEtdate()) from Child where childnumber = " & ChildID & " ) " & _
                 " between AgeFrom and AgeTo " & _
                 " ORDER BY b.ContestId"

        SqlHelper.FillDataset(cnTemp, CommandType.Text, strSQL, dsEligibleContests, tblEligibleContests)

        Dim strChildName As String
        strChildName = ""
        dgEligibleContests.Visible = False
        btnAddSelectedContests.Visible = False
        If Not dsEligibleContests Is Nothing Then
            If dsEligibleContests.Tables.Count > 0 Then
                dgEligibleContests.DataSource = dsEligibleContests.Tables(0).DefaultView
                dgEligibleContests.DataBind()
                If dsEligibleContests.Tables(0).Rows.Count > 0 Then
                    strChildName = dsEligibleContests.Tables(0).Rows(0).Item("ContestantName")
                    dgEligibleContests.Visible = True
                    btnAddSelectedContests.Visible = True
                End If
            End If
            End If

        If dgEligibleContests.Visible = False Then
            lblSelectedChild.Text = ddlChildren.SelectedItem.Text & " is not eligible for any contests."
        Else
            lblSelectedChild.Text = "Eligible Contests for " & ddlChildren.SelectedItem.Text & "."
            If dgEligibleContests.Items.Count > 0 Then
                dgEligibleContests.Visible = True
                btnAddSelectedContests.Visible = True
            Else
                dgEligibleContests.Visible = False
                btnAddSelectedContests.Visible = False
                lblSelectedChild.Text = lblSelectedChild.Text & " No more contests to select. "
            End If
        End If
    End Sub

    Private Sub DisplayRegionalEligibility(ByVal ChildID As Integer)
        Dim dsEligibleContests As DataSet = New DataSet
        Dim tblEligibleContests() As String = {"EligibleContests"}
        Dim prmArray(4) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ChildNumber"
        prmArray(0).Value = ChildID
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@ContestTypeID"
        prmArray(2).Value = ConfigurationManager.AppSettings("EventID")
        prmArray(2).Direction = ParameterDirection.Input

        prmArray(3) = New SqlParameter
        prmArray(3).ParameterName = "@IndID"
        prmArray(3).Value = Session("CustIndID")
        prmArray(3).Direction = ParameterDirection.Input

        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        SqlHelper.FillDataset(cnTemp, CommandType.StoredProcedure, "usp_GetEligibleContests", dsEligibleContests, tblEligibleContests, prmArray)

        Dim strChildName As String
        strChildName = ""
        If Not dsEligibleContests Is Nothing Then
            If dsEligibleContests.Tables.Count > 0 Then
                dgEligibleContests.SelectedIndex = -1
                Dim dvTemp As DataView = dsEligibleContests.Tables(0).DefaultView
                dvTemp.RowFilter = "SelectedContestCode = 0 "
                dgEligibleContests.DataSource = dvTemp
                dgEligibleContests.DataBind()
                If dsEligibleContests.Tables(0).Rows.Count > 0 Then
                    'get child name for display
                    strChildName = dsEligibleContests.Tables(0).Rows(0).Item("First_Name") & " " & dsEligibleContests.Tables(0).Rows(0).Item("Last_Name")
                Else
                End If

                'RegistrationDeadline
            End If
        End If

        If strChildName = "" Then
            lblSelectedChild.Text = ddlChildren.SelectedItem.Text & " is not eligible for any contests."
            dgEligibleContests.Visible = False
            btnAddSelectedContests.Visible = False
        Else
            lblSelectedChild.Text = "Eligible Contests for " & strChildName & "."
            If dgEligibleContests.Items.Count > 0 Then
                dgEligibleContests.Visible = True
                btnAddSelectedContests.Visible = True
            Else
                dgEligibleContests.Visible = False
                btnAddSelectedContests.Visible = False
                lblSelectedChild.Text = lblSelectedChild.Text & " No more contests to select. "
            End If
        End If
    End Sub

    Private Sub LoadChildren()
        Dim objChild As New Child
        Dim dsChild As New DataSet
        Dim strWhere As String

        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()

        strWhere = "MemberId='" & Session("CustIndID") & "' or SpouseID='" & Session("CustIndID") & "'"
        'objChild.SearchChildWhere(cnTemp.ConnectionString, dsChild, strWhere)
        objChild.SearchChildWhere(Application("ConnectionString"), dsChild, strWhere)

        If dsChild.Tables.Count > 0 Then
            If dsChild.Tables(0).Rows.Count > 0 Then
                Dim dvChildren As New DataView
                dvChildren = dsChild.Tables(0).DefaultView
                Dim dtMdate As Date = FormatDateTime(("10/1/" & (Application("ContestYear") - 1)), DateFormat.ShortDate)
                Dim strFilter As String = "((ModifyDate >= '" & dtMdate & "') or (CreateDate >= '" & dtMdate & "' and  ModifyDate is Null ))"
                dvChildren.RowFilter = strFilter

                ddlChildren.DataSource = dvChildren
                ddlChildren.DataBind()
                Session("ChildCount") = dsChild.Tables(0).Rows.Count
                ddlChildren.SelectedIndex = 0
                Session("SelectedChildID") = ddlChildren.SelectedValue
                DisplayEligibleContests(ddlChildren.SelectedValue)
            End If
        End If
        'lblDebug.Text = strWhere
        'lblDebug.Visible = True
    End Sub

    Protected Sub ddlChildren_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChildren.SelectedIndexChanged
        Session("SelectedChildID") = ddlChildren.SelectedValue
        lblWarning2.Text = ""
        lblWarning2.Visible = True
        DisplayEligibleContests(ddlChildren.SelectedValue)
    End Sub

    Private Sub RemoveContest_Click(ByVal source As System.Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgSelectedContests.ItemCommand
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
        If (Not (lbtn) Is Nothing) Then
            Dim ContestantID As Integer = CInt(e.Item.Cells(3).Text)

            Dim info As String = lbtn.CommandArgument
            Dim arInfo() As String = New String((2) - 1) {}
            ' define which character is seperating fields

            arInfo = Split(info, ",", , CompareMethod.Text)
            'CType(ViewState("SelectedContests"), ArrayList).Remove(arInfo(1).ToString & arInfo(2).ToString)

            If Session("EventID") = 1 Then
                If Not arInfo(3) Is Nothing And Convert.ToDecimal(arInfo(3)) > 0.0 Then
                    Session("LATEFEE") -= Convert.ToDecimal(arInfo(3))
                End If
            End If

            lblWarning1.text = ""
            Try
                If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
                Dim ChldNumber = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ChildNumber from contestant where contestant_id=" & ContestantID & "")
                SqlHelper.ExecuteNonQuery(cnTemp, CommandType.StoredProcedure, "Contestant_Delete", New SqlParameter("@ContestantID", ContestantID))
                DeleteMealCharge(ChldNumber)
            Catch ex As Exception
                lblWarning1.Text = ex.Message
                lblWarning1.Text = (lblWarning1.Text + "<BR>Remove Contest failed. Please try again.")
                Return
            End Try

            DisplaySelectedContests()
            DisplayEligibleContests(Session("SelectedChildID"))
        End If
    End Sub

    Private Sub DeleteMealCharge(ByVal ChldNumber As Integer)
        Dim strSQL As String
        strSQL = " Delete from MealCharge WHERE " & _
                 " ContestYear = " & Application("ContestYear") & " and Mealtype='Lunch' and ChildNumber = " & ChldNumber & _
                 " and contestdate not in (select contestdate FROM contest WHERE Contest_Year = " & Application("ContestYear") & _
                 " and contestid in (select contestcode FROM contestant WHERE ContestYear = " & Application("ContestYear") & _
                 " and childnumber = " & ChldNumber & " and chapterid = 1))"
        Try
            If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
            SqlHelper.ExecuteNonQuery(cnTemp, CommandType.Text, strSQL)
        Catch ex As Exception
            lblWarning1.Text = (lblWarning1.Text + "<BR>Remove MealCharge failed. Please try again.")
            Return
        End Try
    End Sub

    Protected Sub dgSelectedContests_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemCreated
        'Highlights row when Mouseover
        Dim gi As DataGridItem
        gi = e.Item
        Select Case gi.ItemType
            Case ListItemType.Item
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""white""")
            Case ListItemType.AlternatingItem
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""lightblue""")
            Case ListItemType.Header

        End Select
    End Sub

    Private Sub dgSelectedContests_ItemDataBound(ByVal sender As System.Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgSelectedContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lbtn As LinkButton = CType(e.Item.FindControl("lbRemoveContest"), LinkButton)
                Dim pdt As Object = DataBinder.Eval(e.Item.DataItem, "PaymentDate")
                'If Not lbtn Is Nothing Then
                'hide the remove option for every entry
                CType(e.Item.FindControl("lbRemoveContest"), LinkButton).Visible = False
                If Session("EventID") = "1" Then
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = Application("NationalFinalsCity")
                Else
                    Dim VolunteerName As String, VolunteerPhone As String, City As String, State As String
                    VolunteerName = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerName"), String)
                    VolunteerPhone = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerPhone"), String)
                    City = CType(DataBinder.Eval(e.Item.DataItem, "City"), String)
                    State = CType(DataBinder.Eval(e.Item.DataItem, "State"), String)

                    Dim sb As StringBuilder = New StringBuilder
                    sb.Append(VolunteerName)
                    sb.Append("<br />")
                    sb.Append(VolunteerPhone)
                    sb.Append("<br />")
                    sb.Append(City)
                    sb.Append(", ")
                    sb.Append(State)
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = sb.ToString
                End If
                'if payment date is not available it is considered as unpaid
                If IsDBNull(pdt) Then
                    'enable the remove contest option for unpaid entries
                    CType(e.Item.FindControl("lbRemoveContest"), LinkButton).Visible = True
                    Dim contestCode As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestID"), String)
                    Dim ContestAbbr As String = CType(DataBinder.Eval(e.Item.DataItem, "ContestAbbr"), String)
                    Dim ChildNumber As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
                    Dim ChapterID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ChapterId"), Integer)
                    Dim contestCategoryID As Integer = CType(DataBinder.Eval(e.Item.DataItem, "ContestCategoryID"), Integer)
                    If ViewState("SelectedContests") Is Nothing Then
                        ViewState("SelectedContests") = New ArrayList
                    End If
                    CType(ViewState("SelectedContests"), ArrayList).Add(ChildNumber.ToString & contestCategoryID.ToString)
                    If IsDate(DataBinder.Eval(e.Item.DataItem, "PaymentDate")) Then
                        If e.Item.DataItem("CreateDate") > e.Item.DataItem("RegistrationDeadline") Then
                            If Application("ContestType") = 1 Then
                                If DateDiff(DateInterval.Day, Today, Convert.ToDateTime(Application("LateRegistrationDate"))) > 0 Then
                                    lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & Application("LateFee")
                                Else
                                    lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & "0.0"
                                End If
                            End If
                        End If
                    Else
                        lbtn.CommandArgument = contestCode & "," + ChildNumber.ToString + "," + contestCategoryID.ToString & "," & "0.0"
                    End If
                    Dim fee As Decimal = CType(DataBinder.Eval(e.Item.DataItem, "Fee"), Decimal)
                    'acumulate fee total
                    nRegFee = (nRegFee + fee)
                    'gather all the contest codes so this is treated as item list in the payment via credit card
                    'Session("ContestsSelected") = (Session("ContestsSelected") _
                    '            + (ContestAbbr + ("(" _
                    '            + (ChildNumber.ToString + ") "))))
                    Session("ContestsSelected") = Session("ContestsSelected") & ContestAbbr & "(" & ChildNumber.ToString & ")(" & ChapterID.ToString & ") "
                Else
                    If Session("EventID") = 1 Then
                        '*****************************************
                        '*** Make sure the Contestant Photo is available  to enable the download link
                        '*****************************************
                        If Not IsDBNull(e.Item.DataItem("photo_image")) Then
                            If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                                Dim linkURL As String = "~/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                                Dim linkDOCURL As String = "~/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                                Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                                Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                                If ((Not (hlnk) Is Nothing) _
                                            AndAlso (linkURL <> "")) Then
                                    hlnk.Visible = True
                                    hlnk.NavigateUrl = linkURL
                                    hlnk.Target = "downloadmateriallink"
                                    hDOClnk.Visible = True
                                    hDOClnk.NavigateUrl = linkDOCURL
                                    hDOClnk.Target = "downloadmateriallink"
                                End If
                            End If
                        End If
                    Else
                        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                            Dim linkURL As String = "~/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                            Dim linkDOCURL As String = "~/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                            Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                            Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                            If ((Not (hlnk) Is Nothing) _
                                        AndAlso (linkURL <> "")) Then
                                hlnk.Visible = True
                                hlnk.NavigateUrl = linkURL
                                hlnk.Target = "downloadmateriallink"
                                hDOClnk.Visible = True
                                hDOClnk.NavigateUrl = linkDOCURL
                                hDOClnk.Target = "downloadmateriallink"
                            End If
                        End If
                    End If
                    ' ''    Dim score1 As Double = 0
                    ' ''    Dim score2 As Double = 0
                    ' ''    Dim score3 As Double = 0
                    ' ''    Dim rank As Integer = 0
                    ' ''    Dim finalscore1 As Double = 0
                    ' ''    Dim finalscore2 As Double = 0
                    ' ''    Dim finalpercentile As Decimal = 0
                    ' ''    Dim finalrank As Integer = 0
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score1")) Then
                    ' ''        score1 = CType(DataBinder.Eval(e.Item.DataItem, "Score1"), Double)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score2")) Then
                    ' ''        score2 = CType(DataBinder.Eval(e.Item.DataItem, "Score2"), Double)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score3")) Then
                    ' ''        score3 = CType(DataBinder.Eval(e.Item.DataItem, "Score3"), Double)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Rank")) Then
                    ' ''        rank = CType(DataBinder.Eval(e.Item.DataItem, "Rank"), Integer)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1")) Then
                    ' ''        finalscore1 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1"), Double)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2")) Then
                    ' ''        finalscore2 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2"), Double)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Rank")) Then
                    ' ''        finalrank = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Rank"), Integer)
                    ' ''    End If
                    ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile")) Then
                    ' ''        finalpercentile = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile"), Decimal)
                    ' ''    End If
                    ' ''    Dim lblScore As Label = CType(e.Item.FindControl("lblScore"), Label)
                    ' ''    If (Not (lblScore) Is Nothing) Then
                    ' ''        If (score1 _
                    ' ''                    + (score2 _
                    ' ''                    + (score3 > 0))) Then
                    ' ''            Dim sb As StringBuilder = New StringBuilder
                    ' ''            sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
                    ' ''            sb.Append("<tr bgcolor=lightblue forecolor=white>")
                    ' ''            sb.Append("<td width=20%><b>Scores</b></td>")
                    ' ''            sb.Append("<td width=20%>Phase 1</td>")
                    ' ''            sb.Append("<td width=20%>Phase 2</td>")
                    ' ''            sb.Append("<td width=20%>Phase 3</td>")
                    ' ''            sb.Append("<td width=20%>Total</td>")
                    ' ''            sb.Append("<td width=20%>Percentile</td>")
                    ' ''            sb.Append("<td width=20%>Rank</td>")
                    ' ''            sb.Append("</tr>")
                    ' ''            sb.Append("<tr>")
                    ' ''            sb.Append("<td width=20%>Regionals</td>")
                    ' ''            If (score1 > 0) Then
                    ' ''                sb.Append(("<td width=20%>" _
                    ' ''                                + (score1.ToString + "</td>")))
                    ' ''            Else
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''            End If
                    ' ''            If (score2 > 0) Then
                    ' ''                sb.Append(("<td width=20%>" _
                    ' ''                                + (score2.ToString + "</td>")))
                    ' ''            Else
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''            End If
                    ' ''            If (score3 > 0) Then
                    ' ''                sb.Append(("<td width=20%>" _
                    ' ''                                + (score3.ToString + "</td>")))
                    ' ''            Else
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''            End If
                    ' ''            If (score1 _
                    ' ''                        + (score2 _
                    ' ''                        + (score3 > 0))) Then
                    ' ''                sb.Append(("<td width=20%>" _
                    ' ''                                + ((score1 _
                    ' ''                                + (score2 + score3)).ToString + "</td>")))
                    ' ''            Else
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''            End If
                    ' ''            sb.Append("<td width=20%>na</td>")
                    ' ''            'no percentile for regionals
                    ' ''            If (rank > 0) Then
                    ' ''                sb.Append(("<td width=20%>" _
                    ' ''                                + (rank.ToString + "</td>")))
                    ' ''            Else
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''            End If
                    ' ''            sb.Append("</tr>")
                    ' ''            If (finalscore1 _
                    ' ''                        + (finalscore2 > 0)) Then
                    ' ''                sb.Append("<tr>")
                    ' ''                sb.Append("<td width=20%>Finals</td>")
                    ' ''                If (finalscore1 > 0) Then
                    ' ''                    sb.Append(("<td width=20%>" _
                    ' ''                                    + (finalscore1.ToString + "</td>")))
                    ' ''                Else
                    ' ''                    sb.Append("<td width=20%>na</td>")
                    ' ''                End If
                    ' ''                If (finalscore2 > 0) Then
                    ' ''                    sb.Append(("<td width=20%>" _
                    ' ''                                    + (finalscore2.ToString + "</td>")))
                    ' ''                Else
                    ' ''                    sb.Append("<td width=20%>na</td>")
                    ' ''                End If
                    ' ''                sb.Append("<td width=20%>na</td>")
                    ' ''                If (finalscore1 _
                    ' ''                            + (finalscore2 > 0)) Then
                    ' ''                    sb.Append(("<td width=20%>" _
                    ' ''                                    + ((finalscore1 + finalscore2).ToString + "</td>")))
                    ' ''                Else
                    ' ''                    sb.Append("<td width=20%>na</td>")
                    ' ''                End If
                    ' ''                If (finalpercentile > 0) Then
                    ' ''                    sb.Append(("<td width=20%>" _
                    ' ''                                    + (finalpercentile.ToString + "</td>")))
                    ' ''                Else
                    ' ''                    sb.Append("<td width=20%>na</td>")
                    ' ''                End If
                    ' ''                If (finalrank > 0) Then
                    ' ''                    sb.Append(("<td width=20%>" _
                    ' ''                                    + (finalrank.ToString + "</td>")))
                    ' ''                Else
                    ' ''                    sb.Append("<td width=20%>na</td>")
                    ' ''                End If
                    ' ''                sb.Append("</tr>")
                    ' ''            End If
                    ' ''            sb.Append("</table>")
                    ' ''            lblScore.Text = sb.ToString
                    ' ''        End If
                    ' ''    End If
                End If
                If Session("ContestDate1") Is Nothing Then
                    Try
                        Session("ContestDate1") = Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString
                    Catch ex As Exception
                        Exit Try
                    End Try
                Else
                    Try
                        If Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString <> Session("ContestDate1") Then
                            If Session("ContestDate2") Is Nothing Then
                                Session("ContestDate2") = Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString
                            End If
                        End If
                    Catch ex As Exception
                        Exit Try
                    End Try
                End If
        End Select
    End Sub

    Protected Sub dgEligibleContests_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEligibleContests.ItemCreated
        'Highlights row when Mouseover
        Dim gi As DataGridItem
        gi = e.Item
        Select Case gi.ItemType
            Case ListItemType.Item
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""white""")
            Case ListItemType.AlternatingItem
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""lightblue""")
            Case ListItemType.Header
        End Select
    End Sub

    Protected Sub dgEligibleContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgEligibleContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim ProductID As Integer, ProductGroupID As Integer, ChildNumber As Integer

                ChildNumber = CType(DataBinder.Eval(e.Item.DataItem, "ChildNumber"), Integer)
                ProductID = CType(DataBinder.Eval(e.Item.DataItem, "ProductID"), Integer)
                ProductGroupID = CType(DataBinder.Eval(e.Item.DataItem, "ProductGroupID"), Integer)
                Select Case Session("EventID")
                    Case 1
                        If NationalContestPaid(ChildNumber, ProductGroupID, ProductID) = True Then
                            CType(e.Item.FindControl("chkSelectContest"), CheckBox).Visible = False
                        Else
                            CType(e.Item.FindControl("chkSelectContest"), CheckBox).Visible = True
                        End If
                    Case 2
                        If ContestPaid(ChildNumber, ProductGroupID, ProductID) = True Then
                            CType(e.Item.FindControl("chkSelectContest"), CheckBox).Visible = False
                        Else
                            CType(e.Item.FindControl("chkSelectContest"), CheckBox).Visible = True
                        End If
                End Select

        End Select
    End Sub

    Protected Sub btnAddSelectedContests_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddSelectedContests.Click
        'sandhya - this piece of code is added because of session timeout issues
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        Dim intCtr As Integer, blnAddContests As Boolean
        Dim intChildNumber As Integer, intContestId As Integer

        blnAddContests = False
        For intCtr = 0 To dgEligibleContests.Items.Count - 1
            Dim myTableCell As TableCell
            myTableCell = dgEligibleContests.Items.SyncRoot.Item(intCtr).Cells(0)
            Dim chkSelected As CheckBox = myTableCell.Controls(1)
            If chkSelected.Checked = True Then
                If Session("CustIndID") = 0 Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                    Exit Sub
                End If
                myTableCell = dgEligibleContests.Items.SyncRoot.Item(intCtr).Cells(2)
                intChildNumber = CInt(myTableCell.Text)
                myTableCell = dgEligibleContests.Items.SyncRoot.Item(intCtr).Cells(3)
                intContestId = CInt(myTableCell.Text)
                If IsThisDataInDB(intChildNumber, intContestId) = False Then
                    If InsertContests(intCtr) = False Then
                        lblWarning2.Text = "Problems inserting records. " & lblWarning2.Text
                    Else
                        'lblWarning2.Text = intChildNumber
                        blnAddContests = True
                    End If
                End If
                lblWarning2.Visible = True
            End If
        Next
        lblSelectedChild.Text = ""
        If blnAddContests = True Then
            InsertContestantsForMeal()
            DisplaySelectedContests()
            DisplayEligibleContests(Session("SelectedChildID"))
        End If
    End Sub

    Private Sub InsertContestantsForMeal()
        Dim dsContestant As New DataSet, drNew As DataRow, prmArrayInsert(12) As SqlParameter
        Dim tblConestant() As String = {"ContestantForMC"}

        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        SqlHelper.FillDataset(cnTemp, CommandType.StoredProcedure, "usp_GetNationalContests", dsContestant, tblConestant, prmArray)

        If dsContestant.Tables(0).Rows.Count > 0 Then
            For Each drNew In dsContestant.Tables(0).Rows
                Try
                    Dim amount As Decimal = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "(select mealday1 as MealAmt from WeekCALENDAR where SatDay1='" & drNew.Item("ContestDate") & "') Union select mealday2 as MealAmt from WeekCALENDAR where SunDay2='" & drNew.Item("ContestDate") & "'")
                    '** Ferdine Silvaa Testing
                    'Response.Write("<BR>" & amount)
                    prmArrayInsert(0) = New SqlParameter("@EventName", "Finals")
                    prmArrayInsert(1) = New SqlParameter("@ContestYear", Application("ContestYear"))
                    prmArrayInsert(2) = New SqlParameter("@AutoMemberID", drNew.Item("ParentID"))
                    prmArrayInsert(3) = New SqlParameter("@ChildNumber", drNew.Item("ChildNumber"))
                    prmArrayInsert(4) = New SqlParameter("@ContestDate", drNew.Item("ContestDate"))
                    prmArrayInsert(5) = New SqlParameter("@Name", drNew.Item("ContestantName"))
                    prmArrayInsert(6) = New SqlParameter("@ModifiedDate", Now)
                    prmArrayInsert(7) = New SqlParameter("@ModifiedUserID", Session("CustIndID"))
                    prmArrayInsert(8) = New SqlParameter("@Gender", drNew.Item("Gender"))
                    prmArrayInsert(9) = New SqlParameter("@Amount", amount)
                    prmArrayInsert(10) = New SqlParameter("@MealType", "Lunch")
                    prmArrayInsert(11) = New SqlParameter("@RetValue", SqlDbType.Int)
                    prmArrayInsert(11).Direction = ParameterDirection.Output
                    If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
                    SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertContestantForMealCharge", prmArrayInsert)
                Catch ex As SqlException
                    Response.Write(ex.ToString())
                End Try
            Next
        End If
    End Sub


    Private Function IsThisDataInDB(ByVal ChildNumber As Integer, ByVal ContestId As Integer) As Boolean
        Dim dsDataInDB As New DataSet
        Dim strSQL As String

        IsThisDataInDB = False
        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        strSQL = " Select * from Contestant WHERE " & _
                 " ContestYear = " & Application("ContestYear") & _
                 " and ChapterId = 1 and ChildNumber = " & ChildNumber & _
                 " and ContestCode = " & ContestId
        dsDataInDB = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, strSQL)

        If Not dsDataInDB Is Nothing Then
            If dsDataInDB.Tables(0).Rows.Count > 0 Then
                IsThisDataInDB = True
            End If
        End If

    End Function

    Private Function InsertContests(ByVal GridItem As Integer) As Boolean
        Dim ChildNumber As Int32, ContestID As Int32, Fee As Decimal
        Dim ContestCategoryID As Int32, ChapterID As Int32
        Dim EventID As Int32, EventCode As String
        Dim ProductGroupID As Int32, ProductGroupCode As String
        Dim ProductID As Int32, ProductCode As String
        Dim myTableCell As TableCell
        Dim param(16) As SqlParameter
        Dim RegDeadline As Date
        If Session("CustIndID") = 0 Then
            Return False
            Exit Function
        End If

        

        'ChildNumber
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(2)
        ChildNumber = CInt(myTableCell.Text)

        'ContestID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(3)
        ContestID = CInt(myTableCell.Text)

        'Fee
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(4)
        Fee = Convert.ToDecimal(Mid(myTableCell.Text, 2))

        'ContestCategoryID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(6)
        ContestCategoryID = CInt(myTableCell.Text)

        'EventID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(7)
        EventID = CInt(myTableCell.Text)

        'EventCode
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(8)
        EventCode = CStr(myTableCell.Text)

        'ProductGroupID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(9)
        ProductGroupID = CInt(myTableCell.Text)

        'ProductGroupCode
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(10)
        ProductGroupCode = CStr(myTableCell.Text)

        'ProductID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(11)
        ProductID = CInt(myTableCell.Text)

        'ProductCode
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(12)
        ProductCode = CStr(myTableCell.Text)

        'ChapterID
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(13)
        ChapterID = CInt(myTableCell.Text)

        'RegDeadline By ** Ferdine Silvaa
        myTableCell = dgEligibleContests.Items.SyncRoot.Item(GridItem).Cells(14)
        RegDeadline = Date.Parse(myTableCell.Text)
        If RegDeadline < Now.Date Then
            lblWarning2.Text = lblWarning2.Text & " <br> Sorry Registration Deadline Passed for " & ProductCode
            Return False
            Exit Function
        Else
            lblWarning2.Visible = False
        End If
        Dim SQLstr As String = "select count(*) from contestant where contestyear = " & Now.Year & " and EventID = " & EventID & " and productid = " & ProductID & " and chapterid = " & ChapterID & " and badgenumber is not null"
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLstr) > 0 Then
            lblWarning2.Text = lblWarning2.Text & " <br> Since badge numbers were already issued, So You can't register for this contest : " & ProductCode
            Return False
            Exit Function
        End If

        Try
            param(0) = New SqlParameter("@ChapterID", ChapterID)
            param(1) = New SqlParameter("@ContestID", ContestID)
            param(2) = New SqlParameter("@ContestYear", Application("ContestYear"))
            param(3) = New SqlParameter("@ParentID", Session("CustIndID"))
            param(4) = New SqlParameter("@ChildNumber", ChildNumber)
            param(5) = New SqlParameter("@Fee", Fee)
            param(6) = New SqlParameter("@ContestCategoryID", ContestCategoryID)
            param(7) = New SqlParameter("@EventId", EventID)
            param(8) = New SqlParameter("@EventCode", EventCode)
            param(9) = New SqlParameter("@ProductGroupId", ProductGroupID)
            param(10) = New SqlParameter("@ProductGroupCode", ProductGroupCode)
            param(11) = New SqlParameter("@ProductId", ProductID)
            param(12) = New SqlParameter("@ProductCode", ProductCode)
            param(13) = New SqlParameter("@CreateDate", Now)
            param(14) = New SqlParameter("@CreatedBy", Session("CustIndID"))
            param(15) = New SqlParameter("@RetValue", SqlDbType.Int)
            param(15).Direction = ParameterDirection.Output

            If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
            If Session("CustIndID") = 0 Then
                Return False
                Exit Function
            End If

            SqlHelper.ExecuteScalar(cnTemp, CommandType.StoredProcedure, "usp_InsertContestant", param)
        Catch ex As SqlException
            lblWarning2.Text = lblWarning2.Text & " " & ex.Message & ContestCategoryID
            Return False
        End Try
        Return True
    End Function

    Private Function NationalContestPaid(ByVal ChildID As Integer, ByVal ProductGroupId As Integer, ByVal ProductID As Integer) As Boolean
        Dim strSQL As String, dsTarget As DataSet
        'Not only paid but also the pending
        '" AND childnumber = " & ChildID & " AND paymentdate is not null and productgroupid = " & _
        strSQL = "Select * from contestant where contestyear = " & Application("ContestYear") & _
                " AND childnumber = " & ChildID & " and productgroupid = " & _
                ProductGroupId & " AND productid = " & ProductID & " AND ChapterID = 1 "

        dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, strSQL)
        If dsTarget.Tables(0).Rows.Count > 0 Then
            NationalContestPaid = True
        Else
            NationalContestPaid = False
        End If
    End Function

    Private Function ContestPaid(ByVal ChildID As Integer, ByVal ProductGroupId As Integer, ByVal ProductID As Integer) As Boolean
        Dim strSQL As String, dsTarget As DataSet
        strSQL = "Select * from contestant where contestyear = " & Application("ContestYear") & _
                " AND childnumber = " & ChildID & " AND paymentdate is not null and productgroupid = " & _
                ProductGroupId & " AND productid = " & ProductID

        dsTarget = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, strSQL)
        If dsTarget.Tables(0).Rows.Count > 0 Then
            ContestPaid = True
        Else
            ContestPaid = False
        End If
    End Function

    Private Sub DisplayPaidContests()
        Select Case Session("EventID")
            Case 1
                DisplayNationalPaidContests()
            Case 2
                DisplayRegionalPaidContests()
            Case Else
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End Select
    End Sub

    Private Sub DisplayNationalPaidContests()
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}
        Dim strSQL As String

        strSQL = ""
        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()

        strSQL = " SELECT b.ContestDate, b.StartTime as ContestTime, b.RegistrationDeadline, b.CheckInTime, " & _
                 " b.EventID, b.EventCode, b.ProductID, b.ProductCode, b.ProductGroupID, b.ProductGroupCode, b.StartTime, b.EndTime, " & _
                 " c.NationalFinalsFee as Fee, a.ContestCode as ContestID, " & _
                 " c.ContestCategoryID, c.ContestCode as ContestAbbr, " & _
                 " c.ContestDesc, a.ContestYear, a.ParentID, a.ChildNumber, " & _
                 " REPLACE(e.FIRST_NAME + ' ' + COALESCE (e.MIDDLE_INITIAL, '') + ' ' + e.LAST_NAME, ' ', ' ') AS ContestantName, " & _
                 " a.contestant_id, a.NationalInvitee, b.NSFChapterID,'' as State,'' as City, a.PaymentDate, a.PaymentReference, " & _
                 " '' as VolunteerName, '' as VolunteerPhone, a.photo_image " & _
                " FROM Contestant a INNER JOIN Contest b " & _
                " On b.ContestID = a.Contestcode " & _
                " INNER JOIN ContestCategory c " & _
                " ON b.ContestCategoryID = c.ContestCategoryID " & _
                " INNER JOIN Child e ON a.ChildNumber = e.ChildNumber " & _
                " AND a.ParentID = e.MEMBERID WHERE (a.ParentID =  " & Session("CustIndID") & " ) " & _
                " and a.ContestYear =    " & Application("ContestYear") & "   and b.EventID = 1 " & _
                " ORDER BY ContestId  "

        SqlHelper.FillDataset(cnTemp, CommandType.Text, strSQL, dsContestant, tblConestant)

        Dim strWhere As String
        Dim dvPaidContests As DataView
        dvPaidContests = dsContestant.Tables(0).DefaultView
        strWhere = " PaymentDate IS NOT Null "
        dvPaidContests.RowFilter = strWhere

        If dvPaidContests.Count > 0 Then
            pnlPaidContests.Visible = True
            dgPaidContests.DataSource = dvPaidContests
            dgPaidContests.DataBind()
        Else
            pnlPaidContests.Visible = False
        End If

    End Sub

    Private Sub DisplayRegionalPaidContests()
        Dim dsContestant As New DataSet
        Dim tblConestant() As String = {"Contestant"}

        Dim prmArray(3) As SqlParameter
        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ParentID"
        prmArray(0).Value = Session("CustIndID")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@LateFeeDate"
        prmArray(2).Value = IIf(Session("EventID").ToString = "1", Application("LateRegistrationDate"), DBNull.Value)
        prmArray(2).Direction = ParameterDirection.Input

        If cnTemp.State = ConnectionState.Closed Then cnTemp.Open()
        SqlHelper.FillDataset(cnTemp, CommandType.StoredProcedure, "usp_GetContests", dsContestant, tblConestant, prmArray)

        Dim strWhere As String
        Dim dvPaidContests As DataView
        dvPaidContests = dsContestant.Tables(0).DefaultView
        strWhere = " PaymentDate IS NOT NULL "
        dvPaidContests.RowFilter = strWhere

        If dvPaidContests.Count > 0 Then
            dvPaidContests = dsContestant.Tables(0).DefaultView
            dvPaidContests.RowFilter = strWhere & " AND TeamLead = 'Y'"

            pnlPaidContests.Visible = True
            If dvPaidContests.Count <= 0 Then
                dvPaidContests = dsContestant.Tables(0).DefaultView
                dvPaidContests.RowFilter = strWhere
            End If
            dgPaidContests.DataSource = dvPaidContests
            dgPaidContests.DataBind()
        Else
            pnlPaidContests.Visible = False
        End If

    End Sub

    Protected Sub dgPaidContests_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaidContests.ItemCreated
        'Highlights row when Mouseover
        Dim gi As DataGridItem
        gi = e.Item
        Select Case gi.ItemType
            Case ListItemType.Item
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""white""")
            Case ListItemType.AlternatingItem
                gi.Attributes.Add("onmouseover", "this.style.backgroundColor=""#FFE0C0""")
                gi.Attributes.Add("onmouseout", "this.style.backgroundColor=""lightblue""")
        End Select
    End Sub

    Protected Sub dgPaidContests_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgPaidContests.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim pdt As Object = DataBinder.Eval(e.Item.DataItem, "PaymentDate")
                If Session("EventID").ToString = "1" Then
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = Application("NationalFinalsCity")
                Else
                    Dim VolunteerName As String, VolunteerPhone As String, City As String, State As String
                    VolunteerName = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerName"), String)
                    VolunteerPhone = CType(DataBinder.Eval(e.Item.DataItem, "VolunteerPhone"), String)
                    City = CType(DataBinder.Eval(e.Item.DataItem, "City"), String)
                    State = CType(DataBinder.Eval(e.Item.DataItem, "State"), String)

                    Dim sb As StringBuilder = New StringBuilder
                    sb.Append(VolunteerName)
                    sb.Append("<br />")
                    sb.Append(VolunteerPhone)
                    sb.Append("<br />")
                    sb.Append(City)
                    sb.Append(", ")
                    sb.Append(State)
                    CType(e.Item.FindControl("lblContactInfo"), Label).Text = sb.ToString
                End If
                'Sandhya - Commented as per Ratnam uncles's instructions not to display download links
                ' ''If Session("EventID").ToString = "1" Then
                ' ''    '*****************************************
                ' ''    '*** Make sure the Contestant Photo is available  to enable the download link
                ' ''    '*****************************************
                ' ''    If Not IsDBNull(e.Item.DataItem("photo_image")) Then
                ' ''        If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                ' ''            Dim linkURL As String = "~/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                ' ''            Dim linkDOCURL As String = "~/NationalCoordinator/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                ' ''            Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                ' ''            Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                ' ''            If ((Not (hlnk) Is Nothing) _
                ' ''                        AndAlso (linkURL <> "")) Then
                ' ''                hlnk.Visible = True
                ' ''                hlnk.NavigateUrl = linkURL
                ' ''                hlnk.Target = "downloadmateriallink"
                ' ''                hDOClnk.Visible = True
                ' ''                hDOClnk.NavigateUrl = linkDOCURL
                ' ''                hDOClnk.Target = "downloadmateriallink"
                ' ''            End If
                ' ''        End If
                ' ''    End If
                ' ''Else
                ' ''    'Sandhya - Commented as per Ratnam uncles's instructions not to display download links
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "DownloadLink")) Then
                ' ''        Dim linkURL As String = "~/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String)
                ' ''        Dim linkDOCURL As String = "~/include/" & CType(DataBinder.Eval(e.Item.DataItem, "DownloadLink"), String).Replace(".pdf", ".doc")

                ' ''        Dim hlnk As HyperLink = CType(e.Item.FindControl("hlDownloadLink"), HyperLink)
                ' ''        Dim hDOClnk As HyperLink = CType(e.Item.FindControl("hlDOCDownloadLink"), HyperLink)

                ' ''        If ((Not (hlnk) Is Nothing) _
                ' ''                    AndAlso (linkURL <> "")) Then
                ' ''            hlnk.Visible = True
                ' ''            hlnk.NavigateUrl = linkURL
                ' ''            hlnk.Target = "downloadmateriallink"
                ' ''            hDOClnk.Visible = True
                ' ''            hDOClnk.NavigateUrl = linkDOCURL
                ' ''            hDOClnk.Target = "downloadmateriallink"
                ' ''        End If
                ' ''    End If
                ' ''    Dim score1 As Double = 0
                ' ''    Dim score2 As Double = 0
                ' ''    Dim score3 As Double = 0
                ' ''    Dim rank As Integer = 0
                ' ''    Dim finalscore1 As Double = 0
                ' ''    Dim finalscore2 As Double = 0
                ' ''    Dim finalpercentile As Decimal = 0
                ' ''    Dim finalrank As Integer = 0
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score1")) Then
                ' ''        score1 = CType(DataBinder.Eval(e.Item.DataItem, "Score1"), Double)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score2")) Then
                ' ''        score2 = CType(DataBinder.Eval(e.Item.DataItem, "Score2"), Double)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Score3")) Then
                ' ''        score3 = CType(DataBinder.Eval(e.Item.DataItem, "Score3"), Double)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Rank")) Then
                ' ''        rank = CType(DataBinder.Eval(e.Item.DataItem, "Rank"), Integer)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1")) Then
                ' ''        finalscore1 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase1"), Double)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2")) Then
                ' ''        finalscore2 = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Phase2"), Double)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Rank")) Then
                ' ''        finalrank = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Rank"), Integer)
                ' ''    End If
                ' ''    If Not IsDBNull(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile")) Then
                ' ''        finalpercentile = CType(DataBinder.Eval(e.Item.DataItem, "Finals_Percentile"), Decimal)
                ' ''    End If
                ' ''    Dim lblScore As Label = CType(e.Item.FindControl("lblScore"), Label)
                ' ''    If (Not (lblScore) Is Nothing) Then
                ' ''        If (score1 _
                ' ''                    + (score2 _
                ' ''                    + (score3 > 0))) Then
                ' ''            Dim sb As StringBuilder = New StringBuilder
                ' ''            sb.Append("<table border=1 width=100%  cellspacing=0 cellpadding=0>")
                ' ''            sb.Append("<tr bgcolor=lightblue forecolor=white>")
                ' ''            sb.Append("<td width=20%><b>Scores</b></td>")
                ' ''            sb.Append("<td width=20%>Phase 1</td>")
                ' ''            sb.Append("<td width=20%>Phase 2</td>")
                ' ''            sb.Append("<td width=20%>Phase 3</td>")
                ' ''            sb.Append("<td width=20%>Total</td>")
                ' ''            sb.Append("<td width=20%>Percentile</td>")
                ' ''            sb.Append("<td width=20%>Rank</td>")
                ' ''            sb.Append("</tr>")
                ' ''            sb.Append("<tr>")
                ' ''            sb.Append("<td width=20%>Regionals</td>")
                ' ''            If (score1 > 0) Then
                ' ''                sb.Append(("<td width=20%>" _
                ' ''                                + (score1.ToString + "</td>")))
                ' ''            Else
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''            End If
                ' ''            If (score2 > 0) Then
                ' ''                sb.Append(("<td width=20%>" _
                ' ''                                + (score2.ToString + "</td>")))
                ' ''            Else
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''            End If
                ' ''            If (score3 > 0) Then
                ' ''                sb.Append(("<td width=20%>" _
                ' ''                                + (score3.ToString + "</td>")))
                ' ''            Else
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''            End If
                ' ''            If (score1 _
                ' ''                        + (score2 _
                ' ''                        + (score3 > 0))) Then
                ' ''                sb.Append(("<td width=20%>" _
                ' ''                                + ((score1 _
                ' ''                                + (score2 + score3)).ToString + "</td>")))
                ' ''            Else
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''            End If
                ' ''            sb.Append("<td width=20%>na</td>")
                ' ''            'no percentile for regionals
                ' ''            If (rank > 0) Then
                ' ''                sb.Append(("<td width=20%>" _
                ' ''                                + (rank.ToString + "</td>")))
                ' ''            Else
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''            End If
                ' ''            sb.Append("</tr>")
                ' ''            If (finalscore1 _
                ' ''                        + (finalscore2 > 0)) Then
                ' ''                sb.Append("<tr>")
                ' ''                sb.Append("<td width=20%>Finals</td>")
                ' ''                If (finalscore1 > 0) Then
                ' ''                    sb.Append(("<td width=20%>" _
                ' ''                                    + (finalscore1.ToString + "</td>")))
                ' ''                Else
                ' ''                    sb.Append("<td width=20%>na</td>")
                ' ''                End If
                ' ''                If (finalscore2 > 0) Then
                ' ''                    sb.Append(("<td width=20%>" _
                ' ''                                    + (finalscore2.ToString + "</td>")))
                ' ''                Else
                ' ''                    sb.Append("<td width=20%>na</td>")
                ' ''                End If
                ' ''                sb.Append("<td width=20%>na</td>")
                ' ''                If (finalscore1 _
                ' ''                            + (finalscore2 > 0)) Then
                ' ''                    sb.Append(("<td width=20%>" _
                ' ''                                    + ((finalscore1 + finalscore2).ToString + "</td>")))
                ' ''                Else
                ' ''                    sb.Append("<td width=20%>na</td>")
                ' ''                End If
                ' ''                If (finalpercentile > 0) Then
                ' ''                    sb.Append(("<td width=20%>" _
                ' ''                                    + (finalpercentile.ToString + "</td>")))
                ' ''                Else
                ' ''                    sb.Append("<td width=20%>na</td>")
                ' ''                End If
                ' ''                If (finalrank > 0) Then
                ' ''                    sb.Append(("<td width=20%>" _
                ' ''                                    + (finalrank.ToString + "</td>")))
                ' ''                Else
                ' ''                    sb.Append("<td width=20%>na</td>")
                ' ''                End If
                ' ''                sb.Append("</tr>")
                ' ''            End If
                ' ''            sb.Append("</table>")
                ' ''            lblScore.Text = sb.ToString
                ' ''        End If
                ' ''    End If
                ' ''End If
        End Select
    End Sub

    Protected Function CheckBadgeNumber() As Boolean
        Dim i As Integer
        Dim myTableCell As TableCell
        Dim contestID As Integer
        Dim flag As Boolean = False
        If dgSelectedContests.Items.Count > 0 Then
            For i = 0 To dgSelectedContests.Items.Count - 1
                myTableCell = dgSelectedContests.Items(i).Cells(1)
                contestID = CInt(myTableCell.Text)
                ' MsgBox("select count(C.contestant_id) from contestant C, Contest Cn where C.contestyear = Cn.Contest_Year and C.EventID = cn.EventId and C.productid = Cn.ProductId and C.chapterid = Cn.NSFChapterID and C.badgenumber is not null and Cn.ContestID=" & contestID & "")
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(C.contestant_id) from contestant C, Contest Cn where C.contestyear = Cn.Contest_Year and C.EventID = cn.EventId and C.productid = Cn.ProductId and C.chapterid = Cn.NSFChapterID and C.badgenumber is not null and Cn.ContestID=" & contestID & "") > 0 Then
                    Return True
                    Exit Function
                End If
            Next
        End If
        Return flag
    End Function


    Protected Sub btnPayNow_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPayNow.Click
        If Session("EventID").ToString = "1" Then 'National
            'Check whether selection made
            'Response.Write("<br>" & dgPaidContests.Items.Count)
            If Session("ContestsSelected") = "" And dgPaidContests.Items.Count = 0 Then
                lblNoContestMsg.ForeColor = Drawing.Color.Red
                lblNoContestMsg.Text = "No contests are selected.  You cannot proceed to the next page."
            ElseIf CheckBadgeNumber() = True Then
                lblNoContestMsg.ForeColor = Drawing.Color.Red
                lblNoContestMsg.Text = "Since badge numbers were already issued, please remove the pending selections; otherwise you will get charged, but cannot participate."
            Else
                Dim redirectURL As String
                '''''         redirectURL = "~/Parents/RegistrationGuestChaperone.aspx"
                redirectURL = "~/MealCharge.aspx"
                Page.Response.Redirect(redirectURL)
            End If
        Else
            '** Ferdine Silva
            Dim i As Integer
            Dim flag As Boolean = False
            For i = 0 To dgSelectedContests.Items.Count - 1
                Dim RegDeadline As Date
                Dim myTableCell As TableCell
                'RegDeadline By ** Ferdine Silvaa
                myTableCell = dgSelectedContests.Items.SyncRoot.Item(i).Cells(14)
                RegDeadline = Date.Parse(myTableCell.Text)
                If RegDeadline < Now.Date Then
                    lblWarning2.Text = lblWarning2.Text & " <br> Sorry Registration Deadline Passed"
                    flag = True
                    Exit For
                End If
            Next
            If flag = False Then
                Session("contestSelSumNo") = 0
                Page.Response.Redirect("contestSelectionSum.aspx")
            End If
            '**Page.Response.Redirect("TermsAndConditions.aspx")
        End If
    End Sub

    Protected Sub btnAddChild_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddChild.Click
        Page.Response.Redirect("AddChild.aspx")
    End Sub
End Class
