﻿Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class DataAnalytics
    Inherits System.Web.UI.Page
    Dim cmdText As String
    Dim ds As DataSet
    Protected Sub Page_Load(sender As Object, e As EventArgs) Handles Me.Load
        If ddlReportType.SelectedValue = "4" Then
            ddlProductGrp.Enabled = True
            ddlYearCount.Enabled = False
        Else
            ddlProductGrp.Enabled = False
            ddlYearCount.Enabled = True
        End If
        If Page.IsPostBack = False Then
            FillProductGroup()
            FillYear()
            FillReportType()
        End If
    End Sub
    Private Sub FillProductGroup()
        Dim dsPrdGrp As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT ProductGroupCode, Name FROM Productgroup Where Eventid=2")
        ddlProductGrp.DataSource = dsPrdGrp.Tables(0)
        ddlProductGrp.DataBind()
        ddlProductGrp.Items.Insert(0, New ListItem("Select", "-1"))
        ddlProductGrp.Items.Add(New ListItem("All", "All"))
    End Sub
    Private Sub FillYear()
        Dim i As Integer
        For i = 5 To 15 Step 5
            ddlYearCount.Items.Add(i.ToString)
        Next
        ddlYearCount.Items(1).Selected = True
    End Sub
    Private Sub FillReportType()
        ddlReportType.Items.Add(New ListItem("Trends in NSF Families", 1))
        ddlReportType.Items.Add(New ListItem("Trends in NSF Families by Contest", 2))
        ddlReportType.Items.Add(New ListItem("Turnover of New Families", 3))
        ddlReportType.Items.Add(New ListItem("Learning Outcomes", 4))
    End Sub

    

    Private Sub Export_LearningOutComes()
        ddlYearCount.Enabled = False
        ddlProductGrp.Enabled = True
        If ViewState("LearningOurComes") Is Nothing Then
            Exit Sub
        End If
        Dim dt As DataTable = ViewState("LearningOurComes")
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "Learning_OutComes_" & ddlProductGrp.SelectedValue & "_" & Now.ToShortDateString & ".xls"
        oSheet.Range("A1:D1").MergeCells = True
        oSheet.Range("A1").Value = "Learning Outcomes"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True
        ' Years of Participation	
        oSheet.Range("A2").Value = "Years of Participation"
        oSheet.Range("A2").Font.Bold = True
        'Number of Children	
        oSheet.Range("B2").Value = "Number of Children"
        oSheet.Range("B2").Font.Bold = True
        '#of Children in top 10 ranks	
        oSheet.Range("C2").Value = "#of Children in top 10 ranks"
        oSheet.Range("C2").Font.Bold = True
        'Percent
        oSheet.Range("D2").Value = "Percent"
        oSheet.Range("D2").Font.Bold = True
        Dim iRowIndex As Integer = 3, j As Integer
        For j = 0 To dt.Rows.Count - 1
            Dim dr As DataRow = dt.Rows(j)
            oSheet.Range("A" & Trim(Str(iRowIndex))).Value = dr("YearCnt")
            oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("ChildCnt")
            oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("ChildRankRangeCnt")
            oSheet.Range("D" & Trim(Str(iRowIndex))).Formula = "=C" & Trim(Str(iRowIndex)) & "/B" & Trim(Str(iRowIndex))
            oSheet.Range("D" & Trim(Str(iRowIndex))).NumberFormat = "00.0%"
            iRowIndex = iRowIndex + 1
        Next
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Private Sub Export_TurnOver()
        If ViewState("TurnOver") Is Nothing Then
            Exit Sub
        End If
        Dim dt As DataTable = ViewState("TurnOver")
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "Trends_in_TurnOverOfNewFamilies_" & Now.ToShortDateString & ".xls"

        oSheet.Range("A1:H1").MergeCells = True
        oSheet.Range("A1").Value = "Turnover in NSF Families"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True

        oSheet.Range("B2:D2").MergeCells = True
        oSheet.Range("B2").Value = "Previous Year"
        oSheet.Range("B2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("B2").Font.Bold = True

        oSheet.Range("E2:G2").MergeCells = True
        oSheet.Range("E2").Value = "Current Year"
        oSheet.Range("E2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("E2").Font.Bold = True

        oSheet.Range("B3").Value = "Cur Families"
        oSheet.Range("B3").Font.Bold = True

        oSheet.Range("C3").Value = "Past Families"
        oSheet.Range("C3").Font.Bold = True

        oSheet.Range("D3").Value = "New Families"
        oSheet.Range("D3").Font.Bold = True

        oSheet.Range("E3").Value = "Cur Families"
        oSheet.Range("E3").Font.Bold = True

        oSheet.Range("F3:G3").MergeCells = True
        oSheet.Range("F3").Value = "Dropouts"
        oSheet.Range("F3").Font.Bold = True

        oSheet.Range("H3").Value = "Percent"
        oSheet.Range("H3").Font.Bold = True

        oSheet.Range("F4").Value = "8th Graders"
        oSheet.Range("F4").Font.Bold = True

        oSheet.Range("G4").Value = "All Other"
        oSheet.Range("G4").Font.Bold = True

        'dsPreYrFamilies Previous AND current year -Cur families
        Dim iRowIndex As Integer = 5, j As Integer
        Dim CRange As IRange
        For j = 0 To dt.Rows.Count - 1
            Dim dr As DataRow = dt.Rows(j)
            oSheet.Range("A" & Trim(Str(iRowIndex))).Value = dr("ContestYear")
            oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("PreCurFamily")
            oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("PrePastFamily")
            If dr("PrePastFamily") <> "0" Then
                oSheet.Range("D" & Trim(Str(iRowIndex))).Formula = "=B" & Trim(Str(iRowIndex)) & "-C" & Trim(Str(iRowIndex))
                oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("CurFamily")
                If dr("Graders") = "0" Then
                    oSheet.Range("F" & Trim(Str(iRowIndex))).Value = "NA"
                Else
                    oSheet.Range("F" & Trim(Str(iRowIndex))).Value = dr("Graders")
                End If
                oSheet.Range("G" & Trim(Str(iRowIndex))).Value = (dr("DropOut") - dr("Graders"))
                CRange = oSheet.Range("H" & Trim(Str(iRowIndex)))
                CRange.Formula = "=G" & Trim(Str(iRowIndex)) & "/D" & Trim(Str(iRowIndex))
                CRange.NumberFormat = "00.0%"
            Else
                oSheet.Range("D" & Trim(Str(iRowIndex))).Formula = "NA"
                oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("CurFamily")
                oSheet.Range("F" & Trim(Str(iRowIndex))).Value = "NA"
                oSheet.Range("G" & Trim(Str(iRowIndex))).Value = "NA"
                oSheet.Range("H" & Trim(Str(iRowIndex))).Value = "NA"
            End If
            iRowIndex = iRowIndex + 1
        Next
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()

    End Sub

    Private Sub Export_NSF_Families_ByContest()
        If ViewState("NSF_Families_ByContest") Is Nothing Then
            lblMsg.Text = "No record is submitted for 'Trends in NSF Families By Contest'"
            Exit Sub
        End If
        ds = ViewState("NSF_Families_ByContest")
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "Trends_in_FamiliesByContest_" & Now.ToShortDateString & ".xls"
        oSheet.Range("A1:M1").MergeCells = True
        oSheet.Range("A1").Value = "Trends in NSF Families by Contest"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True

        Dim i As Integer
        Dim ar As New ArrayList
        ar.Add("ProductId")
        ar.Add("ProductCode")
        Dim unicode As Integer = 66
        Dim character As Char, txt As String
        For i = ddlYearCount.SelectedValue To 0 Step -1
            character = Convert.ToChar(unicode)
            txt = character.ToString
            ar.Add(txt)
            oSheet.Range(txt & "3").Value = Now.AddYears(-i).Year
            oSheet.Range(txt & "3").Font.Bold = True
            unicode = unicode + 1
        Next
        ' add Change column
        character = Convert.ToChar(unicode)
        txt = character.ToString
        ar.Add(txt)
        oSheet.Range(txt & "3").Value = "Change"
        oSheet.Range(txt & "3").Font.Bold = True

        Dim iRowIndex As Integer = 4, j As Integer
        Dim CRange As IRange
        For j = 0 To ds.Tables(0).Rows.Count - 1
            Dim dr As DataRow = ds.Tables(0).Rows(j)
            CRange = oSheet.Range("A" & Trim(Str(iRowIndex)))
            CRange.Value = dr("ProductCode")
            oSheet.Range("A" & Trim(Str(iRowIndex))).Font.Bold = True
            For i = 2 To ar.Count - 1
                If i = (ar.Count - 1) Then
                    CRange = oSheet.Range(ar(i.ToString) & Trim(Str(iRowIndex)))
                    CRange.Formula = "=(" & ar(i - 1) & Trim(Str(iRowIndex)) & "/" & ar(i - 2) & Trim(Str(iRowIndex)) & ")- 1"
                    CRange.NumberFormat = "00.0%"
                    Exit For
                End If
                oSheet.Range(ar(i.ToString) & Trim(Str(iRowIndex))).Value = dr(i)
            Next
            iRowIndex = iRowIndex + 1
        Next
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Private Sub Export_NSF_Families()
        If ViewState("NSF_Families") Is Nothing Then
            Exit Sub
        End If
        ds = ViewState("NSF_Families")
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "Trends_in_Families_" & Now.ToShortDateString & ".xls"

        oSheet.Range("A1:M1").MergeCells = True
        oSheet.Range("A1").Value = "Trends in NSF families"
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True

        oSheet.Range("B3").Value = "Current Year Families"
        oSheet.Range("B3").Font.Bold = True
        oSheet.Range("C3").Value = "Non-8thGrade"
        oSheet.Range("C3").Font.Bold = True
        oSheet.Range("D3").Value = "8thGrade"
        oSheet.Range("D3").Font.Bold = True

        oSheet.Range("E3").Value = "Past Years Families"
        oSheet.Range("E3").Font.Bold = True
        oSheet.Range("F3").Value = "New Families"
        oSheet.Range("F3").Font.Bold = True

        oSheet.Range("G3").Value = "Retention"
        oSheet.Range("G3").Font.Bold = True
        oSheet.Range("H3").Value = "Growth in New Families"
        oSheet.Range("H3").Font.Bold = True
        oSheet.Range("I3").Value = "Growth in Registrations"
        oSheet.Range("I3").Font.Bold = True
        oSheet.Range("J3").Value = "Registrations"
        oSheet.Range("J3").Font.Bold = True
        oSheet.Range("K3").Value = "Reg per Family"
        oSheet.Range("K3").Font.Bold = True
        oSheet.Range("L3").Value = "Fees"
        oSheet.Range("L3").Font.Bold = True
        oSheet.Range("M3").Value = "Fees per Family"
        oSheet.Range("M3").Font.Bold = True

        oSheet.Range("N3").Value = "New Contests"
        oSheet.Range("N3").Font.Bold = True
        oSheet.Range("O3").Value = "Fee Change"
        oSheet.Range("O3").Font.Bold = True

        Dim iRowIndex As Integer = 4
        Dim CRange As IRange
        Dim i As Integer = 0
        While i < ds.Tables(0).Rows.Count
            Dim dr As DataRow = ds.Tables(0).Rows(i)
            If i = 0 And dr("Year") <> "2005" Then
                i = i + 1
                Continue While
            End If

            CRange = oSheet.Range("A" & Trim(Str(iRowIndex)))
            CRange.Value = dr("Year")

            ' Current Year Families
            CRange = oSheet.Range("B" & Trim(Str(iRowIndex)))
            CRange.Value = dr("CYearFamilies")
            CRange.NumberFormat = "#,##0_);(#,##0)"

            ' Non-8th grade
            CRange = oSheet.Range("C" & Trim(Str(iRowIndex)))
            CRange.Value = dr("Non-8thGrade")
            CRange.NumberFormat = "#,##0_);(#,##0)"

            ' Current Year Families
            CRange = oSheet.Range("D" & Trim(Str(iRowIndex)))
            CRange.Value = dr("8thGrade")
            If CRange.Value = 0 Then
                CRange.Value = "NA"
            End If
            CRange.NumberFormat = "#,##0_);(#,##0)"

            'Past Year Families
            CRange = oSheet.Range("E" & Trim(Str(iRowIndex)))
            CRange.Value = dr("PYearFamilies")
            If dr("PYearFamilies") = 0 Then
                CRange.Value = "NA"
            End If
            CRange.NumberFormat = "#,##0_);(#,##0)"

            ' New Families
            If iRowIndex > 4 Then
                CRange = oSheet.Range("F" & Trim(Str(iRowIndex)))
                CRange.Formula = "=B" & Trim(Str(iRowIndex)) & "-E" & Trim(Str(iRowIndex))
                If CRange.Value = 0 Then
                    CRange.Value = "NA"
                End If
                CRange.NumberFormat = "#,##0_);(#,##0)"
            End If

            'Retention
            CRange = oSheet.Range("G" & Trim(Str(iRowIndex)))
            CRange.Formula = "=(E" & Trim(Str(iRowIndex)) & "/C" & Trim(Str(iRowIndex - 1)) & ")*100 "
            If CRange.Value = 0 Then
                CRange.Value = "NA"
            End If
            CRange.NumberFormat = "#,##0.0_);(#,##0.0)"
            If iRowIndex > 4 Then
                'CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                'Growth in New Families
                CRange = oSheet.Range("H" & Trim(Str(iRowIndex)))
                CRange.Formula = "=F" & Trim(Str(iRowIndex)) & "/F" & Trim(Str(iRowIndex - 1)) & "-1"
                If CRange.Value = 0 Then
                    CRange.Value = "NA"
                End If
                CRange.NumberFormat = "00.0%"
                'Growth in Registrations
                CRange = oSheet.Range("I" & Trim(Str(iRowIndex)))
                CRange.Formula = "=J" & Trim(Str(iRowIndex)) & "/J" & Trim(Str(iRowIndex - 1)) & "-1"
                If CRange.Value = 0 Then
                    CRange.Value = "NA"
                End If
                CRange.NumberFormat = "00.0%"
            End If
            'Registration
            CRange = oSheet.Range("J" & Trim(Str(iRowIndex)))
            CRange.Value = dr("Registrations")
            CRange.NumberFormat = "#,##0_);(#,##0)"

            'Reg Per Family
            If iRowIndex > 4 Then
                CRange = oSheet.Range("K" & Trim(Str(iRowIndex)))
                CRange.Formula = "=J" & Trim(Str(iRowIndex)) & "/B" & Trim(Str(iRowIndex))
                CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
            End If
            'Fees
            CRange = oSheet.Range("L" & Trim(Str(iRowIndex)))
            CRange.Value = dr("CurrentFees")
            CRange.NumberFormat = "#,##0_);(#,##0)"

            'Fees per family
            CRange = oSheet.Range("M" & Trim(Str(iRowIndex)))
            CRange.Formula = "=L" & Trim(Str(iRowIndex)) & "/B" & Trim(Str(iRowIndex))
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            iRowIndex = iRowIndex + 1
            i = i + 1
        End While

        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Try
            Select Case ddlReportType.SelectedValue
                Case 1
                    Export_NSF_Families()
                Case 2

                    Export_NSF_Families_ByContest()
                Case 3
                    Export_TurnOver()
                Case 4
                    Export_LearningOutComes()
            End Select
        Catch ex As Exception   '    Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub btnSubmit_Click(sender As Object, e As EventArgs) Handles btnSubmit.Click
        Try
            btnExportAll.Visible = False
            divTable2.Visible = False
            lblMsg.Text = ""
            ltrHTML.Text = ""
            Dim cmdText As String
            Dim strYear As String = "", i As Integer
            Select Case ddlReportType.SelectedValue
                Case 1
                    For i = 0 To (ddlYearCount.SelectedValue + 1)
                        strYear = strYear & "," & Now.AddYears(-i).Year
                    Next
                    strYear = strYear.Substring(1, strYear.Length - 1)
                    cmdText = " DECLARE @sql varchar(MAX) "
                    cmdText = cmdText & "  SET @sql =N'select ContestYear Year,COUNT(distinct(parentID)) ''CYearFamilies'', (select COUNT(distinct(parentID)) from contestant where contestyear=c1.contestyear and (grade<8 or grade is null) and paymentreference is not null and eventid=2 ) ''Non-8thGrade'',"
                    cmdText = cmdText & " ( select COUNT(distinct(parentID)) from contestant ct where PaymentReference is not null and EventId=2 and contestyear=c1.ContestYear and grade>=8 and (select count(distinct(parentid)) from contestant where parentid=ct.parentid and  PaymentReference is not null and EventId=2 and contestyear=c1.ContestYear and (grade<8 or grade is null))=0) ''8thGrade'',"
                    cmdText = cmdText & " ( select COUNT(distinct(C.parentID)) from contestant C "
                    cmdText = cmdText & " Inner Join contestant P on P.EventId=2 and C.EventId=2 and P.ParentID=c.ParentID where C.ContestYear=c1.ContestYear and C.PaymentReference is not null and P.ContestYear <c1.Contestyear  and P.PaymentReference is not null "
                    cmdText = cmdText & " ) ''PYearFamilies'', COUNT(*)  ''Registrations'',Sum(Fee) ''CurrentFees'' from contestant c1 where "
                    cmdText = cmdText & " PaymentReference is not null and EventId=2 group by contestyear having contestyear in (" & strYear & ")  order by contestyear'"
                    cmdText = cmdText & " exec (@sql) "
                    Display_NSF_Families(cmdText)
                Case 2
                    For i = 0 To ddlYearCount.SelectedValue
                        strYear = ",[" & Now.AddYears(-i).Year & "]" & strYear
                    Next
                    strYear = strYear.Substring(1)
                    cmdText = " DECLARE @sql varchar(MAX) "
                    cmdText = cmdText & " set @sql=N' select * from (select distinct parentID Families,Contestyear, ProductID, ProductCode  from contestant where EventId=2  and PaymentReference is not null  ) as p pivot "
                    cmdText = cmdText & " (COUNT(Families) FOR ContestYear in (" & strYear & ")) as d where Productid is not null order by ProductId '"
                    cmdText = cmdText & " exec (@sql) "
                    Display_NSF_Families_ByContest(cmdText)
                Case 3
                    Display_TurnOver()
                Case 4
                    btnExportAll.Visible = True
                    lblMsg.Text = ""
                    ddlYearCount.SelectedIndex = 1
                    If ddlProductGrp.SelectedValue = "-1" Then
                        lblMsg.ForeColor = Color.Red
                        lblMsg.Text = "Select Product Group"
                        Exit Sub
                    End If
                    Display_LearningOutComes()
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Sub Display_NSF_Families(str As String)

        ds = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, str)
        ViewState("NSF_Families") = ds
        Dim strHTML As String = "<table width='95%' border='1' cellspacing='0' cellpadding='0' style='margin-left:30px;'><Tr><th colspan='13'>Trends in NSF families</th></tr>"
        strHTML = strHTML & "<tr style='font-weight:bold;text-align:center'><td></td><td>Current Year Families</td><td>Non-8th Grade</td><td>8th Grade</td><td>Past Years Families</td><td>New Families</td><td>Retention</td><td>Growth in New Families</td><td>Growth in Registrations</td><td>Registrations</td><td>Reg per Family</td><td>Fees</td><td>Fees per Family</td><td>New Contests</td><td>Fee Change</td></tr>"

        Dim i As Integer
        Dim dr As DataRow
        For i = 0 To ds.Tables(0).Rows.Count - 1
            dr = ds.Tables(0).Rows(i)
            If i = 0 And dr("year") <> "2005" Then
                Continue For
            End If
            strHTML = strHTML & "<tr style='text-align:right;'><td width='40px' style='text-align:center;'><b>" & dr("year") & "</b></td><td>" & dr("CYearFamilies") & "</td><td>" & dr("Non-8thGrade") & "</td><td>"
            If dr("8thGrade") <> 0 Then
                strHTML = strHTML & " <a href='#' onclick='javascript:Show_Table2(4," & dr("year") & ")'>" & dr("8thGrade") & "</a>"
            Else
                strHTML = strHTML & IIf(dr("8thGrade") = 0, "NA", dr("8thGrade"))
            End If
            strHTML = strHTML & "</td><td>" & IIf(dr("PYearFamilies") = 0, "NA", dr("PYearFamilies")) & "</td><td>"
            Dim nF As Integer
            nF = CInt(dr("CYearFamilies")) - CInt(dr("PYearFamilies"))

            If i = 0 And dr("year") = "2005" Then
                nF = 0
            End If
            strHTML = strHTML & IIf(nF = 0, "NA", nF.ToString()) & "</td>"
            Dim ret As Decimal, GInF As Decimal, GInR As Decimal
            'Retention
            Try
                ret = (dr("PYearFamilies") / ds.Tables(0).Rows(i - 1).Item("Non-8thGrade")) * 100  ' dr("PYearFamilies") / ds.Tables(0).Rows(i - 1).Item("CYearFamilies") ' dr("Non-8thGrade")
            Catch ex As Exception
                ret = 0
            End Try

            ' If i > 0 Then
            '=B5/B4-1 (New Families/PrevNewFamilies-1)
            Try
                GInF = nF / (CInt(ds.Tables(0).Rows(i - 1).Item("CYearFamilies")) - CInt(ds.Tables(0).Rows(i - 1).Item("PYearFamilies"))) - 1 'dr("CYearFamilies") / ds.Tables(0).Rows(i - 1).Item("CYearFamilies") - 1
            Catch ex As Exception
                GInF = 0
            End Try
            '=E5/E4-1 reg
            Try
                GInR = dr("Registrations") / ds.Tables(0).Rows(i - 1).Item("Registrations") - 1
            Catch ex As Exception
                GInR = 0
            End Try
            strHTML = strHTML & "<td style='text-align:right;'>" & IIf(ret = 0, "NA", Math.Round(Convert.ToDecimal(ret.ToString()), 1) & "%") & "</td>" & "<td style='text-align:right;'>" & IIf(GInF = 0, "NA", FormatPercent(GInF.ToString(), 1)) & "</td>"
            strHTML = strHTML & "<td style='text-align:right;'>" & IIf(GInR = 0, "NA", FormatPercent(GInR.ToString(), 1)) & "</td>"
            'Else
            'strHTML = strHTML & "<td style='text-align:right;'>" & Math.Round(Convert.ToDecimal(ret.ToString()), 1) & "%</td><td></td><td></td>"
            'End If
            strHTML = strHTML & "<td style='text-align:right;'>" & dr("Registrations") & "</td><td style='text-align:right;'>"
            If i > 0 Then
                Dim fReg As Decimal
                fReg = dr("Registrations") / dr("CYearFamilies")
                strHTML = strHTML & Math.Round(Convert.ToDecimal(fReg.ToString()), 2)
            End If
            strHTML = strHTML & "</td><td style='text-align:right;'>" & CInt(dr("CurrentFees")).ToString & "</td><td style='text-align:right;'>" & Math.Round(Convert.ToDecimal((dr("CurrentFees") / dr("CYearFamilies")).ToString()), 2) & "</td>"

            strHTML = strHTML & "<td></td><td></td></tr>"
        Next
        strHTML = strHTML & "</table>"
        ltrHTML.Text = strHTML
    End Sub

    Sub Display_NSF_Families_ByContest(str As String)
        ds = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, str)
        ViewState("NSF_Families_ByContest") = ds
        Dim strHTML As String = "<table width='90%' border='1' cellspacing='0' cellpadding='0' style='margin-left:30px;'><Tr><th colspan='" & ds.Tables(0).Rows.Count - 1 & "'>Trends in NSF Families by Contest</th></tr>"
        strHTML = strHTML & "<tr><th></th>"
        'Draw header
        Dim i As Integer, dr As DataRow, c As Integer, cTotal As Integer
        cTotal = ds.Tables(0).Columns.Count
        For i = 2 To ds.Tables(0).Columns.Count - 1
            strHTML = strHTML & "<th>" & ds.Tables(0).Columns(i).ColumnName & "</th>"
        Next
        strHTML = strHTML & "<th>Change</th></tr>"
        For i = 0 To ds.Tables(0).Rows.Count - 1
            dr = ds.Tables(0).Rows(i)
            strHTML = strHTML & "<tr style='text-align:right;'><td  style='text-align:center;'><b>" & dr("ProductCode") & "</b></td>"
            For c = 2 To cTotal - 1
                strHTML = strHTML & "<td>" & dr(ds.Tables(0).Columns(c).ColumnName) & "</td>"
            Next
            Dim change As Decimal
            ' F4-E4/E4
            Try
                change = (dr(ds.Tables(0).Columns(cTotal - 1).ColumnName) / dr(ds.Tables(0).Columns(cTotal - 2).ColumnName)) - 1
                strHTML = strHTML & "<td>" & FormatPercent(change, 1) & "</td> </tr>"
            Catch ex As Exception
                strHTML = strHTML & "<td></td> </tr>"
                change = 0.0

            End Try
        Next
        strHTML = strHTML & "</table>"
        ltrHTML.Text = strHTML
    End Sub

    Sub Display_TurnOver()
        Dim iPreYear As Integer, iCurYear As Integer
        iPreYear = Now.AddYears(-1).Year
        iCurYear = Now.Year
        Dim strYear As String = "", i As Integer
        For i = 0 To ddlYearCount.SelectedValue
            strYear = strYear & "," & Now.AddYears(-i).Year
        Next
        strYear = strYear.Substring(1, strYear.Length - 1)

        cmdText = " select contestyear,(select count(distinct(parentID)) from contestant where ContestYear=CT.ContestYear-1 and PaymentReference is not null and EventId=2) PreCurFamily,"
        cmdText = cmdText & " (select count(distinct(C.parentID)) from contestant C Inner Join contestant P on P.ParentID=C.ParentID where C.ContestYear=CT.ContestYear-1 and C.PaymentReference is not null and "
        cmdText = cmdText & " P.ContestYear <CT.ContestYear-1  and P.PaymentReference is not null and P.EventId=2 and C.EventId=2) PrePastFamily, "
        cmdText = cmdText & " (select count(distinct(parentID)) from contestant where EventID=2 and ContestYear=CT.ContestYear and PaymentReference is not null) CurFamily,'Graders' as Graders,'DropOut' as DropOut "
        cmdText = cmdText & " from contestant CT where CT.EventId=2 and contestyear in(" & strYear & ") and paymentreference is not null group by contestyear order by contestyear"

        Dim dsCurFamilies As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)

        cmdText = " select contestyear, (select count(distinct(parentid)) from contestant where parentid in( ((select distinct(parentID) from contestant where EventId=2 and ContestYear=CT.ContestYear-1 and PaymentReference is not null)"
        cmdText = cmdText & "  except (select distinct(C.parentID)  from contestant C  Inner Join contestant P on P.ParentID=c.ParentID where C.ContestYear=CT.ContestYear-1 and C.EventId=2 and P.EventId=2 and C.PaymentReference is not null and "
        cmdText = cmdText & " P.ContestYear <CT.ContestYear-1 and P.PaymentReference is not null))  except (select distinct(parentID) from contestant where EventId=2 and ContestYear=CT.ContestYear and PaymentReference is not null)) "
        cmdText = cmdText & " )as DropOut from contestant CT where contestyear in(" & strYear & ") and paymentreference is not null group by contestyear order by contestyear "
        Dim dsDropOutFamilies As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)

        cmdText = " select contestyear, (select count(distinct(parentid)) from contestant c1 where grade>=8 and ((select count(distinct(parentid)) from contestant where (grade<8 or grade is null) and PaymentReference is not null and contestyear=ct.Contestyear-1 and parentid=c1.parentid and eventid=2)=0) and parentid in(((select distinct(parentID) from contestant where EventId=2 and ContestYear=CT.ContestYear-1 and PaymentReference is not null)"
        cmdText = cmdText & "  except (select distinct(C.parentID)  from contestant C  Inner Join contestant P on P.ParentID=c.ParentID where C.ContestYear=CT.ContestYear-1 and C.EventId=2 and P.EventId=2 and C.PaymentReference is not null and "
        cmdText = cmdText & " P.ContestYear <CT.ContestYear-1 and P.PaymentReference is not null))  except (select distinct(parentID) from contestant where EventId=2 and ContestYear=CT.ContestYear and PaymentReference is not null)) "
        cmdText = cmdText & " )as 'Graders' from contestant CT where contestyear in(" & strYear & ") and paymentreference is not null group by contestyear order by contestyear "

        Dim dsGradersFamilies As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)
        Dim dtTurnOver As DataTable = dsCurFamilies.Tables(0)
        For i = 0 To dtTurnOver.Rows.Count - 1
            dtTurnOver.Rows(i)("DropOut") = dsDropOutFamilies.Tables(0).Rows(i)("DropOut")
        Next
        For i = 0 To dtTurnOver.Rows.Count - 1
            dtTurnOver.Rows(i)("Graders") = dsGradersFamilies.Tables(0).Rows(i)("Graders")
        Next
        ViewState("TurnOver") = dtTurnOver
        Dim strHTML As String = "<table width='70%' border='1' cellspacing='0' cellpadding='0' style='margin-left:100px;'><Tr><th colspan='7'>Turnover in NSF Families</th><th></th></tr>"
        strHTML = strHTML & "<tr><th></th><th colspan='3'>Previous Year</th><th colspan='3'>Current Year</th><th></th></tr>"
        strHTML = strHTML & "<tr style='font-weight:bold;text-align:center'><td></td><td>Cur Families</td><td>Past Families</td><td>New Families</td><td>Cur Families</td><td colspan=2>Dropouts</td><td>Percent</td></tr>"

        strHTML = strHTML & "<tr style='font-weight:bold;text-align:center'><td></td><td></td><td></td><td></td><td></td><td>8th Graders</td><td>All other</td><td></td></tr>"

        Dim dr As DataRow, iNewFamilies As Integer, iNon8thGraders As Integer

        For i = 0 To dtTurnOver.Rows.Count - 1
            dr = dtTurnOver.Rows(i)
            strHTML = strHTML & "<tr style='text-align:right;'><td style='text-align:left;'><b>" & dr("ContestYear") & "</b></td><Td>" & dr("PreCurFamily") & "</td><td>" & dr("PrePastFamily") & "</td>"
            If dr("PrePastFamily") <> "0" Then
                iNewFamilies = dr("PreCurFamily") - dr("PrePastFamily")
                iNon8thGraders = (dr("DropOut") - dr("Graders"))
                strHTML = strHTML & "<td>" & iNewFamilies & "</td><td>" & dr("CurFamily") & "</td>"
                If dr("Graders") = "0" Then
                    strHTML = strHTML & "<td>NA</td>"
                Else
                    strHTML = strHTML & "<td><a href='#' onclick='javascript:Show_Table2(3," & dr("ContestYear") & ")'>" & dr("Graders") & "</a></td>"
                End If


                strHTML = strHTML & "<td>" & iNon8thGraders & "</td><td>" & FormatPercent(iNon8thGraders / iNewFamilies, 1) & "</td></tr>"
            Else
                strHTML = strHTML & "<td>NA</td><td>" & dr("CurFamily") & "</td><td>NA</td><td>NA</td>"
                strHTML = strHTML & "<td>NA</td></tr>"
            End If
        Next
        strHTML = strHTML & "</table>"
        ltrHTML.Text = strHTML
    End Sub

    Sub Display_LearningOutComes()

        Dim productgroupcode As String = ddlProductGrp.SelectedValue
        Dim prdCondition As String = " and  ProductGroupCode='" & productgroupcode & "'"
        If ddlProductGrp.SelectedValue = "All" Then
            prdCondition = ""
        End If
        cmdText = " DECLARE @Table Table (YearCnt int, ChildCnt int,ChildRankRangeCnt int) "
        cmdText = cmdText & " declare @idx as int; set @idx=1; "
        cmdText = cmdText & " declare @ChildCnt as int,@ChildRankRangeCnt as int  "
        cmdText = cmdText & "  while (@idx<10) begin  "
        cmdText = cmdText & " if (@idx=9) BEGIN "

        cmdText = cmdText & "  select @ChildCnt=count(distinct(childnumber))  from contestant  where EventId=2 " & prdCondition & " and PaymentReference is not null and  ChildNumber in ( select  ChildNumber from contestant "
        cmdText = cmdText & "   where EventId=2 " & prdCondition & " and PaymentReference is not null group by ChildNumber  having COUNT(distinct(ContestYear))>=@idx ) "
        cmdText = cmdText & "  	select @ChildRankRangeCnt=count(childnumber) from contestant  where EventId=1 and (rank>0 and rank<=10) and ChildNumber in ( select ChildNumber from contestant "
        cmdText = cmdText & "  where  EventId=2  " & prdCondition & "  and PaymentReference is not null group by ChildNumber having COUNT(distinct(ContestYear))>= @idx) "

        cmdText = cmdText & " END ELSE BEGIN"
        cmdText = cmdText & "  select @ChildCnt=count(distinct(childnumber))  from contestant  where EventId=2 " & prdCondition & " and PaymentReference is not null and  ChildNumber in ( select  ChildNumber from contestant "
        cmdText = cmdText & "   where EventId=2 " & prdCondition & " and PaymentReference is not null group by ChildNumber  having COUNT(distinct(ContestYear))= @idx ) "
        cmdText = cmdText & "  	select @ChildRankRangeCnt=count(childnumber) from contestant  where EventId=1 and (rank>0 and rank<=10) and ChildNumber in ( select ChildNumber from contestant "
        cmdText = cmdText & "  where  EventId=2  " & prdCondition & "  and PaymentReference is not null group by ChildNumber having COUNT(distinct(ContestYear))= @idx) "
        cmdText = cmdText & " END"
        cmdText = cmdText & "  insert into @Table values ( @idx, @ChildCnt,@ChildRankRangeCnt) "
        cmdText = cmdText & "  	set @idx=@idx+1 "
        cmdText = cmdText & "    End "
        cmdText = cmdText & "   select YearCnt,ChildCnt,ChildRankRangeCnt from @Table"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)
        Dim dt As DataTable = ds.Tables(0)
        ViewState("LearningOurComes") = dt
        Dim strHTML As String = "<table width='40%' border='1' cellspacing='0' cellpadding='0' style='margin-left:240px;'><Tr><th colspan='4'>Learning Outcomes</th></tr>"
        strHTML = strHTML & "<tr style='font-weight:bold;text-align:center;'><td>Years of Participation</td><td>Number of Children</td><td>#of Children in top 10 ranks</td><Td>Percent</td></tr>"
        Dim i As Integer
        Dim dr As DataRow
        For i = 0 To dt.Rows.Count - 1
            dr = dt.Rows(i)

            strHTML = strHTML & "<Tr style='text-align:right;'><td style='text-align:center;'>" & dr("YearCnt") & "</td><td><a href='#' onclick='javascript:Show_Table2(1," & dr("YearCnt") & ")'>" & dr("ChildCnt") & "</a></td><td><a href='#' onclick='javascript:Show_Table2(2," & dr("YearCnt") & ")'>" & dr("ChildRankRangeCnt") & "</a></td>"
            strHTML = strHTML & "<td>" & FormatPercent(dr("ChildRankRangeCnt") / dr("ChildCnt"), 1) & "</td></tr>"
        Next
        strHTML = strHTML & "</table>"
        ltrHTML.Text = strHTML
    End Sub

    Protected Sub btnShowTable2_Click(sender As Object, e As EventArgs) Handles btnShowTable2.Click
        Try
            divTable2.Visible = True
            Dim sYear As String = "", strTitle As String = "Year Of Participation :"
            cmdText = ""
            If hfType.Value = "1" Or hfType.Value = "2" Then
                Dim productgroupcode As String = ddlProductGrp.SelectedValue
                Dim prdCondition As String = "and ProductGroupCode='" & productgroupcode & "'"
                Dim prdAliasCondition As String = "and CS.ProductGroupCode='" & productgroupcode & "'"
                If ddlProductGrp.SelectedValue = "All" Then
                    prdCondition = ""
                    prdAliasCondition = ""
                End If

                If hfType.Value = "1" Then
                    strTitle = hfSelValue.Value & " Years of Participation :"
                    cmdText = " select CS.ProductCode 'Contest Code',CS.ParentId, CS.ChildNumber,Last_Name 'Child LastName',First_Name 'Child FirstName',cs.Grade,Convert(varchar(10),C.Date_Of_Birth,101) DOB, c.GENDER 'Gender Child',CS.ContestYear Year,cs.rank Rank,chpt.Chaptercode Chapter,F.CPhone 'CPhone Father',F.HPhone HomePhone,F.LastName 'LastName Father',"
                    cmdText = cmdText & " F.FirstName 'FirstName Father',F.Email EmailFather,M.LastName 'LastName Mother',M.FirstName 'FirstName Mother',M.Email EmailMother,M.CPhone 'CPhone Mother' "
                    cmdText = cmdText & " from child c inner join Contestant CS on C.ChildNumber =CS.ChildNumber and CS.PaymentReference is not null Inner Join IndSpouse F on F.Automemberid = c.memberid left join chapter chpt on chpt.chapterid=cs.chapterid "
                    cmdText = cmdText & "	Left Join IndSpouse M on M.Automemberid = C.SpouseId where cs.EventId=2 " & prdAliasCondition & " and cs.ChildNumber in ( "
                    cmdText = cmdText & " select  ChildNumber from contestant where EventId=2 " & prdCondition & " and PaymentReference is not null group by ChildNumber  having COUNT(distinct(ContestYear))=" & hfSelValue.Value & ") Order by CS.ProductCode, LAST_NAME, FIRST_NAME,CS.ContestYear"
                Else
                    strTitle = hfSelValue.Value & " Years of Top 10 Participation :"
                    cmdText = " select CS.ProductCode 'Contest Code',CS.ParentId, CS.ChildNumber,Last_Name 'Child LastName',First_Name 'Child FirstName',cs.Grade,Convert(varchar(10),C.Date_Of_Birth,101) DOB, c.GENDER 'Gender Child',CS.ContestYear Year,cs.rank Rank,chpt.Chaptercode Chapter,F.CPhone 'CPhone Father',F.HPhone HomePhone,F.LastName 'LastName Father',"
                    cmdText = cmdText & " F.FirstName 'FirstName Father',F.Email EmailFather,M.LastName 'LastName Mother',M.FirstName 'FirstName Mother',M.Email EmailMother,M.CPhone 'CPhone Mother' "
                    cmdText = cmdText & " from child c inner join Contestant CS on C.ChildNumber =CS.ChildNumber Inner Join IndSpouse F on F.Automemberid = c.memberid left join chapter chpt on chpt.chapterid=cs.chapterid "
                    cmdText = cmdText & " left Join IndSpouse M on M.Automemberid = C.SpouseId where cs.EventId=1 and (cs.rank>0 and cs.rank<=10)  and cs.ChildNumber in ( "
                    cmdText = cmdText & " select ChildNumber from contestant where EventId=2 " & prdCondition & " and PaymentReference is not null group by ChildNumber having COUNT(distinct(ContestYear))= " & hfSelValue.Value & " ) Order by CS.ProductCode, LAST_NAME, FIRST_NAME,CS.ContestYear"
                End If

            ElseIf hfType.Value = "3" Then
                strTitle = "8th Grade Turnover of New Families : "
                sYear = "(" & hfSelValue.Value & ")"
                cmdText = "  select CS.ProductCode 'Contest Code',CS.ParentId ParentId, CS.ChildNumber ChildNumber,Last_Name 'Child LastName',First_Name 'Child FirstName',cs.Grade,Convert(varchar(10),C.Date_Of_Birth,101) DOB, c.GENDER 'Gender Child',"
                cmdText = cmdText & " CS.ContestYear Year,cs.rank Rank,chpt.Chaptercode Chapter,F.CPhone 'CPhone Father',F.HPhone HomePhone,F.LastName 'LastName Father', F.FirstName 'FirstName Father', "
                cmdText = cmdText & "  F.Email EmailFather,M.LastName 'LastName Mother',M.FirstName 'FirstName Mother',M.Email EmailMother,M.CPhone 'CPhone Mother' "
                cmdText = cmdText & " from child c "
                cmdText = cmdText & " Inner join Contestant CS on C.ChildNumber =CS.ChildNumber Inner Join IndSpouse F on F.Automemberid = c.memberid "
                cmdText = cmdText & " left join chapter chpt on chpt.chapterid=cs.chapterid Left Join IndSpouse M on M.Automemberid = C.SpouseId where cs.EventId=2 and "
                cmdText = cmdText & " cs.grade>=8 and ((select count(distinct(parentid)) from contestant where (grade<8 or grade is null) and PaymentReference is not null and contestyear=" & hfSelValue.Value & "-1 and parentid=cs.parentid  and eventid=2)=0) and "
                cmdText = cmdText & " cs.ParentId in ( "
                cmdText = cmdText & " ((select distinct(parentID) from contestant where EventId=2 and ContestYear=" & hfSelValue.Value & "-1 and PaymentReference is not null)  except (select distinct(C.parentID)  from contestant C  Inner Join contestant P on P.ParentID=c.ParentID where C.ContestYear=" & hfSelValue.Value & "-1 and C.EventId=2 and P.EventId=2 and C.PaymentReference is not null and  P.ContestYear <" & hfSelValue.Value & "-1 and P.PaymentReference is not null))  except (select distinct(parentID) from contestant where EventId=2 and ContestYear=" & hfSelValue.Value & " and PaymentReference is not null) )"
            ElseIf hfType.Value = "4" Then ' Nsf Families 8th grade list
                strTitle = "8th Grade Families_TrendsInNSFFamilies : "
                sYear = "(" & hfSelValue.Value & ")"
                cmdText = "  select CS.ProductCode 'Contest Code',CS.ParentId ParentId, CS.ChildNumber ChildNumber,Last_Name 'Child LastName',First_Name 'Child FirstName',cs.Grade,Convert(varchar(10),C.Date_Of_Birth,101) DOB, c.GENDER 'Gender Child',"
                cmdText = cmdText & " CS.ContestYear Year,cs.rank Rank,chpt.Chaptercode Chapter,F.CPhone 'CPhone Father',F.HPhone HomePhone,F.LastName 'LastName Father', F.FirstName 'FirstName Father', "
                cmdText = cmdText & " F.Email EmailFather,M.LastName 'LastName Mother',M.FirstName 'FirstName Mother',M.Email EmailMother,M.CPhone 'CPhone Mother' "
                cmdText = cmdText & " from child c "
                cmdText = cmdText & " Inner join Contestant CS on C.ChildNumber =CS.ChildNumber "
                cmdText = cmdText & " Inner Join IndSpouse F on F.Automemberid = c.memberid "
                cmdText = cmdText & " left join chapter chpt on chpt.chapterid=cs.chapterid Left Join IndSpouse M on M.Automemberid = C.SpouseId where cs.EventId=2 and "
                cmdText = cmdText & " cs.contestyear=" & hfSelValue.Value & " and cs.paymentreference is not null and  "
                cmdText = cmdText & " ((select count(distinct(parentid)) from contestant where parentid=cs.parentid and PaymentReference is not null and EventId=2 and contestyear=CS.ContestYear-1 and "
                cmdText = cmdText & " (grade<8 or grade is null))=0) "
            End If
            Dim dsTable2 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)
            'Populating a DataTable from database.
            Dim dt As DataTable = dsTable2.Tables(0)
            'Building an HTML string.
            Dim html As StringBuilder = New StringBuilder()
            'Table start.
            html.Append("<table border = '1' cellspacing='0' cellpadding='0'>")
            html.Append("<tr><td colspan=" & dt.Columns.Count + 1 & " style='text-align:center'><h2>Table 2 :  " & strTitle & dt.Rows.Count & sYear & "</h2></td>")
            'Building the Header row.
            html.Append("<tr>")
            html.Append("<th></th>")
            Dim column As DataColumn
            For Each column In dt.Columns
                html.Append("<th>")
                html.Append(column.ColumnName)
                html.Append("</th>")
            Next
            html.Append("</tr>")
            'Building the Data rows.
            Dim iRowCnt As Integer
            iRowCnt = 1
            Dim row As DataRow
            For Each row In dt.Rows
                html.Append("<tr>")
                html.Append("<td>" & iRowCnt & "</td>")
                iRowCnt = iRowCnt + 1
                For Each column In dt.Columns
                    html.Append("<td>")
                    html.Append(row(column.ColumnName))
                    html.Append("</td>")
                Next
                html.Append("</tr>")
            Next
            'Table end.
            html.Append("</table>")
            ltrTable2.Text = html.ToString()
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub btnExportTabl2_Click(sender As Object, e As EventArgs) Handles btnExportTabl2.Click
        Dim strFileName As String = "YearOfParticipation_"
        Select Case hfType.Value
            Case "1" ' Learning Outcomes - #of Childrens
                strFileName = "NumberOfChildrens_Learning_Outcomes_"
            Case "2" ' Learning Outcomes - Top 10 Rank Holders
                strFileName = "Top10RankHolders_Learning_Outcomes_"
            Case "3" ' Turnover of new families
                strFileName = "8th_Grade_TurnoverOfNew_Families_"
            Case "4" ' Trends in nsf families
                strFileName = "8th_Grade_Families_TrendsInNSFFamilies_"
        End Select
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & strFileName & Now.ToShortDateString & ".xls")
        Dim sw As New StringWriter()
        Dim htw As New HtmlTextWriter(sw)
        ltrTable2.RenderControl(htw)
        Response.Write(sw.GetStringBuilder().ToString())
        Response.End()
    End Sub

    Protected Overrides Sub Finalize()
        MyBase.Finalize()
    End Sub

    Protected Sub btnExportAll_Click(sender As Object, e As EventArgs) Handles btnExportAll.Click

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.StoredProcedure, "usp_GetLearningOutComes_All")
        Dim dt As DataTable = ds.Tables(0)
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        Dim FileName As String = "LearningOutComes_All_" & Now.ToShortDateString & ".xls"

        oSheet.Range("C1:V2").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("C1:V2").Font.Bold = True

        oSheet.Range("C1:F1").MergeCells = True
        oSheet.Range("C1").Value = "Learning Outcomes (MB)"
       
        oSheet.Range("G1:J1").MergeCells = True
        oSheet.Range("G1").Value = "Learning Outcomes (SB)"
        'oSheet.Range("G1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("G1").Font.Bold = True

        oSheet.Range("K1:N1").MergeCells = True
        oSheet.Range("K1").Value = "Learning Outcomes (VB)"
        'oSheet.Range("K1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("K1").Font.Bold = True

        oSheet.Range("O1:R1").MergeCells = True
        oSheet.Range("O1").Value = "Learning Outcomes (GB)"
        'oSheet.Range("O1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("O1").Font.Bold = True

        oSheet.Range("S1:V1").MergeCells = True
        oSheet.Range("S1").Value = "Learning Outcomes (SC)"
        'oSheet.Range("S1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("S1").Font.Bold = True

        oSheet.Range("C15:F15").MergeCells = True
        oSheet.Range("C15").Value = "Learning Outcomes (All)"
        'oSheet.Range("C15").HorizontalAlignment = XlHAlign.xlHAlignCenter
        'oSheet.Range("C15").Font.Bold = True

        'MB  ' Number of Children	#of Children in top 10 ranks	Percent
        oSheet.Range("C2").Value = "Years of Participation"
        oSheet.Range("D2").Value = "Number of Children"
        oSheet.Range("E2").Value = "#of Children in top 10 ranks"
        oSheet.Range("F2").Value = "Percent"

        'SB
        oSheet.Range("G2").Value = "Years of Participation"
        oSheet.Range("H2").Value = "Number of Children"
        oSheet.Range("I2").Value = "#of Children in top 10 ranks"
        oSheet.Range("J2").Value = "Percent"

        'VB
        oSheet.Range("K2").Value = "Years of Participation"
        oSheet.Range("L2").Value = "Number of Children"
        oSheet.Range("M2").Value = "#of Children in top 10 ranks"
        oSheet.Range("N2").Value = "Percent"
        'GB
        oSheet.Range("O2").Value = "Years of Participation"
        oSheet.Range("P2").Value = "Number of Children"
        oSheet.Range("Q2").Value = "#of Children in top 10 ranks"
        oSheet.Range("R2").Value = "Percent"
        'SC
        oSheet.Range("S2").Value = "Years of Participation"
        oSheet.Range("T2").Value = "Number of Children"
        oSheet.Range("U2").Value = "#of Children in top 10 ranks"
        oSheet.Range("V2").Value = "Percent"

        'ALL
        oSheet.Range("C15:F16").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("C15:F16").Font.Bold = True
        oSheet.Range("C16").Value = "Years of Participation"
        oSheet.Range("D16").Value = "Number of Children"
        oSheet.Range("E16").Value = "#of Children in top 10 ranks"
        oSheet.Range("F16").Value = "Percent"

        Dim i As Integer = 0, iRowCount As Integer = 3
        For i = 0 To dt.Rows.Count - 1
            Dim dr As DataRow = dt.Rows(i)
            oSheet.Range("C" & Trim(Str(iRowCount))).Value = dr("YearCnt")
            oSheet.Range("G" & Trim(Str(iRowCount))).Value = dr("YearCnt")
            oSheet.Range("K" & Trim(Str(iRowCount))).Value = dr("YearCnt")
            oSheet.Range("O" & Trim(Str(iRowCount))).Value = dr("YearCnt")
            oSheet.Range("S" & Trim(Str(iRowCount))).Value = dr("YearCnt")

            oSheet.Range("D" & Trim(Str(iRowCount))).Value = dr("ChildCntMB")
            oSheet.Range("E" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCntMB")
            oSheet.Range("F" & Trim(Str(iRowCount))).Formula = "=E" & Trim(Str(iRowCount)) & "/D" & Trim(Str(iRowCount))
            oSheet.Range("F" & Trim(Str(iRowCount))).NumberFormat = "00.0%"

            oSheet.Range("H" & Trim(Str(iRowCount))).Value = dr("ChildCntSB")
            oSheet.Range("I" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCntSB")
            oSheet.Range("J" & Trim(Str(iRowCount))).Formula = "=I" & Trim(Str(iRowCount)) & "/H" & Trim(Str(iRowCount))
            oSheet.Range("J" & Trim(Str(iRowCount))).NumberFormat = "00.0%"

            oSheet.Range("L" & Trim(Str(iRowCount))).Value = dr("ChildCntVB")
            oSheet.Range("M" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCntVB")
            oSheet.Range("N" & Trim(Str(iRowCount))).Formula = "=M" & Trim(Str(iRowCount)) & "/L" & Trim(Str(iRowCount))
            oSheet.Range("N" & Trim(Str(iRowCount))).NumberFormat = "00.0%"

            oSheet.Range("P" & Trim(Str(iRowCount))).Value = dr("ChildCntGB")
            oSheet.Range("Q" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCntGB")
            oSheet.Range("R" & Trim(Str(iRowCount))).Formula = "=Q" & Trim(Str(iRowCount)) & "/P" & Trim(Str(iRowCount))
            oSheet.Range("R" & Trim(Str(iRowCount))).NumberFormat = "00.0%"

            oSheet.Range("T" & Trim(Str(iRowCount))).Value = dr("ChildCntSC")
            oSheet.Range("U" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCntSC")
            oSheet.Range("V" & Trim(Str(iRowCount))).Formula = "=U" & Trim(Str(iRowCount)) & "/T" & Trim(Str(iRowCount))
            oSheet.Range("V" & Trim(Str(iRowCount))).NumberFormat = "00.0%"
            iRowCount = iRowCount + 1
        Next

        cmdText = " DECLARE @Table Table (YearCnt int, ChildCnt int,ChildRankRangeCnt int) "
        cmdText = cmdText & " declare @idx as int; set @idx=1; "
        cmdText = cmdText & " declare @ChildCnt as int,@ChildRankRangeCnt as int  "
        cmdText = cmdText & "  while (@idx<10) begin  "
        cmdText = cmdText & " if (@idx=9) BEGIN "

        cmdText = cmdText & "  select @ChildCnt=count(distinct(childnumber))  from contestant  where EventId=2 and PaymentReference is not null and  ChildNumber in ( select  ChildNumber from contestant "
        cmdText = cmdText & "   where EventId=2 and PaymentReference is not null group by ChildNumber  having COUNT(distinct(ContestYear))>=@idx ) "
        cmdText = cmdText & "  	select @ChildRankRangeCnt=count(childnumber) from contestant  where EventId=1 and (rank>0 and rank<=10) and ChildNumber in ( select ChildNumber from contestant "
        cmdText = cmdText & "  where  EventId=2 and PaymentReference is not null group by ChildNumber having COUNT(distinct(ContestYear))>= @idx) "

        cmdText = cmdText & " END ELSE BEGIN"
        cmdText = cmdText & "  select @ChildCnt=count(distinct(childnumber))  from contestant  where EventId=2 and PaymentReference is not null and  ChildNumber in ( select  ChildNumber from contestant "
        cmdText = cmdText & "   where EventId=2 and PaymentReference is not null group by ChildNumber  having COUNT(distinct(ContestYear))= @idx ) "
        cmdText = cmdText & "  	select @ChildRankRangeCnt=count(childnumber) from contestant  where EventId=1 and (rank>0 and rank<=10) and ChildNumber in ( select ChildNumber from contestant "
        cmdText = cmdText & "  where  EventId=2  and PaymentReference is not null group by ChildNumber having COUNT(distinct(ContestYear))= @idx) "
        cmdText = cmdText & " END"
        cmdText = cmdText & "  insert into @Table values ( @idx, @ChildCnt,@ChildRankRangeCnt) "
        cmdText = cmdText & "  	set @idx=@idx+1 "
        cmdText = cmdText & "    End "
        cmdText = cmdText & "   select YearCnt,ChildCnt,ChildRankRangeCnt from @Table"
        Dim ds1 As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, cmdText)
        Dim dt1 As DataTable = ds1.Tables(0)
        iRowCount = 17
        For i = 0 To dt1.Rows.Count - 1
            Dim dr As DataRow = dt1.Rows(i)
            oSheet.Range("C" & Trim(Str(iRowCount))).Value = dr("YearCnt")
            oSheet.Range("D" & Trim(Str(iRowCount))).Value = dr("ChildCnt")
            oSheet.Range("E" & Trim(Str(iRowCount))).Value = dr("ChildRankRangeCnt")
            oSheet.Range("F" & Trim(Str(iRowCount))).Formula = "=E" & Trim(Str(iRowCount)) & "/D" & Trim(Str(iRowCount))
            oSheet.Range("F" & Trim(Str(iRowCount))).NumberFormat = "00.0%"
            iRowCount = iRowCount + 1
        Next



        oSheet.Range("E12").Formula = "=SUM(E3:E11)"
        oSheet.Range("I12").Formula = "=SUM(I3:I11)"
        oSheet.Range("M12").Formula = "=SUM(M3:M11)"
        oSheet.Range("Q12").Formula = "=SUM(Q3:Q11)"
        oSheet.Range("U12").Formula = "=SUM(U3:U11)"
        oSheet.Range("E26").Formula = "=SUM(E17:E25)"
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()
    End Sub
End Class
