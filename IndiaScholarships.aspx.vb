﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NativeExcel
Imports Excel = Microsoft.Office.Interop.Excel
Imports System.IO

Partial Class IndiaScholarships
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            LoadIndianChapters()
            'LoadIndianInstitutions()
            LoadCourses()
            GetStates()
            loadgrid(DGDonorDesig)
            loadgridHS(dgHighSchool)
        End If
    End Sub
    Private Sub LoadIndianChapters()
        Dim conn As SqlConnection
        conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
        conn.Open()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "SELECT * FROM rd_Chapter order by Chapter,State_code,Chapter_Code")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlChapter.DataSource = ds
            ddlChapter.DataBind()
        End If
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", 0))
        ddlChapter.SelectedIndex = 0
        conn.Close()
    End Sub
    Private Sub LoadIndianInstitutions()
        Dim conn As SqlConnection
        conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
        conn.Open()
        'Dim State_Code As String = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "SELECT State_code From rd_Chapter Where ChapterID=" & ddlChapter.SelectedValue) '& ChapterId) '
        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "SELECT * FROM rd_institution I , rd_chapter C  Where charindex(Lower(RTRIM(LTRIM(C.State_Code))),I.InstitutionCode,0)>0 and c.ChapterID=" & ddlChapter.SelectedValue & "order by InstitutionName") 'Where InstitutionCode like '" & State_Code.ToLower & "%' order by InstitutionName") 'ddlChapter.SelectedValue.Trim.ToLower
        If ds.Tables(0).Rows.Count > 0 Then
            ddlInstitution.DataSource = ds
            ddlInstitution.DataBind()
        End If
        ddlInstitution.Items.Insert(0, New ListItem("Select College", 0))
        ddlInstitution.SelectedIndex = 0
        conn.Close()
    End Sub
    Private Sub LoadCourses()
        Dim conn As SqlConnection
        conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
        conn.Open()

        Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, "SELECT * FROM rd_Course order by crs")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlCourse.DataSource = ds
            ddlCourse.DataBind()
        End If
        ddlCourse.Items.Insert(0, New ListItem("Select Course", 0))
        ddlCourse.SelectedIndex = 0
        conn.Close()
    End Sub
    Private Sub GetStates()

        Dim dsStates As DataSet
        dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
        ddlState.DataSource = dsStates.Tables(0)
        ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString()
        ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString()
        ddlState.DataBind()
        ddlState.Items.Insert(0, New ListItem("Select State", String.Empty))
        ddlState.SelectedIndex = 0
    End Sub

    Protected Sub btnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAdd.Click
        'Add new
        Try
            If txtDonor.Text = "" Then
                lblError.Text = "Please Select Donor"
                Exit Sub
            ElseIf ddlDesignation.SelectedIndex = 0 Then
                lblError.Text = "Please Select Designation"
                Exit Sub
            ElseIf txtNumber.Text = "" Then
                lblError.Text = "Please give number"
                Exit Sub
            ElseIf ddlChapter.SelectedIndex = 0 Then
                lblError.Text = "Please Select Chapter"
                Exit Sub
            ElseIf ddlTenure.SelectedIndex = 0 Then
                lblError.Text = "Please select Tenure"
                Exit Sub
            ElseIf ddlStatus.SelectedIndex = 0 Then
                lblError.Text = "Please select Status"
                Exit Sub
            End If

            'Dim conn As SqlConnection
            ' conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
            ' conn.Open()

            'Dim ChapterID As Integer = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT ChapterId FROM rd_Chapter Where Chapter like '%" & ddlChapter.SelectedItem.Text & "%'")
            '  conn.Close()

            Dim SQLInsert As String = "" '
            Dim SQLUpdate As String = "" '
            SQLInsert = SQLInsert & "INSERT INTO DonorDesig(Donortype,MemberId,Designation,Number,Tenure,Status,IndianChapterID,IndianInstitutionCode, GeographicRegion,CourseId,Comments,CreatedDate,CreatedBy) VALUES("
            SQLInsert = SQLInsert & "'" & lblDonorType.Text & "'," & hdnMemberId.Value & ",'" & ddlDesignation.SelectedItem.Text & "'," & txtNumber.Text & ",'" & ddlTenure.SelectedItem.Text & "','" & ddlStatus.SelectedItem.Text & "'," & ddlChapter.SelectedItem.Value & "," & IIf(ddlInstitution.SelectedIndex = 0, "NULL", "'" & ddlInstitution.SelectedItem.Value & "'") & ",'" & txtGeoRegion.Text & "'," & IIf(ddlCourse.SelectedIndex = 0, "NULL", "'" & ddlCourse.SelectedItem.Value & "'") & ",'" & txtComments.Text & "',GetDate()," & Session("LoginID") & ")"
            SQLUpdate = SQLUpdate & "Update DonorDesig set Donortype ='" & lblDonorType.Text & "',MemberId=" & hdnMemberId.Value & ",Designation='" & ddlDesignation.SelectedItem.Text & "',Number=" & txtNumber.Text & ",IndianChapterID=" & ddlChapter.SelectedItem.Value & ",IndianInstitutionCode=" & IIf(ddlInstitution.SelectedIndex = 0, "NULL", "'" & ddlInstitution.SelectedItem.Value & "'") & ", GeographicRegion='" & txtGeoRegion.Text & "',CourseId=" & IIf(ddlCourse.SelectedIndex = 0, "NULL", "'" & ddlCourse.SelectedItem.Value & "'") & ",Comments='" & txtComments.Text & "',Tenure='" & ddlTenure.SelectedItem.Text & "',Status='" & ddlStatus.SelectedItem.Text & "',ModifiedDate=GetDate(),ModifiedBy=" & Session("LoginID") & " Where SchDesg_ID=" & lblSchDesg_ID.Text

            If btnAdd.Text = "Add" Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLInsert) > 0 Then
                    clear()
                    lblError.Text = "Added Sucessfully"
                    loadgrid(DGDonorDesig)
                    loadgridHS(dgHighSchool)

                End If
            ElseIf btnAdd.Text = "Update" Then
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLUpdate) > 0 Then
                    loadgrid(DGDonorDesig)
                    loadgridHS(dgHighSchool)
                    btnAdd.Text = "Add"
                    clear()
                    lblError.Text = "Updated Sucessfully"
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub DGDonorDesig_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Try
            lblError.Text = ""
            Dim index As Integer = e.Item.ItemIndex ' Convert.ToInt32(e.CommandArgument.ToString)

            Dim SchDesg_ID As Integer = DGDonorDesig.DataKeys(e.Item.ItemIndex) '.DataKeys(index)..Value
            Dim delcell As Integer = 0 ' position of the cell 'e.Item.Cells.Count - 1 'e.Item.ItemIndex 
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(delcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")

            If e.CommandName = "Delete" Then
                SchDesg_ID = DGDonorDesig.DataKeys(e.Item.ItemIndex) '.DataKeys(index)..Value
                'delete routine
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete From DonorDesig Where SchDesg_ID=" & SchDesg_ID) > 0 Then
                    lblError.Text = "Record deleted Successfully."
                End If
                loadgrid(DGDonorDesig)
                lblSchDesg_ID.Text = SchDesg_ID.ToString()
            ElseIf e.CommandName = "Modify" Then
                btnAdd.Text = "Update"
                lblSchDesg_ID.Text = SchDesg_ID.ToString()
                loadforUpdate(SchDesg_ID)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Private Sub loadforUpdate(ByVal SchDesg_ID As Integer)
        Try
            'DGDonorDesig.EditItemIndex = CInt(e.Item.ItemIndex)
            ' Dim SchDesg_ID As Integer = DGDonorDesig.DataKeys(e.Item.ItemIndex) 'CInt(e.Item.Cells(3).Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT D.SchDesg_ID,D.Donortype,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME  ELSE REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') END as DonorName,D.MemberID,D.Designation,D.Number,D.Tenure,D.Status,D.IndianChapterID,D.IndianInstitutionCode,D.CourseId,D.GeographicRegion,D.Comments From DonorDesig D Left Join IndSpouse I on (I.AutoMemberID =D.MemberID  AND D.DonorType <> 'OWN')Left Join OrganizationInfo O on (O.AutoMemberID = D.MemberID AND D.DonorType = 'OWN') Where SchDesg_ID=" & SchDesg_ID)
            If ds.Tables(0).Rows.Count > 0 Then
                txtDonor.Text = IIf(ds.Tables(0).Rows(0)("DonorName") Is DBNull.Value, "", ds.Tables(0).Rows(0)("DonorName"))
                lblDonorType.Text = IIf(ds.Tables(0).Rows(0)("DonorType") Is DBNull.Value, "", ds.Tables(0).Rows(0)("DonorType"))
                hdnMemberId.Value = IIf(ds.Tables(0).Rows(0)("MemberId") Is DBNull.Value, "", ds.Tables(0).Rows(0)("MemberId"))
                ddlDesignation.SelectedIndex = ddlDesignation.Items.IndexOf(ddlDesignation.Items.FindByValue(ds.Tables(0).Rows(0)("Designation")))
                txtNumber.Text = IIf(ds.Tables(0).Rows(0)("Number") Is DBNull.Value, "", ds.Tables(0).Rows(0)("Number"))
                ddlTenure.SelectedIndex = ddlTenure.Items.IndexOf(ddlTenure.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Tenure") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Tenure"))))
                ddlStatus.SelectedIndex = ddlStatus.Items.IndexOf(ddlStatus.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Status") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Status"))))
                LoadIndianChapters()
                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(ds.Tables(0).Rows(0)("IndianChapterID")))
                LoadIndianInstitutions() 'ds.Tables(0).Rows(0)("IndianChapterID")
                ddlInstitution.SelectedIndex = ddlInstitution.Items.IndexOf(ddlInstitution.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("IndianInstitutionCode") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("IndianInstitutionCode"))))
                txtGeoRegion.Text = IIf(ds.Tables(0).Rows(0)("GeographicRegion") Is DBNull.Value, "", ds.Tables(0).Rows(0)("GeographicRegion"))
                LoadCourses()
                ddlCourse.SelectedIndex = ddlCourse.Items.IndexOf(ddlCourse.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("Courseid") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("Courseid"))))
                txtComments.Text = IIf(ds.Tables(0).Rows(0)("Comments") Is DBNull.Value, "", ds.Tables(0).Rows(0)("Comments"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim state As String = String.Empty
        Dim email As String = String.Empty
        Dim orgname As String = String.Empty

        Dim strSql As StringBuilder = New StringBuilder()
        Dim strSqlOrg As StringBuilder = New StringBuilder()
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        state = ddlState.SelectedItem.Value.ToString()
        email = txtEmail.Text
        orgname = txtOrgName.Text

        If orgname.Length > 0 Then
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'")
        End If

        If (firstName.Length > 0) Then
            strSql.Append(" I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            Dim length As Integer = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append(" I.lastName like '%" + lastName + "%'")
            End If
        End If

        If (state.Length > 0) Then
            Dim length As Integer = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.State like '%" + state + "%'")
            Else
                strSql.Append("  I.State like '%" + state + "%'")
            End If
        End If

        If (email.Length > 0) Then
            Dim length As Integer = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If (state.Length > 0) Then
            Dim length As Integer = strSql.ToString().Length
            If (length > 0) Then
                strSqlOrg.Append(" and O.State like '%" + state + "%'")
            Else
                strSqlOrg.Append(" O.State like '%" + state + "%'")
            End If
        End If

        If (email.Length > 0) Then
            Dim length As Integer = strSql.ToString().Length
            If (length > 0) Then
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'")
            Else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'")
            End If
        End If

        If (firstName.Length > 0 Or orgname.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Or state.Length > 0) Then
            strSql.Append(" order by I.lastname,I.firstname")
            If (ddlDonorType.SelectedValue = "INDSPOUSE") Then
                SearchMembers(strSql.ToString())
            Else
                SearchOrganization(strSqlOrg.ToString())
            End If
        Else
            lblIndSearch.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state"
            lblIndSearch.Visible = True
            Return
        End If
    End Sub

    Private Sub SearchOrganization(ByVal sqlSt As String)

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " & sqlSt)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " & sqlSt)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub BtnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnSearch.Click

        txtLastName.Text = String.Empty
        txtFirstName.Text = String.Empty
        txtOrgName.Text = String.Empty

        pIndSearch.Visible = True
        txtFirstName.Text = ""
        txtLastName.Text = ""
        txtEmail.Text = ""

    End Sub
    Protected Sub ddlDonorType_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (ddlDonorType.SelectedValue = "Organization") Then
            txtLastName.Text = String.Empty
            txtFirstName.Text = String.Empty
            txtOrgName.Text = String.Empty
            txtLastName.Enabled = False
            txtFirstName.Enabled = False
            txtOrgName.Enabled = True
        Else
            txtLastName.Text = String.Empty
            txtFirstName.Text = String.Empty
            txtLastName.Enabled = True
            txtFirstName.Enabled = True
            txtOrgName.Enabled = False
            txtOrgName.Text = String.Empty
        End If
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As GridViewCommandEventArgs)

        Dim index As Integer = Convert.ToInt32(e.CommandArgument.ToString)
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        txtDonor.Text = IIf(row.Cells(4).Text.Trim() = "OWN", row.Cells(1).Text.Trim(), row.Cells(1).Text.Trim() & " " & row.Cells(2).Text.Trim())
        hdnMemberId.Value = GridMemberDt.DataKeys(index).Value
        lblDonorType.Text = row.Cells(4).Text.Trim()

        txtDonor.Enabled = False
        pIndSearch.Visible = False
        Panel4.Visible = False
    End Sub

    Private Sub loadgrid(ByVal DGDonorDesig As DataGrid)
        Try
            Dim strSQL As String = "SELECT D.SchDesg_ID,D.Donortype,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') END as DonorName,D.MemberID,D.Designation,D.Number,D.Tenure,D.Status,D.IndianChapterID,D.IndianInstitutionCode,D.CourseId,D.GeographicRegion,D.Comments From DonorDesig D Left Join IndSpouse I on (I.AutoMemberID =D.MemberID  AND D.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = D.MemberID AND D.DonorType = 'OWN') where D.Designation='College' order by I.lastname,I.Firstname,O.ORGANIZATION_NAME"
            '"select SchDesg_ID, MemberID,DonorType,,I.FirstName + ''+ I.MiddleInitial +'' +I.LastName as DonorName,Designation,Number,IndianChapterId,IndianInstitutionCode,GeographicRegion,CourseId,Comments FROM DonorDesig " ' EF.EventFeesID, EF.EventID,E.Name as EventName, EF.EventYear,EF.CoachSelCriteria, EF.ProductGroupID,PG.Name as ProductGroupName , EF.ProductID, P.Name as ProductName, EF.GradeFrom, EF.GradeTo,CONVERT(VARCHAR(10), EF.DeadlineDate, 101) AS DeadlineDate,CONVERT(VARCHAR(10), EF.LateRegDLDate, 101) AS LateRegDLDate,EF.RegFee, EF.LateFee,EF.TaxDedRegional,EF.DiscountAmt,EF.ProductLevelPricing,CONVERT(VARCHAR(10), EF.ChangeCoachDL, 101) AS ChangeCoachDL from EventFees EF INNER JOIN Event E ON E.EventId = EF.EventID INNER JOIN ProductGroup PG ON PG.ProductGroupId = EF.ProductGroupID Inner Join Product P ON P.ProductId  = EF.ProductID WHERE EF.EventID  = " & ddlEvent.SelectedValue & " and EF.EventYear = " & ddlEventYear.SelectedValue

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = New DataTable() 'ds.Tables(0)
            Dim dr As DataRow

            Dim InstitutionName, Chapter, course As String
            Dim conn As SqlConnection
            Dim Count As Integer

            dt.Columns.Add("SchDesg_ID", Type.GetType("System.Int32"))
            dt.Columns.Add("DonorType", Type.GetType("System.String"))
            dt.Columns.Add("MemberId", Type.GetType("System.Int32"))
            dt.Columns.Add("DonorName", Type.GetType("System.String"))
            dt.Columns.Add("Designation", Type.GetType("System.String"))
            dt.Columns.Add("Number", Type.GetType("System.Int32"))
            dt.Columns.Add("Tenure", Type.GetType("System.String"))
            dt.Columns.Add("Status", Type.GetType("System.String"))
            dt.Columns.Add("Chapter", Type.GetType("System.String"))
            dt.Columns.Add("InstitutionName", Type.GetType("System.String"))
            dt.Columns.Add("GeographicRegion", Type.GetType("System.String"))
            dt.Columns.Add("Course", Type.GetType("System.String"))
            dt.Columns.Add("Comments", Type.GetType("System.String"))

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                dr = dt.NewRow()
                dr("SchDesg_ID") = ds.Tables(0).Rows(i)("SchDesg_ID")
                dr("DonorType") = ds.Tables(0).Rows(i)("DonorType")
                dr("MemberId") = ds.Tables(0).Rows(i)("MemberId")
                dr("DonorName") = ds.Tables(0).Rows(i)("DonorName")
                dr("Designation") = ds.Tables(0).Rows(i)("Designation")
                dr("Number") = ds.Tables(0).Rows(i)("Number")
                dr("Tenure") = ds.Tables(0).Rows(i)("Tenure")
                dr("Status") = ds.Tables(0).Rows(i)("Status")
                dr("GeographicRegion") = IIf(ds.Tables(0).Rows(i)("GeographicRegion") Is DBNull.Value, "", ds.Tables(0).Rows(i)("GeographicRegion"))
                dr("Comments") = IIf(ds.Tables(0).Rows(i)("Comments") Is DBNull.Value, "", ds.Tables(0).Rows(i)("Comments"))

                conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
                conn.Open()
                Chapter = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT chapter FROM rd_Chapter where chapterID=" & ds.Tables(0).Rows(i)("IndianChapterID")) 'ddlChapter.SelectedValue) 'order by State_code,Chapter_Code")
                InstitutionName = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT InstitutionName FROM rd_institution Where InstitutionCode" & IIf(ds.Tables(0).Rows(i)("IndianInstitutionCode") Is DBNull.Value, " is null", "='" & ds.Tables(0).Rows(i)("IndianInstitutionCode") & "'")) ' order by InstitutionName") 'ddlChapter.SelectedValue.Trim.ToLower
                course = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT Crs from rd_course Where id " & IIf(ds.Tables(0).Rows(i)("Courseid") Is DBNull.Value, "is null", "=" & ds.Tables(0).Rows(i)("Courseid")))
                conn.Close()
                dr("Chapter") = Chapter
                dr("InstitutionName") = InstitutionName
                dr("course") = course
                dt.Rows.Add(dr)
            Next
            DGDonorDesig.DataSource = dt
            DGDonorDesig.Columns(2).Visible = False
            DGDonorDesig.DataBind()
            Session("ExcelData") = dt
            Count = dt.Rows.Count
            If (Count < 1) Then
                lblGridMsg.ForeColor = Color.Red
                lblGridMsg.Text = "No record available."
                DGDonorDesig.Visible = False
            Else
                DGDonorDesig.Visible = True
                lblGridMsg.Text = ""
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub loadgridHS(ByVal dgHighSchool As DataGrid)
        Try
            Dim strSQL As String = "SELECT D.SchDesg_ID,D.Donortype,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') END as DonorName,D.MemberID,D.Designation,D.Number,D.Tenure,D.Status,D.IndianChapterID,D.IndianInstitutionCode,D.CourseId,D.GeographicRegion,D.Comments From DonorDesig D Left Join IndSpouse I on (I.AutoMemberID =D.MemberID  AND D.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = D.MemberID AND D.DonorType = 'OWN') where D.Designation='HighSchool' order by I.lastname,I.Firstname,O.ORGANIZATION_NAME"
            '"select SchDesg_ID, MemberID,DonorType,,I.FirstName + ''+ I.MiddleInitial +'' +I.LastName as DonorName,Designation,Number,IndianChapterId,IndianInstitutionCode,GeographicRegion,CourseId,Comments FROM DonorDesig " ' EF.EventFeesID, EF.EventID,E.Name as EventName, EF.EventYear,EF.CoachSelCriteria, EF.ProductGroupID,PG.Name as ProductGroupName , EF.ProductID, P.Name as ProductName, EF.GradeFrom, EF.GradeTo,CONVERT(VARCHAR(10), EF.DeadlineDate, 101) AS DeadlineDate,CONVERT(VARCHAR(10), EF.LateRegDLDate, 101) AS LateRegDLDate,EF.RegFee, EF.LateFee,EF.TaxDedRegional,EF.DiscountAmt,EF.ProductLevelPricing,CONVERT(VARCHAR(10), EF.ChangeCoachDL, 101) AS ChangeCoachDL from EventFees EF INNER JOIN Event E ON E.EventId = EF.EventID INNER JOIN ProductGroup PG ON PG.ProductGroupId = EF.ProductGroupID Inner Join Product P ON P.ProductId  = EF.ProductID WHERE EF.EventID  = " & ddlEvent.SelectedValue & " and EF.EventYear = " & ddlEventYear.SelectedValue

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = New DataTable() 'ds.Tables(0)
            Dim dr As DataRow

            Dim InstitutionName, Chapter, course As String
            Dim conn As SqlConnection
            Dim Count As Integer

            dt.Columns.Add("SchDesg_ID", Type.GetType("System.Int32"))
            dt.Columns.Add("DonorType", Type.GetType("System.String"))
            dt.Columns.Add("MemberId", Type.GetType("System.Int32"))
            dt.Columns.Add("DonorName", Type.GetType("System.String"))
            dt.Columns.Add("Designation", Type.GetType("System.String"))
            dt.Columns.Add("Number", Type.GetType("System.Int32"))
            dt.Columns.Add("Tenure", Type.GetType("System.String"))
            dt.Columns.Add("Status", Type.GetType("System.String"))
            dt.Columns.Add("Chapter", Type.GetType("System.String"))
            dt.Columns.Add("InstitutionName", Type.GetType("System.String"))
            dt.Columns.Add("GeographicRegion", Type.GetType("System.String"))
            dt.Columns.Add("Course", Type.GetType("System.String"))
            dt.Columns.Add("Comments", Type.GetType("System.String"))

            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                dr = dt.NewRow()
                dr("SchDesg_ID") = ds.Tables(0).Rows(i)("SchDesg_ID")
                dr("DonorType") = ds.Tables(0).Rows(i)("DonorType")
                dr("MemberId") = ds.Tables(0).Rows(i)("MemberId")
                dr("DonorName") = ds.Tables(0).Rows(i)("DonorName")
                dr("Designation") = ds.Tables(0).Rows(i)("Designation")
                dr("Number") = ds.Tables(0).Rows(i)("Number")
                dr("Tenure") = ds.Tables(0).Rows(i)("Tenure")
                dr("Status") = ds.Tables(0).Rows(i)("Status")
                dr("GeographicRegion") = IIf(ds.Tables(0).Rows(i)("GeographicRegion") Is DBNull.Value, "", ds.Tables(0).Rows(i)("GeographicRegion"))
                dr("Comments") = IIf(ds.Tables(0).Rows(i)("Comments") Is DBNull.Value, "", ds.Tables(0).Rows(i)("Comments"))

                conn = New SqlConnection(System.Configuration.ConfigurationManager.ConnectionStrings("NSFIndiaConnectionString").ConnectionString)
                conn.Open()
                Chapter = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT chapter FROM rd_Chapter where chapterID=" & ds.Tables(0).Rows(i)("IndianChapterID")) 'ddlChapter.SelectedValue) 'order by State_code,Chapter_Code")
                InstitutionName = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT InstitutionName FROM rd_institution Where InstitutionCode" & IIf(ds.Tables(0).Rows(i)("IndianInstitutionCode") Is DBNull.Value, " is null", "='" & ds.Tables(0).Rows(i)("IndianInstitutionCode") & "'")) ' order by InstitutionName") 'ddlChapter.SelectedValue.Trim.ToLower
                course = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT Crs from rd_course Where id " & IIf(ds.Tables(0).Rows(i)("Courseid") Is DBNull.Value, "is null", "=" & ds.Tables(0).Rows(i)("Courseid")))
                conn.Close()
                dr("Chapter") = Chapter
                dr("InstitutionName") = InstitutionName
                dr("course") = course
                dt.Rows.Add(dr)
            Next
            dgHighSchool.DataSource = dt
            dgHighSchool.Columns(2).Visible = False
            dgHighSchool.DataBind()
            Session("ExcelDataHS") = dt
            Count = dt.Rows.Count
            If (Count < 1) Then
                lblGridMsg2.ForeColor = Color.Red
                lblGridMsg2.Text = "No record available."
                dgHighSchool.Visible = False
            Else
                dgHighSchool.Visible = True
                lblGridMsg2.Text = ""
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub clear()
        btnAdd.Text = "Add"
        lblSchDesg_ID.Text = ""
        lblDonorType.Text = ""
        hdnMemberId.Value = ""
        txtDonor.Text = ""
        ddlDesignation.SelectedIndex = 0
        txtNumber.Text = ""
        ddlChapter.SelectedIndex = 0
        ddlInstitution.Items.Clear()
        txtGeoRegion.Text = ""
        ddlCourse.SelectedIndex = 0
        txtComments.Text = ""
        lblError.Text = ""
        ddlTenure.SelectedIndex = 0
        ddlStatus.SelectedIndex = 0
    End Sub
    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub
    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        LoadIndianInstitutions()
    End Sub
    Protected Sub BtnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnExport.Click
        Dim attachment As String = "attachment; filename=ScholarshipDesignations.xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", attachment)
        Response.ContentType = "application/vnd.ms-excel"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        loadgrid(dgexport)
        DGDonorDesig.Columns(0).Visible = False
        DGDonorDesig.Columns(1).Visible = False
        DGDonorDesig.HeaderStyle.BackColor = Color.White
        DGDonorDesig.HeaderStyle.ForeColor = Color.Black
        DGDonorDesig.ItemStyle.ForeColor = Color.Black
        DGDonorDesig.ItemStyle.BackColor = Color.White
        DGDonorDesig.AlternatingItemStyle.BackColor = Color.White
        DGDonorDesig.AlternatingItemStyle.ForeColor = Color.Black


        DGDonorDesig.RenderControl(htmlWrite)
        Response.Write("<center><b><font color=blue> Table 1: College Scholarship Designations</font></b></center>")
        Response.Write(stringWrite.ToString())
     
        DGDonorDesig.Columns(0).Visible = True
        DGDonorDesig.Columns(1).Visible = True

        'High School excel report

        Dim stringWrite1 As New System.IO.StringWriter()
        Dim htmlWrite1 As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite1)
        Dim dgexport1 As New DataGrid
        loadgridHS(dgexport1)
        dgHighSchool.Columns(0).Visible = False
        dgHighSchool.Columns(1).Visible = False
        dgHighSchool.HeaderStyle.BackColor = Color.White
        dgHighSchool.HeaderStyle.ForeColor = Color.Black
        dgHighSchool.ItemStyle.ForeColor = Color.Black
        dgHighSchool.ItemStyle.BackColor = Color.White
        dgHighSchool.AlternatingItemStyle.BackColor = Color.White
        dgHighSchool.AlternatingItemStyle.ForeColor = Color.Black

        dgHighSchool.RenderControl(htmlWrite1)
        Response.Write("<center><b><font color=blue> Table 2: High School Scholarship Designations</font></b></center>")
        Response.Write(stringWrite1.ToString())
        Response.[End]()
        Response.End()
        dgHighSchool.Columns(0).Visible = True
        dgHighSchool.Columns(1).Visible = True
    End Sub

    Protected Sub ExporttoExcel(ByRef dt As DataTable)

        Dim filename As String = "attachment; filename=ScholarshipDesignations.xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", filename)
        Response.ContentType = "application/vnd.ms-excel"
        Dim tab As String = ""
        For Each dc As DataColumn In dt.Columns
            tab = "" & vbTab
            Response.Write((tab + dc.ColumnName))
        Next
        Response.Write("" & vbLf)
        Dim i As Integer
        Dim tempstr As String
        For Each dr As DataRow In dt.Rows
            tab = ""
            i = 0
            Do While (i < dt.Columns.Count)
                tab = "" & vbTab
                Response.Write((dr(i).ToString) + tab)
                tempstr = dr(i).ToString

                i = (i + 1)
            Loop
            Response.Write("" & vbLf)
        Next
        Response.End()

        'Response.Clear()
        'Response.Buffer = True
        'Response.AddHeader("content-disposition", _
        '        "attachment;filename=DataTable.csv")
        'Response.Charset = ""
        'Response.ContentType = "application/text"

        'Dim sb As New StringBuilder()
        'For k As Integer = 0 To dt.Columns.Count - 1
        '    'add separator
        '    ' sb.Append(dt.Columns(k).ColumnName + ","c)
        '    Response.Write(dt.Columns(k).ColumnName & ",")
        'Next
        ''append new line
        '' sb.Append(vbCr & vbLf)
        'Response.Write(vbCrLf)
        'For i As Integer = 0 To dt.Rows.Count - 1
        '    For k As Integer = 0 To dt.Columns.Count - 1
        '        'add separator             
        '        'sb.Append(dt.Rows(i)(k).ToString().Replace(",", ";") + ","c)
        '        Response.Write(dt.Rows(i)(k).ToString() & ",")
        '    Next
        '    'append new line
        '    sb.Append(vbLf)
        '    Response.Write(vbCrLf)
        'Next


        'Response.Flush()
        'Response.End()
    End Sub
    'Private Sub makeCSV()
    '    ' Create the CSV file to which grid data will be exported.

    '    Dim sw As New StreamWriter(Server.MapPath("griddata.txt"))
    '    ' First we will write the headers.

    '    'Dim dv As DataView = DirectCast(SqlDataSource_Gymnasts.[Select](DataSourceSelectArguments.Empty), DataView)
    '    'Dim dt As DataTable = dv.Table
    '    ' Dim dt As DataTable = Se

    '    Dim iColCount As Integer = dt.Columns.Count
    '    ' set int i = where to start the row collection from - 0 is the start

    '    Dim i As Integer = 0
    '    While i < 3
    '        sw.Write(dt.Columns(i))

    '        If i < iColCount - 1 Then

    '            sw.Write(",")
    '        End If

    '        System.Math.Max(System.Threading.Interlocked.Increment(i), i - 1)

    '    End While

    '    sw.Write(sw.NewLine)

    '    ' Now write all the rows.

    '    For Each dr As DataRow In dt.Rows
    '        Dim x As Integer = 0

    '        While x < 3
    '            If Not Convert.IsDBNull(dr(x)) Then

    '                sw.Write(dr(x))

    '            End If

    '            If x < iColCount - 1 Then

    '                sw.Write(",")
    '            End If

    '            System.Math.Max(System.Threading.Interlocked.Increment(x), x - 1)

    '        End While

    '        sw.Write(sw.NewLine)

    '    Next

    '    sw.Close()

    '    ' And finally, ask the user where thay want to save the file

    '    Response.ContentType = "text/plain"

    '    Response.AddHeader("Content-Disposition", "attachment; filename=Gymnasts.csv")
    '    Response.TransmitFile(Server.MapPath("griddata.txt"))
    '    Response.[End]()

    'End Sub

    'Protected Sub Button1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Button1.Click
    '    makeCSV()

    'End Sub
    Public Overrides Sub VerifyRenderingInServerForm(ByVal control As Control)
        ' Verifies that the control is rendered
    End Sub

    Protected Sub btnIndClose_onclick(sender As Object, e As EventArgs) Handles btnIndClose.Click
        pIndSearch.Visible = False

    End Sub

    Protected Sub dgHighSchool_ItemCommand(source As Object, e As DataGridCommandEventArgs) Handles dgHighSchool.ItemCommand
        Try
            lblError.Text = ""
            Dim index As Integer = e.Item.ItemIndex ' Convert.ToInt32(e.CommandArgument.ToString)

            Dim SchDesg_ID As Integer = dgHighSchool.DataKeys(e.Item.ItemIndex) '.DataKeys(index)..Value
            Dim delcell As Integer = 0 ' position of the cell 'e.Item.Cells.Count - 1 'e.Item.ItemIndex 
            Dim myTableCell As TableCell
            myTableCell = e.Item.Cells(delcell)
            Dim btnDelete As Button = myTableCell.Controls(0)
            btnDelete.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")

            If e.CommandName = "Delete" Then
                SchDesg_ID = dgHighSchool.DataKeys(e.Item.ItemIndex) '.DataKeys(index)..Value
                'delete routine
                If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete From DonorDesig Where SchDesg_ID=" & SchDesg_ID) > 0 Then
                    lblError.Text = "Record deleted Successfully."
                End If
                loadgridHS(dgHighSchool)
                lblSchDesg_ID.Text = SchDesg_ID.ToString()
            ElseIf e.CommandName = "Modify" Then
                btnAdd.Text = "Update"
                lblSchDesg_ID.Text = SchDesg_ID.ToString()
                loadforUpdate(SchDesg_ID)
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
End Class

