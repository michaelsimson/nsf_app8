﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRSummary.aspx.vb" Inherits="FundRSummary" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left">
    <asp:HyperLink  CssClass ="btn_02" ID="hlnkMainPage" runat="server"></asp:HyperLink>
</div>
<div align="center" style="width :1000px">
<table border="0" cellpadding ="3"  cellspacing = "0" >
    <tr><td align="center">
          <div style="font-size:14px;color:#008000;font-weight:bold">
          <asp:Label ID="lblEventDesc" runat="server"></asp:Label> </div>
    <h4>Selected Products for Fund Raising Events </h4>
    </td> </tr> 
    <tr id="divCheck" runat="server" visible="false"><td align="center" >
        <asp:datagrid id="dgCheck" Visible="false"  CellPadding="7" OnItemDataBound="dgCheck_ItemDataBound" runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" 
          DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF" 
          BorderStyle="None" GridLines="Horizontal">
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
	    <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeFrom" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeFrom") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeTo" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeTo") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeEnabled" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeEnabled") %>'>		</asp:Label> 											
        <asp:Label id="lblQuantityEnabled" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityEnabled") %>'>		</asp:Label> 											
        <asp:Label id="lblPaymentMethod" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label> 											
        <asp:Label runat="server" ID="lblAmount" Visible="false"  CssClass="SmallFont"></asp:Label>
        <asp:Label id="lblFundRFeesID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label> 											
        <asp:Label id="lblProductCode" runat="server" Visible="false"  CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
       <asp:Label id="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
        </ItemTemplate>
          <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
                <asp:Label runat="server" ID="lblSelfees" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
      </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
                    <asp:Label runat="server" ID="lblSelPaymentMode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"SelPaymentMode") %>'></asp:Label>

        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
         <asp:Label runat="server" ID="lblQuantity" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Check"  HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label   id="lblCheckAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label> 											
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn  Visible="false" HeaderText="Credit_Card" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblCreditCardAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn Visible="false"  HeaderText="In Kind" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblInKindAmt"   Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>       
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
		<div style="padding:5px" align="right" >
            <asp:Label ID="lblCheckAmt" runat="server"></asp:Label><br />
            <asp:Button ID="btnExit" Visible="false" OnClick="btnExit_Click" runat="server" Width="75px" Text="Exit" />
             </div>
            
			</td></tr>
    <tr><td align="center" >
        <asp:datagrid id="dgCatalog" Visible="false" CellPadding="7" OnItemDataBound="dgCatalog_ItemDataBound" runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" 
          DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF" 
          BorderStyle="None" GridLines="Horizontal">
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
	    <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeFrom" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeFrom") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeTo" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeTo") %>'>		</asp:Label> 											
        <asp:Label id="lblFeeEnabled" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FeeEnabled") %>'>		</asp:Label> 											
        <asp:Label id="lblQuantityEnabled" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "QuantityEnabled") %>'>		</asp:Label> 											
        <asp:Label id="lblPaymentMethod" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label> 											
        <asp:Label runat="server" ID="lblAmount" Visible="false"  CssClass="SmallFont"></asp:Label>
        <asp:Label id="lblFundRFeesID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label> 											
        <asp:Label id="lblProductCode" runat="server" Visible="false"  CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
       <asp:Label id="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
        </ItemTemplate>
          <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
                <asp:Label runat="server" ID="lblSelfees" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
      </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
                    <asp:Label runat="server" ID="lblSelPaymentMode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"SelPaymentMode") %>'></asp:Label>

        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
         <asp:Label runat="server" ID="lblQuantity" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn Visible="false" HeaderText="Check" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label   id="lblCheckAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label> 											
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn  HeaderText="Credit_Card" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblCreditCardAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn Visible="false"  HeaderText="In Kind" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblInKindAmt"   Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>       
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
			</td></tr>
		<tr><td align = "right" >
		  <asp:Label ID="lblTotal" runat="server"></asp:Label>	
            <br />
		  <asp:Label ID="lblTotalDiscount" runat="server" Visible="False"></asp:Label>			   
		</td> </tr>
    <tr><td align = "left" >
		 Adults attending: <b><asp:Label ID="lblAdult" runat="server"></asp:Label></b>	&nbsp;&nbsp;&nbsp;Children attending: <b><asp:Label ID="lblChildren" runat="server"></asp:Label></b>	
        </td></tr>
		<tr><td align = "right" >
		         <asp:Button ID="btnPay" runat="server" Width="100px" OnClick="btnPay_Click" Text="Pay" />&nbsp;&nbsp;
		</td> </tr> 
		 </table>
	</div> 
</asp:Content>

