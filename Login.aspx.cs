﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Xml.Linq;
using AjaxControlToolkit;
using System.Data.SqlClient;
using System.ComponentModel;
using System.Web.SessionState;
using System.Collections.Generic;
using System.Drawing;
using System.Windows.Forms;

public partial class Login : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {



        if (!IsPostBack)
        {

            Session["VID"] = "";


            if (HttpContext.Current.Request.Cookies["Appcookiename"] != null)
            {
                txtUserName.Text = HttpContext.Current.Request.Cookies["Appcookiename"]["LoginName"].ToString();
                string pass = HttpContext.Current.Request.Cookies["Appcookiename"]["Password"].ToString();
                txtPassword.Attributes.Add("value", pass);
                chkRememberMe.Checked = true;
            }

          
        }

    }
    protected void lnkNewUser_Click(object sender, EventArgs e)
    {
        Session["Page"] = "NewScholars";
        Response.Redirect("Registration.aspx");
    }

    protected void btnLogin_Click(object sender, EventArgs e)
    {

    
        try
        {

            if (chkRememberMe.Checked == true)
            {
                HttpCookie cookies = new HttpCookie("Appcookiename");
                cookies.Values.Add("LoginName", txtUserName.Text);
                cookies.Values.Add("Password", txtPassword.Text);
                cookies.Expires = DateTime.Now.AddDays(120);
                Response.Cookies.Add(cookies);
            }
            BLUser obj = new BLUser();

            obj.UserId = txtUserName.Text;
            obj.Passward = txtPassword.Text;

            DataTable dtuser = BLUser.GetUser(obj);
            if (dtuser.Rows.Count > 0)
            {
                if (dtuser.Rows[0][2].ToString() == "Applicant")
                {
                    HttpContext.Current.Session["newid"] = dtuser.Rows[0][0].ToString();
                    HttpContext.Current.Session["eMail"] = dtuser.Rows[0][1].ToString();
                    HttpContext.Current.Session["EmailID"] = dtuser.Rows[0][1].ToString();
                    HttpContext.Current.Session["UserID"] = dtuser.Rows[0][3].ToString();
                    HttpContext.Current.Session["SR_NO"] = dtuser.Rows[0][4].ToString();
                    HttpContext.Current.Session["SRNO"] = dtuser.Rows[0][4].ToString();
                    HttpContext.Current.Session["USER_TYPE"] = dtuser.Rows[0][2].ToString();
                    HttpContext.Current.Session["USERTYPE"] = dtuser.Rows[0][2].ToString();
                    HttpContext.Current.Session["Role"] = "Applicant";
                    HttpContext.Current.Session["PageIndex"] = "";
                    Session["Page"] = "Login";
                   
                    Response.Redirect("ApplicantFunction.aspx");
                }
               
            }
            else
            {
                lblError.Visible = true;
                lblError.Text = "Invalid UserID/Password!";
            }
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.Text = "ErrorMessage: " + ex.Message;
        }
    }    
    protected void lnkVolLogin_Click(object sender, EventArgs e)
    {
        Response.Redirect("VolunteerLogin.aspx");
    }
    protected void lnkChangePassword_Click(object sender, EventArgs e)
    {
        Response.Redirect("ChangePassword.aspx");
    }

} 
