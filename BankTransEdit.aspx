﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="BankTransEdit.aspx.vb" Inherits="BankTransEdit" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" >
   <head id="Head1" runat="server">
      <title>Donation Receipt</title>
      <link href="css/style.css" type="text/css" rel="stylesheet" />
   </head>
   <body>
      <form id="form1" runat="server">
         <div style="width:950px; text-align:center">
            <table cellspacing="1"  cellpadding="3" style="border:0; width :1000px;text-Align:center; background-color:White;" >
               <tr>
                  <td align="left">
                     <asp:hyperlink id="hlinkParentRegistration" CssClass="btn_02" runat="server" NavigateUrl="~/ManageVoucher.aspx?id=1" >Back to Manage Vouchers</asp:hyperlink>
                     &nbsp;&nbsp;&nbsp;
                  </td>
               </tr>
               <tr>
                  <td class="ItemCenter" align="center"><font face="Arial" size="4">North South Foundation<br />					       
                     BANK DEPOSIT EDITS</font>
                  </td>
               </tr>
               <tr runat="server" id="TrDetailView"  bgcolor="#FFFFFF">
                  <td  align="center">
                      <asp:DataGrid  ID="grdEditVoucher" runat="server"   AutoGenerateColumns="False" AllowSorting="True" DataKeyField="TransactionID"
                        OnItemCommand="grdEditVoucher_ItemCommand" GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666">
                        <%--OnUpdateCommand="grdEditVoucher_UpdateCommand" OnEditCommand="grdEditVoucher_EditCommand"--%>
                        <HeaderStyle Font-Bold="true" ForeColor="White"  />
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                           <%-- <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Select for Match" ></asp:EditCommandColumn>
                              --%> 
                           <asp:ButtonColumn  runat="server"  CommandName="Select for Match" Visible="true" Text="Select for Match"> </asp:ButtonColumn>
                           <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="TransactionID" ReadOnly="true" DataField="TransactionID"></asp:BoundColumn>
                           <asp:TemplateColumn  HeaderText="DepositSlip#"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblDepositSlip" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepSlipNumber")%>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                 <asp:TextBox ID="txtDepositSlip" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositSlip")%>'></asp:TextBox>
                              </EditItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Amount"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblAmount" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label>   <%--'<%#DataBinder.Eval(Container, "DataItem.Amount","{0:c}")%>'--%>
                              </ItemTemplate>
                              <%--<EditItemTemplate>
                                 <asp:TextBox ID="txtDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:TextBox>
                                 </EditItemTemplate>--%>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Transdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblTransdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Transdate","{0:d}")%>'></asp:Label>
                              </ItemTemplate>
                              <%-- <EditItemTemplate>
                                 <asp:TextBox ID="txtCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TRANSACTION_NUMBER")%>'></asp:TextBox>
                                 </EditItemTemplate>--%>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Description"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                          <%-- <asp:TemplateColumn   HeaderText="DepositNumber" >
                              <ItemTemplate>
                                 <asp:Label ID="lblDepositNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.DepositNumber")%>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>--%>
                            <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="BankID" ReadOnly="true" DataField="BankID"></asp:BoundColumn>
                          
                        </Columns>
                     </asp:DataGrid>
                   
                  </td>
               </tr>
               <tr id="TrDonationsView" runat="server">
                  <td align="center">
                      <asp:GridView Width="750px"  Visible="true" DataKeyNames="DepositSlip"  ID="gvEditDonation"   Caption="Donations Info" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" AllowSorting="True" DataKeyField="BankTransID"
                        GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666">
                        <HeaderStyle Font-Bold="true" ForeColor="White"  />
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="VoucherNo" HeaderText="Voucher#" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="DepositSlip" HeaderText="DepositSlip #" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="Amount"  DataFormatString="{0:F}" HeaderText="Amount" />
                           <%--DataFormatString="{0:c}" />--%>
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="DepositDate" HeaderText="DepositDate" DataFormatString="{0:d}" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="BankID" HeaderText="BankID" />
                        </Columns>
                       
                     </asp:GridView>
                  </td>
               </tr>
               <tr id ="TrBnkTrans" runat="server">
                  <td>
                   <asp:DataGrid  ID="grdBnkTransData" runat="server"   AutoGenerateColumns="False" AllowSorting="True" DataKeyField="TransactionID"
                        OnUpdateCommand="grdBnkTransData_UpdateCommand" OnEditCommand="grdBnkTransData_EditCommand" 	
                        GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666">
                        <%-- OnItemCommand="grdExpChecks_ItemCommand"--%>
                        <HeaderStyle Font-Bold="true" ForeColor="White"  />
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                           <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn>
                           <%--<asp:ButtonColumn  runat="server"  CommandName="Select for Match" Visible="true" Text="Select for Match"> </asp:ButtonColumn> 
                              --%> 
                           <asp:BoundColumn HeaderStyle-ForeColor="White" HeaderText="TransactionID" ReadOnly="true" DataField="TransactionID"></asp:BoundColumn>
                           <asp:TemplateColumn  HeaderText="CheckNumber"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblcheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckNumber")%>'></asp:Label>
                              </ItemTemplate>
                              <EditItemTemplate>
                                 <asp:TextBox ID="txtcheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckNo")%>'></asp:TextBox>
                              </EditItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Amount"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblAmount" runat="server" Text='<%# FormatNumber(DataBinder.Eval(Container, "DataItem.Amount"),2) %>'></asp:Label> <%--'<%#DataBinder.Eval(Container, "DataItem.Amount")%>'--%>
                              </ItemTemplate>
                              <%--<EditItemTemplate>
                                 <asp:TextBox ID="txtDonationdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Donationdate")%>'></asp:TextBox>
                                 </EditItemTemplate>--%>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Transdate"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblTransdate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Transdate","{0:d}")%>'></asp:Label>
                              </ItemTemplate>
                              <%-- <EditItemTemplate>
                                 <asp:TextBox ID="txtCheckNumber" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.TRANSACTION_NUMBER")%>'></asp:TextBox>
                                 </EditItemTemplate>--%>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="Description"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblDescription" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Description")%>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                           <asp:TemplateColumn  HeaderText="BankID"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                              <ItemTemplate>
                                 <asp:Label ID="lblBankID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.BankID")%>'></asp:Label>
                              </ItemTemplate>
                              <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                           </asp:TemplateColumn>
                        </Columns>
                     </asp:DataGrid>
                  </td>
               </tr>
               <tr id="TrExpJournal" runat="server">
                  <td>
                      <asp:GridView Width="750px"  Visible="true" DataKeyNames="CheckNumber"  ID="GVExpJournal"   Caption="Exp Journal Data" runat="server" AllowPaging="True"
                        AutoGenerateColumns="False" AllowSorting="True" DataKeyField="TransactionID"
                        GridLines="Horizontal" HeaderStyle-BackColor="#009900" CellPadding="4"  BorderWidth="3px" BorderStyle="Double"
                        BorderColor="#336666">
                        <HeaderStyle Font-Bold="true" ForeColor="White"  />
                        <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                        <Columns>
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="VoucherNo" HeaderText="Voucher#" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="CheckNumber" HeaderText="Check #" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="Amount" DataFormatString="{0:F}" HeaderText="Amount" />
                           <%--DataFormatString="{0:c}" />--%>
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="DatePaid" HeaderText="DatePaid" DataFormatString="{0:d}" />
                           <asp:BoundField HeaderStyle-ForeColor="White" ItemStyle-Width="15%"  HeaderStyle-Width="15%" DataField="BankID" HeaderText="BankID" />
                        </Columns>
                      </asp:GridView>
                  </td>
               </tr>
               <tr><td><asp:Label ID="lblErr" runat="server" ForeColor="Red"></asp:Label>
                     <br />
                     <br />
                    </td></tr>
            </table>
         </div>
      </form>
   </body>
</html>

