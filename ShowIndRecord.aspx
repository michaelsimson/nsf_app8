<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowIndRecord.aspx.vb" Inherits="ShowIndRecord" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>NSF-Indspouse Details</title>
    <link href="Styles.css" type="text/css" rel="stylesheet" />
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table id="tblMain" width="70%">
				<tr>
					<td class="Heading" colspan="2">IndSpouse Details</td>
				</tr>
				<tr>
					<td colspan="2">
					    <asp:datagrid id="dgIndSpouse" runat="server" Width="100%" CssClass="GridStyle" AllowSorting="True"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="memberid">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle  Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderStyle-Wrap="false"  HeaderText="Auto Member ID" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblAutoMemberID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AutomemberID") %>' CssClass="SmallFont">
										</asp:Label>
                                        <div style="display:none">
                                            <asp:Label id="lblDonorType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType")%>' CssClass="SmallFont">
										</asp:Label>
                                        </div>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Relationship" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Relationship") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="First Name" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>									
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Last Name" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblLastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Address1" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblAddress1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address1") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="City" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="State" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Zip" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Country" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Country")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Email" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>								
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Password" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblPwd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.user_pwd") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Gender" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblGender" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Gender")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Home Phone" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HPhone")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Cell Phone" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCellPhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CPhone")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
                <tr runat="server" id="trDupH" visible="false">
					<td class="Heading" colspan="2">Suspected Duplicate Records</td>
				</tr>
				<tr runat="server" id="trDupDetails" visible="false">
					<td colspan="2">
					    <asp:datagrid id="dgDupIndSpouse" runat="server" Width="100%" CssClass="GridStyle" AllowSorting="True"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="memberid">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle  Wrap="False"></HeaderStyle>
							<Columns>
                                 <asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Ind DuplicateID" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblIndDupId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.IndDupID") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>

								<asp:TemplateColumn HeaderStyle-Wrap="false"  HeaderText="Auto Member ID" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblAutoMemberID" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.AutomemberID") %>' CssClass="SmallFont">
										</asp:Label>
                                        <div style="display:none">
                                            <asp:Label id="lblDonorType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType")%>' CssClass="SmallFont">
										</asp:Label>
                                        </div>
									</ItemTemplate>
								</asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="DonorType" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblDType" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.DonorType") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>

								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Relationship" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblRelationship" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Relationship") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="First Name" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblFirstName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>									
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Last Name" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblLastName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Address1" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblAddress1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Address1") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="City" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCity" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="State" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblState" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Zip" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblZip" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Country" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCountry" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Country")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Email" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblEmail" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Email")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>								
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Password" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblPwd" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.user_pwd") %>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Gender" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblGender" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Gender")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Home Phone" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblPhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.HPhone")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								<asp:TemplateColumn  HeaderStyle-Wrap="false" HeaderText="Cell Phone" SortExpression="">
									<ItemTemplate>
										<asp:Label id="lblCellPhone" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.CPhone")%>' CssClass="SmallFont" >
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>		
								
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
						</asp:datagrid>
					</td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:label id="lblAlert" runat="server" Font-Bold="True" CssClass="SmallFont" ForeColor="Red"></asp:label></td>
				</tr>
				<tr>
					<td colspan="2" id="trT" runat="server" visible="false" >
                        
                          <div style="float:right; padding-right:450px;padding-top:30px">
                            <asp:Button ID="btnUnsubscribe" runat="server" Text="UnSubscribe" />

                              <br />
                              <br />
                              <table>
                                    <tr>
                                    <td style="font-weight:bold;background-color:rgb(225, 224, 224)">Newsletter</td><td>:</td><td><asp:Label ID="lblNewsletter" runat="server"></asp:Label></td>
                                </tr>
                              <tr>
                                    <td style="font-weight:bold;background-color:rgb(225, 224, 224)">Email</td><td>:</td><td><asp:Label ID="lblEmail" runat="server"></asp:Label></td>
                                </tr>
                                  <tr>
                                    <td style="font-weight:bold;background-color:rgb(225, 224, 224)">Created Date</td><td>:</td><td><asp:Label ID="lblCreatedDate" runat="server"></asp:Label></td>
                                </tr>
                              </table>
                        </div>   
                        <table style="padding-left:20px;" cellspacing="5">
                                <tr style="background-color:rgb(225, 224, 224)">
                                    <td colspan="3" style="font-weight:bold;"> Date of Latest Record: </td>
                                </tr>
                                    <tr>
                                    <td align="center" style="font-weight:bold;">Table</td><td></td><td align="center" style="font-weight:bold">Date</td>
                                </tr>
                                    <tr>
                                    <td>Contestant</td><td>:</td><td><asp:Label ID="lblConDT" runat="server"></asp:Label></td>
                                </tr>
                                    <tr>
                                    <td>Coaching</td><td>:</td><td><asp:Label ID="lblCoachDT" runat="server"></asp:Label></td>
                                </tr>
                                    <tr>
                                    <td>Workshop Onsite</td><td>:</td><td><asp:Label ID="lblOnSiteDT" runat="server"></asp:Label></td>
                                </tr>
                                    <tr>
                                    <td>Workshop Online</td><td>:</td><td><asp:Label ID="lblOnlineDT" runat="server"></asp:Label></td>
                                </tr>
                            <tr>
                                    <td>Game</td><td>:</td><td><asp:Label ID="lblGameDT" runat="server"></asp:Label></td>
                                </tr>
                            <tr>
                                    <td>Donation</td><td>:</td><td><asp:Label ID="lblDonationDT" runat="server"></asp:Label></td>
                                </tr>
                            

                        </table>    
                                       
					</td>
				</tr>				
 				<tr>
					<td>
					    <asp:Label ID="lblDebug" runat="server" Visible="false"></asp:Label>
					</td>
				</tr> 
	        </table>
        </div>
    </form>
</body>
</html>


 
 
 