﻿<%@ Page Title="" Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CoachSchedReport.aspx.vb" Inherits="CoachSchedReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script language="javascript" type="text/javascript">
        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CalSignUp Help', settings);
            PopupWindow.focus();
        }
    </script>
    <script language="javascript" type="text/javascript">
        function setonScroll(divObj, DgID) {
            var datagrid = document.getElementById(DgID);
            var HeaderCells = datagrid.getElementsByTagName('th');
            var HeaderRow;
            if (HeaderCells == null || HeaderCells.length == 0) {
                var AllRows = datagrid.getElementsByTagName('tr');
                HeaderRow = AllRows[0];
            }
            else {
                HeaderRow = HeaderCells[0].parentNode;
            }

            var DivsTopPosition = parseInt(divObj.scrollTop);

            if (DivsTopPosition > 0) {
                HeaderRow.style.position = 'absolute';
                HeaderRow.style.top = (parseInt(DivsTopPosition - 2)).toString() + 'px';
                HeaderRow.style.width = datagrid.style.width;
                HeaderRow.style.zIndex = '1000';
            }
            else {
                divObj.scrollTop = 0;
                HeaderRow.style.position = 'relative';
                HeaderRow.style.top = '0';
                HeaderRow.style.bottom = '0';
                HeaderRow.style.zIndex = '0';
            }

        }

    </script>
    <%--  <div>--%>
    <table border="0" cellpadding="3" cellspacing="0" width="980">
        <tr>
            <td align="left">
                <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>&nbsp;&nbsp;
            </td>
        </tr>
    </table>
    <table align="center">
        <tr>
            <td align="center" style="font-size: 16px; font-weight: bold; font-family: Calibri">Schedule Report</td>
        </tr>
    </table>
    <br />
    <table border="0" cellpadding="0" cellspacing="0" align="center">
        <tr>
            <td align="right" class="btn_02" style="width: 25px">Admin: </td>
            <td style="width: 25px">
                <asp:DropDownList ID="ddlRole" DataTextField="RoleCode" DataValueField="RoleID" runat="server"></asp:DropDownList></td>
            <td align="right" class="btn_02" style="width: 35px">EventYear: </td>
            <td align="left" style="width: 20px">
                <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="70px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">Event: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="EventCode" DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="100px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">Semester: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged" Width="50px" Height="20px" runat="server">
                </asp:DropDownList>&nbsp;</td>
            <td style="width: 10px"></td>
            <td align="right" class="btn_02" style="width: 25px">ProductGroup: </td>
            <td align="left" style="width: 133px">
                <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            <td align="right" class="btn_02" style="width: 25px">Product: </td>
            <td align="left" style="width: 150px">
                <asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false" runat="server" Height="20px" Width="150px"></asp:DropDownList></td>
            <td style="width: 10px"></td>
            <td>
                <asp:DropDownList ID="ddlVolName" DataTextField="Name" DataValueField="MemberID" runat="server" AutoPostBack="True" Height="20px" Width="150px" Visible="false"></asp:DropDownList></td>
        </tr>
        <tr>
            <td height="20px" width="100px"></td>
        </tr>
        <tr>


            <td align="right"></td>
            <td></td>
            <td align="center" colspan="3">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Visible="false" />&nbsp;</td>
            <td align="center" colspan="4">
                <asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></td>
            <td align="center" colspan="4">
                <asp:Label ID="lblError1" runat="server" ForeColor="Red"></asp:Label></td>
            <td></td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
                <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label></td>
        </tr>
    </table>
    <table align="center" id="Table1" runat="server" visible="false">
        <tr>
            <td align='center' colspan='4'>
                <font face='Arial' size='4'>Table 1 :  Coaches - Accepted</font>

            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbT1" runat="server" Visible="false" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td align="center">
                <div style="float: left;">
                    <asp:Button ID="btnExport" runat="server" Text="Export" Enabled="false" Visible="false" />
                </div>
                <div style="float: right;" id="dvSort" runat="server" visible="false">
                    <div style="float: left;">
                        <asp:Label ID="lblSortBy" runat="server" Text="Sort By"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 5px;">
                        <asp:DropDownList ID="ddlWeekDay" runat="server" Width="88px" AutoPostBack="true" OnSelectedIndexChanged="ddlWeekDay_SelectedIndexChanged"></asp:DropDownList>
                    </div>
                </div>
                <div style="clear: both;"></div>
                <div id="oDiv" runat="server" style="position: relative; top: 0 -2px; left: 0px; width: 100%;">
                    <%--overflow-y:auto--%>
                    <asp:DataGrid ID="DGSchedule" runat="server" DataKeyField="ID" AutoGenerateColumns="False" AllowPaging="true" PageSize="50" PagerStyle-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="numericpages" GridLines="Both" CellPadding="0" Width="100%" CellSpacing="1" BackColor="Navy" BorderWidth="3px" BorderStyle="Groove" BorderColor="Black" ForeColor="White" Font-Bold="True">
                        <FooterStyle ForeColor="Black" BackColor="Gainsboro" Height="30px"></FooterStyle>
                        <%--#CCCCCC"--%>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                        <ItemStyle ForeColor="Black" BackColor="#EEEEEE" Height="28px"></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false" Height="30px"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
                        <EditItemStyle ForeColor="Black" BackColor="#EEEEEE" />
                        <%-- HeaderStyle-BackColor="Gainsboro"--%>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Ser#">
                                <ItemTemplate>
                                    <%# (DGSchedule.PageSize * DGSchedule.CurrentPageIndex) + Container.ItemIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" ItemStyle-Width="50px" HeaderText="SignUpID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " ReadOnly="true" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CoachName" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderText="CoachName"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Level" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderText="Level"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Day1" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="S"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="P"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A1"></asp:BoundColumn>




                            <asp:BoundColumn DataField="Day2" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S2" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P2" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A2" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A2"></asp:BoundColumn>



                            <asp:BoundColumn DataField="Day3" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S3" HeaderText="S" HeaderStyle-Width="25px" ItemStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P3" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A3" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A3"></asp:BoundColumn>



                            <asp:BoundColumn DataField="Day4" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S4" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P4" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A4" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A4"></asp:BoundColumn>



                            <asp:BoundColumn DataField="Day5" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S5" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P5" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A5" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A5"></asp:BoundColumn>



                            <asp:BoundColumn DataField="Day6" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S6" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P6" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A6" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A6"></asp:BoundColumn>


                            <asp:BoundColumn DataField="Day7" ItemStyle-Width="50px" HeaderStyle-Width="50px"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S7" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P7" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A7" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A7"></asp:BoundColumn>



                        </Columns>
                        <AlternatingItemStyle BackColor="#a09f9f" ForeColor="Gainsboro" />
                    </asp:DataGrid>
                </div>
            </td>
        </tr>

    </table>
    <br />
    <table align="center" id="Table2" runat="server" visible="false">
        <tr>
            <td align='center' colspan='4'>
                <font face='Arial' size='4'>Table 2:  Coaches – Not Yet Accepted</font>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lb2" runat="server" Visible="false" ForeColor="Red"></asp:Label></td>
        </tr>
    </table>

    <table width="100%">
        <tr class="noscroll">
            <td align="center">
                <br />
                <div id="Div1" runat="server" style="position: relative; top: 0 -2px; left: 0px; width: 100%;">
                    <%--overflow-y:auto--%>
                    <asp:DataGrid ID="DGScheduleNotAccepted" runat="server" DataKeyField="ID" AutoGenerateColumns="False" AllowPaging="true" PageSize="50" PagerStyle-Position="Bottom" PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="numericpages" GridLines="Both" CellPadding="0" Width="100%" CellSpacing="1" BackColor="Navy" BorderWidth="3px" BorderStyle="Groove" BorderColor="Black" ForeColor="White" Font-Bold="True">
                        <FooterStyle ForeColor="Black" BackColor="Gainsboro" Height="30px"></FooterStyle>
                        <%--#CCCCCC"--%>
                        <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C"></SelectedItemStyle>
                        <ItemStyle ForeColor="Black" BackColor="#EEEEEE" Height="28px"></ItemStyle>
                        <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false" Height="30px"></HeaderStyle>
                        <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
                        <EditItemStyle ForeColor="Black" BackColor="#EEEEEE" />
                        <%-- HeaderStyle-BackColor="Gainsboro"--%>
                        <Columns>
                            <asp:TemplateColumn HeaderText="Ser#">
                                <ItemTemplate>
                                    <%# (DGScheduleNotAccepted.PageSize * DGScheduleNotAccepted.CurrentPageIndex) + Container.ItemIndex + 1%>
                                </ItemTemplate>
                            </asp:TemplateColumn>
                            <asp:BoundColumn DataField="ID" ItemStyle-Width="50px" HeaderText="SignUpID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " ReadOnly="true" Visible="False"></asp:BoundColumn>
                            <asp:BoundColumn DataField="CoachName" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderText="CoachName"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Level" ItemStyle-Width="150px" HeaderStyle-Width="150px" HeaderText="Level"></asp:BoundColumn>
                            <asp:BoundColumn DataField="Day1" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="S" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="P" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A1" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A1" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Day2" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S2" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P2" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A2" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A2" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>


                            <asp:BoundColumn DataField="Day3" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S3" HeaderText="S" HeaderStyle-Width="25px" ItemStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P3" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A3" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A3" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>

                            <asp:BoundColumn DataField="Day4" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S4" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P4" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A4" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A4" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>


                            <asp:BoundColumn DataField="Day5" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S5" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P5" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A5" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A5" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>


                            <asp:BoundColumn DataField="Day6" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S6" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P6" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="White"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A6" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A6" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>



                            <asp:BoundColumn DataField="Day7" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S7" HeaderText="S" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="P7" HeaderText="P" ItemStyle-Width="25px" HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                            <asp:BoundColumn DataField="A7" ItemStyle-Width="25px" HeaderStyle-Width="25px" HeaderText="A7" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>


                        </Columns>
                        <AlternatingItemStyle BackColor="#a09f9f" ForeColor="Gainsboro" />
                    </asp:DataGrid>
                </div>
            </td>
        </tr>
    </table>
    <br />
    <table align="center" id="Table3" runat="server" visible="false">
        <tr>
            <td align='center' colspan='4'>
                <font face='Arial' size='4'>Table 3:  Time Conflict</font>
            </td>
        </tr>
    </table>
    <table width="100%">
        <tr>
            <td></td>
            <td align="center">
                <asp:GridView ID="GriConflict" runat="server">
                </asp:GridView>
            </td>
        </tr>
    </table>
    <%--  </table>
    </table>--%>
</asp:Content>

