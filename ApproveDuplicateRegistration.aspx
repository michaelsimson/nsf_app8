﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ApproveDuplicateRegistration.aspx.vb" Inherits="ApproveDuplicateRegistration" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left" >
&nbsp;&nbsp;&nbsp;<asp:HyperLink runat="server" ID="hlnkMainMenu" Text="Back to Main Page" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink></div>
<div>
<table border="0" cellpadding = "3" cellspacing = "0" style ="width :800px; text-align:center" >
<tr><td align = "center" style="color: #000000;font-family: Arial Rounded MT Bold; font-size: 14px;" >Approve Duplicate Registrations in Regional Contests</td></tr>
<tr><td align="center">
<table border="0" cellpadding = "3" cellspacing = "0" >
<tr id="trvol" runat = "server" >
					<td class="ItemLabel" vAlign="top" noWrap align="right">Parent Email ID</td>
					<td><asp:textbox id="txtUserId" runat="server" CssClass="SmallFont" width="300" MaxLength="50"></asp:textbox><asp:button id="btnLoadChild" OnClick ="btnLoadChild_Click" runat="server" CssClass="FormButtonCenter" Text="Load Contest"></asp:button><br>
						&nbsp;
						<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" ControlToValidate="txtUserId" ErrorMessage="Enter Login Id."
							Display="Dynamic"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" ControlToValidate="txtUserId"
							ErrorMessage="Email Address should be a valid e-mail address " Display="Dynamic" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr>
<tr>
					<td align="center" colspan="2">
                        <asp:Button ID="btnContinue" runat="server" CausesValidation="false" Text="Load All" OnClick="btnContinue_Click" /></td>
				</tr></table> 
</td></tr>
<tr runat="server" id="trStatus" visible ="false" ><td align = "Center" >Status:
<asp:DropDownList ID="ddlApproved" runat="server">
        <asp:ListItem Value="Approved">Approve</asp:ListItem>
        <asp:ListItem  Value="Rejected">Reject</asp:ListItem>
    </asp:DropDownList><br /><br />
    <asp:Button ID="btnUpdate" OnClick="btnUpdate_Click" CausesValidation="false" Text="Update" runat="server" />
     &nbsp;&nbsp;&nbsp;   <asp:Button ID="btnCancel" CausesValidation="false" runat="server"  Text="Cancel" OnClick ="btnCancel_Click" />
</td></tr> 
<tr><td align = "center" >
 <asp:Label ID="lblErr" runat="server" ForeColor = "Red" ></asp:Label></td> </tr> 
<tr><td align = "center" >
<ASP:DATAGRID id="dgselected" SelectedItemStyle-BackColor="#ffff99" runat="server" OnItemCommand="dgselected_ItemCommand"  OnPageIndexChanged ="dgselected_PageIndexChanged"  Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" DataKeyField="DuplicateContestantReg_id" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" PageSize="10"  AllowPaging="true" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="DuplicateContestantReg_id" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="childnumber" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn HeaderText="View">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						<ItemTemplate>
						<asp:LinkButton id="lbtnView" runat="server" CausesValidation="false" CommandName="View" Text="View"></asp:LinkButton>
						</ItemTemplate></ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Approve/Reject" >
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						<ItemTemplate>
						<asp:LinkButton id="lbtnEdit" runat="server" CausesValidation="false" CommandName="Select" Text="Approve/Reject"></asp:LinkButton>
						</ItemTemplate></ASP:TemplateColumn>
						<ASP:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						      <asp:Label ID="lblChildName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                            </ItemTemplate>
						</ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                     
						
						<ASP:BOUNDCOLUMN DataField="FatherName" HeaderText ="Father Name" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="EMail" HeaderText ="Email" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="ChapterCode" HeaderText ="Chapter" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>

                        </COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>
</td></tr>

<tr><td align = "center" height="15px" >&nbsp;
</td></tr>
<tr><td align = "center" >
<ASP:DATAGRID id="DGView" runat="server"  Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal"  CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" PageSize="10"  AllowPaging="False" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle ForeColor="#FFFFFF" HorizontalAlign="center" Font-Size="X-Small"   Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
		                <COLUMNS>
						<ASP:BOUNDCOLUMN ItemStyle-Wrap="false"  DataField="ChapterCode" HeaderStyle-ForeColor="White" HeaderText ="Chapter" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Grade" HeaderStyle-ForeColor="White" HeaderText ="Grade" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ParentName" HeaderStyle-ForeColor="White" HeaderText ="Parent Name" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ChildName" HeaderStyle-ForeColor="White" HeaderText ="Child Name" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="BadgeNumber" HeaderStyle-ForeColor="White" HeaderText ="BadgeNumber" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Score1" HeaderStyle-ForeColor="White" HeaderText ="Score1" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Score2" HeaderStyle-ForeColor="White" HeaderText ="Score2" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Score3" HeaderStyle-ForeColor="White" HeaderText ="Score3" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Rank" HeaderStyle-ForeColor="White" HeaderText ="Rank" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ContestDate" HeaderStyle-ForeColor="White" HeaderText ="Contest Date" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="Fee" HeaderStyle-ForeColor="White"  HeaderText ="Fee" DataFormatString="{0:C}"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductName" HeaderStyle-ForeColor="White" HeaderText ="ProductName" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="PaymentDate" HeaderStyle-ForeColor="White" HeaderText ="PaymentDate"  DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="PaymentMode" HeaderStyle-ForeColor="White" HeaderText ="PaymentMode" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="PaymentReference" HeaderStyle-ForeColor="White" HeaderText ="PaymentReference" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="PaymentNotes" HeaderStyle-ForeColor="White" HeaderText ="PaymentNotes" ></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="CreateDate" HeaderStyle-ForeColor="White" HeaderText ="CreateDate"  DataFormatString="{0:d}"></ASP:BOUNDCOLUMN>
</COLUMNS> 
	</ASP:DATAGRID>
</td></tr>
<tr><td align = "center" >
      <asp:DropDownList AutoPostBack="true" ID="ddlSelection" OnSelectedIndexChanged="ddlSelection_SelectedIndexChanged" runat="server">
          <asp:ListItem>Approved</asp:ListItem>
          <asp:ListItem>Rejected</asp:ListItem>
      </asp:DropDownList>
      <asp:Label ID="lblDuplicateContestantReg_ID" runat="server" ForeColor = "White" ></asp:Label>
      <asp:Label ID="lblProductID" runat="server" ForeColor = "White" ></asp:Label>
      <asp:Label ID="lblChildNumber" runat="server" ForeColor = "White" ></asp:Label>
    
</td></tr>
<tr><td align = "center" >
<ASP:DATAGRID id="DGALL" runat="server"     Width="100%" AutoGenerateColumns="False" AllowSorting="True"
					Height="14px" GridLines="Horizontal" DataKeyField="DuplicateContestantReg_id" CellPadding="4" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" PageSize="10"  AllowPaging="true" ForeColor="White" Font-Bold="True">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
					<COLUMNS>
						<ASP:BOUNDCOLUMN DataField="DuplicateContestantReg_id" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="childnumber" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductCode" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:BOUNDCOLUMN DataField="ProductID" Visible="false"></ASP:BOUNDCOLUMN>
						<ASP:TemplateColumn HeaderText="Child Name" ItemStyle-Width="15%">
						<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
						    <ItemTemplate>
						      <asp:Label ID="lblChildName" runat="server" text='<%#DataBinder.Eval(Container.DataItem, "ChildName")%>'></asp:Label>
                            </ItemTemplate>
						</ASP:TemplateColumn>
                         <ASP:TEMPLATECOLUMN HeaderText="Product" ItemStyle-Width="15%" >
							<HEADERSTYLE Font-Bold="True" ForeColor="White"></HEADERSTYLE>
							<ITEMTEMPLATE><%#DataBinder.Eval(Container.DataItem, "ProductName")%></ITEMTEMPLATE>
						</ASP:TEMPLATECOLUMN>
                     
						
						<ASP:BOUNDCOLUMN DataField="FatherName" HeaderText ="Father Name" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="EMail" HeaderText ="Email" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="Status" HeaderText ="Status" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>
                        <ASP:BOUNDCOLUMN DataField="ChapterCode" HeaderText ="Chapter" HeaderStyle-Font-Bold = "true" HeaderStyle-ForeColor="White"></ASP:BOUNDCOLUMN>

                        </COLUMNS>
                      <HeaderStyle HorizontalAlign="Left" />
                      <ItemStyle HorizontalAlign ="Left" />
		<PAGERSTYLE ForeColor="white" Font-Bold="true" HorizontalAlign="left" Mode="NumericPages"></PAGERSTYLE>
		                    <AlternatingItemStyle BackColor="LightBlue" />
	</ASP:DATAGRID>
	<br />
    <asp:Label ID="lblAllErr" runat="server" ></asp:Label>
</td></tr>
</table>
    
  	</div>
</asp:Content>
