Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlTypes
Imports NorthSouth.BAL
Imports Microsoft.ApplicationBlocks.Data

Namespace NorthSouth.DAL
    ''' <summary>
    ''' Summary description for BlogMessage.
    ''' </summary>
    Public Class DonationDAL
        ''' <summary>
        ''' Database Connection String Property
        ''' </summary>
        Private _connectionString As String
        Private strSql As String
        ''' <summary>
        ''' Database Connection String Property
        ''' </summary>
        Public Property ConnectionString() As String
            Get
                Return _connectionString
            End Get
            Set(ByVal value As String)
                _connectionString = value
            End Set
        End Property
        ''' <summary>
        ''' BlogMessageDAL Constructor
        ''' </summary>
        ''' <param name="ConnectionString"></param>
        Public Sub New(ByVal ConnectionString As String)
            Me.ConnectionString = ConnectionString
        End Sub

        ''' <summary>
        ''' GetConnection Method to Get the Connection String 
        ''' </summary>
        ''' <returns></returns>
        Private Function GetConnection() As SqlConnection
            Return New SqlConnection(Me.ConnectionString)
        End Function
        Public Function GetDonationByID(ByVal _id As Integer, ByVal ParentID As Integer) As SqlDataReader
            ' SqlDataReader that will hold the returned results		
            Dim dr As SqlDataReader = Nothing

            ' Call ExecuteReader static method of SqlHelper class that returns a SqlDataReader
            Dim param As SqlParameter() = New SqlParameter(1) {}

            param(0) = New SqlParameter("@ChildNumber", SqlDbType.Int)
            param(0).Value = _id
            param(0).Direction = ParameterDirection.Input

            param(1) = New SqlParameter("@MEMBERID", SqlDbType.Int)
            param(1).Value = ParentID
            param(1).Direction = ParameterDirection.Input

            dr = SqlHelper.ExecuteReader(Me.GetConnection(), "usp_GetChildByID", param)

            Return dr

        End Function
        ''' <summary>
        ''' Adds New Donation to Donation Table
        ''' </summary>
        ''' <returns></returns>
        Public Function AddDonation(ByVal donation As Donation) As Integer
            strSql = "INSERT INTO DonationsInfo VALUES("
            strSql = strSql & donation.MemberID & "," & donation.DonationNumber & ","
            strSql = strSql & "'" & donation.DonorType & "'," & donation.Amount & ","
            strSql = strSql & "'" & donation.TransactionNumber & "',Convert(varchar,'" & donation.DonationDate & "',101),"
            strSql = strSql & "'" & donation.Method & "','" & donation.Purpose & "',"
            strSql = strSql & "'" & donation.Event & "','" & donation.Endowment & "',"
            strSql = strSql & "'" & donation.Designation & "','" & donation.InMemoryOf & "',"
            strSql = strSql & "'" & donation.Status & "','" & donation.MatchingFlag & "',"
            strSql = strSql & "'" & donation.MatchingEmployer & "','" & donation.Remarks & "',"
            strSql = strSql & "'" & donation.CreateDate & "',null,"
            strSql = strSql & "'" & donation.DeleteReason & "','" & donation.DeletedFlag & "',"
            strSql = strSql & donation.EventID & "," & donation.ChapterID & ","
            strSql = strSql & "'" & donation.RecurrenceFlag & "'," & donation.TaxDeduction & ","
            strSql = strSql & donation.CreatedBy & ",null," & donation.EventYear & ",'" & donation.Anonymous & "',"
            strSql = strSql & "'" & donation.Access & "','" & donation.DepositDate & "','" & donation.DepositSlip & "',"
            strSql = strSql & "'" & donation.MatchEmpID & "','" & donation.DonationType & "', NULL," & donation.BankID & " )"
            Return SqlHelper.ExecuteNonQuery(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function UpdateDonation(ByVal donation As Donation) As Integer
            strSql = "UPDATE DONATIONSINFO SET "
            strSql = strSql & " Amount= " & donation.Amount & ","
			strSql = strSql & " TRANSACTION_NUMBER = '" & donation.TransactionNumber & "',"
            strSql = strSql & " ModifyDate=getdate(),"
            strSql = strSql & " ModifiedBy = " & donation.ModifiedBy & ","
            strSql = strSql & " DonationDate = Convert(Varchar,'" & donation.DonationDate & "',110),"
            strSql = strSql & " MatchingEmployer = '" & donation.MatchingEmployer & "',"
            strSql = strSql & " InMemoryOf = '" & donation.InMemoryOf & "',"
            strSql = strSql & " STATUS = '" & donation.Status & "',"
            strSql = strSql & " Remarks = '" & donation.Remarks & "',"
            strSql = strSql & " Designation= '" & donation.Designation & "',"
            strSql = strSql & " Purpose = '" & donation.Purpose & "',"
            strSql = strSql & " Event = '" & donation.Event & "',"
            strSql = strSql & " MatchingFlag= '" & donation.MatchingFlag & "',"
            strSql = strSql & " Endowment = '" & donation.Endowment & "',"
            strSql = strSql & " Method = '" & donation.Method & "', "
            strSql = strSql & " Anonymous = '" & donation.Anonymous & "', "
            strSql = strSql & " Access = '" & donation.Access & "', "
            strSql = strSql & " EventId = '" & donation.EventID & "', "
            strSql = strSql & " DepositDate = '" & donation.DepositDate & "', "
            strSql = strSql & " DepositSlip = '" & donation.DepositSlip & "', "
            strSql = strSql & " MatchEmpID = '" & donation.MatchEmpID & "', "
            strSql = strSql & " DonationType = '" & donation.DonationType & "', "
            strSql = strSql & " ChapterID = '" & donation.ChapterID & "', "
            strSql = strSql & " BankID = " & donation.BankID
            strSql = strSql & " WHERE MemberID =" & donation.MemberID
            strSql = strSql & " AND DonationID =" & donation.DonationID
            Return SqlHelper.ExecuteNonQuery(Me.GetConnection(), CommandType.Text, strSql)
        End Function
        Public Function GetChapterIDFromOrgByMemberID(ByVal MemberID As Integer, ByVal type As String) As Integer
            If type = "OWN" Then
                strSql = "SELECT ISNULL(ChapterID,0) FROM ORGANIZATIONINFO WHERE AUTOMEMBERID = " & MemberID
            Else
                strSql = "SELECT ISNULL(ChapterID,0) FROM INDSPOUSE WHERE AUTOMEMBERID = " & MemberID
            End If

            Return Convert.ToInt32(SqlHelper.ExecuteScalar(Me.GetConnection(), CommandType.Text, strSql))
            
        End Function
    End Class
End Namespace
