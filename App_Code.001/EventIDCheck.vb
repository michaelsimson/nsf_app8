﻿Imports Microsoft.VisualBasic
Imports System.Data.SqlClient

Public Class EventIDCheck

    Public Shared Function GetEventIDFromDB(ByVal eventId As Integer) As Integer

        Dim strSql As String
        Dim DBeventId As Integer

        Dim conn As New SqlConnection(ConfigurationManager.ConnectionStrings.Item("NSFConnectionString").ConnectionString)

        strSql = "select eventid from EVENT	where eventid=@eventid and lower(status) = 'o'"

        Dim cmd As New SqlCommand(strSql, conn)

        'Add up the parameter, associated it with its value
        cmd.Parameters.AddWithValue("eventid", eventId)

        'Opening Connection for our DB operation  
        conn.Open()

        'Get the results of our query 
        Dim reader As SqlDataReader = cmd.ExecuteReader()
        If (reader.HasRows) Then
            reader.Read()
            DBeventId = reader.GetInt32(0)
        Else
            DBeventId = 0
        End If

        GetEventIDFromDB = DBeventId

    End Function

End Class
