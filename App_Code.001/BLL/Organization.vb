Imports Microsoft.VisualBasic
Imports System.Data.SqlClient
Imports NorthSouth.DAL
Namespace NorthSouth.BAL
    Public Class Organization
        Dim _autoMemberID As Integer
        Dim _memberID As String
        Dim _orgName As String
        Dim _title As String
        Dim _firstName As String
        Dim _initial As String
        Dim _lastName As String
        Dim _address1 As String
        Dim _address2 As String
        Dim _city As String
        Dim _state As String
        Dim _zip As String
        Dim _country As String
        Dim _phone As String
        Dim _fax As String
        Dim _email As String
        Dim _email2 As String
        Dim _referredBy As String
        Dim _matchingGift As String
        Dim _sendMail As String
        Dim _nsfChapter As String
        Dim _sendNewsLetter As String
        Dim _mailingLabel As String
        Dim _sendReceipt As String
        Dim _liasonPerson As String
        Dim _createdDate As String
        Dim _memberSince As String
        Dim _modifyDate As String
        Dim _deleteReason As String
        Dim _deleteFlag As String
        Dim _venue As String
        Dim _sponsor As String
        Dim _website As String
        Dim _createdBy As Integer
        Dim _modifedBy As Integer
        Dim _chapter As Integer
        Dim _irsCat As String
        Dim _busType As String
        Dim _Matching_Gift_URL As String
        Dim _Matching_Gift_Account_Number As String
        Dim _Matching_Gift_User_Id As String
        Dim _Matching_Gift_Password As String

        Public Property AutoMemberID() As Integer
            Get
                Return _autoMemberID
            End Get
            Set(ByVal value As Integer)
                _autoMemberID = value
            End Set
        End Property
        Public Property MemberID() As String
            Get
                Return _memberID
            End Get
            Set(ByVal value As String)
                _memberID = value
            End Set
        End Property
        Public Property OrganizationName() As String
            Get
                Return _orgName
            End Get
            Set(ByVal value As String)
                _orgName = value
            End Set
        End Property
        Public Property Title() As String
            Get
                Return _title
            End Get
            Set(ByVal value As String)
                _title = value
            End Set
        End Property
        Public Property FirstName() As String
            Get
                Return _firstName
            End Get
            Set(ByVal value As String)
                _firstName = value
            End Set
        End Property
        Public Property Initial() As String
            Get
                Return _initial
            End Get
            Set(ByVal value As String)
                _initial = value
            End Set
        End Property
        Public Property LastName() As String
            Get
                Return _lastName
            End Get
            Set(ByVal value As String)
                _lastName = value
            End Set
        End Property
        Public Property Address1() As String
            Get
                Return _address1
            End Get
            Set(ByVal value As String)
                _address1 = value
            End Set
        End Property
        Public Property Address2() As String
            Get
                Return _address2
            End Get
            Set(ByVal value As String)
                _address2 = value
            End Set
        End Property
        Public Property City() As String
            Get
                Return _city
            End Get
            Set(ByVal value As String)
                _city = value
            End Set
        End Property
        Public Property State() As String
            Get
                Return _state
            End Get
            Set(ByVal value As String)
                _state = value
            End Set
        End Property
        Public Property Zip() As String
            Get
                Return _zip
            End Get
            Set(ByVal value As String)
                _zip = value
            End Set
        End Property
        Public Property Country() As String
            Get
                Return _country
            End Get
            Set(ByVal value As String)
                _country = value
            End Set
        End Property
        Public Property Phone() As String
            Get
                Return _phone
            End Get
            Set(ByVal value As String)
                _phone = value
            End Set
        End Property
        Public Property Fax() As String
            Get
                Return _fax
            End Get
            Set(ByVal value As String)
                _fax = value
            End Set
        End Property
        Public Property Email() As String
            Get
                Return _email
            End Get
            Set(ByVal value As String)
                _email = value
            End Set
        End Property
        Public Property Email2() As String
            Get
                Return _email2
            End Get
            Set(ByVal value As String)
                _email2 = value
            End Set
        End Property
        Public Property MatchingGift() As String
            Get
                Return _matchingGift
            End Get
            Set(ByVal value As String)
                _matchingGift = value
            End Set
        End Property
        Public Property SendMail() As String
            Get
                Return _sendMail
            End Get
            Set(ByVal value As String)
                _sendMail = value
            End Set
        End Property
        Public Property ReferredBy() As String
            Get
                Return _referredBy
            End Get
            Set(ByVal value As String)
                _referredBy = value
            End Set
        End Property
        Public Property NSFChapter() As String
            Get
                Return _nsfChapter
            End Get
            Set(ByVal value As String)
                _nsfChapter = value
            End Set
        End Property
        Public Property SendNewsLetter() As String
            Get
                Return _sendNewsLetter
            End Get
            Set(ByVal value As String)
                _sendNewsLetter = value
            End Set
        End Property
        Public Property MailingLabel() As String
            Get
                Return _mailingLabel
            End Get
            Set(ByVal value As String)
                _mailingLabel = value
            End Set
        End Property
        Public Property SendReceipt() As String
            Get
                Return _sendReceipt
            End Get
            Set(ByVal value As String)
                _sendReceipt = value
            End Set
        End Property
        Public Property CreatedDate() As String
            Get
                Return _createdDate
            End Get
            Set(ByVal value As String)
                _createdDate = value
            End Set
        End Property
        Public Property MemberSince() As String
            Get
                Return _memberSince
            End Get
            Set(ByVal value As String)
                _memberSince = value
            End Set
        End Property
        Public Property ModifyDate() As String
            Get
                Return _modifyDate
            End Get
            Set(ByVal value As String)
                _modifyDate = value
            End Set
        End Property
        Public Property DeleteReason() As String
            Get
                Return _deleteReason
            End Get
            Set(ByVal value As String)
                _deleteReason = value
            End Set
        End Property
        Public Property DeletedFlag() As String
            Get
                Return _deleteFlag
            End Get
            Set(ByVal value As String)
                _deleteFlag = value
            End Set
        End Property
        Public Property Venue() As String
            Get
                Return _venue
            End Get
            Set(ByVal value As String)
                _venue = value
            End Set
        End Property
        Public Property Sponsor() As String
            Get
                Return _sponsor
            End Get
            Set(ByVal value As String)
                _sponsor = value
            End Set
        End Property
        Public Property WebSite() As String
            Get
                Return _website
            End Get
            Set(ByVal value As String)
                _website = value
            End Set
        End Property
        Public Property CreatedBy() As Integer
            Get
                Return _createdBy
            End Get
            Set(ByVal value As Integer)
                _createdBy = value
            End Set
        End Property
        Public Property ModifiedBy() As Integer
            Get
                Return _modifedBy
            End Get
            Set(ByVal value As Integer)
                _modifedBy = value
            End Set
        End Property
        Public Property ChapterID() As Integer
            Get
                Return _chapter
            End Get
            Set(ByVal value As Integer)
                _chapter = value
            End Set
        End Property
        Public Property LiasonPerson() As String
            Get
                Return _liasonPerson
            End Get
            Set(ByVal value As String)
                _liasonPerson = value
            End Set
        End Property
        Public Property IRSCat() As String
            Get
                Return _irsCat
            End Get
            Set(ByVal value As String)
                _irsCat = value
            End Set
        End Property
        Public Property BusType() As String
            Get
                Return _busType
            End Get
            Set(ByVal value As String)
                _busType = value
            End Set
        End Property
        Public Property Matching_Gift_URL() As String
            Get
                Return _Matching_Gift_URL
            End Get
            Set(ByVal value As String)
                _Matching_Gift_URL = value
            End Set
        End Property
        Public Property Matching_Gift_Account_Number() As String
            Get
                Return _Matching_Gift_Account_Number
            End Get
            Set(ByVal value As String)
                _Matching_Gift_Account_Number = value
            End Set
        End Property
        Public Property Matching_Gift_User_Id() As String
            Get
                Return _Matching_Gift_User_Id
            End Get
            Set(ByVal value As String)
                _Matching_Gift_User_Id = value
            End Set
        End Property
        Public Property Matching_Gift_Password() As String
            Get
                Return _Matching_Gift_Password
            End Get
            Set(ByVal value As String)
                _Matching_Gift_Password = value
            End Set
        End Property
        Public Function AddOrganization(ByVal connectionString As String) As Integer
            Dim objDAL As New OrganizationDAL(connectionString)
            Return objDAL.AddOrganization(Me)
        End Function
        Public Function UpdateOrganization(ByVal connectionString As String) As Integer
            Dim objDAL As New OrganizationDAL(connectionString)
            Return objDAL.UpdateOrganization(Me)
        End Function
        Public Function GetMaxOrganizationID(ByVal connectionString As String) As Integer
            Dim objDAL As New OrganizationDAL(connectionString)
            Return objDAL.GetMaxOrganizationMemberID()
        End Function
        Public Function GetOrganizationList(ByVal connectionString As String, ByVal sortField As String, ByVal ChapterID As String) As DataSet
            Dim objDAL As New OrganizationDAL(connectionString)
            Return objDAL.GetOrganizations(sortField, ChapterID)
        End Function
        Public Function GetOrganizationByMemberID(ByVal MemberID As String, ByVal connectionString As String) As SqlDataReader
            Dim objDAL As New OrganizationDAL(connectionString)
            Return objDAL.GetOrganizationByMemberID(MemberID)
        End Function
    End Class
End Namespace


