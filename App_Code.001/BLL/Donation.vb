Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports NorthSouth.DAL


Namespace NorthSouth.BAL
    Public Class Donation
        Private _donationNumber As Integer
        Private _memberID As Integer
        Private _donorType As String
        Private _amount As Double
        Private _transactionNumber As String
        Private _donationDate As String
        Private _method As String
        Private _purpose As String
        Private _event As String
        Private _endowment As String
        Private _designation As String
        Private _inMemoryof As String
        Private _status As String
        Private _matchingFlag As String
        Private _matchingEmployer As String
        Private _remarks As String
        Private _createdDate As Date
        Private _modifyDate As Date
        Private _deleteReason As String
        Private _deletedFlag As String
        Private _eventID As Integer
        Private _chapterID As Integer
        Private _recurrenceFlag As String
        Private _taxDeduction As Double
        Private _createdBy As Integer
        Private _modifiedBy As Integer
        Private _eventYear As Integer
        Private _anonymous As String
        Private _access As String
        Private _depositdate As String
        Private _depositslip As String
        Private _matchempid As Integer
        Private _donationtype As String
        Private _bankId As Integer

#Region "Public Properties"
        Public Property DonationID() As Integer
            Get
                Return _donationNumber
            End Get
            Set(ByVal value As Integer)
                _donationNumber = value
            End Set
        End Property

        Public Property MemberID() As Integer
            Get
                Return _memberID
            End Get
            Set(ByVal value As Integer)
                _memberID = value
            End Set
        End Property

        Public Property DonorType() As String
            Get
                Return _donorType
            End Get
            Set(ByVal value As String)
                _donorType = value
            End Set
        End Property

        Public Property Amount() As Double
            Get
                Return _amount
            End Get
            Set(ByVal value As Double)
                _amount = value
            End Set
        End Property

        Public Property TransactionNumber() As String
            Get
                Return _transactionNumber
            End Get
            Set(ByVal value As String)
                _transactionNumber = value
            End Set
        End Property

        Public Property DonationDate() As String
            Get
                Return _donationDate
            End Get
            Set(ByVal value As String)
                _donationDate = value
            End Set
        End Property

        Public Property Method() As String
            Get
                Return _method
            End Get
            Set(ByVal value As String)
                _method = value
            End Set
        End Property

        Public Property Purpose() As String
            Get
                Return _purpose
            End Get
            Set(ByVal value As String)
                _purpose = value
            End Set
        End Property

        Public Property [Event]() As String
            Get
                Return _event
            End Get
            Set(ByVal value As String)
                _event = value
            End Set
        End Property

        Public Property Endowment() As String
            Get
                Return _endowment
            End Get
            Set(ByVal value As String)
                _endowment = value
            End Set
        End Property

        Public Property Designation() As String
            Get
                Return _designation
            End Get
            Set(ByVal value As String)
                _designation = value
            End Set
        End Property

        Public Property InMemoryOf() As String
            Get
                Return _inMemoryof
            End Get
            Set(ByVal value As String)
                _inMemoryof = value
            End Set
        End Property

        Public Property Status() As String
            Get
                Return _status
            End Get
            Set(ByVal value As String)
                _status = value
            End Set
        End Property
        Public Property MatchingFlag() As String
            Get
                Return _matchingFlag
            End Get
            Set(ByVal value As String)
                _matchingFlag = value
            End Set
        End Property

        Public Property MatchingEmployer() As String
            Get
                Return _matchingEmployer
            End Get
            Set(ByVal value As String)
                _matchingEmployer = value
            End Set
        End Property
        Public Property Remarks() As String
            Get
                Return _remarks
            End Get
            Set(ByVal value As String)
                _remarks = value
            End Set
        End Property

        Public Property CreateDate() As Date
            Get
                Return _createdDate
            End Get
            Set(ByVal value As Date)
                _createdDate = value
            End Set
        End Property

        Public Property ModifyDate() As Date
            Get
                Return _modifyDate
            End Get
            Set(ByVal value As Date)
                _modifyDate = value
            End Set
        End Property

        Public Property DeleteReason() As String
            Get
                Return _deleteReason
            End Get
            Set(ByVal value As String)
                _deleteReason = value
            End Set
        End Property

        Public Property DeletedFlag() As String
            Get
                Return _deletedFlag
            End Get
            Set(ByVal value As String)
                _deletedFlag = value
            End Set
        End Property
        Public Property EventID() As Integer
            Get
                Return _eventID
            End Get
            Set(ByVal value As Integer)
                _eventID = value
            End Set
        End Property
        Public Property ChapterID() As Integer
            Get
                Return _chapterID
            End Get
            Set(ByVal value As Integer)
                _chapterID = value
            End Set
        End Property
        Public Property RecurrenceFlag() As String
            Get
                Return _recurrenceFlag
            End Get
            Set(ByVal value As String)
                _recurrenceFlag = value
            End Set
        End Property
        Public Property TaxDeduction() As Double
            Get
                Return _taxDeduction
            End Get
            Set(ByVal value As Double)
                _taxDeduction = value
            End Set
        End Property
        Public Property CreatedBy() As Integer
            Get
                Return _createdBy
            End Get
            Set(ByVal value As Integer)
                _createdBy = value
            End Set
        End Property
        Public Property ModifiedBy() As Integer
            Get
                Return _modifiedBy
            End Get
            Set(ByVal value As Integer)
                _modifiedBy = value
            End Set
        End Property
        Public Property EventYear() As Integer
            Get
                Return _eventYear
            End Get
            Set(ByVal value As Integer)
                _eventYear = value
            End Set
        End Property
        Public Property DonationNumber() As Double
            Get
                Return _donationNumber
            End Get
            Set(ByVal value As Double)
                _donationNumber = value
            End Set
        End Property
        Public Property Anonymous() As String
            Get
                Return _anonymous
            End Get
            Set(ByVal value As String)
                _anonymous = value
            End Set
        End Property
        Public Property Access() As String
            Get
                Return _access
            End Get
            Set(ByVal value As String)
                _access = value
            End Set
        End Property
        Public Property DepositDate() As String

            Get
                Return _depositdate
            End Get
            Set(ByVal value As String)
                _depositdate = value
            End Set
        End Property
        Public Property DepositSlip() As String

            Get
                Return _depositslip
            End Get
            Set(ByVal value As String)
                _depositslip = value
            End Set
        End Property

        Public Property MatchEmpID() As Integer

            Get
                Return _matchempid
            End Get
            Set(ByVal value As Integer)
                _matchempid = value
            End Set
        End Property

        Public Property DonationType() As String
            Get
                Return _donationtype
            End Get
            Set(ByVal value As String)
                _donationtype = value
            End Set
        End Property

        Public Property BankID() As String
            Get
                Return _bankId
            End Get
            Set(ByVal value As String)
                _bankId = value
            End Set
        End Property



#End Region
        Public Sub New()
        End Sub
        ''' <summary>
        ''' Adds New Donation Entry to Donation
        ''' </summary>
        ''' <param name="connectionString"></param>
        ''' <returns></returns>
        Public Function AddDonation(ByVal connectionString As String) As Integer
            Dim objDAL As New DonationDAL(connectionString)
            Return objDAL.AddDonation(Me)
        End Function
        Public Function UpdateDonation(ByVal connectionString As String) As Integer
            Dim objDAL As New DonationDAL(connectionString)
            Return objDAL.UpdateDonation(Me)
        End Function
        Public Function GetChapterID(ByVal LoginID As Integer, ByVal connectionString As String, ByVal type As String) As Integer
            Dim objDAL As New DonationDAL(connectionString)
            Return objDAL.GetChapterIDFromOrgByMemberID(LoginID, type)
        End Function
    End Class
End Namespace

