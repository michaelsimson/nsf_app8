Imports Microsoft.VisualBasic
Imports NorthSouth.DAL
Namespace NorthSouth.BAL
    Public Class Badge
        Public Function GetContestDates(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetContestDates(ChapterID, ContestYear)
        End Function
        Public Function GetContestDates_Score(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestID As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetContestDates_Score(ChapterID, ContestYear, ContestID)
        End Function
        Public Function GetChildsHavingBadges(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetChildsHavingBadges(ChapterID, ContestYear, ContestDates)
        End Function
        Public Function GetChildsHavingBadgesFinals(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal FromRowID As String, ByVal ToRowID As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetChildsHavingBadgesFinals(ChapterID, ContestYear, ContestDates, FromRowID, ToRowID)
        End Function
        Public Function ShowBadge(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal ChildNumber As Double) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.ShowBadge(ChapterID, ContestYear, ContestDates, ChildNumber)
        End Function
        Public Function ShowParticipantCertificate(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.ShowParticipantCertificate(ChapterID, ContestYear, ContestDates)
        End Function
        Public Function GetParticipantsHavingCertificates(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetParticipantsHavingCertificates(ChapterID, ContestYear, ContestDates)
        End Function

        Public Function GetParticipantsHavingCertificatesFinals(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal FromRowID As String, ByVal ToRowID As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetParticipantsHavingCertificatesFinals(ChapterID, ContestYear, ContestDates, FromRowID, ToRowID)
        End Function

        Public Function GetProductNames(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal ChildNumber As Double) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetProductNames(ChapterID, ContestYear, ContestDates, ChildNumber)
        End Function
        Public Function GetLeftSignatures(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetLeftSignatures(ChapterID, ContestYear)
        End Function
        Public Function GetRightSignatures(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetRightSignatures(ChapterID, ContestYear)
        End Function
        Public Function GetAllSignatures(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetAllSignatureList(ChapterID, ContestYear)
        End Function
        Public Function GetChapterName(ByVal connectionString As String, ByVal ChapterID As Integer) As String
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetChapterName(ChapterID)
        End Function
        Public Function GetVolunteerBadges(ByVal connectionString As String, ByVal ChapterID As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetVolunteerBadges(ChapterID)
        End Function
        Public Function GetVolunteerCertificates(ByVal connectionString As String, ByVal ChapterID As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetVolunteerCertificates(ChapterID)
        End Function
        Public Function GetRankCertificates(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetRankCertificates(ChapterID, ContestYear, ContestDates)
        End Function
        Public Function GetSignatureNameByProductCode(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ProductCode As String) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.SignatureNameByProductCode(ChapterID, ContestYear, ProductCode)
        End Function
        Public Function GetRankCertificatesPhase3(ByVal connectionString As String, ByVal ChapterID As Integer, ByVal ContestYear As Integer, ByVal ContestDates As String, ByVal ContestID As Integer, ByVal TopRank As Integer) As DataSet
            Dim objDAL As New BadgeDAL(connectionString)
            Return objDAL.GetRankCertificatesPhase3(ChapterID, ContestYear, ContestDates, ContestID, TopRank)
        End Function
    End Class
End Namespace