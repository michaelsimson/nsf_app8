﻿Imports Microsoft.VisualBasic
Namespace VRegistration
    Public Class Constant
        Public Enum GameGroupCode
            SB = 1
            VB = 2
        End Enum

        Public Const ConnectionString As String = "connectionString"
        Public Const AccessErrorMessage As String = "You do not have access to {0} game.  If you need further assistance, send an email to nsfgame@gmail.com"
        Public Const ExpiredErrorMessage As String = "Your access has expired.  You need to renew to get access.  If you need further assistance, send an email to nsfgame@gmail.com"
        Public Const SpellingBee As String = "Spelling Bee"
        Public Const Vocabulary As String = "Vocabulary"
    End Class
End Namespace

