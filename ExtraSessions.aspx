﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="ExtraSessions.aspx.cs" Inherits="ExtraSessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script src="Scripts/jquery-1.9.1.js"></script>
    <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
    <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
    <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
    <script language="javascript" type="text/javascript">
        function JoinMeeting() {

            var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

            window.open(url, '_blank');

        }

        function showmsg() {
            alert("Coaches can only join their class up to 30 minutes before class time");
        }
        function showAlert() {
            alert("Meeting attendees can only join their class up to 30 minutes before class time");
        }
    </script>

    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Extra Sessions
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <table id="tblExtraSection" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; padding: 10px;">
        <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
            <td align="left">Event year</td>
            <td align="left">Semester</td>
            <td align="left">Coach</td>
            <td align="left">Product Group</td>
            <td align="left">product</td>
            <td align="left">Level</td>
            <td align="left">Session No</td>
        </tr>
        <tr class="ContentSubTitle" align="center">
            <td align="left">
                <asp:DropDownList ID="ddYear" runat="server" Width="85px"
                    AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                </asp:DropDownList>
            </td>

            <td align="left">
                <asp:DropDownList ID="ddlSemester" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlCoach" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" runat="server" Width="200px" AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlLevel" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlSession" runat="server" Width="75px" AutoPostBack="True">
                </asp:DropDownList></td>
        </tr>

        <tr>
            <td align="center" colspan="9">
                <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />
                <asp:Button ID="BtnReset" runat="server" Visible="True" Text="Reset" OnClick="BtnReset_Click" />

            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center" style="font-weight: bold; color: #64A81C;">
        <span id="spnTable1Title" runat="server" visible="false">Table 1 : Extra Sessions</span>


    </div>


    <div style="clear: both;"></div>
    <div>
        <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1150px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                    <ItemTemplate>
                        <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                    <ItemTemplate>
                        <div style="display: none;">
                            <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                            <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                            <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                            <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                            <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                            <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                            <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>
                            <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                            <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"StartDate","{0:yyyy-MM-dd}") %>'>'></asp:Label>
                            <asp:Label ID="lblSignupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SignupID") %>'>'></asp:Label>
                            <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'>'></asp:Label>
                            <asp:Label ID="lblHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'>'></asp:Label>
                        </div>
                        <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="DeleteMeeting" />
                    </ItemTemplate>

                    <ItemStyle HorizontalAlign="Center"></ItemStyle>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
                <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
                <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
                <asp:TemplateField HeaderText="Coach">

                    <ItemTemplate>

                        <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
                <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
                <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


                <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
                <asp:BoundField DataField="StartDate" HeaderText="Extara Session Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
                <asp:TemplateField HeaderText="Begin Time">

                    <ItemTemplate>
                        <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="End Time" Visible="false">

                    <ItemTemplate>
                        <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                    </ItemTemplate>
                </asp:TemplateField>
                <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
                <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
                <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
                <asp:BoundField DataField="Status" HeaderText="Status"></asp:BoundField>


                <asp:TemplateField HeaderText="meeting URL">

                    <ItemTemplate>

                        <div style="display: none;">
                            <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()+" "%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>

                            <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString() %></asp:Label>

                            <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                            <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                            <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                        </div>
                        <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />

                        <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Vroom" HeaderText="Vroom"></asp:BoundField>
                <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>
            </Columns>

            <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

            <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
        </asp:GridView>
    </div>
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostID" value="0" runat="server" />
    <input type="hidden" id="hdnZoomURL" value="0" runat="server" />
</asp:Content>
