﻿Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports NativeExcel
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class InternalAuditReports
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Not IsPostBack Then
            loadbank("")
            Dim year As Integer = Now.Year
            Dim chkDate As String = "04/30/" & year.ToString()
            Dim minMinusval As Integer = -2
            If (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then
                minMinusval = -1
            End If
            Dim i As Integer
            For i = -4 To minMinusval
                year = Now.Year + i
                ddlInvPositionsYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
                ddlbalYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
                ddlYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
            Next
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue(year))
            ddlbalYear.SelectedIndex = ddlbalYear.Items.IndexOf(ddlbalYear.Items.FindByValue(year))
            ddlInvPositionsYear.SelectedIndex = ddlInvPositionsYear.Items.IndexOf(ddlInvPositionsYear.Items.FindByValue(year))
        End If
    End Sub

    Private Sub loadYear(ByVal flag As Integer)
        ddlYear.Items.Clear()
        Dim year As Integer = Now.Year
        Dim i As Integer
        Dim minMinusval As Integer = -2
        Dim chkDate As String = "04/30/" & year.ToString()
        If (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then
            minMinusval = -1
        End If
        If flag = 0 Then
            For i = -4 To minMinusval
                year = Now.Year + i
                ddlYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
            Next
            ddlYear.SelectedIndex = ddlYear.Items.IndexOf(ddlYear.Items.FindByValue(year))
        Else
            year = year + minMinusval
            ddlYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
        End If
    End Sub

    Private Sub loadbank(ByVal whr As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank" & whr & "")
        ddlbank.DataSource = ds
        ddlbank.DataTextField = "BankCode"
        ddlbank.DataValueField = "BankID"
        ddlbank.DataBind()

        ddlbalBank.DataSource = ds
        ddlbalBank.DataTextField = "BankCode"
        ddlbalBank.DataValueField = "BankID"
        ddlbalBank.DataBind()
    End Sub

    Protected Sub hlnkMainPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkMainPage.Click
        divInvPositions.Visible = False
        clearIn()
        If hlnkMainPage.Text = "Add/Update Bank Balances" Then
            hlnkMainPage.Text = "Show Internal Audit Reports"
            hlnkInvPositions.Text = "Add/Update  Inv Positions"
            divmain.Visible = False
            divInvPositions.Visible = False
            divaddupdateBankBalances.Visible = True
            BtnAddUpdate.Text = "Add"
            lblBalErr.Text = ""
            getdata()
        Else
            hlnkMainPage.Text = "Add/Update Bank Balances"
            divmain.Visible = True
            divInvPositions.Visible = False
            divaddupdateBankBalances.Visible = False
        End If
    End Sub

    Protected Sub hlnkInvPositions_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkInvPositions.Click
        If hlnkInvPositions.Text = "Add/Update  Inv Positions" Then
            hlnkInvPositions.Text = "Show Internal Audit Reports"
            hlnkMainPage.Text = "Add/Update Bank Balances"
            divInvPositions.Visible = True
            divmain.Visible = False
            divaddupdateBankBalances.Visible = False
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT BankID, BankCode FROM Bank WHERE BANKID in (4,5,6,7)")
            ddlInvPositionsBank.DataSource = ds
            ddlInvPositionsBank.DataTextField = "BankCode"
            ddlInvPositionsBank.DataValueField = "BankID"
            ddlInvPositionsBank.DataBind()
            getInvPositionsdata()
        Else
            hlnkInvPositions.Text = "Add/Update  Inv Positions"
            divmain.Visible = True
            divInvPositions.Visible = False
            divaddupdateBankBalances.Visible = False
        End If
    End Sub

    Public Sub CreateExcelFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "Internal_Audit_Reports_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"

            Select Case ddlbank.SelectedValue
                Case 1, 2, 3

                    Dim CCDeposits As Double
                    Dim CCDebits As Double
                    Dim REVENUECHECK As Double
                    Dim ACH As Double
                    Dim DEPOSITSLIPNO As String
                    Dim TRANSFERIN As Double
                    Dim INTEREST As Double
                    Dim CHECKNUM As String
                    Dim FEES As Double
                    Dim EXPENSECC As Double
                    Dim EXPENSECHECK As Double
                    Dim EXPENSEGRANT As Double
                    Dim TRANSFEROUT As Double
                    Dim CHECKDESC As String
                    Dim TransDate As Date


                    ' first update bankone details
                    ' update Beg Balance @ cell L7
                    '' sheSheets("BankOne")
                    ' clear existing data
                    '' oSheet.Selection("8:3000")
                    ''** CRange.Clear()
                    oSheet.Range("A1:N1").MergeCells = True
                    oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION "
                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A2").Value = "REGULAR ACCOUNT"
                    oSheet.Range("A2").Font.Bold = True
                    oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
                    oSheet.Range("A3").Font.Bold = True
                    oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
                    oSheet.Range("D2").Font.Bold = True
                    oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
                    oSheet.Range("D3").Font.Bold = True
                    oSheet.Range("D3").NumberFormat = "m/d/yyyy"

                    'sSql = "SELECT * FROM Begining_Balances WHERE FI_YEAR=" & iYear & " AND BANK_CODE = 'BKONE'"
                    'rsBegBal = db.OpenRecordset(sSql, dbOpenSnapshot)

                    'If Not rsBegBal.EOF Then
                    '    sFY_BegDate = CStr(rsBegBal("YEAR_BEG_DATE"))
                    '    sFY_EndDate = CStr(rsBegBal("YEAR_END_DATE"))
                    '    oSheet.Range("L7:L7")
                    '    oSheet.Range("L7:L7").Formula = rsBegBal("BEG_BALANCE")
                    '    oSheet.Range("L7:L7").NumberFormat = "#,##0.00_);(#,##0.00)"
                    'End If

                    ' open bank transactions
                    ' rows count
                    oSheet.Range("A6:O6").Font.Bold = True
                    oSheet.Range("A6:O6").ColumnWidth = 10
                    oSheet.Range("A6").Value = "Date"
                    oSheet.Range("B6").Value = "DepSlip#"
                    oSheet.Range("C6").Value = "Checks"
                    oSheet.Range("D6").Value = "Others"
                    oSheet.Range("E6").Value = "CC Deposits"
                    oSheet.Range("F6").Value = "Trans In"
                    oSheet.Range("G6").Value = "Interest"
                    oSheet.Range("H6").Value = "Check#"
                    oSheet.Range("I6").Value = "Expense"
                    oSheet.Range("J6").Value = "Grant"
                    oSheet.Range("K6").Value = "CC Debits"
                    oSheet.Range("L6").Value = "TransfOut"
                    oSheet.Range("M6").Value = "Balance"
                    oSheet.Range("N6").Value = "Remarks"

                    Dim CurrentRow As Integer
                    Dim sFY_BegDate As String
                    Dim sFY_EndDate As String
                    Dim CurrentTranDate As Date
                    Dim CurrTransCat As String
                    Dim Reason As String

                    sFY_BegDate = "05/01/" & Trim(Str(iYear))
                    sFY_EndDate = "04/30/" & Trim(Str(iYear + 1)) & " 23:59 "
                    Dim sSql As String = "SELECT BankTransID, BankID,CONVERT(date, TransDate) as TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description, AddInfo, DepSlipNumber,GLAccount FROM BankTrans WHERE BANKID=" & ddlbank.SelectedValue & " AND TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY TransDate"
                    Dim CRange As IRange

                    CRange = oSheet.Range("A7")
                    CRange.Value = Convert.ToDateTime("04/30/" & Trim(Str(iYear)))
                    CRange.NumberFormat = "MM/DD/YYYY"
                    CRange = oSheet.Range("M7")
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Amount  from BankBalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & ddlYear.SelectedValue & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

                    'start row for Bankone is 8
                    CurrentRow = 7
                    CurrentTranDate = "1/1/1990"
                    CurrTransCat = ""
                    Dim flag As Boolean = False


                    While rsBankTran.Read()
                        flag = True
                        Reason = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))
                        If Not (CurrentTranDate = rsBankTran("TransDate") And CurrTransCat = "CreditCard" And CurrTransCat = rsBankTran("TransCat")) Then
                            CurrentRow = CurrentRow + 1
                            CurrentTranDate = rsBankTran("TransDate")
                            CurrTransCat = rsBankTran("TransCat")
                            ' date
                            CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("TransDate") 'Format(rsBankTran("TransDate"), "mm/dd/yy") '
                            CRange.NumberFormat = "MM/DD/YYYY"
                            ' row total
                            CRange = oSheet.Range("M" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=M" & Trim(Str(CurrentRow - 1)) & "+C" & Trim(Str(CurrentRow)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow)) & "+F" & Trim(Str(CurrentRow)) & "+G" & Trim(Str(CurrentRow)) & "+I" & Trim(Str(CurrentRow)) & "+J" & Trim(Str(CurrentRow)) & "+K" & Trim(Str(CurrentRow)) & "+L" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                            CCDeposits = 0
                            CCDebits = 0
                            REVENUECHECK = 0
                            ACH = 0
                            TRANSFERIN = 0
                            INTEREST = 0
                            DEPOSITSLIPNO = ""
                            CHECKNUM = ""
                            FEES = 0
                            EXPENSECC = 0
                            EXPENSECHECK = 0
                            EXPENSEGRANT = 0
                            TRANSFEROUT = 0
                            CHECKDESC = ""
                        End If

                        ' credit / debit values

                        If rsBankTran("TransType") = "CREDIT" And rsBankTran("TransCat") = "CreditCard" Then 'CCDeposits
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                            CCDeposits = CCDeposits + rsBankTran("AMOUNT")
                            CRange.Value = IIf(CCDeposits = 0, "", CCDeposits)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                        ElseIf rsBankTran("TransType") = "DEBIT" And rsBankTran("TransCat") = "CreditCard" Then 'CCDebits
                            CRange = oSheet.Range("K" & Trim(Str(CurrentRow)))
                            CCDebits = CCDebits + rsBankTran("AMOUNT")
                            CRange.Value = IIf(CCDebits = 0, "", CCDebits)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                        ElseIf ((rsBankTran("TransType") = "CREDIT" And rsBankTran("TransCat") = "Transfer In") Or (rsBankTran("TransType") = "DEPOSIT" And rsBankTran("TransCat") = "Deposit" And Reason = "Transfer")) Then 'TRANSFERIN
                            CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                            TRANSFERIN = TRANSFERIN + rsBankTran("AMOUNT")
                            CRange.Value = IIf(TRANSFERIN = 0, "", TRANSFERIN)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                        ElseIf ((rsBankTran("TransType") = "DEPOSIT" And rsBankTran("TransCat") = "Deposit") Or (rsBankTran("TransCat") = "DepositError")) Then 'REVENUECHECK
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            REVENUECHECK = REVENUECHECK + rsBankTran("AMOUNT")
                            CRange.Value = IIf(REVENUECHECK = 0, "", REVENUECHECK)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' check number
                            CHECKNUM = IIf(rsBankTran("CHECKNUMBER") Is DBNull.Value, "", rsBankTran("CHECKNUMBER"))
                            If (rsBankTran("Description")) Is Nothing Then
                                CHECKDESC = ""
                            Else
                                CHECKDESC = IIf(rsBankTran("Description") = "", "", rsBankTran("Description"))
                            End If
                            CHECKDESC = CHECKDESC.Replace("DEPOSIT  ID NUMBER", "Dep#")
                            DEPOSITSLIPNO = IIf(rsBankTran("DepSlipNumber") Is DBNull.Value, "", rsBankTran("DepSlipNumber"))
                            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKNUM
                            CRange.NumberFormat = "0"
                            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                            CRange.Value = DEPOSITSLIPNO

                            ' check description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKDESC
                        ElseIf rsBankTran("TransCat") = "FundsReturn" Then 'FundsReturn
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            REVENUECHECK = REVENUECHECK + rsBankTran("AMOUNT")
                            CRange.Value = IIf(REVENUECHECK = 0, "", REVENUECHECK)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' check description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))
                        ElseIf rsBankTran("TransType") = "CREDIT" And rsBankTran("TransCat") = "Donation" Then 'ACH 
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            ACH = ACH + rsBankTran("AMOUNT")
                            CRange.Value = IIf(ACH = 0, "", ACH)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("VendCust") Is DBNull.Value, "", rsBankTran("VendCust"))

                        ElseIf rsBankTran("TransType") = "CREDIT" And rsBankTran("TransCat") = "Interest" Then 'INTEREST
                            CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
                            INTEREST = INTEREST + rsBankTran("AMOUNT")
                            CRange.Value = IIf(INTEREST = 0, "", INTEREST)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                        ElseIf ((rsBankTran("TransType") = "DEBIT" And rsBankTran("TransCat") = "Transfer Out") Or (rsBankTran("TransType") = "CHECK" And rsBankTran("TransCat") = "Check" And Reason = "Transfer")) Then 'TRANSFEROUT 
                            CRange = oSheet.Range("L" & Trim(Str(CurrentRow)))
                            TRANSFEROUT = TRANSFEROUT + rsBankTran("AMOUNT")
                            CRange.Value = IIf(TRANSFEROUT = 0, "", TRANSFEROUT)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))
                            ' check number
                            CHECKNUM = IIf(rsBankTran("CHECKNUMBER") Is DBNull.Value, "", rsBankTran("CHECKNUMBER"))
                            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKNUM
                        ElseIf ((rsBankTran("TransType") = "DEBIT" And rsBankTran("TransCat") = "Grant") Or (rsBankTran("TransType") = "CHECK" And rsBankTran("TransCat") = "Check" And Reason = "Grant")) Then 'EXPENSEGRANT 
                            CRange = oSheet.Range("J" & Trim(Str(CurrentRow)))
                            EXPENSEGRANT = EXPENSEGRANT + rsBankTran("AMOUNT")
                            CRange.Value = IIf(EXPENSEGRANT = 0, "", EXPENSEGRANT)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CHECKNUM = IIf(rsBankTran("CHECKNUMBER") Is DBNull.Value, "", rsBankTran("CHECKNUMBER"))
                            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKNUM
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("VendCust") Is DBNull.Value, "", rsBankTran("VendCust"))
                        ElseIf rsBankTran("TransType") = "CHECK" Then 'EXPENSECHECK
                            Response.Write(rsBankTran("TransType"))
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            EXPENSECHECK = EXPENSECHECK + rsBankTran("AMOUNT")
                            CRange.Value = IIf(EXPENSECHECK = 0, "", EXPENSECHECK)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            If (rsBankTran("Description")) Is Nothing Then
                                CHECKDESC = ""
                            Else
                                CHECKDESC = IIf(rsBankTran("Description") = "", "", rsBankTran("Description"))
                            End If
                            DEPOSITSLIPNO = IIf(rsBankTran("DepSlipNumber") Is DBNull.Value, "", rsBankTran("DepSlipNumber"))
                            ' check number
                            CHECKNUM = IIf(rsBankTran("CHECKNUMBER") Is DBNull.Value, "", rsBankTran("CHECKNUMBER"))
                            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKNUM

                            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                            CRange.Value = DEPOSITSLIPNO

                            ' check description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = CHECKDESC
                        ElseIf rsBankTran("TransCat") = "Shipping" Then 'Shipping
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            EXPENSECHECK = EXPENSECHECK + rsBankTran("AMOUNT")
                            CRange.Value = IIf(EXPENSECHECK = 0, "", EXPENSECHECK)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' check description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("VendCust") Is DBNull.Value, "", rsBankTran("VendCust"))
                        ElseIf rsBankTran("TransCat") = "DepositReturn" Then 'DepositReturn
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            EXPENSECHECK = EXPENSECHECK + rsBankTran("AMOUNT")
                            CRange.Value = IIf(EXPENSECHECK = 0, "", EXPENSECHECK)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' check description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))
                        ElseIf rsBankTran("TransCat") = "Fee" Then 'FEES
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            FEES = FEES + rsBankTran("AMOUNT")
                            CRange.Value = IIf(FEES = 0, "", FEES)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))

                        ElseIf rsBankTran("TransType") = "CREDIT" Then ' other CREDIT  in ACH 
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            ACH = ACH + rsBankTran("AMOUNT")
                            CRange.Value = IIf(ACH = 0, "", ACH)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, rsBankTran("Description"), rsBankTran("Reason"))
                        ElseIf rsBankTran("TransType") = "DEBIT" Then 'FEES
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            FEES = FEES + rsBankTran("AMOUNT")
                            CRange.Value = IIf(FEES = 0, "", FEES)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            ' description
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, rsBankTran("Description"), rsBankTran("Reason"))
                            'ElseIf rsBankTran("TransType") = "DEBIT" And rsBankTran("TransCat") = "Transfer" Then 'TRANSFER 
                            '    CRange = oSheet.Range("L" & Trim(Str(CurrentRow)))
                            '    TRANSFEROUT = TRANSFEROUT + rsBankTran("AMOUNT")
                            '    CRange.Value = IIf(TRANSFEROUT = 0, "", TRANSFEROUT)
                            '    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            '    ' description
                            '    CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            '    CRange.Value = IIf(rsBankTran("Reason") Is DBNull.Value, "", rsBankTran("Reason"))
                        End If
                        'REVENUECHECK
                        'ACH 
                        'TRANSFERIN 
                        'INTEREST 
                        'CHECKNUM 
                        'DEPOSITSLIPNO
                        'FEES 
                        'EXPENSECC 
                        'EXPENSECHECK 
                        'EXPENSEGRANT 
                        'TRANSFEROUT 
                        'CHECKDESC 

                    End While

                    If flag = False Then
                        lblErr.Text = "no record to Show"
                        Exit Sub
                    End If

                    ' '' update colum totals


                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C8:C" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D8:D" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E8:E" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F8:F" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G8:G" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"


                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(I8:I" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(J8:J" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(K8:K" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(L8:L" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M8:M" & Trim(Str(CurrentRow)) & ")"
                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M" & Trim(Str(CurrentRow)) & ":M" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    CRange = oSheet.Range("M" & Trim(Str(CurrentRow + 3)))
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    CRange.Font.Bold = True
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Amount  from BankBalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & Convert.ToInt32(ddlYear.SelectedValue) + 1 & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try

                    ' save the worksheet
                    'Stream workbook 
                    oSheet.Name = ddlbank.SelectedItem.Text
                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                    oWorkbooks.SaveAs(Response.OutputStream)
                    Response.End()
                Case 4, 5, 6, 7, 8
                    'Dim Ticker As String
                    'Dim DepCheck As Integer
                    'Dim Donations As Double
                    'Dim TransferIn As Double
                    'Dim MMKDividends As Double
                    'Dim StockDividends As Double
                    'Dim ExpCheck As Integer
                    'Dim TransferOut As Double
                    'Dim Reinvestment As Double
                    'Dim Grants As Double
                    'Dim Expenses As Double
                    'Dim Balance As Double
                    'Dim TransDate As Date

                    oSheet.Range("A1:N1").MergeCells = True
                    oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION "
                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A2").Value = "Brokerage Account"
                    oSheet.Range("A2").Font.Bold = True
                    oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
                    oSheet.Range("A3").Font.Bold = True
                    oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
                    oSheet.Range("D2").Font.Bold = True
                    oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
                    oSheet.Range("D3").Font.Bold = True
                    oSheet.Range("D3").NumberFormat = "m/d/yyyy"

                    'sSql = "SELECT * FROM Begining_Balances WHERE FI_YEAR=" & iYear & " AND BANK_CODE = 'BKONE'"
                    'rsBegBal = db.OpenRecordset(sSql, dbOpenSnapshot)

                    'If Not rsBegBal.EOF Then
                    '    sFY_BegDate = CStr(rsBegBal("YEAR_BEG_DATE"))
                    '    sFY_EndDate = CStr(rsBegBal("YEAR_END_DATE"))
                    '    oSheet.Range("L7:L7")
                    '    oSheet.Range("L7:L7").Formula = rsBegBal("BEG_BALANCE")
                    '    oSheet.Range("L7:L7").NumberFormat = "#,##0.00_);(#,##0.00)"
                    'End If

                    ' open bank transactions

                    ' rows count
                    Dim InitialRow As Integer = 8
                    oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                    oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                    oSheet.Range("A" & Str(InitialRow) & "").Value = "Date"
                    oSheet.Range("B" & Str(InitialRow) & "").Value = "Ticker"
                    oSheet.Range("C" & Str(InitialRow) & "").Value = "DepCheck"
                    oSheet.Range("D" & Str(InitialRow) & "").Value = "Donations"
                    oSheet.Range("E" & Str(InitialRow) & "").Value = "TransferIn"
                    oSheet.Range("F" & Str(InitialRow) & "").Value = "MMKDividends"
                    oSheet.Range("G" & Str(InitialRow) & "").Value = "StockDividends"
                    oSheet.Range("H" & Str(InitialRow) & "").Value = "ExpCheck"
                    oSheet.Range("I" & Str(InitialRow) & "").Value = "TransferOut"
                    oSheet.Range("J" & Str(InitialRow) & "").Value = "Reinvestment"
                    oSheet.Range("K" & Str(InitialRow) & "").Value = "Grants"
                    oSheet.Range("L" & Str(InitialRow) & "").Value = "Expenses"
                    oSheet.Range("M" & Str(InitialRow) & "").Value = "Balance"
                    oSheet.Range("N" & Str(InitialRow) & "").Value = "Remarks"

                    Dim CurrentRow As Integer
                    Dim sFY_BegDate As String
                    Dim sFY_EndDate As String
                    Dim CurrentTranDate As Date
                    Dim CurrTransCat As String
                    Dim Reason As String

                    sFY_BegDate = "05/01/" & Trim(Str(iYear))
                    sFY_EndDate = "04/30/" & Trim(Str(iYear + 1)) & " 23:59 "
                    Dim sSql As String = "SELECT B1.BrokTransID, B1.BankID, B1.BankCode,CONVERT(date, B1.TransDate) as TransDate, B1.TransType, ISNUll(B1.TransCat,'') as TransCat, B1.Medium, B1.AssetClass, B1.Ticker, B1.Quantity, B1.Price, B1.NetAmount, B1.Comm, B1.TranID, B1.ExpCheckNo, B1.DepCheckNo, B1.Description, B1.OutstdShares, B1.CostBasis, B1.AvgPrice, B1.Disposed_Shares, B1.Disposed_Price, B1.Disposed_Basis, B1.Capital_Gains,B2.Quantity as InvQty,B2.Price as InvPrice FROM BrokTrans B1 Left Join BrokTrans B2 On B2.BankID = B1.BankID AND B2.TransDate=B1.TransDate AND B1.Ticker=B2.Ticker and B1.NetAmount = -(B2.NetAmount) AND B2.TransCat='Reinvest' and B2.TransType='TRN' WHERE B1.BANKID=" & ddlbank.SelectedValue & " AND B1.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B1.TransDate, B1.ticker, B1.transtype "
                    Dim CRange As IRange

                    CRange = oSheet.Range("A5")
                    CRange.Value = "MONEY MARKET"
                    CRange.Font.Bold = True

                    CRange = oSheet.Range("A" & Str(InitialRow + 1))
                    CRange.Value = Convert.ToDateTime("04/30/" & Trim(Str(iYear)))
                    CRange.NumberFormat = "MM/DD/YYYY"
                    CRange = oSheet.Range("M" & Str(InitialRow + 1))
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Amount  from BankBalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & ddlYear.SelectedValue & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

                    'start row for Bankone is 8
                    CurrentRow = InitialRow + 1
                    CurrentTranDate = "1/1/1990"
                    CurrTransCat = ""
                    Dim flag As Boolean = False


                    While rsBankTran.Read()
                        flag = True
                        Reason = IIf(rsBankTran("Description") Is DBNull.Value, "", rsBankTran("Description"))
                        If ((rsBankTran("AssetClass").ToString().ToLower.Trim = "mmk" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "cash") And ((rsBankTran("TransCat").ToString().ToLower.Trim = "div" And rsBankTran("TransType").ToString().ToLower.Trim = "div") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "grants" And rsBankTran("TransType").ToString().ToLower.Trim = "trn"))) Or (rsBankTran("AssetClass").ToString().ToLower.Trim = "stock" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "mutfund") And ((rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or ((rsBankTran("TransCat").ToString().ToLower.Trim = "stcgdiv" Or rsBankTran("TransCat").ToString().ToLower.Trim = "orddiv" Or rsBankTran("TransCat").ToString().ToLower.Trim = "ltcgdiv") And rsBankTran("TransType").ToString().ToLower.Trim = "div")) Then
                            CurrentRow = CurrentRow + 1
                            'CurrentTranDate = rsBankTran("TransDate")
                            'CurrTransCat = rsBankTran("TransCat")
                            ' date

                            CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("TransDate") 'Format(rsBankTran("TransDate"), "mm/dd/yy") '
                            CRange.NumberFormat = "MM/DD/YYYY"
                            ' row total


                            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Ticker")
                            CRange = oSheet.Range("M" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=M" & Trim(Str(CurrentRow - 1)) & "+D" & Trim(Str(CurrentRow)) & "+E" & Trim(Str(CurrentRow)) & "+F" & Trim(Str(CurrentRow)) & "+G" & Trim(Str(CurrentRow)) & "+I" & Trim(Str(CurrentRow)) & "+J" & Trim(Str(CurrentRow)) & "+K" & Trim(Str(CurrentRow)) & "+L" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"



                            'CCDeposits = 0
                            'CCDebits = 0
                            'REVENUECHECK = 0
                            'ACH = 0
                            'TransferIn = 0
                            'INTEREST = 0
                            'DEPOSITSLIPNO = ""
                            'CHECKNUM = ""
                            'FEES = 0
                            'EXPENSECC = 0
                            'EXPENSECHECK = 0
                            'EXPENSEGRANT = 0
                            'TransferOut = 0
                            'CHECKDESC = ""
                            'End If

                            ' credit / debit values

                            ''*Note 1: Before generating this report, sort records by trans date, ticker, trans type 
                            ''Note 2: When Asset Type = 'MMK' or 'Cash', use the following rules: 
                            ''Note 2a: If  TransCat = 'Div' and TransType = 'Div', then enter Quantity into MMK Dividends column 
                            ''Note 2b: If  TransCat = 'TransferIn' and TransType = 'TRN', then enter Net Amount into TransferIn column.  Enter Check# into DepCheck# column 
                            ''Note 2c: If  TransCat = 'TransferOut' and TransType = 'TRN', then enter Net Amount into TransferOut column.  Enter Check# into ExpCheck# column 
                            ''Note 2d: If  TransCat = 'Grants' and Trans Type = 'TRN', then enter Net Amount into Grants column.  Enter Check# into ExpCheck# column 
                            ''Note 2e: Skip all other records 
                            ''Note 3: When Asset Type = 'Stock' or 'MutFund', use the following rules: 
                            ''Note 3a: If  TransCat = 'Div' or TransType = 'Div', then enter Net Amount into Stock Dividends column 
                            ''Note 3b: If  Trans Cat = 'Div' and Trans Type = 'TRN', then enter Net Amount into Reinvestment column 
                            ''Note 3c: Skip all other records 
                            ''Note 4: Insert Ticker in all rows 



                            If rsBankTran("AssetClass").ToString().ToLower.Trim = "mmk" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "cash" Then

                                If rsBankTran("TransCat").ToString().ToLower.Trim = "div" And rsBankTran("TransType").ToString().ToLower.Trim = "div" Then
                                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Quantity")
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("DepCheckNo")
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("ExpCheckNo")
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "grants" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("K" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("ExpCheckNo")
                                End If


                            ElseIf rsBankTran("AssetClass").ToString().ToLower.Trim = "stock" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "mutfund" Then
                                If (rsBankTran("TransCat").ToString().ToLower.Trim = "stcgdiv" Or rsBankTran("TransCat").ToString().ToLower.Trim = "orddiv" Or rsBankTran("TransCat").ToString().ToLower.Trim = "ltcgdiv") And rsBankTran("TransType").ToString().ToLower.Trim = "div" Then 'Stock Dividends 
                                    CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("TransCat")
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then 'Reinvestment
                                    CRange = oSheet.Range("J" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                End If
                            End If
                            'REVENUECHECK
                            'ACH 
                            'TRANSFERIN 
                            'INTEREST 
                            'CHECKNUM 
                            'DEPOSITSLIPNO
                            'FEES 
                            'EXPENSECC 
                            'EXPENSECHECK 
                            'EXPENSEGRANT 
                            'TRANSFEROUT 
                            'CHECKDESC 
                        End If
                    End While

                    If flag = False Then
                        lblErr.Text = "no record to Show"
                        Exit Sub
                    End If

                    ' '' update colum totals


                    'oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C8:C" & Trim(Str(CurrentRow)) & ")"
                    'oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    'oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"

                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G" & Trim(Str(InitialRow + 1)) & ":G" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"


                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(I" & Trim(Str(InitialRow + 1)) & ":I" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(J" & Trim(Str(InitialRow + 1)) & ":J" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(K" & Trim(Str(InitialRow + 1)) & ":K" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(L" & Trim(Str(InitialRow + 1)) & ":L" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M8:M" & Trim(Str(CurrentRow)) & ")"
                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    'oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M" & Trim(Str(CurrentRow)) & ":M" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    CRange = oSheet.Range("M" & Trim(Str(CurrentRow + 3)))
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    CRange.Font.Bold = True
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Amount  from BankBalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & Convert.ToInt32(ddlYear.SelectedValue) + 1 & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try

                    CRange = oSheet.Range("A" & Str(CurrentRow + 5))
                    CRange.Value = "MUTUAL FUNDS/STOCKS"
                    CRange.Font.Bold = True

                    InitialRow = CurrentRow + 7

                    oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                    oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                    oSheet.Range("A" & Str(InitialRow) & "").Value = "Date"
                    oSheet.Range("B" & Str(InitialRow) & "").Value = "Ticker"
                    oSheet.Range("C" & Str(InitialRow) & "").Value = "Quantity"
                    oSheet.Range("D" & Str(InitialRow) & "").Value = "Price"
                    oSheet.Range("E" & Str(InitialRow) & "").Value = "TransferIn"
                    oSheet.Range("F" & Str(InitialRow) & "").Value = "TransferOut"
                    oSheet.Range("G" & Str(InitialRow) & "").Value = "Sale Proceeds"
                    oSheet.Range("H" & Str(InitialRow) & "").Value = "Ord Div"
                    oSheet.Range("I" & Str(InitialRow) & "").Value = "STCG Div"
                    oSheet.Range("J" & Str(InitialRow) & "").Value = "LTCG Div"
                    oSheet.Range("K" & Str(InitialRow) & "").Value = "Buy"
                    oSheet.Range("L" & Str(InitialRow) & "").Value = "Gains/Losses"
                    oSheet.Range("M" & Str(InitialRow) & "").Value = "Disposed Basis"
                    oSheet.Range("N" & Str(InitialRow) & "").Value = "Cost Basis"
                    oSheet.Range("O" & Str(InitialRow) & "").Value = "Remarks"

                    CRange = oSheet.Range("A" & Str(InitialRow + 1))
                    CRange.Value = Convert.ToDateTime("04/30/" & Trim(Str(iYear)))
                    CRange.NumberFormat = "MM/DD/YYYY"
                    CRange = oSheet.Range("N" & Str(InitialRow + 1))
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select sum(CostBasis) from Costbasisbalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & ddlYear.SelectedValue & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                    rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

                    'start row for Bankone is 8
                    CurrentRow = InitialRow + 1
                    CurrentTranDate = "1/1/1990"
                    CurrTransCat = ""

                    While rsBankTran.Read()
                        flag = True
                        Reason = IIf(rsBankTran("Description") Is DBNull.Value, "", rsBankTran("Description"))
                        If (rsBankTran("AssetClass").ToString().ToLower.Trim = "stock" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "mutfund") And ((rsBankTran("TransCat").ToString().ToLower.Trim = "buy" And rsBankTran("TransType").ToString().ToLower.Trim = "buy") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "sell" And rsBankTran("TransType").ToString().ToLower.Trim = "sell") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "buy" And rsBankTran("TransType").ToString().ToLower.Trim = "buy") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "stcgdiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "orddiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div") Or (rsBankTran("TransCat").ToString().ToLower.Trim = "ltcgdiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div")) Then '(rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" And rsBankTran("TransType").ToString().ToLower.Trim = "trn") Or
                            CurrentRow = CurrentRow + 1
                            'CurrentTranDate = rsBankTran("TransDate")
                            'CurrTransCat = rsBankTran("TransCat")
                            ' date
                            CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("TransDate") 'Format(rsBankTran("TransDate"), "mm/dd/yy") '
                            CRange.NumberFormat = "MM/DD/YYYY"
                            ' row total


                            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Ticker")
                            CRange = oSheet.Range("N" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=N" & Trim(Str(CurrentRow - 1)) & "+E" & Trim(Str(CurrentRow)) & "+F" & Trim(Str(CurrentRow)) & "+G" & Trim(Str(CurrentRow)) & "+H" & Trim(Str(CurrentRow)) & "+I" & Trim(Str(CurrentRow)) & "+J" & Trim(Str(CurrentRow)) & "+K" & Trim(Str(CurrentRow)) & "+L" & Trim(Str(CurrentRow)) & "+M" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                            ''Note 1: Before generating this report, sort records by trans date, ticker, trans type 
                            ''Note 2: Skip records when Asset Type = 'MMK' or 'Cash' 
                            ''Note 3: When Asset Type = 'Stock' or 'MutFund', use the following rules: 
                            ''Note 3a: If  Trans Cat = 'Reinvest' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Insert  absolute or positve value of (Net Amount) into Div - Reg column 
                            ''Note 3b: If  Trans Cat = 'Buy' and Trans Type = 'Buy', then enter data into Quantity and Price columns.  Insert  absolute or positve value of (Net Amount) into Buy column 
                            ''Note 3c: If  Trans Cat = 'Sell' and Trans Type = 'Sell', then enter data into Quantity and Price columns.  Insert  Net Amount into Sale Proceeds column. Bisposed Basis should be filled with Qty * Avg price (from this account) 
                            ''Note 3d: If  Trans Cat = 'TransferIn' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Price is AvgPrice (Cost Basis from the other account) Insert  Qty*Price into TransferIn column 
                            ''Note 3e: If  Trans Cat = 'TransferOut' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Price is AvgPrice (Cost Basis from this account). Insert  Qty*Price into TransferOut column 

                            If rsBankTran("AssetClass").ToString().ToLower.Trim = "stock" Or rsBankTran("AssetClass").ToString().ToLower.Trim = "mutfund" Then

                                If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Quantity")
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Price")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" And rsBankTran("TransType").ToString().ToLower.Trim = "buy" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    'CRange.Value = rsBankTran("Quantity")
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Price")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("K" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" And rsBankTran("TransType").ToString().ToLower.Trim = "sell" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    'CRange.Value = rsBankTran("Quantity")
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Price")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("M" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("Quantity") Is DBNull.Value, 0, rsBankTran("Quantity")) * IIf(rsBankTran("AvgPrice") Is DBNull.Value, 0, rsBankTran("AvgPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Quantity")
                                    ''** next line removed for some purpose
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = GetAvgPrice(0, rsBankTran("Ticker"), rsBankTran("Quantity") * -1, rsBankTran("TransDate"), sFY_BegDate, sFY_EndDate)
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                                    CRange.Formula = "=D" & Trim(Str(CurrentRow)) & "*C" & Trim(Str(CurrentRow))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("Quantity")
                                    'CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = GetAvgPrice(ddlbank.SelectedValue, rsBankTran("Ticker"), rsBankTran("Quantity") * -1, rsBankTran("TransDate"), sFY_BegDate, sFY_EndDate)
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                                    CRange.Formula = "=D" & Trim(Str(CurrentRow)) & "*C" & Trim(Str(CurrentRow))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "orddiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvPrice") Is DBNull.Value, rsBankTran("Price"), rsBankTran("InvPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "stcgdiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvPrice") Is DBNull.Value, rsBankTran("Price"), rsBankTran("InvPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvPrice") Is DBNull.Value, rsBankTran("Price"), rsBankTran("InvPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "ltcgdiv" And rsBankTran("TransType").ToString().ToLower.Trim = "div" Then
                                    CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvQty") Is DBNull.Value, rsBankTran("Quantity"), rsBankTran("InvQty"))
                                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvPrice") Is DBNull.Value, rsBankTran("Price"), rsBankTran("InvPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                                    CRange.Value = IIf(rsBankTran("InvPrice") Is DBNull.Value, rsBankTran("Price"), rsBankTran("InvPrice"))
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                    CRange = oSheet.Range("J" & Trim(Str(CurrentRow)))
                                    CRange.Value = rsBankTran("NetAmount")
                                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                                End If
                            End If
                            'REVENUECHECK
                            'ACH 
                            'TRANSFERIN 
                            'INTEREST 
                            'CHECKNUM 
                            'DEPOSITSLIPNO
                            'FEES 
                            'EXPENSECC 
                            'EXPENSECHECK 
                            'EXPENSEGRANT 
                            'TRANSFEROUT 
                            'CHECKDESC 
                        End If
                    End While

                    If flag = False Then
                        lblErr.Text = "no record to Show"
                        Exit Sub
                    End If

                    ' '' update colum totals


                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    ''**Removed for some purpose
                    oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"

                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G" & Trim(Str(InitialRow + 1)) & ":G" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(H" & Trim(Str(InitialRow + 1)) & ":H" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("H" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"


                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(I" & Trim(Str(InitialRow + 1)) & ":I" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                    oSheet.Range("I" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(J" & Trim(Str(InitialRow + 1)) & ":J" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(K" & Trim(Str(InitialRow + 1)) & ":K" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(L" & Trim(Str(InitialRow + 1)) & ":L" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M" & Trim(Str(InitialRow + 1)) & ":M" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    oSheet.Range("N" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(N" & Trim(Str(CurrentRow)) & ":N" & Trim(Str(CurrentRow)) & ")"
                    oSheet.Range("N" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
                    oSheet.Range("N" & Trim(Str(CurrentRow + 2))).Font.Bold = True

                    CRange = oSheet.Range("N" & Str(CurrentRow + 3))
                    Try
                        CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select sum(CostBasis) from Costbasisbalances where BankID=" & ddlbank.SelectedValue & " and Enddate='04/30/" & Convert.ToInt32(ddlYear.SelectedValue) + 1 & "'")
                    Catch ex As Exception
                        CRange.Value = 0
                    End Try
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                    ' save the worksheet
                    'Stream workbook 
                    oSheet.Name = ddlbank.SelectedItem.Text
                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                    oWorkbooks.SaveAs(Response.OutputStream)
                    Response.End()

            End Select

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub CreateCostBasisExcelFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "CostB_" & ddlbank.SelectedItem.Text.Trim & "_" & Trim(Str(iYear + 1)) & ".xls"

            Select Case ddlbank.SelectedValue
                Case 4, 5, 6, 7, 8
                    oSheet.Range("A1:N1").MergeCells = True
                    oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION "
                    oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
                    oSheet.Range("A1").Font.Bold = True
                    oSheet.Range("A2").Value = "Cost Basis Report"
                    oSheet.Range("A2").Font.Bold = True
                    oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
                    oSheet.Range("A3").Font.Bold = True
                    oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
                    oSheet.Range("D2").Font.Bold = True
                    oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
                    oSheet.Range("D3").Font.Bold = True
                    oSheet.Range("D3").NumberFormat = "m/d/yyyy"

                    ' rows count
                    Dim InitialRow As Integer
                    Dim CurrentRow As Integer
                    Dim CurrTicker As String
                    Dim avgPrice As Decimal
                    Dim BegBalTot As String = ""
                    Dim EndBalTot As String = ""
                    Dim SaleFlag As Boolean = False
                    Dim sFY_BegDate As String
                    Dim sFY_EndDate As String
                    sFY_BegDate = "05/01/" & Trim(Str(iYear))
                    sFY_EndDate = "04/30/" & Trim(Str(iYear + 1))
                    'Dim sSql As String = "SELECT  B.BankID, CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount, B.AvgPrice,T.Name as TickerName FROM BrokTrans B  Left Join Ticker T ON T.Ticker = B.Ticker WHERE B.assetClass not in ('MMK','Cash') AND BANKID=7 AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
                    Dim sSql As String = "select Distinct M.BankID,CONVERT(date, C.BegDate) as TransDate,'BB' as TransType, '' as TransCat,M.Symbol as Ticker,ISNUll(C.OutStdShares,0.0) as Quantity,0.0 as Price, ISNUll(C.CostBasis,0.0) as NetAmount,ISNUll(C.AvgPrice,0.0) as AvgPrice,T.Name as TickerName from MarketValue M  Inner Join Ticker T ON T.Ticker = M.Symbol AND (T.Class not in ('MMK') or T.Class is NULL) Left Join CostBasisBalances C ON M.Symbol = C.Ticker  and  C.BankID=" & ddlbank.SelectedValue & "  and C.BegDate='" & sFY_BegDate.Trim & "' where M.BankID=" & ddlbank.SelectedValue & " and M.Date Between '04/30/" & Trim(Str(iYear - 1)) & "' AND '04/30/" & Trim(Str(iYear + 1)) & "'  UNION ALL "
                    sSql = sSql & " SELECT B.BankID, CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount, B.AvgPrice,T.Name as TickerName FROM BrokTrans B  Left Join Ticker T ON T.Ticker = B.Ticker  WHERE B.assetClass not in ('MMK','Cash') AND BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype"
                    'Response.Write(sSql)
                    'Exit Sub
                    Dim CRange As IRange
                    InitialRow = 3
                    Dim reinvestRow As Integer = 0
                    rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
                    Dim bankID As String = 0
                    CurrTicker = ""
                    Dim flag As Boolean = False
                    Dim ds As DataSet
                    CurrentRow = InitialRow
                    While rsBankTran.Read()
                        flag = True
                        If CurrTicker.Trim.ToLower <> rsBankTran("Ticker").ToString().Trim.ToLower Then
                            If CurrTicker <> "" Then
                                '' '' update colum totals
                                CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                                CRange.Value = "End bal"
                                CRange.Font.Bold = True
                                If BegBalTot.Length = 0 Then
                                    BegBalTot = "=E" & Trim(Str(InitialRow + 1))
                                    EndBalTot = "=E" & Trim(Str(CurrentRow + 1))
                                Else
                                    BegBalTot = BegBalTot & "+E" & Trim(Str(InitialRow + 1))
                                    EndBalTot = EndBalTot & "+E" & Trim(Str(CurrentRow + 1))
                                End If
                                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.0000_);(#,##0.0000)"

                                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=E" & Trim(Str(CurrentRow) + 1) & "/C" & Trim(Str(CurrentRow) + 1) '"=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=G" & Trim(Str(CurrentRow)) '"=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                ''oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
                                ''oSheet.Range("F" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                ''oSheet.Range("F" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                ''oSheet.Range("G" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(G" & Trim(Str(InitialRow + 1)) & ":G" & Trim(Str(CurrentRow)) & ")"
                                ''oSheet.Range("G" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                ''oSheet.Range("G" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                ''oSheet.Range("H" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(H" & Trim(Str(InitialRow + 1)) & ":H" & Trim(Str(CurrentRow)) & ")"
                                ''oSheet.Range("H" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                ''oSheet.Range("H" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"
                                If SaleFlag = True Then
                                    'Coding for Sale
                                    oSheet.Range("J" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(J" & Trim(Str(InitialRow + 1)) & ":J" & Trim(Str(CurrentRow)) & ")"
                                    oSheet.Range("J" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                    oSheet.Range("J" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.0000_);(#,##0.0000)"

                                    oSheet.Range("K" & Trim(Str(CurrentRow + 1))).Formula = "=L" & Trim(Str(CurrentRow + 1)) & "/J" & Trim(Str(CurrentRow + 1))
                                    oSheet.Range("K" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                    oSheet.Range("K" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                    oSheet.Range("L" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(L" & Trim(Str(InitialRow + 1)) & ":L" & Trim(Str(CurrentRow)) & ")"
                                    oSheet.Range("L" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                    oSheet.Range("L" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                    oSheet.Range("M" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(M" & Trim(Str(InitialRow + 1)) & ":M" & Trim(Str(CurrentRow)) & ")"
                                    oSheet.Range("M" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                    oSheet.Range("M" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"
                                    SaleFlag = False
                                End If
                                reinvestRow = 0
                                CurrentRow = CurrentRow + 1

                                CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                                CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                                CRange.Font.Bold = True
                                Try
                                    CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Quantity from MarketValue where Date='04/30/" & Trim(Str(iYear + 1)) & "' and Symbol='" & CurrTicker & "' and BankID =" & bankID & "")
                                Catch ex As Exception
                                    CRange.Value = 0
                                End Try
                            End If
                            CurrTicker = rsBankTran("Ticker").ToString()
                            bankID = rsBankTran("bankID").ToString()
                            InitialRow = CurrentRow + 3
                            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
                            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
                            oSheet.Range("A" & Str(InitialRow) & "").Value = CurrTicker
                            oSheet.Range("B" & Str(InitialRow) & "").Value = "Date"
                            oSheet.Range("C" & Str(InitialRow) & "").Value = "Shares"
                            oSheet.Range("D" & Str(InitialRow) & "").Value = "Price"
                            oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"
                            oSheet.Range("F" & Str(InitialRow) & "").Value = "Outstanding Shares"
                            oSheet.Range("G" & Str(InitialRow) & "").Value = "Cost Basis"
                            oSheet.Range("H" & Str(InitialRow) & "").Value = "Avg Price"
                            oSheet.Range("I" & Str(InitialRow) & "").Value = "Remarks"
                            oSheet.Range("J" & Str(InitialRow) & "").Value = rsBankTran("TickerName").ToString()

                            'CRange = oSheet.Range("A" & Str(InitialRow - 3))
                            'CRange.Value = "MONEY MARKET"
                            'CRange.Font.Bold = True                                

                        End If



                        If Not rsBankTran("TransType").ToString().ToLower.Trim = "bb" Then
                            CurrentRow = CurrentRow + 1
                            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("TransDate") 'Format(rsBankTran("TransDate"), "mm/dd/yy") '
                            CRange.NumberFormat = "MM/DD/YYYY"

                            CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=F" & Trim(Str(CurrentRow - 1)) & "+C" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"

                            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=G" & Trim(Str(CurrentRow)) & "/F" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                        End If


                        'Note 3a: If  Trans Cat = 'Reinvest' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Insert  absolute or positve value of (Net Amount) into Amount column 
                        'Note 3b: If  Trans Cat = 'Buy' and Trans Type = 'Buy', then enter data into Quantity and Price columns.  Insert  absolute or positve value of (Net Amount) into Amount Column 
                        'Note 3c: If  Trans Cat = 'Sell' and Trans Type = 'Sell', then enter data into Quantity (enter as a negative number) and Price columns.  Insert  Net Amount into Amount column as negative number. 
                        'Note 3d: If  Trans Cat = 'TransferIn' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Price is AvgPrice (Cost Basis from the other account) Insert  Qty*Price into Amount column 
                        'Note 3e: If  Trans Cat = 'TransferOut' and Trans Type = 'TRN', then enter data into Quantity and Price columns.  Price is AvgPrice as a negative number (Cost Basis from this account). Insert  Qty*Price into Amount column, as a negative number. 

                        If rsBankTran("TransType").ToString().ToLower.Trim = "bb" Then
                            CRange = oSheet.Range("B" & Str(InitialRow + 1))
                            CRange.Value = "Beg bal"
                            CRange.Font.Bold = True
                            CRange = oSheet.Range("C" & Str(InitialRow + 1))
                            CRange.Value = rsBankTran("Quantity")
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange = oSheet.Range("D" & Str(InitialRow + 1))
                            CRange.Value = rsBankTran("AvgPrice")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Str(InitialRow + 1))
                            CRange.Value = rsBankTran("NetAmount")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                            CRange = oSheet.Range("F" & Str(InitialRow + 1))
                            CRange.Formula = "=C" & Trim(Str(InitialRow + 1))
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"

                            CRange = oSheet.Range("G" & Str(InitialRow + 1))
                            CRange.Formula = "=E" & Trim(Str(InitialRow + 1))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

                            CRange = oSheet.Range("H" & Trim(Str(InitialRow + 1)))
                            CRange.Formula = "=G" & Trim(Str(InitialRow + 1) & "/F" & Trim(Str(InitialRow + 1)))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CurrentRow = InitialRow + 1
                        End If

                        If rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                            oSheet.Range("J" & Str(InitialRow) & "").Value = "Disposed Shares"
                            oSheet.Range("K" & Str(InitialRow) & "").Value = "Disposed Price"
                            oSheet.Range("L" & Str(InitialRow) & "").Value = "Disposed Basis"
                            oSheet.Range("M" & Str(InitialRow) & "").Value = "Capital Gains"
                            oSheet.Range("N" & Str(InitialRow) & "").Value = rsBankTran("TickerName").ToString()
                            CRange = oSheet.Range("J" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=-C" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange = oSheet.Range("K" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=H" & Trim(Str(IIf(reinvestRow = 0, InitialRow + 1, reinvestRow)))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("L" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=J" & Trim(Str(CurrentRow)) & "*K" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("M" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=-E" & Trim(Str(CurrentRow)) & "-L" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=G" & Trim(Str(CurrentRow - 1)) & "-L" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            SaleFlag = True
                        ElseIf Not rsBankTran("TransType").ToString().ToLower.Trim = "bb" Then
                            CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
                            CRange.Formula = "=G" & Trim(Str(CurrentRow - 1)) & "+E" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                        End If

                        If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                            'Note 3a (1): If  Trans Cat = 'Reinvest' and Trans Type = 'TRN' and Net Amount is negative, then enter data into Quantity and Price columns.  Insert  absolute or positve value of (Net Amount) into Amount column
                            'Note 3a (2): If  Trans Cat = 'Reinvest' and Trans Type = 'TRN' and Net Amount is positve, then enter data into Quantity as negative and Price columns.  Insert  negative value of (Net Amount) into Amount column
                            reinvestRow = CurrentRow
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Price")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("NetAmount") * -1
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            CRange.Value = "Reinvestment"
                        ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" And rsBankTran("TransType").ToString().ToLower.Trim = "buy" Then
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Quantity")
                            CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Price")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                            CRange.Value = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            CRange.Value = "Buy"
                        ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" And rsBankTran("TransType").ToString().ToLower.Trim = "sell" Then
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Quantity") * -1
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Price")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("NetAmount") * -1
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            CRange.Value = "Sold"
                            ' Diff logic
                        ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Quantity")
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            avgPrice = GetAvgPrice(0, CurrTicker, rsBankTran("Quantity") * -1, rsBankTran("TransDate"), sFY_BegDate, sFY_EndDate)
                            CRange.Value = avgPrice
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))

                            CRange.Value = avgPrice * rsBankTran("Quantity")
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            CRange.Value = "TransferIn"
                        ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" And rsBankTran("TransType").ToString().ToLower.Trim = "trn" Then
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
                            CRange.Value = rsBankTran("Quantity")
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
                            CRange.Value = "=H" & Trim(Str(CurrentRow) - 1)
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                            CRange.Value = "=C" & Trim(Str(CurrentRow)) & "*D" & Trim(Str(CurrentRow))
                            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
                            CRange.Value = "TransferOut"
                        End If
                    End While

                    If flag = False Then
                        lblErr.Text = "no record to Show"
                        Exit Sub
                    Else
                        If CurrTicker <> "" Then
                            '' '' update colum totals
                            CRange = oSheet.Range("B" & Str(CurrentRow + 1))
                            CRange.Value = "End bal"
                            CRange.Font.Bold = True
                            If BegBalTot.Length = 0 Then
                                BegBalTot = "=E" & Trim(Str(InitialRow + 1))
                                EndBalTot = "=E" & Trim(Str(CurrentRow + 1))
                            Else
                                BegBalTot = BegBalTot & "+E" & Trim(Str(InitialRow + 1))
                                EndBalTot = EndBalTot & "+E" & Trim(Str(CurrentRow + 1))
                            End If
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("C" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.0000_);(#,##0.0000)"

                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Formula = "=E" & Trim(Str(CurrentRow) + 1) & "/C" & Trim(Str(CurrentRow) + 1) '"=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("D" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Formula = "=G" & Trim(Str(CurrentRow)) '"=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                            oSheet.Range("E" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"
                            If SaleFlag = True Then
                                'Coding for Sale
                                oSheet.Range("J" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(J" & Trim(Str(InitialRow + 1)) & ":J" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("J" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("J" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                oSheet.Range("K" & Trim(Str(CurrentRow + 1))).Formula = "=L" & Trim(Str(CurrentRow + 1)) & "/J" & Trim(Str(CurrentRow + 1))
                                oSheet.Range("K" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("K" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                oSheet.Range("L" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(L" & Trim(Str(InitialRow + 1)) & ":L" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("L" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("L" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"

                                oSheet.Range("M" & Trim(Str(CurrentRow + 1))).Formula = "=SUM(M" & Trim(Str(InitialRow + 1)) & ":M" & Trim(Str(CurrentRow)) & ")"
                                oSheet.Range("M" & Trim(Str(CurrentRow + 1))).Font.Bold = True
                                oSheet.Range("M" & Trim(Str(CurrentRow + 1))).NumberFormat = "#,##0.00_);(#,##0.00)"
                                SaleFlag = False
                            End If
                            CurrentRow = CurrentRow + 1
                            CRange = oSheet.Range("C" & Trim(Str(CurrentRow + 1)))
                            CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                            CRange.Font.Bold = True
                            Try
                                CRange.Value = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select Quantity from MarketValue where Date='04/30/" & Trim(Str(iYear + 1)) & "' and Symbol='" & CurrTicker & "' and BankID =" & bankID & "")
                            Catch ex As Exception
                                CRange.Value = 0
                            End Try
                        End If
                    End If

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 3)))
                    CRange.Value = "Beg Bal"
                    CRange.Font.Bold = True

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 3)))
                    CRange.Formula = BegBalTot
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    CRange.Font.Bold = True

                    CRange = oSheet.Range("D" & Trim(Str(CurrentRow + 4)))
                    CRange.Value = "End Bal"
                    CRange.Font.Bold = True

                    CRange = oSheet.Range("E" & Trim(Str(CurrentRow + 4)))
                    CRange.Formula = EndBalTot
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    CRange.Font.Bold = True

                    ' save the worksheet
                    'Stream workbook 
                    oSheet.Name = "CostB_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1))
                    Response.Clear()
                    Response.ContentType = "application/vnd.ms-excel"
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel")
                    Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
                    oWorkbooks.SaveAs(Response.OutputStream)
                    Response.End()

            End Select

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Function GetAvgPrice(ByVal BankID As Integer, ByVal CurrTicker As String, ByVal Quantity As Double, ByVal TransDate As String, ByVal sFY_BegDate As String, ByVal sFY_EndDate As String) As Double
        Dim sSql As String
        If BankID = 0 Then
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=(select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID in (select BankID from BrokTrans where Ticker='" & CurrTicker & "' and Quantity =" & Quantity & " and TransCat = 'TransferOut' and TransDate = '" & TransDate & "') AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        Else
            sSql = "SELECT BankID,'01/01/1990' as TransDate,'TRN' as TransType, 'BegBal' as TransCat, Ticker, OutStdShares as Quantity ,AvgPrice,CostBasis as NetAmount from CostBasisBalances where BankID=" & BankID & " and Ticker='" & CurrTicker & "' and BegDate= '" & sFY_BegDate & "' Union all "
            sSql = sSql & "SELECT B.BankID,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount FROM BrokTrans B  WHERE B.assetClass not in ('MMK','Cash') AND B.Ticker='" & CurrTicker & "' AND  B.BankID =" & BankID & " AND "
            sSql = sSql & " ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN  '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "

        End If
        'Response.Write(sSql)
        Dim rsBankTran As SqlDataReader = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)

        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Shares", Type.GetType("System.Decimal"))
        dt.Columns.Add("Price", Type.GetType("System.Decimal"))
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"))
        dt.Columns.Add("OutstandingShares", Type.GetType("System.Decimal"))
        dt.Columns.Add("CostBasis", Type.GetType("System.Decimal"))
        dt.Columns.Add("AvgPrice", Type.GetType("System.Decimal"))
        Dim currentIndex As Integer = -1
        While rsBankTran.Read
            If rsBankTran("TransCat").ToString().ToLower.Trim = "begbal" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount")
                dr("OutstandingShares") = rsBankTran("Quantity")
                dr("CostBasis") = rsBankTran("NetAmount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf currentIndex = -1 Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = 0.0
                dr("Price") = 0.0
                dr("Amount") = 0.0
                dr("OutstandingShares") = 0.0
                dr("CostBasis") = 0.0
                dr("AvgPrice") = 0.0
                dt.Rows.Add(dr)
            End If
            If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity") * -1
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                dt.Rows.Add(dr)
            End If
        End While
        If currentIndex < 0 Then
            Return 0.0
        Else
            Return Math.Round(dt.Rows(currentIndex)("AvgPrice"), 2)
        End If
    End Function

    Private Sub GetCostBasisPrice(ByVal iYear As Integer)

        Dim sFY_BegDate As String
        Dim sFY_EndDate As String
        sFY_BegDate = "05/01/" & Trim(Str(iYear))
        sFY_EndDate = "04/30/" & Trim(Str(iYear + 1))
        Dim sSql As String = "select Distinct M.BankID,CONVERT(date, C.BegDate) as TransDate,'begbal' as TransType, 'begbal' as TransCat,M.Symbol as Ticker,ISNUll(C.OutStdShares,0.0) as Quantity,0.0 as Price, ISNUll(C.CostBasis,0.0) as NetAmount,ISNUll(C.AvgPrice,0.0) as AvgPrice,T.Name as TickerName from MarketValue M  Inner Join Ticker T ON T.Ticker = M.Symbol AND (T.Class not in ('MMK') or T.Class is NULL) Left Join CostBasisBalances C ON M.Symbol = C.Ticker  and  C.BankID=" & ddlbank.SelectedValue & "  and C.BegDate='" & sFY_BegDate.Trim & "' where M.BankID=" & ddlbank.SelectedValue & " and M.Date Between '04/30/" & Trim(Str(iYear - 1)) & "' AND '04/30/" & Trim(Str(iYear + 1)) & "'  UNION ALL "
        sSql = sSql & " SELECT B.BankID, CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Ticker, B.Quantity, B.Price, B.NetAmount, B.AvgPrice,T.Name as TickerName FROM BrokTrans B  Left Join Ticker T ON T.Ticker = B.Ticker  WHERE B.assetClass not in ('MMK','Cash') AND BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype"
        'Response.Write(sSql)

        Dim totShare As Decimal = 0.0
        Dim TotAmount As Decimal = 0.0
        Dim Price As Decimal = 0.0
        Dim dt As DataTable = New DataTable()
        Dim dr As DataRow
        dt.Columns.Add("Shares", Type.GetType("System.Decimal"))
        dt.Columns.Add("Price", Type.GetType("System.Decimal"))
        dt.Columns.Add("Amount", Type.GetType("System.Decimal"))
        dt.Columns.Add("OutstandingShares", Type.GetType("System.Decimal"))
        dt.Columns.Add("CostBasis", Type.GetType("System.Decimal"))
        dt.Columns.Add("AvgPrice", Type.GetType("System.Decimal"))
        Dim currentIndex As Integer = -1
        Dim CurrTicker As String = ""
        Dim SqlConstruct As String = ""
        Dim cnt As Integer = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select COUNT(Ticker) FROM CostBasisBalances WHERE BankID =" & ddlbank.SelectedValue & " AND EndDate='04/30/" & Trim(Str(iYear + 1)) & "'")
        Dim rsBankTran As SqlDataReader = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
        While rsBankTran.Read
            If CurrTicker.Trim.ToLower <> rsBankTran("Ticker").ToString().Trim.ToLower Then
                If CurrTicker <> "" And currentIndex >= 0 Then
                    If TotAmount > 0 And cnt = 0 Then
                        SqlConstruct = SqlConstruct & " INSERT INTO CostBasisBalances(Ticker, BankID, EndDate, BegDate, OutStdShares, CostBasis, AvgPrice) VALUES ('" & CurrTicker & "'," & ddlbank.SelectedValue & ",'04/30/" & Trim(Str(iYear + 1)) & "','05/01/" & Trim(Str(iYear + 1)) & "'," & Math.Round(totShare, 4) & "," & Math.Round(TotAmount, 2) & "," & Math.Round(Price, 2) & ");"
                    ElseIf TotAmount > 0 Then
                        SqlConstruct = SqlConstruct & " Update CostBasisBalances SET OutStdShares=" & Math.Round(totShare, 4) & ", CostBasis=" & Math.Round(TotAmount, 2) & ", AvgPrice=" & Math.Round(Price, 2) & " WHERE Ticker='" & CurrTicker & "' AND BankID=" & ddlbank.SelectedValue & " AND EndDate='04/30/" & Trim(Str(iYear + 1)) & "';"
                    End If
                    currentIndex = -1
                    dt.Clear()
                    totShare = 0.0
                    TotAmount = 0.0
                    Price = 0.0
                End If
            End If
            CurrTicker = rsBankTran("Ticker").ToString().Trim
            If rsBankTran("TransCat").ToString().ToLower.Trim = "begbal" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount")
                dr("OutstandingShares") = rsBankTran("Quantity")
                dr("CostBasis") = rsBankTran("NetAmount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            ElseIf currentIndex = -1 Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = 0.0
                dr("Price") = 0.0
                dr("Amount") = 0.0
                dr("OutstandingShares") = 0.0
                dr("CostBasis") = 0.0
                dr("AvgPrice") = 0.0
                dt.Rows.Add(dr)
            End If
            If rsBankTran("TransCat").ToString().ToLower.Trim = "reinvest" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("Quantity"), rsBankTran("Quantity") * -1)
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "buy" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = IIf(rsBankTran("NetAmount") < 0, rsBankTran("NetAmount") * -1, rsBankTran("NetAmount"))
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity") * -1
                dr("Price") = rsBankTran("AvgPrice")
                dr("Amount") = rsBankTran("NetAmount") * -1
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferin" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = GetAvgPrice(0, CurrTicker, rsBankTran("Quantity") * -1, rsBankTran("TransDate"), sFY_BegDate, sFY_EndDate)
                dr("Amount") = dr("Price") * dr("Shares")
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            ElseIf rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" Then
                currentIndex = currentIndex + 1
                dr = dt.NewRow()
                dr("Shares") = rsBankTran("Quantity")
                dr("Price") = dt.Rows(currentIndex - 1)("AvgPrice")
                dr("Amount") = dr("Price") * dr("Shares")
                dr("OutstandingShares") = dt.Rows(currentIndex - 1)("OutstandingShares") + dr("Shares")
                dr("CostBasis") = dt.Rows(currentIndex - 1)("CostBasis") + dr("Amount")
                If dr("OutstandingShares") = 0 Then
                    dr("AvgPrice") = 0
                Else
                    dr("AvgPrice") = dr("CostBasis") / dr("OutstandingShares")
                End If
                totShare = totShare + dr("Shares")
                TotAmount = TotAmount + dr("Amount")
                If totShare = 0 Then
                    Price = 0
                Else
                    Price = TotAmount / totShare
                End If
                dt.Rows.Add(dr)
            End If
        End While
        'If  And TotAmount > 0 Then
        If CurrTicker <> "" And currentIndex >= 0 And TotAmount > 0 And cnt = 0 Then
            SqlConstruct = SqlConstruct & " INSERT INTO CostBasisBalances(Ticker, BankID, EndDate, BegDate, OutStdShares, CostBasis, AvgPrice) VALUES ('" & CurrTicker & "'," & ddlbank.SelectedValue & ",'04/30/" & Trim(Str(iYear + 1)) & "','05/01/" & Trim(Str(iYear + 1)) & "'," & Math.Round(totShare, 4) & "," & Math.Round(TotAmount, 2) & "," & Math.Round(Price, 2) & ");"
        ElseIf CurrTicker <> "" And currentIndex >= 0 And TotAmount > 0 Then
            SqlConstruct = SqlConstruct & " Update CostBasisBalances SET OutStdShares=" & Math.Round(totShare, 4) & ", CostBasis=" & Math.Round(TotAmount, 2) & ", AvgPrice=" & Math.Round(Price, 2) & " WHERE Ticker='" & CurrTicker & "' AND BankID=" & ddlbank.SelectedValue & " AND EndDate='04/30/" & Trim(Str(iYear + 1)) & "';"
        End If
        If currentIndex > -1 Then
            SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, SqlConstruct)
            'Response.Write(SqlConstruct)
            lblErr.Text = "Values inserted Successfully"
        End If
    End Sub

    'No need this block
    Public Sub CreateCostBasisExcelFile1(ByVal iYear As Integer)
        '    Try
        '        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        '        Dim oSheet As IWorksheet
        '        oSheet = oWorkbooks.Worksheets.Add()
        '        Dim rsBankTran As SqlDataReader

        '        Dim FileName As String = "Cost_Basis_Reports_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"


        '        Dim sSql As String
        '        ' rows count
        '        Dim CurrentRow As Long
        '        Dim sMonth As String
        '        Dim sFY_BegDate As String
        '        Dim sFY_EndDate As String
        '        Dim CurrentMonth As String
        '        Dim CurrentSymbol As String
        '        Dim SymbolStartRow1 As Long
        '        Dim SymbolStartRow2 As Long
        '        Dim SymbolStartRow3 As Long
        '        Dim SymbolStartRow4 As Long

        '        ' cost basic accounting average buy values
        '        Dim AvgQty1 As Double
        '        Dim AvgAmt1 As Double
        '        Dim AvgQty2 As Double
        '        Dim AvgAmt2 As Double
        '        Dim AvgQty3 As Double
        '        Dim AvgAmt3 As Double
        '        Dim AvgQty4 As Double
        '        Dim AvgAmt4 As Double
        '        Dim NetQty As Double
        '        Dim NetAmount As Object

        '        'grand totals STRING
        '        Dim GrandQty1 As String
        '        Dim GrandAmt1 As String
        '        Dim GrandQty2 As String
        '        Dim GrandAmt2 As String
        '        Dim GrandQty3 As String
        '        Dim GrandAmt3 As String
        '        Dim GrandQty4 As String
        '        Dim GrandAmt4 As String

        '        ' cell values
        '        Dim DONATIONS As Double
        '        Dim TRANSFERIN As Double
        '        Dim MMFDIVCASH As Double
        '        Dim STKDIVCASH As Double
        '        Dim TRANSFEROUT As Double
        '        Dim GRANTS As Double
        '        Dim EXPENSE As Double
        '        Dim AccountNum As String


        '        oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION "
        '        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        '        oSheet.Range("A1").Font.Bold = True
        '        oSheet.Range("A2").Value = "Investment Portfolio as of :"
        '        oSheet.Range("A2").Font.Bold = True
        '        'oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
        '        'oSheet.Range("A3").Font.Bold = True



        '        sFY_EndDate = "04/30/" & Trim(Str(iYear + 1))
        '        Dim CRange As IRange
        '        CRange = oSheet.Range("B2")
        '        CRange.Formula = "04/30/" & Trim(Str(iYear + 1))
        '        CRange.NumberFormat = "m/d/yyyy"

        '        oSheet.Range("A5:A6").Merge()
        '        oSheet.Range("A5").Value = "Investment"

        '        oSheet.Range("B5:D5").Merge()
        '        oSheet.Range("B5").Value = "Scholarship A/C"
        '        oSheet.Range("E5:G5").Merge()
        '        oSheet.Range("E5").Value = "General A/C"
        '        oSheet.Range("H5:J5").Merge()
        '        oSheet.Range("H5").Value = "Role Models"
        '        oSheet.Range("K5:M5").Merge()
        '        oSheet.Range("K5").Value = "Donor Advised"
        '        oSheet.Range("N5:N6").Merge()
        '        oSheet.Range("N5").Value = "Remarks"
        '        oSheet.Range("O5:P5").Merge()
        '        oSheet.Range("O5").Value = "Total"

        '        oSheet.Range("B6").Value = "Shares"
        '        oSheet.Range("C6").Value = "Amount"
        '        oSheet.Range("D6").Value = "Date"
        '        oSheet.Range("E6").Value = "Shares"
        '        oSheet.Range("F6").Value = "Amount"
        '        oSheet.Range("G6").Value = "Date"
        '        oSheet.Range("H6").Value = "Shares"
        '        oSheet.Range("I6").Value = "Amount"
        '        oSheet.Range("J6").Value = "Date"
        '        oSheet.Range("K6").Value = "Shares"
        '        oSheet.Range("L6").Value = "Amount"
        '        oSheet.Range("M6").Value = "Date"
        '        oSheet.Range("O6").Value = "Shares"
        '        oSheet.Range("P6").Value = "Amount"
        '        oSheet.Range("A6:P6").Font.Bold = True
        '        oSheet.Range("A5:P5").Font.Bold = True
        '        oSheet.Range("A5:P5").HorizontalAlignment = XlHAlign.xlHAlignCenter
        '        oSheet.Range("A5:P5").VerticalAlignment = XlVAlign.xlVAlignCenter
        '        sSql = "SELECT B.BrokTransID, B.BankID, B.BankCode,CONVERT(date, B.TransDate) as TransDate, B.TransType, ISNUll(B.TransCat,'') as TransCat, B.Medium, B.AssetClass, B.Ticker, B.Quantity, B.Price, B.NetAmount, B.Comm, B.TranID, B.ExpCheckNo, B.DepCheckNo, B.Description, B.OutstdShares, B.CostBasis, B.AvgPrice, B.Disposed_Shares, B.Disposed_Price, B.Disposed_Basis, B.Capital_Gains,ISNull(T.Name,'') as TickerName FROM BrokTrans B  Left Join Ticker T ON T.Ticker = B.Ticker WHERE B.assetClass not in ('MMK','Cash') AND BANKID=" & ddlbank.SelectedValue & " AND ((B.TransCat = 'Reinvest' and B.TransType = 'TRN') OR (B.TransCat = 'Buy' and B.TransType = 'Buy') OR ( B.TransCat = 'Sell' and B.TransType = 'Sell') OR (B.TransCat = 'TransferIn' and B.TransType = 'TRN') OR (B.TransCat = 'TransferOut' and B.TransType = 'TRN')) AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' ORDER BY B.ticker,B.TransDate,B.transtype "
        '        'open TWD transactions
        '        rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)


        '        Dim flag As Boolean = False

        '        'start row for Invesment Porfolio is 8
        '        CurrentRow = 7
        '        SymbolStartRow1 = 7
        '        SymbolStartRow2 = 7
        '        SymbolStartRow3 = 7
        '        SymbolStartRow4 = 7
        '        'initilize grand total strings
        '        GrandQty1 = "="
        '        GrandAmt1 = "="
        '        GrandQty2 = "="
        '        GrandAmt2 = "="
        '        GrandQty3 = "="
        '        GrandAmt3 = "="
        '        GrandQty4 = "="
        '        GrandAmt4 = "="

        '        CurrentSymbol = "SWAMY123"


        '        While rsBankTran.Read()

        '            If CurrentSymbol <> Trim(rsBankTran("Ticker")) Then

        '                ' Symbol total Row
        '                If SymbolStartRow1 < SymbolStartRow2 Then
        '                    SymbolStartRow1 = SymbolStartRow2
        '                Else
        '                    SymbolStartRow2 = SymbolStartRow1
        '                End If

        '                If SymbolStartRow1 < SymbolStartRow3 Then
        '                    SymbolStartRow1 = SymbolStartRow3
        '                    SymbolStartRow2 = SymbolStartRow3
        '                Else
        '                    SymbolStartRow3 = SymbolStartRow1
        '                End If

        '                If SymbolStartRow1 < SymbolStartRow4 Then
        '                    SymbolStartRow1 = SymbolStartRow4
        '                    SymbolStartRow2 = SymbolStartRow4
        '                    SymbolStartRow3 = SymbolStartRow4
        '                Else
        '                    SymbolStartRow4 = SymbolStartRow1
        '                End If

        '                If SymbolStartRow1 <> 7 Then
        '                    ' total row
        '                    SymbolStartRow1 = SymbolStartRow1 + 1
        '                    SymbolStartRow2 = SymbolStartRow2 + 1
        '                    SymbolStartRow3 = SymbolStartRow3 + 1
        '                    SymbolStartRow4 = SymbolStartRow4 + 1

        '                    'grand total strings
        '                    GrandQty1 = GrandQty1 & "C" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandAmt1 = GrandAmt1 & "D" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandQty2 = GrandQty2 & "F" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandAmt2 = GrandAmt2 & "G" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandQty3 = GrandQty3 & "I" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandAmt3 = GrandAmt3 & "J" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandQty4 = GrandQty4 & "L" & Trim(Str(SymbolStartRow1)) & "+"
        '                    GrandAmt4 = GrandAmt4 & "M" & Trim(Str(SymbolStartRow1)) & "+"

        '                    CRange = oSheet.Range("A" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "     Sub Total"

        '                    CRange = oSheet.Range("C" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(C" & Trim(Str(CurrentRow)) & ":C" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '                    CRange = oSheet.Range("D" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(D" & Trim(Str(CurrentRow)) & ":D" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                    CRange = oSheet.Range("F" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(F" & Trim(Str(CurrentRow)) & ":F" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '                    CRange = oSheet.Range("G" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(G" & Trim(Str(CurrentRow)) & ":G" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                    CRange = oSheet.Range("I" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(I" & Trim(Str(CurrentRow)) & ":I" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '                    CRange = oSheet.Range("J" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(J" & Trim(Str(CurrentRow)) & ":J" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                    CRange = oSheet.Range("L" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(L" & Trim(Str(CurrentRow)) & ":L" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '                    CRange = oSheet.Range("M" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = "=SUM(M" & Trim(Str(CurrentRow)) & ":M" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                    ' blank row after each Symbol
        '                    SymbolStartRow1 = SymbolStartRow1 + 1
        '                    SymbolStartRow2 = SymbolStartRow2 + 1
        '                    SymbolStartRow3 = SymbolStartRow3 + 1
        '                    SymbolStartRow4 = SymbolStartRow4 + 1
        '                    'reset average values
        '                    AvgQty1 = 0
        '                    AvgAmt1 = 0
        '                    AvgQty2 = 0
        '                    AvgAmt2 = 0
        '                    AvgQty3 = 0
        '                    AvgAmt3 = 0
        '                    AvgQty4 = 0
        '                    AvgAmt4 = 0
        '                    NetAmount = 0
        '                End If

        '                CurrentSymbol = Trim(rsBankTran("Ticker"))
        '                CurrentRow = SymbolStartRow1 + 1
        '                CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
        '                CRange.Formula = Trim(rsBankTran("TickerName")) & " (" & Trim(rsBankTran("Ticker")) & ")"

        '            End If

        '            AccountNum = rsBankTran("BankID")
        '            Select Case AccountNum
        '                Case "4"
        '                    SymbolStartRow1 = SymbolStartRow1 + 1
        '                    'Date
        '                    CRange = oSheet.Range("B" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = Format(rsBankTran("Transdate"), "mm/dd/yyyy")
        '                    ' if sell get avg. buy cost
        '                    If rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" Or rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Or rsBankTran("TransCat").ToString().ToLower.Trim = "SCC MERGER OUT" Then
        '                        NetQty = rsBankTran("Quantity") * -1
        '                        If AvgQty1 <> 0 Then
        '                            NetAmount = NetQty * (AvgAmt1 / AvgQty1)
        '                        Else
        '                            NetAmount = 0
        '                        End If
        '                    Else
        '                        NetQty = rsBankTran("Quantity")
        '                        NetAmount = rsBankTran("NetAmount") * -1
        '                        AvgAmt1 = AvgAmt1 + NetAmount
        '                        AvgQty1 = AvgQty1 + NetQty
        '                    End If
        '                    'Net Qty
        '                    CRange = oSheet.Range("C" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = NetQty
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '                    'Net amount
        '                    CRange = oSheet.Range("D" & Trim(Str(SymbolStartRow1)))
        '                    CRange.Formula = NetAmount
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                Case "5"
        '                    SymbolStartRow2 = SymbolStartRow2 + 1
        '                    'Date
        '                    CRange = oSheet.Range("E" & Trim(Str(SymbolStartRow2)))
        '                    CRange.Formula = Format(rsBankTran("Transdate"), "mm/dd/yyyy")
        '                    ' if sell get avg. buy cost
        '                    If rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" Or rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Or rsBankTran("TransCat").ToString().ToLower.Trim = "SCC MERGER OUT" Then
        '                        NetQty = rsBankTran("Quantity") * -1
        '                        If AvgQty2 <> 0 Then
        '                            NetAmount = NetQty * (AvgAmt2 / AvgQty2)
        '                        Else
        '                            NetAmount = 0
        '                        End If
        '                    Else
        '                        NetQty = rsBankTran("Quantity")
        '                        NetAmount = rsBankTran("NetAmount") * -1
        '                        AvgAmt2 = AvgAmt2 + NetAmount
        '                        AvgQty2 = AvgQty2 + NetQty
        '                    End If
        '                    'Net Qty
        '                    CRange = oSheet.Range("F" & Trim(Str(SymbolStartRow2)))
        '                    CRange.Formula = NetQty
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '                    'Net amount
        '                    CRange = oSheet.Range("G" & Trim(Str(SymbolStartRow2)))
        '                    CRange.Formula = NetAmount
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                Case "6"
        '                    SymbolStartRow3 = SymbolStartRow3 + 1
        '                    'Date
        '                    CRange = oSheet.Range("H" & Trim(Str(SymbolStartRow3)))
        '                    CRange.Formula = Format(rsBankTran("Transdate"), "mm/dd/yyyy")
        '                    ' if sell get avg. buy cost
        '                    If rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" Or rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Or rsBankTran("TransCat").ToString().ToLower.Trim = "SCC MERGER OUT" Then
        '                        NetQty = rsBankTran("Quantity") * -1
        '                        If AvgQty3 <> 0 Then
        '                            NetAmount = NetQty * (AvgAmt3 / AvgQty3)
        '                        Else
        '                            NetAmount = 0
        '                        End If
        '                    Else
        '                        NetQty = rsBankTran("Quantity")
        '                        NetAmount = rsBankTran("NetAmount") * -1
        '                        AvgAmt3 = AvgAmt3 + NetAmount
        '                        AvgQty3 = AvgQty3 + NetQty
        '                    End If
        '                    'Net Qty
        '                    CRange = oSheet.Range("I" & Trim(Str(SymbolStartRow3)))
        '                    CRange.Formula = NetQty
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '                    'Net amount
        '                    CRange = oSheet.Range("J" & Trim(Str(SymbolStartRow3)))
        '                    CRange.Formula = NetAmount
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '                Case "7"
        '                    SymbolStartRow4 = SymbolStartRow4 + 1
        '                    'Date
        '                    CRange = oSheet.Range("K" & Trim(Str(SymbolStartRow4)))
        '                    CRange.Formula = Format(rsBankTran("Transdate"), "mm/dd/yyyy")
        '                    ' if sell get avg. buy cost
        '                    If rsBankTran("TransCat").ToString().ToLower.Trim = "transferout" Or rsBankTran("TransCat").ToString().ToLower.Trim = "sell" Or rsBankTran("TransCat").ToString().ToLower.Trim = "SCC MERGER OUT" Then
        '                        NetQty = rsBankTran("Quantity") * -1
        '                        If AvgQty4 <> 0 Then
        '                            NetAmount = NetQty * (AvgAmt4 / AvgQty4)
        '                        Else
        '                            NetAmount = 0
        '                        End If
        '                    Else
        '                        NetQty = rsBankTran("Quantity")
        '                        If rsBankTran("TransCat") = "STOCK DONATION" Then
        '                            NetAmount = rsBankTran("NetAmount")
        '                        Else
        '                            NetAmount = rsBankTran("NetAmount") * -1
        '                        End If
        '                        AvgAmt4 = AvgAmt4 + NetAmount
        '                        AvgQty4 = AvgQty4 + NetQty
        '                    End If
        '                    'Net Qty
        '                    CRange = oSheet.Range("L" & Trim(Str(SymbolStartRow4)))
        '                    CRange.Formula = NetQty
        '                    CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '                    'Net amount
        '                    CRange = oSheet.Range("M" & Trim(Str(SymbolStartRow4)))
        '                    CRange.Formula = NetAmount
        '                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
        '            End Select

        '        End While


        '        ' Symbol total Row
        '        If SymbolStartRow1 < SymbolStartRow2 Then
        '            SymbolStartRow1 = SymbolStartRow2
        '        Else
        '            SymbolStartRow2 = SymbolStartRow1
        '        End If

        '        If SymbolStartRow1 < SymbolStartRow3 Then
        '            SymbolStartRow1 = SymbolStartRow3
        '            SymbolStartRow2 = SymbolStartRow3
        '        Else
        '            SymbolStartRow3 = SymbolStartRow1
        '        End If

        '        If SymbolStartRow1 < SymbolStartRow4 Then
        '            SymbolStartRow1 = SymbolStartRow4
        '            SymbolStartRow2 = SymbolStartRow4
        '            SymbolStartRow3 = SymbolStartRow4
        '        Else
        '            SymbolStartRow4 = SymbolStartRow1
        '        End If

        '        ' last symbol row totals
        '        ' total row
        '        SymbolStartRow1 = SymbolStartRow1 + 1
        '        SymbolStartRow2 = SymbolStartRow2 + 1
        '        SymbolStartRow3 = SymbolStartRow3 + 1
        '        SymbolStartRow4 = SymbolStartRow4 + 1

        '        'grand total strings
        '        GrandQty1 = GrandQty1 & "C" & Trim(Str(SymbolStartRow1))
        '        GrandAmt1 = GrandAmt1 & "D" & Trim(Str(SymbolStartRow1))
        '        GrandQty2 = GrandQty2 & "F" & Trim(Str(SymbolStartRow1))
        '        GrandAmt2 = GrandAmt2 & "G" & Trim(Str(SymbolStartRow1))
        '        GrandQty3 = GrandQty3 & "I" & Trim(Str(SymbolStartRow1))
        '        GrandAmt3 = GrandAmt3 & "J" & Trim(Str(SymbolStartRow1))
        '        GrandQty4 = GrandQty4 & "L" & Trim(Str(SymbolStartRow1))
        '        GrandAmt4 = GrandAmt4 & "M" & Trim(Str(SymbolStartRow1))

        '        CRange = oSheet.Range("A" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "     Sub Total"

        '        CRange = oSheet.Range("C" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(C" & Trim(Str(CurrentRow)) & ":C" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '        CRange = oSheet.Range("D" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(D" & Trim(Str(CurrentRow)) & ":D" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '        CRange = oSheet.Range("F" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(F" & Trim(Str(CurrentRow)) & ":F" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '        CRange = oSheet.Range("G" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(G" & Trim(Str(CurrentRow)) & ":G" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '        CRange = oSheet.Range("I" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(I" & Trim(Str(CurrentRow)) & ":I" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '        CRange = oSheet.Range("J" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(J" & Trim(Str(CurrentRow)) & ":J" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '        CRange = oSheet.Range("L" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(L" & Trim(Str(CurrentRow)) & ":L" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"

        '        CRange = oSheet.Range("M" & Trim(Str(SymbolStartRow1)))
        '        CRange.Formula = "=SUM(M" & Trim(Str(CurrentRow)) & ":M" & Trim(Str(SymbolStartRow1 - 1)) & ")"
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '        ' update colum totals
        '        CRange = oSheet.Range("C" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandQty1      '"=SUM(C8:C" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '        CRange = oSheet.Range("D" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandAmt1     '"=SUM(D8:D" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
        '        CRange = oSheet.Range("F" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandQty2     '"=SUM(F8:F" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '        CRange = oSheet.Range("G" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandAmt2     '"=SUM(G8:G" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
        '        CRange = oSheet.Range("I" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandQty3     '"=SUM(I8:I" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '        CRange = oSheet.Range("J" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandAmt3     '"=SUM(J8:J" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
        '        CRange = oSheet.Range("L" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandQty4     '"=SUM(L8:L" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '        CRange = oSheet.Range("M" & Trim(Str(SymbolStartRow1 + 2)))
        '        CRange.Formula = GrandAmt4     '"=SUM(M8:M" & Trim(Str(SymbolStartRow1)) & ")"
        '        CRange.Font.Bold = True
        '        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        '        ' update ROW totals
        '        Dim RN As String
        '        Dim i As Integer = 0
        '        For i = 8 To SymbolStartRow1
        '            '=SUM(C8,F8,I8,L8)
        '            RN = Trim(Str(i))
        '            CRange = oSheet.Range("O" & RN)
        '            CRange.Formula = "=C" & Trim(RN) & "+F" & Trim(RN) & "+I" & Trim(RN) & "+L" & Trim(RN)
        '            CRange.NumberFormat = "#,##0.000_);(#,##0.000)"
        '            CRange = oSheet.Range("P" & Trim(RN))
        '            CRange.Formula = "=D" & Trim(RN) & "+G" & Trim(RN) & "+J" & Trim(RN) & "+M" & Trim(RN)
        '            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
        '        Next



        '        ' save the worksheet
        '        'Stream workbook 
        '        oSheet.Name = ddlbank.SelectedItem.Text
        '        Response.Clear()
        '        Response.ContentType = "application/vnd.ms-excel"
        '        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        '        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        '        oWorkbooks.SaveAs(Response.OutputStream)
        '        Response.End()


        '    Catch ex As Exception
        '        Response.Write(ex.ToString())
        '    End Try
    End Sub

    Public Sub CreateGrantsExcelFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "External_Audit_Report_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"
            oSheet.Range("A2:E2").MergeCells = True
            oSheet.Range("A2").Value = "NORTH SOUTH FOUNDATION"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A3:E3").MergeCells = True
            oSheet.Range("A3").Value = "2009 Form 990, Part II, Line 22"
            oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A4:E4").MergeCells = True
            oSheet.Range("A4").Value = "Grants and Allocations"
            oSheet.Range("A4").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A5:E5").MergeCells = True
            oSheet.Range("A5").Value = "Tax ID: 36-3659998"
            oSheet.Range("A5").HorizontalAlignment = XlHAlign.xlHAlignCenter


            'oSheet.Range("A1").Font.Bold = True
            'oSheet.Range("A2").Value = "External Audit Report"
            'oSheet.Range("A2").Font.Bold = True
            'oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
            'oSheet.Range("A3").Font.Bold = True
            'oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
            'oSheet.Range("D2").Font.Bold = True
            'oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
            'oSheet.Range("D3").Font.Bold = True
            'oSheet.Range("D3").NumberFormat = "m/d/yyyy"

            ' rows count
            Dim InitialRow As Integer
            Dim CurrentRow As Integer
            Dim sFY_BegDate As String
            Dim sFY_EndDate As String

            sFY_BegDate = "05/01/" & Trim(Str(iYear))
            sFY_EndDate = "04/30/" & Trim(Str(iYear + 1)) & " 23:59 "
            Dim sSql As String = "select  CONVERT(date, B.TransDate) as TransDate,E.ExpenseAmount as Amount ,EC.AccountName as Purpose, CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as ReimbursedTo from ExpJournal E Inner Join BankTrans B On B.BankID = E.BanKID and B.CheckNumber = E.CheckNumber Left Join ExpenseCategory EC ON EC.ExpCatID = E.ExpCatID  Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse II ON E.DonorType <> 'OWN' AND E.ReimbMemberid = II.AutoMemberID   where E.TransType ='Grants' AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & "'"
            sSql = sSql & "Union All select  CONVERT(date, B.TransDate) as TransDate,E.ExpenseAmount as Amount,EC.AccountName as Purpose, CASE WHEN E.DonorType = 'OWN' then  O.ORGANIZATION_NAME ELSE II.FirstName +' '+ II.LastName END as ReimbursedTo from ExpJournal E Inner Join BrokTrans B On B.BankID = E.BanKID and B.ExpCheckNo = E.CheckNumber   Left Join ExpenseCategory EC ON EC.ExpCatID = E.ExpCatID  Left Join OrganizationInfo O ON E.DonorType = 'OWN' and O.AutoMemberid =E.ReimbMemberid Left Join IndSpouse II ON E.DonorType <> 'OWN' AND E.ReimbMemberid = II.AutoMemberID  where E.TransType ='Grants' AND B.TransDate BETWEEN '" & sFY_BegDate & "' AND '" & sFY_EndDate & " ' order by TransDate"
            Dim CRange As IRange
            InitialRow = 7
            rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
            oSheet.Range("A" & Str(InitialRow) & "").Value = ""
            oSheet.Range("B" & Str(InitialRow) & "").Value = "Receiving Organization"
            oSheet.Range("C" & Str(InitialRow) & "").Value = "Purpose"
            oSheet.Range("D" & Str(InitialRow) & "").Value = "Date"
            oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"

            Dim flag As Boolean = False
            CurrentRow = InitialRow
            While rsBankTran.Read()
                flag = True
                CurrentRow = CurrentRow + 1
                oSheet.Range("B" & Str(CurrentRow) & "").Value = rsBankTran("ReimbursedTo")
                oSheet.Range("C" & Str(CurrentRow) & "").Value = rsBankTran("Purpose")
                CRange = oSheet.Range("D" & Str(CurrentRow))
                CRange.Value = rsBankTran("TransDate") 'Format(rsBankTran("TransDate"), "mm/dd/yy") '
                CRange.NumberFormat = "MM/DD/YYYY"
                CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
                CRange.Value = rsBankTran("Amount")
                CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
            End While

            If flag = False Then
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Value = "Total"
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

            ' save the worksheet
            'Stream workbook 
            oSheet.Name = ddlbank.SelectedItem.Text
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub CreateCashExcelFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "External_Audit_Report_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"
            oSheet.Range("A2:E2").MergeCells = True
            oSheet.Range("A2").Value = "NORTH SOUTH FOUNDATION"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A3:E3").MergeCells = True
            oSheet.Range("A3").Value = "2009 Form 990, Part II, Line 22"
            oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A4:E4").MergeCells = True
            oSheet.Range("A4").Value = "Grants and Allocations"
            oSheet.Range("A4").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A5:E5").MergeCells = True
            oSheet.Range("A5").Value = "Tax ID: 36-3659998"
            oSheet.Range("A5").HorizontalAlignment = XlHAlign.xlHAlignCenter


            'oSheet.Range("A1").Font.Bold = True
            'oSheet.Range("A2").Value = "External Audit Report"
            'oSheet.Range("A2").Font.Bold = True
            'oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
            'oSheet.Range("A3").Font.Bold = True
            'oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
            'oSheet.Range("D2").Font.Bold = True
            'oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
            'oSheet.Range("D3").Font.Bold = True
            'oSheet.Range("D3").NumberFormat = "m/d/yyyy"

            ' rows count
            Dim InitialRow As Integer
            Dim CurrentRow As Integer
            Dim sFY_BegDate As String = ""
            Dim sFY_EndDate As String = ""
            Dim curBankID As String = 0
            Dim Sql As String = "select distinct top 2 CONVERT(VARCHAR(10), EndDate, 101),EndDate from BankBalances where year(Enddate) <= " & Str(iYear + 1) & " order by EndDate desc"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, Sql)
            If ds.Tables(0).Rows.Count > 1 Then
                sFY_BegDate = ds.Tables(0).Rows(0)(0)
                sFY_EndDate = ds.Tables(0).Rows(1)(0)
            ElseIf ds.Tables(0).Rows.Count = 1 Then
                sFY_BegDate = ds.Tables(0).Rows(0)(0)
                sFY_EndDate = "1/1/1990"
            Else
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            Dim sSql As String = ""
            sSql = sSql & "select BB.BankID,BB.Amount,B.BankName,CONVERT(VARCHAR(10), BB.EndDate, 101) as EndDate from BankBalances BB Inner Join Bank B on B.BankID=BB.BankID where BB.EndDate in ('" & sFY_BegDate & "','" & sFY_EndDate & "') order by BB.BankID"
            Dim CRange As IRange
            InitialRow = 7
            rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
            Response.Write(Sql)
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
            oSheet.Range("A" & Str(InitialRow) & "").Value = ""
            oSheet.Range("B" & Str(InitialRow) & "").Value = "Account Name"
            oSheet.Range("C" & Str(InitialRow) & "").Value = sFY_BegDate
            oSheet.Range("D" & Str(InitialRow) & "").Value = IIf(sFY_EndDate = "1/1/1990", "", sFY_EndDate)
            'oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"

            Dim flag As Boolean = False
            CurrentRow = InitialRow
            While rsBankTran.Read()
                flag = True
                If curBankID <> rsBankTran("BankID") Then
                    CurrentRow = CurrentRow + 1
                    oSheet.Range("B" & Str(CurrentRow) & "").Value = rsBankTran("BankName")
                    curBankID = rsBankTran("BankID")
                End If
                If sFY_BegDate = rsBankTran("EndDate") Then
                    CRange = oSheet.Range("C" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                Else
                    CRange = oSheet.Range("D" & Str(CurrentRow))
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                End If
            End While

            If flag = False Then
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Value = "Total"
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            If Not (sFY_EndDate = "1/1/1990") Then
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            End If

            ' save the worksheet
            'Stream workbook 
            oSheet.Name = ddlbank.SelectedItem.Text
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub


    Public Sub CreateMarketableSecuritiesExcelFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "External_Audit_Report_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"
            oSheet.Range("A2:O2").MergeCells = True
            oSheet.Range("A2").Value = "NORTH SOUTH FOUNDATION"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A3:O3").MergeCells = True
            oSheet.Range("A3").Value = "2009 Form 990, Part II, Line 22"
            oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A4:O4").MergeCells = True
            oSheet.Range("A4").Value = "Investment in Marketable Securities at Cost"
            oSheet.Range("A4").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A5:O5").MergeCells = True
            oSheet.Range("A5").Value = "Tax ID: 36-3659998"
            oSheet.Range("A5").HorizontalAlignment = XlHAlign.xlHAlignCenter


            ' rows count
            Dim InitialRow As Integer
            Dim CurrentRow As Integer
            Dim sFY_BegDate As String = ""
            Dim sFY_EndDate As String = ""
            Dim curTicker As String = ""

            sFY_BegDate = "04/30/" & Trim(Str(iYear))
            sFY_EndDate = "04/30/" & Trim(Str(iYear + 1))

            Dim sSql As String = ""
            sSql = sSql & "SELECT C.Ticker,T.Name,SUM(C.OutStdShares) as Shares, SUM(C.CostBasis) as Amount, CONVERT(VARCHAR(10), C.EndDate, 101)  as EndDate, 0 as BankID FROM CostBasisBalances C INNER JOIN ticker T ON T.Ticker = C.Ticker   WHERE C.EndDate in ('" & sFY_BegDate & "','" & sFY_EndDate & "') GROUP BY C.Ticker,T.Name ,C.EndDate Union All "
            sSql = sSql & "SELECT C.Ticker,T.Name,SUM(C.OutStdShares) as Shares, SUM(C.CostBasis) as Amount, CONVERT(VARCHAR(10), C.EndDate, 101)  as EndDate, C.BankID as BankID FROM CostBasisBalances C INNER JOIN ticker T ON T.Ticker = C.Ticker   WHERE C.EndDate in ('" & sFY_BegDate & "','" & sFY_EndDate & "') GROUP BY C.Ticker,T.Name ,C.EndDate,C.BankID Order By C.Ticker, C.EndDate,6"
            Dim CRange As IRange
            InitialRow = 8
            rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
            oSheet.Range("A" & Str(InitialRow) & "").Value = ""
            oSheet.Range("B" & Str(InitialRow) & "").Value = "Name"
            oSheet.Range("D" & Str(InitialRow - 1) & ":E" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("D" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear - 1) & " - " & Str(iYear)
            oSheet.Range("D" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("D" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("D" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"
            oSheet.Range("F" & Str(InitialRow - 2) & ":G" & Str(InitialRow - 2) & "").MergeCells = True
            oSheet.Range("F" & Str(InitialRow - 2)).Value = "All Accounts"
            oSheet.Range("F" & Str(InitialRow - 2)).Font.Bold = True
            oSheet.Range("F" & Str(InitialRow - 1) & ":G" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("F" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear) & " - " & Str(iYear + 1)
            oSheet.Range("F" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("F" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("F" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("G" & Str(InitialRow) & "").Value = "Amount"

            oSheet.Range("H" & Str(InitialRow - 2) & ":I" & Str(InitialRow - 2) & "").MergeCells = True
            oSheet.Range("H" & Str(InitialRow - 2)).Value = "US Scholarships"
            oSheet.Range("H" & Str(InitialRow - 2)).Font.Bold = True
            oSheet.Range("H" & Str(InitialRow - 2)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("H" & Str(InitialRow - 1) & ":I" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("H" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear) & " - " & Str(iYear + 1)
            oSheet.Range("H" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("H" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("H" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("I" & Str(InitialRow) & "").Value = "Amount"

            oSheet.Range("J" & Str(InitialRow - 2) & ":K" & Str(InitialRow - 2) & "").MergeCells = True
            oSheet.Range("J" & Str(InitialRow - 2)).Value = "General"
            oSheet.Range("J" & Str(InitialRow - 2)).Font.Bold = True
            oSheet.Range("J" & Str(InitialRow - 2)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("J" & Str(InitialRow - 1) & ":K" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("J" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear) & " - " & Str(iYear + 1)
            oSheet.Range("J" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("J" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("J" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("K" & Str(InitialRow) & "").Value = "Amount"

            oSheet.Range("L" & Str(InitialRow - 2) & ":M" & Str(InitialRow - 2) & "").MergeCells = True
            oSheet.Range("L" & Str(InitialRow - 2)).Value = "Endowment"
            oSheet.Range("L" & Str(InitialRow - 2)).Font.Bold = True
            oSheet.Range("L" & Str(InitialRow - 2)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("L" & Str(InitialRow - 1) & ":M" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("L" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear) & " - " & Str(iYear + 1)
            oSheet.Range("L" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("L" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("L" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("M" & Str(InitialRow) & "").Value = "Amount"

            oSheet.Range("N" & Str(InitialRow - 2) & ":O" & Str(InitialRow - 2) & "").MergeCells = True
            oSheet.Range("N" & Str(InitialRow - 2)).Value = "DAF"
            oSheet.Range("N" & Str(InitialRow - 2)).Font.Bold = True
            oSheet.Range("N" & Str(InitialRow - 2)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("N" & Str(InitialRow - 1) & ":O" & Str(InitialRow - 1) & "").MergeCells = True
            oSheet.Range("N" & Str(InitialRow - 1)).Value = "FY - " & Str(iYear) & " - " & Str(iYear + 1)
            oSheet.Range("N" & Str(InitialRow - 1)).HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("N" & Str(InitialRow - 1)).Font.Bold = True
            oSheet.Range("N" & Str(InitialRow) & "").Value = "Share"
            oSheet.Range("O" & Str(InitialRow) & "").Value = "Amount"
            'oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"
            Dim flag As Boolean = False
            CurrentRow = InitialRow
            While rsBankTran.Read()
                flag = True
                If curTicker <> rsBankTran("Ticker") Then
                    CurrentRow = CurrentRow + 1
                    oSheet.Range("B" & Str(CurrentRow) & "").Value = rsBankTran("Name")
                    curTicker = rsBankTran("Ticker")
                End If
                If sFY_BegDate = rsBankTran("EndDate") And rsBankTran("BankID") = 0 Then
                    CRange = oSheet.Range("D" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("E" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                ElseIf rsBankTran("BankID") = 0 Then
                    CRange = oSheet.Range("F" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("G" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                ElseIf rsBankTran("BankID") = 4 Then
                    CRange = oSheet.Range("H" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("I" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                ElseIf rsBankTran("BankID") = 5 Then
                    CRange = oSheet.Range("J" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("K" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                ElseIf rsBankTran("BankID") = 6 Then
                    CRange = oSheet.Range("L" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("M" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                ElseIf rsBankTran("BankID") = 7 Then
                    CRange = oSheet.Range("N" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Shares")
                    CRange.NumberFormat = "#,##0.0000_);(#,##0.0000)"
                    CRange = oSheet.Range("O" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                End If
            End While

            If flag = False Then
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Value = "Total"
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G" & Trim(Str(InitialRow + 1)) & ":G" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

            oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(H" & Trim(Str(InitialRow + 1)) & ":H" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("H" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(I" & Trim(Str(InitialRow + 1)) & ":I" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("I" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(J" & Trim(Str(InitialRow + 1)) & ":J" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("J" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("J" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(K" & Trim(Str(InitialRow + 1)) & ":K" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("K" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("K" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(L" & Trim(Str(InitialRow + 1)) & ":L" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("L" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("L" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(M" & Trim(Str(InitialRow + 1)) & ":M" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("M" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("M" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            oSheet.Range("N" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(N" & Trim(Str(InitialRow + 1)) & ":N" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("N" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("N" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.0000_);(#,##0.0000)"
            oSheet.Range("O" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(O" & Trim(Str(InitialRow + 1)) & ":O" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("O" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("O" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            ' save the worksheet
            'Stream workbook 
            oSheet.Name = ddlbank.SelectedItem.Text.Replace(".", "")
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub CreateInvestmentinMarketableFile(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "Investment_Marketable_" & ddlbank.SelectedItem.Text & "_" & Trim(Str(iYear + 1)) & ".xls"
            oSheet.Range("A2:E2").MergeCells = True
            oSheet.Range("A2").Value = "NORTH SOUTH FOUNDATION"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A3:E3").MergeCells = True
            oSheet.Range("A3").Value = "2009 Form 990, Part II, Line 22"
            oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A4:E4").MergeCells = True
            oSheet.Range("A4").Value = "Investment in Marketable Securities at Cost"
            oSheet.Range("A4").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A5:E5").MergeCells = True
            oSheet.Range("A5").Value = "Tax ID: 36-3659998"
            oSheet.Range("A5").HorizontalAlignment = XlHAlign.xlHAlignCenter


            'oSheet.Range("A1").Font.Bold = True
            'oSheet.Range("A2").Value = "External Audit Report"
            'oSheet.Range("A2").Font.Bold = True
            'oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
            'oSheet.Range("A3").Font.Bold = True
            'oSheet.Range("D2").Value = ddlbank.SelectedItem.Text
            'oSheet.Range("D2").Font.Bold = True
            'oSheet.Range("D3").Value = "04/30/" & Trim(Str(iYear + 1))
            'oSheet.Range("D3").Font.Bold = True
            'oSheet.Range("D3").NumberFormat = "m/d/yyyy"

            ' rows count
            Dim InitialRow As Integer
            Dim CurrentRow As Integer
            Dim sFY_BegDate As String = ""
            Dim sFY_EndDate As String = ""
            Dim curBankID As String = 0
            Dim Sql As String = "select distinct top 2 CONVERT(VARCHAR(10), Date, 101),Date from MarketValue where year(date) <= " & Str(iYear + 1) & " order by Date desc"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, Sql)
            If ds.Tables(0).Rows.Count > 1 Then
                sFY_BegDate = ds.Tables(0).Rows(0)(0)
                sFY_EndDate = ds.Tables(0).Rows(1)(0)
            ElseIf ds.Tables(0).Rows.Count = 1 Then
                sFY_BegDate = ds.Tables(0).Rows(0)(0)
                sFY_EndDate = "1/1/1990"
            Else
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            Dim sSql As String = ""
            sSql = sSql & "select BB.BankID,BB.Amount,B.BankName,CONVERT(VARCHAR(10), BB.EndDate, 101) as EndDate from BankBalances BB Inner Join Bank B on B.BankID=BB.BankID where BB.EndDate in ('" & sFY_BegDate & "','" & sFY_EndDate & "') order by BB.BankID"
            Dim CRange As IRange
            InitialRow = 7
            rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
            Response.Write(Sql)
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
            oSheet.Range("A" & Str(InitialRow) & "").Value = ""
            oSheet.Range("B" & Str(InitialRow) & "").Value = "Account Name"
            oSheet.Range("C" & Str(InitialRow) & "").Value = sFY_BegDate
            oSheet.Range("D" & Str(InitialRow) & "").Value = IIf(sFY_EndDate = "1/1/1990", "", sFY_EndDate)
            'oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"

            Dim flag As Boolean = False
            CurrentRow = InitialRow
            While rsBankTran.Read()
                flag = True
                If curBankID <> rsBankTran("BankID") Then
                    CurrentRow = CurrentRow + 1
                    oSheet.Range("B" & Str(CurrentRow) & "").Value = rsBankTran("BankName")
                    curBankID = rsBankTran("BankID")
                End If
                If sFY_BegDate = rsBankTran("EndDate") Then
                    CRange = oSheet.Range("C" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                Else
                    CRange = oSheet.Range("D" & Str(CurrentRow))
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                End If
            End While

            If flag = False Then
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Value = "Total"
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C" & Trim(Str(InitialRow + 1)) & ":C" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            If Not (sFY_EndDate = "1/1/1990") Then
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
                oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            End If

            ' save the worksheet
            'Stream workbook 
            oSheet.Name = ddlbank.SelectedItem.Text
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub InvestmentsMktValuevsCost(ByVal iYear As Integer)
        Try
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim rsBankTran As SqlDataReader

            Dim FileName As String = "External_Audit_Report_" & ddlbank.SelectedItem.Text.Replace(" ", "").Replace(".", "") & "_" & Trim(Str(iYear + 1)) & ".xls"
            oSheet.Range("A2:G2").MergeCells = True
            oSheet.Range("A2").Value = "NORTH SOUTH FOUNDATION"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A3:G3").MergeCells = True
            oSheet.Range("A3").Value = "2009 Form 990, Part II, Line 22"
            oSheet.Range("A3").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A4:G4").MergeCells = True
            oSheet.Range("A4").Value = "Investment in Marketable Securities at Cost"
            oSheet.Range("A4").HorizontalAlignment = XlHAlign.xlHAlignCenter

            oSheet.Range("A5:G5").MergeCells = True
            oSheet.Range("A5").Value = "Tax ID: 36-3659998"
            oSheet.Range("A5").HorizontalAlignment = XlHAlign.xlHAlignCenter

            'InvestmentsMktValuevsCost
            ' rows count
            Dim InitialRow As Integer
            Dim CurrentRow As Integer
            Dim sFY_BegDate As String = ""
            Dim sFY_EndDate As String = ""
            Dim curBank As String = ""

            sFY_BegDate = "04/30/" & Trim(Str(iYear))
            sFY_EndDate = "04/30/" & Trim(Str(iYear + 1))

            Dim sSql As String = ""
            sSql = sSql & "SELECT SUM(C.CostBasis ) as Amount,B.BankName,B.BankID,'CB' as Source FROM CostBasisBalances C INNER JOIN Bank B ON B.BankID = C.BankID WHERE C.EndDate = '" & sFY_EndDate & "' Group BY B.BankName,B.BankID Union All select SUM(M.MktValue),B.BankName,B.BankID as Amount,'MK' as Source from MarketValue M INNER JOIN Bank B ON B.BankID = M.BankID Inner Join Ticker T ON T.Ticker = M.Symbol AND (T.Class not in ('MMK') or T.Class is NULL) WHERE M.Date = '" & sFY_EndDate & "' Group BY B.BankName,B.BankID Order By B.BankID"
            Dim CRange As IRange
            InitialRow = 8
            rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, sSql)
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").Font.Bold = True
            oSheet.Range("A" & Str(InitialRow) & ":O" & Str(InitialRow) & "").ColumnWidth = 10
            oSheet.Range("A" & Str(InitialRow) & "").Value = ""
            oSheet.Range("B" & Str(InitialRow) & "").Value = "BankName"
            oSheet.Range("D" & Str(InitialRow) & "").Value = "Cost"
            'oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"
            oSheet.Range("F" & Str(InitialRow) & "").Value = "Market Value"
            'oSheet.Range("G" & Str(InitialRow) & "").Value = "Amount"
            'oSheet.Range("E" & Str(InitialRow) & "").Value = "Amount"
            Dim flag As Boolean = False
            CurrentRow = InitialRow
            While rsBankTran.Read()
                flag = True
                If curBank <> rsBankTran("BankID").ToString() Then
                    CurrentRow = CurrentRow + 1
                    oSheet.Range("B" & Str(CurrentRow) & "").Value = rsBankTran("BankName")
                    curBank = rsBankTran("BankID")
                End If
                If rsBankTran("Source").ToString().Trim = "CB" Then
                    CRange = oSheet.Range("D" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    'CRange = oSheet.Range("E" & Str(CurrentRow) & "")
                    'CRange.Value = rsBankTran("Amount")
                    'CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                Else
                    CRange = oSheet.Range("F" & Str(CurrentRow) & "")
                    CRange.Value = rsBankTran("Amount")
                    CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                    'CRange = oSheet.Range("G" & Str(CurrentRow) & "")
                    'CRange.Value = rsBankTran("Amount")
                    'CRange.NumberFormat = "#,##0.00_);(#,##0.00)"
                End If
            End While

            If flag = False Then
                lblErr.Text = "no record to Show"
                Exit Sub
            End If
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Value = "Total"
            oSheet.Range("B" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D" & Trim(Str(InitialRow + 1)) & ":D" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            'oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E" & Trim(Str(InitialRow + 1)) & ":E" & Trim(Str(CurrentRow)) & ")"
            'oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            'oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F" & Trim(Str(InitialRow + 1)) & ":F" & Trim(Str(CurrentRow)) & ")"
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"
            'oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G" & Trim(Str(InitialRow + 1)) & ":G" & Trim(Str(CurrentRow)) & ")"
            'oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
            'oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

            ' save the worksheet
            'Stream workbook 
            oSheet.Name = ddlbank.SelectedItem.Text.Replace(".", "")
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()

        Catch ex As Exception
            Response.Write(ex.ToString())
        End Try
    End Sub
    


    Protected Sub btnGenerate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        If RbtnReport.SelectedValue = "1" Then
            CreateExcelFile(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "2" Then
            CreateCostBasisExcelFile(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "4" Then
            GetCostBasisPrice(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "3" And ddlbank.SelectedValue = "Grants" Then
            CreateGrantsExcelFile(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "3" And ddlbank.SelectedValue = "Cash and Cash Equivalents" Then
            CreateCashExcelFile(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "3" And ddlbank.SelectedValue = "Invest. in Marketable Securities" Then
            CreateMarketableSecuritiesExcelFile(ddlYear.SelectedValue)
        ElseIf RbtnReport.SelectedValue = "3" And ddlbank.SelectedValue = "Investments Mkt Value vs Cost" Then
            InvestmentsMktValuevsCost(ddlYear.SelectedValue)
        Else
            lblErr.Text = "Under Development"
        End If
    End Sub

    Protected Sub BtnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblBalErr.Text = ""
        If BtnAddUpdate.Text = "Add" Then
            If txtBalance.Text.Length < 1 Then
                lblBalErr.Text = "Please Enter beginning balance"
            ElseIf Not IsNumeric(txtBalance.Text) Then
                lblBalErr.Text = "Please Enter numeric values"
            ElseIf (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "select count(BankID) from BankBalances Where  EndDate = '04/30/" & ddlbalYear.SelectedValue & "' and BankID = " & ddlbalBank.SelectedValue & "") = 0) Then
                SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, "Insert into BankBalances ( EndDate, BegDate, BankID, BankCode, Amount,CreatedBy, CreatedDate) values ('04/30/" & ddlbalYear.SelectedValue & "','05/01/" & ddlbalYear.SelectedValue & "'," & ddlbalBank.SelectedValue & ",'" & ddlbalBank.SelectedItem.Text & "'," & txtBalance.Text & "," & Session("LoginID") & ",GetDate())")
                txtBalance.Text = String.Empty
                getdata()
            Else
                lblBalErr.Text = "This values already exist, Please check and update"
                BtnAddUpdate.Text = "update"
            End If
        Else
            SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, "Update BankBalances set Amount=" & txtBalance.Text & ", ModifiedBy=" & Session("LoginID") & ", ModifiedDate=GetDate() Where  EndDate = '04/30/" & ddlbalYear.SelectedValue & "' and BankID = " & ddlbalBank.SelectedValue & "")
            BtnAddUpdate.Text = "Add"
            txtBalance.Text = String.Empty
            getdata()
        End If

    End Sub

    Private Sub getdata()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select B.BankID, B.EndDate,B.BegDate, Bnk.BankCode, B.Amount from BankBalances B Inner Join  Bank bnk ON B.BankID=Bnk.BankID where B.EndDate='04/30/" & ddlbalYear.SelectedValue & "'")
        gvbankbalances.DataSource = ds
        gvbankbalances.DataBind()
    End Sub

    Protected Sub ddlbalYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        getdata()
    End Sub

    Protected Sub BtnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtBalance.Text = String.Empty
        BtnAddUpdate.Text = "Add"
        lblBalErr.Text = ""
    End Sub

    Protected Sub RbtnReport_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadYear(0)
        If RbtnReport.SelectedValue = "3" Then
            ddlbank.Items.Clear()
            ddlbank.Items.Add("Financial Statements")
            ddlbank.Items.Add("Grants")
            ddlbank.Items.Add("US Sch-Endowment")
            ddlbank.Items.Add("Cash and Cash Equivalents")
            ddlbank.Items.Add("Invest. in Marketable Securities")
            ddlbank.Items.Add("Investments Mkt Value vs Cost")
            ddlbank.SelectedIndex = ddlbank.Items.IndexOf(ddlbank.Items.FindByValue("Grants"))
        ElseIf RbtnReport.SelectedValue = "2" Then
            loadbank(" WHERE BANKID in (4,5,6,7) ")
        ElseIf RbtnReport.SelectedValue = "4" Then
            loadbank(" WHERE BANKID in (4,5,6,7) ")
            loadYear(1)
        Else
            loadbank("")
        End If
    End Sub

    Protected Sub BtnInvPositionsUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            SqlHelper.ExecuteNonQuery(Application("Connectionstring"), CommandType.Text, "Update MarketValue set Symbol='" & txtSymbol.Text & "', Quantity=" & txtQuantity.Text & ", Price=" & txtPrice.Text & ", MktValue=" & txtAmount.Text & "  Where MarketValueID=" & hdnMarketValueID.Value & "")
        Catch ex As Exception
            lblInvPositions.Text = "Error in update"
            Exit Sub
        End Try
        getInvPositionsdata()
        lblInvPositions.Text = "Modified Succesfully"
    End Sub

    Protected Sub BtnInvPositions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clearIn()
    End Sub

    Sub clearIn()
        lblInvPositions.Text = String.Empty
        txtAmount.Text = String.Empty
        txtPrice.Text = String.Empty
        txtQuantity.Text = String.Empty
        txtSymbol.Text = String.Empty
        hdnMarketValueID.Value = String.Empty
        trupdate.Visible = False
    End Sub

    Protected Sub gvInvPositions_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvInvPositions.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim MarketValueID As Integer
        MarketValueID = Val(gvInvPositions.DataKeys(index).Value)
        If (MarketValueID) And MarketValueID > 0 Then
            GetSelectedRecord(MarketValueID)
        End If
    End Sub

    Private Sub getInvPositionsdata()
        clearIn()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT MarketValueID, Date, BankID, BankCode, Description, Symbol, Quantity, Price, MktValue FROM MarketValue Where BankID=" & ddlInvPositionsBank.SelectedValue & " and Date='04/30/" & Val(ddlInvPositionsYear.SelectedValue) + 1 & "'")
        gvInvPositions.DataSource = ds
        gvInvPositions.DataBind()
    End Sub

    Private Sub GetSelectedRecord(ByVal MarketValueID As Integer)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT MarketValueID, Symbol, Quantity, Price, MktValue FROM MarketValue Where MarketValueID=" & MarketValueID & "")
        If ds.Tables(0).Rows.Count > 0 Then
            trupdate.Visible = True
            txtAmount.Text = ds.Tables(0).Rows(0)("MktValue")
            txtPrice.Text = ds.Tables(0).Rows(0)("Price")
            txtQuantity.Text = ds.Tables(0).Rows(0)("Quantity")
            txtSymbol.Text = ds.Tables(0).Rows(0)("Symbol")
            hdnMarketValueID.Value = ds.Tables(0).Rows(0)("MarketValueID")
        End If
    End Sub

    Protected Sub ddlInvPositionsBank_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        getInvPositionsdata()
    End Sub

    Protected Sub ddlInvPositionsbalYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        getInvPositionsdata()
    End Sub
End Class
