﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Diagnostics;

namespace VRegistration
{
    public partial class StudentFunctions : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if (Session["LoggedIn"] == null)
                Response.Redirect("MainTest.aspx");
            if ((Session["LoggedIn"] != null && Session["LoggedIn"].ToString() != "True") ||
                (Session["EntryToken"] != null && Session["EntryToken"].ToString() != "Student"))
                Response.Redirect("MainTest.aspx");
            //if (Session["LoginEmail"] != null)
            //Response.Write (Session["LoginEmail"].ToString());
            //Response.Write(Session["Childloginid"].ToString ());
            //Response.Write(Session["ChildPswd"].ToString());

        }
        protected void bttnDownloadPapers_Click(object sender, EventArgs e)
        {
            Response.Redirect("DownloadCoachPapers.aspx");

        }

        protected void lbtnEnterAnswers_Click(object sender, EventArgs e)
        {
            Response.Redirect("ChildTestAnswers.aspx");
        }
    }
}
