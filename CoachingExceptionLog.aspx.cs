﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using Microsoft.ApplicationBlocks.Data;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Script.Serialization;

public partial class CoachingExceptionLog : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            listExceptionFIles();
        }
    }

    public void listExceptionFIles()
    {
        String path = "https://www.northsouth.org/app9/CoachingLog";
        string[] fileArray = Directory.GetFiles(Server.MapPath("~/CoachingLog"));
       // string[] fileArray = Directory.GetFiles(path);
        string tblHtml = string.Empty;
        tblHtml += "<span style='font-weight:bold;'>Table 1: Exception List</span>";
        tblHtml += "<div style='clear:both; margin-bottom:10px;'></div>";
        tblHtml += "<table id='tblExceptions' class='grid' style='border: 1px solid black; border-collapse:collapse;'>";
        tblHtml += "<tr>";
        tblHtml += "<th style='border: 1px solid black; border-collapse:collapse;'>Ser#</th>";
        tblHtml += "<th style='border: 1px solid black; border-collapse:collapse;'>FileName</th>";
        tblHtml += "<th style='border: 1px solid black; border-collapse:collapse;'>Action</th>";
        tblHtml += "</tr>";
        string filename = string.Empty;
        for (int i = 0; i < fileArray.Length; i++)
        {
            filename = fileArray[i].ToString();
            string fileName = System.IO.Path.GetFileName(filename);
            tblHtml += "<tr>";
            tblHtml += "<td style='border: 1px solid black; border-collapse:collapse;'>" + (i + 1) + "</td>";
            tblHtml += "<td style='border: 1px solid black; border-collapse:collapse;'>" + fileName + "</td>";
            tblHtml += "<td style='border: 1px solid black; border-collapse:collapse;'><input type='button' class='btnDownload' value='Download' attr-Name='" + fileName + "'></td>";
            tblHtml += "</tr>";
        }
        tblHtml += "</table>";
        ltrExceptionList.Text = tblHtml;
    }

    protected void btnDownloadConfirm_onClick(object sender, EventArgs e)
    {
        downloadTextFile();
    }

    public void downloadTextFile()
    {
        string pageName = Path.GetFileName(Request.Path);
        string filename = "ZoomMeetingLog_" + DateTime.Now.ToShortDateString().Replace('/', '-') + ".txt";
        string filepath = Server.MapPath("~/CoachingLog/" + hdnFileName.Value);
        FileInfo file = new FileInfo(filepath);

        // Checking if file exists
        if (file.Exists)
        {
            // Clear the content of the response
            Response.ClearContent();

            // LINE1: Add the file name and attachment, which will force the open/cance/save dialog to show, to the header
            Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);

            // Add the file size into the response header
            Response.AddHeader("Content-Length", file.Length.ToString());

            // Set the ContentType
            Response.ContentType = ReturnExtension(file.Extension.ToLower());

            // Write the file into the response (TransmitFile is for ASP.NET 2.0. In ASP.NET 1.1 you have to use WriteFile instead)
            Response.TransmitFile(file.FullName);

            // End the response
            Response.End();
            
        }
    }
    private string ReturnExtension(string fileExtension)
    {
        switch (fileExtension)
        {
            case ".htm":
            case ".html":
            case ".log":
                return "text/HTML";
            case ".txt":
                return "text/plain";
            case ".doc":
                return "application/ms-word";
            case ".tiff":
            case ".tif":
                return "image/tiff";
            case ".asf":
                return "video/x-ms-asf";
            case ".avi":
                return "video/avi";
            case ".zip":
                return "application/zip";
            case ".xls":
            case ".csv":
                return "application/vnd.ms-excel";
            case ".gif":
                return "image/gif";
            case ".jpg":
            case "jpeg":
                return "image/jpeg";
            case ".bmp":
                return "image/bmp";
            case ".wav":
                return "audio/wav";
            case ".mp3":
                return "audio/mpeg3";
            case ".mpg":
            case "mpeg":
                return "video/mpeg";
            case ".rtf":
                return "application/rtf";
            case ".asp":
                return "text/asp";
            case ".pdf":
                return "application/pdf";
            case ".fdf":
                return "application/vnd.fdf";
            case ".ppt":
                return "application/mspowerpoint";
            case ".dwg":
                return "image/vnd.dwg";
            case ".msg":
                return "application/msoutlook";
            case ".xml":
            case ".sdxl":
                return "application/xml";
            case ".xdp":
                return "application/vnd.adobe.xdp+xml";
            default:
                return "application/octet-stream";
        }
    }


}