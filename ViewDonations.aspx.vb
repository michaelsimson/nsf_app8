Imports NorthSouth.BAL
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class ViewDonations
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'If LCase(Session("LoggedIn")) <> "true" Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If
        'If Len(Trim("" & Session("LoginID"))) = 0 Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If
        'If Len(Trim("" & Session("entryToken"))) = 0 Then
        '    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        'End If
        If Not IsPostBack Then
            pnlReport.Visible = False
            If SortExpression Is Nothing Then
                LoadGrid()
            Else
                LoadGrid(SortExpression)
            End If
            If Not Request.QueryString("Id") Is Nothing Then
                pnlReport.Visible = True
                pnlParams.Visible = False
            End If
        End If
        If Not Request.QueryString("Id") Is Nothing Then
            lnkAdd.NavigateUrl = "AddDonation.aspx?Id=" & Request.QueryString("Id") & "&Type=" & Request.QueryString("type")
            hlnkSearch.Visible = True
        Else
            hlnkSearch.Enabled = False
            lnkAdd.Enabled = False
            pnlParams.Visible = True
        End If
        If Session("entryToken").ToString().ToUpper = "VOLUNTEER" Then
            If Session("NavPath") = "Sponsor" Then
                hlnkSearch.NavigateUrl = "search_sponsor.aspx"
            End If
            hlinkParentRegistration.NavigateUrl = "VolunteerFunctions.aspx"
            lnkAdd.Visible = True
        ElseIf Session("entryToken").ToString().ToUpper = "DONOR" Then
            hlinkParentRegistration.NavigateUrl = "DonorFunctions.aspx"
            lnkAdd.Visible = False
            pnlReport.Visible = True
            pnlParams.Visible = False
        End If
    End Sub
    Private Sub LoadGrid(Optional ByVal SortExpresssion As String = "")
        Dim wherecntn As String = ""
        If Request.QueryString("Id") Is Nothing And Not ddlDonorType.SelectedItem.Text = "Both" Then
            wherecntn = " AND a.DonorType='" & ddlDonorType.SelectedValue & "' "
        End If
        Dim objDonation As New Donation
        Dim dsDonation As New DataSet
        Dim strSql As String
        Dim strChapterID As String
        strChapterID = ""
        If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
            strSql = "Select chapterid, chaptercode, state from chapter where "
            If Session("RoleID") = "4" Then
                strSql = strSql & " clusterid in (Select clusterid from chapter where "
                strSql = strSql & " chapterid = " & Session("LoginChapterID") & ") "
            ElseIf Session("RoleID") = "5" Then
                strSql = strSql & " chapterid = " & Session("LoginChapterID") & ""
            End If
            strSql = strSql & " order by state, chaptercode"
            Dim con As New SqlConnection(Application("ConnectionString"))
            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
            While (drNSFChapters.Read())
                If Len(strChapterID) > 0 Then
                    strChapterID = strChapterID + "," + drNSFChapters(0).ToString()
                Else
                    strChapterID = drNSFChapters(0).ToString()
                End If
            End While
        End If

            strSql = "SELECT B.Automemberid,A.MemberID, "
            If Request.QueryString("type") = "OWN" Or ddlDonorType.SelectedValue = "OWN" Then
                strSql = strSql & " Organization_Name + ' ' + last_name 'Name', "
            Else
                strSql = strSql & " firstname + ' ' + lastname 'Name', "
            End If
        strSql = strSql & " amount, transaction_number,DonationID, Convert(Varchar,donationdate,101) donationdate, method, purpose,DonationNumber, DepositDate, DepositSlip, DonationType, chapter.ChapterCode 'Chapter',E.Name as Event ,a.Project,a.suppcode,IsNull(a.EventYear,'') as EventYear"


            If Not Request.QueryString("Id") Is Nothing Then
                strSql = strSql & "  FROM (DonationsInfo a left Join Event E ON A.EventId = E.EventId) Left Outer Join  chapter  on  a.chapterid = chapter.chapterid,"
                If Not Request.QueryString("type") = "OWN" Then
                    strSql = strSql & " IndSpouse b where a.DonorType<>'OWN' AND "
                Else
                    strSql = strSql & " OrganizationInfo b where a.DonorType='OWN' AND "
                End If
                strSql = strSql & "  a.memberid = b.automemberid AND A.MemberID=" & Request.QueryString("Id")

            ElseIf Session("entryToken").ToString().ToUpper = "VOLUNTEER" Then
                strSql = strSql & "  FROM (DonationsInfo a left Join Event E ON A.EventId = E.EventId) Left Outer Join  chapter  on  a.chapterid = chapter.chapterid, "
                If Request.QueryString("type") = "OWN" Or ddlDonorType.SelectedValue = "OWN" Then
                    strSql = strSql & " OrganizationInfo b  where a.DonorType='OWN' AND "
                Else
                    strSql = strSql & " IndSpouse b where a.DonorType<>'OWN' AND"
                End If

                strSql = strSql & " a.memberid = b.automemberid " & wherecntn
            Else
                strSql = strSql & "  FROM (DonationsInfo a left Join Event E ON A.EventId = E.EventId) Left Outer Join  chapter  on  a.chapterid = chapter.chapterid, "
                If Not Request.QueryString("type") = "OWN" Then
                    strSql = strSql & " IndSpouse b where a.DonorType<>'OWN' AND"
                Else
                    strSql = strSql & " OrganizationInfo b  where a.DonorType='OWN' AND  "
                End If
                strSql = strSql & "  a.memberid = b.automemberid AND A.MemberID=" & Session("LoginID")
            End If
            If Session("RoleID") <> "5" Then
                If Len(Session("LoginChapterID")) > 0 Then
                    strSql = strSql & " AND b.ChapterID = " & Session("LoginChapterID")
                    strSql = strSql & " AND a.Anonymous <> 'Yes'"
                End If
            Else
                strSql = strSql & " AND b.ChapterID in ( " & strChapterID & ")"
                strSql = strSql & " AND a.Anonymous <> 'Yes'"
            End If
            If IsDate(txtFromDate.Text) = True Then
                strSql = strSql & " AND donationdate BETWEEN '" & txtFromDate.Text & "' AND '" & txtToDate.Text & "'"
            End If
            If Len(SortExpresssion) = 0 Then
                strSql = strSql & " ORDER BY  Convert(Varchar,donationdate,111) desc "
            Else
                strSql = strSql & " ORDER BY " & SortExpresssion
            End If
        Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim tableName As String() = New String(0) {}
        tableName(0) = "Donation"
        'Response.Write(strSql)
            Try
                SqlHelper.FillDataset(conn, CommandType.Text, strSql, dsDonation, tableName)
                If dsDonation.Tables(0).Rows.Count > 0 Then
                    dgDonationList.Visible = True
                    dgDonationList.DataSource = dsDonation
                    dgDonationList.DataBind()
                    lblAlert.Visible = False
                End If

                If dsDonation.Tables(0).Rows.Count <= 0 Then
                    dgDonationList.Visible = False
                    lblAlert.Visible = True
                    lblAlert.Text = "No donations found."
                End If
            Catch ex As Exception
                lblAlert.Text = strSql
            lblAlert.Text = lblAlert.Text & "<br>" & ex.ToString()
            'Response.Write(ex.ToString())
            End Try
            'lblAlert.Visible = True
            'lblAlert.Text = strSql
    End Sub

    Protected Sub dgDonationList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDonationList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                CType(e.Item.FindControl("hlUpdateDonation"), LinkButton).PostBackUrl = "UpdateDonation.aspx?MemberID=" & e.Item.DataItem("MemberID").ToString & "&DonationID=" & e.Item.DataItem("DonationID").ToString() & "&type=" & Request.QueryString("Type")
                CType(e.Item.FindControl("lbReceipt"), LinkButton).PostBackUrl = "ViewDonationReceipt.aspx?MemberID=" & e.Item.DataItem("MemberID").ToString & "&DonationID=" & e.Item.DataItem("DonationID").ToString() & "&type=" & Request.QueryString("Type") & "&DonationDate=" & e.Item.DataItem("Donationdate").ToString()
                e.Item.Cells(1).Attributes.Add("align", "right")

                If Session("entryToken").ToString().ToUpper = "DONOR" Then
                    CType(e.Item.FindControl("hlUpdateDonation"), LinkButton).Visible = False
                End If
                If Request.QueryString("Id") Is Nothing Then
                    CType(e.Item.FindControl("hlUpdateDonation"), LinkButton).Visible = False
                End If
        End Select
    End Sub
    Protected Sub dgDonationList_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDonationList.PageIndexChanged
        dgDonationList.CurrentPageIndex = e.NewPageIndex
        If SortExpression Is Nothing Then
            LoadGrid()
        Else
            LoadGrid(SortExpression)
        End If
    End Sub
    Protected Sub dgDonationList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgDonationList.SelectedIndexChanged

    End Sub
    Protected Sub dgDonationList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgDonationList.SortCommand
        If e.SortExpression = "" Then Exit Sub
        SortExpression = e.SortExpression
        LoadGrid(e.SortExpression)
    End Sub
    Private Shared _sortExpression As String
    Public Shared Property SortExpression() As String
        Get
            Return _sortExpression
        End Get
        Set(ByVal value As String)
            _sortExpression = value
        End Set
    End Property

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        If Page.IsValid = True Then
            LoadGrid()
            pnlReport.Visible = True
        End If
    End Sub

    Protected Sub CustomValidator1_ServerValidate(ByVal source As Object, ByVal args As System.Web.UI.WebControls.ServerValidateEventArgs) Handles CustomValidator1.ServerValidate
        Dim dt1 As DateTime = Convert.ToDateTime(txtFromDate.Text)
        Dim dt2 As DateTime = Convert.ToDateTime(txtToDate.Text)
        If CDate(dt1.AddYears(2)) < CDate(Date.Now) And Not Session("RoleID") = "1" Then
            args.IsValid = False
            CustomValidator1.ErrorMessage = "Start Date cannot be earlier than 2 years from current date."
            dgDonationList.Visible = False
        ElseIf dt2 > dt1.AddMonths(24) And Not Session("RoleID") = "1" Then
            args.IsValid = False
            CustomValidator1.ErrorMessage = "Difference between Start Date and End Date can not be more than 2 Years."
        Else
            args.IsValid = True
        End If
    End Sub
    Function GetAmount(ByVal Amount As Decimal)
        Return FormatNumber(Amount, 2)
    End Function
End Class
