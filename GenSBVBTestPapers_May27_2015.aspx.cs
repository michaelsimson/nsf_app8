﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Configuration;
using System.IO;
using System.Threading; 

public partial class GenSBVBTestPapers : System.Web.UI.Page
{
    #region <<Variable Declaration>>
    int totalCount;
    int ic;
    int icTemp;
    int icAnsKey;
    int icAnsKey1;
    int icStuPh2=25;
    int icStuCopy = 0;
    int icStuCopyOffUse1 = 0;
    int icStuCopyOffUse2 = 0;
    int icStuCopy1 = 15;
    int icStuCopyOffUse11 = 15;
    int icStuCopyOffUse21 = 15;
    string roleIdColumnName = "RoleId";
    string cString = "ConnectionString";
    #endregion
    #region <<Pre-defined Functions>>
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            
            ic = 0;
            icTemp = 0;
            icAnsKey = 0;
            icAnsKey1 = 15;
            ViewState["increValue"] = ic;
            ViewState["increValueTemp"] = icTemp;
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            //{
            //    Response.Redirect("~/login.aspx?entry=p");
            //}
            //if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            if (Array.IndexOf(new int[] { 1, 97 }, Convert.ToInt32(Session[roleIdColumnName])) < 0)
            {
                Response.Redirect("~/VolunteerFunctions.aspx");
            }
            divTemp.Visible = false;
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();
         
            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));

            divTestPapers.Visible = false;


            if (Request.QueryString["Param"] != null)
            {
                string filename = "";


                if (Request.QueryString["Param"].ToString() == "1")
                {
                    if (Session["ddlEvent"].ToString().Equals("2") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                        report_JudgeCopy();
                    }
                    else if (Session["ddlEvent"].ToString().Equals("1") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        report_JudgeCopy_Finals_Vocab();
                    }

                }
                else if (Request.QueryString["Param"].ToString() == "2")
                {
                    if (Session["ddlEvent"].ToString().Equals("2") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                        report_AnswerKey();
                    }
                    else if (Session["ddlEvent"].ToString().Equals("1") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        report_AnswerKey_Finals_Vocab();
                    }
                }
                else if (Request.QueryString["Param"].ToString() == "3")
                {
                  
                    if (Session["ddlEvent"].ToString().Equals("2") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                        report_StudentCopy();
                    }
                    else if (Session["ddlEvent"].ToString().Equals("1") && Session["ddlProductGroup"].ToString().Equals("VB"))
                    {
                        report_StudentCopy_Finals_Vocab();
                    }
                }
            }
        }
   
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = 0;
        ViewState["increValueTemp"] = 0;
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlSet.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Set";
            lblErr.Visible = true;
        }
        else if (ddlOutput.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Output";
            lblErr.Visible = true;
        }
        else
        {
            Session["ddlYear"] = ddlYear.SelectedValue;
            Session["ddlEvent"] = ddlEvent.SelectedValue;
            Session["ddlProductGroup"] = ddlProductGroup.SelectedValue;
            Session["ddlProduct"] = ddlProduct.SelectedValue;
            Session["ddlProductText"] = ddlProduct.SelectedItem.Text;
            Session["ddlSet"] = ddlSet.SelectedValue;

            String strcheckExistsqry;
            DataSet dscheckexists;
            if (ddlOutput.SelectedIndex == 1)
            {
                lblErr.Text = "";
                strcheckExistsqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new random words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                        generatingRandomWords_Reg_Vocab();
                        displayPubUnpubList();
                    }
                    else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
                    { 
                    //Finals_JVB & Finals_IVB
                        generatingRandomWords_Finals_Vocab();
                        displayPubUnpubList();
                    }
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
                
                
                
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                   lblErr.Text = "";
                strcheckExistsqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new TP words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    generateTPWords_Vocab();
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
            }
            else if (ddlOutput.SelectedIndex == 3)
            {
                lblErr.Text = "";
                lblErr1.Text = "";
                lnkClickHere1.Visible = false;
                lnkClickHere2.Visible = false;
                btnSaveWords.Enabled = false;
                btnExportExcel.Enabled = false;
                 if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
                    {
                        //Reg_JVB & Reg_IVB
                       generateTestPapers();
                    }
                 else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
                 {
                     generateTestPapers_Finals_Vocab();
                 }
            }
        }

       
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillSet();
        fillProduct();

    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
     
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e);
    }
    protected void btnSaveWords_Click(object sender, EventArgs e)
    {
        string strqry = "";
        if (ddlOutput.SelectedIndex == 1)
        {
            strqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            strqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }

        DataSet ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DateTime dt = DateTime.Now;
            // Checking Test Paper Approved or not
            string strApprovedTP = "select * from TestPapers where ContestYear=" + ddlYear.SelectedValue + " and EventId=" + ddlEvent.SelectedValue + " and SetNum=" + ddlSet.SelectedValue + " and ProductGroupCode ='" + ddlProductGroup.SelectedValue + "' and ProductCode='"+Session["ddlProduct"]+"' and DocType = 'TestP' ";
            DataSet dsApprovedStatus = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strApprovedTP);

            // Checking current date >= Contest date (based on the setID) minus 14 days; 
            string strContestDate = "select SatDay1 from WeekCalendar where WeekID=" + ddlSet.SelectedValue + "";
            DataSet dsCheckDate = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strContestDate);
            if (dsCheckDate.Tables[0].Rows.Count > 0)
            {
                dt = (DateTime)dsCheckDate.Tables[0].Rows[0]["SatDay1"];
                dt = dt.AddDays(-14).Date;
            }



            if (dsApprovedStatus.Tables[0].Rows.Count > 0)
            {
                //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "notallowedtoOverwrite();", true);
                lblErr.Text = "Cannot override, since test paper was already uploaded";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else if (DateTime.Now.Date.CompareTo(Convert.ToDateTime(dt)) > 0)
            {
                lblErr.Text = "Cannot override, since current date is Greater than Contest date ";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtooverwrite();", true);
            }

        }
        else
        {
            insertintoTables();
        }

    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {

        if (ddlOutput.SelectedIndex == 1)
        {
            DataTable dtRandWords = (DataTable)Session["GeneratedRandomWords"];

            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=GenerateRandomWords.xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtRandWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated Random Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtRandWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtRandWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtRandWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            DataTable dtGenTPWords = (DataTable)Session["GenTPWords"];

            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=GenerateTPWords.xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtGenTPWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated TP Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtGenTPWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtGenTPWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtGenTPWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
          
            string todelete = "";

            if (ddlOutput.SelectedIndex == 1)
            {

                todelete = "delete from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                todelete = "delete from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }

        }
        catch (Exception ex)
        {

        }
    }
    protected void btnexporttopdf_Click(object sender, EventArgs e)
    {

        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
        {
            Session["PageTitleProductName"] = " Junior Vocab";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
        {
            Session["PageTitleProductName"] = " Intermediate Vocab";
        }

        string param = "1";
        ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
        param = "2";
        ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");
        param = "3";
        ClientScript.RegisterStartupScript(Page.GetType(), param, "<script language='javascript'>fnOpen('" + param + "');</script>");


    }
    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {

        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 5);
        if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //Reg_JVB & Reg_IVB
            generateTestPapers();
        }
        else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            generateTestPapers_Finals_Vocab();
        }
    }
    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        ViewState["increValue"] = Convert.ToInt32(ViewState["increValue"].ToString()) - 10;
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 5);
        if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            //Reg_JVB & Reg_IVB
            generateTestPapers();
        }
        else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
        {
            generateTestPapers_Finals_Vocab();
        }
    }
    protected void rpTestPaper_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


        ic = Convert.ToInt32(ViewState["increValue"].ToString()) + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNo");
        lblSNo.Text = Convert.ToString(ic);

        ViewState["increValue"] = ic;

    }
    protected void lnkClickHere1_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        DataSet ds;
        if (ddlOutput.SelectedIndex == 1)
        {
            ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "");
        }
        else
        {
            ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[Sub-Level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= '"+Session["ddlProduct"]+"' and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + " Order By PubUnp, Level, [Sub-Level],RandSeqID");
        }
        
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            if (ddlOutput.SelectedIndex == 1)
            {
                Session["GeneratedRandomWords"] = ds.Tables[0];
            }
            else
            {
                Session["GenTPWords"] = ds.Tables[0];
            }

            btnSaveWords.Enabled = true;
            btnExportExcel.Enabled = true;
            divTestPapers.Visible = false;
        }
    }
    protected void lnkClickHere2_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        if (ddlOutput.SelectedIndex == 1)
        {
            if (ddlEvent.SelectedValue.Equals("2") && ddlProductGroup.SelectedValue.Equals("VB"))
            {
                //Reg_JVB & Reg_IVB
                generatingRandomWords_Reg_Vocab();
                displayPubUnpubList();
            }
            else if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("VB"))
            {
                generatingRandomWords_Finals_Vocab();
                displayPubUnpubList();
            }

        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            
                generateTPWords_Vocab();
           
        }
        btnSaveWords.Enabled = true;
        btnExportExcel.Enabled = true;
        divTestPapers.Visible = false;
    }
    public override void VerifyRenderingInServerForm(Control control)
    {
        //Required to verify that the control is rendered properly on page
    }
    protected void rpToPDF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {
        
        icTemp = icTemp + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoTemp");
        lblSNo.Text = Convert.ToString(icTemp);
        
    }
    protected void rpStuPhase2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        
    }
    protected void Item_Bound(Object sender, DataListItemEventArgs e)
    {

        icStuPh2 = icStuPh2 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("SNoStuPhase2");
        lblSNo.Text = Convert.ToString(icStuPh2);

    }
    protected void Item_Bound_AnsKey(Object sender, DataListItemEventArgs e)
    {

        icAnsKey = icAnsKey + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoAnsKey");
        lblSNo.Text = Convert.ToString(icAnsKey) + ".";

    }
    protected void Item_Bound_AnsKey1(Object sender, DataListItemEventArgs e)
    {

        icAnsKey1 = icAnsKey1 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoAnsKey1");
        lblSNo.Text = Convert.ToString(icAnsKey1) + ".";

    }
    protected void Item_Bound_StuCopy(Object sender, DataListItemEventArgs e)
    {

        icStuCopy = icStuCopy + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNo");
        lblSNo.Text = Convert.ToString(icStuCopy) + ".";
        
    }
    protected void Item_Bound_StuCopyOffUse1(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse1 = icStuCopyOffUse1 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse1");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse1) + ".";



        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange1");
        if (icStuCopyOffUse1 == 10)
        {
            lblScoreRange.Text = "Score 1-10";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 15)
        {
            lblScoreRange.Text = "Score 11-15";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse1 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 30)
        {
            lblScoreRange.Text = "Score 26-30";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }


    }
    protected void Item_Bound_StuCopyOffUse2(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse2 = icStuCopyOffUse2 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse2");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse2) + ".";

        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange2");
        if (icStuCopyOffUse2 == 10)
        {
            lblScoreRange.Text = "Score 1-10";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 15)
        {
            lblScoreRange.Text = "Score 11-15";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse2 == 30)
        {
            lblScoreRange.Text = "Score 26-30";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }

    }
    protected void Item_Bound_StuCopy1(Object sender, DataListItemEventArgs e)
    {

        icStuCopy1 = icStuCopy1 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNo1");
        lblSNo.Text = Convert.ToString(icStuCopy1) + ".";
        
    }
    protected void Item_Bound_StuCopyOffUse11(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse11 = icStuCopyOffUse11 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse11");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse11) + ".";



        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange11");

        if (icStuCopyOffUse11 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse11 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }


    }
    protected void Item_Bound_StuCopyOffUse21(Object sender, DataListItemEventArgs e)
    {

        icStuCopyOffUse21 = icStuCopyOffUse21 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblStuPh1SNoOffUse21");
        lblSNo.Text = Convert.ToString(icStuCopyOffUse21) + ".";

        Label lblScoreRange = default(Label);
        lblScoreRange = (Label)e.Item.FindControl("lblScoreRange21");
        if (icStuCopyOffUse21 == 20)
        {
            lblScoreRange.Text = "Score 16-20";
            lblScoreRange.Visible = true;
        }
        else if (icStuCopyOffUse21 == 25)
        {
            lblScoreRange.Text = "Score 21-25";
            lblScoreRange.Visible = true;
        }
        else
        {
            lblScoreRange.Visible = false;
        }

    }
    protected void gvPubUnpubList_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        gvPubUnpubList.PageIndex = e.NewPageIndex;

        gvPubUnpubList.DataBind();
        if (ddlOutput.SelectedIndex == 1)
        {
            displayPubUnpubList();
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            //generateTPWords_Vocab();

           
                generateTPWords_Vocab();
           
        }
    }
       //Generate TestPapers for Reg_JVB & Reg_IVB
#endregion
    #region <<User-defined Functions>>

    protected void generateTestPapers_Finals_Vocab()
    {
        divFrontEndView.Visible = true;
        totalCount = 0;
        lblTestPaperPhase.Text = string.Format("<b>Phase: I {0}- Judge Copy(Published)</b>", Session["ddlProductText"]);
        string strquery = "";
        strquery = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        strquery = strquery + " union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        strquery = strquery + " union(select distinct top 15 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 15 ) and sp.PubUnp='Y' and  sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        strquery = strquery + " union(select distinct top 5 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 16 and 20 ) and sp.PubUnp='Y' and sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.[Sub-level]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);

        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;
        }
   

        
        Session["GenerateTestPapers"] = dsJVB.Tables[0];
        totalCount = dsJVB.Tables[0].Rows.Count;
        //rpTestPaper.DataBind();
        if (totalCount > 0)
        {
            //for finals vocabulary binding to gridview 5 words at a time
            bindData_Finals_Vocab();
            gvPubUnpubList.Visible = false;
            divTestPapers.Visible = true;
        }
        else
        {
            lblErr.Text = "No Record Exists!";
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            gvPubUnpubList.Visible = false;
            divTestPapers.Visible = false;
        }
    }
    protected void generateTestPapers() {

        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and sp.PubUnp is null";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and sp.PubUnp='Y'";
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
            strquery = strquery + " (select '"+TextLevel+"' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between "+RandSeqFrom+" and "+RandSeqTo+" ) "+PubUnpubCondition+" and sp.Level="+dr["Level"]+" and sp.[Sub-level]="+dr["SubLevel"]+" and sp.SetID=" + Session["ddlSet"] + " ) union";
           

            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
  
        strquery = strquery + " Order by sp.Phase,sp.[Sub-level]";

       
        
        divFrontEndView.Visible = true;
        totalCount = 0;
        lblTestPaperPhase.Text = string.Format("<b>Phase: I {0}- Judge Copy(Published)</b>", Session["ddlProductText"]);
  
        
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        Session["dsGenerateTestPaper"]=dsJVB;
       //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i=1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;
            
        }
          Session["GenerateTestPapers"] = dsJVB.Tables[0];
          totalCount = dsJVB.Tables[0].Rows.Count;
       //rpTestPaper.DataBind();
          if (totalCount > 0)
          {
              bindData();
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = true;

          }
          else
          {
              lblErr.Text = "No Record Exists!";
              lblErr.Visible = true;
              lblErr.ForeColor = Color.Red;
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = false;
          }
         
    }
    protected void bindData_Finals_Vocab()
    {
        string connectionString = Application[cString].ToString();
        SqlConnection connection = new SqlConnection(connectionString);
        DataSet ds = new DataSet();
        string sql = "";
        sql = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 15 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 15 ) and sp.PubUnp='Y' and  sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 5 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 16 and 20 ) and sp.PubUnp='Y' and sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.[Sub-level]";
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;
        connection.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
        adapter.Fill(ds, val, 5, "SpVocabTPWords");
        connection.Close();
        rpTestPaper.DataSource = ds;
        rpTestPaper.DataBind();

        if (val <= 0)
        {
            lnkBtnPrev.Visible = false;
            lnkBtnNext.Visible = true;
        }

        if (val >= 5)
        {
            lnkBtnPrev.Visible = true;
            lnkBtnNext.Visible = true;
        }

        if ((val + 5) >= totalCount)
        {
            lnkBtnNext.Visible = false;
        }
    }
    protected void bindData()
    {
        string productstr = "";
        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
        {
            productstr = " and sp.level=1";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
        {
            productstr = " and sp.level=2";
        }

        string connectionString = Application[cString].ToString();
        SqlConnection connection = new SqlConnection(connectionString);
        DataSet ds = new DataSet();
        //String sql = "(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level, sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 10 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )";//union(select distinct top 4 sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 4 sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " ) ";
        String sql = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null " + productstr + " and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null " + productstr + " and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 10 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' " + productstr + " and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )";
        sql = sql + " union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' " + productstr + " and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.[Sub-level]";
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;
        connection.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
        adapter.Fill(ds, val, 5, "SpVocabTPWords");
        connection.Close();
        rpTestPaper.DataSource = ds;
        rpTestPaper.DataBind();

        if (val <= 0)
        {
            lnkBtnPrev.Visible = false;
            lnkBtnNext.Visible = true;
        }

        if (val >= 5)
        {
            lnkBtnPrev.Visible = true;
            lnkBtnNext.Visible = true;
        }

        if ((val + 5) >= totalCount)
        {
            lnkBtnNext.Visible = false;
        }
    }
   
    protected void generateTPWords_Vocab()
    {
        string strquery = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "'";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition="";
        for (int i = 0; i < dsSelMatrix.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[i];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()])+1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp is null";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='Y'";
            }
 
            strquery =strquery+ "(select Year,EventID,SetID,"+dr["Phase"]+" as Phase,'"+dr["RoundType"]+"' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + Session["ddlYear"] + " and Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ([RandSeqID] between "+RandSeqFrom+" and "+RandSeqTo+" ) "+PubUnpubCondition+" and Level="+dr["Level"]+" and Sublevel="+dr["Sublevel"]+" and SetID=" + Session["ddlSet"] + " )";
            if (i < dsSelMatrix.Tables[0].Rows.Count - 1)
            {
                strquery = strquery + " union ";
            }
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
        }

        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        if (dsJVB.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = dsJVB.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            Session["GenTPWords"] = dsJVB.Tables[0];
        }
        else
        {
            lblErr.Text = "No Record Exists!";
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Red;
            gvPubUnpubList.Visible = false;
        }

       

    }
    protected void displayPubUnpubList()
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from TempSBVBRandWords");
        //DataTable dtEasy = dsEasy.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            Session["GeneratedRandomWords"] = ds.Tables[0];
        }
    }
    //Generating Random words only for Finals_JVB & Finals_IVB
    protected void generatingRandomWords_Finals_Vocab()
    {
        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
        {
            // Getting Published records with Level 1 Sublevel 1 from Vocabwords_new and insert into Temporary Table
            string strqry = string.Empty;
            strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempSBVBRandWords')) begin drop table TempSBVBRandWords end create table TempSBVBRandWords(Year int,EventID int,SetID int,ProductGroupID int,ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20),PubUnp nvarchar(20),Level int, SubLevel int,OrigSeqID int identity(1,1) primary key,RandomNumber float, RandSeqID int,Word nvarchar(50),  CreatedDate date, CreatedBy int, ModifiedDate date, ModifiedBy int) insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y' and Level=1 and [Sub-level]=1))";
            int i = SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
            //Randomizing the set of records in TempTable and feeded into same table
            DataSet dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=1 and Sublevel=1");
            DataTable dtPubSL1 = dsPubSL1.Tables[0];
            DataTable dtnew = (DataTable)dtPubSL1.Clone();
            int k = 0;
            string updateqry = "";

            if (dtPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=1 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=1 and Sublevel=1 Order By RandomNumber");
            dtPubSL1 = dsPubSL1.Tables[0];
            dtnew = (DataTable)dtPubSL1.Clone();

            updateqry = "";
            if (dtPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=1 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }


            // Getting Published records with Level 1 Sublevel2 from Vocabwords_new and insert into Temporary Table
            strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y' and Level=1 and [Sub-level]=2))";
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
            //Randomizing the set of records in TempTable and feeded into same table
            DataSet dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=1 and Sublevel=2");
            DataTable dtPubSL2 = dsPubSL2.Tables[0];
            dtnew = (DataTable)dtPubSL2.Clone();

            updateqry = "";

            if (dtPubSL2.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL2.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL2.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=1 and [Sublevel]=2  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=1 and Sublevel=2 Order By RandomNumber");
            dtPubSL2 = dsPubSL2.Tables[0];
            dtnew = (DataTable)dtPubSL2.Clone();

            updateqry = "";
            if (dtPubSL2.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL2.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL2.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=1 and [Sublevel]=2  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            // Getting UnPublished records with Level=2 Sublevel1 from Vocabwords_new and insert into Temporary Table
            strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where National_Reserved is not null and Level=2 and [Sub-level]=1))";
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
            //Randomizing the set of records in TempTable and feeded into same table
            DataSet dsUnPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' and Level=2 and Sublevel=1");
            DataTable dtUnPubSL1 = dsUnPubSL1.Tables[0];
            dtnew = (DataTable)dtUnPubSL1.Clone();

            updateqry = "";

            if (dtUnPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' and Level=2 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            dsUnPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' and Level=2 and Sublevel=1 Order By RandomNumber");
            dtUnPubSL1 = dsUnPubSL1.Tables[0];
            dtnew = (DataTable)dtUnPubSL1.Clone();

            updateqry = "";
            if (dtUnPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' and Level=2 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

        }
        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
        {
            // Getting Published records with Level 3 Sublevel 1 from Vocabwords_new and insert into Temporary Table
            string strqry = string.Empty;
            strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempSBVBRandWords')) begin drop table TempSBVBRandWords end create table TempSBVBRandWords(Year int,EventID int,SetID int,ProductGroupID int,ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20),PubUnp nvarchar(20),Level int, SubLevel int,OrigSeqID int identity(1,1) primary key,RandomNumber float, RandSeqID int,Word nvarchar(50),  CreatedDate date, CreatedBy int, ModifiedDate date, ModifiedBy int) insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where National_Reserved is null and Level=3 and [Sub-level]=1))";
            int i = SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
            //Randomizing the set of records in TempTable and feeded into same table
            DataSet dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=3 and Sublevel=1");
            DataTable dtPubSL1 = dsPubSL1.Tables[0];
            DataTable dtnew = (DataTable)dtPubSL1.Clone();
            int k = 0;
            string updateqry = "";

            if (dtPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=3 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null and Level=3 and Sublevel=1 Order By RandomNumber");
            dtPubSL1 = dsPubSL1.Tables[0];
            dtnew = (DataTable)dtPubSL1.Clone();

            updateqry = "";
            if (dtPubSL1.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL1.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL1.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null and Level=3 and [Sublevel]=1  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }


            // Getting UnPublished records with Level 3 Sublevel2 from Vocabwords_new and insert into Temporary Table
            strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where National_Reserved is not null and Level=3 and [Sub-level]=2))";
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
            //Randomizing the set of records in TempTable and feeded into same table
            DataSet dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' and Level=3 and Sublevel=2");
            DataTable dtPubSL2 = dsPubSL2.Tables[0];
            dtnew = (DataTable)dtPubSL2.Clone();

            updateqry = "";

            if (dtPubSL2.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL2.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL2.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' and Level=3 and [Sublevel]=2  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

            dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' and Level=3 and Sublevel=2 Order By RandomNumber");
            dtPubSL2 = dsPubSL2.Tables[0];
            dtnew = (DataTable)dtPubSL2.Clone();

            updateqry = "";
            if (dtPubSL2.Rows.Count > 0)
            {
                for (k = 0; k < dtPubSL2.Rows.Count; k++)
                {
                    dtnew.ImportRow(dtPubSL2.Rows[k]);
                    dtnew.Rows[k]["RandSeqID"] = k + 1;

                    updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                          + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' and Level=3 and [Sublevel]=2  ";
                }
            }
            if (!updateqry.Equals(""))
            {
                SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
            }

           

        }
    }
    //Generating Random words only for Reg_JVB & Reg_IVB
    protected void generatingRandomWords_Reg_Vocab()
    {
     
        string strqry = "";
        string productstr = "";
        if (ddlProduct.SelectedItem.Value.Equals("JVB"))
        {
            productstr = " and level=1";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("IVB"))
        {
            productstr = " and level=2";
        }

        // Getting Published records with Sublevel1 from Vocabwords_new and insert into Temporary Table
        strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempSBVBRandWords')) begin drop table TempSBVBRandWords end create table TempSBVBRandWords(Year int,EventID int,SetID int,ProductGroupID int,ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20),PubUnp nvarchar(20),Level int, SubLevel int,OrigSeqID int identity(1,1) primary key,RandomNumber float, RandSeqID int,Word nvarchar(50),  CreatedDate date, CreatedBy int, ModifiedDate date, ModifiedBy int) insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y'" + productstr + " and [Sub-level]=1))";
        int i = SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=1");
        DataTable dtPubSL1 = dsPubSL1.Tables[0];
        DataTable dtnew = (DataTable)dtPubSL1.Clone();
        int k = 0;
        string updateqry = "";
     
        if (dtPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

        dsPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=1 Order By RandomNumber");
        dtPubSL1 = dsPubSL1.Tables[0];
        dtnew = (DataTable)dtPubSL1.Clone();
       
       updateqry = "";
        if (dtPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID ="+dtnew.Rows[k]["RandSeqID"]+" where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }


        // Getting Published records with Sublevel2 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y'" + productstr + " and [Sub-level]=2))";
        SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=2");
        DataTable dtPubSL2 = dsPubSL2.Tables[0];
        dtnew = (DataTable)dtPubSL2.Clone();
     
        updateqry = "";

        if (dtPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

        dsPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=2 Order By RandomNumber");
        dtPubSL2 = dsPubSL2.Tables[0];
        dtnew = (DataTable)dtPubSL2.Clone();

        updateqry = "";
        if (dtPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

        // Getting UnPublished records with Sublevel1 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Reg_Reserved ='Y'" + productstr + " and [Sub-level]=1))";
        SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsUnPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=1");
        DataTable dtUnPubSL1 = dsUnPubSL1.Tables[0];
        dtnew = (DataTable)dtUnPubSL1.Clone();

        updateqry = "";

        if (dtUnPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

        dsUnPubSL1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=1 Order By RandomNumber");
        dtUnPubSL1 = dsUnPubSL1.Tables[0];
        dtnew = (DataTable)dtUnPubSL1.Clone();

        updateqry = "";
        if (dtUnPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }
        
        // Getting UnPublished records with Sublevel2 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Reg_Reserved ='Y'" + productstr + " and [Sub-level]=2))";
        SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsUnPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=2");
        DataTable dtUnPubSL2 = dsUnPubSL2.Tables[0];
        dtnew = (DataTable)dtUnPubSL2.Clone();

        updateqry = "";

        if (dtUnPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

        dsUnPubSL2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=2 Order By RandomNumber");
        dtUnPubSL2 = dsUnPubSL2.Tables[0];
        dtnew = (DataTable)dtUnPubSL2.Clone();

        updateqry = "";
        if (dtUnPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, updateqry);
        }

    }
    protected void fillSet()
    {

        ddlSet.Items.Clear();
        if (ddlEvent.SelectedIndex == 1)
        {
           
            ddlSet.Items.Add(new ListItem("Select Set"));


            ddlSet.Items.Add(new ListItem("4"));

            ddlSet.SelectedIndex = 1;
            ddlSet.Enabled = false;
        }
        else
        {
          
            ddlSet.Items.Add(new ListItem("Select Set"));

            for (int i = 1; i <= 3; i++)
            {
                ddlSet.Items.Add(new ListItem(i.ToString()));
            }
            ddlSet.SelectedIndex = 0;
            ddlSet.Enabled = true;
        }
    }
    protected void fillProduct()
    {

        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
        DataSet dsstate = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, ddlproductqry);
        ddlProduct.DataSource = dsstate;
        //ddlProductGroup.DataTextField = "ProductCode";
        ddlProduct.DataValueField = "ProductCode";
        ddlProduct.DataTextField = "Name";
        ddlProduct.DataBind();
        ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
    }
    protected void insertintoTables()
    {
        string sql = "";
        if (ddlOutput.SelectedIndex == 1)
        {
            DataTable dt = (DataTable)Session["GeneratedRandomWords"];
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Year"] = "null";
                }
                if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["EventID"] = "null";
                }
                if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SetId"] = "null";
                }
                if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupID"] = "null";
                }
                if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupCode"] = "null";
                }
                if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductID"] = "null";
                }
                if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductCode"] = "null";
                }
                if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["PubUnp"] = "null";
                }
                if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Level"] = "null";
                }
                if (dt.Rows[i]["SubLevel"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SubLevel"] = "null";
                }
                if (dt.Rows[i]["RandSeqID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["RandSeqID"] = 0;
                }
                if (dt.Rows[i]["Word"].ToString().Contains("'"))
                {
                    dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                }
                sql = sql + "insert into SBVBRandWords (Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                      + dt.Rows[i]["Year"].ToString().Trim() + ","
                      + dt.Rows[i]["EventID"].ToString().Trim() + ","
                      + dt.Rows[i]["SetId"].ToString().Trim() + ","
                      + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductCode"].ToString().Trim() + "',case '"
                       + dt.Rows[i]["PubUnp"].ToString().Trim() + "' when 'null' then null when 'Y' then 'Y' end,"
                      + dt.Rows[i]["Level"].ToString().Trim() + ","
                      + dt.Rows[i]["SubLevel"].ToString().Trim() + ","
                      + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                      + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                      + Session["LoginID"] + ")";
                }

        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            DataTable dt = (DataTable)Session["GenTPWords"];


            for (int i = 0; i < dt.Rows.Count; i++)
            {

                if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Year"] = "null";
                }
                if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["EventID"] = "null";
                }
                if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["SetId"] = "null";
                }
                if (dt.Rows[i]["Phase"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Phase"] = "null";
                }
                if (dt.Rows[i]["RoundType"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["RoundType"] = "null";
                }
                if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupID"] = "null";
                }
                if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductGroupCode"] = "null";
                }
                if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductID"] = "null";
                }
                if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["ProductCode"] = "null";
                }
                if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["PubUnp"] = "null";
                }
                if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Level"] = "null";
                }
                if (dt.Rows[i]["Sub-Level"].ToString().Equals(string.Empty))
                {
                    dt.Rows[i]["Sub-Level"] = "null";
                }
                if (dt.Rows[i]["Word"].ToString().Contains("'"))
                {
                    dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                }
                sql = sql + "insert into SpVocabTPWords (Year,EventID,SetID,Phase,RoundType,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[Sub-Level],OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                      + dt.Rows[i]["Year"].ToString().Trim() + ","
                      + dt.Rows[i]["EventID"].ToString().Trim() + ","
                      + dt.Rows[i]["SetId"].ToString().Trim() + ","
                      + dt.Rows[i]["Phase"].ToString().Trim() + ",'"
                      + dt.Rows[i]["RoundType"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                      + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["ProductCode"].ToString().Trim() + "',case '"
                       + dt.Rows[i]["PubUnp"].ToString().Trim() + "' when 'null' then null when 'Y' then 'Y' end,"
                      + dt.Rows[i]["Level"].ToString().Trim() + ","
                      + dt.Rows[i]["Sub-Level"].ToString().Trim() + ","
                      + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                      + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                      + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                      + Session["LoginID"] + ")";

            }
        }


        int j = SqlHelper.ExecuteNonQuery(Application[cString].ToString(), CommandType.Text, sql);

        if (j > 0)
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Blue;
            lblErr.Text = "Saved Successfully";
        }
    }
    protected void generateProjectionSlidePhase2()
    {
        // string m_OutputFile = "~/Phase2SlideProjection.htm";

        //app8
        string m_OutputFile = HttpContext.Current.Server.MapPath("~/Reports/TestPaper/Phase2SlideProjection.htm");
        //local
        //string m_OutputFile = HttpContext.Current.Server.MapPath("~/Phase2SlideProjection.htm");
        StreamWriter sw = new StreamWriter(m_OutputFile, false);

        Server.Execute("TestSlidesPh2Projection.aspx?", sw);

        sw.Flush();
        sw.Close();
    }
    protected void report_StudentCopy_Finals_Vocab()
    {
        string strquery = "";
        strquery = strquery + "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 15 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 15 ) and sp.PubUnp='Y' and sp.Level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 16 and 20 ) and sp.PubUnp='Y' and sp.Level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  ) Order by sp.[Sub-level]";


        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);

        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;

        }

        dlStudentCopy.DataSource = dsJVB;
        dlStudentCopy.DataBind();
        dlStudentCopyOffUse1.DataSource = dsJVB;
        dlStudentCopyOffUse1.DataBind();
        dlStudentCopyOffUse2.DataSource = dsJVB;
        dlStudentCopyOffUse2.DataBind();

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_StudentCopy_Ph1_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        iTextSharp.text.Paragraph pg3;
        iTextSharp.text.Paragraph pg4;

        pg1 = new iTextSharp.text.Paragraph("" + Session["ddlYear"] + " North South Foundation Regionals " + Session["PageTitleProductName"].ToString() + " Bee Phase I ");
        pg2 = new iTextSharp.text.Paragraph("Badge# ____________________ Student Name:  __________________________");
        pg3 = new iTextSharp.text.Paragraph("Please circle correct letter choice and meaning below:");
        pg4 = new iTextSharp.text.Paragraph(" ");

        pdfDoc.Open();

        int ii = 0;
        int stupno = 1;

        if (dlStudentCopy.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            lblYrRegional.Text = Session["ddlYear"] + " Finals";
            lblProductName.Text = Session["ddlProductText"] + " Bee - Phase I";
            //FrontPage student copy
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuCopyFront.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);
            pdfDoc.NewPage();
            // from page no:2

            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            pdfDoc.Add(pg3);
            pdfDoc.Add(pg4);
            stupno++;

            pStudentCopyPhase1.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_StudentCopy()
    {
        string strquery = "";
        string strquery1 = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1 Order by WordCount1 desc";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];
            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp is null";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='Y'";
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }

            if (l < 2)
            {
                strquery = strquery + "(select  '"+TextLevel+"' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between "+RandSeqFrom+" and "+RandSeqTo+" ) "+PubUnpubCondition+" and sp.Level="+dr["Level"]+" and sp.[Sub-level]="+dr["SubLevel"]+" and sp.SetID=" + Session["ddlSet"] + ") union";
            }
            else
            {
                strquery1 = strquery1 + "(select  '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + ") union";
            }
          
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
        strquery1 = strquery1.Substring(0, strquery1.Length - 6);
        strquery = strquery + " Order by sp.Phase,sp.[Sub-level]";
        strquery1 = strquery1 + " Order by sp.Phase,sp.[Sub-level]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);

        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;

        }
        dlStudentCopy.DataSource = dsJVB;
        dlStudentCopy.DataBind();
        dlStudentCopyOffUse1.DataSource = dsJVB;
        dlStudentCopyOffUse1.DataBind();
        dlStudentCopyOffUse2.DataSource = dsJVB;
        dlStudentCopyOffUse2.DataBind();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_StudentCopy_Ph1_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);
        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        iTextSharp.text.Paragraph pg3;
        iTextSharp.text.Paragraph pg4;
        pg1 = new iTextSharp.text.Paragraph("" + Session["ddlYear"] + " North South Foundation Regionals " + Session["PageTitleProductName"].ToString() + " Bee Phase I ");
        pg2 = new iTextSharp.text.Paragraph("Badge# ____________________ Student Name:  __________________________");
        pg3 = new iTextSharp.text.Paragraph("Please circle correct letter choice and meaning below:");
        pg4 = new iTextSharp.text.Paragraph(" ");

        pdfDoc.Open();

        int ii = 0;
        int stupno = 1;

        if (dlStudentCopy.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            lblYrRegional.Text = Session["ddlYear"] + " Regionals";
            lblProductName.Text = Session["ddlProductText"] + " Bee - Phase I";
            //FrontPage student copy
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuCopyFront.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);
            pdfDoc.NewPage();
            // from page no:2

            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            pdfDoc.Add(pg3);
            pdfDoc.Add(pg4);
            stupno++;



            pStudentCopyPhase1.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }
        //second Page:

        DataSet dsJVB1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery1);

        //adding serial number to dataTable
        DataColumn column1 = dsJVB1.Tables[0].Columns.Add("SNo", typeof(Int32));
        column1.SetOrdinal(0);// to put the column in position 0;
        int i1 = 16;
        foreach (DataRow dr in dsJVB1.Tables[0].Rows)
        {
            dr["SNo"] = i1;
            i1++;

        }

        dlStudentCopy1.DataSource = dsJVB1;
        dlStudentCopy1.DataBind();
        dlStudentCopyOffUse11.DataSource = dsJVB1;
        dlStudentCopyOffUse11.DataBind();
        dlStudentCopyOffUse21.DataSource = dsJVB1;
        dlStudentCopyOffUse21.DataBind();

        pdfDoc.Add(pg1);
        pdfDoc.Add(pg2);
        pdfDoc.Add(pg3);
        pdfDoc.Add(pg4);

        ////Office user 1
        StringWriter swHeaderOffUse1 = new StringWriter();
        HtmlTextWriter hwHeaderOffUse1 = new HtmlTextWriter(swHeaderOffUse1);
        pStudentCopyPhase11.RenderControl(hwHeaderOffUse1);
        StringReader srHeaderOffUse1 = new StringReader(swHeaderOffUse1.ToString());
        htmlparser.Parse(srHeaderOffUse1);
        // score sheet;

        StringWriter swHeader = new StringWriter();
        HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
        pScoreSheet.RenderControl(hwHeader);
        StringReader srHeader = new StringReader(swHeader.ToString());
        htmlparser.Parse(srHeader);


        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_JudgeCopy()
    {

        string strquery = "";
        string strqryStuPh2 = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "'";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int l = 0; l < dsSelMatrix.Tables[0].Rows.Count; l++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[l];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and sp.PubUnp is null";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and sp.PubUnp='Y'";
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }
                 strquery = strquery+ "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and "+RandSeqTo+" ) "+PubUnpubCondition+" and sp.Level=" + dr["Level"] + " and sp.[Sub-level]="+dr["SubLevel"]+" and sp.SetID=" + Session["ddlSet"] + " and sp.Phase="+dr["Phase"]+" ) union";
            if (dr["Phase"].ToString().Equals("2"))
            {
                strqryStuPh2 = strqryStuPh2 + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=" + dr["Phase"] + " ) union";
            }
           
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
        strqryStuPh2 = strqryStuPh2.Substring(0,strqryStuPh2.Length-6);
        strquery = strquery + " Order by sp.Phase,sp.[Sub-level]";
        strqryStuPh2 = strqryStuPh2 + " Order by sp.Phase,sp.[Sub-level]";

        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;

        }

        rpToPDF.DataSource = dsJVB;
        rpToPDF.DataBind();
        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_JudgesCopy_Ph1_Ph2.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();

        int i = 0;
        int pno = 1;
        int pnoPh2 = 1;
        string pageTitle = "";
        for (i = 0; i <= rpToPDF.Items.Count - 1; i++)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);

            if (i > 0)
            {
                if (i == 10 || i == 20 || i == 25 || i == 35 || i == 45 || i == 55 || i == 65 || i == 75 || i == 85)
                {

                    if (pno == 2)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Unpublished) - Pg " + pno + " of 3 ";
                    }
                    else if (pno == 3)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Unpublished) - TIE BREAKER WORDS - Pg " + pno + " of 3";
                    }
                    else if (pno == 4)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 5)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 6)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 7)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 8)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 9)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 10)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }

                    pg1 = new iTextSharp.text.Paragraph(pageTitle);
                    pg2 = new iTextSharp.text.Paragraph(" ");
                    pdfDoc.Add(pg1);
                    pdfDoc.Add(pg2);
                    pno++;
                    StringWriter swHeader = new StringWriter();
                    HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                    pTableHeader.RenderControl(hwHeader);
                    StringReader srHeader = new StringReader(swHeader.ToString());
                    htmlparser.Parse(srHeader);
                }
            }
            rpToPDF.Items[i].RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            if (i == 0)
            {
                pg1 = new iTextSharp.text.Paragraph("Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Published) - Pg " + pno + " of 3");
                pdfDoc.Add(pg1);
                pg2 = new iTextSharp.text.Paragraph(" ");
                pdfDoc.Add(pg2);
                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);

            }
            htmlparser.Parse(sread);
        }



        // Phase2 StudentCopy appending with Judge copy Ph1&2

       
        // Judge Copy Phase1 & Phase2

        //strqryStuPh2 = "(select distinct top 35 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null " + productstr + " and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  )";
        //strqryStuPh2 = strqryStuPh2 + "union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null " + productstr + " and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level]";
        
        DataSet dsJVBStuPh2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strqryStuPh2);
        //adding serial number to dataTable
        DataColumn column1 = dsJVBStuPh2.Tables[0].Columns.Add("SNo", typeof(Int32));
        column1.SetOrdinal(0);// to put the column in position 0;
        int k1 = 26;
        foreach (DataRow dr in dsJVBStuPh2.Tables[0].Rows)
        {
            dr["SNo"] = k1;
            k1++;

        }

        rpStuPhase2.DataSource = dsJVBStuPh2;
        rpStuPhase2.DataBind();
        Session["Phase2SlideWords"] = dsJVBStuPh2;

        generateProjectionSlidePhase2();

        if (rpStuPhase2.Items.Count > 0)
        {
            pdfDoc.NewPage();

            pg1 = new iTextSharp.text.Paragraph("Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Student Copy  - Pg 1 of 3");
            pg2 = new iTextSharp.text.Paragraph(" ");
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            //Header
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuPh2Header.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);

            //Records
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            rpStuPhase2.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_JudgeCopy_Finals_Vocab() {
        string strquery = "";

        // Judge Copy Phase1 & Phase2 & Phase 3
        strquery = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 15 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 15 ) and sp.PubUnp='Y' and sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 15 and 20 ) and sp.PubUnp='Y' and sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  ) ";
        strquery = strquery + "union(select distinct top 80 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 6 and 85 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) ";
        strquery = strquery + "union(select distinct top 300 '2/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 21 and 320 ) and sp.PubUnp='Y' and sp.level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=3  ) Order by sp.Phase,sp.[Sub-level] ";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;

        }

        rpToPDF.DataSource = dsJVB;
        rpToPDF.DataBind();



        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_JudgesCopy_Ph1_Ph2.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();



        int i = 0;
        int pno = 1;
        int pnoPh2 = 1;
        string pageTitle = "";
        for (i = 0; i <= rpToPDF.Items.Count - 1; i++)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);

            if (i > 0)
            {
                if (i == 10 || i == 20 || i == 25 || i == 30 || i==40 || i == 50 || i == 60 || i == 70 || i == 80 || i == 90)
                {

                    if (pno == 2)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Published) - Pg " + pno + " of 4 ";
                    }
                    if (pno == 3)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Unpublished) - Pg " + pno + " of 4 ";
                    }
                    else if (pno == 4)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Unpublished) - TIE BREAKER WORDS - Pg " + pno + " of 4";
                    }
                    else if (pno == 5)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 6)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 7)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 8)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 9)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 10)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 11)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }

                    pg1 = new iTextSharp.text.Paragraph(pageTitle);
                    pg2 = new iTextSharp.text.Paragraph(" ");
                    pdfDoc.Add(pg1);
                    pdfDoc.Add(pg2);
                    pno++;
                    StringWriter swHeader = new StringWriter();
                    HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                    pTableHeader.RenderControl(hwHeader);
                    StringReader srHeader = new StringReader(swHeader.ToString());
                    htmlparser.Parse(srHeader);
                }
            }
            rpToPDF.Items[i].RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            if (i == 0)
            {
                pg1 = new iTextSharp.text.Paragraph("Phase I: " + Session["PageTitleProductName"].ToString() + " Words - Judge Copy (Published) - Pg " + pno + " of 4");
                pdfDoc.Add(pg1);
                pg2 = new iTextSharp.text.Paragraph(" ");
                pdfDoc.Add(pg2);
                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);

            }
            htmlparser.Parse(sread);
        }



        // Phase2 StudentCopy appending with Judge copy Ph1&2

        string strqryStuPh2 = "";
        // Judge Copy Phase1 & Phase2
        strqryStuPh2 = "(select distinct top 80 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 6 and 85 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level] ";
        //strqryStuPh2 = strqryStuPh2 + "union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=2  ) ";
        DataSet dsJVBStuPh2 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strqryStuPh2);
        //adding serial number to dataTable
        DataColumn column1 = dsJVBStuPh2.Tables[0].Columns.Add("SNo", typeof(Int32));
        column1.SetOrdinal(0);// to put the column in position 0;
        int k1 = 31;
        foreach (DataRow dr in dsJVBStuPh2.Tables[0].Rows)
        {
            dr["SNo"] = k1;
            k1++;

        }

        rpStuPhase2.DataSource = dsJVBStuPh2;
        rpStuPhase2.DataBind();
        Session["Phase2SlideWords"] = dsJVBStuPh2;

        //generateProjectionSlidePhase2();

        if (rpStuPhase2.Items.Count > 0)
        {
            pdfDoc.NewPage();
            pg1 = new iTextSharp.text.Paragraph("Phase II: " + Session["PageTitleProductName"].ToString() + " Words - Pub / Student Copy  - Pg 1 of 3");
            pg2 = new iTextSharp.text.Paragraph(" ");
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            //Header
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuPh2Header.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);

            //Records
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            rpStuPhase2.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }

        //Phase3 300 words will displayed here
        


        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_AnswerKey_Finals_Vocab()
    {
        string strquery = "";
        strquery = strquery + "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.level=1 and sp.[Sub-level]=2 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 15 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 1 and 15 ) and sp.PubUnp='Y' and sp.Level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  )";
        strquery = strquery + "union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '"+Session["ddlProduct"]+"' and ([RandSeqID] between 15 and 20 ) and sp.PubUnp='Y' and sp.Level=2 and sp.[Sub-level]=1 and sp.SetID=" + Session["ddlSet"] + " and sp.Phase=1  ) Order by sp.Phase,sp.[Sub-level]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;
        }


        rpAnswerKey.DataSource = dsJVB;
        rpAnswerKey.DataBind();



        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_AnswerKey_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        string pageTitleStuPh2 = "";
        pageTitleStuPh2 = "" + Session["ddlYear"] + " North South Foundation Finals " + Session["PageTitleProductName"].ToString() + " Bee Phase I (Answer Key)";
        pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
        pg2 = new iTextSharp.text.Paragraph(" ");
        pdfDoc.Open();

        int ii = 0;
        int stupno = 1;

        if (rpAnswerKey.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);






            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);

            stupno++;



            pAnswerKey.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void report_AnswerKey()
    {
        string strquery = "";
        string strquery1 = "";
        string strdsSelMatrix = "select * from SBVBSelMatrix where Eventid=" + Session["ddlEvent"] + " and ProductGroupCode='" + Session["ddlProductGroup"] + "' and ProductCode='" + Session["ddlProduct"] + "' and Phase=1 Order by WordCount1 desc";
        DataSet dsSelMatrix = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strdsSelMatrix);
        Hashtable ht = new Hashtable();
        string RandSeqFrom = "";
        string RandSeqTo = "";
        string PubUnpubCondition = "";
        for (int i = 0; i < dsSelMatrix.Tables[0].Rows.Count; i++)
        {
            DataRow dr = dsSelMatrix.Tables[0].Rows[i];


            if (ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                RandSeqFrom = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + 1).ToString();
                RandSeqTo = (Convert.ToInt32(ht[dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()]) + Convert.ToInt32(dr["WordCount1"])).ToString();
            }
            else
            {
                RandSeqFrom = "1";
                RandSeqTo = dr["WordCount1"].ToString();
            }

            if ((dr["PubUnpub"].ToString()).Equals("P"))
            {
                PubUnpubCondition = "and PubUnp is null";
            }
            else if ((dr["PubUnpub"].ToString()).Equals("U"))
            {
                PubUnpubCondition = "and PubUnp='Y'";
            }
            string TextLevel = "";
            if (dr["SubLevel"].ToString().Equals("1"))
            {
                TextLevel = dr["Level"] + "/E";
            }
            else
            {
                TextLevel = dr["Level"] + "/D";
            }

            if (i < 2)
            {
                strquery = strquery + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + "   )  union ";
            }
            else
            {
                strquery1 = strquery1 + "(select '" + TextLevel + "' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + Session["ddlYear"] + " and sp.Eventid=" + Session["ddlEvent"] + " and sp.ProductGroupCode='" + Session["ddlProductGroup"] + "' and sp.ProductCode= '" + Session["ddlProduct"] + "' and ([RandSeqID] between " + RandSeqFrom + " and " + RandSeqTo + " ) " + PubUnpubCondition + " and sp.Level=" + dr["Level"] + " and sp.[Sub-level]=" + dr["SubLevel"] + " and sp.SetID=" + Session["ddlSet"] + "   ) union ";
            }
            if (!ht.ContainsKey(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString()))
            {
                ht.Add(dr["PubUnpub"].ToString() + dr["Level"].ToString() + dr["SubLevel"].ToString(), dr["WordCount1"]);
            }
        }
        strquery = strquery.Substring(0, strquery.Length - 6);
        strquery1 = strquery1.Substring(0, strquery1.Length - 6);
        strquery = strquery + " Order by sp.Phase,sp.[Sub-level]";
        strquery1 = strquery1 + " Order by sp.Phase,sp.[Sub-level]";

             
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;
        }

        rpAnswerKey.DataSource = dsJVB;
        rpAnswerKey.DataBind();

        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + Session["ddlSet"] + "_" + Session["ddlYear"] + "_" + Session["ddlProductGroup"] + "_" + Session["ddlProduct"] + "_TestP_05_AnswerKey_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;
        string pageTitleStuPh2 = "";
        pageTitleStuPh2 = "" + Session["ddlYear"] + " North South Foundation Regionals " + Session["PageTitleProductName"].ToString() + " Bee Phase I (Answer Key)";
        pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
        pg2 = new iTextSharp.text.Paragraph(" ");
        pdfDoc.Open();
        int ii = 0;
        int stupno = 1;

        if (rpAnswerKey.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            stupno++;
            pAnswerKey.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }

        // second Page
      
        // Judge Copy Phase1 & Phase2
        
        DataSet dsJVB1 = SqlHelper.ExecuteDataset(Application[cString].ToString(), CommandType.Text, strquery1);
        //adding serial number to dataTable
        DataColumn column1 = dsJVB1.Tables[0].Columns.Add("SNo", typeof(Int32));
        column1.SetOrdinal(0);// to put the column in position 0;
        int k1 = 16;
        foreach (DataRow dr in dsJVB1.Tables[0].Rows)
        {
            dr["SNo"] = k1;
            k1++;
        }
        rpAnswerKey1.DataSource = dsJVB1;
        rpAnswerKey1.DataBind();
        pdfDoc.NewPage();
        pdfDoc.Add(pg1);
        pdfDoc.Add(pg2);
        StringWriter sw11 = new StringWriter();
        HtmlTextWriter hw11 = new HtmlTextWriter(sw11);
        pAnswerKey1.RenderControl(hw11);
        StringReader sr11 = new StringReader(sw11.ToString());
        htmlparser.Parse(sr11);
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    #endregion
}