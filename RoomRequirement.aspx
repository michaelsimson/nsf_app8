﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="RoomRequirement.aspx.vb" Inherits="RoomRequirement" title="Room Requirements" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left" >
<asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
    <table id="RoomReqTable" runat="server" align="center">
        <tr>
            <td>
                Room Requirements By Purpose
            </td>
        </tr>
    </table>
    <table id="SelTable" runat="server">
        <tr>
           <td align="left" Width="100px">
                Event Year</td>
            <td>
                <asp:DropDownList ID="ddlEventYear" DataTextField="EventYear" DataValueField="EventYear"  AutoPostBack="true" runat="server" Width="150px">
                      </asp:DropDownList>
            </td>
            <td align="left" Width="100px">
                Event</td>
            <td>
                <asp:DropDownList ID="ddlEvent" DataTextField="Name" DataValueField="EventId"  AutoPostBack="true" runat="server" Width="150px">
                      </asp:DropDownList>
            </td>
            <td align="left" style="width: 77px">
            </td>
            <td Width="100px">
                Purpose</td>
             <td>
                <asp:DropDownList ID="ddlPurpose"  DataTextField="Purpose" DataValueField=""  AutoPostBack="true" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
             <td align="left" style="width: 77px">
            </td>
          
            <td Width="100px" id ="TdProductGroup" runat ="server" visible ="True">
                ProductGroup
                </td>
                 <td>
                <asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupId" AutoPostBack = "True" runat="server" Width="150px"  visible ="True">
                </asp:DropDownList>
            </td>
            <td align="left" style="width: 77px">
            </td>
            <td Width="100px" id= "TdPhase" runat ="server"  visible ="True"> 
                Phase</td>
           
            <td>
                <asp:DropDownList ID="ddlPhase" AutoPostBack ="true" runat="server" Width="150px"  visible ="True">
                    <%--<asp:ListItem Value="0">Select</asp:ListItem>--%>
                    <asp:ListItem Value="1">Phase1</asp:ListItem>
                    <asp:ListItem Value="2">Phase2</asp:ListItem>
                    <asp:ListItem Value="3">Phase3</asp:ListItem>
                </asp:DropDownList>
            </td>
             <td align="left" style="width: 77px">
                 <br />
            </td>
            <td id="TdDay" runat="server" visible ="False" Width="75px">Day </td>
            <td> 
            <asp:DropDownList ID="ddlDay" AutoPostBack ="true" runat="server" Width="150px"  visible ="False" >
                    <%--<asp:ListItem Value="0">Select</asp:ListItem>--%>
                    <asp:ListItem Value="1">Day1</asp:ListItem>
                    <asp:ListItem Value="2">Day2</asp:ListItem>
                    <asp:ListItem Value="3">Day3</asp:ListItem>
                </asp:DropDownList>
            
            </td> 
        </tr>
        
        <tr><td><asp:Button ID="BtnCopyFrom" runat="server"  Text="Copy To" Width="142px" /></td>
             <td><asp:Button ID="BtnCopyPrevYear" runat="server"  Text="Copy From Previous Year" Width="175px"  Visible="false" /></td>
             <td colspan="5" align="center"><asp:Label ID="lblYearErr" runat="server" Text="" ForeColor="Red"></asp:Label></td>
             <td><asp:Button ID="btnDisplay" runat="server" Text="Display Data" /></td>
        </tr>
       
        
        <tr id="TrCopyFrom" visible ="false" runat="server" >
           <td align="left" Width="100px">
                Event Year</td>
            <td>
                <asp:DropDownList ID="ddlEventYear1" DataTextField="EventYear" DataValueField="EventYear"  AutoPostBack="true" runat="server" Width="150px">
                      </asp:DropDownList>
            </td>
        <td align="left" Width="100px">
                Event</td>
            <td>
                <asp:DropDownList ID="ddlEvent1" DataTextField="Name" DataValueField="EventId"  
                    AutoPostBack="true" runat="server" Width="150px">
                      </asp:DropDownList>
            </td>
            <td align="left" style="width: 77px">
            </td>
            <td Width="100px">
                Purpose</td>
             <td>
                <asp:DropDownList ID="ddlPurpose1" DataTextField="Purpose" DataValueField=""  
                     AutoPostBack="true" runat="server" Width="150px">
                </asp:DropDownList>
            </td>
             <td align="left" style="width: 77px">
            </td>
          
            <td Width="100px" id="TdProductGroup1" runat="server" visible ="False" >
                ProductGroup</td>
            
            <td>
                <asp:DropDownList ID="ddlProductGroup1" DataTextField="Name" 
                    DataValueField="ProductGroupId" AutoPostBack = "True" runat="server" Width="150px" visible ="False">
                </asp:DropDownList>
            </td>
            <td align="left" style="width: 77px">
            </td>
            <td Width="100px" id="TdPhase1" runat="server" visible ="False"> 
                Phase</td>
             <td>
                <asp:DropDownList ID="ddlPhase1" AutoPostBack ="true" runat="server" 
                    Width="150px" Visible ="False">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Phase1</asp:ListItem>
                    <asp:ListItem Value="2">Phase2</asp:ListItem>
                    <asp:ListItem Value="3">Phase3</asp:ListItem>
                </asp:DropDownList>
            </td>
             <td align="left" style="width: 77px">
            </td>
            <td id="TdDay1" runat="server" visible ="False" Width="75px"> Day </td>
            <td> 
            <asp:DropDownList ID="ddlDay1" AutoPostBack ="true" runat="server" Width="150px"  visible ="False">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="1">Day1</asp:ListItem>
                    <asp:ListItem Value="2">Day2</asp:ListItem>
                    <asp:ListItem Value="3">Day3</asp:ListItem>
                </asp:DropDownList>
            
            </td> 
            </tr>
    </table>
    
    <table id="MainTable" runat="server" align ="center" border="1">
                   <tr><td colspan ="2" align="center"><asp:Label ForeColor="Green" ID="lblPurpose" runat="server"></asp:Label> </td></tr> 
                     <tr id="TrRoomGuide" runat="server">
                        <td>
                            Room Guides</td>
                        <td>
                            <asp:DropDownList ID="ddlRoomGuide" DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrRoomMC" runat="server">
                        <td>
                            RoomMC</td>
                        <td>
                            <asp:DropDownList ID="ddlRoomMc" DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrPronouncer" runat="server">
                        <td>
                            Pronouncer</td>
                        <td>
                            <asp:DropDownList ID="ddlPronouncer" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrChiefJudge" runat="server"> 
                        <td>
                            ChiefJudge</td>
                        <td>
                            <asp:DropDownList ID="ddlChiefJudge" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrAssociateJudge" runat="server">
                        <td>
                            AssociateJudge</td>
                        <td>
                            <asp:DropDownList ID="ddlAssociateJudge" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id= "TrLaptopJudge" runat="server">
                        <td>
                            LaptopJudge</td>
                        <td>
                            <asp:DropDownList ID="ddlLaptopJudge"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id ="TrDictHandler" runat="server">
                        <td>
                            DictHandler</td>
                        <td>
                            <asp:DropDownList ID="ddlDictHandler"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrTimer" runat="server">
                        <td>
                            Timer</td>
                        <td>
                            <asp:DropDownList ID="ddlTimer" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrProctor" runat="server">
                        <td>
                            Proctor</td>
                        <td>
                            <asp:DropDownList ID="ddlProctor"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrGrader" runat="server">
                        <td>
                            Grader</td>
                        <td>
                            <asp:DropDownList ID="ddlGrader"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrPodium" runat="server">
                        <td>
                            Podium</td>
                        <td>
                            <asp:DropDownList ID="ddlPodium" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrPodiumMic" runat="server">
                        <td>
                            PodiumMic</td>
                        <td>
                            <asp:DropDownList ID="ddlPodiumMic"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrPodiumLaptop" runat="server">
                        <td>
                            PodiumLaptop</td>
                        <td>
                            <asp:DropDownList ID="ddlPodiumLaptop" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrProjector" runat="server"> 
                        <td>
                            Projector</td>
                        <td>
                            <asp:DropDownList ID="ddlProjector" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrJudgesTable" runat="server">
                        <td>
                            JudgesTable</td>
                        <td>
                            <asp:DropDownList ID="ddlJudgesTable" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrJudgeChairs" runat="server">
                        <td>
                            JudgeChairs</td>
                        <td>
                            <asp:DropDownList ID="ddlJudgeChairs" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrCJMic" runat="server">
                        <td>
                            CJMic</td>
                        <td>
                            <asp:DropDownList ID="ddlCJMic"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrChildMic" runat="server">
                        <td>
                            ChildMic</td>
                        <td>
                            <asp:DropDownList ID="ddlChildMic"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrAudioMixer" runat="server">
                        <td>
                            AudioMixer</td>
                        <td>
                            <asp:DropDownList ID="ddlAudioMixer" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrChildMicStand" runat="server">
                        <td>
                            ChildMicStand</td>
                        <td>
                            <asp:DropDownList ID="ddlChildMicStand"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrDesktopMicStand" runat="server">
                        <td>
                            DesktopMicStand</td>
                        <td>
                            <asp:DropDownList ID="ddlDesktopMicStand"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrAudioWire" runat="server">
                        <td>
                            AudioWire</td>
                        <td>
                            <asp:DropDownList ID="ddlAudioWire"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrExtraMikes" runat="server">
                        <td>
                            ExtraMikes</td>
                        <td>
                            <asp:DropDownList ID="ddlExtraMikes" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrInternetAccess" runat="server">
                        <td>
                            InternetAccess</td>
                        <td>
                            <asp:DropDownList ID="ddlInternetAccess"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrMC" runat="server">
                        <td>
                            MC</td>
                        <td>
                            <asp:DropDownList ID="ddlMC" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrTeamLead" runat="server">
                        <td>
                            TeamLead</td>
                        <td>
                            <asp:DropDownList ID="ddlTeamLead" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrLaptop" runat="server">
                        <td>
                            Laptop</td>
                        <td>
                            <asp:DropDownList ID="ddlLaptop"  DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrTable" runat="server"> 
                        <td>
                            Table</td>
                        <td>
                            <asp:DropDownList ID="ddlTable" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrChair" runat="server">
                        <td>
                            Chair</td>
                        <td>
                            <asp:DropDownList ID="ddlChair" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id= "TrMic" runat="server">
                        <td>
                            MicroPhone</td>
                        <td>
                            <asp:DropDownList ID="ddlMicroPhone" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrMicStand" runat="server">
                        <td>
                            MicStand</td>
                        <td>
                            <asp:DropDownList ID="ddlMicStand" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                       <tr id="TrRegGuide" runat="server">
                        <td>
                            RegGuide</td>
                        <td>
                            <asp:DropDownList ID="ddlRegGuide" DataTextField="ddlText" DataValueField="ddlValue"  runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                     <tr id="TrDeskGuide" runat="server">
                        <td>
                            DeskGuide</td>
                        <td>
                            <asp:DropDownList ID="ddlDeskGuide" DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    <tr id="TrFoodServe" runat="server">
                        <td>
                            FoodServe</td>
                        <td>
                            <asp:DropDownList ID="ddlFoodServe" DataTextField="ddlText" DataValueField="ddlValue" runat="server" Width="100px">
                            </asp:DropDownList>
                        </td>
                    </tr>
                    </table>
           
       <table id="TableAddUpdtCncl" runat="server" visible ="False" align="center"> 
       <tr><td align="center"></td></tr> 
         <tr><td align="center">
         <asp:Button ID="BtnAddUpdate" OnClick="BtnAddUpdate_Click" runat="server" Text="Add/Update" />
         <asp:Button ID="BtnCancel"  OnClick="BtnCancel_Click" runat="server"  Text="Cancel" />
         </td></tr>
         
         <tr><td align="center" ><asp:Label ForeColor ="Red" ID="lblAddUpdate" runat="server" Visible="false"></asp:Label></td></tr>
         <tr><td><asp:DataGrid ID="DGDisplay" runat="server"></asp:DataGrid></td></tr>
     </table>
</asp:Content>

