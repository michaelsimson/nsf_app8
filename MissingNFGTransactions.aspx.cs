﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;

public partial class MissingNFGTransactions_ : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            PopulateEvent(ddlEvent, true);
            PopulateYear(ddlYear, true);
        }
        //LoadGrid();

    }

    # region Private Functions
    private void PopulateYear(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        int i=0;
        ddlObject.Items.Clear();
        do 
        {
            ddlObject.Items.Insert(i, new ListItem(DateTime.Now.AddYears(-i).Year.ToString(), DateTime.Now.AddYears(-i).Year.ToString()));
            i++;
        }
        while (i < 4);

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Year]", "-1"));
        }
    }
    #endregion Private Functions

    #region Gridview Method
    private void PopulateEvent(DropDownList ddlObject, bool blnCreateEmptyItem)
    {
        ArrayList list = new ArrayList();
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;

            list.Add(new ListItem("Chapter Contests", "2"));
            list.Add(new ListItem("National Finals", "1"));
            list.Add(new ListItem("Workshops", "3"));
            list.Add(new ListItem("Coaching", "13"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
               
        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Event]", "-1"));
        }

    }

    protected void gvLog_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        lblMessage.Text = "";
        try
        {
            if (e.CommandArgument != null)
            {
                if (int.TryParse(e.CommandArgument.ToString(), out index))
                {
                    bool dUpdate = false;
                    index = int.Parse((string)e.CommandArgument);
                    if (e.CommandName == "Update")
                    {
                        int dnAmt = int.Parse(gvLog.Rows[index].Cells[16].Text);
                        if (dnAmt > 0){dUpdate = true;}
                        else{dUpdate = false;}

                        hdnPaydate.Value = gvLog.Rows[index].Cells[6].Text;
                        hdnPayRef.Value = gvLog.Rows[index].Cells[19].Text;
                        hdnPayNote.Value = gvLog.Rows[index].Cells[24].Text;

                        if (UpdateNfg(int.Parse(gvLog.Rows[index].Cells[2].Text), int.Parse(gvLog.Rows[index].Cells[1].Text), int.Parse(gvLog.Rows[index].Cells[4].Text), gvLog.Rows[index].Cells[19].Text, dUpdate))
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Green;                            
                            LoadGrid(int.Parse(ddlYear.SelectedValue.ToString()), int.Parse(ddlEvent.SelectedValue.ToString()));
                            lblMessage.Text = "Successfully updated.";
                            
                        }
                        else
                        {
                            lblMessage.ForeColor = System.Drawing.Color.Red;
                            lblMessage.Text = "Error Saving Record.";
                        }

                    }
                }
            }
        }
        catch (Exception ex)
        {
            Response.Write(ex.Message.ToString());
        }
    }

    protected void gvLog_RowUpdating(object sender, GridViewUpdateEventArgs e)
    {

    }

    protected void gvLog_RowUpdated(object sender, GridViewUpdatedEventArgs e)
    {

    }

    protected void gvLog_SelectedIndexChanged(object sender, EventArgs e)
    {

    }

    #endregion Gridview Method

    #region Data Access
    protected void LoadGrid(int EYear,int Eid)
    {
        try
        {
            lblMessage.Text = "";
            string conn = Application["connectionString"].ToString();
            DataSet dsLog = SqlHelper.ExecuteDataset(Application["connectionString"].ToString(), CommandType.Text, "select * from CCSubmitLog C where EventId=" + Eid.ToString() + " and EventYear=" + EYear.ToString() + " and not exists (select * from NFG_Transactions where EventYear=C.EventYear and approval_code=CONVERT(varchar(255),C.CCSubmitLogID) and EventId=C.EventId)");

            if (dsLog.Tables[0].Rows.Count > 0)
            {
                gvLog.DataSource = dsLog;
                gvLog.DataBind();
            }
            else
            {
                gvLog.DataSource = null;
                gvLog.DataBind();
                lblMessage.ForeColor = System.Drawing.Color.Red;
                lblMessage.Text = "Record not found for selected criteria.";
            }
        }
        catch (Exception ex)
        {

        }        
    }
    private bool UpdateNfg(int MemberId,int CCID, int EvId ,string PayRef, bool donation)
    {
        try
        {
        string conn = Application["connectionString"].ToString();

        string sqlNfg="INSERT INTO NFG_Transactions ([Last Name],[First Name],[Address],[City],[State],[Zip],[DonorType],[ChapterId],";
        sqlNfg += "[Contribution Date],[Payment Date],[TotalPayment],[Contribution Amount],[Fee],[LateFee],[MealsAmount],[Email (ok to contact)],";
        sqlNfg += "[approval_status],[approval_code],[asp_session_id],[EventId],[MemberId],[EventYear],[PaymentNotes],";
        sqlNfg += "[event_for],[Source Website],[Donation Type],[Status],[Designated Project])";
        sqlNfg += "SELECT I.[LastName],I.[FirstName],I.[Address1],I.[City],I.[State],I.[Zip],I.[DonorType],I.[ChapterID],";
        sqlNfg += "C.[PaymentDate],C.[PaymentDate],C.[Amount],C.[Donation],C.[Fee],C.[LateFee],C.[Meal],C.[ClientEmail],";
        sqlNfg += "C.[StatusReturn],C.[CCSubmitLogID],C.[PaymentReference],C.[EventID],C.[MemberID],C.[EventYear],C.[PaymentNotes],";
        sqlNfg += "E.[EventCode],'NSF','Credit Card','Complete','INScholar' FROM IndSpouse I, CCSubmitLog C, Event E";
        sqlNfg += " WHERE I.AutoMemberID=" + MemberId.ToString() + " AND C.CCSubmitLogID =" + CCID.ToString() + " AND E.EventId =" + EvId.ToString();
       
        string sqlDnfo = "if not exists(select * from DonationsInfo where TRANSACTION_NUMBER ='"+ hdnPayRef.Value.ToString()  + "') begin ";
        sqlDnfo += "INSERT INTO Donationsinfo([DonationNumber],[MEMBERID],[AMOUNT],[TRANSACTION_NUMBER],[DonationDate],[CreateDate],[EventId],[EventYear],";
        sqlDnfo += "[EVENT],[ChapterId],[DonorType],[METHOD],[PURPOSE],[STATUS],[DeletedFlag],[TaxDeduction]) ";
        sqlDnfo += "SELECT (SELECT MAX([DonationNumber]) + 1 from DonationsInfo), C.[MemberID],C.[Donation],C.[PaymentReference],C.[PaymentDate],C.[PaymentDate],C.[EventID],C.[EventYear],";
        sqlDnfo += "E.[EventCode],I.[ChapterID],'IND','Credit Card','INScholar','Completed','No','100' ";
        sqlDnfo += "FROM CCSubmitLog C,Event E, IndSpouse I";
        sqlDnfo += " WHERE I.AutoMemberID=" + MemberId.ToString() + " AND C.CCSubmitLogID =" + CCID.ToString() + " AND E.EventId =" + EvId.ToString() +" end";
        

        string sqlCR = "UPDATE CR SET CR.[PaymentDate]='" + hdnPaydate.Value.ToString () + "',CR.[PaymentMode]='Credit Card',CR.[PaymentNotes]='" +hdnPayNote.Value.ToString () +"',CR.[PaymentReference]='" + hdnPayRef.Value.ToString() +"', CR.Approved='Y' ";
        sqlCR += "FROM CoachReg CR INNER JOIN CALSIGNUP C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.ProductID=C.ProductID ";
        sqlCR += "and ((Case when C.ProductGroupCode not in('UV') then CR.Level end)=C.Level Or (Case when C.ProductGroupCode in('UV') then CR.Level end)Is null) AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo ";
        sqlCR += "WHERE CR.[PaymentReference] is NULL  and '" + hdnPaydate.Value.ToString () + "' < C.Enddate and CR.Approved='N' and CR.[PMemberID]=" + MemberId.ToString() + " AND CR.EVENTID=13 And  CR.Eventyear >= " + DateTime.Now.Year.ToString();
       
        string sqlReg = "UPDATE Registration SET [PaymentDate]='" + hdnPaydate.Value.ToString() + "', [PaymentMode]='Credit Card', ";
        sqlReg += "[PaymentNotes]='"+ hdnPayNote.Value.ToString () +"', [PaymentReference]='" + hdnPayRef.Value.ToString () +"' WHERE [PaymentReference] is NULL and [MemberID]=" + MemberId.ToString() ;
        sqlReg += " AND EVENTID=3 and EventDate >= '" + hdnPaydate.Value.ToString() + "' And  Eventyear >=" + DateTime.Now.Year.ToString();
     

        string sqlCont = " UPDATE C  SET C.[PaymentDate]='" + hdnPaydate.Value.ToString() + "', C.[PaymentMode]='Credit Card', C.[PaymentNotes]='"+hdnPayNote.Value.ToString ()+"', C.[PaymentReference]='" + hdnPayRef.Value.ToString() + "' ";
        sqlCont+= " From Contestant C INNER JOIN Contest b ON b.ContestID = C.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=b.Contest_Year and Ex.EventID=b.EventId and Ex.ChildNumber=C.ChildNumber ";
        sqlCont += " WHERE C.[PaymentReference] Is NULL And C.[ParentID]=" + MemberId.ToString() + " And  C.Contestyear >=" + DateTime.Now.Year.ToString();

        switch (ddlEvent.SelectedValue.ToString())
        {
            case "1":
            case "2":
                {
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlNfg);
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlCont);
                    if (donation == true){SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlDnfo);}
                }
                break;
            case "3":
                {
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlNfg);
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlReg);
                    if (donation == true) { SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlDnfo); }
                }
                break;
            case "13":
                {
                    SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlNfg);
                    SqlHelper.ExecuteScalar(conn, CommandType.Text,sqlCR);
                    if (donation == true) { SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlDnfo); }
                }
                break;       
        }

        return true;
        }
       catch (Exception ex)
        {
           return false;
        }

    }
    #endregion Data Access

    protected void bttnsubmit_Click(object sender, EventArgs e)
    {
        lblMessage.ForeColor = System.Drawing.Color.Green;
        lblMessage.Text = "";
        if (ddlEvent.SelectedValue.ToString() != "-1" & ddlYear.SelectedValue.ToString() != "-1")
        {
            LoadGrid(int.Parse(ddlYear.SelectedValue.ToString()), int.Parse(ddlEvent.SelectedValue.ToString()));
        }
        else
        {
            lblMessage.ForeColor = System.Drawing.Color.Red;
            lblMessage.Text = "Please select Year and Event.";
        }

    }
}
