
'*****************************************************************************
' Copyright 2003 LinkPoint International, Inc. All Rights Reserved.
' 
' This software is the proprietary information of LinkPoint International, Inc.  
' Use is subject to license terms.
'
'******************************************************************************    

Namespace VRegistration
    Public Class RETURN1
        Inherits LinkPointAPI_cs.LinkPointTxn_Page

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub

        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            If IsPostBack Then
                ' Parse form data
                ParseFormData()
                ' process order
                ProcessOrder()
            End If

        End Sub

        Private Sub ProcessOrder()

            ' create order
            Dim op As LinkPointTransaction.LPOrderPart
            Dim order As LinkPointTransaction.LPOrderPart

            order = LinkPointTransaction.LPOrderFactory.createOrderPart("order")
            ' create a part we will use to build the order
            op = LinkPointTransaction.LPOrderFactory.createOrderPart()

            ' Build 'orderoptions'
            ' For a test, set result to GOOD, DECLINE, or DUPLICATE
            op.put("result", "GOOD")
            op.put("ordertype", "CREDIT")
            ' add 'orderoptions to order
            order.addPart("orderoptions", op)


            ' Build 'merchantinfo'
            op.clear()
            op.put("configfile", configfile)
            ' add 'merchantinfo to order
            order.addPart("merchantinfo", op)


            ' Build 'creditcard'
            op.clear()
            op.put("cardnumber", cardnumber)
            op.put("cardexpmonth", expmonth)
            op.put("cardexpyear", expyear)
            ' add 'creditcard to order
            order.addPart("creditcard", op)


            ' Build 'payment'
            op.clear()
            op.put("chargetotal", total)
            ' add 'payment to order
            order.addPart("payment", op)

            ' Add oid
            op.clear()
            op.put("oid", oid)
            ' add 'transactiondetails to order
            order.addPart("transactiondetails", op)

            ' create transaction object	
            LPTxn = New LinkPointTransaction.LinkPointTxn()

            ' get outgoing XML from 'order' object
            Dim outXml As String = order.toXML()

            ' Call LPTxn
            Dim resp As String = LPTxn.send(keyfile, host, port, outXml)

            'Store transaction data on Session and redirect
            Session("outXml") = outXml
            Session("resp") = resp
            Server.Transfer("afs_status.aspx")
        End Sub


    End Class
End Namespace
