'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/AspiringVolunteers.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page enables the Chapter Co-ordinators and the National 
'           Co-ordinators to view the Persons Interested in Volunteering.  
'
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		11/06/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient

Imports Microsoft.ApplicationBlocks.Data

Imports NorthSouth.BAL


Namespace VRegistration

Partial Class AspiringVolunteers
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label
    Dim dsVolunteerRole As New DataSet
    Dim ddlVolunteerRole As New DropDownList

    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        '*** Populate Volunteer Role DropDowns
        If Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("login.aspx")
        End If

        If Not Page.IsPostBack() Then

            '*** Populate Volunteer Role Dropdown
            If ViewState("VolunteerRole") Is Nothing Then
                dsVolunteerRole = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetVolunteerTasks")
                ViewState("VolunteerRole") = dsVolunteerRole
            End If

            '*** Populate Chapter DropDown
            Dim dsChapters As New DataSet
            Dim objChapters As New Chapter
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
            If dsChapters.Tables.Count > 0 Then
                ddlNSFChapter.DataSource = dsChapters.Tables(0)
                ddlNSFChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterLocation").ToString
                ddlNSFChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlNSFChapter.DataBind()
            End If


            If Not Session("ChapterID") Is Nothing Then
                ddlNSFChapter.Items.FindByValue(Convert.ToInt32(Session("ChapterID"))).Selected = True
                ddlNSFChapter.Enabled = False
                LoadDataGrid(Convert.ToInt32(Session("ChapterID")))
            Else
                ddlNSFChapter.Enabled = True
            End If

        End If
    End Sub
    Public Sub LoadDataGrid(ByVal ChapterID As Integer)
            Dim objIndSpouse As New IndSpouse10
        Dim dsIndSpouse As New DataSet
        Dim strWhere As String
        strWhere = "Chapter = '" & ChapterID & "' AND VolunteerFlag = 'Yes'"
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, strWhere)
        If dsIndSpouse.Tables.Count > 0 Then
            If ViewState("OrderBy") Is Nothing Then
                ViewState("OrderBy") = "AutoMemberID"
            End If

            dsIndSpouse.Tables(0).DefaultView.Sort = ViewState("OrderBy")
            dgVolunteerList.DataSource = dsIndSpouse.Tables(0)
            dgVolunteerList.DataBind()
        End If
    End Sub
    Private Sub dgVolunteerList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVolunteerList.ItemDataBound

        If ViewState("VolunteerRole").Tables.Count > 0 Then
            ddlVolunteerRole.DataSource = ViewState("VolunteerRole").Tables(0)
            ddlVolunteerRole.DataTextField = ViewState("VolunteerRole").Tables(0).Columns("TaskDescription").ToString
            ddlVolunteerRole.DataValueField = ViewState("VolunteerRole").Tables(0).Columns("VolunteerTaskID").ToString
            ddlVolunteerRole.DataBind()
        End If

        Select Case e.Item.ItemType

            Case ListItemType.Item, ListItemType.AlternatingItem
                CType(e.Item.FindControl("hLinkVolunteer"), HyperLink).NavigateUrl = "Registration.aspx?VolunteerID=" & e.Item.DataItem("AutoMemberID").ToString
                CType(e.Item.FindControl("hLinkVolunteer"), HyperLink).Text = e.Item.DataItem("FirstName") & " " & e.Item.DataItem("LastName")

                If Not (IsDBNull(e.Item.DataItem("VolunteerRole1"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("lblVolunteerRole1"), Label).Text = ddlVolunteerRole.Items.FindByValue(e.Item.DataItem("VolunteerRole1")).Text
                    End If
                End If
                If Not (IsDBNull(e.Item.DataItem("VolunteerRole2"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("lblVolunteerRole2"), Label).Text = ddlVolunteerRole.Items.FindByValue(e.Item.DataItem("VolunteerRole2")).Text
                    End If
                End If
                If Not (IsDBNull(e.Item.DataItem("VolunteerRole3"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("lblVolunteerRole3"), Label).Text = ddlVolunteerRole.Items.FindByValue(e.Item.DataItem("VolunteerRole3")).Text
                    End If
                End If
            Case ListItemType.EditItem
                CType(e.Item.FindControl("ddlVolunteerRole1"), DropDownList).DataSource = ViewState("VolunteerRole").Tables(0)
                CType(e.Item.FindControl("ddlVolunteerRole1"), DropDownList).DataTextField = ViewState("VolunteerRole").Tables(0).Columns("TaskDescription").ToString
                CType(e.Item.FindControl("ddlVolunteerRole1"), DropDownList).DataValueField = ViewState("VolunteerRole").Tables(0).Columns("VolunteerTaskID").ToString
                CType(e.Item.FindControl("ddlVolunteerRole1"), DropDownList).DataBind()

                CType(e.Item.FindControl("ddlVolunteerRole2"), DropDownList).DataSource = ViewState("VolunteerRole").Tables(0)
                CType(e.Item.FindControl("ddlVolunteerRole2"), DropDownList).DataTextField = ViewState("VolunteerRole").Tables(0).Columns("TaskDescription").ToString
                CType(e.Item.FindControl("ddlVolunteerRole2"), DropDownList).DataValueField = ViewState("VolunteerRole").Tables(0).Columns("VolunteerTaskID").ToString
                CType(e.Item.FindControl("ddlVolunteerRole2"), DropDownList).DataBind()

                CType(e.Item.FindControl("ddlVolunteerRole3"), DropDownList).DataSource = ViewState("VolunteerRole").Tables(0)
                CType(e.Item.FindControl("ddlVolunteerRole3"), DropDownList).DataTextField = ViewState("VolunteerRole").Tables(0).Columns("TaskDescription").ToString
                CType(e.Item.FindControl("ddlVolunteerRole3"), DropDownList).DataValueField = ViewState("VolunteerRole").Tables(0).Columns("VolunteerTaskID").ToString
                CType(e.Item.FindControl("ddlVolunteerRole3"), DropDownList).DataBind()

                If Not (IsDBNull(e.Item.DataItem("VolunteerRole1"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("ddlVolunteerRole1"), DropDownList).SelectedValue = e.Item.DataItem("VolunteerRole1")
                    End If
                End If
                If Not (IsDBNull(e.Item.DataItem("VolunteerRole2"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("ddlVolunteerRole2"), DropDownList).SelectedValue = e.Item.DataItem("VolunteerRole2")
                    End If
                End If
                If Not (IsDBNull(e.Item.DataItem("VolunteerRole3"))) Then
                    If e.Item.DataItem("VolunteerRole1") <> 0 Then
                        CType(e.Item.FindControl("ddlVolunteerRole3"), DropDownList).SelectedValue = e.Item.DataItem("VolunteerRole3")
                    End If
                End If
        End Select
    End Sub
    Private Sub dgVolunteerList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgVolunteerList.SelectedIndexChanged
        Session("VolunteerID") = dgVolunteerList.DataKeys(dgVolunteerList.SelectedIndex)
    End Sub
    Private Sub ddlNSFChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNSFChapter.SelectedIndexChanged

        '*** Populate Volunteer DataGrid
        Dim strChapterID As String
        Dim strWhere As String
        strChapterID = ddlNSFChapter.SelectedValue
        strWhere = " Chapter = " & strChapterID.ToString & "' AND VolunteerFlag = 'Yes'"
        LoadDataGrid(strWhere)

    End Sub

    Private Sub dgVolunteerList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgVolunteerList.SortCommand
        If e.SortExpression = ViewState("OrderBy") Then
            ViewState("OrderBy") = e.SortExpression & " DESC"
        Else
            ViewState("OrderBy") = e.SortExpression
        End If
        LoadDataGrid(Convert.ToInt32(Session("ChapterID")))
    End Sub

    Private Sub dgVolunteerList_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVolunteerList.EditCommand
        Dim i As Integer
        Dim intIndexKey As Integer
            'Dim strChapterID As String
            'Dim strWhere As String

        intIndexKey = dgVolunteerList.DataKeys(e.Item.ItemIndex)
        dgVolunteerList.ShowFooter = False
        LoadDataGrid(ddlNSFChapter.SelectedItem.Value)
        For i = 0 To dgVolunteerList.DataKeys.Count - 1
            If (dgVolunteerList.DataKeys(i) = intIndexKey) Then
                dgVolunteerList.EditItemIndex = i
                Exit For
            End If
        Next
        Page.DataBind()
    End Sub

    Private Sub dgVolunteerList_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVolunteerList.CancelCommand
        dgVolunteerList.EditItemIndex = -1
        LoadDataGrid(ddlNSFChapter.SelectedItem.Value)
    End Sub

    Private Sub dgVolunteerList_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgVolunteerList.UpdateCommand
        dgVolunteerList.EditItemIndex = -1
        LoadDataGrid(ddlNSFChapter.SelectedItem.Value)
    End Sub
End Class
End Namespace

