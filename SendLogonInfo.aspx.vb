'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/SendLogonInfo.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page allows the User to Send Emails to Parents 
'
'           Will utilize the following stored procedures
'           1) NSF\GetParentList
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		01/30/2006	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data


Namespace VRegistration
    Partial Class SendLogonInfo
        Inherits System.Web.UI.Page
        Dim cnTemp As SqlConnection
        Const FromAddress As String = "nsfcontests@gmail.com"

#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents DataGrid1 As System.Web.UI.WebControls.DataGrid


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

        End Sub

        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
            Try
                Dim client As New SmtpClient()
                Dim email As New MailMessage

                'Build Email Message
                Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
                client.Host = host

                email.From = New MailAddress("nsfcontests@gmail.com")
                email.To.Add(sMailTo)
                email.Subject = sSubject
                email.IsBodyHtml = True
                email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
                email.Body = sBody
                client.Send(email)
                Return True

            Catch ex As Exception
                Return False
            End Try

        End Function

        Dim objRandom As New System.Random( _
          CType(System.DateTime.Now.Ticks Mod System.Int32.MaxValue, Integer))

        Public Function GetRandomNumber( _
          Optional ByVal Low As Integer = 1, _
          Optional ByVal High As Integer = 999) As Integer
            ' Returns a random number,
            ' between the optional Low and High parameters
            Return objRandom.Next(Low, High + 1)
        End Function

        Private Sub btnSend_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSend.Click
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim dsParents As New DataSet
            Dim tblParents() As String = {"Parents"}
            'Dim emailbody As String
            'Dim re As StreamReader
            Dim strInvalidEmails As New StringBuilder
            Dim strEmailFailure As New StringBuilder
            Dim strEmailDuplicate As New StringBuilder

            '*** Populate Parent Data

            'SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "GetParentList", dsParents, tblParents)

            'If dsParents.Tables.Count > 0 Then
            '    For Each drParent As DataRow In dsParents.Tables(0).Rows
            '        If drParent("EMail") <> "" Then

            '            If Regex.IsMatch(drParent("EMail").ToString.Trim, "\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*", RegexOptions.IgnoreCase) Then
            '                Dim param(12) As SqlParameter
            '                param(0) = New SqlParameter("@ChapterID", drParent("ChapterID"))
            '                param(1) = New SqlParameter("@date_expires", DateAdd("y", 1, Now))
            '                param(2) = New SqlParameter("@date_added", Now)
            '                param(3) = New SqlParameter("@last_login", "1900/01/01")
            '                param(4) = New SqlParameter("@date_updated", "1900/01/01")
            '                param(5) = New SqlParameter("@view_tasks", "Yes")
            '                param(6) = New SqlParameter("@user_name", drParent("FirstName") & GetRandomNumber())
            '                param(7) = New SqlParameter("@parent_email", DBNull.Value)
            '                param(8) = New SqlParameter("@active", "Yes")
            '                param(9) = New SqlParameter("@user_email", drParent("Email").ToString)
            '                param(10) = New SqlParameter("@user_pwd", drParent("LastName") & GetRandomNumber())
            '                param(11) = New SqlParameter("@user_role", "Parent")
            '                Try
            '                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_AddNSFUser", param)
            '                Catch
            '                    strEmailDuplicate.Append("Email Duplicate Not Send to:" & drParent("EMail") & ", " & drParent("LastName").ToString & ", " & drParent("LastName").ToString & ", " & drParent("ChapterID").ToString & vbCrLf)
            '                Finally
            '                    Server.ClearError()
            '                End Try

            '                're = File.OpenText(Server.MapPath("Parent_Logon_email.htm"))
            '                'emailbody = re.ReadToEnd
            '                're.Close()
            '                'emailbody = emailbody.Replace("[LASTNAME]", drParent("LastName").ToString)
            '                'emailbody = emailbody.Replace("[FIRSTNAME]", drParent("FirstName").ToString)
            '                'emailbody = emailbody.Replace("[USERID]", param(9).Value.ToString)
            '                'emailbody = emailbody.Replace("[PASSWORD]", param(10).Value.ToString)

            '                'If SendEmail("Online Registration for 2007 NSF Regionals /  Very Important", emailbody, drParent("EMail")) = False Then

            '                '    strEmailFailure.Append("Email Not Send to:" & drParent("EMail") & ", " & drParent("LastName").ToString & ", " & drParent("LastName").ToString & ", " & drParent("ChapterID").ToString & vbCrLf)

            '                'End If
            '            Else
            '                strInvalidEmails.Append("Invalid Email" & drParent("EMail") & ", " & drParent("LastName").ToString & ", " & drParent("LastName").ToString & ", " & drParent("ChapterID").ToString & vbCrLf)
            '            End If
            '        End If
            '    Next
            'End If
            'lblInvalidMailAddress.Text = strInvalidEmails.ToString
            'lblEMailError.Text = strEmailFailure.ToString
            'lblDuplicate.Text = strEmailDuplicate.ToString
            'strInvalidEmails = Nothing
            'strEmailFailure = Nothing
            'strEmailDuplicate = Nothing
            CorrectionEMail()
        End Sub

        Private Sub CorrectionEMail()
            Dim emailbody As String
            Dim re As StreamReader

            Dim dsLogins As New DataSet
            Dim tblLogins() As String = {"Logins"}
            Dim conn As New SqlConnection(Application("ConnectionString"))

            SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetEmailIDPasswordList", dsLogins, tblLogins)
            For Each drLogins As DataRow In dsLogins.Tables(0).Rows
                If drLogins("user_email") <> "" Then
                    re = File.OpenText(Server.MapPath("Parent_Logon_email.htm"))
                    emailbody = re.ReadToEnd
                    re.Close()
                    emailbody = emailbody.Replace("[USERID]", drLogins("user_email").ToString)
                    emailbody = emailbody.Replace("[PASSWORD]", drLogins("user_pwd").ToString)

                    If SendEmail("Online Registration for 2007 NSF Regionals /  Very Important", emailbody.ToString, drLogins("user_email")) = False Then

                        Response.Write("Email Not Send to:" & drLogins("user_email") & ", " & drLogins("chapterID").ToString & vbCrLf)
                    Else
                        Response.Write("Email Send to:" & drLogins("user_email") & ", " & drLogins("chapterID").ToString & vbCrLf)
                    End If

                End If
            Next


        End Sub

        Private Sub SendSpouseNonEmail()
            Dim emailbody As String
            Dim re As StreamReader

            Dim dsLogins As New DataSet
            Dim tblLogins() As String = {"Logins"}
            Dim conn As New SqlConnection(Application("ConnectionString"))

            SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetSpouseEmailNoneList", dsLogins, tblLogins)
            For Each drLogins As DataRow In dsLogins.Tables(0).Rows
                If drLogins("EMailID") <> "" Then
                    re = File.OpenText(Server.MapPath("SpouseEmailNone.htm"))
                    emailbody = re.ReadToEnd
                    re.Close()

                    If SendEmail("Online Registration for 2007 NSF Regionals /  Very Important", emailbody, drLogins("EMailID")) = False Then

                    End If

                End If
            Next
            Response.Write("Emails Send Successfully!!!")
        End Sub

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Dim dsEmails As New DataSet
            Dim tblEmails() As String = {"EmailContacts"}
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim txtQueryText As String = Replace(txtSQLCommand.Text, "'", "''")
            Dim sbEmailList As New StringBuilder
            SqlHelper.FillDataset(conn, CommandType.Text, txtSQLCommand.Text, dsEmails, tblEmails)

            If dsEmails.Tables.Count > 0 Then
                dgEmail.DataSource = dsEmails.Tables(0)
                dgEmail.DataBind()
                Dim ctr As Integer
                For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                    sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("Email").ToString)
                    If ctr <= dsEmails.Tables(0).Rows.Count - 1 Then
                        sbEmailList.Append(",")
                    End If
                Next
                txtSendList.Text = sbEmailList.ToString
            End If
        End Sub

        Private Sub btnSendEmail_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSendEmail.Click

            Try
                Dim dsParents As New DataSet
                Dim tblParents() As String = {"Parents"}
                Dim sbEmailSuccess As New StringBuilder
                Dim sbEmailFailure As New StringBuilder
                Dim FromAddress As String = "nsfcontests@gmail.com"
                'If txtFrom.Text = "" Then Exit Sub
                If txtEmailSubject.Text = "" Then Exit Sub
                'If txtEmailBodtText.Text = "" Then Exit Sub

                cnTemp = New SqlConnection(Application("ConnectionString"))
                SqlHelper.FillDataset(cnTemp, CommandType.StoredProcedure, "GetParentsEmail", dsParents, tblParents)

                If dsParents.Tables.Count > 0 Then
                    For Each dr As DataRow In dsParents.Tables(0).Rows
                        Try
                            If EmailAddressCheck(dr.Item("EmailID").ToString) Then
                                If SendEmail(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBodtText.Text, "'", "''"), dr.Item("EmailID").ToString) = True Then
                                    sbEmailSuccess.Append("Email Send To:" & dr.Item("EmailID").ToString & "<BR>")
                                Else
                                    sbEmailFailure.Append("Email Send To:" & dr.Item("EmailID").ToString & "<BR>")
                                End If
                            End If
                        Catch ex As Exception

                        End Try
                    Next
                End If
            Catch ex As Exception
            End Try

        End Sub

        Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
            Try
                Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
                If emailAddressMatch.Success Then
                    EmailAddressCheck = True
                Else
                    EmailAddressCheck = False
                End If
            Catch ex As Exception
                EmailAddressCheck = False
            End Try
        End Function

    End Class

End Namespace


