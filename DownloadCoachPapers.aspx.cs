﻿using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.IO;
using System.Net;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Globalization;
using System.Drawing;
using VRegistration;
public partial class DownloadCoachPapers : System.Web.UI.Page
{
    EntityCoachPapers p_objECP = new EntityCoachPapers();
    int MemberID;
    int ChildID;
    int CurrentYear;
    protected void Page_Load(object sender, EventArgs e)
    {
        lblMainError.Text = "";
        if (Session["StudentId"] != null)
        {
            ChildID = Int32.Parse(Session["StudentId"].ToString());
            //Response.Write(ChildID.ToString());
            //lbltitle.Text = "Student Function";
            lnkredirect.Text = "Back to Student Functions Page";
        }
        else
        {
            ChildID = -1;
            //lbltitle.Text = "Parent Function";
            lnkredirect.Text = "Back to Parent Functions Page";
        }

        if (Session["CustIndID"] != null)
        {
            MemberID = Int32.Parse(Session["CustIndID"].ToString());
        }
        else
        {
            Response.Redirect("~/Maintest.aspx");
        }

        CurrentYear = Int32.Parse(DateTime.Now.Year.ToString()) - 1;

        if (!Page.IsPostBack)
        {
            lblMainError.Visible = false;
            //PopulateLevelNew(ddlLevel,0,0,true);
            LoadYear();
            loadPhase(ddlSemester, ddlEventYear.SelectedValue);
            LoadDropDowns();

            //int Rval = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(PMemberID) from CoachReg where PMemberID =" + MemberID +" and Approved='Y' and EventYear ="+CurrentYear ));

            //if (Rval > 0)
            //{
            //    if (ChildID != -1)
            //    {
            //        GetChildName(ddlChildName, true, true);
            //        GetProductGroupCodes(ddlproductgroup, true, Int32.Parse(ddlChildName.SelectedValue));
            //        GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), CurrentYear, ddlproduct, true);
            //        // PopulateLevel(ddlLevel, ddlproductgroup.SelectedItem.Text, true);  
            //        PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue.ToString()), PGId(ddlproduct), true);
            //        // GetProductCodes(Int32.Parse(ddlproductgroup.SelectedValue ), ddlproduct, true); 
            //    }
            //    else
            //    {

            //        GetChildName(ddlChildName, true, false);
            //        GetProductGroupCodes(ddlproductgroup, true, Int32.Parse(ddlChildName.SelectedValue));
            //        GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), CurrentYear, ddlproduct, true);
            //        // PopulateLevel(ddlLevel, ddlproductgroup.SelectedItem.Text, true);  
            //        PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue.ToString()), PGId(ddlproduct), true);
            //        // GetProductCodes(Int32.Parse(ddlproductgroup.SelectedValue ), ddlproduct, true); 
            //    }
            //    //GetProductGroupCodes(ddlproductgroup, true, -1);               
            //    //ddlproduct.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            //    //ddlproduct.Enabled = false;
            //    //Panel1.Visible = false;
            //    //Panel2.Visible = true;
            //}
            //else
            //{
            //    Panel2.Visible = false;
            //    Panel1.Visible = true;
            //    lblMainError.Visible = true;
            //    lblMainError.Text = "Sorry, You are not Registered for this year ("+CurrentYear.ToString () +")<br>" +"Please Register for a Contest to Avail this Feature";
            //}          
        }

    }
    private void LoadYear()
    {
        ddlEventYear.Items.Clear();
        //if ((Session["RoleId"].ToString() == "1") || (Session["RoleId"].ToString() == "32"))
        //{
        int Minyear, Maxyear;
        try
        {
            Minyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MIN(eventyear) from Coachpapers"));
            Maxyear = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from Coachpapers"));

            int year = DateTime.Now.Year;
            int j = 0;
            for (int i = Minyear; i <= year; i++)
            {
                ddlEventYear.Items.Add(new ListItem((i.ToString() + "-" + (i + 1).ToString().Substring(2, 2)), i.ToString()));


                j = j + 1;
            }
            ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(eventyear) from CoachReg where EventId=13 and PMemberId=" + Session["CustIndID"].ToString() + "").ToString();

            //ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText((Maxyear).ToString()));
            //if (DateTime.Now.Month <= 5) //If todate is less than May 31st, then default year = current year - 1 else default year = current year
            //{
            //    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText((year - 1).ToString()));
            //}
            //else
            //{
            //    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText((year).ToString()));
            //}
        }
        catch (SqlException se)
        {
            //lblMessage.Text = se.Message;
            return;
        }
    }

    private void LoadDropDowns()
    {

        int Rval = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(PMemberID) from CoachReg where PMemberID =" + MemberID + " and Approved='Y' and Semester= '" + ddlSemester.SelectedValue + "' and EventYear =" + Int32.Parse(ddlEventYear.SelectedValue)));// CurrentYear));

        if (Rval > 0)
        {
            btnSearch.Enabled = true;
            if (ChildID != -1)
            {
                int Count = 0;
                try
                {
                    string Cmdtext = "select count(*) from Indspouse where AutoMemberid=" + ChildID + "";
                    Count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext));
                }
                catch
                {

                }
                if (Count > 0)
                {
                    GetChildName(ddlChildName, true, false);
                }
                else
                {
                    GetChildName(ddlChildName, true, true);
                }

                if (ddlChildName.SelectedValue != "-1")
                {
                    GetProductGroupCodes(ddlproductgroup, true, Int32.Parse(ddlChildName.SelectedValue));
                    GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), Int32.Parse(ddlEventYear.SelectedValue), ddlproduct, true);//CurrentYear
                                                                                                                                                                           // PopulateLevel(ddlLevel, ddlproductgroup.SelectedItem.Text, true);  
                    PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue.ToString()), PGId(ddlproduct), true);
                    // GetProductCodes(Int32.Parse(ddlproductgroup.SelectedValue ), ddlproduct, true); 
                }
            }
            else
            {

                GetChildName(ddlChildName, true, false);
                if (ddlChildName.SelectedValue != "-1")
                {
                    GetProductGroupCodes(ddlproductgroup, true, Int32.Parse(ddlChildName.SelectedValue));
                    GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), Int32.Parse(ddlEventYear.SelectedValue), ddlproduct, true);//CurrentYear
                                                                                                                                                                           // PopulateLevel(ddlLevel, ddlproductgroup.SelectedItem.Text, true);  
                    PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue.ToString()), PGId(ddlproduct), true);
                    // GetProductCodes(Int32.Parse(ddlproductgroup.SelectedValue ), ddlproduct, true); 
                }
            }
            //GetProductGroupCodes(ddlproductgroup, true, -1);               
            //ddlproduct.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            //ddlproduct.Enabled = false;
            //Panel1.Visible = false;
            //Panel2.Visible = true;
        }
        else
        {
            //Panel2.Visible = false;
            Panel1.Visible = true;
            lblMainError.Visible = true;
            lblMainError.Text = "Sorry, You did not register for online coaching this year (" + ddlEventYear.SelectedValue + ")<br>" + "Please register for online coaching to avail this feature Or Check with the previous year details.";
            ddlChildName.Enabled = false;
            gvTestPapers.Visible = false;
            btnSearch.Enabled = false;

        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {

    }
    private void GetChildName(DropDownList ddlObject, bool blnCreateEmptyItem, bool Child)
    {
        try
        {
            string SQLStr;
            if (Child)
            {
                SQLStr = "select Distinct C.[FIRST_NAME] + ' ' + C.[MIDDLE_INITIAL] + ' ' + C.[LAST_NAME] AS ChildName,  C.[ChildNumber]  from [Child] C Inner Join CoachReg CR on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber where C.MEMBERID =" + MemberID + " and C.[ChildNumber]=" + ChildID + " and CR.Approved='Y' and CR.Semester='" + ddlSemester.SelectedValue + "' and CR.EventYear=" + Int32.Parse(ddlEventYear.SelectedValue);//Year(GETDATE())";
            }
            else
            {
                SQLStr = "select Distinct case when CR.AdultId is null then C.[FIRST_NAME] + ' ' + C.[MIDDLE_INITIAL] + ' ' + C.[LAST_NAME] +'-'+'Child' else IP.FirstName +' '+ IP.LastName +'-'+'Adult' end AS ChildName, case when CR.AdultId is null then C.[ChildNumber] else CR.AdultId end as ChildNumber  from CoachReg CR  left Join [Child] C on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) where CR.PMEMBERID =" + MemberID + " and CR.Approved='Y' and CR.Semester='" + ddlSemester.SelectedValue + "' and CR.EventYear = " + Int32.Parse(ddlEventYear.SelectedValue); //Year(GETDATE())";
            }
            //Response.Write(SQLStr);
            DataSet dsChildName;
            dsChildName = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SQLStr);
            ddlObject.DataSource = dsChildName;
            ddlObject.DataTextField = "ChildName";
            ddlObject.DataValueField = "ChildNumber";
            ddlObject.DataBind();

            if (dsChildName.Tables[0].Rows.Count == 1)
            {
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
                ddlObject.SelectedIndex = 0;
            }
            else if (dsChildName.Tables[0].Rows.Count < 1)
            {
                ddlObject.SelectedIndex = -1;
                ddlObject.Enabled = false;
            }
            else
            {
                ddlObject.SelectedIndex = -1;
                ddlObject.Enabled = true;
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Child Name]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            Label1.Text = se.ToString();
            return;
        }
    }

    private void GetProductGroupCodes(DropDownList ddlObject, bool blnCreateEmptyItem, int cNum)
    {

        try
        {
            DataSet dsproductgroup;

            string Cmdtext = "SELECT distinct PG .[Name], CAST(PG.ProductGroupId AS varchar(15))+'-'+PG.ProductGroupCode AS CodeandID from dbo.[ProductGroup] PG INNER JOIN dbo.[CoachReg] A  ON PG.ProductGroupCode = A.ProductGroupCode Where A.PMemberID =" + MemberID + " and A.Approved='Y' and A.Semester='" + ddlSemester.SelectedValue + "' and A.EventYear=" + Int32.Parse(ddlEventYear.SelectedValue) + "  and PG.EventId =13";
            if (ddlChildName.SelectedItem.Text.Split('-')[1].Trim() == "Adult")
            {
                Cmdtext += " and A.AdultId=" + cNum.ToString() + "";
            }
            else
            {
                Cmdtext += " and A.ChildNumber=" + cNum.ToString() + "";
            }

            dsproductgroup = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext); //CurrentYear
            ddlObject.DataSource = dsproductgroup;
            ddlObject.DataTextField = "Name";// "ProductGroupCode";
            ddlObject.DataValueField = "CodeandID";//"ProductGroupId";
            ddlObject.DataBind();

            if (dsproductgroup.Tables[0].Rows.Count == 1)
            {
                hdnPgroup.Value = "1";
                blnCreateEmptyItem = false;
                ddlObject.Enabled = false;
                ddlObject.SelectedIndex = 0;
                GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), Int32.Parse(ddlEventYear.SelectedValue), ddlproduct, true); //CurrentYear

            }
            else if (dsproductgroup.Tables[0].Rows.Count < 1)
            {
                hdnPgroup.Value = "0";
                ddlObject.SelectedIndex = -1;
                ddlObject.Enabled = false;
            }
            else
            {
                hdnPgroup.Value = "1";
                ddlObject.SelectedIndex = -1;
                ddlObject.Enabled = true;
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            //lblMessage.Text = se.Message;
            return;
        }
    }

    private void GetProductCodes(string ProductGroupCode, int MemberID, int ChildNumber, int EventYear, DropDownList ddlObject, bool blnCreateEmptyItem)
    {

        try
        {
            if (ddlproductgroup.Text != "-1")
            {
                SqlParameter[] param = new SqlParameter[4];
                param[0] = new SqlParameter("@ProductGroupCode", ProductGroupCode);
                param[1] = new SqlParameter("@MemberID", MemberID);
                param[2] = new SqlParameter("@ChildNumber", ChildNumber);
                param[3] = new SqlParameter("@EventYear", EventYear);

                DataSet dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetByProductGroupId_CP_Download", param);
                if (ddlChildName.SelectedItem.Text.Split('-')[1].Trim() == "Adult")
                {
                    string Cmdtext = "SELECT distinct PG .[Name], CAST(PG.ProductId AS varchar(15))+'-'+PG.ProductCode AS CodeandID from dbo.[Product] PG INNER JOIN dbo.[CoachReg] A  ON PG.ProductCode = A.ProductCode Where A.PMemberID =" + MemberID + " and A.Approved='Y' and A.Semester='" + ddlSemester.SelectedValue + "' and A.EventYear=" + Int32.Parse(ddlEventYear.SelectedValue) + "  and PG.EventId =13";
                    if (ddlChildName.SelectedItem.Text.Split('-')[1].Trim() == "Adult")
                    {
                        Cmdtext += " and A.AdultId=" + ChildNumber.ToString() + "";
                    }
                    else
                    {
                        Cmdtext += " and A.ChildNumber=" + ChildNumber.ToString() + "";
                    }
                    dsproduct = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
                }
                ddlObject.DataSource = dsproduct;
                ddlObject.DataTextField = "Name";//"ProductCode";
                ddlObject.DataValueField = "CodeandID";// "ProductId";
                ddlObject.DataBind();
                ddlObject.Enabled = true;

                if (dsproduct.Tables[0].Rows.Count == 1)
                {
                    blnCreateEmptyItem = false;
                    ddlObject.Enabled = false;
                    ddlObject.SelectedIndex = 0;
                }
                else if (dsproduct.Tables[0].Rows.Count < 1)
                {
                    ddlObject.SelectedIndex = -1;
                }
                else
                {
                    ddlObject.SelectedIndex = -1;
                }
            }
            else
            {
                ddlObject.Items.Clear();
                ddlObject.Enabled = false;
            }
            if (blnCreateEmptyItem)
            {
                ddlObject.Items.Insert(0, new ListItem("[Select Product]", "-1"));
            }
            ddlObject.SelectedIndex = 0;
        }
        catch (SqlException se)
        {
            // lblMessage.Text = se.Message;
            Label1.Text = se.Message.ToString();
            return;
        }
    }

    protected void ddlFlrvProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvTestPapers.DataSource = null;
        gvTestPapers.DataBind();
        Label1.Text = "";

        GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), Int32.Parse(ddlEventYear.SelectedValue), ddlproduct, true);//CurrentYear
        PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue), PGId(ddlproduct), true);

    }

    protected void Button3_Click(object sender, EventArgs e)
    {

    }

    protected void Button2_Click(object sender, EventArgs e)
    {

    }

    protected void ddlChildName_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvTestPapers.DataSource = null;
        gvTestPapers.DataBind();
        Label1.Text = "";

        GetProductGroupCodes(ddlproductgroup, true, Int32.Parse(ddlChildName.SelectedValue));
        GetProductCodes(PGCode(ddlproductgroup), MemberID, Int32.Parse(ddlChildName.SelectedValue), Int32.Parse(ddlEventYear.SelectedValue), ddlproduct, true); //CurrentYear
        // PopulateLevel(ddlLevel, ddlproductgroup.SelectedItem.Text, true);  
        PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue.ToString()), PGId(ddlproduct), true);
        // GetProductCodes(Int32.Parse(ddlproductgroup.SelectedValue ), ddlproduct, true);      

    }
    protected void gvTestPapers_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void gvTestPapers_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        int index = -1;
        string dloadFname;
        if (e.CommandArgument != null)
        {
            if (int.TryParse(e.CommandArgument.ToString(), out index))
            {
                if (e.CommandName == "Download")
                {
                    dloadFname = gvTestPapers.Rows[index].Cells[15].Text;

                    DownloadFile(System.Configuration.ConfigurationManager.AppSettings["CoachPapersPath"], dloadFname);

                }
            }
        }
    }

    private void GetCoachPapers(EntityCoachPapers objECP)
    {
        string StudentType = ddlChildName.SelectedItem.Text.Split('-')[1].Trim();
        string[] formats = new string[] { "MM/dd/yyyy" };
        string Qdt = DateTime.Today.ToString(@"MM\/dd\/yyyy");
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        string sqlCommand = "usp_SelectByDate_ParentDownload";
        SqlParameter[] param = new SqlParameter[8];
        param[0] = new SqlParameter("@ReleaseDate", Qdt);
        param[1] = new SqlParameter("@ProductGroupCode", objECP.ProductGroupCode);
        param[2] = new SqlParameter("@ProductCode", objECP.ProductCode);
        param[3] = new SqlParameter("@PMemberId", Int32.Parse(Session["CustIndID"].ToString()));
        param[4] = new SqlParameter("@Level", objECP.Level);
        param[5] = new SqlParameter("@EventYear", Int32.Parse(ddlEventYear.SelectedValue));// Int32.Parse (DateTime.Now.Year.ToString ()));
        param[6] = new SqlParameter("@ChildId", objECP.ChildId);
        param[7] = new SqlParameter("@StudType", StudentType.Trim());
        try
        {
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, sqlCommand, param);
            if (ds.Tables[0].Rows.Count > 0)
            {
                DataTable dt = ds.Tables[0];
                DataView dv = new DataView(dt);
                gvTestPapers.Visible = true;
                gvTestPapers.DataSource = dv;
                gvTestPapers.DataBind();
                Label1.Text = "";
            }
            else
            {
                if (ddlChildName.SelectedValue.ToString() != "-1")
                {
                    if (hdnPgroup.Value == "1")
                    {
                        string grpCondtion = string.Empty;
                        if (PGId(ddlproductgroup).ToString() != "-1")
                        {
                            grpCondtion = " and ProductGroupID =" + PGId(ddlproductgroup);
                        }

                        int rCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select COUNT(*) from event where EventId =13 and status='O'"));

                        if (rCount == 0)
                        {
                            Label1.Text = "Registration is not open. no files are available.";
                            return;
                        }

                        //ProductGroupID =" + PGId(ddlproductgroup) + " and
                        int Dcount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, "select distinct COUNT(*) from EventFees where EventID =13 and EventYear =" + ddlEventYear.SelectedValue + grpCondtion + " and  DeadlineDate > '" + DateTime.Now.ToShortDateString() + "'"));

                        if (Dcount > 0)
                        {
                            Label1.Text = "Registration has not closed. No documents will be available until the classes start as determined by your coach.";
                            return;
                        }

                        if (Dcount == 0)
                        {
                            Label1.Text = "Download material is not yet available. Wait until your coach communicates to you.";
                            return;
                        }
                    }
                    Label1.Text = "Sorry you are not registered for any coaching class.";
                    gvTestPapers.DataSource = null;
                    gvTestPapers.DataBind();
                }
                else
                {
                    Label1.Text = "Please Select a Child.";
                    gvTestPapers.DataSource = null;
                    gvTestPapers.DataBind();
                }

            }
        }
        catch (Exception ex)
        {
            Label1.Text = ex.ToString();
        }

    }

    public class EntityCoachPapers
    {
        public Int32 ProductId = -1;
        public Int32 ProductGroupId = -1;
        public DateTime? QRDate = null;
        public string ProductGroupCode = string.Empty;
        public string ProductCode = string.Empty;
        public string Level = string.Empty;
        public Int32 ChildId = -1;




        public string getCompleteFTPFilePath(String TestFileName)
        {
            return String.Format("{0}/{1}",
                      System.Configuration.ConfigurationManager.AppSettings["FTPCoachPapersPath"], TestFileName);
        }
        public EntityCoachPapers()
        {
        }

    }

    private void DownloadFile(string sVirtualPath, string DfName)
    {
        try
        {
            string filePath = string.Concat(Server.MapPath(sVirtualPath), DfName);
            Context.Items["DOWNLOAD_FILE_PATH"] = filePath;
            Server.Transfer("downloader.aspx");
        }
        catch (Exception ex)
        {
            //lblMessage.Text = ex.ToString();
        }
    }

    protected void btnSearch_Click(object sender, EventArgs e)
    {

        //Label1.Text = "";
        //if (ddlChildName.SelectedValue.ToString() != "-1")
        //{
        if (ddlChildName.SelectedValue.ToString() == "-1")
        {
            Label1.Text = "Please Select Child Name";
            return;
        }

        string CmdValidation = "select distinct EndDate from CoachingDateCal where EventYear=" + ddlEventYear.SelectedValue + " and Semester='" + ddlSemester.SelectedValue + "' and ProductId=" + ddlproduct.SelectedValue.ToString().Split('-')[0] + " and ScheduleType='Term'";
             DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdValidation);
        string EndDate = string.Empty;
        string TodayDate = Convert.ToDateTime(DateTime.Now.ToString()).ToShortDateString();
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ds.Tables[0].Rows[0]["EndDate"] != null)
            {
                 EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToShortDateString();
            }
        }
        if (EndDate != "") {
            DateTime dtTodayDate = Convert.ToDateTime(TodayDate);
            DateTime dtEndDate = Convert.ToDateTime(EndDate);


            if (dtEndDate >= dtTodayDate)
            {
                p_objECP.ProductGroupCode = PGCode(ddlproductgroup);
                p_objECP.ProductCode = PGCode(ddlproduct);//.SelectedItem.Text;
                p_objECP.ChildId = Int32.Parse(ddlChildName.SelectedValue.ToString());
                if (ddlLevel.SelectedValue != "-1")
                {
                    p_objECP.Level = ddlLevel.SelectedItem.Text;
                }

                GetCoachPapers(p_objECP);
            }
            else
            {
               // Label1.Text = "Past download material is not available to the Students/Parents.";
            }
        }
        else
        {
            Label1.Text = "Download material is not yet available. Wait until your coach communicates to you.";
        }

    }

    private int PGId(DropDownList ddlObject)
    {
        int index;
        int PGI = -1;
        index = ddlObject.SelectedValue.ToString().IndexOf("-");
        if (index > 0)
        {
            PGI = Int32.Parse(ddlObject.SelectedValue.ToString().Trim().Substring(0, index));
        }
        return PGI;
    }

    private string PGCode(DropDownList ddlObject)
    {
        int index = -1;
        string PGC = string.Empty;
        index = ddlObject.SelectedValue.ToString().Trim().LastIndexOf("-");
        if (index > 0)
        {
            index++;
            PGC = ddlObject.SelectedValue.ToString().Substring(index);
        }
        return PGC;
    }

    private void PopulateLevelNew(DropDownList ddlObject, int ChildId, int PrId, bool blnCreateEmptyItem)
    {
        ddlObject.Enabled = true;
        string CmdText = "SELECT DISTINCT Level FROM [CoachReg] WHERE Approved ='Y' and PMemberID = " + Session["CustIndID"].ToString() + " AND EventYear = " + Int32.Parse(ddlEventYear.SelectedValue) + "  AND ProductID = " + PrId.ToString() + "";
        if (ddlChildName.SelectedItem.Text.Split('-')[1].Trim() == "Adult")
        {
            CmdText += " AND AdultId = " + ChildId.ToString() + "";
        }
        else
        {
            CmdText += " AND ChildNumber = " + ChildId.ToString() + "";
        }
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText); //Int32.Parse(DateTime.Now.Year.ToString()) 
        ddlObject.DataSource = ds;
        ddlObject.DataTextField = "Level";
        ddlObject.DataValueField = "Level";
        ddlObject.DataBind();
        if (ds.Tables[0].Rows.Count < 2)
        {
            blnCreateEmptyItem = false;
            ddlObject.Enabled = false;
        }
        if (ds.Tables[0].Rows.Count == 0)
        {
            blnCreateEmptyItem = true;
            ddlObject.Enabled = false;
        }

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
        }

    }

    private void PopulateLevel(DropDownList ddlObject, string iCondition, bool blnCreateEmptyItem)
    {
        ArrayList list = new ArrayList();
        ddlObject.Items.Clear();
        ddlObject.Enabled = true;
        if (iCondition.ToLower() == "math")
        {
            list.Add(new ListItem("Beginner", "BEG"));
            list.Add(new ListItem("Intermediate", "INT"));
            list.Add(new ListItem("Advanced", "ADV"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition.ToLower() == "sat")
        {
            list.Add(new ListItem("Junior", "JR"));
            list.Add(new ListItem("Senior", "SR"));
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
        }
        else if (iCondition == "NA")
        {
            ddlObject.Enabled = false;
        }
        else
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = false;
            blnCreateEmptyItem = false;
        }

        if (blnCreateEmptyItem)
        {
            ddlObject.Items.Insert(0, new ListItem("[Select Level]", "-1"));
        }

    }
    protected void ddlproduct_SelectedIndexChanged(object sender, EventArgs e)
    {
        gvTestPapers.DataSource = null;
        gvTestPapers.DataBind();
        Label1.Text = "";

        PopulateLevelNew(ddlLevel, Int32.Parse(ddlChildName.SelectedValue), PGId(ddlproduct), true);

    }
    protected void lnkredirect_Click(object sender, EventArgs e)
    {
        if (ChildID != -1)
        {
            Response.Redirect("StudentFunctions.aspx");
        }
        else
        {
            Response.Redirect("UserFunctions.aspx");
        }
    }
    protected void ddlEventYear_SelectedIndexChanged(object sender, EventArgs e)
    {

        lblMainError.Text = "";
        Label1.Text = "";
        LoadDropDowns();

    }
    protected void ddlSemester_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlChildName.Items.Clear();
        ddlproductgroup.Items.Clear();
        ddlproduct.Items.Clear();
        LoadDropDowns();
    }
    private void loadPhase(DropDownList ddlObject, string year)
    {
        ddlObject.Items.Clear();
        ArrayList arrPhase = new ArrayList();
        Common objCommon = new Common();
        arrPhase = objCommon.GetSemesters();
        for (int i = 0; i <= 2; i++)
        {

            ddlObject.Items.Add(new ListItem(arrPhase[i].ToString(), arrPhase[i].ToString()));
        }
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(year);
        //if (Session["RoleId"].ToString() == "88")
        //{

        string Cmdtext = "Select max(Semester) from coachreg where PMemberId=" + Session["CustIndID"].ToString() + " and EventYear='" + ddlEventYear.SelectedValue + "'";
        try
        {
            string Semester = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext).ToString();
            ddlObject.SelectedValue = Semester;
        }
        catch
        {
        }
        //}
    }
}
