﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Text;
using System.Web.UI;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;

public partial class UssAwards : System.Web.UI.Page
{
    double sum;
    double sponsorsum;
    double othersum;
    double sponsorexpsum;
    double otherexpsum;
    string NoDoller;
    string TxtAmountVAl;
    string UpdateNoDoll;
    DataTable dtExp = new DataTable();
    string sqlStrSponsorown = "SELECT  distinct U.SponsorID,O.ORGANIZATION_NAME as SponsorName,O.FIRST_NAME+ ' ' + o.LAST_NAME as [contact name] ,U.DonorType,(Select replace( convert( varchar(32), cast( '$' + cast( SUM(GrossAmount) AS varchar(32) ) AS money ), 1 ), '.0000', '' ) From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=O.AutoMemberID ) as [Total Amount], (Select SUM(GrossAmount)  From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=O.AutoMemberID ) as T,"
                            + " (Select MAX(ContestYear) From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=o.AutoMemberID ) as [Most Recent Year],O.Email,O.PHONE,O.Address1,O.ADDRESS2,O.City,O.State,O.Zip FROM USSAward U "
                            + "LEFT JOIN  OrganizationInfo O  ON O.AutoMemberID =U.SponsorID WHERE NoDol is null and U.SponsorID IS NOT NULL  AND U.DonorType='OWN' ORDER BY T DESC";

    string sqlStrSponsorIND = "SELECT  distinct (U.SponsorID),REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') as SponsorName,U.DonorType,  "
                            + "(Select replace( convert( varchar(32), cast( '$' + cast( SUM(GrossAmount) AS varchar(32) ) AS money ), 1 ), '.0000', '' ) "
                            + "From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=I.AutoMemberID ) as [Total Amount],(Select  SUM(GrossAmount) From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=I.AutoMemberID ) as T,(Select MAX(ContestYear) From USSAward where NoDol is null and SponsorID=U.SponsorID AND SponsorID=I.AutoMemberID ) as [Most Recent Year],"
                            + "I.Email,I.HPHONE,I.Address1,I.ADDRESS2,I.City,I.State,I.Zip FROM USSAward U left JOIN IndSpouse I ON I.AutoMemberID =U.SponsorID WHERE NoDol is null and U.SponsorID IS NOT NULL  AND U.DonorType='IND' ORDER BY T DESC";

    string sqlStrGranteeOwn = " SELECT  distinct U.GranteeId,O.ORGANIZATION_NAME as GranteeName,O.FIRST_NAME+ ' ' + o.LAST_NAME as [contact name] ,U.DonorType,(Select COUNT(GranteeId) from USSPayment where GranteeID=U.GranteeID) as Students,"
                            + "O.Email,O.PHONE,O.Address1,O.ADDRESS2,O.City,O.State,O.Zip FROM USSPayment U LEFT JOIN  OrganizationInfo O  ON O.AutoMemberID =U.GranteeID "
                            + " WHERE U.GranteeId IS NOT NULL AND U.DonorType='OWN' ORDER BY GranteeName ASC";

    string sqlStrGranteeIND = "SELECT  distinct (U.GranteeID),REPLACE(I.FirstName + ' ' + COALESCE (I.MiddleInitial, '') + ' ' + I.LastName, ' ', ' ') as GranteeName,U.DonorType, (Select COUNT(GranteeId) from USSPayment where GranteeID=U.GranteeID) as Students,"
                           + "I.Email,I.HPHONE,I.Address1,I.ADDRESS2,I.City,I.State,I.Zip FROM USSPayment U left JOIN IndSpouse I ON I.AutoMemberID =U.GranteeID"
                           + " WHERE U.GranteeID IS NOT NULL AND  U.DonorType='IND' ORDER BY Granteename ASC";
    
    //string Liability = "SELECT U.USSAwardID,U.ChildNumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, U.Grade,U.Rank,U.ContestYear,(6+U.Grade+YEAR(getdate())-U.ContestYear) as [Est Age],U.ProductGroupCode as ProdGrpCode,U.ProductCode as ProdCode,cast(U.[GrossAmount] as decimal(10,2)) as GrossAmt, "
    //                       + "CASE WHEN U.SponsorID IS not null Then cast(U.[GrossAmount] as decimal(10,2)) end as Sponsored,CASE WHEN U.SponsorID IS  null Then cast(U.[GrossAmount] as decimal(10,2)) end as Other,CASE WHEN (6+U.Grade+YEAR(getdate())-U.ContestYear)>25 AND U.SponsorID IS not null Then cast(U.[GrossAmount] as decimal(10,2)) end as SponsExp,CASE WHEN (6+U.Grade+YEAR(getdate())-U.ContestYear)>25 and U.SponsorID IS  null Then"
    //                       + " cast(U.[GrossAmount] as decimal(10,2)) end as OtherExp,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType,U.PaidFull,CP.ChapterCode as Chapter, U.MemberID,I.FirstName+' '+I.LastName as ParentName From USSAward U  left join Chapter CP on cp.ChapterID =u.ChapterID INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN') where  PaidFull <>'Y' or PaidFull is null order by U.ContestYear DESC,U.ProductGroupID, U.ProductID, U.Rank ASC";

    string Liability = "SELECT U.SponsorID,U.USSAwardID,U.ChildNumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, U.Grade,U.Rank,U.ContestYear,(6+U.Grade+YEAR(getdate())-U.ContestYear) as [Est Age],U.ProductGroupCode as ProdGrpCode,U.ProductCode as ProdCode,cast(U.[GrossAmount] as decimal(10,2)) as GrossAmt,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') "
                           + "END as SponsorName,CASE WHEN U.SponsorID IS not null Then cast(U.[GrossAmount] as decimal(10,2)) end as Sponsored,CASE WHEN U.SponsorID IS  null Then cast(U.[GrossAmount] as decimal(10,2)) end as Other,CASE WHEN (6+U.Grade+YEAR(getdate())-U.ContestYear)>25 AND U.SponsorID IS not null Then cast(U.[GrossAmount] as decimal(10,2)) end as SponsExp,CASE WHEN (6+U.Grade+YEAR(getdate())-U.ContestYear)>25 and U.SponsorID IS  null Then"
                           + " cast(U.[GrossAmount] as decimal(10,2)) end as OtherExp,U.DonorType,U.PaidFull,CP.ChapterCode as Chapter, U.MemberID,I.FirstName+' '+I.LastName as ParentName From USSAward U  left join Chapter CP on cp.ChapterID =u.ChapterID INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN') where   PaidFull <>'Y' or PaidFull is null and NoDol is null  order by U.ContestYear DESC,U.ProductGroupID, U.ProductID, U.Rank ASC";
    
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoggedIn"] == null || Session["LoggedIn"].ToString().ToLower() != "true")
        {
            Server.Transfer("login.aspx?entry=v");
        }
        if (!IsPostBack)
        {

            if (Session["LoggedIn"] == null || Session["LoggedIn"].ToString().ToLower() != "true")
            {
                Server.Transfer("login.aspx?entry=" + Session["entryToken"]);
            }
            if (Session["RoleId"] != null && (Session["RoleId"].ToString() == "1" | Session["RoleId"].ToString() == "2"))
            {

            }
            else
            {

            }
       
            txtAmount.Enabled = true;
            hdnChildID.Value = string.Empty;
            hdnMemberid.Value = string.Empty;
            hdnDonorType.Value = string.Empty;
            hdnSponsorId.Value = string.Empty;    
            PopulateChoice(ddlChoice);
            populateState(DDState);
            divPay.Visible = false;
            divChildSearch.Visible = false;
            divMain.Visible = false;
            divimport.Visible = false;
            divawardlist.Visible = false;
            divrecon.Visible = false;
            divdrildown.Visible = false;
            divuntil.Visible = false;
            divchdetails.Visible = false;
            divSummary.Visible = false;
            divsumdrildown.Visible = false;
            hdnYear.Value = "0";        
        }
    }

    protected void PopulateYear(DropDownList ddlObject)
    {
        try
        {
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();
            if (ddlObject.ID == "ddlYearUntil")
            {
                for (int i = 2000; i <= MaxYear; i++)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            else
            {
               
                for (int i = 1993; i <= MaxYear; i++)
                {
                    list.Add(new ListItem(i.ToString(), i.ToString()));
                }
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Year]", "-1"));
        }
        catch (Exception ex) { }

    }

    protected void PopulateGrade(DropDownList ddlObject)
    {       
        ArrayList list = new ArrayList();
        for (int i = 1; i <= 13; i++)
        {
            list.Add(new ListItem(i.ToString(), i.ToString()));
        }
        ddlObject.Items.Clear();
        ddlObject.DataSource = list;
        ddlObject.DataTextField = "Text";
        ddlObject.DataValueField = "Value";
        ddlObject.DataBind();     
        ddlObject.Items.Insert(0, new ListItem("[Select Grade]", "-1")); 
    }
    protected void PopulateChoice(DropDownList ddlObject)
    {
        ArrayList list = new ArrayList();
        list.Add(new ListItem("US Scholarship Awards", "1"));
        list.Add(new ListItem("US Scholarship Disbursements", "2"));
        list.Add(new ListItem("US Scholarship Awards List", "3"));
        list.Add(new ListItem("Awards/Disbursements Reconciliation", "4"));
        list.Add(new ListItem("USS Awards: Temporary Restricted Funds", "5"));
        list.Add(new ListItem("Liability Report", "8"));
        list.Add(new ListItem("Sponsors Contact List", "6"));
        list.Add(new ListItem("Grantees List ", "7"));
        ddlObject.Items.Clear();
        ddlObject.DataSource = list;
        ddlObject.DataTextField = "Text";
        ddlObject.DataValueField = "Value";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem ("[Select Choice]","-1"));

    }
    protected void populateState(DropDownList ddlObject)
    {
        try
        {
            DataSet dsState = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT Distinct StateCode,Name FROM StateMaster ORDER BY NAME");
            if (dsState.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "Name";
                ddlObject.DataValueField = "StateCode";
                ddlObject.DataSource = dsState;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select State]", "-1"));
            }
        }
        catch (Exception ex) { }
    }
    protected void PopulateRank(DropDownList ddlObject)
    {
        try
        {
            ArrayList list = new ArrayList();
            for (int i = 1; i <= 3; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddlObject.Items.Clear();
            ddlObject.DataSource = list;
            ddlObject.DataTextField = "Text";
            ddlObject.DataValueField = "Value";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Rank]", "-1"));
        }
        catch (Exception ex) { }
    }
    protected void PopulateChapter(DropDownList ddlObject)
    {
        try
        {
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "Select ChapterID,ChapterCode from Chapter order by state,ChapterCode");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ChapterCode";
                ddlObject.DataValueField = "ChapterID";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();           
                ddlObject.Items.Insert(0, new ListItem("[Select Chapter]", "-1"));              
            }
        }
        catch (Exception ex) {}
    }
    protected void PopulatePG(DropDownList ddlObject)
    {
        try
        {
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductGroupCode,ProductGroupId from ProductGroup where eventid=1");
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ProductGroupCode";
                ddlObject.DataValueField = "ProductGroupId";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select Product Group]", "-1"));
            }
        }
        catch (Exception ex) { }
    }
    protected void PopulatePrCode(DropDownList ddlObject)
    {
        try
        {
            ddlObject.Items.Clear();
            DataSet dsChapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select ProductCode,Productid from Product where EventId=1 and ProductGroupID=" + ddlProductGroup.SelectedItem.Value);
            if (dsChapter.Tables[0].Rows.Count >= 1)
            {
                ddlObject.DataTextField = "ProductCode";
                ddlObject.DataValueField = "Productid";
                ddlObject.DataSource = dsChapter;
                ddlObject.DataBind();
                ddlObject.Items.Insert(0, new ListItem("[Select Product Code]", "-1"));
            }
            else { ddlObject.Items.Insert(0, new ListItem("[Select Product Code]", "-1")); }
        }
        catch (Exception ex) { }
    }
    protected void PopulateChild(DropDownList ddlObject)
    {
        try
        {
            string strSql=string.Empty ;
            if (ddlYear.SelectedItem.Value == "-1")
            {
               strSql ="SELECT DISTINCT CH.FIRST_NAME +' '+ CH.LAST_NAME as ChildName, cast(U.ChildNumber as varchar(25)) +'-' + cast(U.MemberID as varchar(25)) as IDS  from USSAward U INNER JOIN Child CH on Ch.ChildNumber=U.childNumber";
            }
            else
            {
               strSql ="SELECT DISTINCT CH.FIRST_NAME +' '+ CH.LAST_NAME as ChildName, cast(U.ChildNumber as varchar(25)) +'-' + cast(U.MemberID as varchar(25)) as IDS  from USSAward U INNER JOIN Child CH on Ch.ChildNumber=U.childNumber WHERE ContestYear="+ddlYear.SelectedItem.Text ;
            }
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strSql);
            DataTable dt = ds.Tables[0];
            ddlObject.DataSource = dt;
            ddlObject.DataTextField = "ChildName";
            ddlObject.DataValueField = "IDS";
            ddlObject.DataBind();
            ddlObject.Items.Insert(0, new ListItem("[Select Child]", "-1"));
        }
        catch (Exception ex) { }
    }
    protected void rdChoice_SelectedIndexChanged(object sender, EventArgs e)
    {
        int idx= int.Parse (ddlChoice.SelectedItem.Value.ToString ());
        if (idx == 1)
        { 
            divMain.Visible = false;
            divChildSearch.Visible = false;
            divPay.Visible = true;
            FetchRecord();
            loadGrid();
        } 
        if(idx==2)
        {
            divMain.Visible = true;
            divPay.Visible = false;
            divChildSearch.Visible = false;
            FetchRecord();
        }
    }
    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlProductGroup.SelectedItem .Value !="-1")
        {
            PopulatePrCode(ddlProduct);
        }
    }
    protected void ddlDonorType_SelectedIndexChanged(object sender, EventArgs e)
    {
        if(ddlDonorType.SelectedValue == "Organization") 
        {
            txtLastName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtOrgName.Text = string.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true;
        }
        else
        {
            txtLastName.Text = string.Empty;
            txtFirstName.Text = string.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = string.Empty;
        }
    }
    protected void btnSearch_onClick(object sender, EventArgs e)
    {
        lblchError.Text="";
        bool executeQuery = false;
        string WhCont = string.Empty;
        string sqlString = "SELECT I.Chapter as Center, CH.ChildNumber,CH.FIRST_NAME +' ' + CH.LAST_NAME as ChildName, ";
        sqlString += " C.BadgeNumber,I.FirstName +' ' + I.LastName as ParentName,CH.MEMBERID,I.Email as ParentsEmail,I.HPhone as ParentsPhone, ";
        sqlString += " C.ProductCode  as RegisteredFor, C.CreateDate as RegisteredDate,CASE isnull(C.paymentreference,'Pending') WHEN 'Pending' then 'Pending' ELSE 'Paid' END 'PaymentStatus', ";
        sqlString += " C.PaymentDate,C.PaymentReference,ISP.FirstName +' ' + ISP.LastName as SpouseName ,C.ProductID,C.ProductGroupId,C.Rank,C.GRADE,C.ContestYear FROM  ";
        sqlString += " CHILD CH INNER JOIN IndSpouse I ON CH.Memberid=I.AutoMemberID INNER JOIN IndSpouse ISP ON CH.Memberid=ISP.Relationship LEFT JOIN Contestant C ON C.ChildNumber =CH.ChildNumber";
        if (txtChName.Text != string.Empty) 
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and CH.first_name + ' ' + CH.last_name  like '%" + txtChName.Text + "%' ";
            }
        }
        if (txtParentName.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.firstname + ' ' + I.lastname like '%" + txtParentName.Text + "%'";
                executeQuery = true;
            }
        }
        if (txtSpouseName.Text != string.Empty) 
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where ISP.firstname + ' ' + ISP.lastname like '%" + txtSpouseName.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and ISP.firstname + ' ' + ISP.lastname like '%" + txtSpouseName.Text + "%'";
                executeQuery = true;
            }
        }
        if (txtEmail.Text != string.Empty) 
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where (I.Email like '%" + txtEmail.Text + "%' or ISP.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and (I.Email like '%" + txtEmail.Text + "%' or ISP.Email like '%" + txtEmail.Text + "%')";
                executeQuery = true;
            }
        }
        if (txtPhone.Text != string.Empty)
        {
            if (WhCont == string.Empty)
            {
                WhCont = WhCont + " Where I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
            else
            {
                WhCont = WhCont + " and I.Hphone like '%" + txtPhone.Text + "%'";
                executeQuery = true;
            }
        }
        try
        {
            DataSet ds=null;
            if (executeQuery == true)
            {
                ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlString+WhCont );
            }
          if (ds.Tables[0].Rows.Count > 0 )
          {
            gvChSearch.DataSource = ds.Tables[0];
            gvChSearch.DataBind();
            lblchError.Text = "";
          }
          else
          {
          lblchError.Text = "No Record found";
          }          
        }
        catch (Exception ex){}
    }
    protected void btnsearchChild_Click(object sender, EventArgs e)
    {
        lblupStatus.Text = "";
        lblA.Text = "";
        try
        {
            gvChSearch.DataSource = null;
            gvChSearch.DataBind();
            txtChName.Text = "";
            txtParentName.Text = "";
            txtEmail.Text = "";
            txtPhone.Text = "";
            txtSpouseName.Text = "";
            divChildSearch.Visible = true;
            tblIndSearch.Visible = true;
            tblIndSearch1.Visible = false;
            lblSearch.Text = "Search for a Child.";
        }
        catch (Exception ex) { }  
    }
    protected void gvChSearch_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        try
        {
            for (int i = 0; i < e.Row.Cells.Count; i++)
            {
                e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
                if (e.Row.Cells[i].Text == "&nbsp;") { e.Row.Cells[i].Text = String.Empty; }
            }
        }
        catch (Exception ex) { }
    }
    protected void gvChSearch_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        
        int index = -1;
        try
        {
            if (e.CommandArgument != null)
            {

                if (int.TryParse(e.CommandArgument.ToString(), out index))
                {
                    index = int.Parse((string)e.CommandArgument);
                    if (e.CommandName == "Select")
                    {
                        if (ddlChoice.SelectedItem.Value.ToString ()=="1")
                        {
                            if (lblSearch.Text == "Search for Sponsor.")
                            {
                                hdnDonorType.Value = gvChSearch.Rows[index].Cells[11].Text;
                                hdnSponsorId.Value = gvChSearch.Rows[index].Cells[1].Text;
                                txtSponsor.Text = gvChSearch.Rows[index].Cells[2].Text + " " + gvChSearch.Rows[index].Cells[3].Text;
                                txtSponsor.Enabled = false;
                                divChildSearch.Visible = false;
                            }
                            else
                            {
                                txtChildName.Text = gvChSearch.Rows[index].Cells[3].Text;
                                hdnChildID.Value = gvChSearch.Rows[index].Cells[2].Text;
                                hdnMemberid.Value = gvChSearch.Rows[index].Cells[6].Text;
                                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByText(gvChSearch.Rows[index].Cells[1].Text));
                                ddlContestYear.SelectedIndex = ddlContestYear.Items.IndexOf(ddlContestYear.Items.FindByValue(gvChSearch.Rows[index].Cells[19].Text));
                                ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByValue(gvChSearch.Rows[index].Cells[18].Text));
                                ddlRank.SelectedIndex = ddlGrade.Items.IndexOf(ddlRank.Items.FindByValue(gvChSearch.Rows[index].Cells[17].Text));
                                ddlProductGroup.SelectedIndex = ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByValue(gvChSearch.Rows[index].Cells[16].Text));
                                PopulatePrCode(ddlProduct);
                                ddlProduct.SelectedIndex = ddlProduct.Items.IndexOf(ddlProduct.Items.FindByValue(gvChSearch.Rows[index].Cells[15].Text));
                                txtChildName.Enabled = false;
                                divChildSearch.Visible = false;
                            }                           
                        }
                        if (ddlChoice.SelectedItem.Value.ToString() == "2")
                        {
                            txtGrantee.Text = gvChSearch.Rows[index].Cells[2].Text + " " + gvChSearch.Rows[index].Cells[3].Text ;
                            txtGrantee.Enabled = false;
                            hdnGranteeID.Value = gvChSearch.Rows[index].Cells[1].Text;                          
                            hdnGrDonorType.Value = gvChSearch.Rows[index].Cells[11].Text;
                            divChildSearch.Visible = false;
                        }
                    }
                }
            }
        }
        catch (Exception ex){}
    }
 protected void btnclose_Click(object sender, EventArgs e)
    {
        divChildSearch.Visible = false;
    }
 protected bool dataCheck()
    {
            bool state = true;
            lblA.Text = "";
            if (txtChildName.Text  == string.Empty)
            {
                lblA.Text = "Please enter child Name.";
                state = false;
                return state;
            }
            if (hdnChildID.Value  == string.Empty)
            {
                lblA.Text = "Please search for child.";
                state = false;
                return state;
            }
            if (hdnMemberid.Value == string.Empty)
            {
                lblA.Text = "Please search for child.";
                state = false;
                return state;
            }
            if (ddlContestYear.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select contest year.";
                state = false;
                return state;
            }

            if (ddlGrade.SelectedItem.Value == "-1")
            {
                if (int.Parse(ddlContestYear.SelectedItem.Text) > 2009)
                {
                    lblA.Text = "Please select grade.";
                    state = false;
                    return state;
                }
            }
            if (ddlChapter.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select Chapter.";
                state = false;
                return state;
            }

            if (ddlProductGroup.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select Product Group.";
                state = false;
                return state;
            }
            if (ddlProduct.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select Product Code.";
                state = false;
                return state;
            }
            if (ddlRank.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select Rank.";
                state = false;
                return state;
            }
            if ((ddlProductGroup.SelectedItem.Text != "BB") && (DDNoDoller.SelectedValue == "2"))
              {
                  lblA.Text = "Please select No Dollar as No.";
                  state = false;
                  return state;
              }

            if ((txtAmount.Text == string.Empty | txtAmount.Text == "") && (ddlProductGroup.SelectedItem.Text!="BB"))
            {
                lblA.Text = "Please enter Amount.";
                state = false;
                return state;
            }
    
            if (string.IsNullOrEmpty(TxtCity.Text))
            {
                lblA.Text = "Please enter City.";
                state = false;
                return state;
            }
            if (DDState.SelectedItem.Value == "-1")
            {
                lblA.Text = "Please select State.";
                state = false;
                return state;
            }
               
            else
            {
                int num;
                bool result = int.TryParse(txtAmount.Text, out num);
                if (result)
                {
                    if (num < 1)
                    {
                        lblA.Text = "Please enter Valid Amount.";
                        state = false;
                        return state;
                    }
                }
            }

            if ((ddlPFull.SelectedItem.Value == "-1") && (ddlProductGroup.SelectedItem.Text != "BB"))
            {
                lblA.Text = "Please Select Paid full option.";
                state = false;
                return state;
            }
            return state;      
    }
 protected void colMissingException()
 {
     try
     {
         SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "select nodol FROM USSAward");
     }
     catch (Exception ex)
     { 
         //Response.Write(ex.ToString()); 
         Response.Write("<script>alert('Invalid column name 'NoDol' Please add this column to Ussaward')</script>");
        

     }
 }
 protected void btnAdd_Click(object sender, EventArgs e)
    {
        lblupStatus.Text = "";

      try
      {

          
        lblA.Text = "";
     
        bool datavalid = dataCheck();
        if (datavalid == false) { return; }
        if (!this.IsValid) { return; }
        int cnt = 0;
        string dntype = string.Empty;
        string strSql = string.Empty;
        string PFull;
        UpdateNoDolstring();
        if (string.IsNullOrEmpty(txtAmount.Text))
        {
            TxtAmountVAl = "NULL";
        }
        else
        {
            TxtAmountVAl = txtAmount.Text;
        }
        string Chgrade = ddlGrade.SelectedItem.Value == "-1" ? "NULL" : ddlGrade.SelectedItem.Text;
        if ((ddlProductGroup.SelectedItem.Text == "BB"))
        {
             PFull = ddlPFull.SelectedItem.Value == "-1" ? "NULL" : "'Y'";
        }
        else
        {
             PFull = ddlPFull.SelectedItem.Value == "2" ? "NULL" : "'Y'";
        }
       
        string productGroupAmount = string.Empty;
        if (hdnSponsorId.Value == string.Empty | hdnDonorType.Value == string.Empty)
        {
            if ((ddlProductGroup.SelectedItem.Text == "BB"))
            {
                productGroupAmount = "";
            }
            else
            {
                productGroupAmount="and [GrossAmount]=" + txtAmount.Text + " and [PaidFull]=" + PFull +"";
            }
            UpdateNoDolstring();
            strSql = "SELECT COUNT(USSAwardID) FROM USSAward WHERE  [ChildNumber]=" + hdnChildID.Value + " and [ChapterID] =" + ddlChapter.SelectedItem.Value + " and [MemberID]=" + hdnMemberid.Value + " and [Grade] =" + Chgrade + " and [Rank]=" + ddlRank.SelectedItem.Text + " and [ContestYear] =" + ddlContestYear.SelectedItem.Text + " and [ProductGroupID] = " + ddlProductGroup.SelectedItem.Value + " and city='" + TxtCity.Text + "' and ST='" + DDState.SelectedValue + "' and NoDol " + UpdateNoDoll + " and [ProductID] =" + ddlProduct.SelectedItem.Value + " " + productGroupAmount + " ";
        }
        else
        {
             if ((ddlProductGroup.SelectedItem.Text == "BB"))
            {
                productGroupAmount = "";
            }
            else
            {
              
                productGroupAmount="and [GrossAmount]=" + txtAmount.Text + " and [PaidFull]=" + PFull+ "";
            }
             UpdateNoDolstring();
             strSql = "SELECT COUNT(USSAwardID) FROM USSAward WHERE  [ChildNumber]=" + hdnChildID.Value + " and [ChapterID] =" + ddlChapter.SelectedItem.Value + " and [MemberID]=" + hdnMemberid.Value + " and [Grade] =" + Chgrade + " and [Rank]=" + ddlRank.SelectedItem.Text + " and [ContestYear] =" + ddlContestYear.SelectedItem.Text + " and [ProductGroupID] = " + ddlProductGroup.SelectedItem.Value + "and city='" + TxtCity.Text + "' and ST='" + DDState.SelectedValue + "' and NoDol " + UpdateNoDoll + " and [ProductID] =" + ddlProduct.SelectedItem.Value + "   and [SponsorID]=" + hdnSponsorId.Value + " and [DonorType]='" + hdnDonorType.Value + "' " + productGroupAmount + "";
        }
        if (btnAdd.Text == "Add")
        {             
            cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql).ToString());
            if (cnt == 0)
            {
                hdnSponsorId.Value = hdnSponsorId.Value == string.Empty ? "NULL" : hdnSponsorId.Value;
                dntype  = hdnDonorType.Value == string.Empty ? "NULL" : "'" + hdnDonorType.Value + "'";
                //PFull = ddlPFull.SelectedItem.Text == "NO" ? "NULL" : "'Y'";
               // hdnDonorType.Value =hdnDonorType.Value =string.Empty ? 
                NoDoll();
                
                
                string tstSql = "Insert Into [USSAward]([ChildNumber],[ChapterID],[MemberID],[Grade],[Rank],[ContestYear],[ProductGroupID],[ProductGroupCode],[ProductID],[ProductCode],[GrossAmount],[SponsorID],[DonorType],[PaidFull],[CreatedDate],[CreatedBy],City,ST,NoDol) ";
                tstSql += " Values (" + hdnChildID .Value  + "," + ddlChapter.SelectedItem.Value   + "," + hdnMemberid .Value  + "," + Chgrade   + "," + ddlRank.SelectedItem.Value   + "," +ddlContestYear.SelectedItem.Text +",";
                tstSql += ddlProductGroup.SelectedItem.Value + ",'" + ddlProductGroup.SelectedItem.Text + "'," + ddlProduct.SelectedItem.Value + ",'" + ddlProduct.SelectedItem.Text + "'," + TxtAmountVAl.ToString() + "," + hdnSponsorId.Value + "," + dntype + "," + PFull + ", getdate()," + Session["LoginID"].ToString() + ",'" + TxtCity.Text + "','" + DDState.SelectedValue + "'," + NoDoller + ")";
               // //Response.Write(tstSql);
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, tstSql);
                FetchRecord();
                Clear();
                lblA.Text = "Record Added Successfully";
        }
        else
        {
            lblA.Text = "Record Already Exists";
        }
        }
        else if (btnAdd.Text == "Update")
        {
            NoDoll();
            string Itemname = string.Empty;
            hdnSponsorId.Value = hdnSponsorId.Value == string.Empty ? "NULL" : hdnSponsorId.Value;
            dntype = hdnDonorType.Value == string.Empty ? "NULL" : "'" + hdnDonorType.Value + "'";
            //string PFull = ddlPFull.SelectedItem.Text == "NO" ? "NULL" : "'Y'";
          //  cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, strSql).ToString());
            //if (cnt == 0)
            //{
                string UpdSql = " Update USSAward set [ChildNumber]= " + hdnChildID.Value + ",[ChapterID] = " + ddlChapter.SelectedItem.Value 
                    + ",[MemberID] =" + hdnMemberid.Value
                    + ",[Grade] =" + Chgrade + ",[Rank] =" + ddlRank.SelectedItem.Text + ",[ContestYear] =" + ddlContestYear.SelectedItem.Text + ",[ProductGroupID] =" + ddlProductGroup.SelectedItem.Value + ",[ProductGroupCode] ='" + ddlProductGroup.SelectedItem.Text
                    + "',[ProductID] =" + ddlProduct.SelectedItem.Value + ",[ProductCode] ='" + ddlProduct.SelectedItem.Text + "',[GrossAmount] =" + TxtAmountVAl.ToString() + ",[SponsorID]=" + hdnSponsorId.Value
                    + ",[DonorType]=" + dntype + ",[PaidFull]=" + PFull + ",[City]='" + TxtCity.Text + "',[ST]='" + DDState.SelectedValue
                    + "',NoDol=" + NoDoller + ", [ModifiedDate]=getdate(),[ModifiedBy]=" + Session["LoginID"].ToString() + " where [USSAwardID] = " + Session["USSAwardID"].ToString();
              //  Response.Write(UpdSql);
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, UpdSql);
                FetchRecord();
                Clear();
                lblA.Text = "Record updated Successfully.";
                btnAdd.Text = "Add";
           // }
            //else
            //{
            //    lblA.Text = "Record Already Exists.";
            //}
        }
      }
      catch (Exception ex) { //Response.Write(ex.ToString()); 

          

      }
}
 protected void UpdateNoDolstring()
 {

       if (DDNoDoller.SelectedValue == "2")
     {
         UpdateNoDoll ="='Y'";
     }
     else
     {
         UpdateNoDoll = "is null";
     }
     
 }
 protected void gvAwards_RowCommand(object sender, GridViewCommandEventArgs e)
{
    try
    {
        int index = int.Parse(e.CommandArgument.ToString());
        int TransID;
        TransID = Int32.Parse(gvAwards.Rows[index].Cells[1].Text);

        if (e.CommandName == "Select")
        {
            btnAdd.Text = "Update";
            Session["USSAwardID"] = TransID;
            loadforUpdate(TransID);
            lblupStatus.Text = "";
        }
    }
    catch (Exception ex) { }    
}
 private void loadforUpdate(int USSAID)
    {
         try
        {
            string sqlStr = "SELECT  U.USSAwardID, U.ChildNumber,U.MemberID,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, I.Chapter, I.FirstName+' '+I.LastName as ParentName,U.Grade,U.Rank,U.ContestYear,U.ProductGroupCode,U.ProductCode,cast(U.[GrossAmount] as decimal(19,0)) as GrossAmount ,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType,U.SponsorID,U.PaidFull,case when U.City IS not null then U.City else I.City end as City,case when U.ST IS not null then U.ST else I.State end as ST,U.Nodol From USSAward U INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN') WHERE USSAwardID =" + USSAID;
            DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString (), CommandType.Text,sqlStr);
            if (ds.Tables[0].Rows.Count > 0) 
            {                 
                txtChildName.Text =ds.Tables[0].Rows[0]["ChildName"].ToString ();
                hdnChildID.Value =ds.Tables[0].Rows[0]["ChildNumber"].ToString ();
                hdnMemberid.Value =ds.Tables[0].Rows[0]["MemberID"].ToString ();
                ddlContestYear.SelectedIndex =ddlContestYear.Items.IndexOf(ddlContestYear.Items.FindByText( ds.Tables[0].Rows[0]["ContestYear"].ToString ()));
                ddlChapter.SelectedIndex =ddlChapter.Items.IndexOf(ddlChapter.Items.FindByText( ds.Tables[0].Rows[0]["Chapter"].ToString ()));
                ddlGrade.SelectedIndex =ddlGrade.Items.IndexOf(ddlGrade.Items.FindByText( ds.Tables[0].Rows[0]["Grade"].ToString ()));
                ddlProductGroup.SelectedIndex =ddlProductGroup.Items.IndexOf(ddlProductGroup.Items.FindByText( ds.Tables[0].Rows[0]["ProductGroupCode"].ToString ()));
                PopulatePrCode(ddlProduct); 
                ddlProduct.SelectedIndex =ddlProduct.Items.IndexOf(ddlProduct.Items.FindByText( ds.Tables[0].Rows[0]["ProductCode"].ToString ()));
                ddlRank.SelectedIndex =ddlRank.Items.IndexOf(ddlRank.Items.FindByText( ds.Tables[0].Rows[0]["Rank"].ToString ()));
                if (ds.Tables[0].Rows[0]["PaidFull"].ToString() == "Y")
                {
                    ddlPFull.SelectedIndex = ddlPFull.Items.IndexOf(ddlPFull.Items.FindByText("Yes"));
                }
                else
                {
                    ddlPFull.SelectedIndex = ddlPFull.Items.IndexOf(ddlPFull.Items.FindByText("No"));

                }
                txtAmount.Text = ds.Tables[0].Rows[0]["GrossAmount"].ToString();
                txtSponsor.Text = ds.Tables[0].Rows[0]["SponsorName"].ToString();
                hdnDonorType.Value = ds.Tables[0].Rows[0]["DonorType"].ToString();
                hdnSponsorId.Value = ds.Tables[0].Rows[0]["SponsorID"].ToString();
                string st=ds.Tables[0].Rows[0]["ST"].ToString();
                string StrNodoll;
                StrNodoll = ds.Tables[0].Rows[0]["NoDol"].ToString();
                if (StrNodoll == "Y")
                {
                    DDNoDoller.SelectedValue = "2";
                }
               
                    DDState.SelectedValue = ds.Tables[0].Rows[0]["ST"].ToString();
                
                TxtCity.Text = ds.Tables[0].Rows[0]["City"].ToString();
                NoDolConditions();
                lblA.Text = "";
            }
        }
        catch (Exception ex){}
    }
 private void FetchRecord()
    {
        try
        {
            lblA.Text = "";
            string SqlStr = string.Empty;
            if (ddlChoice.SelectedItem.Value.ToString () == "1")
            {
                if (ddlChName.SelectedItem.Value != "-1")
                {
                    SqlStr = "SELECT U.USSAwardID,U.ChildNumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, CP.ChapterCode as Chapter,U.MemberID, I.FirstName+' '+I.LastName as ParentName,U.Grade,U.Rank,U.ContestYear,U.ProductGroupCode,U.ProductCode,cast(U.[GrossAmount] as decimal(19,0)) as GrossAmount,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType,U.PaidFull From USSAward U INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber left join Chapter CP on cp.ChapterID =u.ChapterID INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN') WHERE U.ChildNumber=" + CHId(ddlChName) + " order by U.ContestYear DESC, U.ProductGroupID, U.ProductID, U.Rank ASC";
                }
                else
                {
                    SqlStr = "SELECT U.USSAwardID,U.ChildNumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, CP.ChapterCode as Chapter, "+
                    " U.MemberID,I.FirstName+' '+I.LastName as ParentName,U.Grade,U.Rank,U.ContestYear,U.ProductGroupCode,U.ProductCode,"+
                    " cast(U.[GrossAmount] as decimal(19,0)) as GrossAmount,"+
                    " CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType,U.PaidFull, "+
                    " case when U.City IS not null then U.City else I.City end as City,case when U.ST IS not null then U.ST else I.State end as ST,U.Nodol From USSAward U"+
                    "  left join Chapter CP on cp.ChapterID =u.ChapterID INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') "+
                    " Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN')  order by U.ContestYear DESC, U.ProductGroupID, U.ProductID, U.Rank ASC";
                }
            }
            if (ddlChoice.SelectedItem.Value.ToString() == "2")
            {
                SqlStr = "SELECT U.USSAwardID,U.ChildNumber,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, CP.ChapterCode as Chapter,U.MemberID, I.FirstName+' '+I.LastName as ParentName,U.Grade,U.Rank,U.ContestYear,U.ProductGroupCode,U.ProductCode,cast(U.[GrossAmount] as decimal(19,0)) as GrossAmount,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType,U.PaidFull From USSAward U left join Chapter CP on cp.ChapterID =u.ChapterID INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.SponsorID  AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.SponsorID AND U.DonorType = 'OWN')  order by U.ContestYear DESC, U.ProductGroupID, U.ProductID, U.Rank ASC";
            }
            DataSet dsRecords;
            dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlStr);
            DataTable dt = dsRecords.Tables[0];
            DataView dv = new DataView(dt);
            if (ddlChoice.SelectedItem.Value.ToString() == "1")
            {
                gvAwards.DataSource = dt;
                gvAwards.DataBind();                
            }
          
            Session["Exportusaw"] = dt;
            btnSearch.Enabled = true;
            if (dt.Rows.Count == 0)
            {
                lblA.Text = "No Records to Display";
            }
        }
        catch (Exception ex) { }
    }
 private void Clear()
 {
     btnAdd.Text = "Add";
     hdnChildID.Value = string.Empty;
     hdnMemberid.Value = string.Empty;
     hdnDonorType.Value = string.Empty;
     hdnSponsorId.Value = string.Empty;     
     ddlContestYear.SelectedIndex = 0;
     ddlGrade.SelectedIndex = 0;
     ddlChapter.SelectedIndex =0;
     ddlProductGroup.SelectedIndex = 0;
     ddlProduct.SelectedIndex = 0;
     ddlRank.SelectedIndex = 0;
     ddlPFull.SelectedIndex = 0;
     txtAmount.Text = string.Empty;
     txtChildName.Text = string.Empty;
     txtChildName.Enabled = true;
     txtSponsor.Text = string.Empty;
     txtSponsor.Enabled = true;
     TxtCity.Text = string.Empty;
     DDState.SelectedIndex = 0;
     DDNoDoller.SelectedValue = "1";
     NoDolConditions();
 }
 protected void btnCancel_Click(object sender, EventArgs e)
 {
     lblupStatus.Text = "";
     Clear();
 }
 private int CHId(DropDownList ddlObject)
 {
     int index;
     int CHID = -1;
     index = ddlObject.SelectedValue.ToString().IndexOf("-");
     if (index > 0)
     {
         CHID = Int32.Parse(ddlObject.SelectedValue.ToString().Trim().Substring(0, index));
     }
     return CHID;
 }
 private int MeMID(DropDownList ddlObject)
 {
     int index = -1;
     int MEID = -1;
     index = ddlObject.SelectedValue.ToString().Trim().LastIndexOf("-");
     if (index > 0)
     {
         index++;
         MEID = Int32.Parse(ddlObject.SelectedValue.ToString().Substring(index));
     }
     return MEID;
 }
 protected void ddlChName_SelectedIndexChanged(object sender, EventArgs e)
 {
     FetchRecord();
 }
 protected void btnGranteeSearch_Click(object sender, EventArgs e)
 {
     gvChSearch.DataSource = null;
     gvChSearch.DataBind();
     txtChName.Text = "";
     txtParentName.Text = "";
     txtEmail.Text = "";
     txtPhone.Text = "";
     txtSpouseName.Text = "";
     divChildSearch.Visible = true;
     tblIndSearch.Visible = false;
     tblIndSearch1.Visible = true;
     lblSearch.Text = "Search for a Grantee.";
 }
 protected void btncloseG_Click(object sender, EventArgs e)
 {
     divChildSearch.Visible = false;
 }
 protected void Button3_Click(object sender, EventArgs e)
 {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty; 
        string email = string.Empty; 
        string orgname = string.Empty; 
       
        StringBuilder strSql =new StringBuilder();
        StringBuilder strSqlOrg =new StringBuilder ();
      
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        email = txtEmail1.Text;
        orgname = txtOrgName.Text;

        if( orgname.Length > 0 )
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }       

        if (firstName.Length > 0) 
        {
            strSql.Append(" I.firstName like '%" + firstName + "%'");
        }
        if (lastName.Length > 0) 
        {
            int length  = strSql.ToString().Length ;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append(" I.lastName like '%" + lastName + "%'");
            }
        }
        if (state.Length > 0) 
        {
            int length  = strSql.ToString().Length;
            if (length > 0) 
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {        
                strSql.Append("  I.State like '%" + state + "%'");            
            }
        }
        if (email.Length > 0) 
        {
           int length  = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.Email like '%" + email + "%'");
            }
            else
            {
                strSql.Append("  I.Email like '%" + email + "%'");
            }
        }
        if (state.Length > 0) 
        {
           int length  = strSql.ToString().Length;
            if (length > 0)
            {
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            }
            else
            {
                strSqlOrg.Append(" O.State like '%" + state + "%'");
            }      
        }
        if (email.Length > 0) 
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            }
            else
            {
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");
            }
        }

        if (firstName.Length > 0 | orgname.Length > 0 | lastName.Length > 0 | email.Length > 0 | state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
            {
                SearchMembers(strSql.ToString());
            }
            else
            {
                SearchOrganization(strSqlOrg.ToString());
            }
        }
        else
        {           
            lblchError.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";
            lblchError.Visible = true;
            return;
        }
 }
 private void SearchOrganization(string sqlSt )
    {
        DataSet ds  = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt   = ds.Tables[0];
        int Count  = dt.Rows.Count;
        DataView dv  = new DataView(dt);
        gvChSearch.DataSource =dt;
        gvChSearch.DataBind() ;

        if (Count > 0) 
        {
           lblchError.Text ="";
        }
        else
        {
            lblchError.Text = "No match found";
           
        }
    }
 private void SearchMembers(string sqlSt )
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt  = ds.Tables[0];
        int Count  = dt.Rows.Count;
        DataView dv  =new DataView(dt);
        gvChSearch.DataSource = dt;
        gvChSearch.DataBind();
        if (Count > 0) 
        {
             lblchError.Text = "";            
        }
        else
        {
            lblchError.Text = "No match found";          
        }
    }
 private void GetStates()
 {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("Select State", string.Empty));
        ddlState.SelectedIndex = 0;
 }
 protected void btnPadd_Click(object sender, EventArgs e)
 {
     int amtval;

     lblB.Text = "";
     bool datavalid = DataValidate ();
     if (datavalid == false) { return; }
     if (!this.IsValid) { return; }
     int cnt = 0;     
     string PFull = ddlPaidFull.SelectedItem.Value == "2" ? "NULL" : "'Y'";
     int.TryParse(txtRest.Text, out amtval);
     string RstAmt = amtval <= 0 ? "NULL" : txtRest.Text;
     amtval = 0;
     int.TryParse(txtUnrest.Text, out amtval);
     string UnrstAmt = amtval <= 0 ? "NULL" : txtUnrest.Text;
     amtval = 0;
     int.TryParse(txtExcess.Text, out amtval);
     string Ecap = amtval <= 0 ? "NULL" : txtExcess.Text;
     amtval = 0;
     int.TryParse(txtinterest.Text, out amtval);
     string Interest = amtval <= 0 ? "NULL" : txtinterest.Text;
     amtval = 0;
     int.TryParse(txtDntoNSF.Text, out amtval);
     string dtnsf = amtval <= 0 ? "NULL" : txtDntoNSF.Text;

     //string dtnsf = string.Empty;
     //if (txtDntoNSF.Text == string.Empty)
     //{
     //    dtnsf = "NULL";
     //}
     //else
     //{
     //    dtnsf = txtDntoNSF.Text;
     //}
         
     if (btnPadd.Text == "Add")
     { 
         //RestAmt	UnrestAmt	ExcessOfCap	Interest	PaidFull

         string insSql = " INSERT INTO [USSPayment]([ChildNumber],[MemberID],[GranteeID],[DonorType],[BankID],[DatePaid],[CheckNumber],[Amount],[NSFDonation],[RestAmt],[UnrestAmt],[ExcessOfCap],[Interest],[PaidFull],[CreatedDate],[CreatedBy]) VALUES (";
         insSql += CHId(ddlChName).ToString() + "," + MeMID(ddlChName).ToString() + "," + hdnGranteeID.Value + ",'" + hdnGrDonorType.Value + "'," + ddlBank.SelectedItem.Text + ",'" + txtPaid.Text + "'," + txtcheck.Text + "," + txtPaidAmount.Text + "," + dtnsf + ","+ RstAmt +"," + UnrstAmt + "," + Ecap +"," +Interest +"," + PFull + ",getdate()," + Session["LoginID"].ToString() + ")";
         ////Response.Write(insSql);
         SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, insSql);
         UpdateExpJurnal(int.Parse(txtcheck.Text));
         loadGrid();
         ClearPay();
         lblB.Text = "Record Added Successfully.";    
     }
     else if (btnPadd.Text == "Update")
     {
         string UpdSql = "UPDATE [USSPayment] SET [ChildNumber]=" + CHId(ddlChName).ToString() + ",[MemberID]=" + MeMID(ddlChName).ToString() + ",[GranteeID]=" + hdnGranteeID.Value + ",[DonorType]='" + hdnGrDonorType.Value + "',[BankID]=" + ddlBank.SelectedItem.Text + ",[DatePaid]='" + txtPaid.Text + "',[CheckNumber]=" + txtcheck.Text + ",[Amount]=" + txtPaidAmount.Text + ",[NSFDonation]=" + dtnsf + ",[RestAmt]=" + RstAmt + ",[UnrestAmt]=" + UnrstAmt + ",[ExcessOfCap]=" + Ecap + ",[Interest]=" + Interest + ",[PaidFull]="+PFull +",[ModifiedDate]=getdate(),[ModifiedBy]=" + Session["LoginID"].ToString() + " where [USSPaidID] = " + Session["USSPaidID"].ToString();
         SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, UpdSql);
         UpdateExpJurnal(int.Parse (txtcheck.Text));
         loadGrid();
         ClearPay();
         lblB.Text = "Record updated Successfully.";
         btnPadd.Text = "Add";         
     }
 }
 protected void bttnPcancel_Click(object sender, EventArgs e)
 {
     ClearPay();
 }
 protected void Button4_Click(object sender, EventArgs e)
 {
     gvChSearch.DataSource = null;
     gvChSearch.DataBind();
     txtChName.Text = "";
     txtParentName.Text = "";
     txtEmail.Text = "";
     txtPhone.Text = "";
     txtSpouseName.Text = "";
     divChildSearch.Visible = true;
     tblIndSearch.Visible = false;
     tblIndSearch1.Visible = true;
     lblSearch.Text = "Search for Sponsor.";
 }
 protected void ddlYear_SelectedIndexChanged(object sender, EventArgs e)
 {
     PopulateChild(ddlChName); 
 }
 protected void loadGrid()
 {
     try
     {
         string SqlStr = "SELECT  U.USSPaidID, U.ChildNumber,Case when ch.Childnumber is null then convert (varchar(50),U.childNumber) else CH.FIRST_NAME +' '+ CH.LAST_NAME end AS ChildName,U.MemberID, Case when I.Automemberid is null then Convert(Varchar(50),U.Memberid) else I.FirstName+' '+I.LastName end as ParentName,CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as GranteeName,U.DonorType,U.BankID,convert (varchar(10),U.DatePaid,101) as DatePaid,U.CheckNumber,cast(U.[Amount] as decimal(19,0)) as Amount ,cast(U.[NSFDonation] as decimal(19,0)) as NSFDonation, cast(U.[RestAmt] as decimal(19,0)) as RestAmt,cast(U.[UnrestAmt] as decimal(19,0)) as UnrestAmt,cast(U.[ExcessOfCap] as decimal(19,0)) as ExcessOfCap,cast(U.[Interest] as decimal(19,0)) as Interest,U.[PaidFull] From USSPayment U LEFT JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber LEFT JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.GranteeID   AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.GranteeID  AND U.DonorType = 'OWN') order by U.CheckNumber DESC";
         DataSet dsRecords;
         dsRecords = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, SqlStr);
         DataTable dt = dsRecords.Tables[0];
         DataView dv = new DataView(dt);
         gvPayment.DataSource = dt;
         gvPayment.DataBind();
         Session["ExportusPay"] = dt;
     }
     catch (Exception ex) { 
     //Response.Write(ex.ToString());
     }
 }
 protected bool DataValidate()
 {
     bool state = true;
     lblA.Text = "";
     //if (ddlYear.SelectedItem.Value == "-1")
     //{
     //    lblB.Text = "Please select contest year.";
     //    state = false;
     //    return state;
     //}
     if (ddlChName.SelectedItem.Value == "-1")
     {       
         lblB.Text = "Please select Child Name.";
         state = false;
         return state;   
     }
     if (txtGrantee.Text == string.Empty)
     {
         lblB.Text = "Please enter Grantee Name.";
         state = false;
         return state;
     }
     if (hdnGranteeID.Value == string.Empty)
     {
         lblB.Text = "Please search for Grantee.";
         state = false;
         return state;
     }
     if (hdnGrDonorType.Value == string.Empty)
     {
         lblB.Text = "Please search for Grantee.";
         state = false;
         return state;
     }
     if (ddlBank.SelectedItem.Value == "-1")
     {
         lblB.Text = "Please select Bank ID.";
         state = false;
         return state;
     }
     if (txtPaid.Text == string.Empty)
     {
         lblB.Text = "Please enter Paid Date.";
         state = false;
         return state;
     }
     if (txtcheck.Text == string.Empty)
     {
         lblB.Text = "Please enter Check Number.";
         state = false;
         return state;
     }
     if (txtPaidAmount.Text == string.Empty)
     {
         lblB.Text = "Please enter Paid Amount.";
         state = false;
         return state;
     }
     
     return state;
 }
 protected void gvPayment_RowCommand(object sender, GridViewCommandEventArgs e)
 {
     try
     {
         int index = int.Parse(e.CommandArgument.ToString());
         int TransID;
         TransID = Int32.Parse(gvPayment.Rows[index].Cells[1].Text);

         if (e.CommandName == "Select")
         {
             btnPadd.Text = "Update";            
             Session["USSPaidID"] = TransID;
             loadforUpdatePay(TransID);
            
         }
     }
     catch (Exception ex) { //Response.Write(ex.ToString()); 
     }    
 }
 private void loadforUpdatePay(int USSPID)
 {
     try
     {
         string sqlStr = "SELECT  U.USSPaidID, U.ChildNumber,U.MemberID,U.GranteeID,CH.FIRST_NAME +' '+ CH.LAST_NAME AS ChildName, I.FirstName+' '+I.LastName as ParentName,U.DonorType,U.BankID,convert (varchar(10),U.DatePaid,101) as DatePaid,U.CheckNumber,cast(U.[Amount] as decimal(19,0)) as Amount ,cast(U.[NSFDonation] as decimal(19,0)) as NSFDonation,cast(U.[RestAmt] as decimal(19,0)) as RestAmt,cast(U.[UnrestAmt] as decimal(19,0)) as UnrestAmt,cast(U.[ExcessOfCap] as decimal(19,0)) as ExcessOfCap,cast(U.[Interest] as decimal(19,0)) as Interest,U.[PaidFull],CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as GranteeName,U.DonorType,U.GranteeID From USSPayment U INNER JOIN CHILD CH ON CH.ChildNumber=U.ChildNumber INNER JOIN IndSpouse I on I.AutomemberID=U.MemberID Left Join IndSpouse ISP on (ISP.AutoMemberID =U.GranteeID   AND U.DonorType <> 'OWN') Left Join OrganizationInfo O on (O.AutoMemberID = U.GranteeID  AND U.DonorType = 'OWN') Where U.USSPaidID =" + USSPID;
         DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, sqlStr);
         if (ds.Tables[0].Rows.Count > 0)
         {
             ddlChName.SelectedIndex = ddlChName.Items.IndexOf(ddlChName.Items.FindByText(ds.Tables[0].Rows[0]["ChildName"].ToString()));
             txtGrantee.Text = ds.Tables[0].Rows[0]["GranteeName"].ToString();
             hdnGranteeID.Value = ds.Tables[0].Rows[0]["GranteeID"].ToString();
             hdnGrDonorType.Value = ds.Tables[0].Rows[0]["DonorType"].ToString();
             ddlBank.SelectedIndex = ddlBank.Items.IndexOf(ddlBank.Items.FindByText(ds.Tables[0].Rows[0]["BankID"].ToString()));
             txtPaid.Text = ds.Tables[0].Rows[0]["DatePaid"].ToString();
             txtcheck.Text = ds.Tables[0].Rows[0]["CheckNumber"].ToString();
             txtPaidAmount.Text = ds.Tables[0].Rows[0]["Amount"].ToString();
             txtDntoNSF.Text = ds.Tables[0].Rows[0]["NSFDonation"].ToString();
             txtRest.Text = ds.Tables[0].Rows[0]["RestAmt"].ToString();
             txtUnrest.Text = ds.Tables[0].Rows[0]["UnrestAmt"].ToString();
             txtExcess.Text = ds.Tables[0].Rows[0]["ExcessOfCap"].ToString();
             txtinterest.Text = ds.Tables[0].Rows[0]["Interest"].ToString();
             if (ds.Tables[0].Rows[0]["PaidFull"].ToString() == "Y")
             {
                 ddlPaidFull.SelectedIndex = ddlPaidFull.Items.IndexOf(ddlPFull.Items.FindByText("Yes"));
             }
             else
             {
                 ddlPaidFull.SelectedIndex = ddlPaidFull.Items.IndexOf(ddlPFull.Items.FindByText("No"));
             }
         }
     }
     catch (Exception ex) { //Response.Write(ex.ToString());
     }
 }
 private void UpdateExpJurnal(int checkNo)
 {
     try
     {
         //Response.Write("select COUNT(TransactionID) from ExpJournal where CheckNumber=" + checkNo);
         int cnt = int.Parse(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select COUNT(TransactionID) from ExpJournal where CheckNumber=" + checkNo).ToString());
         if (cnt == 0)
         {
             int PaidYear = DateTime.Parse(txtPaid.Text).Year;           
             string strSql = " INSERT INTO ExpJournal (ChapterID,EventYear,EventID,IncMemberID,ReimbMemberID,";
             strSql += "DonorType,ToChapterID,ExpCatID,ExpCatCode,ExpenseAmount,";
             strSql += "DateIncurred,TreatID,TreatCode,NationalApprovalDate,NationalApprover,";
             strSql += "NationalApprovalFlag,ChapterApprovalFlag,Comments,BanKID,ToBankID,CheckNumber,";
             strSql += "CreatedDate,CreatedBy,Paid,DatePaid,TransType,Account,";
             strSql += "PaymentMethod,ReportDate,ProviderName,RestTypeFrom,RestTypeTo)";
             strSql += " VALUES (109," + PaidYear + ",1,NULL," + hdnGranteeID.Value+",'";
             strSql += hdnGrDonorType.Value + "',109,29,'SchDom'," + txtPaidAmount.Text + ",'";
             strSql += txtPaid.Text +"',5,'Grantee','"+txtPaid.Text +"',"+ Session["LoginID"].ToString() +",'";
             strSql += "Approved','Pending','USS Payment app'," + ddlBank.SelectedItem.Text + ",NULL," + txtcheck.Text + ",";
             strSql += "getdate()," + Session["LoginID"].ToString() + ",'Y','" + txtPaid.Text + "','Grants',70110,";
             strSql += "'Check',getdate(),NULL,'Temp Restricted',NULL)";
             //Response.Write(strSql);
             SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, strSql);
             //lblB.Text = "ExpoJurnal Updated.";
         }
     }
     catch (Exception ex) { //Response.Write(ex.ToString()); 
     }

 }
 private void ClearPay()
 {
     ddlYear.SelectedIndex = 0;
     ddlChName.SelectedIndex = 0;
     txtGrantee.Text = string.Empty;
     txtcheck.Text = string.Empty;
     hdnGranteeID.Value = string.Empty;
     hdnGrDonorType.Value = string.Empty;
     txtPaid.Text = string.Empty;
     txtPaidAmount.Text = string.Empty;
     txtDntoNSF.Text = string.Empty;
     txtGrantee.Enabled = true;
     txtRest.Text = string.Empty;
     txtUnrest.Text = string.Empty;
     txtinterest.Text = string.Empty;
     txtExcess.Text = "";
     ddlPaidFull.SelectedIndex = 0;
     btnPadd.Text = "Add";

 }
 protected void ddlChoice_SelectedIndexChanged(object sender, EventArgs e)
 {
     lblupStatus.Text = "";
         if (ddlChoice.SelectedItem.Value.ToString() == "1" )//| ddlChoice.SelectedItem.Value.ToString() == "2")
         {
                      
             PopulateYear(ddlContestYear);            
             PopulateGrade(ddlGrade);
             PopulateRank(ddlRank);             
             PopulateChapter(ddlChapter);
             PopulatePG(ddlProductGroup);
             PopulatePrCode(ddlProduct);
             PopulateYear(ddlYear);
             PopulateChild(ddlChName);
             FetchRecord();
             GetStates();
             divPay.Visible = false;
             divuntil.Visible = false;
             divawardlist.Visible = false;
             divrecon.Visible = false;
             divChildSearch.Visible = false;
             divdrildown.Visible = false;
             divSummary.Visible = false;
             divsumdrildown.Visible = false;
             btnWinList.Visible = true;
             divibutton.Visible = true;
             divMain.Visible = true;
             Gvlist.Visible = false;
             Btnexcel.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             GVliability.Visible = false;
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "2")
         {
             PopulateYear(ddlYear);
             PopulateChild(ddlChName);
             lblB.Text = "";
             loadGrid();
             divMain.Visible = false;
             divChildSearch.Visible = false;
             divuntil.Visible = false;
             divrecon.Visible = false;
             btnWinList.Visible = false;
             divawardlist.Visible = false;
             divibutton.Visible = false;
             divSummary.Visible = false;
             divdrildown.Visible = false;
             divsumdrildown.Visible = false;
             divPay.Visible = true;
             Gvlist.Visible = false;
             Btnexcel.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             GVliability.Visible = false;

         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "3")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divrecon.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divdrildown.Visible = false;
             divuntil.Visible = false;
             divSummary.Visible = false;
             FetchAwardList();
             divawardlist.Visible = true;
             Gvlist.Visible = false;
             Btnexcel.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             GVliability.Visible = false;
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "4")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divawardlist.Visible = false;
             divdrildown.Visible = false;
             PopulateYear(ddlYearUntil);
             gvReconsile.DataSource = null;
             gvReconsile.DataBind();
             divrecon.Visible = true;
             btnextoexcel.Visible = false;
             divsumdrildown.Visible = false;
             divSummary.Visible = false;
             divuntil.Visible = true;
             Gvlist.Visible = false;
             Btnexcel.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             GVliability.Visible = false;
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "5")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divawardlist.Visible = false;
             divdrildown.Visible = false;
             divsumdrildown.Visible = false;
             divrecon.Visible = false ;
             btnextoexcel.Visible = false;
             PopulateYear(ddlYearFrom );
             PopulateYear(ddlYearTo);
             GridView1.DataSource = null;
             GridView1.DataBind();
             btnExportSummary.Enabled = false;
             divuntil.Visible = false;
             divSummary.Visible = true;
             Gvlist.Visible = false;
             Btnexcel.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             GVliability.Visible = false;
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "6")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divrecon.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divdrildown.Visible = false;
             divuntil.Visible = false;
             divSummary.Visible = false;
             lborg.Visible = true;
             lbind.Visible = true;
             SponsorList();
             Gvlist.Visible = true;
             Btnexcel.Visible = true;
             divawardlist.Visible = false;
             GVliability.Visible = false;
            
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "7")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divrecon.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divdrildown.Visible = false;
             divuntil.Visible = false;
             divSummary.Visible = false;
             lborg.Visible = true;
             lbind.Visible = true;
             GranteeList();
             Btnexcel.Visible = true;
             GVliability.Visible = false;
         }
         else if (ddlChoice.SelectedItem.Value.ToString() == "8")
         {
             divMain.Visible = false;
             divPay.Visible = false;
             divrecon.Visible = false;
             divChildSearch.Visible = false;
             divibutton.Visible = false;
             divdrildown.Visible = false;
             divuntil.Visible = false;
             divSummary.Visible = false;
             lborg.Visible = false;
             lbind.Visible = false;
             SponsorList();
             Gvlist.Visible = false;
             Gvgrantee.Visible = false;
             Btnexcel.Visible = true;

         }
     
 }
 protected void gvAwards_PageIndexChanging(object sender, GridViewPageEventArgs e)
 {
     try
     {
         gvAwards.PageIndex = e.NewPageIndex;
         gvAwards.DataSource = (DataTable)Session["Exportusaw"];
         gvAwards.DataBind();
     }
     catch (Exception ex) { //Response.Write(ex.ToString()); 
     }
 }
 protected void gvPayment_SelectedIndexChanging(object sender, GridViewSelectEventArgs e)
 {
    
 }
 protected void gvPayment_PageIndexChanging(object sender, GridViewPageEventArgs e)
 {
     try
     {
         gvPayment.PageIndex = e.NewPageIndex;
         gvPayment.DataSource = (DataTable)Session["ExportusPay"];
         gvPayment.DataBind();
     }
     catch (Exception ex) { //Response.Write(ex.ToString()); 
     }
 }
 protected void btnWinList_Click(object sender, EventArgs e)
 {
     try
     {
         winList();
     }
     catch (Exception ex) { }  
 }
 protected void winList()
 {
     lblupStatus.Text = "";
     btndYes.Text = "Yes";
     btndNo.Text = "No";
     gvPayAwards.DataSource = null;
     gvPayAwards.DataBind();
     divChildSearch.Visible = false;
     divibutton.Visible = false;
     divMain.Visible = false;
     divimport.Visible = true;
     btnWinList.Visible = false;
     lblimportTitle.Visible = true;
     ddlChoice.Enabled = false;
     try
     {
         int cntYear = 0;
         cntYear = Convert.ToInt16(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select MAX(contestyear) from USSAward "));
         if (cntYear > 0)
         {
             cntYear++;
             lblimportTitle.Text = "Download Records for Year:" + cntYear.ToString();
             hdnYear.Value = cntYear.ToString();
             btndYes.Enabled = true;
         }
         else
         {
             btndYes.Enabled = false;
         }
     }
     catch (Exception ex) { }  
 }
 protected void btndNo_Click(object sender, EventArgs e)
 {
     divimport.Visible = false;
     divMain.Visible = true;
     btnWinList.Visible = true;
     divibutton.Visible = true;
     ddlChoice.Enabled = true;
 }
 protected void FetchAwardList()
 {
     string sqlStr = "SELECT U.USSAwardID, U.ContestYear,U.childNumber,Case when ch.Childnumber is null then convert (varchar(50),U.childNumber) else CH.FIRST_NAME +' '+ CH.LAST_NAME end AS ChildName,CP.ChapterCode as Chapter,U.MemberID,Case when I.Automemberid is null then Convert(Varchar(50),U.Memberid) else I.FirstName+' '+I.LastName end as ParentName,U.Grade,U.Rank,U.ProductGroupCode,U.ProductCode,cast(U.[GrossAmount] as decimal(19,0)) as Amount,";
     sqlStr +=" CASE WHEN U.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,U.DonorType, ";
     sqlStr +=" I.Email ,I.HPhone,I.CPhone,I.Address1,I.City,I.State,I.Zip,ISPO.FirstName +' ' + ISPO.LastName as SpouseName,ISPO.Email AS SpouseEmail FROM USSAward U LEFT JOIN Child CH ON CH.ChildNumber =U.ChildNumber LEFT JOIN IndSpouse I ON I.AutoMemberID =U.MemberID LEFT JOIN Chapter CP ON CP.ChapterID =U.ChapterID ";
     sqlStr += " LEFT JOIN OrganizationInfo O ON O.AutoMemberID =U.SponsorID LEFT JOIN IndSpouse ISP ON ISP.AutoMemberID =U.SponsorID LEFT JOIN IndSpouse ISPO ON ISPO.Relationship =U.MemberID where U.NoDol is null order by U.ContestYear DESC, U.ProductGroupID, U.ProductID, U.Rank";
     try
     {
         DataSet dsawlist = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
         if (dsawlist.Tables[0].Rows.Count > 0)
         {
             DataTable dt = dsawlist.Tables[0];
             gvAwardList.DataSource = dt;
             gvAwardList.DataBind();
             Session["AwardList"] = dt;
         }
     }
     catch (Exception ex) { }
 }

 protected void SponsorList()
 {

     try
     {
       
         DataTable dt1 = new DataTable();
         DataTable dt = new DataTable();
        
         if (ddlChoice.SelectedItem.Value.ToString() == "8")
         {
             string suma;
             string sponsor;
             string other;
             string sponexp;
             string otherexp;
            
             Gvgrantee.Visible = false;
             DataSet dsawlisLiability = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Liability);
             if (dsawlisLiability.Tables[0].Rows.Count > 0)
             {
                 GVliability.Visible = true;
                 dt1 = dsawlisLiability.Tables[0];
                 dt1.Columns.RemoveAt(5);
                 DataRow dr = dt1.NewRow();
                 dr[3] = "Total (Sum)";
                 foreach (DataRow drnew in dt1.Rows)
                 {
                     if (!DBNull.Value.Equals(drnew[9]))
                     {
                         suma = drnew[9].ToString();
                         if (suma != "")
                         {
                             sum += Convert.ToDouble(drnew[9]);
                         }
                     }
                     if (!DBNull.Value.Equals(drnew[11]))
                     {
                         sponsor = drnew[11].ToString();
                         if (sponsor != "")
                         {
                             sponsorsum += Convert.ToDouble(drnew[11]);
                         }
                     }
                     if (!DBNull.Value.Equals(drnew[12]))
                     {
                         other = drnew[12].ToString();
                         if (other != "")
                         {
                             othersum += Convert.ToDouble(drnew[12]);
                         }
                     }
                     if (!DBNull.Value.Equals(drnew[13]))
                     {
                         sponexp = drnew[13].ToString();
                         if (sponexp != "")
                         {
                             sponsorexpsum += Convert.ToDouble(drnew[13]);
                         }
                     }
                     if (!DBNull.Value.Equals(drnew[14]))
                     {
                         otherexp = drnew[14].ToString();
                         if (otherexp != "")
                         {
                             otherexpsum += Convert.ToDouble(drnew[14]);
                         }
                     }

                 }
                   dr[9] = sum;
                   dr[11] = sponsorsum;
                   dr[12] = othersum;
                   dr[13] = sponsorexpsum;
                   dr[14] = otherexpsum;
                 dt1.Rows.Add(dr);
                 GVliability.DataSource = dt1;
                 GVliability.DataBind();
                 Session["Liability"] = dt1;

             }
             else
             {
                 GVliability.Visible = false;
             }
         }
         else
         {
             DataSet dsawlistown = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStrSponsorown);
             DataSet dsawlistIND = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStrSponsorIND);
             if (dsawlistown.Tables[0].Rows.Count > 0)
             {
                 Gvlist.Visible = true;
                 dt1 = dsawlistown.Tables[0];
                 dt1.Columns.RemoveAt(5);
                 Gvlist.DataSource = dt1;
                 Gvlist.DataBind();

             }
             else
             {
                 lborg.Visible = false;
                 Gvlist.Visible = false;
             }
             if (dsawlistIND.Tables[0].Rows.Count > 0)
             {
                 Gvgrantee.Visible = true;
                 dt = dsawlistIND.Tables[0];
                 dt.Columns.RemoveAt(4);

                 Gvgrantee.DataSource = dt;
                 Gvgrantee.DataBind();

             }
             else
             {
                 lbind.Visible = false;
                 Gvgrantee.Visible = false;
             }
             dt1.Merge(dt);
             Session["SponsorList"] = dt1;

         }
     }
     catch (Exception ex) { }
 }
 protected void GranteeList()
 {
    
     try
     {
         DataTable dt1=new DataTable();
         DataTable dt = new DataTable();
         DataSet dsawlistown1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStrGranteeOwn);
         DataSet dsawlistIND1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStrGranteeIND);
         if (dsawlistown1.Tables[0].Rows.Count > 0)
         {
             Gvlist.Visible = true;
              dt1 = dsawlistown1.Tables[0];
             Gvlist.DataSource = dt1;
             Gvlist.DataBind();
             //Session["SponsorList"] = dt1;
         }
         else
         {
             lborg.Visible = false;
             Gvlist.Visible = false;
         }
         if (dsawlistIND1.Tables[0].Rows.Count > 0)
         {
             Gvgrantee.Visible = true;
              dt = dsawlistIND1.Tables[0];
             Gvgrantee.DataSource = dt;
             Gvgrantee.DataBind();
             //Session["SponsorList"] = dt;
         }
         else
         {
             lbind.Visible = false;
             Gvgrantee.Visible = false;
         }
         if ((dsawlistIND1.Tables[0].Rows.Count == 0)&&(dsawlistown1.Tables[0].Rows.Count==0))
         {
             Gvgrantee.Visible = false;
             Gvlist.Visible = false;

         }
         dt1.Merge(dt);
         Session["GranteeList"] = dt1;
     }
     catch (Exception ex) { }
 }
 protected void btndYes_Click(object sender, EventArgs e)
 {
     if (btndYes.Text == "Yes")
     {
         if (int.Parse(hdnYear.Value.ToString()) > 0)
         {
             string sqlstr=" Select C.ContestYear,C.ChildNumber,case when (C.ProductGroupCode ='EW' or C.productgroupcode='PS')  then (Select Chapterid from Indspouse where automemberid=C.Parentid)  else C1.ChapterID end as ChapterID ,C.ParentID,C.Grade,C.Rank,C.ProductGroupID,C.ProductGroupCode,C.ProductID,C.ProductCode from contestant C  ";
             sqlstr += " left join Contestant C1 on c1.ChildNumber = c.ChildNumber and C1.EventId =2 and C1.ContestYear =" + hdnYear.Value.ToString() + " and c1.ProductGroupCode =c.ProductGroupCode and c.Rank between 1 and 3 and c1.ProductGroupCode<>'BB' where C.ContestYear= " + hdnYear.Value.ToString() + " and c.EventId=1 and c.ProductGroupCode<>'BB' and c.Rank between 1 and 3 ";
             sqlstr += " order by c.productgroupID, c.ProductID, rank ";
             DataSet dstemp = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr);
             if (dstemp.Tables[0].Rows.Count > 0)
             {
                 DataTable dt = dstemp.Tables[0];
                 gvPayAwards.DataSource = dt;
                 gvPayAwards.DataBind();
                 btndYes.Text = "Update";
                 btndNo.Text = "Cancel";
                 lblimportTitle.Visible = false;
             }
             else
             {
                 btndYes.Text = "Yes";
                 lblimportTitle.Text = "No Record Found For the Selected Year.";
                 btndYes.Text = "Yes";
                 btndNo.Text = "No";
             }
         }
     }
     else if (btndYes.Text == "Update")
     {
         string sqlupdate="INSERT INTO USSAward (ContestYear,ChildNumber,ChapterID,MemberID,Grade,Rank,ProductGroupID ,ProductGroupCode ,ProductID,ProductCode,CreatedDate,CreatedBy  ) ";
         sqlupdate +=" Select C.ContestYear,C.ChildNumber,case when (C.ProductGroupCode ='EW' or C.productgroupcode='PS')  then (Select Chapterid from Indspouse where automemberid=C.Parentid)  else C1.ChapterID end as ChapterID ,C.ParentID,C.Grade,C.Rank,C.ProductGroupID,C.ProductGroupCode,C.ProductID,C.ProductCode,GETDATE(),"+ Session["LoginID"].ToString () +" from contestant C ";
         sqlupdate += " left join Contestant C1 on c1.ChildNumber = c.ChildNumber and C1.EventId =2 and C1.ContestYear =" + hdnYear.Value.ToString() + " and c1.ProductGroupCode =c.ProductGroupCode and c.Rank between 1 and 3 and c1.ProductGroupCode<>'BB' where C.ContestYear=" + hdnYear.Value.ToString() + " and c.EventId=1 and c.ProductGroupCode<>'BB' and c.Rank between 1 and 3 order by c.productgroupID, c.ProductID, rank";
         try
         {
             SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlupdate);
             lblupStatus.Text = "Record Updated successfully.";
             divimport.Visible = false;
             divMain.Visible = true;
             FetchRecord();
             btnWinList.Visible = true;
             divibutton.Visible = true;
             ddlChoice.Enabled = true;
         }
         catch (Exception ex){}        
     }
 }
 protected void gvAwardList_PageIndexChanging(object sender, GridViewPageEventArgs e)
 {
     try
     {
         gvAwardList.PageIndex = e.NewPageIndex;
         gvAwardList.DataSource = (DataTable)Session["AwardList"];
         gvAwardList.DataBind();
     }
     catch (Exception ex) { }
 }

 private void FechReconlist()
 {
     int cYear = int.Parse(ddlYearUntil.SelectedItem.Text) + 1;
    string sqlStr="with tbl as  (select  A.ChildNumber,Ch.FIRST_NAME +' '+ch.LAST_NAME  as ChildName,ch.FIRST_NAME ,ch.LAST_NAME ,";
    sqlStr += " a.MemberID ,I.FirstName +' '+I.LastName as ParentName ,  cast(SUM(A.GrossAmount)as decimal (19,0)) as CumAward , ";
    sqlStr += " (select cast (sum(grossamount) as decimal(19,0)) from ussaward where NoDol is null and sponsorid is not null and ChildNumber =a.ChildNumber and ContestYear <=" + ddlYearUntil.SelectedItem.Text + ") as Sponsored,  ";
    sqlStr += " (select cast (sum(grossamount) as decimal(19,0)) from ussaward where NoDol is null and sponsorid is null and ChildNumber =a.ChildNumber and ContestYear <=" + ddlYearUntil.SelectedItem.Text + ") as Other, ";
     sqlStr += " (select (cast(SUM(Amount) as decimal (19,0)))from USSPayment where ChildNumber =a.ChildNumber and DatePaid <= '04/30/" + cYear.ToString() + "') as CumPayment ,";
     sqlStr += " (select (CAST(sum(RestAmt) as decimal (19,0))) from USSPayment where ChildNumber =a.ChildNumber and DatePaid <= '04/30/" + cYear.ToString() + "') as PmtSponsored, ";
     sqlStr += " (select (CAST(sum(UnrestAmt) as decimal (19,0))) from USSPayment where ChildNumber =a.ChildNumber and DatePaid <= '04/30/" + cYear.ToString() + "') as PmtOther, ";
     sqlStr += " (select (CAST(sum(Interest) as decimal (19,0))) from USSPayment where ChildNumber =a.ChildNumber and DatePaid <= '04/30/" + cYear.ToString() + "') as Interest,";
     sqlStr += " A.PaidFull from USSAward A left join child CH on ch.ChildNumber=A.childnumber left join IndSpouse I on I.AutoMemberID =A.MemberID where NoDol is null and A.ContestYear <=" + ddlYearUntil.SelectedItem.Text + " group by A.ChildNumber,A.MemberID,ch.FIRST_NAME ,ch.LAST_NAME,I.FirstName ,I.LastName,A.PaidFull)";
     sqlStr += " select  tbl.ChildNumber ,tbl.ChildName,tbl.MemberID,tbl.ParentName,tbl.CumAward,tbl.Sponsored,tbl.Other ,tbl.CumPayment,tbl.PmtSponsored,tbl.PmtOther,tbl.Interest,tbl.PaidFull from tbl order by tbl.LAST_NAME ,tbl.FIRST_NAME ";
     //sqlStr +=" UNION all select null,null,null,'Total',sum(tbl.CumAward),sum(tbl.Sponsored),SUM(tbl.Other),SUM(tbl.CumPayment),SUM(tbl.PmtSponsored),SUM(tbl.PmtOther),sum(tbl.Interest),null from tbl  ";     
     DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlStr);
     if (ds1.Tables[0].Rows.Count > 0)
     {
         DataTable dt = ds1.Tables[0];
         object cAward;
         object Sp;
         object oth;
         object cPayment;
         object pOth;
         object pSp;
         object pInt;
         cAward = dt.Compute("Sum(CumAward)", "");
         Sp = dt.Compute("Sum(Sponsored)", "");
         oth = dt.Compute("Sum(Other)", "");
         cPayment = dt.Compute("Sum(CumPayment)", "");
         pSp = dt.Compute("Sum(PmtSponsored)", "");
         pOth = dt.Compute("Sum(PmtOther)", "");
         pInt = dt.Compute("Sum(Interest)", "");
         DataRow row = dt.NewRow();
         row["ChildNumber"] = DBNull.Value ;
         row["ChildName"] = DBNull.Value ;
         row["MemberID"] = DBNull.Value ;
         row["ParentName"] = "Total";
         row["CumAward"] = cAward;
         row["Sponsored"] = Sp;
         row["Other"] = oth;
         row["CumPayment"] = cPayment;
         row["PmtSponsored"] = pSp;
         row["PmtOther"]=pOth ;
         row["Interest"] = pInt;
         dt.Rows.Add(row);
     
         gvReconsile.DataSource = dt;
         gvReconsile.DataBind();
         Session["exportReconsile"] = dt;
         btnextoexcel.Visible = true;
     }
     else
     {
         btnextoexcel.Visible = false;
         lbluntil.Text = "No records found.";
     }
 }
 public void GeneralExport1(DataTable dtdata, string fname)
 {
     try
     {
         string attach = string.Empty;
         attach = "attachment;filename=" + fname;
         Response.ClearContent();
         Response.AddHeader("content-disposition", attach);
         Response.ContentType = "application/vnd.xls";
         if (dtdata != null)
         {
             foreach (DataColumn dc in dtdata.Columns)
             {
                 Response.Write(dc.ColumnName + "\t");
             }
             Response.Write(System.Environment.NewLine);
             foreach (DataRow dr in dtdata.Rows)
             {
                 for (int i = 0; i < dtdata.Columns.Count; i++)
                 {
                     Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                 }
                 Response.Write("\n");
             }
             Response.Flush();
             HttpContext.Current.Response.Flush();
             HttpContext.Current.Response.SuppressContent = true;
             HttpContext.Current.ApplicationInstance.CompleteRequest();
         }
     }
     catch (Exception ex)
     {
         Response.Write(ex.ToString());
     }
 }
 public void ExportTableData(DataTable dtdata)
 {
     string attach = string.Empty;    
     attach = "attachment;filename=USScholarshipAwardsList.xls";    
     Response.ClearContent();
     Response.AddHeader("content-disposition", attach);
     Response.ContentType = "application/vnd.xls";// "application/vnd.openxmlformats";// "application/vnd.xls";//ms-excel";
     if (dtdata != null)
     {
         foreach (DataColumn dc in dtdata.Columns)
         {
             //Response.Write(dc.ColumnName + "\t");
             //sep = ";";
         }
         //Response.Write(System.Environment.NewLine);
         foreach (DataRow dr in dtdata.Rows)
         {
             for (int i = 0; i < dtdata.Columns.Count; i++)
             {

                 //Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
             }
             //Response.Write("\n");
         }
         Response.Flush();
         Response.End();
     }
 }

 public void ExportTableData1(DataTable dtdata)
 {
     string attach = string.Empty;
     attach = "attachment;filename=AwardsDisbursements Reconciliation.xls";
     Response.ClearContent();
     Response.AddHeader("content-disposition", attach);
     Response.ContentType = "application/vnd.xls";// "application/vnd.openxmlformats";// "application/vnd.xls";//ms-excel";
     if (dtdata != null)
     {
         foreach (DataColumn dc in dtdata.Columns)
         {
             //Response.Write(dc.ColumnName + "\t");
             //sep = ";";
         }
         //Response.Write(System.Environment.NewLine);
         foreach (DataRow dr in dtdata.Rows)
         {
             for (int i = 0; i < dtdata.Columns.Count; i++)
             {

                 //Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
             }
             //Response.Write("\n");
         }
         Response.Flush();
         Response.End();
     }
 }
 protected void btnExport_Click(object sender, EventArgs e)
 {
     FetchAwardList();

     GeneralExport1((DataTable)Session["AwardList"], "USScholarshipAwardsList.xls");

     //ExportTableData((DataTable)Session["AwardList"]);
 }
 protected void gvAwardList_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
 }
 protected void gvAwards_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
 }
 protected void gvPayment_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
 }
 protected void gvReconsile_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[4].Text == "Total")
         {
             e.Row.Cells[4].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[4].Font.Bold = true;
             e.Row.Cells[5].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[5].Font.Bold = true;
             e.Row.Cells[6].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[6].Font.Bold = true;
             e.Row.Cells[7].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[7].Font.Bold = true;
             e.Row.Cells[8].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[8].Font.Bold = true;
             e.Row.Cells[9].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[9].Font.Bold = true;
             e.Row.Cells[10].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[10].Font.Bold = true;
             e.Row.Cells[11].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[11].Font.Bold = true;
             e.Row.Cells[12].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[12].Font.Bold = true;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             //e.Row.Cells[0].Visible = false;

         }
         else
         {
             //e.Row.Cells[5].BackColor = System.Drawing.Color.YellowGreen;
             //e.Row.Cells[6].BackColor = System.Drawing.Color.YellowGreen;
             //e.Row.Cells[7].BackColor = System.Drawing.Color.YellowGreen;
             //e.Row.Cells[8].BackColor = System.Drawing.Color.LightYellow  ;
             //e.Row.Cells[9].BackColor = System.Drawing.Color.LightYellow  ;
             //e.Row.Cells[10].BackColor = System.Drawing.Color.LightYellow ;

         }
        
     }
 }
 protected void btnextoexcel_Click(object sender, EventArgs e)
 {
     GeneralExport1((DataTable)Session["exportReconsile"], "AwardsDisbursements Reconciliation.xls");

     //ExportTableData1 ((DataTable) Session["exportReconsile"]);
 }
 protected void gvReconsile_RowCommand(object sender, GridViewCommandEventArgs e)
 {
       
        int index = -1;
        try
        {
            if (e.CommandArgument != null)
            {

                if (int.TryParse(e.CommandArgument.ToString(), out index))
                {
                    index = int.Parse((string)e.CommandArgument);
                    if (e.CommandName == "Select")
                    {
                        lbluntil.Text = string.Empty;
                        ////Response.Write(gvReconsile.Rows[index].Cells[1].Text);
                        if (ddlYearUntil.SelectedItem.Value.ToString() == "-1")
                        {
                            lbluntil.Text = "Please choose year until and press submit button.";
                        }
                        else
                        {
                            FetchDrildown(gvReconsile.Rows[index].Cells[1].Text);
                            Session["ChIDDril"] = gvReconsile.Rows[index].Cells[1].Text;
                            divrecon.Visible = false;
                            divdrildown.Visible = true;
                        }
                    }
                }
            }
        }
        catch (Exception ex) { }
 }
 protected void gvdrilaward_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
   
         Button lb = (Button)e.Row.Cells[0].Controls[0].FindControl ("btnEdit");
         Label lblCap= (Label)e.Row.Cells[14].Controls[0].FindControl("lblDtype");
       
         if (lblCap .Text == "Total Sponsored")
         {
             e.Row.Cells[14].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[15].BackColor = System.Drawing.Color.Yellow ;            
             e.Row.Cells[14].Font.Bold = true;
             e.Row.Cells[15].Font.Bold = true;            
             lb.Visible = false;
         }
         else if (lblCap.Text == "Total Other")
         {            
             e.Row.Cells[14].BackColor = System.Drawing.Color.GreenYellow  ;
             e.Row.Cells[15].BackColor = System.Drawing.Color.GreenYellow;
             e.Row.Cells[14].Font.Bold = true;
             e.Row.Cells[15].Font.Bold = true;
             lb.Visible = false;
         }
         else if (lblCap .Text == "Total Amount")
         {
             e.Row.Cells[14].BackColor = System.Drawing.Color.Aqua ;
             e.Row.Cells[15].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[14].Font.Bold = true;
             e.Row.Cells[15].Font.Bold = true;
             lb.Visible = false;
         }
     }
 }
 protected void gvdrilpayment_RowDataBound(object sender, GridViewRowEventArgs e)
 {

     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         Button lb = (Button)e.Row.Cells[0].Controls[0].FindControl("btnEdit");
         Label lblCap = (Label)e.Row.Cells[15].Controls[0].FindControl("lblDonorType");
         if (lblCap.Text == "Total Award Amount")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].Font.Bold = true;
             lb.Visible = false;
         }
         else if (lblCap.Text == "Total Paid Amount")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].Font.Bold = true;
             lb.Visible = false;
         }       
     }
 }
 private void FetchDrildown(string childID)
 {
     string YCont=string.Empty ;
     string YCont1 = string.Empty;
     string YCont2 = string.Empty;
     string YCont3 = string.Empty;
     if (ddlYearUntil.SelectedItem.Value .ToString ()!="-1")
     {
         YCont =" and A.ContestYear<="+ ddlYearUntil.SelectedItem.Text ;
         YCont1 = " and year(A.DatePaid)<= " + ddlYearUntil.SelectedItem.Text;
         YCont2= " and year(DatePaid)<= " + ddlYearUntil.SelectedItem.Text;
         YCont3 = " and ContestYear<=" + ddlYearUntil.SelectedItem.Text;
     }
     string sqlstr="with vtl as (select A.USSAwardID,A.ContestYear,A.ProductGroupID,A.ProductGroupCode,A.ProductID,A.ProductCode,A.ChildNumber,ch.first_name+' ' +ch.last_name as ChildName,A.Grade,A.Rank,cast(A.GrossAmount as decimal(19,0)) as GrossAmount ,A.SponsorID, ";
     sqlstr +=" CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName, A.DonorType,A.PaidFull from USSAward A  ";
     sqlstr += " left join Child Ch on ch.ChildNumber =a.ChildNumber left join OrganizationInfo O on o.AutoMemberID =a.SponsorID left join IndSpouse ISP on isp.AutoMemberID =a.SponsorID where A.ChildNumber =" + childID + YCont+ ")";
     sqlstr += " select * from vtl union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Sponsored',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is not null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Other',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Amount',cast (SUM(vtl.grossamount)as varchar(50)) from vtl  order by ContestYear Desc,ProductGroupID ,ProductID ,rank";

     string sqlstr1 = "select A.USSPaidID,a.Childnumber,ch.first_name+' ' +ch.last_name as ChildName, A.MemberID, cast (A.Amount as decimal(19,0))as PaidAmount,cast (A.RestAmt as decimal(19,0))as RestAmt,cast(A.UnrestAmt as decimal(19,0))as UnrestAmt,cast(A.ExcessOfCap as decimal(19,0))as ExcessOfCap,cast(A.Interest as decimal(19,0))as Interest, convert (varchar(10),A.DatePaid,101) as DatePaid,A.CheckNumber,A.BankID,A.GranteeID, CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as GranteeName,";
     sqlstr1+="  A.DonorType,A.PaidFull   from USSPayment A  left join OrganizationInfo O on o.AutoMemberID =A.GranteeID  left join IndSpouse ISP on isp.AutoMemberID =A.GranteeID  left join Child Ch on ch.ChildNumber =A.ChildNumber where A.ChildNumber ="+childID + YCont1 ;
     sqlstr1 += " union all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Award Amount', cast((select SUM(GrossAmount)from USSAward where ChildNumber=" + childID + YCont3 +") as varchar(50))  union all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Paid Amount', cast((select SUM(Amount) from usspayment where Childnumber =" + childID + YCont2 +")as varchar(50)) ";
 

     DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr);
     DataSet ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr1);

     if (ds1.Tables[0].Rows.Count  > 0)
     {
         DataTable dt1 = ds1.Tables[0];
         gvdrilaward.DataSource = dt1;
         gvdrilaward.DataBind();
     }
     if (ds2.Tables[0].Rows.Count > 0)
     {
         DataTable dt2 = ds2.Tables[0];
         gvdrilpayment.DataSource = dt2;
         gvdrilpayment.DataBind();
     }
 }
 protected void LinkButton1_Click(object sender, EventArgs e)
 {
     divdrildown.Visible = false;
     divrecon.Visible = true;
 }
 protected void btnSubmit_Click(object sender, EventArgs e)
 {
     lbluntil.Text = "";
     if (ddlYearUntil.SelectedItem.Text != "[Select Year]")
     {
         FechReconlist();
     }
     else
     {
         lbluntil.Text = "Please select a Year.";
     }
 }
 protected void LinkButton3_Click(object sender, EventArgs e)
 {
     //FetchChildDetails(ddlChName.SelectedItem.Value.ToString());
     //divchildview.Visible = true;
 }
 protected void LinkButton2_Click(object sender, EventArgs e)
 {
     //divchildview.Visible = false;
 }
 private void FetchChildDetails(string childID )
 {
     string sqlstr = "with vtl as (select A.ContestYear,A.ProductGroupID,A.ProductGroupCode,A.ProductID,A.ProductCode,A.ChildNumber,ch.first_name+' ' +ch.last_name as ChildName,A.Grade,A.Rank,cast(A.GrossAmount as decimal(19,0)) as GrossAmount ,A.SponsorID, ";
     sqlstr += " CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName, A.DonorType,A.PaidFull from USSAward A  ";
     sqlstr += " left join Child Ch on ch.ChildNumber =a.ChildNumber left join OrganizationInfo O on o.AutoMemberID =a.SponsorID left join IndSpouse ISP on isp.AutoMemberID =a.SponsorID where A.ChildNumber =" + childID + " )";
     sqlstr += " select * from vtl union  all select null,null,null,null,null,null,null,null,null,null,null,null,'Total Sponsored',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is not null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,'Total Others',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,'Total Amount',cast (SUM(vtl.grossamount)as varchar(50)) from vtl  order by ContestYear Desc,ProductGroupID ,ProductID ,rank";

     DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr);

     if (ds1.Tables[0].Rows.Count > 0)
     {
         DataTable dt1 = ds1.Tables[0];
         gvChildDetails.DataSource = dt1;
         gvChildDetails.DataBind();
     } 
 }
 protected void gvChDetails_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[11].Text == "Total Sponsored")
         {
             e.Row.Cells[11].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[12].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[11].Font.Bold = true;
             e.Row.Cells[12].Font.Bold = true;
         }
         else if (e.Row.Cells[11].Text == "Total Others")
         {
             e.Row.Cells[11].BackColor = System.Drawing.Color.GreenYellow;
             e.Row.Cells[12].BackColor = System.Drawing.Color.GreenYellow;
             e.Row.Cells[11].Font.Bold = true;
             e.Row.Cells[12].Font.Bold = true;
         }
         else if (e.Row.Cells[11].Text == "Total Amount")
         {
             e.Row.Cells[11].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[12].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[11].Font.Bold = true;
             e.Row.Cells[12].Font.Bold = true;
         }
     }
 }
 protected void LinkButton2_Click1(object sender, EventArgs e)
 {
     divchdetails.Visible = false;
 }
 protected void LinkButton3_Click1(object sender, EventArgs e)
 {
     if (ddlChName.SelectedItem.Value != "-1")
     {
         FetchChildDetails(CHId(ddlChName).ToString());
         divchdetails.Visible = true;
     }
     else
     {

     }

 }

 protected void gvChildDetails_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[12].Text == "Total Sponsored")
         {             
             e.Row.Cells[12].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[13].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[12].Font.Bold = true;
             e.Row.Cells[13].Font.Bold = true;
         }
         else if (e.Row.Cells[12].Text == "Total Others")
         {           
             e.Row.Cells[12].BackColor = System.Drawing.Color.GreenYellow;
             e.Row.Cells[13].BackColor = System.Drawing.Color.GreenYellow;
             e.Row.Cells[12].Font.Bold = true;
             e.Row.Cells[13].Font.Bold = true;
         }
         else if (e.Row.Cells[12].Text == "Total Amount")
         {           
             e.Row.Cells[12].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[13].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[12].Font.Bold = true;
             e.Row.Cells[13].Font.Bold = true;
         }
     }
 }
 protected void gvdrilaward_RowEditing(object sender, GridViewEditEventArgs e)
 {    
     gvdrilaward.EditIndex = e.NewEditIndex;
     FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 private void FetchAwardForEdit(string aID)
 {
     string sqlstr = "with vtl as (select A.USSAwardID,A.ContestYear,A.ProductGroupID,A.ProductGroupCode,A.ProductID,A.ProductCode,A.ChildNumber,ch.first_name+' ' +ch.last_name as ChildName,A.Grade,A.Rank,cast(A.GrossAmount as decimal(19,0)) as GrossAmount ,A.SponsorID, ";
     sqlstr += " CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName, A.DonorType,A.PaidFull from USSAward A  ";
     sqlstr += " left join Child Ch on ch.ChildNumber =a.ChildNumber left join OrganizationInfo O on o.AutoMemberID =a.SponsorID left join IndSpouse ISP on isp.AutoMemberID =a.SponsorID where USSAwardID =" + aID + " )";
     sqlstr += " select * from vtl union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Sponsored',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is not null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Other',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is null ";
     sqlstr += " union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Amount',cast (SUM(vtl.grossamount)as varchar(50)) from vtl  order by ContestYear Desc,ProductGroupID ,ProductID ,rank";

     DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr);  
     if (ds1.Tables[0].Rows.Count > 0)
     {
         DataTable dt1 = ds1.Tables[0];
         gvdrilaward.DataSource = dt1;
         gvdrilaward.DataBind();
     }   
 }
 protected void gvdrilaward_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
 {
     gvdrilaward.EditIndex = -1;
     FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 protected void gvdrilaward_RowUpdating(object sender, GridViewUpdateEventArgs e)
 {  
     Label AwID = (Label)gvdrilaward.Rows[e.RowIndex].FindControl("lblID");    
     TextBox GrAmount = (TextBox)gvdrilaward.Rows[e.RowIndex].FindControl("txtGrossAmount");    
     SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE USSAward Set GrossAmount=" + GrAmount.Text + ",[ModifiedDate]=getdate(),[ModifiedBy]="+Session["LoginID"].ToString() +" where ussawardID=" + AwID.Text );
     gvdrilaward.EditIndex = -1;
     FetchDrildown(Session["ChIDDril"].ToString());
     FechReconlist();
     
    }
 protected void gvdrilpayment_RowEditing(object sender, GridViewEditEventArgs e)
 {
     gvdrilpayment.EditIndex = e.NewEditIndex;
     FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 protected void gvdrilpayment_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
 {
     gvdrilpayment.EditIndex = -1;
     FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 protected void gvdrilpayment_RowUpdating(object sender, GridViewUpdateEventArgs e)
 {
     Label PayID = (Label)gvdrilpayment.Rows[e.RowIndex].FindControl("lblID");
     TextBox Pamt = (TextBox)gvdrilpayment.Rows[e.RowIndex].FindControl("txtPaidAmt");
     TextBox rstAmt = (TextBox)gvdrilpayment.Rows[e.RowIndex].FindControl("txtRestAmt");
     TextBox urAmt = (TextBox)gvdrilpayment.Rows[e.RowIndex].FindControl("txtUrAmt");
     TextBox exAmt = (TextBox)gvdrilpayment.Rows[e.RowIndex].FindControl("txtExcessC");
     TextBox intAmt = (TextBox)gvdrilpayment.Rows[e.RowIndex].FindControl("txtIntrst");

     string PaidAmt = Pamt.Text.Trim() == ""  ? "NULL" : Pamt.Text;
     string RestAmt = rstAmt.Text.Trim() == "" ? "NULL" : rstAmt.Text;
     string UnrstAmt = urAmt.Text.Trim() == "" ? "NULL" : urAmt.Text;
     string ExCap = exAmt.Text.Trim() == "" ? "NULL" : exAmt.Text;
     string InterestAmt = intAmt.Text.Trim() == "" ? "NULL" : intAmt.Text;

     string sqlUpdate = "Update USSPayment Set [Amount]=" + PaidAmt + ",[RestAmt]=" + RestAmt + ",[UnrestAmt]=" + UnrstAmt  +",[ExcessOfCap]=" + ExCap + ",[Interest]=" + InterestAmt + ",[ModifiedDate]=getdate(),[ModifiedBy]="+Session["LoginID"].ToString() +" Where usspaidid=" + PayID.Text;
     SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlUpdate);
     gvdrilpayment.EditIndex = -1;
     FetchDrildown(Session["ChIDDril"].ToString());
     FechReconlist();
 }
 protected void btnCalculate_Click(object sender, EventArgs e)
 {
     Label4.Text = "";
     int yearFrom = 0;
     int yearTo = 0;
     if (ddlYearFrom.SelectedItem.Value.ToString() == "-1")
     {
         Label4.Text = "Invalid Selection.";
         return;
     }
     if (ddlYearTo.SelectedItem.Value.ToString() == "-1")
     {
         Label4.Text = "Invalid Selection.";
         return;
     }
       yearFrom = int.Parse ( ddlYearFrom.SelectedItem.Value.ToString ());
       yearTo = int.Parse (ddlYearTo.SelectedItem.Value.ToString ());
       if (yearFrom > yearTo)
       {
           Label4.Text = "Invalid Selection, Year-To must be greater or equal to Year-from.";
           return;
       }
       string sqlPayment = string.Empty ;
       string sqlPayment1 = " select SponsorName,SponsorID,DonorType, ";
       string balA= "100100";
       string sqlFirst = " (select 'Begining Balance' as SponsorName, null as SponsorID,null as DonorType,";
       string sqlTitle = "select 'Restricted Donations:' as SponsorName,null as SponsorID,null as DonorType,";
       string mainSQl = " from ( ";
       string dsql = " Select SponsorName,SponsorID,DonorType,";     
    for (int i = yearFrom ; i <= yearTo ; i++)
    {
        int j=i;        
        dsql += "PARSENAME(Convert(Varchar(10),Pvt.[" + i + "],1),2) as [FY " + i + "-" + (j + 1).ToString().Substring(2, 2).ToString() + "]";
        sqlPayment1 += "PARSENAME(Convert(Varchar(10),Pvt.[" + i + "],1),2) as [FY " + i + "-" + (j + 1).ToString().Substring(2, 2).ToString() + "]";
        if (i == yearFrom)
        {
            sqlFirst +=  "PARSENAME(CONVERT(VARCHAR(30), CAST("+ balA +" AS MONEY), 1),2) as [FY " + i + "-" + (j + 1).ToString().Substring(2, 2).ToString() + "]";
           // mainSQl += " select CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName ,A.SponsorID,A.DonorType,SUM(A.GrossAmount) as Amount ,'" +i.ToString () +"' As ContestYear from USSAward A left join OrganizationInfo O on O.AutoMemberID =A.SponsorID left join IndSpouse ISP on ISP.AutoMemberID =A.SponsorID inner join DonationsInfo D on D.MEMBERID =a.SponsorID and A.ContestYear =D.EventYear and D.PURPOSE ='USScholar'  and d.DepositDate between '05/01/" + i.ToString() + "' and '04/30/" + (j + 1).ToString() + "' where A.ContestYear between " + i.ToString () + " and " + (j + 1).ToString() + " and A.SponsorID is not null group by A.ContestYear,A.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,A.SponsorID " ;//) as tbl";
            mainSQl += " Select CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName ,D.MEMBERID as SponsorID,D.DonorType,SUM(D.Amount) as Amount ,'" + i.ToString() + "' As ContestYear from Donationsinfo D left join OrganizationInfo O on O.AutoMemberID=d.MEMBERID left join IndSpouse ISP on ISP.AutoMemberID =d.MEMBERID  where year(D.DepositDate) between " + i.ToString() + " and " + (j + 1).ToString() + "  and D.PURPOSE ='USScholar'  and d.DepositDate between '05/01/" + i.ToString() + "' and '04/30/"+ (j + 1).ToString() + "' group by year(D.DepositDate),D.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,D.MEMBERID";
            sqlPayment += "with tbl1 as (select CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,A.GranteeID as SponsorID,A.DonorType,SUM(A.Amount) as Amount ,'" + i.ToString() + "' as ContestYear from USSPayment A  left join OrganizationInfo O on O.AutoMemberID =A.GranteeID left join IndSpouse ISP on ISP.AutoMemberID =A.GranteeID  where A.DatePaid  between '05/01/" + i.ToString() + "' and '04/30/" + (j + 1).ToString() + "' and A.GranteeID   is not null group by A.DatePaid,A.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,A.GranteeID ";                
        }
        else
        {
            sqlFirst += "null" + " as [FY " + i + "-" + (j + 1).ToString().Substring(2, 2).ToString() + "]";
            sqlPayment += "union select CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,A.GranteeID as SponsorID,A.DonorType,SUM(A.Amount) as Amount ,'" + i.ToString() + "' as ContestYear from USSPayment A  left join OrganizationInfo O on O.AutoMemberID =A.GranteeID left join IndSpouse ISP on ISP.AutoMemberID =A.GranteeID  where A.DatePaid  between '05/01/" + i.ToString() + "' and '04/30/" + (j + 1).ToString() + "' and A.GranteeID   is not null group by A.DatePaid,A.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,A.GranteeID ";
           // mainSQl += " union select CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName ,A.SponsorID,A.DonorType,SUM(A.GrossAmount) as Amount ,'" + i.ToString() + "' As ContestYear from USSAward A left join OrganizationInfo O on O.AutoMemberID =A.SponsorID left join IndSpouse ISP on ISP.AutoMemberID =A.SponsorID inner join DonationsInfo D on D.MEMBERID =a.SponsorID and A.ContestYear =D.EventYear and D.PURPOSE ='USScholar'  and d.DepositDate between '05/01/" + i.ToString() + "' and '04/30/" + (j + 1).ToString() + "' where A.ContestYear between " + i.ToString() + " and " + (j + 1).ToString() + " and A.SponsorID is not null group by A.ContestYear,A.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,A.SponsorID ";//) as tbl"; 
            mainSQl += " union Select CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName ,D.MEMBERID as SponsorID,D.DonorType,SUM(D.Amount) as Amount ,'" + i.ToString() + "' As ContestYear from Donationsinfo D left join OrganizationInfo O on O.AutoMemberID=d.MEMBERID left join IndSpouse ISP on ISP.AutoMemberID =d.MEMBERID  where year(D.DepositDate) between " + i.ToString() + " and " + (j + 1).ToString() + "  and D.PURPOSE ='USScholar'  and d.DepositDate between '05/01/" + i.ToString() + "' and '04/30/" + (j + 1).ToString() + "' group by year(D.DepositDate),D.DonorType ,O.ORGANIZATION_NAME ,ISP.FirstName,ISP.LastName,D.MEMBERID";

        }        
        sqlTitle += "null";
        if (i < yearTo) 
        { 
            dsql += ","; 
            sqlFirst += ","; 
            sqlTitle += ","; 
            sqlPayment1 += ","; 
        }
        else 
        {
            if (i == yearTo) 
            {
                dsql += mainSQl + " ) as tbl  pivot (sum(Amount) for tbl.ContestYear in (";
                sqlFirst += ") union all "; sqlTitle += " union all "; 
                sqlPayment1+=" from tbl1 pivot (sum(Amount) for tbl1.ContestYear in (";
                sqlPayment += ")";
             
            } 
        }
    }
    for (int i = yearFrom ; i <= yearTo ; i++)
    {
        dsql += "[" + i + "]";
        sqlPayment1 += "[" + i + "]";
        if (i < yearTo) { dsql += ","; sqlPayment1 += ","; } else { if (i == yearTo) { dsql += ")) as Pvt"; sqlPayment1 += ")) As Pvt"; } }
    }  
   //  Response .Write (sqlFirst + sqlTitle + dsql );
    DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlFirst + sqlTitle + dsql);
    DataSet ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlPayment + sqlPayment1);
    DataTable dt=null;
    DataTable dt1=null;
    if (ds1.Tables[0].Rows.Count > 0)
    {
        dt = ds1.Tables[0];
        DataRow dr = dt.NewRow();
        dr[0] = "Donations Sub-Total";
        for (int i = 3; i <= dt.Columns.Count - 1; i++)
        {
            dr[i] = 0;
            double sum = 0;
            string suma = "";
            foreach (DataRow drnew in dt.Rows)
            {
                if(drnew != dt.Rows[0])
                {
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }
                    }
                }
            }
            
            dr[i] = String.Format("{0:0,0}", sum);
        }
        dt.Rows.Add(dr);
        if (ds2.Tables[0].Rows.Count > 0)
        {
            DataRow dr2 = dt.NewRow();
            dr2[0] = "Disbursements/Restriction Released:";
            dt.Rows.Add(dr2);
        }
    }
    if (ds2.Tables[0].Rows.Count > 0)
    {
        dt1 = ds2.Tables[0];
        DataRow dr1 = dt1.NewRow();

        dr1[0] = "Disbursement Sub-Total";
        for (int i = 3; i <= dt1.Columns.Count - 1; i++)
        {
            dr1[i] = 0;
            double sum = 0;
            string suma = "";
            foreach (DataRow drnew in dt1.Rows)
            {              
                    if (!DBNull.Value.Equals(drnew[i]))
                    {
                        suma = drnew[i].ToString();
                        if (suma != "")
                        {
                            sum += Convert.ToDouble(drnew[i]);
                        }
                    }               
            }
            dr1[i] = String.Format("{0:0,0}", sum);
        }
        dt1.Rows.Add(dr1);
    }
    DataTable dtF = new DataTable();
    if (dt!=null)
    {
        dtF = dt.Copy();
    }
    if (dt1!=null)
    {
        dtF.Merge(dt1);
    }
    //object sumObject;
    //sumObject = table.Compute("Sum(Total)", "EmpID = 5");
    DataRow dr3 = dtF.NewRow();
    dr3[0] = "Ending Balance:";
    for (int i = 3; i <= dtF.Columns.Count - 1; i++)
    {
        dr3[i] = 0;
        double sum = 0;
        string suma = "";
        foreach (DataRow drnew in dtF.Rows)
        {
            
            if (!DBNull.Value.Equals(drnew[i]))
            {
                suma = drnew[i].ToString();
                //dtF.Rows.Add(dr3);
                if (suma != "")
                {
                   if( drnew[0].ToString() == "Begining Balance")
                   {
                        sum += Convert.ToDouble(drnew[i]);
                   }
                   if (drnew[0].ToString() == "Donations Sub-Total")
                   {
                       sum = sum + Convert.ToDouble(drnew[i]);
                   }
                   if (drnew[0].ToString() == "Disbursement Sub-Total")
                   {
                       sum = sum - Convert.ToDouble(drnew[i]);
                   }
                }
            }
           
         }
        if (i < dtF.Columns.Count - 1)
        {
            dtF.Rows[0][i + 1] = String.Format("{0:0,0}", sum); 
        }
        dr3[i] = String.Format("{0:0,0}", sum);      
    }
    dtF.Rows.Add(dr3);
    if (dtF.Rows.Count > 0)
    {
        GridView1.DataSource = dtF;
        GridView1.DataBind();
        Session["ExpSummary"] = dtF;
        btnExportSummary.Enabled = true;
    }
    else
    {
        btnExportSummary.Enabled = false;
    }   
 }
 protected void GridView1_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[1].Text == "Donations Sub-Total")
         {
             //e.Row.Cells[0].Enabled   = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             for (int i = 0; i < e.Row.Cells.Count; i++)
             {
                 e.Row.Cells[i].BackColor = System.Drawing.Color.Yellow;
                 e.Row.Cells[i].Font.Bold = true;
             }
         }
         else if (e.Row.Cells[1].Text == "Disbursement Sub-Total")
         {
             //e.Row.Cells[0].Enabled = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             for (int i = 0; i < e.Row.Cells.Count; i++)
             {
                 e.Row.Cells[i].BackColor = System.Drawing.Color.Yellow;
                 e.Row.Cells[i].Font.Bold = true;
             }
         }
         else if (e.Row.Cells[1].Text == "Begining Balance")
         {
             //e.Row.Cells[0].Enabled = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             for (int i = 0; i < e.Row.Cells.Count; i++)
             {
                 e.Row.Cells[i].BackColor = System.Drawing.Color.Lime;
                 e.Row.Cells[i].Font.Bold = true;
             }
         }
         else if (e.Row.Cells[1].Text == "Disbursements/Restriction Released:")
         {
                 //e.Row.Cells[0].Enabled = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
                 lb.Visible = false;
                 e.Row.Cells[0].BackColor = System.Drawing.Color.LightSkyBlue;
                 e.Row.Cells[1].BackColor = System.Drawing.Color.LightSkyBlue;
                 e.Row.Cells[1].Font.Bold = true;
                // e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
         }
         else if (e.Row.Cells[1].Text == "Restricted Donations:")
         {
             //e.Row.Cells[0].Enabled = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             e.Row.Cells[0].BackColor = System.Drawing.Color.LightSkyBlue;
             e.Row.Cells[1].BackColor = System.Drawing.Color.LightSkyBlue;
             e.Row.Cells[1].Font.Bold = true;
            // e.Row.Cells[0].ForeColor = System.Drawing.Color.White;
         }
         else if (e.Row.Cells[1].Text == "Ending Balance:")
         {
             //e.Row.Cells[0].Enabled = false;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
             for (int i = 0; i < e.Row.Cells.Count; i++)
             {
                 e.Row.Cells[i].BackColor = System.Drawing.Color.Aqua;
                 e.Row.Cells[i].Font.Bold = true;               
             }
         }
     }
 }
 protected void btnExportSummary_Click(object sender, EventArgs e)
 {
     GeneralExport1((DataTable)Session["ExpSummary"], "ReportSummary.xls");

    // ExportSummary((DataTable)Session["ExpSummary"]);
 }
 public void ExportSummary(DataTable dtdata)
 {
     string attach = string.Empty;
     attach = "attachment;filename=ReportSummary.xls";
     Response.ClearContent();
     Response.AddHeader("content-disposition", attach);
     Response.ContentType = "application/vnd.xls";// "application/vnd.openxmlformats";// "application/vnd.xls";//ms-excel";
     if (dtdata != null)
     {
         foreach (DataColumn dc in dtdata.Columns)
         {
             //Response.Write(dc.ColumnName + "\t");
             //sep = ";";
         }
         //Response.Write(System.Environment.NewLine);
         foreach (DataRow dr in dtdata.Rows)
         {
             for (int i = 0; i < dtdata.Columns.Count; i++)
             {

                 //Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
             }
             //Response.Write("\n");
         }
         Response.Flush();
         Response.End();
     }
 }
 protected void GridView1_RowCommand(object sender, GridViewCommandEventArgs e)
 {
     try
     {
         int index = int.Parse(e.CommandArgument.ToString());
         
         if (e.CommandName == "Select")
         {             
             Session["SAIDD"]=GridView1.Rows[index].Cells[2].Text;
             Session["RowID"] = index;
             FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);
             divSummary.Visible = false;
             divsumdrildown.Visible = true;
         }
     }
     catch (Exception ex) { }  
 }
 private void FetchDrildSummary(string childID,string YF,string YT)
 {
     int Ytwo = int.Parse(YT);
     Ytwo += 1;
     bool Award = false;
     bool payment = false;

     for (int i = (int)Session["RowID"]; i <= GridView1.Rows.Count; i++)
      {
        string text= GridView1.Rows[i].Cells[1].Text;
        if (text == "Donations Sub-Total")
        {
            Award = true;
            break;            
        }
        if (text == "Disbursement Sub-Total")
        {
            payment = true;
            break;
        }
      }
     string sqlstr1 = string.Empty ;
     string sqlstr = string.Empty; 
     sqlstr = "with vtl as (select A.USSAwardID,A.ContestYear,A.ProductGroupID,A.ProductGroupCode,A.ProductID,A.ProductCode,A.ChildNumber,ch.first_name+' ' +ch.last_name as ChildName,A.Grade,A.Rank,cast(A.GrossAmount as decimal(19,0)) as GrossAmount,convert (varchar(10),d.DepositDate,101) as DepositeDate,A.SponsorID, ";
     sqlstr += " CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName, A.DonorType,A.PaidFull from USSAward A  ";
     sqlstr += " left join Child Ch on ch.ChildNumber =a.ChildNumber left join OrganizationInfo O on o.AutoMemberID =a.SponsorID left join IndSpouse ISP on isp.AutoMemberID =a.SponsorID left join DonationsInfo D on D.MEMBERID =a.SponsorID and d.EventYear =a.ContestYear and d.PURPOSE ='USScholar'  where a.SponsorID =" + childID + " and a.ContestYear between " +YF+ " and " +YT+" )";
     sqlstr += " select * from vtl union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Sponsored',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is not null ";

     sqlstr1 = "select A.USSPaidID,a.Childnumber,ch.first_name+' ' +ch.last_name as ChildName, A.MemberID, cast (A.Amount as decimal(19,0))as PaidAmount,cast (A.RestAmt as decimal(19,0))as RestAmt,cast(A.UnrestAmt as decimal(19,0))as UnrestAmt,cast(A.ExcessOfCap as decimal(19,0))as ExcessOfCap,cast(A.Interest as decimal(19,0))as Interest, convert (varchar(10),A.DatePaid,101) as DatePaid,A.CheckNumber,A.BankID,A.GranteeID, CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as GranteeName,";
     sqlstr1 += "  A.DonorType,A.PaidFull   from USSPayment A  left join OrganizationInfo O on o.AutoMemberID =A.GranteeID  left join IndSpouse ISP on isp.AutoMemberID =A.GranteeID  left join Child Ch on ch.ChildNumber =A.ChildNumber where A.DatePaid  between '05/01/" + YF + "' and '04/30/" + Ytwo + "' and A.GranteeID   is not null and A.GranteeID =" + childID;
     sqlstr1 += " union all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Paid Amount', cast((select SUM(Amount) from usspayment where DatePaid  between '05/01/" + YF + "' and '04/30/" + Ytwo + "' and GranteeID   is not null and GranteeID = " + childID + ")as varchar(50)) ";

     if (Award == true)
     {
         sqlstr1 = "select A.USSPaidID,a.Childnumber,ch.first_name+' ' +ch.last_name as ChildName, A.MemberID, cast (A.Amount as decimal(19,0))as PaidAmount,cast (A.RestAmt as decimal(19,0))as RestAmt,cast(A.UnrestAmt as decimal(19,0))as UnrestAmt,cast(A.ExcessOfCap as decimal(19,0))as ExcessOfCap,cast(A.Interest as decimal(19,0))as Interest, convert (varchar(10),A.DatePaid,101) as DatePaid,A.CheckNumber,A.BankID,A.GranteeID, CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as GranteeName,";
         sqlstr1 += "  A.DonorType,A.PaidFull   from USSPayment A  left join OrganizationInfo O on o.AutoMemberID =A.GranteeID  left join IndSpouse ISP on isp.AutoMemberID =A.GranteeID  left join Child Ch on ch.ChildNumber =A.ChildNumber where A.DatePaid  between '05/01/" + YF + "' and '04/30/" + Ytwo + "' and A.GranteeID   is not null and a.ChildNumber in (select ChildNumber from USSAward where SponsorID =" + childID + " and contestyear between " + YF + " and " + Ytwo + " and PaidFull='Y')";
         sqlstr1 += " union all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Paid Amount', cast((select SUM(Amount) from usspayment where DatePaid  between '05/01/" + YF + "' and '04/30/" + Ytwo + "' and GranteeID   is not null and ChildNumber in (select ChildNumber   from USSAward where SponsorID =" + childID + " and contestyear between " + YF + " and " + Ytwo +" and PaidFull='Y'))as varchar(50)) ";

     }
     if (payment == true)
     {
         sqlstr = "with vtl as (select A.USSAwardID,A.ContestYear,A.ProductGroupID,A.ProductGroupCode,A.ProductID,A.ProductCode,A.ChildNumber,ch.first_name+' ' +ch.last_name as ChildName,A.Grade,A.Rank,cast(A.GrossAmount as decimal(19,0)) as GrossAmount,convert (varchar(10),d.DepositDate,101) as DepositeDate,A.SponsorID, ";
         sqlstr += " CASE WHEN A.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + COALESCE (ISP.MiddleInitial, '') + ' ' + ISP.LastName, ' ', ' ') END as SponsorName, A.DonorType,A.PaidFull from USSAward A  ";
         sqlstr += " left join Child Ch on ch.ChildNumber =a.ChildNumber left join OrganizationInfo O on o.AutoMemberID =a.SponsorID left join IndSpouse ISP on isp.AutoMemberID =a.SponsorID left join DonationsInfo D on D.MEMBERID =a.SponsorID and d.EventYear =a.ContestYear and d.PURPOSE ='USScholar'  where A.ChildNumber in (Select ChildNumber from  usspayment where granteeid =" + childID + ") and a.SponsorID is not null )";
         sqlstr += " select * from vtl union  all select null,null,null,null,null,null,null,null,null,null,null,null,null,null,'Total Sponsored',cast (SUM(vtl.grossamount)as varchar(50)) from vtl where vtl.SponsorID is not null ";
     }

     string sqlDnfo = "Select D.DonationNumber,D.Transaction_Number,CASE WHEN D.DonorType='OWN' THEN O.ORGANIZATION_NAME ELSE REPLACE(ISP.FirstName + ' ' + ISP.LastName, ' ', ' ') END as SponsorName,D.MEMBERID as SponsorID,D.DonorType,cast (D.Amount as decimal(19,0))as Amount,year(d.DepositDate) as EventYear,convert (varchar(10),D.DepositDate,101) as DepositDate ,D.PURPOSE,d.DonationType from Donationsinfo D left join OrganizationInfo O on O.AutoMemberID =D.MEMBERID left join IndSpouse ISP on ISP.AutoMemberID =D.MEMBERID   where D.DepositDate between '05/01/" + YF + "' and '04/30/" + Ytwo + "' AND D.PURPOSE='USScholar' and D.Memberid=" + childID;
     sqlDnfo += "  union select  null,null,null,null,null,null,null,null,'Total',convert (varchar(50),cast(SUM(amount) as decimal (19,0)))from DonationsInfo  where DepositDate between '05/01/" + YF + "' and '04/30/" + Ytwo + "' AND PURPOSE='USScholar' and Memberid=" + childID;
     
     DataSet ds1 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr);
     DataSet ds2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlstr1);
     DataSet ds3 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, sqlDnfo);
     if (ds1.Tables[0].Rows.Count > 0)
     {
         DataTable dt1 = ds1.Tables[0];
         gvAwardDetails1.DataSource = dt1;
         gvAwardDetails1.DataBind();
     }
     if (ds2.Tables[0].Rows.Count > 0)
     {          
         DataTable dt2 = ds2.Tables[0];
         gvPaymentDetails.DataSource = dt2;
         gvPaymentDetails.DataBind();
     }
     if (ds3.Tables[0].Rows.Count > 0)
     {          
         DataTable dt3 = ds3.Tables[0];
         gvDinfo.DataSource = dt3;
         gvDinfo.DataBind();
     } 
 }
 protected void LinkButton4_Click(object sender, EventArgs e)
 {
     divsumdrildown.Visible = false;
     divSummary.Visible = true;
 }
 protected void gvAwardDetails_RowDataBound(object sender, GridViewRowEventArgs e)
 {
      for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[14].Text == "Total Sponsored")
         {
             e.Row.Cells[14].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[14].Font.Bold = true;
             e.Row.Cells[15].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[15].Font.Bold = true;
               Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
         }   
     }
 }
 protected void gvPaymentDetails_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         if (e.Row.Cells[15].Text == "Total Paid Amount")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[16].Font.Bold = true;
             Button lb = (Button)e.Row.Cells[0].Controls[0];
             lb.Visible = false;
         }
     }   
 }
 protected void gvAwardDetails1_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
 {
     gvAwardDetails1.EditIndex = -1;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);

 }
 protected void gvAwardDetails1_RowEditing(object sender, GridViewEditEventArgs e)
 {
     gvAwardDetails1.EditIndex = e.NewEditIndex;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);
 }
 protected void gvAwardDetails1_RowUpdating(object sender, GridViewUpdateEventArgs e)
 {
     Label AwID = (Label)gvAwardDetails1.Rows[e.RowIndex].FindControl("lblID1");
     TextBox GrAmount = (TextBox)gvAwardDetails1.Rows[e.RowIndex].FindControl("txtGrossAmount1");
     SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, "UPDATE USSAward Set GrossAmount=" + GrAmount.Text + ",[ModifiedDate]=getdate(),[ModifiedBy]=" + Session["LoginID"].ToString() + " where ussawardID=" + AwID.Text);
     gvAwardDetails1.EditIndex = -1;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);
 }
 protected void gvAwardDetails1_RowCommand(object sender, GridViewCommandEventArgs e)
 {
    
 }
 protected void gvAwardDetails1_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {

         Button lb = (Button)e.Row.Cells[0].Controls[0].FindControl("btnEdit0");
         Label lblCap = (Label)e.Row.Cells[15].Controls[0].FindControl("lblDtype0");

         if (lblCap.Text == "Total Sponsored")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].Font.Bold = true;
             lb.Visible = false;
         }

     }
 }
 protected void gvPaymentDetails_RowCancelingEdit(object sender, GridViewCancelEditEventArgs e)
 {
     gvPaymentDetails.EditIndex = -1;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);

    // FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 protected void gvPaymentDetails_RowDataBound1(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");

     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
         Button lb = (Button)e.Row.Cells[0].Controls[0].FindControl("btnEdit2");
         Label lblCap = (Label)e.Row.Cells[15].Controls[0].FindControl("lblDonorType2");
         if (lblCap.Text == "Total Award Amount")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].Font.Bold = true;
             lb.Visible = false;
         }
         else if (lblCap.Text == "Total Paid Amount")
         {
             e.Row.Cells[15].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[16].BackColor = System.Drawing.Color.Aqua;
             e.Row.Cells[15].Font.Bold = true;
             e.Row.Cells[16].Font.Bold = true;
             lb.Visible = false;
         }
     }
 }
 protected void gvPaymentDetails_RowEditing(object sender, GridViewEditEventArgs e)
 {
     gvPaymentDetails.EditIndex = e.NewEditIndex;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);

     //FetchDrildown(Session["ChIDDril"].ToString()); 
 }
 protected void gvPaymentDetails_RowUpdating(object sender, GridViewUpdateEventArgs e)
 {
     Label PayID = (Label)gvPaymentDetails.Rows[e.RowIndex].FindControl("lblID2");
     TextBox Pamt = (TextBox)gvPaymentDetails.Rows[e.RowIndex].FindControl("txtPaidAmt2");
     TextBox rstAmt = (TextBox)gvPaymentDetails.Rows[e.RowIndex].FindControl("txtRestAmt2");
     TextBox urAmt = (TextBox)gvPaymentDetails.Rows[e.RowIndex].FindControl("txtUrAmt2");
     TextBox exAmt = (TextBox)gvPaymentDetails.Rows[e.RowIndex].FindControl("txtExcessC2");
     TextBox intAmt = (TextBox)gvPaymentDetails.Rows[e.RowIndex].FindControl("txtIntrst2");

     string PaidAmt = Pamt.Text.Trim() == "" ? "NULL" : Pamt.Text;
     string RestAmt = rstAmt.Text.Trim() == "" ? "NULL" : rstAmt.Text;
     string UnrstAmt = urAmt.Text.Trim() == "" ? "NULL" : urAmt.Text;
     string ExCap = exAmt.Text.Trim() == "" ? "NULL" : exAmt.Text;
     string InterestAmt = intAmt.Text.Trim() == "" ? "NULL" : intAmt.Text;

     string sqlUpdate = "Update USSPayment Set [Amount]=" + PaidAmt + ",[RestAmt]=" + RestAmt + ",[UnrestAmt]=" + UnrstAmt + ",[ExcessOfCap]=" + ExCap + ",[Interest]=" + InterestAmt + ",[ModifiedDate]=getdate(),[ModifiedBy]=" + Session["LoginID"].ToString() + " Where usspaidid=" + PayID.Text;
     SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sqlUpdate);
     gvPaymentDetails.EditIndex = -1;
     FetchDrildSummary(Session["SAIDD"].ToString(), ddlYearFrom.SelectedItem.Text, ddlYearTo.SelectedItem.Text);

 }

 public void GeneralExport(DataTable dtdata, string fname)
 {
     string attach = string.Empty;
     attach = "attachment;filename="+ fname;
     Response.ClearContent();
     Response.AddHeader("content-disposition", attach);
     Response.ContentType = "application/vnd.xls";
     if (dtdata != null)
     {
         foreach (DataColumn dc in dtdata.Columns)
         {
             Response.Write(dc.ColumnName + "\t");
             //sep = ";";
         }
         Response.Write(System.Environment.NewLine);
         foreach (DataRow dr in dtdata.Rows)
         {
             for (int i = 0; i < dtdata.Columns.Count; i++)
             {

                 Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
             }
             Response.Write("\n");
         }

         // Response.Write(sw.ToString());
         //response.End()                           //       Thread was being aborted.
         Response.Flush();
         HttpContext.Current.Response.Flush();
         HttpContext.Current.Response.SuppressContent = true;
         HttpContext.Current.ApplicationInstance.CompleteRequest();
     }
 }
 protected void btnExport1_Click(object sender, EventArgs e)
 {
     GeneralExport((DataTable)Session["Exportusaw"],"USScholarshipAwardList.xls"); 
 }
 protected void btnExport2_Click(object sender, EventArgs e)
 {
     GeneralExport((DataTable)Session["ExportusPay"], "USSPaymentList.xls");
 }
 protected void gvDinfo_RowDataBound(object sender, GridViewRowEventArgs e)
 {
     for (int i = 0; i < e.Row.Cells.Count; i++)
     {
         e.Row.Cells[i].Attributes.Add("style", "white-space: nowrap;");
     }
     if (e.Row.RowType == DataControlRowType.DataRow)
     {
     if(e.Row.Cells[8].Text =="Total")
          {
             e.Row.Cells[8].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[9].BackColor = System.Drawing.Color.Yellow;
             e.Row.Cells[8].Font.Bold = true;
             e.Row.Cells[9].Font.Bold = true;
           }

     }

 }
 protected void NoDoll()
 {
     if (DDNoDoller.SelectedValue == "2")
     {
         NoDoller = "'Y'";
     }
     else
     {
         NoDoller = "NULL";
     }
 }
 protected void Btnexcel_Click(object sender, EventArgs e)
 {
   
 

     if (ddlChoice.SelectedItem.Value.ToString() == "7")
     {
         GeneralExport((DataTable)Session["GranteeList"], "GranteesList.xls");
     }
     else if (ddlChoice.SelectedItem.Value.ToString() == "8")
     {
         GeneralExport((DataTable)Session["Liability"], "Liability Report.xls");
     }
     else
     {
         GeneralExport((DataTable)Session["SponsorList"], "SponsorsContactList.xls");
        // GeneralExport((DataTable)Session["SponsorList1"], "SponsorsContactList.xls");
     }
    
 }
 protected void Gvlist_RowDataBound(object sender, GridViewRowEventArgs e)
 {
    
 }


 protected void GVliability_PageIndexChanging(object sender, GridViewPageEventArgs e)
 {
     try
     {
         GVliability.PageIndex = e.NewPageIndex;
         GVliability.DataSource = (DataTable)Session["Liability"];
         GVliability.DataBind();
     }
     catch (Exception ex) { }
 }
 protected void DDNoDoller_SelectedIndexChanged(object sender, EventArgs e)
 {
     NoDolConditions();
 }
 protected void NoDolConditions()
 {
     if (DDNoDoller.SelectedValue == "2")
     {
         txtAmount.Text = string.Empty;
         txtAmount.Enabled = false;
         ddlPFull.SelectedValue = "-1";
         ddlPFull.Enabled = false;
         txtSponsor.Text = string.Empty;
         txtSponsor.Enabled = false;
         Button4.Enabled = false;
     }
     else
     {
         Button4.Enabled = true;
         txtAmount.Enabled = true;
         txtSponsor.Enabled = true;
         ddlPFull.Enabled = true;
     }
 }
}
