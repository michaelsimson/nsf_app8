﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports System.Web.Services

Partial Class FundRReg
    Inherits System.Web.UI.Page
    Dim IsDinnerPaid As Boolean = False
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Not IsPostBack Then
            Session("ContestRegDetails") = Nothing

            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                loadDropdown()
                Session("FundDonorType") = "IND"
                Session("FundEmail") = Session("LoginEmail")
                hlnkMainPage.NavigateUrl = "UserFunctions.aspx"
                hlnkMainPage.Text = "Back to Parent Functions"
                Session("EventId") = 9
            ElseIf Session("entryToken").ToString.ToUpper() = "DONOR" Then
                loadDropdown()
                Session("FundDonorType") = "IND"
                Session("FundEmail") = Session("LoginEmail")
                hlnkMainPage.NavigateUrl = "DonorFunctions.aspx"
                hlnkMainPage.Text = "Back to Donor Functions"
                Session("EventId") = 9
            ElseIf Request.QueryString("id") = 1 And Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                'To search for others
                pIndSearch.Visible = True
                trH1.Visible = False
                trH2.Visible = False
                Session("EventID") = 9
            ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Session("EventId") = 9
                'his own volunteer
                Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType,Email,chapterID,FirstName+' '+LastName as Name from Indspouse where automemberid=" & Session("LoginId") & "")
                While reader.Read()
                    If reader("DonorType").ToUpper() = "IND" Then
                        lblCustIndID.Text = reader("Automemberid")
                        Session("FundEmail") = reader("Email")
                        Session("CustIndID") = reader("Automemberid")
                        ltl1.Text = "Name : " & reader("Name")
                        Session("FundDonorType") = "IND"
                    ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                        lblCustIndID.Text = reader("Relationship")
                        Session("CustIndID") = reader("Relationship")
                        ltl1.Text = "Name : " & reader("Name")
                        Session("FundDonorType") = "IND"
                        setSpouseValues(Session("CustIndID"))
                    End If
                End While
                loadDropdown()
            Else
                Response.Redirect("maintest.aspx")
            End If
            Session("EventYear") = Now.Year
        End If
        GridCaption()
    End Sub

    Private Sub LoadChild()
        'Get grade details
        Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetContestCategoryGrade")
        Session("Grades") = dsGrade.Tables(0)
        Dim dt As New DataTable
        dt.Columns.Add("Id", GetType(Integer))
        dt.Columns.Add("ChildNumber", GetType(Integer))
        dt.Columns.Add("FIRST_NAME", GetType(String))
        dt.Columns.Add("LAST_NAME", GetType(String))
        dt.Columns.Add("Gender", GetType(String))
        dt.Columns.Add("DOB", GetType(String))
        dt.Columns.Add("Grade", GetType(Integer))
        dt.Columns.Add("SB", GetType(Integer))
        dt.Columns.Add("MB", GetType(Integer))
        dt.Columns.Add("SC", GetType(Integer))
        dt.Columns.Add("GB", GetType(Integer))
        Dim strSql As String
        strSql = "select TOP 3 *  from( SELECT C.ChildNumber ChildNumber,FIRST_NAME ,LAST_NAME,Gender,Convert(varchar(10),DATE_OF_BIRTH,101) DOB, C.Grade Grade,FCR.ProductGroupCode ProductGroupCode "
        strSql = strSql & " From Child C LEFT JOIN FundRContestReg FCR on C.ChildNumber=Fcr.ChildNumber  Where C.Memberid in (" & Session("CustIndID") & ")   GROUP BY C.CHILDNUMBER,FIRST_NAME ,LAST_NAME,Gender ,DATE_OF_BIRTH, C.Grade,FCR.ProductGroupCode"
        strSql = strSql & " ) as pvt pivot(count(ProductGroupCode) FOR ProductGroupCode IN (SB,MB,SC,GB)) AS d"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        Dim rCnt As Integer, iCnt As Int16
        rCnt = 1
        If ds.Tables(0).Rows.Count > 0 Then
            For iCnt = 0 To ds.Tables(0).Rows.Count - 1
                Dim dr As DataRow = ds.Tables(0).Rows(iCnt)
                dt.Rows.Add(rCnt, dr("ChildNumber"), dr("FIRST_NAME"), dr("LAST_NAME"), dr("Gender"), dr("DOB"), dr("Grade"), dr("SB"), dr("MB"), dr("SC"), dr("GB"))
                rCnt = rCnt + 1
            Next
        End If
        For iCnt = rCnt To 3
            dt.Rows.Add(iCnt, 0, "", "", "Male", "", 0, 0, 0, 0, 0)
        Next
        dgChild.DataSource = dt
        dgChild.DataBind()
        ViewState("ChildDetails") = dt
    End Sub

    Private Sub loadDropdown()
        Dim SQLstr As String = "select  FC.FundRCalID, FC.ChapterID, FC.ChapterCode, FC.EventYear, FC.VenueID, FC.EventDate, FC.StartTime + ' - ' + FC.EndTime as Timings, FC.Building, FC.CampStartDate, FC.CampEndDate, "
        SQLstr = SQLstr & " FC.EventDescription, O.ORGANIZATION_NAME as VenueName from FundRaisingCal FC Inner JOIN OrganizationInfo O ON FC.VenueID = O.AutoMemberID  "
        SQLstr = SQLstr & " where FC.CampStartDate < = GETDATE() and FC.CampEndDate >=GETDATE() order By FC.EventDate "
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLstr)
        If ds.Tables(0).Rows.Count > 0 Then
            trH1.Visible = True
            gvEvents.DataSource = ds
            gvEvents.DataBind()
            gvEvents.SelectedIndex = -1
            gvEvents.Visible = True
        Else
            gvEvents.DataSource = Nothing
            gvEvents.DataBind()
            gvEvents.Visible = False
        End If
    End Sub

    Protected Sub gvEvents_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles gvEvents.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim selectedRow As GridViewRow = gvEvents.Rows(index)
        lblEventDesc.Text = gvEvents.Rows(index).Cells(5).Text
        Session("EventDesc") = lblEventDesc.Text
        Session("EventDate") = gvEvents.Rows(index).Cells(2).Text
        lblEventDate.Text = Session("EventDate")
        lblFundRCalID.Text = gvEvents.DataKeys(index).Value
        loadPaidgrid(gvEvents.DataKeys(index).Value)
        loadgrid(gvEvents.DataKeys(index).Value)
    End Sub

    Private Sub loadgrid(ByVal FundRCalID As Integer)
        Dim strSQL As String = "SELECT FF.FundRFeesID, FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID, CASE WHEN FR.Quantity IS NULL THEN 0 else FR.Quantity END AS Quantity,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN  FF.Share = 'No' THEN FF.FeeFrom ELSE FR.UnitFee END else FR.UnitFee END AS  FEE,CASE WHEN FR.UnitFee IS NULL THEN CASE WHEN FF.FeeTo IS NULL AND FF.Share = 'No' THEN 'False' ELSE 'True' END else 'True' END AS  FeeEnabled, Case When FF.Exclusive = 'Yes' OR FF.Share ='Yes' THEN 'False' ELSE 'TRUE' END AS QuantityEnabled, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.PaymentMode, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, ISNULL(FR.TotalAmt,0) TotalAmt ,FR.PaymentMode as SelPaymentMode"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID Left JOIN FundRReg FR ON FR.PaymentReference is Null and FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear and FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType ='" & Session("FundDonorType") & "'"
        strSQL = strSQL & " WHERE FF.FundRCalID = " & FundRCalID
        strSQL = strSQL & " order by FF.ProductGroupID,FF.FeeFrom Desc"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim dr As DataRow, i As Int16
        For i = 0 To dt.Rows.Count - 1
            dr = dt.Rows(i)
            If dr.Item("ProductId").ToString = "105" Then
                Dim drNew As DataRow = dt.NewRow
                dt.Rows.InsertAt(dt.NewRow(), 5)
                Exit For
            End If
        Next
        If (dt.Rows.Count < 1) Then
            lblerr.Text = "No Products are for sale now."
        Else
            dgCatalog.DataSource = dt
            dgCatalog.DataBind()
            GridCaption()
            trH2.Visible = True
            lblerr.Text = ""
            'DisabelDinnerAtFirst()
            lblTotal.Text = "Total  :  $" & CalcAmount()
        End If

        loadPaidgrid(FundRCalID)
        lblerr.Text = ""
        DisabelDinnerAtFirst()
        lblTotal.Text = "Total  :  $" & CalcAmount()
    End Sub

    Private Sub loadPaidgrid(ByVal FundRCalID As Integer)
        ViewState("IsDinnerPaid") = "False"
        Dim strSQL As String = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
        strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, isnull(FR.TotalAmt,0) TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference"
        strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear"
        strSQL = strSQL & " WHERE FR.FundRCalID = " & FundRCalID & " AND FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "'"
        strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
            Dim dt As DataTable = ds.Tables(0)
            If dt.Rows.Count > 0 Then
                dgPaidProducts.DataSource = dt
                dgPaidProducts.DataBind()
                trH4.Visible = True
            Else
                trH4.Visible = False
            End If
        Catch ex As Exception
        End Try
    End Sub
    Private Sub DisabelDinnerAtFirst()
        If Not ViewState("IsDinnerPaid") Is Nothing Then
            Dim item As DataGridItem, lblProductId As Label, chk As CheckBox
            For Each item In dgCatalog.Items
                chk = CType(item.FindControl("chkSelect"), CheckBox)
                'If chk.Checked = True Then
                lblProductId = CType(item.FindControl("lblProductId"), Label)
                If lblProductId.Text = "73" Then
                    chk.Checked = True
                    chk.Enabled = False
                    Dim txtQuantity As TextBox = CType(item.FindControl("txtQuantity"), TextBox)
                    Dim txtAmount As TextBox = CType(item.FindControl("txtQuantity"), TextBox)
                    txtQuantity.Text = 1
                    If ViewState("IsDinnerPaid") = "False" Then
                        txtQuantity_TextChanged(txtQuantity, New EventArgs)
                    End If
                    txtQuantity.Enabled = False
                    txtAmount.Enabled = False
                    Exit For
                End If
                ' End If
            Next
            'End If
        End If
    End Sub
    Protected Sub dgPaidProducts_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                Dim lblamount As Label, lblStatus As Label, lblProductCode As Label, lblPaymentReference As Label, lblProductId As Label
                lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                lblProductCode = CType(e.Item.FindControl("lblProductCode"), Label)
                lblProductId = CType(e.Item.FindControl("lblProductId"), Label)
                lblPaymentReference = CType(e.Item.FindControl("lblPaymentReference"), Label)
                'check if dinner is already inserted 
                If lblProductId.Text.ToLower.Trim = "73" And lblPaymentReference.Text.ToLower <> "pending" Then
                    ViewState("IsDinnerPaid") = "True"
                End If
                If lblProductCode.Text.ToLower.Trim = "child" Or lblProductCode.Text.ToLower.Trim = "adult" Then
                    lblPaymentReference.Visible = False
                    Dim lblPaymentMode As Label = CType(e.Item.FindControl("lblPaymentMode"), Label)
                    lblPaymentMode.Visible = False
                End If
                lblamount.Text = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                Dim PaymentMode As String = e.Item.DataItem("PaymentMode").ToString().Trim
                If PaymentMode = "Credit Card" Then
                    Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblamount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf PaymentMode = "Check" Then
                    Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Text = lblamount.Text
                    lblCheckAmt.Visible = True
                ElseIf PaymentMode = "In Kind" Then
                    Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Text = lblamount.Text
                    lblInKindAmt.Visible = True
                End If
        End Select
    End Sub

    Protected Sub dgCatalog_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)
        Try
            'assign onkeyup for bee row's quty txt box to handle validation
            If e.Item.ItemIndex = 5 Then
                Exit Sub
            End If
            Select Case e.Item.ItemType

                Case ListItemType.Item, ListItemType.AlternatingItem, ListItemType.SelectedItem
                    Dim Chk1 As CheckBox = CType(e.Item.FindControl("chkSelect"), CheckBox)
                    Dim lblamount As Label, ddlPaymntMthd As DropDownList, lblStatus As Label, txtQuantity As TextBox, txtAmount As TextBox, lblFeeFrom As Label, lblFeeTo As Label, lblPaymentMethod As Label, lblAmt As Label
                    lblStatus = CType(e.Item.FindControl("lblStatus"), Label)
                    txtQuantity = CType(e.Item.FindControl("txtQuantity"), TextBox)
                    txtAmount = CType(e.Item.FindControl("txtAmount"), TextBox)
                    lblFeeFrom = CType(e.Item.FindControl("lblFeeFrom"), Label)
                    lblFeeTo = CType(e.Item.FindControl("lblFeeTo"), Label)
                    lblAmt = CType(e.Item.FindControl("lblAmt"), Label)
                    lblPaymentMethod = CType(e.Item.FindControl("lblPaymentMethod"), Label)
                    ddlPaymntMthd = CType(e.Item.FindControl("ddlPaymntMthd"), DropDownList)
                    If lblFeeTo.Text.Trim.Length = 0 Then
                        lblFeeTo.Text = 0
                    End If
                    If lblPaymentMethod.Text.Trim = "CreditCard only" Then
                        ddlPaymntMthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlPaymntMthd.Enabled = False
                    ElseIf lblPaymentMethod.Text.Trim = "CreditCard or Check only" Then
                        ddlPaymntMthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlPaymntMthd.Items.Insert(1, New ListItem("Check", "2"))
                        'ddlPaymntMthd.Enabled = True
                        ddlPaymntMthd.SelectedIndex = 0
                    ElseIf lblPaymentMethod.Text.Trim = "CreditCard, Check or In Kind" Then
                        ddlPaymntMthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlPaymntMthd.Items.Insert(1, New ListItem("Check", "2"))
                        ddlPaymntMthd.Items.Insert(2, New ListItem("In Kind", "3"))
                        ddlPaymntMthd.SelectedIndex = 0
                    End If
                    Try
                        lblAmt.Text = IIf(lblFeeTo.Text.Trim.Length > 1, Math.Round(Decimal.Parse(lblFeeFrom.Text.Trim), 0) & " - " & Math.Round(Decimal.Parse(lblFeeTo.Text.Trim), 0), Math.Round(Decimal.Parse(lblFeeFrom.Text.Trim), 0))
                    Catch ex As Exception
                    End Try
                    Dim lblProductID As Label
                    If lblStatus.Text = "0" Then
                        Chk1.Checked = False
                        txtQuantity.Enabled = False
                        txtAmount.Enabled = False
                        Dim lblProductGroupCode As Label
                        lblProductGroupCode = CType(e.Item.FindControl("lblProductGroupCode"), Label)
                        If lblProductGroupCode.Text.Trim = "USSchol" Then
                            lblProductID = CType(e.Item.FindControl("lblProductID"), Label)
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(P.ProductId) from  contestcategory C Inner Join Product P On p.EventId = 1 AND C.ContestCode= P.ProductCode WHERE C.contestyear = " & Session("EventYear") & " and C.NationalFinalsStatus = 'Active' and C.Contestcode<>'BB' AND C.ContestCode not in (Select SpProductCode from BeeRankSponsor where  EventYear = " & Session("EventYear") & " AND ProductID=" & lblProductID.Text & " and PaymentReference is Not null)") < 1 Then
                                Chk1.Enabled = False
                            End If
                        End If
                    Else
                        ddlPaymntMthd.Enabled = True
                        Chk1.Checked = True
                        txtQuantity.Enabled = True
                        txtAmount.Enabled = True
                        lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                        Dim totalAmt As Decimal = 0
                        If Not e.Item.DataItem("TotalAmt").Equals(DBNull.Value) Then
                            totalAmt = Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                        End If
                        lblamount.Text = totalAmt 'Math.Round(Decimal.Parse(e.Item.DataItem("TotalAmt")), 2)
                        Dim PaymentMode As String = e.Item.DataItem("SelPaymentMode").ToString().Trim
                        If PaymentMode = "Credit Card" Then
                            Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                            lblCreditCardAmt.Text = lblamount.Text
                            lblCreditCardAmt.Visible = True
                        ElseIf PaymentMode = "Check" Then
                            Dim lblCheckAmt As Label = CType(e.Item.FindControl("lblCheckAmt"), Label)
                            lblCheckAmt.Text = lblamount.Text
                            lblCheckAmt.Visible = True
                            ddlPaymntMthd.SelectedIndex = ddlPaymntMthd.Items.IndexOf(ddlPaymntMthd.Items.FindByText("Check"))
                        ElseIf PaymentMode = "In Kind" Then
                            Dim lblInKindAmt As Label = CType(e.Item.FindControl("lblInKindAmt"), Label)
                            lblInKindAmt.Text = lblamount.Text
                            ddlPaymntMthd.SelectedIndex = ddlPaymntMthd.Items.IndexOf(ddlPaymntMthd.Items.FindByText("In Kind"))
                            lblInKindAmt.Visible = True
                        End If

                    End If
                    lblProductID = CType(e.Item.FindControl("lblProductID"), Label)
                    If lblProductID.Text = "105" Then
                        txtQuantity.Enabled = False
                        Dim unPaidQty As Integer = GetUnPaidRegistration()
                        If unPaidQty > 0 Then
                            Chk1.Checked = True
                            txtAmount.Enabled = True
                            lblamount = CType(e.Item.FindControl("lblAmount"), Label)
                            lblamount.Text = unPaidQty * 10
                            txtQuantity.Text = unPaidQty
                            Dim lblCreditCardAmt As Label = CType(e.Item.FindControl("lblCreditCardAmt"), Label)
                            lblCreditCardAmt.Text = lblamount.Text
                            lblCreditCardAmt.Visible = True
                        End If
                    ElseIf lblProductID.Text = "134" Or lblProductID.Text = "135" Or lblProductID.Text = "136" Then
                        '134- Adult, 135- Child,105 - Contest
                        Dim ddlQty As DropDownList = e.Item.FindControl("ddlQty")
                        txtQuantity.Visible = False

                        ddlQty.Items.Add(New ListItem("Select", -1))
                        Dim i, startIndx, endIndx As Integer
                        startIndx = 1
                        endIndx = 6
                        If lblProductID.Text = "135" Or lblProductID.Text = "136" Then
                            startIndx = 0
                            endIndx = 4
                        End If
                        For i = startIndx To endIndx
                            ddlQty.Items.Add(New ListItem(i, i))
                        Next
                        If Chk1.Checked = False Then
                            ddlQty.SelectedIndex = 0
                        Else
                            ddlQty.SelectedIndex = ddlQty.Items.IndexOf(ddlQty.Items.FindByValue(txtQuantity.Text))
                        End If
                        If Chk1.Checked = True Then
                            ddlQty.Visible = True
                            ddlQty.Enabled = True
                        Else
                            ddlQty.Visible = True
                            ddlQty.Enabled = False
                        End If
                    End If
                    If lblProductID.Text = "105" Or lblProductID.Text = "134" Or lblProductID.Text = "135" Or lblProductID.Text = "136" Or lblProductID.Text = "73" Then
                        txtAmount.Enabled = False
                    End If
                    ' Hide the parent to register their child to stop registration with contest( because contest date is reached)
                    If lblProductID.Text = "105" Then
                        Chk1.Checked = False
                        Chk1.Enabled = False
                    End If
            End Select
        Catch ex As Exception
        End Try
    End Sub

    Protected Sub ddlPaymntMthd_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlPaymntMthd As DropDownList = CType(sender, DropDownList)
        Dim dgitem As DataGridItem = CType(ddlPaymntMthd.Parent.Parent, DataGridItem)
        Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
        Dim lblCreditCardAmt As Label = CType(dgitem.FindControl("lblCreditCardAmt"), Label)
        Dim lblCheckAmt As Label = CType(dgitem.FindControl("lblCheckAmt"), Label)
        Dim lblInKindAmt As Label = CType(dgitem.FindControl("lblInKindAmt"), Label)
        If ddlPaymntMthd.SelectedValue = "1" Then
            lblCreditCardAmt.Text = lblAmount.Text
            lblCreditCardAmt.Visible = True
            lblCheckAmt.Visible = False
            lblInKindAmt.Visible = False
        ElseIf ddlPaymntMthd.SelectedValue = "2" Then
            lblCreditCardAmt.Visible = False
            lblCheckAmt.Visible = True
            lblInKindAmt.Visible = False
            lblCheckAmt.Text = lblAmount.Text
        ElseIf ddlPaymntMthd.SelectedValue = "3" Then
            lblInKindAmt.Text = lblAmount.Text
            lblCreditCardAmt.Visible = False
            lblCheckAmt.Visible = False
            lblInKindAmt.Visible = True
        End If
        lblTotal.Text = "Total  :  $" & CalcAmount()
    End Sub

    Protected Sub btnCancelTotal_Click(sender As Object, e As System.EventArgs)
        Dim unPaidBee As Integer = GetUnPaidRegistration()
        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('hide'," & unPaidBee & ");", True)
        Dim chk As CheckBox, lblProductId As Label, item As DataGridItem
        For Each item In dgCatalog.Items
            If item.ItemIndex = 5 Then Continue For
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                lblProductId = CType(item.FindControl("lblProductId"), Label)
                If lblProductId.Text = "105" Then
                    Dim txtQuantity As TextBox = CType(item.FindControl("txtQuantity"), TextBox)
                    Dim txtAmount As TextBox = CType(item.FindControl("txtAmount"), TextBox)
                    txtAmount.Enabled = False
                    txtQuantity.Enabled = False
                    Exit For
                End If
            End If
        Next
    End Sub

    Protected Sub btnContestRegSubmit_Click(sender As Object, e As System.EventArgs)
        Try
            Dim curQty As Integer = 0
            Dim iFundRCalId As Integer = CInt(lblFundRCalID.Text)
            Dim dgitem As DataGridItem, chk As CheckBox, ChildNumber As Integer, ddlgrade As DropDownList
            Dim strsql As String = "", strSelect As String = ""
            For Each dgitem In dgChild.Items
                ChildNumber = CInt(CType(dgitem.FindControl("lblChildNumber"), Label).Text)
                Dim txtFirstName As TextBox = dgitem.FindControl("txtFirstName")
                Dim txtLastName As TextBox = dgitem.FindControl("txtLastName")
                If ChildNumber = 0 Then
                    If txtFirstName.Text <> "" Then
                        Dim strCmd As String = " Select count(*) from Child Where FIRST_NAME='" & txtFirstName.Text & "' and MIDDLE_INITIAL ='' and LAST_NAME='" & txtLastName.Text & "' "
                        Dim iCnt As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strCmd)
                        If iCnt <> 0 Then
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('Duplicate Exists');PopUpNoOfConts('show',0);", True)
                            Exit Sub
                        End If
                    End If
                End If
            Next
            For Each dgitem In dgChild.Items
                strsql = " Declare @Y as int; set @y=Year(GetDate()); "
                ChildNumber = CInt(CType(dgitem.FindControl("lblChildNumber"), Label).Text)
                ddlgrade = CType(dgitem.FindControl("ddlGrade"), DropDownList)
                ' insert child information
                If ChildNumber = 0 Then
                    Dim txtFirstName As TextBox = dgitem.FindControl("txtFirstName")
                    Dim txtLastName As TextBox = dgitem.FindControl("txtLastName")
                    If txtFirstName.Text <> "" Then
                        Dim txtDOB As TextBox = dgitem.FindControl("txtDOB")
                        Dim ddlGender As DropDownList = dgitem.FindControl("ddlGender")
                        strSelect = " INSERT INTO Child (MemberId,First_Name,MIDDLE_INITIAL,Last_Name,Gender,DATE_OF_BIRTH,Grade,CreateDate) VALUES(" & hfMemberId.Value & ",'" & txtFirstName.Text & "',"
                        strSelect = strSelect & "'','" & txtLastName.Text & "','" & ddlGender.SelectedItem.Text & "','" & txtDOB.Text & "'," & ddlgrade.SelectedItem.Text & ",GetDate()) SELECT @@IDENTITY"
                        ChildNumber = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSelect)
                    Else
                        Continue For
                    End If
                End If
                'SB
                chk = CType(dgitem.FindControl("chkSpelling"), CheckBox)
                If chk.Checked = True And chk.Enabled = True Then
                    curQty = curQty + 1
                    strSelect = "select * from FundRContestReg where FundRCalId= " & iFundRCalId & " and Memberid=" & Session("CustIndId") & " and Childnumber= " & ChildNumber & " and ProductGroupCode='SB'"
                    strsql = strsql & " IF (NOT EXISTS(" & strSelect & ")) BEGIN"
                    strsql = strsql & " INSERT INTO FundRContestReg (EventYear,EventID,FundRCalID,EventDate,ChapterID,MemberId,ChildNumber,Grade,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Fee,CreatedDate,CreateBy)"
                    strsql = strsql & " select Fc.EventYear,Fc.EventId, FC.FundRCalId, FC.EventDate,FC.ChapterId, " & Session("CustIndId") & "," & ChildNumber & "," & ddlgrade.SelectedItem.Value
                    strsql = strsql & " ,P.ProductGroupId,'SB',P.ProductId,P.ProductCode,10,Cast(GetDate() as date)," & Session("LoginID") & " From FundRaisingCal FC , ContestCategory CC inner join Product P on CC.ProductGroupcode= P.ProductGroupcode and P.ProductCode=CC.ContestCode and P.EventId=2 Where FundRCalId=" & iFundRCalId
                    strsql = strsql & " and CC.ContestYear=@Y and CC.ProductGroupCode='SB' and " & ddlgrade.SelectedItem.Value & " between GradeFrom and GradeTo ;"
                    strsql = strsql & " END; "
                    '  strsql = strsql & " UPDATE FundRContestReg SET"
                End If
                'MB
                chk = CType(dgitem.FindControl("chkMath"), CheckBox)
                If chk.Checked = True And chk.Enabled = True Then
                    curQty = curQty + 1
                    strSelect = "select * from FundRContestReg where FundRCalId= " & iFundRCalId & " and Memberid=" & Session("CustIndId") & " and Childnumber= " & ChildNumber & " and ProductGroupCode='MB'"
                    strsql = strsql & " IF (NOT EXISTS(" & strSelect & ")) BEGIN"
                    strsql = strsql & " INSERT INTO FundRContestReg (EventYear,EventID,FundRCalID,EventDate,ChapterID,MemberId,ChildNumber,Grade,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Fee,CreatedDate,CreateBy)"
                    strsql = strsql & " select Fc.EventYear,Fc.EventId, FC.FundRCalId, FC.EventDate,FC.ChapterId, " & Session("CustIndId") & "," & ChildNumber & "," & ddlgrade.SelectedItem.Value
                    strsql = strsql & " ,P.ProductGroupId,'MB',P.ProductId,P.ProductCode,10,Cast(GetDate() as date)," & Session("LoginID") & " From FundRaisingCal FC , ContestCategory CC inner join Product P on CC.ProductGroupcode= P.ProductGroupcode and P.ProductCode=CC.ContestCode and P.EventId=2 Where FundRCalId=" & iFundRCalId
                    strsql = strsql & " and CC.ContestYear=@Y and CC.ProductGroupCode='MB' and " & ddlgrade.SelectedItem.Value & " between GradeFrom and GradeTo ;"
                    strsql = strsql & " END; "
                End If
                'SC
                chk = CType(dgitem.FindControl("chkScience"), CheckBox)
                If chk.Checked = True And chk.Enabled = True Then
                    curQty = curQty + 1
                    strSelect = "select * from FundRContestReg where FundRCalId= " & iFundRCalId & " and Memberid=" & Session("CustIndId") & " and Childnumber= " & ChildNumber & " and ProductGroupCode='SC'"
                    strsql = strsql & " IF (NOT EXISTS(" & strSelect & ")) BEGIN"
                    strsql = strsql & " INSERT INTO FundRContestReg (EventYear,EventID,FundRCalID,EventDate,ChapterID,MemberId,ChildNumber,Grade,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Fee,CreatedDate,CreateBy)"
                    strsql = strsql & " select Fc.EventYear,Fc.EventId, FC.FundRCalId, FC.EventDate,FC.ChapterId, " & Session("CustIndId") & "," & ChildNumber & "," & ddlgrade.SelectedItem.Value
                    strsql = strsql & " ,P.ProductGroupId,'SC',P.ProductId,P.ProductCode,10,Cast(GetDate() as date)," & Session("LoginID") & " From FundRaisingCal FC , ContestCategory CC inner join Product P on CC.ProductGroupcode= P.ProductGroupcode and P.ProductCode=CC.ContestCode and P.EventId=2 Where FundRCalId=" & iFundRCalId
                    strsql = strsql & " and CC.ContestYear=@Y and CC.ProductGroupCode='SC' and " & ddlgrade.SelectedItem.Value & " between GradeFrom and GradeTo ;"
                    strsql = strsql & " END; "
                End If
                'GB
                chk = CType(dgitem.FindControl("chkGeography"), CheckBox)
                If chk.Checked = True And chk.Enabled = True Then
                    curQty = curQty + 1
                    strSelect = "select * from FundRContestReg where FundRCalId= " & iFundRCalId & " and Memberid=" & Session("CustIndId") & " and Childnumber= " & ChildNumber & " and ProductGroupCode='GB'"
                    strsql = strsql & " IF (NOT EXISTS(" & strSelect & ")) BEGIN"
                    strsql = strsql & " INSERT INTO FundRContestReg (EventYear,EventID,FundRCalID,EventDate,ChapterID,MemberId,ChildNumber,Grade,ProductGroupID,ProductGroupCode,ProductID,ProductCode,Fee,CreatedDate,CreateBy)"
                    strsql = strsql & " select Fc.EventYear,Fc.EventId, FC.FundRCalId, FC.EventDate,FC.ChapterId, " & Session("CustIndId") & "," & ChildNumber & "," & ddlgrade.SelectedItem.Value
                    strsql = strsql & " ,P.ProductGroupId,'GB',P.ProductId,P.ProductCode,10,Cast(GetDate() as date)," & Session("LoginID") & " From FundRaisingCal FC , ContestCategory CC inner join Product P on CC.ProductGroupcode= P.ProductGroupcode and P.ProductCode=CC.ContestCode and P.EventId=2 Where FundRCalId=" & iFundRCalId
                    strsql = strsql & " and CC.ContestYear=@Y and CC.ProductGroupCode='GB' and " & ddlgrade.SelectedItem.Value & " between GradeFrom and GradeTo ;"
                    strsql = strsql & " END; "
                End If
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strsql)
            Next
            curQty = GetUnPaidRegistration()
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('hide'," & curQty & ");", True)
            dgitem = dgCatalog.Items(CInt(Session("ContestRow")))
            Dim txtQuantity As TextBox = CType(dgitem.FindControl("txtQuantity"), TextBox)
            Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
            Dim ddlPaymntMthd As DropDownList, lblStatus As Label, txtAmount As TextBox
            lblStatus = CType(dgitem.FindControl("lblStatus"), Label)
            txtAmount = CType(dgitem.FindControl("txtAmount"), TextBox)
            ddlPaymntMthd = CType(dgitem.FindControl("ddlPaymntMthd"), DropDownList)
            strsql = " Update FR SET FR.ChapterId=FF.ChapterId, FR.ChapterCode=FF.ChapterCode, FR.EventId=FF.EventId, FR.EventCode=FF.EventCode, FR.EventDate=FF.EventDate, FR.MemberId = " & Session("CustIndID") & ", FR.DonorType='" & Session("FundDonorType") & "', FR.ProductGroupId=FF.ProductGroupId, FR.ProductGroupCode=FF.ProductGroupCode, FR.ProductId=FF.ProductId, FR.ProductCode=FF.ProductCode, FR.EventYear=FF.EventYear, FR.Quantity=" & curQty & ", FR.UnitFee=" & txtAmount.Text & ", FR.TotalAmt=" & lblAmount.Text & ", FR.TaxDeduction=FF.TaxDeduction, FR.PaymentMode='" & ddlPaymntMthd.SelectedItem.Text & "',FR.FundRFeesId=FF.FundRFeesID, FR.FundRCalID=FF.FundRCalID,  FR.ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID")
            strsql = strsql & " from FundRReg FR, FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & " AND FR.FundRRegID= " & lblStatus.Text
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strsql)
            Catch ex As Exception
            End Try
            If Not Session("ContestRow") Is Nothing Then
                Session("ContestRow") = Nothing
            End If
            loadgrid(lblFundRCalID.Text)
        Catch ex As Exception
        End Try
    End Sub

    Function GetUnPaidRegistration() As Integer
        Dim iQty As Integer = 0
        Dim strSql As String = "Select * from FundRContestReg Where FundRCalId=" & lblFundRCalID.Text & " AND MemberId=" & Session("CustIndID")
        Dim dsFundRContest As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        Dim dr() As DataRow = dsFundRContest.Tables(0).Select("paymentreference is null")
        If dr.Length > 0 Then
            iQty = dr.Length
        End If
        If iQty = 0 Then
            Dim dg As DataGridItem, chk As CheckBox
            If Not Session("ContestRow") Is Nothing Then
                dg = dgCatalog.Items(CInt(Session("ContestRow")))
                chk = dgCatalog.Items(dg.ItemIndex).FindControl("chkSelect")
                chk.Checked = False
                Dim txtQty As TextBox = dgCatalog.Items(dg.ItemIndex).FindControl("txtQuantity")
                txtQty.Enabled = False
            End If
        End If
        Return iQty
    End Function
    Protected Sub chkSelectProduct_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim chk As CheckBox = CType(sender, CheckBox)
        err.Text = ""
        Dim dgitem As DataGridItem = CType(chk.Parent.Parent, DataGridItem)
        Dim txtFirstName As TextBox = CType(dgitem.FindControl("txtFirstName"), TextBox)
        If txtFirstName.Visible = True And txtFirstName.Text = "" And chk.Checked = True Then
            err.Text = "Please enter child details"
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('show',0);", True)
            Exit Sub
        End If
        Dim chkSpelling As CheckBox = CType(dgitem.FindControl("chkSpelling"), CheckBox)
        Dim chkMath As CheckBox = CType(dgitem.FindControl("chkMath"), CheckBox)
        Dim chkScience As CheckBox = CType(dgitem.FindControl("chkScience"), CheckBox)
        Dim chkGeography As CheckBox = CType(dgitem.FindControl("chkGeography"), CheckBox)
        Dim lblTotal As Label = CType(dgitem.FindControl("lblTotal"), Label)
        Dim total As Integer = 0
        If chkSpelling.Checked Then total = total + 1
        If chkMath.Checked Then total = total + 1
        If chkScience.Checked Then total = total + 1
        If chkGeography.Checked Then total = total + 1
        lblTotal.Text = total
        total = 0
        For Each dgitem In dgChild.Items
            If CType(dgitem.FindControl("lblTotal"), Label).Text.Trim <> "" Then
                total = total + CInt(CType(dgitem.FindControl("lblTotal"), Label).Text)
            End If
        Next
        dgChild.Columns(dgChild.Columns.Count - 1).FooterText = total
        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('show',0);", True)
    End Sub

    Private Sub GridCaption()
        If dgCatalog.Items.Count > 0 Then
            Dim colCnt As Integer = dgCatalog.Columns.Count
            dgCatalog.Items(5).Cells.Clear()
            dgCatalog.Items(5).Cells.Add(New TableCell())
            dgCatalog.Items(5).Cells(0).ColumnSpan = colCnt
            dgCatalog.Items(5).Cells(0).Text = "Donation Categories"
            dgCatalog.Items(5).Cells(0).Font.Bold = True
            dgCatalog.Items(5).Cells(0).HorizontalAlign = HorizontalAlign.Left
        End If
    End Sub

    Protected Sub chkSelect_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim chk As CheckBox = CType(sender, CheckBox)
            Dim dgitem As DataGridItem = CType(chk.Parent.Parent, DataGridItem)
            If dgitem.ItemIndex = 5 Then
                Exit Sub
            End If
            Dim txtQuantity As TextBox = CType(dgitem.FindControl("txtQuantity"), TextBox)
            Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
            Dim ddlPaymntMthd As DropDownList, lblPaymentMethod As Label, lblStatus As Label, txtAmount As TextBox, lblFeeFrom As Label, lblFeeTo As Label, lblFeeEnabled As Label, lblQuantityEnabled As Label
            lblStatus = CType(dgitem.FindControl("lblStatus"), Label)
            txtAmount = CType(dgitem.FindControl("txtAmount"), TextBox)
            lblFeeFrom = CType(dgitem.FindControl("lblFeeFrom"), Label)
            lblFeeTo = CType(dgitem.FindControl("lblFeeTo"), Label)
            lblFeeEnabled = CType(dgitem.FindControl("lblFeeEnabled"), Label)
            lblQuantityEnabled = CType(dgitem.FindControl("lblQuantityEnabled"), Label)
            lblAmount = CType(dgitem.FindControl("lblAmount"), Label)
            ddlPaymntMthd = CType(dgitem.FindControl("ddlPaymntMthd"), DropDownList)
            lblPaymentMethod = CType(dgitem.FindControl("lblPaymentMethod"), Label)

            If chk.Checked = True Then
                Dim lblProductGroupCode As Label
                lblProductGroupCode = CType(dgitem.FindControl("lblProductGroupCode"), Label)
                Dim lblProductID As Label, lblCreditCardAmt As Label
                lblProductID = CType(dgitem.FindControl("lblProductID"), Label)
                If lblProductID.Text = "73" And ViewState("IsDinnerPaid").ToString.ToLower = "true" Then
                    Exit Sub
                End If
                lblCreditCardAmt = CType(dgitem.FindControl("lblCreditCardAmt"), Label)
                lblFundRFeesID.Text = CType(dgitem.FindControl("lblFundRFeesID"), Label).Text
                If lblProductGroupCode.Text.Trim = "USSchol" Then
                    chk.Checked = False
                    ddlProductPaymentmthd.Items.Clear()
                    If lblPaymentMethod.Text.Trim = "CreditCard only" Then
                        ddlProductPaymentmthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlProductPaymentmthd.Enabled = False
                    ElseIf lblPaymentMethod.Text.Trim = "CreditCard or Check only" Then
                        ddlProductPaymentmthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlProductPaymentmthd.Items.Insert(1, New ListItem("Check", "2"))
                        'ddlPaymntMthd.Enabled = True
                        ddlPaymntMthd.SelectedIndex = 0
                    ElseIf lblPaymentMethod.Text.Trim = "CreditCard, Check or In Kind" Then
                        ddlProductPaymentmthd.Items.Insert(0, New ListItem("Credit Card", "1"))
                        ddlProductPaymentmthd.Items.Insert(1, New ListItem("Check", "2"))
                        ddlProductPaymentmthd.Items.Insert(2, New ListItem("In Kind", "3"))
                        ddlProductPaymentmthd.SelectedIndex = 0
                    End If
                    loadproduct(lblProductID.Text)
                    div1.Visible = False
                    div2.Visible = True
                    Exit Sub
                End If
                If lblProductID.Text = "105" Then 'Bee
                    lblAmount.Text = (CInt(txtQuantity.Text) * Decimal.Parse(txtAmount.Text.Replace("$", "")))
                    Session("ContestRow") = dgitem.ItemIndex.ToString
                    ' Add bee in fundrReg
                    Dim StrSQL As String
                    If lblStatus.Text = "0" Then
                        StrSQL = "INSERT INTO FundRReg(ChapterId, ChapterCode, EventId, EventCode, EventDate, MemberId, DonorType, ProductGroupId, ProductGroupCode, ProductId, ProductCode, EventYear, Quantity, UnitFee, TotalAmt, TaxDeduction, PaymentMode,FundRFeesId, FundRCalID, CreateDate, CreatedBy) "
                        StrSQL = StrSQL & " select FF.ChapterId, FF.ChapterCode, FF.EventId, FF.EventCode, FF.EventDate," & Session("CustIndID") & ",'" & Session("FundDonorType") & "', FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode, FF.EventYear," & txtQuantity.Text & "," & txtAmount.Text & "," & lblAmount.Text & ", FF.TaxDeduction,'" & ddlPaymntMthd.SelectedItem.Text & "',FF.FundRFeesId, FF.FundRCalID,Getdate()," & Session("LoginID")
                        StrSQL = StrSQL & " from FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & " ; select SCOPE_IDENTITY() "
                        '  lblerr.Text = "Inserted Successfully"
                    Else
                        StrSQL = " Update FR SET FR.ChapterId=FF.ChapterId, FR.ChapterCode=FF.ChapterCode, FR.EventId=FF.EventId, FR.EventCode=FF.EventCode, FR.EventDate=FF.EventDate, FR.MemberId = " & Session("CustIndID") & ", FR.DonorType='" & Session("FundDonorType") & "', FR.ProductGroupId=FF.ProductGroupId, FR.ProductGroupCode=FF.ProductGroupCode, FR.ProductId=FF.ProductId, FR.ProductCode=FF.ProductCode, FR.EventYear=FF.EventYear, FR.Quantity=" & txtQuantity.Text & ", FR.UnitFee=" & txtAmount.Text & ", FR.TotalAmt=" & lblAmount.Text & ", FR.TaxDeduction=FF.TaxDeduction, FR.PaymentMode='" & ddlPaymntMthd.SelectedItem.Text & "',FR.FundRFeesId=FF.FundRFeesID, FR.FundRCalID=FF.FundRCalID,  FR.ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID")
                        StrSQL = StrSQL & " from FundRReg FR, FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & " AND FR.FundRRegID= " & lblStatus.Text & " ; select " & lblStatus.Text
                    End If
                    Try
                        lblStatus.Text = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)
                    Catch ex As Exception
                    End Try
                    LoadChild()

                    Dim iMemberID As Double
                    If Request.QueryString("Id") Is Nothing Then
                        iMemberID = Session("CustIndID")
                    Else
                        iMemberID = Request.QueryString("Id")
                    End If
                    hfMemberId.Value = iMemberID
                    txtHFBeeAmount.Text = lblCreditCardAmt.ClientID.ToString()
                    txtHFBeeQty.Text = txtQuantity.ClientID.ToString()
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('show',0);", True)
                    txtQuantity.Text = 0
                Else
                    txtQuantity.Text = 1
                End If
                'txtQuantity.Text = 1
                txtQuantity.Enabled = lblQuantityEnabled.Text
                If lblProductID.Text = "134" Or lblProductID.Text = "135" Or lblProductID.Text = "136" Then
                    Dim ddlQty As DropDownList = dgitem.FindControl("ddlQty")
                    txtQuantity.Visible = False
                    ddlQty.Enabled = True
                End If
                If lblProductID.Text = "105" Or lblProductID.Text = "134" Or lblProductID.Text = "135" Or lblProductID.Text = "136" Or lblProductID.Text = "73" Then
                    txtAmount.Enabled = False
                Else
                    txtAmount.Enabled = lblFeeEnabled.Text
                End If
                If txtAmount.Text.Length > 0 Then lblAmount.Text = Decimal.Parse(txtAmount.Text) * (CInt(txtQuantity.Text))
                'To handle product id =134 and 135
                If lblProductID.Text = "134" Or lblProductID.Text = "135" Then
                    Dim iQty As Integer = CInt(txtQuantity.Text) - 2
                    If iQty > 0 Then
                        lblAmount.Text = Decimal.Parse(txtAmount.Text) * (CInt(txtQuantity.Text))
                    Else
                        lblAmount.Text = "0.00"
                    End If
                End If
                lblTotal.Text = "Total  :  $" & CalcAmount()
                If ddlPaymntMthd.Items.Count > 1 Then
                    ddlPaymntMthd.Enabled = True
                End If
                If ddlPaymntMthd.SelectedValue = "1" Then
                    lblCreditCardAmt = CType(dgitem.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = Val(lblAmount.Text)
                    lblCreditCardAmt.Visible = True
                ElseIf ddlPaymntMthd.SelectedValue = "2" Then
                    Dim lblCheckAmt As Label = CType(dgitem.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Visible = True
                    lblCheckAmt.Text = Val(lblAmount.Text)
                ElseIf ddlPaymntMthd.SelectedValue = "3" Then
                    Dim lblInKindAmt As Label = CType(dgitem.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Text = Val(lblAmount.Text)
                    lblInKindAmt.Visible = True
                End If
            Else
                Dim lblProductID As Label
                lblProductID = CType(dgitem.FindControl("lblProductID"), Label)
                If lblProductID.Text = "105" Then
                    Dim strSql As String
                    strSql = " DELETE FROM FundRReg WHERE MemberId = " & Session("CustIndID") & " and ProductId=105 and EventYear= Year(GetDate()) and EventId=9 and FundRCalId= " & lblFundRCalID.Text & " and PaymentReference is null"
                    strSql = strSql & "; Delete from FundRContestReg where MemberId = " & Session("CustIndID") & " and EventYear= Year(GetDate()) and EventId=9 and FundRCalId= " & lblFundRCalID.Text & " and PaymentReference is null"
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                    lblStatus.Text = "0"
                End If
                txtQuantity.Text = 0
                lblAmount.Text = String.Empty
                txtQuantity.Enabled = False
                txtAmount.Enabled = False
                ddlPaymntMthd.Enabled = False
                If lblProductID.Text = "134" Or lblProductID.Text = "135" Or lblProductID.Text = "136" Then
                    Dim ddlQty As DropDownList = dgitem.FindControl("ddlQty")
                    ddlQty.SelectedIndex = 0
                    txtQuantity.Visible = False
                    ddlQty.Visible = True
                    ddlQty.Enabled = False
                End If
                lblTotal.Text = "Total  :  $" & CalcAmount()
            End If
        Catch ex As Exception
        End Try
    End Sub

    Function CalcAmount() As Decimal
        If checkquantity() Then
            Exit Function
        End If
      

        Dim totamount As Decimal = 0.0
        Dim chkAmt As Decimal = 0.0
        Dim CreditAmt As Decimal = 0.0
        Dim InkindAmt As Decimal = 0.0
        Dim item As DataGridItem
        For Each item In dgCatalog.Items
            If item.ItemIndex = 5 Then Continue For

            Dim lblAmount As Label = CType(item.FindControl("lblAmount"), Label)
            Dim lblDGCreditCardAmt As Label = CType(item.FindControl("lblCreditCardAmt"), Label)
            Dim lblDGCheckAmt As Label = CType(item.FindControl("lblCheckAmt"), Label)
            Dim lblDGInKindAmt As Label = CType(item.FindControl("lblInKindAmt"), Label)
            Dim ddlPaymntMthd As DropDownList = CType(item.FindControl("ddlPaymntMthd"), DropDownList)

            Dim lblProductID As Label
            lblProductID = CType(item.FindControl("lblProductID"), Label)

            If lblProductID.Text = "73" And ViewState("IsDinnerPaid").ToString.ToLower = "true" Then

                Continue For
            End If
            If ddlPaymntMthd.SelectedValue = "1" Then

                lblDGCreditCardAmt.Text = lblAmount.Text
                lblDGCreditCardAmt.Visible = True
                lblDGCheckAmt.Visible = False
                lblDGInKindAmt.Visible = False
                If lblAmount.Text.Length > 0 Then
                    CreditAmt = CreditAmt + Decimal.Parse(lblAmount.Text.Replace("$", ""))
                End If
            ElseIf ddlPaymntMthd.SelectedValue = "2" Then
                lblDGCreditCardAmt.Visible = False
                lblDGCheckAmt.Visible = True
                lblDGInKindAmt.Visible = False
                lblDGCheckAmt.Text = lblAmount.Text
                If lblAmount.Text.Length > 0 Then
                    chkAmt = chkAmt + Decimal.Parse(lblAmount.Text.Replace("$", ""))
                End If
            ElseIf ddlPaymntMthd.SelectedValue = "3" Then
                lblDGInKindAmt.Text = lblAmount.Text
                lblDGCreditCardAmt.Visible = False
                lblDGCheckAmt.Visible = False
                lblDGInKindAmt.Visible = True
                If lblAmount.Text.Length > 0 Then
                    InkindAmt = InkindAmt + Decimal.Parse(lblAmount.Text.Replace("$", ""))
                End If
            End If
            
            lblCheck.Text = String.Format("{0:f2}", chkAmt)
            lblCredit.Text = String.Format("{0:f2}", CreditAmt)
            lblInKind.Text = String.Format("{0:f2}", InkindAmt)
            If lblAmount.Text.Length > 0 Then
                totamount = totamount + Decimal.Parse(lblAmount.Text.Replace("$", ""))
            End If
        Next
        Return String.Format("{0:f2}", totamount)
    End Function

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim strSql As String
        strSql = " DELETE FROM FundRReg WHERE MemberId = " & Session("CustIndID") & " and EventYear= Year(GetDate()) and EventId=9 and FundRCalId= " & lblFundRCalID.Text & " and PaymentReference is null"
        strSql = strSql & "; Delete from FundRContestReg where MemberId = " & Session("CustIndID") & " and EventYear= Year(GetDate()) and EventId=9 and FundRCalId= " & lblFundRCalID.Text & " and PaymentReference is null"
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
        loadgrid(lblFundRCalID.Text)
    End Sub

    Private Sub clear()

    End Sub

    Function checkquantity() As Boolean

        lblerr.Text = ""
        Dim flag As Boolean = False
        Dim item As DataGridItem
        Dim chk As CheckBox, txtQuantity As TextBox, txtAmount As TextBox
        For Each item In dgCatalog.Items
            If item.ItemIndex = 5 Then Continue For
            Dim lblFeeFrom As Label = CType(item.FindControl("lblFeeFrom"), Label)
            Dim lblFeeEnabled As Label = CType(item.FindControl("lblFeeEnabled"), Label)
            Dim lblFeeTo As Label = CType(item.FindControl("lblFeeTo"), Label)
            Dim lblShare As Label = CType(item.FindControl("lblShare"), Label)
            Dim lblProductName As Label = CType(item.FindControl("lblProductName"), Label)
            Dim lblProductID As Label = CType(item.FindControl("lblProductID"), Label)

            chk = CType(item.FindControl("chkSelect"), CheckBox)
            txtQuantity = CType(item.FindControl("txtQuantity"), TextBox)
            txtAmount = CType(item.FindControl("txtAmount"), TextBox)
            If chk.Checked = True Then
                If IsNumeric(txtQuantity.Text) = False Then
                    lblerr.Text = "Please enter non negative numeric data in Quantity field for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtQuantity.Text) < 0 Then
                    lblerr.Text = "Please enter non negative numeric data in Quantity field for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf IsNumeric(txtAmount.Text) = False Then
                    lblerr.Text = "Please enter non negative numeric data in Amount field for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf Decimal.Parse(txtAmount.Text) < 0 Then
                    lblerr.Text = "Please enter non negative numeric data in Amount  field for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf lblShare.Text.Trim = "Yes" And lblFeeEnabled.Text.Trim = "True" And Decimal.Parse(txtAmount.Text) < 50 Then
                    lblerr.Text = "Minimum Amount must be $50.00 for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf lblFeeEnabled.Text.Trim = "True" And lblShare.Text.Trim = "No" And Decimal.Parse(txtAmount.Text) < Decimal.Parse(lblFeeFrom.Text) Then
                    lblerr.Text = "The Amount must be between the fee specified for " & lblProductName.Text & "."
                    Return True
                    Exit Function
                ElseIf lblFeeEnabled.Text.Trim = "True" And lblShare.Text.Trim = "No" And lblFeeTo.Text.Length > 0 And Val(lblFeeTo.Text) > 0 Then
                    If Decimal.Parse(txtAmount.Text) > Decimal.Parse(lblFeeTo.Text) Then
                        lblerr.Text = "The Amount must be less than " & lblFeeTo.Text & " specified for " & lblProductName.Text & "."
                        Return True
                        Exit Function
                    End If
                End If
            End If
        Next
        Return flag
    End Function

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim flag As Boolean = False
        Dim totamount As Decimal
        If checkquantity() Then
            Exit Sub
        Else
            Dim IsDinnerSel, IsBeeSel As Boolean
            IsDinnerSel = False
            IsBeeSel = False
            Dim chk As CheckBox, StrSQL As String, lblStatus As Label
            Dim lblProductGroupCode As String, lblProductCode As String
            Dim txtQuantity As TextBox, txtAmount As TextBox, lblFundRFeesID As Label, lblamount As Label, ddlPaymntMthd As DropDownList
            Dim ddlQty As DropDownList, item As DataGridItem, lblProductID As Label
            Dim CheckIsSelected As Boolean = False, ShowConfMissedToSelBee As Boolean = False
            Dim NoOfAdult, NoOfChildren, NoOfExtraTicket As Integer
            NoOfAdult = 0
            NoOfChildren = 0
            NoOfExtraTicket = 0
            For Each item In dgCatalog.Items
                If item.ItemIndex = 5 Then Continue For
                Dim lblFeeFrom As Label = CType(item.FindControl("lblFeeFrom"), Label)
                Dim lblFeeEnabled As Label = CType(item.FindControl("lblFeeEnabled"), Label)
                Dim lblFeeTo As Label = CType(item.FindControl("lblFeeTo"), Label)
                Dim lblShare As Label = CType(item.FindControl("lblShare"), Label)
                Dim lblProductName As Label = CType(item.FindControl("lblProductName"), Label)
                lblProductID = CType(item.FindControl("lblProductID"), Label)
                chk = CType(item.FindControl("chkSelect"), CheckBox)
                txtQuantity = CType(item.FindControl("txtQuantity"), TextBox)
                txtAmount = CType(item.FindControl("txtAmount"), TextBox)
                If lblProductID.Text = "73" And ViewState("IsDinnerPaid").ToString.ToLower = "true" Then
                    Continue For
                End If
                ' Comment on Oct06-2015 to avoid validation with contest registration ie Parents can't click and register for contests anymore.
                'If lblProductID.Text.Trim = "105" And chk.Checked = True And txtQuantity.Text = "0" Then
                '    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('Quantity cannot be zero or blank once Contests is checked.');", True)
                '    Exit Sub
                'End If
                ' validate if Adult or Childrens quantity is not selected as "Select"
                If (lblProductID.Text.Trim = "134" Or lblProductID.Text = "135") Then
                    If chk.Checked = False And lblProductID.Text = "134" Then
                        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('Please select the number of " & lblProductName.Text & "');", True)
                        Exit Sub
                    ElseIf chk.Checked = True Then
                        ddlQty = item.FindControl("ddlQty")
                        If ddlQty.SelectedItem.Value = "-1" Then
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('Please select the number of " & lblProductName.Text & "');", True)
                            Exit Sub
                        End If
                        If lblProductID.Text = "134" Then
                            NoOfAdult = ddlQty.SelectedItem.Value
                        ElseIf lblProductID.Text = "135" Then
                            NoOfChildren = ddlQty.SelectedItem.Value
                        End If
                    End If
                End If
                If chk.Checked = True Then
                    CheckIsSelected = True
                End If
                ' validate if bee is selected
                'Commented on Oct06_2015 to stop contest registration
                'If lblProductID.Text = "105" And chk.Checked = False And hfMissedToSelBee.Value.ToLower = "true" Then
                '    ShowConfMissedToSelBee = True
                'End If
                'Validate Extra ticket 
                If chk.Checked = True Then
                    If lblProductID.Text = "136" Then
                        ddlQty = CType(item.FindControl("ddlQty"), DropDownList)
                        If ddlQty.SelectedItem.Value = "-1" Then
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('Please Select the quantity of " & lblProductName.Text & "');", True)
                            Exit Sub
                        End If
                        NoOfExtraTicket = ddlQty.SelectedItem.Value
                    End If
                End If
            Next

            If CheckIsSelected = False Then ' validate if the user didn't select any boxes
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('No selections are made.');", True)
                Exit Sub
            ElseIf CheckIsSelected = True And ShowConfMissedToSelBee = True Then ' validate if the user missed to select bee box
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ConfMissedToSelBee();", True)
                Exit Sub
            End If
            ' Show popup to Continue/Modify
            If hfTicketContinue.Value <> "True" Then
                StrSQL = " declare @Adult as integer,@Children as integer, @ExtraTicket as integer,@y as integer"
                StrSQL = StrSQL & " set @y= year(GETDATE()) "
                StrSQL = StrSQL & " select @Adult=isnull(sum(quantity),0) from fundrreg FR INNER JOIN FUNDRFEES FE on FE.ProductId= FR.ProductId and FE.EventId=FR.EventId and FE.EventYear=FR.EventYear and FE.FundRCalID =FR.FundRCalID "
                StrSQL = StrSQL & " where FR.Eventid=9 and FR.EventYear=@y and FR.ProductCode= 'Adult' and FR.FundRCalid=" & lblFundRCalID.Text & " and FR.MemberId=" & Session("CustIndID") & " and paymentreference is not null"
                StrSQL = StrSQL & " select @Children=isnull(sum(quantity),0) from fundrreg FR INNER JOIN FUNDRFEES FE on FE.ProductId= FR.ProductId and FE.EventId=FR.EventId and FE.EventYear=FR.EventYear and FE.FundRCalID =FR.FundRCalID "
                StrSQL = StrSQL & " where FR.Eventid=9 and FR.EventYear=@y and FR.ProductCode= 'Child' and FR.FundRCalid=" & lblFundRCalID.Text & " and FR.MemberId=" & Session("CustIndID") & " and paymentreference is not null"
                StrSQL = StrSQL & " select @ExtraTicket=isnull(sum(quantity),0) from fundrreg FR INNER JOIN FUNDRFEES FE on FE.ProductId= FR.ProductId and FE.EventId=FR.EventId and FE.EventYear=FR.EventYear and FE.FundRCalID =FR.FundRCalID "
                StrSQL = StrSQL & " where FR.Eventid=9 and FR.EventYear=@y and FR.ProductCode= 'Dinner2' and FR.FundRCalid=" & lblFundRCalID.Text & " and FR.MemberId=" & Session("CustIndID") & " and paymentreference is not null"
                StrSQL = StrSQL & " select @Adult,@Children, @ExtraTicket "
                Dim dsTicket As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                Dim TotAdult, TotChildren, TotTicket As Integer
                TotAdult = NoOfAdult + CInt(dsTicket.Tables(0).Rows(0).Item(0))
                TotChildren = NoOfChildren + CInt(dsTicket.Tables(0).Rows(0).Item(1))
                TotTicket = NoOfExtraTicket + CInt(dsTicket.Tables(0).Rows(0).Item(2))

                If (TotAdult + TotChildren) > (4 + TotTicket) Then
                    ' Total attendees exceeded the number of dinner tickets purchased.  Please buy extra  dinner tickets.
                    btnOkContinue.Visible = False
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpTicket('show','Total attendees exceeded the number of dinner tickets purchased. Please buy extra dinner tickets.');", True)
                    Exit Sub
                End If
                If TotTicket > 0 And (TotAdult + TotChildren) < (4 + TotTicket) Then
                    btnOkContinue.Visible = True
                    ' While you bought extra dinner tickets, Total Number of Adults and Children attending appears to be less than number of tickets.
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpTicket('show','While you bought extra dinner tickets, Total Number of Adults and Children attending appears to be less than number of tickets.');", True)
                    Exit Sub
                End If
            End If
            lblerr.Text = ""
            totamount = 0.0
            Dim discountAmt As Decimal = 0.0
            Session("Donation") = 0
            Session("DONATIONFOR") = "INScholar"
            For Each item In dgCatalog.Items
                If item.ItemIndex = 5 Then Continue For
                chk = CType(item.FindControl("chkSelect"), CheckBox)
                lblStatus = CType(item.FindControl("lblStatus"), Label)
                If chk.Checked = True Then
                    lblProductID = CType(item.FindControl("lblProductId"), Label)

                    If lblProductID.Text = "73" And ViewState("IsDinnerPaid").ToString.ToLower = "true" Then
                        Continue For
                    End If
                    flag = True
                    txtQuantity = CType(item.FindControl("txtQuantity"), TextBox)
                    txtAmount = CType(item.FindControl("txtAmount"), TextBox)
                    lblamount = CType(item.FindControl("lblAmount"), Label)
                    ddlPaymntMthd = CType(item.FindControl("ddlPaymntMthd"), DropDownList)
                    lblFundRFeesID = CType(item.FindControl("lblFundRFeesID"), Label)
                    totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
                    If totamount = 0 Then
                        flag = False
                    End If
                    lblProductGroupCode = CType(item.FindControl("lblProductGroupCode"), Label).Text
                    lblProductCode = CType(item.FindControl("lblProductCode"), Label).Text
                    ' discount = dinner charge +  Bee charge
                    If lblProductID.Text.ToLower.Trim = "73" Or lblProductID.Text.ToLower.Trim = "105" Then
                        discountAmt = discountAmt + Decimal.Parse(lblamount.Text.Replace("$", ""))
                    End If
                    If lblStatus.Text = "0" And Not lblProductGroupCode.Trim() = "USSchol" Then
                        StrSQL = "INSERT INTO FundRReg(ChapterId, ChapterCode, EventId, EventCode, EventDate, MemberId, DonorType, ProductGroupId, ProductGroupCode, ProductId, ProductCode, EventYear, Quantity, UnitFee, TotalAmt, TaxDeduction, PaymentMode,FundRFeesId, FundRCalID, CreateDate, CreatedBy) "
                        StrSQL = StrSQL & " select FF.ChapterId, FF.ChapterCode, FF.EventId, FF.EventCode, FF.EventDate," & Session("CustIndID") & ",'" & Session("FundDonorType") & "', FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode, FF.EventYear," & txtQuantity.Text & "," & txtAmount.Text & "," & lblamount.Text & ", FF.TaxDeduction,'" & ddlPaymntMthd.SelectedItem.Text & "',FF.FundRFeesId, FF.FundRCalID,Getdate()," & Session("LoginID")
                        StrSQL = StrSQL & " from FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text
                        lblerr.Text = "Inserted Successfully"
                    Else
                        StrSQL = " Update FR SET FR.ChapterId=FF.ChapterId, FR.ChapterCode=FF.ChapterCode, FR.EventId=FF.EventId, FR.EventCode=FF.EventCode, FR.EventDate=FF.EventDate, FR.MemberId = " & Session("CustIndID") & ", FR.DonorType='" & Session("FundDonorType") & "', FR.ProductGroupId=FF.ProductGroupId, FR.ProductGroupCode=FF.ProductGroupCode, FR.ProductId=FF.ProductId, FR.ProductCode=FF.ProductCode, FR.EventYear=FF.EventYear, FR.Quantity=" & txtQuantity.Text & ", FR.UnitFee=" & txtAmount.Text & ", FR.TotalAmt=" & lblamount.Text & ", FR.TaxDeduction=FF.TaxDeduction, FR.PaymentMode='" & ddlPaymntMthd.SelectedItem.Text & "',FR.FundRFeesId=FF.FundRFeesID, FR.FundRCalID=FF.FundRCalID,  FR.ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID")
                        StrSQL = StrSQL & " from FundRReg FR, FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & " AND FR.FundRRegID= " & lblStatus.Text
                    End If
                    Try
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                    Catch ex As Exception
                        lblerr.Text = StrSQL
                        lblerr.Text = lblerr.Text & "<br>" & ex.ToString()
                        Exit Sub
                    End Try
                    ' Calculate Discount amount                   
                    lblProductGroupCode = lblProductGroupCode.Trim.ToLower
                    If lblProductGroupCode = "general" Or lblProductGroupCode = "recawd" Then
                        Session("Donation") = Session("Donation") + Decimal.Parse(lblamount.Text.Replace("$", ""))
                    End If

                ElseIf Not lblStatus.Text = "0" Then
                    StrSQL = " DELETE FROM FundRReg WHERE FundRRegID = " & lblStatus.Text
                    StrSQL = StrSQL & "; Delete from BeeRankSponsor where FundRRegID =" & lblStatus.Text
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQL)
                End If
            Next
            ' If the total payment exceeds $500, then waive the charge for dinner and contests for children.  So the discount = dinner charge +  Bee charge
            If totamount >= 500 Then
                Session("Discount") = discountAmt
            Else
                Session("Discount") = Nothing
            End If
        End If
            If flag = True Then
                Session("FundRCalID") = lblFundRCalID.Text
                Response.Redirect("FundRSummary.aspx")
        Else
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:alert('No selections are made to continue payment.');", True)

            loadPaidgrid(lblFundRCalID.Text)
        End If
    End Sub

    Protected Sub txtQuantity_TextChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        ' Dim chk As CheckBox = CType(sender, CheckBox)
        Dim txtTemp As TextBox = CType(sender, TextBox)
        Dim dgitem As DataGridItem = CType(txtTemp.Parent.Parent, DataGridItem)
        Dim txtAmount As TextBox = CType(dgitem.FindControl("txtAmount"), TextBox)
        Dim txtQuantity As TextBox = CType(dgitem.FindControl("txtQuantity"), TextBox)
        Dim lblAmount As Label = CType(dgitem.FindControl("lblAmount"), Label)
        Dim lblProductId As Label = CType(dgitem.FindControl("lblProductId"), Label)
        Dim ddlPaymntMthd As DropDownList = CType(dgitem.FindControl("ddlPaymntMthd"), DropDownList)
        If IsNumeric(txtQuantity.Text) = False Then
            lblerr.Text = "Please enter only numeric data in Quantity field."
        ElseIf Decimal.Parse(txtQuantity.Text) < 0 Then
            lblerr.Text = "Please enter non negative numeric data in Quantity field."
        ElseIf IsNumeric(txtAmount.Text) = False Then
            lblerr.Text = "Please enter only numeric data in Amount field."
        ElseIf Decimal.Parse(txtAmount.Text) < 0 Then
            lblerr.Text = "Please enter non negative numeric data in Amount field."
        Else
            lblerr.Text = ""
            Dim chk As CheckBox = CType(dgitem.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                If txtQuantity.Text.Length > 0 Then
                    If Not CInt(txtQuantity.Text) = 0 Then
                        If lblProductId.Text = "134" Or lblProductId.Text = "135" Then
                            Dim qty As Integer = CInt(txtQuantity.Text) - 2
                            If qty > 0 Then
                                lblAmount.Text = ((CInt(txtQuantity.Text) - 2) * Decimal.Parse(txtAmount.Text.Replace("$", "")))
                            Else
                                lblAmount.Text = "0.00"
                            End If
                        Else
                            lblAmount.Text = (CInt(txtQuantity.Text) * Decimal.Parse(txtAmount.Text.Replace("$", "")))
                        End If
                    Else
                        lblAmount.Text = "0.00"
                        If lblProductId.Text = "134" Or lblProductId.Text = "135" Then
                        Else
                            chk.Checked = False
                        End If

                    End If
                    lblTotal.Text = "Total  :  $" & CalcAmount()
                End If
                If ddlPaymntMthd.SelectedValue = "1" Then
                    Dim lblCreditCardAmt As Label = CType(dgitem.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Text = lblAmount.Text
                    lblCreditCardAmt.Visible = True
                ElseIf ddlPaymntMthd.SelectedValue = "2" Then
                    Dim lblCheckAmt As Label = CType(dgitem.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Visible = True
                    lblCheckAmt.Text = lblAmount.Text
                ElseIf ddlPaymntMthd.SelectedValue = "3" Then
                    Dim lblInKindAmt As Label = CType(dgitem.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Text = lblAmount.Text
                    lblInKindAmt.Visible = True
                End If
            Else
                txtQuantity.Text = 0
                txtQuantity.Enabled = False
                lblAmount.Text = "$0.00"
                lblTotal.Text = "Total  :  $" & CalcAmount()
                If ddlPaymntMthd.SelectedValue = "1" Then
                    Dim lblCreditCardAmt As Label = CType(dgitem.FindControl("lblCreditCardAmt"), Label)
                    lblCreditCardAmt.Visible = False
                ElseIf ddlPaymntMthd.SelectedValue = "2" Then
                    Dim lblCheckAmt As Label = CType(dgitem.FindControl("lblCheckAmt"), Label)
                    lblCheckAmt.Visible = False
                ElseIf ddlPaymntMthd.SelectedValue = "3" Then
                    Dim lblInKindAmt As Label = CType(dgitem.FindControl("lblInKindAmt"), Label)
                    lblInKindAmt.Visible = False
                End If
            End If
        End If
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim firstName As String = String.Empty
        lblCustIndID.Text = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        Dim strSqlOrg As New StringBuilder

        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" & firstName & "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
                strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" & lastName & "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
                strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" & lastName & "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
                strSqlOrg.Append(" and O.EMAIL like '%" & email & "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
                strSqlOrg.Append(" O.EMAIL like '%" & email & "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by 1,2,3")
            'strSqlOrg.Append(" order by O.ORGANIZATION_NAME")
            SearchMembers(strSql.ToString(), strSqlOrg.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String, ByVal sqlStOrg As String)
        Dim strSQL As String = " select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " & sqlStOrg & " Union All"
        strSQL = strSQL & " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " & sqlSt
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No member match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        clearsession()
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        If row.Cells(10).Text.Trim = "OWN" Then
            Session("FundEmail") = row.Cells(3).Text.Trim
            lblCustIndID.Text = GridMemberDt.DataKeys(index).Value
            Session("CustIndID") = GridMemberDt.DataKeys(index).Value
            ltl1.Text = "Name : " & row.Cells(1).Text.Trim
            Session("FundDonorType") = "OWN"
        Else
            Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType,Email,chapterID,FirstName+' '+LastName as Name from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & "")
            While reader.Read()
                If reader("DonorType").ToUpper() = "IND" Then
                    lblCustIndID.Text = reader("Automemberid")
                    Session("FundEmail") = reader("Email")
                    Session("CustIndID") = reader("Automemberid")
                    ltl1.Text = "Name : " & reader("Name")
                    Session("FundDonorType") = "IND"
                ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                    lblCustIndID.Text = reader("Relationship")
                    Session("CustIndID") = reader("Relationship")
                    ltl1.Text = "Name : " & reader("Name")
                    Session("FundDonorType") = "IND"
                    setSpouseValues(Session("CustIndID"))
                End If
            End While
            reader.Close()
        End If
        If Not lblCustIndID.Text = String.Empty Then
            ' Panel4.Visible = False
            pIndSearch.Visible = False
            ' lblIndSearch.Visible = False
            'trH1.Visible = True
            'trH2.Visible = True
            'trH3.Visible = True
            btnSrchParent.Visible = True
            'deleteOldRec(Session("CustIndID"))
            dgCatalog.Visible = True
            loadDropdown()
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "No data found"
        End If
    End Sub

    Private Sub setSpouseValues(ByVal Memberid As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Email,chapterID from Indspouse where automemberid=" & Memberid & "")
        If ds.Tables(0).Rows.Count > 0 Then
            Session("FundEmail") = ds.Tables(0).Rows(0)("Email")
        End If
    End Sub

    Protected Sub btnSrchParent_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = True
        ltl1.Text = String.Empty
        trH1.Visible = False
        trH2.Visible = False
        'trH3.Visible = False
        dgCatalog.Visible = False
        lblTotal.Text = ""
    End Sub
    Private Sub clearsession()
        Session.Remove("FundMemberID")
        Session.Remove("Donation")
        Session.Remove("outXml")
        Session.Remove("PaymentReference")
        Session.Remove("R_Approved")
    End Sub

    Private Sub loadproduct(ByVal prductID As String)
        'Response.Write("select P.ProductId,P.ProductCode,P.Name as ProductName,C.ProductGroupId, C.ProductGroupCode  from  contestcategory C Inner Join Product P On p.EventId = 1 AND C.ContestCode= P.ProductCode WHERE C.contestyear = " & Session("EventYear") & " and C.NationalFinalsStatus = 'Active' and C.Contestcode<>'BB' AND C.ContestCode not in (Select ProductCode from BeeRankSponsor where  EventYear = " & Session("EventYear") & " AND ProductID=" & prductID & " and PaymentReference is Not null)")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select P.ProductId,P.ProductCode,P.Name as ProductName,C.ProductGroupId, C.ProductGroupCode  from  contestcategory C Inner Join Product P On p.EventId = 1 AND C.ContestCode= P.ProductCode WHERE C.contestyear = " & Session("EventYear") & " and C.NationalFinalsStatus = 'Active' and C.Contestcode<>'BB' AND C.ContestCode not in (Select SpProductCode from BeeRankSponsor where  EventYear = " & Session("EventYear") & " AND ProductID=" & prductID & " and PaymentReference is Not null)")
        lstProducts.DataSource = ds
        lstProducts.DataBind()
    End Sub

    Protected Sub btnPrdContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblPrdError.Text = ""
        'Coding for insert
        Dim StrSQl, StrSQl1 As String
        Dim productIDs As String = "0"
        Dim i, cnt As Integer
        cnt = 0
        If lstProducts.Items.Count > 0 Then
            For i = 0 To lstProducts.Items.Count - 1
                If lstProducts.Items(i).Selected Then
                    cnt = cnt + 1
                    productIDs = productIDs & "," & lstProducts.Items(i).Value.ToString()
                End If
            Next i
        Else
        End If
        If cnt > 0 Then
            Dim FFFeesId As String

            StrSQl = "select COUNT(FR.FundRRegID) FROM FundRFees FF  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = FR.ProductId and FF.EventYear = FR.EventYear"
            StrSQl = StrSQl & " WHERE FF.FundRFeesID=" & lblFundRFeesID.Text & " AND  FR.FundRCalID = FF.FundRCalID  AND FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "'"

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQl) > 0 Then
                StrSQl1 = "select Top 1 FR.FundRRegID FROM FundRFees FF  Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = FR.ProductId and FF.EventYear = FR.EventYear"
                StrSQl1 = StrSQl1 & " WHERE FF.FundRFeesID=" & lblFundRFeesID.Text & " AND  FR.FundRCalID = FF.FundRCalID  AND FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "'"
                FFFeesId = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQl1)
                StrSQl = " Update FR SET FR.ChapterId=FF.ChapterId, FR.ChapterCode=FF.ChapterCode, FR.EventId=FF.EventId, FR.EventCode=FF.EventCode, FR.EventDate=FF.EventDate, FR.MemberId = " & Session("CustIndID") & ", FR.DonorType='" & Session("FundDonorType") & "', FR.ProductGroupId=FF.ProductGroupId, FR.ProductGroupCode=FF.ProductGroupCode, FR.ProductId=FF.ProductId, FR.ProductCode=FF.ProductCode, FR.EventYear=FF.EventYear, FR.Quantity=" & cnt & ", FR.UnitFee=FF.FeeFrom, FR.TotalAmt=" & cnt & "*FF.FeeFrom, FR.TaxDeduction=FF.TaxDeduction, FR.PaymentMode='" & ddlProductPaymentmthd.SelectedItem.Text & "', FR.FundRCalID=FF.FundRCalID,  FR.ModifyDate=GetDate(), ModifiedBy=" & Session("LoginID")
                StrSQl = StrSQl & " from FundRReg FR, FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & " AND FR.FundRRegID= " & FFFeesId
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
            Else
                StrSQl1 = "INSERT INTO FundRReg(ChapterId, ChapterCode, EventId, EventCode, EventDate, MemberId, DonorType, ProductGroupId, ProductGroupCode, ProductId, ProductCode, EventYear, Quantity, UnitFee, TotalAmt, TaxDeduction, PaymentMode, FundRCalID, CreateDate, CreatedBy) "
                StrSQl1 = StrSQl1 & " select FF.ChapterId, FF.ChapterCode, FF.EventId, FF.EventCode, FF.EventDate," & Session("CustIndID") & ",'" & Session("FundDonorType") & "', FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode, FF.EventYear," & cnt & ",FF.FeeFrom," & cnt & "*FF.FeeFrom, FF.TaxDeduction,'" & ddlProductPaymentmthd.SelectedItem.Text & "', FF.FundRCalID,Getdate()," & Session("LoginID")
                StrSQl1 = StrSQl1 & " from FundRFees  FF where FF.FundRFeesID = " & lblFundRFeesID.Text & "; Select Scope_Identity()"
                FFFeesId = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQl1)
            End If

            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "delete from BeeRankSponsor  where FundRRegID = " & FFFeesId & " AND PaymentReference is Null and SpProductID not in (" & productIDs & ") ")
            If lstProducts.Items.Count > 0 Then
                For i = 0 To lstProducts.Items.Count - 1
                    If lstProducts.Items(i).Selected Then
                        'Response.Write("select Count (BeeRSponsorID)  from BeeRankSponsor  where FundRRegID = " & FFFeesId & " and SpProductID = " & lstProducts.Items(i).Value.ToString())
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count (BeeRSponsorID)  from BeeRankSponsor  where FundRRegID = " & FFFeesId & " and SpProductID = " & lstProducts.Items(i).Value.ToString()) = 0 Then
                            StrSQl = "INSERT INTO BeeRankSponsor (MemberID, ChapterID, FundRRegID, EventID, EventYear, FundRCalID, ProductGroupID, ProductGroupCode, ProductID, ProductCode, SpProductGroupID, SpProductGroupCode, SpProductID, SpProductCode, Amount, TaxDeduction,DonorType,PaymentMode)"
                            StrSQl = StrSQl & " select " & Session("CustIndID") & ", FF.ChapterId, " & FFFeesId & ", FF.EventId, FF.EventYear,FF.FundRCalID, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode, P.ProductGroupId ,P.ProductGroupCode ,P.ProductId ,P.ProductCode,FF.FeeFrom,FF.TaxDeduction,'" & Session("FundDonorType") & "','" & ddlProductPaymentmthd.SelectedItem.Text & "'"
                            StrSQl = StrSQl & " from FundRFees  FF INNER JOIN Product P ON P.ProductID = " & lstProducts.Items(i).Value.ToString() & " where FF.FundRFeesID = " & lblFundRFeesID.Text & "; "
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrSQl)
                        End If
                    End If
                Next i
            Else
            End If
            div1.Visible = True
            div2.Visible = False
            loadgrid(lblFundRCalID.Text)
        Else
            lblPrdError.Text = "Please select one Product"
        End If

    End Sub

    Protected Sub btnPrdCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnPrdCancel.Click
        div1.Visible = True
        div2.Visible = False
    End Sub

    'Protected Sub btnFillChild_Click(sender As Object, e As EventArgs)
    '    LoadChild()
    '    Dim unPaidBee As Integer = GetUnPaidRegistration()
    '    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('show'," & unPaidBee & ");", True)
    'End Sub
    Protected Sub btnCalc_Click(sender As Object, e As EventArgs)

        lblTotal.Text = "Total  :  $" & CalcAmount()
        Dim chk As CheckBox, lblProductId As Label, item As DataGridItem
        For Each item In dgCatalog.Items
            If item.ItemIndex = 5 Then Continue For
            chk = CType(item.FindControl("chkSelect"), CheckBox)
            If chk.Checked = True Then
                lblProductId = CType(item.FindControl("lblProductId"), Label)
                If lblProductId.Text = "105" Then
                    Dim txtQuantity As TextBox = CType(item.FindControl("txtQuantity"), TextBox)
                    Dim txtAmount As TextBox = CType(item.FindControl("txtAmount"), TextBox)
                    txtQuantity.Enabled = False
                    txtAmount.Enabled = False

                    Exit For
                End If
            End If
        Next
    End Sub

    Protected Sub dgChild_ItemDataBound(sender As Object, e As DataGridItemEventArgs) Handles dgChild.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.AlternatingItem, ListItemType.Item
                Dim lblChildNumber As Label = e.Item.FindControl("lblChildNumber")
                If lblChildNumber.Text = "0" Then
                    Dim lblFirstName As Label = e.Item.FindControl("lblFirstName")
                    Dim txtFirstName As TextBox = e.Item.FindControl("txtFirstName")
                    Dim lblLastName As Label = e.Item.FindControl("lblLastName")
                    Dim txtLastName As TextBox = e.Item.FindControl("txtLastName")
                    Dim lblDOB As Label = e.Item.FindControl("lblDOB")
                    Dim txtDOB As TextBox = e.Item.FindControl("txtDOB")

                    lblFirstName.Visible = False
                    lblLastName.Visible = False
                    lblDOB.Visible = False

                    txtFirstName.Visible = True
                    txtLastName.Visible = True
                    txtDOB.Visible = True

                End If
                Dim lblGrade As Label = e.Item.FindControl("lblGrade")
                Dim ddlGrade As DropDownList = e.Item.FindControl("ddlGrade")
                ddlGrade.SelectedIndex = ddlGrade.Items.IndexOf(ddlGrade.Items.FindByText(lblGrade.Text))
                Dim lblGender As Label = e.Item.FindControl("lblGender")
                Dim ddlGender As DropDownList = e.Item.FindControl("ddlGender")
                ddlGender.SelectedIndex = ddlGender.Items.IndexOf(ddlGender.Items.FindByText(lblGender.Text))

                Dim lblTotal As Label = e.Item.FindControl("lblTotal")

                Dim chkSpelling As CheckBox = CType(e.Item.FindControl("chkSpelling"), CheckBox)
                Dim chkMath As CheckBox = CType(e.Item.FindControl("chkMath"), CheckBox)
                Dim chkScience As CheckBox = CType(e.Item.FindControl("chkScience"), CheckBox)
                Dim chkGeography As CheckBox = CType(e.Item.FindControl("chkGeography"), CheckBox)
                Dim dt As DataTable
                dt = Session("Grades")
                For Each dr As DataRow In dt.Rows
                    If dr("ProductGroupCode") = "SB" Then
                        If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                            chkSpelling.Enabled = True
                        Else
                            chkSpelling.Enabled = False
                        End If
                    ElseIf dr("ProductGroupCode") = "MB" Then
                        If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                            chkMath.Enabled = True
                        Else
                            chkMath.Enabled = False
                        End If
                    ElseIf dr("ProductGroupCode") = "SC" Then
                        If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                            chkScience.Enabled = True
                        Else
                            chkScience.Enabled = False
                        End If
                    ElseIf dr("ProductGroupCode") = "GB" Then
                        If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                            chkGeography.Enabled = True
                        Else
                            chkGeography.Enabled = False
                        End If

                    End If

                Next
                Dim lblSB As Label, lblMB As Label, lblSC As Label, lblGB As Label
                lblSB = CType(e.Item.FindControl("lblSB"), Label)
                lblMB = CType(e.Item.FindControl("lblMB"), Label)
                lblSC = CType(e.Item.FindControl("lblSC"), Label)
                lblGB = CType(e.Item.FindControl("lblGB"), Label)
                If CInt(lblSB.Text) > 0 Then
                    chkSpelling.Enabled = False
                End If
                If CInt(lblMB.Text) > 0 Then
                    chkMath.Enabled = False
                End If
                If CInt(lblSC.Text) > 0 Then
                    chkScience.Enabled = False
                End If
                If CInt(lblGB.Text) > 0 Then
                    chkGeography.Enabled = False
                End If
                Dim total As Integer = 0
                If chkSpelling.Checked Then total = total + 1
                If chkMath.Checked Then total = total + 1
                If chkScience.Checked Then total = total + 1
                If chkGeography.Checked Then total = total + 1
                lblTotal.Text = total
                total = 0
                Dim dgitemChild As DataGridItem
                For Each dgitemChild In dgChild.Items
                    If CType(dgitemChild.FindControl("lblTotal"), Label).Text.Trim <> "" Then
                        total = total + CInt(CType(dgitemChild.FindControl("lblTotal"), Label).Text)
                    End If
                Next

        End Select
    End Sub

    Protected Sub ddlQty_SelectedIndexChanged(sender As Object, e As EventArgs)
        Dim ddlQty As DropDownList = CType(sender, DropDownList)
        Dim dgitem As DataGridItem = CType(ddlQty.Parent.Parent, DataGridItem)
        Dim txtQuantity As TextBox = dgitem.FindControl("txtQuantity")
        If ddlQty.SelectedIndex > 0 Then
            txtQuantity.Text = ddlQty.SelectedValue
        Else
            txtQuantity.Text = "0"
        End If
        txtQuantity_TextChanged(txtQuantity, New System.EventArgs)

    End Sub
    Protected Sub ddlGrade_SelectedIndexChanged(sender As Object, e As EventArgs)

        Dim ddlGrade As DropDownList = CType(sender, DropDownList)
        Dim dgitem As DataGridItem = CType(ddlGrade.Parent.Parent, DataGridItem)


        Dim chkSpelling As CheckBox = CType(dgitem.FindControl("chkSpelling"), CheckBox)
        Dim chkMath As CheckBox = CType(dgitem.FindControl("chkMath"), CheckBox)
        Dim chkScience As CheckBox = CType(dgitem.FindControl("chkScience"), CheckBox)
        Dim chkGeography As CheckBox = CType(dgitem.FindControl("chkGeography"), CheckBox)
        Dim dt As DataTable
        dt = Session("Grades")
        For Each dr As DataRow In dt.Rows
            If dr("ProductGroupCode") = "SB" Then
                If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                    chkSpelling.Enabled = True
                Else
                    chkSpelling.Enabled = False
                End If
            ElseIf dr("ProductGroupCode") = "MB" Then
                If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                    chkMath.Enabled = True
                Else
                    chkMath.Enabled = False
                End If
            ElseIf dr("ProductGroupCode") = "SC" Then
                If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                    chkScience.Enabled = True
                Else
                    chkScience.Enabled = False
                End If
            ElseIf dr("ProductGroupCode") = "GB" Then
                If ddlGrade.SelectedValue >= dr("GradeFrom") And ddlGrade.SelectedValue <= dr("GradeTo") Then
                    chkGeography.Enabled = True
                Else
                    chkGeography.Enabled = False
                End If
            End If
        Next
        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpNoOfConts('show',0);", True)
    End Sub


    Protected Sub btnOkContinue_Click(sender As Object, e As EventArgs) Handles btnOkContinue.Click
        hfTicketContinue.Value = "True"
        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpTicket('hide','');", True)
        btnContinue_Click(sender, e)
    End Sub

    Protected Sub btnTicketCancel_Click(sender As Object, e As EventArgs) Handles btnTicketCancel.Click
        ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpTicket('hide','');", True)
    End Sub
End Class
