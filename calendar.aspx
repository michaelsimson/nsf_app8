<%@ Page language="vb" runat="server" %>
<%@ Import Namespace="System.Data.SqlClient" %>

<!--#include file="regionals_header.aspx"-->

<%
    Dim queryState As String
    Dim StateValue As String
    Dim IsAnyError As Boolean
    Dim contestyear As Integer
    'default value
    IsAnyError = True
    'default value for state = NO means do not show any info
    queryState = "NO"

    If Request.QueryString.Count = 0 Then
        queryState = "NO"
        IsAnyError = False
    ElseIf Request.QueryString.Count = 1 Then
        'accept only one querystring value
        'check if contest value included - only one contest value
        If Request.QueryString("state").Length > 0 Then
            'contest value can not be empty
            If Request.QueryString("state") <> "" Then
                'read contest value
                StateValue = Request.QueryString("state")
                'expect 2 characters only
                If Len(StateValue) = 2 Then
                    'check if state name is in our States list
                    Select Case StateValue
                        Case "AL", "AZ", "CA", "CO", "CT", "DE", "FL", "GA", "IA", "IL", "IN", "KS", "LA", "MA", "MD", "MI", "MN", "MO", "MS", "NC", "NH", "NJ", "NY", "OH", "OR", "PA", "SC", "TN", "TX", "UT", "VA", "WA", "WI"
                            queryState = StateValue
                            IsAnyError = False
                        Case Else
                            IsAnyError = True
                            queryState = "NO"
                    End Select
                End If
            End If
        End If
    End If

If IsAnyError = True Then
    response.redirect("http://www.northsouth.org")
End If

    Dim cn As SqlConnection = New SqlConnection("Data Source=sql.northsouth.org;User ID=northsouth;Password=uttaramdakshinam;Initial Catalog=northsouth_prd;Connect Timeout=120")
    ''**This line is used to fetch the connection string from web.config file.
    ''(System.Configuration.ConfigurationManager.ConnectionStrings("NSFConnectionString").ConnectionString)
    Dim cmd As SqlCommand
    Dim rs As SqlDataReader
    Dim strquery As String
    
    cn.Open()
    strquery = "select MAX(contest_year) as contestyear from contest"
    cmd = New SqlCommand(strquery, cn)
    'ExecuteScalar for retriving one value from the query like Count, Max,Min,ID,etc
    contestyear = cmd.ExecuteScalar()
    cn.Close()

    cn.Open()
    strquery = "select distinct State from Chapter where status = 'A'"
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
%>

<div class="title02"><%=contestyear%> Calendar</div>

<div class="txt01" align="justify">
This is the main page of <span class="txt01_strong"><%= contestYear %> Regional Contest Calendar</span>.
This page provides a detailed information about the Contests for every Chapter.
The <span class="txt01_strong">Chapters are listed by State</span>, so click on a state to see information
about Contest Date, Venue and name, email and phone number of person(s) to be
contacted for the Contests.
<p>
*Caution:  Before you select contests and register, make certain that contests are 
offered in your chapter.  If you do not see any contests in your chapter,  consult your 
chapter coordinator.  You cannot select contests and register until the contest calendar 
is set for your location. It is possible that the calendar may exist for a neighboring 
chapter in your cluster in which case you can choose to register there or wait until your
chapter calendar is set.
</p>

<span class="btn_01">
<%
   Dim firstState as Integer
   firstState = 1
%>
<%  While rs.read()%>
  <% If firstState = 1 Then
        firstState = 0
     else %>
     |
  <% end if %>
  <%If Request.QueryString("state") = rs(0) Then%>
     <%=rs(0)%>  
  <% else %>
     <a href="calendar.aspx?state=<%=rs(0)%>"> <%=rs(0)%> </a>
  <% end if %>
<%  End While%>
<%cn.Close() %>
</span>

<% if request.querystring("state") = "" then%>

  <p class="txt01_strong">
    Click on a state you are interested in to view detailed contest calendar.
  </p>
  
<%else%>

 <p class="txt01"> 
   Following is the Contest Calendar of Chapters in State: <span class="txt01_strong"><%=request.querystring("state")%></span>
 </p>

<% end if %>

<%
    cn.Open()
    If Request.QueryString("state") = "" Then
        strquery = "select ChapterID, ChapterCode from Chapter where status='A' and State='NO'" 
    else
        strquery = "select ChapterID, ChapterCode from Chapter where status='A' and State='" & request.querystring("state") & "' order by State"
    End if
    cmd = New SqlCommand(strquery, cn)
    rs = cmd.ExecuteReader()
%>

<table width="100%" border="0" cellpadding="3" cellspacing="1" bgcolor="#CCCCCC">
    <tr>
        <td width="15%" height="24" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Center</td>
        <td width="35%" class="btn_06"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
        <tr>
            <td width="36%" bgcolor="#CCCCCC">&nbsp;&nbsp;Contest </td>
            <td width="64%" bgcolor="#CCCCCC">&nbsp;&nbsp;Date/Check-in Time</td>
        </tr>
        </table></td>
        <td width="15%" class="btn_06">&nbsp;&nbsp;&nbsp;&nbsp;Venue</td>
        <td width="35%" class="btn_06"><table width="100%" border="0" cellpadding="0" cellspacing="1" bgcolor="#666666">
        <tr>
            <td width="50%" bgcolor="#CCCCCC">&nbsp;&nbsp;ContactName </td>
            <td width="50%" bgcolor="#CCCCCC">&nbsp;&nbsp;Email &amp; Phone</td>
        </tr>
        </table></td>
    </tr>

<%
    Dim cmd2 As SqlCommand
    Dim cmd3 As SqlCommand
    Dim strquery2 As String
    Dim strquery3 As String
    Dim rs3 As SqlDataReader
    Dim numberofrecords As Integer
    Dim firstRec As Integer
    Dim prevRegDate As String
    'I have added one more Sqlconnection since one connection can have one active Reader only.
    'After using the reader after while we must close the Connection
    Dim cn1 As SqlConnection = New SqlConnection("Data Source=sql.northsouth.org;User ID=northsouth;Password=uttaramdakshinam;Initial Catalog=northsouth_prd;Connect Timeout=120")
    While rs.Read()%>
  <tr>
    <td bgcolor="#FFFFFF" valign="top" class="txt01_strong"><%=rs(1)%></td>

    <td bgcolor="#FFFFFF" valign="top" class="txt01">
        <table width="100%" border=1 cellpadding="0" cellspacing="0" bordercolor="#CCCCCC">
        <%
            strquery2 = "select count(*) as numberofrecords from Contest where Contest_Year = " & contestyear & " and NSFChapterID =" & rs(0)
            cmd2 = New SqlCommand(strquery2, cn1)
            cn1.Open()
            numberofrecords = cmd2.ExecuteScalar()
            cn1.Close()
            If numberofrecords = 0 Then
        %>
        <tr>
            <td width="100%">Contests not yet scheduled for this center.</td>
        </tr>
        <%
            else

            strquery3 = "select b.ContestCode, a.ContestDate, a.CheckinTime, a.Contest_Year, a.RegistrationDeadline from Contest a, ContestCategory b where a.Contest_Year  = " &  contestyear  & "  and a.ContestCategoryID = b.ContestCategoryID and a.NSFChapterID =" & rs(0) & " order by a.RegistrationDeadline Desc, a.ProductId"
            cn1.Open()
            cmd3 = New SqlCommand(strquery3, cn1)
            rs3 = cmd3.ExecuteReader()
            firstRec = 1
            While rs3.read()
                If firstRec = 1 Then
                    prevRegDate = rs3(4) & ""
                    firstRec = 0
            %>
            <tr>
                <td width="36%" class="txt01_strong">Reg. Closes:</td>
                <td width="64%" class="txt01_strong">rs3(4)</td>
            </tr>
            <%
            Else
                If prevRegDate <> (rs3(4) & "") Then
            %>
                    </table>
                                       <table width="100%" border="1" cellpadding="0" cellspacing="0" bordercolor="#666666">
                    <tr>
                        <td width="36%" class="txt01_strong">Reg. Closes:</td>
                        <td width="64%" class="txt01_strong">rs3(4)</td>
                    </tr>
            <%
            End If
        End If
            %>
            <tr>
                <td width="36%" class="txt01"><%=rs3(0)%></td>
                <td width="64%" class="txt01"><%=rs3(1)%> &nbsp;&nbsp; <%=rs3(2)%></td>
            </tr>
           
<% End While 
End If
'Closing the connection
  cn1.Close()
%>
  
        </table>
    </td>

    <td bgcolor="#FFFFFF"> &nbsp; </td>
    <td bgcolor="#FFFFFF"> &nbsp; </td>
  </tr>
<%  End While%>
<%cn.Close()%>

</table>
</div>


<!--#include file="regionals_footer.aspx"-->

