

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Email_TC.aspx.vb" Inherits="Email_TC" title="National Coordinator's - Send EMail" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <table style="width: 680px">
      <tr>
         <td style="width: 680px;">
            <asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="284px" ></asp:HyperLink>
         </td>
      </tr>
   </table>
   <table>
      <tr>
         <td align="center" style="width: 542px">
            <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label>
            <asp:Label ID="lblerr" runat="server" ForeColor="Red" Text="You are not allowed to use this application." Visible="False" Width="471px"></asp:Label>
         </td>
      </tr>
   </table>
   <table id="tabletarget" runat="server" width="100%" border="0" cellpadding="0" cellspacing="3">
      <tr style="height:25px">
         <td style="width: 6px" dir="ltr" nowrap="nowrap" visible="false" colspan="6">
            &nbsp;
         </td>
      </tr>
      <tr>
        
         <td style="width: 84px; height: 28px;">
            <asp:Label ID="lblevent" runat="server" Text="Event" Font-Bold="true"></asp:Label><br />
              <asp:DropDownList id="drpevent"  DataTextField="Name" DataValueField="EventID" Width="140px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="drpevent_SelectedIndexChanged">
            </asp:DropDownList>
         </td>
         <td  align="left" style="width: 170px; height: 28px;" colspan="2">
         <asp:Label ID="lblChapterList" runat="server" Text="Chapter List" Font-Bold="true"></asp:Label><br />
            <asp:ListBox Width="45%" Height="60px" ID="lstchapter" runat="server" AutoPostBack="true" SelectionMode="Multiple" DataValueField="chapterID" DataTextField="ChapterCode">
            </asp:ListBox> &nbsp;<asp:TextBox ID="txtselectedchapter" runat="server" TextMode="MultiLine" Enabled="false" Width="45%" Height="60px"></asp:TextBox>
         </td>
          <td style="width: 84px; height: 28px;">
            <asp:Label ID="lblyear" Text="EventYear" runat="server" Width="140px" Font-Bold="true"></asp:Label><br />
            <asp:ListBox id="lstyear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server" OnSelectedIndexChanged="lstyear_SelectedIndexChanged" AutoPostBack="true">
            </asp:ListBox>
         </td>
         <td style="width: 84px; height: 28px;">
            <asp:Label ID="lblWeekOf" Text="WeekOf" runat="server" Font-Bold="true" ></asp:Label><br />
            <asp:ListBox id="lstWeekOf" DataValueField="Week" DataTextField="Week" Enabled="false" SelectionMode="Multiple"  Width="140px" Height="75px" runat="server"  AutoPostBack="true" >
            </asp:ListBox>
         </td>
         <td style="width: 40px;text-align:right">
            &nbsp;
         </td>
      </tr>
      <tr></tr>
       <tr style="height:25px">
           <td style="width: 84px; height: 28px;">
            <asp:Label ID="lblproductgroup" Text="ProductGroup" Width="140px" runat="server" Font-Bold="true"></asp:Label><br />
             <asp:ListBox id="lstProductGroup" DataValueField="ProductGroupCode" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" >
            </asp:ListBox>
         </td>
         <td style="width: 84px; height: 28px;">
            <asp:Label ID="lblProductid" Text="Product" runat="server" Width="140px" Font-Bold="true"></asp:Label><br />
            <asp:ListBox id="lstProductid" Enabled="false" DataValueField="ProductCode" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged">
            </asp:ListBox>
            <br /><br />
         </td>
         <td  align="left" style="width: 84px; height: 28px;">
            <asp:Label ID="lblassignrole" Text="Role" runat="server" Width="140px" Font-Bold="true"></asp:Label><br />
            <asp:ListBox id="lstAssignRole" Enabled="false" DataValueField="RoleID" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" OnSelectedIndexChanged="lstAssignRole_SelectedIndexChanged" AutoPostBack="true" >
            </asp:ListBox>
            <br />
         </td>
         <td style="width: 84px; height: 28px;">&nbsp;</td>
         <td>
            <br />
         </td>
          <td style="width: 40px;text-align:right">
            &nbsp;
         </td>
      </tr>
      <tr>
         <td style="width: 6px; height: 58px;"></td>
         <td colspan="2" style="width: 64px; height: 58px;">
         <asp:Label ID ="lblPageErr" runat="server" Text="" ForeColor ="Red"></asp:Label>
         </td>
         <td style="width: 84px; height: 58px;"></td>
       
         <td style="height: 58px" colspan="2">
           <%-- <asp:Label ID="lblNote" runat="server" ForeColor="Red" 
               Text="**Note:  Sends email if and only if <br /> a) Valid Email flag is NULL and  <br /> b) Newsletter is not in 2 or 3" 
               Visible="true"></asp:Label>--%>
         </td>
         <td colspan="2" style="height: 58px">
             &nbsp;</td>
      </tr>
      <tr>
         <td style="height: 24px; width: 6px;"></td>
         <td style="height: 24px; width: 64px;"></td>
         <td style="width: 253px; height: 24px;" colspan="2">
            <asp:Button ID="btnselectmail" runat="server" Text="Continue" />
         </td>
         <td>&nbsp;</td>
          <td style="width: 40px;text-align:right">
            &nbsp;
         </td> 
         <td style="width: 40px;text-align:right">
            &nbsp;
         </td>
      </tr>
      
   </table>
</asp:Content>

