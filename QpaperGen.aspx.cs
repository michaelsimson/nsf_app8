﻿using System;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using Microsoft.ApplicationBlocks.Data;
using System.Globalization;

public partial class QpaperGen : System.Web.UI.Page
{
    string Connectionstring = "Connectionstring";
    string strUnionSql;
    DataSet ds;
    string WCNTQry = string.Empty;
    string CommonQry = string.Empty;
    string WCNTQrynew = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            {
                Response.Redirect("~/login.aspx?entry=p");
            }
        }
        catch
        {
            SessionExp();
        }
    }
    private void SessionExp()
    {
        if (Session["LoggedIn"] == null)
        {
            Response.Redirect("~/Maintest.aspx");

        }
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        try
        {
            strUnionSql = "Select 'Phase1' as Phase,GBQ.[SeqNo],GBQ.Category,GBQ.Question,GBQ.A,GBQ.B, GBQ.C,GBQ.D,GBQ.E,GBQ.Answer,GBQ.[LetterAnswer],GBQ.[MultipleChoice] FROM GBQ Where [GBQ].[Category] = 'ZZzz'";
            string StrFirstQry = "SELECT Rules.Phase1,Rules.Phase2,Rules.Phase3,Rules.Category from [Rules] where Rules.Category in (Select distinct GBQ.Category from GBQ) ";
            ds = SqlHelper.ExecuteDataset(Application[Connectionstring].ToString(), CommandType.Text, StrFirstQry);
            DataTable Dtnew = ds.Tables[0];
            for (int i = 0; i < Dtnew.Rows.Count; i++)
            {
                CommonQry = strUnionSql + " UNION Select TOP " + Dtnew.Rows[i][0].ToString() + " 'Phase1' as Phase,GBQ.[SeqNo],GBQ.Category,GBQ.Question," +
                    "GBQ.A,GBQ.B, GBQ.C,GBQ.D,GBQ.E,GBQ.Answer,GBQ.[LetterAnswer],GBQ.[MultipleChoice] FROM GBQ Where [GBQ].[Category] = '" + Dtnew.Rows[i][3].ToString() + "'"
                    + " UNION Select TOP " + Dtnew.Rows[i][1].ToString() + " 'Phase1' as Phase,GBQ.[SeqNo],GBQ.Category,GBQ.Question," +
                    "GBQ.A,GBQ.B, GBQ.C,GBQ.D,GBQ.E,GBQ.Answer,GBQ.[LetterAnswer],GBQ.[MultipleChoice] FROM GBQ Where [GBQ].[Category] = '" + Dtnew.Rows[i][3].ToString() + "'"
                    + " UNION Select TOP " + Dtnew.Rows[i][2].ToString() + " 'Phase1' as Phase,GBQ.[SeqNo],GBQ.Category,GBQ.Question," +
                    "GBQ.A,GBQ.B, GBQ.C,GBQ.D,GBQ.E,GBQ.Answer,GBQ.[LetterAnswer],GBQ.[MultipleChoice] FROM GBQ Where [GBQ].[Category] = '" + Dtnew.Rows[i][3].ToString() + "'";
                if (WCNTQry != string.Empty)
                {
                    WCNTQrynew = CommonQry;
                    WCNTQry = WCNTQry + " union all " + WCNTQrynew;
                }
                else
                {
                    WCNTQry = CommonQry;
                }
            }
            ds = SqlHelper.ExecuteDataset(Application[Connectionstring].ToString(), CommandType.Text, WCNTQry);
            DataTable DtQuestion = ds.Tables[0];
            GeneralExport(DtQuestion, "QuestionPaper.xls");
        }
        catch
        {
             SessionExp();
        }
    }
    public void GeneralExport(DataTable dtdata, string fname)
    {
        try
        {
            string attach = string.Empty;
            attach = "attachment;filename=" + fname;
            Response.ClearContent();
            Response.AddHeader("content-disposition", attach);
            Response.ContentType = "application/vnd.xls";
            if (dtdata != null)
            {
                foreach (DataColumn dc in dtdata.Columns)
                {
                    Response.Write(dc.ColumnName + "\t");
                }
                Response.Write(System.Environment.NewLine);
                foreach (DataRow dr in dtdata.Rows)
                {
                    for (int i = 0; i < dtdata.Columns.Count; i++)
                    {
                        Response.Write(dr[i].ToString().Replace("\t", " ") + "\t");
                    }
                    Response.Write("\n");
                }
                Response.Flush();
                HttpContext.Current.Response.Flush();
                HttpContext.Current.Response.SuppressContent = true;
                HttpContext.Current.ApplicationInstance.CompleteRequest();
            }
        }
        catch
        {
            SessionExp();
        }
    }
}