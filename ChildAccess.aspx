<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="ChildAccess.aspx.cs" Inherits="ChildAccess" Title="Child Access" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="js/jquery-1.11.0.min.js"></script>
    <style type="text/css">
        .modal {
            display: none; /* Hidden by default */
            position: fixed; /* Stay in place */
            z-index: 1; /* Sit on top */
            left: 0;
            top: 0;
            width: 100%; /* Full width */
            height: 100%; /* Full height */
            overflow: auto; /* Enable scroll if needed */
            background-color: rgb(0,0,0); /* Fallback color */
            background-color: rgba(0,0,0,0.4); /* Black w/ opacity */
        }

        /* Modal Content/Box */
        .modal-content {
            background-color: #fefefe;
            margin: 15% auto; /* 15% from the top and centered */
            padding: 25px;
            border: 1px solid #888;
            width: 30%; /* Could be more or less, depending on screen size */
            height: auto;
        }

        /* The Close Button */
        .close {
            color: #aaa;
            float: right;
            font-size: 28px;
            font-weight: bold;
        }

            .close:hover,
            .close:focus {
                color: black;
                text-decoration: none;
                cursor: pointer;
            }

        .web_dialog_overlay {
            position: fixed;
            top: 0;
            right: 0;
            bottom: 0;
            left: 0;
            height: 100%;
            width: 100%;
            margin: 0;
            padding: 0;
            background: #000000;
            opacity: .15;
            filter: alpha(opacity=15);
            -moz-opacity: .15;
            z-index: 101;
            display: none;
        }

        .web_dialog {
            display: none;
            position: fixed;
            width: 520px;
            height: 200px;
            top: 55%;
            left: 50%;
            margin-left: -190px;
            margin-top: -100px;
            background-color: #ffffff;
            border: 2px solid #336699;
            padding: 0px;
            z-index: 102;
            font-family: Verdana;
            font-size: 10pt;
        }

        .web_dialog_title {
            border-bottom: solid 2px #336699;
            background-color: #336699;
            padding: 4px;
            color: White;
            font-weight: bold;
        }

            .web_dialog_title a {
                color: White;
                text-decoration: none;
            }

        .align_right {
            text-align: right;
        }
    </style>

    <script type="text/javascript">
        function OpenConfirmationBox() {
          
            ShowDialog(true);

            //var PopupWindow = null;
            //settings = 'width=700,height=600,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            //PopupWindow = window.open('SupportTracking.aspx?Action=Conf', 'CalSignUp Help', settings);
            //PopupWindow.focus();
        }
        function closeConfirmationBox() {
            HideDialog();

        }

        function ShowDialog(modal) {
            $("#overlay").show();

            $("#dvUpdateTicket").fadeIn(300);

            if (modal) {
                $("#overlay").unbind("click");
            }
            else {
                $("#overlay").click(function (e) {
                    HideDialog();
                });
            }
        }
        function HideDialog() {
            $("#overlay").hide();
            $("#dvUpdateTicket").hide();
        }

        function openPopu() {
            var modal = document.getElementById('myModal');
            modal.style.display = "block";
            var password = document.getElementById('<%= hdnPwd.ClientID%>').value;
            $("#lblPassoerd").text(password);
            $("#txtPasswordEdit").hide();
            $("#lblPassoerd").show();
            $("#spnSuccess").text("");
        }
        $(document).on("click", ".close", function (e) {
            var modal = document.getElementById('myModal');
            modal.style.display = "none";
            window.location.href = "ChildAccess.aspx";
        });
        $(document).on("click", "#BtnEdit", function (e) {
            if ($(this).val() == "Edit") {

                $(this).val("Update");

                $("#txtPasswordEdit").show();
                $("#lblPassoerd").hide();
                $("#txtPasswordEdit").val(document.getElementById('<%= hdnPwd.ClientID%>').value);

            } else {
                $(this).val("Edit");
                var password = $("#txtPasswordEdit").val();
                var childNumber = document.getElementById('<%= hdnChildNumber.ClientID%>').value;
                updateChildPwd(password, childNumber);
            }
        });

        function updateChildPwd(password, childNumber) {



            var jsonData = JSON.stringify({ Password: password, ChildNumber: childNumber });

            $.ajax({
                type: "POST",
                url: "ChildAccess.aspx/UpdatePassword",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {
                    $("#spnSuccess").text("Password updated successfully.");
                    $("#txtPasswordEdit").hide();
                    $("#lblPassoerd").show();

                    getPassword(password, childNumber);

                }, failure: function (e) {

                }
            });
        }

        function getPassword(password, childNumber) {
            var jsonData = JSON.stringify({ Password: password, ChildNumber: childNumber });

            $.ajax({
                type: "POST",
                url: "ChildAccess.aspx/GetPassword",
                data: jsonData,
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                success: function (data) {

                    $("#lblPassoerd").text(password);


                }, failure: function (e) {

                }
            });
        }


    </script>

    <asp:Panel ID="Panel1" runat="server">
        <asp:HyperLink ID="backToUserFunctions" CssClass="btn_02" NavigateUrl="UserFunctions.aspx" runat="server">Back to Parent Functions Page</asp:HyperLink>
        <center>
            <asp:Label ID="lblTitle" runat="server" Text="Manage Student Login/Email"
                Font-Bold="True" Font-Size="Large" ForeColor="#009900"></asp:Label>
            <br />
            <asp:HiddenField ID="hdSelChildName" runat="server" />
            <br />
        </center>
        <div align="center">
            <asp:GridView ID="gvChild" runat="server" AutoGenerateColumns="False"
                BackColor="White" BorderColor="#CC9966" BorderStyle="None" BorderWidth="1px"
                CellPadding="4" Height="63px" HeaderStyle-BackColor="#009900" HeaderStyle-ForeColor="White"
                Width="80%" OnRowCreated="gvChild_RowCreated" OnRowDataBound="gvChild_RowDataBound" OnRowCommand="gvChild_RowCommand">
                <RowStyle BackColor="White" ForeColor="#330099" />
                <HeaderStyle BackColor="#009900" ForeColor="White" />
                <Columns>
                    <asp:BoundField DataField="ChildNumber" HeaderText="Child Number"
                        SortExpression="ChildNumber">
                        <ControlStyle BorderColor="Black" Width="50px" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False"
                            BorderColor="Black" />
                        <ItemStyle Wrap="False" BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ChildName" HeaderText="Name Of Child"
                        SortExpression="ChildName">
                        <ControlStyle Width="100px" BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" HorizontalAlign="Left"
                            VerticalAlign="Middle" Wrap="False" BorderColor="Black" />
                        <ItemStyle Wrap="False" BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="GRADE" HeaderText="Grade" SortExpression="Grade">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DateOfBirth" HeaderText="Date Of Birth"
                        SortExpression="DateOfBirth">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False"
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Email" HeaderText="Email Address"
                        SortExpression="Email" HtmlEncode="False" HtmlEncodeFormatString="False">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False"
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <%-- <asp:BoundField DataField="PwD" HeaderText="Password" SortExpression="PwD">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False"
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>--%>
                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Password" HeaderStyle-BackColor="#009900" HeaderStyle-ForeColor="White" ItemStyle-BorderColor="Black">

                        <ItemTemplate>
                            <asp:Button ID="BtnShowPwd" runat="server" Text="Show PWD" CommandName="ShowPWD" />
                            <div style="display: none;">
                                <asp:Label ID="lblChildPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                                <asp:Label ID="lblEmail" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Email") %>'>'></asp:Label>
                                <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--     <asp:ButtonField ButtonType="Button" CommandName="ShowPWD" Text="Show PWD">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:ButtonField>--%>
                    <%--
                         //   As per the Request from Dr. C, Commanded by Dino on September 07 2016 :  To disable all fields which are related to Online Class Email and make it for future use.
                         <asp:BoundField  HeaderText="Email Address" DataField="OnlineClassEmail" 
                        SortExpression="Email" HtmlEncode="False" HtmlEncodeFormatString="False">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" ForeColor="White" Wrap="False" 
                            BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>--%>

                    <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" HeaderStyle-BackColor="#009900" HeaderStyle-ForeColor="White" ItemStyle-BorderColor="Black">

                        <ItemTemplate>
                            <asp:Button ID="BtnSelect" runat="server" Text="Select" CommandName="Select" />
                            <div style="display: none;">
                                <asp:Label ID="lblChildPwd1" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Pwd") %>'>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <%--   <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Select">
                        <ControlStyle BorderColor="Black" />
                        <HeaderStyle BackColor="#009900" BorderColor="Black" />
                        <ItemStyle BorderColor="Black" />
                    </asp:ButtonField>--%>
                </Columns>
                <FooterStyle BackColor="#FFFFCC" ForeColor="#330099" />
                <PagerStyle BackColor="#FFFFCC" ForeColor="#330099" HorizontalAlign="Center" />
                <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="#663399" />
                <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="#FFFFCC" />
            </asp:GridView>
        </div>

        <asp:Label ID="lblPid" runat="server" Text="Label" Visible="False"></asp:Label>
        <asp:Label ID="lblchid" runat="server" Text="Label" Visible="False"></asp:Label>
        <br />


    </asp:Panel>
    <div>
        <center>
            <asp:Label ID="lblResult" runat="server" Font-Bold="True"></asp:Label>
            <asp:Label ID="lblError" runat="server" Font-Bold="True" ForeColor="Red"></asp:Label></center>
        <asp:Panel ID="Panel2" runat="server" Visible="False">
            <div align="center">
                <table style="width: 95%; height: 57px;">
                    <tr>
                        <td align="left">
                            <b>Homework Submission :</b>  </td>
                        <td align="left">Email Address</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmailID" runat="server" Enabled="False"
                                Style="text-align: left" Width="175px"></asp:TextBox>
                        </td>
                        <td align="left">Confirm Email Address</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmailIDConfirm" runat="server" Enabled="False" Style="text-align: left" Width="175px"></asp:TextBox>
                        </td>
                    </tr>
                    <tr>
                        <td align="left" style="height: 46px"></td>
                        <td align="left" style="height: 46px">Password</td>
                        <td align="left" style="height: 46px">
                            <asp:TextBox ID="txtPassword" runat="server" Enabled="False" Width="150px"
                                Style="text-align: left"></asp:TextBox>
                        </td>
                        <td align="left" style="height: 46px">Confirm Password</td>
                        <td align="left" style="height: 46px">
                            <asp:TextBox ID="txtPasswordConfirm" runat="server" Enabled="False" Style="text-align: left" Width="150px"></asp:TextBox>
                        </td>
                    </tr>
                    <%-- <tr>
                            <td align="left" style="height: 51px" >
                               <b> Join Online Class :</b>
                                <br /><span style="color :Red; font-family:Arial; vertical-align:top">&nbsp;* - mandatory</span>
                            </td>
                            <td align="left" style="height: 51px">Email Address <span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;*</span></td>
                            <td style="height: 51px" align="left" ;>
                                <asp:TextBox ID="txtOnlineEmailID" runat="server" Enabled="False" style="text-align: left" Width="175px"></asp:TextBox>
                            </td>
                            <td align="left" style="height: 51px">Confirm Email Address <span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;*</span></td>
                            <td align="left" style="height: 51px">
                                <asp:TextBox ID="txtOnlineEmailIDConfirm" runat="server" Enabled="False" style="text-align: left; margin-top: 0px;" Width="175px"></asp:TextBox>
                            </td>
                        </tr>--%>
                    <tr>
                        <td align="left">&nbsp;</td>
                        <td align="left"></td>
                        <td align="left">
                            <asp:Button ID="btnAdd" runat="server" Enabled="False" OnClick="btnAdd_Click" Text="Add" Visible="false" />
                            <asp:Button ID="btnUpdate" runat="server" Enabled="False" OnClick="btnUpdate_Click" Text="Update" />
                        </td>
                        <td align="left">&nbsp;</td>
                        <td align="left">&nbsp;</td>
                    </tr>
                </table>
            </div>
        </asp:Panel>
        <div style="float: right">
            <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
        </div>
        <br />
        <br />
        <div style="color: blue; font-size: 12pt">
            <b>Note:</b>  If your child doesn't have an email address, the child can use parent's email to login and submit the homework.  In this case, Email address & pwd can remain blank.  <%--However to join online class, you must fill in your (or your spouse's) email address above.--%>
        </div>
    </div>

   
    <!-- The Modal -->
    <div id="myModal" class="modal">

        <!-- Modal content -->
        <div class="modal-content">
            <span class="close">&times;</span>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div style="float: left; width: 100px;">
                <span><b>Password:</b></span>
            </div>
            <div style="float: left; width: 150px;" id="dvLable">

                <span id="lblPassoerd" style="font-weight: bold;"></span>
                <input type="text" id="txtPasswordEdit" style="display: none;" />
            </div>

            <div style="float: left; width: 100px;">
                <input type="button" id="BtnEdit" value="Edit" />

            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center><span style="font-weight: bold; color: green;" id="spnSuccess"></span></center>
            <%--   <div id="dvEditSection" style="display: none;">
                <div style="float: left; width: 100px;">
                    <span><b>Password:</b></span>
                </div>
                <div style="float: left; width: 150px;">
                </div>
                <div style="float: left; width: 100px;">
                    <asp:Button ID="BtnSubmitPwd" runat="server" Text="Update" />
                </div>
            </div>--%>
        </div>

    </div>

    <div id="output"></div>

    <div id="overlay" class="web_dialog_overlay"></div>

    <div id="dvUpdateTicket" class="web_dialog">
        <center>
            <div style="margin-top: 50px;">
                <asp:Label ID="lblUpdateOpt" runat="server" ForeColor="#FF6600">This appears to be a duplicate. Please select an option.</asp:Label>
            </div>
        </center>
        <center>
        </center>
    </div>

    <input type="hidden" runat="server" id="hdnPwd" value="" />
    <input type="hidden" runat="server" id="hdnChildNumber" value="" />
</asp:Content>

