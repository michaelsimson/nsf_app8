﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.UI.Page
Imports System.Text.RegularExpressions
Imports System.Data

Partial Class FileAccessMgmt
    Inherits System.Web.UI.Page
    Dim ServerPath As String = "d:\inetpub\wwwroot\northsouth\"
    Dim urlPath As String = "http://www.northsouth.org/"
    Protected returnValue As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            'loadLevel(ddlLevel1, 1, "")
            loadMemberAccess()
            loadroles()
            loadvolunteer()
            'BtnDelete.Attributes.Add("onclick", "return confirm('Are sure you want to delete the file?');")
        End If
        'Response.Write()
    End Sub

    Private Sub loadroles()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select roleID,RoleCode from role where roleid in(43,90,1,2,3,4,5) order by RoleId desc")
        ddlRole.DataSource = ds.Tables(0)
        ddlRole.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRole.Items.Insert(0, New ListItem("Select Role", "0"))
        End If
    End Sub

  
    Private Sub loadMemberAccess()
        'Response.Write("select Top 1 FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID") & " order by level")
        Dim level As Integer
        'If Session("RoleId").ToString() = "1" Then 'Or (Session("RoleId").ToString() = "90")
        '    level = 0
        'Else
        '    level = 13
        'End If
        If (Session("RoleId").ToString() = "90") Or (Session("RoleId").ToString() = "43") Then
            'This query is for roleID 90 and 43
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") > 1 Then
                divAll.Visible = False
                divMulti.Visible = True
                Dim dsV As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select F.FolderListID,F.DisplayName  from VolDocAccess V Inner Join FolderTree F ON V.FolderListID = F.FolderListID Where V.MemberID =" & Session("LoginID") & "")
                ddlselectFolder.DataSource = dsV
                ddlselectFolder.DataBind()
                If dsV.Tables(0).Rows.Count > 0 Then
                    ddlselectFolder.Items.Insert(0, New ListItem("Select Folder", "0"))
                    ddlselectFolder.Items(0).Selected = True
                End If
                lblVErr.Text = "Please select a Folder"
                Exit Sub
            End If
        End If
        Dim SQLQuery As String = ""
        If Session("RoleId").ToString() = "1" Then
            level = 0
        ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Then
            'This query is for roleID 2,3,4,5
            SQLQuery = "select FT.FolderListID,FT.Level,dbo.ufn_getFolderName(FT.FolderListID) as Folders from FolderTree FT Inner Join Chapter C ON FT.FName=C.WebFolderName WHERE C.ChapterID = " & Session("selChapterID")
        Else
            level = 13
            SQLQuery = "select Top 1 FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID")
        End If
        selectlevels(level, SQLQuery)
        divAll.Visible = True

        '** Have to make default location chapter co-ordinators  ************

    End Sub

    Private Sub selectlevels(ByVal level As Integer, ByVal SQLQuery As String)
        Dim folderID As Integer = 0
        Dim selectedDDL As String()
        Dim arrstr As String()
        If SQLQuery.Length > 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            '** Have to make default location chapter co-ordinators  ************
            If ds.Tables(0).Rows.Count > 0 Then
                level = ds.Tables(0).Rows(0)("Level")
                If IsDBNull(ds.Tables(0).Rows(0)("FolderListID")) = False Then
                    folderID = ds.Tables(0).Rows(0)("FolderListID")
                    selectedDDL = ds.Tables(0).Rows(0)("Folders").Split("|")
                End If
            End If
        End If
        Select Case (level)
            Case 1
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                lblLevel.Text = "1"
                lblParentFID.Text = arrstr(0)
            Case 2
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                lblLevel.Text = "2"
                lblParentFID.Text = arrstr(0)
            Case 3
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                lblLevel.Text = "3"
                lblParentFID.Text = arrstr(0)
            Case 4
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                lblLevel.Text = "4"
                lblParentFID.Text = arrstr(0)
            Case 5
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                lblLevel.Text = "5"
                lblParentFID.Text = arrstr(0)
            Case 6
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                lblLevel.Text = "6"
                lblParentFID.Text = arrstr(0)
            Case 7
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                lblLevel.Text = "7"
                lblParentFID.Text = arrstr(0)
            Case 8
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                lblLevel.Text = "8"
                lblParentFID.Text = arrstr(0)
            Case 9
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                lblLevel.Text = "9"
                lblParentFID.Text = arrstr(0)
            Case 10
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                lblLevel.Text = "10"
                lblParentFID.Text = arrstr(0)
            Case 11
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                lblLevel.Text = "11"
                lblParentFID.Text = arrstr(0)
            Case 12
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                ddlLevel12.Items.RemoveAt(ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByValue("0")))
                ddlLevel12.SelectedIndex = ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByText(selectedDDL(11)))
                ddlLevel12.Enabled = False
                arrstr = ddlLevel12.SelectedValue.Split("|")
                lblLevel.Text = "12"
                lblParentFID.Text = arrstr(0)
            Case 0
                loadLevel(ddlLevel1, 1, "")
                lblLevel.Text = "0"
                lblParentFID.Text = "Null"
            Case Else
                Response.Redirect("WebPageMgmtMain.aspx")
        End Select
        constructpath(level)
    End Sub

    Protected Sub ddlLevel1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        If Not ddlLevel1.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "1"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel2, 2, arrstr(0).Trim)
        Else
            lblLevel.Text = "0"
            lblParentFID.Text = "Null"
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel2.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel3, 3, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "1"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel3.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel4, 4, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel4.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel5, 5, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel5.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel6, 6, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel6.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel7, 7, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel7.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel8, 8, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel8_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel8.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel9, 9, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel9_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel9.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel10, 10, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel10_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel10.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel11, 11, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel11.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel11.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel12, 12, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel12_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel12.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel12.SelectedValue.Split("|")
            lblLevel.Text = "12"
            lblParentFID.Text = arrstr(0)
            'loadLevel(ddlLevel12, 12, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel11.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub

    Private Sub loadLevel(ByVal ddl As DropDownList, ByVal level As Integer, ByVal ParentFID As String)

        Dim whereContn As String = ""
        If level > 1 Then
            whereContn = " AND ParentFID=" & ParentFID
        Else
            lblLevel.Text = "0"
            lblParentFID.Text = "Null"
        End If
        Dim strSql As String
        Try
            strSql = "SELECT  Case WHEN Level=1 THEN CONVERT(Varchar,FolderListID) + '|' + FName Else  CONVERT(Varchar,FolderListID) + '|' + FName + '|' + CONVERT(Varchar,ParentFID) END   AS Value, DisplayName FROM FolderTree WHERE Level = " & level & whereContn & " ORDER BY DisplayName"

        Catch ex As Exception
            lblNoAccess.Text = strSql
        End Try
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        ddl.DataSource = ds
        ddl.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            ddl.Items.Insert(0, New ListItem("Select Folder", "0"))
            ddl.Items(0).Selected = True
            ddl.Enabled = True
        Else
            ddl.Enabled = False
        End If
    End Sub

    Private Sub constructpath(ByVal level As String)
        lblError.Text = ""
        Select Case level
            Case "0"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                DisableDdl(1)
            Case "1"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                DisableDdl(2)
            Case "2"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                DisableDdl(3)
            Case "3"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                DisableDdl(4)
            Case "4"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                DisableDdl(5)
            Case "5"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                DisableDdl(6)
            Case "6"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                DisableDdl(7)
            Case "7"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                DisableDdl(8)
            Case "8"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                DisableDdl(9)
            Case "9"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                DisableDdl(10)
            Case "10"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                DisableDdl(11)
            Case "11"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                setPath(ddlLevel11)
                DisableDdl(12)
            Case "12"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                setPath(ddlLevel11)
                setPath(ddlLevel12)
                'DisableDdl(level)
        End Select
        loadfiles()
    End Sub
    Private Sub DisableDdl(ByVal level As String)
        Select Case level
            Case "11"
                ddldisable(ddlLevel12)
            Case "10"
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "9"
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "8"
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "7"
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "6"
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "5"
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "4"
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "3"
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "2"
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "1"
                ddldisable(ddlLevel2)
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
        End Select
    End Sub
    Private Sub ddldisable(ByVal ddl As DropDownList)
        ddl.Items.Clear()
        ddl.Enabled = False
    End Sub
    Private Sub setPath(ByVal ddl As DropDownList)
        Dim arrstr As String() = ddl.SelectedValue.Split("|")
        lblPath.Text = lblPath.Text & arrstr(1).Trim & "\"
        lblURL.Text = lblURL.Text & arrstr(1).Trim & "/"
    End Sub

    Private Sub clearall()
        trfilelist.Visible = True
        ddlFiles.Items.Clear()
        lblError.Text = ""
        lblPath.Text = ServerPath
        lblURL.Text = urlPath
        loadLevel(ddlLevel1, 1, "")
        DisableDdl("1")
        ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue("0"))
        loadMemberAccess()
        ' lblRenameFolder.Text = String.Empty
        'lblRenamePath.Text = String.Empty
    End Sub

    Protected Sub BtnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clearall()
    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        loadfiles()
    End Sub
    Private Sub loadfiles()
        ''Try
        ''    Dim strFileSize As String = ""
        ''    'Dim di As New IO.DirectoryInfo(Server.MapPath(TextBox1.Text))
        ''    Dim di As New IO.DirectoryInfo(lblPath.Text)
        ''    Dim aryFi As IO.FileInfo() = di.GetFiles()
        ''    Dim fi As IO.FileInfo
        ''    Dim flag As Boolean = False
        ''    ddlFiles.Items.Clear()
        ''    For Each fi In aryFi
        ''        flag = True
        ''        ddlFiles.Items.Add(fi.Name)
        ''    Next
        ''    If flag = True Then
        ''        ddlFiles.Items.Insert(0, New ListItem("Select File", "0"))
        ''    Else
        ''        lblError.Text = "No file found"
        ''    End If
        ''Catch ex As Exception
        ''    lblError.Text = ex.ToString()
        ''End Try
        Try
            Dim SQLQuery As String
            If Session("RoleId") = "1" Then
                SQLQuery = " Select FileListID,FileName  from FileList  Where FolderListID = " & lblParentFID.Text
            Else
                SQLQuery = " Select FileListID,FileName  from FileList  Where FileListID Not in (select FileListID from RestFileAccess) AND FolderListID = " & lblParentFID.Text & " Union All select FL.FileListID,FL.FileName from FileList FL INNER JOIN RestFileAccess RF ON RF.FilelistID = FL.FileListID  Where Rf.RoleID = " & Session("RoleId") & " AND FL.FolderListID =" & lblParentFID.Text
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            '** Have to make default location chapter co-ordinators  ************
            ddlFiles.DataSource = ds.Tables(0)
            ddlFiles.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlFiles.Items.Insert(0, New ListItem("Select File", "0"))
            Else
                lblError.Text = "No file found"
            End If
        Catch ex As Exception
            lblError.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub BtnAddnew_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlFiles.SelectedValue = "0" And Not ddlRole.SelectedValue = "0" And ddlFiles.Items.Count > 0 Then
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from RestFileAccess where FileName='" & ddlFiles.SelectedItem.Text & "' AND FolderListID=" & lblParentFID.Text & " AND RoleID=" & ddlRole.SelectedValue & "") < 1 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into RestFileAccess(FolderListID,FileName,FileListID, RoleID, CreateDate, CreatedBy) Values (" & lblParentFID.Text & ",'" & ddlFiles.SelectedItem.Text & "'," & ddlFiles.SelectedValue & "," & ddlRole.SelectedValue & ",Getdate()," & Session("LoginID") & ")")
                clearall()
                loadvolunteer()
                lblError.Text = "Updated Successfully"
            Else
                lblError.Text = "This record already exist"
            End If
        Else
            lblError.Text = "please select file and Role"
        End If
    End Sub


    Private Sub loadvolunteer()
        'dbo.ufn_getFolderURL dunction is used here
        Dim strsql As String = "SELECT  R.RestFAID, R.FileName,dbo.ufn_getFolderURL(R.FolderListID) as FolderURL,Rol.RoleCode,R.RoleId, R.FolderListID FROM RestFileAccess  R Inner Join Role Rol ON Rol.RoleID = R.RoleID"
        Dim ds As DataSet
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strsql)
            'select V.VolDocAccessID,I.FirstName + ' ' + I.LastName as VName,V.Level,CASE WHEN V.FolderListID IS NUll THEN NULL ELSE dbo.ufn_getFolderURL(V.FolderListID) END as FolderURL,V.AccessType,V.IPAddress from voldocaccess V inner join IndSpouse I ON V.MemberID = I.AutoMemberID")
            Dim dt As DataTable = ds.Tables(0)
            If ds.Tables(0).Rows.Count > 0 Then
                lblTable11.Visible = True
                DGVolunteer.DataSource = dt
                DGVolunteer.DataBind()
                Session("volDataSet") = ds
            Else
                DGVolunteer.DataSource = Nothing
                DGVolunteer.DataBind()
                lblTable11.Visible = False
                lblError.Text = "No Restricted File exist in the table"
            End If
        Catch ex As Exception
            Response.Write(ex.ToString())
            Response.Write(strsql)
        End Try

    End Sub

    Protected Sub DGVolunteer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        Dim volDocAccessID As Integer = DGVolunteer.DataKeys(e.Item.ItemIndex)
        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM RestFileAccess WHERE  RestFAID =" & volDocAccessID & "")
                loadvolunteer()
            Catch ex As Exception
                lblError.Text = ex.Message
                lblError.Text = (lblError.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub DGVolunteer_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGVolunteer.EditCommand
        DGVolunteer.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = CType(Session("volDataSet"), DataSet)
        Dim page As Integer = DGVolunteer.CurrentPageIndex
        Dim pageSize As Integer = DGVolunteer.PageSize
        Dim currentRowIndex As Integer

        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr
        loadvolunteer()
    End Sub

    Protected Sub DGVolunteer_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGVolunteer.ItemDataBound
        '    ferdine
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            btn = CType(e.Item.Cells(0).Controls(0), LinkButton)
            btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete Restricted File Access of " & e.Item.DataItem("FileName") & " record?');")
        End If

    End Sub

    Public Sub ddlRole_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim RoleID As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("RoleID") Is DBNull.Value) Then
                RoleID = dr.Item("RoleID")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select roleID,RoleCode from role where roleid in(43,90,1,2,3,4,5) order by RoleId desc")
            ddlTemp.DataSource = ds.Tables(0)
            ddlTemp.DataBind()
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(RoleID))
        Else
            Return
        End If
    End Sub

    Protected Sub DGVolunteer_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGVolunteer.UpdateCommand
        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim VolDocAccessID As Integer
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim FolderListID, Filename As String
        If (Not dr Is Nothing) Then
            Filename = dr.Item("FileName")
            FolderListID = dr.Item("FolderListID")
        End If
        Dim roleid As String = CStr(CType(e.Item.FindControl("ddlRole"), DropDownList).SelectedItem.Value)
        Try
            VolDocAccessID = DGVolunteer.DataKeys(e.Item.ItemIndex)
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from RestFileAccess where FileName='" & Filename & "' AND FolderListID=" & FolderListID & " AND RoleID=" & roleid & " and  RestFAID not in (" & VolDocAccessID & ")") > 0 Then
                lblError.Text = "This record already Exist"
                Exit Sub
            End If

            sqlStr = "UPDATE RestFileAccess SET RoleID = " & roleid & " WHERE  RestFAID=" & VolDocAccessID
            SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
            lblError.Text = "Updated Successfully"
        Catch ex As SqlException
            'ex.message
            lblError.Text = sqlStr & "<br> Error:updating the record" + ex.ToString
            Return
        End Try
        DGVolunteer.EditItemIndex = -1
        loadvolunteer()
    End Sub

    Protected Sub DGVolunteer_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles DGVolunteer.CancelCommand
        Dim ds As DataSet = CType(Session("volDataSet"), DataSet)
        If (Not ds Is Nothing) Then
            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGVolunteer.CurrentPageIndex
            Dim pageSize As Integer = DGVolunteer.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            lblError.Text = ""
            'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DGVolunteer.EditItemIndex = -1
            loadvolunteer()
        Else
            Return
        End If
    End Sub
    Protected Sub DGVolunteer_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGVolunteer.PageIndexChanged
        DGVolunteer.CurrentPageIndex = e.NewPageIndex
        DGVolunteer.EditItemIndex = -1
        loadvolunteer()
    End Sub

End Class

