﻿

Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
' Created by :Ferdine

Partial Class Email_NFInvites
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'User code to initialize the page here
        'only Roleid = 1 or 2, Can access this page
        If Not Page.IsPostBack Then
            Session("EmailSource") = "Email_NFInvites"
            Session("Productcode") = ""
            Dim roleid As Integer = Session("RoleID")
            If (roleid = 1) Or (roleid = 2) Then

                ' Load Year into ListBox
                Dim year As Integer = 0
                year = Convert.ToInt32(DateTime.Now.Year)

                lstyear.Items.Insert(0, Convert.ToString(year + 1))
                lstyear.Items.Insert(1, DateTime.Now.Year.ToString())
                lstyear.Items.Insert(2, Convert.ToString(year - 1))
                lstyear.Items.Insert(3, Convert.ToString(year - 2))
                lstyear.Items.Insert(4, Convert.ToString(year - 3))
                lstyear.Items.Insert(5, Convert.ToString(year - 4))
                lstyear.Items(1).Selected = True
                Session("Year") = lstyear.Items(1).Text

                LoadProductGroup()   ' method to  Load Product group ListBox
                LoadProductID()      ' method to  Load Product ID ListBox
            Else
                lblerr.Visible = True
                tabletarget.Visible = False
            End If
        End If
        Session("emaillist") = ""
    End Sub

    Private Sub LoadProductGroup()
        Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupId = C.ProductGroupId and C.ContestYear in (" & Session("Year") & ") and c.NationalFinalsStatus = 'Active' and P.EventId=1  order by P.ProductGroupID"
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductGroup.DataSource = drproductgroup
        lstProductGroup.DataBind()
        lstProductGroup.Items.Insert(0, "All")
        lstProductGroup.Items(0).Selected = True
    End Sub

    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim productgroup As String = ""
        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            lstProductid.Enabled = False
            Dim sbvalues As New StringBuilder
            Dim strSql1 As String = "Select Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupId = C.ProductGroupId and C.ContestYear in (" & Session("Year") & ") and c.NationalFinalsStatus = 'Active' and P.EventId=1  order by P.ProductGroupID"
            'SELECT   P.ProductGroupID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupId = C.ProductGroupId and C.ContestYear in (" & Session("Year") & ") and c.NationalFinalsStatus = 'Active' and P.EventId=1  order by P.ProductGroupID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql1)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If

        Dim strSql As String = "Select ProductCode, Name from Product where EventID in (1) and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
        Dim drproductid As SqlDataReader
        drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        lstProductid.DataSource = drproductid
        lstProductid.DataBind()
        lstProductid.Items.Insert(0, "All")
        lstProductid.Items(0).Selected = True
    End Sub

    Protected Sub btnselectmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnselectmail.Click
        ' Continue to Email button
        ' will select emails depending on selections made
        ' tableemail.Visible = True


        Dim dsEmails As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = ""
        ' Dim chapterid As String = ""

        If lstProductGroup.Items(0).Selected = True Then  ' if selected item in Productgroup is ALL
            Dim sbvalues As New StringBuilder
            strSql = "SELECT  Distinct P.ProductGroupID as ID, P.Name from ProductGroup P, contestcategory C  where P.ProductGroupId = C.ProductGroupId and C.ContestYear in (" & Session("Year") & ") and c.NationalFinalsStatus = 'Active' and P.EventId=1  order by P.ProductGroupID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("ID").ToString)
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("ProductGroupId") = sbvalues.ToString
            End If
        End If
        If lstProductid.Items(0).Selected = True And Session("Productcode") = "" Then  ' if selected item in ProductId is ALL
            Dim sbvalues As New StringBuilder
            strSql = "Select ProductCode as code, Name from Product where EventID in (1) and Status='O' and ProductGroupID In (" & Session("ProductGroupId") & ") order by ProductID"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
            If ds.Tables.Count > 0 Then
                Dim i As Integer
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    sbvalues.Append("'")
                    sbvalues.Append(ds.Tables(0).Rows(i).Item("code").ToString)
                    sbvalues.Append("'")
                    If i < ds.Tables(0).Rows.Count - 1 Then
                        sbvalues.Append(",")
                    End If
                Next
                Session("Productcode") = sbvalues.ToString
            End If
        End If
        Dim StrSQL1 As String
        Try
            If drpregistrationtype.SelectedItem.Value = 1 Then   'All invitees
                StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productcode") & ")  and a.nationalinvitee=1 and invite_decline_comments is null and a.EventID in (2))) or (donortype = 'SPOUSE' and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productcode") & ")  and a.nationalinvitee=1 and invite_decline_comments is null and a.EventID in (2)))) group by Email"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            ElseIf drpregistrationtype.SelectedItem.Value = 2 Then 'Paid Parent's Emails
                StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productcode") & "))) or (donortype = 'SPOUSE' and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is not null and a.EventID in (1) and a.Productcode in (" & Session("Productcode") & ")))) group by Email"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            ElseIf drpregistrationtype.SelectedItem.Value = 3 Then  'Pending Parent's Emails
                StrSQL1 = "SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (select a.parentid from contestant a where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productcode") & "))) or (donortype = 'SPOUSE' and relationship in (select a.parentid from contestant a  where a.contestyear in (" & Session("Year") & ") and a.PaymentReference is null and a.EventID in (1) and a.Productcode in (" & Session("Productcode") & ")))) group by Email"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            ElseIf drpregistrationtype.SelectedItem.Value = 4 Then  'invitees less Paid
                StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productcode") & ") and a.nationalinvitee=1 and invite_decline_comments is null and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productcode") & ") and eventid=1 and paymentreference is not null);SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            ElseIf drpregistrationtype.SelectedItem.Value = 5 Then  'invitees less Paid less Pending
                StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productcode") & ") and a.nationalinvitee=1 and invite_decline_comments is null and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productcode") & ") and eventid=1);SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            ElseIf drpregistrationtype.SelectedItem.Value = 6 Then  'Photos Not yet sent
                'StrSQL1 = "select a.parentid into #TmpMemberIds from contestant a where a.contestyear in (" & Session("Year") & ") and a.Productcode in (" & Session("Productcode") & ") and a.nationalinvitee=1 and invite_decline_comments is null and a.ParentID not in (select ParentID from contestant where contestyear in (" & Session("Year") & ") and Productcode in (" & Session("Productcode") & ") and eventid=1);SELECT distinct(Email) as EmailID From IndSpouse where email<>'' and ((donortype = 'IND' and automemberid in (select parentid from #TmpMemberIds)) or (donortype = 'SPOUSE' and relationship in (select parentid from #TmpMemberIds))) group by Email;Drop  table #TmpMemberIds"
                StrSQL1 = "SELECT distinct(a.Email) as EmailID From IndSpouse a where email<>'' and email is not null and ((donortype = 'IND' and automemberid in (Select ParentID from Contestant where ContestYear in (" & Session("Year") & ") and EventId = 1 and PaymentReference is not null and photo_image is null and Productcode in (" & Session("Productcode") & "))) or (donortype = 'SPOUSE' and relationship in (Select ParentID from Contestant where ContestYear in (" & Session("Year") & ") and EventId = 1 and PaymentReference is not null and photo_image is null and Productcode in (" & Session("Productcode") & "))))"
                dsEmails = SqlHelper.ExecuteDataset(conn, CommandType.Text, StrSQL1)
            End If
            '**
            'lblerr.Text = StrSQL1
            'lblerr.Visible = True
            'Exit Sub
            ' depending on selections  emails list will be selected
            'Dim tblEmails() As String = {"EmailContacts"}
            If dsEmails.Tables.Count > 0 Then
                Dim sbEmailList As New StringBuilder
                If dsEmails.Tables(0).Rows.Count > 0 Then
                    Dim ctr As Integer
                    For ctr = 0 To dsEmails.Tables(0).Rows.Count - 1
                        sbEmailList.Append(dsEmails.Tables(0).Rows(ctr).Item("EmailID").ToString)
                        If ctr <= dsEmails.Tables(0).Rows.Count - 2 Then
                            sbEmailList.Append(",")
                        End If
                    Next
                    Session("emaillist") = sbEmailList.ToString
                End If
                Session("sentemaillistenable") = "No"
                Response.Redirect("emaillist.aspx")
            Else
                lblerr.Text = "No Valid Email Selection Made"
                lblerr.Visible = True
            End If
        Catch ex As Exception
            lblerr.Text = StrSQL1
            lblerr.Text = lblerr.Text & ex.ToString()
            lblerr.Visible = True
        End Try
    End Sub

    Protected Sub drpevent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Event from Registered Parents
        LoadProductGroup()
    End Sub

    Protected Sub lstProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product Group from Registered Parents
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstProductGroup.Items.Count - 1
            If lstProductGroup.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductGroup.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("ProductGroupId") = sbvalues.ToString
        End If
        lstProductid.Enabled = True
        Session("Productcode") = ""
        LoadProductID()
    End Sub

    Protected Sub lstProductid_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in Product ID from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 1 To lstProductid.Items.Count - 1
            If lstProductid.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstProductid.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append("'")
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                sbvalues.Append("'")
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Productcode") = sbvalues.ToString
        End If
        ' End If
    End Sub

    Protected Sub lstyear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'on selection change in year from Registered Parents
        Dim i As Integer
        Dim ds As New DataSet
        Dim dt As New DataTable("selected")
        Dim dc As DataColumn = New DataColumn("Values1")
        dc.DataType = System.Type.GetType("System.String")
        dt.Columns.Add(dc)
        Dim dr As DataRow
        For i = 0 To lstyear.Items.Count - 1
            If lstyear.Items(i).Selected Then
                dr = dt.NewRow()
                dr("Values1") = lstyear.Items(i).Value
                dt.Rows.Add(dr)
            End If
        Next
        ds.Tables.Add(dt)
        ds.AcceptChanges()

        If ds.Tables.Count > 0 Then
            Dim sbvalues As New StringBuilder
            Dim a As Integer
            For a = 0 To ds.Tables(0).Rows.Count - 1
                sbvalues.Append(ds.Tables(0).Rows(a).Item("Values1").ToString)
                If a < ds.Tables(0).Rows.Count - 1 Then
                    sbvalues.Append(",")
                End If
            Next
            Dim st As String = sbvalues.ToString
            Session("Year") = sbvalues.ToString
        End If
    End Sub

End Class

