﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="IndiaLogin.aspx.cs" Inherits="IndiaLogin"  MasterPageFile="~/NSFMasterPage.master"%>

<%--<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="IndiaLogin.aspx.cs" Inherits="IndiaLogin" %>--%>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
        <link href="Content/bootstrap.css" rel="stylesheet" type="text/css" />
    <!-- Custom CSS -->
    <link href="Content/sb-admin.css" rel="stylesheet" type="text/css" />
    <!-- Morris Charts CSS -->
    <link href="Content/plugins/morris.css" rel="stylesheet" type="text/css" />
    <!-- Custom Fonts -->
    <link href="Content/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />

    <link rel="stylesheet" href="Content/JGrid/jqx.base.css" type="text/css" />
    <link rel="stylesheet" href="Content/JGrid/jqx.bootstrap.css" type="text/css" />
    <script src="Content/JGrid/jqx-all.js" type="text/javascript"></script>
     
    <table width="100%"  >
            <tr>
                <td class="title02" valign="top" nowrap align="center">
                    Welcome to North South Foundation <br />
                            <asp:Label ID="lblTitle" runat="server"></asp:Label> LOGIN PAGE <br>
                   
                </td>
            </tr>
        <tr>
            <td>  
                <div class="col-md-3"></div>
                <div class="col-md-6">
                 <div class="panel panel-green">
                        <div class="panel-heading">
                            <h3 class="panel-title">
                                User Login</h3>
                        </div>
                        <div class="panel-body">
                            <div class="form-group">
                                <label>
                                    Username</label>
                                <asp:TextBox class="form-control" placeholder="Username" id="txtUserID" runat="server" />
                            </div>
                            <div class="form-group">
                                <label>
                                    Password</label>
                                <asp:TextBox  TextMode ="Password" class="form-control" placeholder="Password" id="txtPWD"  runat="server" />
                            </div>
                             <center>
                          <asp:Button class="btn btn-success" id="btnLogin" Text="Login !"  runat="server" OnClick="btnLogin_Click"/> </center>
                            <center><asp:Label ID="lblErr" runat="server" ForeColor="red"></asp:Label></center>
                            <div class="text-right">
                                <a href="forgot.aspx">Forgot Password<i class="fa fa-arrow-circle-right"></i></a>
                               <%-- <a href="ChangePassword.aspx">Change Password<i class="fa fa-arrow-circle-right"></i></a>--%>
                            </div>
                           
                        </div>
                     </div>
                    
                    </div>
            </td>
        </tr>
        </table>
</asp:Content>

