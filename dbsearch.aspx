<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="dbsearch.aspx.vb" Inherits="dbsearch" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
          <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
            <tr bgcolor="#FFFFFF" >
                <td class="Heading" colspan="4" align="center" >Search Records</td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td colspan="4" >
                    <asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Main Menu</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                    <asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="~/Registration.aspx?Type=Individual">Add an Individual</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                    <asp:hyperlink id="Hyperlink2" runat="server" NavigateUrl="~/AddOrganization.aspx">Add an Organization</asp:hyperlink>&nbsp;&nbsp;&nbsp;
                </td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            
            <tr bgcolor="#FFFFFF" >
               <td  colspan="4" align="center"><asp:Label ID="Label1" runat="server" Text="Search by one or more attributes given below" ></asp:Label></td>
            </tr>
            <tr>
                <td>&nbsp;</td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    E-Mail:</td>
                <td >
                    <asp:TextBox ID="tbEmail" runat="server" CssClass="inputBox"> </asp:TextBox>
                    </td>
                <td >
                    First Name/Organization:</td>
                <td >
                    <asp:TextBox ID="tbFnameOrg" runat="server" CssClass="inputBox"></asp:TextBox>
                    </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Last Name:</td>
                <td >
                    <asp:TextBox ID="tbLname" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    Street:</td>
                <td >
                    <asp:TextBox ID="tbStreet" runat="server" CssClass="inputBox"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    City:</td>
                <td >
                    <asp:TextBox ID="tbCity" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    State:</td>
                <td >
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="inputBox">
                    </asp:DropDownList>
                 </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Zip Code:</td>
                <td >
                    <asp:TextBox ID="tbZip" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    Home Phone:</td>
                <td >
                    <asp:TextBox ID="tbPhone" runat="server" CssClass="inputBox"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Referred by:</td>
                <td >
                    <asp:TextBox ID="tbRef" runat="server" CssClass="inputBox"></asp:TextBox></td>
                <td >
                    Liaison Person:
                </td>
                <td >
                    <asp:TextBox ID="tbLiaison" runat="server" CssClass="inputBox"></asp:TextBox></td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    NSF Chapter</td>
                <td >
                    <asp:DropDownList ID="ddlChapter" runat="server" CssClass="inputBox">
                    </asp:DropDownList></td>
                <td >
                </td>
                <td >
                </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td  align="center" colspan="4"> 
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="FormButton" />
                </td>
            </tr>
        </table>
        <br />
        
    </div>
</asp:Content>



 
 
 