﻿
var roleID;
var accepted = "";
var coachname = "";
var parentName = "";
var parentEmail = "";
var source = "";
var email = "";
var entryCode = "";

$(function (e) {


    var strContentHtml = "";

    //$(document).on("mouseover", ".lnkCoachName", function (e) {
    //    alert("");
    //});

    $(".lnkCoachName").qtip({ // Grab some elements to apply the tooltip to
        content: {

            text: function (event, api) {
                var dvHtml = "";
                var coachId = $(this).attr("attr-coachid");
                accepted = $(this).attr("attr-Accepted");

                coachname = $(this).attr("attr-name");
                email = $(this).attr("attr-email");
                var productGroupId = $(this).attr("attr-ProductGroupID");

                var productId = $(this).attr("attr-productID");
                var sessionNo = $(this).attr("attr-SessionNo");
                var semester = $(this).attr("attr-Semester");
                var level = $(this).attr("attr-level");
                var year = $(this).attr("attr-year"); 
                if (accepted == "Y") {

                    var url = "";
                    if (source == "CC") {
                        url = "../";
                    } else {
                        url = "";
                    }

                    var dvHtml = "";
                    dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Calendar signup</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CoachClassCalNew.aspx?coachID=' + $(this).attr("attr-coachid") + '&Semester=' + $(this).attr("attr-semester") + '&SessionNo=' + $(this).attr("attr-SessionNo") + '&pId=' + $(this).attr("attr-productID") + '&pgId=' + $(this).attr("attr-ProductGroupID") + '&SNo=' + sessionNo + '&level=' + level + '" target="_blank">Set up Class Calendar</a></div>';
                    if (roleID == "88") {
                    } else {
                        dvHtml += ' <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a  attr-coachId=' + $(this).attr("attr-coachid") + ' style="text-decoration:none; color:blue; cursor:pointer;" class = "ancCoachInfo">Coach Contact Information</a> </div>';
                    }

                    dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'ViewContactList.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '&Semester=' + $(this).attr("attr-semester") + '&SessionNo=' + $(this).attr("attr-SessionNo") + '&pId=' + $(this).attr("attr-productID") + '&pgId=' + $(this).attr("attr-ProductGroupID") + '&level=' + level + '&year=' + year + '" target="_blank" >Contact List of Students</a> </div>';

                    dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'ViewSATTestScores.aspx?coachflag=2&coachID=' + $(this).attr("attr-coachid") + '&Semester=' + $(this).attr("attr-semester") + '&SessionNo=' + $(this).attr("attr-SessionNo") + '&pId=' + $(this).attr("attr-productID") + '&pgId=' + $(this).attr("attr-ProductGroupID") + '&level=' + level + '" target="_blank" >View Test Scores</a> </div>';

                    if (roleID != "88") {
                        dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a class="ancOpenEmail" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" >Send Email</a> </div>';

                        dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a class="ancSendEmailParents" style="text-decoration:none; color:blue; cursor:pointer;" href="' + url + 'EmailCoach.aspx?Semester=' + semester + '&PGID=' + productGroupId + '&PID=' + productId + '&SNo=' + sessionNo + '&CoachID=' + coachId + '&level=' + level + '" target="_blank" >Send Email to Parents and Students</a> </div>';

                    }

                    //return dvHtml;

                } else {
                    if (roleID == "88") {
                        dvHtml = "";
                    } else {
                        dvHtml = '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="CalendarSignup.aspx?coachID=' + $(this).attr("attr-coachid") + '" target="_blank">Coach Calendar signup</a></div><div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a style="text-decoration:none; color:blue;cursor:pointer;" attr-coachId=' + $(this).attr("attr-coachid") + ' class="ancCoachInfo" target="_blank">Coach Contact Information</a> </div>';
                    }
                }
                var jsonData = { MemberId: coachId };
                return $.ajax({
                    type: "POST",
                    url: "SetupZoomSessions.aspx/GetCoachSessionInfo",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify(jsonData),
                    async: true,
                    cache: false,
                    success: function (data) {
                        var ProductCode = "";
                        var sessionKey = "";
                        var sessionNo = "";

                        $.each(data.d, function (index, value) {

                            dvHtml += '<div style="clear:both; margin-bottom:5px;"></div>';

                            dvHtml += '<div style="float:left;"><span style="font-weight:bold; font-size:15px;">Product:</span>  <span style="font-size:15px;">' + value.ProductName + '</span> , <span style="font-weight:bold; font-size:15px;">Session#:</span>  <span style="font-size:15px;">' + value.SessionNo + '</span> , <span style="font-weight:bold; font-size:15px;">SessionKey:</span>  <span style="font-size:15px;">' + value.SessionKey + '</span></div>';

                        });


                    }
                }).then(function (data) {

                    return dvHtml;
                });


            },

            title: function (event, api) {

                return '<span style="font-weight:bold; font-size:14px;">' + coachname + ' </span>';

            },
            button: 'Close'
        },
        hide: {
            event: false
        },
        style: {
            classes: 'qtip-green qtip-shadow qTipWidthCustom'
        },
        show: {
            solo: true
        }
    })

    if (roleID != "88") {


        $(".lnkParentName").qtip({ // Grab some elements to apply the tooltip to
            content: {

                text: function (event, api) {

                    var joinUrl = "https://northsouth.zoom.us/j/";
                    var sessionKey = $(this).attr("atr-SessionKey");

                    joinUrl = joinUrl + sessionKey;
                    email = $(this).attr("attr-email");

                    return '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + $(this).attr("attr-parentId") + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a  style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx?memberID=' + $(this).attr("attr-parentId") + '" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx?memberID=' + $(this).attr("attr-parentId") + '">Change Coach</a> </div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; font-weight:bold;">Join Link: ' + joinUrl + '</a> </div> <div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; font-weight:bold;"><a class="ancOpenEmail" style="color:blue; text-decoration:none; cursor:pointer;" >Send Email</a> </div>';
                },

                title: function (event, api) {
                    parentName = $(this).attr("attr-name");
                    return '<span style="font-weight:bold; font-size:14px;">' + parentName + '</span>';
                },

                button: 'Close'
            },
            hide: {
                event: false
            },
            style: {
                classes: 'qtip-green qtip-shadow'
            },
            show: {
                solo: true
            }
        })


    }

    //Used the below function for ExCoachng Send Email
    $(".lnkParentNameEx").qtip({ // Grab some elements to apply the tooltip to
        content: {

            text: function (event, api) {


                email = $(this).attr("attr-email");

                return '  <div style="font-size:14px; margin-bottom:5px;"><a  style="text-decoration:none; font-weight:bold;"><a class="ancOpenEmail" style="color:blue; text-decoration:none; cursor:pointer;" >Send Email</a> </div>';
            },

            title: function (event, api) {
                parentName = $(this).attr("attr-name");
                return '<span style="font-weight:bold; font-size:14px;">' + parentName + '</span>';
            },

            button: 'Close'
        },
        hide: {
            event: false
        },
        style: {
            classes: 'qtip-green qtip-shadow'
        },
        show: {
            solo: true
        }
    })

    $(".lnkSupport").qtip({ // Grab some elements to apply the tooltip to
        content: {


            text: function (event, api) {
                var memberID = $(this).attr("attr-UmemberID");
                entryCode = $(this).attr("attr-entryCode");
                email = $(this).attr("attr-email");
                var sRoleID = "";

                var dvHtml = "";
                var url = "";
                if (source == "CC") {
                    url = "../";
                } else {
                    url = "";
                }
                var isCoach = "0";
                var isParent = "0";
                var isBoth = "0";
                $.ajax({

                    type: "POST",
                    url: "SupportTracking.aspx/ValidateTicketOpener",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    data: JSON.stringify({ MemberID: memberID }),
                    async: true,
                    cache: false,
                    success: function (data) {


                    }
                }).then(function (data) {

                    $.each(data.d, function (index, value) {

                        isCoach = value.IsCoach;
                        isParent = value.IsParent;
                    });

                    if (isCoach != "0" && isParent != "0") {
                        isBoth = "1";
                    }

                    if (isParent != "0" && isCoach == "0") {

                        dvHtml += '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + memberID + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a  style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx?memberID=' + memberID + '" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx?memberID=' + memberID + '">Change Coach</a> </div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; font-weight:bold;"><a class="ancOpenEmail" style="color:blue; text-decoration:none; cursor:pointer;" >Send Email</a> </div>';

                    } else if (isCoach != "0" && isParent == "0") {

                        if (sRoleID == "88") {
                            dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CalendarSignup.aspx?coachID=' + memberID + '" target="_blank">Coach Calendar signup</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CoachClassCalNew.aspx?coachID=' + memberID + '&SessionNo=' + $(this).attr("attr-SessionNo") + '&pId=' + $(this).attr("attr-productID") + '&pgId=' + $(this).attr("attr-ProductGroupID") + '&SNo=' + sessionNo + '&level=' + level + '" target="_blank">Set up Class Calendar</a></div>';

                            dvHtml += ' <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a  attr-coachId=' + memberID + ' style="text-decoration:none; color:blue; cursor:pointer;" class = "ancCoachInfo">Coach Contact Information</a> </div>';

                            dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'ViewContactList.aspx?coachflag=2&coachID=' + memberID + '" target="_blank" >Contact List of Students</a> </div>';
                            if (roleID != "88") {
                                dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a class="ancOpenEmail" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" >Send Email</a> </div>';

                            }
                        }
                    } else if (isBoth != "0") {

                        dvHtml += "<div style='font-weight:bold; text-align:center;'>----------Coach----------</div>";
                        dvHtml += "<div style='clear:both;margin-bottom:5px;'></div>";
                        dvHtml += '<div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CalendarSignup.aspx?coachID=' + memberID + '" target="_blank">Coach Calendar signup</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'CoachClassCalNew.aspx?coachID=' + memberID + '&SessionNo=' + $(this).attr("attr-SessionNo") + '&pId=' + $(this).attr("attr-productID") + '&pgId=' + $(this).attr("attr-ProductGroupID") + '&SNo=' + sessionNo + '&level=' + level + '" target="_blank">Set up Class Calendar</a></div>';

                        dvHtml += ' <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a  attr-coachId=' + memberID + ' style="text-decoration:none; color:blue; cursor:pointer;" class = "ancCoachInfo">Coach Contact Information</a> </div>';

                        dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a style="text-decoration:none; color:blue;" href="' + url + 'ViewContactList.aspx?coachflag=2&coachID=' + memberID + '" target="_blank" >Contact List of Students</a> </div>';
                        if (roleID != "88") {
                            dvHtml += '<div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a class="ancOpenEmail" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" >Send Email</a> </div>';

                        }

                        dvHtml += "<div style='clear:both;margin-bottom:10px;'></div>";

                        dvHtml += "<div style='font-weight:bold; text-align:center;'>----------Parent----------</div>";
                        dvHtml += "<div style='clear:both;margin-bottom:5px;'></div>";

                        dvHtml += '<div style="font-size:14px;"><a class="ancParentInfo" ezmodal-target="#demo" style="text-decoration:none; color:blue; cursor:pointer;" target="_blank" class="parentInfo" attr-parentID=' + memberID + '>Parent Contact information</a></div> <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px;"><a  style="text-decoration:none; color:blue;" href="CoachingRegistrationUpdate.aspx?memberID=' + memberID + '" target="_blank">Registration Update</a></div>  <div style="clear:both; margin-bottom:5px;"></div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; color:blue;" href="Admin/ChangeCoaching.aspx?memberID=' + memberID + '">Change Coach</a> </div><div style="font-size:14px; margin-bottom:5px;"><a target="_blank" style="text-decoration:none; font-weight:bold;"><a class="ancOpenEmail" style="color:blue; text-decoration:none; cursor:pointer;" >Send Email</a> </div>';

                    } else {

                        dvHtml += "<div style='font-weight:bold; text-align:center;'>No contents</div>";
                    }


                    api.set('content.text', dvHtml);
                });


                return "Loading..";
            },




            title: function (event, api) {
                parentName = $(this).attr("attr-name");
                return '<span style="font-weight:bold; font-size:14px;">' + parentName + '</span>';
            },
            button: 'Close',
        },


        hide: {
            event: false
        },
        style: {
            classes: 'qtip-green qtip-shadow'
        },
        show: {
            solo: true
        }

    });





});


$(document).on("click", ".ancParentInfo", function (e) {

    $(".dvSendEMailContent").hide();
    $(".dvgridContent").show();
    $(".btnSenEmail").hide();
    $(".spnMemberTitle").text("Parent Information");
    $(".tblCoacnInfo").hide();
    $(".tblParentInfo").show();
    $(".btnPopUP").trigger("click");
});

$(document).on("mouseover", ".ancParentInfo", function (e) {
    $(".dvSendEMailContent").hide();
    $(".dvgridContent").show();
    $(".btnSenEmail").hide();
    $(".spnMemberTitle").text("Parent Information");
    getParentInfo($(this).attr("attr-parentID"));
});

$(document).on("click", ".ancCoachInfo", function (e) {
    $(".spnMemberTitle").text("Coach Information");
    $(".dvSendEMailContent").hide();
    $(".btnSenEmail").hide();
    $(".dvgridContent").show();
    $(".tblCoacnInfo").show();
    $(".tblParentInfo").hide();
    $(".btnPopUP").trigger("click");
});

$(document).on("mouseover", ".ancCoachInfo", function (e) {

    getCoachInfo($(this).attr("attr-coachid"));
});

$(document).on("click", ".ancOpenEmail", function (e) {
    $(".spnMemberTitle").text("Send Email");
    $(".dvSendEMailContent").show();
    $(".btnSenEmail").show();
    $(".dvgridContent").hide();

    $(".txtTo").val(email)
    $(".btnPopUP").trigger("click");

});
$(document).on("click", ".btnSenEmail", function (e) {

    senEmailToCoachAndparents();
});


function getParentInfo(pMemberID) {
    $(".spnMemberTitle").text = "Parent Information";
    var jsonData = { PMemberID: pMemberID };
    $.ajax({
        type: "POST",
        url: "SetupZoomSessions.aspx/ListParentInfo",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(jsonData),
        async: true,
        cache: false,
        success: function (data) {

            var tblHtml = "";
            tblHtml += " <thead>";
            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent1 Name</td>";
            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>Parent2 Name</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Parent1 Email</td>";
            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Parent2 Email</td>";


            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>City</td>";
            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white;  padding:3px 3px;'>State</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child Name</td>";


            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Child EMail</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


            tblHtml += "</tr>";
            tblHtml += " </thead>";

            $.each(data.d, function (index, value) {
                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + (index + 1) + "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndFirstName + " " + value.IndLastName + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseFirstName + " " + value.SpouseLastName + "";
                tblHtml += "</td>";


                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.IndEMail + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.SpouseEmail + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.City + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.State + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildFirstName + " " + value.ChildLastName + "";
                tblHtml += "</td>";



                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.ChildEmail + "";
                tblHtml += "</td>";


                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                tblHtml += "</td>";
                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                tblHtml += "</td>";


                tblHtml += "</tr>";
            });

            $(".tblParentInfo").html(tblHtml);


        }
    })
}

function getCoachInfo(cMemberID) {
    $(".spnMemberTitle").text = "Coach Information";
    var jsonData = { CMemberID: cMemberID };
    var url = "Zoom API/SetupZoomSessions.aspx/ListCoachInfo";
    if (source == "CC") {
        url = "../SetupZoomSessions.aspx/ListCoachInfo";
    } else {
        url = "SetupZoomSessions.aspx/ListCoachInfo";
    }


    $.ajax({
        type: "POST",
        url: url,
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(jsonData),
        async: true,
        cache: false,
        success: function (data) {

            var tblHtml = "";
            tblHtml += " <thead>";
            tblHtml += " <tr style='border: 1px solid black; border-collapse: collapse; background-color:#706f65; height:30px; color:white; font-weight:bold;'>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>ser#</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>Coach Name</td>";

            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px; width:80px;'>Coach Email</td>";



            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>CPhone</td>";
            tblHtml += " <td style='border: 1px solid black; border-collapse: collapse; vertical-align:middle; color:white; padding:3px 3px;'>HPhone</td>"


            tblHtml += "</tr>";
            tblHtml += " </thead>";

            $.each(data.d, function (index, value) {
                tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + (index + 1) + "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CoachFirstname + " " + value.Coachlastname + "";
                tblHtml += "</td>";



                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CoachEmail + "";
                tblHtml += "</td>";

                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.CPhone + "";
                tblHtml += "</td>";
                tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; padding:3px 3px;'>" + value.HPhone + "";
                tblHtml += "</td>";


                tblHtml += "</tr>";
            });

            $(".tblCoacnInfo").html(tblHtml);


        }
    })
}

function getEmailByMeberID(memberId) {
    var jsonData = { MemberID: memberId };
    $.ajax({
        type: "POST",
        url: "SetupZoomSessions.aspx/getEmailBasedOnMemberID",
        contentType: "application/json; charset=utf-8",
        dataType: "json",
        data: JSON.stringify(jsonData),
        async: true,
        cache: false,
        success: function (data) {
            $.each(data.d, function (index, value) {
                parentEmail = value.IndEMail;
            });
        }
    });
}


function senEmailToCoachAndparents() {

    $("#fountainTextG").css("display", "block");
    $("#overlay").css("display", "block");
    var from = $(".txFrom").val();
    var to = $(".txtTo").val();

    var cc = $(".txtCC").val();
    var subject = $(".txtSubject").val();

    var body = $(".mailBody").val().replace(/\n/gi, '<br />');

    var jsonData = JSON.stringify({ FromEmail: from, ToEmail: to, Subject: subject, Body: body, CC: cc });

    var url = "SetupZoomSessions.aspx/SendEmailsCoachesAndParents";
    if (source == "CC") {
        url = "../SetupZoomSessions.aspx/SendEmailsCoachesAndParents";
    } else {
        url = "SetupZoomSessions.aspx/SendEmailsCoachesAndParents";
    }

    if (validateEmaul() == 1) {
        $.ajax({
            type: "POST",
            url: url,
            contentType: "application/json; charset=utf-8",
            dataType: "json",
            data: jsonData,
            async: true,
            cache: false,
            success: function (data) {

                var retVal = data.d;
                if (retVal == 1) {
                    $(".spnErrMsg").text("Email has been sent successfully");
                    $("#fountainTextG").css("display", "none");
                    $("#overlay").css("display", "none");
                    $().toastmessage('showToast', {
                        text: 'Emails sent successfully',
                        sticky: true,
                        position: 'top-right',
                        type: 'success',
                        close: function () { console.log("toast is closed ..."); }
                    });
                }

            },

            failure: function (response) {
                $("#fountainTextG").css("display", "none");
                $("#overlay").css("display", "none");
            }
        });
    }
}


function validateEmaul() {
    var retVal = 1;

    var from = $(".txFrom").val();
    var to = $(".txtTo").val();
    var cc = $(".txtCC").val();
    var subject = $(".txtSubject").val();
    var body = $(".mailBody").val();

    if (from == "") {
        retVal = -1;
        $(".spnErrMsg").text("Please enter From email");
    } else if (to == "") {
        retVal = -1;
        $(".spnErrMsg").text("Please enter To email");
    } else if (subject == "") {
        retVal = -1;
        $(".spnErrMsg").text("Please enter Subject");
    } else if (body == "") {
        retVal = -1;
        $(".spnErrMsg").text("Please enter Content");
    }
    return retVal;
}

