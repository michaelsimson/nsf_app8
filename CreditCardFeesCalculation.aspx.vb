﻿Imports NorthSouth.BAL
Imports System.IO
Imports System.Net
Imports NativeExcel
Imports System.Net.Mail
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class CreditCardFeesCalculation
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Not IsPostBack Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select Distinct   BankDepCode  from CreditCardType")
            ddlCC.DataSource = ds
            ddlCC.DataTextField = "BankDepCode"
            ddlCC.DataValueField = "BankDepCode"
            ddlCC.DataBind()
            ddlCC.Items.Insert(0, New ListItem("All Cards", "All"))
            Dim year As Integer = Now.Year
            Dim i As Integer
            For i = -3 To -1
                year = Now.Year + i
                ddlYear.Items.Add(New ListItem(year.ToString() & "-" & (year + 1).ToString(), year))
            Next
        End If

    End Sub

    Protected Sub BtnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim sFY_BegDate As String
        Dim sFY_EndDate As String
        sFY_BegDate = "05/01/" & ddlYear.SelectedValue
        sFY_EndDate = "04/30/" & Convert.ToInt32(ddlYear.SelectedValue + 1).ToString() & " 23:59 "
        'from banktrans  VendCust=''   where TransCat  = 'CreditCard'
        Dim NFGBrand As String
        Dim BankTranVendCust As String

        If ddlCC.SelectedValue = "All" Then
            NFGBrand = "WHERE"
            BankTranVendCust = ""
        Else
            NFGBrand = " INNER JOIN CreditCardType CC ON CC.CreditCardType = NFG.Brand WHERE CC.BankDepCode='" & ddlCC.SelectedValue & "' AND "
            BankTranVendCust = " AND VendCust='" & ddlCC.SelectedValue & "'"
        End If
        Dim Sql As String = "Select sum(CreditCC) as CreditCC, Sum (DebitCC) as DebitCC, Sum (Registrations) as Registrations, SUM(MSAmount)as MSAmount,RIGHT(CONVERT(VARCHAR(9), Transdate, 6), 6) as TransDate from "
        Sql = Sql & "(select SUM(Amount) as CreditCC, 0 as DebitCC ,0 as Registrations, 0 as MSAmount, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate  from banktrans where TransCat  = 'CreditCard' and TransType = 'CREDIT' and TransDate Between '" & sFY_BegDate & "' AND '" & sFY_EndDate & "' " & BankTranVendCust & " Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4))  Union All "
        Sql = Sql & "select  0 as CreditCC, SUM(Amount) as DebitCC, 0 as  Registrations, 0 as MSAmount, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) as TransDate from banktrans where TransCat  = 'CreditCard' and TransType = 'DEBIT' and TransDate Between '" & sFY_BegDate & "' AND '" & sFY_EndDate & "'" & BankTranVendCust & " Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), TransDate, 103), 4)) Union All "
        Sql = Sql & "select  0 as CreditCC, 0 as DebitCC,SUM(NFG.TotalPayment) as Registrations, 0 as MSAmount, Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from NFG_Transactions NFG " & NFGBrand & " NFG.MS_TransDate   between '" & sFY_BegDate & "' and '" & sFY_EndDate & "'  Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4)) Union All "
        Sql = Sql & "select  0 as CreditCC, 0 as DebitCC,0 as Registrations, SUM(NFG.MS_Amount) as MSAmount ,Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103), 4)) as TransDate from ChargeRec NFG " & NFGBrand & " NFG.NFG_UniqueID is Null and NFG.MS_TransDate  between '" & sFY_BegDate & "' and '" & sFY_EndDate & "'  Group by Convert(date,SUBSTRING(CONVERT(VARCHAR(10), NFG.MS_TransDate, 103),4,2)+ '/1/' + RIGHT(CONVERT(VARCHAR(10),NFG.MS_TransDate, 103), 4))"
        Sql = Sql & ") Tmp Group by TransDate"

        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets.Add()
        oSheet.Range("A1:L1").MergeCells = True
        oSheet.Range("A1").Value = "NORTH SOUTH FOUNDATION "
        oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
        oSheet.Range("A1").Font.Bold = True
        oSheet.Range("A2").Value = "REGULAR ACCOUNT"
        oSheet.Range("A2").Font.Bold = True
        oSheet.Range("A3").Value = "FOR THE FISCAL YEAR ENDING "
        oSheet.Range("A3").Font.Bold = True
        oSheet.Range("D3").Value = "04/30/" & Convert.ToInt32(ddlYear.SelectedValue + 1).ToString()
        oSheet.Range("D3").Font.Bold = True
        oSheet.Range("D3").NumberFormat = "m/d/yyyy"

        'sSql = "SELECT * FROM Begining_Balances WHERE FI_YEAR=" & iYear & " AND BANK_CODE = 'BKONE'"
        'rsBegBal = db.OpenRecordset(sSql, dbOpenSnapshot)

        'If Not rsBegBal.EOF Then
        '    sFY_BegDate = CStr(rsBegBal("YEAR_BEG_DATE"))
        '    sFY_EndDate = CStr(rsBegBal("YEAR_END_DATE"))
        '    oSheet.Range("L7:L7").Select()
        '    oSheet.Range("L7:L7").Formula = rsBegBal("BEG_BALANCE")
        '    oSheet.Range("L7:L7").NumberFormat = "#,##0.00_);(#,##0.00)"
        'End If

        ' open bank transactions
        ' rows count
        oSheet.Range("A6:L6").Font.Bold = True
        oSheet.Range("A6:L6").ColumnWidth = 10
        oSheet.Range("A6").Value = "FY Year"
        oSheet.Range("B6").Value = "Name"
        oSheet.Range("C6").Value = "Deposit"
        oSheet.Range("D6").Value = "Debit"
        oSheet.Range("E6").Value = "Deposit less Reg"
        oSheet.Range("F6").Value = "All Fees"
        oSheet.Range("G6").Value = "Registrations"
        oSheet.Range("H6").Value = "Fees Percent"
        oSheet.Range("I6").Value = "Missed Payments"
        oSheet.Range("J6").Value = "Reg+MP"
        oSheet.Range("K6").Value = "Fees+MP"
        oSheet.Range("L6").Value = "Adj Percent"

        Dim CRange As IRange
        Dim rsBankTran As SqlDataReader
        rsBankTran = SqlHelper.ExecuteReader(Application("Connectionstring"), CommandType.Text, Sql)

        'start row for Bankone is 8
        Dim CurrentRow As Integer
        CurrentRow = 7
        Dim InitCurrRow As Integer = 8
        Dim flag As Boolean = False

        While rsBankTran.Read()
            flag = True
            CurrentRow = CurrentRow + 1
            ' date
            CRange = oSheet.Range("A" & Trim(Str(CurrentRow)))
            CRange.Value = ddlYear.SelectedValue
            If CurrentRow >= InitCurrRow + 8 Then
                CRange.Value = ddlYear.SelectedValue + 1
            End If

            'CRange.NumberFormat = "MM/DD/YYYY"
            ' row total
            CRange = oSheet.Range("E" & Trim(Str(CurrentRow)))
            CRange.Formula = "=-G" & Trim(Str(CurrentRow)) & "+C" & Trim(Str(CurrentRow)) ''G - C modified
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("F" & Trim(Str(CurrentRow)))
            CRange.Formula = "=-D" & Trim(Str(CurrentRow)) & "-E" & Trim(Str(CurrentRow)) ''-D+E Modified
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("H" & Trim(Str(CurrentRow)))
            CRange.Formula = "=F" & Trim(Str(CurrentRow)) & "/G" & Trim(Str(CurrentRow)) & "*100"
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("B" & Trim(Str(CurrentRow)))
            CRange.Value = rsBankTran("TransDate")

            CRange = oSheet.Range("C" & Trim(Str(CurrentRow)))
            CRange.Value = rsBankTran("CreditCC")
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("D" & Trim(Str(CurrentRow)))
            CRange.Value = rsBankTran("DebitCC")
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("G" & Trim(Str(CurrentRow)))
            CRange.Value = rsBankTran("Registrations")
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("I" & Trim(Str(CurrentRow)))
            CRange.Value = rsBankTran("MSAmount")
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"


            CRange = oSheet.Range("J" & Trim(Str(CurrentRow)))
            CRange.Formula = "=G" & Trim(Str(CurrentRow)) & "+I" & Trim(Str(CurrentRow))
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("K" & Trim(Str(CurrentRow)))
            CRange.Formula = "=F" & Trim(Str(CurrentRow)) & "+I" & Trim(Str(CurrentRow))
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

            CRange = oSheet.Range("L" & Trim(Str(CurrentRow)))
            CRange.Formula = "=(K" & Trim(Str(CurrentRow)) & "*100)/J" & Trim(Str(CurrentRow))
            CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        End While

        If flag = False Then
            lblErr.Text = "no record to Show"
            Exit Sub
        End If

        ' '' update colum totals
        oSheet.Range("A" & Trim(Str(CurrentRow + 2))).Value = "Total"
        oSheet.Range("A" & Trim(Str(CurrentRow + 2))).Font.Bold = True

        oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(C8:C" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("C" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("C" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(D8:D" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("D" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("D" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(E8:E" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("E" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("E" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(F8:F" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("F" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("F" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(G8:G" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("G" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("G" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Formula = "=F" & Trim(Str(CurrentRow + 2)) & "/G" & Trim(Str(CurrentRow + 2)) & "*100"
        oSheet.Range("H" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("H" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Formula = "=SUM(I8:I" & Trim(Str(CurrentRow)) & ")"
        oSheet.Range("I" & Trim(Str(CurrentRow + 2))).Font.Bold = True
        oSheet.Range("I" & Trim(Str(CurrentRow + 2))).NumberFormat = "#,##0.00_);(#,##0.00)"

        CRange = oSheet.Range("J" & Trim(Str(CurrentRow + 2)))
        CRange.Formula = "=G" & Trim(Str(CurrentRow + 2)) & "+I" & Trim(Str(CurrentRow + 2))
        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        CRange = oSheet.Range("K" & Trim(Str(CurrentRow + 2)))
        CRange.Formula = "=F" & Trim(Str(CurrentRow + 2)) & "+I" & Trim(Str(CurrentRow + 2))
        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        CRange = oSheet.Range("L" & Trim(Str(CurrentRow + 2)))
        CRange.Formula = "=(K" & Trim(Str(CurrentRow + 2)) & "*100)/J" & Trim(Str(CurrentRow + 2))
        CRange.NumberFormat = "#,##0.00_);(#,##0.00)"

        ' save the worksheet
        'Stream workbook 
        Dim FileName As String = "CreditCardFeesCalculation_" & ddlCC.SelectedItem.Text & "_" & ddlYear.SelectedValue & ".xls"
        oSheet.Name = ddlCC.SelectedItem.Text
        Response.Clear()
        Response.ContentType = "application/vnd.ms-excel"
        Response.AddHeader("Content-Type", "application/vnd.ms-excel")
        Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
        oWorkbooks.SaveAs(Response.OutputStream)
        Response.End()

    End Sub

    Private Sub getdata()
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "select B.BankID, B.FiscalYear, Bnk.BankCode, B.BegBalance from BankBalances B Inner Join  Bank bnk ON B.BankID=Bnk.BankID where B.fiscalyear=" & ddlbalYear.SelectedValue & "")
        'gvbankbalances.DataSource = ds
        'gvbankbalances.DataBind()

    End Sub
End Class
