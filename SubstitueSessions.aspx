﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="SubstitueSessions.aspx.cs" Inherits="SubstitueSessions" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function ConfirmmeetingCancel() {
                if (confirm("Are you sure you want to delete this substitute coach?")) {
                    document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();
                }
            }
            function JoinMeeting() {

                var url = document.getElementById("<%=hdnWebExMeetURL.ClientID%>").value;

                window.open(url, '_blank');

            }
            function StartMeeting() {

                JoinMeeting();

            }
            function deleteMeeting() {


                if (confirm("Are you sure want to remove substitute coach of this meeting?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
            }
        }

        function startChildMeeting() {
            var url = document.getElementById("<%=hdnChildMeetingURL.ClientID%>").value;
            $("#ancClick").target = "_blank";
            $("#ancClick").attr("href", url);
            $("#ancClick").attr("target", "_blank");
            document.getElementById("ancClick").click();
            //var win = window.open(url, '_blank');

            //if (win) {
            //    //Browser has allowed it to be opened
            //    win.focus();
            //} else {
            //    //Broswer has blocked it
            //    alert('Please allow popups for this site');
            //}

        }
        function joinChildMeeting() {
            startChildMeeting();
        }

        function showAlert(prdCode, coachName) {
            var startTime = document.getElementById("<%=hdnSessionStartTime.ClientID%>").value;
            var startMins = document.getElementById("<%=hdnStartMins.ClientID%>").value;
            var dueMins = parseInt(startMins);
            var msg = "";
            if (dueMins > 0) {
                msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
            }
            else if (dueMins >= -15) {
                msg = "The coach has not started the class.";
            } else if (dueMins < -15) {
                msg = "The coach has either not started or may have cancelled the class.";
            } else {
                msg = "Class of " + coachName + " scheduled for time: " + startTime + " Eastern has not yet started. It will start in " + startMins + " mins and/or " + coachName + " has not started the class.";
            }
            //var msg = "Meeting of " + coachName + " - " + prdCode + " is Not In-Progress";
            alert(msg);
        }
        function JoinMeeting() {

            var url = document.getElementById("<%=hdnZoomURL.ClientID%>").value;

            window.open(url, '_blank');

        }

        function showmsg() {
            alert("Coaches can only join their class up to 30 minutes before class time");
        }


        function showAlertmsgClassStatus() {

            alert("Your previous class status was not yet updated. Please set the status of the class appropriately");

        }

        </script>
    </div>
    <a id="ancClick" target="_blank" href=""></a>
    <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
    <asp:Button ID="btnDeleteMeeting" runat="server" Text="Delete Meeting" Style="display: none;" OnClick="btnDeleteMeeting_Click" />
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Substitute Sessions
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>



    <table id="Table1" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; padding: 10px;">
        <tr class="ContentSubTitle" align="center" style="background-color: honeydew;">
            <td align="left">Event year</td>
            <td align="left">Event</td>
            <td align="left">Chapter</td>
            <td align="left">Semester</td>
            <td align="left">Coach</td>
            <td align="left">Product Group</td>
            <td align="left">product</td>
            <td align="left">Level</td>
            <td align="left">Session No</td>
        </tr>

        <tr class="ContentSubTitle" align="center">
            <td align="left">
                <asp:DropDownList ID="ddYear" runat="server" Width="85px"
                    AutoPostBack="True" OnSelectedIndexChanged="ddYear_SelectedIndexChanged">
                </asp:DropDownList>
            </td>
            <td align="left">
                <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddchapter" runat="server" Width="100px" AutoPostBack="True" Enabled="false">
                    <asp:ListItem Value=""></asp:ListItem>
                    <asp:ListItem Value="112">Coaching, US</asp:ListItem>
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlPhase" runat="server" Width="75px" AutoPostBack="True" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlCoach" OnSelectedIndexChanged="ddlCoach_SelectedIndexChanged" runat="server" Width="200px" AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="ProductGroup" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlProduct" runat="server" DataTextField="Product" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" AutoPostBack="true">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlLevel" runat="server" Width="75px" AutoPostBack="True">
                </asp:DropDownList></td>
            <td align="left">
                <asp:DropDownList ID="ddlSession" runat="server" Width="75px" AutoPostBack="True">
                    <asp:ListItem Value="1">1</asp:ListItem>
                    <asp:ListItem Value="2">2</asp:ListItem>
                    <asp:ListItem Value="3">3</asp:ListItem>
                    <asp:ListItem Value="4">4</asp:ListItem>
                    <asp:ListItem Value="5">5</asp:ListItem>
                </asp:DropDownList></td>
        </tr>
        <tr>
            <td align="center" colspan="9">
                <asp:Button ID="btnSeacrh" runat="server" Text="Submit" OnClick="btnSeacrh_Click" /></td>
        </tr>
    </table>


    <div style="clear: both; margin-bottom: 20px;"></div>

    <div style="margin-left: auto; margin-right: auto; padding: 10px; width: 80%; border: 1px solid #c6cccd;">
        <center>
            <h2>Assign Substitute Coach</h2>
        </center>

        <table id="Table2" runat="server" visible="true" align="center" style="width: 80%;">


            <tr class="ContentSubTitle">
                <td>
                    <asp:Label ID="Label1" runat="server" Font-Bold="true" Visible="false">Substitute Date</asp:Label>
                </td>
                <td>
                    <asp:DropDownList ID="DDlSubDate" runat="server" Visible="false" OnSelectedIndexChanged="DDlSubDate_SelectedIndexChanged" AutoPostBack="True">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                    </asp:DropDownList>
                </td>

                <td align="left">
                    <asp:Label ID="lblSubRecurDate" runat="server" Font-Bold="true">Regular Class Week#</asp:Label>
                </td>
                <td align="left">
                    <asp:TextBox ID="TxtWeekNo" runat="server"></asp:TextBox>
                </td>

                <td id="td15" runat="server">Substitute Coach
                </td>
                <td id="td16" runat="server" align="left">
                    <asp:DropDownList ID="DDLSubstituteCoach" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="0">Select</asp:ListItem>

                    </asp:DropDownList>

                </td>
                <td id="td17" runat="server" align="left" visible="false">Reason
                </td>
                <td id="td18" runat="server" align="left" visible="false">
                    <asp:DropDownList ID="ddlReason" runat="server" AutoPostBack="True">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="Illness">Illness</asp:ListItem>
                        <asp:ListItem Value="Emergency">Emergency</asp:ListItem>
                        <asp:ListItem Value="Business Trip">Business Trip</asp:ListItem>
                        <asp:ListItem Value="Work Related">Work Related</asp:ListItem>
                        <asp:ListItem Value="Holiday">Holiday</asp:ListItem>
                        <asp:ListItem Value="Other">Other</asp:ListItem>
                    </asp:DropDownList>

                </td>
            </tr>




            <tr>
                <td colspan="8">



                    <table align="center">
                        <tr>


                            <td>
                                <asp:Button ID="btnSubmit" runat="server" Visible="false" Text="Submit" OnClick="btnSubmit_Click" Style="height: 26px" />
                            </td>
                            <td>
                                <asp:Button ID="BtnCreateSession" runat="server" Text="Assign Substitute Coach" OnClick="btnCreateMeeting_Click" />
                            </td>
                            <td>
                                <asp:Button ID="btnCancel" runat="server" Visible="false" Text="Cancel" OnClick="btnCancelMeeting_Click" />
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>


        </table>
    </div>


    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" align="center" Style="color: red;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <table id="tblAddNewMeeting" runat="server" visible="false" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">

        <tr class="ContentSubTitle">





            <td align="left" nowrap="nowrap" id="Td5" runat="server" runat="server" visible="false">Meeting Password </td>
            <td style="width: 100px" runat="server" align="left" id="Td6" runat="server" visible="false">
                <asp:TextBox ID="txtMeetingPwd" TextMode="Password" runat="server" Width="100" Wrap="False"></asp:TextBox>
                <br />
                <br />
            </td>
            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Start Date</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:TextBox ID="txtDate" runat="server" Enabled="false" Style="width: 95px;"></asp:TextBox>
                <br />
                MM/DD/YYYY</td>
            <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Begin Time </td>
            <td style="width: 439px" align="left">
                <asp:TextBox ID="txtTime" runat="server" Enabled="false" Style="width: 95px;"></asp:TextBox>
                <br />
                HH : MM : SS
            </td>
        </tr>


        <tr class="ContentSubTitle">


            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Substitute Date</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:TextBox ID="txtSubstituteDate" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                MM/DD/YYYY</td>
            <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Substitute Time </td>
            <td style="width: 439px" align="left">
                <asp:TextBox ID="txtSubstituteTime" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                HH : MM : SS
            </td>



            <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Duration (Mins)</td>
            <td style="width: 100px" runat="server" align="left" id="ddchapterdrop">
                <asp:TextBox ID="txtDuration" runat="server" Width="93px"></asp:TextBox>
            </td>

            <td align="left" nowrap="nowrap" id="Td7" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Time Zone </td>
            <td style="width: 100px" runat="server" align="left" id="Td8">
                <asp:DropDownList ID="ddlTimeZone" runat="server" Width="110px"
                    AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="11" Selected="True">EST/EDT (Eastern or New York Time)</asp:ListItem>
                    <asp:ListItem Value="7">CST/CDT (Central or Chicago Time)</asp:ListItem>
                    <asp:ListItem Value="6">MST/MDT (Mountain or Denver Time)</asp:ListItem>
                    <asp:ListItem Value="4">PST/PDT (Pacific or West Coast Time)</asp:ListItem>
                </asp:DropDownList>
                <div style="clear: both; margin-bottom: 10px;"></div>
            </td>

        </tr>

        <tr id="Tr1" class="ContentSubTitle" runat="server" visible="false">
            <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; End Date</td>

            <td align="left" nowrap="nowrap" style="width: 41px">
                <asp:TextBox ID="txtEndDate" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                MM/DD/YYYY</td>

            <td style="width: 70px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;End Time </td>
            <td style="width: 439px" align="left">
                <asp:TextBox ID="txtEndTime" runat="server" Style="width: 95px;"></asp:TextBox>
                <br />
                HH : MM : SS
            </td>
            <td align="left" nowrap="nowrap" id="Td9" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Day </td>
            <td style="width: 100px" runat="server" align="left" id="Td10">
                <asp:DropDownList ID="ddlDay" runat="server" Width="110px"
                    AutoPostBack="True">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="MONDAY">Monday</asp:ListItem>
                    <asp:ListItem Value="TUESDAY">Tuesday</asp:ListItem>
                    <asp:ListItem Value="WEDNESDAY">Wednesday</asp:ListItem>
                    <asp:ListItem Value="THURSDAY">Thursday</asp:ListItem>
                    <asp:ListItem Value="FRIDAY">Friday</asp:ListItem>
                    <asp:ListItem Value="SATURDAY">Saturday</asp:ListItem>
                    <asp:ListItem Value="SUNDAY">Sunday</asp:ListItem>
                </asp:DropDownList>
            </td>

            <td align="left" nowrap="nowrap" id="TdUserIDTitle" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;WebEx UserID</td>
            <td align="left" nowrap="nowrap" id="TdUserID" runat="server">
                <asp:TextBox ID="txtUserID" runat="server" Style="width: 95px;"></asp:TextBox>
            </td>
            <td align="left" nowrap="nowrap" id="TdPWDTitle" runat="server">PWD</td>
            <td align="left" nowrap="nowrap" id="TdPWD" runat="server">
                <asp:TextBox ID="txtPWD" TextMode="Password" runat="server" Style="width: 95px;"></asp:TextBox>
            </td>
        </tr>
        <tr>
            <td colspan="10">



                <table align="center">
                    <tr>
                        <td>
                            <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Substitute Session" OnClick="btnCreateMeeting_Click" />
                        </td>

                    </tr>
                </table>
            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblSuccess" runat="server" Style="color: blue;"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" style="font-weight: bold; color: #64A81C;">
        <span id="spnTable1Title" runat="server" visible="false">Table 1 : Substitute Sessions</span>


    </div>
    <div style="clear: both;"></div>
    <center><span id="spnTblMsg" style="font-weight: bold; color: red;" runat="server"></span></center>
    <div style="clear: both;"></div>
    <div style="float: left; width: 50px;">
        <span style="font-weight: bold;">Filter :</span>
    </div>
    <div style="float: left;">
        <asp:DropDownList ID="ddlFilterBy" runat="server" OnSelectedIndexChanged="ddlFilterBy_SelectedIndexChanged" AutoPostBack="true">
            <asp:ListItem Value="None">None</asp:ListItem>
            <asp:ListItem Value="Year">By Year</asp:ListItem>
            <asp:ListItem Value="ProductGroup">By ProductGroup</asp:ListItem>
            <asp:ListItem Value="Product">By Product</asp:ListItem>
            <asp:ListItem Value="Coach">By Coach</asp:ListItem>
        </asp:DropDownList>

    </div>
    <div style="float: left;" id="dvFilterSection" runat="server" visible="false">
        <div style="float: left; margin-left: 10px; width: 125px;" id="dvYearSection" runat="server" visible="false">
            <div style="float: left; width: 50px;">
                <span style="font-weight: bold;">Year</span>
            </div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlYearFilter" runat="server">
                    <asp:ListItem Value="2016">2016</asp:ListItem>
                    <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; margin-left: 10px; width: 170px;" id="dvFilterPrdGroupSection" runat="server" visible="false">
            <div style="float: left; width: 100px;">
                <span style="font-weight: bold;">Product Group</span>
            </div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlProductGroupFilter" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; width: 160px; margin-left: 10px;" id="dvProductFilter" runat="server" visible="false">
            <div style="float: left; width: 75px;">
                <span style="font-weight: bold;">Product</span>
            </div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlProductFilter" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; width: 290px; margin-left: 10px;" id="dvCoachFilter" runat="server" visible="false">
            <div style="float: left; width: 60px;">
                <span style="font-weight: bold;">Coach</span>
            </div>
            <div style="float: left;">
                <asp:DropDownList ID="ddlCoachFilter" runat="server">
                    <asp:ListItem Value="0">Select</asp:ListItem>

                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; width: 200px;" id="dvButton" runat="server" visible="false">

            <div style="float: left;">
                <asp:Button ID="btnSearch" runat="server" Text="Search" OnClick="btnSearch_Click" />
            </div>
        </div>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <asp:Button ID="btnAddNewMeeting" runat="server" Visible="false" Text="New Meeting" Style="float: right;" />
    <div style="clear: both;"></div>
    <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdMeeting" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 1250px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdMeeting_RowCommand" OnRowCancelingEdit="GrdMeeting_RowCancelingEdit" EnableModelValidation="True" PageSize="20" AllowPaging="true" OnPageIndexChanging="GrdMeeting_PageIndexChanging">
        <Columns>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" Visible="false">

                <ItemTemplate>
                    <asp:Button ID="btnSelect" runat="server" Text="Select" CommandName="Select" />
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                <ItemTemplate>
                    <div style="display: none;">
                        <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                        <asp:Label ID="lblChapterID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                        <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>
                        <asp:Label ID="lblTimeZoneID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"TimeZoneID") %>'>'></asp:Label>
                        <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'>'></asp:Label>
                        <asp:Label ID="lblWebExPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'>'></asp:Label>

                        <%-- <asp:Label ID="lblWebUID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WebUID") %>'>'></asp:Label>
                        <asp:Label ID="lblVroom" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'>'></asp:Label>
                        <asp:Label ID="LabelblWebPwd" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WebPwd") %>'>'></asp:Label>--%>
                        <asp:Label ID="lblWebConfLogID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"VideoConfLogID") %>'>'></asp:Label>

                        <asp:Label ID="LbkMeetingURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingUrl") %>'>'></asp:Label>
                        <%--  <asp:Label ID="lblSubDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SubstituteDate","{0:MM-dd-yyyy}") %>'>'></asp:Label>--%>
                        <asp:Label ID="lblSubDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SubstituteDate","{0:yyyy-MM-dd}") %>'>'></asp:Label>
                        <asp:Label ID="lblWeekNo" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"WeekNo") %>'>'></asp:Label>

                        <asp:Label ID="lblSubstituteCoachID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Substitute") %>'>'></asp:Label>
                    </div>
                    <asp:Button ID="btnModifyMeeting" runat="server" Text="Modify" CommandName="Modify" />
                    <asp:Button ID="btnCancelMeeting" runat="server" Text="Cancel" CommandName="unassigned" />
                </ItemTemplate>

                <ItemStyle HorizontalAlign="Center"></ItemStyle>
            </asp:TemplateField>
            <asp:TemplateField>
                <HeaderTemplate>
                    Ser#
                </HeaderTemplate>
                <ItemTemplate>
                    <asp:Label ID="lblSRNO" runat="server"
                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="EventYear" HeaderText="Year"></asp:BoundField>
            <asp:BoundField DataField="EventCode" HeaderText="Event"></asp:BoundField>
            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter"></asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="Product Group"></asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
            <asp:BoundField DataField="Semester" HeaderText="Semester"></asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
            <asp:BoundField DataField="Session" HeaderText="Session"></asp:BoundField>
            <asp:TemplateField HeaderText="Substitute Coach">

                <ItemTemplate>

                    <asp:LinkButton runat="server" ID="lnkSubCoach" Text='<%# Bind("SubCoach")%>' CommandName="SelectLink"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="Coach">

                <ItemTemplate>

                    <asp:LinkButton runat="server" ID="lnkCoach" Text='<%# Bind("Coach")%>' CommandName="SelectLink"></asp:LinkButton>
                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Coach" HeaderText="Coach"></asp:BoundField>--%>
            <asp:BoundField DataField="SessionKey" HeaderText="Meeting Key"></asp:BoundField>
            <asp:BoundField DataField="MeetingPwd" HeaderText="Meeting Password"></asp:BoundField>


            <asp:BoundField DataField="Day" HeaderText="Day"></asp:BoundField>
            <asp:BoundField DataField="Date" HeaderText="Class Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
            <asp:BoundField DataField="Date" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" Visible="false"></asp:BoundField>
            <asp:BoundField DataField="Date" HeaderText="Substitute Date" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
            <asp:BoundField DataField="WeekNo" HeaderText="WeekNo"></asp:BoundField>
            <asp:TemplateField HeaderText="Begin Time">

                <ItemTemplate>
                    <asp:Label ID="hlTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <asp:TemplateField HeaderText="End Time" Visible="false">

                <ItemTemplate>
                    <asp:Label ID="hlEndTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EndTime") %>'><%# Eval("EndTime").ToString().Substring(0,Math.Min(5,Eval("EndTime").ToString().Length)) %></asp:Label>

                </ItemTemplate>
            </asp:TemplateField>
            <%--<asp:BoundField DataField="Time" HeaderText="Time" DataFormatString="{0:N5}"></asp:BoundField>--%>
            <asp:BoundField DataField="Duration" HeaderText="Duration (Mins)"></asp:BoundField>
            <asp:BoundField DataField="TimeZone" HeaderText="TimeZone"></asp:BoundField>
            <asp:BoundField DataField="Status" HeaderText="Status" Visible="false"></asp:BoundField>


            <asp:TemplateField HeaderText="meeting URL">

                <ItemTemplate>

                    <div style="display: none;">
                        <asp:LinkButton runat="server" ID="MyHyperLinkControl" Text='<%# Eval("MeetingUrl").ToString()%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("MeetingUrl").ToString()%>'></asp:LinkButton>
                        <asp:Label ID="lblBegTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"BeginTime") %>'><%# Eval("BeginTime").ToString().Substring(0,Math.Min(5,Eval("BeginTime").ToString().Length)) %></asp:Label>

                        <asp:Label ID="lblMeetDay" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                        <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                        <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                    </div>
                    <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />
                    <%--    <asp:HyperLink ID="MyHyperLinkControl" Target="_blank" NavigateUrl='<%# Bind("MeetingUrl")%>' runat="server" ToolTip='<%# Bind("MeetingUrl")%>'><%# Eval("MeetingUrl").ToString().Substring(0,Math.Min(20,Eval("MeetingUrl").ToString().Length))+"...." %></asp:HyperLink>--%>
                </ItemTemplate>
            </asp:TemplateField>
            <asp:BoundField DataField="UserID" HeaderText="User ID" Visible="false"></asp:BoundField>
            <asp:BoundField DataField="Pwd" HeaderText="Pwd" Visible="false"></asp:BoundField>
            <asp:BoundField DataField="Vroom" HeaderText="Vroom"></asp:BoundField>
        </Columns>

        <HeaderStyle BackColor="#FFFFCC"></HeaderStyle>

        <RowStyle HorizontalAlign="Left" CssClass="SmallFont"></RowStyle>
    </asp:GridView>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <table id="trChildList" runat="server" visible="false">
            <tr>
                <td align="center">
                    <div style="font-weight: bold; color: #64A81C;">Table 2 : Child List</div>
                    <div style="clear: both;">
                    </div>

                    <div style="clear: both;"></div>
                    <asp:Label ID="lblChildMsg" runat="server" Style="color: red;"></asp:Label>
                    <div style="clear: both;"></div>
                    <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdStudentsList" AutoGenerateColumns="False" runat="server" RowStyle-CssClass="SmallFont" Style="width: 800px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdStudentsList_RowCommand">
                        <Columns>
                            <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">

                                <ItemTemplate>
                                    <div style="display: none;">
                                        <asp:Label ID="lblChildNumber" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ChildNumber") %>'>'></asp:Label>
                                        <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CMemberID") %>'>'></asp:Label>
                                        <asp:Label ID="lblPMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"PMemberID") %>'>'></asp:Label>
                                        <asp:Label ID="LblChildURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SubstituteURL") %>'>'></asp:Label>
                                    </div>
                                    <asp:Button ID="btnSelect" runat="server" Text="Register" CommandName="Register" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Ser#
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSRNO" runat="server"
                                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:BoundField DataField="Name" HeaderText="Child"></asp:BoundField>
                            <asp:BoundField DataField="ParentName" HeaderText="Parent"></asp:BoundField>
                            <asp:BoundField DataField="Gender" HeaderText="Gender"></asp:BoundField>
                            <asp:BoundField DataField="WebExEmail" HeaderText="Email"></asp:BoundField>
                            <asp:BoundField DataField="City" HeaderText="City"></asp:BoundField>
                            <asp:BoundField DataField="Country" HeaderText="Country"></asp:BoundField>
                            <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                            <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group"></asp:BoundField>
                            <asp:BoundField DataField="ProductCode" HeaderText="Product"></asp:BoundField>
                            <asp:BoundField DataField="Level" HeaderText="Level"></asp:BoundField>
                            <asp:BoundField DataField="AttendeeID" HeaderText="Attendee ID"></asp:BoundField>
                            <asp:BoundField DataField="RegisteredID" HeaderText="Registered ID"></asp:BoundField>
                            <asp:TemplateField HeaderText="Substitute meeting URL">

                                <ItemTemplate>

                                    <asp:LinkButton runat="server" ID="HlAttendeeMeetURL" Text='<%# Eval("AttendeeJoinURL").ToString().Substring(0,Math.Min(20,Eval("AttendeeJoinURL").ToString().Length))+""%>' CommandName="SelectMeetingURL" ToolTip='<%# Eval("AttendeeJoinURL").ToString()%>'></asp:LinkButton>

                                    <%--   <asp:HyperLink ID="hlChildLink" Target="_blank" NavigateUrl='<%# Eval("AttendeeJoinURL").ToString() %>' runat="server" ToolTip='<%# Eval("AttendeeJoinURL").ToString() %>'><%# Eval("AttendeeJoinURL").ToString() %></asp:HyperLink>--%>
                                </ItemTemplate>
                            </asp:TemplateField>
                        </Columns>

                    </asp:GridView>
                    <div style="clear: both;"></div>
                    <asp:Button ID="btnClosetable2" Style="margin-bottom: 10px;" runat="server" Text="Close Table 2" OnClick="btnClosetable2_Click" />
                </td>
            </tr>
        </table>
    </div>

    <input type="hidden" id="hdnSessionKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey" value="0" runat="server" />
    <input type="hidden" id="hdsnRegistrationKey1" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingPwd" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingUrl" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingStatus" value="0" runat="server" />
    <input type="hidden" id="hdnHostURL" value="0" runat="server" />
    <input type="hidden" id="hdnWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnWebExPwd" value="0" runat="server" />
    <input type="hidden" id="hdnCoachID" value="0" runat="server" />
    <input type="hidden" id="hdnChildID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeID" value="0" runat="server" />
    <input type="hidden" id="hdnMeetingAttendeeURL" value="0" runat="server" />
    <input type="hidden" id="hdnException" value="Error Occured....." runat="server" />
    <input type="hidden" id="hdnProductGroupID" value="0" runat="server" />
    <input type="hidden" id="hdnProductID" value="0" runat="server" />

    <input type="hidden" id="hdnWebExMeetURL" runat="server" value="" />
    <input type="hidden" id="hdnChildMeetingURL" value="0" runat="server" />
    <input type="hidden" id="HdnLevel" value="0" runat="server" />

    <input type="hidden" id="hdnCoachname" value="0" runat="server" />
    <input type="hidden" id="hdnSessionStartTime" value="0" runat="server" />
    <input type="hidden" id="hdnStartMins" value="0" runat="server" />
    <input type="hidden" id="hdnVroom" value="0" runat="server" />
    <input type="hidden" id="hdnWebConflogID" value="0" runat="server" />

    <input type="hidden" id="hdnSubWebExID" value="0" runat="server" />
    <input type="hidden" id="hdnSubWebExPwd" value="0" runat="server" />

    <input type="hidden" id="hdnSubWebRoom" value="0" runat="server" />
    <input type="hidden" id="hdnZoomURL" runat="server" value="" />
    <input type="hidden" id="hdnSessionNo" runat="server" value="" />
    <input type="hidden" id="hdnStartDate" runat="server" value="" />
    <input type="hidden" id="hdnHostID" runat="server" value="" />
    <input type="hidden" id="hdnWeekNo" runat="server" value="" />



</asp:Content>
