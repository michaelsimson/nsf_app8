<%@ Page Language="C#" AutoEventWireup="true" CodeFile="MedalCountWeekly.aspx.cs" Inherits="Reports_MedalCountWeekly" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
</head>
<body>
    <h2 align="center">
			Total Medal Count</h2>
		<form id="Form1" method="post" runat="server">
			<asp:label id="lblError" style="Z-INDEX: 101; LEFT: 40px; POSITION: absolute; TOP: 65px" runat="server"></asp:label>
			<asp:label id="lblChapter" style="Z-INDEX: 103; LEFT: 39px; POSITION: absolute; TOP: 112px"
				runat="server"></asp:label>
			<asp:DropDownList id="ddlWeek" style="Z-INDEX: 104; LEFT: 375px; POSITION: absolute; TOP: 86px" runat="server"
				AutoPostBack="True">
				<asp:ListItem Value="1">Week 1</asp:ListItem>
				<asp:ListItem Value="2">Week 2</asp:ListItem>
				<asp:ListItem Value="3">Week 3</asp:ListItem>
				<asp:ListItem Value="4">Week 4</asp:ListItem>
				<asp:ListItem Value="5">Week 5</asp:ListItem>
				<asp:ListItem Value="6">Week 6</asp:ListItem>
				<asp:ListItem Value="7">Week 7</asp:ListItem>
				<asp:ListItem Value="8">Week 8</asp:ListItem>
				<asp:ListItem Value="9">Week 9</asp:ListItem>
				<asp:ListItem Value="10">Week 10</asp:ListItem>
			</asp:DropDownList>
			<asp:datagrid id="DataGrid1" style="Z-INDEX: 102; LEFT: 78px; POSITION: absolute; TOP: 157px"
				runat="server"></asp:datagrid>
			<asp:Button id="btnSave" style="Z-INDEX: 105; LEFT: 632px; POSITION: absolute; TOP: 92px" runat="server"
				Text="Save as Excel" onclick="btnSave_Click"></asp:Button>
            <asp:Label ID="lblDate" runat="server" Style="z-index: 103; left: 44px; position: absolute;
                top: 85px"></asp:Label>
        </form>
</body>
</html>


 
 
 