Imports System
Imports System.Text
Imports System.Configuration
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Data.SqlClient
Imports System.Drawing
Imports Microsoft.ApplicationBlocks.Data
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Collections.Specialized
Imports System.Globalization
Imports LinkPointTransaction
Imports System.Text.RegularExpressions
Imports NorthSouth.BAL
Imports nsf.Entities
Imports System.Net.Mail
Imports GDE4
Imports GDE4.Service
Imports GDE4.Transaction
Namespace VRegistration
    ' <summary>
    ' Summary description for regnlregnet_Pay.
    ' </summary>
    Partial Class reg_Pay
        Inherits LinkPointAPI_cs.LinkPointTxn_Page

        'Private us As CultureInfo = New CultureInfo("en-US")

        'http://regxlib.com/REDetails.aspx?regexp_id=540
        Private nRegFee As Decimal = 0
        Private SaleAmt As Decimal = 0.0
        Private FundRAmt As Decimal = 0.0
        Private nDonationAmt As Decimal = 0
        Private nTotalAmt As Decimal = 0
        Private nMealsAmt As Decimal = 0.0
        Private nLateFee As Decimal = 0
        Dim isTestMode As Boolean = False
        Protected RegularExpressionValidator1 As System.Web.UI.WebControls.RegularExpressionValidator
        '*****************************************
        Dim Cust_id As String = String.Empty
        Dim e4_BankMessage As String = String.Empty
        Dim e4_Auth As String = String.Empty
        Dim e4_result As String = String.Empty
        Dim e4_Message As String = String.Empty
        Dim e4_Error As String = String.Empty
        Dim e4_Code As String = String.Empty
        Dim e4_Id As String = String.Empty
        Dim e4_Pass As String = String.Empty
        Protected EmailID As String
        Dim txnStatus As String = String.Empty
        Dim txtAuth As String = String.Empty
        Dim custIndId As Integer = 0

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            Dim blnContinue As Boolean = True

            'If Session("LoggedIn") <> "True" Then
            '    Server.Transfer("login.aspx")
            'End If
            If (Session("ContestsSelected") = Nothing) Then
                If (Session("EventId") = 1) Then
                    If (Session("Mealcharges") = Nothing) Then
                        blnContinue = False
                    End If
                ElseIf (Session("EventId") = 13) Then
                    ''**lblRgfee.Visible = False
                    blnContinue = True
                ElseIf (Session("EventId") = 5 Or Session("EventId") = 12 Or Session("EventId") = 17) Then
                    lblRgfee.Visible = False
                    blnContinue = True
                ElseIf (Session("EventId") = 10) Then
                    lblRgfee.Visible = False
                    trsale.Visible = True
                    blnContinue = True
                ElseIf (Session("EventId") = 9) Then
                    lblRgfee.Visible = False
                    trDonation.Visible = False
                    trsale.Visible = False
                    trFundR.Visible = True
                    blnContinue = True
                Else
                    blnContinue = False
                End If
            End If
            'If blnContinue = False Then
            '    If Session("EventId") = 3 Then
            '        Response.Redirect("WkShopRegistration.aspx")
            '    Else
            '        Response.Redirect("ContestantRegistration.aspx")
            '    End If
            'End If

            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)
            'Response.Write("SESSION DETAILS FROM EARLIER PAGES:")
            'commented by Meena, not sure what this piece of code is for.
            'If Session("IndID") Is Nothing Then
            'If Session("FatherID") Is Nothing Then
            'Session("FatherID") = Session("IndID")
            'End If
            'End If
            lblRegFee.Text = ""
            lblDonation.Text = ""
            lblTotalAmount.Text = ""
            lblMealsAmount.Text = ""
            lblLateFee.Text = ""
            lblSaleAmount.Text = ""
            lblFundRAmt.Text = ""
            If (Not (Session("RegFee")) Is Nothing) Then
                'Response.Write("Registration fee:" + Session("RegFee").ToString + "<br>")

                nRegFee = CType(Session("RegFee"), Integer)
                lblRegFee.Text = nRegFee.ToString("c", New CultureInfo("en-US"))
                nTotalAmt = nRegFee
            Else
                'Response.Write("Session(RegFee) value not set" + "<br>")

            End If
            If (Not (Session("MealCharges")) Is Nothing) Then
                'Response.Write("Meals amount :" + Session("MealCharges").ToString + "<br>")
                nMealsAmt = CType(Session("MealCharges"), Integer)
                lblMealsAmount.Text = nMealsAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nMealsAmt
            Else
                'Response.Write("Session(MealCharges) not set" + "<br>")
            End If

            If (Not (Session("LATEFEE")) Is Nothing) Then
                'Response.Write("Latefee:" + Session("LATEFEE").ToString + "<br>")

                nLateFee = CType(Session("LATEFEE"), Integer)
                lblLateFee.Text = nLateFee.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += nLateFee
            Else
                'Response.Write("Session(LATEFEE) value not set" + "<br>")
            End If
            If (Not (Session("SaleAmt")) Is Nothing) Then
                SaleAmt = CType(Session("SaleAmt"), Integer)
                lblSaleAmount.Text = SaleAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += SaleAmt
            End If

            If (Not (Session("FundRAmt")) Is Nothing) Then
                FundRAmt = CType(Session("FundRAmt"), Integer)
                lblFundRAmt.Text = FundRAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt += FundRAmt
            End If

            If (Not (Session("Donation")) Is Nothing) Then
                'Response.Write("Donation:" + Session("Donation").ToString + "<br>")
                nDonationAmt = CType(Session("Donation"), Integer)
                lblDonation.Text = nDonationAmt.ToString("c", New CultureInfo("en-US"))
                nTotalAmt = (nTotalAmt + nDonationAmt)
            Else
                'Response.Write("Donation session value not set" + "<br>")
            End If
            If lblMealsAmount.Text.Trim().Equals("") Or lblMealsAmount.Text.Trim().Equals("$0.00") Then
                lblMeals.Visible = False
                lblMealsAmount.Visible = False
            End If
            If lblLateFee.Text.Trim().Equals("") Or lblLateFee.Text.Trim().Equals("$0.00") Then
                lblLatefeetext.Visible = False
                lblLateFee.Visible = False
            End If


            'If (nTotalAmt = 0) Then
            '    If Session("EventId") = 1 Or Session("EventId") = 2 Then
            '        Response.Redirect("ContestantRegistration.aspx")
            '    ElseIf Session("EventId") = 3 Then
            '        Response.Redirect("WkShopRegistration.aspx")
            '    ElseIf Session("EventId") = 13 Then
            '        Response.Redirect("CoachingRegistration.aspx")
            '    ElseIf Session("EventId") = 5 Or Session("EventId") = 12 Or Session("EventId") = 17 Then
            '        Response.Redirect("Don_athon_custom.aspx?id=" & Session("WalkMarathonID"))
            '    ElseIf Session("EventId") = 10 Then
            '        Response.Redirect("ShoppingCatalog.aspx")
            '    ElseIf Session("EventId") = 9 Then
            '        If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
            '            Response.Redirect("FundRReg.aspx")
            '        Else
            '            Response.Redirect("FundRReg.aspx?id=1")
            '        End If
            '    Else
            '        Response.Redirect("UserFunctions.aspx")
            '    End If
            'End If
            lblTotalAmount.Text = nTotalAmt.ToString("c", New CultureInfo("en-US"))
            If Not Page.IsPostBack Then

                Dim countries As DataSet = New DataSet
                countries.ReadXml(MapPath("countries.xml"))
                ddlCountry.DataSource = countries
                ddlCountry.DataValueField = "value"
                ddlCountry.DataTextField = "text"
                ddlCountry.DataBind()

                Dim dsStates As DataSet

                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlState.DataSource = dsStates.Tables(0)
                    ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlState.DataBind()
                End If
                GetIPAddress(lblIP)
                PrePopulate()
                ddlYear.Items.Clear()
                Dim li As ListItem = New ListItem(" ", "")
                ddlYear.Items.Add(li)
                Dim i As Integer = System.DateTime.Today.Year
                Do While (i _
                            <= (System.DateTime.Today.Year + 9))
                    Dim j As Integer = (i - 2000)
                    li = New ListItem(i.ToString, j.ToString)
                    ddlYear.Items.Add(li)
                    i = (i + 1)
                Loop

                '    Dim nsfMaster As NSFMasterPage = Me.Master
                '    If Session("EventID") = 13 Then
                '        nsfMaster.addBackMenuItem("CoachingRegistration.aspx")
                '    ElseIf Session("EventID") = 13 Then
                '        nsfMaster.addBackMenuItem("Reg_Donate.aspx")
                '    ElseIf Session("EventID") = 3 Then
                '        nsfMaster.addBackMenuItem("WkShopRegistration.aspx")
                '    ElseIf Session("EventID") = 10 Then
                '        nsfMaster.addBackMenuItem("ShoppingCatalog.aspx")
                '    ElseIf Session("EventId") = 12 Then
                '        ''** update Feature for WMSponsorDonation
                '    ElseIf Session("EventId") = 9 Then
                '        If Session("EntryToken").ToString.ToUpper() = "PARENT" Or Session("entryToken").ToString.ToUpper() = "DONOR" Then
                '            nsfMaster.addBackMenuItem("FundRReg.aspx")
                '        Else
                '            nsfMaster.addBackMenuItem("FundRReg.aspx?id=1")
                '        End If
                '    Else
                '        nsfMaster.addBackMenuItem("ContestantRegistration.aspx")
                '    End If
            End If
        End Sub
        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub

        Private Sub PrePopulateOld()
            Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt
            objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), Session("CustIndID"))
            If objIndSpouseExt.Id > 0 Then
                txtAddress1.Text = objIndSpouseExt.Address1
                txtAddress2.Text = objIndSpouseExt.Address2
                txtCity.Text = objIndSpouseExt.City
                ddlState.Items.FindByValue(objIndSpouseExt.State).Selected = True
                ddlCountry.Items.FindByValue(objIndSpouseExt.Country).Selected = True
                txtZip.Text = objIndSpouseExt.Zip
            End If
        End Sub

        Private Sub PrePopulate()

            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim drIndSpouse As SqlDataReader
            Try
                If Session("EventId") = 9 Then
                    If Session("FundDonorType") = "OWN" Then
                        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.Text, "select  O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.[STATE],O.Zip,Ch.ChapterCode,'OWN' as DonorType,CASE WHEN O.COUNTRY like 'United States' THEN 'US' ELSE O.COUNTRY END as Country  from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE O.AutoMemberID = " & Session("CustIndID"))
                        ' Iterate through DataReader, should only be one 
                        While (drIndSpouse.Read())
                            If Not drIndSpouse.IsDBNull(0) Then
                                nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                                txtAddress1.Text = drIndSpouse("Address1").ToString()
                                txtCity.Text = drIndSpouse("City").ToString()
                                If drIndSpouse("State").ToString().Trim.Length > 0 Then ddlState.Items.FindByValue(drIndSpouse("State").ToString().Trim).Selected = True
                                ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                                txtZip.Text = drIndSpouse("Zip").ToString()
                                lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                            End If
                        End While
                    Else
                        drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndSpouseByID", New SqlParameter("@autoMemberID", Session("CustIndID")))
                        ' Iterate through DataReader, should only be one 
                        While (drIndSpouse.Read())
                            If Not drIndSpouse.IsDBNull(0) Then
                                nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                                txtAddress1.Text = drIndSpouse("Address1").ToString()
                                txtAddress2.Text = drIndSpouse("Address2").ToString()
                                txtCity.Text = drIndSpouse("City").ToString()
                                ddlState.Items.FindByValue(drIndSpouse("State").ToString()).Selected = True
                                ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                                txtZip.Text = drIndSpouse("Zip").ToString()
                                lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                            End If
                        End While
                    End If
                Else
                    drIndSpouse = SqlHelper.ExecuteReader(conn, CommandType.StoredProcedure, "usp_GetIndSpouseByID", New SqlParameter("@autoMemberID", Session("CustIndID")))
                    ' Iterate through DataReader, should only be one 
                    While (drIndSpouse.Read())
                        If Not drIndSpouse.IsDBNull(0) Then
                            nameLabel.Text = drIndSpouse("FirstName").ToString() + " " + drIndSpouse("LastName").ToString()
                            txtAddress1.Text = drIndSpouse("Address1").ToString()
                            txtAddress2.Text = drIndSpouse("Address2").ToString()
                            txtCity.Text = drIndSpouse("City").ToString()
                            ddlState.Items.FindByValue(drIndSpouse("State").ToString()).Selected = True
                            ddlCountry.Items.FindByValue(drIndSpouse("Country").ToString()).Selected = True
                            txtZip.Text = drIndSpouse("Zip").ToString()
                            lblmail.Text = drIndSpouse("Email").ToString() 'Added 19/08/2013  
                        End If
                    End While
                End If
                If Not drIndSpouse Is Nothing Then drIndSpouse.Close()
            Finally
                drIndSpouse = Nothing
            End Try
        End Sub

        Protected Sub PrepareFormData()
            ''bname = txtCardHolderName.Text
            ''baddr1 = txtAddress1.Text
            ''baddr2 = txtAddress2.Text
            ''bstate = ddlState.SelectedValue.ToString
            ''bcity = txtCity.Text
            ''bcountry = ddlCountry.SelectedValue.ToString
            ''bzip = txtZip.Text
            ''cardnumber = txtCardNumber.Text
            ''expmonth = ddlMonth.SelectedValue.ToString
            ''expyear = ddlYear.SelectedValue.ToString
            ''cvmindicator = "not_provided"

            'cvmvalue = Request.Form("cvmvalue")
            'subtotal = Request.Form("subtotal")
            'tax = Request.Form("tax")
            'shipping = nDonationAmt.ToString
            'total = nTotalAmt.ToString

            '***Testing Purposes only
            subtotal = Request.Form("subtotal")
            tax = Request.Form("tax")
            shipping = nDonationAmt.ToString
            total = nTotalAmt.ToString

            refnumber = Request.Form("refnumber")
            If Session("EventID") = 12 Then
                comments = " Donation:" + nDonationAmt.ToString
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            ElseIf (Not (Session("ContestsSelected")) Is Nothing) Then
                comments = CType(Session("ContestsSelected"), String)
                comments = (comments + (" RegFee:" + nRegFee.ToString))
                comments = (comments + (" Mealsamount:" + nMealsAmt.ToString))
                comments = comments + (" LateFee:" + nLateFee.ToString)
                comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            ElseIf (Not (Session("SaleItems")) Is Nothing) Then
                comments = CType(Session("SaleItems"), String)
                comments = (comments + (" SaleAmt:" + SaleAmt.ToString))
                comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            ElseIf (Not (Session("FundRItems")) Is Nothing) Then
                comments = CType(Session("FundRItems"), String)
                comments = (comments + (" FundRAmt:" + FundRAmt.ToString))
                'comments = (comments + (" Donation:" + nDonationAmt.ToString))
                comments = (comments + (" Total:" + nTotalAmt.ToString))
            Else
                comments = ""
            End If

            If Session("EventID") = 10 Then
                referred = ("Sale on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 13 Then
                referred = ("Coaching Registration on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 12 Then
                referred = ("Walkathon Donation on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 9 Then
                referred = ("Fundraising Payment on " + System.DateTime.Now.ToLongTimeString)
            ElseIf Session("EventID") = 3 Then
                referred = ("Workshop Registration on " + System.DateTime.Now.ToLongTimeString)
            Else
                referred = ("Contest Registration on " + System.DateTime.Now.ToLongTimeString)
            End If

            result = Request.Form("result")
            origin = Request.Form("origin")

            configCDE4()
            'GetConfigParams()
        End Sub
        Private Sub TrimTextBoxEntries(ByRef txtbox As TextBox)
            txtbox.Text = txtbox.Text.Trim
        End Sub
        ' Function to Check for CreditCard.
        Public Function IsCreditCard(ByVal strToCheck As String) As Boolean
            Dim objAlphaNumericPattern As Regex = New Regex("^(?:(?<Visa>4\d{3})|(?<Mastercard>5[1-5]\d{2})|(?<Discover>6011)|(?<DinersClub>(?:3[68]\d{2})|(?:30[0-5]\d))|(?<AmericanExpress>3[47]\d{2}))([ -]?)(?(DinersClub)(?:\d{6}\1\d{4})|(?(AmericanExpress)(?:\d{6}\1\d{5})|(?:\d{4}\1\d{4}\1\d{4})))$")
            Return objAlphaNumericPattern.IsMatch(strToCheck)
        End Function

        Private Sub lbContinue_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles lbContinue.Click
            'sandhya - to take care of session time out issues
            ''If LCase(Session("LoggedIn")) <> "true" Then
            ''    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            ''    Exit Sub
            ''End If

            lblCardError.Text = ""
            lblMessage.Text = ""
            TrimTextBoxEntries(txtCardNumber)
            If (IsCreditCard(txtCardNumber.Text) = False) Then
                lblCardError.Text = "Invalid card number"
                Return
            End If
            If ((Convert.ToInt32(ddlMonth.SelectedValue.ToString) < System.DateTime.Today.Month) _
                        AndAlso (Convert.ToInt32(ddlYear.SelectedValue.ToString) _
                        <= (System.DateTime.Today.Year - 2000))) Then
                lblMessage.Text = "For a valid card the Card Expiration must be in future."
                Return
            End If
            If Page.IsValid Then
                TrimTextBoxEntries(txtCardHolderName)
                TrimTextBoxEntries(txtAddress1)
                TrimTextBoxEntries(txtAddress2)
                TrimTextBoxEntries(txtCity)
                TrimTextBoxEntries(txtZip)
                lblMessage.Text = ""
                lbContinue.Enabled = False
                ' Parse form data
                PrepareFormData()
                ' process order
                ProcessOrder()
            End If
        End Sub

        Private Sub ProcessOrder()
            ' '' create order
            ''Dim order As LPOrderPart = LPOrderFactory.createOrderPart("order")
            ' '' create a part we will use to build the order
            ''Dim op As LPOrderPart = LPOrderFactory.createOrderPart
            ' '' Build 'orderoptions'
            ''op.put("ordertype", "SALE")
            ' '' add 'orderoptions to order
            ''order.addPart("orderoptions", op)
            ' '' add notes   
            ''op.clear()
            ''op.put("comments", comments)
            ''op.put("referred", referred)
            ''order.addPart("notes", op)
            ' '' Build 'merchantinfo'
            ''op.clear()
            ''op.put("configfile", configfile)
            ' '' add 'merchantinfo to order
            ''order.addPart("merchantinfo", op)
            ' '' Build 'billing'
            ' '' Required for AVS. If not provided, 
            ' '' transactions will downgrade.
            ''op.clear()
            ''op.put("name", bname)
            ''op.put("address1", baddr1)
            ''op.put("address2", baddr2)
            ''op.put("city", bcity)
            ''op.put("state", bstate)
            ''op.put("country", bcountry)
            ''op.put("zip", bzip)
            ''op.put("addrnum", baddrnum)
            ' '' add 'billing to order
            ''order.addPart("billing", op)
            ' '' Build 'creditcard'
            ''op.clear()
            ''op.put("cardnumber", cardnumber)
            ''op.put("cardexpmonth", expmonth)
            ''op.put("cardexpyear", expyear)
            ' '' add 'creditcard to order
            ''order.addPart("creditcard", op)
            ' '' Build 'payment'
            ''op.clear()
            ''op.put("chargetotal", total)
            '' ''op.put("shipping", shipping)
            ' '' add 'payment to order
            ''order.addPart("payment", op)

            '' ''Start add Items
            '' '' Create some parts we use to build order itmes
            ''Dim items As LPOrderPart = LPOrderFactory.createOrderPart()
            ''Dim item As LPOrderPart = LPOrderFactory.createOrderPart()
            ''Dim itemcnt As Int16
            ''itemcnt = 1
            ' '' build fee item
            ''If nRegFee > 0 Then
            ''    item.put("id", 1001)
            ''    item.put("description", "Fee")
            ''    item.put("quantity", Convert.ToInt32(nRegFee).ToString)
            ''    item.put("price", 1)
            ''    'add 'item' to 'items' collection
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ' '' build donation item
            ''If shipping > 0 Then
            ''    item.clear()
            ''    item.put("id", 1002)
            ''    item.put("description", "Donation")
            ''    item.put("quantity", Convert.ToInt32(shipping).ToString)
            ''    item.put("price", 1)
            ''    'add 'item' to 'items' collection
            ''    itemcnt += 1
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ' '' build latefee
            ''If nLateFee > 0 Then
            ''    item.clear()
            ''    item.put("id", 1003)
            ''    item.put("description", "Late Fee")
            ''    item.put("quantity", Convert.ToInt32(nLateFee).ToString)
            ''    item.put("price", 1)
            ''    'add 'latefee' to 'items' collection
            ''    itemcnt += 1
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ' '' build mealsamount
            ''If nMealsAmt > 0 Then
            ''    item.clear()
            ''    item.put("id", 1004)
            ''    item.put("description", "Meals")
            ''    item.put("quantity", Convert.ToInt32(nMealsAmt).ToString)
            ''    item.put("price", 1)
            ''    'add 'latefee' to 'items' collection
            ''    itemcnt += 1
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ''If SaleAmt > 0 Then
            ''    item.clear()
            ''    item.put("id", 1006)
            ''    item.put("description", "Shop")
            ''    item.put("quantity", Convert.ToInt32(SaleAmt).ToString)
            ''    item.put("price", 1)
            ''    'add 'latefee' to 'items' collection
            ''    itemcnt += 1
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ''If FundRAmt > 0 Then
            ''    item.clear()
            ''    item.put("id", 1007)
            ''    item.put("description", "Fundraising")
            ''    item.put("quantity", Convert.ToInt32(FundRAmt).ToString)
            ''    item.put("price", 1)
            ''    'add 'latefee' to 'items' collection
            ''    itemcnt += 1
            ''    items.addPart("item", item, itemcnt)
            ''End If

            ' '' add 'items' to order
            ''order.addPart("items", items)

            '' ''End add Items

            ' '' create transaction object    
            ''Dim LPTxn As LinkPointTxn = New LinkPointTxn
            ' '' get outgoing XML from the 'order' object
            ''Dim outXml As String = order.toXML

            ''Dim resp As String = Nothing

            ''Try
            ''    ' Call LPTxn
            ''    resp = LPTxn.send(keyfile, host, port, outXml)
            ''Catch
            ''    Response.Write("Error processing payment information")
            ''End Try

            ''Try
            ''    'Session(outXml) = outXml.ToString   'Not sure what this is for (Meena)
            ''    Session("outXml") = outXml.ToString
            ''    ParseResponse(resp)
            ''Catch
            ''    Response.Write("There was an error in parsing the response back from LP<BR>")
            ''    Response.Write(outXml.ToString)
            ''    If (isTestMode = True) Then
            ''        Response.Write("<BR>Debug:Resp back from LP :" + resp)
            ''        Response.Write("<br>")
            ''    End If

            ''End Try
            ''lblMessage.Text = ""


            '*************************************************
            '*               Modified 19/08/2013             *
            '*************************************************

            Dim ws As New GDE4.Service()
            Dim txn As New GDE4.Transaction()
            Dim cAddress As String
            PreUpdate()
            Try
                cAddress = txtAddress1.Text + "|" + txtZip.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + ddlCountry.SelectedValue.ToString
                'txn.ExactID = "AD7642-07" 'User Name
                'txn.Password = "kg5vh7t2" 'Password
                txn.ExactID = e4_Id
                txn.Password = e4_Pass
                txn.Transaction_Type = "01" 'Purchase 04 for Refund
                txn.CardHoldersName = txtCardHolderName.Text.Trim()
                txn.Card_Number = txtCardNumber.Text.Trim() '"4111111111111111" '
                txn.DollarAmount = nTotalAmt.ToString("f2")
                txn.Expiry_Date = gDate()
                txn.VerificationStr1 = cAddress.Trim()
                'txn.VerificationStr2 = txtcvv.Text.Trim() "not used in reg_pay"
                txn.Client_Email = lblmail.Text.Trim()
                txn.Client_IP = lblIP.Text.Trim()
                txn.Customer_Ref = Cust_id.ToString()

                Dim result As GDE4.TransactionResult = ws.SendAndCommit(txn)
                e4_BankMessage = result.Bank_Message.ToString()
                e4_Auth = result.Authorization_Num.ToString()
                e4_result = result.CTR
                e4_Message = result.EXact_Message.ToString()
                e4_Error = result.Error_Description.ToString()
                e4_Code = result.EXact_Resp_Code.ToString()

                'Label2.Text = result.CTR.ToString + "<br>" + result.Customer_Ref.ToString() + "<br>" _
                '+ result.VerificationStr1.ToString() + "<br>" + result.Bank_Message.ToString() + "<br>" + result.Bank_Resp_Code.ToString() + "<br>" + result.Bank_Resp_Code_2.ToString()

            Catch ex As Exception
                'Label2.ForeColor = System.Drawing.Color.Red
                'Label2.Text = ex.ToString
                Response.Write(ex.ToString)
                lbContinue.Enabled = True
            End Try

            PostUpdate()
            ' If (R_Approved = "APPROVED") Then
            If (e4_BankMessage = "Approved") Then
                'Store transaction data on Session and redirect
                'Session("outXml") = outXml
                'Session("resp") = resp

                If (SavePaymentInfo() = True) Then
                    'Session("PaymentReference") = R_OrderNum  'added - 1/15/07
                    Session("PaymentReference") = e4_Auth
                    Session("R_Approved") = "APPROVED"
                    If Session("EventId") = 3 Then
                        Response.Redirect("Wrkshop_Success.aspx")
                    ElseIf Session("EventId") = 13 Then
                        Response.Redirect("CoachingSuccess.aspx")
                    ElseIf Session("EventId") = 10 Then
                        Response.Redirect("SaleSuccess.aspx")
                    ElseIf Session("EventId") = 9 Then
                        Response.Redirect("FundRSuccess.aspx")
                    Else
                        Response.Redirect("reg_Success_Final2.aspx")
                    End If

                Else
                    lblMessage.Text = "Error while saving the payment information to NFG_TRANSACTIONS table, but the Credit Card Transaction is successful."
                End If
            Else
                lblMessage.Text = "We have encountered the following error while processing your credit card"
                lblMessage.Text = (lblMessage.Text + ("<BR>Card Status:" + e4_BankMessage)) 'Approved
                lblMessage.Text = (lblMessage.Text + ("<BR>Error:" + e4_Error)) 'Error
                lblMessage.Text = (lblMessage.Text + ("<BR>Message:" + e4_Message)) 'Message
                lblMessage.Text = (lblMessage.Text + ("<BR>ResponseCode:" + e4_Code)) 'Response Code
                lblMessage.Text = (lblMessage.Text + "<BR>Please check card information and try again")
                tblError.Visible = True
                Try
                    Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
                    Dim eventId As Integer = 0
                    Dim eventCode As String
                    If (Not Session("EventID") Is Nothing) Then
                        eventId = CInt(Session("EventID").ToString)
                    End If
                    eventCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", eventId))

                    Dim param(10) As SqlParameter
                    param(0) = New SqlParameter("@ChapterID", Session("ChapterId"))
                    param(1) = New SqlParameter("@EventID", Session("EventId"))
                    param(2) = New SqlParameter("@EventCode", eventCode)
                    param(3) = New SqlParameter("@EventYear", Session("EventYear").ToString)
                    param(4) = New SqlParameter("@MemberID", Session("CustIndId"))
                    param(5) = New SqlParameter("@CardStatus", e4_BankMessage)
                    param(6) = New SqlParameter("@Error", e4_Error)
                    param(7) = New SqlParameter("@Message", e4_Message)
                    param(8) = New SqlParameter("@FraudCode", e4_Code)
                    param(9) = New SqlParameter("@CreateDate", Now())
                    SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_CCError_Insert", param)
                    'write code Send email.
                    SendDasMessage("Error While processing your credit card", lblMessage.Text, "chitturi9@gmail.com")

                    lbContinue.Enabled = True
                    tblError.Visible = True
                Catch

                End Try

            End If
        End Sub

        Private Function gDate() As String
            Dim dt As String
            dt = ddlMonth.SelectedValue.ToString() + Right(ddlYear.Text, 2)
            Return dt
        End Function

        Private Sub GetIPAddress(ByRef ilbl As Label)
            Dim IPAdd As String = String.Empty
            Try
                IPAdd = Request.ServerVariables("HTTP_X_FORWARDED_FOR")
                If String.IsNullOrEmpty(IPAdd) Then
                    IPAdd = Request.ServerVariables("REMOTE_ADDR")
                End If

                ilbl.Text = IPAdd.ToString
            Catch ex As Exception
                Response.Write("Error getting IP Address : " + ex.Message.ToString)
            End Try

        End Sub

        Private Function SavePaymentInfo() As Boolean

            'save to NFG transactions
            Dim sb As StringBuilder = New StringBuilder
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim eventId As Integer = 0
            Dim eventCode As String
            Dim parentLName As String = ""
            Dim parentFName As String = ""
            Dim parentEmail As String = ""
            Dim DonorType As String = ""
            Dim NFG_Flag As Boolean = False
            'Dim loginEmail As String = ""
            Dim custIndId As String = DBNull.Value.ToString
            Dim ds As DataSet

            If (Not Session("EventID") Is Nothing) Then
                eventId = CInt(Session("EventID").ToString)
            End If
            eventCode = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetEventCodeByID", New SqlParameter("@EventId", eventId))

            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If

            Try
                If Session("EventId") = 9 Then
                    If Session("FundDonorType") = "OWN" Then
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select ORGANIZATION_NAME as firstName,email from OrganizationInfo where automemberId = " + custIndId)
                        If (ds.Tables.Count > 0) Then
                            If (ds.Tables(0).Rows.Count > 0) Then
                                parentLName = ""
                                parentFName = ds.Tables(0).Rows(0)("firstname")
                                If ds.Tables(0).Rows(0)("email").ToString().Trim.Length > 0 Then parentEmail = ds.Tables(0).Rows(0)("email").ToString().Trim
                                DonorType = "OWN"
                            End If
                        End If
                    Else
                        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstName,lastName,email from indspouse where automemberId = " + custIndId)
                        If (ds.Tables.Count > 0) Then
                            If (ds.Tables(0).Rows.Count > 0) Then
                                parentLName = ds.Tables(0).Rows(0)("lastname")
                                parentFName = ds.Tables(0).Rows(0)("firstname")
                                parentEmail = ds.Tables(0).Rows(0)("email")
                                DonorType = "IND"
                            End If
                        End If
                    End If
                Else
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, "select firstName,lastName,email from indspouse where automemberId = " + custIndId)
                    If (ds.Tables.Count > 0) Then
                        If (ds.Tables(0).Rows.Count > 0) Then
                            parentLName = ds.Tables(0).Rows(0)("lastname")
                            parentFName = ds.Tables(0).Rows(0)("firstname")
                            parentEmail = ds.Tables(0).Rows(0)("email")
                            DonorType = "IND"
                        End If
                    End If
                End If
            Catch ex As Exception
                Response.Write(ex.ToString())
                Exit Function
            End Try

            'If (Not Session("parentLName") Is Nothing) Then
            '    parentLName = Session("parentLName").ToString
            'End If
            'If (Not Session("parentFName") Is Nothing) Then
            '    parentFName = Session("parentFName").ToString
            'End If
            'If (Not Session("LoginEmail") Is Nothing) Then
            '    loginEmail = Session("LoginEmail").ToString
            'End If

            ' Commmented and replace below to avoid duplicate reocrds in NFG_Transactions  27-03-2013

            'sb.Append(" INSERT INTO NFG_TRANSACTIONS([Last Name], ")
            'sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
            'sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
            'sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
            'sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear,PaymentNotes,DonorType")
            'sb.Append(" )  VALUES ( '<LASTNAME>', ")
            'sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
            'sb.Append(" GETDATE(), 'NSF', <DONATIONAMOUNT>, GETDATE(), ")
            'sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', <MEALSAMOUNT>, <LATEFEE>, ")
            'sb.Append(" <EVENTID>,'<EVENTFOR>','<DONATIONPURPOSE>',<PARENTID>,<CHAPTERID>,<REGFEE>,<TOTALAMOUNT>,'<EVENTYEAR>','<COMMENTS>','" & DonorType & "')")
            ''******

            If (nDonationAmt > 0) Then
                'save to donations info
                sb.Append(" declare @donationnumber int ")
                sb.Append(" SELECT @donationnumber = MAX(DonationNumber) + 1 ")
                sb.Append(" FROM DonationsInfo WHERE [MEMBERID]=<PARENTID> ")
                sb.Append(" ")
                sb.Append("  if @donationnumber is null ")
                sb.Append("	begin ")
                sb.Append(" set @donationnumber = 1 ")
                sb.Append("	end ")
                sb.Append(" else ")
                sb.Append(" begin ")
                sb.Append(" set @donationnumber = @donationnumber + 1  ")
                sb.Append("	end ")
                sb.Append(" INSERT INTO DonationsInfo([MEMBERID], [DonationNumber], [DonorType],  ")
                sb.Append(" [AMOUNT], [TRANSACTION_NUMBER], [Anonymous], [DonationDate], [METHOD],  ")
                sb.Append(" [PURPOSE], [EVENT], [STATUS], [CreateDate], [DeletedFlag], ")
                sb.Append(" [EVENTID],[EVENTYEAR],[CHAPTERID],[TAXDEDUCTION]) ")
                sb.Append(" VALUES(<PARENTID>, @donationnumber, 'IND',  ")
                sb.Append(" <DONATIONAMOUNT>, '<PAYMENTREFERENCE>','No', getdate(), 'Credit Card',  ")
                sb.Append(" '<DONATIONPURPOSE>', 'Registration', 'Completed', getdate(), 'No', ")
                sb.Append(" <EVENTID>, '<EVENTYEAR>', <CHAPTERID>, <TAXDEDUCTION>) ")
            End If
            'save to Contestants
            Try

                If Session("EventId") = 3 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE Registration ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" and EventDate  >= GetDate() And  Eventyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From Registration WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND EVENTID=" & Session("EventId") & " and EventDate  >= GetDate() And  Eventyear >= " & Year(Today()) & " ") > 0 Then
                        NFG_Flag = True
                    End If

                ElseIf Session("EventId") = 12 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE WMSponsor ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>', ")
                    sb.Append(" [PaidAmount]=<TOTALAMOUNT> ")
                    sb.Append(" WHERE [WMSponsorID]=" & Session("WMSponsorID") & "")

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From WMSponsor WHERE [WMSponsorID]=" & Session("WMSponsorID") & " and PaymentReference is null") > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 13 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE CR ")
                    sb.Append(" SET CR.[PaymentDate]=getdate(),  ")
                    sb.Append(" CR.[PaymentMode]='Credit Card',  ")
                    sb.Append(" CR.[PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" CR.[PaymentReference]='<PAYMENTREFERENCE>', CR.Approved='Y' ")
                    sb.Append(" FROM CoachReg CR INNER JOIN COACHCAL C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo   WHERE CR.[PaymentReference] is NULL  and GETDATE()< C.Enddate and CR.Approved='N' and CR.[PMemberID]=<PARENTID> ")
                    sb.Append(" AND CR.EVENTID=" & Session("EventId"))
                    sb.Append(" And  CR.Eventyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) FROM CoachReg CR INNER JOIN COACHCAL C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo   WHERE CR.[PaymentReference] is NULL  and GETDATE()< C.Enddate and CR.Approved='N' and CR.[PMemberID]=" & custIndId & "   AND CR.EVENTID=" & Session("EventId") & " And  CR.Eventyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 10 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE SaleTran ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> ")
                    sb.Append(" AND EVENTID=" & Session("EventId"))
                    sb.Append(" And  Eventyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From SaleTran WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & "  AND EVENTID=" & Session("EventId") & " And  Eventyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                ElseIf Session("EventId") = 9 Then
                    sb.Append(" ")
                    sb.Append(" UPDATE FundRReg ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID"))
                    sb.Append(" AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "'")
                    sb.Append(" And  Eventyear <= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) From FundRReg WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID") & " AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "' And  Eventyear <= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If

                    sb.Append("; UPDATE BeeRankSponsor ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [MemberID]=<PARENTID> AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID"))
                    sb.Append(" AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "'")
                    sb.Append(" And  Eventyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From BeeRankSponsor  WHERE [PaymentReference] is NULL and [MemberID]=" & custIndId & " AND PaymentMode = 'Credit Card'  AND  FundRCalID =" & Session("FundRCalID") & "  AND EVENTID=" & Session("EventId") & " AND DonorType ='" & DonorType & "' And  Eventyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                Else
                    sb.Append(" ")
                    sb.Append(" UPDATE C ")
                    sb.Append(" SET C.[PaymentDate]=getdate(),  ")
                    sb.Append(" C.[PaymentMode]='Credit Card',  ")
                    sb.Append(" C.[PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" C.[PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" From Contestant C INNER JOIN Contest b ON b.ContestID = C.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=b.Contest_Year and Ex.EventID=b.EventId and Ex.ChildNumber=C.ChildNumber")
                    sb.Append(" WHERE C.[PaymentReference] Is NULL And C.[ParentID]= <PARENTID> ")
                    sb.Append(" And  C.Contestyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From  Contestant C INNER JOIN Contest b ON b.ContestID = C.Contestcode Left Join ExContestant Ex On Ex.ChapterID=b.NSFChapterID and Ex.ProductID=b.ProductId and Ex.ProductGroupID=b.ProductGroupId and Ex.ContestYear=b.Contest_Year and Ex.EventID=b.EventId and Ex.ChildNumber=C.ChildNumber WHERE C.[PaymentReference] Is NULL And C.[ParentID]= " & custIndId & "  And  C.Contestyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                End If

                'save meals charge.
                If (eventId = 1) Then
                    sb.Append(" ")
                    sb.Append(" UPDATE MealCharge ")
                    sb.Append(" SET [PaymentDate]=getdate(),  ")
                    sb.Append(" [PaymentMode]='Credit Card',  ")
                    sb.Append(" [PaymentNotes]='<COMMENTS>', ")
                    sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
                    sb.Append(" WHERE [PaymentReference] is NULL and [AutoMemberID]=<PARENTID>  ")
                    sb.Append(" And  Contestyear >= " & Year(Today()))

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) From MealCharge  WHERE [PaymentReference] is NULL and [AutoMemberID]=" & custIndId & "  And  Contestyear >= " & Year(Today())) > 0 Then
                        NFG_Flag = True
                    End If
                End If


                If NFG_Flag = True Then
                    sb.Append(" INSERT INTO NFG_TRANSACTIONS([Last Name], ")
                    sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
                    sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
                    sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
                    sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear,PaymentNotes,DonorType")
                    sb.Append(" )  VALUES ( '<LASTNAME>', ")
                    sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
                    sb.Append(" GETDATE(), 'NSF', <DONATIONAMOUNT>, GETDATE(), ")
                    sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', <MEALSAMOUNT>, <LATEFEE>, ")
                    sb.Append(" <EVENTID>,'<EVENTFOR>','<DONATIONPURPOSE>',<PARENTID>,<CHAPTERID>,<REGFEE>,<TOTALAMOUNT>,'<EVENTYEAR>','<COMMENTS>','" & DonorType & "')")
                Else
                    sb.Append(" INSERT INTO PaymentError([Last Name], ")
                    sb.Append(" [First Name], Address, City, State, Zip, [Email (ok to contact)],")
                    sb.Append(" [Contribution Date], [Source Website], [Contribution Amount], [Payment Date], ")
                    sb.Append(" Status, approval_status, approval_code, asp_session_id, MealsAmount, LateFee,")
                    sb.Append(" Eventid, Event_for,[Designated Project],memberid,chapterid,Fee,TotalPayment,eventYear,PaymentNotes,DonorType")
                    sb.Append(" )  VALUES ( '<LASTNAME>', ")
                    sb.Append(" '<FIRSTNAME>', '<ADDRESS>', '<CITY>', '<STATE>', '<ZIP>', '<PARENTEMAIL>',")
                    sb.Append(" GETDATE(), 'NSF', <DONATIONAMOUNT>, GETDATE(), ")
                    sb.Append(" '<STATUS>', '<APPROVAL_STATUS>', '<APPROVAL_CODE>', '<PAYMENTREFERENCE>', <MEALSAMOUNT>, <LATEFEE>, ")
                    sb.Append(" <EVENTID>,'<EVENTFOR>','<DONATIONPURPOSE>',<PARENTID>,<CHAPTERID>,<REGFEE>,<TOTALAMOUNT>,'<EVENTYEAR>','<COMMENTS>','" & DonorType & "')")

                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
            'save to contest charity
            sb.Append(" ")
            sb.Append(" UPDATE Contest_Charity ")
            sb.Append(" SET [PaymentDate]=getdate(),  ")
            sb.Append(" [PaymentMode]='Credit Card', ")
            sb.Append(" [PaymentNotes]='<COMMENTS>', ")
            sb.Append(" [PaymentReference]='<PAYMENTREFERENCE>' ")
            sb.Append(" WHERE [PaymentReference] is NULL and [MEMBERID]=<PARENTID>  ")

            'now replace all the parameters with actual values
            sb.Replace("<PARENTID>", custIndId)
            sb.Replace("<LASTNAME>", parentLName.Replace("'", "''"))
            sb.Replace("<FIRSTNAME>", parentFName.Replace("'", "''"))
            sb.Replace("<ADDRESS>", (txtAddress1.Text.Trim + (" " + txtAddress2.Text.Trim))) ' to be modified
            'sb.Replace("<ADDRESS>", (baddr1.Trim + (" " + baddr2.Trim)).Replace("'", "''"))
            'sb.Replace("<CITY>", bcity.Trim.Replace("'", "''"))
            'sb.Replace("<STATE>", bstate.Replace("'", "''"))
            'sb.Replace("<ZIP>", bzip)
            sb.Replace("<CITY>", txtCity.Text)
            sb.Replace("<STATE>", ddlState.SelectedValue.ToString)
            sb.Replace("<ZIP>", txtZip.Text)
            sb.Replace("<PARENTEMAIL>", parentEmail)
            sb.Replace("<REGFEE>", nRegFee.ToString)
            sb.Replace("<DONATIONAMOUNT>", nDonationAmt.ToString)
            'sb.Replace("<PAYMENTREFERENCE>", R_OrderNum)
            'sb.Replace("<APPROVAL_CODE>", R_Code)
            'sb.Replace("<APPROVAL_STATUS>", R_Approved)
            sb.Replace("<PAYMENTREFERENCE>", e4_Auth)
            sb.Replace("<APPROVAL_CODE>", Cust_id)
            sb.Replace("<APPROVAL_STATUS>", e4_BankMessage)
            sb.Replace("<STATUS>", "Complete")
            sb.Replace("<COMMENTS>", comments.Replace("'", "''"))
            sb.Replace("<DONATIONPURPOSE>", Session("DONATIONFOR").ToString)
            sb.Replace("<MEALSAMOUNT>", nMealsAmt.ToString)
            sb.Replace("<LATEFEE>", nLateFee.ToString)
            sb.Replace("<EVENTID>", eventId)
            sb.Replace("<EVENTFOR>", eventCode)
            sb.Replace("<EVENTYEAR>", Session("EventYear").ToString)
            sb.Replace("<CHAPTERID>", Session("CustIndChapterID").ToString) 'coming from reg_donate page.
            sb.Replace("<TAXDEDUCTION>", CType(100, String))
            sb.Replace("<TOTALAMOUNT>", nTotalAmt.ToString)
            Dim saved As Boolean = True
            Dim test As String = sb.ToString
            'lbltest.Text = test
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, sb.ToString)
            Catch se As SqlException
                SendDasMessage("NSF - Error In Payment", sb.ToString() + "<br> Error <br>" + se.ToString(), "j_remilton@yahoo.com")
                lblMessage.Text = "The following error occured while saving the payment information.Please contact for technical support" & _
                "."
                lblMessage.Text = (lblMessage.Text + se.Message)
                tblError.Visible = True
                saved = False
            End Try
            Return saved
        End Function

        Private Sub lbRegistration_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbRegistration.Click
            Dim redirectURL As String
            If Session("entryToken") = "Volunteer" Then
                redirectURL = "VolunteerFunctions.aspx"
            ElseIf Application("EventID") = 1 Then
                redirectURL = "UserFunctions.aspx"
            ElseIf Session("EventId") = 3 Then
                redirectURL = "UserFunctions.aspx"
            ElseIf Session("EventId") = 13 Then
                redirectURL = "UserFunctions.aspx"
            ElseIf Session("EventId") = 10 Then
                redirectURL = "UserFunctions.aspx"
            ElseIf Session("EventId") = 9 And Session("entryToken") = "Parent" Then
                redirectURL = "UserFunctions.aspx"
            ElseIf Session("EventId") = 9 And Session("entryToken") = "Donor" Then
                redirectURL = "DonorFunctions.aspx"
            Else
                redirectURL = "UserFunctions.aspx"
            End If
            Response.Redirect(redirectURL)
        End Sub

        Private Sub SendDasMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)

            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host
            Try
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End Sub

        Private Sub PreUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim cAddress As String
            Dim eventId As Integer = 0
            Dim ccL As String
            Dim ccF As String
            Dim pName As String = System.IO.Path.GetFileNameWithoutExtension(Request.Path)
            Dim sqlparams(27) As SqlClient.SqlParameter

            ccL = Right(txtCardNumber.Text, 4)
            ccF = Left(txtCardNumber.Text, 4)
            If (Not Session("EventID") Is Nothing) Then
                eventId = CInt(Session("EventID").ToString)
            End If

            If (Not Session("CustIndId") Is Nothing) Then
                custIndId = Session("CustIndId").ToString
            End If
            cAddress = txtAddress1.Text + "|" + txtZip.Text + "|" + txtCity.Text + "|" + ddlState.SelectedValue.ToString + "|" + ddlCountry.SelectedValue.ToString
            sqlparams(0) = New SqlParameter("@MemberID", SqlDbType.Int)
            sqlparams(0).Value = CInt(custIndId.ToString)
            sqlparams(1) = New SqlParameter("@EventYear", SqlDbType.Int)
            sqlparams(1).Value = CInt(Now.Year.ToString)
            sqlparams(2) = New SqlParameter("@EventID", SqlDbType.Int)
            sqlparams(2).Value = CInt(eventId.ToString)
            sqlparams(3) = New SqlParameter("@AspxPage", SqlDbType.VarChar)
            sqlparams(3).Value = pName.ToString
            sqlparams(4) = New SqlParameter("@PaymentDate", SqlDbType.SmallDateTime)
            sqlparams(4).Value = System.DateTime.Now.ToString
            sqlparams(5) = New SqlParameter("@IPAddress", SqlDbType.VarChar)
            sqlparams(5).Value = lblIP.Text
            sqlparams(6) = New SqlParameter("@TransType", SqlDbType.VarChar)
            sqlparams(6).Value = "01"
            sqlparams(7) = New SqlParameter("@Platform", SqlDbType.VarChar)
            sqlparams(7).Value = "Demo"
            sqlparams(8) = New SqlParameter("@StatusSend", SqlDbType.VarChar)
            sqlparams(8).Value = "Sent"
            sqlparams(9) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(9).Value = DBNull.Value
            sqlparams(10) = New SqlParameter("@Amount", SqlDbType.Float)
            sqlparams(10).Value = nTotalAmt.ToString("f2")
            sqlparams(11) = New SqlParameter("@Fee", SqlDbType.Float)
            sqlparams(11).Value = System.DBNull.Value
            sqlparams(12) = New SqlParameter("@LateFee", SqlDbType.Float)
            sqlparams(12).Value = System.DBNull.Value
            sqlparams(13) = New SqlParameter("@Meal", SqlDbType.Float)
            sqlparams(13).Value = System.DBNull.Value
            sqlparams(14) = New SqlParameter("@Donation", SqlDbType.Float)
            sqlparams(14).Value = nTotalAmt.ToString("f2")
            sqlparams(15) = New SqlParameter("@CCF4", SqlDbType.VarChar)
            sqlparams(15).Value = ccF.ToString
            sqlparams(16) = New SqlParameter("@CCL4", SqlDbType.VarChar)
            sqlparams(16).Value = ccL.ToString
            sqlparams(17) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(17).Value = System.DBNull.Value
            sqlparams(18) = New SqlParameter("@CCName", SqlDbType.VarChar)
            sqlparams(18).Value = txtCardHolderName.Text
            sqlparams(19) = New SqlParameter("@CCZip", SqlDbType.VarChar)
            sqlparams(19).Value = txtZip.Text
            sqlparams(20) = New SqlParameter("@ClientEmail", SqlDbType.VarChar)
            sqlparams(20).Value = lblMail.Text
            sqlparams(21) = New SqlParameter("@PaymentNotes", SqlDbType.VarChar)
            sqlparams(21).Value = comments.ToString()
            sqlparams(22) = New SqlParameter("@CreateDate", SqlDbType.SmallDateTime)
            sqlparams(22).Value = System.DateTime.Now.ToString
            sqlparams(23) = New SqlParameter("@CreatedBy", SqlDbType.Int)
            sqlparams(23).Value = CInt(custIndId.ToString)
            sqlparams(24) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(24).Value = System.DateTime.Now.ToString
            sqlparams(25) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(25).Value = custIndId
            sqlparams(26) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(26).Direction = ParameterDirection.Output
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_InsertCCSubmitLog", sqlparams)
                Cust_id = sqlparams(26).Value.ToString()
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support" & _
              "."
                lblMessage.Text = (lblMessage.Text + ex.Message)
                tblError.Visible = True

            End Try

        End Sub

        Private Sub PostUpdate()
            Dim conn As SqlConnection = New SqlConnection(Application("ConnectionString"))
            Dim sqlparams(6) As SqlClient.SqlParameter

            sqlparams(0) = New SqlParameter("@StatusReturn", SqlDbType.VarChar)
            sqlparams(0).Value = e4_BankMessage
            sqlparams(1) = New SqlParameter("@PaymentReference", SqlDbType.VarChar)
            sqlparams(1).Value = e4_Auth
            sqlparams(2) = New SqlParameter("@Modifydate", SqlDbType.SmallDateTime)
            sqlparams(2).Value = System.DateTime.Now
            sqlparams(3) = New SqlParameter("@ModifiedBy", SqlDbType.Int)
            sqlparams(3).Value = custIndId
            sqlparams(4) = New SqlParameter("@CCSubmitLogID", SqlDbType.BigInt)
            sqlparams(4).Value = Cust_id.ToString
            Try
                SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_UpdateCCSubmitLog", sqlparams)
            Catch ex As Exception
                lblMessage.Text = "The following error occured while saving the payment information to CCSubmitLog.Please contact for technical support" & _
              "."
                lblMessage.Text = (lblMessage.Text + ex.Message)
                tblError.Visible = True

            End Try


            'Dim cmd As New SqlCommand()
            'cmd.CommandType = CommandType.StoredProcedure
            'cmd.CommandText = "usp_UpdateCCSubmitLog"
            'cmd.Parameters.Add("@StatusReturn", SqlDbType.VarChar).Value = e4_BankMessage
            'cmd.Parameters.Add("@PaymentReference", SqlDbType.VarChar).Value = e4_Auth
            'cmd.Parameters.Add("@Modifydate", SqlDbType.SmallDateTime).Value = System.DateTime.Now
            'cmd.Parameters.Add("@ModifiedBy", SqlDbType.Int).Value = custIndId
            'cmd.Parameters.Add("@CCSubmitLogID", SqlDbType.BigInt).Value = Cust_id.ToString
            'cmd.Connection = conn
            'Try
            '    conn.Open()
            '    cmd.ExecuteNonQuery()
            'Catch ex As Exception
            '    Response.Write(ex.ToString)
            'Finally
            '    'conn.Close()
            '    'conn.Dispose()
            'End Try
        End Sub

        Sub configCDE4()
            Dim G4 As NameValueCollection = CType(ConfigurationManager.GetSection("GDE4/crd"), NameValueCollection)
            e4_Id = G4("E4UserName")
            e4_Pass = G4("E4Password")
            'lblStatus.Text = "Config File : " + d1 _
            '+ "<br>" + "Key File : " + d2 + "<br>" _
            '+ "Host : " + d3 + "<br>" + "Port : " + d4 _
            '+ "<br>" + "Result : " + d5
        End Sub
    End Class

End Namespace


