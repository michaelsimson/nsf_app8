<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true"
    CodeFile="ManageCoachPapers.aspx.cs" Inherits="ManageTestPapers" Title="Manage Coach Papers" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <link href="css/jquery.qtip.min.css" rel="stylesheet" />

    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>
    <script type="text/javascript">
        function ConfirmRepOverWrite(RepToYear, RepToWeekNo) {
            var str;
            if (RepToWeekNo == 0) {
                str = "Are you sure you want to override all the files from year " + RepToYear + "?";
            }
            else {
                str = "Are you sure you want to override the files of week " + RepToWeekNo + " from year " + RepToYear + "?";
            }
            if (confirm(str)) {
                document.getElementById('<%= btnRepConfirmToSave.ClientID %>').click();
            }
        }
        function ConfirmWithWeekSetNo(RepToYear) {
            if (confirm("We don�t recommend copying to a different week. Are you sure you want to copy to a different week?")) {
                document.getElementById('<%= btnRepConfirmToSubmit.ClientID %>').click();
            }
        }
        function Confirm() {
            var confirm_value = document.createElement("INPUT");
            confirm_value.type = "hidden";
            confirm_value.name = "confirm_value";
            if (confirm("Are you sure there is no Question Paper for the students this period?")) {
                confirm_value.value = "Yes";
            } else {
                confirm_value.value = "No";
            }

            document.forms[0].appendChild(confirm_value);
        }

        $(function (e) {

            $("#ancUploadHelp").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {

                        var dvHtml = "";
                        dvHtml += "<div><center><span style='font-weight:bold; color: blue; font-size:16px;'>Doc Type</span></center></div> <div style='clear:both; margin-bottom:10px;'></div>";
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">Homework is designated as Q (questions). Q is the primary document to be uploaded each week. Documents A, S, T are supplements to follow with Q. Without uploading Q first, you should not upload the supplements. </span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += "<div><center><span style='font-weight:bold; color: blue; font-size:16px;'>Set#</span></center></div> <div style='clear:both; margin-bottom:10px;'></div>";
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">Set# is the same as Week# in the current implementation. A, S, T will have the same Set# as Q. </span></div> ';

                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += "<div><center><span style='font-weight:bold; color: green; font-size:16px;'>File Name Convention Format</span></center></div> <div style='clear:both; margin-bottom:10px;'></div>";
                        dvHtml += "<div><span style='font-weight:bold; color: blue; font-size:16px;'>  Default File Format:</span></div> <div style='clear:both; margin-bottom:10px;'></div>";

                        dvHtml += "<div><span style='font-weight:bold;font-size:16px;'> 2013_Coaching_HW_MB_MB2_INT_Set2_Wk2_Sec1_Q.zip</span></div> <div style='clear:both; margin-bottom:10px;'></div>";

                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">';
                        dvHtml += " <ul><li>Year (2013)</li> <li>Event (Coaching)</li> <li>Paper Type (HW)</li> <li>Product Group (MB)</li> <li>Product Code (MB2)</li> <li>Level (INT) - If level column is disabled, then the naming would be 'NA'</li> <li>Set Number (Set2)</li> <li>Week Number (Wk2)</li> <li>Section Number (Sec1)</li> <li> Document Type (Q)</li></ul>  </div> ";
                        dvHtml += '<div style="clear:both; margin-bottom:20px;"></div>';
                        dvHtml += "<div><span style='font-weight:bold;font-size:16px;'> If Level Colum contains no data or disabled, then the uploading file name would be the following</span></div> <div style='clear:both; margin-bottom:10px;'></div>";

                        dvHtml += "<div><span style='font-weight:bold;font-size:16px;'>   2013_Coaching_HW_MB_MB2_<span style='color:red;'>NA</span>_Set2_Wk2_Sec1_Q.zip</span></div> <div style='clear:both; margin-bottom:10px;'></div>";

                        return dvHtml;

                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Instructions to follow</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                }

            })

            $(".ancHelpSubkect").qtip({ // Grab some elements to apply the tooltip to
                content: {

                    text: function (event, api) {

                        var dvHtml = "";
                        dvHtml += "<div><center><span style='font-weight:bold; color: blue; font-size:16px;'>Doc Type:</span></center></div> <div style='clear:both; margin-bottom:10px;'></div>";
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">Homework is designated as Q (questions). Q is the primary document to be uploaded each week. Documents A, S, T are supplements to follow with Q. Without uploading Q first, you should not upload the supplements.  </span></div> ';
                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                        dvHtml += "<div><center><span style='font-weight:bold; color: blue; font-size:16px;'>Set#</span></center></div> <div style='clear:both; margin-bottom:10px;'></div>";
                        dvHtml += '<div style="font-size:14px;"><span style="text-decoration:none; ">Set# is the same as Week# in the current implementation. A, S, T will have the same Set# as Q. </span></div> ';

                        dvHtml += '<div style="clear:both; margin-bottom:10px;"></div>';

                     

                        return dvHtml;

                    },

                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Instructions to follow</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                }

            })

            

        });


        //   
    </script>
    <script language="javascript" type="text/javascript">
        function PopupPicker(ctl) {
            var PopupWindow = null;
            settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('CoachPapHelp.aspx?ID=' + ctl, 'Coach Papers Help', settings);
            PopupWindow.focus();
        }
    </script>
    <style>
        .qTipWidth {
            max-width: 900px !important;
        }

        .dwnlodcenter {
            text-align: center;
        }

        .style1 {
            width: 386px;
        }

        .style4 {
            height: 21px;
            width: 442px;
        }

        .style5 {
            text-align: left;
        }

        .style11 {
            width: 69px;
        }

        .style12 {
            text-align: left;
            width: 442px;
        }

        .style13 {
            width: 220px;
        }

        .style15 {
            width: 191px;
        }

        .style17 {
            height: 21px;
            width: 139px;
        }

        .style18 {
            height: 42px;
            width: 139px;
        }

        .style19 {
            width: 10px;
        }

        .style20 {
            width: 25px;
        }

        .style21 {
            width: 70px;
        }

        .style22 {
            width: 37px;
        }

        .style23 {
            width: 30px;
        }

        .style24 {
            width: 57px;
        }

        .style25 {
            width: 59px;
        }

        .style26 {
            width: 139px;
        }

        .style30 {
            height: 2px;
            width: 717px;
        }

        .style31 {
            height: 2px;
            width: 104px;
        }

        .style32 {
            width: 41px;
        }

        .style33 {
            width: 526px;
        }

        .style35 {
            width: 717px;
        }

        .stynew {
            width: 114px;
        }

        .marginL0 {
            margin-left: 0px;
        }

        .style36 {
            width: 104px;
        }

        .style37 {
            width: 442px;
        }

        .Initial {
            display: block;
            padding: 4px 18px 4px 18px;
            float: left;
            color: Black;
            font-weight: bold;
            border: 1px solid #ccc;
            background-color: #f1f1f1;
        }

            .Initial:hover {
                color: White;
                background: Grey no-repeat right top;
            }

        .Clicked {
            float: left;
            display: block;
            background: #8CC403 no-repeat right top;
            padding: 4px 18px 4px 18px;
            color: Black;
            font-weight: bold;
            color: White;
        }
    </style>

    <div align="left">
        <asp:HyperLink ID="backToVolunteerFunctions" CssClass="btn_02" NavigateUrl="VolunteerFunctions.aspx" runat="server">  Back to Volunteer Functions</asp:HyperLink>

    </div>

    <div style="clear: both; margin-bottom: 15px;"></div>

    <table style="width: 100%;" align="center">
        <tr>
            <td>
                <asp:Button Text="Upload Papers" CausesValidation="false" BorderStyle="None" ID="Tab1" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab1_Click" />
                <asp:Button Text="Download Papers" CausesValidation="false" BorderStyle="None" ID="Tab2" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab2_Click" />
                <asp:Button Text="Modify Papers" CausesValidation="false" BorderStyle="None" ID="Tab3" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab3_Click" />
                <asp:Button Text="Replicate Papers" CausesValidation="false" BorderStyle="None" ID="Tab4" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab4_Click" />
                <asp:Button Text="Test Sections" CausesValidation="false" BorderStyle="None" ID="Tab5" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab5_Click" />
                <asp:Button Text="Answer Key" CausesValidation="false" BorderStyle="None" ID="Tab6" Style="cursor: pointer;" CssClass="Initial" runat="server"
                    OnClick="Tab6_Click" />
                <div style="clear: both;"></div>
                <table style="width: 100%; border-top: 1px solid #b0a6a6; height: 2px;"></table>

            </td>
        </tr>
    </table>

    <div align="center" style="display: none;">

        <asp:DropDownList ID="dllfileChoice" runat="server" AutoPostBack="True" OnSelectedIndexChanged="dllfileChoice_SelectedIndexChanged"
            Visible="False">
        </asp:DropDownList>

    </div>

    <asp:Label ID="lblNoPermission" runat="server" ForeColor="Red" Visible="false"></asp:Label>
    <div style="clear: both; margin-bottom: 15px;"></div>

    <asp:HiddenField ID="hdnCPID" runat="server" />

    <asp:Panel ID="Panel1" runat="server" Visible="False" Style="text-align: center"
        BackColor="White">
        <asp:Label ID="Label8" runat="server" Font-Bold="True" Font-Size="Large" ForeColor="Black" Style="text-align: center; font-family: Arial, Helvetica, sans-serif; font-size: large;" Text="Upload Papers"></asp:Label>
        <div align="center">
            <div style="clear: both; margin-bottom: 15px;"></div>
            <div style="float: left;">
                <a id="ancUploadHelp" style="color: blue; font-weight: bold; cursor: pointer;">Help?</a>
            </div>
            <div style="clear: both; margin-bottom: 15px;"></div>
            <div id="dvUploadPaper" runat="server" style="margin: auto; width: 1000px;" visible="true">
                <div style="margin-top: 10px; margin-left: 20px;">

                    <div style="float: left;">
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="Label23" runat="server" Font-Bold="true" Text="Event Year"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 18px;">
                                <asp:DropDownList ID="ddlContestYear" runat="server" Height="22px" Width="110px" AutoPostBack="true">
                                    <%-- onselectedindexchanged="ddlContestYear_SelectedIndexChanged"--%>
                                </asp:DropDownList>

                            </div>
                        </div>

                        <div style="float: left; margin-left: 20px; display: none;">
                            <div style="float: left;">
                                <asp:Label ID="Label32" runat="server" Font-Bold="true" Text="Event"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <asp:DropDownList ID="ddlEvent" runat="server" Height="22px" Width="110px">
                                </asp:DropDownList>

                            </div>
                        </div>

                        <div style="float: left; margin-left: 20px;">
                            <div style="float: left;">
                                <asp:Label ID="Label24" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <asp:DropDownList ID="ddlSemester" runat="server" Height="22px" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="ddlSemester_SelectedIndexChanged">
                                </asp:DropDownList>
                            </div>
                        </div>
                        <div style="float: left; margin-left: 20px;">
                            <div style="float: left;">
                                <asp:Label ID="Label25" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="true"
                                    OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                                    Width="110px" Height="22px" OnTextChanged="ddlProductGroup_TextChanged">
                                </asp:DropDownList>

                            </div>
                        </div>
                        <div style="float: left; margin-left: 20px;">
                            <div style="float: left;">
                                <asp:Label ID="Label26" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <asp:DropDownList ID="ddlProduct" runat="server"
                                    OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Width="110px"
                                    Height="22px" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                        </div>

                        <div style="float: left; margin-left: 20px;">
                            <div style="float: left;">
                                <asp:Label ID="Label10" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 10px;">
                                <asp:DropDownList ID="ddlLevel" runat="server" OnSelectedIndexChanged="ddlLevel_SelectedIndexChanged"
                                    Height="22px" Width="110px" AutoPostBack="true">
                                </asp:DropDownList>

                            </div>
                        </div>

                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div style="float: left;">


                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="Label28" runat="server" Font-Bold="true" Text="Paper Type"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 16px;">
                                <asp:DropDownList ID="ddlNoOfContestants" runat="server" Height="20px"
                                    Style="margin-left: 0px" Width="110px">
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div style="float: left; margin-left: 23px;">
                            <div style="float: left;">
                                <asp:Label ID="Label29" runat="server" Font-Bold="true" Text="Doc Type"></asp:Label>
                            </div>
                            <div style="float: left; margin-left: 8px;">
                                <asp:DropDownList ID="ddlDocType" runat="server" Height="20px" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="ddlDocType_SelectedIndexChanged">
                                    <asp:ListItem Text="Questions" Value="Q" Selected="True"></asp:ListItem>
                                    <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                                    <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                                    <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                                </asp:DropDownList>
                            </div>
                        </div>

                        <div style="float: left; margin-left: 21px;">
                            <div style="float: left;">
                                <asp:Label ID="Label30" runat="server" Font-Bold="true">Week# </asp:Label>
                            </div>
                            <div style="float: left; margin-left: 56px;">
                                <asp:DropDownList ID="ddlWeek" runat="server" Height="22px" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="ddlWeek_SelectedIndexChanged">
                                </asp:DropDownList>


                            </div>
                        </div>
                        <div style="float: left; margin-left: 20px;">

                            <div style="float: left;">
                                <asp:Label ID="Label33" runat="server" Font-Bold="true">Section </asp:Label>
                            </div>
                            <div style="float: left; margin-left: 12px;">
                                <asp:DropDownList ID="ddlSections" runat="server" Height="20px" Width="110px">
                                </asp:DropDownList>
                            </div>


                        </div>




                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>
                    <div style="float: left; display: none;">
                        <div style="float: left;">
                            <div style="float: left;">
                                <asp:Label ID="Label31" runat="server" Font-Bold="true">Set# </asp:Label>
                            </div>
                            <div style="float: left; margin-left: 49px;">
                                <asp:DropDownList ID="ddlSet" runat="server" Height="22px" Width="110px" Enabled="false">
                                </asp:DropDownList>
                            </div>
                        </div>



                    </div>
                    <div style="clear: both; margin-bottom: 10px;"></div>



                </div>
            </div>


        </div>
    </asp:Panel>

    <asp:Panel ID="PanelAdd" runat="server" Visible="False" Style="height: 120px; margin: auto; width: 1000px;">

        <div style="margin-left: 20px;">
            <div style="float: left;">
                <div style="float: left;">
                    <asp:Label ID="Label34" runat="server" Font-Bold="true" Text="File Name"></asp:Label>
                </div>
                <div style="float: left; margin-left: 23px;">
                    <asp:FileUpload ID="FileUpLoad1" runat="server" Height="22px"
                        Style="text-align: left" />
                </div>
                <div style="float: left;">(Ex:2013_Coaching_HW_MB_MB2_INT_Set2_Wk2_Sec1_Q.zip) 	</div>
                <div style="clear: both;"></div>
                <div style="float: left; margin-left: 49px;">
                    <div style="float: left;">

                        <div style="float: left; margin-left: 26px;">
                            <asp:CheckBox ID="ckFname" runat="server" Font-Bold="True"
                                Style="text-align: left"
                                Text="Upload File without Naming Convention" />
                        </div>
                    </div>
                </div>

            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label35" runat="server" Font-Bold="true">Description </asp:Label>
                    </div>

                    <div style="float: left; margin-left: 14px;">
                        <asp:TextBox ID="txtDescription" runat="server" Height="45px" Text=""
                            TextMode="MultiLine" Width="875px"></asp:TextBox>
                    </div>
                </div>
            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label36" runat="server" Font-Bold="true">Password </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 24px;">
                        <asp:TextBox ID="TxtPassword" runat="server" Text=""></asp:TextBox>
                        <asp:Label ID="Label37" runat="server" EnableViewState="false"
                            ForeColor="Green"> (Optional)</asp:Label>
                    </div>
                </div>
            </div>



            <div style="float: left; margin-left: 20px;">
                <div style="float: left;">

                    <div style="float: left; margin-left: 21px;">
                        <asp:Button ID="UploadBtn" runat="server" OnClick="UploadBtn_Click"
                            Text="Upload File" Width="105px" Style="height: 26px" />
                    </div>
                </div>
            </div>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="text-align: center">
                <asp:Label ID="lblMessage" runat="server" EnableViewState="false"
                    ForeColor="Red"></asp:Label>
            </div>
            <div style="float: right;">
                <asp:LinkButton ID="LinkButton1" runat="server" CausesValidation="False"
                    OnClick="LinkButton1_Click">Show Uploaded files</asp:LinkButton>
                <div style="clear: both;"></div>
                <asp:Label ID="lblstatus" runat="server" ForeColor="Red" Visible="False"></asp:Label>
            </div>
        </div>
    </asp:Panel>


    <asp:Panel ID="PanelModify" runat="server" Visible="False">

        <div style="height: 120px; margin: auto; width: 1000px;">
            <div style="margin-left: 20px;">

                <div style="float: left;">
                    <div style="float: left;">
                        <div style="float: left;">
                            <asp:Label ID="Label6" runat="server" Font-Bold="true">Description </asp:Label>
                        </div>

                        <div style="float: left; margin-left: 10px;">
                            <asp:TextBox ID="txtDescriptionM" runat="server" Height="45px" Text=""
                                TextMode="MultiLine" Width="875px"></asp:TextBox>
                        </div>
                    </div>
                </div>

                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="float: left;">
                    <div style="float: left;">
                        <div style="float: left;">
                            <asp:Label ID="Label9" runat="server" Font-Bold="true">Password </asp:Label>
                        </div>
                        <div style="float: left; margin-left: 18px;">
                            <asp:TextBox ID="TxtPasswordM" runat="server" Text="" CssClass="marginL0"
                                Enabled="False"></asp:TextBox>
                        </div>
                    </div>
                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="float: left;">
                    <asp:Button ID="btnUpdate" runat="server" Text="Update"
                        OnClick="btnUpdate_Click" Enabled="False" />
                    <asp:Button ID="btncancel" runat="server" OnClick="btncancel_Click"
                        Text="Cancel" Enabled="False" />
                </div>


            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <div>
                    <asp:Label ID="lblMerror" runat="server" ForeColor="#FF3300"></asp:Label>
                </div>
            </center>
        </div>




        <%-- <div>
            <table style="margin: 0 auto 0 125px; width: 87%;">
                <tr>
                    <td class="style36" style="text-align: left;">
                        <asp:Label ID="Label15" runat="server" Style="text-align: right"
                            Text="Description"></asp:Label>
                    </td>
                    <td style="text-align: left;" class="style35"></td>
                </tr>
                <tr>
                    <td class="style31" style="text-align: left;">
                        <asp:Label ID="Label16" runat="server" Style="text-align: right"
                            Text="Password"></asp:Label>
                    </td>
                    <td style="text-align: left; margin-left: 0px;" class="style30"></td>
                </tr>
                <tr>
                    <td class="style31" style="text-align: left;">&nbsp;</td>
                    <td class="style30" style="text-align: left; margin-left: 0px;"></td>
                </tr>
                <tr>
                    <td class="style31" style="text-align: left;">&nbsp;</td>
                    <td class="style30" style="text-align: left; margin-left: 0px;"></td>
                </tr>
            </table>
        </div>--%>
    </asp:Panel>

    <asp:Panel ID="PanelSections" runat="server" BorderWidth="1px" BorderColor="black" Width="100%" Visible="False">
        <div align="center">
            <asp:Label ID="lblReplace" ForeColor="Red" Text="Are you sure there is no Question Paper for the students this period?" runat="server"></asp:Label><br />
            <asp:Button ID="bttnYes" runat="server" Text="Yes" OnClick="bttnYes_Click" />
            &nbsp;&nbsp;<asp:Button
                ID="bttnNo" runat="server" Text="No" OnClick="bttnNo_Click" />
        </div>
    </asp:Panel>

    <asp:Panel ID="Panel5" runat="server" BorderWidth="1px" BorderColor="black" Width="100%" Visible="False">
        <div align="center">
            <asp:Label ID="Label1" ForeColor="Red" Text="This is a duplicate.  Do you want to replace the current file?" runat="server"></asp:Label>
            <div style="clear: both;"></div>
            <asp:Button ID="Button1" runat="server" Text="Yes" OnClick="btnYes_Click" />
            &nbsp;&nbsp;<asp:Button
                ID="Button2" runat="server" Text="No" OnClick="btnNo_Click" />
        </div>
    </asp:Panel>
    <div style="clear: both;"></div>


    <asp:Panel ID="Panel6" runat="server" Width="100%" Visible="False">
        <div align="center">
            <div style="clear: both;"></div>

            <asp:Label ID="Label17" runat="server" Font-Bold="True" Font-Size="Small"
                Text="Existing Record"></asp:Label>

            <asp:GridView ID="gvDuplicate" runat="server" AutoGenerateColumns="False"
                Style="text-align: center" Width="907px">
                <Columns>
                    <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId"
                        SortExpression="CoachPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="QPaperId" HeaderText="QPaperId"
                        SortExpression="QPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventYear" HeaderText="EventYear"
                        SortExpression="EventYear">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId"
                        SortExpression="ProductId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventId" HeaderText="EventId"
                        SortExpression="EventId">
                        <ControlStyle BackColor="#00CC00" />
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PaperType" HeaderText="PaperType"
                        SortExpression="PaperType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DocType" HeaderText="DocType"
                        SortExpression="DocType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"
                        SortExpression="ProductGroupCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"
                        SortExpression="ProductCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Sections" HeaderText="SecNum"
                        SortExpression="Sections">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName"
                        SortExpression="TestFileName">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Description" HeaderText="Description"
                        SortExpression="Description">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Password" HeaderText="Password"
                        SortExpression="Password">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId"
                        SortExpression="ProductGroupId" Visible="False" />
                </Columns>
                <SelectedRowStyle BackColor="#99FFCC" />
            </asp:GridView>
            <div style="clear: both;"></div>
            <asp:Label ID="Label18" runat="server" Font-Bold="True" Font-Size="Small"
                Text="Modified Input"></asp:Label>
            <table class="ItemLabelCenter" frame="border" rules="all">
                <tr bgcolor="#FFFF80">
                    <td>Year</td>
                    <td>PaperType</td>
                    <td>DocType</td>
                    <td>P.Group</td>
                    <td>Product</td>
                    <td>Level</td>
                    <td>Sec#</td>
                    <td>Week#</td>
                    <td>Set#</td>
                    <td>File Name</td>
                </tr>
                <tr>
                    <td>
                        <asp:Label ID="lbluiyear" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblPtype" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbldoctype" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblpgroup" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblProduct" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbllevel" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblsec" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lblweek" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbluiset" runat="server" Text="Label"></asp:Label>
                    </td>
                    <td>
                        <asp:Label ID="lbluifname" runat="server" Text="Label"></asp:Label>
                    </td>
                </tr>
            </table>
        </div>
    </asp:Panel>

    <div style="clear: both; margin-bottom: 15px;"></div>

    <asp:Panel ID="pnlReplicate" runat="server" Visible="False">
        <center>
            <asp:Label ID="Label2" runat="server" Text="Replicate Coach Papers" Width="317px" Font-Bold="True" Font-Size="Large"></asp:Label>
            <div style="clear: both;"></div>

            <table style="width: 100%" border="0" cellpadding="4" cellspacing="0" bordercolor="black">
                <tr>
                    <td colspan="4" style="font-size: 13px; font-weight: bold; text-align: left;">From</td>
                    <td nowrap="nowrap" style="font-size: 13px; font-weight: bold">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                </tr>
                <tr>
                    <td bgcolor="honeydew" class="style11" nowrap="nowrap"><b>Event Year</b></td>
                    <td bgcolor="honeydew" style="display: none;" class="style20" nowrap="nowrap">Event</td>
                    <td bgcolor="honeydew" class="style20" nowrap="nowrap"><b>Semester</b></td>
                    <td bgcolor="honeydew" class="style21" nowrap="nowrap"><b>Product Group</b></td>
                    <td bgcolor="honeydew" class="style22" nowrap="nowrap"><b>Product</b></td>
                    <td bgcolor="honeydew" class="style23" nowrap="nowrap"><b>Level</b></td>
                    <td bgcolor="honeydew" class="style25" nowrap="nowrap"><b>Paper Type</b></td>
                    <td bgcolor="honeydew" class="style19" nowrap="nowrap"><b>DocType</b></td>
                    <td bgcolor="honeydew" class="style19" nowrap="nowrap"><b>Week#</b></td>

                    <td bgcolor="honeydew" style="display: none;" class="style24" nowrap="nowrap">Set#</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:DropDownList ID="ddlRepFrmYear" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRepFrmYear_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style20" style="display: none;">
                        <asp:DropDownList ID="ddlRepFrmEvent" Style="display: none;" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="style20">
                        <asp:DropDownList ID="ddlRepFrmSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRepFrmSemester_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style21">
                        <asp:DropDownList ID="ddlRepFrmPrdGroup" runat="server" AutoPostBack="true" Height="19px" Width="119px" OnSelectedIndexChanged="ddlRepFrmPrdGroup_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style22">
                        <asp:DropDownList ID="ddlRepFrmProduct" runat="server"
                            OnSelectedIndexChanged="ddlRepFrmProduct_SelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="style23">
                        <asp:DropDownList ID="ddlRepFrmLevel" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlRepFrmLevel_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style25">
                        <asp:DropDownList ID="ddlRepFrmPaperType" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRepFrmPaperType_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style24">
                        <asp:DropDownList ID="DDLDocTypeRepFrm" runat="server" Height="20px" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="DDLDocTypeRepFrm_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Questions" Value="Q"></asp:ListItem>
                            <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                            <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style19">
                        <asp:DropDownList ID="ddlRepFrmWeekNo" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRepFrmWeekNo_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>

                    <td class="style24" style="display: none;">

                        <asp:DropDownList ID="ddlRepFrmSetNo" Style="display: none;" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlRepFrmSetNo_SelectedIndexChanged">
                        </asp:DropDownList>

                    </td>
                    <td style="width: 115px"></td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">&nbsp;</td>
                    <td class="style20">&nbsp;</td>
                    <td colspan="5" style="font-size: 13px; font-weight: bold; text-align: center">
                        <asp:Label ID="lblRepMsg" runat="server"></asp:Label>
                        <div style="clear: both;"></div>
                    </td>
                    <td class="style24">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
            </table>

            <table style="width: 100%" border="0" cellpadding="4" cellspacing="0" bordercolor="black">
                <tr>
                    <td nowrap="nowrap" style="font-size: 13px; font-weight: bold; text-align: left">To</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap" class="style21">&nbsp;</td>
                    <td style="font-size: 13px; font-weight: bold">&nbsp;</td>
                    <td nowrap="nowrap" class="style23">&nbsp;</td>
                    <td nowrap="nowrap" class="style25">&nbsp;</td>
                    <td nowrap="nowrap" class="style19">&nbsp;</td>
                    <td nowrap="nowrap" class="style24">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                </tr>
                <tr>
                    <td bgcolor="honeydew" class="style11" nowrap="nowrap"><b>Event Year</b></td>
                    <td bgcolor="honeydew" class="style20" style="display: none;" nowrap="nowrap">Event</td>
                    <td bgcolor="honeydew" class="style20" nowrap="nowrap"><b>Semester</b></td>
                    <td bgcolor="honeydew" class="style21" nowrap="nowrap"><b>Product Group</b> </td>
                    <td bgcolor="honeydew" class="style22" nowrap="nowrap"><b>Product</b> </td>
                    <td bgcolor="honeydew" class="style23" nowrap="nowrap"><b>Level</b></td>
                    <td bgcolor="honeydew" class="style25" nowrap="nowrap"><b>Paper Type</b></td>
                    <td bgcolor="honeydew" class="style19" nowrap="nowrap"><b>DocType</b></td>
                    <td bgcolor="honeydew" class="style19" nowrap="nowrap"><b>Week#</b></td>
                    <td style="display: none;" bgcolor="honeydew" class="style24" nowrap="nowrap">Set#</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:DropDownList ID="ddlRepToYear" runat="server" AutoPostBack="true">
                        </asp:DropDownList>
                    </td>
                    <td class="style20" style="display: none;">
                        <asp:DropDownList ID="ddlRepToEvent" Style="display: none;" runat="server" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style20">
                        <asp:DropDownList ID="ddlRepToSemester" AutoPostBack="true" OnSelectedIndexChanged="ddlRepToSemester_SelectedIndexChanged" runat="server" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style21">
                        <asp:DropDownList ID="ddlRepToPrdGroup" runat="server" AutoPostBack="true"
                            Height="19px" Width="119px" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style22">
                        <asp:DropDownList ID="ddlRepToProduct" runat="server" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style23">
                        <asp:DropDownList ID="ddlRepToLevel" runat="server" Width="100px" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style25">
                        <asp:DropDownList ID="ddlRepToPaperType" runat="server" Enabled="False">
                        </asp:DropDownList>
                    </td>
                    <td class="style24">
                        <asp:DropDownList ID="DDlRepToDocType" runat="server" Height="20px" Width="110px" AutoPostBack="true" OnSelectedIndexChanged="DDlRepToDocType_SelectedIndexChanged">
                            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Questions" Value="Q"></asp:ListItem>
                            <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                            <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style19">
                        <asp:DropDownList ID="ddlRepToWeekNo" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlRepToWeekNo_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style24" style="display: none;">

                        <asp:DropDownList Style="display: none;" ID="ddlRepToSetNo" runat="server">
                        </asp:DropDownList>

                    </td>
                    <td style="width: 115px"></td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:Button ID="btnRepSubmit" runat="server" Height="26px"
                            OnClick="btnRepSubmit_Click" Style="width: 61px" Text="Submit" Width="90px" />
                        <div style="display: none">
                            <asp:Button ID="btnRepConfirmToSave" runat="server" OnClick="btnRepConfirmToSave_Click" Text="ReplicateSaveAfterConfirm" Width="27px" />
                            <asp:Button ID="btnRepConfirmToSubmit" runat="server" OnClick="btnRepConfirmToSubmit_Click" Text="ReplicateSubmitAfterConfirm" Width="27px" />
                        </div>
                    </td>
                    <td class="style20">
                        <asp:Button ID="btnRepCancel" runat="server" Height="26px" OnClick="btnReset_Click"
                            Text="Cancel" Width="60px" />
                    </td>
                    <td colspan="5" style="text-align: center;">
                        <asp:Button ID="btnRepSave" runat="server" Height="26px" OnClick="btnRepSave_Click" Text="Save Replication" Enabled="False" />
                    </td>
                    <td class="style24">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
            </table>


            <asp:Label ID="lblRep_Source" runat="server" Font-Bold="True">Table 1 : List of Files � From (Source) CoachPapers</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblRep_NoRec" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvTPSource" runat="server" AutoGenerateColumns="False" DataKeyNames="CoachPaperId,TestFileName" OnRowCommand="gvTPSource_RowCommand" Style="text-align: center" Width="907px" OnRowDataBound="gvTPSource_RowDataBound">
                <Columns>
                    <%--0--%>
                    <asp:ButtonField ButtonType="Image" CommandName="Download" HeaderText="Download" ImageUrl="~/Images/download.png" ShowHeader="True">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" HorizontalAlign="Center" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" Wrap="True" />
                    </asp:ButtonField>
                    <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId" SortExpression="CoachPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="QPaperId" HeaderText="QPaperId" SortExpression="QPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                    </asp:BoundField>
                    <%--3--%>
                    <asp:BoundField DataField="EventYear" HeaderText="EventYear" SortExpression="EventYear">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventId" HeaderText="EventId" SortExpression="EventId">
                        <ControlStyle BackColor="#00CC00" />
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <%--6--%>
                    <asp:BoundField DataField="PaperType" HeaderText="PaperType" SortExpression="PaperType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DocType" HeaderText="DocType" SortExpression="DocType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <%--10--%>
                    <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>

                    <asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="Semester">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>

                    <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Sections" HeaderText="SecNum" SortExpression="Sections">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <%--14--%>
                    <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" SortExpression="TestFileName">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:Label ID="lblRep_Destination" runat="server" Font-Bold="True"> Table 1A : List of Files � To (Destination) CoachPapers</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblRep_NoRecord" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvTPDestination" runat="server" AutoGenerateColumns="False" DataKeyNames="CoachPaperId,TestFileName" OnRowCommand="gvTPDestination_RowCommand" Style="text-align: center" Width="907px" OnRowDataBound="gvTPDestination_RowDataBound">
                <Columns>
                    <%-- 0--%>
                    <asp:ButtonField ButtonType="Image" CommandName="Download" HeaderText="Download" ImageUrl="~/Images/download.png" ShowHeader="True">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" HorizontalAlign="Center" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" Wrap="True" />
                    </asp:ButtonField>
                    <asp:BoundField HeaderText="CoachPaperId" SortExpression="CoachPaperId" DataField="CoachPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField HeaderText="QPaperId" SortExpression="QPaperId" DataField="QPaperId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventYear" HeaderText="EventYear" SortExpression="EventYear">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId" SortExpression="ProductId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <%-- 5--%>
                    <asp:BoundField DataField="EventId" HeaderText="EventId" SortExpression="EventId">
                        <ControlStyle BackColor="#00CC00" />
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PaperType" HeaderText="PaperType" SortExpression="PaperType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DocType" HeaderText="DocType" SortExpression="DocType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode" SortExpression="ProductGroupCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="ProductCode" SortExpression="ProductCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>

                    <asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="Semester">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>

                    <%--   11--%>
                    <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>

                    <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Sections" HeaderText="SecNum" SortExpression="Sections">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter" HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName" SortExpression="TestFileName">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Description" HeaderText="Description" SortExpression="Description">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Password" HeaderText="Password" SortExpression="Password">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>

            <asp:Label ID="lblFromTS" runat="server" Font-Bold="True"> Table 2:  From - Test Sections</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblFromTSNoRec" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvFromTS" runat="server" AutoGenerateColumns="False" Style="text-align: center" Width="907px"
                HeaderStyle-BackColor="#b02786" HeaderStyle-BorderColor="Black" HeaderStyle-ForeColor="White">
                <Columns>
                    <asp:BoundField HeaderText="TestSetUpSectionsID" DataField="TestSetUpSectionsID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CoachPaperID" DataField="CoachPaperID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionNumber" DataField="SectionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Subject" DataField="Subject" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="EventYear" DataField="EventYear" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionTimeLimit" DataField="SectionTimeLimit" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="NumberOfQuestions" DataField="NumberOfQuestions" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionType" DataField="QuestionType" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="NumberOfChoices" DataField="NumberOfChoices" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumberFrom" DataField="QuestionNumberFrom" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumberTo" DataField="QuestionNumberTo" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Penalty" DataField="Penalty" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>

            <asp:Label ID="lblToTS" runat="server" Font-Bold="True"> Table 2A:  To - Test Sections</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblToTSNoRec" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvToTS" runat="server" AutoGenerateColumns="False" Style="text-align: center" Width="907px"
                HeaderStyle-BackColor="#b02786" HeaderStyle-BorderColor="Black" HeaderStyle-ForeColor="White" OnRowDataBound="gvToTS_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="TestSetUpSectionsID" DataField="TestSetUpSectionsID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CoachPaperID" DataField="CoachPaperID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionNumber" DataField="SectionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Subject" DataField="Subject" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="EventYear" DataField="EventYear" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionTimeLimit" DataField="SectionTimeLimit" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="NumberOfQuestions" DataField="NumberOfQuestions" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionType" DataField="QuestionType" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="NumberOfChoices" DataField="NumberOfChoices" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumberFrom" DataField="QuestionNumberFrom" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumberTo" DataField="QuestionNumberTo" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Penalty" DataField="Penalty" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>

            <asp:Label ID="lblFromAK" runat="server" Font-Bold="True"> Table 3: From - Answer Key</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblFromAKNoRec" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvFromAK" runat="server" AutoGenerateColumns="False" Style="text-align: center" Width="907px"
                HeaderStyle-BackColor="#03a9f4" HeaderStyle-BorderColor="Black" HeaderStyle-ForeColor="White">
                <Columns>
                    <asp:BoundField HeaderText="AnswerKeyRecID" DataField="AnswerKeyRecID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CoachPaperID" DataField="CoachPaperID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionNumber" DataField="SectionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="EventYear" DataField="EventYear" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumber" DataField="QuestionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CorrectAnswer" DataField="CorrectAnswer" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="DifficultyLevel" DataField="DifficultyLevel" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Manual" DataField="Manual" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>

            <asp:Label ID="lblToAK" runat="server" Font-Bold="True"> Table 3A: To - Answer Key</asp:Label>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblToAKNoRec" runat="server" Font-Bold="True" ForeColor="red"></asp:Label></center>
            <asp:GridView ID="gvToAK" runat="server" AutoGenerateColumns="False" Style="text-align: center" Width="907px"
                HeaderStyle-BackColor="#03a9f4" HeaderStyle-BorderColor="Black" HeaderStyle-ForeColor="White" OnRowDataBound="gvToAK_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="AnswerKeyRecID" DataField="AnswerKeyRecID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CoachPaperID" DataField="CoachPaperID" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Level" DataField="Level" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="SectionNumber" DataField="SectionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="EventYear" DataField="EventYear" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="QuestionNumber" DataField="QuestionNumber" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="CorrectAnswer" DataField="CorrectAnswer" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="DifficultyLevel" DataField="DifficultyLevel" HeaderStyle-ForeColor="White" />
                    <asp:BoundField HeaderText="Manual" DataField="Manual" HeaderStyle-ForeColor="White" />
                </Columns>
            </asp:GridView>

        </center>
    </asp:Panel>


    <asp:Panel ID="Panel2" runat="server" Visible="False">
        <center>
            <asp:Label ID="Label7" runat="server" Text="Filter/Download Coach Papers"
                Width="317px" Font-Bold="True"
                Font-Size="Large" Style="text-align: center;"></asp:Label>
        </center>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <center>
            <table style="width: 100%" border="0" cellpadding="4" cellspacing="0" bordercolor="black">
                <tr>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style11">Year</td>
                    <td bgcolor="honeydew" nowrap="nowrap" class="style20">Event</td>
                    <td bgcolor="honeydew" nowrap="nowrap" class="style20">Semester</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style21">Product Group Code</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style22">Product Code</td>
                    <td bgcolor="honeydew" nowrap="nowrap" class="style23">Level</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style25">Paper Type</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style25">Doc. Type</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style19">Week#</td>
                    <td nowrap="nowrap" bgcolor="honeydew" class="style24">Set#</td>
                    <td nowrap="nowrap">&nbsp;</td>
                    <td nowrap="nowrap">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:DropDownList ID="ddlFlrYear" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFlrYear_SelectedIndexChanged" Width="85px">
                        </asp:DropDownList>
                    </td>
                    <td class="style20">
                        <asp:DropDownList ID="ddlFlrEvent" runat="server">
                        </asp:DropDownList>
                    </td>
                    <td class="style20">
                        <asp:DropDownList ID="ddlFlrSemester" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlFlrSemester_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style21">
                        <asp:DropDownList ID="ddlFlrProductGroup" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFlrProductGroup_SelectedIndexChanged"
                            Height="19px" Width="119px">
                        </asp:DropDownList>
                    </td>
                    <td class="style22">
                        <asp:DropDownList ID="ddlFlrProduct" runat="server"
                            OnSelectedIndexChanged="ddlFlrProduct_SelectedIndexChanged" AutoPostBack="True">
                        </asp:DropDownList>
                    </td>
                    <td class="style23">
                        <asp:DropDownList ID="ddlFlrLevel" runat="server" Width="100px" AutoPostBack="True" OnSelectedIndexChanged="ddlFlrLevel_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style25">
                        <asp:DropDownList ID="ddlFlrNoOfContestants" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFlrNoOfContestants_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>

                    <td style="text-align: left;">
                        <asp:DropDownList ID="DDlDDocType" runat="server" Height="20px" Width="140px" OnSelectedIndexChanged="DDlDDocType_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Text="All" Value="0" Selected="True"></asp:ListItem>
                            <asp:ListItem Text="Questions" Value="Q"></asp:ListItem>
                            <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                            <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                        </asp:DropDownList>
                    </td>
                    <td class="style19">
                        <asp:DropDownList ID="ddlFlrWeek" runat="server" AutoPostBack="true"
                            OnSelectedIndexChanged="ddlFlrWeek_SelectedIndexChanged">
                        </asp:DropDownList>
                    </td>
                    <td class="style24">

                        <asp:DropDownList ID="ddlFlrSet" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlFlrSet_SelectedIndexChanged">
                        </asp:DropDownList>

                    </td>
                    <td style="width: 115px"></td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
                <tr>
                    <td class="style11">
                        <asp:Button ID="btnSearch" runat="server" Height="26px"
                            OnClick="btnSearch_Click" Style="width: 61px" Text="Search" Width="90px" />
                    </td>
                    <td class="style20">
                        <asp:Button ID="btnReset" runat="server" Height="26px" OnClick="btnReset_Click"
                            Text="Reset" Width="60px" />
                    </td>
                    <td colspan="5">
                        <asp:Label ID="lblSearchErr" runat="server" ForeColor="Red"></asp:Label>
                    </td>
                    <td class="style24">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                    <td style="width: 115px">&nbsp;</td>
                </tr>
            </table>
        </center>

    </asp:Panel>

    <asp:Panel ID="Panel4" runat="server" Visible="False">
        <asp:Label ID="Label13" runat="server" Text="Download Coach Papers"
            Width="249px" Font-Bold="True"
            Font-Size="Large" Height="20px"></asp:Label>
        &nbsp;&nbsp;&nbsp; 
        
        
        <div style="clear: both;"></div>
        <asp:Label ID="Label14" runat="server" Text="Select Week range"> </asp:Label>
        &nbsp;
        <asp:DropDownList ID="ddlFlrWeekForExamReceiver" runat="server" OnSelectedIndexChanged="ddlFlrWeekForExamReceiver_SelectedIndexChanged">
        </asp:DropDownList>
        <asp:Button ID="ButtonForExamReceiver" runat="server" OnClick="ButtonForExamReceiver_Click" Text="Search" />
        <div style="clear: both;"></div>

    </asp:Panel>

    <div style="clear: both; margin-bottom: 30px;"></div>
    <div id="dvSearchDownloadPapers" runat="server" style="height: 120px; margin: auto; width: 1000px;" visible="false">
        <div style="margin-top: 10px; margin-left: 20px;">
            <center>
                <h2><span id="spnDonloadPaper" runat="server">Download Papers</span></h2>
            </center>

            <div style="float: left;">
                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="lblEventyear" runat="server" Font-Bold="true" Text="Event Year"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlEventyearFilter" runat="server" Width="100" OnSelectedIndexChanged="ddlEventyearFilter_SelectedIndexChanged" AutoPostBack="true">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Label ID="Label3" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlPhaseFilter" runat="server" OnSelectedIndexChanged="ddlPhaseFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                            <asp:ListItem Value="Fall" Selected="True">Fall</asp:ListItem>
                            <asp:ListItem Value="Spring">Spring</asp:ListItem>
                            <asp:ListItem Value="Summer">Summer</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Label ID="Label4" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductGroupFilter" OnSelectedIndexChanged="ddlProductGroupFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Label ID="Label5" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:DropDownList ID="ddlProductFilter" OnSelectedIndexChanged="ddlProductFilter_SelectedIndexChanged" AutoPostBack="true" runat="server" Width="100px">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Label ID="Label11" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 36px;">
                        <asp:DropDownList ID="ddlLevelFilter" runat="server" OnSelectedIndexChanged="ddlLevelFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">
                            <asp:ListItem Value="Select">Select</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div style="float: left;">

                <div style="float: left;">
                    <div style="float: left;">
                        <asp:Label ID="Label19" runat="server" Font-Bold="true" Text="Paper Type"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 8px;">
                        <asp:DropDownList ID="ddlPaperTypeFilter" runat="server" OnSelectedIndexChanged="ddlPaperTypeFilter_SelectedIndexChanged" AutoPostBack="true" Width="100">

                            <asp:ListItem Value="-1">Select</asp:ListItem>
                            <asp:ListItem Value="HW" Selected="True">Homework</asp:ListItem>
                            <asp:ListItem Value="PT">PreTest</asp:ListItem>
                            <asp:ListItem Value="RT">RegTest</asp:ListItem>
                            <asp:ListItem Value="FT">FinalTest</asp:ListItem>


                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 23px;">
                    <div style="float: left;">
                        <asp:Label ID="Label20" runat="server" Font-Bold="true" Text="Doc Type"></asp:Label>
                    </div>
                    <div style="float: left; margin-left: 9px;">
                        <asp:DropDownList ID="ddlDocTypeFilter" runat="server" Width="100px" OnSelectedIndexChanged="ddlDocTypeFilter_SelectedIndexChanged" AutoPostBack="true">
                            <asp:ListItem Value="-1">Select</asp:ListItem>
                            <asp:ListItem Text="Questions" Value="Q"></asp:ListItem>
                            <asp:ListItem Text="Answers/Solutions" Value="A"></asp:ListItem>
                            <asp:ListItem Text="Student Material" Value="S"></asp:ListItem>
                            <asp:ListItem Text="Teachers Only Material" Value="T"></asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>

                <div style="float: left; margin-left: 21px;">
                    <div style="float: left;">
                        <asp:Label ID="Label21" runat="server" Font-Bold="true">Week# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 55px;">
                        <asp:DropDownList ID="ddlWeekFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlWeekFilter_SelectedIndexChanged" Width="100">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <div style="float: left;">
                        <asp:Label ID="Label22" runat="server" Font-Bold="true">Section# </asp:Label>
                    </div>
                    <div style="float: left; margin-left: 2px;">
                        <asp:DropDownList ID="DDLSetNoFilter" runat="server" AutoPostBack="true" OnSelectedIndexChanged="DDLSetNoFilter_SelectedIndexChanged" Width="100">
                        </asp:DropDownList>
                    </div>
                </div>



                <div style="float: left; margin-left: 9px;">

                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="btnClearFilter" runat="server" Text="Clear" OnClick="btnClearFilter_Click" />
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>

    <asp:Panel ID="Panel3" runat="server" Visible="False">
        <center>
            <asp:Label ID="lblChapter" runat="server" CssClass="title02" Visible="false"></asp:Label>
            <div style="clear: both;"></div>
            <asp:Label ID="LblexamRecErr" runat="server" ForeColor="Red"></asp:Label>
        </center>
        <center>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <asp:GridView ID="gvTestPapers" runat="server"
                AutoGenerateColumns="False" o="gvTestPapers_RowDataBound"
                OnRowCommand="gvTestPapers_RowCommand"
                OnSelectedIndexChanged="gvTestPapers_SelectedIndexChanged"
                OnSorting="gvTestPapers_Sorting" Style="text-align: center" Width="907px"
                DataKeyNames="CoachPaperId,TestFileName">
                <Columns>
                    <asp:ButtonField ButtonType="Image" CommandName="Download"
                        HeaderText="Download" ImageUrl="~/Images/download.png" ShowHeader="True">
                        <HeaderStyle BackColor="#33CC33" ForeColor="White" HorizontalAlign="Center"
                            BorderColor="Black" />
                        <ItemStyle CssClass="dwnlodcenter" HorizontalAlign="Center" Wrap="True"
                            BorderColor="Black" />

                    </asp:ButtonField>

                    <%--    <asp:TemplateField ItemStyle-HorizontalAlign="Center" ItemStyle-CssClass="dwnlodcenter" HeaderText="Action" HeaderStyle-BackColor="#33CC33" HeaderStyle-ForeColor="White">

                        <ItemTemplate>
                            <asp:Button ID="btnDownload" runat="server" CommandName="Download" ImageUrl="~/Images/download.png" />
                        </ItemTemplate>
                    </asp:TemplateField>--%>

                    <asp:BoundField DataField="CoachPaperId" HeaderText="CoachPaperId"
                        SortExpression="CoachPaperId" Visible="false">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="QPaperId" HeaderText="QPaperId"
                        SortExpression="QPaperId" Visible="false">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventYear" Visible="false" HeaderText="EventYear"
                        SortExpression="EventYear">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductId" HeaderText="ProductId"
                        SortExpression="ProductId" Visible="false">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="EventId" Visible="false" HeaderText="EventId"
                        SortExpression="EventId">
                        <ControlStyle BackColor="#00CC00" />
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="PaperType" HeaderText="PaperType" Visible="false"
                        SortExpression="PaperType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="DocType" HeaderText="DocType"
                        SortExpression="DocType">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"
                        SortExpression="ProductGroupCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"
                        SortExpression="ProductCode">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Semester" Visible="false" HeaderText="Semester" SortExpression="Semester">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" />
                    </asp:BoundField>
                    <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="SetNum" HeaderText="SetNum" Visible="false" SortExpression="SetNum">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Sections" HeaderText="SecNum"
                        SortExpression="Sections">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                            HorizontalAlign="Center" />
                    </asp:BoundField>
                    <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName"
                        SortExpression="TestFileName">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Description" HeaderText="Description"
                        SortExpression="Description">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                    <asp:BoundField DataField="Password" HeaderText="Password"
                        SortExpression="Password">
                        <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                        <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
                    </asp:BoundField>
                </Columns>
            </asp:GridView>
            <asp:Label ID="lblPrdError" runat="server" ForeColor="Red"></asp:Label>
        </center>
        <asp:SqlDataSource ID="SqlDataSource1" runat="server"></asp:SqlDataSource>
        <asp:HiddenField ID="hdnTechNational" runat="server" />
    </asp:Panel>

    <asp:Panel ID="ModifyGrid" runat="server">
        <asp:HiddenField ID="hdntlead" runat="server" />

    </asp:Panel>
    <asp:HiddenField ID="hdnChapterID" runat="server" />

    <asp:HiddenField ID="hdnTempFileName" runat="server" />
    <asp:HiddenField ID="ULFile" runat="server" />
    <asp:HiddenField ID="hdnsection" runat="server" />
    <asp:HiddenField ID="hdnuploaded" runat="server" />
    <div style="clear: both;"></div>


    <asp:GridView ID="gvModify" runat="server" AutoGenerateColumns="False"
        DataKeyNames="ProductGroupId,CoachPaperId, ProductId"
        OnSelectedIndexChanged="gvModify_SelectedIndexChanged"
        Style="text-align: center" Width="907px">
        <Columns>
            <asp:ButtonField ButtonType="Button" CommandName="Select" Text="Modify">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:ButtonField>
            <asp:BoundField DataField="CoachPaperId" Visible="false" HeaderText="CoachPaperId"
                SortExpression="CoachPaperId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="QPaperId" Visible="false" HeaderText="QPaperId"
                SortExpression="QPaperId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
            </asp:BoundField>
            <asp:BoundField DataField="EventYear" Visible="false" HeaderText="EventYear"
                SortExpression="EventYear">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductId" Visible="false" HeaderText="ProductId"
                SortExpression="ProductId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="EventId" Visible="false" HeaderText="EventId"
                SortExpression="EventId">
                <ControlStyle BackColor="#00CC00" />
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="PaperType" HeaderText="PaperType"
                SortExpression="PaperType">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="DocType" HeaderText="DocType"
                SortExpression="DocType">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupCode" HeaderText="ProductGroupCode"
                SortExpression="ProductGroupCode">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductCode" HeaderText="ProductCode"
                SortExpression="ProductCode">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Level" HeaderText="Level" SortExpression="Level">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" />
            </asp:BoundField>

            <asp:BoundField DataField="Semester" HeaderText="Semester" SortExpression="Level">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" />
            </asp:BoundField>

            <asp:BoundField DataField="WeekId" HeaderText="WeekId" SortExpression="WeekId">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="SetNum" HeaderText="SetNum" SortExpression="SetNum">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="Sections" HeaderText="SecNum"
                SortExpression="Sections">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" CssClass="dwnlodcenter"
                    HorizontalAlign="Center" />
            </asp:BoundField>
            <asp:BoundField DataField="TestFileName" HeaderText="CoachFileName"
                SortExpression="TestFileName">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Description" HeaderText="Description"
                SortExpression="Description">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="Password" HeaderText="Password"
                SortExpression="Password">
                <HeaderStyle BackColor="#33CC33" BorderColor="Black" ForeColor="White" />
                <ItemStyle BorderColor="Black" HorizontalAlign="Left" />
            </asp:BoundField>
            <asp:BoundField DataField="ProductGroupId" HeaderText="ProductGroupId"
                SortExpression="ProductGroupId" Visible="False" />
        </Columns>
        <SelectedRowStyle BackColor="#99FFCC" />
    </asp:GridView>
    <asp:HiddenField ID="hdnupfile" runat="server" />
    <asp:Label ID="lblhiddenCpId" runat="server" Visible="False"></asp:Label>
    <asp:Label ID="lblhiddenFname" runat="server" Visible="False"></asp:Label>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvTestSectionPanel" runat="server" visible="false">
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvSeacrhCoachpapersTM" runat="server" style="margin: auto; width: 1100px;" visible="true">
            <div style="margin-top: 10px; margin-left: 20px;">
                <center>
                    <h2><span id="spnTestTitle" runat="server" style="color: green;">Test Sections Setup</span></h2>
                </center>

                <div style="float: left;">
                    <div style="float: left;">
                        <div style="float: left;">
                            <asp:Label ID="Label12" runat="server" Font-Bold="true" Text="Event Year"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddlEventYearTM" AutoPostBack="true" OnSelectedIndexChanged="ddlEventYearTM_SelectedIndexChanged" runat="server" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px;">
                        <div style="float: left;">
                            <asp:Label ID="Label15" runat="server" Font-Bold="true" Text="Semester"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddlSemesterTM" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSemesterTM_SelectedIndexChanged" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px;">
                        <div style="float: left;">
                            <asp:Label ID="Label16" runat="server" Font-Bold="true" Text="Product Group"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddlProductGroupTM"
                                DataTextField="Name" DataValueField="ProductGroupID"
                                OnSelectedIndexChanged="ddlProductGroupTM_SelectedIndexChanged"
                                AutoPostBack="true" Enabled="false" runat="server" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 30px;">
                        <div style="float: left;">
                            <asp:Label ID="Label27" runat="server" Font-Bold="true" Text="Product"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:DropDownList ID="ddlProductTM" DataTextField="Name" OnSelectedIndexChanged="ddlProductTM_SelectedIndexChanged" DataValueField="ProductID" runat="server" AutoPostBack="true" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                </div>
                <div style="clear: both; margin-bottom: 10px;"></div>
                <div style="float: left;">

                    <div style="float: left;">
                        <div style="float: left;">
                            <asp:Label ID="Label38" runat="server" Font-Bold="true" Text="Level"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 41px;">
                            <asp:DropDownList ID="ddlLevelTM" DataTextField="Name" DataValueField="Level" Enabled="false" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlLevelTM_SelectedIndexChanged" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>
                    <div style="float: left; margin-left: 29px;">
                        <div style="float: left;">
                            <asp:Label ID="Label39" runat="server" Font-Bold="true" Text="Paper Type"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 1px;">
                            <asp:DropDownList ID="ddlPaperTypeTM" runat="server" Width="110">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="HW" Selected="True">HomeWork</asp:ListItem>
                                <asp:ListItem Value="PT">Pretest</asp:ListItem>
                                <asp:ListItem Value="RT">Reg Test</asp:ListItem>
                                <asp:ListItem Value="FT">Final Test</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div style="float: left; margin-left: 32px;">
                        <div style="float: left;">
                            <asp:Label ID="Label40" runat="server" Font-Bold="true">Week# </asp:Label>
                        </div>
                        <div style="float: left; margin-left: 55px;">
                            <asp:DropDownList ID="ddlWeekIDTM" runat="server" OnSelectedIndexChanged="ddlWeekIDTM_SelectedIndexChanged" AutoPostBack="True" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div style="float: left; margin-left: 32px; display: none;">
                        <div style="float: left;">
                            <asp:Label ID="Label41" runat="server" Font-Bold="true">Set# </asp:Label>
                        </div>
                        <div style="float: left; margin-left: 5px;">
                            <asp:DropDownList ID="ddlSetNoTM" runat="server" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>

                    <div id="Div1" style="float: left; margin-left: 30px;" runat="server">
                        <div style="float: left;">
                            <asp:Label ID="Label42" runat="server" Font-Bold="true" Text="Sections"></asp:Label>
                        </div>
                        <div style="float: left; margin-left: 5px;">
                            <asp:DropDownList ID="ddlSectionsTM" runat="server" Width="110">
                                <asp:ListItem Value="0">Select</asp:ListItem>
                            </asp:DropDownList>
                        </div>
                    </div>


                    <div id="dvTestSectionSubmit" runat="server" visible="false" style="float: left; margin-left: 20px;">

                        <div style="float: left; margin-left: 10px;">
                            <asp:Button ID="btnClearFilterTM" runat="server" Text="Submit" OnClick="btnClearFilterTM_Click" />
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:Button ID="btnRepTM" runat="server" OnClick="btnRepTM_Click" Text="Replicate" />
                        </div>

                    </div>
                    <div id="dvTestAnswerKeySubmit" runat="server" visible="false" style="float: left; margin-left: 20px;">

                        <div style="float: left; margin-left: 10px;">
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </div>
                        <div style="float: left; margin-left: 10px;">
                            <asp:Button ID="btnCancelPage" runat="server" Text="Cancel" OnClick="btnCancelPage_Click" />
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>

        <table align="center">
            <tr>
                <td align="center">
                    <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblPrd" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" runat="server" Text="" Visible="false"></asp:Label></td>
            </tr>
            <tr>
                <td>
                    <asp:Label ID="lblAnsKeyError" runat="server" Text="" ForeColor="Red"></asp:Label></td>
            </tr>

        </table>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <center>
            <asp:Label ID="lblValText" runat="server" ForeColor="Red"></asp:Label></center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvTestSectionSetup" runat="server" visible="false">

            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <table id="tblCopy" runat="server" visible="false">
                    <tr>
                        <td align="left">Copy From:
                <asp:DropDownList ID="ddlCopyFrom" DataTextField="CoachPaperID" DataValueField="CoachPaperID" OnSelectedIndexChanged="ddlCopyFrom_SelectedIndexChanged" runat="server" AutoPostBack="true">
                    <asp:ListItem Value="-1">Select PaperID</asp:ListItem>
                </asp:DropDownList>
                            CopyTo:<asp:DropDownList ID="ddlCopyTo" DataTextField="CoachPaperID" DataValueField="CoachPaperID" runat="server" AutoPostBack="true">
                                <asp:ListItem Value="-1">Select PaperID</asp:ListItem>
                            </asp:DropDownList>
                            <asp:Button ID="btnContinue" OnClick="btnContinue_Click" runat="server" Text="Continue" />
                            &nbsp;&nbsp; 
                            <asp:Button ID="btnCancelCopy" OnClick="btnCancelCopy_Click" runat="server" Text="Cancel" />
                            <asp:Label ID="lblCopyErr" runat="server" Text="" ForeColor="Red"></asp:Label><br />

                        </td>
                    </tr>
                </table>
            </center>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <table align="center" id="tblTestSection" runat="server" visible="false">
                <tr>
                    <td align="center" colspan="8">
                        <table border="1" runat="server" cellpadding="5">
                            <tr>
                                <td align="left">CoachPaperId</td>
                                <td align="left">
                                    <asp:TextBox ID="txtTestNumber" runat="server" Enabled="false"></asp:TextBox>
                                    <asp:HiddenField ID="hdnTestSetUpSectionsId" runat="server" />
                                </td>
                            </tr>
                            <tr style="display: none;">
                                <td align="left">Level</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlLevel1" runat="server" Enabled="false"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left">SectionNumber</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlSectionNo" runat="server"></asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left">Subject</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlSubject" runat="server">
                                        <%--  
                                              <asp:ListItem Value="">Select</asp:ListItem>
                                              <asp:ListItem Value="Math">Math</asp:ListItem>
                                              <asp:ListItem Value="CriticalReading">Critical Reading</asp:ListItem>
                                              <asp:ListItem Value="Writing">Writing</asp:ListItem>
                                              <asp:ListItem Value="Pre-Mathcounts">Pre-Mathcounts</asp:ListItem>
                                              <asp:ListItem Value="Mathcounts">Mathcounts</asp:ListItem>
                                        --%>
                                    </asp:DropDownList>&nbsp;
                                <a class="ancHelpSubkect" style="cursor: pointer; color: blue; text-decoration: none;">Help</a></td>
                            </tr>
                            <tr>
                                <td align="left">Section Time Limit</td>
                                <td align="left">
                                    <asp:TextBox ID="txtTimeLimit" runat="server"></asp:TextBox>
                                    minutes
                                               <asp:RangeValidator ControlToValidate="txtTimeLimit" ID="RangeValidator1" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left"># Of Questions</td>
                                <td align="left">
                                    <asp:TextBox ID="txtNoOfQues" runat="server"></asp:TextBox>
                                    <asp:RangeValidator ControlToValidate="txtNoOfQues" ID="RangeValidator2" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Question Type</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlQuestionType" runat="server" OnSelectedIndexChanged="ddlQuestionType_SelectedIndexChanged" AutoPostBack="true">
                                        <asp:ListItem Value="">Select choice</asp:ListItem>
                                        <asp:ListItem Value="RadioButton">Multiple choice</asp:ListItem>
                                        <asp:ListItem Value="TextArea">Non-multiple choice</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="left"># Of Choices</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlNoOfChoices" runat="server" Enabled="false"></asp:DropDownList></td>
                            </tr>

                            <tr>
                                <td align="left">Question No. From</td>
                                <td align="left">
                                    <asp:TextBox ID="txtQuesNoFrom" runat="server"></asp:TextBox>
                                    <asp:RangeValidator ControlToValidate="txtQuesNoFrom" ID="RangeValidator3" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Question No. To</td>
                                <td align="left">
                                    <asp:TextBox ID="txtQuesNoTo" runat="server"></asp:TextBox>
                                    <asp:RangeValidator ControlToValidate="txtQuesNoTo" ID="RangeValidator4" runat="server" ErrorMessage="Only Numeric" MaximumValue="100" Type="Double"></asp:RangeValidator>
                                </td>
                            </tr>
                            <tr>
                                <td align="left">Penalty</td>
                                <td align="left">
                                    <asp:DropDownList ID="ddlPenalty" runat="server" Enabled="false">
                                        <asp:ListItem Value="Y">Yes</asp:ListItem>
                                        <asp:ListItem Value="N">No</asp:ListItem>
                                    </asp:DropDownList></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Button ID="btnAdd" OnClick="btnAdd_Click" runat="server" Text="Add" />
                                    &nbsp;&nbsp;&nbsp; 
                                    <asp:Button ID="BtnCancelTestSection" runat="server" OnClick="BtnCancelTestSection_Click" Text="Clear" /></td>
                            </tr>
                            <tr>
                                <td align="center" colspan="2">
                                    <asp:Label ID="lblErr" ForeColor="Red" runat="server"></asp:Label>&nbsp;</td>
                            </tr>
                        </table>
                    </td>
                </tr>


            </table>


            <table id="tblDGTestSection" runat="server" cellpadding="2" cellspacing="0" align="center" border="0">
                <tr>
                    <td align="center">
                        <asp:Label ID="lblTestSection" runat="server" Text="" Font-Bold="True" ForeColor="Green"></asp:Label>

                        <asp:DataGrid ID="DGTestSection" runat="server" DataKeyField="TestSetUpSectionsID" AutoGenerateColumns="False"
                            CellPadding="4" BorderStyle="None" CellSpacing="2" ForeColor="Black"
                            OnItemCommand="DGTestSection_Itemcommand">
                            <FooterStyle BackColor="#CCCCCC" />
                            <SelectedItemStyle BackColor="Green" Font-Bold="True" ForeColor="Black" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                            <HeaderStyle BackColor="#A69B00" ForeColor="Blue" />
                            <ItemStyle BackColor="White" />
                            <EditItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />

                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="TestSetUpSectionsID" HeaderText="ID" Visible="false" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionNumber" HeaderText="SectionNumber" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Subject" HeaderText="Subject" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionTimelimit" HeaderText="Timelimit" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfQuestions" HeaderText="#OfQuestions" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionType" HeaderText="QuestionType" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Penalty" HeaderText="Penalty" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfChoices" HeaderText="NumberOfChoices" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberFrom" HeaderText="QuestionNoFrom" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberTo" HeaderText="QuestionNumberTo" />

                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>
            <div style="clear: both; margin-bottom: 10px;"></div>

            <table id="tblDGCoachPaper" runat="server" align="center" cellpadding="2" cellspacing="0" border="0">
                <tr>
                    <td colspan="8" align="center">
                        <asp:Label CssClass="btn02" ID="lblCoachPaper" runat="server" Text="" ForeColor="Brown" Font-Bold="true"></asp:Label>

                        <asp:DataGrid ID="DGCoachPapers" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                            BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                            OnItemCommand="DGCoachPapers_ItemCommand">
                            <FooterStyle BackColor="#CCCCCC" />
                            <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                            <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                            <ItemStyle BackColor="White" />
                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                        <div style="display: none;">
                                            <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                            <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestSetup" HeaderText="TestSetup"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="PaperID"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>

                            </Columns>

                        </asp:DataGrid></td>
                </tr>
            </table>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvTestAnswerKey" runat="server" visible="false">

            <table align="center" id="tblDGAnswerKey" runat="server" width="600px" visible="false" border="1">

                <tr bgcolor="#FFFFFF">
                    <td colspan="8" align="center">
                        <div>
                            <asp:Label ID="lblAnswerKeyTitle" runat="server" Font-Bold="true" Style="float: left;"></asp:Label>
                            <asp:Label ID="lblAnswerKey" runat="server" Text="" ForeColor="#AE5C00" Font-Bold="true"></asp:Label>
                            <span style="float: right">
                                <asp:Button ID="btnManualAll" runat="server" Text="Set All Manual" OnClick="btnManualAll_Click" /></span>
                        </div>
                        <asp:DataGrid ID="dgAnswerKey" runat="server" Width="100%" AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" OnItemDataBound="dgAnswerKey_ItemDataBound"
                            PagerStyle-Mode="numericpages" DataKeyField="AnswerKeyRecID" AllowPaging="True" AllowSorting="True" PageSize="30">
                            <FooterStyle CssClass="GridFooter"></FooterStyle>
                            <SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
                            <AlternatingItemStyle Wrap="False"></AlternatingItemStyle>
                            <ItemStyle Wrap="False"></ItemStyle>
                            <HeaderStyle BackColor="#C49C00"></HeaderStyle>
                            <Columns>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="AnswerKeyRecID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>
                                <%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="CoachPaperID" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" Visible="false" HeaderText="TestNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="SectionNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" Visible="false" HeaderText="SectionNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" Visible="false" DataField="Level" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="Level" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="QuestionNumber" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="QuestionNumber" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="CorrectAnswer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="105px" HeaderStyle-Width="105px" ItemStyle-BackColor="Gainsboro">
                                    <ItemTemplate>
                                        <asp:TextBox ID="txtCorrectAnswer" runat="server" TextMode="MultiLine" Rows="3" Columns="20" OnPreRender="ddlCorrectAnswer_PreRender" Text='<%#DataBinder.Eval(Container.DataItem,"CorrectAnswer") %>'></asp:TextBox>
                                        <asp:DropDownList ID="ddlCorrectAnswer" runat="server" Width="95px" OnPreRender="ddlCorrectAnswer_PreRender1" SelectedValue='<%#DataBinder.Eval(Container.DataItem,"CorrectAnswer_DD") %>'>
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="A">A</asp:ListItem>
                                            <asp:ListItem Value="B">B</asp:ListItem>
                                            <asp:ListItem Value="C">C</asp:ListItem>
                                            <asp:ListItem Value="D">D</asp:ListItem>
                                            <asp:ListItem Value="E">E</asp:ListItem>
                                            <asp:ListItem Value="F">F</asp:ListItem>
                                            <asp:ListItem Value="G">G</asp:ListItem>
                                            <asp:ListItem Value="H">H</asp:ListItem>
                                            <asp:ListItem Value="I">I</asp:ListItem>
                                            <asp:ListItem Value="J">J</asp:ListItem>
                                            <%----%>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="DifficultyLevel" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlDiffLevel" runat="server" OnPreRender="ddlDiffLevel_PreRender1" SelectedValue='<%#DataBinder.Eval(Container.DataItem,"DifficultyLevel") %>'>
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="1">1</asp:ListItem>
                                            <asp:ListItem Value="2">2</asp:ListItem>
                                            <asp:ListItem Value="3">3</asp:ListItem>
                                            <asp:ListItem Value="4">4</asp:ListItem>
                                            <asp:ListItem Value="5">5</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Center" HeaderText="Manual" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" ItemStyle-BackColor="Gainsboro">
                                    <ItemTemplate>
                                        <asp:DropDownList ID="ddlManual" runat="server" OnPreRender="ddlManual_PreRender" SelectedValue='<%#DataBinder.Eval(Container.DataItem,"Manual") %>'>
                                            <asp:ListItem Value="">No</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                        </asp:DropDownList>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="QuestionType" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="QuestionType" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>
                                <asp:BoundColumn ItemStyle-HorizontalAlign="Center" DataField="CorrectAnswer" HeaderStyle-HorizontalAlign="Center" ItemStyle-Width="20px" HeaderStyle-Width="20px" HeaderText="CorrAnswer" ItemStyle-BackColor="Gainsboro" Visible="false"></asp:BoundColumn>

                            </Columns>
                            <PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC" Mode="NumericPages"></PagerStyle>
                        </asp:DataGrid>
                    </td>
                </tr>
                <tr id="TrSaveAnsKey" runat="server">
                    <td align="center">
                        <asp:Button ID="btnSaveAnswers" runat="server" Text="Save Answers" OnClick="btnSaveAnswers_Click" /></td>
                    <td align="center">
                        <asp:Button ID="btnCancelAnswerKey" runat="server" Text="Clear" OnClick="btnCancelAnswerKey_Click" /></td>


                </tr>
            </table>

            <table id="TblTestSection_AK" runat="server" cellpadding="2" cellspacing="0" align="center" border="0">
                <tr>
                    <td align="center">
                        <asp:Label ID="lblAnswerKey_AK" runat="server" Font-Bold="True" ForeColor="Green"></asp:Label>

                        <asp:DataGrid ID="DGTestSection1" runat="server" DataKeyField="TestSetUpSectionsID" AutoGenerateColumns="False"
                            CellPadding="4" BorderStyle="None" CellSpacing="2" ForeColor="Black"
                            OnItemCommand="DGTestSection1_Itemcommand">
                            <FooterStyle BackColor="#CCCCCC" />
                            <%--<SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />--%>
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                            <HeaderStyle BackColor="#A69B00" ForeColor="Blue" />
                            <ItemStyle BackColor="White" />
                            <EditItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />


                            <Columns>
                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnTSSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn DataField="TestSetUpSectionsID" HeaderText="ID" Visible="false" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="CoachPaperID" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionNumber" HeaderText="SectionNumber" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Subject" HeaderText="Subject" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SectionTimelimit" HeaderText="Timelimit" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfQuestions" HeaderText="#OfQuestions" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionType" HeaderText="QuestionType" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="NumberOfChoices" HeaderText="NumberOfChoices" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberFrom" HeaderText="QuestionNoFrom" />
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="QuestionNumberTo" HeaderText="QuestionNumberTo" />

                            </Columns>
                        </asp:DataGrid>
                    </td>
                </tr>
            </table>

            <div style="clear: both; margin-bottom: 10px;"></div>
            <table id="Table2" runat="server" align="center" cellpadding="2" cellspacing="0" border="0">
                <tr>
                    <td colspan="8" align="center">
                        <asp:Label CssClass="btn02" ID="Label44" runat="server" Text="" ForeColor="Brown" Font-Bold="true"></asp:Label>

                        <asp:DataGrid ID="DataGrid2" runat="server" DataKeyField="CoachPaperID" AutoGenerateColumns="False" CellPadding="4"
                            BackColor="#CCCCCC" BorderColor="Brown" BorderWidth="1px" BorderStyle="Solid" CellSpacing="2" ForeColor="Black"
                            OnItemCommand="DGCoachPapers_ItemCommand">
                            <FooterStyle BackColor="#CCCCCC" />
                            <SelectedItemStyle BackColor="Gainsboro" Font-Bold="True" ForeColor="Black" />
                            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" Mode="NumericPages" />
                            <HeaderStyle BackColor="#BE7C00" ForeColor="Blue" />
                            <ItemStyle BackColor="White" />
                            <Columns>

                                <asp:TemplateColumn>
                                    <ItemTemplate>
                                        <asp:LinkButton ID="lbtnSelect" runat="server" CommandName="Select" Text="Select"></asp:LinkButton>
                                        <div style="display: none;">
                                            <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'>'></asp:Label>
                                            <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'>'></asp:Label>

                                        </div>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestSetup" HeaderText="TestSetup"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachPaperID" HeaderText="PaperID"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="ProductGroup"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Level" HeaderText="Level"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="WeekId" HeaderText="WeekId"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="SetNum" HeaderText="SetNum"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Sections" HeaderText="Sections"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="PaperType" HeaderText="PaperType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DocType" HeaderText="DocType"></asp:BoundColumn>
                                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TestFileName" HeaderText="TestFileName"></asp:BoundColumn>

                            </Columns>

                        </asp:DataGrid></td>
                </tr>
            </table>
        </div>
    </div>
    <input type="hidden" runat="server" id="hdnPGIDTM" />
    <input type="hidden" runat="server" id="hdnPIdTM" />
    <input type="hidden" runat="server" id="hdnLevelTM" />
    <input type="hidden" runat="server" id="hdnWeekNoTM" />
    <input type="hidden" runat="server" id="hdnSectionTM" />

    <input type="hidden" runat="server" id="hdnPGId" />
    <input type="hidden" runat="server" id="hdnPId" />
    <input type="hidden" runat="server" id="hdnLevel" />
    <input type="hidden" runat="server" id="hdnWeekNo" />
    <input type="hidden" runat="server" id="hdnSectionR" />
    <input type="hidden" runat="server" id="hdnYear" />
    <input type="hidden" runat="server" id="hdnSemester" />
    <input type="hidden" runat="server" id="hdnDocType" />


    <input type="hidden" runat="server" id="hdnPGIdDW" />
    <input type="hidden" runat="server" id="hdnPIdDw" />
    <input type="hidden" runat="server" id="hdnLevelDw" />
    <input type="hidden" runat="server" id="hdnWeekNoDw" />
    <input type="hidden" runat="server" id="hdnSectionDw" />
    <input type="hidden" runat="server" id="hdnEventYearDw" />
    <input type="hidden" runat="server" id="hdnSemesterDw" />


    <input type="hidden" runat="server" id="hdnPgIdRP" />
    <input type="hidden" runat="server" id="hdnPIdRP" />
    <input type="hidden" runat="server" id="hdnLevelRP" />
    <input type="hidden" runat="server" id="hdnWeekRP" />
    <input type="hidden" runat="server" id="hdnPaperTypeRP" />
    <input type="hidden" runat="server" id="hdnDocTypeRP" />
    <input type="hidden" runat="server" id="hdnSemesterRP" />

    <asp:HiddenField ID="hdnpaperType" runat="server" Value="" />
</asp:Content>





