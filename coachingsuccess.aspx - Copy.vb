﻿
Imports System
Imports System.Web
Imports LinkPointTransaction
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports NorthSouth.BAL

Namespace VRegistration
    Partial Class coachingsuccess
        Inherits System.Web.UI.Page

        'Private us As CultureInfo = New CultureInfo("en-US")
        Protected order As String
        Protected resp As String
        Protected fIE5 As Boolean
        Public nRegFee As Decimal = 0
        Dim sbContests As New StringBuilder


        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            If (Not Session("LoggedIn") Is Nothing) Then
                If Session("LoggedIn") <> "True" Then
                    Dim entryToken As String = Nothing
                    If (Not Session("entryToken") Is Nothing) Then
                        entryToken = Session("entryToken").ToString().Substring(0, 1)
                        Server.Transfer("login.aspx?entry=" + entryToken)
                    Else
                        Server.Transfer(System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL"))
                    End If

                End If
            End If

            Dim isTestMode As Boolean = False
            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)

            If Not Session("CustIndID") = Nothing Then
                Session("ParentID") = Session("CustIndID")
            End If
            ' Put user code to initialize the page here
            If Not IsPostBack Then

                Dim sb As New StringBuilder
                Dim bc As HttpBrowserCapabilities = Request.Browser
                Dim re As StreamReader
                'Dim reDAS As StreamReader
                Dim nDonationAmt As Decimal = 0
                Dim nTaxDeductibleAmount As Decimal = 0
                Dim nregfee As Decimal = Session("RegFee")
                Dim nTaxDeductibleRegFee = CType(Session("RegFee"), Decimal) * (2 / 3)
                Dim emailBody As String = ""
                Dim screenConfirmText As String = ""
                Dim subMail As String
                subMail = "Confirmation received for NSF Coaching"
                Dim strDonationMessage As String = ""
                Dim approved As String = ""
                Dim paymentReference As String = ""
                Dim CoachEmail As String = ""

                If (isTestMode = True) Then
                    subMail = "Test Email(no real credit card Transactions):" + "Confirmation received for NSF 2007 Contests"
                End If

                If (Not Session("R_APPROVED") Is Nothing) Then
                    approved = Session("R_APPROVED").ToString
                End If

                If (Not Session("PaymentReference") Is Nothing) Then
                    paymentReference = Session("PaymentReference").ToString
                End If

                If (Not Session("CoachEmail") Is Nothing) Then
                    CoachEmail = Session("CoachEmail").ToString
                End If

                If (approved = "APPROVED") Then
                    DisplayContests()
                    ' DisplayMealcharges()

                    fIE5 = ((bc.Browser = "IE") _
                                AndAlso (bc.MajorVersion > 4))
                    order = CType(Session("outXml"), String)
                    ' resp = CType(Session("resp"), String)  'commented on 1/15/07
                    ' ParseResponse(resp)                   'commented on 1/15/07

                    If (Not (Session("Donation")) Is Nothing) Then
                        nDonationAmt = CType(Session("Donation"), Decimal)
                    End If
                    '    Session("PaymentReference") = R_OrderNum

                    If (nDonationAmt > 0) Then
                        'strDonationMessage = "Thank you also for your generous donation of " & FormatCurrency(nDonationAmt) & ".<BR> This will help NSF’s goal of providing scholarships ($250 each) to 500 poor but meritorious students in India for the year 2006-2007."
                        strDonationMessage = "We also thank you for your generous donation of " & FormatCurrency(nDonationAmt) & ".This will help in reaching our goal of providing 2500 scholarships ($250 each) to those who excel among the poor go to college in India for the upcoming academic year."
                    End If
                    'Your tax-deductible contribution is:  ( donation amount + 2/3 * AMNT)
                    nTaxDeductibleAmount = (nDonationAmt + (nTaxDeductibleRegFee))

                    Dim eventType As String
                    If (Session("EventID") = 1) Then
                        eventType = "Finals"
                    Else
                        eventType = "Regional"
                    End If

                    Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select I.Email,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME as ChildName,I2.FirstName +' '+ I2.LastName as ParentName, I2.Email as ParentEMail,CASE WHEN P.CoachName is NULL then P.ProductCode Else P.CoachName End AS ProductCode,CR.Level,CR.ProductID FROM CoachReg CR INNER JOIN CalSignUp C ON CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  AND C.EventYear = CR.EventYear AND C.Phase=CR.Phase and C.SessionNo = CR.SessionNo   Inner JOIN Product P ON C.ProductID=P.ProductID  Inner JOIN  IndSpouse I ON I.AutoMemberID = C.MemberID INNER JOIN Child Ch ON Ch.ChildNumber=CR.ChildNumber  INNER JOIN IndSpouse I2 ON Ch.MEMBERID = I2.AutoMemberID  WHERE CR.PaymentReference ='" & paymentReference & "'") ''** and C.StartDate < GETDATE ()")
                    Dim Emailid, Mailbody As String
                    While reader.Read()
                        Emailid = reader("Email")
                        CoachEmail = Emailid
                        Mailbody = "Dear Coach, <br><br>Note : Do not reply to the email above.  "
                        Mailbody = Mailbody & "<br><br>New student: " & reader("ChildName") & ", Parent Name: " & reader("ParentName") & ", Email: " & reader("ParentEMail") & ", " & reader("ProductCode") & ", Coach: " & Emailid & ", Level: " & reader("Level") & "."
                        ''Commented for Testing
                        SendEmail("A New student is joining ", Mailbody, Emailid)
                        ''Dim CoachAdminEmail As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 I.Email from IndSpouse I Inner JOIN Volunteer V ON V.MemberID=I.AutomemberID and V.RoleID=89 and V.ProductID=" & reader("ProductID"))
                        ''SendEmail("A New student is joining ", Mailbody, CoachAdminEmail)
                    End While

                    Dim eventYear As String
                    eventYear = Now.Year
                    '********  16072010 file name to be changed *********
                    re = File.OpenText(Server.MapPath("coachingConfirmingEmail.htm"))
                    emailBody = re.ReadToEnd
                    re.Close()
                    'emailBody = emailBody.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    emailBody = emailBody.Replace("[PAYMENTREFERENCE]", paymentReference)
                    emailBody = emailBody.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    emailBody = emailBody.Replace("[DATAGRID]", sbContests.ToString)
                    emailBody = emailBody.Replace("[DONATIONTEXT]", strDonationMessage)
                    emailBody = emailBody.Replace("[EVENTYEAR]", eventYear)
                    emailBody = emailBody.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nregfee))
                    emailBody = emailBody.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    emailBody = emailBody.Replace("[COACHEMAIL]", CoachEmail)

                    If Session("ProductCodeCoach") = "MB1" Or Session("ProductCodeCoach") = "MB2" Or Session("ProductCodeCoach") = "MB3" Or Session("ProductCodeCoach") = "MB4" Then
                        emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>Pre-Mathcounts:<a href='mailto:nsfpremathcounts@hotmail.com'>nsfpremathcounts@hotmail.com</a> </p>")
                        emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")
                    ElseIf Session("ProductCodeCoach") = "SATE" Or Session("ProductCodeCoach") = "SATM" Then
                        emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>SAT <a href='mailto:nsfsat@hotmail.com'>nsfsat@hotmail.com</a></p>")
                        ' emailBody = emailBody.Replace("[Link]", "")
                        emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")

                    ElseIf Session("ProductCodeCoach") = "JUV" Or Session("ProductCodeCoach") = "IUV" Or Session("ProductCodeCoach") = "SUV" Then
                        emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>Universal Values: <a href='mailto:nsfvalues.parents@gmail.com'>nsfvalues.parents@gmail.com</a> </p>")
                        ' emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc' Class='btn_02' target='_blank'>FAQ</a>")
                        emailBody = emailBody.Replace("[Link]", "")
                    Else
                        'emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>Pre-Mathcounts:<a href='mailto:nsfpremathcounts@hotmail.com'>nsfpremathcounts@hotmail.com</a> </p>")
                        'emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")
                    End If

                    ' SendDasMessage("Donation to North South Foundation", emailBody, CType(Session("LoginEmail"), String))

                    ''***Screen Population logic

                    re = File.OpenText(Server.MapPath("coachingConfirmingEmail.htm"))
                    screenConfirmText = re.ReadToEnd
                    re.Close()
                    'screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", paymentReference)
                    screenConfirmText = screenConfirmText.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    screenConfirmText = screenConfirmText.Replace("[DATAGRID]", sbContests.ToString)
                    screenConfirmText = screenConfirmText.Replace("[DONATIONTEXT]", strDonationMessage)
                    screenConfirmText = screenConfirmText.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nregfee))
                    screenConfirmText = screenConfirmText.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    screenConfirmText = screenConfirmText.Replace("[EVENTYEAR]", eventYear)
                    screenConfirmText = screenConfirmText.Replace("[EVENTTYPE]", eventType)
                    screenConfirmText = screenConfirmText.Replace("[COACHEMAIL]", CoachEmail)
                    'emailBody = emailBody.Replace("[EMAIL4CONTACT]", CoachEmail)
                    '<p>1. Mathcounts: <a href="mailto:nsfmathcounts@hotmail.com">nsfmathcounts@hotmail.com</a> </p><p>2. Pre-Mathcounts:<a href="mailto:nsfpremathcounts@hotmail.com">nsfpremathcounts@hotmail.com</a>
                    'If Session("ProductCodeCoach") = "MB2" Then
                    '    screenConfirmText = screenConfirmText.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")
                    'ElseIf Session("ProductCodeCoach") = "SATE" Or Session("ProductCodeCoach") = "SATM" Then
                    '    screenConfirmText = screenConfirmText.Replace("[Link]", "")
                    'Else
                    '    screenConfirmText = screenConfirmText.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc' Class='btn_02' target='_blank'>FAQ</a>")
                    'End If

                    If Session("ProductCodeCoach") = "MB1" Or Session("ProductCodeCoach") = "MB2" Or Session("ProductCodeCoach") = "MB3" Or Session("ProductCodeCoach") = "MB4" Then
                        screenConfirmText = screenConfirmText.Replace("[EMAIL4CONTACT]", "<p>Pre-Mathcounts:<a href='mailto:nsfpremathcounts@hotmail.com'>nsfpremathcounts@hotmail.com</a> </p>")
                        screenConfirmText = screenConfirmText.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")
                    ElseIf Session("ProductCodeCoach") = "SATE" Or Session("ProductCodeCoach") = "SATM" Then
                        screenConfirmText = screenConfirmText.Replace("[EMAIL4CONTACT]", "<p>SAT <a href='mailto:nsfsat@hotmail.com'>nsfsat@hotmail.com</a></p>")
                        screenConfirmText = screenConfirmText.Replace("[Link]", "")

                    ElseIf Session("ProductCodeCoach") = "JUV" Or Session("ProductCodeCoach") = "IUV" Or Session("ProductCodeCoach") = "SUV" Then
                        emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>Universal Values: <a href='mailto:nsfvalues.parents@gmail.com'>nsfvalues.parents@gmail.com</a> </p>")
                        ' emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Mathcounts - Level Determination.doc' Class='btn_02' target='_blank'>FAQ</a>")
                        emailBody = emailBody.Replace("[Link]", "")
                    Else
                        'emailBody = emailBody.Replace("[EMAIL4CONTACT]", "<p>Pre-Mathcounts:<a href='mailto:nsfpremathcounts@hotmail.com'>nsfpremathcounts@hotmail.com</a> </p>")
                        'emailBody = emailBody.Replace("[Link]", "<a href='../Private/BulletinBoard/Parent/Coaching/Pre-Mathcounts - Level Determination.doc'  Class='btn_02' target='_blank'>FAQ</a>")
                    End If

                    lblDonationMessage.Text = screenConfirmText.ToString
                    screenConfirmText = Nothing

                    'Response.Write(sbContests.ToString())

                    '*************************************************
                    Dim NewEmail As String = ""
                    NewEmail = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Email From IndSPouse where AutoMemberID=" & Session("CustIndID"))
                    ''Commented for Testing in App8 on 17-09-2012
                    If SendEmail(subMail, emailBody.ToString, NewEmail) Then ' CType(Session("LoginEmail"), String)
                        'If SendEmail(subMail, emailBody.ToString, CType("chitturi9@gmail.com", String)) Then
                        lblEmailStatus.Text = "An email was sent to you to confirm your registration."
                    Else
                        lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
                    End If

                    ''*** DAS Message EMail Communication
                    'Uncomment the following section if DAS email  needs to be sent.
                    'Dim strDASMessage As String
                    'reDAS = File.OpenText(Server.MapPath("DASMessage.htm"))
                    'strDASMessage = reDAS.ReadToEnd
                    'subMail = "DAS Message from North South Foundation"
                    'If (isTestMode = True) Then
                    '    subMail = "Test Email:" + "DAS Message from North South Foundation"
                    'End If
                    'SendDasMessage("DAS Message from North South Foundation", strDASMessage, CType(Session("LoginEmail"), String))
                End If
                End If
        End Sub

        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub

        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
            'Build Email Message
            Dim email As New MailMessage
            If Session("ProductCodeCoach") = "MB2" Or Session("ProductCodeCoach") = "MB1" Or Session("ProductCodeCoach") = "MB3" Or Session("ProductCodeCoach") = "MB4" Then
                email.From = New MailAddress("nsfpremathcounts@hotmail.com")
            ElseIf Session("ProductCodeCoach") = "SATE" Or Session("ProductCodeCoach") = "SATM" Then
                email.From = New MailAddress("nsfsat@hotmail.com")
            ElseIf Session("ProductCodeCoach") = "JUV" Or Session("ProductCodeCoach") = "IUV" Or Session("ProductCodeCoach") = "SUV" Then
                email.From = New MailAddress("nsfvalues.parents@gmail.com")
            Else
                'email.From = New MailAddress("nsfmathcounts@hotmail.com")
            End If
            'email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host

            Dim ok As Boolean = True
            Try
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
            Return ok
        End Function
        Private Sub SendDasMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)

            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@gmail.com")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))

            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host

            Try
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End Sub


        Private Sub DisplayContests()
            Dim sb As New StringBuilder

            Dim rowcount As Int32 = 0

            Dim connContest As New SqlConnection(Application("ConnectionString"))

            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}
            Dim ProdGroupCount As Double = 0.0

            'Session("PaymentReference")
            Dim SQLStr As String
            SQLStr = "select CR.CoachRegID,Ch.FIRST_NAME + ' ' + Ch.LAST_NAME AS ChildName, P.Name as ProductName,P.ProductCode,P.ProductGroupID,I.FirstName + ' ' + I.LastName as CoachName,C.Level,C.MaxCapacity,CR.ChildNumber,C.Day,CONVERT(varchar(15),CAST(TIME AS TIME),100) as Time,C.StartDate,C.Enddate,I.City,I.State,Case when CR.PaymentReference IS NULL then 'Y' Else 'N' End as Status "
            SQLStr = SQLStr & ",EF.RegFee,CASE WHEN GETDATE()> DATEADD(dd,1,EF.DeadlineDate) THEN EF.LateFee ELSE '' END as LateFee ,EF.ProductLevelPricing,CR.PaymentDate, CR.PaymentMode, CR.PaymentReference, CR.PaymentNotes from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join CalSignUp C ON "
            SQLStr = SQLStr & "CR.PaymentReference ='" & Session("PaymentReference") & "' AND CR.CMemberID = C.MemberID and CR.EventYear=C.EventYear and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  and C.Phase=CR.Phase and C.SessionNo = CR.SessionNo  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & " and CR.EventYear=" & Now.Year
            SqlHelper.FillDataset(connContest, CommandType.Text, SQLStr, dsContestant, tblConestant)
            If dsContestant.Tables.Count > 0 Then
                If dsContestant.Tables(0).Rows.Count > 0 Then
                    sb.Append("<table border=1 width= 900px  cellspacing=0 cellpadding=0>")
                    sb.Append("<tr bgcolor=lightblue forecolor=white>")
                    sb.Append("<td align=Center  width=15%><b>Child Name</b></td>")
                    sb.Append("<td align=Center  width=15%>Product</td>")
                    sb.Append("<td align=Center  width=15%>Coach Name</td>")
                    sb.Append("<td align=Center  width=25%>Coaching Date Time</td>")
                    sb.Append("<td align=Center  width=20%>Payment Info</td>")
                    sb.Append("</tr>")
                    Response.Write("<!-- I am here-->")
                    Response.Write("<!-- I am here2-->")
                    For rowcount = 0 To dsContestant.Tables(0).Rows.Count - 1
                        Response.Write("<!-- I am here3-->")
                        sb.Append("<tr>")
                        sb.Append("<td align=Center>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ChildName").ToString() + "</td>")
                        sb.Append("<td align=Center>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ProductCode").ToString() + "</td>")
                        sb.Append("<td align=Center>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("CoachName").ToString() + "</td>")
                        sb.Append("<td  align=Left>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("Day").ToString() + dsContestant.Tables(0).Rows(rowcount).Item("Time").ToString() + "<BR>")
                        sb.Append("Start Date : " + Date.Parse(dsContestant.Tables(0).Rows(rowcount).Item("StartDate").ToString()) + "</td>") '"<BR>" Geetha suggested to remove enddate
                        'sb.Append("End Date : " + Date.Parse(dsContestant.Tables(0).Rows(rowcount).Item("EndDate").ToString()) + "</td>")
                        sb.Append("<td align=Left>RegFee:")
                        ProdGroupCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From EventFees Where EventID=" & Session("EventID") & " and EventYear=" & Now.Year() & " and ProductGroupID=" & dsContestant.Tables(0).Rows(rowcount).Item("ProductGroupID") & " and ProductLevelPricing ='N'")
                        If ProdGroupCount > 0 Then
                            sb.Append(FormatCurrency((dsContestant.Tables(0).Rows(rowcount).Item("RegFee") / ProdGroupCount)).ToString() + "&nbsp;")
                        Else
                            sb.Append(FormatCurrency(dsContestant.Tables(0).Rows(rowcount).Item("RegFee")).ToString() + "&nbsp;")
                        End If

                        sb.Append(IIf(dsContestant.Tables(0).Rows(rowcount).Item("LateFee") <= "0", "", "Late Fee:" & FormatCurrency(dsContestant.Tables(0).Rows(rowcount).Item("LateFee").ToString())) + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentDate").ToString() + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentReference").ToString() + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentMode").ToString() + "</td></tr>")
                    Next
                    If Session("DiscountAmt") > 0 Then
                        sb.Append("<tr><td align=Center>Overall Discount</td><td></td><td></td><td></td>")
                        sb.Append("<td align='left'> Discount Amt:  ")
                        sb.Append(FormatCurrency(Session("DiscountAmt")).ToString() + "&nbsp;")
                        sb.Append("</td></tr>")
                    End If
                    sb.Append("</table>")
                Else
                    sb.Append(" ")
                End If
            End If
            sbContests.Append(sb.ToString)
            connContest = Nothing
        End Sub
    End Class

End Namespace

