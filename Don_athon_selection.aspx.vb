﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.IO

Partial Class Don_athon_selection
    Inherits System.Web.UI.Page
    Public i As Integer = 0
    'usp_SelectWalkMarathon
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack = True Then
            ' ltlTitle.Text = "North South Foundation - Excel-a-Thon : Noble Cause Through Brilliant Minds!"
            Dim homeURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL")
            Dim loginURL As String = System.Configuration.ConfigurationManager.AppSettings.Get("LoginURL")
            addMenuItemAt(0, "Home", homeURL)
            If (Session("LoggedIn") = "True") Then
                If Session("entryToken").ToString() = "Parent" Then
                    hlinkParentRegistration.Visible = True
                ElseIf Session("entryToken").ToString() = "Volunteer" Then
                    hlinkVolunteerRegistration.Visible = True
                ElseIf Session("entryToken").ToString() = "Donor" Then
                    hlinkDonorFunctions.Visible = True
                End If
                addLogoutMenuItem()
            Else
                If (loginURL <> Nothing And Session("entryToken") <> Nothing) Then
                    loginURL = loginURL + Session("entryToken").ToString().Substring(0, 1)
                    addMenuItem("Login", loginURL)
                End If
            End If
            If Request.QueryString("Ev") Is Nothing Then
                Literal1.Text = "No Event is there to show."
                BtnRegister.Enabled = False
                Exit Sub
            End If
            lblHeading.Text = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "Select name from Event where EventID=" & Request.QueryString("Ev").ToString())
            Dim ds_WalkaThon As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT   D.DonateAThonID,D.VenueAndTime  FROM DonateAThonCal D  WHERE  (D.EndDate >= GETDATE()) and D.EventID=" & Request.QueryString("Ev").ToString())
            ddlWalkaThon.DataSource = ds_WalkaThon
            ddlWalkaThon.DataTextField = "VenueAndTime"
            ddlWalkaThon.DataValueField = "DonateAThonID"
            ddlWalkaThon.DataBind()
            If ddlWalkaThon.Items.Count > 0 Then
                getDoanteAThon()
            Else
                Literal1.Text = "No Event is there to show"
                BtnRegister.Enabled = False
                trSelection.Visible = False
            End If

        End If
    End Sub

    Public Sub addMenuItem(ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addMenuItem(ByVal itemText As String, ByVal href As String)
        addMenuItem(New MenuItem("&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal itemText As String, ByVal href As String)
        addMenuItemAt(index, New MenuItem("&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" + itemText + "&nbsp;&nbsp;", itemText.ToUpper(), "", href))
    End Sub
    Public Sub addMenuItemAt(ByVal index As Integer, ByVal menuItem As MenuItem)
        If (Not menuItem Is Nothing) Then
            Dim mText As String = menuItem.Value
            Dim newItem As MenuItem = NavLinks.FindItem(mText.ToUpper()) 'if already present, fix the url
            If (newItem Is Nothing) Then
                Me.NavLinks.Items.Add(menuItem)
            Else
                newItem.NavigateUrl = menuItem.NavigateUrl
            End If
        End If
    End Sub

    Public Sub addLogoutMenuItem()
        addMenuItemAt(NavLinks.Items.Count, "Logout", "logout.aspx") 'add to the end
    End Sub
   
    Protected Sub ddlWalkaThon_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
       getDoanteAThon()
    End Sub

    Public Sub getDoanteAThon()
        Dim ds_WalkaThon As New DataSet
        Try
            ds_WalkaThon = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "Select D.DonateAThonID, D.EventID, D.EventYear, D.ClusterID, D.VenueAndTime, D.Description, CONVERT(VARCHAR(12),D.StartDate, 107) as StartDate, CONVERT(VARCHAR(12),D.Eventdate, 107) as EventDate,CONVERT(VARCHAR(12),D.EndDate, 107) as EndDate,C.ClusterCode from  DonateAThonCal D Inner Join Cluster C ON C.ClusterID= D.ClusterID where DonateAThonID = " & ddlWalkaThon.SelectedValue & "")
            If ds_WalkaThon.Tables(0).Rows.Count > 0 Then
                Literal1.Text = "<br><br><Table><tr><td align='left'> <b>Venue And Time</b> </td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("VenueAndTime").ToString() & "<br>"
                Literal1.Text = Literal1.Text & "</td></tr><tr><td align='left'><b>Description</b> </td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("Description").ToString() & "<br>"
                Literal1.Text = Literal1.Text & "</td></tr><tr><td align='left'><b>StartDate </b></td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("StartDate").ToString() & "<br>"
                Literal1.Text = Literal1.Text & "</td></tr><tr><td align='left'><b>EventDate</b> </td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("EventDate").ToString() & "<br>"
                Literal1.Text = Literal1.Text & "</td></tr><tr><td align='left'><b>EndDate</b> </td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("EndDate").ToString() & "<br>"
                Literal1.Text = Literal1.Text & "</td></tr><tr><td align='left'><b>Cluster</b> </td><td align='left'>: " & ds_WalkaThon.Tables(0).Rows(0)("ClusterCode").ToString() & "</td></tr></table>"
            End If
        Catch ex As Exception
            Response.Write("Select D.DonateAThonID, D.EventID, D.EventYear, D.ClusterID, D.VenueAndTime, D.Description, D.StartDate, D.EventDate,D.EndDate,C.ClusterCode from  DonateAThonCal D Inner Join Cluster C ON C.ClusterID= D.ClusterID where DonateAThonID = " & ddlWalkaThon.SelectedValue & "")
        End Try
        
    End Sub


    Protected Sub BtnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnRegister.Click
        Session("DonateAThonID") = ddlWalkaThon.SelectedValue
        Response.Redirect("DThonRegis.aspx?Ev=" + Request.QueryString("Ev").ToString())
    End Sub
End Class
