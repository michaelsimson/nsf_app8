<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="SummarySelections_Game.aspx.vb" Inherits="SummarySelections_Game"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
	<table width="100%" border="0">
				<tr>
					<td class="Heading"  align="center" colspan="2">
                         Summary of Your Game Selections
					</td>
				</tr>
				<tr>
					<td class="SmallFont">Parent Detailed Information
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<%--<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>--%>
							<tr>
								<td><asp:label id="lblStateZip" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">ChiChild/Children Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px"><asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="GameID"
							CellPadding="4" BorderWidth="1px" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblName" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Name") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblChildId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChildId") %>' CssClass="SmallFont" Visible="false">
										</asp:Label>
									</ItemTemplate>

<HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Prepclubs Selected" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblPrepclub" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductName") %>' CssClass="SmallFont">
										</asp:Label>
										<asp:Label id="lblProductId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ProductCode") %>' CssClass="SmallFont" Visible ="false" >
										</asp:Label>
									</ItemTemplate>

<HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Fee" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" HeaderStyle-Font-Underline="false">
									<ItemTemplate>
										<asp:Label id="lblFee" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Fee","{0:N2}") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>

<HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter" HeaderStyle-Font-Bold="true" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblChapter" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Chapter") %>' CssClass="SmallFont"></asp:Label>&nbsp;
										<asp:Label id="lblChapterId" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ChapterId") %>' CssClass="SmallFont" Visible ="false" ></asp:Label>
									</ItemTemplate>

<HeaderStyle Font-Bold="True" Font-Underline="False" ForeColor="#990000"></HeaderStyle>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid>
						</td>
                        
				</tr>
				
								<tr><td class="SmallFont" style="width:340px" align="right">
								Total Amount to be paid :
                        <asp:Label ID="lblTotalAmt" style="color:Red" runat="server" Text="Label" Width="118px"></asp:Label>
					</td>
					<td>&nbsp;</td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnContinue" runat="server" CssClass="FormButtonCenter" Text="Continue" ></asp:button></td>
				</tr>
				<tr>
				<td colspan="2">&nbsp;&nbsp;</td>
				</tr>
				<tr>
					<td   colspan="2" class="SmallFont"  style="color:Red; height: 19px;">
					Please verify your selections for each child.   
					</td>
				</tr>
				<tr>
				<td   colspan="2" class="SmallFont" style="color:Red">
                    Once paid, your payment is non-refundable. 2/3rd of the fee is a tax-deductible contribution.
				</tr>
			</table>
			<table id="Table1">
				<tr>
					<td class="ContentSubTitle" align="left" colspan="2">
						<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></td>
				</tr>
			</table>
</asp:Content>

