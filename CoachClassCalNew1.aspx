﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CoachClassCalNew1.aspx.cs" Inherits="CoachClassCalNew" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css">
    <link href="css/ClassCal.css" rel="stylesheet" />
    <link href='http://fonts.googleapis.com/css?family=Nunito:400,300' rel='stylesheet' type='text/css'>
    <link href="css/HtmlGridTable.css" rel="stylesheet" />
    <script src="Scripts/jquery-1.9.1.js"></script>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="Scripts/jquery.toastmessage.js"></script>

    <link href="css/jquery.toastmessage.css" rel="stylesheet" />
    <link href="css/BeatPicker.min.css" rel="stylesheet" />
    <script src="js/BeatPicker.min.js"></script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <%--  <link href="css/Loader.css" rel="stylesheet" />--%>

    <div>
        <script type="text/javascript">



            $(function (e) {

                // showPage();
                $(".beatpicker-clearButton").hide();
                listCoaches();


            });
            $(document).on("click", "#btnSubmit", function (e) {


                if (validateSubmit() == "1") {

                    $("#spnGridTitle").show();

                    loadSchedule(0);
                }
            });
            function loadSchedule(coachClassCalID) {

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, Semester: semester, Level: level, SessionNo: sessionNo, CoachClassCalID: coachClassCalID } });

                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var tblHtml = "";
                        tblHtml += "<thead>";
                        tblHtml += " <tr>";

                        tblHtml += "<th>Action</th>";
                        tblHtml += "<th>Date</th>";
                        tblHtml += " <th>Time</th>";
                        tblHtml += "<th>Class Stype</th>";
                        tblHtml += "<th>Week#</th>";
                        tblHtml += "<th>Status</th>";
                        tblHtml += "<th>Makeup</th>";
                        tblHtml += "<th>Substitute</th>";

                        tblHtml += "<th>HwRelDate</th>";
                        tblHtml += "<th>HWDueDate</th>";
                        tblHtml += "<th>SRelDate</th>";
                        tblHtml += "<th>ARelDate</th>";
                        tblHtml += "</tr>";
                        tblHtml += "</thead>";
                        var responseCount = 0;
                        var acceptedCount = 0;
                        var i = 0;
                        var surveyTitle = "";
                        if (JSON.stringify(data.d.length) > 0) {

                            tblHtml += "<tbody>";
                            $.each(data.d, function (index, value) {


                                tblHtml += "<tr>";

                                tblHtml += "<tr>";
                                //tblHtml += '<td><div style="float:left;"><input type="button" id="btnModify" attr-CoachClassID=' + value.CoachClassCalID + ' style="padding: 5px; font-size: 16px; cursor: pointer;" value="Modify" class="btnClass modify"  /></div><div style="float:left;"> <input type="button" id="btnDelete" attr-CoachClassID=' + value.CoachClassCalID + ' style="padding: 5px; font-size: 16px; cursor: pointer;" value="Delete" class="btnClass modify"  /></div></td>';


                                if (roleID == "88") {
                                    tblHtml += '<td><div style="float:left;"><a attr-CoachClassID=' + value.CoachClassCalID + ' class="modify" title="Modify" style="cursor:pointer;"><i style="color:blue;" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div></td>';
                                } else {
                                    tblHtml += '<td><div style="float:left;"><a attr-CoachClassID=' + value.CoachClassCalID + ' class="modify" title="Modify" style="cursor:pointer;"><i style="color:blue;" class="fa fa-pencil-square-o fa-2x" aria-hidden="true"></i></a> </div><div style="float:left; position:relative; left:10px;"><a style="cursor:pointer;" attr-CoachClassID=' + value.CoachClassCalID + ' class="delete" title="Delete"><i class="fa fa-trash-o fa-2x" style="color:red;" aria-hidden="true"></i></a></div></td>';
                                }
                                tblHtml += "<td>" + value.Date + "</td>";
                                tblHtml += "<td>" + value.Time + "</td>";
                                tblHtml += "<td>" + value.ClassType + "</td>";
                                tblHtml += "<td>" + value.WeekNo + "</td>";
                                tblHtml += "<td>" + value.Status + "</td>";
                                tblHtml += "<td>" + value.Makeup + "</td>";
                                tblHtml += "<td>" + value.SubstituteCoachName + "</td>";

                                tblHtml += "<td>" + (value.HWDeadlineDate == null ? "" : value.HWDeadlineDate) + "</td>";
                                tblHtml += "<td>" + (value.HWDueDate == null ? "" : value.HWDueDate) + "</td>";
                                tblHtml += "<td>" + (value.SRelDate == null ? "" : value.SRelDate) + "</td>";
                                tblHtml += "<td>" + (value.ARelDate == null ? "" : value.ARelDate) + "</td>";

                                tblHtml += "</tr>";


                            });


                        } else {
                            tblHtml += "<tr style='border: 1px solid black; border-collapse: collapse;'>";
                            tblHtml += "<td style='border: 1px solid black; border-collapse: collapse; color:red; font-weight:bold;' colspan='13' align='center'>No record exists";
                            tblHtml += "</td>";
                            tblHtml += "</tr>";



                        }
                        tblHtml += "</tbody>";
                        $("#table").html(tblHtml);

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            $(document).on("click", "#btnAddNextClass", function (e) {
                $("#dvAddClass").show();
                $("#selClassType").removeAttr("disabled");
                $("#txtWeekNo").attr("disabled", "disabled");
                $("#txtClassDate").attr("disabled", "disabled");
                $("#selTime").attr("disabled", "disabled");

            });

            $(document).on("click", "#btnCancel", function (e) {
                $("#dvAddClass").hide();
            });
            $(document).on("click", ".modify", function (e) {
                $("#dvAddClass").show();
            });

            function postNewClass() {
                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();
                var status = $("#selStatus").val();
                var hrelDate = $("#txtHwRelDate").val();
                var dueDate = $("#txtDueDate").val();
                var sRelDate = $("#txtSRelDate").val();
                var aRelDate = $("#txtAnsDate").val();
                var weekNo = $("#txtWeekNo").val();
                var day = "Wednesday";
                var duration = "120";
                var substituteCoachId = 0;

                if (classType == "Substitute") {
                    substituteCoachId = $("#selSubCoach").val();

                }


                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: date, Day: day, Time: time, Duration: duration, WeekNo: weekNo, Status: status, ClassType: classType, Substitute: substituteCoachId } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/PostNewClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.RetVal > 0) {

                                    if (value.CoachpaperID > 0) {

                                        inserReleaseDates(value.CoachpaperID);
                                    }
                                    loadSchedule(0);
                                    $("#spnGridTitle").show();
                                    $().toastmessage('showToast', {
                                        text: 'Class added successfully!',
                                        sticky: true,
                                        position: 'top-right',
                                        type: 'success',
                                        close: function () { console.log("toast is closed ..."); }
                                    });
                                    clear();
                                }
                            });
                        }

                    }
                });
            }

            $(document).on("click", "#btnSave", function (e) {

                validateMakeupSessions();
                //var classType = $("#selClassType").val();


                //if (validateAddClass() == "1") {
                //    if (classType == "Substitute") {
                //        verifyRegularClasCancelled();
                //        var status = $("#hdnStatus").val();
                //        if (status != "" && status != "On") {

                //            if ($("#hdnCoachClassCalID").val() != "0") {
                //                updateCoachClassSchedule();
                //            } else {
                //                postNewClass();
                //            }

                //        } else {

                //            $().toastmessage('showToast', {
                //                text: 'Substitute class can only be created if the regular class is cancelled.',
                //                sticky: true,
                //                position: 'top-right',
                //                type: 'error',
                //                close: function () { console.log("toast is closed ..."); }
                //            });
                //        }
                //    } else if (classType == "Makeup") {
                //        $("#txtClassDate").removeAttr("disabled");
                //        $("#selTime").removeAttr("disabled");
                //    } else {
                //        $("#txtClassDate").attr("disabled", "disabled");
                //        $("#selTime").attr("disabled", "disabled");
                //        if (validateAddClass() == "1") {
                //            if ($("#hdnCoachClassCalID").val() != "0") {
                //                updateCoachClassSchedule();

                //            } else {
                //                postNewClass();
                //            }
                //        }
                //    }
                //}

            });

            function showHideDateTime() {
                var classType = $("#selClassType").val();

                if (classType == "Substitute") {

                    $("#dvSubstitute").show();
                    $("#selTime").attr("disabled", "disabled");
                    $("#txtClassDate").attr("disabled", "disabled");
                    $("#fieldAddClass").css("width", "940");
                    $("#txtWeekNo").removeAttr("disabled");
                } else if (classType == "Makeup") {
                    $("#selTime").removeAttr("disabled");
                    $("#txtClassDate").removeAttr("disabled");
                    $("#txtWeekNo").removeAttr("disabled");
                } else if (classType == "Extra") {
                    $("#selTime").removeAttr("disabled");
                    $("#txtClassDate").removeAttr("disabled");
                    $("#txtWeekNo").removeAttr("disabled");
                } else {

                    $("#dvSubstitute").hide();
                    $("#selTime").attr("disabled", "disabled");
                    $("#txtClassDate").attr("disabled", "disabled");
                    $("#fieldAddClass").css("width", "740;");
                    $("#txtWeekNo").attr("disabled", "disabled");
                    getWeekNo();
                }
            }

            $(document).on("change", "#selClassType", function (e) {

                showHideDateTime();
            });

            function verifyRegularClasCancelled() {
                var classType = $("#selClassType").val();
                var status;
                if (classType == "Substitute") {
                    var memberID = $("#selCoach").val();
                    var eventId = $("#selEvent").val();
                    var eventyear = $("#selEventyear").val();
                    var pgId = $("#selProductGroup").val();
                    var pgcode = $("#selProductGroup option:selected").text();
                    var pId = $("#selProduct").val();
                    var pCode = $("#selProduct option:selected").text();
                    var level = $("#selLevel").val();
                    var sessionNo = $("#selSession").val();
                    var semester = $("#selSemester").val();
                    var weekNo = $("#txtWeekNo").val();
                    var classType = "Regular";

                    var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, WeekNo: weekNo, ClassType: classType } });


                    $.ajax({
                        type: "POST",
                        url: "CoachClassCalNew.aspx/VerifyRegularClassStatus",
                        data: jsonData,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        success: function (data) {
                            if (JSON.stringify(data.d.length) > 0) {

                                $.each(data.d, function (index, value) {
                                    status = value.Status;
                                    $("#hdnStatus").val();
                                });
                            }
                        }
                    });

                }

                return status;
            }

            $(document).on("click", "#btnCancel", function (e) {

                clear();

            });

            function clear() {

                $("#selClassType").val("0");
                $("#selSubCoach").val("0");
                $("#selStatus").val("On");
                $("#txtWeekNo").val("");
                $("#txtHwRelDate").val("");
                $("#txtSRelDate").val("");
                $("#txtDueDate").val("");
                $("#txtAnsDate").val("");
                $("#selTime").val("0");
                $("#txtClassDate").val("");
                $("#dvClassDate").show();
                $("#dvClassTime").show();
                $("#dvSubstitute").hide();
            }
            function loadScheduleBaseonId(coachClassCalID) {

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductId: pId, Semester: semester, Level: level, SessionNo: sessionNo, CoachClassCalID: coachClassCalID } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                $("#selClassType").val(value.ClassType);

                                $("#selStatus").val(value.Status);
                                $("#txtWeekNo").val(value.WeekNo);

                                $("#txtClassDate").val(value.Date);

                                $("#selTime").val(value.Time);

                                $("#selSubCoach").val(value.Substitute);

                                $("#selClassType").attr("disabled", "disabled");
                                $("#txtWeekNo").attr("disabled", "disabled");
                                $("#txtClassDate").attr("disabled", "disabled");
                                $("#selTime").attr("disabled", "disabled");

                                if (value.ClassType == "Substitute") {
                                    $("#dvSubstitute").show();
                                    $("#fieldAddClass").css("width", "940");
                                } else {
                                    $("#dvSubstitute").hide();
                                    $("#fieldAddClass").css("width", "740");
                                }

                                $("#txtHwRelDate").val(value.HWDeadlineDate);
                                $("#txtDueDate").val(value.HWDueDate);
                                $("#txtSRelDate").val(value.SRelDate);
                                $("#txtAnsDate").val(value.ARelDate);

                            });
                        }
                    },
                });
            }

            $(document).on("click", ".modify", function (e) {
                var coachClassCalID = $(this).attr("attr-CoachClassID");

                $("#hdnCoachClassCalID").val(coachClassCalID);
                loadScheduleBaseonId(coachClassCalID);
            });




            function statusMessage(message) {
                $().toastmessage('showToast', {
                    text: message,
                    sticky: true,
                    position: 'top-right',
                    type: 'error',
                    close: function () { console.log("toast is closed ..."); }
                });
            }

            function validateAddClass() {
                var retVal = "1";
                var msg = "";
                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var classType = $("#selClassType").val();
                var date = $("#txtClassDate").val();
                var time = $("#selTime").val();
                var status = $("#selStatus").val();
                var weekNo = $("#txtWeekNo").val();

                if (eventyear == "0") {
                    retVal = "-1";
                    msg = "Please select Event year";
                } else if (semester == "0") {
                    retVal = "-1";
                    msg = "Please select Semester";
                } else if (memberID == "0") {
                    retVal = "-1";
                    msg = "Please select Coach";
                } else if (pgId == "0") {
                    retVal = "-1";
                    msg = "Please select ProductGroup";
                } else if (pId == "0") {
                    retVal = "-1";
                    msg = "Please select Product";
                } else if (level == "0") {
                    retVal = "-1";
                    msg = "Please select Level";
                } else if (sessionNo == "0") {
                    retVal = "-1";
                    msg = "Please select sessionNo";
                } else if (classType == "0") {
                    retVal = "-1";
                    msg = "Please select Class Type";
                } else if (classType == "Regular" || classType == "Makeup" || classType == "Extra") {
                    if (date == "") {
                        retVal = "-1";
                        msg = "Please enter Date";
                    }
                } else if (classType == "Regular" || classType == "Makeup" || classType == "Extra") {
                    if (time == "0") {
                        retVal = "-1";
                        msg = "Please enter Time";
                    }
                } else if (weekNo == "") {
                    retVal = "-1";
                    msg = "Please enter Week No";
                } else if (status == "0") {
                    retVal = "-1";
                    msg = "Please select Status";
                }
                if (msg != "") {
                    statusMessage(msg);
                }
                return retVal;

            }

            function updateCoachClassSchedule() {
                var coachClassCalID = $("#hdnCoachClassCalID").val();
                var status = $("#selStatus").val();

                var jsonData = JSON.stringify({ objCoachClass: { CoachClassCalID: coachClassCalID, Status: status } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/UpdateCoachClassSchedule",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d) == 1) {
                            loadSchedule(0);
                            $("#spnGridTitle").show();
                            $().toastmessage('showToast', {
                                text: 'Class updated successfully!',
                                sticky: true,
                                position: 'top-right',
                                type: 'success',
                                close: function () { console.log("toast is closed ..."); }
                            });

                        }
                    }
                });
            }

            function getClassStartDateAndEndDate() {

                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();


                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, WeekNo: weekNo } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetClassStartDateAndEndDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                var day = value.Day;
                                var startDate = value.ClassStartDate;
                                var endDate = value.ClassEndDate;
                                var hDueDate = value.HWDueDate;
                                var aRelDate = value.ARelDate;
                                var sRelDate = value.SRelDate;
                                $("#txtClassDate").val(startDate);
                                $("#selTime").val(value.Time);
                                $("#hdnDay").val(day);
                                $("#txtHwRelDate").val(startDate);
                                $("#txtDueDate").val(hDueDate);
                                $("#txtSRelDate").val(sRelDate);
                                $("#txtAnsDate").val(aRelDate);
                            });
                        }
                    }
                });

                return status;
            }


            $(document).on("keyup", "#txtWeekNo", function (e) {
                if ($(this).val().length > 0) {

                    var classType = $("#selClassType").val();
                    if (classType == "Substitute") {
                        getClassStartDateAndEndDate();
                    }
                }
            });



            function getWeekNo() {
                var classType = $("#selClassType").val();
                var status = "On";

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                var jsonData = JSON.stringify({ CoachClassCal: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, ClassType: classType, Status: status } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/GetWeekNo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                var weekNo = parseInt(value.WeekNo) + 1;
                                $("#txtWeekNo").val(weekNo);
                                getClassStartDateAndEndDate();
                            });
                        };
                    }
                });
            }

            function inserReleaseDates(coachPaperID) {

                var classType = $("#selClassType").val();
                var status = "On";

                var memberID = $("#selCoach").val();

                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var hwRelDate = $("#txtHwRelDate").val();
                var hwDueDate = $("#txtDueDate").val();
                var aRelDate = $("#txtSRelDate").val();
                var sRelDate = $("#txtAnsDate").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;



                var jsonData = JSON.stringify({ objCoachClass: { CoachpaperID: coachPaperID, SessionNo: sessionNo, Semester: semester, HWDeadlineDate: hwRelDate, HWDueDate: hwDueDate, ARelDate: aRelDate, SRelDate: sRelDate, LoginID: loginID, MemberId: memberID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/InsertCoachRelDate",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $().toastmessage('showToast', {
                                text: 'Class added successfully!',
                                sticky: true,
                                position: 'top-right',
                                type: 'success',
                                close: function () { console.log("toast is closed ..."); }
                            });
                            clear();

                            loadSchedule(0);
                            $("#spnGridTitle").show();
                        }
                    }
                });

            }

            function listCoaches() {


                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var loginID = document.getElementById("<%=hdnLoginID.ClientID%>").value;
                var roleID = document.getElementById("<%=hdnRoleID.ClientID%>").value;

                if (roleID != "88") {
                    loginID = 0;
                }

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: loginID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListCoaches",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var coachId;
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {

                                $("#selCoach").append($("<option></option>").val
              (value.MemberID).html(value.Coachname));
                                coachId = value.MemberID;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selCoach").removeAttr("disabled");
                            } else {
                                $("#selCoach").val(coachId);
                                $("#selCoach").attr("disabled", "disabled");
                                listProductGroups();
                            }
                        }
                    }
                });

            }
            function listProductGroups() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListproductGroups",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var productGroupID;
                            $.each(data.d, function (index, value) {
                                $("#selProductGroup").append($("<option></option>").val
            (value.ProductGroupId).html(value.ProductGroup));
                                productGroupID = value.ProductGroupId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProductGroup").removeAttr("disabled");
                            } else {
                                $("#selProductGroup").val(productGroupID);
                                $("#selProductGroup").attr("disabled", "disabled");
                                listProducts();
                            }
                        }
                    }
                });
            }
            function listProducts() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID } });


                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListProducts",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var productId;
                            $.each(data.d, function (index, value) {
                                $("#selProduct").append($("<option></option>").val
              (value.ProductId).html(value.Product));
                                productId = value.ProductId;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selProduct").removeAttr("disabled");
                            } else {
                                $("#selProduct").val(productId);
                                $("#selProduct").attr("disabled", "disabled");
                                listlevels();
                            }
                        }
                    }
                });
            }
            function listlevels() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListLevel",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var level;
                            $.each(data.d, function (index, value) {
                                $("#selLevel").append($("<option></option>").val
                                            (value.Level).html(value.Level));
                                level = value.Level;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selLevel").removeAttr("disabled");
                            } else {
                                $("#selLevel").val(level);
                                $("#selLevel").attr("disabled", "disabled");
                                listSessionNO();
                            }
                        }
                    }
                });
            }
            function listSessionNO() {
                var eventyear = $("#selEventyear").val();
                var semester = $("#selSemester").val();
                var memberID = $("#selCoach").val();
                var productGroupID = $("#selProductGroup").val();
                var productId = $("#selProduct").val();
                var level = $("#selLevel").val();

                var jsonData = JSON.stringify({ objProdGrp: { Eventyear: eventyear, Semester: semester, MemberID: memberID, ProductGroupId: productGroupID, ProductId: productId, Level: level } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ListSessionNo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {
                            var sessionNo;
                            $.each(data.d, function (index, value) {
                                $("#selSession").append($("<option></option>").val
               (value.SessionNo).html(value.SessionNo));
                                sessionNo = value.SessionNo;
                            });

                            if (JSON.stringify(data.d.length) > 1) {

                                $("#selSession").removeAttr("disabled");
                            } else {
                                $("#selSession").val(sessionNo);
                                $("#selSession").attr("disabled", "disabled");
                            }
                        }
                    }
                });
            }

            $(document).on("change", "#selCoach", function (e) {
                listProductGroups();
            });
            $(document).on("change", "#selProductGroup", function (e) {
                listProducts();
            });
            $(document).on("change", "#selProduct", function (e) {
                listlevels();
            });
            $(document).on("change", "#selLevel", function (e) {
                listSessionNO();
            });

            function showPage() {
                document.getElementById("loader").style.display = "none";
                document.getElementById("myDiv").style.display = "block";
            }

            function validateSubmit() {

                var retval = "1";

                var memberID = $("#selCoach").val();

                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();

                if (eventyear == "0") {
                    retval = "-1";
                    msg = "Please select Event year";
                } else if (semester == "0") {
                    retval = "-1";
                    msg = "Please select Semester";
                } else if (memberID == "0") {
                    retval = "-1";
                    msg = "Please select Coach";
                } else if (pgId == "0") {
                    retval = "-1";
                    msg = "Please select ProductGroup";
                } else if (pId == "0") {
                    retval = "-1";
                    msg = "Please select Product";
                } else if (level == "0") {
                    retval = "-1";
                    msg = "Please select Level";
                } else if (sessionNo == "0") {
                    retval = "-1";
                    msg = "Please select sessionNo";
                }
                if (retval == "-1") {
                    statusMessage(msg);
                }
                return retval;

            }

            function deleteClass(coachClassCalID) {

                var jsonData = JSON.stringify({ objCoachClass: { CoachClassCalID: coachClassCalID } });

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/DeleteClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $().toastmessage('showToast', {
                                text: 'Deleted successfully',
                                sticky: true,
                                position: 'top-right',
                                type: 'success',
                                close: function () { console.log("toast is closed ..."); }
                            });
                            loadSchedule(0);
                        }
                    },
                });
            }

            $(document).on("click", ".delete", function (e) {
                var coachClassCalID = $(this).attr("attr-CoachClassID");

                if (confirm("Are you sure want to delete?")) {

                    deleteClass(coachClassCalID);
                }
            });

            function validateMakeupSessions() {

                var classDate = $("#txtClassDate").val();
                var classTime = $("#selTime").val();


                var classType = $("#selClassType").val();
                var status;

                var memberID = $("#selCoach").val();
                var eventId = $("#selEvent").val();
                var eventyear = $("#selEventyear").val();
                var pgId = $("#selProductGroup").val();
                var pgcode = $("#selProductGroup option:selected").text();
                var pId = $("#selProduct").val();
                var pCode = $("#selProduct option:selected").text();
                var level = $("#selLevel").val();
                var sessionNo = $("#selSession").val();
                var semester = $("#selSemester").val();
                var weekNo = $("#txtWeekNo").val();


                var jsonData = JSON.stringify({ objCoachClass: { MemberId: memberID, EventId: eventId, EventYear: eventyear, ProductGroupId: pgId, ProductGroup: pgcode, ProductId: pId, Product: pCode, Semester: semester, Level: level, SessionNo: sessionNo, Date: classDate, Time: classTime } });


              

                $.ajax({
                    type: "POST",
                    url: "CoachClassCalNew.aspx/ValidatemakeupExtraClass",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        if (JSON.stringify(data.d.length) > 0) {

                            $.each(data.d, function (index, value) {
                                if (value.Retval == "1") {

                                    if ($("#hdnCoachClassCalID").val() != "0") {
                                        if (validateAddClass() == "1") {
                                            updateCoachClassSchedule();
                                        }
                                    } else {
                                        if (validateAddClass() == "1") {
                                            postNewClass();
                                        }
                                    }

                                } else {
                                    statusMessage("Makeup date and time should be greater than current date and time");
                                }
                            });
                        }
                    },
                });


            }

        </script>
    </div>
    <%--    <link href="css/CoachClassCal.css" rel="stylesheet" />
    <div>
        <center>
            <h2>Coach Class Calendar</h2>
        </center>
    </div>
    <div class="form">

        <div>
            <div style="float: left;">
                <span id="spnEventYear">Event Year</span>
            </div>
            <div style="float: left; margin-left: 10px;">
                <select id="selEventYeat">
                    <option value="0">Seleect</option>
                    <option value="2017-18">2017-18</option>
                    <option value="2016-17" selected="selected">2016-17</option>
                </select>
            </div>
        </div>
        <div>
            <div style="float: left;">
                <span id="Span1">Event</span>
            </div>
            <div style="float: left; margin-left: 10px;">
                <input type="text" id="txtEvent" />
            </div>
        </div>

    </div>--%>

    <div>
        <center>
            <h2>Coach Class Calendar</h2>
        </center>
    </div>
    <div style="width: 1000px; margin-left: auto; margin-right: auto;">
        <center>
            <fieldset style="padding: 0px;">

                <div style="float: left;">
                    <label for="name">Event Year:</label>
                    <select id="selEventyear">
                        <option value="0">Select</option>
                        <option value="2017">2017-18</option>
                        <option value="2016" selected="selected">2016-17</option>
                    </select>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <label for="mail">Event:</label>
                    <select id="selEvent">

                        <option value="13" selected="selected">Coaching</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Semester:</label>
                    <select id="selSemester" style="width: 100px;">
                        <option value="Fall">Fall</option>
                        <option value="Spring" selected="selected">Spring</option>
                        <option value="Summer">Summer</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="">Coach:</label>
                    <select id="selCoach" style="width: 150px;">

                        <option value="0">Select</option>


                    </select>
                </div>



                <div style="float: left; margin-left: 20px;">
                    <label for="selPgGroup">Product Group:</label>
                    <select id="selProductGroup" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Product:</label>
                    <select id="selProduct" style="width: 100px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Level:</label>
                    <select id="selLevel" style="width: 150px;">
                        <option value="0">Select</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Session:</label>
                    <select id="selSession">
                        <option value="0">Select</option>

                    </select>
                </div>

            </fieldset>
        </center>
    </div>

    <div style="clear: both;"></div>
    <div align="center">
        <center>
            <input type="button" id="btnSubmit" value="Submit" class="btnClass" style="cursor: pointer;" />

        </center>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="left">


        <input type="button" class="btnClass" id="btnAddNextClass" value="Add New Class" style="cursor: pointer;" />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvAddClass" align="center" style="margin-left: auto; margin-right: auto; padding: 10px; border: 1px solid #c6cccd; display: none;">
        <center>
            <h2>Add New Class</h2>
            <fieldset id="fieldAddClass" style="padding: 0; width: 740px;">

                <div style="float: left;">
                    <label for="name">Class Type</label>
                    <select id="selClassType">
                        <option value="0">Select</option>
                        <option value="Regular">Regular</option>
                        <option value="Makeup">Makeup</option>
                        <option value="Substitute">Substitute</option>
                        <option value="Extra">Extra</option>
                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Week #:</label>
                    <input type="text" id="txtWeekNo" style="padding: 6px;" />
                </div>
                <div style="float: left; margin-left: 20px;" id="dvClassDate">
                    <label for="mail">Class Date:</label>
                    <input type="text" style="padding: 6px;" disabled="disabled" id="txtClassDate" data-beatpicker="true" />
                </div>

                <div style="float: left; margin-left: 20px;" id="dvClassTime">
                    <label for="password">Class Time:</label>
                    <select id="selTime" disabled="disabled" style="width: 120px;">
                        <option value="0">Select</option>
                        <option value="08:00">8:00 AM</option>
                        <option value="09:00">9:00 AM</option>
                        <option value="10:00">10:00 AM</option>
                        <option value="11:00">11:00 AM</option>
                        <option value="12:00">12:00 PM</option>
                        <option value="13:00">01:00 PM</option>
                        <option value="14:00">02:00 PM</option>
                        <option value="15:00">03:00 PM</option>
                        <option value="16:00">04:00 PM</option>
                        <option value="17:00">05:00 PM</option>
                        <option value="18:00">06:00 PM</option>
                        <option value="19:00">07:00 PM</option>
                        <option value="20:00">08:00 PM</option>
                        <option value="21:00">09:00 PM</option>
                        <option value="22:00">10:00 PM</option>
                        <option value="23:00">11:00 PM</option>

                    </select>
                </div>



                <div style="float: left; margin-left: 20px; display: none;" id="dvSubstitute">
                    <label for="" id="lblSubtituteCoach">Substitute Coach:</label>
                    <select id="selSubCoach" style="width: 150px;">
                        <option value="82930">Chintan Maheshwari</option>

                    </select>
                </div>

                <div style="float: left; margin-left: 20px;">
                    <label for="password">Status:</label>
                    <select id="selStatus" style="width: 120px;">
                        <option value="On">On</option>
                        <option value="Cancelled">Cancelled</option>
                    </select>
                </div>


            </fieldset>
            <fieldset style="padding: 0; width: 740px;">
                <div style="float: left;">
                    <label for="mail">Home Work Release Date:</label>
                    <input type="text" style="padding: 6px;" id="txtHwRelDate" data-beatpicker="true" data-beatpicker-format="['MM','DD','YYYY'],separator:'/'" />
                </div>
                <div style="float: left; margin-left: 20px;">
                    <label for="mail">Home Work Due Date:</label>
                    <input type="text" style="padding: 6px;" id="txtDueDate" data-beatpicker="true" data-beatpicker-format="['MM','DD','YYYY'],separator:'/'" />
                </div>
                <div style="float: left; margin-left: 20px;">
                    <label for="mail">Answer Release Date:</label>
                    <input type="text" style="padding: 6px;" id="txtAnsDate" data-beatpicker="true" data-beatpicker-format="['MM','DD','YYYY'],separator:'/'" />
                </div>
                <div style="float: left; margin-left: 20px;">
                    <label for="mail">Student Release Date:</label>
                    <input type="text" style="padding: 6px;" id="txtSRelDate" data-beatpicker="true" data-beatpicker-format="['MM','DD','YYYY'],separator:'/'" />
                </div>



            </fieldset>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <center>
                <input type="button" class="btnClass" id="btnSave" value="Save" style="cursor: pointer;" />
                <input type="button" class="btnClass" id="btnCancel" value="Cancel" style="cursor: pointer;" />
                <%--  <button id="btnSave" style="font-size: 16px; cursor: pointer;">Save</button>

                <button id="btnCancel" style="font-size: 16px; cursor: pointer;">Cancel</button>--%>
            </center>
        </center>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <span id="spnGridTitle" style="display: none;">Table 1: Coach Class Calendar</span>
    </center>
    <div style="clear: both; margin-bottom: 1px;"></div>
    <div>
        <table id="table">
            <%--  <thead>
                <tr>
                    <th>Action</th>
                    <th>Date</th>
                    <th>Time</th>
                    <th>Class Stype</th>
                    <th>Week#</th>
                    <th>Status</th>
                    <th>Makeup</th>
                    <th>Substitute</th>
                    <th>Reason</th>
                    <th>HwRelDate</th>
                    <th>HWDueDate</th>
                    <th>SRelDate</th>
                    <th>ARelDate</th>

                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>
                        <button id="Button1" style="padding: 5px; font-size: 16px; cursor: pointer;">Modify</button></td>
                    <td>03/25/2017</td>
                    <td>6:00 PM</td>
                    <td>Regular</td>
                    <td>1</td>
                    <td>On</td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                </tr>

            </tbody>--%>
        </table>

        <div id="loader"></div>

        <div style="display: none;" id="myDiv" class="animate-bottom">
        </div>
        <input type="hidden" value="0" id="hdnCoachClassCalID" />
        <input type="hidden" value="0" id="hdnStatus" />
        <input type="hidden" value="0" id="hdnDay" />
        <input type="hidden" value="0" id="hdnLoginID" runat="server" />
        <input type="hidden" value="0" id="hdnRoleID" runat="server" />
</asp:Content>
