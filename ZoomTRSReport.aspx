﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="ZoomTRSReport.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="WebExTRSReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <style type="text/css">
        .ui-datepicker-trigger {
            padding-left: 5px;
            position: relative;
            top: 3px;
        }

        .ui-datepicker {
            font-size: 8pt !important;
        }

        .style8 {
            width: 99px;
        }

        .style10 {
            width: 147px;
        }

        .style12 {
            width: 84px;
            text-align: left;
        }

        .style16 {
            width: 126px;
        }

        .style17 {
            width: 88px;
        }

        .style20 {
            width: 113px;
            text-align: left;
        }

        .style47 {
            width: 100%;
        }

        .style53 {
            width: 179px;
        }

        .style60 {
            width: 68px;
        }

        .style67 {
            width: 15px;
            font-weight: bold;
        }

        .style70 {
            width: 286px;
        }

        .style87 {
        }

        .style90 {
            width: 184px;
        }

        .style95 {
            width: 93px;
        }

        .style97 {
            width: 117px;
        }

        .style98 {
            width: 37px;
        }

        .style99 {
            width: 120px;
            text-align: left;
        }

        .style101 {
            width: 116px;
        }

        .style102 {
            width: 10px;
        }

        .style118 {
            width: 286px;
            text-align: left;
        }

        .style120 {
            width: 15px;
        }

        .style121 {
            width: 63px;
        }

        .style129 {
            width: 133px;
        }

        .style1 {
            width: 100%;
            height: 92px;
        }

        .style158 {
            width: 95px;
        }

        .style160 {
            width: 271px;
            text-align: left;
        }

        .style162 {
            width: 121px;
        }

        .style163 {
            width: 4px;
        }

        .style165 {
        }

        .style167 {
            text-align: left;
        }

        .style176 {
            width: 199px;
            text-align: left;
        }

        .style177 {
            width: 101px;
            text-align: left;
        }

        .style178 {
            width: 95px;
            text-align: left;
        }

        .style185 {
            width: 152px;
        }

        .style186 {
            width: 101px;
        }

        .style187 {
            width: 181px;
            text-align: left;
        }

        .style191 {
            width: 158px;
        }

        .style195 {
            font-weight: bold;
            width: 158px;
        }

        .style197 {
            width: 54px;
            font-weight: bold;
        }

        .style198 {
            width: 54px;
        }

        .style199 {
            text-align: left;
            width: 182px;
        }

        .style200 {
            width: 120px;
        }

        .style201 {
            width: 179px;
            height: 29px;
        }

        .style202 {
            width: 68px;
            height: 29px;
        }

        .style203 {
            width: 15px;
            height: 29px;
        }

        .style204 {
            width: 10px;
            height: 29px;
        }

        .style205 {
            width: 117px;
            height: 29px;
        }

        .style206 {
            height: 29px;
        }

        .style207 {
            width: 286px;
            height: 29px;
        }
    </style>


    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>
        <script language="javascript" type="text/javascript">
            $(document).on("click", "#ancConfName", function (e) {
                var date = $(this).attr("attr-date");
                var hostID = $(this).attr("attr-hostID");
                var sessionKey = $(this).attr("attr-sessionkey");
                var duration = $(this).attr("attr-duration");
                document.getElementById("<%=hdnDate.ClientID%>").value = date;
                document.getElementById("<%=hdnSessionKey.ClientID%>").value = sessionKey;
                document.getElementById("<%=hdnHostID.ClientID%>").value = hostID;
                document.getElementById("<%=hdnDuration.ClientID%>").value = duration;
                document.getElementById('<%= btnMeetingCancelConfirm.ClientID%>').click();

                //$(this).closest("tr").css("background-color", "blue");

            });

            $(document).on("click", "#btnGenerateReportUsage", function (e) {
                document.getElementById('<%= BtnExportExcelUsage.ClientID%>').click();
            });
            $(document).on("click", "#btnGenerateReportAttendee", function (e) {
                document.getElementById('<%= BtnExportAttendee.ClientID%>').click();
            });

            $(function (e) {
                var pickerOpts = {
                    dateFormat: $.datepicker.regional['fr']
                };
                $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });
                $("#<%=TxtStartDate.ClientID %>").datepicker({ dateFormat: 'yy-mm-dd' });
                $("#<%=TxtEndDate.ClientID %>").datepicker({ dateFormat: 'yy-mm-dd' });

            });

        </script>
        <asp:Button ID="btnMeetingCancelConfirm" Style="display: none;" runat="server" OnClick="btnMeetingCancelConfirm_onClick" />
        <asp:Button ID="BtnExportExcelUsage" Style="display: none;" runat="server" OnClick="BtnExportExcelUsage_onClick" />
        <asp:Button ID="BtnExportAttendee" Style="display: none;" runat="server" OnClick="BtnExportAttendee_onClick" />
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">
            Zoom Training Sessions Report
                     <br />
            <br />
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div align="center">
            <asp:Label ID="lblErrMsg" ForeColor="Red" runat="server"></asp:Label>
        </div>
        <table id="tblCoachReg" runat="server" style="margin-left: auto; margin-right: auto; width: 75%; background-color: #ffffcc;">
            <tr class="ContentSubTitle" align="center">
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Year</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="ddYear" runat="server" Width="75px">
                    </asp:DropDownList>

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Semester</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDLSemester" AutoPostBack="true" runat="server" Width="75px" OnSelectedIndexChanged="DDLSemester_SelectedIndexChanged">
                    </asp:DropDownList>

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Coach</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlCoach" runat="server" Width="75px" AutoPostBack="true" OnSelectedIndexChanged="DDlCoach_SelectedIndexChanged">
                    </asp:DropDownList>
                    <br />
                    <br />
                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Product</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlProduct" runat="server" Width="75px" OnSelectedIndexChanged="DDlProduct_SelectedIndexChanged" AutoPostBack="true">
                    </asp:DropDownList>
                    <br />
                    <br />
                </td>

                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;SessionNo</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:DropDownList ID="DDlSessionNo" runat="server" Width="75px">
                    </asp:DropDownList>

                </td>

                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;From Date</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:TextBox ID="TxtStartDate" runat="server" Width="80px"></asp:TextBox>

                </td>
                <td style="width: 100px" align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;To Date</td>

                <td align="left" nowrap="nowrap" style="width: 41px">
                    <asp:TextBox ID="TxtEndDate" runat="server" Width="80px"></asp:TextBox>

                </td>


            </tr>
            <tr>
                <td colspan="14" align="center">
                    <asp:Button ID="BtnGenerateReport" runat="server" Text="Generate Report" OnClick="BtnGenerateReport_Click" />
                </td>
            </tr>
        </table>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" id="dvUsageReport" runat="server">
            <asp:Literal ID="LtrUsageReport" runat="server"></asp:Literal>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center" id="dvAttendeeReport" runat="server">
            <asp:Literal ID="LtrAttendeeReport" runat="server"></asp:Literal>
        </div>
        <div style="clear: both; margin-bottom: 20px;"></div>
        <div align="center">
            <asp:Button ID="BtnCloseTable2" Visible="false" runat="server" Text="Close Table 2" OnClick="BtnCloseTable2_Click" />
        </div>

        <input type="hidden" id="hdnDate" runat="server" value="0" />
        <input type="hidden" id="hdnUserID" runat="server" value="0" />
        <input type="hidden" id="hdnPwd" runat="server" value="0" />
        <input type="hidden" id="hdnTopic" runat="server" value="0" />
        <input type="hidden" id="hdnIsExcel" runat="server" value="0" />
        <input type="hidden" id="hdnIsAttendeeExcel" runat="server" value="0" />
        <input type="hidden" id="hdnHostID" runat="server" value="0" />
        <input type="hidden" id="hdnSessionKey" runat="server" value="0" />
        <input type="hidden" id="hdnDuration" runat="server" value="0" />
    </div>
</asp:Content>
