Imports System.Data
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL


Namespace VRegistration

Partial Class VolunteerRole
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim strRoles As New StringBuilder
    Dim strRolesSP As New StringBuilder
    Dim intIndTotalSelect As Integer = 0
    Dim intSpouseTotalSelect As Integer = 0


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
            If Not Page.IsPostBack = True Then
                If LCase(Session("LoggedIn")) <> "true" Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                Page.MaintainScrollPositionOnPostBack = True
                If Not Request.QueryString("Id") Is Nothing Then
                    hlinkParentRegistration.NavigateUrl = "Registration.aspx?Id=" & Request.QueryString("Id")
                End If
                Dim conn As New SqlConnection(Application("ConnectionString"))
                Dim cmd As New SqlCommand
                cmd.Connection = conn
                conn.Open()
                cmd.CommandType = CommandType.StoredProcedure
                cmd.CommandText = "usp_GetVolunteerTaskByCategory"

                Dim da As New SqlDataAdapter
                Dim ds As New DataSet
                da.SelectCommand = cmd
                da.Fill(ds)
                If ds.Tables.Count > 3 Then
                    '*** Organizational Support: non-event specific / General:
                    chkOrganizational.DataSource = ds.Tables(1)
                    chkOrganizational.DataTextField = ds.Tables(1).Columns("TaskDescription").ToString
                    chkOrganizational.DataValueField = ds.Tables(1).Columns("VolunteerTaskID").ToString
                    chkOrganizational.DataBind()

                    chkOrganizationalSP.DataSource = ds.Tables(1)
                    chkOrganizationalSP.DataTextField = ds.Tables(1).Columns("TaskDescription").ToString
                    chkOrganizationalSP.DataValueField = ds.Tables(1).Columns("VolunteerTaskID").ToString
                    chkOrganizationalSP.DataBind()

                    '*** Chapters in US/Canada (including Regional Contests):
                    chkUSChapters.DataSource = ds.Tables(2)
                    chkUSChapters.DataTextField = ds.Tables(2).Columns("TaskDescription").ToString
                    chkUSChapters.DataValueField = ds.Tables(2).Columns("VolunteerTaskID").ToString
                    chkUSChapters.DataBind()

                    chkUSChaptersSP.DataSource = ds.Tables(2)
                    chkUSChaptersSP.DataTextField = ds.Tables(2).Columns("TaskDescription").ToString
                    chkUSChaptersSP.DataValueField = ds.Tables(2).Columns("VolunteerTaskID").ToString
                    chkUSChaptersSP.DataBind()

                    '*** National Finals (Only for this event):
                    chkNationalFinals.DataSource = ds.Tables(0)
                    chkNationalFinals.DataTextField = ds.Tables(0).Columns("TaskDescription").ToString
                    chkNationalFinals.DataValueField = ds.Tables(0).Columns("VolunteerTaskID").ToString
                    chkNationalFinals.DataBind()

                    chkNationalFinalsSP.DataSource = ds.Tables(0)
                    chkNationalFinalsSP.DataTextField = ds.Tables(0).Columns("TaskDescription").ToString
                    chkNationalFinalsSP.DataValueField = ds.Tables(0).Columns("VolunteerTaskID").ToString
                    chkNationalFinalsSP.DataBind()

                    '*** Chapters in India (Scholarships in India / Starting Contests in India / Liaison work with India chapters):
                    chkNSFIndia.DataSource = ds.Tables(3)
                    chkNSFIndia.DataTextField = ds.Tables(3).Columns("TaskDescription").ToString
                    chkNSFIndia.DataValueField = ds.Tables(3).Columns("VolunteerTaskID").ToString
                    chkNSFIndia.DataBind()

                    chkNSFIndiaSP.DataSource = ds.Tables(3)
                    chkNSFIndiaSP.DataTextField = ds.Tables(3).Columns("TaskDescription").ToString
                    chkNSFIndiaSP.DataValueField = ds.Tables(3).Columns("VolunteerTaskID").ToString
                    chkNSFIndiaSP.DataBind()
                End If
                conn = Nothing
                If Session("VolunteerInd") = True Then
                    tblIndRole.Visible = True
                Else
                    tblIndRole.Visible = False
                End If
                If Session("VolunteerSP") = True Then
                    tblSpouseRole.Visible = True
                Else
                    tblSpouseRole.Visible = False
                End If

                Dim objIndividual As New IndSpouse10
                Dim connInd As New SqlConnection(Application("ConnectionString"))
                If Request.QueryString("id") Is Nothing Then
                    objIndividual.GetIndSpouseByID(connInd.ConnectionString, Session("CustIndID"))
                Else
                    'objIndividual.GetIndSpouseByID(connInd.ConnectionString, Request.QueryString("ID"))
                    Dim strIndSpouse As String
                    strIndSpouse = "AutoMemberID=" & Request.QueryString("ID")
                    Dim dsIndSpouse As New DataSet
                    objIndividual.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, strIndSpouse)
                    Dim intIndID As Integer
                    If dsIndSpouse.Tables.Count > 0 Then
                        If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                            If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                                End If
                            Else
                                If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                Else
                                    intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                                End If
                            End If
                        End If
                        objIndividual.GetIndSpouseByID(connInd.ConnectionString, intIndID)
                    End If
                End If
                lblIndName.Text = objIndividual.FirstName & " " & objIndividual.LastName
                'rblMailingLabelInd.Items.FindByValue(objIndividual.MailingLabel.ToString).Selected = True
                'rblSendNewsletterInd.Items.FindByValue(objIndividual.NewsLetter.ToString).Selected = True
                'rblSendReceiptInd.Items.FindByValue(objIndividual.SendReceipt.ToString).Selected = True

                If objIndividual.VolunteerRole1.ToString <> "" And objIndividual.VolunteerRole1.ToString <> "0" Then
                    Select Case objIndividual.VolunteerRole1
                        Case 1 To 30
                            Try
                                chkNationalFinals.Items.FindByValue(objIndividual.VolunteerRole1.ToString).Selected = True
                            Catch
                                chkNationalFinals.ClearSelection()
                            End Try
                        Case 31 To 51
                            Try
                                chkOrganizational.Items.FindByValue(objIndividual.VolunteerRole1.ToString).Selected = True
                            Catch
                                chkOrganizational.ClearSelection()
                            End Try
                        Case 52 To 77
                            Try
                                chkUSChapters.Items.FindByValue(objIndividual.VolunteerRole1.ToString).Selected = True
                            Catch
                                chkUSChapters.ClearSelection()
                            End Try
                        Case 78 To 82
                            Try
                                chkNSFIndia.Items.FindByValue(objIndividual.VolunteerRole1.ToString).Selected = True
                            Catch
                                chkNSFIndia.ClearSelection()
                            End Try
                    End Select
                End If
                If objIndividual.VolunteerRole2.ToString <> "" And objIndividual.VolunteerRole2.ToString <> "0" Then
                    Select Case objIndividual.VolunteerRole2
                        Case 1 To 30
                            Try
                                chkNationalFinals.Items.FindByValue(objIndividual.VolunteerRole2.ToString).Selected = True
                            Catch
                                chkNationalFinals.ClearSelection()
                            End Try
                        Case 31 To 51
                            Try
                                chkOrganizational.Items.FindByValue(objIndividual.VolunteerRole2.ToString).Selected = True
                            Catch
                                chkOrganizational.ClearSelection()
                            End Try
                        Case 52 To 77
                            Try
                                chkUSChapters.Items.FindByValue(objIndividual.VolunteerRole2.ToString).Selected = True
                            Catch
                                chkUSChapters.ClearSelection()
                            End Try
                        Case 78 To 82
                            Try
                                chkNSFIndia.Items.FindByValue(objIndividual.VolunteerRole2.ToString).Selected = True
                            Catch
                                chkNSFIndia.ClearSelection()
                            End Try
                    End Select
                End If
                If objIndividual.VolunteerRole3.ToString <> "" And objIndividual.VolunteerRole3.ToString <> "0" Then
                    Select Case objIndividual.VolunteerRole3
                        Case 1 To 30
                            Try
                                chkNationalFinals.Items.FindByValue(objIndividual.VolunteerRole3.ToString).Selected = True
                            Catch
                                chkNationalFinals.ClearSelection()
                            End Try
                        Case 31 To 51
                            Try
                                chkOrganizational.Items.FindByValue(objIndividual.VolunteerRole3.ToString).Selected = True
                            Catch
                                chkOrganizational.ClearSelection()
                            End Try
                        Case 52 To 77
                            Try
                                chkUSChapters.Items.FindByValue(objIndividual.VolunteerRole3.ToString).Selected = True
                            Catch
                                chkUSChapters.ClearSelection()
                            End Try
                        Case 78 To 82
                            Try
                                chkNSFIndia.Items.FindByValue(objIndividual.VolunteerRole3.ToString).Selected = True
                            Catch
                                chkNSFIndia.ClearSelection()
                            End Try
                    End Select
                End If
                connInd = Nothing
                Dim objSpouse As New IndSpouse10
                Dim dsSpouse As New DataSet
                Dim strSpouse As String
                Dim intSpouseID As Integer

                strSpouse = "Relationship='" & Session("CustIndID") & "'"
                objSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, strSpouse)

                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                        Session("CustSpouseID") = intSpouseID
                    End If
                End If
                If Not Session("CustSpouseID") Is Nothing Then
                    If Session("CustSpouseID") > 0 Then

                        Dim connSP As New SqlConnection(Application("ConnectionString"))
                        objSpouse.GetIndSpouseByID(connSP.ConnectionString, intSpouseID)
                        lblSPName.Text = objSpouse.FirstName & " " & objSpouse.LastName

                        If objSpouse.VolunteerRole1.ToString <> "" Then
                            Select Case objSpouse.VolunteerRole1
                                Case 1 To 30
                                    Try
                                        chkNationalFinalsSP.Items.FindByValue(objSpouse.VolunteerRole1.ToString).Selected = True
                                    Catch
                                        chkNationalFinalsSP.ClearSelection()
                                    End Try
                                Case 31 To 51
                                    Try
                                        chkOrganizationalSP.Items.FindByValue(objSpouse.VolunteerRole1.ToString).Selected = True
                                    Catch
                                        chkOrganizationalSP.ClearSelection()
                                    End Try
                                Case 52 To 77
                                    Try
                                        chkUSChaptersSP.Items.FindByValue(objSpouse.VolunteerRole1.ToString).Selected = True
                                    Catch
                                        chkUSChaptersSP.ClearSelection()
                                    End Try
                                Case 78 To 82
                                    Try
                                        chkNSFIndiaSP.Items.FindByValue(objSpouse.VolunteerRole1.ToString).Selected = True
                                    Catch
                                        chkNSFIndiaSP.ClearSelection()
                                    End Try
                            End Select
                        End If
                        If objSpouse.VolunteerRole2.ToString <> "" Then
                            Select Case objSpouse.VolunteerRole2
                                Case 1 To 30
                                    Try
                                        chkNationalFinalsSP.Items.FindByValue(objSpouse.VolunteerRole2.ToString).Selected = True
                                    Catch
                                        chkNationalFinalsSP.ClearSelection()
                                    End Try
                                Case 31 To 51
                                    Try
                                        chkOrganizationalSP.Items.FindByValue(objSpouse.VolunteerRole2.ToString).Selected = True
                                    Catch
                                        chkOrganizationalSP.ClearSelection()
                                    End Try
                                Case 52 To 77
                                    Try
                                        chkUSChaptersSP.Items.FindByValue(objSpouse.VolunteerRole2.ToString).Selected = True
                                    Catch
                                        chkUSChaptersSP.ClearSelection()
                                    End Try
                                Case 78 To 82
                                    Try
                                        chkNSFIndiaSP.Items.FindByValue(objSpouse.VolunteerRole2.ToString).Selected = True
                                    Catch
                                        chkNSFIndiaSP.ClearSelection()
                                    End Try
                            End Select
                        End If
                        If objSpouse.VolunteerRole3.ToString <> "" Then
                            Select Case objSpouse.VolunteerRole3
                                Case 1 To 30
                                    Try
                                        chkNationalFinalsSP.Items.FindByValue(objSpouse.VolunteerRole3.ToString).Selected = True
                                    Catch
                                        chkNationalFinalsSP.ClearSelection()
                                    End Try
                                    chkNationalFinalsSP.Items.FindByValue(objSpouse.VolunteerRole3.ToString).Selected = True
                                Case 31 To 51
                                    Try
                                        chkOrganizationalSP.Items.FindByValue(objSpouse.VolunteerRole3.ToString).Selected = True
                                    Catch
                                        chkOrganizationalSP.ClearSelection()
                                    End Try
                                Case 52 To 77
                                    Try
                                        chkUSChaptersSP.Items.FindByValue(objSpouse.VolunteerRole3.ToString).Selected = True
                                    Catch
                                        chkUSChaptersSP.ClearSelection()
                                    End Try
                                Case 78 To 82
                                    Try
                                        chkNSFIndiaSP.Items.FindByValue(objSpouse.VolunteerRole3.ToString).Selected = True
                                    Catch
                                        chkNSFIndiaSP.ClearSelection()
                                    End Try
                            End Select
                        End If
                        connSP = Nothing
                    End If
                End If
            End If

            'due to firfox issues his hyperlink was disenabled 4/11/2008
            hlinkParentRegistration.Enabled = False
            hlinkParentRegistration.Visible = False
        End Sub

    Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
        Dim i As Integer
        If Session("VolunteerInd") = True Then
            '***Validate Individual Role Info
            For i = 0 To chkOrganizational.Items.Count - 1
                If chkOrganizational.Items(i).Selected = True Then
                    strRoles.Append(chkOrganizational.Items(i).Value & "~")
                    intIndTotalSelect += 1
                End If
            Next
            For i = 0 To chkUSChapters.Items.Count - 1
                If chkUSChapters.Items(i).Selected = True Then
                    strRoles.Append(chkUSChapters.Items(i).Value & "~")
                    intIndTotalSelect += 1
                End If
            Next
            For i = 0 To chkNationalFinals.Items.Count - 1
                If chkNationalFinals.Items(i).Selected = True Then
                    strRoles.Append(chkNationalFinals.Items(i).Value & "~")
                    intIndTotalSelect += 1
                End If
            Next
            For i = 0 To chkNSFIndia.Items.Count - 1
                If chkNSFIndia.Items(i).Selected = True Then
                    strRoles.Append(chkNSFIndia.Items(i).Value & "~")
                    intIndTotalSelect += 1
                End If
            Next
            If intIndTotalSelect = 0 Then
                lblError.Text = "You have not selected any Roles for Individual. You may  select upto 3 Roles  of interest."
                lblError.Visible = True
                Exit Sub
            End If
            If intIndTotalSelect > 3 Then
                lblError.Text = "You have selected more than three roles for Indivdual. Please select upto 3 Roles  of interest."
                lblError.Visible = True
                Exit Sub
            End If

        End If

        '***Validate Spouse Role Info
        If Session("VolunteerSP") = True Then
            For i = 0 To chkOrganizationalSP.Items.Count - 1
                If chkOrganizationalSP.Items(i).Selected = True Then
                    strRolesSP.Append(chkOrganizationalSP.Items(i).Value & "~")
                    intSpouseTotalSelect += 1
                End If
            Next
            For i = 0 To chkUSChaptersSP.Items.Count - 1
                If chkUSChaptersSP.Items(i).Selected = True Then
                    strRolesSP.Append(chkUSChaptersSP.Items(i).Value & "~")
                    intSpouseTotalSelect += 1
                End If
            Next
            For i = 0 To chkNationalFinalsSP.Items.Count - 1
                If chkNationalFinalsSP.Items(i).Selected = True Then
                    strRolesSP.Append(chkNationalFinalsSP.Items(i).Value & "~")
                    intSpouseTotalSelect += 1
                End If
            Next
            For i = 0 To chkNSFIndiaSP.Items.Count - 1
                If chkNSFIndiaSP.Items(i).Selected = True Then
                    strRolesSP.Append(chkNSFIndiaSP.Items(i).Value & "~")
                    intSpouseTotalSelect += 1
                End If
            Next
            If intSpouseTotalSelect = 0 Then
                lblError.Text = "You have not selected any Roles for Spouse. You may  select upto 3 Roles  of interest."
                lblError.Visible = True
                Exit Sub
            End If
            If intSpouseTotalSelect > 3 Then
                lblError.Text = "You have selected more than three roles for Spouse. Please select upto 3 Roles  of interest."
                lblError.Visible = True
                Exit Sub
            End If
        End If

        Dim arrRoles() As String
        Dim prmArray(4) As SqlParameter
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand

        arrRoles = Split(strRoles.ToString, "~", 4, CompareMethod.Text)

        cmd.Connection = conn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_UpdateVolunteerRoles"
        conn.Open()
            '*** Testing Purposes
            If Request.QueryString("ID") Is Nothing Then
                cmd.Parameters.AddWithValue("@AutoMemberID", DbType.Int32).Value = Session("CustIndID")
            Else
                cmd.Parameters.AddWithValue("@AutoMemberID", DbType.Int32).Value = Request.QueryString("ID")
            End If
            'cmd.Parameters.Add("@AutoMemberID", DbType.Int32).Value = Session("CustIndID")
            cmd.Parameters.AddWithValue("@VolunteerRole1", DbType.Int32).Value = arrRoles(0)

            If intIndTotalSelect > 2 Then
                cmd.Parameters.AddWithValue("@VolunteerRole3", DbType.Int32).Value = arrRoles(2)
            Else
                cmd.Parameters.AddWithValue("@VolunteerRole3", DbType.Int32).Value = DBNull.Value
            End If

            If intIndTotalSelect > 1 Then
                cmd.Parameters.AddWithValue("@VolunteerRole2", DbType.Int32).Value = arrRoles(1)
            Else
                cmd.Parameters.AddWithValue("@VolunteerRole2", DbType.Int32).Value = DBNull.Value
            End If

            cmd.Parameters.AddWithValue("@MailingLabel", DbType.String).Value = DBNull.Value
            cmd.Parameters.AddWithValue("@NewsLetter", DbType.String).Value = DBNull.Value
            cmd.Parameters.AddWithValue("@SendReceipt", DbType.String).Value = DBNull.Value

            cmd.ExecuteScalar()

            conn.Close()

            cmd = Nothing
            conn = Nothing

            If Session("VolunteerSP") = True Then
                UpdateSpouseInfo()
            End If

            Dim Str As String = ""
            Str = "MemberId='" & Session("CustIndID") & "'"
            If Not Session("CustSpouseID") = "0" Then
                Str = Str & " or SpouseID='" & Session("CustSpouseID") & "'"
            End If

            Dim objChild As New Child
            Dim dsChild As New DataSet

            objChild.SearchChildWhere(Application("ConnectionString"), dsChild, Str)

            If dsChild.Tables.Count > 0 Then
                If dsChild.Tables(0).Rows.Count > 0 Then
                    If Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                        If Session("NavPath") = "Search" Then
                            Response.Redirect("dbsearchResults.aspx")
                        ElseIf Session("NavPath") = "Sponsor" Then
                            Response.Redirect("Search_Sponsor.aspx")
                        Else
                            Response.Redirect("VolunteerFunctions.aspx")
                        End If
                    ElseIf Session("entryToken").ToString.ToUpper = "DONOR" Then
                        Response.Redirect("DonorFunctions.aspx")
                    ElseIf Session("entryToken").ToString.ToUpper = "PARENT" Then
                        Response.Redirect("MainChild.aspx")
                    End If
                ElseIf Session("entryToken").ToString.ToUpper = "DONOR" Then
                    Response.Redirect("DonorFunctions.aspx")
                ElseIf Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                    If Session("NavPath") = "Search" Then
                        Response.Redirect("dbsearchResults.aspx")
                    ElseIf Session("NavPath") = "Sponsor" Then
                        Response.Redirect("Search_Sponsor.aspx")
                    Else
                        btnSubmit.Enabled = False
                        hlinkParentRegistration.Enabled = False
                        lblError.Visible = True
                        lblError.Text = "Your profile was saved successfully.  Please click on Home to log out."
                    End If
                Else
                    Response.Redirect("MainChild.aspx")
                End If
            End If
        End Sub
    Private Sub UpdateSpouseInfo()
        Dim arrRolesSP() As String
        Dim prmArray(4) As SqlParameter
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand

        arrRolesSP = Split(strRolesSP.ToString, "~", 4, CompareMethod.Text)

        cmd.Connection = conn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_UpdateVolunteerRoles"
        conn.Open()
            '*** Testing Purposes
            
            cmd.Parameters.AddWithValue("@AutoMemberID", DbType.Int32).Value = Session("CustSpouseID")
            cmd.Parameters.AddWithValue("@VolunteerRole1", DbType.Int32).Value = arrRolesSP(0)

        If intSpouseTotalSelect > 2 Then
                cmd.Parameters.AddWithValue("@VolunteerRole3", DbType.Int32).Value = arrRolesSP(2)
        Else
                cmd.Parameters.AddWithValue("@VolunteerRole3", DbType.Int32).Value = DBNull.Value
        End If
            If intSpouseTotalSelect > 1 Then
                cmd.Parameters.AddWithValue("@VolunteerRole2", DbType.Int32).Value = arrRolesSP(1)
            Else
                cmd.Parameters.AddWithValue("@VolunteerRole2", DbType.Int32).Value = DBNull.Value
            End If

            cmd.Parameters.AddWithValue("@MailingLabel", DbType.String).Value = DBNull.Value
            cmd.Parameters.AddWithValue("@NewsLetter", DbType.String).Value = DBNull.Value
            cmd.Parameters.AddWithValue("@SendReceipt", DbType.String).Value = DBNull.Value

        cmd.ExecuteScalar()

        conn.Close()

        cmd = Nothing
        conn = Nothing
    End Sub
End Class

End Namespace

