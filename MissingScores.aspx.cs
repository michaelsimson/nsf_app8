﻿using Microsoft.ApplicationBlocks.Data;
using NativeExcel;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MissingScores : System.Web.UI.Page
{

    #region <<Variable Declaration>>
    string strConnection = "connectionstring";
    #endregion
    #region <<Pre-defined Functions>>
    protected void Page_Load(object sender, EventArgs e)
    {
        //Session["LoginID"] = "4240";
        //Session["RoleId"] = 1;

        try
        {
            if (!IsPostBack)
            {

                if (Session["LoginID"] == null)
                {
                    Response.Redirect("~/Maintest.aspx");
                }
                int MaxYear = DateTime.Now.Year;
                for (int i = MaxYear; i >= (DateTime.Now.Year - 1); i--)
                {
                    ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
                }
                ddlEvent.SelectedIndex = 2;
                missRank.Checked = true;
                FillDropdownBasedOnRole();
            }



        }
        catch (Exception ex)
        {
            //Response.Write("Err :" + ex.ToString());
        }
    }

    private void FillDropdownBasedOnRole()
    {
        if (Convert.ToInt32(Session["RoleId"].ToString()) == 1 || Convert.ToInt32(Session["RoleId"].ToString()) == 2)
        {
            fillZone();
            fillCluster();
            fillChapter();
            ddlZone.Enabled = true;
            ddlCluster.Enabled = true;
            ddlChapter.Enabled = true;

        }
        else if (Convert.ToInt32(Session["RoleId"].ToString()) == 3)
        {

            //int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select zoneid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and zoneid is not null"));
            int zid = Convert.ToInt32(Session["selZoneID"].ToString());
            string ddlChapterqry;
            try
            {
                ddlChapterqry = string.Format("select * from Chapter where zoneid={0} and clusterid in (select clusterid from zone where zoneid={1})  and state not in ('US') order by State, ChapterCode", zid, zid);
                DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);
                ddlChapter.DataSource = dsstate;
                ddlChapter.DataTextField = "ChapterCode";
                ddlChapter.DataValueField = "ChapterId";
                ddlChapter.DataBind();
                ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
                ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));

                if (dsstate.Tables[0].Rows.Count == 1)
                {
                    ddlChapter.SelectedIndex = 2;
                    ddlChapter.Enabled = false;
                }

                string ddlClusterqry;


                ddlClusterqry = string.Format("select * from Cluster where zoneid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", zid);

                DataSet dsstate1 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);

                ddlCluster.DataSource = dsstate1;
                ddlCluster.DataTextField = "ClusterCode";
                ddlCluster.DataValueField = "ClusterId";
                ddlCluster.DataBind();
                ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
                ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));
                if (dsstate1.Tables[0].Rows.Count == 1)
                {
                    ddlCluster.SelectedIndex = 2;
                    ddlCluster.Enabled = false;
                }

                string ddlZoneqry;


                ddlZoneqry = string.Format("select * from Zone where zoneid={0} and zonecode not in ('Admin','Coaching','Game','Home','InactiveZ') order by Name", zid);

                DataSet dsstate2 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlZoneqry);

                ddlZone.DataSource = dsstate2;
                ddlZone.DataTextField = "Name";
                ddlZone.DataValueField = "ZoneId";
                ddlZone.DataBind();
                ddlZone.Items.Insert(0, new ListItem("ALL", "0"));
                ddlZone.Items.Insert(0, new ListItem("Select Zone", "-1"));
                ddlZone.SelectedIndex = 2;


            }
            catch (Exception ex)
            { }
            ddlZone.Enabled = false;

        }
        else if (Convert.ToInt32(Session["RoleId"].ToString()) == 5)
        {

            // int cid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select top 1 chapterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and chapterid is not null"));
            int cid = Convert.ToInt32(Session["selChapterID"].ToString());
            int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
            int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select clusterid from chapter where chapterid=" + cid + ""));
            string ddlZoneqry;
            try
            {

                ddlZoneqry = string.Format("select * from Zone where zoneid={0} and zonecode not in ('Admin','Coaching','Game','Home','InactiveZ') order by Name", zid);

                DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlZoneqry);

                ddlZone.DataSource = dsstate;
                ddlZone.DataTextField = "Name";
                ddlZone.DataValueField = "ZoneId";
                ddlZone.DataBind();
                ddlZone.Items.Insert(0, new ListItem("ALL", "0"));
                ddlZone.Items.Insert(0, new ListItem("Select Zone", "-1"));
                ddlZone.SelectedIndex = 2;



                string ddlClusterqry;


                ddlClusterqry = string.Format("select * from Cluster where clusterid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", clid);

                DataSet dsstate1 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);

                ddlCluster.DataSource = dsstate1;
                ddlCluster.DataTextField = "ClusterCode";
                ddlCluster.DataValueField = "ClusterId";
                ddlCluster.DataBind();
                ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
                ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));
                ddlCluster.SelectedIndex = 2;

                string ddlChapterqry;
                ddlChapterqry = string.Format("select * from Chapter where chapterid={0}  and state not in ('US') order by State, ChapterCode", cid);

                DataSet dsstate2 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);

                ddlChapter.DataSource = dsstate2;
                ddlChapter.DataTextField = "ChapterCode";
                ddlChapter.DataValueField = "ChapterId";
                ddlChapter.DataBind();
                ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
                ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));
                if (dsstate2.Tables[0].Rows.Count == 1)
                {
                    ddlChapter.SelectedIndex = 2;
                    ddlChapter.Enabled = false;
                }
            }
            catch (Exception ex1)
            { }
            ddlZone.Enabled = false;
            ddlCluster.Enabled = false;
        }
        else if (Convert.ToInt32(Session["RoleId"].ToString()) == 4)
        {

            //int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select Clusterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and Clusterid is not null"));
            int clid = Convert.ToInt32(Session["selClusterID"].ToString());
            int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from cluster where clusterid=" + clid + ""));
            string ddlClusterqry;


            ddlClusterqry = string.Format("select * from Cluster where clusterid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", clid);

            DataSet dsstate1 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);

            ddlCluster.DataSource = dsstate1;
            ddlCluster.DataTextField = "ClusterCode";
            ddlCluster.DataValueField = "ClusterId";
            ddlCluster.DataBind();
            ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
            ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));
            ddlCluster.SelectedIndex = 2;
            string ddlZoneqry;
            ddlZoneqry = string.Format("select * from Zone where zoneid={0} and zonecode not in ('Admin','Coaching','Game','Home','InactiveZ') order by Name", zid);

            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlZoneqry);

            ddlZone.DataSource = dsstate;
            ddlZone.DataTextField = "Name";
            ddlZone.DataValueField = "ZoneId";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("ALL", "0"));
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "-1"));
            ddlZone.SelectedIndex = 2;
            //
            string ddlChapterqry;
            ddlChapterqry = string.Format("select * from chapter where zoneid={0} and clusterid={1} and state not in ('US') order by State, ChapterCode", zid, clid);

            DataSet dsstate2 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);

            ddlChapter.DataSource = dsstate2;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterId";
            ddlChapter.DataBind();
            ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
            ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));
            if (dsstate2.Tables[0].Rows.Count == 1)
            {
                ddlChapter.SelectedIndex = 2;
                ddlChapter.Enabled = false;
            }

            ddlZone.Enabled = false;
            ddlCluster.Enabled = false;

        }
        else if (Convert.ToInt32(Session["RoleId"].ToString()) == 9)
        {
            int cid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select chapterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and chapterid is not null"));
            int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select Clusterid from chapter where chapterid=" + cid + ""));
            int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
            string ddlClusterqry;


            ddlClusterqry = string.Format("select * from Cluster where clusterid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", clid);

            DataSet dsstate1 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);

            ddlCluster.DataSource = dsstate1;
            ddlCluster.DataTextField = "ClusterCode";
            ddlCluster.DataValueField = "ClusterId";
            ddlCluster.DataBind();
            ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
            ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));
            ddlCluster.SelectedIndex = 2;
            string ddlZoneqry;
            ddlZoneqry = string.Format("select * from Zone where zoneid={0} and zonecode not in ('Admin','Coaching','Game','Home','InactiveZ') order by Name", zid);

            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlZoneqry);

            ddlZone.DataSource = dsstate;
            ddlZone.DataTextField = "Name";
            ddlZone.DataValueField = "ZoneId";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("ALL", "0"));
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "-1"));
            ddlZone.SelectedIndex = 2;
            //
            string ddlChapterqry;
            ddlChapterqry = string.Format("select * from chapter where chapterid={0}  and state not in ('US') order by State, ChapterCode", cid);

            DataSet dsstate2 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);

            ddlChapter.DataSource = dsstate2;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterId";
            ddlChapter.DataBind();
            ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
            ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));
            ddlChapter.SelectedIndex = 2;

            ddlZone.Enabled = false;
            ddlCluster.Enabled = false;
            ddlChapter.Enabled = false;
        }
    }
    private void FillClusterByZone()
    {
        int zid = Convert.ToInt32(ddlZone.SelectedValue);
        try
        {
            ddlChapter.Enabled = true;
            string ddlClusterqry;
            if (zid == 0)
            {
                if (Convert.ToInt32(Session["RoleId"].ToString()) == 3)
                {
                    zid = Convert.ToInt32(Session["selZoneID"].ToString());
                    ddlClusterqry = string.Format("select * from Cluster where zoneid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", zid);
                }
                else
                    ddlClusterqry = string.Format("select * from Cluster where Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode");
            }
            else
                ddlClusterqry = string.Format("select * from Cluster where zoneid={0} and Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode", zid);
            DataSet dsstate1 = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);
            ddlCluster.DataSource = dsstate1;
            ddlCluster.DataTextField = "ClusterCode";
            ddlCluster.DataValueField = "ClusterId";
            ddlCluster.DataBind();
            ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
            ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));
            ddlCluster.SelectedIndex = 0;
            if (dsstate1.Tables[0].Rows.Count == 1)
            {
                ddlCluster.SelectedIndex = 2;
                ddlCluster.Enabled = false;
            }
            if (zid == 0)
                ddlCluster.SelectedIndex = 1;

            FillChapterByCluster();
        }
        catch (Exception ex)
        { }
    }
    private void FillChapterByCluster()
    {
        int zid = Convert.ToInt32(ddlZone.SelectedValue);
        int cid = Convert.ToInt32(ddlCluster.SelectedValue);
        try
        {
            string ddlChapterqry;
            if (cid <= 0)
            {
                if (zid <= 0)
                {
                    ddlChapterqry = string.Format("select * from Chapter where state not in ('US') order by State, ChapterCode");
                }
                else
                    ddlChapterqry = string.Format("select * from Chapter where zoneid={0} and clusterid in (select clusterid from zone where zoneid={0})  and state not in ('US') order by State, ChapterCode", zid);
            }
            else
                ddlChapterqry = string.Format("select * from Chapter where zoneid={0} and clusterid ={1}  and state not in ('US') order by State, ChapterCode", zid, cid);
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);
            ddlChapter.DataSource = dsstate;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterId";
            ddlChapter.DataBind();
            ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
            ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));
        }
        catch (Exception ex)
        { }
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        btnExport.Visible = false;
        if (ddlEvent.SelectedValue.Equals("2"))
        {
            if (ddlChapter.SelectedIndex != 0)
                goto GOTORef;
            if (ddlCluster.SelectedIndex != 0)
                goto GOTORef;

            if (ddlZone.SelectedIndex == 0)
            {
                lblErr.Text = "Please select Zone";
                clear(); return;
            }
            else if (ddlCluster.SelectedIndex == 0)
            {

                lblErr.Text = "Please select Cluster";
                clear(); return;
            }
            else if (ddlChapter.SelectedIndex == 0)
            {
                lblErr.Text = "Please select Chapter";
                clear(); return;
            }
            GOTORef:
            lblErr.Text = string.Empty;
            btnExport.Visible = true;
            fillGridView();
            fillGridView1();
        }
        else
        {
            btnExport.Visible = true;
            lblErr.Text = string.Empty;
            fillGridView();
            fillGridView1();
        }

    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 1)
        {
            ddlZone.Enabled = false;
            ddlCluster.Enabled = false;
            ddlChapter.Enabled = false;
        }
        else
        {
            ddlZone.Enabled = true;
            ddlCluster.Enabled = true;
            ddlChapter.Enabled = true;
        }
    }
    protected void ddlZone_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlCluster.Enabled = true;
        ddlChapter.Enabled = true;
        if (ddlZone.SelectedItem.Text.Contains("ALL"))
        {
            // FillDropdownBasedOnRole ();
            ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
            ddlCluster.Items.Insert(0, new ListItem("Select Chapter", "-1"));
            ddlCluster.SelectedIndex = 1;
            ddlCluster.Enabled = false;
            FillChapterByCluster(); ddlChapter.SelectedIndex = 0;
            // ddlChapter.Enabled = false;
        }
        else if (ddlZone.SelectedValue == "-1")
        {
            FillDropdownBasedOnRole();
            ddlZone.SelectedIndex = 0;
            ddlCluster.SelectedIndex = 0;
            ddlChapter.SelectedIndex = 0;
        }
        else
            FillClusterByZone();
    }
    protected void ddlCluster_SelectedIndexChanged(object sender, EventArgs e)
    {
        //if (ddlCluster.SelectedItem.Text.Contains("ALL"))
        //{
        //    ddlChapter.SelectedIndex = 1;
        //    ddlChapter.Enabled = false;
        //}
        //else
        //{
        //    ddlChapter.SelectedIndex = 0;
        //    ddlChapter.Enabled = true;
        //}
        FillChapterByCluster();
    }
    #endregion
    #region <<User-defined Functions>>
    private void fillChapter()
    {
        string ddlChapterqry;
        try
        {
            ddlChapterqry = "select * from Chapter where state not in ('US') order by State, ChapterCode";// exclude US

            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlChapterqry);
            ddlChapter.DataSource = dsstate;
            ddlChapter.DataTextField = "ChapterCode";
            ddlChapter.DataValueField = "ChapterId";
            ddlChapter.DataBind();
            ddlChapter.Items.Insert(0, new ListItem("ALL", "0"));
            ddlChapter.Items.Insert(0, new ListItem("Select Chapter", "-1"));
            ListItem selectedListItem = ddlChapter.Items.FindByValue(Session["selChapterID"].ToString());
            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
        }
        catch (Exception e)
        { }
    }

    private void fillCluster()
    {
        string ddlClusterqry;
        try
        {
            ddlClusterqry = "select * from Cluster where Clustercode not in ('Admin','Coaching','Game','Home','InactiveC') Order By ClusterCode";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlClusterqry);
            ddlCluster.DataSource = dsstate;
            ddlCluster.DataTextField = "ClusterCode";
            ddlCluster.DataValueField = "ClusterId";
            ddlCluster.DataBind();
            ddlCluster.Items.Insert(0, new ListItem("ALL", "0"));
            ddlCluster.Items.Insert(0, new ListItem("Select Cluster", "-1"));

            ListItem selectedListItem = ddlCluster.Items.FindByValue(Session["selClusterID"].ToString());

            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
        }
        catch (Exception e)
        { }
    }

    private void fillZone()
    {
        string ddlZoneqry;
        try
        {
            ddlZoneqry = "select * from Zone where zonecode not in ('Admin','Coaching','Game','Home','InactiveZ') order by Name "; //Except Administration(16), Online Coaching(18), Game(19)
            DataSet dsstate = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, ddlZoneqry);
            ddlZone.DataSource = dsstate;
            ddlZone.DataTextField = "Name";
            ddlZone.DataValueField = "ZoneId";
            ddlZone.DataBind();
            ddlZone.Items.Insert(0, new ListItem("ALL", "0"));
            ddlZone.Items.Insert(0, new ListItem("Select Zone", "-1"));
            ListItem selectedListItem = ddlZone.Items.FindByValue(Session["selZoneID"].ToString());

            if (selectedListItem != null)
            {
                selectedListItem.Selected = true;
            }
        }
        catch (Exception e)
        { }
    }
    public int getZoneId()
    {
        int zid = 0, cid, clid;
        //////
        //When All is for zone, cluster, chapter then we have to pass zone id based on roleid's
        try
        {
            if (Convert.ToInt32(Session["RoleId"].ToString()) == 3)
            {
                if (ddlZone.SelectedIndex == 0)
                {
                    zid = Convert.ToInt32(Session["selZoneID"].ToString());
                }
            }
            else if (Convert.ToInt32(Session["RoleId"].ToString()) == 5)
            {
                cid = Convert.ToInt32(Session["selChapterID"].ToString());
                zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
                clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select clusterid from chapter where chapterid=" + cid + ""));
            }
            else if (Convert.ToInt32(Session["RoleId"].ToString()) == 4)
            {
                clid = Convert.ToInt32(Session["selClusterID"].ToString());
                zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from cluster where clusterid=" + clid + ""));
            }
            else if (Convert.ToInt32(Session["RoleId"].ToString()) == 9)
            {

                cid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select chapterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and chapterid is not null"));
                clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select Clusterid from chapter where chapterid=" + cid + ""));
                zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
            }
        }
        catch (Exception e)
        {
            return 0;
        }
        return zid;
    }
    protected void fillGridView()
    {
        lblTable1.Text = "Table-1:  SB, VB, MB, SC, GB, BB";
        string strquery = "";
        strquery = "DECLARE @TempScore TABLE( Year Char( 30 ), Event integer ,Chapter varchar(50), ProductGroup varchar(30), Product varchar(30), ChapterId integer, State varchar(100));";
        strquery = strquery + " insert into @TempScore ";
        strquery = strquery + "select c.contest_year as Year,c.EventID as Event,b.chaptercode as Chapter,c.productGroupCode as ProductGroup, c.productcode as Product, c.NSFChapterId as ChapterId, b.State as State from contest c Inner Join chapter b on b.chapterid = c.nsfchapterid where contest_year=" + ddlYear.SelectedValue + " and eventid=" + ddlEvent.SelectedValue + "";
        if (ddlZone.SelectedIndex != 1 && ddlZone.SelectedIndex != 0)
        {
            strquery = strquery + " and b.zoneid=" + ddlZone.SelectedValue + "";
        }
        if (ddlCluster.SelectedIndex != 1 && ddlCluster.SelectedIndex != 0)
        {
            strquery = strquery + " and b.clusterid=" + ddlCluster.SelectedValue + "";
        }
        if (ddlChapter.SelectedIndex != 1 && ddlChapter.SelectedIndex != 0)
        {
            strquery = strquery + " and b.Chapterid=" + ddlChapter.SelectedValue + "";
        }
        ////////
        ////When All is for zone, cluster, chapter then we have to pass zone id based on roleid's
        //try
        //{
        //    if (Convert.ToInt32(Session["RoleId"].ToString()) == 3)
        //    {
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            int zid = Convert.ToInt32(Session["selZoneID"].ToString());
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 5)
        //    {
        //        int cid = Convert.ToInt32(Session["selChapterID"].ToString());
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
        //        int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select clusterid from chapter where chapterid=" + cid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //        if (ddlChapter.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.ChapterId=" + cid + "";
        //        }

        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 4)
        //    {
        //        int clid = Convert.ToInt32(Session["selClusterID"].ToString());
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from cluster where clusterid=" + clid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 9)
        //    {

        //        int cid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select chapterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and chapterid is not null"));
        //        int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select Clusterid from chapter where chapterid=" + cid + ""));
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //        if (ddlChapter.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.ChapterId=" + cid + "";
        //        }
        //    }
        //}
        //catch (Exception e)
        //{
        // //   Response.Write("Error :" + e.ToString());
        //}
        ////////
        strquery = strquery + " and contestdate in ((select convert(varchar(10),SatDay1,101) from weekcalendar where Year(SatDay1)=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + ")union(select convert(varchar(10),SunDay2,101) from weekcalendar where Year(SatDay1)=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + ")) ";
        if (missRank.Checked == true)
        {
            strquery = strquery + " and not exists (select a.rank from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and (a.rank is not null and a.rank<>0)) ";
        }
        else if (missScore.Checked == true)
        {
            strquery = strquery + " and not exists (select a.score1,a.score2,a.score3 from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and a.score1 is not null and a.score2 is not null  ";
            if (ddlEvent.SelectedValue.Equals("1"))
            {
                strquery = strquery + " and a.score3 is not null";
            }
            strquery = strquery + " )";
        }
        // strquery = strquery + " and not exists (select count(*) from scoredetail where chapterid = c.nsfchapterid and  productid = c.productid and contestyear=" + ddlYear.SelectedValue + " and  attendanceflag='N' and attendanceflag is not null and attendanceflag <>'' HAVING count(*) > 0)";
        //Changed by Sims since absentees list no need for missing score
        strquery = strquery + "  and not  exists ( select count(*) from scoredetail s where s.chapterid = c.nsfchapterid and s.productid = c.productid and s.contestyear=" + ddlYear.SelectedValue + " and attendanceflag ='N' having count(*) <> ";
        strquery = strquery + " ( select count(*) from scoredetail  s2 where s2.chapterid=c.nsfchapterid and s2.productid =c.productid and s2.contestyear=" + ddlYear.SelectedValue + " )) ";

        strquery = strquery + " and exists (select count(*) from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and ";
        strquery = strquery + " paymentreference is not null and productgroupcode in ('SB', 'VB', 'MB', 'SC', 'GB','BB') HAVING Count(*)>0) order by b.state, b.chaptercode, c.productid; ";

        strquery = strquery + " SELECT DISTINCT    t.Year,t.Event,ChapterId, State,t.Chapter,t.ProductGroup, STUFF((SELECT ', ', i.product as [text()]";
        strquery = strquery + "  FROM @TempScore i WHERE i.Chapter = t.Chapter and i.ProductGroup=t.ProductGroup  FOR XML PATH (''))";
        strquery = strquery + " , 1, 2, '') as Product FROM   @TempScore t";

        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, strquery);
        ViewState["Table1"] = null;
        if (dsJVB.Tables[0].Rows.Count > 0)
        {
            ViewState["Table1"] = dsJVB.Tables[0];
            lblErr.Text = string.Empty;
            gvMissingScore.DataSource = dsJVB.Tables[0];
            gvMissingScore.DataBind();
            gvMissingScore.Visible = true;
            ArrayList arrList = new ArrayList();
            string ChapterId = string.Empty;
            for (int i = 0; i < gvMissingScore.Rows.Count; i++)
            {
                string prdCode = gvMissingScore.Rows[i].Cells[6].Text;
                ChapterId = gvMissingScore.Rows[i].Cells[2].Text;
                arrList.Add(prdCode);
            }
            for (int i = 0; i < arrList.Count; i++)
            {
                ChapterId = gvMissingScore.Rows[i].Cells[2].Text;
                string productCode = arrList[i].ToString();
                string strProductCode = string.Empty;
                string altPrdCode = string.Empty;
                for (int j = 0; j < productCode.Split(',').Length; j++)
                {
                    strProductCode += "'" + productCode.Split(',')[j].ToString().Trim() + "'" + ",";
                    altPrdCode += productCode.Split(',')[j].ToString().Trim() + "(0)" + ",";
                }

                strProductCode = strProductCode.TrimEnd(',');
                string CmdText = string.Empty;
                if (ddlChapter.SelectedValue != "0")
                {
                    CmdText = "select count(*) as CountSet, productcode from Contestant C where C.productcode in (" + strProductCode + ") and C.ContestYear=" + ddlYear.SelectedValue + " and C.chapterId=" + ddlChapter.SelectedValue + " and C.paymentreference is not null and C.ChildNumber not in (select ChildNumber from ScoreDetail where contestyear=C.ContestYear and chapterId=C.chapterId and productcode=C.productcode and attendanceFlag ='N' ) group by productcode";
                }
                else
                {
                    CmdText = "select count(*) as CountSet, productcode from Contestant C where C.productcode in (" + strProductCode + ") and C.ContestYear=" + ddlYear.SelectedValue + " and C.chapterId=" + ChapterId + " and C.paymentreference is not null and C.ChildNumber not in (select ChildNumber from ScoreDetail where contestyear=C.ContestYear and chapterId=C.chapterId and productcode=C.productcode and attendanceFlag ='N' ) group by productcode";
                }


                DataSet dsCount = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, CmdText);
                string strPrdCount = string.Empty;
                if (null != dsCount && dsCount.Tables.Count > 0)
                {
                    if (dsCount.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsCount.Tables[0].Rows)
                        {
                            strPrdCount += dr["productcode"].ToString() + "(" + dr["CountSet"].ToString() + ")" + ",";
                        }
                    }
                    else
                    {
                        strPrdCount += altPrdCode + ",";
                    }

                }

                strPrdCount = strPrdCount.TrimEnd(',');
                gvMissingScore.Rows[i].Cells[6].Text = strPrdCount;
            }
        }
        else
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "No Record Exists!";
            lblErr.Visible = true;
            gvMissingScore.Visible = false;
        }
    }
    protected void fillGridView1()
    {
        lblTable2.Text = "Table-2:  EW, PS";

        string strquery = "";
        strquery = "DECLARE @TempScore TABLE( Year Char( 30 ), Event integer ,Chapter  varchar(50), ProductGroup varchar(30), product varchar(30), ChapterId integer, State varchar(100));";
        strquery = strquery + " insert into @TempScore ";


        strquery = strquery + "select c.contest_year as Year,c.EventID as Event,b.chaptercode as Chapter,c.productGroupCode as ProductGroup, c.productcode as Product, c.NSFChapterId as ChapterId, b.State as State from contest c Inner Join chapter b on b.chapterid = c.nsfchapterid  where contest_year=" + ddlYear.SelectedValue + " and eventid=" + ddlEvent.SelectedValue + "";
        if (ddlZone.SelectedIndex != 1 && ddlZone.SelectedIndex != 0)
        {
            strquery = strquery + " and b.zoneid=" + ddlZone.SelectedValue + "";
        }
        if (ddlCluster.SelectedIndex != 1 && ddlCluster.SelectedIndex != 0)
        {
            strquery = strquery + " and b.clusterid=" + ddlCluster.SelectedValue + "";
        }
        if (ddlChapter.SelectedIndex != 1 && ddlChapter.SelectedIndex != 0)
        {
            strquery = strquery + " and b.ChapterId=" + ddlChapter.SelectedValue + "";
        }
        ////////
        ////When All is for zone, cluster, chapter then we have to pass zone id based on roleid's
        //try
        //{
        //    if (Convert.ToInt32(Session["RoleId"].ToString()) == 3)
        //    {
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            int zid = Convert.ToInt32(Session["selZoneID"].ToString());
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 5)
        //    {
        //        int cid = Convert.ToInt32(Session["selChapterID"].ToString());
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
        //        int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select clusterid from chapter where chapterid=" + cid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //        if (ddlChapter.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.ChapterId=" + cid + "";
        //        }

        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 4)
        //    {
        //        int clid = Convert.ToInt32(Session["selClusterID"].ToString());
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from cluster where clusterid=" + clid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //    }
        //    else if (Convert.ToInt32(Session["RoleId"].ToString()) == 9)
        //    {

        //        int cid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select chapterid from Volunteer where memberid=" + Session["LoginId"].ToString() + " and chapterid is not null"));
        //        int clid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select Clusterid from chapter where chapterid=" + cid + ""));
        //        int zid = Convert.ToInt32(SqlHelper.ExecuteScalar(Application[strConnection].ToString(), CommandType.Text, "select zoneid from chapter where chapterid=" + cid + ""));
        //        if (ddlZone.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.zoneid=" + zid + "";
        //        }
        //        if (ddlCluster.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.clusterid=" + clid + "";
        //        }
        //        if (ddlChapter.SelectedIndex == 0)
        //        {
        //            strquery = strquery + " and b.ChapterId=" + cid + "";
        //        }
        //    }
        //}
        //catch (Exception e)
        //{
        //   // Response.Write("Error :" + e.ToString());
        //}
        /////
        strquery = strquery + " and contestdate in ((select convert(varchar(10),SatDay1,101) from weekcalendar where Year(SatDay1)=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + ") union (select convert(varchar(10),SunDay2,101) from weekcalendar where Year(SatDay1)=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + ")) ";
        if (missRank.Checked == true)
        {
            strquery = strquery + " and not exists (select a.rank from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and (a.rank is not null and a.rank<>0)) ";
        }
        else if (missScore.Checked == true)
        {
            strquery = strquery + " and not exists (select a.score1,a.score2,a.score3 from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and a.score1 is not null and a.score2 is not null ";
            if (ddlEvent.SelectedValue.Equals("1"))
            {
                strquery = strquery + " and a.score3 is not null";
            }

            strquery = strquery + " ) ";

        }
        // strquery = strquery + " and not exists (select count(*) from scoredetail where chapterid = c.nsfchapterid and  productid = c.productid  and contestyear=" + ddlYear.SelectedValue + " and  attendanceflag='N' and attendanceflag is not null and attendanceflag <>'' HAVING count(*) > 0)";
        strquery = strquery + "  and  exists ( select count(*) from scoredetail s where s.chapterid = c.nsfchapterid and s.productid = c.productid and s.contestyear=" + ddlYear.SelectedValue + " and attendanceflag ='N' having count(*) <> ";
        strquery = strquery + " ( select count(*) from scoredetail  s2 where s2.chapterid=c.nsfchapterid and s2.productid =c.productid and s2.contestyear=" + ddlYear.SelectedValue + " )) ";

        strquery = strquery + " and exists (select count(*) from contestant a where a.chapterid = c.nsfchapterid and a.productid = c.productid and a.contestyear=" + ddlYear.SelectedValue + " and ";
        strquery = strquery + " paymentreference is not null and productgroupcode in ('EW','PS') HAVING Count(*)>0) order by b.state, b.chaptercode, c.productid;";

        strquery = strquery + " SELECT DISTINCT    t.Year,t.Event,ChapterId, State,t.Chapter,t.ProductGroup, STUFF((SELECT ', ', i.product as [text()]";
        strquery = strquery + "  FROM @TempScore i WHERE i.Chapter = t.Chapter and i.ProductGroup=t.ProductGroup  FOR XML PATH (''))";
        strquery = strquery + " , 1, 2, '') as Product FROM   @TempScore t";

        DataSet dsJVB = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, strquery);
        ViewState["Table2"] = null;

        if (dsJVB.Tables[0].Rows.Count > 0)
        {
            ViewState["Table2"] = dsJVB.Tables[0];
            lblErr1.Text = "";
            gvMissingScore2.DataSource = dsJVB.Tables[0];
            gvMissingScore2.DataBind();
            gvMissingScore2.Visible = true;

            ArrayList arrList = new ArrayList();
            for (int i = 0; i < gvMissingScore2.Rows.Count; i++)
            {
                string prdCode = gvMissingScore2.Rows[i].Cells[6].Text;
                arrList.Add(prdCode);
            }
            for (int i = 0; i < arrList.Count; i++)
            {
                string productCode = arrList[i].ToString();
                string strProductCode = string.Empty;
                string altPrdCode = string.Empty;
                for (int j = 0; j < productCode.Split(',').Length; j++)
                {
                    strProductCode += "'" + productCode.Split(',')[j].ToString().Trim() + "'" + ",";
                    altPrdCode += productCode.Split(',')[j].ToString().Trim() + "(0)" + ",";
                }
                strProductCode = strProductCode.TrimEnd(',');
                string CmdText = "select count(*) as CountSet, productcode from Contestant C where C.productcode in (" + strProductCode + ") and C.ContestYear=" + ddlYear.SelectedValue + " and C.chapterId=" + ddlChapter.SelectedValue + " and C.paymentreference is not null and C.ChildNumber not in (select ChildNumber from ScoreDetail where contestyear=C.ContestYear and chapterId=C.chapterId and productcode=C.productcode and attendanceFlag <>'N' ) group by productcode";

                DataSet dsCount = SqlHelper.ExecuteDataset(Application[strConnection].ToString(), CommandType.Text, CmdText);
                string strPrdCount = string.Empty;
                if (null != dsCount && dsCount.Tables.Count > 0)
                {
                    if (dsCount.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsCount.Tables[0].Rows)
                        {
                            strPrdCount += dr["productcode"].ToString() + "(" + dr["CountSet"].ToString() + ")" + ",";
                        }
                    }
                    else
                    {
                        strPrdCount += altPrdCode + ",";
                    }

                }
                strPrdCount = strPrdCount.TrimEnd(',');
                gvMissingScore2.Rows[i].Cells[6].Text = strPrdCount;
            }
        }
        else
        {
            lblErr1.ForeColor = Color.Red;
            lblErr1.Text = "No Record Exists!";
            lblErr1.Visible = true;
            gvMissingScore2.Visible = false;
        }
    }
    protected void clear()
    {
        lblErr.Visible = true;
        lblErr.ForeColor = Color.Red;
        lblTable1.Text = string.Empty;
        lblTable2.Text = string.Empty;
        lblErr1.Text = string.Empty;


        gvMissingScore.Visible = false;
        gvMissingScore2.Visible = false;
    }
    #endregion



    protected void btnExport_Click(object sender, EventArgs e)
    {
        try
        {
            int InitialRow = 1;
            IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
            IWorksheet oSheet = oWorkbooks.Worksheets.Add();
            oSheet.Name = "Missing Scores";

            String FileName = "MissingScores_";
            if (missRank.Checked == true)
            {
                oSheet.Name = "Missing Ranks";
                FileName = "MissingRanks_";
            }
            FileName = FileName + ddlYear.SelectedValue + "_" + ddlChapter.SelectedItem.Text + "_" + DateTime.Now.Date.ToShortDateString().Replace("/", "_") + ".xls";

            oSheet.Range["A2:E2"].MergeCells = true;
            oSheet.Range["A2"].Value = "Missing Scores Details";
            if (missRank.Checked == true)
                oSheet.Range["A2"].Value = "Missing Ranks Details";
            oSheet.Range["A2"].Font.Color = Color.Green;
            oSheet.Range["A2"].Font.Size = 15;
            oSheet.Range["A2"].HorizontalAlignment = XlHAlign.xlHAlignCenter;

            oSheet.Range["A4:E4"].Font.Bold = true;
            oSheet.Range["A4:E4"].Font.Size = 10;
            oSheet.Range["A4:E4"].MergeCells = true;
            oSheet.Range["A4"].Value = "Table-1 : SB, VB, MB, SC, GB, BB";
            oSheet.Range["A4"].HorizontalAlignment = XlHAlign.xlHAlignCenter;

            InitialRow = 5;
            oSheet.Range["A" + InitialRow + ":E" + InitialRow + ""].Font.Bold = true;
            oSheet.Range["A" + InitialRow + ":E" + InitialRow + ""].ColumnWidth = 30;
            oSheet.Range["A" + InitialRow + ""].ColumnWidth = 5;
            oSheet.Range["A" + InitialRow + ""].Value = "Year";
            oSheet.Range["B" + InitialRow + ""].ColumnWidth = 5;
            oSheet.Range["B" + InitialRow + ""].Value = "Event"; oSheet.Range["C" + InitialRow + ""].ColumnWidth = 30;
            oSheet.Range["C" + InitialRow + ""].Value = "Chapter";
            oSheet.Range["D" + InitialRow + ""].ColumnWidth = 5;
            oSheet.Range["D" + InitialRow + ""].Value = "Product Group";
            oSheet.Range["E" + InitialRow + ""].Value = "Product";

            DataTable dt;
            if (ViewState["Table1"] != null)
            {
                dt = (DataTable)ViewState["Table1"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InitialRow = InitialRow + 1;
                    DataRow dr = dt.Rows[i];
                    oSheet.Range["A" + InitialRow + ""].Value = dr["Year"];
                    oSheet.Range["B" + InitialRow + ""].Value = dr["Event"];
                    oSheet.Range["C" + InitialRow + ""].Value = dr["Chapter"];
                    oSheet.Range["D" + InitialRow + ""].Value = dr["ProductGroup"];
                    oSheet.Range["E" + InitialRow + ""].Value = dr["Product"];
                }
            }

            if (ViewState["Table2"] != null)
            {

                InitialRow = InitialRow + 2;

                oSheet.Range["A" + InitialRow + ":E" + InitialRow].Font.Bold = true;
                oSheet.Range["A" + InitialRow + ":E" + InitialRow].Font.Size = 10;
                oSheet.Range["A" + InitialRow + ":E" + InitialRow].MergeCells = true;
                oSheet.Range["A" + InitialRow].Value = "Table-2: EW, PS";
                oSheet.Range["A" + InitialRow].HorizontalAlignment = XlHAlign.xlHAlignCenter;

                dt = (DataTable)ViewState["Table2"];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    InitialRow = InitialRow + 1;
                    DataRow dr = dt.Rows[i];
                    oSheet.Range["A" + InitialRow + ""].Value = dr["Year"];
                    oSheet.Range["B" + InitialRow + ""].Value = dr["Event"];
                    oSheet.Range["C" + InitialRow + ""].Value = dr["Chapter"];
                    oSheet.Range["D" + InitialRow + ""].Value = dr["ProductGroup"];
                    oSheet.Range["E" + InitialRow + ""].Value = dr["Product"];
                }
            }

            Response.Clear();
            Response.ContentType = "application/vnd.ms-excel";
            Response.AddHeader("Content-Type", "application/vnd.ms-excel");
            Response.AddHeader("Content-Disposition", "attachment;filename=" + FileName);
            oWorkbooks.SaveAs(Response.OutputStream);
            Response.End();
        }
        catch (Exception ex)
        {
        }
    }
}