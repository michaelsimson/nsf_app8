﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Web;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;

public partial class TestSlidesPh2Projection : System.Web.UI.Page
{
    int icTemp;
    int TotalCount;
    protected void Page_Load(object sender, EventArgs e)
    { 
        if (!IsPostBack)
        {
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }
            //icTemp = 26;
           // ViewState["increValue"] = icTemp;
            bindData();
           
        }
        
        
    }


    protected void bindData()
    {
        icTemp = Convert.ToInt32(Session["PreviousWordCount"].ToString());
        DataTable dt = (DataTable)Session["Phase2SlideWords"];
        TotalCount=dt.Rows.Count;
        rSlideProjection.DataSource = dt;
        rSlideProjection.DataBind();
    }


    protected void rSlideProjection_ItemBound(object sender, RepeaterItemEventArgs e)
    {
        if (e.Item.ItemType == ListItemType.Item || e.Item.ItemType == ListItemType.AlternatingItem)
        {

        icTemp = icTemp + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("SNoStuPhase2");
        lblSNo.Text = Convert.ToString(icTemp);
        Label lblProducttitle = (Label)e.Item.FindControl("lblProductTitle");
        Label lblEValue = (Label)e.Item.FindControl("lblEValue");
            Label lblENo=(Label)e.Item.FindControl("lblENo");
        HiddenField hfPhaseValue = (HiddenField)e.Item.FindControl("hdPhaseValue");
        int hfvalue = Convert.ToInt32(hfPhaseValue.Value.ToString());
        if (hfvalue==2)
        {
            lblProducttitle.Text = Session["ddlProduct"].ToString() + "  - PHASE  II";
        }else if(hfvalue==3)
        {
            lblProducttitle.Text = Session["ddlProduct"].ToString() + "  - PHASE  III";
        }

        if (icTemp == Convert.ToInt32(Session["PreviousWordCount"].ToString()) + 1)
            {
                ((HtmlGenericControl)(e.Item.FindControl("divPrev"))).Attributes["style"] += ("display:none;)");
            }
        else if (icTemp == (Convert.ToInt32(Session["PreviousWordCount"].ToString())+TotalCount))
            {
                ((HtmlGenericControl)(e.Item.FindControl("divNext"))).Attributes["style"] += ("display:none;)");
            }

        if (lblEValue.Text.Equals(""))
        {
            lblENo.Visible = false;
        }
       }

    }

    
    
}