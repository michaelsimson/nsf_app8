﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using NativeExcel;
using System.Drawing;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;

public partial class ZoomSessions : System.Web.UI.Page
{
    public static string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public static string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    public class Zoom
    {
        public string Topic { get; set; }
        public string SessionKey { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Password { get; set; }
        public string HostURL { get; set; }
        public string JoinURL { get; set; }
        public string TimeZone { get; set; }
        public string CoachName { get; set; }
    }
    public class Attendees
    {
        public string AttendeeId { get; set; }
        public string AttendeeName { get; set; }
        public string AttendeeEmail { get; set; }
        public string JoinURl { get; set; }
        public string Date { get; set; }
        public string Time { get; set; }
        public string Duration { get; set; }
        public string SessionType { get; set; }
        public string SessionKey { get; set; }
    }
    public class CoHost
    {
        public string CoHostId { get; set; }
        public string CoHostName { get; set; }
        public string CoHostEmail { get; set; }

        public string Date { get; set; }
        public string Time { get; set; }
        public string Duration { get; set; }
        public string SessionType { get; set; }
        public string SessionKey { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            string LoginEmail = Session["LoginEmail"].ToString();
            hdnLoginOrgEmail.Value = LoginEmail;
            LoginEmail = LoginEmail.Substring(0, Session["LoginEmail"].ToString().IndexOf('@'));
            LoginEmail = LoginEmail + "@northsouth.org";
            hdnLoginEmail.Value = LoginEmail;
            FillZoomSessionsHost();
            FillZoomSessions();
            FillCoHostSessions();
        }
    }

    public void FillZoomSessions()
    {
        string CmdText = " select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, ZA.Date, ZA.Time, ZA.Year, ZA.JoinURL, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, ZA.AttendeeId, ZA.Sessionkey, ZA.joinURL, WC.Topic from ZoomAttendee ZA inner join WebConfSessions WC on (ZA.SessionKey=WC.SessionKey and ZA.CMemberId=WC.MemberId) inner join Indspouse IP on (IP.AutoMemberId=ZA.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId) where ZA.MemberId=" + Session["LoginId"].ToString() + " and WC.Date>=GetDate()";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAttendee.DataSource = ds;
                gvAttendee.DataBind();
                dvAttendeeGrid.Visible = true;
                dvZoomSection.Visible = true;
            }
            else
            {
                gvAttendee.DataSource = ds;
                gvAttendee.DataBind();
                lblAttendeeNoRecord.Text = "No record exists.";
                lblAttendeeNoRecord.Visible = true;
            }
        }
    }
    protected void gvAttendee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                gvAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string meetingLink = ((Label)gvAttendee.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;

                hdnHostURL.Value = meetingLink;
                string beginTime = ((Label)gvAttendee.Rows[selIndex].FindControl("lblDate") as Label).Text;
                string day = ((Label)gvAttendee.Rows[selIndex].FindControl("lblTime") as Label).Text;
                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 30 && day == today)
                {
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                }
                else
                {

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showAlert();", true);
                }
            }
        }
        catch
        {

        }
    }

    public void FillZoomSessionsHost()
    {
        string CmdText = "   select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, WC.Date, WC.Time, WC.Year, WC.HostURL,WC.MemberId, WC.EventId, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, WC.Sessionkey, WC.joinURL, VC.HostId, WC.Topic, WC.RecurringType, WC.EndDate, WC.HostURL from  WebConfSessions WC  inner join Indspouse IP on (IP.AutoMemberId=WC.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId)  inner join VirtualRoomLookUp VC on (VC.Vroom=WC.Vroom) where WC.MemberId=" + Session["LoginId"].ToString() + "  and WC.Date>=GetDate()";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdZoomSessionsHost.DataSource = ds;
                GrdZoomSessionsHost.DataBind();
                dvHostSection.Visible = true;
                dvHostNoRecord.Visible = true;
            }
            else
            {
                GrdZoomSessionsHost.DataSource = ds;
                GrdZoomSessionsHost.DataBind();
                lblHostNoRecord.Text = "No record exists.";
                lblHostNoRecord.Visible = true;
            }
        }
    }

    protected void btnAddAttendee_Click(object sender, EventArgs e)
    {
        if (spnAttendeeTitle.InnerText == "Attendee")
        {
            AddAttendee();
        }
        else
        {
            AddCoHost();
        }
    }
    public void AddAttendee()
    {
        try
        {

            string AttendeeName = ddlAttendee.SelectedValue;
            if (AttendeeName == "-1")
            {
                lblErr.Text = "Please select Attendee Name.";
                lblErr.ForeColor = Color.Red;
            }
            else if (Session["LoginId"].ToString() == ddlAttendee.SelectedValue)
            {
                lblErr.Text = "Please select other Attendee since  " + ddlAttendee.SelectedItem.Text + " is a  Host.";
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                string DupText = "select count(*) from ZoomAttendee where SessionKey=" + hdnSessionKey.Value + " and MemberId=" + ddlAttendee.SelectedValue + "";

                int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, DupText));
                if (count > 0)
                {
                    lblErr.Text = "Attendee already exists.";
                    lblErr.ForeColor = Color.Red;
                }
                else
                {
                    string eventId = hdnEventId.Value;
                    if (eventId.Trim() == "")
                    {
                        eventId = "null";
                    }
                    string CmdText = " insert into ZoomAttendee (EventId, year, CMemberId, MemberId, Date, Time, SessionKey, JoinURL, CreatedDate, CreatedBy) values(" + eventId + ", " + DateTime.Now.Year + ", " + hdnMemberId.Value + "," + ddlAttendee.SelectedValue + ", '" + hdnDate.Value + "','" + hdnTime.Value + "', '" + hdnSessionKey.Value + "', '" + hdnJoinURl.Value + "', GetDate(), " + Session["LoginId"].ToString() + ")";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblErr.Text = "Attendee added successfully!";
                    lblErr.ForeColor = Color.Blue;
                    PopulateAttendees();
                }
            }
        }
        catch
        {

        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        dvAttendee.Visible = false;
    }

    protected void GrdZoomSessionsHost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "AddAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                hdnJoinURl.Value = JoinUrl;
                dvAttendee.Visible = true;
                spnAttendeeTitle.InnerText = "Attendee";
                FillVolunteer();
            }
            else if (e.CommandName == "View")
            {

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                //string EventId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                //string CMemberId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                //string Date = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                //string Time = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                //string JoinUrl = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                //hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                //hdnDate.Value = Date;
                //hdnTime.Value = Time;
                //hdnMemberId.Value = CMemberId;
                //hdnJoinURl.Value = JoinUrl;
                //dvAttendee.Visible = true;
                PopulateAttendees();

            }
            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                string HostId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblhostId") as Label).Text;
                hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                DateTime dtDate = Convert.ToDateTime(Date);
                string todayDate = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                DateTime dtToday = Convert.ToDateTime(todayDate);

                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(Time, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 15 && dtDate == dtToday)
                {
                    getmeetingIfo(SessionKey, HostId);
                    string cmdText = "";


                    string meetingLink = hdnHostURL.Value;

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                }
                else
                {

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                }

            }

            else if (e.CommandName == "AddCoHost")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                string HostURL = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblHostURL") as Label).Text;
                string Duration = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                hdnJoinURl.Value = JoinUrl;
                hdnHostURL.Value = HostURL;
                hdnDuration.Value = Duration;
                dvAttendee.Visible = true;
                spnAttendeeTitle.InnerText = "Co-Host";
                FillVolunteer();
            }
        }
        catch
        {

        }
    }

    private void FillVolunteer()
    {
        string CmdText;
        try
        {
            // if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            // {

            CmdText = "select distinct V.MemberId , IP.FirstName+' '+Ip.LastName as name, Ip.Firstname, IP.LastName from indspouse Ip inner join Volunteer V on (V.memberId=IP.AutoMemberId)  Order by Ip.Firstname, IP.LastName ";

            //   }
            //else
            //{
            //    CmdText = "select distinct V.MemberId , IP.FirstName+' '+Ip.LastName as name, Ip.Firstname, IP.LastName from indspouse Ip inner join Volunteer V on (V.memberId=IP.AutoMemberId) where V.MemberId=" + Session["LoginId"].ToString() + "  Order by Ip.Firstname, IP.LastName ";
            //}

            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            DataSet myDataSet = new DataSet();

            ddlAttendee.DataSource = dsstate;
            ddlAttendee.DataTextField = "Name";
            ddlAttendee.DataValueField = "MemberId";

            ddlAttendee.DataBind();


            ddlAttendee.Items.Insert(0, new ListItem("Select", "-1"));



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    protected void GrdAttendee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "DeleteMeeting")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string Attendeeid = ((Label)GrdAttendee.Rows[selIndex].FindControl("lblAttendeeId") as Label).Text;
                hdnAttendeeId.Value = Attendeeid;

                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteMeeting();", true);

            }
        }
        catch
        {

        }
    }

    public void PopulateAttendees()
    {
        string CmdText = " select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, ZA.Date, ZA.Time, ZA.Year, ZA.JoinURL, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, ZA.AttendeeId, ZA.Sessionkey, ZA.joinURL from ZoomAttendee ZA inner join WebConfSessions WC on (ZA.SessionKey=WC.SessionKey and ZA.CMemberId=WC.MemberId) inner join Indspouse IP on (IP.AutoMemberId=ZA.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId) where ZA.SessionKey=" + hdnSessionKey.Value + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                GrdAttendee.DataSource = ds;
                GrdAttendee.DataBind();
                dvAttendees.Visible = true;
                dvAttendeesSection.Visible = true;
            }
            else
            {
                GrdAttendee.DataSource = ds;
                GrdAttendee.DataBind();
                LblAttendeesNoRecord.Text = "No record exists.";
                LblAttendeesNoRecord.Visible = true;
            }

        }
    }
    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        string CmdText = string.Empty;
        CmdText = "Delete from ZoomAttendee where SessionKey='" + hdnSessionKey.Value + "' and AttendeeId=" + hdnAttendeeId.Value + "";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        lblErr.Text = "Attendee removed from the zoom session successfully.";
        lblErr.ForeColor = Color.Red;
        PopulateAttendees();
    }

    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }

    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();

                string status = json["Status"].ToString();
                if (status == "1")
                {
                    hdnHostURL.Value = json["join_url"].ToString();
                }

            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }

    }

    [WebMethod]
    public static List<Zoom> GetMeetingInfo(string SessionKey, String HostId)
    {
        List<Zoom> ObjListZoom = new List<Zoom>();
        string URL = string.Empty;
        try
        {


            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + SessionKey + "";
            urlParameter += "&host_id=" + HostId + "";
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameter.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameter);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();

            }

            var parser = new JavaScriptSerializer();

            dynamic obj = parser.Deserialize<dynamic>(postResponse);



            JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            var serializer = new JavaScriptSerializer();
            SessionKey = json["id"].ToString();
            // hdnHostURL.Value = json["start_url"].ToString();

            string HostURL = json["start_url"].ToString();
            string joinUrl = json["join_url"].ToString();
            string Password = json["password"].ToString();
            string TimeZone = json["timezone"].ToString();

            string Topic = json["topic"].ToString();


            string Date = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select Date from WebConfSessions where SessionKey=" + SessionKey + "").ToString();
            string Time = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  Time  from WebConfSessions where SessionKey=" + SessionKey + "").ToString();

            string CoachName = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  IP.FirstName+' '+IP.LastName  from IndSpouse IP inner join WebConfSessions wc on (IP.AutoMemberId=WC.MemberId) where SessionKey=" + SessionKey + "").ToString();

            String strDate = Convert.ToDateTime(Date).ToString("MMM dd, yyyy");
            String strTime = Convert.ToDateTime(Time).ToString("hh:mm tt");

            Zoom ObjZoom = new Zoom();
            ObjZoom.JoinURL = joinUrl;
            ObjZoom.Password = Password;
            ObjZoom.Topic = Topic;
            ObjZoom.Date = strDate;
            ObjZoom.Time = strTime;
            ObjZoom.TimeZone = TimeZone;
            ObjZoom.CoachName = CoachName;
            ObjListZoom.Add(ObjZoom);
        }
        catch
        {
        }
        return ObjListZoom;

    }

    [WebMethod]
    public static List<Attendees> GetAttendees(string SessionKey)
    {
        List<Attendees> ObjListAttendee = new List<Attendees>();

        try
        {

            string CmdText = " select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, ZA.Date, ZA.Time, ZA.Year, ZA.JoinURL, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, ZA.AttendeeId, ZA.Sessionkey, ZA.joinURL from ZoomAttendee ZA inner join WebConfSessions WC on (ZA.SessionKey=WC.SessionKey and ZA.CMemberId=WC.MemberId) inner join Indspouse IP on (IP.AutoMemberId=ZA.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId) where ZA.SessionKey=" + SessionKey + "";
            DataSet ds = SqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Attendees ObjAttendees = new Attendees();
                        ObjAttendees.AttendeeId = dr["AttendeeId"].ToString();
                        ObjAttendees.AttendeeName = dr["Name"].ToString();
                        ObjAttendees.AttendeeEmail = dr["Email"].ToString();
                        ObjAttendees.SessionType = dr["SessionType"].ToString();
                        ObjAttendees.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        ObjAttendees.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("hh:mm tt");
                        ObjAttendees.Duration = dr["Duration"].ToString();
                        ObjAttendees.SessionKey = dr["SessionKey"].ToString();
                        ObjListAttendee.Add(ObjAttendees);
                    }
                }
                else
                {

                }

            }
        }
        catch
        {
        }
        return ObjListAttendee;
    }

    [WebMethod]
    public static int DeleetAteendees(string AttendeeId)
    {
        int Retval = -1;

        try
        {

            string CmdText = " Delete from ZoomAttendee where AttendeeId=" + AttendeeId + "";
            SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, CmdText);
            Retval = 1;
        }
        catch
        {
        }
        return Retval;
    }

    public void FillCoHostSessions()
    {
        string CmdText = "   select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, WC.Date, WC.Time, WC.Year, WC.HostURL,WC.MemberId, WC.EventId, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, WC.Sessionkey, WC.joinURL, VC.HostId, WC.Topic, WC.RecurringType, WC.EndDate from WebConf_CoHost WH inner join WebConfSessions WC on (WH.SessionKey=WC.SessionKey)  inner join Indspouse IP on (IP.AutoMemberId=WH.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId)  inner join VirtualRoomLookUp VC on (VC.Vroom=WC.Vroom) where WH.MemberId=" + Session["LoginId"].ToString() + "  and WC.Date>=GetDate()";
        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {

                GrdCoHost.DataSource = ds;
                GrdCoHost.DataBind();
                dvCoHost.Visible = true;

            }
            else
            {
                dvCoHost.Visible = true;
                GrdCoHost.DataSource = ds;
                GrdCoHost.DataBind();
                dvCoHost.Visible = true;
                lblCoHostNoRecord.Text = "No record exists.";
                lblCoHostNoRecord.Visible = true;
            }
        }
    }

    protected void GrdCoHost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "AddAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                hdnJoinURl.Value = JoinUrl;
                dvAttendee.Visible = true;
                FillVolunteer();
            }
            else if (e.CommandName == "View")
            {

                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                //string EventId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                //string CMemberId = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                //string Date = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                //string Time = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                //string JoinUrl = ((Label)GrdZoomSessionsHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                //hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                //hdnDate.Value = Date;
                //hdnTime.Value = Time;
                //hdnMemberId.Value = CMemberId;
                //hdnJoinURl.Value = JoinUrl;
                //dvAttendee.Visible = true;
                PopulateAttendees();

            }
            else if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                string HostId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblhostId") as Label).Text;
                hdnEventId.Value = EventId;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                DateTime dtDate = Convert.ToDateTime(Date);
                string todayDate = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                DateTime dtToday = Convert.ToDateTime(todayDate);

                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(Time, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 15 && dtDate == dtToday)
                {
                    getmeetingIfo(SessionKey, HostId);
                    string cmdText = "";


                    string meetingLink = hdnHostURL.Value;

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", true);
                }
                else
                {

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg();", true);
                }

            }
        }
        catch
        {

        }
    }

    public void AddCoHost()
    {
        string AttendeeName = ddlAttendee.SelectedValue;
        if (AttendeeName == "-1")
        {
            lblErr.Text = "Please select Co-Host Name.";
            lblErr.ForeColor = Color.Red;
        }
        else if (hdnMemberId.Value == ddlAttendee.SelectedValue)
        {
            lblErr.Text = "Please select other Co-Host since  " + ddlAttendee.SelectedItem.Text + " is a  Host.";
            lblErr.ForeColor = Color.Red;
        }

        else
        {
            if (IsZoomAttendee() > 0)
            {
                lblErr.Text = "Please select other Co-Host since  " + ddlAttendee.SelectedItem.Text + " is already an attendee for this session.";
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                string DupText = "select count(*) from WebConf_CoHost where SessionKey=" + hdnSessionKey.Value + " and MemberId=" + ddlAttendee.SelectedValue + "";

                int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, DupText));
                if (count > 0)
                {
                    lblErr.Text = "Duplicate exists.";
                    lblErr.ForeColor = Color.Red;
                }
                else
                {

                    string CmdText = " insert into WebConf_CoHost (MemberId, SessionKey, HostURL, CreatedDate, CreatedBy) values(" + ddlAttendee.SelectedValue + ", '" + hdnSessionKey.Value + "', '" + hdnHostURL.Value + "', Getdate(), " + Session["LoginId"].ToString() + ")";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblErr.Text = "Co-Host added successfully!";
                    lblErr.ForeColor = Color.Blue;
                    dvAttendee.Visible = false;

                }
            }
        }
    }
    public int IsZoomAttendee()
    {
        int Retval = 0;
        string CmdText = string.Empty;
        CmdText = "select count(*) from ZoomAttendee where SessionKey=" + hdnSessionKey.Value + " and memberId=" + ddlAttendee.SelectedValue + "";
        int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, CmdText));
        Retval = count;
        return Retval;
    }
    [WebMethod]
    public static List<CoHost> GetCoHost(string SessionKey)
    {
        List<CoHost> ObjListCoHost = new List<CoHost>();
        string CmdText = "";
        CmdText = "select IP.FirstName, IP.LAstName, IP.FirstName+' '+IP.LastName as Name, IP.Email, WS.Date, WS.Time, case when wS.sessionType=2 then 'Single' when wS.SessionType=3 then 'Recurring' end as SessionType, WS.Duration, WS.Year, WC.CoHostId from WebConf_CoHost WC inner join Indspouse IP on (WC.MemberId=IP.AutoMemberId) inner join WebConfSessions WS on (WS.SessionKey=WC.SessionKey) where WS.SessionKey=" + SessionKey + "";
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoHost objCoHost = new CoHost();
                        objCoHost.CoHostId = dr["CoHostId"].ToString();
                        objCoHost.CoHostName = dr["Name"].ToString();
                        objCoHost.CoHostEmail = dr["Email"].ToString();
                        objCoHost.Date = dr["Date"].ToString();
                        objCoHost.Time = dr["Time"].ToString();
                        ObjListCoHost.Add(objCoHost);
                    }
                }
                else
                {

                }
            }
        }
        catch (Exception ex)
        {

        }
        return ObjListCoHost;
    }

    [WebMethod]
    public static int DeleteCoHosts(string CoHostId)
    {
        int Retval = -1;

        try
        {

            string CmdText = " Delete from WebConf_CoHost where CoHostId=" + CoHostId + "";
            SqlHelper.ExecuteNonQuery(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, CmdText);
            Retval = 1;
        }
        catch
        {
        }
        return Retval;
    }

    public int CheckAvailabilityOfVolunteer()
    {
        int Retval = 1;
        string CmdText = string.Empty;
        string Day = string.Empty;
        string Time = string.Empty;
        Double Duration = 0;
        CmdText = "select Date,Time, Duration from WebConfSessions where memberId=" + ddlAttendee.SelectedValue + "";
        DataSet ds = new DataSet();
        DataSet dsAttendee = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Duration = Convert.ToDouble(dr["Duration"].ToString());
                        Day = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                        string MeetingDay = Convert.ToDateTime(hdnDate.Value).ToString("dddd");
                        string StartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + dr["Time"].ToString();
                        DateTime dtStartTime = Convert.ToDateTime(StartTime);
                        DateTime dtEndTime = Convert.ToDateTime(StartTime).AddMinutes(Duration);

                        string MeetingStartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + hdnTime.Value;
                        DateTime dtMeetingStartTime = Convert.ToDateTime(MeetingStartTime);
                        DateTime dtMeetingEndTime = Convert.ToDateTime(MeetingStartTime).AddMinutes(Convert.ToInt32(hdnDuration.Value));

                        if (Day == MeetingDay)
                        {
                            if (dtStartTime >= dtMeetingStartTime || dtStartTime <= dtMeetingEndTime || dtEndTime >= dtMeetingStartTime || dtEndTime <= dtMeetingEndTime)
                            {
                                Retval = -1;
                            }
                        }
                    }

                }
            }
            if (Retval == 1)
            {
                CmdText = "select WC.Date, WC.Time, WC.Duration from WebConfSessions Wc inner join ZoomAttendee ZA on (ZA.SessionKey=Wc.SessionKey) where ZA.MemberId=" + ddlAttendee.SelectedValue + "";
                dsAttendee = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                if (dsAttendee.Tables.Count > 0)
                {
                    if (dsAttendee.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            Duration = Convert.ToDouble(dr["Duration"].ToString());
                            Day = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                            string MeetingDay = Convert.ToDateTime(hdnDate.Value).ToString("dddd");
                            string StartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + dr["Time"].ToString();
                            DateTime dtStartTime = Convert.ToDateTime(StartTime);
                            DateTime dtEndTime = Convert.ToDateTime(StartTime).AddHours(Duration);

                            string MeetingStartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + hdnTime.Value;
                            DateTime dtMeetingStartTime = Convert.ToDateTime(MeetingStartTime);
                            DateTime dtMeetingEndTime = Convert.ToDateTime(MeetingStartTime).AddHours(Convert.ToInt32(hdnDuration.Value));

                            if (Day == MeetingDay)
                            {
                                if (dtStartTime >= dtMeetingStartTime || dtStartTime <= dtMeetingEndTime || dtEndTime >= dtMeetingStartTime || dtEndTime <= dtMeetingEndTime)
                                {
                                    Retval = -1;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch
        {

        }
        return Retval;
    }

    public void EmailContent(string SessionKey, string HostId, string AttendeeEmail)
    {
        try
        {

            string URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + SessionKey + "";
            urlParameter += "&host_id=" + HostId + "";
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameter.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameter);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();

            }

            var parser = new JavaScriptSerializer();

            dynamic obj = parser.Deserialize<dynamic>(postResponse);



            JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            var serializer = new JavaScriptSerializer();
            SessionKey = json["id"].ToString();
            // hdnHostURL.Value = json["start_url"].ToString();

            string HostURL = json["start_url"].ToString();
            string joinUrl = json["join_url"].ToString();
            string Password = json["password"].ToString();
            string TimeZone = json["timezone"].ToString();

            string Topic = json["topic"].ToString();


            string Date = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select Date from WebConfSessions where SessionKey=" + SessionKey + "").ToString();
            string Time = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  Time  from WebConfSessions where SessionKey=" + SessionKey + "").ToString();

            string CoachName = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  IP.FirstName+' '+IP.LastName  from IndSpouse IP inner join WebConfSessions wc on (IP.AutoMemberId=WC.MemberId) where SessionKey=" + SessionKey + "").ToString();

            String strDate = Convert.ToDateTime(Date).ToString("MMM dd, yyyy");
            String strTime = Convert.ToDateTime(Time).ToString("hh:mm tt");

            string tblHtml = string.Empty;
            tblHtml += "<table>";
            tblHtml += "<tr><td><span>Hi there,</span><br /><br /></td></tr>";


            tblHtml += "<tr><td><span>" + CoachName + " is inviting you to a scheduled Zoom meeting</span><br /><br /></td></tr>";


            tblHtml += "<tr><td><span>Time: " + strDate + " " + strTime + " " + TimeZone + "(US and Canada)</span><br /><br /></td></tr>";

            tblHtml += "<tr><td><span>Join from PC, Mac, Linux, iOS or Android: " + joinUrl + "</span></td></tr>";
            tblHtml += "<tr><td><span>Password: " + Password + "</span><br /><br /></td></tr>";

            tblHtml += "<tr><td><span>Or iPhone one-tap :</span></td></tr>";
            tblHtml += "<tr><td><span>US: +16465588656,,550101276# or +16699006833,,550101276#</span></td></tr>";
            tblHtml += "<tr><td><span>Or Telephone:</span></td></tr>";
            tblHtml += "<tr><td><span>Dial(for higher quality, dial a number based on your current location):</span></td></tr>";
            tblHtml += "<tr><td><span>US: +1 646 558 8656 or +1 669 900 6833</span></td></tr>";
            tblHtml += "<tr><td><span>Meeting ID: " + SessionKey + "</span></td></tr>";
            tblHtml += "<tr><td><span>International numbers available: https://zoom.us/u/bg1A7g1o</span></br></td></tr>";


            tblHtml += "</table>";
            SendEmailToAttendee(tblHtml, AttendeeEmail);
        }
        catch
        {
        }
    }

    public void SendEmailToAttendee(string tblHtml, string AttendeeEmail)
    {
        string LoginEmail = Session["LoginEmail"].ToString();
        //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";
        string volunteerEmail = AttendeeEmail.TrimEnd(';');
        string fromEMail = LoginEmail;
        string subject = "Zoom Invite";
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromEMail);
            mail.Body = tblHtml;
            mail.Subject = subject;

            // mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

            String[] strEmailAddressList = null;
            String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Match EmailAddressMatch = default(Match);
            strEmailAddressList = volunteerEmail.Replace(',', ';').Split(';');
            foreach (object item in strEmailAddressList)
            {
                EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);

                if (EmailAddressMatch.Success)
                {

                    mail.Bcc.Add(new MailAddress(item.ToString().Trim()));

                }
            }

            // mail.Bcc.Add(new MailAddress("michael.simson@capestart.com".Trim()));
            SmtpClient client = new SmtpClient();
            // client.Host = host;
            mail.IsBodyHtml = true;
            try
            {
                client.Send(mail);

                //lblError.Text = ex.ToString();
            }
            catch (Exception ex)
            {
            }
        }
        catch
        {
        }
    }
}