﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using NativeExcel;
using System.Drawing;
using System.Net;
using System.IO;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;

public partial class CreateZoomSessionOwkshop : System.Web.UI.Page
{
    public static string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public static string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";

    public class ZoomSessions
    {
        public string Topic { get; set; }
    }
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        lblErr.Text = "";
        if (!IsPostBack)
        {
            string LoginEmail = Session["LoginEmail"].ToString();
            hdnLoginOrgEmail.Value = LoginEmail;
            LoginEmail = LoginEmail.Substring(0, Session["LoginEmail"].ToString().IndexOf('@'));
            LoginEmail = LoginEmail + "@northsouth.org";
            hdnLoginEmail.Value = LoginEmail;

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            {
                // ddlMeetingType.SelectedValue = "2";
            }
            else
            {
                //ddlMeetingType.SelectedValue = "2";
                // ddlMeetingType.Enabled = false;
            }
            FillVolunteer();

            FillZoomSessions();
        }

    }


    private void FillRoles()
    {
        try
        {


            //ddlProductGroup
            string ddlproductgroupqry;

            //if (Convert.ToInt32(Session["RoleId"]) == 89)
            //{
            //    ddlproductgroupqry = "select distinct p.Name,v.ProductGroupID,v.ProductGroupCode from volunteer v inner join ProductGroup p on v.ProductGroupID=p.ProductGroupID and  v.Eventid=20  where v.EventYear=" + ddlYear.SelectedValue + " and v.Memberid=" + Session["LoginID"] + " and v.RoleId=" + Session["RoleId"] + " and v.ProductId is not Null  Order by v.ProductGroupId";
            //}
            //else
            //{
            ddlproductgroupqry = "select  distinct R.RoleId, R.RoleCode from Role R inner join Volunteer V on (R.ROleId=V.ROleId) where V.MemberId=" + ddlVolunteer.SelectedValue + "";
            // ddlproductgroupqry = "select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" + ddlEvent.SelectedValue + " and ProductId is not Null Order by ProductGroupId";
            //}
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductgroupqry);

            ddlRole.DataSource = dsstate;
            ddlRole.DataTextField = "RoleCode";
            ddlRole.DataValueField = "RoleId";
            ddlRole.DataBind();
            ddlRole.Items.Insert(0, new ListItem("Select", "-1"));
            if (dsstate.Tables[0].Rows.Count == 1)
            {
                ddlRole.SelectedValue = dsstate.Tables[0].Rows[0]["RoleId"].ToString();
                ddlRole.Enabled = false;
                FillTeam();
                FillEvent();
                dvRole.Visible = false;
                dvTeam.Visible = false;
                dvEvent.Visible = false;
            }
            else
            {
                ddlRole.Enabled = true;
                dvRole.Visible = true;
                dvTeam.Visible = true;
                dvEvent.Visible = true;
                ddlEvent.Enabled = true;
                ddlEvent.Items.Clear();
                ddlteam.Visible = true;
                ddlteam.Items.Clear();
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    private void FillVolunteer()
    {
        string CmdText;
        try
        {
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            {

                CmdText = "select distinct V.MemberId , IP.FirstName+' '+Ip.LastName +' - '+ Ip.Email as name, Ip.Firstname, IP.LastName from indspouse Ip inner join Volunteer V on (V.memberId=IP.AutoMemberId)  Order by Ip.Firstname, IP.LastName ";

            }
            else
            {
                CmdText = "select distinct V.MemberId , IP.FirstName+' '+Ip.LastName +' - '+ Ip.Email as name, Ip.Firstname, IP.LastName from indspouse Ip inner join Volunteer V on (V.memberId=IP.AutoMemberId) where V.MemberId=" + Session["LoginId"].ToString() + "  Order by Ip.Firstname, IP.LastName ";
            }

            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            DataSet myDataSet = new DataSet();
            ddlVolunteer.DataSource = dsstate;
            ddlVolunteer.DataTextField = "Name";
            ddlVolunteer.DataValueField = "MemberId";

            ddlVolunteer.DataBind();

            ddlAttendee.DataSource = dsstate;
            ddlAttendee.DataTextField = "Name";
            ddlAttendee.DataValueField = "MemberId";

            ddlAttendee.DataBind();

            ddlCoHost.DataSource = dsstate;
            ddlCoHost.DataTextField = "Name";
            ddlCoHost.DataValueField = "MemberId";

            ddlCoHost.DataBind();



            ddlVolunteer.Items.Insert(0, new ListItem("Select", "-1"));
            ddlAttendee.Items.Insert(0, new ListItem("Select", "-1"));
            ddlCoHost.Items.Insert(0, new ListItem("Select", "-1"));
            ddlVolunteer.Enabled = true;
            if (dsstate.Tables[0].Rows.Count == 1)
            {
                ddlVolunteer.SelectedIndex = 1;
                ddlVolunteer.Enabled = false;
                FillRoles();
            }
            else
            {
                ddlVolunteer.Enabled = true;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    private void FillTeam()
    {
        ddlteam.Items.Clear();
        string Cmdtext;
        try
        {
            Cmdtext = " Select distinct T.TeamId,T.TeamName from VolTeamMatrix T inner join Role R on (R.TeamId=T.teamid) where R.RoleId=" + ddlRole.SelectedValue + "";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
            DataSet myDataSet = new DataSet();

            ddlteam.DataTextField = "TeamName";
            ddlteam.DataValueField = "TeamId";
            ddlteam.DataSource = dsstate;
            ddlteam.DataBind();
            ddlteam.Items.Insert(0, new ListItem("Select", "-1"));
            ddlteam.Enabled = true;
            if (dsstate.Tables[0].Rows.Count > 0)
            {
                if (dsstate.Tables[0].Rows.Count == 1)
                {
                    ddlteam.SelectedIndex = 1;
                    ddlteam.Enabled = false;

                    txtTopic.Text = ddlVolunteer.SelectedItem.Text.Split('-')[0].Trim() + " - " + ddlRole.SelectedItem.Text;
                    //if (dvRole.Visible == false)
                    //{
                    //    dvTeam.Visible = false;
                    //}
                    //else
                    //{
                    //    dvTeam.Visible = true;
                    //}
                }
                else
                {
                    dvTeam.Visible = true;
                    ddlteam.Enabled = true;
                }
            }
            else
            {
                ddlteam.DataSource = dsstate;
                ddlteam.DataBind();
                ddlteam.Items.Insert(0, new ListItem("No Team found", "-1"));
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    private void FillEvent()
    {
        string Cmdtext;
        try
        {
            Cmdtext = " select  distinct Eventid, EventCode from  Volunteer where MemberId=" + ddlVolunteer.SelectedValue + " and RoleId=" + ddlRole.SelectedValue + " and EventId is not null";
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
            DataSet myDataSet = new DataSet();

            ddlEvent.DataTextField = "EventCode";
            ddlEvent.DataValueField = "Eventid";
            ddlEvent.DataSource = dsstate;
            ddlEvent.DataBind();
            ddlEvent.Items.Insert(0, new ListItem("Select", "-1"));
            ddlEvent.Enabled = true;
            if (dsstate.Tables[0].Rows.Count > 0)
            {


                if (dsstate.Tables[0].Rows.Count == 1)
                {
                    ddlEvent.SelectedIndex = 1;
                    ddlEvent.Enabled = false;

                    txtTopic.Text = ddlVolunteer.SelectedItem.Text.Split('-')[0].Trim() + " - " + ddlRole.SelectedItem.Text;
                    //dvEvent.Visible = false;
                    //if (dvRole.Visible == false)
                    //{
                    //    dvEvent.Visible = false;
                    //}
                    //else
                    //{
                    //    dvEvent.Visible = true;
                    //}
                }
                else
                {
                    dvEvent.Visible = true;
                    ddlEvent.Enabled = true;
                }
            }
            if (dsstate.Tables[0].Rows.Count <= 0)
            {
                ddlEvent.DataSource = dsstate;
                ddlEvent.DataBind();
                ddlEvent.Items.Insert(0, new ListItem("No Event found", "-1"));
                // lblErr.Text = "You don't have an access to this application.";
                //  lblErr.ForeColor = Color.Red;

            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }
    protected void btnCreateMeeting_Click1(object sender, EventArgs e)
    {
        if (validateCreateZoom() == 1)
        {
            string HostId = checkAvailableVrooms(Convert.ToInt32(ddlDuration.SelectedValue));
            hdnHostId.Value = HostId;
            if (btnCreateMeeting.Text == "Update Session")
            {
                if (DeleteMeeting() > 0)
                {
                    createZoomMeeting("1");
                    lblErr.Text = "Zoom session updated successfully.";
                }
            }
            else
            {
                createZoomMeeting("1");
            }
        }

    }
    protected void btnClear_Click1(object sender, EventArgs e)
    {
        Response.Redirect("CreateZoomSessions.aspx");
    }

    protected void FillZoomSessions()
    {
        try
        {
            string fillqry = " select distinct wc.year, wc.RoleId, wc.ConfId, wc.EventId, wc.memberId, wc.TeamId, wc.Date, wc.Time, wc.Duration, case when wc.SessionType=2 then 'Single' when wc.SessionType=3 then 'Recurring' end as SessionType, wc.Sessionkey, wc.HostURl, wc.JoinURL,IP.FirstName, IP.LastName, IP.FirstName+' '+IP.Lastname as name, E.EventCode, VT.TeamName, V.RoleCode, wc.Vroom, VL.HostId, wc.joinURL, WC.EndDate, WC.RecurringType, WC.HostURL from WebConfSessions wc inner join Indspouse IP on (IP.AutomemberId=wc.memberId) left join Volteammatrix VT on (VT.teamId=wc.teamId) inner join Volunteer V on (V.RoleId=wc.Roleid and V.memberId=v.memberId) inner join VirtualRoomLookup VL on (wc.Vroom=VL.Vroom) left join Event E on (E.EventId=wc.EventId) where wc.year=" + ddlYear.SelectedValue + "";
            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2" || Session["RoleID"].ToString() == "96" || Session["RoleID"].ToString() == "5")
            {
                fillqry += " order by Ip.FirstName, IP.LastName ASC";
            }
            else
            {
                fillqry += " and wc.MemberId=" + Session["LoginId"].ToString() + "";
            }

            DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, fillqry);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    gvOnlineWkShopZoom.DataSource = ds;
                    gvOnlineWkShopZoom.DataBind();
                    gvOnlineWkShopZoom.Visible = true;
                    Session["Sessionno"] = ds.Tables[0];
                    lblNoRecord.Visible = false;

                    for (int i = 0; i < gvOnlineWkShopZoom.Rows.Count; i++)
                    {
                        string TodayDate = DateTime.Now.ToShortDateString();
                        DateTime drDateToday = Convert.ToDateTime(TodayDate);
                        string MeetingDate = gvOnlineWkShopZoom.Rows[i].Cells[7].Text.ToString().Trim();
                        DateTime dtMeetDate = Convert.ToDateTime(MeetingDate);
                        string SessionType = gvOnlineWkShopZoom.Rows[i].Cells[10].Text.ToString();
                        if (dtMeetDate < drDateToday && SessionType.ToString().Trim() == "3")
                        {
                            ((Button)gvOnlineWkShopZoom.Rows[i].FindControl("btnJoinMeeting") as Button).Visible = false;
                            gvOnlineWkShopZoom.Rows[i].Cells[14].Text = "";
                        }
                        else
                        {
                            ((Button)gvOnlineWkShopZoom.Rows[i].FindControl("btnJoinMeeting") as Button).Enabled = true;
                        }
                        //if (SessionType.ToString().Trim() == "3")
                        //{
                        //    gvOnlineWkShopZoom.Rows[i].Cells[10].Text = "Recurring";
                        //}
                        //else
                        //{
                        //    gvOnlineWkShopZoom.Rows[i].Cells[10].Text = "Single";
                        //}
                    }
                }
                else
                {
                    lblNoRecord.Visible = true;
                    gvOnlineWkShopZoom.DataSource = ds;
                    gvOnlineWkShopZoom.DataBind();
                }

            }
            else
            {
                lblNoRecord.Visible = true;
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void createZoomMeeting(string mode)
    {
        try
        {


            string URL = string.Empty;
            string durationHrs = ddlDuration.SelectedValue;
            string service = "1";
            if (mode == "1")
            {
                URL = "https://api.zoom.us/v1/meeting/create";
            }
            else if (mode == "2")
            {
                URL = "https://api.zoom.us/v1/meeting/update";
            }
            string urlParameter = string.Empty;

            string test = (DateTime.UtcNow.ToString("s") + "Z").ToString();

            string date = ddlworkshopDate.Text;
            string time = ddlTime.SelectedValue;
            string MakeupDateTime = date + " " + time;
            DateTime dtMakeupDate = Convert.ToDateTime(MakeupDateTime).AddHours(4);

            string StrMakeupDate = dtMakeupDate.ToString("yyyy-MM-dd");
            string StrMakeupTime = dtMakeupDate.ToString("HH:mm:ss");

            string StrMakeupDateTime = StrMakeupDate + "T" + StrMakeupTime + "Z";

            string dateTime = date + "T" + time + "Z";

            string topic = ddlVolunteer.SelectedItem.Text.Split('-')[0].Trim() + "-" + ddlRole.SelectedItem.Text;

            DateTime dtFromS = new DateTime();
            DateTime dtEnds = DateTime.Now;
            double mins = 40.0;
            DateTime dtTo = new DateTime();
            if (DateTime.TryParse(time, out dtFromS))
            {
                TimeSpan TS = dtFromS - dtEnds;
                mins = TS.TotalMinutes;
                dtTo = dtFromS.AddHours(4);
            }
            string timeFormat = dtTo.Hour + ":" + dtTo.Minute + ":" + dtTo.Second;
            string MeetingType = "2";
            if (RbtnRecurring.Checked == true)
            {
                MeetingType = "3";
            }

            dateTime = date + "T" + timeFormat + "Z";
            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostId.Value + "";
            if (mode == "1")
            {
                urlParameter += "&topic=" + topic + "";
                urlParameter += "&password=training";
                urlParameter += "&type=" + MeetingType + "";
                urlParameter += "&start_time=" + StrMakeupDateTime + "";
                urlParameter += "&duration=" + ddlDuration.SelectedValue + "";
                urlParameter += "&timezone=America/Chicago";
                urlParameter += "&option_jbh=true";
                urlParameter += "&option_host_video=true";
                urlParameter += "&option_audio=both";
                urlParameter += "&option_auto_record_type=none";
            }


            makeZoomAPICall(urlParameter, URL, service);


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();




            }
            if (serviceType == "1")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                string SessionKey = json["id"].ToString();
                // hdnHostURL.Value = json["start_url"].ToString();

                string HostURL = json["start_url"].ToString();
                string joinUrl = json["join_url"].ToString();

                try
                {
                    string EventId;
                    string TeamId;
                    if (ddlEvent.Items.Count > 0 && ddlEvent.SelectedValue != "-1")
                    {
                        EventId = "" + ddlEvent.SelectedValue + "";
                    }
                    else
                    {
                        EventId = "null";
                    }
                    if (ddlteam.Items.Count > 0 && ddlteam.SelectedValue != "-1")
                    {
                        TeamId = "" + ddlteam.SelectedValue + "";
                    }
                    else
                    {
                        TeamId = "null";
                    }
                    string topic = ddlVolunteer.SelectedItem.Text.Split('-')[0].Trim() + "-" + ddlRole.SelectedItem.Text;
                    string cmdText = string.Empty;
                    string MeetingType = "2";
                    if (RbtnRecurring.Checked == true)
                    {
                        MeetingType = "3";
                    }

                    if (MeetingType == "2")
                    {
                        cmdText = " insert into WebConfSessions (RoleId,Year, MemberId, TeamId, EventId, Date, Time, Duration, SessionType, SessionKey, HostURL, JoinURL, CreateDate, CreatedBy, vroom, Topic)";
                        cmdText += " values (" + ddlRole.SelectedValue + ", " + ddlYear.SelectedValue + ", " + ddlVolunteer.SelectedValue + "," + TeamId + "," + EventId + ", '" + ddlworkshopDate.Text + "', '" + ddlTime.SelectedValue + "', " + ddlDuration.SelectedValue + ",'" + MeetingType + "', " + SessionKey + ", '" + HostURL + "', '" + joinUrl + "', GetDate(), " + Session["LoginId"].ToString() + ", " + hdnVroom.Value + ", '" + topic + "')";
                    }
                    else if (MeetingType == "3")
                    {
                        cmdText = " insert into WebConfSessions (RoleId,Year, MemberId, TeamId, EventId, Date, Time, Duration, SessionType, SessionKey, HostURL, JoinURL, CreateDate, CreatedBy, vroom, Topic, RecurringType,EndDate )";
                        cmdText += " values (" + ddlRole.SelectedValue + ", " + ddlYear.SelectedValue + ", " + ddlVolunteer.SelectedValue + "," + TeamId + "," + EventId + ", '" + ddlworkshopDate.Text + "', '" + ddlTime.SelectedValue + "', " + ddlDuration.SelectedValue + ",'" + MeetingType + "', " + SessionKey + ", '" + HostURL + "', '" + joinUrl + "', GetDate(), " + Session["LoginId"].ToString() + ", " + hdnVroom.Value + ", '" + topic + "', '" + DDLRecurrinType.SelectedItem.Text + "', '" + TxtEndDate.Text + "')";
                    }


                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                    lblErr.Text = "Zoom session created successfully.";
                    if (hdnConfId.Value != "")
                    {
                        cmdText = "Update ZoomAttendee set Date='" + ddlworkshopDate.Text + "', Time ='" + ddlTime.SelectedValue + "', SessionKey='" + SessionKey + "' where Sessionkey='" + hdnSessionKey.Value + "'";

                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
                    }

                    if (ddlCoHost.SelectedItem.Value != "-1")
                    {
                        string CmdText = " insert into WebConf_CoHost (MemberId, SessionKey, HostURL, CreatedDate, CreatedBy) values(" + ddlCoHost.SelectedValue + ", '" + SessionKey + "', '" + hdnHostURL.Value + "', Getdate(), " + Session["LoginId"].ToString() + ")";
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    }
                    // cmdText = " update Registration_OnlineWkshop set JoinURL='" + joinUrl + "' where EventYear=" + ddlYear.SelectedValue + " and ProductGroupId=" + ddlProductGroup.SelectedValue + " and ProductId=" + ddlProduct.SelectedValue + "";
                    //SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    lblErr.ForeColor = Color.Green;
                    FillZoomSessions();
                    if ((hdnDuration.Value != ddlDuration.SelectedValue || hdnDate.Value != ddlworkshopDate.Text || hdnTime.Value != ddlTime.SelectedValue) && hdnConfId.Value != "")
                    {
                        string AttendeeEmail = string.Empty;
                        string HostId = string.Empty;
                        string CmdText = "select Vl.HostId, Ip.Email from ZoomAttendee ZA inner join Indspouse IP on (IP.AutoMemberId=ZA.MemberId) inner join WebConfSessions WC on (WC.SessionKey=ZA.SessionKey) inner join VirtualRoomLookUp Vl on (WC.Vroom=Vl.Vroom) where ZA.SessionKey=" + SessionKey + "";

                        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                        if (ds.Tables.Count > 0)
                        {
                            if (ds.Tables[0].Rows.Count > 0)
                            {
                                foreach (DataRow dr in ds.Tables[0].Rows)
                                {
                                    AttendeeEmail += dr["Email"].ToString() + ";";
                                    HostId = dr["HostId"].ToString();
                                }
                            }
                        }


                        EmailContent(SessionKey, HostId, AttendeeEmail);
                    }
                }
                catch
                {
                }
            }

            if (serviceType == "5")
            {
                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();

                hdnHostURL.Value = json["start_url"].ToString();



            }



        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }

    public string checkAvailableVrooms(int Duration)
    {

        string cmdtext = string.Empty;
        DataSet ds = new DataSet();
        string hostID = string.Empty;

        string strStartTime = ddlTime.SelectedValue;

        string strStartDay = Convert.ToDateTime(ddlworkshopDate.Text).ToString("dddd");
        Duration = Duration + 30;
        string strEndTime = Convert.ToDateTime(strStartTime).ToString();
        strStartTime = Convert.ToDateTime(strStartTime).ToString();

        DateTime dtStarttime = new DateTime();
        DateTime dtEndTime = new DateTime();
        DateTime dtPrStartTime = Convert.ToDateTime(strStartTime).AddMinutes(-30);
        DateTime dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);

        //string StrSHr = strStartTime.Substring(0, 2);
        //int iShr = Convert.ToInt32(StrSHr);
        //string StrEhr = Convert.ToDateTime(strEndTime).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
        //int iEhr = Convert.ToInt32(StrEhr);
        //if ((iShr > 7 & iEhr < 8))
        //{
        //    dtPrEndTime = Convert.ToDateTime("23:59:00");
        //}
        //else
        //{
        dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration);
        //  }

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day, cs.Productgroupcode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" + ddlYear.SelectedValue + " and cs.Accepted='Y' order by Time";

        Double iDurationBasedOnPG = 0;


        try
        {
            string vRooms = string.Empty;


            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        try
                        {
                            dtStarttime = Convert.ToDateTime(dr["Begin"].ToString());
                            if (dr["ProductGroupCode"].ToString() == "COMP")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "GB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "LSP")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "MB")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SAT")
                            {
                                iDurationBasedOnPG = 1.5;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "SC")
                            {
                                iDurationBasedOnPG = 2;
                            }
                            else if (dr["ProductGroupCode"].ToString() == "UV")
                            {
                                iDurationBasedOnPG = 1;
                            }
                            //string StrSHr1 = Convert.ToDateTime(dr["Begin"].ToString()).ToString().Substring(0, 2);
                            //int iShr1 = Convert.ToInt32(StrSHr1);
                            //string StrEhr1 = Convert.ToDateTime(dr["Begin"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                            //int iEhr1 = Convert.ToInt32(StrEhr1);
                            //if ((iShr1 > 7 & iEhr1 < 8))
                            //{
                            //    dtEndTime = Convert.ToDateTime("23:59:00");
                            //}
                            //else
                            //{
                            dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                            //    }

                            // dtEndTime = Convert.ToDateTime(dr["Begin"].ToString()).AddHours(iDurationBasedOnPG);
                            string Meetingday = dr["Day"].ToString();
                            string meetVroom = dr["Vroom"].ToString();




                            if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                            {


                                vRooms += dr["VRoom"].ToString() + ",";
                            }
                        }
                        catch
                        {
                        }

                        //vRooms += dr["VRoom"].ToString() + ",";
                    }
                }
            }



            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" + ddlYear.SelectedValue + " and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by VC.BeginTime";
            try
            {
                DataSet dsSingle = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            try
                            {
                                dtStarttime = Convert.ToDateTime(dr["BeginTime"].ToString());
                                if (dr["ProductGroupCode"].ToString() == "COMP")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "GB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "LSP")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "MB")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SAT")
                                {
                                    iDurationBasedOnPG = 1.5;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "SC")
                                {
                                    iDurationBasedOnPG = 2;
                                }
                                else if (dr["ProductGroupCode"].ToString() == "UV")
                                {
                                    iDurationBasedOnPG = 1;
                                }
                                //string StrSHr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).ToString().Substring(0, 2);
                                //int iShr1 = Convert.ToInt32(StrSHr1);
                                //string StrEhr1 = Convert.ToDateTime(dr["BeginTime"].ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2);
                                //int iEhr1 = Convert.ToInt32(StrEhr1);
                                //if ((iShr1 > 7 & iEhr1 < 8))
                                //{
                                //    dtEndTime = Convert.ToDateTime("23:59:00");
                                //}
                                //else
                                //{
                                dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);
                                // }
                                // dtEndTime = Convert.ToDateTime(dr["BeginTime"].ToString()).AddHours(iDurationBasedOnPG);

                                string Meetingday = dr["Day"].ToString();
                                string meetVroom = dr["Vroom"].ToString();

                                if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == dr["Day"].ToString())
                                {


                                    vRooms += dr["VRoom"].ToString() + ",";
                                }

                                //if ((dtPrStartTime >= dtStarttime || dtPrEndTime <= dtEndTime) && (strStartDay == dr["Day"].ToString()))
                                //{
                                //    Vroom objVroom = new Vroom();
                                //    objVroom.Day = dr["Day"].ToString();
                                //    objVroom.HostId = dr["HostId"].ToString();
                                //    ObjVroomList.Add(objVroom);

                                //    vRooms += dr["VRoom"].ToString() + ",";
                                //}
                            }
                            catch
                            {
                            }

                            // vRooms += dr["VRoom"].ToString() + ",";
                        }
                    }
                }
            }
            catch
            {
            }

            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Duration,VC.Time, VC.Date from  WebConfSessions VC  inner join VirtualRoomLookup VL on(VL.Vroom=VC.Vroom) where VC.Year=" + ddlYear.SelectedValue + " and Date>=getDate() order by VC.Time";
            try
            {
                DataSet dsSingle = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
                if (dsSingle.Tables.Count > 0)
                {
                    if (dsSingle.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in dsSingle.Tables[0].Rows)
                        {
                            try
                            {
                                dtStarttime = Convert.ToDateTime(dr["Time"].ToString());

                                iDurationBasedOnPG = Convert.ToInt32(dr["Duration"].ToString());

                                dtEndTime = Convert.ToDateTime(dr["Time"].ToString()).AddMinutes(iDurationBasedOnPG);

                                string Meetingday = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                                string meetVroom = dr["Vroom"].ToString();

                                if ((dtStarttime >= dtPrStartTime || dtStarttime <= dtPrStartTime) && dtStarttime <= dtPrEndTime && dtEndTime >= dtPrStartTime && (dtEndTime <= dtPrEndTime || dtEndTime >= dtPrEndTime) && strStartDay == Meetingday)
                                {


                                    vRooms += dr["VRoom"].ToString() + ",";
                                }


                            }
                            catch
                            {
                            }

                        }
                    }
                }
            }
            catch
            {
            }


            if (!string.IsNullOrEmpty(vRooms))
            {
                vRooms = vRooms.TrimEnd(',');
            }

            if (!string.IsNullOrEmpty(vRooms))
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom not in (" + vRooms + ") and Vroom in (25,26,27) order by vroom desc";
            }
            else
            {
                cmdtext = "select HostID, USerID, PWD, vroom from VirtualRoomLookUp where Vroom in (25,26,27) order by vroom desc";
            }

            DataSet dsVroom = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdtext);
            if (dsVroom.Tables.Count > 0)
            {
                if (dsVroom.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in dsVroom.Tables[0].Rows)
                    {
                        hostID = dr["HostID"].ToString();
                        hdnVroom.Value = dr["Vroom"].ToString();
                    }
                }
                else
                {
                    hostID = "";
                }
            }

            // hostID = "";
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Practice Session");
        }
        return hostID;
    }

    public int validateCreateZoom()
    {
        int retVal = 1;

        if (ddlVolunteer.SelectedValue == "-1")
        {
            retVal = -1;
            lblErr.Text = "Please select Volunteer";
            lblErr.ForeColor = Color.Red;
        }

        else if (ddlRole.SelectedValue == "-1")
        {
            retVal = -1;
            lblErr.Text = "Please select Role";
            lblErr.ForeColor = Color.Red;
        }
        //else if (ddlteacher.SelectedItem.Value == "-1")
        //{
        //    retVal = -1;
        //    lblErr.Text = "Please select teacher";
        //    lblErr.ForeColor = Color.Red;
        //}
        else if (ddlworkshopDate.Text == "")
        {
            retVal = -1;
            lblErr.Text = "Please enter date";
            lblErr.ForeColor = Color.Red;

        }
        else if (ddlTime.SelectedValue == "0")
        {
            retVal = -1;
            lblErr.Text = "Please select time";
            lblErr.ForeColor = Color.Red;
        }
        else if (ddlDuration.SelectedValue == "0")
        {
            retVal = -1;
            lblErr.Text = "Please select duration";
            lblErr.ForeColor = Color.Red;
        }
        else if (ddlworkshopDate.Text != "-1")
        {
            try
            {
                string Date = Convert.ToDateTime(ddlworkshopDate.Text).ToShortDateString();
                DateTime dtDate = Convert.ToDateTime(Date);
                string todayDate = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                DateTime dtToday = Convert.ToDateTime(todayDate);
                if (dtDate < dtToday)
                {
                    retVal = -1;
                    lblErr.Text = "Date should be greater than today date.";
                    lblErr.ForeColor = Color.Red;
                }
            }
            catch
            {
            }
        }
        else if (RbtnRecurring.Checked == true)
        {
            if (DDLRecurrinType.SelectedValue == "0")
            {
                retVal = -1;
                lblErr.Text = "Please select Recurring Type.";
                lblErr.ForeColor = Color.Red;
            }
            if (TxtEndDate.Text == "")
            {
                retVal = -1;
                lblErr.Text = "Please select End Date.";
                lblErr.ForeColor = Color.Red;
            }
        }
        if (ddlCoHost.SelectedValue != "-1")
        {
            if (ddlCoHost.SelectedValue == ddlVolunteer.SelectedValue)
            {
                retVal = -1;
                lblErr.Text = "Host and CO-Host could not be same.";
                lblErr.ForeColor = Color.Red;
            }
        }
        return retVal;
    }


    public void getmeetingIfo(string sessionkey, string HostID)
    {
        try
        {
            string URL = string.Empty;
            string service = "5";
            URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + sessionkey + "";
            urlParameter += "&host_id=" + HostID + "";

            makeZoomAPICall(urlParameter, URL, service);
        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }

    }
    protected void gvOnlineWkShopZoom_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Join")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                gvOnlineWkShopZoom.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string sessionKey = string.Empty;
                sessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblStSessionkey") as Label).Text;
                string beginTime = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblTime") as Label).Text;
                string day = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblDate") as Label).Text;
                string hostID = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblStHostID") as Label).Text;
                string Date = Convert.ToDateTime(day).ToShortDateString();
                DateTime dtDate = Convert.ToDateTime(Date);
                string todayDate = Convert.ToDateTime(DateTime.Now).ToShortDateString();
                DateTime dtToday = Convert.ToDateTime(todayDate);

                DateTime dtFromS = new DateTime();
                DateTime dtEnds = DateTime.Now;
                double mins = 40.0;
                if (DateTime.TryParse(beginTime, out dtFromS))
                {
                    TimeSpan TS = dtFromS - dtEnds;
                    mins = TS.TotalMinutes;

                }
                string today = DateTime.Now.DayOfWeek.ToString();
                if (mins <= 15 && dtDate == dtToday)
                {
                    getmeetingIfo(sessionKey, hostID);
                    string cmdText = "";
                    // cmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + "";

                    //  SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

                    string meetingLink = hdnHostURL.Value;

                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                }
                else
                {
                    //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting()", true);
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "showmsg()", true);
                }
            }
            else if (e.CommandName == "Modify")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                gvOnlineWkShopZoom.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string ConfId = string.Empty;

                string SessionKey = string.Empty;
                string HostId = string.Empty;
                ConfId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblWebConfId") as Label).Text;
                SessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                HostId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblHostId") as Label).Text;

                ConfId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblWebConfId") as Label).Text;
                string Date = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string Duration = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnDuration.Value = Duration;

                hdnHostId.Value = HostId;
                hdnSessionKey.Value = SessionKey;
                hdnConfId.Value = ConfId;
                btnCreateMeeting.Text = "Update Session";

                btnClear.Text = "Cancel";

                string Cmdtext = "select * from WebConfSessions where ConfId=" + ConfId + "";
                DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
                if (ds.Tables.Count > 0)
                {
                    if (ds.Tables[0].Rows.Count > 0)
                    {
                        string Memberid = ds.Tables[0].Rows[0]["memberid"].ToString();
                        string RoleId = ds.Tables[0].Rows[0]["RoleId"].ToString();
                        string Year = ds.Tables[0].Rows[0]["Year"].ToString();
                        string TeamId = ds.Tables[0].Rows[0]["TeamId"].ToString();
                        string EventId = ds.Tables[0].Rows[0]["EventId"].ToString();
                        Date = Convert.ToDateTime(ds.Tables[0].Rows[0]["Date"].ToString()).ToString("MM/dd/yyyy");
                        Time = ds.Tables[0].Rows[0]["Time"].ToString();
                        Duration = ds.Tables[0].Rows[0]["Duration"].ToString();
                        string SessionType = ds.Tables[0].Rows[0]["SessionType"].ToString();

                        string EndDate = string.Empty;
                        if (ds.Tables[0].Rows[0]["EndDate"] != null)
                        {
                            EndDate = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        string RecurringType = ds.Tables[0].Rows[0]["RecurringType"].ToString();
                        FillVolunteer();
                        ddlVolunteer.SelectedValue = Memberid;
                        FillRoles();
                        ddlRole.SelectedValue = RoleId;
                        string Topic = ddlVolunteer.SelectedItem.Text.Split('-')[0].Trim() + "-" + ddlRole.SelectedItem.Text;
                        FillTeam();
                        ddlteam.SelectedValue = TeamId;
                        FillEvent();
                        ddlEvent.SelectedValue = EventId;

                        if (SessionType == "2")
                        {
                            RbtnSingle.Checked = true;
                            RbtnRecurring.Checked = false;
                        }
                        else if (SessionType == "3")
                        {
                            RbtnRecurring.Checked = true;
                            RbtnSingle.Checked = false;
                            dvRecurringType.Visible = true;
                            dvEndDate.Visible = true;
                            TxtEndDate.Text = EndDate;
                            DDLRecurrinType.SelectedItem.Text = RecurringType;

                        }
                        //ddlMeetingType.SelectedValue = SessionType;
                        ddlworkshopDate.Text = Date;
                        ddlTime.SelectedValue = Time;
                        ddlDuration.SelectedValue = Duration;


                    }
                }
            }
            else if (e.CommandName == "DeleteSession")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
                int selIndex = row.RowIndex;
                gvOnlineWkShopZoom.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string ConfId = string.Empty;
                string SessionKey = string.Empty;
                string HostId = string.Empty;
                ConfId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblWebConfId") as Label).Text;
                SessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblSessionKey") as Label).Text;
                HostId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblHostId") as Label).Text;
                hdnHostId.Value = HostId;
                hdnSessionKey.Value = SessionKey;
                hdnConfId.Value = ConfId;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmDelete();", true);
            }
            else if (e.CommandName == "invite")
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "popUp();", true);
            }
            else if (e.CommandName == "AddAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                string Hostkey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblHostKey") as Label).Text;
                hdnHostId.Value = Hostkey;
                hdnEventId.Value = EventId;
                hdnMeetingKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                hdnJoinURl.Value = JoinUrl;
                dvAttendee.Visible = true;
                spnAttendee.InnerText = "Attendee";
            }
            else if (e.CommandName == "ViewAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string JoinUrl = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblJoinURL") as Label).Text;
                hdnEventId.Value = EventId;
                hdnMeetingKey.Value = SessionKey;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnMemberId.Value = CMemberId;
                hdnJoinURl.Value = JoinUrl;

                //dvAttendee.Visible = true;

                ZoomAttendee();
                PopulateCoHost();
            }
            else if (e.CommandName == "AddHost")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                string EventId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttEventId") as Label).Text;
                string CMemberId = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblCMemberID") as Label).Text;
                string Date = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttDate") as Label).Text;
                string Time = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttTime") as Label).Text;
                string SessionKey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblAttSessionKey") as Label).Text;
                string HostURL = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblHostURL") as Label).Text;
                string Duration = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblDuration") as Label).Text;
                string Hostkey = ((Label)gvOnlineWkShopZoom.Rows[selIndex].FindControl("lblHostKey") as Label).Text;
                hdnHostId.Value = Hostkey;
                hdnEventId.Value = EventId;
                hdnMeetingKey.Value = SessionKey;
                hdnSessionKey.Value = SessionKey;
                hdnDate.Value = Date;
                hdnTime.Value = Time;
                hdnDuration.Value = Duration;
                hdnMemberId.Value = CMemberId;
                hdnHostURL.Value = HostURL;
                dvAttendee.Visible = true;
                spnAttendee.InnerText = "Co-Host";
            }
        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }

    }
    public void ZoomAttendee()
    {
        string CmdText = " select IP.FirstName, IP.LastName, IP.FirstName +' '+IP.LastName as Name, IP.EMail, ZA.Date, ZA.Time, ZA.Year, ZA.JoinURL, case when WC.SessionType=2 then 'Single' when WC.SessionType=3 then 'Recurring' end as SessionType, IP1.FirstName +' '+Ip1.lastName as HostName, IP1. EMail as HostEmail, WC.Duration, ZA.AttendeeId, ZA.Sessionkey from ZoomAttendee ZA inner join WebConfSessions WC on (ZA.SessionKey=WC.SessionKey and ZA.CMemberId=WC.MemberId) inner join Indspouse IP on (IP.AutoMemberId=ZA.MemberId) inner join Indspouse IP1 on (IP1.AutoMemberId=WC.MemberId) where WC.SessionKey=" + hdnMeetingKey.Value + "";

        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
        if (ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                gvAttendee.DataSource = ds;
                gvAttendee.DataBind();
                dvAttendeeGrid.Visible = true;
                dvAttendeeSection.Visible = true;
                dvHostNameS.Visible = true;
                lblHostName.Text = "Host Name: " + ds.Tables[0].Rows[0]["HostName"].ToString();
                lblHostEmail.Text = "Host Email: " + ds.Tables[0].Rows[0]["HostEmail"].ToString();

                for (int i = 0; i < gvAttendee.Rows.Count; i++)
                {
                    if (ds.Tables[0].Rows[i]["Name"].ToString().Trim() == ddlAttendee.SelectedItem.Text.Trim())
                    {
                        gvAttendee.Rows[i].BackColor = Color.Azure;
                    }
                }
            }
            else
            {
                gvAttendee.DataSource = ds;
                gvAttendee.DataBind();
                lblAttendeeNoRecord.Text = "No record exists.";
                dvAttendeeSection.Visible = true;
                dvHostNameS.Visible = false;
            }
        }
    }
    protected void ddlVolunteer_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillRoles();
    }

    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {
        FillTeam();
        FillEvent();
    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void BtnDeleteMeeting_Click(object sender, EventArgs e)
    {
        if (DeleteMeeting() > 0)
        {
            FillZoomSessions();
            lblErr.Text = "Zoom session deleted successfully.";
            lblErr.ForeColor = Color.Green;
        }
    }
    public int DeleteMeeting()
    {
        int RetVal = 1;
        try
        {
            string URL = string.Empty;
            string service = "3";
            URL = "https://api.zoom.us/v1/meeting/delete";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&host_id=" + hdnHostId.Value + "";
            urlParameter += "&id=" + hdnSessionKey.Value + "";

            makeZoomAPICall(urlParameter, URL, service);
            string CmdText = " Delete from WebConfSessions where ConfId=" + hdnConfId.Value + "";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            CmdText = " Delete from WebConf_CoHost where SessionKey=" + hdnSessionKey.Value + " and MemberId=" + ddlCoHost.SelectedValue + "";
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        }

        catch (Exception ex)
        {
            RetVal = -1;
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }


        return RetVal;
    }

    [WebMethod]
    public static List<SendEmail> GetFromEmail(long MemberId)
    {

        int retVal = -1;
        List<SendEmail> ObjListEmail = new List<SendEmail>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();
            int EventYear = 2017;

            string cmdText = " select distinct Email from indspouse where AutoMemberId=" + MemberId + "";


            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SendEmail objEmail = new SendEmail();
                        objEmail.Email = dr["Email"].ToString();
                        ObjListEmail.Add(objEmail);
                    }
                }
            }

        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Create Zoom Sessions");
        }
        return ObjListEmail;
    }

    public class SendEmail
    {
        public string Email { get; set; }
    }

    protected void btnAddAttendee_Click(object sender, EventArgs e)
    {
        if (spnAttendee.InnerText == "Attendee")
        {
            AddAttendee();
        }
        else
        {

            AddCoHost();
        }
    }
    public void AddAttendee()
    {
        string AttendeeName = ddlAttendee.SelectedValue;
        if (AttendeeName == "-1")
        {
            lblErr.Text = "Please select Attendee Name.";
            lblErr.ForeColor = Color.Red;
        }
        else if (hdnMemberId.Value == ddlAttendee.SelectedValue)
        {
            lblErr.Text = "Please select other Attendee since  " + ddlAttendee.SelectedItem.Text + " is a  Host.";
            lblErr.ForeColor = Color.Red;
        }
        else
        {
            string DupText = "select count(*) from ZoomAttendee where SessionKey=" + hdnMeetingKey.Value + " and MemberId=" + ddlAttendee.SelectedValue + "";

            int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, DupText));
            if (count > 0)
            {
                lblErr.Text = "Attendee already exists.";
                lblErr.ForeColor = Color.Red;
            }
            else
            {


                string CmdText = " insert into ZoomAttendee (EventId, year, CMemberId, MemberId, Date, Time, SessionKey, JoinURL, CreatedDate, CreatedBy) values(" + hdnEventId.Value + ", " + ddlYear.SelectedValue + ", " + hdnMemberId.Value + "," + ddlAttendee.SelectedValue + ", '" + hdnDate.Value + "','" + hdnTime.Value + "', '" + hdnMeetingKey.Value + "', '" + hdnJoinURl.Value + "', GetDate(), " + Session["LoginId"].ToString() + ")";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                lblErr.Text = "Attendee added successfully!";
                lblErr.ForeColor = Color.Blue;
                dvAttendee.Visible = false;
                ZoomAttendee();
                string AttendeeEmail = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select email from Indspouse where AutoMemberId=" + ddlAttendee.SelectedValue + "").ToString();
                EmailContent(hdnMeetingKey.Value, hdnHostId.Value, AttendeeEmail);
            }
        }
    }
    protected void btnCancel_Click(object sender, EventArgs e)
    {
        dvAttendee.Visible = false;
    }

    protected void gvAttendee_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "DeleteAttendee")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                gvAttendee.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
                string SessionKey = ((Label)gvAttendee.Rows[selIndex].FindControl("lblSessionkey") as Label).Text;
                hdnMeetingKey.Value = SessionKey;
                string AttendeeId = ((Label)gvAttendee.Rows[selIndex].FindControl("lblAttendeeId") as Label).Text;
                hdnAttendeeId.Value = AttendeeId;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteMeeting();", true);
            }
        }
        catch
        {

        }
    }

    protected void BtnDelete_Click(object sender, EventArgs e)
    {
        string CmdText = string.Empty;
        CmdText = "Delete from ZoomAttendee where SessionKey='" + hdnMeetingKey.Value + "' and AttendeeId=" + hdnAttendeeId.Value + "";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        lblErr.Text = "Attendee removed from the zoom session successfully.";
        ZoomAttendee();
    }



    //protected void ddlMeetingType_SelectedIndexChanged(object sender, EventArgs e)
    //{
    //    if (ddlMeetingType.SelectedValue == "3")
    //    {
    //        dvEndDate.Visible = true;
    //        dvRecurringType.Visible = true;
    //    }
    //    else if (ddlMeetingType.SelectedItem.Value == "2")
    //    {
    //        dvEndDate.Visible = false;
    //        dvRecurringType.Visible = false;
    //    }
    //}
    public void AddCoHost()
    {
        string AttendeeName = ddlAttendee.SelectedValue;
        if (AttendeeName == "-1")
        {
            lblErr.Text = "Please select Co-Host Name.";
            lblErr.ForeColor = Color.Red;
        }
        else if (hdnMemberId.Value == ddlAttendee.SelectedValue)
        {
            lblErr.Text = "Please select other Co-Host since  " + ddlAttendee.SelectedItem.Text + " is a  Host.";
            lblErr.ForeColor = Color.Red;
        }

        else
        {
            if (IsZoomAttendee() > 0)
            {
                lblErr.Text = "Please select other Co-Host since  " + ddlAttendee.SelectedItem.Text + " is already an attendee for this session.";
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                string DupText = "select count(*) from WebConf_CoHost where SessionKey=" + hdnMeetingKey.Value + " and MemberId=" + ddlAttendee.SelectedValue + "";

                int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, DupText));
                if (count > 0)
                {
                    lblErr.Text = "Duplicate exists.";
                    lblErr.ForeColor = Color.Red;
                }
                else
                {
                    //  if (CheckAvailabilityOfVolunteer() > 0)
                    // {
                    string CmdText = " insert into WebConf_CoHost (MemberId, SessionKey, HostURL, CreatedDate, CreatedBy) values(" + ddlAttendee.SelectedValue + ", '" + hdnSessionKey.Value + "', '" + hdnHostURL.Value + "', Getdate(), " + Session["LoginId"].ToString() + ")";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
                    lblErr.Text = "Co-Host added successfully!";
                    lblErr.ForeColor = Color.Blue;
                    dvAttendee.Visible = false;
                    //  }
                    // else
                    // {
                    //   lblErr.Text = "Co-Host is not available at this Date/Time.";
                    //  lblErr.ForeColor = Color.Red;
                    // }

                }
            }
        }
    }

    public void PopulateCoHost()
    {
        string CmdText = "";
        CmdText = "select IP.FirstName, IP.LAstName, IP.FirstName+' '+IP.LastName as Name, IP.Email, WS.Date, WS.Time, case when wS.sessionType=2 then 'Single' when wS.SessionType=3 then 'Recurring' end as SessionType, WS.Duration, WS.Year, WC.CoHostId, IP1.FirstName+' '+IP1.LastName as HostName, IP1.EMail as HostEMail from WebConf_CoHost WC inner join Indspouse IP on (WC.MemberId=IP.AutoMemberId) inner join WebConfSessions WS on (WS.SessionKey=WC.SessionKey) inner join Indspouse IP1 on (IP1.AutoMemberId=WS.MemberId) where WS.SessionKey=" + hdnSessionKey.Value + "";
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdCoHost.DataSource = ds;
                    GrdCoHost.DataBind();
                    dvHostName.Visible = true;
                    dvCoHost.Visible = true;
                    lblHostN.Text = "Host Name: " + ds.Tables[0].Rows[0]["HostName"].ToString();
                    lblHostE.Text = "Host Email: " + ds.Tables[0].Rows[0]["HostEMail"].ToString();
                    for (int i = 0; i < gvAttendee.Rows.Count; i++)
                    {
                        if (ds.Tables[0].Rows[i]["Name"].ToString().Trim() == ddlAttendee.SelectedItem.Text.Trim())
                        {
                            GrdCoHost.Rows[i].BackColor = Color.Azure;
                        }
                    }
                }
                else
                {
                    GrdCoHost.DataSource = ds;
                    GrdCoHost.DataBind();
                    lblCoHostNoRecord.Text = "No record exists.";
                    dvCoHost.Visible = true;
                    dvHostName.Visible = false;
                }
            }
        }
        catch (Exception ex)
        {

        }
    }

    public int IsZoomAttendee()
    {
        int Retval = 0;
        string CmdText = string.Empty;
        CmdText = "select count(*) from ZoomAttendee where SessionKey=" + hdnSessionKey.Value + " and memberId=" + ddlAttendee.SelectedValue + "";
        int count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, CmdText));
        Retval = count;
        return Retval;
    }

    protected void GrdCoHost_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "DeleteHost")
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);

                int selIndex = row.RowIndex;
                GrdCoHost.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");

                string coHostId = ((Label)GrdCoHost.Rows[selIndex].FindControl("lblCoHostId") as Label).Text;
                hdnCoHostId.Value = coHostId;
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteCoHost();", true);
            }
        }
        catch
        {

        }
    }

    protected void BtnDeletCoHost_Click(object sender, EventArgs e)
    {
        string CmdText = string.Empty;
        CmdText = "Delete from WebConf_CoHost where CoHostId='" + hdnCoHostId.Value + "'";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
        lblErr.Text = "Co-host removed from the zoom session successfully.";
        PopulateCoHost();
    }

    public int CheckAvailabilityOfVolunteer()
    {
        int Retval = 1;
        string CmdText = string.Empty;
        string Day = string.Empty;
        string Time = string.Empty;
        Double Duration = 0;
        CmdText = "select Date,Time, Duration from WebConfSessions where memberId=" + ddlAttendee.SelectedValue + "";
        DataSet ds = new DataSet();
        DataSet dsAttendee = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
            if (ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        Duration = Convert.ToDouble(dr["Duration"].ToString());
                        Day = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                        string MeetingDay = Convert.ToDateTime(hdnDate.Value).ToString("dddd");
                        string StartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + dr["Time"].ToString();
                        DateTime dtStartTime = Convert.ToDateTime(StartTime);
                        DateTime dtEndTime = Convert.ToDateTime(StartTime).AddMinutes(Duration);

                        string MeetingStartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + hdnTime.Value;
                        DateTime dtMeetingStartTime = Convert.ToDateTime(MeetingStartTime);
                        DateTime dtMeetingEndTime = Convert.ToDateTime(MeetingStartTime).AddMinutes(Convert.ToInt32(hdnDuration.Value));

                        if (Day == MeetingDay)
                        {
                            if (dtStartTime >= dtMeetingStartTime || dtStartTime <= dtMeetingEndTime || dtEndTime >= dtMeetingStartTime || dtEndTime <= dtMeetingEndTime)
                            {
                                Retval = -1;
                            }
                        }
                    }

                }
            }
            if (Retval == 1)
            {
                CmdText = "select WC.Date, WC.Time, WC.Duration from WebConfSessions Wc inner join ZoomAttendee ZA on (ZA.SessionKey=Wc.SessionKey) where ZA.MemberId=" + ddlAttendee.SelectedValue + "";
                dsAttendee = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, CmdText);
                if (dsAttendee.Tables.Count > 0)
                {
                    if (dsAttendee.Tables[0].Rows.Count > 0)
                    {
                        foreach (DataRow dr in ds.Tables[0].Rows)
                        {
                            Duration = Convert.ToDouble(dr["Duration"].ToString());
                            Day = Convert.ToDateTime(dr["Date"].ToString()).ToString("dddd");
                            string MeetingDay = Convert.ToDateTime(hdnDate.Value).ToString("dddd");
                            string StartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + dr["Time"].ToString();
                            DateTime dtStartTime = Convert.ToDateTime(StartTime);
                            DateTime dtEndTime = Convert.ToDateTime(StartTime).AddHours(Duration);

                            string MeetingStartTime = DateTime.Now.ToString("MM/dd/yyyy") + " " + hdnTime.Value;
                            DateTime dtMeetingStartTime = Convert.ToDateTime(MeetingStartTime);
                            DateTime dtMeetingEndTime = Convert.ToDateTime(MeetingStartTime).AddHours(Convert.ToInt32(hdnDuration.Value));

                            if (Day == MeetingDay)
                            {
                                if (dtStartTime >= dtMeetingStartTime || dtStartTime <= dtMeetingEndTime || dtEndTime >= dtMeetingStartTime || dtEndTime <= dtMeetingEndTime)
                                {
                                    Retval = -1;
                                }
                            }
                        }
                    }
                }
            }
        }
        catch
        {

        }
        return Retval;
    }

    public void EmailContent(string SessionKey, string HostId, string AttendeeEmail)
    {
        try
        {

            string URL = "https://api.zoom.us/v1/meeting/get";
            string urlParameter = string.Empty;

            urlParameter += "api_key=" + apiKey + "";
            urlParameter += "&api_secret=" + apiSecret + "";
            urlParameter += "&data_type=JSON";
            urlParameter += "&id=" + SessionKey + "";
            urlParameter += "&host_id=" + HostId + "";
            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameter.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameter);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();

            }

            var parser = new JavaScriptSerializer();

            dynamic obj = parser.Deserialize<dynamic>(postResponse);



            JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


            var serializer = new JavaScriptSerializer();
            SessionKey = json["id"].ToString();
            // hdnHostURL.Value = json["start_url"].ToString();

            string HostURL = json["start_url"].ToString();
            string joinUrl = json["join_url"].ToString();
            string Password = json["password"].ToString();
            string TimeZone = json["timezone"].ToString();

            string Topic = json["topic"].ToString();


            string Date = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select Date from WebConfSessions where SessionKey=" + SessionKey + "").ToString();
            string Time = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  Time  from WebConfSessions where SessionKey=" + SessionKey + "").ToString();

            string CoachName = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"].ToString(), CommandType.Text, "Select  IP.FirstName+' '+IP.LastName  from IndSpouse IP inner join WebConfSessions wc on (IP.AutoMemberId=WC.MemberId) where SessionKey=" + SessionKey + "").ToString();

            String strDate = Convert.ToDateTime(Date).ToString("MMM dd, yyyy");
            String strTime = Convert.ToDateTime(Time).ToString("hh:mm tt");

            string tblHtml = string.Empty;
            tblHtml += "<table>";
            tblHtml += "<tr><td><span>Hi there,</span><br /><br /></td></tr>";


            tblHtml += "<tr><td><span>" + CoachName + " is inviting you to a scheduled Zoom meeting</span><br /><br /></td></tr>";


            tblHtml += "<tr><td><span>Time: " + strDate + " " + strTime + " " + TimeZone + "(US and Canada)</span><br /><br /></td></tr>";

            tblHtml += "<tr><td><span>Join from PC, Mac, Linux, iOS or Android: " + joinUrl + "</span></td></tr>";
            tblHtml += "<tr><td><span>Password: " + Password + "</span><br /><br /></td></tr>";

            tblHtml += "<tr><td><span>Or iPhone one-tap :</span></td></tr>";
            tblHtml += "<tr><td><span>US: +16465588656,,550101276# or +16699006833,,550101276#</span></td></tr>";
            tblHtml += "<tr><td><span>Or Telephone:</span></td></tr>";
            tblHtml += "<tr><td><span>Dial(for higher quality, dial a number based on your current location):</span></td></tr>";
            tblHtml += "<tr><td><span>US: +1 646 558 8656 or +1 669 900 6833</span></td></tr>";
            tblHtml += "<tr><td><span>Meeting ID: " + SessionKey + "</span></td></tr>";
            tblHtml += "<tr><td><span>International numbers available: https://zoom.us/u/bg1A7g1o</span></br></td></tr>";


            tblHtml += "</table>";
            SendEmailToAttendee(tblHtml, AttendeeEmail);
        }
        catch
        {
        }
    }
    public void SendEmailToAttendee(string tblHtml, string AttendeeEmail)
    {
        string LoginEmail = Session["LoginEmail"].ToString();
        //AttendeeEmail = "michael.simson@capestart.com";
        //  volunteerEmail = "michael.simson@capestart.com;arul.raj@capestart.com,emim.lumina@capestart.com;";
        string volunteerEmail = AttendeeEmail.TrimEnd(';');
        string fromEMail = LoginEmail;
        string subject = "Zoom Invite";
        try
        {
            MailMessage mail = new MailMessage();
            mail.From = new MailAddress(fromEMail);
            mail.Body = tblHtml;
            mail.Subject = subject;

            // mail.CC.Add(new MailAddress("chitturi9@gmail.com"));

            String[] strEmailAddressList = null;
            String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
            Match EmailAddressMatch = default(Match);
            strEmailAddressList = volunteerEmail.Replace(',', ';').Split(';');
            foreach (object item in strEmailAddressList)
            {
                EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);

                if (EmailAddressMatch.Success)
                {

                    mail.Bcc.Add(new MailAddress(item.ToString().Trim()));

                }
            }

            // mail.Bcc.Add(new MailAddress("michael.simson@capestart.com".Trim()));
            SmtpClient client = new SmtpClient();
            // client.Host = host;
            mail.IsBodyHtml = true;
            try
            {
                client.Send(mail);

                //lblError.Text = ex.ToString();
            }
            catch (Exception ex)
            {
            }
        }
        catch
        {
        }
    }
    protected void RbtnRecurring_CheckedChanged(Object sender, EventArgs e)
    {
        if (RbtnRecurring.Checked == true)
        {
            RbtnRecurring.Checked = true;
            dvEndDate.Visible = true;
            dvRecurringType.Visible = true;
        }
        else if (RbtnSingle.Checked == true)
        {
            RbtnSingle.Checked = true;
            dvEndDate.Visible = false;
            dvRecurringType.Visible = false;

        }
    }
}



