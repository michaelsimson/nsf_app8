<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="BadgeNumbers.aspx.vb" Inherits="BadgeNumbers" title="Untitled Page" %>
<%@ Register TagPrefix="vs"  Namespace="Custom.Web.UI.WebControls"  Assembly="GroupRadioButton" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<script language="javascript" type="text/javascript">
function ValidateBothGridViews()
{
    var rdoChoice = false;     
    var alertMessage = "";
 
 var rbOriginal = document.getElementsByName("myrad");    
   
    for(i = 0; i < rbOriginal.length; i++)    
  {        
  if (rbOriginal[i].checked)        
  {            
  rdoChoice = true;              
  break;
  }
  }
  if(!rdoChoice)    
  {        
  alertMessage += "Please select one Radio button \n";           
  }
  if(alertMessage != "")    
  {        
  alert(alertMessage);        
  return false;    
  }    
  else    
  {        
  return true;    
  }
  }    
</script>

<table style="margin-left:400px; vertical-align:middle" >
<tr >
					<td class="Heading" style="text-align:center; vertical-align:middle">
					<asp:Label ID="lbltitle" runat="server" Text="Generate Badge Numbers" Height="43px"  ></asp:Label>
					</td>       
				</tr>
				</table>
				<%--<asp:Label ID="lbltitle" runat="server" Text="Generate Badge Numbers"  Height="43px" Font-Bold="True" Font-Size="Large" ForeColor="Blue"  ></asp:Label>--%>
				<table cellSpacing="0" cellPadding="10" border="0">
				<tr>
			    <td style="height: 25px">
			    <asp:Label ID="lblnotemsg" runat="server" Text="Note: You can generate badge numbers only after deadline for contest registration has passed."></asp:Label>
				</td>
				</tr>
				<tr>
				<td>
				<asp:Label ID="lblmsg" runat="server" Text="Label" Visible="false" ForeColor="Red"></asp:Label>
				</td>
</tr>
</table>

<br />
<asp:Label ID="lblbadgenumber" runat="server" BorderWidth="5px" BorderColor="white" Text="Badge numbers ready to be generated" ></asp:Label>
<br /><br />

<asp:GridView ID="grdbadgenumber" runat="server" AutoGenerateColumns="False" CellPadding="0" style="margin-left: 10px">
    <Columns>
    
    <asp:TemplateField>   
   <ItemTemplate>
   <vs:GroupRadioButton ID="MyRadioBadgenumbers" runat="server" GroupName="myrad" />  
     </ItemTemplate>
   </asp:TemplateField> 
   
   <asp:BoundField  ReadOnly="True" HeaderText="Chapter" 
                  DataField="chaptercode" >
       <ItemStyle ForeColor="Black" Width="150px" HorizontalAlign="Center" />      
       <HeaderStyle Font-Size="Small" ForeColor="Black" />
   </asp:BoundField>
   <asp:BoundField  ReadOnly="True" HeaderText="Contest"
                  DataField="contestcode" >
       <ItemStyle ForeColor="Black" Width="100px" HorizontalAlign="Center" />       
       <HeaderStyle Font-Size="Small" ForeColor="Black" />
    </asp:BoundField>
    
    </Columns>
    
    </asp:GridView>
 <br />
<table cellSpacing="0" cellPadding="10" border="0">
<tr> 
  <td style="height: 11px"> Contest Link </td> 
</tr>
<tr>
<%--<td> <%=rs.fields(1)%> </td>--%>
    <td> <asp:LinkButton ID="lnkbadge" runat="server" Text="GenerateBadgeNumbers" OnClientClick="javascript:return ValidateBothGridViews()" ></asp:LinkButton></td> 
 </tr>
</table>

<br />
<br />
<asp:Label ID="lblgenbadgenumber" runat="server" Text="Badge numbers had already been Generated" BorderWidth="5px" BorderColor="white"  ></asp:Label>
<br /><br />

               

<asp:GridView ID="grdgenbadgenumber" runat="server" zAutoGenerateColumns="False" CellPadding="0" style="margin-left: 10px" >
    <Columns>
    <asp:BoundField  ReadOnly="True" HeaderText="Chapter ID" 
                  DataField="chaptercode" >
       <ItemStyle ForeColor="Black" Width="150px" HorizontalAlign="Center" />      
        <HeaderStyle Font-Size="Small" ForeColor="Black" />
   </asp:BoundField>
   <asp:BoundField  ReadOnly="True"  HeaderText="Contest" 
                  DataField="contestcode" >
       <ItemStyle ForeColor="Black" Width="100px" HorizontalAlign="Center" />       
       <HeaderStyle Font-Size="Small" ForeColor="Black" />
    </asp:BoundField>
    </Columns>
     </asp:GridView>
     
     
<br />
<br />
<asp:Label ID="lblregdate" runat="server" Text="Deadline has not yet passed" BorderWidth="5px" BorderColor="white"  ></asp:Label>
<br /><br />
<asp:GridView ID="grdregdate" runat="server" AutoGenerateColumns="False" CellPadding="0" style="margin-left: 10px" >
    <Columns>
    <asp:BoundField  ReadOnly="True" HeaderText="Chapter ID" 
                  DataField="chaptercode" >
       <ItemStyle ForeColor="Black" Width="150px" HorizontalAlign="Center"/>      
        <HeaderStyle Font-Size="Small" ForeColor="Black" />
   </asp:BoundField>
   <asp:BoundField  ReadOnly="True"  HeaderText="Contest"
                  DataField="contestcode" >
       <ItemStyle ForeColor="Black" Width="100px" HorizontalAlign="Center" />       
       <HeaderStyle Font-Size="Small" ForeColor="Black" />
    </asp:BoundField>
    </Columns>
     </asp:GridView>
<br />

 <asp:HyperLink runat="server" ID="hlnkback" Text="Back" BorderWidth="5" BorderColor="white"  ></asp:HyperLink>
   
<%--<a href=javascript:history.back();> Back </a>--%>

</asp:Content>

