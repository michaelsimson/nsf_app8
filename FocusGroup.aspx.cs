﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
using System.Configuration;
using System.IO;
using System.Threading;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using NativeExcel;
using Microsoft.ApplicationBlocks.Data;


public partial class FocusGroup : System.Web.UI.Page
{
    public string ChapterIDs = string.Empty;
    protected void Page_Load(object sender, EventArgs e)
    {
        spnErrMsg.InnerText = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        //PopulateFocusGroup();
        if (!IsPostBack)
        {

            if (Session["RoleID"].ToString() == "1" || Session["RoleID"].ToString() == "2")
            {
                DDLChapter.Enabled = true;
                DDLParticipantsType.Enabled = true;
                DDLFrom.Enabled = true;
                DDLTarget.Enabled = true;
                DDLTo.Enabled = true;
                BtnSubmit.Enabled = true;
                FillCluster();
                fillChapter();
            }
            else
            {
                DDLChapter.Enabled = false;
                DDLParticipantsType.Enabled = false;
                DDLFrom.Enabled = false;
                DDLTarget.Enabled = false;
                DDLTo.Enabled = false;
                BtnSubmit.Enabled = false;
            }
        }
    }

    public void PopulateFocusGroup()
    {
        try
        {
            string tableTitle = "";
            string cmdText = "";
            DataSet ds = new DataSet();
            ChapterIDs = Session["ChapterIDs"].ToString();
            if (DDLParticipantsType.SelectedValue == "Partcipants")
            {
                if (DDLFrom.SelectedValue == "5 Years or more")
                {
                    cmdText = "select distinct AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State,C.ParentID,C.ChapterID Chapter,CH.ChapterCode from IndSpouse IP inner join Contestant C on (C.ParentID=IP.AutoMemberID)  inner join Chapter CH on (Ch.ChapterID=C.ChapterID) where C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-10) ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear)>=5 order by Chapter, LastName, FirstName";

                    tableTitle = "Table 1: 5 years or more in the last " + DDLTarget.SelectedValue + " years";
                }
                else
                {
                    cmdText = " select distinct AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode from IndSpouse IP inner join  Contestant C on (C.ParentID=IP.AutoMemberID) inner join Chapter CH on (Ch.ChapterID=C.ChapterID) WHERE C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-6) ";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear) between " + DDLFrom.SelectedValue + " and " + DDLTo.SelectedValue + "  order by Chapter, LastName, FirstName";

                    tableTitle = "Table 1: " + DDLFrom.SelectedValue + " to " + DDLTo.SelectedValue + " Years in the last " + DDLTarget.SelectedValue + " years";
                }
            }
            else
            {
                if (DDLScenario.SelectedValue == "1")
                {
                    cmdText = "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2012) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2013) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2014) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2013,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }


                    cmdText += "order by C.ContestYear,CH.ChapterCode,IP.LastName,IP.FirstName";

                    tableTitle = "Table 1: first time in 2014, not in 2015; first time in 2013, not 2015; first time in 2012, but not  in 2015";
                }
                else if (DDLScenario.SelectedValue == "2")
                {

                    cmdText = "select distinct parentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on(CH.ChapterID=C.ChapterID) where ParentID in(select ParentID from Contestant where PaymentReference is not null and ParentID in(select ParentID from Contestant where ContestYear in (2009,2010,2011,2012)) and ParentID not in( select ParentID from Contestant where ContestYear in (2013,2014))) and PaymentReference is not null and ContestYear in (2009,2010,2011,2012) ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by ParentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode having COUNT(distinct contestyear)=3  order by CH.ChapterCode, LastName,FirstName  ";

                    tableTitle = "table 1: 3 out of 6 years, but not in 2013 & 2014";
                }
                else if (DDLScenario.SelectedValue == "3")
                {

                    cmdText = " select distinct parentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on(CH.ChapterID=C.ChapterID) where ParentID in(select ParentID from Contestant where PaymentReference is not null and ParentID in(select ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011)) and ParentID not in( select ParentID from Contestant where ContestYear in (2012,2013,2014))) and PaymentReference is not null and ContestYear in (2005,2006,2007,2008,2009,2010,2011)   ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by ParentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode having COUNT(distinct contestyear)=4 order by CH.ChapterCode, LastName,FirstName";

                    tableTitle = "Table 1: 4 out of last 10 years, but not in 2012, 2013, 2014";
                }
            }

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    spnStatus.Visible = false;
                    BtnExportToExcel.Visible = true;
                    spnTitle.InnerText = tableTitle;
                    GrdFocusGroup.DataSource = ds;
                    GrdFocusGroup.DataBind();
                }
                else
                {
                    spnStatus.Visible = true;
                    GrdFocusGroup.DataSource = ds;
                    GrdFocusGroup.DataBind();
                }
            }
            else
            {
                spnStatus.Visible = true;
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void DDLParticipantsType_SelectedIndexChanged(object sender, EventArgs e)
    {
        try
        {

            if (DDLParticipantsType.SelectedValue == "Partcipants")
            {
                dvParticipants.Visible = true;
                dvNonParticipants.Visible = false;
                DDLFrom.SelectedValue = "0";
                DDLTo.SelectedValue = "0";
                DDLTarget.SelectedValue = "0";
            }
            else if (DDLParticipantsType.SelectedValue == "Non-Participants")
            {
                dvParticipants.Visible = false;
                dvNonParticipants.Visible = true;

            }
        }
        catch
        {
        }
    }
    protected void DDLFrom_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (DDLFrom.SelectedValue == "5 Years or more")
        {
            tdTo.Visible = false;
            tdToCtrl.Visible = false;
        }
        else
        {
            tdTo.Visible = true;
            tdToCtrl.Visible = true;
        }
    }
    protected void BtnSubmit_Click(object sender, EventArgs e)
    {
        if (validateFocusGroup() == "1")
        {
            GrdFocusGroup.Visible = true;
            GrdFocusGroup.PageIndex = 0;
            PopulateFocusGroup();
        }
        else
        {
            GrdFocusGroup.Visible = false;
            spnTitle.InnerText = "";
        }
    }

    public void fillChapter()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        cmdText = "select ChapterID, ChapterCode from Chapter order by State, ChapterCode";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                DDLChapter.DataValueField = "ChapterID";
                DDLChapter.DataTextField = "ChapterCode";
                DDLChapter.DataSource = ds;
                DDLChapter.DataBind();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ChapterIDs += dr["ChapterID"].ToString() + ",";
                }
            }
            else
            {
                DDLChapter.DataSource = ds;
                DDLChapter.DataBind();
            }

            DDLChapter.Items.Insert(0, new ListItem("All", "0"));
            DDLChapter.Items.Insert(0, new ListItem("Select", "-1"));
            Session["ChapterIDs"] = ChapterIDs.TrimEnd(',');

        }
    }
    protected void GrdFocusGroup_PageIndexChanging(object sender, GridViewPageEventArgs e)
    {
        GrdFocusGroup.PageIndex = e.NewPageIndex;
        PopulateFocusGroup();
    }

    public void ExportToExcel()
    {
        int CurrentYear = DateTime.Now.Year;
        string Year = Convert.ToString(DateTime.Now.Year);
        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        string FromVal = string.Empty;
        string tableName = string.Empty;
        string FileName = string.Empty;
        string headerTitle = string.Empty;
        string ChapterName = string.Empty;
        string ClusterName = string.Empty;
        try
        {
            string tableTitle = "";

            ChapterIDs = Session["ChapterIDs"].ToString();
            if (DDLParticipantsType.SelectedValue == "Partcipants")
            {
                if (DDLFrom.SelectedValue == "5 Years or more")
                {
                    cmdText = "select AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State,C.ParentID,C.ChapterID Chapter,CH.ChapterCode from IndSpouse IP inner join Contestant C on (C.ParentID=IP.AutoMemberID)  inner join Chapter CH on (Ch.ChapterID=C.ChapterID) where C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-10) ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear)>=5 order by Chapter, LastName, FirstName";

                    tableTitle = "Partcipants";
                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        FileName = "FocusGroup_None_" + ChapterName + "_Part_" + DDLFrom.SelectedValue.Replace(" ", "") + "_outof" + DDLTarget.SelectedValue + "";

                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_Part_" + DDLFrom.SelectedValue.Replace(" ", "") + "_outof" + DDLTarget.SelectedValue + "";
                    }
                    headerTitle = "Title:" + DDLFrom.SelectedValue + " in the last " + DDLTarget.SelectedValue + " years";

                }
                else
                {
                    cmdText = " select AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode from IndSpouse IP inner join  Contestant C on (C.ParentID=IP.AutoMemberID) inner join Chapter CH on (Ch.ChapterID=C.ChapterID) WHERE C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-6) ";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear) between " + DDLFrom.SelectedValue + " and " + DDLTo.SelectedValue + "  order by Chapter, LastName, FirstName";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");

                        ChapterName = ChapterName.Replace(" ", "");
                        //ChapterName = DDLChapter.SelectedItem.Text.Trim();


                        FileName = "FocusGroup_None_" + ChapterName + "_Part_" + DDLFrom.SelectedValue + "To" + DDLTo.SelectedValue + "_outof" + DDLTarget.SelectedValue + "";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");
                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_Part_" + DDLFrom.SelectedValue + "To" + DDLTo.SelectedValue + "_outof" + DDLTarget.SelectedValue + "";

                    }


                    tableTitle = "Partcipants";
                    headerTitle = "Title: " + DDLFrom.SelectedValue + " to " + DDLTo.SelectedValue + " Years in the last " + DDLTarget.SelectedValue + " years";

                }
            }
            else
            {
                if (DDLScenario.SelectedValue == "1")
                {
                    cmdText = "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2012) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2013,2014,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2013) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2014,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2014) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2013,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }


                    cmdText += "order by C.ContestYear,CH.ChapterCode,IP.LastName,IP.FirstName";


                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");

                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option1";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option1";
                    }


                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: first time in 2014, not in 2015; first time in 2013, not 2015; first time in 2012, but not  in 2015";
                }
                else if (DDLScenario.SelectedValue == "2")
                {


                    cmdText = "select distinct ParentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from Contestant C inner join IndSpouse IP on (IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ParentID in(select distinct parentID from contestant where ContestYear>(year(GetDate())-6) and ParentID not in(select ParentID from Contestant where ContestYear in (2013,2014)) and PaymentReference is not null  group by parentID having count(distinct ContestYear)=3) and ContestYear>(year(GetDate())-6)  and PaymentReference is not null   ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " order by CH.ChapterCode, IP.LastName,IP.FirstName ";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");

                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option2";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");
                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option2";
                    }

                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: 3 out of 6 years, but not in 2013 & 2014";
                }
                else if (DDLScenario.SelectedValue == "3")
                {

                    cmdText = "  select distinct AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from IndSpouse IP inner join Contestant C on (IP.AutoMemberID=C.ParentID) inner join Chapter CH on (C.ChapterID=CH.ChapterID) WHERE C.ContestYear>(year(GETDATE())-10) and  C.ContestYear not in (2012,2013,2014) and C.PaymentReference is not null   ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct C.ContestYear)=4 order by Chapter, LastName, FirstName";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");


                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option3";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option3";
                    }

                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: 4 out of last 10 years, but not in 2012, 2013, 2014";
                }
            }
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    ds.Tables[0].Columns.Add("Ser#").SetOrdinal(0);
                    for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                    {
                        ds.Tables[0].Rows[i]["Ser#"] = i + 1;

                    }
                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;
                    ds.Tables[0].TableName = tableTitle;

                    string filename = "" + FileName + "_" + monthDay + "_" + year + ".xls";
                    ExcelHelper.ToExcelWithHeader(ds, filename, Page.Response, headerTitle);
                }
            }
        }
        catch (Exception ex)
        {

        }
    }
    protected void BtnExportToExcel_Click(object sender, EventArgs e)
    {
        SaveExcelAll();
        //ExportToExcel();
    }

    public string validateFocusGroup()
    {
        string retval = "1";
        if (DDLParticipantsType.SelectedValue == "Partcipants")
        {
            if (DDLChapter.SelectedValue == "-1")
            {
                spnErrMsg.InnerText = "Please select Chapter";
                retval = "-1";
            }
            else if (DDLFrom.SelectedValue == "0")
            {
                spnErrMsg.InnerText = "Please select From (Years)";
                retval = "-1";
            }
            else if (DDLFrom.SelectedValue != "5 Years or more")
            {
                if (DDLTo.SelectedValue == "0")
                {
                    spnErrMsg.InnerText = "Please select To (Years)";
                    retval = "-1";
                }
                else if (DDLFrom.SelectedValue == DDLTo.SelectedValue)
                {
                    spnErrMsg.InnerText = "From (Years) and To (Years) should not be equal.";
                    retval = "-1";
                }
                else if (Convert.ToInt32(DDLFrom.SelectedValue) > Convert.ToInt32(DDLTo.SelectedValue))
                {
                    spnErrMsg.InnerText = "From (Years) value should be less than To (Years).";
                    retval = "-1";
                }
                else if (DDLTarget.SelectedValue == "0")
                {
                    spnErrMsg.InnerText = "Please select Target (Years)";
                    retval = "-1";
                }
            }
            else if (DDLTarget.SelectedValue == "0")
            {
                spnErrMsg.InnerText = "Please select Target (Years)";
                retval = "-1";
            }

        }
        else if (DDLParticipantsType.SelectedValue == "Non-Participants")
        {
            if (DDLChapter.SelectedValue == "-1")
            {
                spnErrMsg.InnerText = "Please select Chapter";
                retval = "-1";
            }
            else if (DDLScenario.SelectedValue == "0")
            {
                spnErrMsg.InnerText = "Please select Scenario.";
                retval = "-1";
            }
        }
        return retval;
    }

    public void FillCluster()
    {
        string cmdText = "";
        DataSet ds = new DataSet();
        cmdText = "select distinct Cl.ClusterID, CL.ClusterCode from Cluster CL inner join Chapter CH on(CL.ClusterID=CH.ClusterID) and Cl.ClusterID <>55 order by ClusterCode";
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                DDLCluster.DataValueField = "ClusterID";
                DDLCluster.DataTextField = "ClusterCode";
                DDLCluster.DataSource = ds;
                DDLCluster.DataBind();
            }
            else
            {
                DDLCluster.DataSource = ds;
                DDLCluster.DataBind();
            }
            DDLCluster.Items.Insert(0, new ListItem("Select One", "0"));
        }
    }
    protected void DDLCluster_SelectedIndexChanged(object sender, EventArgs e)
    {


        string cmdText = "";
        DataSet ds = new DataSet();
        if (DDLCluster.SelectedValue == "0")
        {
            cmdText = "select ChapterID, ChapterCode from Chapter order by State, ChapterCode ";
        }

        else
        {
            cmdText = "select ChapterID, ChapterCode from Chapter where ClusterID=" + DDLCluster.SelectedValue + " order by State, ChapterCode ";
        }
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
        if (ds.Tables[0] != null)
        {
            if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {

                DDLChapter.DataValueField = "ChapterID";
                DDLChapter.DataTextField = "ChapterCode";
                DDLChapter.DataSource = ds;
                DDLChapter.DataBind();

                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    ChapterIDs += dr["ChapterID"].ToString() + ",";
                }
            }
            else
            {
                DDLChapter.DataSource = ds;
                DDLChapter.DataBind();
            }

            if (DDLCluster.SelectedValue == "0")
            {
                DDLChapter.Items.Insert(0, new ListItem("All", "0"));
                DDLChapter.Items.Insert(0, new ListItem("Select", "-1"));
                DDLChapter.SelectedValue = "-1";
                DDLChapter.Enabled = true;
            }
            else
            {
                DDLChapter.Items.Insert(0, new ListItem("All", "0"));
                DDLChapter.SelectedValue = "0";
                DDLChapter.Enabled = false;
            }


            Session["ChapterIDs"] = ChapterIDs.TrimEnd(',');
        }
    }

    public void SaveExcelAll()
    {

        int CurrentYear = DateTime.Now.Year;
        string Year = Convert.ToString(DateTime.Now.Year);
        string cmdText = string.Empty;
        DataSet ds = new DataSet();
        string FromVal = string.Empty;
        string tableName = string.Empty;
        string FileName = string.Empty;
        string headerTitle = string.Empty;
        string ChapterName = string.Empty;
        string ClusterName = string.Empty;
        try
        {
            string tableTitle = "";

            ChapterIDs = Session["ChapterIDs"].ToString();
            if (DDLParticipantsType.SelectedValue == "Partcipants")
            {
                if (DDLFrom.SelectedValue == "5 Years or more")
                {
                    cmdText = "select distinct AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State,C.ParentID,C.ChapterID Chapter,CH.ChapterCode from IndSpouse IP inner join Contestant C on (C.ParentID=IP.AutoMemberID)  inner join Chapter CH on (Ch.ChapterID=C.ChapterID) where C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-10) ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear)>=5 order by Chapter, LastName, FirstName";

                    tableTitle = "Partcipants";
                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        FileName = "FocusGroup_None_" + ChapterName + "_Part_" + DDLFrom.SelectedValue.Replace(" ", "") + "_outof" + DDLTarget.SelectedValue + "";

                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_Part_" + DDLFrom.SelectedValue.Replace(" ", "") + "_outof" + DDLTarget.SelectedValue + "";
                    }
                    headerTitle = "Title:" + DDLFrom.SelectedValue + " in the last " + DDLTarget.SelectedValue + " years";

                }
                else
                {
                    cmdText = " select distinct AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode from IndSpouse IP inner join  Contestant C on (C.ParentID=IP.AutoMemberID) inner join Chapter CH on (Ch.ChapterID=C.ChapterID) WHERE C.PaymentReference is not null and C.ContestYear >=(year(GETDATE())-6) ";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += " group by AutoMemberID, FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, Ip.State, Chapter, C.ParentID,C.ChapterID,CH.ChapterCode having count(distinct ContestYear) between " + DDLFrom.SelectedValue + " and " + DDLTo.SelectedValue + "  order by Chapter, LastName, FirstName";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");

                        ChapterName = ChapterName.Replace(" ", "");
                        //ChapterName = DDLChapter.SelectedItem.Text.Trim();


                        FileName = "FocusGroup_None_" + ChapterName + "_Part_" + DDLFrom.SelectedValue + "To" + DDLTo.SelectedValue + "_outof" + DDLTarget.SelectedValue + "";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");
                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_Part_" + DDLFrom.SelectedValue + "To" + DDLTo.SelectedValue + "_outof" + DDLTarget.SelectedValue + "";

                    }


                    tableTitle = "Partcipants";
                    headerTitle = "Title: " + DDLFrom.SelectedValue + " to " + DDLTo.SelectedValue + " Years in the last " + DDLTarget.SelectedValue + " years";

                }
            }
            else
            {
                if (DDLScenario.SelectedValue == "1")
                {
                    cmdText = "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2012) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2013) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }

                    cmdText += "Union All ";

                    cmdText += "select distinct ParentID,AutoMemberID, ContestYear,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode,C.ContestYear from Contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on (CH.ChapterID=C.ChapterID) where ContestYear in(2014) and PaymentReference is not null and ParentID not in (select distinct ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011,2012,2013,2015,2016)) and PaymentReference is not null";

                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }


                    cmdText += "order by C.ContestYear,CH.ChapterCode,IP.LastName,IP.FirstName";


                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");

                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option1";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option1";
                    }


                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: first time in 2014, not in 2015; first time in 2013, not 2015; first time in 2012, but not  in 2015";
                }
                else if (DDLScenario.SelectedValue == "2")
                {

                    cmdText = "select distinct parentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on(CH.ChapterID=C.ChapterID) where ParentID in(select ParentID from Contestant where PaymentReference is not null and ParentID in(select ParentID from Contestant where ContestYear in (2009,2010,2011,2012)) and ParentID not in( select ParentID from Contestant where ContestYear in (2013,2014))) and PaymentReference is not null and ContestYear in (2009,2010,2011,2012) ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by ParentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode having COUNT(distinct contestyear)=3  order by CH.ChapterCode, LastName,FirstName  ";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");

                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option2";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");
                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option2";
                    }

                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: 3 out of 6 years, but not in 2013 & 2014";
                }
                else if (DDLScenario.SelectedValue == "3")
                {


                    cmdText = " select distinct parentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode from contestant C inner join IndSpouse IP on(IP.AutoMemberID=C.ParentID) inner join Chapter CH on(CH.ChapterID=C.ChapterID) where ParentID in(select ParentID from Contestant where PaymentReference is not null and ParentID in(select ParentID from Contestant where ContestYear in (2005,2006,2007,2008,2009,2010,2011)) and ParentID not in( select ParentID from Contestant where ContestYear in (2012,2013,2014))) and PaymentReference is not null and ContestYear in (2005,2006,2007,2008,2009,2010,2011)   ";
                    if (DDLChapter.SelectedValue == "0")
                    {
                        cmdText += " AND C.ChapterId in(" + ChapterIDs + ")";
                    }
                    else
                    {
                        cmdText += " AND C.ChapterId =" + DDLChapter.SelectedValue + "";
                    }
                    cmdText += " group by ParentID,AutoMemberID,FirstName, LastName, Gender,Email, HPhone, CPhone, IP.City, IP.State, Chapter,C.ParentID,C.ChapterID,CH.ChapterCode having COUNT(distinct contestyear)=4 order by CH.ChapterCode, LastName,FirstName";

                    if (DDLCluster.SelectedValue == "0")
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");


                        FileName = "FocusGroup_None_" + ChapterName + "_NonPart_Option3";
                    }
                    else
                    {
                        ChapterName = DDLChapter.SelectedItem.Text.Replace(",", "");
                        ChapterName = ChapterName.Replace(" ", "");
                        ClusterName = DDLCluster.SelectedItem.Text.Replace(",", "");
                        ClusterName = ClusterName.Replace(" ", "");

                        FileName = "FocusGroup_" + ClusterName + "_" + ChapterName + "_NonPart_Option3";
                    }

                    tableTitle = "Non-Partcipants";
                    headerTitle = "Title: 4 out of last 10 years, but not in 2012, 2013, 2014";
                }
            }
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {

                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();
                    oSheet1 = oWorkbooks.Worksheets.Add();
                    oSheet.Name = tableTitle;
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = headerTitle;
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "AutoMemberID";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "First Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Last Name";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "Gender";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "Email";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "HPhone";
                    oSheet.Range["G3"].Font.Bold = true;

                    oSheet.Range["H3"].Value = "CPhone";
                    oSheet.Range["H3"].Font.Bold = true;

                    oSheet.Range["I3"].Value = "City";
                    oSheet.Range["I3"].Font.Bold = true;

                    oSheet.Range["J3"].Value = "State";
                    oSheet.Range["J3"].Font.Bold = true;

                    oSheet.Range["K3"].Value = "Chapter";
                    oSheet.Range["K3"].Font.Bold = true;

                    if (DDLScenario.SelectedValue == "1")
                    {
                        oSheet.Range["L3"].Value = "Year";
                        oSheet.Range["L3"].Font.Bold = true;
                    }

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["AutoMemberID"];

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["FirstName"];

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["LastName"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["Gender"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["H" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                        CRange = oSheet.Range["I" + iRowIndex.ToString()];
                        CRange.Value = dr["City"];

                        CRange = oSheet.Range["J" + iRowIndex.ToString()];
                        CRange.Value = dr["State"];

                        CRange = oSheet.Range["K" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterCode"];

                        if (DDLScenario.SelectedValue == "1")
                        {
                            CRange = oSheet.Range["L" + iRowIndex.ToString()];
                            CRange.Value = dr["ContestYear"];
                        }

                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;

                    string filename = "" + FileName + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();



                }
            }


        }
        catch
        {
        }
    }
}