﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports System.Net.Mail
Partial Class ApproveDuplicateRegistration
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            If Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
            ElseIf (Session("RoleId").ToString() = "29") Then
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from Volunteer where RoleId=29 and [National]='Y' and MemberID = " & Session("LoginID")) < 1 Then
                    Response.Redirect("volunteerFunctions.aspx")
                End If
            Else
                Response.Redirect("volunteerFunctions.aspx")
            End If
            LoadAlldata()
        End If
        DGView.Visible = False
    End Sub
    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtUserId.Text = ""
        Loaddata()
    End Sub

    Private Sub Loaddata()
        clear()
        dgselected.SelectedIndex = -1
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        Dim drDuplicate As DataSet
        SQLStr = "SELECT  DupCnst.DuplicateContestantReg_id, C.ChapterCode, I.EMail,I.FirstName +' '+ I.LastName AS FatherName, Ch.FIRST_NAME +' '+ ch.LAST_NAME AS ChildName,P.Name as ProductName, DupCnst.ChapterID, "
        SQLStr = SQLStr & "DupCnst.ContestCode, DupCnst.Grade, DupCnst.ContestYear, DupCnst.ParentID, DupCnst.ChildNumber, DupCnst.Fee, DupCnst.ContestCategoryID, DupCnst.ContestID, DupCnst.EventId, DupCnst.EventCode, "
        SQLStr = SQLStr & "DupCnst.ProductGroupId, DupCnst.ProductGroupCode, DupCnst.ProductId, DupCnst.ProductCode, DupCnst.Contestant_ID, DupCnst.Status, DupCnst.CreatedBy, DupCnst.CreateDate "
        SQLStr = SQLStr & "FROM DuplicateContestantReg DupCnst "
        SQLStr = SQLStr & "Inner Join Child Ch ON Ch.ChildNumber = DupCnst.ChildNumber "
        SQLStr = SQLStr & "Inner Join Indspouse I On Ch.MemberID = I.AutoMemberid "
        SQLStr = SQLStr & "Inner Join Chapter C On C.ChapterID=DupCnst.ChapterID "
        SQLStr = SQLStr & "Inner Join Product P ON DupCnst.ProductID=P.ProductId WHERE DupCnst.Status='Pending' and DupCnst.ContestYear=Year(Getdate()) ORDER BY Ch.LAST_NAME, Ch.FIRST_NAME "
        Try
            drDuplicate = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
        Catch ex As Exception
            Response.Write(SQLStr)
        End Try

        Session("drDuplicate") = drDuplicate
        dgselected.DataSource = drDuplicate
        dgselected.DataBind()
        If dgselected.Items.Count > 0 Then
            lblErr.Text = ""
            dgselected.Visible = True
        Else
            dgselected.Visible = False
            lblErr.Text = "No Pending record found"
        End If
    End Sub
    Private Sub getMemberid()
        Try
            Dim memberid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where Email = '" & txtUserId.Text & "'")
            If memberid > 0 Then
                Loaddata(memberid)
            Else
                lblErr.Text = "Sorry EMail Doesn't Match with our database"
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnLoadChild_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Page.IsValid = True Then
            getMemberid()
        End If
    End Sub
    Private Sub Loaddata(ByVal Memberid As String)
        clear()
        dgselected.SelectedIndex = -1
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        SQLStr = "SELECT  DupCnst.DuplicateContestantReg_id, C.ChapterCode, I.EMail,I.FirstName +' '+ I.LastName AS FatherName, Ch.FIRST_NAME +' '+ ch.LAST_NAME AS ChildName,P.Name as ProductName, DupCnst.ChapterID, "
        SQLStr = SQLStr & "DupCnst.ContestCode, DupCnst.Grade, DupCnst.ContestYear, DupCnst.ParentID, DupCnst.ChildNumber, DupCnst.Fee, DupCnst.ContestCategoryID, DupCnst.ContestID, DupCnst.EventId, DupCnst.EventCode, "
        SQLStr = SQLStr & "DupCnst.ProductGroupId, DupCnst.ProductGroupCode, DupCnst.ProductId, DupCnst.ProductCode, DupCnst.Contestant_ID, DupCnst.Status, DupCnst.CreatedBy, DupCnst.CreateDate "
        SQLStr = SQLStr & "FROM DuplicateContestantReg DupCnst "
        SQLStr = SQLStr & "Inner Join Child Ch ON Ch.ChildNumber = DupCnst.ChildNumber "
        SQLStr = SQLStr & "Inner Join Indspouse I On Ch.MemberID = I.AutoMemberid "
        SQLStr = SQLStr & "Inner Join Chapter C On C.ChapterID=DupCnst.ChapterID "
        SQLStr = SQLStr & "Inner Join Product P ON DupCnst.ProductID=P.ProductId WHERE DupCnst.ParentID=" & Memberid & " AND DupCnst.Status='Pending'  and DupCnst.ContestYear=Year(Getdate())  ORDER BY Ch.LAST_NAME, Ch.FIRST_NAME "
        Try
            Dim drDuplicate As DataSet = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
            Session("drDuplicate") = drDuplicate
            dgselected.DataSource = drDuplicate
            dgselected.DataBind()
            If dgselected.Items.Count > 0 Then
                lblErr.Text = ""
                dgselected.Visible = True
            Else
                dgselected.Visible = False
                lblErr.Text = "No Duplicate Registration record found given Member Email"
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub dgselected_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Dim DuplicateContestantReg_id As Integer
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "View") Then
            DuplicateContestantReg_id = CInt(e.Item.Cells(0).Text)
            LoadDetails(DuplicateContestantReg_id)
        Else
            Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnEdit"), LinkButton)
            If (Not (lbtn) Is Nothing) Then
                DuplicateContestantReg_id = CInt(e.Item.Cells(0).Text)
                lblDuplicateContestantReg_ID.Text = DuplicateContestantReg_id
                trStatus.Visible = True
                'load  for Approve/Reject
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select count(C.Contestant_ID) from contestant C Inner Join DuplicateContestantReg D ON C.ContestID=D.ContestID where C.BadgeNumber is Not NULL and D.DuplicateContestantReg_ID = " & DuplicateContestantReg_id & "") > 0 Then
                    lblErr.Text = "BadgeNumber Issued Please Reject it."
                    ddlApproved.SelectedIndex = ddlApproved.Items.IndexOf(ddlApproved.Items.FindByValue("Rejected"))
                    ddlApproved.Enabled = False
                End If
            End If
        End If
    End Sub

    Private Sub LoadDetails(ByVal DuplicateContestantReg_id As Integer)

        Dim StrSQl As String = "SELECT Cnst.ChapterID, I.FirstName+' '+I.LastName as ParentName,Chd.FIRST_NAME +' '+ Chd.LAST_NAME as ChildName, Ch.ChapterCode, Cnst.Grade, Cnst.ContestYear, Cnst.ParentID, Cnst.ChildNumber, Cnst.BadgeNumber, Cnst.Score1, Cnst.Score2, Cnst.Score3, Cnst.Rank, Cnst.NationalInvitee,Cn.ContestDate,"
        StrSQl = StrSQl & " Cnst.Fee,P.name as ProductName, Cnst.PaymentDate, Cnst.PaymentMode, Cnst.PaymentReference, Cnst.PaymentNotes, Cnst.CreateDate "
        StrSQl = StrSQl & " FROM Contestant Cnst  Inner Join DuplicateContestantReg DCnstR on   Cnst.ContestYear = DCnstR.ContestYear "
        StrSQl = StrSQl & " AND   Cnst.ChildNumber = DCnstR.ChildNumber AND Cnst.ParentID =   DCnstR.ParentID  and  Cnst.ProductID = DCnstR.ProductID  and Cnst.EventID = DCnstR.EventID"
        StrSQl = StrSQl & " INNER JOIN Product p ON cnst.ProductId = P.ProductId Inner Join Chapter Ch On Cnst.ChapterID=Ch.ChapterID "
        StrSQl = StrSQl & " INNER JOIN IndSpouse I ON I.AutomemberiD = Cnst.parentID "
        StrSQl = StrSQl & " INNER JOIN Child Chd ON Chd.ChildNumber= Cnst.ChildNumber "
        StrSQl = StrSQl & " Inner Join Contest Cn On Cn.ContestID=Cnst.ContestID"
        StrSQl = StrSQl & " WHERE DuplicateContestantReg_id = " & DuplicateContestantReg_id & " And Cnst.PaymentReference Is Not null"
        Dim drDuplicate As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQl)
        DGView.DataSource = drDuplicate
        DGView.DataBind()
        If DGView.Items.Count > 0 Then
            DGView.Visible = True
        Else
            DGView.Visible = False
            lblErr.Text = "No Registration record found."
        End If
    End Sub

    Protected Sub dgselected_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs)
        dgselected.CurrentPageIndex = e.NewPageIndex
        dgselected.EditItemIndex = -1
        dgselected.DataSource = Session("drDuplicate")
        dgselected.DataBind()
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub

    Private Sub clear()
        trStatus.Visible = False
        lblDuplicateContestantReg_ID.Text = String.Empty
    End Sub

    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim sFrom As String = "nsfcontests@gmail.com"
        Dim mail As MailMessage = New MailMessage(sFrom, sMailTo, sSubject, sBody)
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        mail.IsBodyHtml = True
        Dim ok As Boolean = True
        Try
            client.Send(mail)
        Catch e As Exception
            ok = False
        End Try
    End Sub

    Protected Sub btnUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Swap data Here
        'Move from Contestant & Update the status in Status Field
        Dim strSQL As String
        If ddlApproved.SelectedValue = "Approved" Then
            strSQL = "INSERT INTO DuplicateContestantReg(ChapterID, ContestCode, ContestYear, ParentID, ChildNumber, Fee, ContestCategoryID, ContestID, EventId, EventCode, "
            strSQL = strSQL & " ProductGroupId, ProductGroupCode, ProductId, ProductCode, TempLink, Contestant_ID, PaymentDate, PaymentMode, PaymentReference, PaymentNotes, "
            strSQL = strSQL & " OldCreateDate, Status, CreatedBy, CreateDate,Grade)"
            strSQL = strSQL & " Select  Cn.ChapterID, Cn.ContestCode, Cn.ContestYear, Cn.ParentID, Cn.ChildNumber, Cn.Fee, Cn.ContestCategoryID, Cn.ContestID, Cn.EventId, Cn.EventCode, "
            strSQL = strSQL & " Cn.ProductGroupId, Cn.ProductGroupCode, Cn.ProductId, Cn.ProductCode,Cn. TempLink, Cn.Contestant_ID, Cn.PaymentDate, Cn.PaymentMode, Cn.PaymentReference, Cn.PaymentNotes, "
            strSQL = strSQL & " Cn.CreateDate, 'Duplicate' as Status," & Session("LoginID") & " as CreatedBy,GETDATE() as CreateDate,cn.Grade  from Contestant Cn Inner Join DuplicateContestantReg DCn on   Cn.ContestYear = DCn.ContestYear "
            strSQL = strSQL & " And Cn.ChildNumber = DCn.ChildNumber And Cn.ParentID = DCn.ParentID And Cn.ProductID = DCn.ProductID And Cn.EventID = DCn.EventID"
            strSQL = strSQL & " WHERE DuplicateContestantReg_id =" & lblDuplicateContestantReg_ID.Text & " And Cn.PaymentReference Is Not null;"
            strSQL = strSQL & " Delete from Contestant  WHERE contestant_id in (Select Cn.contestant_id from Contestant Cn Inner Join DuplicateContestantReg DCn on   Cn.ContestYear = DCn.ContestYear "
            strSQL = strSQL & " AND   Cn.ChildNumber = DCn.ChildNumber AND Cn.ParentID =   DCn.ParentID  and  Cn.ProductID = DCn.ProductID  and Cn.EventID = DCn.EventID"
            strSQL = strSQL & " WHERE DuplicateContestantReg_id =" & lblDuplicateContestantReg_ID.Text & " And Cn.PaymentReference Is Not null);"
            strSQL = strSQL & " Insert Into Contestant(ChapterID, ContestCode, Grade, ContestYear, ParentID, ChildNumber, Fee, ContestCategoryID, ContestID, EventId, EventCode, "
            strSQL = strSQL & " ProductGroupId, ProductGroupCode, ProductId, ProductCode, CreatedBy, CreateDate)"
            strSQL = strSQL & " SELECT    ChapterID, ContestCode, Grade, ContestYear, ParentID, ChildNumber, Fee, ContestCategoryID, ContestID, EventId, EventCode, "
            strSQL = strSQL & " ProductGroupId, ProductGroupCode, ProductId, ProductCode, CreatedBy, GETDATE()  FROM  DuplicateContestantReg "
            strSQL = strSQL & " WHERE DuplicateContestantReg_id =" & lblDuplicateContestantReg_ID.Text & ";"
            strSQL = strSQL & " Update DuplicateContestantReg set Status='Approved',ModifiedDate=GETDATE(), ModifiedBy=" & Session("LoginID") & ",Contestant_ID=(Select Max(Contestant_ID) from Contestant) where DuplicateContestantReg_id =" & lblDuplicateContestantReg_ID.Text
        Else
            strSQL = "Update DuplicateContestantReg set Status='Rejected',ModifiedDate=GETDATE(), ModifiedBy=" & Session("LoginID") & " where DuplicateContestantReg_id =" & lblDuplicateContestantReg_ID.Text
        End If
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSQL)
        'send emails to Parents
        Dim CustIndMail As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 I.Email from DuplicateContestantReg Dup Inner Join IndSpouse I ON I.AutoMemberID = Dup.ParentID where Dup.DuplicateContestantReg_id=" & lblDuplicateContestantReg_ID.Text)
        Dim Mailmsg As String
        If ddlApproved.SelectedValue = "Approved" Then
            Mailmsg = "Dear Sir<br>Your duplicate registration was approved. Now you can go ahead and pay to complete the registration.<br> NSF Admin"
        Else
            Mailmsg = "Dear Sir<br>Your duplicate registration was denied.<br> NSF Admin"
        End If

        SendEmail("NSF Contest - Duplicate registration was " & ddlApproved.SelectedValue, Mailmsg, CustIndMail)

        If txtUserId.Text.Length > 3 Then
            getMemberid()
        Else
            Loaddata()
        End If
        clear()
        lblErr.Text = "Updated Successfully"
    End Sub

    Private Sub LoadAlldata()
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim SQLStr As String
        Dim drDuplicate As DataSet
        SQLStr = "SELECT  DupCnst.DuplicateContestantReg_id, C.ChapterCode, I.EMail,I.FirstName +' '+ I.LastName AS FatherName, Ch.FIRST_NAME +' '+ ch.LAST_NAME AS ChildName,P.Name as ProductName, DupCnst.ChapterID, "
        SQLStr = SQLStr & "DupCnst.ContestCode, DupCnst.Grade, DupCnst.ContestYear, DupCnst.ParentID, DupCnst.ChildNumber, DupCnst.Fee, DupCnst.ContestCategoryID, DupCnst.ContestID, DupCnst.EventId, DupCnst.EventCode, "
        SQLStr = SQLStr & "DupCnst.ProductGroupId, DupCnst.ProductGroupCode,DupCnst.Status, DupCnst.ProductId, DupCnst.ProductCode, DupCnst.Contestant_ID, DupCnst.Status, DupCnst.CreatedBy, DupCnst.CreateDate "
        SQLStr = SQLStr & "FROM DuplicateContestantReg DupCnst "
        SQLStr = SQLStr & "Inner Join Child Ch ON Ch.ChildNumber = DupCnst.ChildNumber "
        SQLStr = SQLStr & "Inner Join Indspouse I On Ch.MemberID = I.AutoMemberid "
        SQLStr = SQLStr & "Inner Join Chapter C On C.ChapterID=DupCnst.ChapterID "
        SQLStr = SQLStr & "Inner Join Product P ON DupCnst.ProductID=P.ProductId WHERE DupCnst.Status in (" & IIf(ddlSelection.SelectedItem.Text = "Approved", "'Approved','Duplicate'", "'Rejected'") & ") and DupCnst.ContestYear=Year(Getdate()) ORDER BY  Ch.LAST_NAME, Ch.FIRST_NAME,DupCnst.ProductId,DupCnst.duplicatecontestantReg_id "
        Try
            drDuplicate = SqlHelper.ExecuteDataset(conn, CommandType.Text, SQLStr)
        Catch ex As Exception
            Response.Write(SQLStr)
        End Try
        Session("drALL") = drDuplicate
        DGALL.DataSource = drDuplicate
        DGALL.DataBind()
        If DGALL.Items.Count > 0 Then
            lblAllErr.Text = ""
            DGALL.Visible = True
        Else
            DGALL.Visible = False
            lblAllErr.Text = "No " & ddlSelection.SelectedItem.Text & " record found."
        End If
    End Sub
  
    Protected Sub ddlSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadAlldata()
    End Sub

    Protected Sub DGALL_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGALL.PageIndexChanged
        DGALL.CurrentPageIndex = e.NewPageIndex
        DGALL.EditItemIndex = -1
        DGALL.DataSource = Session("drALL")
        DGALL.DataBind()
    End Sub
End Class

