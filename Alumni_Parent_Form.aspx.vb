﻿Imports System.Data.SqlClient
Imports System.Net.Mail
Imports Microsoft.ApplicationBlocks.Data
Partial Class Alumni_Parent_Form
    Inherits System.Web.UI.Page
    'Dim conn As New SqlConnection("Data Source=sql.northsouth.org; Initial Catalog=northsouth_dev; User ID='northsouthdev'; Password='everykosamu';")
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Session("AlumniMemberID") Is Nothing Then
            Response.Redirect("Alumni_Login.aspx")
        End If
        If Not IsPostBack Then
            loadStates(Me.ddlState, Me.ddlCountry.SelectedValue)
        End If
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not Page.IsValid Then
            Return
            Exit Sub
        Else
            Dim state As String
            If ddlCountry.SelectedValue = "IN" Or ddlCountry.SelectedValue = "US" Then
                state = ddlState.SelectedItem.Text
                lblstate.Text = ""
            ElseIf txtState.Text.Length < 1 Then
                lblstate.Text = "*"
                Exit Sub
            Else
                lblstate.Text = ""
                state = txtState.Text
            End If
            Dim SQLStr As String
            Try
                SQLStr = "Insert INTO  Ind_Alumini_ParentDetails(AlumniMemberID,FatherName, MotherName,  PhoneNumber, Address1, Address2, Address3, City, State, ZipCode, Country,createDate) VALUES("
                SQLStr = SQLStr & Session("AlumniMemberID") & ",'" & txtFName.Text & "','" & txtMName.Text & "','" & txtHomePhoneInd.Text & "','" & txtAddress1Ind.Text & "','" & txtAddress2Ind.Text & "','" & txtAddress3Ind.Text & "','" & txtCityInd.Text & "','" & state
                SQLStr = SQLStr & "','" & txtZipInd.Text & "','" & ddlCountry.SelectedItem.Text & "',Getdate())"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLStr)
                clear()
                Response.Redirect("Alumni_Education_Form.aspx")
                lblErr.Text = "Inserted Successfully"
            Catch ex As Exception
                lblErr.Text = SQLStr.ToString() & "<br>" & ex.ToString()
            End Try
        End If
    End Sub
    Private Sub clear()
        txtFName.Text = String.Empty
        txtMName.Text = String.Empty
        txtHomePhoneInd.Text = String.Empty
        txtAddress1Ind.Text = String.Empty
        txtAddress2Ind.Text = String.Empty
        txtAddress3Ind.Text = String.Empty
        txtCityInd.Text = String.Empty
        txtState.Text = String.Empty
        txtState.Visible = False
        txtZipInd.Text = String.Empty
        ddlCountry.SelectedIndex = ddlCountry.Items.IndexOf(ddlCountry.Items.FindByValue("IN"))
        loadStates(Me.ddlState, Me.ddlCountry.SelectedValue)
    End Sub
    Protected Sub ddlCountry_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs)
        If loadStates(Me.ddlState, Me.ddlCountry.SelectedValue) Then
            txtState.Visible = False
        Else
            Me.ddlState.Visible = False
            Me.txtState.Visible = True
        End If
    End Sub
    Private Function loadStates(ByRef ddlControl As DropDownList, ByVal p_country As String) As Boolean
        Dim rtnValue As Boolean
        If p_country = "IN" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsIndiaStates As DataSet
            dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
            If dsIndiaStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsIndiaStates.Tables(0)
                ddlControl.DataTextField = dsIndiaStates.Tables(0).Columns("StateName").ToString
                ddlControl.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        ElseIf p_country = "US" Then
            ddlControl.Visible = True
            '*** Populate State DropDown
            Dim dsStates As DataSet

            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
            If dsStates.Tables.Count > 0 Then
                ddlControl.Items.Clear()
                ddlControl.DataSource = dsStates.Tables(0)
                ddlControl.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlControl.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlControl.DataBind()
                ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If
            rtnValue = True
        End If
        Return rtnValue
    End Function

    Protected Sub btncancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clear()
    End Sub
End Class



