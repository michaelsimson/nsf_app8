﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="MissingScores.aspx.cs" Inherits="MissingScores" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table align="center" width="100%">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>

        </tr>
        <tr>

            <td>
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Missing Scores</strong>
                </div>
            </td>
        </tr>
        <tr>

            <td></td>
        </tr>
        <tr align="center">

            <td>
                <asp:RadioButton ID="missRank" runat="server" Text="Missing Ranks" GroupName="missing" />
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:RadioButton ID="missScore" runat="server" Text="Missing Scores" GroupName="missing" />
            </td>
        </tr>
        <tr>
            <td align="center">

                <table visible="true">

                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td align="left">
                            <asp:DropDownList ID="ddlYear" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged">
                                <asp:ListItem Value="2">Chapter Contests</asp:ListItem>
                                <asp:ListItem Value="1">Finals</asp:ListItem>

                            </asp:DropDownList>
                        </td>
                        <td>Zone</td>
                        <td>
                            <asp:DropDownList ID="ddlZone" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlZone_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Cluster</td>
                        <td>
                            <asp:DropDownList ID="ddlCluster" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlCluster_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Chapter</td>
                        <td>
                            <asp:DropDownList ID="ddlChapter" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td align="center">

                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>
                    </tr>





                </table>



            </td>




        </tr>
    </table>

    <table>
        <tr align="center">
            <td>
                <div style="float: left">
                    <asp:Button ID="btnExport" runat="server" Text="Export to Excel" OnClick="btnExport_Click" Visible="false" />
                </div>
                <b>
                    <asp:Label ID="lblTable1" runat="server" Text=""></asp:Label><br />
                    <asp:Label ID="lblErr" runat="server" Visible="false" Text="Label"></asp:Label></b>
                <div id="divMS1" style="overflow: auto; height: 300px;" runat="server">
                    <asp:GridView ID="gvMissingScore" CellPadding="4" CellSpacing="2" AutoGenerateColumns="False" runat="server">
                        <Columns>
                            <asp:BoundField DataField="Year" HeaderText="Year"></asp:BoundField>
                            <asp:BoundField DataField="Event" HeaderText="Event"></asp:BoundField>
                            <asp:BoundField DataField="ChapterId" HeaderText="ChapterId"></asp:BoundField>
                            <asp:BoundField DataField="State" HeaderText="State"></asp:BoundField>
                            <asp:BoundField DataField="Chapter" HeaderText="Chapter"></asp:BoundField>
                            <asp:BoundField DataField="ProductGroup" HeaderText="ProductGroup"></asp:BoundField>
                            <asp:BoundField DataField="Product" HeaderText="Product"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </div>
            </td>
        </tr>
        <tr align="center">
            <td>
                <b>
                    <asp:Label ID="lblTable2" runat="server" Text=""></asp:Label><br />
                    <asp:Label ID="lblErr1" runat="server" Visible="false" Text="Label"></asp:Label></b>
                <div id="divMS2" style="overflow: auto; height: 200px; width: 912px;" runat="server">
                    <asp:GridView ID="gvMissingScore2" AutoGenerateColumns="True" runat="server" Width="886px"></asp:GridView>
                </div>
            </td>
        </tr>
    </table>
</asp:Content>
