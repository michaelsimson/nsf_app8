﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="AddUpdSpashPageContents.aspx.cs" Inherits="AddUpdSpashPageContents" MasterPageFile="~/NSFMasterPage.master" %>

<%@ Register Assembly="CKEditor.NET" Namespace="CKEditor.NET" TagPrefix="CKEditor" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <div>

        <script type="text/javaScript">
            function DeleteConfirm() {


                if (confirm("Are you sure want to delete?")) {

                    document.getElementById("<%=btnDeleteMeeting.ClientID%>").click();
                }
            }
        </script>
    </div>

    <asp:Button ID="btnDeleteMeeting" runat="server" Style="display: none;" OnClick="btnDeleteMeeting_Click" />

    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Add/Update Splash Page Contents
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <table align="center" style="width: 250px;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Year</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="ddlYear" runat="server" Width="100px" AutoPostBack="true" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="2015" Selected="True">2015</asp:ListItem>
                        <asp:ListItem Value="2016">2016</asp:ListItem>

                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>
            <tr>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Event</td>
                <td align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="100px" AutoPostBack="True">
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>
            <tr>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Content Type</td>
                <td align="left">
                    <asp:DropDownList ID="ddlContentType" runat="server" Width="125px">
                        <asp:ListItem Value="Coaches">Coaches</asp:ListItem>
                        <asp:ListItem Value="Parents/Students">Parents/Students</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>

            <tr>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Content Body</td>
                <td align="left">

                    <CKEditor:CKEditorControl ID="txtContentText" runat="server" Width="800px"></CKEditor:CKEditorControl>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>

            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" Visible="true" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </td>


            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <div style="clear: both;"></div>
        <center>
            <asp:Label ID="Label1" runat="server" ForeColor="Green" Font-Bold="true">Table 1: Coaching Contents</asp:Label></center>
        <div style="clear: both;"></div>

        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="Left" ID="GrdLevel" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 700px; margin-bottom: 10px; border: none;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdLevel_RowCommand">
            <Columns>
                <asp:TemplateField HeaderText="Ser#">
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action" HeaderStyle-Width="135px">
                    <ItemTemplate>
                        <asp:Button ID="BtnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteContent" />
                        <div style="display: none;">
                            <asp:Label runat="server" ID="lblContentID" Text='<%#DataBinder.Eval(Container.DataItem,"ContentID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblContent" Text='<%#DataBinder.Eval(Container.DataItem,"ContentText") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblContentType" Text='<%#DataBinder.Eval(Container.DataItem,"ContentType") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="EventYear" HeaderText="Event Year"></asp:BoundField>
                <asp:BoundField DataField="EventID" HeaderText="Event ID"></asp:BoundField>
                <asp:BoundField DataField="EventName" HeaderText="EventName"></asp:BoundField>
                <asp:BoundField DataField="ContentType" HeaderText="ContentType"></asp:BoundField>
                <asp:TemplateField HeaderText="Content Body">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblContentText" Text='<%#DataBinder.Eval(Container.DataItem,"ContentText") %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
            </Columns>
        </asp:GridView>
    </div>

    <input type="hidden" id="hdnContentID" runat="server" />
</asp:Content>
