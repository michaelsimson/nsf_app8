﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="TestSlidesPh2Projection_April28_2015.aspx.cs" Inherits="TestSlidesPh2Projection" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <input id="txtHidden" style="width: 28px" type="hidden" value="0" runat="server" /> 
   

    <script type="text/javascript">
        function answer(txt) {
            alert(txt)
        }
        var mwin = null;
        var awin = null;
        var pwin = null;
        var Ans;
        function MeaningWindow(word, meaning) {
            if (mwin != null) mwin.close()
            mwin = window.open('#', 'newWin2', 'toolbar=yes,location=yes,scrollbars=yes,status=yes,width=700,height=500')
            mwin.document.write("<html><head><title>Meaning</title></head><body bgcolor='#FFFFFF' text='#000000'><div align='left'><h1><font FACE='Verdana' SIZE='6' color='#990000'>Meaning for - " + word + "<br><br>" + meaning + "</font></h1></div></body></html>")
            mwin.document.close
        }

        function AnswerWindow(word, answer) {
   
            if (awin != null) awin.close()
            awin = window.open('#', 'newWin3', 'toolbar=yes,location=yes,scrollbars=yes,status=yes,width=600,height=300')
            awin.document.write("<html><head><title>Answer</title></head><body bgcolor='#FFFFFF' text='#000000'><div align='left'><h1><font FACE='Verdana' SIZE='6' color='#FF0000'>Answer for - " + word + "<br><br>" + answer + "</font></h1></div></body></html>")
            awin.document.close
        }


</script>
     <div><asp:LinkButton ID="lnkBtnPrev" Visible="true" runat="server" style="float:left;" Font-Underline="False" OnClick="lnkBtnPrev_Click" Font-Bold="True"><< Prev Word </asp:LinkButton>
		&nbsp;&nbsp;   
		<asp:LinkButton ID="lnkBtnNext" Visible="true" runat="server" style="float:right; margin-left: 32px;" Font-Underline="False" OnClick="lnkBtnNext_Click" Font-Bold="True">Next Word >></asp:LinkButton></div>
    <asp:Repeater ID="rSlideProjection" runat="server" OnItemDataBound="rSlideProjection_ItemBound" >
        <ItemTemplate>
   <P style='page-break-before: always'> &nbsp;
			<a NAME="word29"></a>
			<div align="center"><FONT FACE='Verdana' SIZE='9'><b>JV Group - I - PHASE  II</b></FONT></div>
			<br><br><br>
			<table width=100% border=0 cellspacing=10 cellpadding=10>
			<tr >
				<td style="width: 24%"> <FONT FACE="Verdana" SIZE="6"><b>Level: <%# DataBinder.Eval(Container.DataItem, "Level")%> </b> </td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b><asp:Label runat="server" ID="SNoStuPhase2" Text=""  ></asp:Label>.</b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%></b></td>
			</tr>
			<tr>
				<td style="width: 24%"></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>A. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "A")%> </b></td>
			</tr>
			<tr>
				<td style="width: 24%"></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>B. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "B")%> </b></td>
			</tr>
			<tr>
				<td style="width: 24%"></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>C. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "C")%> </b></td>
			</tr>
			<tr>
				<td style="width: 24%"> </td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>D. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "D")%> </b></td>
			</tr>
			
			<tr>
				<td style="width: 24%"></td>
				<td width=10%> <FONT FACE="Verdana" SIZE="6"><b>E. </b></td>
				<td width=70%> <FONT FACE="Verdana" SIZE="6"><b><%# DataBinder.Eval(Container.DataItem, "E")%> </b></td>
			</tr>
			
			</table>
			
				
			 &nbsp; &nbsp;&nbsp; &nbsp;
			<a href='javascript:MeaningWindow("<%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%>","<%# DataBinder.Eval(Container.DataItem, "Meaning")%>")'
				onMouseOver="window.status='status bar text';return true" onFocus="window.status='status bar text';return true"
				onMouseOut="window.status='status bar text';return true">Meaning</a>
            &nbsp; &nbsp;&nbsp; &nbsp;
			<a href='javascript:AnswerWindow("<%# DataBinder.Eval(Container.DataItem, "Word").ToString().ToUpper()%>","<%# DataBinder.Eval(Container.DataItem, "Answer")%>")'
				onMouseOver="window.status='status bar text';return true" onFocus="window.status='status bar text';return true"
				onMouseOut="window.status='status bar text';return true">Answer</a>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
			<br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br><br>
       </ItemTemplate>

	</asp:Repeater>
    
    </asp:content>
