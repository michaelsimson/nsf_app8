<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="Donor_pay_success.aspx.vb" Inherits="Donor_pay_success" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table  style="margin:50px"  cellspacing="0"   cellpadding="0" border="0">
<TR >
<td class="largewordingbold" style="height: 16px" align="left" ></td>
</TR>
<tr><td style="height: 64px" align="left"   > &nbsp;<br />
     <asp:Label ID="lblCurrDate" runat="server"></asp:Label><br />
<br />
</td>
</tr>

<tr>
<td class="largewordingbold" style="height: 48px" align="left">
       
<asp:Label ID="lbldonor" Text="Donor:" runat="server"></asp:Label>
<asp:Label ID="lblName" runat="server"></asp:Label><br />
<asp:Label ID="lblAddress1" runat="server"></asp:Label> 
<asp:Label ID="lblAddress2" runat="server"></asp:Label><br />
<asp:Label ID="lblCity" runat="server"></asp:Label>&sbquo;
<asp:Label ID="lblState" runat="server"></asp:Label>
<asp:Label ID="lblZip" runat="server"></asp:Label>
<br />
<br />
</td>
</tr>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lbldonorMessage" runat="server" Text="Dear donor," ></asp:Label>
<br />
<br />
</td>
</tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lblthanksmsg" runat="server" Text="Thank you for your generous contribution to the North South Foundation." ></asp:Label>
<br />
<br />
</td>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lbltxmsg" runat="server" ></asp:Label>
</td>
</tr>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lblpayment" runat="server" ></asp:Label>
</td>
</tr>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lblcontribution" runat="server" ></asp:Label>
<br />
<br />
</td>
</tr>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
<asp:Label ID="lblfamilymsg" runat="server" Text="You or your family did not receive any benefit from North South Foundation in return for this Contribution." ></asp:Label>
<br />
<br />
</td>
</tr>
<tr>
<td class ="largewordingbold" style="height: 16px" align="left">
If your company matches your donation to non-profit organizations, please obtain a matching gifts form from your employer,<br /> fill out the top portion and mail to north South Foundation.  By doing this, you double your contribution to the Foundation. 
<br />
<br />
</td>
</tr>
<tr>
<td class="largewordingbold" style="height: 16px" align="left"><asp:label id="lblMessage" runat="server"></asp:label></td>
				</tr>
<tr>
<td class="largewordingbold" style="height: 38px" align="left">
<asp:Label ID="lblEmailStatus" runat="server"></asp:Label>
<br />
<br />
</td>
</tr>

<%--<tr>
<td class="largewordingbold" align="left" style="height: 16px">
</td>
</tr>--%>
<tr>
<td class="largewordingbold" align="left">
<font color="red" size ="4"> Please do not use Back Button on your browser to avoid
duplicate charges on your credit card.
</font>
<br />
<br />
</td>
</tr>
    <tr>

<TD class="largewordingbold" align="left"></TD>
    </tr>
				
				<tr>
					<td class="ContentSubTitle" align="left" colSpan="2"></td>
				</tr>
				<tr>
				    <td align="left">
				        Once again, we thank you for your contribution. 
                        <br />
                        <br />
                        <br />
                        North South Foundation
                        <br />
                        2 Marissa Ct, Burr Ridge, IL 60527
                        <br />
                        Tax-ID: 36-3659998
                        <br />
                        Non-profit Organization under the 501(c)(3) Code.<br />
                        <a href="http://www.northsouth.org">http://www.northsouth.org</a>

				    </td>
				</tr>
			</table>
			
			<table width="100%">
				<tr>
					<TD class="largewordingbold" align="center"><FONT color="#cc3300" size="4"><STRONG></STRONG></FONT></TD>
					<td class="Content">
					</td>
				</tr>
				<tr>
					<td>
						<asp:LinkButton OnClick="LinkButton1_Click" ID="LinkButton1" runat="server">Back to Main Page</asp:LinkButton>
					</td>
				</tr>
			</table>

</asp:Content>

