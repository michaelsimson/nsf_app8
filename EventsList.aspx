<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.EventsList" CodeFile="EventsList.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>EventsList</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%">
				<tr>
					<td class="Heading" noWrap align="center" colSpan="2">
						Contest&nbsp;List
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right">Contest&nbsp;Year</td>
					<td vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlEventYear" runat="server" CssClass="SmallFont" AutoPostBack="True">
							<asp:ListItem>Select Contest Year</asp:ListItem>
							<asp:ListItem Value="1995">1995</asp:ListItem>
							<asp:ListItem Value="1996">1996</asp:ListItem>
							<asp:ListItem Value="1997">1997</asp:ListItem>
							<asp:ListItem Value="1998">1998</asp:ListItem>
							<asp:ListItem Value="1999">1999</asp:ListItem>
							<asp:ListItem Value="2000">2000</asp:ListItem>
							<asp:ListItem Value="2001">2001</asp:ListItem>
							<asp:ListItem Value="2002">2002</asp:ListItem>
							<asp:ListItem Value="2003">2003</asp:ListItem>
							<asp:ListItem Value="2004">2004</asp:ListItem>
							<asp:ListItem Value="2005">2005</asp:ListItem>
							<asp:ListItem Value="2006">2006</asp:ListItem>
							<asp:ListItem Value="2007">2007</asp:ListItem>
							<asp:ListItem Value="2008">2008</asp:ListItem>
							<asp:ListItem Value="2009">2000</asp:ListItem>
							<asp:ListItem Value="2010">2010</asp:ListItem>
							<asp:ListItem Value="2011">2011</asp:ListItem>
							<asp:ListItem Value="2012">2012</asp:ListItem>
							<asp:ListItem Value="2013">2013</asp:ListItem>
							<asp:ListItem Value="2014">2014</asp:ListItem>
							<asp:ListItem Value="2015">2015</asp:ListItem>
							<asp:ListItem Value="2016">2016</asp:ListItem>
							<asp:ListItem Value="2017">2017</asp:ListItem>
							<asp:ListItem Value="2018">2018</asp:ListItem>
							<asp:ListItem Value="2019">2019</asp:ListItem>
							<asp:ListItem Value="2020">2020</asp:ListItem>
						</asp:dropdownlist></td>
				</tr>
				<tr>
					<td colSpan="2" class="ContentSubTitle"><asp:datagrid id="dgContestList" runat="server" CssClass="GridStyle" Width="100%" AllowSorting="True"
							AutoGenerateColumns="False" BorderWidth="1px" CellPadding="4" DataKeyField="ContestID">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Contest" SortExpression="ContestDescription">
									<ItemTemplate>
										<asp:HyperLink id="hlEditEvent" runat="server"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Chapter Name" SortExpression="ChapterName">
									<ItemTemplate>
										<asp:Label id="lblContestCity" runat="server" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Type" SortExpression="ContestTypeName">
									<ItemTemplate>
										<asp:Label id=Label2 runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.ContestTypeName") %>' CssClass=SmallFont>
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Date" SortExpression="ContestDate">
									<ItemTemplate>
										<asp:Label id="lblContestDate" runat="server" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Contest Time" SortExpression="StartTime">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblEventTime" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="CheckIn Time" SortExpression="CheckinTime">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblCheckInTime" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText=" Registration Deadline" SortExpression="RegistrationDeadline">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblRegistrationDeadLine" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Building / Room" SortExpression="Building">
									<ItemTemplate>
										<asp:Label runat="server" ID="lblBuilding" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
							</Columns>
							<PagerStyle HorizontalAlign="Center" ForeColor="#330099" BackColor="#FFFFCC"></PagerStyle>
						</asp:datagrid>** Please Click the Link in the Contest Column in order to Edit 
						the Contest</td>
				</tr>
			</table>
			<div>
				<asp:HyperLink id="hlinkChapterFunctions" runat="server" NavigateUrl="ChapterMain.aspx">Back to Chapter Functions</asp:HyperLink>
			</div>
		</form>
	</body>
</HTML>

 

 
 
 