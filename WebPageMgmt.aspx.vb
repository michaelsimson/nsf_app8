﻿Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Web.UI.Page
Imports System.Text.RegularExpressions
Imports System.Data

'*********************  FERDINE SILVA  Oct 22, 2010 ***************************
'*  All RoleID with 1,2,3,4,5 have permission to access the webpagemgmt	      *
'*  RoleID 1 has access to all public, Private, etc (Root)              	  *
'*  RoleID 2 - 5 has the access to chapter Panel selection they come through  *
'*  RoleID 1-5 should not have any assignmant in VolDocAccess table    	      *
'*  Assigned volunteer cant be let to delete from Volunteer table.       	  *
'******************************************************************************

'dbo.ufn_getFolderURL function is used here
Partial Public Class WebPageMgmt
    Inherits System.Web.UI.Page
    'Implements System.Web.UI.ICallbackEventHandler
    Dim ServerPath As String = "d:\inetpub\wwwroot\northsouth\"
    Dim urlPath As String = "http://www.northsouth.org/"
    Protected returnValue As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not Page.IsPostBack Then
            'loadLevel(ddlLevel1, 1, "")
            loadMemberAccess()
            BtnDelete.Attributes.Add("onclick", "return confirm('Are sure you want to delete the file?');")
        End If
        'Response.Write()
    End Sub
    '' hide server side button
    '        btnSave.Style.Add(HtmlTextWriterStyle.Display, "none")
    'Dim cbReference As String = Page.ClientScript.GetCallbackEventReference(Me, "arg", "ReceiveServerData", "context", "processError", False)
    'Dim callbackScript As String = "function CallServer(arg, context) { " + cbReference + ";}"
    '        Page.ClientScript.RegisterClientScriptBlock(Me.GetType(), "CallServer", callbackScript, True)

    'Public Sub RaiseCallbackEvent(ByVal eventArgument As String) Implements System.Web.UI.ICallbackEventHandler.RaiseCallbackEvent
    '    'dummy processing
    '    System.Threading.Thread.Sleep(5000)
    '    'set return value
    '    returnValue = File.Exists(lblPath.Text & Path.GetFileName(eventArgument))
    'End Sub

    'Public Function GetCallbackResult() As String Implements System.Web.UI.ICallbackEventHandler.GetCallbackResult
    '    Return returnValue.ToString().ToLower()
    'End Function
    'Protected Sub btnSave_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSave.Click
    '    MsgBox("test")
    '    If FileUpLoad1.HasFile Then
    '        Dim fileExt As String
    '        Dim filecount As Integer = 0
    '        fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName)
    '        '.aspx, .doc, .docx, .xls, .xlsx, .pdf, .config, .txt, .zip, .zipx
    '        If fileExt.ToLower = ".csv" Or fileExt.ToLower = ".aspx" Or fileExt.ToLower = ".html" Or fileExt.ToLower = ".htm" Or fileExt.ToLower = ".jpg" Or fileExt.ToLower = ".jpeg" Or fileExt.ToLower = ".gif" Or fileExt.ToLower = ".png" Or fileExt.ToLower = ".doc" Or fileExt.ToLower = ".docx" Or fileExt.ToLower = ".xls" Or fileExt.ToLower = ".xlsx" Or fileExt.ToLower = ".pdf" Or fileExt.ToLower = ".config" Or fileExt.ToLower = ".txt" Or fileExt.ToLower = ".zip" Or fileExt.ToLower = "" Or fileExt.ToLower = ".zipx" Then
    '            Try
    '                'SELECT     TOP (200) FileListID, FolderListID, FileName, CreateDate, CreatedBy, ModifyDate, ModifiedBy FROM FileList
    '                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from FileList where FileName='" & FileUpLoad1.FileName & "' AND FolderListID=" & lblParentFID.Text & "") < 1 Then
    '                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into FileList(FolderListID, FileName, CreateDate, CreatedBy) Values(" & lblParentFID.Text & ",'" & FileUpLoad1.FileName & "',Getdate()," & Session("LoginID") & ")")
    '                Else
    '                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update FileList set ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & "  WHERE FileName='" & FileUpLoad1.FileName & "' AND FolderListID=" & lblParentFID.Text & "")
    '                End If
    '                FileUpLoad1.PostedFile.SaveAs(lblPath.Text & FileUpLoad1.FileName)
    '                lblError.Text = "file uploaded successfully"
    '                trfilelist.Visible = True
    '                trfileUpload.Visible = False
    '                loadfiles()
    '            Catch ex As Exception
    '                Response.Write("Insert into FileList(FolderListID, FileName, CreateDate, CreatedBy) Values(" & lblParentFID.Text & ",'" & FileUpLoad1.FileName & "',Getdate()," & Session("LoginID") & ")")
    '                lblError.Text = ex.ToString()
    '            End Try
    '        Else
    '            lblError.Text = "File Format not Accepted"
    '        End If
    '    Else
    '        lblError.Text = "No uploaded file"
    '    End If
    'End Sub

    Protected Sub ddlselectFolder_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlselectFolder.SelectedValue = "0" Then
            lblVErr.Text = ""
            DisableDdl("1")
            selectlevels(13, "select FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID") & " AND FolderListID=" & ddlselectFolder.SelectedValue & " ")
            divAll.Visible = True
            setAccessEnable(" AND FolderListID=" & ddlselectFolder.SelectedValue & " ")
        Else
            lblVErr.ForeColor = Color.Red
            lblVErr.Text = "Please select a folder"
        End If
    End Sub

    Private Sub loadMemberAccess()
        'Response.Write("select Top 1 FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID") & " order by level")
        Dim level As Integer
        'If Session("RoleId").ToString() = "1" Then 'Or (Session("RoleId").ToString() = "90")
        '    level = 0
        'Else
        '    level = 13
        'End If
        If (Session("RoleId").ToString() = "90") Or (Session("RoleId").ToString() = "43") Then
            'This query is for roleID 90 and 43
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select COUNT(*) from VolDocAccess where MemberID =" & Session("LoginID") & "") > 1 Then
                divAll.Visible = False
                divMulti.Visible = True
                Dim dsV As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select F.FolderListID,F.DisplayName  from VolDocAccess V Inner Join FolderTree F ON V.FolderListID = F.FolderListID Where V.MemberID =" & Session("LoginID") & "")
                ddlselectFolder.DataSource = dsV
                ddlselectFolder.DataBind()
                If dsV.Tables(0).Rows.Count > 0 Then
                    ddlselectFolder.Items.Insert(0, New ListItem("Select Folder", "0"))
                    ddlselectFolder.Items(0).Selected = True
                End If
                lblVErr.Text = "Please select a Folder"
                Exit Sub
            End If
        End If
        Dim SQLQuery As String = ""
        If Session("RoleId").ToString() = "1" Then
            level = 0
        ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Then
            'This query is for roleID 2,3,4,5
            SQLQuery = "select FT.FolderListID,FT.Level,dbo.ufn_getFolderName(FT.FolderListID) as Folders from FolderTree FT Inner Join Chapter C ON FT.FName=C.WebFolderName WHERE C.ChapterID = " & Session("selChapterID")
        Else
            level = 13
            SQLQuery = "select Top 1 FolderListID,Level,CASE WHEN FolderListID IS Null then Null Else dbo.ufn_getFolderName(FolderListID) End as Folders from VolDocAccess where MemberID =" & Session("LoginID")
        End If
        selectlevels(level, SQLQuery)
        divAll.Visible = True
        setAccessEnable("")
        '** Have to make default location chapter co-ordinators  ************

    End Sub

    Private Sub selectlevels(ByVal level As Integer, ByVal SQLQuery As String)
        Dim folderID As Integer = 0
        Dim selectedDDL As String()
        Dim arrstr As String()
        If SQLQuery.Length > 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            '** Have to make default location chapter co-ordinators  ************
            If ds.Tables(0).Rows.Count > 0 Then
                level = ds.Tables(0).Rows(0)("Level")
                If IsDBNull(ds.Tables(0).Rows(0)("FolderListID")) = False Then
                    folderID = ds.Tables(0).Rows(0)("FolderListID")
                    selectedDDL = ds.Tables(0).Rows(0)("Folders").Split("|")
                End If
            End If
        End If
        Select Case (level)
            Case 1
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                lblLevel.Text = "1"
                lblParentFID.Text = arrstr(0)
            Case 2
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                lblLevel.Text = "2"
                lblParentFID.Text = arrstr(0)
            Case 3
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                lblLevel.Text = "3"
                lblParentFID.Text = arrstr(0)
            Case 4
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                lblLevel.Text = "4"
                lblParentFID.Text = arrstr(0)
            Case 5
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                lblLevel.Text = "5"
                lblParentFID.Text = arrstr(0)
            Case 6
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                lblLevel.Text = "6"
                lblParentFID.Text = arrstr(0)
            Case 7
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                lblLevel.Text = "7"
                lblParentFID.Text = arrstr(0)
            Case 8
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                lblLevel.Text = "8"
                lblParentFID.Text = arrstr(0)
            Case 9
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                lblLevel.Text = "9"
                lblParentFID.Text = arrstr(0)
            Case 10
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                lblLevel.Text = "10"
                lblParentFID.Text = arrstr(0)
            Case 11
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                lblLevel.Text = "11"
                lblParentFID.Text = arrstr(0)
            Case 12
                loadLevel(ddlLevel1, 1, "")
                ddlLevel1.SelectedIndex = ddlLevel1.Items.IndexOf(ddlLevel1.Items.FindByText(selectedDDL(0)))
                ddlLevel1.Enabled = False
                arrstr = ddlLevel1.SelectedValue.Split("|")
                loadLevel(ddlLevel2, 2, arrstr(0))
                ddlLevel2.Items.RemoveAt(ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByValue("0")))
                ddlLevel2.SelectedIndex = ddlLevel2.Items.IndexOf(ddlLevel2.Items.FindByText(selectedDDL(1)))
                ddlLevel2.Enabled = False
                arrstr = ddlLevel2.SelectedValue.Split("|")
                loadLevel(ddlLevel3, 3, arrstr(0))
                ddlLevel3.Items.RemoveAt(ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByValue("0")))
                ddlLevel3.SelectedIndex = ddlLevel3.Items.IndexOf(ddlLevel3.Items.FindByText(selectedDDL(2)))
                ddlLevel3.Enabled = False
                arrstr = ddlLevel3.SelectedValue.Split("|")
                loadLevel(ddlLevel4, 4, arrstr(0))
                ddlLevel4.Items.RemoveAt(ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByValue("0")))
                ddlLevel4.SelectedIndex = ddlLevel4.Items.IndexOf(ddlLevel4.Items.FindByText(selectedDDL(3)))
                ddlLevel4.Enabled = False
                arrstr = ddlLevel4.SelectedValue.Split("|")
                loadLevel(ddlLevel5, 5, arrstr(0))
                ddlLevel5.Items.RemoveAt(ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByValue("0")))
                ddlLevel5.SelectedIndex = ddlLevel5.Items.IndexOf(ddlLevel5.Items.FindByText(selectedDDL(4)))
                ddlLevel5.Enabled = False
                arrstr = ddlLevel5.SelectedValue.Split("|")
                loadLevel(ddlLevel6, 6, arrstr(0))
                ddlLevel6.Items.RemoveAt(ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByValue("0")))
                ddlLevel6.SelectedIndex = ddlLevel6.Items.IndexOf(ddlLevel6.Items.FindByText(selectedDDL(5)))
                ddlLevel6.Enabled = False
                arrstr = ddlLevel6.SelectedValue.Split("|")
                loadLevel(ddlLevel7, 7, arrstr(0))
                ddlLevel7.Items.RemoveAt(ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByValue("0")))
                ddlLevel7.SelectedIndex = ddlLevel7.Items.IndexOf(ddlLevel7.Items.FindByText(selectedDDL(6)))
                ddlLevel7.Enabled = False
                arrstr = ddlLevel7.SelectedValue.Split("|")
                loadLevel(ddlLevel8, 8, arrstr(0))
                ddlLevel8.Items.RemoveAt(ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByValue("0")))
                ddlLevel8.SelectedIndex = ddlLevel8.Items.IndexOf(ddlLevel8.Items.FindByText(selectedDDL(7)))
                ddlLevel8.Enabled = False
                arrstr = ddlLevel8.SelectedValue.Split("|")
                loadLevel(ddlLevel9, 9, arrstr(0))
                ddlLevel9.Items.RemoveAt(ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByValue("0")))
                ddlLevel9.SelectedIndex = ddlLevel9.Items.IndexOf(ddlLevel9.Items.FindByText(selectedDDL(8)))
                ddlLevel9.Enabled = False
                arrstr = ddlLevel9.SelectedValue.Split("|")
                loadLevel(ddlLevel10, 10, arrstr(0))
                ddlLevel10.Items.RemoveAt(ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByValue("0")))
                ddlLevel10.SelectedIndex = ddlLevel10.Items.IndexOf(ddlLevel10.Items.FindByText(selectedDDL(9)))
                ddlLevel10.Enabled = False
                arrstr = ddlLevel10.SelectedValue.Split("|")
                loadLevel(ddlLevel11, 11, arrstr(0))
                ddlLevel11.Items.RemoveAt(ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByValue("0")))
                ddlLevel11.SelectedIndex = ddlLevel11.Items.IndexOf(ddlLevel11.Items.FindByText(selectedDDL(10)))
                ddlLevel11.Enabled = False
                arrstr = ddlLevel11.SelectedValue.Split("|")
                loadLevel(ddlLevel12, 12, arrstr(0))
                ddlLevel12.Items.RemoveAt(ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByValue("0")))
                ddlLevel12.SelectedIndex = ddlLevel12.Items.IndexOf(ddlLevel12.Items.FindByText(selectedDDL(11)))
                ddlLevel12.Enabled = False
                arrstr = ddlLevel12.SelectedValue.Split("|")
                lblLevel.Text = "12"
                lblParentFID.Text = arrstr(0)
            Case 0
                loadLevel(ddlLevel1, 1, "")
                lblLevel.Text = "0"
                lblParentFID.Text = "Null"
            Case Else
                Response.Redirect("WebPageMgmtMain.aspx")
        End Select
        constructpath(level)
    End Sub

    Protected Sub ddlLevel1_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        If Not ddlLevel1.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "1"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel2, 2, arrstr(0).Trim)
        Else
            lblLevel.Text = "0"
            lblParentFID.Text = "Null"
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel2_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel2.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel3, 3, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel1.SelectedValue.Split("|")
            lblLevel.Text = "1"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel3_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel3.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel4, 4, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel2.SelectedValue.Split("|")
            lblLevel.Text = "2"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel4_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel4.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel5, 5, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel3.SelectedValue.Split("|")
            lblLevel.Text = "3"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel5_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel5.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel6, 6, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel4.SelectedValue.Split("|")
            lblLevel.Text = "4"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel6_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel6.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel7, 7, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel5.SelectedValue.Split("|")
            lblLevel.Text = "5"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel7_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel7.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel8, 8, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel6.SelectedValue.Split("|")
            lblLevel.Text = "6"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel8_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel8.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel9, 9, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel7.SelectedValue.Split("|")
            lblLevel.Text = "7"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel9_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel9.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel10, 10, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel8.SelectedValue.Split("|")
            lblLevel.Text = "8"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel10_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel10.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel11, 11, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel9.SelectedValue.Split("|")
            lblLevel.Text = "9"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel11_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel11.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel11.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
            loadLevel(ddlLevel12, 12, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel10.SelectedValue.Split("|")
            lblLevel.Text = "10"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub
    Protected Sub ddlLevel12_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlLevel12.SelectedValue = "0" Then
            Dim arrstr As String() = ddlLevel12.SelectedValue.Split("|")
            lblLevel.Text = "12"
            lblParentFID.Text = arrstr(0)
            'loadLevel(ddlLevel12, 12, arrstr(0).Trim)
        Else
            Dim arrstr As String() = ddlLevel11.SelectedValue.Split("|")
            lblLevel.Text = "11"
            lblParentFID.Text = arrstr(0)
        End If
        constructpath(lblLevel.Text)
    End Sub

    Private Sub loadLevel(ByVal ddl As DropDownList, ByVal level As Integer, ByVal ParentFID As String)

        Dim whereContn As String = ""
        If level > 1 Then
            whereContn = " AND ParentFID=" & ParentFID
        Else
            lblLevel.Text = "0"
            lblParentFID.Text = "Null"
        End If
        Dim strSql As String
        Try
            strSql = "SELECT  Case WHEN Level=1 THEN CONVERT(Varchar,FolderListID) + '|' + FName Else  CONVERT(Varchar,FolderListID) + '|' + FName + '|' + CONVERT(Varchar,ParentFID) END   AS Value, DisplayName FROM FolderTree WHERE Level = " & level & whereContn & " ORDER BY DisplayName"

        Catch ex As Exception
            lblNoAccess.Text = strSql
        End Try
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        ddl.DataSource = ds
        ddl.DataBind()
        If ds.Tables(0).Rows.Count > 0 Then
            ddl.Items.Insert(0, New ListItem("Select Folder", "0"))
            ddl.Items(0).Selected = True
            ddl.Enabled = True
        Else
            ddl.Enabled = False
        End If
    End Sub

    Private Sub constructpath(ByVal level As String)
        lblError.Text = ""
        Select Case level
            Case "0"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                DisableDdl(1)
            Case "1"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                DisableDdl(2)
            Case "2"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                DisableDdl(3)
            Case "3"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                DisableDdl(4)
            Case "4"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                DisableDdl(5)
            Case "5"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                DisableDdl(6)
            Case "6"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                DisableDdl(7)
            Case "7"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                DisableDdl(8)
            Case "8"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                DisableDdl(9)
            Case "9"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                DisableDdl(10)
            Case "10"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                DisableDdl(11)
            Case "11"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                setPath(ddlLevel11)
                DisableDdl(12)
            Case "12"
                lblPath.Text = ServerPath
                lblURL.Text = urlPath
                setPath(ddlLevel1)
                setPath(ddlLevel2)
                setPath(ddlLevel3)
                setPath(ddlLevel4)
                setPath(ddlLevel5)
                setPath(ddlLevel6)
                setPath(ddlLevel7)
                setPath(ddlLevel8)
                setPath(ddlLevel9)
                setPath(ddlLevel10)
                setPath(ddlLevel11)
                setPath(ddlLevel12)
                'DisableDdl(level)
        End Select
        loadfiles()
    End Sub
    Private Sub DisableDdl(ByVal level As String)
        Select Case level
            Case "11"
                ddldisable(ddlLevel12)
            Case "10"
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "9"
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "8"
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "7"
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "6"
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "5"
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "4"
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "3"
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "2"
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
            Case "1"
                ddldisable(ddlLevel2)
                ddldisable(ddlLevel3)
                ddldisable(ddlLevel4)
                ddldisable(ddlLevel5)
                ddldisable(ddlLevel6)
                ddldisable(ddlLevel7)
                ddldisable(ddlLevel8)
                ddldisable(ddlLevel9)
                ddldisable(ddlLevel10)
                ddldisable(ddlLevel11)
                ddldisable(ddlLevel12)
        End Select
    End Sub
    Private Sub ddldisable(ByVal ddl As DropDownList)
        ddl.Items.Clear()
        ddl.Enabled = False
    End Sub
    Private Sub setPath(ByVal ddl As DropDownList)
        Dim arrstr As String() = ddl.SelectedValue.Split("|")
        lblPath.Text = lblPath.Text & arrstr(1).Trim & "\"
        lblURL.Text = lblURL.Text & arrstr(1).Trim & "/"
    End Sub

    Private Sub clearall()
        trfileUpload.Visible = False
        trfilelist1.Visible = True
        trfilelist.Visible = True
        ddlFiles.Items.Clear()
        lblError.Text = ""
        lblPath.Text = ServerPath
        lblURL.Text = urlPath
        loadLevel(ddlLevel1, 1, "")
        DisableDdl("1")
        loadMemberAccess()
        ' lblRenameFolder.Text = String.Empty
        'lblRenamePath.Text = String.Empty
    End Sub

    Protected Sub BtnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        clearall()
    End Sub

    Protected Sub Btndelete_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not ddlFiles.SelectedValue = "0" And Not ddlFiles.Items.Count < 1 Then
            Dim filename As String = Left(ddlFiles.SelectedItem.Text, ddlFiles.SelectedItem.Text.LastIndexOf("."))
            Dim deletedfilename As String = filename & "_" & Now.Year & Now.Month & Now.Day & "_" & Now.Millisecond & System.IO.Path.GetExtension(ddlFiles.SelectedItem.Text)
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO  FileDelete (FileListID, FolderListID, FileName, DestFolder, NewFileName,Status, FLCreateDate, FLCreatedBy, FLModifyDate, FLModifiedBy, CreateDate, CreatedBy) SELECT  FileListID, FolderListID, FileName,'" & lblPath.Text & "','" & deletedfilename & "','Deleted', CreateDate, CreatedBy, ModifyDate, ModifiedBy, GetDate()," & Session("LoginID") & " FROM FileList WHERE  FileName='" & ddlFiles.SelectedItem.Text & "' AND FolderListID=" & lblParentFID.Text & ";Delete from FileList WHERE FileName='" & ddlFiles.SelectedItem.Text & "' AND FolderListID=" & lblParentFID.Text & "")
            '**Move to Public/StoreDelFiles Folder with timestamp
            File.Move(lblPath.Text & ddlFiles.SelectedItem.Text, ServerPath & "Public\StoreDelFiles\" & deletedfilename)
            File.Delete(lblPath.Text & ddlFiles.SelectedItem.Text)
            loadfiles()
            lblError.Text = "Deleted Successfully"
        Else
            lblError.Text = "Please select a File"
        End If
    End Sub

    Protected Sub BtnRename_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        trfileUpload.Visible = False
        trRename.Visible = True
        trDownload.Visible = False
    End Sub

    Protected Sub BtnUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        trfileUpload.Visible = True
        trfilelist.Visible = False
        trDownload.Visible = False
        trfilelist1.Visible = False
        FileUpLoad1.Visible = True
        btnConfirmUpload.Visible = False
        btnFileUpload.Visible = True
        lblupload.Visible = True
        lbltempfilename.Text = String.Empty
        lblfiletemp.Text = String.Empty
    End Sub

    Protected Sub btnDownload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownload.Click
        'If Not ddlFiles.SelectedValue = "0" Then
        '    Dim file As System.IO.FileInfo = New System.IO.FileInfo(lblPath.Text & ddlFiles.SelectedItem.Text) '-- if the file exists on the server
        '    If file.Exists Then 'set appropriate headers
        '        Response.Clear()
        '        Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
        '        Response.AddHeader("Content-Length", file.Length.ToString())
        '        Response.ContentType = "application/octet-stream"
        '        Response.WriteFile(file.FullName)
        '        Response.End()
        '    Else
        '        lblError.Text = "This file does not exist."
        '    End If 'nothing in the URL as HTTP GET
        'Else
        '    lblError.Text = "Please select a File"
        'End If
        If ddlFiles.Items.Count > 0 Then
            lstFiles.Items.Clear()
            loadlistfiles()
            If lstFiles.Items.Count > 5 Then
                lstFiles.Height = "100"
            End If

            trDownload.Visible = True
            trRename.Visible = False
            trfileUpload.Visible = False
            trfilelist.Visible = False
            trfilelist1.Visible = False
        Else
            lblError.Text = "No file to download"
        End If
    End Sub

    Protected Sub btnDownloadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadCancel.Click
        trfilelist.Visible = True
        trfilelist1.Visible = True
        trDownload.Visible = False
    End Sub

    Private Sub setAccessEnable(ByVal VolDocAccesscntn As String)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " select AccessType,IPAddress from VolDocAccess where MemberID =" & Session("LoginID") & VolDocAccesscntn)
        'Request.ServerVariables("REMOTE_ADDR")
        'Read (Download Only)
        'Write (Download/Upload)
        'Delete (Delete folder/file)
        'Create Sub-folder
        'All
        If ds.Tables(0).Rows.Count > 0 Then
            If Trim(ds.Tables(0).Rows(0)("AccessType")) = "Read" Then
                btnDownload.Enabled = True
            ElseIf Trim(ds.Tables(0).Rows(0)("AccessType")) = "Write" Then
                btnDownload.Enabled = True
                BtnUpload.Enabled = True
            ElseIf Trim(ds.Tables(0).Rows(0)("AccessType")) = "Delete" Then
                btnDownload.Enabled = True
                BtnUpload.Enabled = True
                BtnDelete.Enabled = True
            ElseIf Trim(ds.Tables(0).Rows(0)("AccessType")) = "All" Then
                btnDownload.Enabled = True
                BtnUpload.Enabled = True
                BtnDelete.Enabled = True
                BtnRename.Enabled = True
            End If
            If Not ds.Tables(0).Rows(0)("IPAddress") Is DBNull.Value Then
                If Not Trim(ds.Tables(0).Rows(0)("IPAddress")) = Request.ServerVariables("REMOTE_ADDR") Then
                    divAll.Visible = False
                    lblNoAccess.Text = "Sorry, IP Mismatch. Your Current IP Address is : " & Request.ServerVariables("REMOTE_ADDR")
                Else
                    lblNoAccess.Text = String.Empty
                End If
            Else
                lblNoAccess.Text = String.Empty
            End If
        ElseIf Session("RoleId").ToString() = "1" Then
            'Admin
            btnDownload.Enabled = True
            BtnUpload.Enabled = True
            BtnDelete.Enabled = True
            BtnRename.Enabled = True
        ElseIf (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5") Then
            'This query is for roleID 2,3,4,5

        Else
            divAll.Visible = False
            divMulti.Visible = False
            lblNoAccess.Text = "Sorry, You have No permission"
        End If

    End Sub

    Protected Sub btnContinue_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        loadfiles()
    End Sub
    Private Sub loadlistfiles()
        Try
            lblDownloaderr.Text = "Press Ctrl and select multiple Files to download"
            Dim SQLQuery As String
            If Session("RoleId") = "1" Then
                SQLQuery = " Select FileListID,FileName  from FileList  Where FolderListID = " & lblParentFID.Text
            Else
                SQLQuery = " Select FileListID,FileName  from FileList  Where FileListID Not in (select FileListID from RestFileAccess) AND FolderListID = " & lblParentFID.Text & " Union All select FL.FileListID,FL.FileName from FileList FL INNER JOIN RestFileAccess RF ON RF.FilelistID = FL.FileListID  Where Rf.RoleID = " & Session("RoleId") & " AND FL.FolderListID =" & lblParentFID.Text
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            '** Have to make default location chapter co-ordinators  ************
            lstFiles.DataSource = ds.Tables(0)
            lstFiles.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                lstFiles.Enabled = True

                If Not ddlFiles.SelectedValue = "0" Then
                    lstFiles.SelectedIndex = lstFiles.Items.IndexOf(lstFiles.Items.FindByValue(ddlFiles.SelectedValue))
                End If
            Else
                lstFiles.Enabled = False
            End If
        Catch ex As Exception
            lblError.Text = ex.ToString()
        End Try
    End Sub
    Private Sub loadfiles()
        Try
            ''################## Loading Files from Folders ################
            ''Dim strFileSize As String = ""
            ' ''Dim di As New IO.DirectoryInfo(Server.MapPath(TextBox1.Text))
            ''Dim di As New IO.DirectoryInfo(lblPath.Text)
            ''Dim aryFi As IO.FileInfo() = di.GetFiles()
            ''Dim fi As IO.FileInfo
            ''Dim flag As Boolean = False
            ''ddlFiles.Items.Clear()
            ''For Each fi In aryFi
            ''    flag = True
            ''    ddlFiles.Items.Add(fi.Name)
            ''Next
            ''If flag = True Then
            Dim SQLQuery As String
            If Session("RoleId") = "1" Then
                SQLQuery = " Select FileListID,FileName  from FileList  Where FolderListID = " & lblParentFID.Text
            Else
                SQLQuery = " Select FileListID,FileName  from FileList  Where FileListID Not in (select FileListID from RestFileAccess) AND FolderListID = " & lblParentFID.Text & " Union All select FL.FileListID,FL.FileName from FileList FL INNER JOIN RestFileAccess RF ON RF.FilelistID = FL.FileListID  Where Rf.RoleID = " & Session("RoleId") & " AND FL.FolderListID =" & lblParentFID.Text
            End If
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLQuery)
            '** Have to make default location chapter co-ordinators  ************
            ddlFiles.DataSource = ds.Tables(0)
            ddlFiles.DataBind()
            If ds.Tables(0).Rows.Count > 0 Then
                ddlFiles.Items.Insert(0, New ListItem("Select File", "0"))
            Else
                lblError.Text = "No file found"
            End If
        Catch ex As Exception
            lblError.Text = ex.ToString()
        End Try
    End Sub

    Protected Sub btnConfirmUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If lbltempfilename.Text.Length > 0 Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update FileList set ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & "  WHERE FileName='" & lbltempfilename.Text & "' AND FolderListID=" & lblParentFID.Text & "")

                '**Move to Public/StoreOldFiles Folder with timestamp
                Dim filename As String = Left(lbltempfilename.Text, lbltempfilename.Text.LastIndexOf("."))
                Dim deletedfilename As String = filename & "_" & Now.Year & Now.Month & Now.Day & "_" & Now.Millisecond & System.IO.Path.GetExtension(lbltempfilename.Text)
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "INSERT INTO  FileDelete (FileListID, FolderListID, FileName, DestFolder, NewFileName,Status, FLCreateDate, FLCreatedBy, FLModifyDate, FLModifiedBy, CreateDate, CreatedBy) SELECT  FileListID, FolderListID, FileName,'" & lblPath.Text & "','" & deletedfilename & "','Updated', CreateDate, CreatedBy, ModifyDate, ModifiedBy, GetDate()," & Session("LoginID") & " FROM FileList WHERE  FileName='" & lbltempfilename.Text & "' AND FolderListID=" & lblParentFID.Text & "")
                File.Move(lblPath.Text & lbltempfilename.Text, ServerPath & "Public\StoreOldFiles\" & deletedfilename)
                'File.Delete(lblPath.Text & lbltempfilename.Text)
                File.Move(lblPath.Text & lblfiletemp.Text, lblPath.Text & lbltempfilename.Text)
                lblError.Text = "File replaced successfully"
                lbltempfilename.Text = String.Empty
                lblfiletemp.Text = String.Empty
                FileUpLoad1.Visible = True
                trfilelist.Visible = True
                trfilelist1.Visible = True
                btnConfirmUpload.Visible = False
                btnFileUpload.Visible = True
                lblupload.Visible = True
                trfileUpload.Visible = False
                loadfiles()
            Catch ex As Exception
                'Response.Write("Update FileList set ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & "  WHERE FileName='" & lbltempfilename.Text & "' AND FolderListID=" & lblParentFID.Text & "")
                lblError.Text = ex.ToString()
            End Try
        Else
            lblError.Text = "No uploaded file"
        End If
    End Sub

    Protected Sub btnFileUpload_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnFileUpload.Click
        If FileUpLoad1.HasFile Then
            Dim fileExt As String
            Dim filecount As Integer = 0
            fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName)
            '.aspx, .doc, .docx, .xls, .xlsx, .pdf, .config, .txt, .zip, .zipx
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(lblPath.Text & FileUpLoad1.FileName.Replace(" ", ""))
            If fileExt.ToLower = ".js" Or fileExt.ToLower = ".css" Or fileExt.ToLower = ".csv" Or fileExt.ToLower = ".aspx" Or fileExt.ToLower = ".html" Or fileExt.ToLower = ".htm" Or fileExt.ToLower = ".jpg" Or fileExt.ToLower = ".jpeg" Or fileExt.ToLower = ".gif" Or fileExt.ToLower = ".png" Or fileExt.ToLower = ".doc" Or fileExt.ToLower = ".docx" Or fileExt.ToLower = ".xls" Or fileExt.ToLower = ".xlsx" Or fileExt.ToLower = ".pdf" Or fileExt.ToLower = ".config" Or fileExt.ToLower = ".txt" Or fileExt.ToLower = ".zip" Or fileExt.ToLower = ".zipx" Or fileExt.ToLower = ".cur" Then
                Try
                    If file.Exists Then
                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from RestFileAccess where FileName ='" & FileUpLoad1.FileName.Replace(" ", "") & "' AND FolderListID=" & lblParentFID.Text & "") = 0 Then
                            lblError.Text = "File already present. Please press Confirm Replace to replace the existing file or click Cancel and upload again"
                            lbltempfilename.Text = FileUpLoad1.FileName.Replace(" ", "")
                            lblfiletemp.Text = "temp" & fileExt
                            FileUpLoad1.PostedFile.SaveAs(lblPath.Text & "temp" & fileExt)
                            btnFileUpload.Visible = False
                            lblupload.Visible = False
                            btnConfirmUpload.Visible = True
                            FileUpLoad1.Visible = False
                        ElseIf SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(*) from RestFileAccess where FileName ='" & FileUpLoad1.FileName.Replace(" ", "") & "' AND FolderListID=" & lblParentFID.Text & " AND RoleID= " & Session("RoleId") & "") > 0 Then
                            lblError.Text = "File already present. Please press Confirm Replace to replace the existing file or click Cancel and upload again"
                            lbltempfilename.Text = FileUpLoad1.FileName.Replace(" ", "")
                            lblfiletemp.Text = "temp" & fileExt
                            FileUpLoad1.PostedFile.SaveAs(lblPath.Text & "temp" & fileExt)
                            btnFileUpload.Visible = False
                            lblupload.Visible = False
                            btnConfirmUpload.Visible = True
                            FileUpLoad1.Visible = False
                        Else
                            Response.Write("select COUNT(*) from RestFileAccess where FileName ='" & FileUpLoad1.FileName.Replace(" ", "") & "' AND FolderListID=" & lblParentFID.Text & " AND RoleID= " & Session("RoleId") & "")
                            lblError.Text = "Sorry, Its Restricted File, You cannot replace the Existing file"
                        End If
                        Exit Sub

                    End If
                    'SELECT     TOP (200) FileListID, FolderListID, FileName, CreateDate, CreatedBy, ModifyDate, ModifiedBy FROM FileList
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from FileList where FileName='" & FileUpLoad1.FileName.Replace(" ", "") & "' AND FolderListID=" & lblParentFID.Text & "") < 1 Then
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into FileList(FolderListID, FileName, CreateDate, CreatedBy) Values(" & lblParentFID.Text & ",'" & FileUpLoad1.FileName.Replace(" ", "") & "',Getdate()," & Session("LoginID") & ")")
                    Else
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update FileList set ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & "  WHERE FileName='" & FileUpLoad1.FileName.Replace(" ", "") & "' AND FolderListID=" & lblParentFID.Text & "")
                    End If
                    FileUpLoad1.PostedFile.SaveAs(lblPath.Text & FileUpLoad1.FileName.Replace(" ", ""))
                    lblError.Text = "file uploaded successfully"
                    trfilelist.Visible = True
                    trfilelist1.Visible = True
                    trfileUpload.Visible = False
                    loadfiles()
                Catch ex As Exception
                    'Response.Write("Insert into FileList(FolderListID, FileName, CreateDate, CreatedBy) Values(" & lblParentFID.Text & ",'" & FileUpLoad1.FileName & "',Getdate()," & Session("LoginID") & ")")
                    lblError.Text = ex.ToString()
                End Try
            Else
                lblError.Text = "File Format not Accepted"
            End If
        Else
            lblError.Text = "No uploaded file"
        End If
    End Sub

    Protected Sub btnFileUploadCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = String.Empty
        If lblfiletemp.Text.Length > 0 Then
            lblError.Text = String.Empty
            Dim file As System.IO.FileInfo = New System.IO.FileInfo(lblPath.Text & lblfiletemp.Text)
            If file.Exists Then
                file.Delete()
            End If
        End If
        btnConfirmUpload.Visible = False
        btnFileUpload.Visible = True
        lblupload.Visible = True
        trfilelist.Visible = True
        trfilelist1.Visible = True
        trfileUpload.Visible = False
        lbltempfilename.Text = String.Empty
        lblfiletemp.Text = String.Empty
        FileUpLoad1.Visible = True
    End Sub

    Protected Sub btnRenameFile_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        'Rename
        If Not ddlFiles.SelectedValue = "0" And txtRenameFile.Text.Length > 0 Then
            Try
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from FileList where FileName='" & ddlFiles.SelectedItem.Text & "' AND FolderListID=" & lblParentFID.Text & "") < 1 Then
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into FileList(FolderListID, FileName, CreateDate, CreatedBy) Values(" & lblParentFID.Text & ",'" & txtRenameFile.Text.Replace(" ", "") & System.IO.Path.GetExtension(ddlFiles.SelectedItem.Text) & "',Getdate()," & Session("LoginID") & ")")
                Else
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update FileList set FileName='" & txtRenameFile.Text.Replace(" ", "") & "', ModifyDate= Getdate(), ModifiedBy=" & Session("LoginID") & " WHERE FileName='" & ddlFiles.SelectedItem.Text & "' AND FolderListID=" & lblParentFID.Text & "")
                End If
                File.Move(lblPath.Text & ddlFiles.SelectedItem.Text, lblPath.Text & txtRenameFile.Text & System.IO.Path.GetExtension(ddlFiles.SelectedItem.Text))
                lblError.Text = "File renamed successfully"
                trRename.Visible = False
                loadfiles()
            Catch ex As Exception
                lblError.Text = "Same File Name Exist"
            End Try
        Else
            lblError.Text = "Please select File and Enter New File Name"
        End If
    End Sub

    Protected Sub btnRenameCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        txtRenameFile.Text = ""
        trRename.Visible = False
    End Sub

    Protected Sub btnDownloadfiles_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadfiles.Click
        Dim i As Integer
        Dim Flag As Boolean = False
        For i = 0 To lstFiles.Items.Count - 1
            If lstFiles.Items(i).Selected Then
                Flag = True
                'file = New System.IO.FileInfo(lblPath.Text & lstFiles.Items(i).Text) '-- if the file exists on the server
                ClientScript.RegisterStartupScript(Me.GetType(), lstFiles.Items(i).Value, "<script language='javascript'>fnOpen('" & lstFiles.Items(i).Value & "');</script>")
                'If file.Exists Then 'set appropriate headers
                '    Response.Clear()
                '    Response.AddHeader("Content-Disposition", "attachment; filename=" & file.Name)
                '    Response.AddHeader("Content-Length", file.Length.ToString())
                '    Response.ContentType = "application/octet-stream"
                '    Response.WriteFile(file.FullName)
                '    Response.End() 'if file does not exist
                'Else
                '    lblError.Text = "This file does not exist."
                'End If 'nothing in the URL as HTTP GET
            End If
        Next
        If Flag = False Then
            lblDownloaderr.Text = "No file selected"
        Else
            lblDownloaderr.Text = ""
        End If
    End Sub
End Class

