﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="EmailFundRaising.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="EmailFundRaising" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Email FundRaising
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <asp:Panel ID="Panel3" runat="server" Visible="true">
        <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px">
            <tr>
                <td align="center" bgcolor="#99CC33" id="Td2"><span class="title03">Send Email to Patrons</span></td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">Event Year</td>
                            <td align="left">
                                <asp:ListBox ID="lstYear" SelectionMode="Multiple" runat="server"
                                    Width="150px" AutoPostBack="true"></asp:ListBox>
                            </td>
                        </tr>

                        <tr>
                            <td align="left">Type of Registration</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlRegistype1" runat="server" Height="24px" Width="150px">
                                    <asp:ListItem Selected="True" Value="1">Paid Registrations</asp:ListItem>
                                    <asp:ListItem Value="2">Pending Registrations</asp:ListItem>

                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Button ID="ParentStudEmail" runat="server" Text="Send EMail" OnClick="ParentStudEmail_Click" />
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblerr1" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>

                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
</asp:Content>
