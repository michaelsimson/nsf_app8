﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="WebConfControl.aspx.cs" Inherits="WebConfControl" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Web Conf Control
             <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <table align="center" style="width: 250px;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Year</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="ddlYear" runat="server" Width="100px">
                      

                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>
            <tr>
                <td align="left" nowrap="nowrap" style="font-weight: bold;">Event</td>
                <td align="left">
                    <asp:DropDownList ID="ddEvent" runat="server" Width="100px" Enabled="false" AutoPostBack="True">
                        <asp:ListItem Value="13">Coaching</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>
            <tr>
                <td align="left" nowrap="nowrap"><b>Product Group</b></td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddlProductGroup" runat="server" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" Width="100px"
                        AutoPostBack="True">
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>
            <tr>
                <td align="left" nowrap="nowrap"><b>Product</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="ddlProduct" runat="server" Width="100px" AutoPostBack="True">
                    </asp:DropDownList>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>

            </tr>

            <tr>
                <td align="left" nowrap="nowrap"><b>Session Creation Date</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:TextBox ID="txtSessionCreationDate" runat="server" Width="100px" Height="18px"></asp:TextBox>
                    <b>(MM-DD-YYYY)</b>
                    <div style="margin-bottom: 5px; clear: both;"></div>
                </td>
            </tr>

            <tr>
                <td align="left" nowrap="nowrap"><b>Coach Change</b> </td>

                <td align="left" nowrap="nowrap">
                    <asp:DropDownList ID="DdlCoachChange" runat="server" Width="100px" AutoPostBack="True">
                        <asp:ListItem Value="N">N</asp:ListItem>
                        <asp:ListItem Value="Y">Y</asp:ListItem>
                    </asp:DropDownList>
                    <div style="margin-bottom: 20px; clear: both;"></div>
                </td>

            </tr>

            <tr>
                <td colspan="2" align="center">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" Visible="true" OnClick="btnSubmit_Click" />
                    <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
                </td>


            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GrdSessionCreation" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" Style="width: 900px; margin-bottom: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdSessionCreation_RowCommand">
            <Columns>
                <asp:TemplateField>
                    <ItemTemplate>
                        <asp:Button ID="BtnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <div style="display: none;">
                            <asp:Label runat="server" ID="LblWebConfControlID" Text='<%#DataBinder.Eval(Container.DataItem,"WebConfControlID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lbEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label runat="server" ID="LblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                            <asp:Label runat="server" ID="LblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="EventYear" HeaderText="Event Year"></asp:BoundField>
                <asp:BoundField DataField="EventID" HeaderText="Event ID"></asp:BoundField>
                <asp:BoundField DataField="EventName" HeaderText="Event Name"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupName" HeaderText="Product Group"></asp:BoundField>
                <asp:BoundField DataField="ProductName" HeaderText="Product"></asp:BoundField>
                <asp:BoundField DataField="SessionCreationDate" HeaderText="SessionCreationDate" DataFormatString="{0:MM-dd-yyyy}"></asp:BoundField>
                <asp:BoundField DataField="CoachChange" HeaderText="Coach Change"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>
    <input type="hidden" id="hdnWebConfControlID" value="0" runat="server" />
</asp:Content>

