﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="CoachingRegCount.aspx.cs" Inherits="CoachingRegCount" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <asp:Label ID="lblPageTitle" runat="server">Coaching Registration Count</asp:Label>

        <br />
        <br />
    </div>
    <br />
    <table>
        <tr>
            <td style="width: 120px;"></td>
            <td align="left" nowrap="nowrap">Report Type&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 200px;">
                <asp:DropDownList ID="DDLReportType" runat="server" Width="190px" AutoPostBack="true" OnSelectedIndexChanged="DDLReportType_SelectedIndexChanged">
                    <asp:ListItem Value="1">Coaching Registration Count </asp:ListItem>
                    <asp:ListItem Value="2">Coaching Children by Chapter </asp:ListItem>
                    <asp:ListItem Value="3">Coaches by Chapter </asp:ListItem>
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap">From Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 100px;">
                <asp:DropDownList ID="ddlFromYear" runat="server" Width="100px">
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap">To Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 100px;">
                <asp:DropDownList ID="ddlToYear" runat="server" Width="100px">
                </asp:DropDownList>
            </td>
            <td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" /></td>
            <td>
                <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

            </td>

        </tr>

    </table>
    <br />
    <table>
        <tr>
            <td style="width: 387px;"></td>
            <td></td>
            <td>
                <asp:Button ID="BtnExpo"
                    runat="server" Visible="false"
                    Text="Export to Excel" OnClick="BtnExpo_Click" /></td>
        </tr>
    </table>
    <br />
    <div id="Div1" runat="server" align="center">
        <table>
            <tr>
                <td>
                    <asp:GridView ID="GVcount" runat="server" EnableModelValidation="True" OnRowDataBound="GVcount_RowDataBound">

                        <RowStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>

    <div id="Div2" runat="server" align="center">
        <table>
            <tr>
                <td>
                    <asp:GridView ID="GrdReportByChapter" runat="server">
                      
                        <RowStyle HorizontalAlign="Left" />
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </div>
</asp:Content>

