﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Configuration;
using System.IO;
using System.Threading;

public partial class GenSBVBTestPapers : System.Web.UI.Page
{
    int totalCount;
    int ic;
    int icTemp;
    int icAnsKey;
    int icStuPh2=25;
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            ic = 0;
            icTemp = 0;
            icAnsKey = 0;
            
            ViewState["increValue"] = ic;
            ViewState["increValueTemp"] = icTemp;
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            //{
            //    Response.Redirect("~/login.aspx?entry=p");
            //}
            if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            {
                Response.Redirect("~/VolunteerFunctions.aspx");
            }
            divTemp.Visible = false;
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));

            divTestPapers.Visible = false;



            ThreadStart testThread1Start = new ThreadStart(testThread1);
            ThreadStart testThread2Start = new ThreadStart(testThread2);

            Thread[] testThread = new Thread[2];
            testThread[0] = new Thread(testThread1Start);
            testThread[1] = new Thread(testThread2Start);

            foreach (Thread myThread in testThread)
            {
                myThread.Start();
            }
 
        }


       
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }


    public void testThread1()
    {
        Response.Write("METHOD 1");
    }

    public void testThread2()
    {
        Response.Write("METHOD 2");
    }

    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlSet.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Set";
            lblErr.Visible = true;
        }
        else if (ddlOutput.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Output";
            lblErr.Visible = true;
        }
        else
        {
            String strcheckExistsqry;
            DataSet dscheckexists;
            if (ddlOutput.SelectedIndex == 1)
            {
                lblErr.Text = "";
                strcheckExistsqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new random words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    generatingRandomWords();
                    displayPubUnpubList();
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
                
                
                
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                   lblErr.Text = "";
                strcheckExistsqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                dscheckexists = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strcheckExistsqry);
                if (dscheckexists.Tables[0].Rows.Count > 0)
                {
                    lblErr.Text = "Data already exists.  Do you want the existing data displayed?";
                    lnkClickHere1.Visible = true;
                    lblErr.Visible = true;
                    lblErr.ForeColor = Color.Red;

                    lblErr1.Text = ".  Else if you want to generate new TP words again, ";
                    lnkClickHere2.Visible = true;
                    lblErr1.Visible = true;
                    lblErr1.ForeColor = Color.Red;

                    gvPubUnpubList.Visible = false;
                    divTestPapers.Visible = false;
                }
                else
                {
                    generateTPWords();
                    btnSaveWords.Enabled = true;
                    btnExportExcel.Enabled = true;
                    divTestPapers.Visible = false;
                }
            }
            else if (ddlOutput.SelectedIndex == 3)
            {
                lblErr.Text = "";
                lblErr1.Text = "";
                lnkClickHere1.Visible = false;
                lnkClickHere2.Visible = false;
                generateTestPapers();
            }
        }

       
    }
    protected void generateTestPapers() {
        totalCount = 0;
        lblTestPaperPhase.Text = "<b>Phase: I " + ddlProduct.SelectedItem.Text+" - Judge Copy(Published)</b>";
        string strquery = "";
        strquery = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 10 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.[Sub-level]";
      
        
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);

       //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int i=1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = i;
            i++;
            
        }
        //binding to temporary gridview
        //gvTemp.DataSource = dsJVB.Tables[0];
        //gvTemp.DataBind();


        //binding to temporary repeater
        //rpToPDF.DataSource = dsJVB;
        //rpToPDF.DataBind();
        
     

          //rpTestPaper.DataSource = dsJVB.Tables[0];
          Session["GenerateTestPapers"] = dsJVB.Tables[0];
          totalCount = dsJVB.Tables[0].Rows.Count;
       //rpTestPaper.DataBind();
          if (totalCount > 0)
          {
              bindData();
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = true;
          }
          else
          {
              lblErr.Text = "No Record Exists!";
              lblErr.Visible = true;
              lblErr.ForeColor = Color.Red;
              gvPubUnpubList.Visible = false;
              divTestPapers.Visible = false;
          }
         
   
       
    }
    protected void bindData()
    {
        string connectionString = Application["ConnectionString"].ToString();
        SqlConnection connection = new SqlConnection(connectionString);
        DataSet ds = new DataSet();
        //String sql = "(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level, sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 10 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 5 case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then '1/E' when 'Intermediate Vocabulary' then '2/D' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end as Level,sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " )";//union(select distinct top 4 sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " )union(select distinct top 4 sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " ) ";
        String sql = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 10 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.[Sub-level]";
        int val = Convert.ToInt16(txtHidden.Value);
        if (val <= 0)
            val = 0;
        connection.Open();

        SqlDataAdapter adapter = new SqlDataAdapter(sql, connection);
        adapter.Fill(ds, val, 5, "SpVocabTPWords");
        connection.Close();
        rpTestPaper.DataSource = ds;
        rpTestPaper.DataBind();

        if (val <= 0)
        {
            lnkBtnPrev.Visible = false;
            lnkBtnNext.Visible = true;
        }

        if (val >= 5)
        {
            lnkBtnPrev.Visible = true;
            lnkBtnNext.Visible = true;
        }

        if ((val + 5) >= totalCount)
        {
            lnkBtnNext.Visible = false;
        }
    }
    protected void generateTPWords()
    {
        string strquery="";
        
        if (ddlProduct.SelectedItem.Value.Equals("Junior Vocabulary"))
        {

            strquery = "(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=1 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 10 ) and PubUnp='Y' and Level=1 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,1 as Phase,'Tie Breaker' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 11 and 15 ) and PubUnp='Y' and Level=1 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,2 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=1 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,2 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " ) Order By PubUnp, Level, SubLevel,RandSeqID";
            //strquery = "(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=1 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=2 )union (select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 10 ) and PubUnp='Y' and Level=1 and Sublevel=2 )union(select RandSeqID,Word,'Tie Breaker' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 11 and 15 ) and PubUnp='Y' and Level=1 and Sublevel=2 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,2 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=1 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,2 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=2 ) Order By PubUnp, Level, SubLevel,RandSeqID";
            DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
            if (dsJVB.Tables[0].Rows.Count > 0)
            {
                gvPubUnpubList.DataSource = dsJVB.Tables[0];
                gvPubUnpubList.DataBind();
                gvPubUnpubList.Visible = true;
                Session["GenTPWords"] = dsJVB.Tables[0];
            }
            else
            {
                lblErr.Text = "No Record Exists!";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
                gvPubUnpubList.Visible = false;
            }

        }
        else if (ddlProduct.SelectedItem.Value.Equals("Intermediate Vocabulary"))
        {
            strquery = "(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=2 and Sublevel=1 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=2 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + ")union(select Year,EventID,SetID,1 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 1 and 10 ) and PubUnp='Y' and Level=2 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,1 as Phase,'Tie Breaker' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 11 and 15 ) and PubUnp='Y' and Level=2 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " )union(select Year,EventID,SetID,2 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=2 and Sublevel=1 and SetID=" + ddlSet.SelectedValue + ")union(select Year,EventID,SetID,2 as Phase,'Regular' as RoundType,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnp,Level,Sublevel as [Sub-level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate,CreatedBy,ModifiedDate,ModifiedBy from SBVBRandWords where Year=" + ddlYear.SelectedValue + " and Eventid=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=2 and Sublevel=2 and SetID=" + ddlSet.SelectedValue + " ) Order By PubUnp, Level, SubLevel,RandSeqID";
            //strquery = "(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=1 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 5 ) and PubUnp is null and Level=1 and Sublevel=2 )union (select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 1 and 10 ) and PubUnp='Y' and Level=1 and Sublevel=2 )union(select RandSeqID,Word,'Tie Breaker' as RoundType,PubUnp,ProductGroupCode,ProductCode,1 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 11 and 15 ) and PubUnp='Y' and Level=1 and Sublevel=2 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,2 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=1 )union(select RandSeqID,Word,'Regular' as RoundType,PubUnp,ProductGroupCode,ProductCode,2 as Phase,Level,Sublevel as [Sub-level] from SBVBRandWords where ([RandSeqID] between 6 and 190 ) and PubUnp is null and Level=1 and Sublevel=2 ) Order By PubUnp, Level, SubLevel,RandSeqID";
            DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
            if (dsJVB.Tables[0].Rows.Count > 0)
            {
                gvPubUnpubList.DataSource = dsJVB.Tables[0];
                gvPubUnpubList.DataBind();
                gvPubUnpubList.Visible = true;
                Session["GenTPWords"] = dsJVB.Tables[0];
            }
            else
            {
                lblErr.Text = "No Record Exists!";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
                gvPubUnpubList.Visible = false;
            }
        }
        

    }
    protected void displayPubUnpubList()
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from TempSBVBRandWords");
        //DataTable dtEasy = dsEasy.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            Session["GeneratedRandomWords"] = ds.Tables[0];
        }
    }
    protected void generatingRandomWords()
    {
        
        string strqry = "";
        string productstr = "";
        if (ddlProduct.SelectedItem.Value.Equals("Junior Vocabulary"))
        {
            productstr = " and level=1";
        }
        else if (ddlProduct.SelectedItem.Value.Equals("Intermediate Vocabulary"))
        {
            productstr = " and level=2";
        }


        // Getting Published records with Sublevel1 from Vocabwords_new and insert into Temporary Table
        strqry = "IF ( EXISTS (SELECT * FROM INFORMATION_SCHEMA.TABLES WHERE TABLE_NAME = N'TempSBVBRandWords')) begin drop table TempSBVBRandWords end create table TempSBVBRandWords(Year int,EventID int,SetID int,ProductGroupID int,ProductGroupCode nvarchar(20), ProductID int, ProductCode nvarchar(20),PubUnp nvarchar(20),Level int, SubLevel int,OrigSeqID int identity(1,1) primary key,RandomNumber float, RandSeqID int,Word nvarchar(50),  CreatedDate date, CreatedBy int, ModifiedDate date, ModifiedBy int) insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='" + ddlProductGroup.SelectedItem.Text + "' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y'" + productstr + " and [Sub-level]=1))";
        int i = SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsPubSL1 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=1");
        DataTable dtPubSL1 = dsPubSL1.Tables[0];
        DataTable dtnew = (DataTable)dtPubSL1.Clone();
        int k = 0;
        string updateqry = "";
     
        if (dtPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

        dsPubSL1 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=1 Order By RandomNumber");
        dtPubSL1 = dsPubSL1.Tables[0];
        dtnew = (DataTable)dtPubSL1.Clone();
       
       updateqry = "";
        if (dtPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID ="+dtnew.Rows[k]["RandSeqID"]+" where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }


        // Getting Published records with Sublevel2 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Regional ='Y'" + productstr + " and [Sub-level]=2))";
        SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsPubSL2 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=2");
        DataTable dtPubSL2 = dsPubSL2.Tables[0];
        dtnew = (DataTable)dtPubSL2.Clone();
     
        updateqry = "";

        if (dtPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

        dsPubSL2 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp is null " + productstr + " and Sublevel=2 Order By RandomNumber");
        dtPubSL2 = dsPubSL2.Tables[0];
        dtnew = (DataTable)dtPubSL2.Clone();

        updateqry = "";
        if (dtPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp is null " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

        // Getting UnPublished records with Sublevel1 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Reg_Reserved ='Y'" + productstr + " and [Sub-level]=1))";
        SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsUnPubSL1 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=1");
        DataTable dtUnPubSL1 = dsUnPubSL1.Tables[0];
        dtnew = (DataTable)dtUnPubSL1.Clone();

        updateqry = "";

        if (dtUnPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

        dsUnPubSL1 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=1 Order By RandomNumber");
        dtUnPubSL1 = dsUnPubSL1.Tables[0];
        dtnew = (DataTable)dtUnPubSL1.Clone();

        updateqry = "";
        if (dtUnPubSL1.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL1.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL1.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=1  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }




        // Getting UnPublished records with Sublevel2 from Vocabwords_new and insert into Temporary Table
        strqry = "insert into TempSBVBRandWords(Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,Word,  CreatedDate, CreatedBy) ((select  " + ddlYear.SelectedValue + " as Year,(select EventId from event where Eventid=" + ddlEvent.SelectedValue + ") as EventID," + ddlSet.SelectedValue + " as SetId, ( select ProductGroupID from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupID,( select ProductGroupCode from productgroup where name='Vocabulary' and Eventid=" + ddlEvent.SelectedValue + ") as ProductGroupCode, (select ProductID from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductId, (select ProductCode from Product where Name='" + ddlProduct.SelectedItem.Text + "' and EventId=" + ddlEvent.SelectedValue + ") as ProductCode,case " + ddlEvent.SelectedValue + " when 1 then National_Reserved when 2 then Reg_Reserved end as PubUnp,Level, [Sub-level],Word, convert(varchar(20),getdate(),101) as CreatedDate, " + Session["LoginID"] + " as CreatedBy from Vocabwords_new vn where Reg_Reserved ='Y'" + productstr + " and [Sub-level]=2))";
        SqlHelper.ExecuteNonQuery(Application["Connectionstring"].ToString(), CommandType.Text, strqry);
        //Randomizing the set of records in TempTable and feeded into same table
        DataSet dsUnPubSL2 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=2");
        DataTable dtUnPubSL2 = dsUnPubSL2.Tables[0];
        dtnew = (DataTable)dtUnPubSL2.Clone();

        updateqry = "";

        if (dtUnPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandomNumber =Rand() where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

        dsUnPubSL2 = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select * from TempSBVBRandWords where PubUnp='Y' " + productstr + " and Sublevel=2 Order By RandomNumber");
        dtUnPubSL2 = dsUnPubSL2.Tables[0];
        dtnew = (DataTable)dtUnPubSL2.Clone();

        updateqry = "";
        if (dtUnPubSL2.Rows.Count > 0)
        {
            for (k = 0; k < dtUnPubSL2.Rows.Count; k++)
            {
                dtnew.ImportRow(dtUnPubSL2.Rows[k]);
                dtnew.Rows[k]["RandSeqID"] = k + 1;

                updateqry = updateqry + "update TempSBVBRandWords set RandSeqID =" + dtnew.Rows[k]["RandSeqID"] + " where OrigSeqID="
                      + dtnew.Rows[k]["OrigSeqID"].ToString().Trim() + " and PubUnp='Y' " + productstr + " and [Sublevel]=2  ";
            }
        }
        if (!updateqry.Equals(""))
        {
            SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
        }

    }
   

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {

        fillSet();
        fillProduct();
       
    }
    protected void fillProduct() { 
    
   
        //ddlProductGroup
        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
            
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            
            ddlProduct.DataSource = dsstate;
            ddlProductGroup.DataTextField = "ProductCode";
            ddlProduct.DataValueField = "Name";
            ddlProduct.DataBind();


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
        
    
    }
   
   
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e);
    }
    protected void fillSet()
    {
        if (ddlEvent.SelectedIndex == 1)
        {
            ddlSet.Items[0].Attributes.Add("style", "display:none");
            ddlSet.Items[1].Attributes.Add("style", "display:none");
            ddlSet.Items[2].Attributes.Add("style", "display:none");
            ddlSet.Items[3].Attributes.Add("style", "display:none");
            ddlSet.SelectedIndex = 4;
            ddlSet.Enabled = false;
        }
        else
        {
            ddlSet.Items[4].Attributes.Add("style", "display:none");
            ddlSet.SelectedIndex = 0;
            ddlSet.Enabled = true;
        }
    }

    protected void insertintoTables()
    {
           string sql = "";
           if (ddlOutput.SelectedIndex == 1)
           {
              
               DataTable dt = (DataTable)Session["GeneratedRandomWords"];
               for (int i = 0; i < dt.Rows.Count; i++)
               {

                   if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Year"] = "null";
                   }
                   if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["EventID"] = "null";
                   }
                   if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["SetId"] = "null";
                   }
                   if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductGroupID"] = "null";
                   }
                   if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductGroupCode"] = "null";
                   }
                   if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductID"] = "null";
                   }
                   if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductCode"] = "null";
                   }
                   if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["PubUnp"] = "null";
                   }
                   if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Level"] = "null";
                   }
                   if (dt.Rows[i]["SubLevel"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["SubLevel"] = "null";
                   }
                   if (dt.Rows[i]["Word"].ToString().Contains("'"))
                   {
                       dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                   }
                   sql = sql + "insert into SBVBRandWords (Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                         + dt.Rows[i]["Year"].ToString().Trim() + ","
                         + dt.Rows[i]["EventID"].ToString().Trim() + ","
                         + dt.Rows[i]["SetId"].ToString().Trim() + ","
                         + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                         + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["ProductCode"].ToString().Trim() + "',case '"
                          + dt.Rows[i]["PubUnp"].ToString().Trim() + "' when 'null' then null when 'Y' then 'Y' end,"
                         + dt.Rows[i]["Level"].ToString().Trim() + ","
                         + dt.Rows[i]["SubLevel"].ToString().Trim() + ","
                         + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                         + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                         + Session["LoginID"] + ")";

               }

           }
           else if (ddlOutput.SelectedIndex == 2)
           {
               DataTable dt = (DataTable)Session["GenTPWords"];


               for (int i = 0; i < dt.Rows.Count; i++)
               {

                   if (dt.Rows[i]["Year"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Year"] = "null";
                   }
                   if (dt.Rows[i]["EventID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["EventID"] = "null";
                   }
                   if (dt.Rows[i]["SetId"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["SetId"] = "null";
                   }
                   if (dt.Rows[i]["Phase"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Phase"] = "null";
                   }
                   if (dt.Rows[i]["RoundType"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["RoundType"] = "null";
                   }
                   if (dt.Rows[i]["ProductGroupID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductGroupID"] = "null";
                   }
                   if (dt.Rows[i]["ProductGroupCode"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductGroupCode"] = "null";
                   }
                   if (dt.Rows[i]["ProductID"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductID"] = "null";
                   }
                   if (dt.Rows[i]["ProductCode"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["ProductCode"] = "null";
                   }
                   if (dt.Rows[i]["PubUnp"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["PubUnp"] = "null";
                   }
                   if (dt.Rows[i]["Level"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Level"] = "null";
                   }
                   if (dt.Rows[i]["Sub-Level"].ToString().Equals(string.Empty))
                   {
                       dt.Rows[i]["Sub-Level"] = "null";
                   }
                   if (dt.Rows[i]["Word"].ToString().Contains("'"))
                   {
                       dt.Rows[i]["Word"] = dt.Rows[i]["Word"].ToString().Replace("'", " ");
                   }
                   sql = sql + "insert into SpVocabTPWords (Year,EventID,SetID,Phase,RoundType,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[Sub-Level],OrigSeqID,RandSeqID,Word,CreatedDate, CreatedBy) values("

                         + dt.Rows[i]["Year"].ToString().Trim() + ","
                         + dt.Rows[i]["EventID"].ToString().Trim() + ","
                         + dt.Rows[i]["SetId"].ToString().Trim() + ","
                         + dt.Rows[i]["Phase"].ToString().Trim() + ",'"
                         + dt.Rows[i]["RoundType"].ToString().Trim() + "',"
                         + dt.Rows[i]["ProductGroupID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["ProductGroupCode"].ToString().Trim() + "',"
                         + dt.Rows[i]["ProductID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["ProductCode"].ToString().Trim() + "',case '"
                          + dt.Rows[i]["PubUnp"].ToString().Trim() + "' when 'null' then null when 'Y' then 'Y' end,"
                         + dt.Rows[i]["Level"].ToString().Trim() + ","
                         + dt.Rows[i]["Sub-Level"].ToString().Trim() + ","
                         + dt.Rows[i]["OrigSeqID"].ToString().Trim() + ","
                         + dt.Rows[i]["RandSeqID"].ToString().Trim() + ",'"
                         + dt.Rows[i]["Word"].ToString().Trim() + "',getDate(),"
                         + Session["LoginID"] + ")";

               }
           }

      
        int j = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, sql);
 
        if (j > 0 )
        {
            lblErr.Visible = true;
            lblErr.ForeColor = Color.Blue;
            lblErr.Text = "Saved Successfully";
        }
    }
    protected void btnSaveWords_Click(object sender, EventArgs e)
    {
        string strqry = "";
        if (ddlOutput.SelectedIndex == 1)
        {
            strqry = "select * from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            strqry = "select * from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
        }
        
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            DateTime dt=DateTime.Now;
            // Checking Test Paper Approved or not
            string strApprovedTP = "select * from TestPapers where ContestYear=" + ddlYear.SelectedValue + " and EventId=" + ddlEvent.SelectedValue + " and SetNum=" + ddlSet.SelectedValue + " and ProductGroupCode ='" + ddlProductGroup.SelectedValue + "' and ProductCode=case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and DocType = 'TestP' ";
            DataSet dsApprovedStatus = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strApprovedTP);

            // Checking current date >= Contest date (based on the setID) minus 14 days; 
            string strContestDate = "select SatDay1 from WeekCalendar where WeekID=" + ddlSet.SelectedValue + "";
            DataSet dsCheckDate = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strContestDate);
            if(dsCheckDate.Tables[0].Rows.Count > 0)
            {
                dt = (DateTime)dsCheckDate.Tables[0].Rows[0]["SatDay1"];
                dt = dt.AddDays(-14).Date;
            }



            if (dsApprovedStatus.Tables[0].Rows.Count > 0)
            {
                //System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "notallowedtoOverwrite();", true);
                lblErr.Text = "Cannot override, since test paper was already uploaded";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else if (DateTime.Now.Date.CompareTo(Convert.ToDateTime(dt)) > 0)
            {
                lblErr.Text = "Cannot override, since current date is Greater than Contest date ";
                lblErr.Visible = true;
                lblErr.ForeColor = Color.Red;
            }
            else
            {
                System.Web.UI.ScriptManager.RegisterStartupScript(this, this.GetType(), "temp", "confirmtooverwrite();", true);
            }

        }
        else
        {
            insertintoTables();
        }
        
    }
    protected void ddlProduct_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void btnExportExcel_Click(object sender, EventArgs e)
    {

        if (ddlOutput.SelectedIndex == 1)
        {
            DataTable dtRandWords = (DataTable)Session["GeneratedRandomWords"];

            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=GenerateRandomWords.xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtRandWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated Random Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtRandWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtRandWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtRandWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            DataTable dtGenTPWords = (DataTable)Session["GenTPWords"];

            Response.Clear();

            Response.AppendHeader("content-disposition", "attachment;filename=GenerateTPWords.xls");

            Response.Charset = "";
            Response.Write("\n<center><table border=1><tr style='height:35px'><td colspan=" + (dtGenTPWords.Columns.Count) + " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'>Generated TP Words </td></tr>");
            Response.Write("<tr style='color:black;font-weight:bold;text-align:center;vertical-align:middle;'>");
            foreach (DataColumn dc in dtGenTPWords.Columns)
            {


                Response.Write("<th>" + dc.ColumnName + "</th>");

            }
            Response.Write("</tr>");

            Response.Write(System.Environment.NewLine);
            foreach (DataRow dr in dtGenTPWords.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtGenTPWords.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString().Replace("\t", " ") + "</td>");
                }
                Response.Write("</tr>");
            }

            Response.Write("</table></center>");
            Response.Flush();
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();
        }
    }
    protected void hiddenbtn_Click(object sender, EventArgs e)
    {
        try
        {
            //Response.Write("before update");

            string todelete = "";

            if (ddlOutput.SelectedIndex == 1)
            {

                todelete = "delete from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year="+ddlYear.SelectedValue+"";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }
            else if (ddlOutput.SelectedIndex == 2)
            {
                todelete = "delete from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, todelete);
                insertintoTables();
            }

            
            
          
        }
        catch (Exception ex)
        {

        }
    }
    protected void btnexporttopdf_Click(object sender, EventArgs e)
    {
       
        
       //report_JudgeCopy();
       report_AnswerKey();
       
    }
    
    protected void report_JudgeCopy()
    {
        string strquery = "";
        // Judge Copy Phase1 & Phase2
        strquery = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 10 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) union(select distinct top 35 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=2  )union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;

        }

        rpToPDF.DataSource = dsJVB;
        rpToPDF.DataBind();



        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + ddlSet.SelectedValue + "_" + ddlYear.SelectedValue + "_" + ddlProductGroup.SelectedValue + "_" + ddlProduct.SelectedValue + "_TestP_05_JudgesCopy_Ph1_Ph2.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();



        int i = 0;
        int pno = 1;
        int pnoPh2 = 1;
        string pageTitle = "";
        for (i = 0; i <= rpToPDF.Items.Count - 1; i++)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);

            if (i > 0)
            {
                if (i == 10 || i == 20 || i == 25 || i == 35 || i == 45 || i == 55 || i == 65 || i == 75 || i == 85)
                {
                    
                    if (pno == 2)
                    {
                        pageTitle = "Phase I: Junior Vocab Words - Judge Copy (Unpublished) - Pg " + pno + " of 3 ";
                    }
                    else if (pno == 3)
                    {
                        pageTitle = "Phase I: Junior Vocab Words - Judge Copy (Unpublished) - TIE BREAKER WORDS - Pg " + pno + " of 3";
                    }
                    else if (pno == 4)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 5)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 6)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 7)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 8)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 9)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }
                    else if (pno == 10)
                    {
                        pdfDoc.NewPage();
                        pageTitle = "Phase II: Junior Vocab Words - Pub / Judge Copy - Pg " + pnoPh2 + " of 7";
                        pnoPh2++;
                    }

                    pg1 = new iTextSharp.text.Paragraph(pageTitle);
                    pg2 = new iTextSharp.text.Paragraph(" ");
                    pdfDoc.Add(pg1);
                    pdfDoc.Add(pg2);
                    pno++;
                    StringWriter swHeader = new StringWriter();
                    HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                    pTableHeader.RenderControl(hwHeader);
                    StringReader srHeader = new StringReader(swHeader.ToString());
                    htmlparser.Parse(srHeader);
                }
            }
            rpToPDF.Items[i].RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            if (i == 0)
            {
                pg1 = new iTextSharp.text.Paragraph("Phase I: Junior Vocab Words - Judge Copy (Published) - Pg " + pno + " of 3");
                pdfDoc.Add(pg1);
                pg2 = new iTextSharp.text.Paragraph(" ");
                pdfDoc.Add(pg2);
                pno++;
                StringWriter swHeader = new StringWriter();
                HtmlTextWriter hwHeader = new HtmlTextWriter(swHeader);
                pTableHeader.RenderControl(hwHeader);
                StringReader srHeader = new StringReader(swHeader.ToString());
                htmlparser.Parse(srHeader);

            }
            htmlparser.Parse(sread);
        }



        // Phase2 StudentCopy appending with Judge copy Ph1&2

        string strqryStuPh2 = "";
        // Judge Copy Phase1 & Phase2
        strqryStuPh2 = "(select distinct top 35 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=2  )union(select distinct top 35 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 6 and 190 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=2  ) Order by sp.Phase,sp.[Sub-level]";
        DataSet dsJVBStuPh2 = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqryStuPh2);
        //adding serial number to dataTable
        DataColumn column1 = dsJVBStuPh2.Tables[0].Columns.Add("SNo", typeof(Int32));
        column1.SetOrdinal(0);// to put the column in position 0;
        int k1 = 26;
        foreach (DataRow dr in dsJVBStuPh2.Tables[0].Rows)
        {
            dr["SNo"] = k1;
            k1++;

        }

        rpStuPhase2.DataSource = dsJVBStuPh2;
        rpStuPhase2.DataBind();

        //int ii = 0;
        //int stupno = 1;
        //string pageTitleStuPh2 = "";
        //for (ii = 0; ii <= rpStuPhase2.Items.Count - 1; ii++)
        //{
        //    StringWriter sw1 = new StringWriter();
        //    HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
        //    pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);

        //    if (ii > 0)
        //    {
        //        if (ii == 30 || ii == 60)
        //        {
        //            //pdfDoc.NewPage();
        //            if (stupno == 2)
        //            {
        //                pageTitleStuPh2 = "Phase II: Junior Vocab Words - Pub / Student Copy - Pg " + stupno + " of 3 ";
        //            }
        //            else if (stupno == 3)
        //            {
        //                pageTitleStuPh2 = "Phase II: Junior Vocab Words - Pub / Student Copy - Pg " + stupno + " of 3";
        //            }

        //            pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
        //            pg2 = new iTextSharp.text.Paragraph(" ");
        //            pdfDoc.Add(pg1);
        //            pdfDoc.Add(pg2);
        //            stupno++;

        //        }
        //    }
        //    rpStuPhase2.Items[ii].RenderControl(hw1);
        //    string repeaterTable = sw1.ToString();
        //    StringReader sread = new StringReader(repeaterTable);
        //    if (ii == 0)
        //    {
        //        pg1 = new iTextSharp.text.Paragraph("Phase II: Junior Vocab Words - Pub / Student Copy  - Pg " + stupno + " of 3");
        //        pdfDoc.Add(pg1);
        //        pg2 = new iTextSharp.text.Paragraph(" ");
        //        pdfDoc.Add(pg2);
        //        stupno++;


        //    }
        //    htmlparser.Parse(sread);
        //}
        if (rpStuPhase2.Items.Count > 0)
        {
            pdfDoc.NewPage();

            pg1 = new iTextSharp.text.Paragraph("Phase II: Junior Vocab Words - Pub / Student Copy  - Pg 1 of 3");
            pg2 = new iTextSharp.text.Paragraph(" ");
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);
            //Header
            StringWriter swStuPhase2Header = new StringWriter();
            HtmlTextWriter hwStuPhase2Header = new HtmlTextWriter(swStuPhase2Header);
            pStuPh2Header.RenderControl(hwStuPhase2Header);
            StringReader srStuPhase2Header = new StringReader(swStuPhase2Header.ToString());
            htmlparser.Parse(srStuPhase2Header);

            //Records
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);
            rpStuPhase2.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);
            htmlparser.Parse(sread);
        }
        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }

    protected void report_AnswerKey()
    {
        string strquery = "";
        // Judge Copy Phase1 & Phase2
        strquery = "(select distinct top 5 '1/E' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=1 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 5 ) and sp.PubUnp is null and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 10 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 1 and 10 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  )union(select distinct top 5 '1/D' as Level,sp.Phase,sp.[Sub-level],sp.Word,vn.A,vn.B,vn.C,vn.D,vn.E,vn.Answer,vn.Meaning from SpVocabTPWords sp inner join VocabWords_new vn on sp.Word=vn.Word where sp.Year=" + ddlYear.SelectedValue + " and sp.Eventid=" + ddlEvent.SelectedValue + " and sp.ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and sp.ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and ([RandSeqID] between 11 and 15 ) and sp.PubUnp='Y' and sp.Level=1 and sp.[Sub-level]=2 and sp.SetID=" + ddlSet.SelectedValue + " and sp.Phase=1  ) Order by sp.Phase,sp.[Sub-level]";
        DataSet dsJVB = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strquery);
        //adding serial number to dataTable
        DataColumn column = dsJVB.Tables[0].Columns.Add("SNo", typeof(Int32));
        column.SetOrdinal(0);// to put the column in position 0;
        int k = 1;
        foreach (DataRow dr in dsJVB.Tables[0].Rows)
        {
            dr["SNo"] = k;
            k++;
        }


        rpAnswerKey.DataSource = dsJVB;
        rpAnswerKey.DataBind();



        Response.ContentType = "application/pdf";
        Response.AddHeader("content-disposition", "attachment;filename=Set" + ddlSet.SelectedValue + "_" + ddlYear.SelectedValue + "_" + ddlProductGroup.SelectedValue + "_" + ddlProduct.SelectedValue + "_TestP_05_AnswerKey_Ph1.pdf");
        Response.Cache.SetCacheability(HttpCacheability.NoCache);
        StringWriter sw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(sw);
        iTextSharp.text.Document pdfDoc = new iTextSharp.text.Document(iTextSharp.text.PageSize.A4, 20, 20, 20, 20);
        iTextSharp.text.html.simpleparser.HTMLWorker htmlparser = new iTextSharp.text.html.simpleparser.HTMLWorker(pdfDoc);
        iTextSharp.text.pdf.PdfWriter.GetInstance(pdfDoc, Response.OutputStream);

        StringWriter swAnsKey = new StringWriter();
        HtmlTextWriter hwAnsKey = new HtmlTextWriter(swAnsKey);
        iTextSharp.text.Paragraph pg1;
        iTextSharp.text.Paragraph pg2;

        pdfDoc.Open();

        int ii = 0;
        int stupno = 1;
        string pageTitleStuPh2 = "";
        if (rpAnswerKey.Items.Count > 0)
        {
            StringWriter sw1 = new StringWriter();
            HtmlTextWriter hw1 = new HtmlTextWriter(sw1);
            pdfDoc.SetPageSize(iTextSharp.text.PageSize.A4);


            pageTitleStuPh2 = "" + ddlYear.SelectedValue + " North South Foundation Regionals Junior Vocabulary Bee Phase I (Answer Key)";


            pg1 = new iTextSharp.text.Paragraph(pageTitleStuPh2);
            pg2 = new iTextSharp.text.Paragraph(" ");
            pdfDoc.Add(pg1);
            pdfDoc.Add(pg2);

            stupno++;



            rpAnswerKey.RenderControl(hw1);
            string repeaterTable = sw1.ToString();
            StringReader sread = new StringReader(repeaterTable);

            htmlparser.Parse(sread);

        }

        pdfDoc.Close();
        Response.Write(pdfDoc);
        Response.End();
    }
    protected void lnkBtnNext_Click(object sender, EventArgs e)
    {
        
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) + 5);
        generateTestPapers();
    }
    protected void lnkBtnPrev_Click(object sender, EventArgs e)
    {
        ViewState["increValue"]=Convert.ToInt32(ViewState["increValue"].ToString()) - 10;
        txtHidden.Value = Convert.ToString(Convert.ToInt16(txtHidden.Value) - 5);
        generateTestPapers();
    }
    protected void rpTestPaper_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

        
            ic = Convert.ToInt32(ViewState["increValue"].ToString()) + 1;
            Label lblSNo = default(Label);
            lblSNo = (Label)e.Item.FindControl("lblSNo");
            lblSNo.Text = Convert.ToString(ic);

            ViewState["increValue"] = ic;
       
    }
    protected void lnkClickHere1_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        DataSet ds;
        if (ddlOutput.SelectedIndex == 1)
        {
            ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,SubLevel,OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from SBVBRandWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + "");
        }
        else 
        {
            ds = SqlHelper.ExecuteDataset(Application["Connectionstring"].ToString(), CommandType.Text, "select Year,EventID,SetID,ProductGroupID,ProductGroupCode, ProductID, ProductCode,PubUnp,Level,[Sub-Level],OrigSeqID,RandSeqID,Word, convert(varchar(20),CreatedDate,101) as CreatedDate, CreatedBy,convert(varchar(20),ModifiedDate,101) as ModifiedDate,ModifiedBy from spVocabTPWords where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and ProductCode= case '" + ddlProduct.SelectedValue + "' when 'Junior Vocabulary' then 'JVB' when 'Intermediate Vocabulary' then 'IVB' when 'Junior Spelling' then 'JSB' when 'Senior Spelling' then 'SSB' end and SetID=" + ddlSet.SelectedValue + " and Year=" + ddlYear.SelectedValue + " Order By PubUnp, Level, [Sub-Level],RandSeqID");
        }
        //DataTable dtEasy = dsEasy.Tables[0];
        if (ds.Tables[0].Rows.Count > 0)
        {
            gvPubUnpubList.DataSource = ds.Tables[0];
            gvPubUnpubList.DataBind();
            gvPubUnpubList.Visible = true;
            if (ddlOutput.SelectedIndex == 1)
            {
                Session["GeneratedRandomWords"] = ds.Tables[0];
            }
            else
            {
                Session["GenTPWords"] = ds.Tables[0];
            }

            btnSaveWords.Enabled = true;
            btnExportExcel.Enabled = true;
            divTestPapers.Visible = false;
        }
    }
    protected void lnkClickHere2_Click(object sender, EventArgs e)
    {
        lblErr.Text = "";
        lblErr1.Text = "";
        lnkClickHere1.Visible = false;
        lnkClickHere2.Visible = false;
        if (ddlOutput.SelectedIndex == 1)
        {
            generatingRandomWords();
            displayPubUnpubList();

        }
        else if (ddlOutput.SelectedIndex == 2)
        {
            generateTPWords();
        }
        btnSaveWords.Enabled = true;
        btnExportExcel.Enabled = true;
        divTestPapers.Visible = false;
    }

    public override void VerifyRenderingInServerForm(Control control)
    {
        //Required to verify that the control is rendered properly on page
    }
   
    protected void rpToPDF_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {

       
        icTemp = icTemp + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoTemp");
        lblSNo.Text = Convert.ToString(icTemp);

        

    }
    protected void rpStuPhase2_ItemDataBound(object sender, RepeaterItemEventArgs e)
    {


      



    }
    protected void Item_Bound(Object sender, DataListItemEventArgs e)
    {

        icStuPh2 = icStuPh2 + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("SNoStuPhase2");
        lblSNo.Text = Convert.ToString(icStuPh2);

    }
    protected void Item_Bound_AnsKey(Object sender, DataListItemEventArgs e)
    {

        icAnsKey = icAnsKey + 1;
        Label lblSNo = default(Label);
        lblSNo = (Label)e.Item.FindControl("lblSNoAnsKey");
        lblSNo.Text = Convert.ToString(icAnsKey)+".";

    }

    

   
   
}