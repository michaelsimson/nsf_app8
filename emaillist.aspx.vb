Imports NativeExcel
Imports System.Data.SqlClient
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports System.Data
Imports iTextSharp.text.pdf
Imports iTextSharp.text.pdf.parser
Imports System.Net.Mime.MediaTypeNames
Imports iTextSharp.text
Imports Microsoft.Office.Interop

' Created by :Shalini


Partial Class emaillist
    Inherits System.Web.UI.Page

    Dim attach As Attachment
    Dim ServerPath As String
    Dim fileExt As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("RoleID") = 1
        'Response.Write(Session("LoginEmail") + Session("RoleID").ToString())
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Session("EmailSource") = "Email_CC" Then  'if source is Email_CC, then hide from email dropdown 
            ClientScript.RegisterStartupScript(Me.GetType(), "Hide_fromdropdown", "<script language=""javascript"">document.getElementById('" & drpfrom.ClientID & "').style.display = 'none';</script>")
            Hypbackemail.NavigateUrl = "Email_CC.aspx"  ' Navigate Back to email hyperlink 
        ElseIf Session("EmailSource") = "Email_NC" Then 'if source is Email_NC, then hide from email textbox
            '** ferdine ClientScript.RegisterStartupScript(Me.GetType(), "Hide_fromtext", "<script language=""javascript"">document.getElementById('" & txtfrom.ClientID & "').style.display = 'none';</script>")
            Hypbackemail.NavigateUrl = "Email_NC.aspx"  ' Navigate Back to email hyperlink 
        ElseIf Session("EmailSource") = "Email_NFInvites" Then 'if source is Email_NC, then hide from email textbox
            ClientScript.RegisterStartupScript(Me.GetType(), "Hide_fromtext", "<script language=""javascript"">document.getElementById('" & txtfrom.ClientID & "').style.display = 'none';</script>")
            Hypbackemail.NavigateUrl = "Email_NFInvites.aspx"  ' Navigate Back to email hyperlink 
        ElseIf Session("EmailSource") = "EmailCoach" Then 'if source is Email_NC, then hide from email textbox
            '** ClientScript.RegisterStartupScript(Me.GetType(), "Hide_fromtext", "<script language=""javascript"">document.getElementById('" & txtfrom.ClientID & "').style.display = 'none';</script>")
            Hypbackemail.NavigateUrl = "EmailCoach.aspx"  ' Navigate Back to email hyperlink 
            Session("mailEventID") = "13"
        ElseIf Session("EmailSource") = "Email_TC" Then 'if source is Email_TC, then hide from email textbox
            'Added on 16-04-2014
            Hypbackemail.NavigateUrl = "Email_TC.aspx"  ' Navigate Back to email hyperlink 
        ElseIf Session("EmailSource") = "Email_OnlineWS" Then
            Hypbackemail.NavigateUrl = "Email_OnLineWkShop.aspx"
        ElseIf Session("EmailSource") = "EmailFundR" Then
            Hypbackemail.NavigateUrl = "EmailFundRaising.aspx"
        ElseIf Session("EmailSource") = "Email_HR" Then
            Hypbackemail.NavigateUrl = "SendEmail_HR.aspx"
        End If


        If Not Page.IsPostBack Then
            txtSendList.Text = Session("emaillist")  ' sentemaillist if not page post  back
            If Session("EmailSource") = "Email_CC" Then  'from email if not page post  back
                txtfrom.Text = Convert.ToString(Session("LoginEmail"))
            ElseIf Session("EmailSource") = "Email_NC" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_NFInvites" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_TC" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "EmailFundR" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "EmailCoach" And Not drpfrom.SelectedItem.Text = "Select From Email" And txtfrom.Text.Length = 0 Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_HR" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            End If
        Else
            txtSendList.Text = txtSendList.Text  ' sentemaillist if page post  back

            If Session("EmailSource") = "Email_CC" Then 'from email if page post back
                txtfrom.Text = txtfrom.Text
            ElseIf Session("EmailSource") = "Email_NC" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_NFInvites" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_TC" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_OnlineWS" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "EmailFundR" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "EmailCoach" And Not drpfrom.SelectedItem.Text = "Select From Email" And txtfrom.Text.Length = 0 Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            ElseIf Session("EmailSource") = "Email_HR" And Not drpfrom.SelectedItem.Text = "Select From Email" Then
                txtfrom.Text = drpfrom.SelectedItem.Text
            End If

        End If
        'Jan23
        Try

            'If roleID is 5,88,89,96, then insert nsfemail from the IndSpouse table and disable the From field. If nsfemail is null,
            'then take stem of the Email column (before @ character) and append northsouth.org to it to insert into
            'the From field.
            If (Session("RoleID") = 5 Or Session("RoleID") = 88 Or Session("RoleID") = 89 Or Session("RoleID") = 96) Then
                'Dim nsfemail As String = (SqlHelper.ExecuteScalar(Application("ConnectionString").ToString, CommandType.Text, "select nsfemail from indspouse where email='" & Session("LoginEmail") & "'")).ToString()
                'If nsfemail = "" Then

                Dim LoginEmail As String = Session("LoginEmail")
                Dim AtIndex As Integer = LoginEmail.IndexOf("@")
                Dim NewNSFEmail = LoginEmail.Substring(0, AtIndex) + "@northsouth.org"
                txtfrom.Text = NewNSFEmail
                txtfrom.Enabled = False
                drpfrom.Enabled = False
            Else

            End If

        Catch ex As Exception

        End Try

        lblerr.Text = ""
        lblfromemailerror.Text = ""

        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then  ' for attachment
            '  Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            attach = New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName)
        End If
        If Not ccemaillist.Text.Trim() = "" Then
            Dim s As String = ccemaillist.Text
            Dim words As String() = s.Split(",")
            Dim word As String
            Dim CCList(words.Length - 1) As String
            Dim i As Integer
            i = 0
            For Each word In words
                word = word.Trim()
                CCList(i) = word
                i = i + 1
            Next
            Dim value As String = String.Join(",", CCList)
            txtSendList.Text = txtSendList.Text + "," + value
        End If
        lblEmailCount.Text = "# of Emails to be sent: " & EmailCount(txtSendList).ToString()
        If Not Session("ParentChildMailCnt") Is Nothing Then
            Dim strCnt As String = Session("ParentChildMailCnt")
            lblCnt.Text = "(# of Parents : " & strCnt.Substring(0, strCnt.IndexOf(":"))
            lblCnt.Text = lblCnt.Text & " ; # of Students :" & strCnt.Substring(strCnt.IndexOf(":")) & ")"
        End If
        ' Response.Write(Session("ChapterID").ToString() & "<br>")
        If Session("EmailSource") = "Email_NC" Or Session("EmailSource") = "EmailCoach" Or Session("EmailSource") = "Email_OnlineWS" Then
            Dim sqlstr As String = "Select ChapterID from Indspouse where automemberid=" & Session("LoginID").ToString
            Dim strChapterid As String = Convert.ToString(SqlHelper.ExecuteScalar(Application("ConnectionString").ToString, CommandType.Text, sqlstr))
            Session("mailChapterID") = strChapterid
        ElseIf Session("EmailSource") = "Email_NFInvites" Then
            Session("mailChapterID") = "1"
            Session("mailEventID") = "1"
        End If

        If (Session("mailEventID") = "13" Or Session("mailEventID") = "20") Then
            Session("mailChapterID") = "109"
        End If

        If Session("sentemaillistenable") = "Yes" Then  'To enable sentemaillist textbox if own list radio button is checked in Previous page
            lblsentemailnote.Visible = True
            txtSendList.Enabled = True
            If Not Session("ExcelFileUploadedorNot") Is Nothing Then
                If Session("ExcelFileUploadedorNot") = "Yes" Then
                    txtSendList.Text = Session("emaillist")
                End If
            End If

        ElseIf Session("sentemaillistenable") = "No" Then
            lblsentemailnote.Visible = False
            txtSendList.Enabled = False
        End If

    End Sub
    Protected Sub fillOwnEmail_list()
        'Dim oWorkbooks As IWorkbook = NativeExcel.Factory.OpenWorkbook("D:/NSF/App8Full/Email_ownlist.xlsx")
        Dim oWorkbooks As IWorkbook = NativeExcel.Factory.OpenWorkbook(Server.MapPath("~/UploadFiles/tempEmail.xlsx"))
        Dim oSheet As IWorksheet
        oSheet = oWorkbooks.Worksheets(1)
        Dim cells As IRange
        cells = oSheet.UsedRange

        Dim stremaillist As String
        stremaillist = ""
        Dim i As Integer
        Dim j As Integer
        For i = 2 To cells.Rows.Count
            For j = 1 To cells.Columns.Count
                stremaillist = stremaillist & cells(i, j).Value.ToString().Trim() & ","
            Next
        Next
        stremaillist = stremaillist.Substring(0, stremaillist.Length - 1)
        txtSendList.Text = stremaillist
    End Sub


    Protected Sub btnsendemail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsendemail.Click
        'For other RoleIDs, If null, allow the user to enter the email address
        'into the From field, but validate. If the part after @ doesn�t match northsouth.org, give an error
        'message.
        Try

            If (Session("RoleID") <> 5 And Session("RoleID") <> 88 And Session("RoleID") <> 89 Or Session("RoleID") <> 96) Then
                Dim enteredNSFEmail As String = txtfrom.Text
                If Not enteredNSFEmail.EndsWith("@northsouth.org") Then
                    lblerr.Text = "Email ID should end with @northsouth.org"
                    Exit Sub
                End If
            End If

        Catch ex As Exception
        End Try

        'will send email to selected emails

        Dim strEmailAddressList() As String
        Dim item As Object
        Dim sbEmailSuccess As New StringBuilder
        Dim sbEmailFailure As New StringBuilder
        Dim mm As MailMessage
        Dim pattern As String = "^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$" ' validate the from email both in dropdown and in textbox depending on the source
        Dim emailAddressMatch As Match = Regex.Match(txtfrom.Text, pattern)

        If Not emailAddressMatch.Success Then
            lblfromemailerror.Text = "You must enter an Correct email address"
        ElseIf txtfrom.Text = "" Then
            lblerr.Text = "From email should not be empty"
        Else
            If txtSendList.Text = "" Then
                lblerr.Text = "Send email list is empty"
            Else
                If txtEmailSubject.Text = "" Then
                    lblerr.Text = "Subject line should not be blank"
                Else
                    If txtEmailBody.Text = "" Then
                        lblerr.Text = "Message Text box should not be blank"
                    Else

                        btnsendemail.Enabled = False
                        strEmailAddressList = Split(txtSendList.Text, ",")

                        Dim Count As Integer = 0
                        Dim SentEmailsCount As Integer = 0
                        Dim TotalEmailsCount As Integer = 0

                        For Each item In strEmailAddressList
                            TotalEmailsCount += 1  ' total number of emails in sent email list
                        Next

                        Dim conn As New SqlConnection(Application("ConnectionString"))
                        ViewState("Nowtime") = Now()
                        'will insert after validating the all controls in page and start sending the 1st email
                        Dim EmailSubj As String = txtEmailSubject.Text.Replace("'", "''")
                        If Session("mailEventID") = Nothing Then
                            Session("mailEventID") = "0"
                        End If
                        If Session("mailChapterID") = Nothing Then
                            Session("mailChapterID") = "0"
                        End If

                        Dim str As String
                        If Convert.ToString(Session("mailEventID")) = "2,13" Then
                            str = "insert into SentEmailLog(Source,EventID,ChapterID,LoginEmail,FromEmail,EmailSubject,RoleID,MemberID,StartTime,TotalEmails_Count) values( '" + Convert.ToString(Session("EmailSource")) + "',2," + Convert.ToString(Session("mailChapterID")) + ",'" + Convert.ToString(Session("LoginEmail")) + "','" + txtfrom.Text + "','" + EmailSubj + "','" + Convert.ToString(Session("RoleID")) + "','" + Convert.ToString(Session("LoginID")) + "','" + ViewState("Nowtime") + "','" + TotalEmailsCount.ToString + "')"
                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str)
                        Else
                            str = "insert into SentEmailLog(Source,EventID,ChapterID,LoginEmail,FromEmail,EmailSubject,RoleID,MemberID,StartTime,TotalEmails_Count) values( '" + Convert.ToString(Session("EmailSource")) + "'," + Convert.ToString(Session("mailEventID")) + "," + Convert.ToString(Session("mailChapterID")) + ",'" + Convert.ToString(Session("LoginEmail")) + "','" + txtfrom.Text + "','" + EmailSubj + "','" + Convert.ToString(Session("RoleID")) + "','" + Convert.ToString(Session("LoginID")) + "','" + ViewState("Nowtime") + "','" + TotalEmailsCount.ToString + "')"
                            SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str)
                        End If

                        Dim emailadd As String
                        Try
                            Dim eBody As String = " <a href='https://northsouth.org" + Request.ApplicationPath.ToString() + "/EmailNotification.aspx'>Unsubscribe</a> from this list.  Please note:  Being a NSF patron, we would like you to be a brand ambassador to forward NSF emails to other young parents so that they can take the opportunity to participate in various NSF activities.  Once you unsubscribe, we lose your contact permanently and will no longer be able to communicate with you. Thanks."

                            For Each item In strEmailAddressList
                                emailadd = Trim(item.ToString.Replace(vbCrLf, ""))
                                If (Count = 500) Then  ' for every 500 emails sent
                                    ''Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert(' " & SentEmailsCount & "message sent ');"), True)
                                    ''**System.Threading.Thread.Sleep(10000) ' will pause for 10 seconds
                                    Count = 0
                                End If

                                If EmailAddressCheck(emailadd) Then
                                    mm = GetMailMessage(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), emailadd, txtfrom.Text)
                                    ' txtEmailBody.Text = Replace(txtEmailBody.Text, """", "'")

                                    If SendEmail(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''") + eBody, emailadd, txtfrom.Text, mm) = True Then ''** 
                                        sbEmailSuccess.Append("Email Send To:" & strEmailAddressList.Length & "<BR>")
                                        Count += 1
                                        SentEmailsCount += 1  ' number of emails sent successfully from email list 
                                    End If
                                Else
                                    sbEmailFailure.Append("Email Failed To:" & emailadd & "<BR>")
                                End If
                            Next
                            mm = GetMailMessage(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''"), Session("LoginEmail"), txtfrom.Text)

                            SendEmail(Replace(txtEmailSubject.Text, "'", "''"), Replace(txtEmailBody.Text, "'", "''") + eBody, Session("LoginEmail"), txtfrom.Text, mm)
                        Catch
                            Response.Write("Error Occured: <BR>")
                        End Try
                        lblEMailError.Text = sbEmailFailure.ToString   'shows emailid, if failed to sent email 
                        'update the table row once, all emails are sent
                        Dim str1 As String = "update SentEmailLog Set Endtime = '" + Now() + "', SentEmails_Count = '" + SentEmailsCount.ToString + "' where  SentEmailLogID = (select max(sentemaillogID) from sentemaillog where memberid = '" + Convert.ToString(Session("LoginID")) + "')"
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str1)
                        Dim email As New MailMessage
                        email = New MailMessage(txtfrom.Text, Session("LoginEmail"))
                        SendEmail("NSF - Sent Email Status - " & Replace(txtEmailSubject.Text, "'", "''"), "Dear Sir,<br>" & ">>Subject : " & Replace(txtEmailSubject.Text, "'", "''") & "<br>" & SentEmailsCount & " emails were successfully sent.<br> NSF admin", Session("LoginEmail"), txtfrom.Text, email)

                        ''Added On 23-04-2014 to Send Email to login voluneer on the list of Ignored emails-Email_TC Application.
                        If Session("IgnoredEmailList") <> "" And Session("EmailSource") = "Email_TC" Then
                            SendEmail_IgnList("NSF - Ignored Email List", "", Session("LoginEmail"), txtfrom.Text, email)
                        End If

                        '**************************************************************************************************************'
                        lblmailSuccessCount.Text = "# of Emails sent: " & SentEmailsCount
                        If lblEMailError.Text = "" Then
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "statusMessage();", True)
                            lblstatus.Text = ""
                        Else
                            Page.ClientScript.RegisterStartupScript(Me.GetType(), "showmessage", Server.HtmlEncode("alert('Failed Sending to below Emails');"), True)
                            lblstatus.Text = ""
                        End If
                    End If
                End If
            End If
        End If
    End Sub

    Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal smailfrom As String, ByVal email As MailMessage) As Boolean ''**

        'Build Email Message
        'Dim email As New MailMessage
        email.From = New MailAddress(smailfrom)
        '   email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
        Dim ok As Boolean = True
        Try

            client.Timeout = 20000
            client.Send(email)
        Catch e As Exception
            lblerr.Text = lblerr.Text & " <br> " & sMailTo & " " & e.Message.ToString
            ok = False
            'Return False
        End Try
        Return ok

    End Function
    Private Function SendEmail_IgnList(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal smailfrom As String, ByVal email As MailMessage) As Boolean ''**

        'Build Email Message
        'Dim email As New MailMessage
        email.From = New MailAddress(smailfrom)
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure

        Dim StrBody As String = ""
        StrBody = "--------------------------------------------------------- <br />"
        StrBody = StrBody & " Please note that the following emails are on our vendor's suppression list.  So your email was not sent to them.  You need to handle them differently.<br />"
        StrBody = StrBody & " -----------------------------------------------------------------<br />"
        StrBody = StrBody & Session("IgnoredEmailList") & "<br />"

        email.Body = StrBody
        'leave blank to use default SMTP server
        Dim client As New SmtpClient()
        Dim ok As Boolean = True
        Try
            'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")

            client.Timeout = 20000
            client.Send(email)
        Catch e As Exception
            lblerr.Text = lblerr.Text & " <br> " & sMailTo & " " & e.Message.ToString
            ok = False
            'Return False
        End Try
        Return ok

    End Function

    Private Function GetMailMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sMailFrom As String) As MailMessage
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim mm As New MailMessage

        '(1) Create the MailMessage instance
        Try
            mm = New MailMessage(sMailFrom, sMailTo)
        Catch
            Response.Write("Email Address Having Problem is " & sMailTo & " From Address:" & sMailFrom)
        End Try

        '(2) Assign the MailMessage's properties
        mm.Subject = sSubject
        mm.Body = sBody
        mm.IsBodyHtml = True

        strEmailAddressList = Split(sMailTo, ",")
        Dim EmailAdd As String
        For Each item In strEmailAddressList
            'check if email address is valid
            EmailAdd = Trim(item.ToString.Replace(vbCrLf, ""))
            If EmailAddressCheck(EmailAdd) Then
                'mm.Bcc.Add(EmailAdd)
                mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            End If
        Next
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then
            ' Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            ' mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))
            mm.Attachments.Add(attach)
        End If
        Return mm

    End Function

    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        'Validate email address
        Try
            Dim pattern As String = "^[a-zA-Z0-9][\w\.-\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            '"^[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
            If emailAddressMatch.Success Then
                EmailAddressCheck = True
            Else
                EmailAddressCheck = False
            End If
        Catch ex As Exception
            EmailAddressCheck = False
        End Try
    End Function

    Function EmailCount(ByRef mailstring As TextBox) As Integer

        If Trim(mailstring.Text) <> String.Empty Then
            Dim MailArray As String() = mailstring.Text.Split(",")
            Return MailArray.Length()
        Else
            Return 0
        End If

    End Function

    Protected Sub insertfilecontentintoeditor_Click(sender As Object, e As EventArgs) Handles insertfilecontentintoeditor.Click
        If AttachmentFile.HasFile() Then
            lblFileUploadError.Visible = False
            ServerPath = Server.MapPath("~/UploadFiles/")

            Dim FileName As [String] = AttachmentFile.FileName()
            'txtEmailBody.Text = "<b>Hai</b>";
            fileExt = System.IO.Path.GetExtension(AttachmentFile.FileName)
            AttachmentFile.PostedFile.SaveAs(ServerPath + "tempEmail" + fileExt)
            If fileExt.Equals(".pdf") Then

                Dim pdfReader As New PdfReader(ServerPath + "tempEmail" + fileExt)

                For page As Integer = 1 To pdfReader.NumberOfPages
                    Dim strategy As ITextExtractionStrategy = New SimpleTextExtractionStrategy()
                    Dim currentText As String = PdfTextExtractor.GetTextFromPage(pdfReader, page, strategy)

                    currentText = Encoding.UTF8.GetString(ASCIIEncoding.Convert(Encoding.[Default], Encoding.UTF8, Encoding.[Default].GetBytes(currentText)))
                    'text.Append(currentText)
                    txtEmailBody.Text = txtEmailBody.Text & currentText
                Next
                pdfReader.Close()
            Else

                Dim content As String = ""
                Dim line As String = ""
                Dim reader As New StreamReader(ServerPath + "tempEmail" + fileExt)
                line = reader.ReadLine
                While line IsNot Nothing
                    content += line
                    line = reader.ReadLine
                End While
                reader.Close()
                txtEmailBody.Text = txtEmailBody.Text & content
            End If
        Else
            lblFileUploadError.Text = "Please choose file to Insert into Editor"
            lblFileUploadError.Visible = True
            lblFileUploadError.ForeColor = Color.Red

        End If

    End Sub
End Class
