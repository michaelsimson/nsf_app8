﻿<%@ Page Language="VB" AutoEventWireup="false" CodeFile="Event_RegDetails.aspx.vb" Inherits="Event_RegDetails" %>

<%@ Register Assembly="MSCaptcha" Namespace="MSCaptcha" TagPrefix="cc1" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
    </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />

    <link href="css/style.css" rel="stylesheet" type="text/css" />
    <style type="text/css">
        <!--
        body {
            margin-left: 0px;
            margin-top: 0px;
            margin-right: 0px;
            margin-bottom: 0px;
            text-align: center;
            width: 100%;
        }

        .style1 {
            color: #FFFFFF;
            font-family: "Arial Rounded MT Bold";
            font-size: 14px;
        }

        .rtbn td {
            font-size: 17px;
        }

        -->
    </style>

    <script src='https://www.google.com/recaptcha/api.js'></script>

</head>
<body>
    <form id="form1" runat="server">


        <table cellpadding="0" cellspacing="0" border="0" style="width: 100%; text-align: center">
            <tr>
                <td align="center">
                    <table border="1" cellpadding="0" cellspacing="0" bordercolor="#189A2C" style="width: 1000px; text-align: center">
                        <tr>
                            <td>
                                <table cellpadding="0" cellspacing="0" border="0" style="width: 1000px; text-align: center">
                                    <tr>
                                        <td>
                                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                <tr>
                                                    <td width="24%" valign="top" align="right">
                                                        <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                                                        <map name="Map" id="Map">
                                                            <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                                                        </map>
                                                    </td>
                                                    <td width="76%" valign="top">
                                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                            <tr>
                                                                <td>
                                                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                                                        <tr>
                                                                            <td width="51%" background="images_new/topbg_1.jpg">
                                                                                <img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                                                                            <td width="49%" valign="top" background="images_new/img_03.jpg">
                                                                                <table width="89%" border="0" cellspacing="0" cellpadding="0">
                                                                                    <tr>
                                                                                        <td width="30%">&nbsp;</td>
                                                                                        <td width="70%">
                                                                                            <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                                                                                <tr>
                                                                                                    <td valign="middle" style="padding-left: 10px; padding-top: 5px">
                                                                                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                                                                                            <Items>
                                                                                                            </Items>
                                                                                                        </asp:Menu>
                                                                                                    </td>
                                                                                                </tr>
                                                                                            </table>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td colspan="2" align="center">
                                                                                            <br />
                                                                                            <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server"></asp:Label></td>
                                                                                    </tr>
                                                                                </table>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                            <tr>
                                                                <td height="35" valign="middle" background="images_new/menubg.jpg">
                                                                    <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                        <tr>
                                                                            <td style="text-align: center; width: 10px"></td>
                                                                            <td style="vertical-align: middle; text-align: left;" class="style1">
                                                                                <asp:Label ID="lblPageCaption" runat="server" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" Text="Noble Cause Through Brilliant Minds!"></asp:Label>
                                                                                <asp:Literal ID="ltlEvents" runat="server"></asp:Literal>
                                                                            </td>
                                                                            <td style="text-align: center; width: 10px"></td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td bgcolor="#ffffff" align="center">
                                            <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                <tr>
                                                    <td align="left" class="rtbn">
                                                        <asp:RadioButtonList ID="rbtnUser" AutoPostBack="true" runat="server" CellSpacing="5"
                                                            RepeatDirection="Horizontal" CellPadding="10" Style="margin-left: 175px; font-size: 17px;">
                                                            <asp:ListItem Selected="True" Value="1">Existing User</asp:ListItem>
                                                            <asp:ListItem Value="2">New User</asp:ListItem>
                                                        </asp:RadioButtonList>

                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trlogin">
                                                    <td align="center">
                                                        <table border="0" cellpadding="0" cellspacing="0" align="center" width="70%">
                                                            <tr>
                                                                <td width="6px" height="6px" background="images/tl.gif"></td>
                                                                <td height="6px" background="images/t.gif"></td>
                                                                <td width="11px" height="6px" background="images/tr.gif"></td>
                                                            </tr>
                                                            <tr>
                                                                <td background="images/l.gif"></td>
                                                                <td align="center" style="font-size: 15px;">
                                                                    <table id="tblLogin" runat="server" border="0">
                                                                        <tr>
                                                                            <td colspan="3" valign="middle" align="left" class="txt01_strong">Existing NSF Donors - Please Sign On:
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" nowrap align="right">Email </td>
                                                                            <td align="left">
                                                                                <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" Width="300" CssClass="text1"></asp:TextBox><br>
                                                                                <asp:RequiredFieldValidator ID="RFVLEmail" runat="server" Display="Dynamic"
                                                                                    ErrorMessage="Enter Login Id." ControlToValidate="txtUserId"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                                                                                        ID="rgvEmail" runat="server" CssClass="smFont" Display="Dynamic"
                                                                                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId"
                                                                                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
                                                                            <td align="left">Click here if you
                <asp:HyperLink ID="Hyperlink1" runat="server" CssClass="btn_02" NavigateUrl="Forgot.aspx">forgot your password</asp:HyperLink></td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td valign="top" nowrap align="left">Password&nbsp;
                                                                            </td>
                                                                            <td align="left">
                                                                                <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" Width="300" CssClass="text1"
                                                                                    TextMode="Password"></asp:TextBox>
                                                                                <br />
                                                                                <asp:RequiredFieldValidator ID="RFVPwd"
                                                                                    runat="server" ErrorMessage="Enter Password." ControlToValidate="txtPassword"></asp:RequiredFieldValidator>
                                                                                <asp:Label ID="lblErr" runat="server"></asp:Label>

                                                                            </td>
                                                                            <td align="left"></td>
                                                                        </tr>
                                                                        <tr>

                                                                            <td colspan="2" align="center">
                                                                                <asp:Button ID="btnLogin" runat="server" OnClick="btnLogin_Click" Text="Login" Visible="False"></asp:Button></td>

                                                                            <td align="center"></td>
                                                                        </tr>
                                                                    </table>

                                                                </td>
                                                                <td background="images/r.gif"></td>
                                                            </tr>

                                                            <tr>
                                                                <td width="6px" height="12px" background="images/bl.gif"></td>
                                                                <td height="tpx" background="images/b.gif"></td>
                                                                <td width="11px" height="12px" background="images/br.gif"></td>
                                                            </tr>
                                                        </table>
                                                    </td>
                                                </tr>
                                                <tr runat="server" id="trNewlogin">
                                                    <td align="center">
                                                        <table runat="server" id="trall" visible="false" border="0" cellpadding="0" cellspacing="0" align="center" width="70%">
                                                            <tr>
                                                                <td width="6px" height="6px" background="images/tl.gif"></td>
                                                                <td height="6px" background="images/t.gif"></td>
                                                                <td width="11px" height="6px" background="images/tr.gif"></td>
                                                            </tr>
                                                            <tr>
                                                                <td background="images/l.gif"></td>
                                                                <td align="center" style="font-size: 15px;">
                                                                    <table cellpadding="1" id="tblinput" runat="server" cellspacing="3" border="0" width="100%">
                                                                        <tr>
                                                                            <td colspan="4" align="left" class="txt01_strong">New Donors - Please enter your informations: 
                                                                                <asp:Label ID="lblMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="left">First Name <span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span> </td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtFname" Width="175px" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left">Last Name<span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtLname" Width="175px" runat="server"></asp:TextBox></td>
                                                                        </tr>
                                                                        <tr bgcolor="#FFFFFF">
                                                                            <td colspan="4" align="left">
                                                                                <asp:CheckBox ID="chk1" Text="Keep my name anonymous" runat="server" /></td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="left">EMail  <span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="TxtEMail" Width="175px" runat="server"></asp:TextBox>

                                                                                <asp:RegularExpressionValidator ID="revPrimaryEmailInd" runat="server" ControlToValidate="txtEmail" Display="Dynamic"
                                                                                    ErrorMessage="Enter Valid EMail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                            <td align="left">Address <span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtAddress" TextMode="SingleLine" Width="175px" runat="server"></asp:TextBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="left">City<span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtCity" Width="175px" runat="server"></asp:TextBox>
                                                                            </td>
                                                                            <td align="left">State  <span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:DropDownList ID="ddlState" runat="server" Width="175px"></asp:DropDownList>
                                                                                <asp:TextBox ID="txtState" runat="server" Width="150px" Visible="false"></asp:TextBox>
                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="left">Country  </td>
                                                                            <td align="left">:
                                                                                <asp:DropDownList ID="ddlCountry" OnSelectedIndexChanged='ddlCountry_SelectedIndexChanged' runat="server" Width="175px" AutoPostBack="True">
                                                                                    <asp:ListItem Value="IN">India</asp:ListItem>
                                                                                    <asp:ListItem Value="US" Selected="True">United States</asp:ListItem>
                                                                                    <asp:ListItem Value="CA">Canada</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>
                                                                            <td align="left">Zip Code  <span style="color: Red; font-family: Arial; vertical-align: top">&nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtZip" Width="175px" runat="server"></asp:TextBox>
                                                                                <asp:RegularExpressionValidator ID="RegularExpressionValidator1" runat="server" ControlToValidate="txtZip"
                                                                                    ErrorMessage="Zip code wrong format" ValidationExpression="\d{5,6}(-\d{4})?"></asp:RegularExpressionValidator>

                                                                            </td>
                                                                        </tr>

                                                                        <tr runat="server" visible="false" id="trNewUser">
                                                                            <td align="left">Gender<span style="color: Red; font-family: Arial; vertical-align: top"> *</span></td>
                                                                            <td align="left">:
                                                                                <asp:DropDownList ID="ddlGenderInd" runat="server" CssClass="SmallFont">
                                                                                    <asp:ListItem Value=" ">Select Gender</asp:ListItem>
                                                                                    <asp:ListItem Value="Male">Male</asp:ListItem>
                                                                                    <asp:ListItem Value="Female">Female</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                            </td>

                                                                            <td align="left">Home Phone<span style="color: Red; font-family: Arial; vertical-align: top">
                   &nbsp;*</span></td>
                                                                            <td align="left">:
                                                                                <asp:TextBox ID="txtHomePhoneInd" runat="server" CssClass="SmallFont" Width="175px"></asp:TextBox>&nbsp;<br />
                                                                                &nbsp;&nbsp;  xxx-xxx-xxxx
                                                                                <br />
                                                                                <asp:RequiredFieldValidator ID="rfvHomePhoneInd" Enabled="false" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic" ErrorMessage="Home Phone Number is required."></asp:RequiredFieldValidator>


                                                                            </td>
                                                                        </tr>

                                                                        <tr>
                                                                            <td align="left">&nbsp;</td>
                                                                            <td align="left">&nbsp;</td>

                                                                            <td align="left" colspan="2">
                                                                                <asp:RegularExpressionValidator ID="revHomePhoneInd" runat="server" ControlToValidate="txtHomePhoneInd" Display="Dynamic"
                                                                                    ErrorMessage="Home Phone No should be in xxx-xxx-xxxx format" ValidationExpression="\d\d\d-\d\d\d-\d\d\d\d"></asp:RegularExpressionValidator>
                                                                            </td>
                                                                        </tr>
                                                                    </table>


                                                                </td>
                                                                <td background="images/r.gif"></td>
                                                            </tr>

                                                            <tr>
                                                                <td width="6px" height="12px" background="images/bl.gif"></td>
                                                                <td height="tpx" background="images/b.gif"></td>
                                                                <td width="11px" height="12px" background="images/br.gif"></td>
                                                            </tr>
                                                        </table>

                                                        <table id="Table2" border="0" cellpadding="0" cellspacing="0" align="center" width="70%">
                                                            <tr>
                                                                <td width="6px" height="6px" background="images/tl.gif"></td>
                                                                <td height="6px" background="images/t.gif"></td>
                                                                <td width="11px" height="6px" background="images/tr.gif"></td>
                                                            </tr>
                                                            <tr>
                                                                <td background="images/l.gif"></td>
                                                                <td align="left" style="font-size: 15px;">
                                                                    <table cellpadding="3" id="Table3" runat="server" cellspacing="5" border="0" width="100%">
                                                                        <tr>
                                                                            <td align="left" colspan="4" runat="server" visible="false">

                                                                                <table border="0" cellpadding="3" cellspacing="0" width="100%">
                                                                                    <tr>
                                                                                        <td colspan="4" align="left" class="txt01_strong">Please select donation Amount : </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">Campaign</td>
                                                                                        <td align="left">:
                                                                                <asp:DropDownList ID="ddlCampaign" runat="server" CssClass="SmallFont">
                                                                                    <asp:ListItem Value="18">Excel-a-thon </asp:ListItem>
                                                                                    <asp:ListItem Value="12">Marathon</asp:ListItem>
                                                                                    <asp:ListItem Value="5">Walk-a-thon</asp:ListItem>
                                                                                    <asp:ListItem Value="9">MathCore/Genius Tests</asp:ListItem>
                                                                                    <asp:ListItem Value="14">Tournment</asp:ListItem>
                                                                                    <asp:ListItem Value="6">DAS</asp:ListItem>
                                                                                    <asp:ListItem Value="11" Selected="True">None</asp:ListItem>
                                                                                    <asp:ListItem Value="1">Finals</asp:ListItem>
                                                                                </asp:DropDownList>
                                                                                        </td>

                                                                                        <td align="left" style="height: 14px;">Purpose</td>
                                                                                        <td align="left" style="height: 14px">:
                                                                                            <asp:DropDownList ID="ddlPurpose" runat="server" CssClass="SmallFont">
                                                                                                <asp:ListItem Value="INScholar">India Scholarships</asp:ListItem>
                                                                                                <asp:ListItem Value="USScholar">US Scholarships</asp:ListItem>
                                                                                                <asp:ListItem Value="USBee">US Bees</asp:ListItem>
                                                                                                <asp:ListItem Value="No Pref">No preference</asp:ListItem>
                                                                                            </asp:DropDownList>
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td align="left">In Memory of</td>
                                                                                        <td align="left" colspan="3">:
                                                                                            <asp:TextBox ID="txtInMemoryOf" runat="server" Width="350px"></asp:TextBox></td>

                                                                                    </tr>
                                                                                    <tr id="tdNormal" runat="server">
                                                                                        <td></td>
                                                                                        <td colspan="3">Amount<span style="color: Red; font-family: Arial;">
                   &nbsp;*</span>
                                                                                            <span style="font-family: Arial; font-weight: normal">(100% Tax Deductable)</span>

                                                                                            <asp:RadioButton ID="rbtn20" Text="$20" GroupName="DnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtn50" Text="$50" GroupName="DnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtn100" Text="$100" GroupName="DnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnothr" GroupName="DnAmt" Checked="true" runat="server" />
                                                                                            <asp:TextBox ID="txtAmt" runat="server" Text="0" Width="50px"></asp:TextBox>

                                                                                            <asp:CheckBox ID="chkAmt" Text="Hide my Donation Amount" runat="server" /></td>
                                                                                    </tr>
                                                                                    <tr id="tdFinals" runat="server" visible="false">
                                                                                        <td colspan="4">Amount<span style="color: Red; font-family: Arial;">
                   &nbsp;*</span>
                                                                                            <span style="font-family: Arial; font-weight: normal">(100% Tax Deductable)</span>

                                                                                            <asp:RadioButton ID="rbtnF50T" Text="$5,000" Checked="true" GroupName="FinalsDnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnF25T" Text="$2,500" GroupName="FinalsDnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnF10T" Text="$1,000" GroupName="FinalsDnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnF5H" Text="$500" GroupName="FinalsDnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnF25H" Text="$250" GroupName="FinalsDnAmt" runat="server" />

                                                                                            <asp:RadioButton ID="rbtnothF" GroupName="FinalsDnAmt" runat="server" />
                                                                                            <asp:TextBox ID="txtAmtF" runat="server" Text="0" Width="50px"></asp:TextBox>
                                                                                            <br />
                                                                                            <asp:CheckBox ID="chkAmtF" Text="Hide my Donation Amount" runat="server" /></td>
                                                                                    </tr>
                                                                                </table>

                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trDn1" runat="server" visible="false">
                                                                            <td align="center" colspan="4">
                                                                                <asp:RadioButtonList ID="RbtnRegType" AutoPostBack="false" OnSelectedIndexChanged="RbtnRegType_SelectedIndexChanged" runat="server" RepeatDirection="Horizontal">
                                                                                    <%--                                                          <asp:ListItem Value="1">Cash</asp:ListItem>
                                                                                    <asp:ListItem Value="2">Check</asp:ListItem>--%>
                                                                                    <asp:ListItem Value="3" Selected="True">Credit Card</asp:ListItem>
                                                                                </asp:RadioButtonList>
                                                                                <asp:Label ID="lblCashCheckmsg" Style="font-weight: normal" Text="Please pay/send the Cash/Check to the Child or Parent." Visible="True" runat="server"></asp:Label>

                                                                            </td>
                                                                        </tr>
                                                                        <tr id="trDn2" runat="server">
                                                                            <td align="left" style="display:none;">Comments </td>
                                                                            <td align="left" style="vertical-align: middle; display:none;">&nbsp;
                                  <asp:TextBox ID="txtComments" Width="175px" Height="50px" TextMode="MultiLine" runat="server"></asp:TextBox>
                                                                                <br />
                                                                                Not more than 100 words
                      
                                                                            </td>

                                                                            <td align="center" colspan="2">
                                                                                <div runat="server" id="trrecapcha" visible="false">
                                                                                    <cc1:CaptchaControl ID="ccJoin" runat="server" CaptchaBackgroundNoise="none" CaptchaLength="5" CaptchaHeight="60" CaptchaWidth="200" CaptchaLineNoise="None" CaptchaMinTimeout="5" CaptchaMaxTimeout="240" />
                                                                                    <br />
                                                                                    <asp:TextBox ID="txtCap" runat="server"></asp:TextBox>
                                                                                    <asp:RequiredFieldValidator ID="RFVReCahpcha" runat="server"
                                                                                        ControlToValidate="txtCap" Enabled="False" ErrorMessage="*"></asp:RequiredFieldValidator>
                                                                                </div>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="4">
                                                                                <div class="g-recaptcha" id="rcaptcha" data-sitekey="6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"></div>
                                                                                <noscript>
                                                                                    <div style="width: 302px;">
                                                                                        <div style="width: 302px; height: 422px; position: relative;">
                                                                                            <div style="width: 302px; height: 422px; position: absolute;">
                                                                                                <iframe src="https://www.google.com/recaptcha/api/fallback?k=6LetKAwTAAAAAKDSI5QLPpjdu8KyK3ag_ozKhbZF"
                                                                                                    frameborder="0" scrolling="no"
                                                                                                    style="width: 302px; height: 422px; border-style: none;"></iframe>
                                                                                            </div>
                                                                                        </div>
                                                                                        <div style="width: 300px; height: 60px; border-style: none; bottom: 12px; left: 25px; margin: 0px; padding: 0px; right: 25px; background: #f9f9f9; border: 1px solid #c1c1c1; border-radius: 3px;">
                                                                                            <textarea id="g-recaptcha-response" name="g-recaptcha-response"
                                                                                                class="g-recaptcha-response"
                                                                                                style="width: 250px; height: 40px; border: 1px solid #c1c1c1; margin: 10px 25px; padding: 0px; resize: none;"
                                                                                                rows="5" cols="210">
        </textarea>
                                                                                        </div>
                                                                                    </div>
                                                                                </noscript>
                                                                                <span id="spCaptcha" style="margin-left: 100px; color: red" runat="server" visible="false">You can't leave Captcha Code empty</span>
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="4">
                                                                                <asp:Button ID="btnSubmit" runat="server" Text="Continue"
                                                                                    OnClick="btnSubmit_Click" />
                                                                                <br />

                                                                                <span style="color: Red; font-weight: normal; font-family: Arial; vertical-align: top">&nbsp;* - Mandatory</span><br />
                                                                            </td>
                                                                        </tr>
                                                                        <tr>
                                                                            <td align="center" colspan="4">
                                                                                <asp:Label ID="hdnMemberID" runat="server" Visible="false"></asp:Label>
                                                                                <asp:HiddenField ID="hdnEventID" runat="server" />
                                                                            </td>
                                                                        </tr>

                                                                    </table>

                                                                    <table cellpadding="3" id="tblContinue" runat="server" visible="false" cellspacing="0" border="0">
                                                                        <tr>
                                                                            <td align="left"><b>Congratulations!  Your email address and details were accepted. Your login info was sent to your email address for your use in the future in accessing our system. 
						 Keep them in a safe place. Without them, you cannot access our system. 
                  <asp:HyperLink ID="hlink" CssClass="btn_02" runat="server">Click Here </asp:HyperLink>
                                                                                to continue.</b>
                                                                            </td>
                                                                        </tr>
                                                                    </table>
                                                                </td>
                                                                <td background="images/r.gif"></td>
                                                            </tr>

                                                            <tr>
                                                                <td width="6px" height="12px" background="images/bl.gif"></td>
                                                                <td height="tpx" background="images/b.gif"></td>
                                                                <td width="11px" height="12px" background="images/br.gif"></td>
                                                            </tr>
                                                        </table>

                                                    </td>
                                                </tr>

                                                <tr>
                                                    <td align="right" bgcolor="#99CC33" style="height: 25px; vertical-align: middle;" class="style2">© North South Foundation. All worldwide rights reserved.
                                                        <a class="btn_01" href="/public/main/privacy.aspx">Copyright</a>&nbsp;&nbsp;&nbsp;
                                                    </td>
                                                </tr>
                                            </table>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
        <input type="hidden" runat="server" id="hdnChapterId" />
        <input type="hidden" runat="server" id="hdnChapterCode" />
    </form>
</body>

</html>

