﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdateContests.aspx.vb" Inherits="AddUpdateContests" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<script type="text/javascript" language="javascript">
			window.history.forward(1);

			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl,'DatePicker',settings);
				PopupWindow.focus();
			}

		</script>
		    <div style ="text-align :left "><asp:HyperLink runat="server" NavigateUrl="~/VolunteerFunctions.aspx" CssClass="btn_02" ID="hlinkChapterFunctions">back to Volunteer Functions</asp:HyperLink></div>

	    <table width="100%">
				<tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
					<td colspan="2" class="Heading" align="center" style="width: 991px" >
						<strong><asp:Label ID="lblHeading1" runat="server" ></asp:Label></strong><br />
						<strong><asp:Label ID="lblHeading2" runat="server" ></asp:Label></strong>
					</td>
				</tr>
				<%--<tr>
				<td align="left" colspan="2">
				<strong>
				<asp:Label ID="lblYear" runat="server"  Text="Contest Year"> </asp:Label></strong> &nbsp;&nbsp;
				<asp:DropDownList ID="drpYear" runat="server" AutoPostBack="true"></asp:DropDownList> </td>
				
				</tr>--%>
				
  				  <tr>
  				        <td colspan="2" style="width: 991px">
  				        <asp:label ID="lblGridError" runat="server" visible="False" width="100%" ForeColor="Red" Font-Bold="True" Font-Size="Medium"></asp:label>
				        <asp:DataGrid OnUpdateCommand="grdTarget_UpdateCommand" OnEditCommand="grdTarget_EditCommand" ID="grdTarget" runat="server" CssClass="mediumwording" Width="100%" AutoGenerateColumns="False" AllowSorting="True" DataKeyField="ContestID"
					 GridLines="Horizontal" CellPadding="4" BackColor="White" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666">
					<ItemStyle Font-Size="X-Small" Font-Names="Verdana" ForeColor="#333333" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" Font-Names="Verdana" Font-Bold="True" ForeColor="White" BackColor="#330099" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>
                                 <asp:BoundColumn DataField="ContestID" Visible="False" ></asp:BoundColumn>
                                <asp:BoundColumn DataField="ContestCategoryID" Visible="False" ></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="Contests" ItemStyle-Width="10%">
                                      <ItemTemplate >
                                        <asp:HyperLink ID="hlinkContestCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>' ToolTip='<%#DataBinder.Eval(Container, "DataItem.ProductDescription")%>'></asp:HyperLink>
                                      </ItemTemplate>
                                    <HeaderStyle Width="10%" ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn >
                                <asp:TemplateColumn HeaderText="Sponsor" ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate >
                                        <asp:Label ID="lblContestSponsor" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorName")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorID" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorID")%>'></asp:Label>
                                         <asp:Label ID="lblContestSponsorType" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.SponsorType")%>'></asp:Label>
                                     </ItemTemplate>
                                      <EditItemTemplate>
                                        <asp:DropDownList id="ddlContestSponsor" runat="server" DataTextField="SponsorName" DataValueField="SponsorID" OnPreRender="SetDropDown_Sponsor" AutoPostBack="false"></asp:DropDownList>
                                        <asp:DropDownList id="ddlContestSponsorType" runat="server" Visible="false" DataTextField="SponsorType" DataValueField="SponsorID" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="Venue"  ItemStyle-Width="15%"  HeaderStyle-Width="15%">
                                      <ItemTemplate>
                                         <asp:Label ID="lblContestVenue" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Venue")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                         <asp:DropDownList ID="ddlContestVenue" runat="server" DataTextField="VenueName" DataValueField="VenueID"  OnPreRender="SetDropDown_Venue" AutoPostBack="false"></asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="15%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Registration Deadline"  ItemStyle-Width="10%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:Label ID="lblEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:Label>
                                          <asp:TextBox OnPreRender ="getRegDeadline" ID="txtEditRegDeadLine" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RegistrationDeadline", "{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                      <HeaderStyle Width="10%"  ForeColor="White" Font-Bold="true" />
                                 </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Check in Time" ItemStyle-Width="5%" >
                                      <ItemTemplate>
                                          <asp:Label ID="lblCheckInTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CheckInTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList ID="ddlCheckInTime" runat="server" OnPreRender="SetDropDown_CheckInTime">
                              				       <asp:ListItem Value="TBD">TBD</asp:ListItem>
                              				       <asp:ListItem Value="6:00">6:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="6:15">6:15 AM</asp:ListItem>
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
					                                <asp:ListItem Value="6:45">6:45 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:15">7:15 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>                                          </asp:DropDownList>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn> 
                                <asp:TemplateColumn HeaderText="Contest Date"  ItemStyle-Width="14%" HeaderStyle-Width="14%">
                                      <ItemTemplate><asp:Label ID="lblContestDate" runat="Server" Text='<%#DataBinder.Eval(Container, "DataItem.ContestDate", "{0:MM/dd/yyyy}")%>'></asp:Label>
                                      </ItemTemplate>
                                     <EditItemTemplate>
                                        <asp:DropDownList ID="ddlGridContestDt" runat="server" DataTextField="WeekendDt" OnPreRender="SetDropDown_ContestDt" AutoPostBack="false">
	                                    </asp:DropDownList><asp:Label ID="lblGridContestDate" runat="Server" Width="100%" visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.ContestDate", "{0:d}")%>'></asp:Label>
                                   </EditItemTemplate>
                                    <HeaderStyle Width="14%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Start Time"  ItemStyle-Width="5%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblStartTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartTime")%>' ></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:DropDownList ID="ddlStartTime" runat="server" OnPreRender="SetDropDown_StartTime" >
					                                <asp:ListItem Value="TBD">TBD</asp:ListItem>
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
					                                <asp:ListItem Value="6:45">6:45 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:15">7:15 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                                      </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" wrap="true" />
                                </asp:TemplateColumn>
                                <asp:TemplateColumn  HeaderText="End Time"  ItemStyle-Width="5%">
                                    <ItemTemplate>
                                          <asp:Label ID="lblEndTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndTime")%>' ></asp:Label>
                                    </ItemTemplate>
                                    <EditItemTemplate>
                                        <asp:DropDownList ID="ddlEndTime" runat="server" OnPreRender="SetDropDown_EndTime">
					                                <asp:ListItem Value="TBD">TBD</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                                   
                                    </EditItemTemplate>
                                    <HeaderStyle Width="5%"  ForeColor="White" Font-Bold="true" Wrap="true" />
                                </asp:TemplateColumn>
                                <%--<asp:TemplateColumn HeaderText="Building"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtBuilding" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Building")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>                                
                                <asp:TemplateColumn HeaderText="Room"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtRoom" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Room")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn> --%> 
                               <asp:TemplateColumn HeaderText="Capacity"  ItemStyle-Width="2%">
                                      <ItemTemplate>
                                          <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Capacity")%>'></asp:Label>
                                      </ItemTemplate>
                                      <EditItemTemplate>
                                          <asp:TextBox ID="txtCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Capacity")%>'></asp:TextBox>
                                      </EditItemTemplate>
                                    <HeaderStyle Width="2%"  ForeColor="White" Font-Bold="true" />
                                </asp:TemplateColumn>                   
       <asp:EditCommandColumn  ButtonType="PushButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
       <asp:ButtonColumn Text="Delete" ButtonType="PushButton" CommandName="Delete"></asp:ButtonColumn>
                                                             
                         </Columns>
  				</asp:DataGrid>
  				        </td>
  				  </tr>				
  				  <tr style="border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
				    <td colspan="2" class="ContentSubTitle" align="center" style="width: 991px" >
				        &nbsp;<asp:Label ID="lblYearErr" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr></table>

   <table width="100%" border="2" style="border-left-color: black; border-bottom-color: black; border-top-style: solid; border-top-color: black; border-right-style: solid; border-left-style: solid; border-right-color: black; border-bottom-style: solid;">
<tr align="center" >
    <td colspan="2"><strong>Select Contest Details:</strong></td>
    <td align="left" colspan="2" style="width: 50%" >
        <asp:Button ID="btnAdd" runat="server" Text="Add Selected Contests" CssClass="SmallFont"  Enabled ="false"  BackColor="Gainsboro" />
        <asp:Label ID="lblDataErr" runat="server" CssClass="SmallFont" Visible="false" ForeColor="Red" Width="40%"></asp:Label>
        <asp:Label ID="lblError" runat="server" CssClass="SmallFont" Visible="false" ForeColor="Red"></asp:Label>
    </td>
</tr>
<tr style="height: 20%; border-top-style: solid; border-right-style: solid; border-left-style: solid; border-bottom-style: solid;">
    <td align="left"  colspan="2" rowspan="8" style="width: 30%" >
        Contests:<br />
		<asp:CheckBoxList id="lstContests" runat="server" autopostback="false" width="100%" CssClass="SmallFont" EnableViewState="true" DataTextField="Name" DataValueField="ProductID"></asp:CheckBoxList>
	</td>
	<td style="width: 15%">
        Contest Date:</td>
	<td style="width: 55%">
	 <asp:DropDownList ID="ddlContestDt" runat="server" Width="20%"  DataTextField="WeekendDt" >
	        </asp:DropDownList>
        	        <%--<asp:textbox id="txtContestDt1" runat="server" CssClass="SmallFont"></asp:textbox> <A href="javascript:PopupPicker('txtWkShopDt1', 200, 200);">
                    <IMG src="Images/Calendar.gif" border="0"/> </A>--%>
</td>
  
</tr>
<tr>
	<td style="width: 15%">Checkin Time:</td>
	<td style="width: 55%">
	        <%--<asp:textbox id="txtCheckinTime" runat="server" CssClass="SmallFont" ></asp:textbox>--%>
	       <asp:DropDownList ID="ddlCheckInTime1" runat="server" OnPreRender="SetDropDown_CheckInTime">
	                                                <asp:ListItem Value="TBD">TBD</asp:ListItem>
                              				        <asp:ListItem Value="6:00">6:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="6:15">6:15 AM</asp:ListItem>
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
					                                <asp:ListItem Value="6:45">6:45 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:15">7:15 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem> 
                                          </asp:DropDownList>                
	        
    </td>
</tr>
<tr>
	<td style="width: 15%">Start Time:</td>
	<td style="width: 55%">
	        <%--<asp:textbox id="txtStartTime" runat="server" CssClass="SmallFont" ></asp:textbox>--%>
	        <asp:DropDownList ID="ddlStartTime1" runat="server" OnPreRender="SetDropDown_StartTime" >
					                                <asp:ListItem Value="TBD">TBD</asp:ListItem>
					                                <asp:ListItem Value="6:30">6:30 AM</asp:ListItem>
					                                <asp:ListItem Value="6:45">6:45 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:00">7:00 AM</asp:ListItem>
                              				        <asp:ListItem Value="7:15">7:15 AM</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                    
    </td>
</tr>
<tr>
	<td style="width: 15%">End Time:</td>
	<td style="width: 55%">
	        <%--<asp:textbox id="txtEndTime" runat="server" CssClass="SmallFont" ></asp:textbox>--%>
	      <asp:DropDownList ID="ddlEndTime1" runat="server" OnPreRender="SetDropDown_EndTime">
					                                <asp:ListItem Value="TBD">TBD</asp:ListItem>
					                                <asp:ListItem Value="7:30">7:30 AM</asp:ListItem>
					                                <asp:ListItem Value="7:45">7:45 AM</asp:ListItem>
					                                <asp:ListItem Value="8:00">8:00 AM</asp:ListItem>
					                                <asp:ListItem Value="8:15">8:15 AM</asp:ListItem>
					                                <asp:ListItem Value="8:30">8:30 AM</asp:ListItem>
					                                <asp:ListItem Value="8:45">8:45 AM</asp:ListItem>
					                                <asp:ListItem Value="9:00">9:00 AM</asp:ListItem>
					                                <asp:ListItem Value="9:15">9:15 AM</asp:ListItem>
					                                <asp:ListItem Value="9:30">9:30 AM</asp:ListItem>
					                                <asp:ListItem Value="9:45">9:45 AM</asp:ListItem>
					                                <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
					                                <asp:ListItem Value="10:15">10:15 AM</asp:ListItem>
					                                <asp:ListItem Value="10:30">10:30 AM</asp:ListItem>
					                                <asp:ListItem Value="10:45">10:45 AM</asp:ListItem>
					                                <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
					                                <asp:ListItem Value="11:15">11:15 AM</asp:ListItem>
					                                <asp:ListItem Value="11:30">11:30 AM</asp:ListItem>
					                                <asp:ListItem Value="11:45">11:45 AM</asp:ListItem>
					                                <asp:ListItem Value="12:00">12:00 NOON</asp:ListItem>
					                                <asp:ListItem Value="12:15">12:15 PM</asp:ListItem>
					                                <asp:ListItem Value="12:30">12:30 PM</asp:ListItem>
					                                <asp:ListItem Value="12:45">12:45 PM</asp:ListItem>
					                                <asp:ListItem Value="13:00">1:00 PM</asp:ListItem>
					                                <asp:ListItem Value="13:15">1:15 PM</asp:ListItem>
					                                <asp:ListItem Value="13:30">1:30 PM</asp:ListItem>
					                                <asp:ListItem Value="13:45">1:45 PM</asp:ListItem>
					                                <asp:ListItem Value="14:00">2:00 PM</asp:ListItem>
					                                <asp:ListItem Value="14:15">2:15 PM</asp:ListItem>
							                        <asp:ListItem Value="14:30">2:30 PM</asp:ListItem>
							                        <asp:ListItem Value="14:45">2:45 PM</asp:ListItem>
							                        <asp:ListItem Value="15:00">3:00 PM</asp:ListItem>
							                        <asp:ListItem Value="15:15">3:15 PM</asp:ListItem>
							                        <asp:ListItem Value="15:30">3:30 PM</asp:ListItem>
							                        <asp:ListItem Value="15:45">3:45 PM</asp:ListItem>
							                        <asp:ListItem Value="16:00">4:00 PM</asp:ListItem>
							                        <asp:ListItem Value="16:15">4:15 PM</asp:ListItem>
							                        <asp:ListItem Value="16:30">4:30 PM</asp:ListItem>
							                        <asp:ListItem Value="16:45">4:45 PM</asp:ListItem>
							                        <asp:ListItem Value="17:00">5:00 PM</asp:ListItem>
							                        <asp:ListItem Value="17:15">5:15 PM</asp:ListItem>
							                        <asp:ListItem Value="17:30">5:30 PM</asp:ListItem>
							                        <asp:ListItem Value="17:45">5:45 PM</asp:ListItem>
							                        <asp:ListItem Value="18:00">6:00 PM</asp:ListItem>
							                        <asp:ListItem Value="18:15">6:15 PM</asp:ListItem>
							                        <asp:ListItem Value="18:30">6:30 PM</asp:ListItem>
							                        <asp:ListItem Value="18:45">6:45 PM</asp:ListItem>
							                        <asp:ListItem Value="19:00">7:00 PM</asp:ListItem>
							                        <asp:ListItem Value="19:15">7:15 PM</asp:ListItem>
							                        <asp:ListItem Value="19:30">7:30 PM</asp:ListItem>
							                        <asp:ListItem Value="19:45">7:45 PM</asp:ListItem>
							                        <asp:ListItem Value="20:00">8:00 PM</asp:ListItem>
							                        <asp:ListItem Value="20:15">8:15 PM</asp:ListItem>
							                        <asp:ListItem Value="20:30">8:30 PM</asp:ListItem>
							                        <asp:ListItem Value="20:45">8:45 PM</asp:ListItem>
							                        <asp:ListItem Value="21:00">9:00 PM</asp:ListItem>
							                        <asp:ListItem Value="21:15">9:15 PM</asp:ListItem>
							                        <asp:ListItem Value="21:30">9:30 PM</asp:ListItem>
							                        <asp:ListItem Value="21:45">9:45 PM</asp:ListItem>
							                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
							                        <asp:ListItem Value="22:15">10:15 PM</asp:ListItem>
							                        <asp:ListItem Value="22:30">10:30 PM</asp:ListItem>
							                        <asp:ListItem></asp:ListItem>
                                          </asp:DropDownList>                                     
    </td>
</tr>

<tr>
	<td style="width: 15%">Capacity:</td>
	<td style="width: 55%">
	        <asp:textbox id="txtCapacity" runat="server" CssClass="SmallFont"></asp:textbox>
    </td>
</tr>
<tr>
    <td style="width: 15%">Select Venue:</td>
    <td style="width: 55%">
        <asp:dropdownlist ID="ddlVenue" runat="server" Width="95%" CssClass="SmallFont"  DataTextField="VenueName" DataValueField="VenueID" >
        </asp:dropdownlist>
    </td>
</tr>
<tr>
    <td style="width: 15%">Select Sponsor Type :</td>
    <td style="width: 55%">
        <asp:RadioButtonlist ID="rbtnMemberType" runat="server" AutoPostBack="true" RepeatDirection="Horizontal"  Width="412px">
            <asp:ListItem Selected="True" Value="O">Organization</asp:ListItem>
            <asp:ListItem Value="I">Individual</asp:ListItem>
        </asp:RadioButtonlist></td>
</tr>
<tr>
    <td style="width: 15%">Select Sponsor:</td>
    <td style="width: 55%">
        <asp:dropdownlist ID="ddlSponsor" runat="server" Width="95%"  DataTextField="SponsorName" DataValueField="SponsorID" >
        </asp:dropdownlist>
    </td>
</tr>
<tr>
<td colspan="4"><asp:Label ID="lblErr" runat="server" width="100%"></asp:Label></td>
</tr>
   </table>
    
</asp:Content>

