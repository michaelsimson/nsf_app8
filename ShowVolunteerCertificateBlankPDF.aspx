<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowVolunteerCertificateBlankPDF.aspx.vb" Inherits="ShowVolunteerCertificateBlankPDF" %>
<%@ Reference Page="~/GenerateParticipantCertificates.aspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Untitled Page</title>
      <style type="text/css" media="screen" >

        p.MsoNormal, li.MsoNormal, div.MsoNormal
	    {
	        margin:0in;
	        margin-bottom:.0001pt;
	        text-autospace:none;
	        font-size:10.0pt;
	        font-family:"Times New Roman","serif";
	    }  
        @page Section1
	        {
	            size:11.0in 8.5in;
	            margin:.5in 1.0in .5in 1.0in;
	        }
        div.Section1
	        {
	            page:Section1;
	        }
     
        </style>

</head>
<body>
    <form id="form1" runat="server">
        <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
        <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return pdf_report();" />
 
           <div>
            <asp:Repeater runat="server" ID="rptCertificate" OnItemDataBound="rptCertificate_OnItemDataBound">
                <ItemTemplate>
             <div class="Section1" style="page-break-before:always">     
               <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0" >                
                 <tr>
                  <td colspan="8">
                  <table cellspacing="0" cellpadding="0" width="98%"  align="center" border="0">
                  <tr>
                  <td rowspan="3" align="left" width="20%">
                  
               <asp:Image ID="imgLogo" runat="server" ImageUrl="http://www.northsouth.org/app8/Images/nsf.jpg"/>                         
                  </td>
                  
                  <td  rowspan="5" align="left" width="80%">
               <%-- <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg1A.jpg"/><br />
                     <% If Session("SelChapterID") = 1 Then%>
                            <asp:Image runat="server" ID="imgTitleNational"  ImageUrl="http://www.northsouth.org/app8/Images/image007_National.gif"/>
                        <%else %>
                            <asp:Image runat="server" ID="imgTitle"  ImageUrl="http://www.northsouth.org/app8/Images/CertImg2A.jpg"/>
                            <%end if %>--%>
                            <asp:Image runat="server" ID="imgHeader"  ImageUrl="http://www.northsouth.org/app8/Images/nsftitle.JPG"/>
                  </td>
                  </tr>

               
                  </table>
                  </td>
                  </tr>
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                           <br />
                        </td>
                    </tr>
               <%--     
                     <tr>
                        <td colspan="8" align="center">
                                    <asp:Image runat="server" ID="ImageCertMain" ImageUrl="http://www.northsouth.org/app8/Images/CertMainA.jpg" />
                        </td>
                    </tr>  --%>   
                 <tr>
                        <td colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle1" ForeColor="#988600" Text="Certificate of Appreciation"></asp:Label>
                        </td>
                    </tr>
                        <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                           <br />
                        </td>
                    </tr>
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblTitle2" ForeColor="#988600" Text="awarded to"  ></asp:Label>
                        </td>
                    </tr>
               <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                           <br /> 
                        </td>
                    </tr>
                    <tr>
                        <td class="ItemCenter" colspan="8" align="center" valign="top" >
                            <asp:Label runat="server" ID="lblName"  Text="________________________________________________"></asp:Label>
                        </td>
                    </tr>
                     <tr>
                        <td colspan="8"><br /></td>
                    </tr> 
        
                    <tr>
                        <td colspan="8" style="text-align:justify;">
                               <asp:Label runat="server" ID="lblcomm"  Font-Bold="true" >
                        for volunteering and providing exceptional service to the foundation 
<%--                            <asp:Label runat="server" ID="lblContestYear"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.contestyear") %>'></asp:Label>
--%>                             <% If Session("SelChapterID") = 1 Then%>
                            in the National Championship Finals held during  <asp:Label runat="server" ID="Label3"  Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ContestDate") %>'></asp:Label> at
                            <%Else%>
                            during the 2013 Regional contests held at the
                            <%end if %>
                            <asp:Label runat="server" ID="lblLocation" Font-Bold="true" Text='<%# DataBinder.Eval(Container,"DataItem.ChapterCode") %>'></asp:Label> 
                              <% If Session("SelChapterID") = 1 Then%>
                              .
                               <%Else%>
                               Chapter.
                               <% end if %>
                            </asp:Label>
                
                            </b>
                        </td>
                    </tr>
                     
                     <tr>
                        <td colspan="8"><br /></td>
                    </tr> 
                     <tr> 
                        <td colspan="8" align="center">
              <asp:Label runat="server" ID="lblnew" ForeColor="#988600" Font-Italic="true" Text="We couldn't do it without you!"></asp:Label>
                        </td>
                    </tr> 
                    <tr>
                        <td colspan="8"><br /></td>
                    </tr>
                    <tr>
                        <td colspan="8" style="text-align:justify;" >
                            
                               <asp:Label runat="server" ID="lblNSF"  Font-Italic="true"> North South Foundation (NSF) is a non-profit organization involved in implementing educational programs for children in North America and India. The Foundation believes that this world can be a better place to live if the children of today are better prepared to be good citizens of tomorrow. Toward this end, the Foundation encourages children to endeavor to become the best they can be, by giving their best. Further, while it is self-evident that all humans are created equal, it is education that is paramount to actually realizing the rights of equality including life, liberty and the pursuit of happiness as the Founding Fathers of this Nation envisaged more than two hundred years ago.
                       </asp:Label>
                        </td>
                    </tr>
               
                 
          
                    <tr>
                        <td colspan="8" width="100%">
                             <table cellspacing="2" cellpadding="2" width="98%" border="0" >                
                                <tr >
                                <td ></td>
                                
                                    <td rowspan="4" width="20%" align="center">
                        
                                <asp:Image runat="server" ID="Image1" ImageUrl="http://www.northsouth.org/app8/Images/CertImg3A.jpg" />
                   

                                    </td> 
                                
                                     <td ></td>
                                </tr>
                                     <tr >
                                    <td align="left" >
                                    <br /> <br />
                                     <asp:Image runat="server" ID="Image2"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                
                             
                                
                                    <td align="left" >
                                     <br /> <br />
                                      <asp:Image runat="server" ID="Image3"  ImageUrl="http://www.northsouth.org/app8/Images/Signline.jpg" Width="90%" />
                                    </td> 
                                </tr>
                     
                          <%-- <tr>
                                    <td ><br /></td>
                                    <td ><br /></td>
                                </tr>--%>
                                <tr>
                                    <td align="left"  valign="top"> 
                                        <asp:Label runat="server" ID="lblLeftTitle" Font-Bold="true"  Text='<%# Session("LeftTitle") %>'></asp:Label>.
                                        <asp:Label runat="server" ID="lblLeftSignature"    Font-Bold="true" Text='<%# Session("LeftSignatureName") %>'></asp:Label>                                        
       
                                    </td>
                          
                                    <td align="left" valign="top" >
                                        <asp:Label runat="server" ID="lblRightTitle" Font-Size="14"  Font-Bold="true" Text='<%# Session("RightTitle") %>'></asp:Label>.
                                        <asp:Label  runat="server" ID="lblRightSignature"  Font-Size="14"    Font-Bold="true" Text='<%# Session("RightSignatureName") %>'></asp:Label>
                             
                                        </td>
                               
                                </tr> 
                                <tr>
                                    <td align="left"  valign="top" >
                                    <asp:Label runat="server" Font-Size="14" ID="lblSigTitle"   Text='<%# Session("LeftSignatureTitle") %>'></asp:Label>

                                    </td>
                                    <td align="left" valign="top" >
                                   <asp:Label  runat="server" ID="lblRightSigTitle"    Text='<%# Session("RightSignatureTitle") %>'></asp:Label>

                                    </td>
                                </tr>
                                
                            </table>
                        </td> 
                    </tr>                   
		       </table> 
		       </div> 
		       </ItemTemplate>	   
		       </asp:Repeater>
		       
		</div>
		<asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" class="tblMain" cellpadding="0" width="100%"  align="left" border="0" >
                <tr >
                    <td class="Heading">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
    </form>
</body>
</html>


 
 
 