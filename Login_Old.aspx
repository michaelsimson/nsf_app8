 <%@ Page Language="vb" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false"
    Trace="false" Inherits="VRegistration.Login" CodeFile="Login.aspx.vb" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <!--<table width="100%" >
				<tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<H1 align="center"><FONT face="Arial" color="#0000ff" size="3"><B>Welcome to North South 
									Foundation Website</B></FONT>&nbsp;<BR>
						</H1>
					</td>
				</tr>
				<tr>
					<td style="height: 150px">
						<P><FONT class=MidFont>Based on feedback, we have made enhancements to the user-interface this year. 
						Your continued feedback is important to make the interface even more user-friendly.  
						Send your comments to <A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com</U></A>&nbsp; 
						Please be as specific as possible so that we can be more effective in our response.</FONT>
											
						</P>
						<P><FONT  class=MidFont>We have added more options to your menu. 
						 You will also be able to register online for workshops when they are available in your area.</FONT>&nbsp;<BR>
						</P>
						<P><FONT  class=MidFont>You need your email and password to access the system. 
						The email must be the same one you used last time to enter this system. 
						If you forgot the email you used last time, please send us an email to 
						<A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com </U></A>
						giving your name, address, phone and other details. </FONT>&nbsp;
												
					</td>
				</tr>							
			</table>
		-->
    <asp:Panel runat="server" ID="pnlParent">
        <table width="100%" id="tblParent" runat="server">
            <tr>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1 align="center">
                        <font face="Arial" color="#0000ff"><b>Welcome to North South Foundation
                            <br />
                            PARENT LOGIN PAGE</b></font>&nbsp;<br>
                    </h1>
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="MidFont">&nbsp;&nbsp;Dear Parents, </font>
                    </p>
                    <p>
                        <font class="MidFont">Based on feedback, we have again made enhancements to the user-interface
                            this year. Your continued feedback is important to make your Internet experience
                            more user-friendly. Send your comments to <a href="mailto:nsfcontests@gmail.com"
                                target="_blank">nsfcontests@gmail.com</a> Please be as specific as possible
                            so that we can be more effective in our response. </font>
                    </p>
                    <p>
                        <font class="MidFont">We have added more options to your menu. You will also be able
                            to register online for workshops when they are available in your area. </font>
                        &nbsp;</p>
                    <p>
                        <font class="MidFont">If you are a first timer to NSF, please select the option �New
                            to NSF.� You will be asked to fill out your profile including your spouse. For registering
                            contests, we need profiles on both parents. </font>
                    </p>
                    <p>
                        <font class="MidFont">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to enter
                            this system. If you forgot the email you used last time, please follow one of the
                            options given below to retrieve it. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlVolunteers">
        <table width="100%" id="tblVolunteers" runat="server">
            <tr>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1 align="center">
                        <font face="Arial" color="#0000ff" size="3"><b>Welcome to North South Foundation
                            <br />
                            VOLUNTEER LOGIN PAGE</b></font>&nbsp;<br>
                    </h1>
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="MidFont">Our goal is to make your Internet experience more user-friendly
                            so that your time is well spent in serving the cause of the Foundation. We want
                            to automate more functions over tine in order to achieve more bang for the valuable
                            time you spend with NSF. Please send your feedback to <a href="Mailto:nsfcontests@gmail.com">
                                nsfcontests@gmail.com</a> Please be as specific as possible so that we can be
                            more effective in making improvements. </font>
                    </p>
                    <p>
                        <font class="MidFont">If you are a first timer to NSF, please select the option �New
                            to NSF.� You will be asked to fill out your profile. For married people, we request
                            profiles on both spouses. </font>&nbsp;</p>
                    <p>
                        <font class="MidFont">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to enter
                            this system. If you forgot the email you used last time, please follow one of the
                            options given below to retrieve it. </font>
                    </p>
                    <p>
                        <font class="MidFont">You should use the same email and password whether you are entering
                            the system as a parent, donor or volunteer. Further, if you have a spouse, each
                            spouse should use a different email to login. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlDonors">
        <table width="100%" id="tblDonors" runat="server">
            <tr>
                <td class="ContentSubTitle" valign="top" nowrap align="center">
                    <h1 align="center">
                        <font face="Arial" color="#0000ff" size="3"><b>Welcome to North South Foundation<br />
                            DONOR LOGIN PAGE</b></font>&nbsp;<br>
                    </h1>
                </td>
            </tr>
            <tr>
                <td style="height: 150px">
                    <p>
                        <font class="MidFont">Thank you for being a donor to the North South Foundation. We
                            value your contribution very highly. People like you make it possible for NSF to
                            continue its mission year after year. Our goal is to make your online donation experience
                            a pleasant one. </font>
                    </p>
                    <p>
                        <font class="MidFont">If you are a first timer to NSF, please select the option �New
                            to NSF.� You will be asked to fill out a form. For uniformity, we use the same form
                            for donors, parents and volunteers. Although we love to have all the information
                            in our records, you only need to provide your name, address, email and phone number
                            to meet IRS requirements. We do not share your information with any third party.
                        </font>&nbsp;</p>
                    <p>
                        <font class="MidFont">If you are not a first timer, you need your email and password
                            to access the system. The email must be the same one you used last time to login.
                            If you forgot the email you used last time, please follow one of the options given
                            below to retrieve it. </font>
                    </p>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <table id="tblLogin" runat="server" style="width: 49%;" border="0">
        <tr >
            <td style="width: 342px;" >
            </td>
            <td class="ContentSubTitle" valign="middle"  align="left" style="width: 500px;">
                <h1>
                    Sign On</h1>
            </td>
        </tr>
        <tr>
            <td valign="top" nowrap align="right" style="width: 342px; height: 20px;">
                E-mail</td>
            <td style="width: 689px; height: 37px;">
                <asp:TextBox ID="txtUserId" runat="server" MaxLength="50" Width="300" CssClass="SmallFont"></asp:TextBox><br>
                <asp:RequiredFieldValidator ID="RequiredFieldValidator1" runat="server" Display="Dynamic"
                    ErrorMessage="Enter Login Id." ControlToValidate="txtUserId"></asp:RequiredFieldValidator><asp:RegularExpressionValidator
                        ID="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
                        ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId"
                        ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator></td>
        </tr>
        <tr>
            <td valign="top" nowrap align="right" style="width: 342px; height: 26px;">
                Password
            </td>
            <td style="width: 689px; height: 26px;">
                <asp:TextBox ID="txtPassword" runat="server" MaxLength="30" Width="150" CssClass="smFont"
                    TextMode="Password"></asp:TextBox><asp:RequiredFieldValidator ID="RequiredFieldValidator2"
                        runat="server" ErrorMessage="Enter Password." ControlToValidate="txtPassword"></asp:RequiredFieldValidator></td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td align="left" style="width: 689px">
                <asp:Button ID="btnLogin" runat="server" CssClass="FormButton" Text="Login"></asp:Button></td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td class="smallfont" style="width: 689px">
                Click here if you
                <asp:HyperLink ID="Hyperlink1" runat="server" Font-Size="Medium" NavigateUrl="Forgot.aspx"> forgot your password</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td class="smallfont" style="width: 689px">
                Click here to
                <asp:HyperLink Text="change password" Font-Size="Medium" runat="server" NavigateUrl="~/ChangePassword.aspx"
                    ID="lnkChangePwd"></asp:HyperLink>&nbsp;</td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td class="smallfont" style="width: 689px">
                Click here if you
                <asp:HyperLink Text="forgot login ID" Font-Size="Medium" runat="server" NavigateUrl="~/Forgot_loginid.aspx"
                    ID="HyperLink2"></asp:HyperLink>&nbsp;
            </td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td class="smallfont" style="width: 689px">
                Click here if your
                <asp:HyperLink ID="HyperLink4" runat="server" Font-Size="Medium" NavigateUrl="ChangeEmail.aspx"> email address for login is no longer valid</asp:HyperLink>
            </td>
        </tr>
        <tr>
            <td style="width: 342px">
            </td>
            <td class="smallfont" style="width: 689px">
                Click here if you are
                <asp:HyperLink ID="HyperLink3" runat="server" Font-Size="Medium" NavigateUrl="createuser.aspx">new to NSF</asp:HyperLink>
            </td>
        </tr>
    </table>
    <table id="tblErrorLogin" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p>
                    Login Attempt failed. Invalid Email and/or password. Please try again.
                </p>
            </td>
        </tr>
    </table>
    <table id="tblMissingRole" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p>
                    Sorry, we don't yet have any assigned roles for you in our records. Please consult
                    your contact or if you don't have a contact, send an email to <a href="mailto:volunteer@northsouth.org"
                        target="_blank">volunteer@northsouth.org</a> with all your details.
                </p>
            </td>
        </tr>
    </table>
    <table id="tblMissingProfile" visible="false" runat="server">
        <tr>
            <td class="ErrorFont">
                <p>
                    Your personal profile is not in our records. Please press Continue to enter your
                    personal profile
                </p>
                <asp:Button ID="btnContinue" runat="server" CssClass="FormButton" Text="Continue"></asp:Button></td>
        </tr>
    </table>
    <table id="tblMissingToken" runat="server" visible="false">
        <tr>
            <td class="ErrorFont">
                <p>
                Error: Missing entry token.
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a parent,<a href="login.aspx?entry=p"> login here.</a>
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a volunteer, <a href="login.aspx?entry=v">login here.</a>
            </td>
        </tr>
        <tr>
            <td class="smallfont">
                If you are a donor, <a href="login.aspx?entry=d">login here.</a>
            </td>
        </tr>
    </table>
    <!--
			<table id="tblLinks" runat="server" style="width: 659px; height: 1px">
				<tr>
					<td>
						<a id="homeLink" runat="server">  Home</a>
						
					</td>
				</tr>
				</table>
				-->
</asp:Content>
