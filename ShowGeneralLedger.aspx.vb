Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class ShowGeneralLedger
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim StartDate, EndDate, Status, TransType As String
        Dim BegRecId, EndRecId As Integer
        Dim Cnt, count As Integer
        Dim tableExist As String = ""
        Dim reader, reader1 As SqlDataReader

        Dim ds, ds_Closed As DataSet
        Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center>Table Name</td><td align=Center>TransType</td><td align=Center>Status</td><td align=Center>Beg Date</td><td align=Center>End Date</td></tr>"
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='DGN'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE)  WHERE TransType='DGN' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate,FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>DonationChecks </td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE)  WHERE TransType='DGN' and Fy.Status='O' Group by  GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>DonationChecks</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If

            'TableStr = TableStr & "<tr><td align=Center>DonationChecks</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            'reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>DonationChecks</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='OTH'")
        If Cnt > 0 Then

            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='OTH' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Other Deposits</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='OTH'  and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Other Deposits</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Other Deposits</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='CGN'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='CGN' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Credit Cards</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='CGN' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Credit Cards</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Credit Cards</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='IGN'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='IGN' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Investment Income</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='IGN' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Investment Income</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Investment Income</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='VPS'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='VPS' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Vouchers-Expense Checks</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='VPS' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Vouchers-Expense Checks</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Vouchers-Expense Checks</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='DGE'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='DGE' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>ACT-EFT Donations</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='DGE' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            'count = ds.Tables(0).Rows.Count
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>ACT-EFT Donations</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>ACT-EFT Donations</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='VPE'")
        If Cnt > 0 Then

            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='VPE' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center> Bank-CCFees </td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On CAST(GL.BankDate AS DATE) between CAST(FY.StartDate AS DATE) and CAST(FY.Enddate AS DATE) WHERE TransType='VPE'  and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center> Bank-CCFees </td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center> Bank-CCFees </td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='IGT'")
        If Cnt > 0 Then

            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGT' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Vouchers - TransferOut</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGT' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Vouchers - TransferOut</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Vouchers - TransferOut</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='IGB'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGB' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center>Buy Transactions</td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGB' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center>Buy Transactions</td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center>Buy Transactions</td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from GeneralLedger Where TransType='IGS'")
        If Cnt > 0 Then
            ds_Closed = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(FY.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(FY.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGS' and FY.Status ='C' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by GL.EndDate desc")
            count = ds_Closed.Tables(0).Rows.Count
            If ds_Closed.Tables(0).Rows.Count > 0 Then
                Status = ds_Closed.Tables(0).Rows(0)("Status")
                TransType = ds_Closed.Tables(0).Rows(0)("TransType")
                StartDate = ds_Closed.Tables(0).Rows(0)("StartDate")
                EndDate = ds_Closed.Tables(0).Rows(0)("Enddate")
                TableStr = TableStr & "<tr><td align=Center> Sell Transactions </td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
            End If
            count = 0
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, " SELECT  GL.TransType,FY.Status,CONVERT(VARCHAR(10),Min(GL.StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(GL.Enddate),101) as Enddate FROM GeneralLedger GL Inner Join FiscalYear FY On GL.BankDate between FY.StartDate and FY.Enddate  WHERE TransType='IGS' and FY.Status ='O' Group by GL.TransType,GL.StartDate,GL.Enddate, FY.Status Order by Year(GL.StartDate) Asc,GL.StartDate Asc ")
            If ds.Tables(0).Rows.Count > 0 Then
                TableStr = TableStr & "<tr><td align=Center> Sell Transactions </td><td align=Center>" & ds.Tables(0).Rows(0)("TransType") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("Status") & "</td><td align=Center>" & ds.Tables(0).Rows(0)("StartDate") & " </td><td align=Center> " & ds.Tables(0).Rows(0)("EndDate") & "</td></tr>"
                For i As Integer = 1 To ds.Tables(0).Rows.Count - 1
                    Status = ds.Tables(0).Rows(i)("Status")
                    TransType = ds.Tables(0).Rows(i)("TransType")
                    StartDate = ds.Tables(0).Rows(i)("StartDate")
                    EndDate = ds.Tables(0).Rows(i)("Enddate")
                    TableStr = TableStr & "<tr><td align=Center></td><td align=Center>" & TransType & "</td><td align=Center>" & Status & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
                Next
            End If
        Else
            TableStr = TableStr & "<tr><td align=Center> Sell Transactions </td><td align=Center></td><td align=Center> N </td><td align=Center></td><td align=Center> </td></tr>"
        End If
        Status = ""
        StartDate = ""
        EndDate = ""
        BegRecId = 0
        EndRecId = 0
        count = 0
        Cnt = 0

        TableStr = TableStr & "</Table><br><br>"
        Literal1.Text = TableStr.ToString
    End Sub
    'reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "SELECT Top 1 CONVERT(VARCHAR(10),MAX(StartDate),101) as StartDate,CONVERT(VARCHAR(10),MAX(Enddate),101) as Enddate From GeneralLedger Where TransType='IGS' order by EndDate desc ")
    '    While reader.Read()
    '        StartDate = reader("StartDate")
    '        EndDate = reader("Enddate")
    '    End While
    '    TableStr = TableStr & "<tr><td align=Center> Sell Transactions </td><td align=Center> Y </td><td align=Center>" & Cnt & "</td><td align=Center>" & StartDate & " </td><td align=Center> " & EndDate & "</td></tr>"
    '    reader.Close()
    '    Cnt = 0
    'reader.Close()
End Class
