using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using Microsoft.ApplicationBlocks.Data;
using System.Data.SqlClient;
using System.Net.Mail;
 
public partial class ChangeEmail2 : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        GridView1.DataSource = ChangeEmailDs;
        GridView1.DataBind();

    }
  
    protected void GridView1_RowEditing(object sender, GridViewEditEventArgs e)
    {

    }
    protected void GridView1_SelectedIndexChanged(object sender, EventArgs e)
    {
        int CustEmail2Match = 0;
        int SpouseEmail2Match = 0;
        int CustEmailOk = 0;
        int SpouseEmailOk = 0;
        int SpouseMemberID;
     string Name = GridView1.SelectedRow.Cells[5].Text + " "  + GridView1.SelectedRow.Cells[6].Text ;
        DataTable tblSpouse,tblDuplicateList,TblVerSpoIsVol;
        bool isDuplicate, isDuplicate1,IsVolunteer,IsSpouseVol, isExisting, isExisting1;
       SqlConnection  conn = new SqlConnection(Application["ConnectionString"].ToString());
       string s = GridView1.SelectedRow.Cells[0].Text.TrimStart();
       
        int SelectedAutomemberID = Convert.ToInt32(((LinkButton)GridView1.SelectedRow.FindControl("lnkmemberID")).Text);
       SpouseMemberID =   Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_GetSpouseID", new SqlParameter("@MemberID", SelectedAutomemberID), new SqlParameter("@Type",GridView1.SelectedRow.Cells[2].Text   )));
       // SpouseMemberIDTableAdapters.IndSpouseTableAdapter  SpouseID = new SpouseMemberIDTableAdapters.IndSpouseTableAdapter();
       //tblSpouse = SpouseID.GetData(SelectedAutomemberID);
       //GridView2.DataSource = tblSpouse;
       //GridView2.DataBind();

        if (GridView1.SelectedRow.Cells[0].Text != "&nbsp;")
        {
        // Verify in LoginMAster
       isDuplicate = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_IsDuplicateEmail", new SqlParameter("@NewEmail", Server.HtmlEncode(GridView1.SelectedRow.Cells[0].Text))));
        
            //verify in Indspouse
       DuplicateEmailListTableAdapters.IndSpouseTableAdapter DuplicateInndList = new DuplicateEmailListTableAdapters.IndSpouseTableAdapter();
           
       tblDuplicateList = DuplicateInndList.GetData(GridView1.SelectedRow.Cells[0].Text);
       int count = tblDuplicateList.Rows.Count;
    
            //Verify SecondaryEmails in Indspouse.

   
       if (isDuplicate == false)
       {
           if (count == 0)
           {
               CustEmailOk = 1;

               SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "usp_ChangeEmail", new SqlParameter("@NewEmail", Server.HtmlEncode(GridView1.SelectedRow.Cells[0].Text)), new SqlParameter("@CurrentEmail", GridView1.SelectedRow.Cells[1].Text), new SqlParameter("@ModifiedBy", Session["LoginEmail"]));

               //change email
               sentemail("Email Change Request", "Email Of" + Name + "Was Changed", GridView1.SelectedRow.Cells[0].Text);
               GridView1.DataSource = ChangeEmailDs;
               GridView1.DataBind();

           }
           else
           {
               //if (Convert.ToInt32(tblDuplicateList.Rows[0][0]) != SelectedAutomemberID)
               //{
          
                   // Erorr Message: Some one else is using 
                   for (int i = 0; i <= tblDuplicateList.Rows.Count -1; i++)
                   {

                       if (Convert.ToInt32(tblDuplicateList.Rows[i][0]) != SpouseMemberID && Convert.ToInt32(tblDuplicateList.Rows[0][0]) != SelectedAutomemberID)
                       {
                           //outside
                           //Error Message
                           sentemail( "Email Change Request", "The new email you gave already exists in our records",GridView1.SelectedRow.Cells[1].Text);
                          

                       }
                       else
                       {

                           IsSpouseVol = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_verIfSpouseisVol", new SqlParameter("@Email", GridView1.SelectedRow.Cells[0].Text), new SqlParameter("@SpouseID", SpouseMemberID)));
                           //ChangeEmailTableAdapters.verifySpouseisVolunteerTableAdapter objverify = new ChangeEmailTableAdapters.verifySpouseisVolunteerTableAdapter();
                           //TblVerSpoIsVol = objverify.GetData(GridView1.SelectedRow.Cells[0].Text, SpouseMemberID);
                           if (IsSpouseVol == true)
                           {
                               sentemail("Email Change Request", "Your spouse already has the new email you gave us", GridView1.SelectedRow.Cells[1].Text);
                          
                              // Eror Message
                               

                           }
                       }
                       
                   }
              // }
               //veirfy secondary email of  one of the spouse
               isExisting = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ClearScondaryEmail", new SqlParameter("@MemberID", SelectedAutomemberID), new SqlParameter("@NewEmail", Server.HtmlEncode(GridView1.SelectedRow.Cells[0].Text))));
               if (isExisting)
               {
                   CustEmail2Match = 1;
                   
               }

               //veirfy secondary email of the  other spouse


               isExisting1 = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_ClearScondaryEmail", new SqlParameter("@MemberID", SpouseMemberID), new SqlParameter("@NewEmail", Server.HtmlEncode(GridView1.SelectedRow.Cells[0].Text))));
               if (isExisting1)
               {
                   SpouseEmail2Match = 1;
                  // SendEmail();
               }

             

               IsVolunteer = Convert.ToBoolean(SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "usp_IsVolunteer", new SqlParameter("@MemberID", SpouseMemberID), new SqlParameter("@CurrentEmail", GridView1.SelectedRow.Cells[0].Text)));
               if (IsVolunteer == true)
               {

                   //Error messgae
                   sentemail("Email Change Request", "Your spouse already has the new email you gave us",GridView1.SelectedRow.Cells[1].Text);
               }

               else
               {
                   //change email of the spouse
                   sentemail("Email Change Request", "Your spouse already has the new email you gave us", GridView1.SelectedRow.Cells[1].Text);
               }
               

               //verify if the other spouse is using the new email
                    
           }
       }
       

        }

        
    }

    private void sentemail(string sSubject, string sBody, string strMailTo)
    {
        string sFrom = System.Configuration.ConfigurationManager.AppSettings.Get("ContestEmail");
        string host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost");
        MailMessage mail = new MailMessage(sFrom, strMailTo, sSubject, sBody);
        
        SmtpClient client = new SmtpClient();
         client.Host = host;
          mail.IsBodyHtml = true;
        bool ok = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
            ok = false;
        }


    }
    protected void ChangeEmailDs_Selecting(object sender, SqlDataSourceSelectingEventArgs e)
    {

    }
}

 