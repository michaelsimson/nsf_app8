

<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="BulkMailPO.aspx.vb" Inherits="BulkMailPO" title="Bulk Mail PO" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
   <div style="text-align:left">
      <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
      &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
   </div>
   <div  style ="width:100%;text-align: center ">
      <script language="javascript" type="text/javascript">
          function PopupPicker(ctl, w, h) {
              var PopupWindow = null;
              settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
              PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
              PopupWindow.focus();
          }
      </script>
      <script language="javascript" type="text/javascript">
          function PopupPicker() {
              var PopupWindow = null;
              settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
              PopupWindow = window.open('BulkMailPOHelp.aspx'  , 'BulkMailPOHelp', settings);
              PopupWindow.focus();
          }
      </script>
      <table border="0px"  cellpadding = "5px" cellspacing = "0px" class ="SmallFont" align="center" >
         <tr>
            <td align = "center"  colspan ="3"> Bulk Surface Mail List</td>
         </tr>
         <%--<tr><td align = "left">From Date</td><td align = "left" ><asp:TextBox ID="txtFrmDate" runat="server"></asp:TextBox></td><td align = "left" > <a href="javascript:PopupPicker('<%=txtFrmDate.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a></td> </tr> 
            --%>
         <tr>
            <td align = "left" >To Date </td>
            <td align = "left">
               <asp:TextBox ID="txtToDate" runat="server"></asp:TextBox>
            </td>
            <td align = "left">  <a href="javascript:PopupPicker('<%=txtToDate.ClientID%>', 200, 200);"><img src="images/calendar.gif" border="0"></a></td>
         </tr>
         <tr>
            <td align = "left">Amount</td>
            <td align = "left" >
               <asp:TextBox ID="txtAmt" runat="server" Text="100"></asp:TextBox>
            </td>
            <td align = "left" ><a href="javascript:PopupPicker();">Help</a></td>
         </tr>
         <tr>
            <td align = "center" colspan="3">
               <asp:Button ID="btnGenerate" OnClick ="btnGenerate_Click" runat="server" Text="Generate report" />
               &nbsp;&nbsp;&nbsp;
               <asp:Button ID="btnExportAll" OnClick="btnExportAll_Click" runat="server" Text="Export" Enabled="false" />
            </td>
         </tr>
      </table>
      <table border="0px"  cellpadding = "5px" cellspacing = "0px" class ="SmallFont" align="center" >
         <tr>
            <td align ="center" colspan="3">
               <asp:Button ID="btnExport" Width="90px" OnClick="btnExport_Click" runat="server" Text="Export Donor" Enabled="false" />
               &nbsp;&nbsp;&nbsp; 
               <asp:Button ID="btnExport1" Width="110px" OnClick="btnExport1_Click" runat="server" Text="Export Volunteer" Enabled="false" />
               &nbsp;&nbsp;&nbsp; 
               <asp:Button ID="btnExport2" Width="120px" OnClick="btnExport2_Click" runat="server" Text="Export WellWisher" Enabled="false" />
            </td>
         </tr>
         <tr>
            <td align = "center" colspan="3">* Date is optional</td>
         </tr>
      </table>
      <asp:Label ID="lblVErr" runat="server" ></asp:Label>
      <br />
      <asp:GridView Visible="false"    HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" RowStyle-CssClass="SmallFont">
         <Columns>
            <%--<asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
               --%>
            <asp:BoundField DataField="AutomemberId" headerText="Member Id" ></asp:BoundField>
            <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
            <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
            <asp:BoundField DataField="SpFirstName" headerText="Sp_FirstName" ></asp:BoundField>
            <asp:BoundField DataField="SpLastName" headerText="Sp_LastName" ></asp:BoundField>
            <asp:BoundField DataField="PubName" headerText="PubName" ></asp:BoundField>
            <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
            <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
            <asp:BoundField DataField="CPhone" headerText="Cell Phone" ></asp:BoundField>
            <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
            <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
            <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
            <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
            <asp:BoundField DataField="RoleID" headerText="RoleID" ></asp:BoundField>
            <asp:BoundField DataField="StatusType" headerText="Flag" ></asp:BoundField>
            <asp:BoundField DataField="DonationAmount" headerText="Donation Amount" ItemStyle-HorizontalAlign ="Right" DataFormatString="{0:c}" ></asp:BoundField>
         </Columns>
      </asp:GridView>
      <br />
      <asp:Label ID="Label1" Visible="false" Font-Bold="True"  runat="server" Text="Volunteer" ></asp:Label>
      <br />
      <asp:GridView Visible="false"    HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt1" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" RowStyle-CssClass="SmallFont">
         <Columns>
            <%--<asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
               --%>
            <asp:BoundField DataField="AutomemberId" headerText="Member Id" ></asp:BoundField>
            <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
            <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
            <asp:BoundField DataField="SpFirstName" headerText="Sp_FirstName" ></asp:BoundField>
            <asp:BoundField DataField="SpLastName" headerText="Sp_LastName" ></asp:BoundField>
            <asp:BoundField DataField="PubName" headerText="PubName" ></asp:BoundField>
            <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
            <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
            <asp:BoundField DataField="CPhone" headerText="Cell Phone" ></asp:BoundField>
            <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
            <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
            <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
            <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
            <asp:BoundField DataField="RoleID" headerText="RoleID" ></asp:BoundField>
            <asp:BoundField DataField="StatusType" headerText="Flag" ></asp:BoundField>
            <asp:BoundField DataField="DonationAmount" headerText="Donation Amount" ItemStyle-HorizontalAlign ="Right" DataFormatString="{0:c}" ></asp:BoundField>
         </Columns>
      </asp:GridView>
      <br /> 
      <asp:Label ID="Label2" Visible="False"  runat="server" Text="Well-wishers" 
         Font-Bold="True" ></asp:Label>
      <br />
      <asp:GridView Visible="false"    HorizontalAlign="center" RowStyle-HorizontalAlign="Left"  ID="GridMemberDt2" DataKeyNames="AutomemberId" AutoGenerateColumns = "false" runat="server" RowStyle-CssClass="SmallFont">
         <Columns>
            <%--<asp:ButtonField DataTextField="AutomemberId" headerText="Member Id" ></asp:ButtonField>
               --%>
            <asp:BoundField DataField="AutomemberId" headerText="Member Id" ></asp:BoundField>
            <asp:BoundField DataField="FirstName" headerText="FirstName" ></asp:BoundField>
            <asp:BoundField DataField="LastName" headerText="Last Name" ></asp:BoundField>
            <asp:BoundField DataField="SpFirstName" headerText="Sp_FirstName" ></asp:BoundField>
            <asp:BoundField DataField="SpLastName" headerText="Sp_LastName" ></asp:BoundField>
            <asp:BoundField DataField="PubName" headerText="PubName" ></asp:BoundField>
            <asp:BoundField DataField="email" headerText="E-Mail" ></asp:BoundField>
            <asp:BoundField DataField="HPhone" headerText="Home Phone" ></asp:BoundField>
            <asp:BoundField DataField="CPhone" headerText="Cell Phone" ></asp:BoundField>
            <asp:BoundField DataField="address1" headerText="Address" ></asp:BoundField>
            <asp:BoundField DataField="city" headerText="City" ></asp:BoundField>
            <asp:BoundField DataField="state" headerText="State" ></asp:BoundField>
            <asp:BoundField DataField="zip" headerText="Zip" ></asp:BoundField>
            <asp:BoundField DataField="RoleID" headerText="RoleID" ></asp:BoundField>
            <asp:BoundField DataField="StatusType" headerText="Flag" ></asp:BoundField>
            <asp:BoundField DataField="DonationAmount" headerText="Donation Amount" ItemStyle-HorizontalAlign ="Right" DataFormatString="{0:c}" ></asp:BoundField>
         </Columns>
      </asp:GridView>
   </div>
</asp:Content>

