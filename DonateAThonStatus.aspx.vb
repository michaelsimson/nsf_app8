﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Partial Class DonateAThonStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack = True Then

            If Not Session("CustIndID") Is Nothing Then
                Dim year As Integer = 0
                year = Convert.ToInt32(DateTime.Now.Year)

                ddlEventYear.Items.Insert(0, Convert.ToString(year))
                ddlEventYear.Items.Insert(1, Convert.ToString(year - 1))
                ddlEventYear.Items.Insert(2, Convert.ToString(year - 2))
                ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
                loadddl()
            Else
                Response.Redirect("Maintest.aspx")
            End If
        End If
    End Sub

    Private Sub loadddl()
        Dim strsql As String = "select CASE WHEN WM.ChapterID=1 THEN 'National Finals : ' Else 'Chapter Contests :' END + ' ' + E.Name  + ' - ' + CASE WHEN WM.Relationship='Child' THEN C.First_Name+' '+C.Last_Name Else CASE WHEN WM.Relationship='Spouse' THEN I1.FirstName +' ' + I1.LastName ELSE I.FirstName +' ' + I.LastName END END as WName , WM.WalkMarID  from WalkMarathon WM INNER JOIN IndSpouse I ON WM.MemberID = I.AutoMemberID Left Join IndSpouse I1 ON WM.WMemberID = I1.AutoMemberID AND  WM.Relationship='Spouse' INNER JOIN Event E ON WM.EventID = E.EventId Left Join Child C On C.ChildNumber = WM.WMemberID and WM.Relationship='Child' where WM.EventYear  = " & ddlEventYear.SelectedItem.Text & " and WM.MemberID=" & Session("CustIndID") & " Order By WM.CreateDate Desc"
        Dim ds As DataSet = New DataSet()
        Try
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strsql)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlSelection.Visible = True
                ddlSelection.DataSource = ds
                ddlSelection.DataBind()
                HlinkCustomPage.Visible = True
                HlinkCustomPage.NavigateUrl = "http://" + Request.Url.Host + Request.ApplicationPath + "/Don_athon_custom.aspx?NSFDONATE=" + encode(ddlSelection.SelectedValue.ToString())
                getData()
                div2.Visible = True
            Else
                ddlSelection.Visible = False
                HlinkCustomPage.Visible = False
                GVwList.Visible = False
                lblSponsor.Visible = False
                lblErr.Text = "No record to Display"
                div1.Visible = True
                lblErr.Text = "no registration to display"
                div2.Visible = False
            End If
        Catch ex As Exception
            Response.Write(strsql & "<BR>" & ex.ToString())
        End Try
    End Sub

    Function encode(ByVal inputText As String) As String
        Dim bytesToEncode As Byte() = Encoding.UTF8.GetBytes(inputText)
        Dim encodedText As String = Convert.ToBase64String(bytesToEncode)
        Return encodedText
    End Function

    Protected Sub ddlSelection_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        getData()
        HlinkCustomPage.NavigateUrl = "http://" + Request.Url.Host + Request.ApplicationPath + "/Don_athon_custom.aspx?NSFDONATE=" + encode(ddlSelection.SelectedValue.ToString())
    End Sub

    Private Sub getData()
        lblErr.Text = ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select I.FirstName+' '+I.LastName as Name,WM.CreateDate, WM.PledgeAmount as PledgeAmount,WM.Comment from  WMSponsor WM Inner Join IndSpouse I ON I.AutoMemberID = WM.MemberID  where ((WM.PaymentMode ='Credit Card' and WM.PaymentReference is not null) OR (WM.PaymentMode <>'Credit Card')) AND WM.WalkMarID = " & ddlSelection.SelectedValue & "")
        If ds.Tables(0).Rows.Count > 0 Then
            lblSponsor.Visible = True
            GVwList.DataSource = ds
            GVwList.DataBind()
            GVwList.Visible = True
            div1.Visible = False
        Else
            GVwList.Visible = False
            lblSponsor.Visible = False
            lblErr.Text = "No record to Display"
            div1.Visible = True
        End If

    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        loadddl()
    End Sub
End Class
