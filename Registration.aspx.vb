'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/Registration.aspx  
'   Author    : NSF
'   Contact   : NSF'
'	Desc:	This page enables the user to Register them as Volunteer  
'           to the NSF.'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetVolunteer
'           2) NSF\usp_GetVolunteerSpouse
'           3) NSF\usp_Insert_Volunteer
'           4) NSF\'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Net
Imports System.Net.Mail
Imports System.Text
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Web.SessionState
Imports nsf.Entities
Imports nsf.Data.SqlClient

Namespace VRegistration

    Partial Class Registration
        Inherits System.Web.UI.Page
        'Implements System.Web.UI.ICallbackEventHandler
#Region " Web Form Designer Generated Code "

        'This call is required by the Web Form Designer.
        <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

        End Sub
        Protected WithEvents txtSecEmailSp As System.Web.UI.WebControls.TextBox
        Protected WithEvents ckbAddressSp As System.Web.UI.WebControls.CheckBox


        Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
            'CODEGEN: This method call is required by the Web Form Designer
            'Do not modify it using the code editor.
            InitializeComponent()
        End Sub

#End Region
        Dim clientData As String
        Dim FirstTimeIndicator As Boolean = False
        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load, Me.Load
            Try
                'Session("LoggedIn") = "true"
                'Session("entryToken") = "parent"
                'Session("RoleId") = 1
                'Session("LoginID") = "74347"
                'Session("LoginEmail") = "mahanthi.bukkapatnam@gmail.com"
                If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
                    Server.Transfer("login.aspx")
                End If
                If Session("LoginEmail") Is Nothing Then
                    Server.Transfer("login.aspx")
                End If

                lblMsg.Text = String.Empty
                If LCase(Session("LoggedIn")) <> "true" Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                If Not Session("entryToken") Is Nothing Then
                    If Session("entryToken").ToString = "Parent" Then
                        If Not Session("PanelFrom") Is Nothing Then
                            Dim PanelId As String = Session("PanelFrom").ToString
                            If PanelId = "RegCont" Or PanelId = "Finals" Or PanelId = "Workshop" Or PanelId = "PrepClub" Or PanelId = "Games" Or PanelId = "OnlineCoaching" Or PanelId = "General" Then
                                FirstTimeIndicator = False
                            Else
                                FirstTimeIndicator = True
                            End If
                        Else
                            FirstTimeIndicator = True
                        End If
                    End If
                End If

                Dim objIndSpouse As New IndSpouse10

                Dim objSpouse As IndSpouse10
                Dim nextPage As Boolean

                If Session("nextPageName") Is Nothing Then
                    Session("nextPageName") = "MainChild.aspx"
                End If

                hide1.Visible = False
                hide2.Visible = False

                If Page.IsPostBack = False Then
                    Session("AddUpdateFlag") = "False"
                    Session("GetIndSpouseID") = 0
                    setSpMandatoryIndicator(False)
                    If Session("entryToken").ToString.ToUpper() = "DONOR" Then
                        hlnkMainMenu.Text = "Back to Main Page"
                        rfvHomePhoneInd.Enabled = False
                    ElseIf Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                        hlnkMainMenu.Text = "Back to Main Page"
                    ElseIf Session("entryToken").ToString.ToUpper() = "PARENT" Then
                        hlnkMainMenu.Text = "Back to Main Page"
                    ElseIf Session("entryToken").ToString.ToUpper() = "WALKATHON_MARATHON" Then
                        hlnkMainMenu.Text = "Back to Main Page"
                    End If
                    Page.MaintainScrollPositionOnPostBack = True
                    '*** Populate State DropDown
                    Dim dsStates As DataSet

                    dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                    If dsStates.Tables.Count > 0 Then
                        ddlStateInd.DataSource = dsStates.Tables(0)
                        ddlStateInd.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                        ddlStateInd.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                        ddlStateInd.SelectedIndex = -1
                        ddlStateInd.DataBind()
                        ddlStateInd.Items.Insert(0, New ListItem("Select State", String.Empty))
                        ddlStateSp.DataSource = dsStates.Tables(0)
                        ddlStateSp.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                        ddlStateSp.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                        ddlStateSp.SelectedIndex = -1
                        ddlStateSp.DataBind()
                        ddlStateSp.Items.Insert(0, New ListItem("Select State", String.Empty))
                    End If
                    ddlStateInd.SelectedIndex = 0
                    ddlStateSp.SelectedIndex = 0
                    '*** Populate Chapter DropDown
                    '*** Populate Chapter Names List
                    Dim objChapters As New NorthSouth.BAL.Chapter
                    Dim dsChapters As New DataSet
                    objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

                    If dsChapters.Tables.Count > 0 Then
                        ddlChapterInd.DataSource = dsChapters.Tables(0)
                        ddlChapterInd.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                        ddlChapterInd.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                        ddlChapterInd.DataBind()
                        ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                        If Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                            If Len(Session("LoginChapterID")) > 0 Then
                                ddlChapterInd.SelectedValue = Session("LoginChapterID")
                                ddlChapterInd.Enabled = False
                            End If
                        End If
                        If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
                            ddlChapterInd.Items.Clear()
                            Dim strSql As String
                            strSql = "Select chapterid, chaptercode, state from chapter "
                            If Session("RoleID") = "4" Then
                                strSql = strSql & " where clusterid in (Select clusterid from "
                                strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                            ElseIf Session("RoleID") = "5" Then
                                strSql = strSql & " where chapterid = " + Session("LoginChapterID") & ""
                            End If
                            strSql = strSql & " order by state, chaptercode"
                            Dim con As New SqlConnection(Application("ConnectionString"))
                            Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                            While (drNSFChapters.Read())
                                ddlChapterInd.Items.Add(New ListItem(drNSFChapters(1).ToString(), drNSFChapters(0).ToString()))
                            End While
                            If ddlChapterInd.Items.Count > 1 Then
                                ddlChapterInd.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                                ddlChapterInd.Enabled = True
                            ElseIf ddlChapterInd.Items.Count = 1 Then
                                ddlChapterInd.Enabled = False
                            End If
                        End If
                    End If
                    setSpouseValidators(False)
                    If Not Request.QueryString("Type") Is Nothing Then
                        If Request.QueryString("Type") = "Individual" Then
                            If Session("NavPath") = "Sponsor" Then
                                hlnkPrevPage.Visible = True
                            ElseIf Session("NavPath") = "Search" Then
                                hlnkPrevPage.Visible = True
                            Else
                                hlnkPrevPage.Visible = False
                            End If
                        Else
                            Exit Sub
                        End If
                        If Session("NavPath") = "Search" Then
                            rfvEmail.Enabled = False
                            revPrimaryEmailInd.Enabled = False
                            rfvPrimaryEmailSp.Enabled = False
                            revPrimaryEMailSp.Enabled = False
                            rfvHomePhoneInd.Enabled = False
                            rfvCountryInd.Enabled = False
                            rfvCountryOfOriginSp.Enabled = False
                        ElseIf Session("NavPath") = "Sponsor" Then
                            rfvEmail.Enabled = False
                            revPrimaryEmailInd.Enabled = False
                            rfvHomePhoneInd.Enabled = False
                            rfvPrimaryEmailSp.Enabled = False
                            revPrimaryEMailSp.Enabled = False
                        End If
                    Else
                        'Get IndID and SpouseID for the givn Logon Person
                        Dim StrIndSpouse As String = ""
                        Dim intIndID As Integer = 0
                        Dim dsIndSpouse As New DataSet
                        If Request.QueryString("Id") Is Nothing Then
                            StrIndSpouse = "Email='" & Session("LoginEmail") & "'"
                            hlnkPrevPage.Visible = True
                            If Not Session("NavPath") = "Sponsor" And Not Session("NavPath") = "Search" Then
                                hlnkPrevPage.Visible = False
                            End If
                        Else
                            StrIndSpouse = "AutoMemberID=" & Request.QueryString("ID")
                        End If
                        If Session("NavPath") = "Search" Then
                            rfvEmail.Enabled = False
                            rfvHomePhoneInd.Enabled = False
                            revPrimaryEmailInd.Enabled = False
                            rfvPrimaryEmailSp.Enabled = False
                            revPrimaryEMailSp.Enabled = False
                            rfvCountryInd.Enabled = False
                            rfvCountryOfOriginSp.Enabled = False
                            hlnkPrevPage.Visible = True
                        ElseIf Session("NavPath") = "Sponsor" Then
                            rfvEmail.Enabled = False
                            rfvHomePhoneInd.Enabled = False
                            revPrimaryEmailInd.Enabled = False
                            rfvPrimaryEmailSp.Enabled = False
                            revPrimaryEMailSp.Enabled = False
                            hlnkPrevPage.Visible = True
                        End If

                        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)
                        If dsIndSpouse.Tables.Count > 0 Then
                            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                                    End If
                                Else
                                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                    Else
                                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("RelationShip")
                                    End If
                                End If
                            Else
                                txtPrimaryEmailInd.Text = Session("LoginEmail")
                                txtPrimaryEmailInd.Enabled = False
                                Try
                                    ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                                Catch
                                    ddlChapterInd.Items.FindByValue(String.Empty).Selected = True
                                End Try
                            End If
                        Else
                            txtPrimaryEmailInd.Text = Session("LoginEmail")
                            txtPrimaryEmailInd.Enabled = False
                            ddlChapterInd.Items.FindByValue(Session("ChapterID")).Selected = True
                        End If

                        If intIndID > 0 Then
                            'Start Grade locking code
                            '*******************************************
                            '**** Code Added By FERDINE Jan 05 2010 ****
                            '*******************************************
                            If (Not (Session("RoleId") = Nothing)) Then
                                If (((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "3") Or (Session("RoleId").ToString() = "4") Or (Session("RoleId").ToString() = "5"))) Then
                                Else
                                    lockchapter(intIndID)
                                End If
                            Else
                                lockchapter(intIndID)
                            End If
                            'get spouse
                            Dim objIndSpouseExt As IndSpouseExt = New IndSpouseExt 'IndSpouseExt = New IndSpouseExt

                            objIndSpouseExt.GetIndSpouseByID(Application("ConnectionString"), intIndID)

                            If objIndSpouseExt.Id > 0 Then
                                objIndSpouse = objIndSpouseExt
                            End If
                            Me.btnSubmit.Text = "Continue"
                            nextPage = True
                        End If
                        'set spouse validators to false.
                        txtPrimaryEmailInd.ReadOnly = False
                        txtPrimaryEmailSp.ReadOnly = False
                        ddlStateOfOriginInd.Visible = True
                        ddlStateOfOriginSp.Visible = True
                        loadStates(Me.ddlStateOfOriginInd, "IN")
                        loadStates(Me.ddlStateOfOriginSp, "IN")
                        If Not objIndSpouse Is Nothing Then
                            If objIndSpouse.Id > 0 Then
                                'Found Session Ferdine
                                Session("GetIndSpouseID") = objIndSpouse.Id
                                With objIndSpouse
                                    Session("CustIndID") = .Id
                                    If Trim(.Title.Length) > 0 Then
                                        Try
                                            ddlTitleInd.Items.FindByValue(.Title).Selected = True
                                        Catch
                                        End Try
                                    End If
                                    txtFirstNameInd.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                    txtLastNameInd.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                    txtAddress1Ind.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                    txtAddress2Ind.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                    txtCityInd.Text = StrConv(.City, VbStrConv.ProperCase)
                                    If Trim(.State.Length) > 0 Then
                                        ddlStateInd.SelectedValue = .State
                                    End If
                                    txtZipInd.Text = .Zip
                                    If Trim(.Country.Length) > 0 Then
                                        ddlCountryInd.SelectedValue = .Country
                                    End If
                                    If Trim(.Gender.Length) > 0 Then
                                        ddlGenderInd.SelectedValue = .Gender
                                    End If
                                    txtHomePhoneInd.Text = .HPhone
                                    txtWorkPhoneInd.Text = .WPhone
                                    txtWorkFaxInd.Text = .WFax
                                    txtCellPhoneInd.Text = .CPhone
                                    txtPrimaryEmailInd.Text = .Email
                                    lblhiddenInd.Text = .Email
                                    txtPrimaryEmailInd.Enabled = False
                                    txtSecondaryEmailInd.Text = .SecondaryEmail

                                    If Trim(.Education.Length) > 0 Then
                                        ddlEducationalInd.SelectedValue = .Education
                                    End If
                                    If Trim(.Career.Length) > 0 Then
                                        ddlCareerInd.SelectedValue = .Career
                                    End If
                                    txtEmployerInd.Text = StrConv(Server.HtmlDecode(.Employer), VbStrConv.ProperCase)
                                    If Trim(.CountryOfOrigin.Length) > 0 Then
                                        ddlCountryOfOriginInd.SelectedValue = .CountryOfOrigin
                                    End If
                                    If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
                                        If Trim(.StateOfOrigin.Length) > 0 Then
                                            ddlStateOfOriginInd.SelectedValue = .StateOfOrigin
                                        End If
                                        txtStateOfOriginInd.Visible = False
                                    Else
                                        txtStateOfOriginInd.Visible = True
                                        txtStateOfOriginInd.Text = .StateOfOrigin
                                        ddlStateOfOriginInd.Visible = False
                                    End If

                                    If .YouthVol = "Yes" Then
                                        rbVolunteerInd.SelectedIndex = 0
                                        rbVolunteerInd_SelectedIndexChanged(rbVolunteerInd, New EventArgs)
                                    Else
                                        rbVolunteerInd.SelectedIndex = 1
                                    End If

                                    If .Liaison = "Yes" Or .Liaison = "No" Then
                                        rbLiaisonInd.SelectedValue = .Liaison
                                    Else
                                        rbLiaisonInd.Items(0).Selected = False
                                        rbLiaisonInd.Items(1).Selected = False
                                    End If

                                    If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                        rbSponsorInd.SelectedValue = .Sponsor
                                    Else
                                        rbSponsorInd.Items(0).Selected = False
                                        rbSponsorInd.Items(1).Selected = False
                                    End If

                                    ddlMaritalStatusInd.SelectedValue = .MaritalStatus

                                    If Trim(.Chapter.Length) > 0 Then
                                        Try
                                            ddlChapterInd.Items.FindByValue(.ChapterID).Selected = True
                                        Catch
                                        End Try
                                    End If
                                    Try
                                        If Trim(.ReferredBy.Length) >= 4 And Not IsDBNull(.ReferredBy) Then
                                            ddlReferredByInd.Items.FindByValue(.ReferredBy).Selected = True
                                        End If
                                    Catch ex As Exception

                                    End Try
                                    Dim StrSpouse As String = ""
                                    Dim intSpouseID As Integer = 0
                                    Dim dsSpouse As New DataSet
                                    StrSpouse = "Relationship='" & Session("CustIndID") & "'"
                                    objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                                    If dsSpouse.Tables.Count > 0 Then
                                        If dsSpouse.Tables(0).Rows.Count > 0 Then
                                            intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                                        End If
                                    End If
                                    If intSpouseID > 0 Then
                                        objSpouse = New IndSpouse10
                                        objSpouse.GetIndSpouseByID(Application("ConnectionString"), intSpouseID)
                                        Session("GetSpouseID") = objSpouse.Id
                                        If Not objSpouse Is Nothing Then
                                            With objSpouse
                                                Session("CustSpouseID") = .Id
                                                If Trim(.Title.Length) > 0 Then
                                                    Try
                                                        ddlTitleSp.Items.FindByValue(.Title).Selected = True
                                                    Catch
                                                    End Try
                                                End If
                                                txtFirstNameSp.Text = StrConv(.FirstName, VbStrConv.ProperCase)
                                                rbVolunteerInd.Items(1).Selected = True
                                                rbVolunteerInd.Items(0).Enabled = False
                                                txtLastNameSp.Text = StrConv(.LastName, VbStrConv.ProperCase)
                                                txtAddress1Sp.Text = StrConv(.Address1, VbStrConv.ProperCase)
                                                txtAddress2Sp.Text = StrConv(.Address2, VbStrConv.ProperCase)
                                                txtCitySp.Text = StrConv(.City, VbStrConv.ProperCase)
                                                If Trim(.State.Length) > 0 Then
                                                    ddlStateSp.SelectedValue = .State
                                                End If
                                                txtZipSp.Text = .Zip
                                                If Trim(.Country.Length) > 0 Then
                                                    ddlCountrySp.SelectedValue = .Country
                                                End If
                                                If Trim(.Gender.Length) > 0 Then
                                                    ddlGenderSp.SelectedValue = .Gender
                                                End If

                                                txtHomePhoneSp.Text = .HPhone
                                                txtWorkPhoneSp.Text = .WPhone
                                                txtWorkFaxSp.Text = .WFax
                                                txtCellPhoneSp.Text = .CPhone

                                                If Trim(.Email.Length) = 0 Then
                                                    txtPrimaryEmailSp.Enabled = True
                                                Else
                                                    txtPrimaryEmailSp.Text = .Email
                                                    lblhiddensp.Text = .Email
                                                    txtPrimaryEmailSp.Enabled = False
                                                End If
                                                txtSecondaryEmailSp.Text = .SecondaryEmail
                                                If Trim(.Education.Length) > 0 Then
                                                    ddlEducationSp.SelectedValue = .Education
                                                End If
                                                If Trim(.Career.Length) > 0 Then
                                                    ddlCareerSp.SelectedValue = .Career
                                                End If
                                                txtEmployerSp.Text = StrConv(Server.HtmlDecode(.Employer), VbStrConv.ProperCase)

                                                If Trim(.CountryOfOrigin.Length) > 0 Then
                                                    ddlCountryOfOriginSp.SelectedValue = .CountryOfOrigin
                                                End If

                                                If loadStates(Me.ddlStateOfOriginSp, Me.ddlCountryOfOriginSp.SelectedValue) Then
                                                    If Trim(.StateOfOrigin.Length) > 0 Then
                                                        ddlStateOfOriginSp.SelectedValue = .StateOfOrigin
                                                    End If
                                                    txtStateOfOriginSp.Visible = False
                                                Else
                                                    txtStateOfOriginSp.Visible = True
                                                    txtStateOfOriginSp.Text = .StateOfOrigin
                                                    ddlStateOfOriginSp.Visible = False
                                                End If

                                                txtStateOfOriginSp.Text = .StateOfOrigin

                                                If .Liaison = "Yes" Or .Liaison = "No" Then
                                                    rbLiaisonSp.SelectedValue = .Liaison
                                                Else
                                                    rbLiaisonSp.Items(0).Selected = False
                                                    rbLiaisonSp.Items(1).Selected = False
                                                End If

                                                If .Sponsor = "Yes" Or .Sponsor = "No" Then
                                                    rbSponsorSp.SelectedValue = .Sponsor
                                                Else
                                                    rbSponsorSp.Items(0).Selected = False
                                                    rbSponsorSp.Items(1).Selected = False
                                                End If
                                                ddlMaritalStatusSp.SelectedValue = .MaritalStatus
                                                If .PrimaryContact = "Y" Then
                                                    Me.ckbPrimaryContact.Checked = True
                                                End If
                                            End With
                                            setSpouseValidators(True)
                                        End If
                                    End If
                                End With
                            End If
                        End If
                    End If
                End If
                ' to search duplicate and disable "Continue"
                ' added by Bindhu
                DuplicateRecord()

                If Session("RoleID") = "1" Or Session("RoleID") = "2" Or Session("RoleID") = "96" Or Session("RoleID") = "37" Or Session("RoleID") = "38" Then
                    txtPrimaryEmailInd.ReadOnly = False
                    txtPrimaryEmailInd.Enabled = True
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailSp.ReadOnly = False
                    Me.rfvPrimaryEmailSp.Enabled = False
                End If
                If Session("RoleID") = "37" And Session("LoginChapterID") = "48" Then
                    If Len(txtPrimaryEmailInd.Text) > 0 Then
                        txtPrimaryEmailInd.ReadOnly = True
                        txtPrimaryEmailInd.Enabled = False
                    Else
                        txtPrimaryEmailInd.ReadOnly = False
                        txtPrimaryEmailInd.Enabled = True
                    End If
                    If Len(txtPrimaryEmailSp.Text) > 0 Then
                        txtPrimaryEmailSp.Enabled = False
                        txtPrimaryEmailSp.ReadOnly = True
                        Me.rfvPrimaryEmailSp.Enabled = False
                    Else
                        txtPrimaryEmailSp.Enabled = True
                        txtPrimaryEmailSp.ReadOnly = False
                        setSpouseValidators(True)
                        Me.rfvPrimaryEmailSp.Enabled = False
                    End If
                ElseIf Session("RoleID") = "38" And Session("LoginChapterID") = "48" Then
                    If Len(txtPrimaryEmailInd.Text) > 0 Then
                        txtPrimaryEmailInd.ReadOnly = True
                        txtPrimaryEmailInd.Enabled = False
                    Else
                        txtPrimaryEmailInd.ReadOnly = False
                        txtPrimaryEmailInd.Enabled = True
                    End If
                    If Len(txtPrimaryEmailSp.Text) > 0 Then
                        txtPrimaryEmailSp.Enabled = False
                        txtPrimaryEmailSp.ReadOnly = True
                        Me.rfvPrimaryEmailSp.Enabled = False
                    Else
                        txtPrimaryEmailSp.Enabled = True
                        txtPrimaryEmailSp.ReadOnly = False
                        setSpouseValidators(True)
                        Me.rfvPrimaryEmailSp.Enabled = False
                    End If
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Sub DuplicateRecord()

            Dim cmd As String
            If Len(txtPrimaryEmailInd.Text) > 0 Then
                cmd = "SELECT COUNT(*) FROM IndSpouse where email = '" + txtPrimaryEmailInd.Text + "' GROUP BY Email HAVING Count(*)>1"
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmd) > 0 Then
                    btnSubmit.Enabled = False
                    lblErrDupInd.Visible = True
                    lblErrorDuplicate.Visible = True
                    lnkbtnChangeInd.Enabled = False
                End If
            Else
                lnkbtnChangeInd.Enabled = False
            End If
            If Len(txtPrimaryEmailSp.Text) > 0 Then
                cmd = "SELECT COUNT(*) FROM  IndSpouse where email = '" + txtPrimaryEmailSp.Text + "' GROUP BY Email HAVING Count(*)>1"
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmd) > 0 Then
                    btnSubmit.Enabled = False
                    lblErrDupSp.Visible = True
                    lblErrorDuplicate.Visible = True
                    lnkbtnChangeSp.Enabled = False
                End If
            Else
                lnkbtnChangeSp.Enabled = False
            End If
        End Sub

        Private Sub GetSpouseData()
        End Sub
        Private Function SpValidation() As Boolean
            If ddlTitleSp.SelectedItem.Text = "Select Title" Or Len(Trim(txtPrimaryEmailSp.Text)) < 1 Or Len(Trim(txtFirstNameSp.Text)) < 1 Or Len(Trim(txtLastNameSp.Text)) < 1 Or Len(Trim(txtCitySp.Text)) < 1 Or Trim(ddlStateSp.SelectedValue.ToString) = "" Or Trim(ddlCountrySp.SelectedValue.ToString) = "" Or Trim(ddlGenderSp.SelectedValue.ToString) = "" Or Trim(ddlCountryOfOriginSp.SelectedValue.ToString) = "" Or Trim(ddlMaritalStatusSp.SelectedValue.ToString) = "" Then
                Dim strFieldName As String = ""
                If ddlTitleSp.SelectedItem.Text = "Select Title" Then
                    strFieldName = strFieldName + " Title"
                End If
                If Len(Trim(txtPrimaryEmailSp.Text)) < 1 Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " Email"
                End If
                If Len(Trim(txtFirstNameSp.Text)) < 1 Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " FirstName"
                End If
                If Len(Trim(txtLastNameSp.Text)) < 1 Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " LastName"
                End If
                If Len(Trim(txtCitySp.Text)) < 1 Then
                    If strFieldName.Length = 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " City"
                End If
                If Trim(ddlStateSp.SelectedValue.ToString) = "" Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " State"
                End If

                If Trim(ddlCountrySp.SelectedValue.ToString) = "" Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " Country"
                End If
                If Trim(ddlGenderSp.SelectedValue.ToString) = "" Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " Gender"
                End If
                If Trim(ddlMaritalStatusSp.SelectedValue.ToString) = "" Then
                    If strFieldName.Length > 0 Then
                        strFieldName = strFieldName + " ,"
                    End If
                    strFieldName = strFieldName + " Marital  Status"
                End If
                lblErrorStatus.Text = "In your spouse profile, " & strFieldName & " is not complete. Please complete it."
                Return False
            End If
            Return True
        End Function

        Private Sub btnSubmit_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnSubmit.Click
            Try
                setSpMandatoryIndicator(False)
                lblErrorStatus.ForeColor = Color.Red
                lblErrorStatus.Text = ""
                lblDuplicateMsg.Text = ""
                Session("AddUpdateFlag") = "True"
                If LCase(Session("LoggedIn")) <> "true" Then
                    Server.Transfer("login.aspx?entry=" & Session("entryToken"))
                End If
                Dim indId As Integer
                Dim spId As Integer
                Dim strSql As String
                'Validate duplication for name
                If NameDuplication() = True Then
                    Exit Sub
                End If

                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    h_indemail.Value = txtPrimaryEmailInd.Text
                    h_spouseemail.Value = txtPrimaryEmailSp.Text

                    Dim PanelId As String
                    If Session("PanelFrom") Is Nothing Then
                        PanelId = "New"
                    Else
                        PanelId = Session("PanelFrom").ToString
                    End If
                    If PanelId = "RegCont" Or PanelId = "Finals" Or PanelId = "Workshop" Or PanelId = "PrepClub" Or PanelId = "Games" Or PanelId = "OnlineCoaching" Or PanelId = "General" Then
                        If FirstTimeIndicator = True Then
                            Reg_FirstTime(False)
                            Me.Validate()
                        Else

                            If rbVolunteerInd.SelectedIndex = 1 Then
                                If ddlMaritalStatusInd.SelectedItem.Text = "Divorced" Then
                                    If (Len(txtFirstNameSp.Text) > 0 Or Len(txtLastNameSp.Text) > 0 Or Len(txtPrimaryEmailSp.Text) > 0) And ddlMaritalStatusSp.SelectedItem.Text <> "Divorced" Then
                                        lblErrorStatus.Text = "Marital status must be Divorced on both sides"
                                        Exit Sub
                                    End If
                                End If


                                If ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" Then
                                    lblErrorStatus.Text = "Invalid marital status."
                                    Exit Sub
                                End If
                                If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text <> "Deceased" Then
                                    If Len(txtFirstNameSp.Text) > 0 Or Len(txtLastNameSp.Text) > 0 Or Len(txtPrimaryEmailSp.Text) > 0 Then
                                        lblErrorStatus.Text = "If one of the spouse marital status is widowed, the other must be deceased."
                                        Exit Sub
                                    End If
                                ElseIf (ddlMaritalStatusInd.SelectedItem.Text = "Widowed" Or ddlMaritalStatusInd.SelectedItem.Text = "Never Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Legally Separated") And ddlMaritalStatusSp.SelectedItem.Text <> "Deceased" Then
                                    If Len(txtFirstNameSp.Text) > 0 Or Len(txtLastNameSp.Text) > 0 Or Len(txtPrimaryEmailSp.Text) > 0 Then
                                        lblErrorStatus.Text = "If one of the spouse marital status is widowed, the other must be deceased."
                                        Exit Sub
                                    End If
                                End If
                                If ddlMaritalStatusInd.SelectedItem.Text = "Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Re-Married" Or ((Len(txtFirstNameSp.Text) > 0 Or Len(txtLastNameSp.Text) > 0 Or Len(txtPrimaryEmailSp.Text) > 0) And ddlMaritalStatusInd.SelectedItem.Text = "Divorced") Then
                                    MSpouseDetails(True)
                                    CheckSpouseRecords()
                                Else
                                    CheckIndRecords()
                                    MSpouseDetails(False)
                                    Me.Validate()
                                End If
                            Else
                                CheckIndRecords()
                            End If
                            If EmailDuplication() = False Then
                                Exit Sub
                            End If

                            If EmailValidation() = False Then
                                Me.Validate()
                                Exit Sub
                            End If
                        End If
                    Else
                        reZip.Enabled = False
                        reFirstnamesp.Enabled = False
                        reLastNamesp.Enabled = False
                        reZipsp.Enabled = False
                        rfvMaritalStatusSP.Enabled = False
                        rfvMaritalStatus.Enabled = False
                    End If
                Else
                    If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                        h_indemail.Value = txtPrimaryEmailInd.Text
                        h_spouseemail.Value = txtPrimaryEmailSp.Text
                        ' Added by Bindhu on Dec03,2014
                        If Session("NavPath") = "Search" Then
                            reFirstname.Enabled = False
                            reLastName.Enabled = False
                            reZip.Enabled = False
                            reFirstnamesp.Enabled = False
                            reLastNamesp.Enabled = False
                            reZipsp.Enabled = False
                            rfvMaritalStatusSP.Enabled = False
                            rfvMaritalStatus.Enabled = False
                            rfvEmail.Enabled = False
                            If Len(Trim(txtPrimaryEmailInd.Text)) > 0 Then
                                If ValidateEmailID(txtPrimaryEmailInd.Text) = True Then
                                    If EmailDuplication() = False Then
                                        Exit Sub
                                    End If
                                    If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email='" + txtPrimaryEmailInd.Text + "' and user_email<>'' ") = 0 Then
                                        Dim strPwd As String = GetNewPassword(txtPrimaryEmailInd.Text)
                                        If CreateEmailLM(txtPrimaryEmailInd.Text, strPwd, "IN") = True Then
                                            SendMailProfileCreated(txtPrimaryEmailInd.Text, strPwd)
                                        End If
                                    End If
                                    If txtPrimaryEmailInd.Text <> lblhiddenInd.Text Then
                                        SendMailProfileUpdated(lblhiddenInd.Text, txtPrimaryEmailInd.Text)
                                    End If
                                Else
                                    lblErrorStatus.Text = "Invalid Primary E-mail for the individual."
                                    Exit Sub
                                End If
                            End If
                        ElseIf Session("NavPath") <> "Search" And EmailValidation() = False Then
                            Exit Sub
                        End If
                    End If
                    reFirstname.Enabled = False
                    reLastName.Enabled = False
                    reZip.Enabled = False
                    reFirstnamesp.Enabled = False
                    reLastNamesp.Enabled = False
                    reZipsp.Enabled = False
                    rfvMaritalStatusSP.Enabled = False
                    rfvMaritalStatus.Enabled = False
                End If
                ''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
                If rbVolunteerInd.SelectedIndex = 1 Then
                    If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                        setSpouseValidators(True)
                        If Session("NavPath") <> "Search" Then
                            If Len(txtPrimaryEmailSp.Text) <= 0 Then
                                lblMsg.Visible = True
                                lblMsg.Text = "Primary Email Address Should not be blank."
                                lblMsg.ForeColor = Color.Red
                                Exit Sub
                            Else
                                If SpValidation() = False Then
                                    Exit Sub
                                End If
                            End If
                        End If
                        If Session("NavPath") = "Search" Then
                            If Len(Trim(txtPrimaryEmailSp.Text)) > 0 Then
                                If ValidateEmailID(txtPrimaryEmailSp.Text) = True Then
                                    If EmailDuplication() = False Then
                                        Exit Sub
                                    End If
                                    If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email='" + txtPrimaryEmailSp.Text + "' and user_email<>'' ") = 0 Then
                                        Dim strPwd As String = GetNewPassword(txtPrimaryEmailSp.Text)
                                        If CreateEmailLM(txtPrimaryEmailSp.Text, strPwd, "SP") = True Then
                                            SendMailProfileCreated(txtPrimaryEmailSp.Text, strPwd)
                                        End If
                                    End If
                                    If txtPrimaryEmailSp.Text <> lblhiddensp.Text Then
                                        SendMailProfileUpdated(lblhiddensp.Text, txtPrimaryEmailSp.Text)
                                    End If
                                Else
                                    lblErrorStatus.Text = "Invalid Primary E-mail for the Spouse."
                                    Exit Sub
                                End If
                            End If
                        ElseIf EmailValidation() = False And Session("NavPath") <> "Search" Then
                            Exit Sub
                        End If
                    End If
                End If
                If Page.IsValid Then
                    indId = UpdateIndividualInfo()
                    If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                        If Session("LoginID") Is Nothing Then
                            Session("LoginID") = indId
                        End If
                    End If
                    If lblDuplicateMsg.Visible = False Then
                        If Val(indId) > 0 Then
                            strSql = "UPDATE INDSPOUSE SET PrimaryIndSpouseID=" & spId
                            If Not Session("LoginID") Is Nothing Then
                                strSql = strSql & ",CreatedBy=" & Session("LoginID")
                            Else
                                strSql = strSql & ",CreatedBy=" & indId
                            End If
                            strSql = strSql & " WHERE AutomemberID=" & indId
                            strSql = strSql & " AND DonorType='IND'"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                            ''Added on 06-02-2014 to Send Emails to Parents.
                            ''Parents can receive emails only when newsletter is null or 9.
                            If Session("EntryToken").ToString.ToUpper() = "PARENT" And SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From IndSpouse where AutoMemberID =" & indId & " and (NewsLetter is not null And NewsLetter not in ('9'))") Then
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update IndSpouse set NewsLetter=Null where AutoMemberID =" & indId & " and (NewsLetter is not null And NewsLetter not in ('9'))")
                            End If
                        End If
                        If Trim(txtFirstNameSp.Text).Length > 0 And Trim(txtLastNameSp.Text).Length > 0 Then
                            If Val(indId) > 0 Then
                                spId = UpdateSpouseInfo(indId)
                            End If
                            'set spouseid for individual.
                            If spId > 0 Then
                                strSql = "UPDATE INDSPOUSE SET "
                                If Not Session("LoginID") Is Nothing Then
                                    strSql = strSql & "CreatedBy=" & Session("LoginID")
                                Else
                                    strSql = strSql & "CreatedBy=" & indId
                                End If
                                strSql = strSql & " WHERE AutomemberID=" & spId
                                strSql = strSql & " AND DonorType='SPOUSE'"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                                ''Added on 06-02-2014 to Send Emails to Parents.
                                ''Parents can receive emails only when newsletter is null or 9.
                                If Session("EntryToken").ToString.ToUpper() = "PARENT" And SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From IndSpouse where AutoMemberID =" & spId & " and (NewsLetter is not null And NewsLetter not in ('9'))") Then
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update IndSpouse set NewsLetter=Null where AutoMemberID =" & spId & " and (NewsLetter is not null And NewsLetter not in ('9'))")
                                End If
                            End If
                        Else
                            Dim objIndividual As New IndSpouse10
                            'objIndividual = SessobjIndSpouse
                            'Error Causing Area BY ferdine 28/04/2010
                            If Not Session("GetIndSpouseID").ToString() = 0 Then
                                objIndividual.GetIndSpouseByID(Application("ConnectionString"), Convert.ToInt32(Session("GetIndSpouseID")))
                                objIndividual.PrimaryIndSpouseID = Nothing
                                objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                            End If
                        End If
                    End If
                    'set session vars for following screens.
                    Session("CustIndID") = indId
                    Session("CustSpouseID") = spId
                    'cleanup...
                    'SessobjIndSpouse = Nothing
                    Session("GetIndSpouseID") = 0
                    Session("GetSpouseID") = Nothing
                    If Session("ParentsChapterID") Is Nothing Then
                        Session("ParentsChapterID") = ddlChapterInd.SelectedItem.Value
                    End If
                    If lblDuplicateMsg.Visible = False Then
                        If rbVolunteerInd.SelectedValue = "Yes" Then
                            Session("VolunteerInd") = True
                        Else
                            Session("VolunteerInd") = False
                        End If
                        If rbVolunteerInd.SelectedIndex = 0 Then
                            Session("VolunteerLoginId") = Session("CustIndID")
                        Else
                            Session("VolunteerLoginId") = Nothing
                        End If

                        ' If the navigation is coming through dbsearch side,The user should get the dbsearch page.
                        If Session("NavPath") = "Search" Then
                            If Not Session("CustIndID") Is Nothing Then
                                Session("sSQL") = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse "
                                If Not Session("CustSpouseID") Is Nothing Then
                                    Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & "," & Session("CustSpouseID") & ")"
                                Else
                                    Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & ")"
                                End If
                            End If
                            Session.Remove("GetIndSpouseID")
                            Session.Remove("CustIndID")
                            Session.Remove("GetSpouseID")
                            Session.Remove("CustSpouseID")
                            Response.Redirect("dbSearchResults.aspx")
                        End If
                        If Not Request("Action") Is Nothing Then
                            If Request("Action") = "ChapterFunction" Then
                                Response.Redirect("RegistrationList.aspx")
                            End If
                        Else
                            Select Case Session("EventID")
                                Case 1, 2
                                    If Request("ParentUpdate") = "True" Then
                                        Response.Redirect("UserFunctions.aspx")
                                    End If
                            End Select
                        End If
                        If Session("entryToken").ToString.ToUpper = "DONOR" Then
                            Response.Redirect("DonorFunctions.aspx")
                        ElseIf Session("entryToken").ToString.ToUpper = "WALKATHON_MARATHON" Then
                            Response.Redirect("WalkathonMarathonFunctions.aspx")
                        ElseIf Session("entryToken").ToString.ToUpper = "PARENT" Then
                            If Request.QueryString("Id") Is Nothing Then
                                Response.Redirect("VolunteerSignUpnew.aspx")
                            Else
                                Response.Redirect("VolunteerSignUpnew.aspx?Id=" & Request.QueryString("Id"))
                            End If
                        Else
                            If Session("NavPath") = "Sponsor" Then
                                If Not Session("CustIndID") Is Nothing Then
                                    Session("sSQL") = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse "
                                    If Not Session("CustSpouseID") Is Nothing Then
                                        Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & "," & Session("CustSpouseID") & ")"
                                    Else
                                        Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & ")"
                                    End If
                                End If
                                Session.Remove("GetIndSpouseID")
                                Session.Remove("CustIndID")
                                Session.Remove("GetSpouseID")
                                Session.Remove("CustSpouseID")
                                Response.Redirect("search_sponsor.aspx")
                            ElseIf Session("NavPath") = "Search" Then
                                If Not Session("CustIndID") Is Nothing Then
                                    Session("sSQL") = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse "
                                    If Not Session("CustSpouseID") Is Nothing Then
                                        Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & "," & Session("CustSpouseID") & ")"
                                    Else
                                        Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & ")"
                                    End If
                                End If
                                Session.Remove("GetIndSpouseID")
                                Session.Remove("CustIndID")
                                Session.Remove("GetSpouseID")
                                Session.Remove("CustSpouseID")
                                Response.Redirect("dbSearchResults.aspx")
                            Else
                                btnSubmit.Enabled = False
                                lblDuplicateMsg.Visible = True
                                lblDuplicateMsg.ForeColor = Color.Red
                                lblDuplicateMsg.Text = "Your profile was saved successfully.  Please click on Home to log out."
                            End If
                        End If

                    End If
                Else
                    lblDuplicateMsg.Visible = False
                End If
            Catch ex As Exception
            End Try
        End Sub

        Private Function SearchEmailDupl() As Boolean
            If ValidateEmailID(txtPrimaryEmailInd.Text) = True Then
                Dim cmdText As String = "", Flag As Integer
                If Session("CustIndID") = 0 Then
                    cmdText = "if not exists (select user_email from login_master where user_email ='" & txtPrimaryEmailInd.Text & "' and user_email<>'') Select 0 else Select 1"
                    Flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                    If Flag = 1 Then
                        lblErrorStatus.Text = "Duplicate Exists for individual primary email"
                        Return False
                    End If
                ElseIf Session("CustIndID") <> 0 Then
                    If txtPrimaryEmailInd.Text <> lblhiddenInd.Text Then
                        cmdText = "if not exists (select user_email from login_master where user_email ='" & txtPrimaryEmailInd.Text & "' and user_email<>'') Select 0 else Select 1"
                        Flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                        If Flag = 1 Then
                            lblErrorStatus.Text = "Duplicate Exists for individual primary email"
                            Return False
                        End If
                    End If
                End If
                If Session("GetIndSpouseID") = 0 Then
                    cmdText = "if not exists (select user_email from login_master where user_email ='" & txtPrimaryEmailSp.Text & "' and user_email<>'') Select 0 else Select 1"
                    Flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                    If Flag = 1 Then
                        lblErrorStatus.Text = "Duplicate Exists for spouse primary email"
                        Return False
                    End If
                ElseIf Session("GetIndSpouseID") <> 0 Then
                    If txtPrimaryEmailSp.Text <> lblhiddensp.Text Then
                        cmdText = "if not exists (select user_email from login_master where user_email ='" & txtPrimaryEmailSp.Text & "' and user_email<>'') Select 0 else Select 1"
                        Flag = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                        If Flag = 1 Then
                            lblErrorStatus.Text = "Duplicate Exists for spouse primary email"
                            Return False
                        End If
                    End If
                End If
                If EmailDuplication() = False Then
                    Return False
                End If
            Else
                lblErrorStatus.Text = "Invalid Primary E-mail for the individual."
                Return False
            End If
            Return True
        End Function
        Private Function UpdateIndividualInfo() As Integer
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim objIndividual As New IndSpouse10
            Dim indID As Integer
            If Not Session("GetIndSpouseID") = 0 Then
                objIndividual.GetIndSpouseByID(Application("ConnectionString"), Session("GetIndSpouseID"))
            End If
            Dim bIsNewRecord As Boolean
            lblDuplicateMsg.Visible = False
            bIsNewRecord = False
            With objIndividual
                .Title = IIf(ddlTitleInd.SelectedItem.Value = "", DBNull.Value, ddlTitleInd.SelectedItem.Value)
                .FirstName = StrConv(txtFirstNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtFirstNameInd.Text.Substring(1)), VbStrConv.ProperCase)
                .LastName = StrConv(txtLastNameInd.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtLastNameInd.Text.Substring(1)), VbStrConv.ProperCase)
                .DonorType = "IND"
                If Session("entryToken").ToString.ToUpper = "VOLUNTEER" Then
                    If Request.QueryString("Type") Is Nothing Then
                        If Request.QueryString("Id") Is Nothing Then
                            .MemberID = Session("LoginID")
                            If Session("LoginID") Is Nothing Then
                                bIsNewRecord = True
                            End If
                        ElseIf Not Request.QueryString("Id") Is Nothing Then
                            .MemberID = Request.QueryString("Id")
                        End If
                    Else
                        .MemberID = String.Empty
                        bIsNewRecord = True
                    End If
                Else
                    If Session("LoginID") Is Nothing Then
                        bIsNewRecord = True
                    Else
                        bIsNewRecord = False
                        .MemberID = Session("LoginID")
                    End If
                End If
                'set old attributes.
                If Not Session("GetIndSpouseID") = 0 Then
                    If .Address1 <> Server.HtmlEncode(txtAddress1Ind.Text) Or
                        .Address2 <> Server.HtmlEncode(txtAddress2Ind.Text) Or
                        .City <> Server.HtmlEncode(txtCityInd.Text) Then
                        .Address1_Old = .Address1
                        .Address2_Old = .Address2
                        .City_old = .City
                        .State_Old = .State
                        .ZipCode_Old = .Zip
                        .PrimaryEmail_Old = .Email
                        .HomePhone_Old = .HPhone
                        .CellPhone_Old = .CPhone
                    End If
                Else
                    .CreateDate = Now
                    .MemberSince = Now
                End If

                .Address1 = StrConv(Server.HtmlEncode(txtAddress1Ind.Text), VbStrConv.ProperCase)
                .Address2 = StrConv(Server.HtmlEncode(txtAddress2Ind.Text), VbStrConv.ProperCase)
                .City = StrConv(Server.HtmlEncode(txtCityInd.Text), VbStrConv.ProperCase)
                .State = ddlStateInd.SelectedValue
                .Zip = Server.HtmlEncode(txtZipInd.Text)
                'sg 01/27/2007.Email = Session("LoginEmail")
                If Len(txtPrimaryEmailInd.Text) = 0 Then
                    .Email = ""
                Else
                    .Email = Server.HtmlEncode(txtPrimaryEmailInd.Text)
                End If
                .HPhone = Server.HtmlEncode(txtHomePhoneInd.Text)
                .CPhone = Server.HtmlEncode(txtCellPhoneInd.Text)
                .Country = ddlCountryInd.SelectedValue
                .Gender = ddlGenderInd.SelectedValue
                .WPhone = Server.HtmlEncode(txtWorkPhoneInd.Text)
                .WFax = Server.HtmlEncode(txtWorkFaxInd.Text)
                .SecondaryEmail = Server.HtmlEncode(txtSecondaryEmailInd.Text)
                .Education = Server.HtmlEncode(ddlEducationalInd.SelectedValue)
                .Career = Server.HtmlEncode(ddlCareerInd.SelectedValue)
                .Employer = StrConv(Server.HtmlEncode(txtEmployerInd.Text), VbStrConv.ProperCase)
                .CountryOfOrigin = ddlCountryOfOriginInd.SelectedValue
                If .CountryOfOrigin = "IN" Or .CountryOfOrigin = "US" Then
                    .StateOfOrigin = Me.ddlStateOfOriginInd.SelectedValue
                Else
                    .StateOfOrigin = Server.HtmlEncode(txtStateOfOriginInd.Text)
                End If

                .YouthVol = rbVolunteerInd.SelectedValue
                .ReferredBy = Server.HtmlEncode(ddlReferredByInd.SelectedValue)

                .ChapterID = ddlChapterInd.SelectedValue
                .Chapter = ddlChapterInd.SelectedItem.Text

                .MaritalStatus = ddlMaritalStatusInd.SelectedValue
                .Liaison = rbLiaisonInd.SelectedValue
                .Sponsor = rbSponsorInd.SelectedValue
                If rbVolunteerInd.SelectedValue = "No" Then
                    .VolunteerRole1 = Nothing
                    .VolunteerRole2 = Nothing
                    .VolunteerRole3 = Nothing
                End If
                If .CreateDate = Nothing Then
                    .CreateDate = Now
                End If
                If .MemberSince = Nothing Then
                    .MemberSince = Now
                End If
                Dim sqlDateTime As SqlTypes.SqlDateTime
                sqlDateTime = SqlTypes.SqlDateTime.Null

                If bIsNewRecord = True Then
                    .ModifyDate = sqlDateTime
                    If Session("LoginID") Is Nothing Then
                        .CreatedBy = Nothing
                    Else
                        .CreatedBy = Session("LoginID")
                    End If
                    .ModifiedBy = Nothing
                Else
                    .ModifyDate = Now
                    If Session("LoginID") Is Nothing Then
                        .ModifiedBy = Nothing
                    Else
                        .ModifiedBy = Session("LoginID")
                    End If
                End If
                .PrimaryContact = "No"
                Session("ParentLName") = .LastName
                Session("ParentFName") = .FirstName
            End With

            'If Not objIndividual.MemberID Is Nothing And Not objIndividual.MemberID.Trim() = "" Then
            If Trim(objIndividual.MemberID) <> String.Empty Then
                objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                indID = objIndividual.Id()
                bIsNewRecord = False
            End If

            If bIsNewRecord = True Then
                Dim objInd As New IndSpouseExt
                Dim cnt As Integer = 0
                Dim ChCnt As Integer = 0
                'If objInd.CheckIndSpouseDuplicateEmail(Application("ConnectionString"), objIndividual.Email, objIndividual.LastName, objIndividual.Address1, objIndividual.HPhone) = 0 Then
                Dim emails As String = ""
                Dim strSql As String
                Dim strSqlInd, strSqlSp As String
                Dim conn, Conn1 As New SqlConnection(Application("ConnectionString"))
                Dim drDuplicates As SqlDataReader
                Dim bIsDuplicateFound As Boolean
                Dim Emailflag As Boolean = False
                Dim iDuplicateID As Double
                bIsDuplicateFound = False
                Dim IndDupFlag As Boolean = False
                Dim AutoMemberID, SpouseAutoMemberID, Count As Integer
                emails = emails & " UPPER(EMAIL) IN('" & Trim(txtPrimaryEmailInd.Text.ToUpper()) & "'"
                If Len(Trim(txtPrimaryEmailSp.Text)) > 0 And rbVolunteerInd.SelectedIndex = 1 Then
                    emails = emails & ",'" & Trim(txtPrimaryEmailSp.Text.ToUpper()) & "'"
                End If
                cnt = SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "select Count(*) from IndSpouse where email<>'' and " & emails & ")")
                ChCnt = SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "SELECT COUNT(*) FROM Child WHERE EMail<>'' AND" & emails & ")")
                If cnt > 0 And rbVolunteerInd.SelectedIndex = 1 Then
                    AutoMemberID = SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "select Top 1 Case When DonorType='Spouse' then Relationship Else AutoMemberID End from IndSpouse where Email='" & txtPrimaryEmailSp.Text & "'")
                    bIsDuplicateFound = True
                    Emailflag = True
                ElseIf ChCnt > 0 Then
                    bIsDuplicateFound = True
                    Emailflag = True
                Else
                    strSql = "SELECT AutoMemberID FROM IndSpouse WHERE "
                    strSql = strSql & " UPPER(FirstName) ='" & Trim(txtFirstNameInd.Text.ToUpper()) & "' AND "
                    strSql = strSql & " UPPER(LastName) ='" & Trim(txtLastNameInd.Text.ToUpper()) & "'"
                    'End If
                    strSql = strSql & " AND DonorType='IND'"
                    drDuplicates = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                    Dim bIsDuplicateFoundflag As Integer = 1
                    Dim NotDuplicateFlag As Integer = 1

                    'Ferdine Silva ** To Find the duplicate of Many Ind with Same Name 
                    While drDuplicates.Read()
                        AutoMemberID = Convert.ToInt32(drDuplicates(0))
                        Dim StrDup As String = "SELECT COUNT(DonorType) FROM IndSpouse WHERE DonorType = 'SPOUSE' AND Relationship =" & AutoMemberID
                        Count = Convert.ToString(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrDup))
                        If Count > 0 Then
                            Dim drDuplicates1 As SqlDataReader
                            If Trim(Me.txtLastNameSp.Text).Length > 0 And Trim(Me.txtFirstNameSp.Text).Length > 0 Then
                                Dim strSql1 As String
                                strSql1 = "SELECT AutoMemberID FROM IndSpouse WHERE "
                                strSql1 = strSql1 & " UPPER(FirstName) ='" & Trim(txtFirstNameSp.Text.ToUpper()) & "' AND "
                                strSql1 = strSql1 & " UPPER(LastName) ='" & Trim(txtLastNameSp.Text.ToUpper()) & "'"
                                strSql1 = strSql1 & " AND DonorType='SPOUSE' AND Relationship=" & AutoMemberID
                                drDuplicates1 = SqlHelper.ExecuteReader(Conn1, CommandType.Text, strSql1)
                                If drDuplicates1.Read() Then
                                    SpouseAutoMemberID = Convert.ToInt32(drDuplicates1(0))
                                    bIsDuplicateFoundflag = 0
                                Else
                                    SpouseAutoMemberID = 0
                                    NotDuplicateFlag = 0
                                End If
                                Conn1.Close()
                            End If
                        End If
                        bIsDuplicateFound = True
                    End While
                    'Ferdine Silva * testing closes
                    conn.Close()
                    If bIsDuplicateFoundflag = 1 And NotDuplicateFlag = 0 Then
                        bIsDuplicateFound = False
                    End If
                End If
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    If Session("RoleID") = "1" Or Session("RoleID") = "2" Or Session("RoleID") = "96" Or Session("RoleID") = "37" Or Session("RoleID") = "38" Then
                        bIsDuplicateFound = False
                        lblDuplicateMsg.Visible = False
                        Dim strNational As String
                        strNational = ""
                    Else
                        If AutoMemberID > 0 Then
                            bIsDuplicateFound = True
                            lblDuplicateMsg.Visible = True
                            lblDuplicateMsg.Text = "Duplicate Record Found."
                        End If
                    End If
                End If

                Dim iDuplicateSID As Double = 0

                '** INSERTION OF IND RECORD INDDUPLICATE TABLE
                strSqlInd = "INSERT INTO INDDUPLICATE VALUES("
                strSqlInd = strSqlInd & "'" & Session("EntryToken").ToString.ToUpper & "',"
                '** Added to make the email unique and set status as DuplicateEmail
                'If Emailflag = True Then
                '    strSqlInd = strSqlInd & "'DuplicateEmail',"
                'Else
                strSqlInd = strSqlInd & "'Pending',"
                'End If
                strSqlInd = strSqlInd & AutoMemberID & ","
                strSqlInd = strSqlInd & "'IND',"
                strSqlInd = strSqlInd & "NULL," & ddlChapterInd.SelectedItem.Value & ",'" & ddlTitleInd.SelectedItem.Text & "',"
                strSqlInd = strSqlInd & "'" & txtFirstNameInd.Text & "',"
                strSqlInd = strSqlInd & "'',"
                strSqlInd = strSqlInd & "'" & txtLastNameInd.Text & "',"
                strSqlInd = strSqlInd & "'" & txtAddress1Ind.Text & "',"
                strSqlInd = strSqlInd & "'" & txtAddress2Ind.Text & "',"
                strSqlInd = strSqlInd & "'" & txtCityInd.Text & "',"
                strSqlInd = strSqlInd & "'" & ddlStateInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & txtZipInd.Text & "',"
                strSqlInd = strSqlInd & "'" & ddlCountryInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & ddlGenderInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & txtHomePhoneInd.Text & "',"
                strSqlInd = strSqlInd & "'" & txtCellPhoneInd.Text & "',"
                strSqlInd = strSqlInd & "'',"
                strSqlInd = strSqlInd & "'" & txtWorkPhoneInd.Text & "',"
                strSqlInd = strSqlInd & "'" & txtWorkFaxInd.Text & "',"
                strSqlInd = strSqlInd & "'" & txtPrimaryEmailInd.Text & "',"
                strSqlInd = strSqlInd & "'" & txtSecondaryEmailInd.Text & "',"
                strSqlInd = strSqlInd & "'" & ddlEducationalInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & ddlCareerInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & txtEmployerInd.Text & "',"
                strSqlInd = strSqlInd & "'" & ddlCountryOfOriginInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'',"
                strSqlInd = strSqlInd & "'" & ddlMaritalStatusInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'" & ddlChapterInd.SelectedItem.Text & "',"
                strSqlInd = strSqlInd & "NULL,"
                strSqlInd = strSqlInd & "'" & ddlReferredByInd.SelectedItem.Value & "',"
                strSqlInd = strSqlInd & "'',"
                strSqlInd = strSqlInd & "'" & rbLiaisonInd.SelectedValue & "',"
                strSqlInd = strSqlInd & "'" & rbSponsorInd.SelectedValue & "',"
                strSqlInd = strSqlInd & "NULL,"
                strSqlInd = strSqlInd & "NULL,"
                strSqlInd = strSqlInd & "NULL,"
                strSqlInd = strSqlInd & "'" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "',"
                If Session("LoginID") Is Nothing Then
                    strSqlInd = strSqlInd & "0,"
                Else
                    strSqlInd = strSqlInd & Session("LoginID") & ","
                End If
                strSqlInd = strSqlInd & "'',"
                strSqlInd = strSqlInd & "Null,"
                strSqlInd = strSqlInd & "NULL,NULL,NULL,NULL,"
                If rbVolunteerInd.SelectedIndex = 1 Then
                    strSqlInd = strSqlInd & "NULL"
                Else
                    strSqlInd = strSqlInd & "'" & rbVolunteerInd.SelectedValue & "'"
                End If
                strSqlInd = strSqlInd & ")"
                strSqlInd = strSqlInd & "Select Scope_Identity()"

                If bIsDuplicateFound = False Then
                    lblDuplicateMsg.Visible = False
                    indID = objIndividual.AddIndSpouse(Application("ConnectionString"))
                Else
                    'Inserting into IndDuplicate Table
                    'If Session("EntryToken").ToString.ToUpper() <> "VOLUNTEER" Then
                    If bIsDuplicateFound = True Then
                        lblDuplicateMsg.Visible = True
                        lblDuplicateMsg.Text = "Duplicate Record Found."
                        Session("GetSpouseID") = Nothing
                        If Len(AutoMemberID) > 0 Then
                            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(IndDupID) FROM INDDUPLICATE WHERE Status<>'Completed' AND DonorType='IND' AND AutoMemberID=" & AutoMemberID & " and (DATEADD(dd,1,CreateDate) + 15) > = GETDATE()") > 0 Then
                                iDuplicateID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT  Top 1 IndDupID FROM INDDUPLICATE WHERE Status<>'Completed' AND DonorType='IND' AND AutoMemberID=" & AutoMemberID)
                                IndDupFlag = False
                            ElseIf Len(txtFirstNameInd.Text) > 0 Then
                                '12-03-2013  /**** strSqlInd was moved above to insert IND and SPOUSE records together****/
                                iDuplicateID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSqlInd)
                                IndDupFlag = True
                            End If
                            ''Commented on 12-03-2013  
                            '' SendEmailValidationMail(AutoMemberID, iDuplicateID)
                            ''Commented to avoid the check for existence of Spouse record in INDDuplicate
                            ''If SpouseAutoMemberID > 0 And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(IndDupID) FROM INDDUPLICATE WHERE Status<>'Completed' AND DonorType='Spouse' AND AutoMemberID=" & AutoMemberID & "and (DATEADD(dd,1,CreateDate) +15) > = GETDATE()") > 0 Then
                            ''    iDuplicateSID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Top 1 IndDupID FROM INDDUPLICATE WHERE Status<>'Completed' AND DonorType='Spouse' AND AutoMemberID=" & AutoMemberID)
                            ''Else

                            If Len(txtFirstNameSp.Text) > 0 Then
                                '''' 
                                'Dim spId As Integer
                                'If Val(AutoMemberID) > 0 Then
                                '    spId = UpdateSpouseInfo(AutoMemberID)
                                'End If
                                ''set spouseid for individual.
                                'If spId > 0 Then
                                '    strSql = "UPDATE INDSPOUSE SET "
                                '    If Not Session("LoginID") Is Nothing Then
                                '        strSql = strSql & "CreatedBy=" & Session("LoginID")
                                '    Else
                                '        strSql = strSql & "CreatedBy=" & AutoMemberID
                                '    End If
                                '    strSql = strSql & " WHERE AutomemberID=" & spId
                                '    strSql = strSql & " AND DonorType='SPOUSE'"
                                '    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
                                '    If Session("EntryToken").ToString.ToUpper() = "PARENT" And SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Select COUNT(*) From IndSpouse where AutoMemberID =" & spId & " and (NewsLetter is not null And NewsLetter not in ('9'))") Then
                                '        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Update IndSpouse set NewsLetter=Null where AutoMemberID =" & spId & " and (NewsLetter is not null And NewsLetter not in ('9'))")
                                '    End If
                                'End If
                                Session("GetSpouseID") = Nothing

                                ''12-03-2013  
                                '** INSERTION OF SPOUSE RECORD TO INDDUPLICATE TABLE 
                                strSqlSp = "INSERT INTO INDDUPLICATE VALUES("
                                strSqlSp = strSqlSp & "'" & Session("EntryToken").ToString.ToUpper & "',"

                                '** Added to make the email unique ans set status as DuplicateEmail
                                strSqlSp = strSqlSp & "'Pending',"
                                strSqlSp = strSqlSp & AutoMemberID & ","
                                strSqlSp = strSqlSp & "'SPOUSE',"
                                strSqlSp = strSqlSp & iDuplicateID & "," & ddlChapterInd.SelectedItem.Value & ",'" & IIf(ddlTitleSp.SelectedIndex = 0, "", ddlTitleSp.SelectedItem.Text) & "',"
                                strSqlSp = strSqlSp & "'" & txtFirstNameSp.Text & "',"
                                strSqlSp = strSqlSp & "'',"
                                strSqlSp = strSqlSp & "'" & txtLastNameSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtAddress1Sp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtAddress2Sp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtCitySp.Text & "',"
                                strSqlSp = strSqlSp & "'" & ddlStateSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & txtZipSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & ddlCountrySp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & ddlGenderSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & txtHomePhoneSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtCellPhoneSp.Text & "',"
                                strSqlSp = strSqlSp & "'',"
                                strSqlSp = strSqlSp & "'" & txtWorkPhoneSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtWorkFaxSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtPrimaryEmailSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & txtSecondaryEmailSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & ddlEducationSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & ddlCareerSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & txtEmployerSp.Text & "',"
                                strSqlSp = strSqlSp & "'" & ddlCountryOfOriginSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'',"
                                strSqlSp = strSqlSp & "'" & ddlMaritalStatusSp.SelectedItem.Value & "',"
                                strSqlSp = strSqlSp & "'" & ddlChapterInd.SelectedItem.Text & "',"
                                strSqlSp = strSqlSp & "NULL," '" & rbVolunteerSp.SelectedValue & "
                                strSqlSp = strSqlSp & "NULL,"
                                strSqlSp = strSqlSp & "'',"
                                strSqlSp = strSqlSp & "'" & rbLiaisonSp.SelectedValue & "',"
                                strSqlSp = strSqlSp & "'" & rbSponsorSp.SelectedValue & "',"
                                strSqlSp = strSqlSp & "NULL,"
                                strSqlSp = strSqlSp & "NULL,"
                                strSqlSp = strSqlSp & "NULL,"
                                strSqlSp = strSqlSp & "'" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "',"
                                strSqlSp = strSqlSp & "NULL,"
                                strSqlSp = strSqlSp & "'',"
                                strSqlSp = strSqlSp & "Null,"
                                strSqlSp = strSqlSp & "NULL,NULL,NULL,"
                                strSqlSp = strSqlSp & "" & iDuplicateID & ",NULL);"
                                strSqlSp = strSqlSp & "Select Scope_Identity()"
                                If IndDupFlag = True Then
                                    iDuplicateSID = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSqlSp)
                                End If
                            End If
                            SendEmailValidationMail(AutoMemberID, iDuplicateID)
                        End If
                        If iDuplicateSID > 0 Then
                            Response.Redirect("DuplicateReg.aspx?MemberID=" & iDuplicateID & "&MemberSID=" & iDuplicateSID & "&AMemID=" & AutoMemberID)
                        Else
                            Response.Redirect("DuplicateReg.aspx?MemberID=" & iDuplicateID & "&AMemID=" & AutoMemberID)
                        End If
                    End If
                End If
            End If

            If objIndividual.Gender.Equals("Male", StringComparison.CurrentCultureIgnoreCase) Then
                Session("FatherID") = objIndividual.Id
            Else
                Session("MotherID") = objIndividual.Id
            End If
            If objIndividual.Id.ToString() <> "" Then
                Session("GetIndSpouseID") = objIndividual.Id
            End If
            Return indID
        End Function
        Private Function UpdateSpouseInfo(ByVal p_IndID As Integer) As Integer
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            Dim objSpouse As New IndSpouse10
            Dim spouseID As Integer
            Dim tAddress1 As String = ""
            Dim tAddress2 As String = ""
            Dim tCity As String = ""

            If Not Session("GetSpouseID") Is Nothing Then
                objSpouse.GetIndSpouseByID(Application("ConnectionString"), Session("GetSpouseID"))
            End If
            Dim bIsNewRecord As Boolean
            With objSpouse
                objSpouse.PrimaryIndSpouseID = p_IndID
                .Title = IIf(ddlTitleSp.SelectedItem.Value = "", DBNull.Value, ddlTitleSp.SelectedItem.Value)
                .FirstName = StrConv(txtFirstNameSp.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtFirstNameSp.Text.Substring(1)), VbStrConv.ProperCase)
                .LastName = StrConv(txtLastNameSp.Text.Substring(0, 1).ToUpper + Server.HtmlEncode(txtLastNameSp.Text.Substring(1)), VbStrConv.ProperCase)
                .DonorType = "SPOUSE"
                .Relationship = p_IndID

                If Me.ckbPrimaryContact.Checked Then
                    .PrimaryContact = "Yes"
                Else
                    .PrimaryContact = "No"
                End If
                'set old attributes.
                If Not Session("GetSpouseID") Is Nothing Then
                    If .Address1 <> tAddress1.ToString Or
                        .Address2 <> tAddress2.ToString Or
                        .City <> tCity Then
                        .Address1_Old = .Address1
                        .Address2_Old = .Address2
                        .City_old = .City
                        .State_Old = .State
                        .ZipCode_Old = .Zip
                        .PrimaryEmail_Old = .Email
                        .HomePhone_Old = .HPhone
                        .CellPhone_Old = .CPhone
                    End If
                Else
                    .CreateDate = Now
                    .MemberSince = Now
                    bIsNewRecord = True
                End If
                .Address1 = StrConv(Server.HtmlEncode(txtAddress1Sp.Text), VbStrConv.ProperCase)
                .Address2 = StrConv(Server.HtmlEncode(txtAddress2Sp.Text), VbStrConv.ProperCase)
                .City = StrConv(Server.HtmlEncode(txtCitySp.Text), VbStrConv.ProperCase)
                .State = ddlStateSp.SelectedValue
                .Zip = Server.HtmlEncode(txtZipSp.Text)
                .Country = ddlCountrySp.SelectedValue
                .Gender = ddlGenderSp.SelectedValue
                .HPhone = Server.HtmlEncode(txtHomePhoneSp.Text)
                .CPhone = Server.HtmlEncode(txtCellPhoneSp.Text)
                .WPhone = Server.HtmlEncode(txtWorkPhoneSp.Text)
                .WFax = Server.HtmlEncode(txtWorkFaxSp.Text)
                .Email = Server.HtmlEncode(txtPrimaryEmailSp.Text)
                .SecondaryEmail = Server.HtmlEncode(txtSecondaryEmailSp.Text)
                .Education = Server.HtmlEncode(ddlEducationSp.SelectedValue)
                .Career = Server.HtmlEncode(ddlCareerSp.SelectedValue)
                .Employer = StrConv(Server.HtmlEncode(txtEmployerSp.Text), VbStrConv.ProperCase)
                .CountryOfOrigin = ddlCountryOfOriginSp.SelectedValue
                If .CountryOfOrigin = "IN" Or .CountryOfOrigin = "US" Then
                    .StateOfOrigin = Me.ddlStateOfOriginSp.SelectedValue
                Else
                    .StateOfOrigin = Server.HtmlEncode(txtStateOfOriginSp.Text)
                End If

                .MaritalStatus = ddlMaritalStatusSp.SelectedValue
                .Liaison = rbLiaisonSp.SelectedValue
                .Sponsor = rbSponsorSp.SelectedValue
                .ChapterID = ddlChapterInd.SelectedValue
                .Chapter = ddlChapterInd.SelectedItem.Text
                .CreatedBy = Session("LoginID")
                Dim sqlDateTime As SqlTypes.SqlDateTime
                sqlDateTime = SqlTypes.SqlDateTime.Null
                .ModifyDate = SqlTypes.SqlDateTime.Null
                .ModifiedBy = Nothing
                If .CreateDate = Nothing Then
                    .CreateDate = Now
                End If

                If .MemberSince = Nothing Then
                    .MemberSince = Now
                End If
                If bIsNewRecord = False Then
                    .ModifyDate = Now
                    .ModifiedBy = Session("LoginID")
                End If
            End With

            If Not Session("GetSpouseID") Is Nothing Then
                objSpouse.UpdateIndSpouse(Application("ConnectionString"))
                spouseID = objSpouse.Id()
            Else
                Dim objInd As New IndSpouseExt
                spouseID = objSpouse.AddIndSpouse(Application("ConnectionString"))
            End If

            If objSpouse.Gender.Equals("Male", StringComparison.CurrentCultureIgnoreCase) Then
                Session("FatherID") = objSpouse.Id
            Else
                Session("MotherID") = objSpouse.Id
            End If
            Return spouseID
        End Function

        Private Sub txtFirstNameSp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtFirstNameSp.TextChanged
            If Trim(Me.txtFirstNameSp.Text).Length > 0 Then
                setSpouseValidators(True)
            Else
                setSpouseValidators(False)
            End If

            If Session("NavPath") = "Search" Then
                Me.rfvCountryOfOriginSp.Enabled = False
                Me.rfvEmail.Enabled = False
                Me.revPrimaryEmailInd.Enabled = False
                Me.revPrimaryEMailSp.Enabled = False
                Me.rfvPrimaryEmailSp.Enabled = False
            End If
        End Sub
        Private Sub setSpMandatoryIndicator(bFlag As Boolean)
            spTitle.Visible = bFlag
            spFN.Visible = bFlag
            spLN.Visible = bFlag
            spAddr.Visible = bFlag
            spCity.Visible = bFlag
            spState.Visible = bFlag

            spZip.Visible = bFlag
            spCntry.Visible = bFlag
            spGender.Visible = bFlag

            spHPhone.Visible = bFlag
            spMarital.Visible = bFlag
            spEmail.Visible = bFlag
            spCountryOrgin.Visible = bFlag
        End Sub
        Private Sub setSpouseValidators(ByVal p_Flag As Boolean)
            setSpMandatoryIndicator(p_Flag)
            Me.rfvTitleSp.Enabled = p_Flag
            Me.rfvFirstNameSp.Enabled = p_Flag
            Me.rfvLastNameSp.Enabled = p_Flag
            setSpouseAddressValidators(p_Flag)
            '  If rbVolunteerSp.SelectedValue = "Yes" Or ((Session("entryToken").ToString = "Parent") Or Session("entryToken").ToString = "Volunteer") Then
            Me.rfvPrimaryEmailSp.Enabled = p_Flag
            'Me.rfvCountryOfOriginSp.Enabled = p_Flag
            '/*********Updated on 13-03-2014 to disable Country validation when navigated through db search page***********/
            If Session("NavPath") = "Search" Then
                Me.rfvCountryOfOriginSp.Enabled = False
                Me.rfvEmail.Enabled = False
                Me.revPrimaryEmailInd.Enabled = False
                Me.revPrimaryEMailSp.Enabled = False
                Me.rfvPrimaryEmailSp.Enabled = False
            Else
                Me.rfvCountryOfOriginSp.Enabled = p_Flag
            End If
            rfvZipSp.Enabled = p_Flag
            rfvGenderSp.Enabled = p_Flag
            '  rfvVolSp.Enabled = p_Flag
        End Sub

        Private Sub setSpouseAddressValidators(ByVal p_flag As Boolean)
            Me.rfvAddress1Sp.Enabled = p_flag
            Me.rfvCitySp.Enabled = p_flag
            Me.rfvStateSp.Enabled = p_flag
            Me.rfvCountrySp.Enabled = p_flag
            ' Me.rfvHomePhoneSp.Enabled = p_flag
            Me.rfvZipSp.Enabled = p_flag
        End Sub

        Private Sub ckbAddressSp_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ckbAddressSp.CheckedChanged
            If Me.ckbAddressSp.Checked Then
                setSpouseAddressValidators(False)
            Else
                setSpouseAddressValidators(True)
            End If
        End Sub

        Private Sub txtLastNameSp_TextChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles txtLastNameSp.TextChanged
            If Trim(Me.txtLastNameSp.Text).Length > 0 Then
                setSpouseValidators(True)
            Else
                setSpouseValidators(False)
            End If
        End Sub

        Private Sub ddlCountryOfOriginInd_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountryOfOriginInd.SelectedIndexChanged
            If loadStates(Me.ddlStateOfOriginInd, Me.ddlCountryOfOriginInd.SelectedValue) Then
                txtStateOfOriginInd.Visible = False
            Else
                Me.ddlStateOfOriginInd.Visible = False
                Me.txtStateOfOriginInd.Visible = True
            End If
        End Sub
        Private Function loadStates(ByRef ddlControl As DropDownList, ByVal p_country As String) As Boolean
            Dim rtnValue As Boolean
            If p_country = "IN" Then
                ddlControl.Visible = True
                '*** Populate State DropDown
                Dim dsIndiaStates As DataSet
                dsIndiaStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetIndiaStates")
                If dsIndiaStates.Tables.Count > 0 Then
                    ddlControl.Items.Clear()
                    ddlControl.DataSource = dsIndiaStates.Tables(0)
                    ddlControl.DataTextField = dsIndiaStates.Tables(0).Columns("StateName").ToString
                    ddlControl.DataValueField = dsIndiaStates.Tables(0).Columns("StateCode").ToString
                    ddlControl.DataBind()
                    ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
                End If
                rtnValue = True
            ElseIf p_country = "US" Then
                ddlControl.Visible = True
                '*** Populate State DropDown
                Dim dsStates As DataSet

                dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetStates")
                If dsStates.Tables.Count > 0 Then
                    ddlControl.Items.Clear()
                    ddlControl.DataSource = dsStates.Tables(0)
                    ddlControl.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                    ddlControl.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                    ddlControl.DataBind()
                    ddlControl.Items.Insert(0, New ListItem("Select State", String.Empty))
                End If
                rtnValue = True
            End If
            Return rtnValue
        End Function

        Private Sub ddlCountryOfOriginSp_SelectedIndexChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles ddlCountryOfOriginSp.SelectedIndexChanged
            If loadStates(Me.ddlStateOfOriginSp, Me.ddlCountryOfOriginSp.SelectedValue) Then
                txtStateOfOriginSp.Visible = False
            Else
                Me.ddlStateOfOriginSp.Visible = False
                Me.txtStateOfOriginSp.Visible = True
            End If
        End Sub
        Private Function checkContestantRegistered(ByVal indId As Integer, ByVal spouseId As Integer, ByVal contestYear As Integer) As Boolean
            Dim prmArray(2) As SqlParameter
            Dim rtnValue As Boolean
            Dim count As Integer

            prmArray(0) = New SqlParameter("@IndId", DbType.UInt32)
            prmArray(0).Value = indId

            prmArray(1) = New SqlParameter("@SpouseId", DbType.UInt32)
            prmArray(1).Value = spouseId

            prmArray(2) = New SqlParameter("@ContestYear", DbType.UInt32)
            prmArray(2).Value = contestYear

            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.StoredProcedure, "usp_CheckContestantRegistered", prmArray)

            If count > 0 Then
                rtnValue = True
            End If

            Return rtnValue
        End Function

        Protected Sub ckbPrimaryContact_CheckedChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            If txtPrimaryEmailSp.Text.Length < 3 Then
                lblMsg.Text = "Primary contact needs valid email."
            End If
        End Sub
        Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@northsouth.org")
            email.To.Add(sMailTo)
            email.Subject = sSubject
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            Dim client As New SmtpClient()
            Dim ok As Boolean = True
            Try
                ' client.Send(email)
            Catch e As Exception
                ok = False
            End Try
        End Sub

        Private Sub SendEmailValidationMail(ByVal AutoMemberID As Integer, ByVal iDuplicateID As Integer)
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@northsouth.org"
            sMailTo = "nsfcontests@northsouth.org"
            sSubject = "Invalid Email Record with Duplicate Registration"
            Dim mm As New MailMessage(SMailFrom, sMailTo)
            mm.Subject = sSubject
            Dim strSql As String
            strSql = "SELECT * FROM IndSpouse WHERE ValidEmailFlag='N' AND AutoMemberID=" & AutoMemberID
            Dim drIndDup As SqlDataReader
            drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If (drIndDup.Read()) Then
                sBody = "Please contact the following parent for duplicate registration with another email. Kindly do a follow up thourgh phone and correct his/her Record to avoid confusion. " & vbCrLf
                sBody = sBody & " Original Record Details in IndSpouse table " & vbCrLf
                sBody = sBody & "First Name: " & drIndDup("FirstName") & vbCrLf
                sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
                sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
                sBody = sBody & "City: " & drIndDup("City") & vbCrLf
                sBody = sBody & "State: " & drIndDup("State") & vbCrLf
                sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
                sBody = sBody & "Email: " & drIndDup("Email") & vbCrLf
                sBody = sBody & "New Email: " & txtPrimaryEmailInd.Text & vbCrLf
                If Len(txtPrimaryEmailSp.Text) > 0 Then sBody = sBody & "New Email: " & txtPrimaryEmailSp.Text & vbCrLf
                sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
                sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
                sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
                sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
            Else
                Exit Sub
            End If

            strSql = "SELECT * FROM IndDuplicate WHERE IndDupID=" & iDuplicateID
            drIndDup = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            If (drIndDup.Read()) Then
                sBody = sBody & " Duplicate, New Record Details in IndDuplicate table " & vbCrLf
                sBody = sBody & "First Name: " & drIndDup("FirstName") & vbCrLf
                sBody = sBody & "Last Name: " & drIndDup("LastName") & vbCrLf
                sBody = sBody & "Address1: " & drIndDup("Address1") & vbCrLf
                sBody = sBody & "City: " & drIndDup("City") & vbCrLf
                sBody = sBody & "State: " & drIndDup("State") & vbCrLf
                sBody = sBody & "Zip: " & drIndDup("Zip") & vbCrLf
                sBody = sBody & "Email: " & drIndDup("Email") & vbCrLf
                sBody = sBody & "Gender: " & drIndDup("Gender") & vbCrLf
                sBody = sBody & "Home Phone: " & drIndDup("HPhone") & vbCrLf
                sBody = sBody & "Cell Phone: " & drIndDup("CPhone") & vbCrLf
                sBody = sBody & "Chapter: " & drIndDup("Chapter") & vbCrLf
            Else
                Exit Sub
            End If
            mm.Body = sBody
            Dim client As New SmtpClient()
            Try
                ' client.Send(mm)
            Catch ex As Exception
            End Try
        End Sub
        Private Sub lockchapter(ByVal MemId As Integer)

            '1) If the existing chapter is inactive (Status = 'I' in the chapter SQL table), then enable the Select Chapter field to allow the parent to fix the chapter. We cannot have inactive chapter in IndSpouse record.  This is the opportunity to fix.
            Dim iCount As Integer
            If Not Session("ChapterID") Is Nothing Then
                iCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from chapter c where chapterid=" & Session("ChapterID") & " and status='I'")
                If iCount > 0 Then
                    ddlChapterInd.Enabled = True
                    Exit Sub
                End If
            End If

            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim EvntYear As Integer = 0
            Dim CurrentYear As Integer = Now.Year()

            'If current date is between Sept 15 and Dec 31 (both dates are included), current contest year (xx) = Current year + 1
            'If current date is between Jan 1 and September 14 (both dates are included), current contest year (xx) = Current year
            Dim frmDate As String = "01/01/" & CurrentYear
            Dim toDate As String = "09/14/" & CurrentYear
            Dim NowDate As String = Date.Now.ToShortDateString()

            'If Convert.ToDateTime(NowDate).ToString("MM/dd/yyyy") >= Convert.ToDateTime(frmDate) And Convert.ToDateTime(NowDate).ToString("MM/dd/yyyy") <= Convert.ToDateTime(toDate) Then
            '    EvntYear = CurrentYear
            'Else
            '    EvntYear = CurrentYear + 1
            'End If

            'If Convert.ToDateTime(Now).ToString("MM/dd/yyyy") >= Convert.ToDateTime(frmDate) And Convert.ToDateTime(Now).ToString("MM/dd/yyyy") <= Convert.ToDateTime(toDate) Then
            '    EvntYear = CurrentYear
            'Else
            '    EvntYear = CurrentYear + 1
            'End If

            'Select count(*) from contestant where contestyear=xx and parentID = nnnnn 
            'If the count not zero, then disable the select chapter field.
            iCount = SqlHelper.ExecuteScalar(conn, CommandType.Text, "select count(*) from contestant where contestyear=" & EvntYear & " and ParentId=" & MemId)
            If iCount > 0 Then
                ddlChapterInd.Enabled = False
            End If

            'Dim chkDate As String
            'Dim conn As New SqlConnection(Application("ConnectionString"))
            'Dim EvntYear As Integer = 0
            'Dim CurrentYear As Integer = Now.Year()
            'EvntYear = SqlHelper.ExecuteScalar(conn, CommandType.Text, "SELECT ContestYear FROM Contestant where contestant_id = (select MAX(contestant_id) FROM  Contestant where ParentID=" & MemId & ")")
            'If EvntYear < CInt(CurrentYear) Then
            'Else
            '    chkDate = "09/14/" & EvntYear.ToString()
            '    If EvntYear = CInt(CurrentYear) And (Convert.ToDateTime(Now).ToString("MM/dd/yyyy") > Convert.ToDateTime(chkDate)) Then

            '    Else
            '        ddlChapterInd.Enabled = False
            '    End If
            'End If

        End Sub

        Protected Sub hlnkPrevPage_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkPrevPage.Click
            If Session("NavPath") = "Sponsor" Then
                '*******************************************
                '*** Code Modified By FERDINE Feb 01 2011***
                '*******************************************
                If Session("AddUpdateFlag") = "True" Then
                    If Not Session("CustIndID") Is Nothing Then
                        Session("sSQL") = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse "
                        If Not Session("CustSpouseID") Is Nothing Then
                            Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & "," & Session("CustSpouseID") & ")"
                        Else
                            Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & ")"
                        End If
                    End If
                End If
                Session.Remove("GetIndSpouseID")
                Session.Remove("CustIndID")
                Session.Remove("GetSpouseID")
                Session.Remove("CustSpouseID")
                Response.Redirect("search_sponsor.aspx")
            ElseIf Session("NavPath") = "Search" Then
                '*******************************************
                '*** Code Modified By FERDINE Feb 01 2011***
                '*******************************************
                'only If add new or update is done the query must be changed in session 
                If Session("AddUpdateFlag") = "True" Then
                    If Not Session("CustIndID") Is Nothing Then
                        Session("sSQL") = "Select MemberID,AutoMemberID,CASE WHEN DonorType = 'IND' THEN AutoMemberID Else Relationship END as CustINDID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse "
                        If Not Session("CustSpouseID") Is Nothing Then
                            Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & "," & Session("CustSpouseID") & ")"
                        Else
                            Session("sSQL") = Session("sSQL") & "WHERE AutoMemberid in (" & Session("CustIndID") & ")"
                        End If
                    End If
                End If
                Session.Remove("GetIndSpouseID")
                Session.Remove("CustIndID")
                Session.Remove("GetSpouseID")
                Session.Remove("CustSpouseID")
                Response.Redirect("dbsearchresults.aspx")
            End If
        End Sub

        Protected Function NameDuplication() As Boolean
            lblDuplicateMsg.ForeColor = Color.Red
            lblDuplicateMsg.Visible = True
            lblDuplicateMsg.Text = ""
            Dim ErrorExists As Boolean = False
            Dim outMessage As String = ""
            'Check duplication
            'Individual is Exists
            Dim iDuplInd As Integer = 0, iDuplSp As Integer = 0
            If btnSubmit.Text.Trim = "Submit" Then
                If Len(txtFirstNameInd.Text) > 0 And Len(txtLastNameInd.Text) > 0 Then
                    ' If loggedin user is not a youth volunteer
                    If rbVolunteerInd.SelectedIndex = 1 Then
                        If Len(txtFirstNameSp.Text) > 0 And Len(txtLastNameSp.Text) > 0 Then
                            iDuplInd = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from IndSpouse I1, IndSpouse I2 where I2.Relationship=I1.AutomemberID and ( (I1.FirstName+I1.LastName='" & txtFirstNameInd.Text & "'+'" & txtLastNameInd.Text & "' and I2.FirstName+I2.Lastname='" & txtFirstNameSp.Text.Trim & "'+'" & txtLastNameSp.Text.Trim & "') OR (I1.FirstName+I1.LastName='" & txtFirstNameSp.Text & "'+'" & txtLastNameSp.Text & "' and I2.FirstName+I2.Lastname='" & txtFirstNameInd.Text.Trim & "'+'" & txtLastNameInd.Text.Trim & "') )")
                            outMessage = "together"
                        Else
                            iDuplInd = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select case when ((select count(*) from  indspouse I1 where I1.firstname='" & txtFirstNameInd.Text & "' and I1.lastname='" & txtLastNameInd.Text & "') =( select count(*) from IndSpouse where relationship in ( select automemberid from  indspouse I1 where I1.firstname='" & txtFirstNameInd.Text & "' and I1.lastname='" & txtLastNameInd.Text & "' ))) then 0 else 1 end ")
                            outMessage = "for individual"
                        End If
                    End If
                End If
            ElseIf btnSubmit.Text.Trim = "Continue" Then
                If Len(txtFirstNameInd.Text) > 0 And Len(txtLastNameInd.Text) > 0 Then
                    ' If loggedin user is not a youth volunteer
                    If rbVolunteerInd.SelectedIndex = 1 Then
                        If Len(txtFirstNameSp.Text) > 0 And Len(txtLastNameSp.Text) > 0 Then
                            iDuplInd = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from IndSpouse I1, IndSpouse I2 where I2.Relationship=I1.AutomemberID AND I1.AutoMemberId<>" & Session("CustIndID") & " and ((I1.FirstName+I1.LastName='" & txtFirstNameInd.Text.Trim & "'+'" & txtLastNameInd.Text.Trim & "' and I2.FirstName+I2.Lastname='" & txtFirstNameSp.Text.Trim & "'+'" & txtLastNameSp.Text.Trim & "') OR (I1.FirstName+I1.LastName='" & txtFirstNameSp.Text.Trim & "'+'" & txtLastNameSp.Text.Trim & "' and I2.FirstName+I2.Lastname='" & txtFirstNameInd.Text.Trim & "'+'" & txtLastNameInd.Text.Trim & "') )")
                            outMessage = "together"
                        Else
                            iDuplInd = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select case when ((select count(*) from  indspouse where automemberid <>" & Session("CustIndID") & " and firstname='" & txtFirstNameInd.Text & "' and lastname='" & txtLastNameInd.Text & "') =( select count(*) from IndSpouse where relationship in ( select automemberid from indspouse where automemberid <>" & Session("CustIndID") & "  and Firstname='" & txtFirstNameInd.Text & "' and lastname='" & txtLastNameInd.Text & "' ))) then 0 else 1 end ")
                            outMessage = "for individual"
                        End If
                    End If
                End If
            End If
            If iDuplInd > 0 Then
                ErrorExists = True
                outMessage = "Duplicate name exists " & outMessage
            Else
                outMessage = ""
            End If
lbl:
            lblDuplicateMsg.Text = outMessage.ToString
            Return ErrorExists
        End Function

        Protected Function EmailDuplication() As Boolean
            lblDuplicateMsg.ForeColor = Color.Red
            lblDuplicateMsg.Visible = True
            lblDuplicateMsg.Text = ""

            Dim cnt As Integer = 0
            Dim ChCnt As Integer = 0
            Dim emails As String = ""
            Dim conn, Conn1 As New SqlConnection(Application("ConnectionString"))
            Dim bIsDuplicateFound As Boolean
            Dim Emailflag As Boolean = False

            bIsDuplicateFound = False
            Dim IndDupFlag As Boolean = False
            emails = emails & " UPPER(EMAIL) IN('" & Trim(txtPrimaryEmailInd.Text.ToUpper()) & "'"
            If rbVolunteerInd.SelectedIndex = 1 Then
                If Len(Trim(txtPrimaryEmailSp.Text)) > 0 Then
                    emails = emails & ",'" & Trim(txtPrimaryEmailSp.Text.ToUpper()) & "'"
                End If
            End If
            Dim OriginalEmailInd As String = ""
            Dim OriginalEmailSP As String = ""
            Dim OriginalEmailChildInd As String = ""
            Dim OriginalEmailChildSp As String = ""
            Dim flgExists As Boolean = False
            Dim outMessage As String = ""

            If txtPrimaryEmailInd.Text <> lblhiddenInd.Text Then
                OriginalEmailInd = CStr(SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "select email from IndSpouse where email<>'' and email='" & txtPrimaryEmailInd.Text & "'"))
                OriginalEmailChildInd = CStr(SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "Select Email from Child where Email<>'' and Email='" & txtPrimaryEmailInd.Text & "'"))
                If OriginalEmailInd = txtPrimaryEmailInd.Text Then
                    flgExists = True
                    outMessage = "Father's Email ID already exists in Parent database."
                    lblDuplicateMsg.Text = outMessage.ToString
                    GoTo lbl
                    'Exit Function
                ElseIf OriginalEmailChildInd = txtPrimaryEmailInd.Text Then
                    flgExists = True
                    outMessage = "Father's - Email ID already exists in Child database."
                    lblDuplicateMsg.Text = outMessage.ToString
                    GoTo lbl
                    'Exit Function
                End If
            End If

            If rbVolunteerInd.SelectedIndex = 1 Then
                If txtPrimaryEmailSp.Text <> lblhiddensp.Text Then
                    OriginalEmailSP = CStr(SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "select email from IndSpouse where email <>'' and SecondaryEmail='" & txtPrimaryEmailSp.Text & "'"))
                    OriginalEmailChildSp = CStr(SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "Select Email from Child where Email<>'' and Email='" & txtPrimaryEmailSp.Text & "'"))
                    OriginalEmailInd = CStr(SqlHelper.ExecuteScalar(Conn1, CommandType.Text, "Select Email from IndSpouse where Email<>'' and Email='" & txtPrimaryEmailSp.Text & "'"))

                    If OriginalEmailSP = txtPrimaryEmailSp.Text Or OriginalEmailInd = txtPrimaryEmailSp.Text Then
                        flgExists = True
                        outMessage = "Spouse - Email ID already exists in Parent database."
                        lblDuplicateMsg.Text = outMessage.ToString
                        GoTo lbl
                        'Exit Function
                    ElseIf OriginalEmailChildSp = txtPrimaryEmailSp.Text Then
                        outMessage = "Spouse - Email ID already exists in Child database."
                        flgExists = True
                        lblDuplicateMsg.Text = outMessage.ToString
                        GoTo lbl
                        ' Exit Function
                    End If
                End If
                'Changed By Bindhu on Mar 23,2015
                If txtPrimaryEmailSp.Text.Trim <> "" And txtPrimaryEmailInd.Text.Equals(txtPrimaryEmailSp.Text) Then
                    flgExists = True
                    outMessage = "Spouse Email ID already exists in Parent database."
                    lblDuplicateMsg.Text = outMessage.ToString
                    GoTo lbl
                End If
            End If

lbl:
            If flgExists = True Then
                Return False
            Else
                Return True
            End If
        End Function
        Protected Sub hlnkMainMenu_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlnkMainMenu.Click

            If Session("entryToken").ToString.ToUpper() = "DONOR" Then
                Response.Redirect("DonorFunctions.aspx")
            ElseIf Session("entryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Session.Remove("GetIndSpouseID")
                Session.Remove("CustIndID")
                Session.Remove("GetSpouseID")
                Session.Remove("CustSpouseID")
                Response.Redirect("VolunteerFunctions.aspx")
            ElseIf Session("entryToken").ToString.ToUpper() = "PARENT" Then
                Response.Redirect("UserFunctions.aspx")
            ElseIf Session("entryToken").ToString.ToUpper() = "WALKATHON_MARATHON" Then
                Response.Redirect("WalkathonMarathonFunctions.aspx")
            Else
                Response.Redirect("MainTest.aspx")
            End If
        End Sub

        Protected Sub ZipCodeValidateion()
            If ddlStateInd.SelectedValue.ToString() = "VT" Or ddlStateInd.SelectedValue.ToString() = "ME" Or ddlStateInd.SelectedValue.ToString() = "NH" Or ddlStateInd.SelectedValue.ToString() = "MA" Or ddlStateInd.SelectedValue.ToString() = "CT" Or ddlStateInd.SelectedValue.ToString() = "NJ" Or ddlStateInd.SelectedValue.ToString() = "RI" Then
                RegularExpressionValidator1.Enabled = False
                If Val(txtZipInd.Text) = 0 Then
                    reZip.Enabled = True
                    reZip.IsValid = False
                Else
                    reZip.Enabled = True
                    reZip.Validate()
                End If
            Else
                reZip.Enabled = False
                If Val(txtZipInd.Text) = 0 Then
                    RegularExpressionValidator1.Enabled = True
                    RegularExpressionValidator1.IsValid = False
                Else
                    RegularExpressionValidator1.Enabled = True
                    RegularExpressionValidator1.Validate()
                End If
            End If

            If rbVolunteerInd.SelectedIndex = 1 Then
                If ddlStateSp.SelectedValue.ToString() = "VT" Or ddlStateSp.SelectedValue.ToString() = "ME" Or ddlStateSp.SelectedValue.ToString() = "NH" Or ddlStateSp.SelectedValue.ToString() = "MA" Or ddlStateSp.SelectedValue.ToString() = "CT" Or ddlStateSp.SelectedValue.ToString() = "NJ" Or ddlStateSp.SelectedValue.ToString() = "RI" Then
                    RegularExpressionValidator2.Enabled = False
                    If Val(txtZipSp.Text) = 0 Then
                        reZipsp.Enabled = True
                        reZipsp.IsValid = False
                    Else
                        reZipsp.Enabled = True
                        reZipsp.Validate()
                    End If
                Else
                    reZipsp.Enabled = False
                    If Val(txtZipSp.Text) = 0 Then
                        RegularExpressionValidator2.Enabled = True
                        RegularExpressionValidator2.IsValid = False
                    Else
                        RegularExpressionValidator2.Enabled = True
                        RegularExpressionValidator2.Validate()
                    End If
                End If
            End If
        End Sub
        Protected Function CreateEmailLM(ByVal email As String, ByVal pwd As String, ByVal INDSP As String) As Boolean
            Dim stInd As Boolean
            Dim sqlstr As String = "INSERT INTO login_master (user_email,user_pwd,date_added,user_role,active,view_tasks,chapterId,chapter) VALUES('" & email & "','" & pwd & "',GETDATE(),'Parent','Yes','Yes'," & ddlChapterInd.SelectedValue.ToString & ",'" & ddlChapterInd.SelectedItem.Text & "')"
            Try
                SqlHelper.ExecuteNonQuery(Application("Connectionstring").ToString, CommandType.Text, sqlstr)
                stInd = True
            Catch ex As Exception
                stInd = False
                lblErrorStatus.Text = "Error Creating record into Login_master.Please provide the complete details."
            End Try
            Return stInd
        End Function
        Protected Function EmailValidation(stype As String) As Boolean
            Dim vStatus As Boolean = True
            h_indemail.Value = txtPrimaryEmailInd.Text
            h_spouseemail.Value = txtPrimaryEmailSp.Text
            Dim conn As String = Application("Connectionstring").ToString
            Dim sqlstr As String = ""
            If stype = "IND" Then
                sqlstr = "if not exists (select user_email from login_master where user_email ='" & h_indemail.Value & "' and user_email<>'') Select 0 else Select 1"
                If CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlstr)) = 0 Then
                    If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" Then
                    ElseIf ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusSp.SelectedItem.Text = "Widowed" Then
                    Else
                        ' Commented by Bindhu on Nov20-2014    
                        txtPrimaryEmailInd_Popup.Text = txtPrimaryEmailInd.Text
                        If Len(txtPrimaryEmailInd.Text) = 0 Then
                            txtPrimaryEmailInd_Popup.Enabled = True
                            lblErrMsgInd_Popup.ForeColor = Color.Red
                            lblErrMsgInd_Popup.Text = "Primary E-mail is missing. Please enter E-mail address and password to create a login for the Ind."
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailInd('show');", True)
                        Else
                            txtPrimaryEmailInd_Popup.Enabled = False
                            lblErrMsgInd_Popup.ForeColor = Color.Red
                            lblErrMsgInd_Popup.Text = "Password missing for primary E-mail(Ind). Please provide password for this email to establish login."
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailInd('show');", True)
                        End If
                        GeneratePwd(txtPrimaryEmailInd_Popup.Text, txtIndPassword, txtIndPassword1)
                        vStatus = False
                    End If
                End If
            ElseIf stype = "SP" Then
                sqlstr = "if not exists (select user_email from login_master where user_email ='" & h_spouseemail.Value & "' and user_email<>'') Select 0 else Select 1"
                If ddlMaritalStatusInd.SelectedItem.Text = "Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Re-Married" Then
                    If txtFirstNameSp.Text.Trim() = "" And ddlTitleSp.SelectedItem.Text.Trim() = "" And txtLastNameSp.Text.Trim() = "" Then
                        lblErrorStatus.ForeColor = Color.Red
                        lblErrorStatus.Text = "Spouse information is missing.Please enter spouse information"
                        vStatus = False
                        Return vStatus
                    End If
                End If

                '  If ddlMaritalStatusInd.SelectedItem.Text = "Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Re-Married" Then
                If txtFirstNameSp.Text.Trim() <> "" And ddlTitleSp.SelectedItem.Text.Trim() <> "" And txtLastNameSp.Text.Trim() <> "" Then
                    If CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlstr)) = 0 Then
                        If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" Then
                        ElseIf ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusSp.SelectedItem.Text = "Widowed" Then
                        Else
                            ' Commented by Bindhu on Nov20-2014   
                            txtPrimaryEmailSp_Popup.Text = txtPrimaryEmailSp.Text
                            If Len(txtPrimaryEmailSp.Text) = 0 Then
                                txtPrimaryEmailSp_Popup.Enabled = True
                                lblErrMsgSp_Popup.ForeColor = Color.Red
                                lblErrMsgSp_Popup.Text = "Primary E-mail is missing. Please enter E-mail address and password to create a login for the spouse."
                                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailSp('show');", True)
                            Else
                                txtPrimaryEmailSp_Popup.Enabled = False
                                lblErrMsgSp_Popup.ForeColor = Color.Red
                                lblErrMsgSp_Popup.Text = "Password missing for primary E-mail(Spouse). Please provide password for this email to establish login."
                                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailSp('show');", True)
                            End If
                            GeneratePwd(txtPrimaryEmailSp_Popup.Text, txtSpPassword, txtSpPassword1)
                            vStatus = False
                        End If
                    End If
                End If
                'Else
                '    lblErrorStatus.ForeColor = Color.Red
                '    lblErrorStatus.Text = "Please Select Marital Status to create login for the spouse"
                '    vStatus = False
                'End If
            End If
            Return vStatus
        End Function

        Protected Function EmailValidation() As Boolean
            Dim vStatus As Boolean = True
            h_indemail.Value = txtPrimaryEmailInd.Text
            h_spouseemail.Value = txtPrimaryEmailSp.Text

            Dim conn As String = Application("Connectionstring").ToString
            Dim sqlstr As String = "if not exists (select user_email from login_master where user_email ='" & h_indemail.Value & "' and user_email<>'') Select 0 else Select 1"
            Dim sqlstr1 As String = "if not exists (select user_email from login_master where user_email ='" & h_spouseemail.Value & "' and user_email<>'') Select 0 else Select 1"

            If (h_indemail.Value = h_spouseemail.Value) Or (h_indemail.Value.ToString.ToLower = h_spouseemail.Value.ToString.ToLower) Then

                If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" Then
                ElseIf ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusInd.SelectedItem.Text = "Widowed" Then
                Else
                    lblErrorStatus.Text = "The email address in the right side (Spouse) should be different from the left side."
                    txtPrimaryEmailSp.Enabled = True
                    txtPrimaryEmailInd.Enabled = True
                    vStatus = False
                End If
                'End If
            Else
                If CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlstr)) = 0 Then
                    If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" And rbVolunteerInd.SelectedIndex = 1 Then
                    ElseIf ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And rbVolunteerInd.SelectedIndex = 1 Then
                    Else

                        txtPrimaryEmailInd_Popup.Text = txtPrimaryEmailInd.Text
                        If Len(txtPrimaryEmailInd.Text) = 0 Then
                            txtPrimaryEmailInd_Popup.Enabled = True
                            lblErrMsgInd_Popup.ForeColor = Color.Red
                            lblErrMsgInd_Popup.Text = "Primary E-mail is missing. Please enter E-mail address and password to create a login for the Ind."
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailInd('show');", True)
                        Else
                            txtPrimaryEmailInd_Popup.Enabled = False
                            lblErrMsgInd_Popup.ForeColor = Color.Red
                            lblErrMsgInd_Popup.Text = "Password missing for primary E-mail(Ind). Please provide password for this email to establish login."
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailInd('show');", True)
                        End If
                        GeneratePwd(txtPrimaryEmailInd_Popup.Text, txtIndPassword, txtIndPassword1)
                        vStatus = False
                    End If
                End If
                If ddlMaritalStatusInd.SelectedItem.Text = "Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Re-Married" Then
                    If txtFirstNameSp.Text.Trim() = "" And ddlTitleSp.SelectedItem.Text.Trim() = "" And txtLastNameSp.Text.Trim() = "" Then
                        lblErrorStatus.ForeColor = Color.Red
                        lblErrorStatus.Text = "Spouse information is missing.Please enter spouse information"
                        vStatus = False
                        Return vStatus
                    End If
                End If
                If txtFirstNameSp.Text.Trim() <> "" And ddlTitleSp.SelectedItem.Text.Trim() <> "" And txtLastNameSp.Text.Trim() <> "" Then
                    If CInt(SqlHelper.ExecuteScalar(conn, CommandType.Text, sqlstr1)) = 0 Then
                        If ddlMaritalStatusSp.SelectedItem.Text = "Widowed" And ddlMaritalStatusInd.SelectedItem.Text = "Deceased" Then
                        ElseIf ddlMaritalStatusSp.SelectedItem.Text = "Deceased" And ddlMaritalStatusSp.SelectedItem.Text = "Widowed" Then
                        Else
                            txtPrimaryEmailSp_Popup.Text = txtPrimaryEmailSp.Text
                            If Len(txtPrimaryEmailSp.Text) = 0 Then
                                txtPrimaryEmailSp_Popup.Enabled = True
                                lblErrMsgSp_Popup.ForeColor = Color.Red
                                lblErrMsgSp_Popup.Text = "Primary E-mail is missing. Please enter E-mail address and password to create a login for the spouse."
                                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailSp('show');", True)
                            Else
                                txtPrimaryEmailSp_Popup.Enabled = False
                                lblErrMsgSp_Popup.ForeColor = Color.Red
                                lblErrMsgSp_Popup.Text = "Password missing for primary E-mail(Spouse). Please provide password for this email to establish login."
                                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailSp('show');", True)
                            End If
                            ' Generate Password
                            GeneratePwd(txtPrimaryEmailSp.Text, txtSpPassword, txtSpPassword1)
                            vStatus = False
                        End If
                    End If
                End If
                '  End If
            End If
            '  End If
            Return vStatus
        End Function

        Private Function RandomNumber() As Integer
            Dim min As Integer, max As Integer
            min = 100
            max = 999
            Dim rnd As New Random
            Return rnd.Next(min, max)
        End Function

        Private Function GetNewPassword(strUName As String) As String
            Dim iRnd As Integer
            While True
                iRnd = RandomNumber()
                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email='" + strUName + "' and  user_pwd='" + "welcome" + iRnd.ToString() + "' and user_email<>'' ") = 0 Then
                    Exit While
                End If
            End While
            Return "Welcome" + iRnd.ToString()
        End Function

        Private Sub GeneratePwd(strUName As String, ByRef txtPwd As TextBox, ByRef txtConfPwd As TextBox)
            Dim strPwd As String
            strPwd = GetNewPassword(strUName)
            txtPwd.Attributes.Add("Value", strPwd)
            txtConfPwd.Attributes.Add("Value", strPwd)
        End Sub

        Protected Sub txtFirstNameInd_TextChanged(sender As Object, e As EventArgs) Handles txtFirstNameInd.TextChanged
        End Sub
        Protected Sub txtZipInd_TextChanged(sender As Object, e As EventArgs) Handles txtZipInd.TextChanged
        End Sub
        Protected Sub btnCreateSp_Click(sender As Object, e As EventArgs) Handles btnCreateSp.Click
            Dim IsPswdWinVisible As Boolean = False
            lblErrMsgSp_Popup.ForeColor = Color.Red
            lblErrMsgSp_Popup.Text = ""
            If ValidateEmailID(txtPrimaryEmailSp_Popup.Text) = True Then
                If ValidateEmailID(txtConfPrimaryEmailSp_Popup.Text) = True Then
                    If txtPrimaryEmailSp_Popup.Text <> txtConfPrimaryEmailSp_Popup.Text Then
                        lblErrMsgSp_Popup.Text = "Confirm email address does not match."
                        IsPswdWinVisible = True
                        GoTo lblShow
                    End If
                    ' Primary email address should be different than ind
                    If Len(txtPrimaryEmailInd.Text) > 0 Then
                        If txtPrimaryEmailSp_Popup.Text = txtPrimaryEmailInd.Text Then
                            lblErrMsgSp_Popup.Text = "The primary email address should be different from the email for the ind!"
                            IsPswdWinVisible = True
                            GoTo lblShow
                        End If
                    End If
                    If Len(txtSpPassword.Text) >= 3 And txtSpPassword.Text = txtSpPassword1.Text Then
                        If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email= '" + txtPrimaryEmailSp_Popup.Text + "' and user_pwd='" + txtSpPassword.Text + "'") > 0 Then
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:MsgToUpdateDupSp();", True)
                        Else
                            If CreateEmailLM(txtPrimaryEmailSp_Popup.Text, txtSpPassword.Text, "SP") = True Then
                                lnkbtnChangeSp.Enabled = True
                                IsPswdWinVisible = False
                                ' Update Spouse email                               
                                UpdateEmailAddress("", txtPrimaryEmailSp_Popup.Text, txtSpPassword.Text, "SP")
                                ' Assign Value
                                AssignMailValue(txtPrimaryEmailSp_Popup.Text, "SP", True)
                                GoTo lblShow
                            End If
                        End If
                    Else
                        If (Len(txtSpPassword.Text) < 3) Then
                            lblErrMsgSp_Popup.Text = "Minimum password length is three characters."
                            IsPswdWinVisible = True
                            GoTo lblShow
                        End If
                        If txtSpPassword.Text <> txtSpPassword1.Text Then
                            lblErrMsgSp_Popup.Text = "Confirm password does not match."
                            IsPswdWinVisible = True
                            GoTo lblShow
                        End If
                    End If
                Else
                    lblErrMsgSp_Popup.Text = "Invalid Confirmation Primary E-mail on the right side."
                    IsPswdWinVisible = True
                    GoTo lblShow
                End If
            Else
                lblErrMsgSp_Popup.Text = "Invalid Primary E-mail for the spouse."
                IsPswdWinVisible = True
                GoTo lblShow
            End If
lblShow:
            If IsPswdWinVisible = True Then
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailSp('show');", True)
            End If
        End Sub
        Protected Sub MSpouseDetails(ByVal state As Boolean)
            rfvTitleSp.Enabled = state
            rfvFirstNameSp.Enabled = state
            reFirstnamesp.Enabled = state
            rfvLastNameSp.Enabled = state
            reLastNamesp.Enabled = state
            rfvAddress1Sp.Enabled = state
            rfvCitySp.Enabled = state
            rfvStateSp.Enabled = state
            rfvZipSp.Enabled = state
            reZipsp.Enabled = state
            RegularExpressionValidator2.Enabled = state
            rfvCountrySp.Enabled = state
            rfvGenderSp.Enabled = state
            revHomePhoneSp.Enabled = state
            revCellPhoneSp.Enabled = state
            revWorkPhoneSp.Enabled = state
            revWorkFaxSp.Enabled = state
            revPrimaryEMailSp.Enabled = state
            rfvPrimaryEmailSp.Enabled = state
            revSecondaryEMailSp.Enabled = state
            rfvCountryOfOriginSp.Enabled = state

            rfvMaritalStatusSP.Enabled = state
            ZipCodeValidateion()
        End Sub
        Protected Sub Reg_FirstTime(ByVal state As Boolean)
            rfvTitleInd.Enabled = True
            rfvFirstName.Enabled = True
            reFirstname.Enabled = True
            rfvLastName.Enabled = True
            reLastName.Enabled = True
            If ddlMaritalStatusInd.SelectedItem.Text = "Married" Or ddlMaritalStatusInd.SelectedItem.Text = "Re-Married" Then
                rfvTitleSp.Enabled = True
                rfvFirstNameSp.Enabled = True
                reFirstnamesp.Enabled = True
                rfvLastNameSp.Enabled = True
                reLastNamesp.Enabled = True
            End If
            rfvAddress1.Enabled = state
            rfvAddress1Sp.Enabled = state
            rfvCity.Enabled = state
            rfvCitySp.Enabled = state
            rfvStateInd.Enabled = state
            rfvStateSp.Enabled = state
            rfvZip.Enabled = state
            rfvZipSp.Enabled = state
            RegularExpressionValidator1.Enabled = state
            RegularExpressionValidator2.Enabled = state
            reZip.Enabled = state
            reZipsp.Enabled = state
            rfvCountry.Enabled = state
            rfvCountrySp.Enabled = state
            rfvGenderInd.Enabled = state
            rfvGenderSp.Enabled = state
            revHomePhoneInd.Enabled = state
            revHomePhoneSp.Enabled = state
            rfvHomePhoneInd.Enabled = state
            revCellPhoneInd.Enabled = state
            revCellPhoneSp.Enabled = state
            revWorkPhoneInd.Enabled = state
            revWorkPhoneSp.Enabled = state
            revWorkFaxInd.Enabled = state
            revWorkFaxSp.Enabled = state
            revPrimaryEmailInd.Enabled = state
            revPrimaryEMailSp.Enabled = state
            rfvEmail.Enabled = state
            rfvPrimaryEmailSp.Enabled = state
            revSecondaryEMail.Enabled = state
            revSecondaryEMailSp.Enabled = state
            rfvCountryInd.Enabled = state
            rfvCountryOfOriginSp.Enabled = state

            rfvChapterInd.Enabled = state
            rfvMaritalStatusSP.Enabled = state
            rfvMaritalStatus.Enabled = state
        End Sub
        Protected Sub btncreatInd_Click(sender As Object, e As EventArgs) Handles btncreatInd.Click
            Dim IsPswdWinVisible As Boolean = False
            lblErrMsgInd_Popup.ForeColor = Color.Red
            Me.Validate()
            If ValidateEmailID(txtPrimaryEmailInd_Popup.Text) = True Then
                If ValidateEmailID(txtConfPrimaryEmailInd_Popup.Text) = True Then
                    If txtPrimaryEmailInd_Popup.Text <> txtConfPrimaryEmailInd_Popup.Text Then
                        lblErrMsgInd_Popup.Text = "Confirm email address does not match."
                        IsPswdWinVisible = True
                        GoTo lblInd
                    End If
                    If Len(txtPrimaryEmailSp.Text) > 0 Then
                        If txtPrimaryEmailInd_Popup.Text = txtPrimaryEmailSp.Text Then
                            lblErrMsgInd_Popup.Text = "The primary email address should be different from the email for the spouse!"
                            IsPswdWinVisible = True
                            GoTo lblInd
                        End If
                    End If
                    If Len(txtIndPassword.Text) >= 3 And txtIndPassword.Text = txtIndPassword1.Text Then
                        If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email= '" + txtPrimaryEmailInd_Popup.Text + "' and user_pwd='" + txtIndPassword.Text + "'") > 0 Then
                            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:MsgToUpdateDupInd();", True)
                        Else
                            If CreateEmailLM(txtPrimaryEmailInd_Popup.Text, txtIndPassword.Text, "IN") = True Then
                                'update indspouse
                                UpdateEmailAddress("", txtPrimaryEmailInd_Popup.Text, txtIndPassword.Text, "IND")
                                AssignMailValue(txtPrimaryEmailInd_Popup.Text, "IND", True)
                                lnkbtnChangeInd.Enabled = True
                                IsPswdWinVisible = False
                                GoTo lblInd
                            End If
                        End If
                    Else
                        If (Len(txtIndPassword.Text) < 3) Then
                            lblErrMsgInd_Popup.Text = "Minimum password length is three characters."
                            IsPswdWinVisible = True
                            GoTo lblInd
                        End If
                        If txtIndPassword.Text <> txtIndPassword1.Text Then
                            lblErrMsgInd_Popup.Text = "Confirm password does not match."
                            IsPswdWinVisible = True
                            GoTo lblInd
                        End If
                    End If
                Else
                    lblErrMsgInd_Popup.Text = "Invalid Confirmation Primary E-mail on the left side."
                    IsPswdWinVisible = True
                    GoTo lblInd
                End If
            Else
                lblErrMsgInd_Popup.Text = "Invalid Primary E-mail on the left side."
                IsPswdWinVisible = True
                GoTo lblInd
            End If
lblInd:
            If IsPswdWinVisible = True Then
                ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpEmailInd('show');", True)
            End If
        End Sub
        Private Function ValidateEmailID(ByVal mailstring As String) As Boolean
            ''Added for testing - 20-09-2013 
            Dim Valid As Boolean
            Try
                Valid = Regex.IsMatch(mailstring, "\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase)
            Catch ex As Exception
            End Try
            If Not Valid Then
                Return False
            Else
                Return True
            End If
        End Function
        Private Sub CheckIndRecords()
            If ddlTitleInd.SelectedItem.Text = "Select Title" Or Len(Trim(txtPrimaryEmailInd.Text)) < 1 Or Len(Trim(txtFirstNameInd.Text)) < 1 Or Len(Trim(txtLastNameInd.Text)) < 1 Or Len(Trim(txtCityInd.Text)) < 1 Or Trim(ddlStateInd.SelectedValue.ToString) = "" Or Trim(ddlCountryInd.SelectedValue.ToString) = "" Or Trim(ddlGenderInd.SelectedValue.ToString) = "" Or Trim(ddlCountryOfOriginInd.SelectedValue.ToString) = "" Or Trim(ddlMaritalStatusInd.SelectedValue.ToString) = "" Or Trim(rbVolunteerInd.SelectedValue.ToString) = "" Then
                lblErrorStatus.Text = "Please fill all the required fields."
                Me.Validate()
            End If
        End Sub
        Private Sub CheckSpouseRecords()
            If ddlTitleSp.SelectedItem.Text = "Select Title" And Len(Trim(txtFirstNameSp.Text)) < 1 And Len(Trim(txtPrimaryEmailSp.Text)) < 1 And Len(Trim(txtLastNameSp.Text)) < 1 And Len(Trim(txtCitySp.Text)) < 1 And Trim(ddlStateSp.SelectedValue.ToString) = "" And Trim(ddlCountrySp.SelectedValue.ToString) = "" And Trim(ddlGenderSp.SelectedValue.ToString) = "" And Trim(ddlCountryOfOriginSp.SelectedValue.ToString) = "" And Trim(ddlMaritalStatusSp.SelectedValue.ToString) = "" Then
                lblErrorStatus.Text = "The spouse profile is missing on the right side. Please enter the profile."
                setSpMandatoryIndicator(True)
                Me.Validate()
            ElseIf ddlTitleSp.SelectedItem.Text = "Select Title" Or Len(Trim(txtPrimaryEmailSp.Text)) < 1 Or Len(Trim(txtFirstNameSp.Text)) < 1 Or Len(Trim(txtLastNameSp.Text)) < 1 Or Len(Trim(txtCitySp.Text)) < 1 Or Trim(ddlStateSp.SelectedValue.ToString) = "" Or Trim(ddlCountrySp.SelectedValue.ToString) = "" Or Trim(ddlGenderSp.SelectedValue.ToString) = "" Or Trim(ddlCountryOfOriginSp.SelectedValue.ToString) = "" Or Trim(ddlMaritalStatusSp.SelectedValue.ToString) = "" Then
                lblErrorStatus.Text = "The spouse profile is not complete. Please complete it."
                setSpMandatoryIndicator(True)
                Me.Validate()

            End If
        End Sub
        Protected Sub btnChngEmailInd_Click(sender As Object, e As EventArgs)
            ViewState("CntPswdTrialInd") = Nothing
            ViewState("CurEmailInd") = Nothing
            txtChngeEmailInd.Text = txtPrimaryEmailInd.Text
            txtNewEmailInd.Text = ""
            txtConfNewEmailInd.Text = ""
            lblErrChngeEmailInd_Popup.Text = ""
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpChngEmailInd('show');", True)
        End Sub
        Protected Sub btnChngEmailSp_Click(sender As Object, e As EventArgs)
            ViewState("CntPswdTrialSp") = Nothing
            ViewState("CurEmailSp") = Nothing
            txtChngeEmailSp.Text = txtPrimaryEmailSp.Text
            txtNewEmailSp.Text = ""
            txtConfNewEmailSp.Text = ""
            lblErrChngeEmailSp_Popup.Text = ""
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpChngEmailSp('show');", True)
        End Sub

        Protected Sub btnUpdateEmailInd_Click(sender As Object, e As EventArgs) Handles btnUpdateEmailInd.Click
            Try
                txtChngeEmailInd.Text = txtPrimaryEmailInd.Text
                lblErrChngeEmailInd_Popup.Text = ""
                lblErrorStatus.Text = ""
                ' To clear viewstate handle hiddenfield
                If hfPopUpInd.Value = "Ind" Then
                    ViewState("CntPswdTrialInd") = Nothing
                    ViewState("CurEmailInd") = Nothing
                    hfPopUpInd.Value = ""
                End If
                Dim IsWinVisible As Boolean = False
                lblErrChngeEmailInd_Popup.ForeColor = Color.Red
                ' validate primary email id
                If ValidateEmailID(txtChngeEmailInd.Text) = False Then
                    lblErrChngeEmailInd_Popup.Text = "Invalid Primary E-mail address."
                    IsWinVisible = True
                    GoTo lblShow
                End If
                If Session("NavPath") <> "Search" Then
                    If Len(Trim(txtChngePswdInd.Text)) = 0 Then
                        lblErrChngeEmailInd_Popup.Text = "Please enter password for the current email."
                        txtChngeEmailInd.Focus()
                        IsWinVisible = True
                        GoTo lblShow
                    ElseIf (Len(txtChngePswdInd.Text) < 3) Then
                        lblErrChngeEmailInd_Popup.Text = "Minimum password length is three characters."
                        IsWinVisible = True
                    End If
                End If

                If Len(txtNewEmailInd.Text) = 0 Then
                    lblErrChngeEmailInd_Popup.Text = "Please enter new email address"
                    IsWinVisible = True
                    GoTo lblShow
                End If
                If Len(txtConfNewEmailInd.Text) = 0 Then
                    lblErrChngeEmailInd_Popup.Text = "Confirm new email address is required"
                    IsWinVisible = True
                    GoTo lblShow
                End If
                Dim cmdText As String, iCntIndSpouse, iCntLogin As Integer
                ' Validate new email address
                If ValidateEmailID(txtNewEmailInd.Text) = True Then
                    ' Validate confirm new email address
                    If ValidateEmailID(txtConfNewEmailInd.Text) = True Then
                        If txtNewEmailInd.Text <> txtConfNewEmailInd.Text Then
                            lblErrChngeEmailInd_Popup.Text = "Confirm New Email address does not match."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                        If txtChngeEmailInd.Text.Equals(txtConfNewEmailInd.Text) Then
                            lblErrChngeEmailInd_Popup.Text = "Current mail and New Email address are same. Please try with another."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                        If Session("NavPath") = "Search" Then
                            If ValidateEmailID(txtNewEmailInd.Text) = True Then
                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from IndSpouse where email<>'' and email='" & txtNewEmailInd.Text & "'") > 0 Then
                                    lblErrChngeEmailInd_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                    IsWinVisible = True
                                    GoTo lblShow
                                ElseIf SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "Select Email from Child where Email<>'' and Email='" & txtNewEmailInd.Text & "'") > 0 Then
                                    lblErrChngeEmailInd_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                    IsWinVisible = True
                                    GoTo lblShow
                                End If
                                If (txtChngePswdInd.Text <> "") Then
                                    '  cmdText = "delete from login_master where user_email='" + txtChngeEmailInd.Text + "'"
                                    '  SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                                End If

                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email='" + txtNewEmailInd.Text + "' and user_email<>'' ") = 0 Then
                                    'hidden by Sims on 19/04/2018
                                    'If (txtChngePswdInd.Text <> "") Then
                                    ' Dim strPwd As String = GetNewPassword(txtNewEmailInd.Text)
                                    'Dim strPwd As String = txtChngePswdInd.Text
                                    'If CreateEmailLM(txtNewEmailInd.Text, strPwd, "IN") = True Then
                                    '    SendMailProfileCreated(txtNewEmailInd.Text, strPwd)
                                    '    lblErrorStatus.Visible = True
                                    '    lblErrorStatus.ForeColor = Color.Green
                                    '    lblErrorStatus.Text = "Email address was changed."
                                    'End If
                                    'Else
                                    If (txtChngePswdInd.Text <> "") Then
                                        cmdText = "select count(*) from login_master where user_email='" + txtChngeEmailInd.Text + "' and user_pwd='" + txtChngePswdInd.Text + "'"
                                        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0) Then
                                            Dim cmdUpdateEmail As String = String.Empty
                                            cmdUpdateEmail = "update Login_master set user_Email='" & txtNewEmailInd.Text & "' where user_email='" & txtChngeEmailInd.Text & "'"
                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdUpdateEmail)
                                        Else
                                            lblErrChngeEmailInd_Popup.Text = "Please enter correct password for the current email."
                                            IsWinVisible = True
                                            GoTo lblShow
                                        End If
                                    Else

                                        Dim cmdUpdateEmail As String = String.Empty
                                        cmdUpdateEmail = "update Login_master set user_Email='" & txtNewEmailInd.Text & "' where user_email='" & txtChngeEmailInd.Text & "'"
                                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdUpdateEmail)
                                    End If
                                    ' End If

                                End If
                                ' update currentemail
                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from indspouse where email='" + txtChngeEmailInd.Text + "' and email<>'' ") > 0 Then
                                    cmdText = "update indspouse set email='" + txtNewEmailInd.Text + "' where email='" + txtChngeEmailInd.Text + "' "
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                                    lblErrorStatus.Visible = True
                                    lblErrorStatus.ForeColor = Color.Green
                                    lblErrorStatus.Text = "Email address was changed."
                                End If
                            Else
                                lblErrChngeEmailInd_Popup.Text = "Invalid Primary E-mail for the individual."
                                IsWinVisible = True
                                GoTo lblShow
                            End If
                            ' SendMailProfileUpdated(txtChngeEmailInd.Text, txtNewEmailInd.Text)
                            txtPrimaryEmailInd.Text = txtConfNewEmailInd.Text
                            Exit Sub
                        End If
                        ' New email should not exist in IndSpouse 
                        ' New email should not exist in Login_master 
                        cmdText = "select count(*) from login_master where user_email='" + txtChngeEmailInd.Text + "' and user_pwd='" + txtChngePswdInd.Text + "'"
                        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0) Then

                            cmdText = "select count(*) from login_master where user_email='" + txtConfNewEmailInd.Text + "'"
                            iCntLogin = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText))
                            cmdText = "select count(*) from indspouse where email='" + txtConfNewEmailInd.Text + "'"
                            iCntIndSpouse = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText))
                            If iCntIndSpouse > 0 And iCntLogin > 0 Then
                                ' error msg duplicate
                                lblErrChngeEmailInd_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                IsWinVisible = True
                                GoTo lblShow
                            ElseIf iCntIndSpouse = 0 And iCntLogin > 0 Then
                                If Len(txtPrimaryEmailSp.Text) > 0 Then
                                    If txtConfNewEmailInd.Text = txtPrimaryEmailSp.Text Then
                                        lblErrChngeEmailInd_Popup.Text = "The new email address should be different from the email for the spouse!"
                                        IsWinVisible = True
                                        GoTo lblShow
                                    End If
                                End If
                                ' ask pswd
                                cmdText = "select count(*) from login_master where user_email='" + txtConfNewEmailInd.Text + "' and user_pwd='" + Trim(txtConfNewPwdInd.Text) + "'"
                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0 Then
                                    ' delete currentemail
                                    cmdText = "delete from login_master where user_email='" + txtChngeEmailInd.Text + "' and user_pwd='" + Trim(txtChngePswdInd.Text) + "' "
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)

                                    ' Update Ind email
                                    UpdateEmailAddress(txtChngeEmailInd.Text, txtConfNewEmailInd.Text, "", "IND")
                                    ' Assign Value
                                    AssignMailValue(txtConfNewEmailInd.Text, "IND", False)

                                    If Session("LoginEmail") = txtChngeEmailInd.Text Then
                                        Session("LoginEmail") = txtConfNewEmailInd.Text
                                    End If
                                    IsWinVisible = False
                                    ViewState("CntPswdTrialInd") = Nothing
                                Else

                                    IsWinVisible = True
                                    trConfPwdInd.Visible = True
                                    ' ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:var tr=document.getElementById('trConfPwdInd');tr.style.visibility='visible';PopUpChngEmailInd('show');", True)
                                    lblErrChngeEmailInd_Popup.Text = "Password exists for the new email address.  Please enter."

                                    If ViewState("CurEmailInd") = Nothing Then
                                        ViewState("CurEmailInd") = txtNewEmailInd.Text
                                    End If
                                    If ViewState("CurEmailInd") <> txtNewEmailInd.Text Then
                                        ViewState("CurEmailInd") = Nothing
                                    End If

                                    ' Error: invalid pswd -ask for 3 trial
                                    If ViewState("CntPswdTrialInd") = Nothing Then
                                        ViewState("CntPswdTrialInd") = "1"
                                    Else
                                        Dim i As Integer
                                        i = Convert.ToInt32(ViewState("CntPswdTrialInd"))
                                        i = i + 1
                                        ViewState("CntPswdTrialInd") = i.ToString()
                                        If i > 3 Then
                                            spIndAck.Visible = True
                                            tblInd.Visible = False
                                            IsWinVisible = True
                                            ViewState("CntPswdTrialInd") = 0
                                        End If
                                    End If
                                End If
                            Else
                                If Len(txtPrimaryEmailSp.Text) > 0 Then
                                    If txtConfNewEmailInd.Text = txtPrimaryEmailSp.Text Then
                                        lblErrChngeEmailInd_Popup.Text = "The new email address should be different from the email of the spouse!"
                                        IsWinVisible = True
                                        GoTo lblShow
                                    End If
                                End If
                                ' update login
                                cmdText = "update login_master set user_email='" + txtConfNewEmailInd.Text + "' where user_email='" + txtChngeEmailInd.Text + "'"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)

                                ''update indspouse
                                UpdateEmailAddress(txtChngeEmailInd.Text, txtConfNewEmailInd.Text, "", "IND")
                                ' Assign Value
                                AssignMailValue(txtConfNewEmailInd.Text, "IND", False)
                                If Session("LoginEmail") = txtChngeEmailInd.Text Then
                                    Session("LoginEmail") = txtConfNewEmailInd.Text
                                End If
                                IsWinVisible = False
                                GoTo lblShow
                            End If
                        Else
                            lblErrChngeEmailInd_Popup.Text = "Please enter correct password for the current email."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                    Else
                        lblErrChngeEmailInd_Popup.Text = "Invalid Confirm New E-mail address"
                        IsWinVisible = True
                        GoTo lblShow
                    End If
                Else
                    lblErrChngeEmailInd_Popup.Text = "Invalid New E-mail address."
                    IsWinVisible = True
                    GoTo lblShow
                End If
lblShow:
                If IsWinVisible = True Then
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpChngEmailInd('show');", True)
                End If
            Catch ex As Exception
            End Try
        End Sub

        Sub AssignMailValue(strNewEmail As String, sType As String, isNew As Boolean)
            If sType = "IND" Then
                txtPrimaryEmailInd.Text = strNewEmail 'txtNewEmailInd.Text
                lblhiddenInd.Text = strNewEmail ' txtNewEmailInd.Text
                'Session("LoginEmail") = strNewEmail ' txtNewEmailInd.Text
                lblErrorStatus.ForeColor = Color.Green
                If isNew = True Then
                    lblErrorStatus.Text = "Login Record Created for ind E-mail."
                Else
                    lblErrorStatus.Text = "Email address was changed."
                End If
            ElseIf sType = "SP" Then
                txtPrimaryEmailSp.Text = strNewEmail ' txtNewEmailSp.Text
                lblhiddensp.Text = strNewEmail 'txtNewEmailSp.Text
                lblErrorStatus.ForeColor = Color.Green
                If isNew = True Then
                    lblErrorStatus.Text = "Login Record Created for spouse."
                Else
                    lblErrorStatus.Text = "Email address was changed."
                End If
            End If
        End Sub

        Protected Sub btnUpdateEmailSp_Click(sender As Object, e As EventArgs) Handles btnUpdateEmailSp.Click
            Try
                txtChngeEmailSp.Text = txtPrimaryEmailSp.Text
                lblErrChngeEmailSp_Popup.Text = ""
                lblErrorStatus.Text = ""
                Dim IsWinVisible As Boolean = False
                lblErrChngeEmailSp_Popup.ForeColor = Color.Red
                ' To clear viewstate handle hiddenfield
                If hfPopUpSp.Value = "Sp" Then
                    ViewState("CntPswdTrialSp") = Nothing
                    ViewState("CurEmailSp") = Nothing
                    hfPopUpSp.Value = ""
                End If
                ' validate primary email id
                If ValidateEmailID(txtChngeEmailSp.Text) = False Then
                    lblErrChngeEmailSp_Popup.Text = "Invalid Primary E-mail address."
                    IsWinVisible = True
                    GoTo lblShow
                End If
                If Session("NavPath") <> "Search" Then
                    If Len(Trim(txtChngePswdSp.Text)) = 0 Then
                        lblErrChngeEmailSp_Popup.Text = "Please enter password for the current email."
                        txtChngeEmailSp.Focus()
                        IsWinVisible = True
                        GoTo lblShow
                    ElseIf (Len(txtChngePswdSp.Text) < 3) Then
                        lblErrChngeEmailSp_Popup.Text = "Minimum password length is three characters."
                        IsWinVisible = True
                    End If
                End If
                If Len(txtNewEmailSp.Text) = 0 Then
                    lblErrChngeEmailSp_Popup.Text = "Please enter new email address"
                    IsWinVisible = True
                    GoTo lblShow
                End If

                If Len(txtConfNewEmailSp.Text) = 0 Then
                    lblErrChngeEmailSp_Popup.Text = "Confirm new email address is required"
                    IsWinVisible = True
                    GoTo lblShow
                End If
                Dim cmdText As String, iCntIndSpouse, iCntLogin As Integer
                ' Validate new email address
                If ValidateEmailID(txtNewEmailSp.Text) = True Then
                    ' Validate confirm new email address
                    If ValidateEmailID(txtConfNewEmailSp.Text) = True Then
                        If txtNewEmailSp.Text <> txtConfNewEmailSp.Text Then
                            lblErrChngeEmailSp_Popup.Text = "Confirm New Email address does not match."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                        If txtChngeEmailSp.Text.Equals(txtConfNewEmailSp.Text) Then
                            lblErrChngeEmailSp_Popup.Text = "Current mail and New Email address are same. Please try with another."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                        If Session("NavPath") = "Search" Then
                            If ValidateEmailID(txtNewEmailSp.Text) = True Then
                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from IndSpouse where email<>'' and email='" & txtNewEmailSp.Text & "'") > 0 Then
                                    lblErrChngeEmailSp_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                    IsWinVisible = True
                                    GoTo lblShow
                                ElseIf SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "Select Email from Child where Email<>'' and Email='" & txtNewEmailSp.Text & "'") > 0 Then
                                    lblErrChngeEmailSp_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                    IsWinVisible = True
                                    GoTo lblShow
                                End If

                                cmdText = "delete from login_master where user_email='" + txtChngeEmailSp.Text + "'"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)

                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from login_master where user_email='" + txtNewEmailSp.Text + "' and user_email<>'' ") = 0 Then
                                    Dim strPwd As String = GetNewPassword(txtNewEmailSp.Text)
                                    If CreateEmailLM(txtNewEmailSp.Text, strPwd, "SP") = True Then
                                        SendMailProfileCreated(txtNewEmailSp.Text, strPwd)
                                        lblErrorStatus.Visible = True
                                        lblErrorStatus.ForeColor = Color.Green
                                        lblErrorStatus.Text = "Email address was changed."
                                    End If
                                End If
                                ' update currentemail
                                If SqlHelper.ExecuteScalar(Application("Connectionstring").ToString, CommandType.Text, "select count(*) from indspouse where email='" + txtChngeEmailSp.Text + "' and email<>'' ") > 0 Then
                                    cmdText = "update indspouse set email='" + txtNewEmailSp.Text + "' where email='" + txtChngeEmailSp.Text + "' "
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                                    lblErrorStatus.Visible = True
                                    lblErrorStatus.ForeColor = Color.Green
                                    lblErrorStatus.Text = "Email address was changed."
                                End If
                            Else
                                lblErrChngeEmailSp_Popup.Text = "Invalid Primary E-mail for the individual."
                                IsWinVisible = True
                                GoTo lblShow
                            End If
                            '   SendMailProfileUpdated(txtChngeEmailSp.Text, txtNewEmailSp.Text)
                            txtPrimaryEmailSp.Text = txtNewEmailSp.Text
                            Exit Sub
                        End If
                        ' New email should not exist in IndSpouse 
                        ' New email should not exist in Login_master 
                        cmdText = "select count(*) from login_master where user_email='" + txtChngeEmailSp.Text + "' and user_pwd='" + txtChngePswdSp.Text + "'"
                        If (SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0) Then
                            cmdText = "select count(*) from login_master where user_email='" + txtConfNewEmailSp.Text + "'"
                            iCntLogin = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText))
                            cmdText = "select count(*) from indspouse where email='" + txtConfNewEmailSp.Text + "'"
                            iCntIndSpouse = Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText))
                            If iCntIndSpouse > 0 And iCntLogin > 0 Then
                                ' error msg duplicate
                                lblErrChngeEmailSp_Popup.Text = "New Email address already exists in a profile. Please try with another."
                                IsWinVisible = True
                                GoTo lblShow
                            ElseIf iCntIndSpouse = 0 And iCntLogin > 0 Then
                                If Len(txtPrimaryEmailInd.Text) > 0 Then
                                    If txtConfNewEmailSp.Text = txtPrimaryEmailInd.Text Then
                                        lblErrChngeEmailSp_Popup.Text = "The new email address should be different from the email of the ind!"
                                        IsWinVisible = True
                                        GoTo lblShow
                                    End If
                                End If
                                ' ask pswd
                                cmdText = "select count(*) from login_master where user_email='" + txtConfNewEmailSp.Text + "' and user_pwd='" + Trim(txtConfNewPwdSp.Text) + "'"
                                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0 Then
                                    ' delete currentemail
                                    cmdText = "delete from login_master where user_email='" + txtChngeEmailSp.Text + "' and user_pwd='" + Trim(txtChngePswdSp.Text) + "' "
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                                    ' Update Ind email
                                    UpdateEmailAddress(txtChngeEmailSp.Text, txtConfNewEmailSp.Text, "", "SP")
                                    AssignMailValue(txtConfNewEmailSp.Text, "SP", False)
                                    If Session("LoginEmail") = txtChngeEmailSp.Text Then
                                        Session("LoginEmail") = txtConfNewEmailSp.Text
                                    End If
                                    IsWinVisible = False
                                    ViewState("CntPswdTrialSp") = Nothing
                                Else
                                    IsWinVisible = True
                                    trConfPwdSp.Visible = True
                                    lblErrChngeEmailSp_Popup.Text = "Password exists for the new email address.  Please enter."
                                    If ViewState("CurEmailSp") = Nothing Then
                                        ViewState("CurEmailSp") = txtNewEmailSp.Text
                                    End If
                                    If ViewState("CurEmailSp") <> txtNewEmailSp.Text Then
                                        ViewState("CurEmailSp") = Nothing
                                    End If
                                    If ViewState("CntPswdTrialSp") = Nothing Then
                                        ViewState("CntPswdTrialSp") = "1"
                                    Else
                                        Dim i As Integer
                                        i = Convert.ToInt32(ViewState("CntPswdTrialSp"))
                                        i = i + 1
                                        ViewState("CntPswdTrialSp") = i.ToString()
                                        If i > 3 Then
                                            spSpAck.Visible = True
                                            tblSp.Visible = False
                                            IsWinVisible = True
                                            ViewState("CntPswdTrialSp") = 0
                                        End If
                                    End If
                                End If
                            Else
                                If Len(txtPrimaryEmailInd.Text) > 0 Then
                                    If txtConfNewEmailSp.Text = txtPrimaryEmailInd.Text Then
                                        lblErrChngeEmailSp_Popup.Text = "The new email address should be different from the email of the ind!"
                                        IsWinVisible = True
                                        GoTo lblShow
                                    End If
                                End If
                                ' update
                                cmdText = "update login_master set user_email='" + txtConfNewEmailSp.Text + "' where user_email='" + txtChngeEmailSp.Text + "'"
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                                ' Update Ind email
                                UpdateEmailAddress(txtChngeEmailSp.Text, txtConfNewEmailSp.Text, "", "SP")
                                AssignMailValue(txtConfNewEmailSp.Text, "SP", False)
                                If Session("LoginEmail") = txtChngeEmailSp.Text Then
                                    Session("LoginEmail") = txtConfNewEmailSp.Text
                                End If
                                IsWinVisible = False
                                GoTo lblShow
                            End If
                        Else
                            lblErrChngeEmailSp_Popup.Text = "Please enter correct password for the current email."
                            IsWinVisible = True
                            GoTo lblShow
                        End If
                    Else
                        lblErrChngeEmailSp_Popup.Text = "Invalid Confirm New E-mail address"
                        IsWinVisible = True
                        GoTo lblShow
                    End If
                Else
                    lblErrChngeEmailSp_Popup.Text = "Invalid New E-mail address."
                    IsWinVisible = True
                    GoTo lblShow
                End If
lblShow:
                If IsWinVisible = True Then
                    ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:PopUpChngEmailSp('show');", True)
                End If
            Catch ex As Exception
                '   Response.Write("Err :" & ex.ToString)
            End Try
        End Sub
        Protected Sub lnkBtnClickHereInd_Click(sender As Object, e As EventArgs) Handles lnkBtnClickHereInd.Click
            spIndAck.Visible = False
            tblInd.Visible = True
            trConfPwdInd.Visible = False
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ShowChangeMailPopUp('show');", True)
        End Sub
        Protected Sub lnkBtnClickHereSp_Click(sender As Object, e As EventArgs) Handles lnkBtnClickHereSp.Click
            spSpAck.Visible = False
            tblSp.Visible = True
            trConfPwdSp.Visible = False
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ShowChangeMailPopUpSp('show');", True)
        End Sub
        Protected Sub lnkbtnChangeInd_Click(sender As Object, e As EventArgs) Handles lnkbtnChangeInd.Click
            '  lblErrorStatus.Text = ""
            trConfPwdInd.Visible = False
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ShowChangeMailPopUp('show');", True)
        End Sub
        Protected Sub lnkbtnChangeSp_Click(sender As Object, e As EventArgs) Handles lnkbtnChangeSp.Click
            trConfPwdSp.Visible = False
            ClientScript.RegisterStartupScript(Page.GetType(), "Javascript", "javascript:ShowChangeMailPopUpSp('show');", True)
        End Sub
        Protected Sub btnUpdateDupSp_Click(sender As Object, e As EventArgs) Handles btnUpdateDupSp.Click
            UpdateEmailAddress("", txtPrimaryEmailSp_Popup.Text, "", "SP")
            AssignMailValue(txtPrimaryEmailSp_Popup.Text, "SP", False)
            lnkbtnChangeSp.Enabled = True
        End Sub
        Protected Sub btnUpdateDupInd_Click(sender As Object, e As EventArgs) Handles btnUpdateDupInd.Click
            UpdateEmailAddress("", txtPrimaryEmailInd_Popup.Text, "", "IND")
            AssignMailValue(txtPrimaryEmailInd_Popup.Text, "IND", False)
            lnkbtnChangeInd.Enabled = True
        End Sub
        Private Sub UpdateEmailAddress(strOldEmail As String, strNewEmail As String, strPwd As String, sType As String)
            Dim id As String = ""
            If sType = "IND" Then
                id = Session("GetIndSpouseID") 'Session("NewPwdInd") = strPwd
            ElseIf sType = "SP" Then
                id = Session("GetSpouseID") 'Session("NewPwdSp") = strPwd
            End If
            'update indspouse
            If id > 0 Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update indspouse set email='" + strNewEmail + "' where automemberid=" + id)
            End If
            ' Send mail to the user about change mail
            If Len(Trim(strOldEmail)) > 0 Then
                SendMailProfileUpdated(strOldEmail, strNewEmail)
            End If
            ' For newly created email
            If Len(Trim(strPwd)) > 0 Then
                SendMailProfileCreated(strNewEmail, strPwd)
            End If
        End Sub

        Private Sub SendMailProfileCreated(ByVal strUserId As String, ByVal strPwd As String)
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@northsouth.org"
            sMailTo = strUserId
            sSubject = "Profile Added to the NSF"
            Dim mail As New MailMessage(SMailFrom, sMailTo)
            mail.Subject = sSubject
            sBody = "Dear User, <br><br>Note : Do not reply to the email above."
            sBody = sBody & " <br><br>Your profile was added to the <b>NSF</b> database.  You can access your record, get donation receipt and participate in <b>NSF</b> activities with the following credential.  "
            sBody = sBody & "<br><br>User ID: " + strUserId + "<br>Password: " + strPwd
            mail.IsBodyHtml = True
            mail.Body = sBody
            Dim client As New SmtpClient()
            Try
                '  client.Send(mail)
            Catch ex As Exception
            End Try
        End Sub
        Private Sub SendMailProfileUpdated(strOldUserId As String, strNewUserId As String)
            Dim SMailFrom As String
            Dim sMailTo As String
            Dim sSubject As String
            Dim sBody As String
            SMailFrom = "nsfcontests@northsouth.org" 'nsfcontests@gmail.com
            sMailTo = strNewUserId
            sSubject = "Login - Email Changed to the NSF"
            Dim mail As New MailMessage(SMailFrom, sMailTo)
            mail.Subject = sSubject
            sBody = "Dear User, <br><br>Note : Do not reply to the email above."
            sBody = sBody & " <br><br>You have changed your email address from " + strOldUserId + " to " + strNewUserId
            mail.IsBodyHtml = True
            mail.Body = sBody
            Dim client As New SmtpClient()
            Try
                ' client.Send(mail)
            Catch ex As Exception
            End Try
        End Sub

        Sub SpouseValidation(bStatus As Boolean)
            rfvTitleSp.Enabled = bStatus
            rfvFirstNameSp.Enabled = bStatus
            reFirstnamesp.Enabled = bStatus
            rfvLastNameSp.Enabled = bStatus
            reLastNamesp.Enabled = bStatus
            rfvAddress1Sp.Enabled = bStatus
        End Sub

        Protected Sub rbVolunteerInd_SelectedIndexChanged(sender As Object, e As EventArgs) Handles rbVolunteerInd.SelectedIndexChanged
            If rbVolunteerInd.SelectedIndex = 0 Then
                If txtFirstNameSp.Text.Trim.Length > 0 Or txtLastNameSp.Text.Trim.Length > 0 Then
                    lblErrorStatus.Text = "You cannot select Yes, since you have a spouse."
                    rbVolunteerInd.SelectedIndex = 1
                Else
                    lblErrorStatus.Text = ""
                    tblSpouse.Visible = False
                    SpouseValidation(False)
                    txtFirstNameSp.Text = ""
                    txtLastNameSp.Text = ""
                    ddlMaritalStatusInd.SelectedIndex = ddlMaritalStatusInd.Items.IndexOf(ddlMaritalStatusInd.Items.FindByValue("NeverM"))
                    ddlMaritalStatusInd.Enabled = False
                End If
            Else
                ddlMaritalStatusInd.Enabled = True
                tblSpouse.Visible = True
                SpouseValidation(True)
            End If
        End Sub
    End Class
End Namespace

