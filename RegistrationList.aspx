<%@ Page Language="vb" AutoEventWireup="false" Inherits="VRegistration.RegistrationList" CodeFile="RegistrationList.aspx.vb" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>Volunteerlist</title>
		<meta content="Microsoft Visual Studio .NET 7.1" name="GENERATOR">
		<meta content="Visual Basic .NET 7.1" name="CODE_LANGUAGE">
		<meta content="JavaScript" name="vs_defaultClientScript">
		<meta content="http://schemas.microsoft.com/intellisense/ie5" name="vs_targetSchema">
		<LINK href="Styles.css" type="text/css" rel="stylesheet">
		<script language="javascript">
			window.history.forward(1);
		</script>
	</HEAD>
	<body>
		<form id="Form1" method="post" runat="server">
			<table width="100%" align="center">
				<TR>
					<TD class="Heading" vAlign="top" align="center" width="100%" colSpan="2">Contest 
						Registeration&nbsp;List
					</TD>
				</TR>
				<tr>
					<td colSpan="2"><br>
					</td>
				</tr>
				<tr width="100%">
					<td class="ItemLabel" vAlign="top" noWrap width="50%">NSF Chapter</td>
					<td class="ItemText" vAlign="top" noWrap align="left"><asp:dropdownlist id="ddlNSFChapter" runat="server" AutoPostBack="True" CssClass="SmallFont"></asp:dropdownlist></td>
				</tr>
				<tr>
					<td colSpan="2"><br>
					</td>
				</tr>
				<tr>
					<td width="100%" colSpan="2"><asp:datagrid id="dgVolunteerList" runat="server" CssClass="GridStyle" AutoGenerateColumns="False"
							AllowSorting="True" BorderWidth="0px" CellSpacing="1" CellPadding="3" DataKeyField="AutoMemberID" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Volunteer ID" SortExpression="AutoMemberID">
									<ItemTemplate>
										<asp:HyperLink runat="server" id="hLinkVolunteer"></asp:HyperLink>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Last Name" SortExpression="LastName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LastName") %>' CssClass="SmallFont">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="First Name" SortExpression="FirstName">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FirstName") %>' CssClass="SmallFont">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="City" SortExpression="City">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.City") %>' CssClass="SmallFont">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="State" SortExpression="State">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.State") %>' CssClass="SmallFont">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Zip Code" SortExpression="Zip">
									<ItemTemplate>
										<asp:Label runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
									<EditItemTemplate>
										<asp:TextBox runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.Zip") %>' CssClass="SmallFont">
										</asp:TextBox>
									</EditItemTemplate>
								</asp:TemplateColumn>
							</Columns>
						</asp:datagrid></td>
				</tr>
			</table>
			<div><asp:hyperlink id="hlinkChapterFunctions" runat="server" NavigateUrl="ChapterMain.aspx">Back to Chapter Functions</asp:hyperlink></div>
		</form>
	</body>
</HTML>

 

 
 
 