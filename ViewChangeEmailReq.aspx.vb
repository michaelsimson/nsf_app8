﻿Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Net
Imports System.Net.Mail
Imports NorthSouth.DAL
Imports System.Data.SqlClient
Partial Class ViewChangeEmailReq
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
    End Sub

    Sub LoadGrid()
        Dim strSql As String
        Dim top As String
        If DdlRecords.SelectedItem.Text = "All" Then
            top = ""
        Else
            top = "TOP " & DdlRecords.SelectedItem.Value & " "
        End If
        strSql = "SELECT " & top & " ChangeEmailID, OldEmail, NewEmail, OldE_IndCount, NewE_IndCount, OldMemberID, NewMemberID, Status,  Remarks,  CreatedDate FROM  ChangeEmail WHERE Status = '" + ddlStatus.SelectedValue + "' ORDER BY CreatedDate DESC"
        Dim dsDuplicates As New DataSet
        dsDuplicates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
        If dsDuplicates.Tables(0).Rows.Count > 0 Then
            dgDuplicates.Visible = True
            dgDuplicates.DataSource = dsDuplicates
            dgDuplicates.DataBind()
            lblAlert.Text = ""
        Else
            dgDuplicates.Visible = False
            lblAlert.Text = "No " & ddlStatus.SelectedValue & " Record Found."
        End If
    End Sub

    Protected Sub dgDuplicates_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgDuplicates.ItemDataBound
        If e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Then
            If DataBinder.Eval(e.Item.DataItem, "Status") = "Pending" Then
                e.Item.Cells(0).Enabled = True
            Else
                CType(e.Item.FindControl("lbtnChangeEmail"), LinkButton).Visible = False
            End If
        End If
    End Sub

    Protected Sub dgDuplicates_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgDuplicates.ItemCommand
        lblAlert.Text = "Change Email is under Development"
    End Sub

    Protected Sub dgDuplicates_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles dgDuplicates.PageIndexChanged
        dgDuplicates.CurrentPageIndex = e.NewPageIndex
        LoadGrid()
    End Sub

    Protected Sub btnRunReport_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        dgDuplicates.CurrentPageIndex = 0
        LoadGrid()
    End Sub
End Class
