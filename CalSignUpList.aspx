﻿<%@ Page Title="" Language="C#" MasterPageFile="~/NSFMasterPage.master" Debug="true" AutoEventWireup="true" CodeFile="CalSignUpList.aspx.cs" Inherits="CalSignUpList" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        <strong>Coach List
        </strong>
    </div>

    <div style="clear: both;"></div>

    <div style="clear: both;">
        <br />
    </div>
    <div style="font-size: 20px;">
        <asp:LinkButton ID="lbprevious" runat="server" Text="Previous"
            Visible="false" OnClick="lbprevious_Click">Previous</asp:LinkButton>
    </div>
    <asp:Panel ID="Panel1" runat="server" Style="width: 1200px; margin-right: auto; margin-left: auto;">
        <div style="width: 1200px; margin-right: auto; margin-left: auto;">
            <%-- <center>Coach List from Calendar Signup</center>
            <div style="clear: both; margin-bottom: 20px;"></div>--%>
            <div id="dvFrstRow">
                <div style="float: left; width: 175px;">
                    <div style="float: left; width: 80px;">
                        <span style="font-weight: bold;">Event Year</span>
                    </div>
                    <div style="float: left; width: 90px;">
                        <asp:DropDownList runat="server" ID="lstYear" AutoPostBack="true" OnSelectedIndexChanged="lstYear_SelectedIndexChanged" Width="85"></asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 175px;">
                    <div style="float: left; width: 80px;">
                        <span style="font-weight: bold;">Semester</span>
                    </div>
                    <div style="float: left; width: 90px;">
                        <asp:DropDownList ID="ddlPhase" Width="85px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 220px;">
                    <div style="float: left; width: 90px;">
                        <span style="font-weight: bold;">Product Group</span>
                    </div>
                    <div style="float: left; width: 130px;">
                        <asp:DropDownList ID="lstProductGroup" runat="server" Width="120px"
                            DataTextField="Name" DataValueField="ProductGroupId"
                            AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 220px;">
                    <div style="float: left; width: 80px;">
                        <span style="font-weight: bold;">Product</span>
                    </div>
                    <div style="float: left; width: 130px;">
                        <asp:DropDownList ID="lstProduct" SelectionMode="Multiple" runat="server" AutoPostBack="true" Width="120px" OnSelectedIndexChanged="lstProduct_SelectedIndexChanged">
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 175px;">
                    <div style="float: left; width: 80px;">
                        <span style="font-weight: bold;">Session#</span>
                    </div>
                    <div style="float: left; width: 90px;">
                        <asp:DropDownList ID="ddlSessionNo" Width="85px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="ddlSessionNo_SelectedIndexChanged">
                            <asp:ListItem Selected="True" Value="0">All</asp:ListItem>
                            <asp:ListItem Value="1">1</asp:ListItem>
                            <asp:ListItem Value="2">2</asp:ListItem>
                            <asp:ListItem Value="3">3</asp:ListItem>
                            <asp:ListItem Value="4">4</asp:ListItem>
                            <asp:ListItem Value="5">5</asp:ListItem>
                        </asp:DropDownList>
                    </div>
                </div>
                <div style="float: left; width: 220px;">
                    <div style="float: left; width: 120px;">
                        <span style="font-weight: bold;">Type of Registration</span>
                    </div>
                    <div style="float: left; width: 90px;">
                        <asp:DropDownList ID="ddlRegistype" runat="server" Width="90px" OnSelectedIndexChanged="ddlRegistype_SelectedIndexChanged" AutoPostBack="true">

                            <asp:ListItem Selected="True" Value="1">All</asp:ListItem>
                            <asp:ListItem Value="2">Accepted</asp:ListItem>
                            <%-- <asp:ListItem Value="3">Not Accepted</asp:ListItem>--%>
                        </asp:DropDownList>
                    </div>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div>
                <center>
                    <asp:Button ID="btnSendEMail" runat="server" Text="Clear" OnClick="btnSendEMail_Click1" />

                </center>
            </div>
            <div style="clear: both; margin-bottom: 10px;"></div>
            <div>
                <center>
                    <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>
                    <asp:Label ID="lblPrd" runat="server" Text="" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" runat="server" Text="" Visible="false"></asp:Label>
                </center>
            </div>
        </div>

        <table border="1" cellpadding="0" cellspacing="0" align="center" width="400px" runat="server" visible="false">
            <tr>
                <td align="center" bgcolor="#99CC33" id="tbl1"><span class="title03">Coach List from Calendar Signup</span></td>
            </tr>
            <tr>
                <td align="center">
                    <table border="0" cellpadding="3px" cellspacing="0" width="100%">
                        <tr>
                            <td align="left">Event Year</td>
                            <td align="left">
                                <%-- <asp:ListBox ID="lstYear" SelectionMode="Multiple" runat="server" Width="150px"
                                    AutoPostBack="true" OnSelectedIndexChanged="lstYear_SelectedIndexChanged"></asp:ListBox>--%>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Semester</td>
                            <td align="left"></td>
                        </tr>
                        <tr>
                            <td align="left">Product Group Name </td>
                            <td align="left"><%-- <asp:DropDownList ID="DropDownList1" runat="server">
            </asp:DropDownList>--%>
                             
                            </td>
                        </tr>
                        <tr>
                            <td align="left">Product Name </td>
                            <td align="left"></td>
                        </tr>

                        <tr>
                            <td align="left">Session #</td>
                            <td align="left"></td>
                        </tr>

                        <tr>
                            <td align="left" style="height: 27px">Type of Registration</td>
                            <td align="left" style="height: 27px"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td align="center" colspan="2"></td>
                        </tr>
                        <tr>
                            <td></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <br />
    <asp:Panel ID="pnlCoachList" runat="server" Visible="false">
        <table width="100%">
            <tr>
                <td style="text-align: center" colspan="2">
                    <h3 style="text-align: center">Table 1: Coach List</h3>
                </td>
            </tr>
            <tr>
                <td style="width: 60%">
                    <div style="float: left;">
                        <asp:Button ID="btnSortBy" runat="server" Text="Sort by Last Name, First Name" OnClick="btnSortBy_Click" Width="204px" />
                    </div>
                    <div style="float: left; margin-left: 10px;">
                        <asp:Button ID="Button1" runat="server" Text="Export to Excel" OnClick="Button1_Click" Enabled="false" />
                    </div>
                </td>

                <td style="float: right">
                    <asp:Button ID="btnSortBy2" runat="server" Text="Sort by Years(Desc), Sessions, Last Name, First Name" Width="340px" OnClick="btnSortBy2_Click" />
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <asp:GridView HorizontalAlign="Center" RowStyle-HorizontalAlign="Left" ID="GridCoachList"
                        runat="server" AllowPaging="true" PageSize="30" OnPageIndexChanging="GridCoachList_PageIndexChanging">
                        <Columns>

                            <asp:TemplateField ItemStyle-Width="20px">
                                <ItemTemplate>
                                    <asp:HiddenField ID="HdmemberId" Value='<%# Bind("MemberID")%>' runat="server" />
                                    <asp:Button runat="server" ID="btnSelect" Text="Select" OnClick="lnkView_Click" />
                                </ItemTemplate>
                            </asp:TemplateField>
                            <asp:TemplateField>
                                <HeaderTemplate>
                                    Ser#
                                </HeaderTemplate>
                                <ItemTemplate>
                                    <asp:Label ID="lblSRNO" runat="server"
                                        Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                                </ItemTemplate>
                            </asp:TemplateField>


                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
        </table>
    </asp:Panel>
    <asp:Panel ID="pnlCoachClassDetails" runat="server" HorizontalAlign="Center" Visible="false">
        <h3>Table 2: Coach Classes</h3>
        <br />
        <asp:GridView ID="GriDEdit" runat="server" align="center" DataKeyNames="SignupID"
            EnableViewState="true" AutoGenerateColumns="false" OnRowEditing="GriDEdit_RowEditing"
            OnRowCancelingEdit="GriDEdit_RowCancelingEdit"
            OnRowUpdating="GriDEdit_RowUpdating" OnRowDataBound="GriDEdit_RowDataBound">
            <Columns>
                <asp:TemplateField>

                    <ItemTemplate>
                        <asp:Button ID="btnEdit" Text="Modify" runat="server" CommandName="Edit" />

                    </ItemTemplate>

                    <EditItemTemplate>
                        <asp:Button ID="btnUpdate" Text="Update" runat="server" CommandName="Update" BackColor="SkyBlue" />

                        <asp:Button ID="btnCancel" Text="Cancel" runat="server" CommandName="Cancel" BackColor="SkyBlue" />

                    </EditItemTemplate>


                </asp:TemplateField>
                <asp:TemplateField HeaderText="ID">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblID" Text='<%#Eval("SignupID") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="FirstName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblFirstName" Text='<%#Eval("FirstName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="LastName">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblLastname" Text='<%#Eval("LastName") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="ProductGroup">
                    <ItemTemplate>
                        <asp:HiddenField ID="HdnProd" Value='<%# Bind("ProductGroup")%>' runat="server" />
                        <asp:Label runat="server" ID="lblProductGroup" Text='<%#Eval("ProductGroup") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="productcode">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblproductcode" Text='<%#Eval("productcode") %>' />
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="Semester">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbphase" Text='<%#Eval("Semester") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="Hdnphase" Value='<%# Bind("Semester")%>' runat="server" />
                        <asp:DropDownList ID="DPhase" runat="server" Width="150px" AutoPostBack="false" OnPreRender="Semester">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="Txtphase" Text='<%#Eval("Semester") %>' />
                    </EditItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="level">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbllevel" Text='<%#Eval("level") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="Hdnlevel" Value='<%# Bind("level")%>' runat="server" />
                        <asp:DropDownList ID="ddlevel" runat="server" Width="150px" AutoPostBack="false" OnPreRender="level">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="Txtlevel" Text='<%#Eval("level") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="SessionNo">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblsessionNO" Text='<%#Eval("SessionNo") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="HdnSession" Value='<%# Bind("SessionNo")%>' runat="server" />
                        <asp:DropDownList ID="ddSession" runat="server" Width="150px" AutoPostBack="false" OnPreRender="SessionNo">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxtSession" Text='<%#Eval("SessionNo") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Year">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblYear" Text='<%#Eval("Year") %>' />
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Day">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblDay" Text='<%#Eval("Day") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="HdnDay" Value='<%# Bind("Day")%>' runat="server" />
                        <asp:DropDownList ID="ddday" runat="server" Width="150px" AutoPostBack="true" OnPreRender="Day" OnSelectedIndexChanged="ddday_SelectedIndexChanged">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxtDay" Text='<%#Eval("Day") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Time">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lbltime" Text='<%#Eval("time") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <b>
                            <asp:Label runat="server" ID="lbltimeNew" Text='<%#Eval("time") %>' /></b>
                        <asp:DropDownList ID="ddtime" runat="server" Width="150px" AutoPostBack="false" OnPreRender="Time">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxTime" Text='<%#Eval("time") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Accept">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblAccepted" Text='<%#Eval("Accepted") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="HdnAccept" Value='<%# Bind("Accepted")%>' runat="server" />
                        <asp:DropDownList ID="ddlAccept" runat="server" Width="150px" AutoPostBack="false" OnPreRender="Accepted">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxAccept" Text='<%#Eval("Accepted") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>

                <asp:TemplateField HeaderText="MaxCap">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblMaxCap" Text='<%#Eval("MaxCapacity") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="Hdden" Value='<%# Bind("MaxCapacity")%>' runat="server" />
                        <asp:DropDownList ID="ddlMax" runat="server" Width="150px" AutoPostBack="false" OnPreRender="MaxCap">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxtMax" Text='<%#Eval("MaxCapacity") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>


                <asp:TemplateField HeaderText="Cycle">
                    <ItemTemplate>

                        <asp:Label runat="server" ID="lbCycle" Text='<%#Eval("Cycle")%>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="Hdncycle" Value='<%# Bind("Cycle")%>' runat="server" />
                        <asp:DropDownList ID="DCycle" runat="server" Width="150px" AutoPostBack="false" OnPreRender="Cycle">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxtCycle" Text='<%#Eval("Cycle") %>' />
                    </EditItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="VRoom">
                    <ItemTemplate>

                        <asp:Label runat="server" ID="lblVroom" Text='<%#Eval("VRoom")%>' />
                    </ItemTemplate>
                    <EditItemTemplate>
                        <asp:HiddenField ID="HdncVroom" Value='<%# Bind("VRoom")%>' runat="server" />
                        <asp:DropDownList ID="ddVroom" runat="server" Width="150px" AutoPostBack="false" OnPreRender="VRoom">
                        </asp:DropDownList>
                        <asp:TextBox runat="server" Visible="false" ID="TxtVroom" Text='<%#Eval("VRoom") %>' />
                    </EditItemTemplate>

                </asp:TemplateField>

                <asp:TemplateField HeaderText="#of Students">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblStudentCount" Text='<%#Eval("NoOfStudents") %>' />
                    </ItemTemplate>

                </asp:TemplateField>


                <asp:TemplateField HeaderText="UserID">
                    <ItemTemplate>

                        <asp:Label runat="server" ID="lblUserID" Text='<%#Eval("UserID")%>' />
                    </ItemTemplate>
                    <EditItemTemplate>

                        <asp:TextBox runat="server" ID="TxtUser" Text='<%#Eval("UserID") %>' />
                        <asp:RegularExpressionValidator ID="revPrimaryEmailInd" runat="server" ControlToValidate="TxtUser"
                            Display="Dynamic"
                            ErrorMessage="E-Mail Address should be a Valid one" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:RegularExpressionValidator>
                    </EditItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField HeaderText="PWD">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblPwd" Text='<%#Eval("PWD") %>' />
                    </ItemTemplate>
                    <EditItemTemplate>

                        <asp:TextBox runat="server" ID="Txtpwd" Text='<%#Eval("PWD") %>' />
                    </EditItemTemplate>
                </asp:TemplateField>


                <%-- <asp:BoundField DataField="RecType" HeaderText="RecType"></asp:BoundField>--%>
            </Columns>
        </asp:GridView>
    </asp:Panel>
    <asp:Label ID="lblMemberID" runat="server" Font-Bold="true" Visible="false"></asp:Label>
    <asp:Label ID="lbProd" runat="server" Font-Bold="true" Visible="false"></asp:Label>
    <asp:Label ID="lblday" runat="server" Font-Bold="true" Visible="false"></asp:Label>
</asp:Content>

