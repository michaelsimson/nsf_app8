<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AssignRole.aspx.vb" Inherits="AssignRole" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <asp:Button ID="BtnContinue" runat="server" Style="z-index: 100; left: 481px; position: absolute;
        top: 397px" Text="Continue" />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <br />
    <table border="1" style="z-index: 100; left: 389px; position: absolute; top: 167px">
    
    <tr>
    <td  class="ItemLabel">
    <asp:Label ID="LblEvent" runat="server" Text="Event"></asp:Label>
    </td>
        
    <td style="width: 170px"><asp:DropDownList ID="DdlEvent" runat="server" DataSourceID="EventDataSource" DataTextField="EventCode" DataValueField="EventId" Width="170px" AppendDataBoundItems="True">
        <asp:ListItem>[Select Item]</asp:ListItem>
        </asp:DropDownList></td>
        
    </tr>
<tr>
    <td class="ItemLabel" style="height: 26px">
        <asp:Label  ID="lblRoleCat" runat="server" Text="RoleCategory"></asp:Label>
    </td>
    <td style="height: 26px; width: 170px;"><asp:DropDownList ID="DdlRoleCategory" runat="server" AutoPostBack="True" EnableViewState="False" Width="170px">
        <asp:ListItem Value="0">[Select Item]</asp:ListItem>
        <asp:ListItem Value="National">National</asp:ListItem>
        <asp:ListItem Value="Zonal">Zonal</asp:ListItem>
        <asp:ListItem Value="Chapter">Chapter</asp:ListItem>
        <asp:ListItem Value="Finals">Finals</asp:ListItem>
        <asp:ListItem Value="Cluster">Cluster</asp:ListItem>
        <asp:ListItem Value="indiaChapter">India Chapter</asp:ListItem>
        </asp:DropDownList></td>
    
    </tr>
    
        <tr>
    <td class="ItemLabel">
        <asp:Label  ID="lblRole" runat="server" Text="Role"></asp:Label>
    </td>
    <td style="width: 170px">
    
        <asp:DropDownList ID="DdlRole" runat="server" AppendDataBoundItems="True" DataSourceID="RoleDataSource" DataTextField="Selection" DataValueField="RoleId" EnableViewState="False" Width="170px">
            <asp:ListItem>[Select Item]</asp:ListItem>
        </asp:DropDownList>
    
    </td>
    
    </tr><tr>
    <td class="ItemLabel">
    <asp:RadioButton   ID="RblZone" runat="server" Text="Zone" GroupName="Category" />
    </td>
    <td style="width: 170px"><asp:DropDownList ID="DdlZone" runat="server" DataSourceID="ZoneDataSource" DataTextField="ZoneCode" DataValueField="ZoneId" Width="170px" AppendDataBoundItems="True" AutoPostBack="True">
        <asp:ListItem Value="0">[Select Zone]</asp:ListItem>
        </asp:DropDownList>
    
    </td>
    <td><asp:DropDownList ID="ddlZoneDesc" Width="250px" runat="server" AppendDataBoundItems="True" DataSourceID="ZoneDesDataSource" DataTextField="Description" DataValueField="ZoneId" EnableViewState="False">
        <asp:ListItem>[Zone Description]</asp:ListItem>
        </asp:DropDownList></td>
    </tr><tr>
        
    <td class="ItemLabel">
        <asp:RadioButton   ID="RblCluster" runat="server" Text="Cluster" GroupName="Category" />
    
    </td>
        
    <td style="height: 26px; width: 170px;">
        <asp:DropDownList ID="DdlCluster" runat="server" DataSourceID="ClusterDataSource" DataTextField="ClusterCode" DataValueField="ClusterId" Width="170px" AppendDataBoundItems="True" AutoPostBack="True">
            <asp:ListItem Value="0">[Select item]</asp:ListItem>
        </asp:DropDownList>
    
    </td>
    <td ><asp:DropDownList ID="DdlClusterDesc" Width="250px" runat="server" AppendDataBoundItems="True" EnableViewState="False" DataSourceID="ClusterDecDataSource" DataTextField="Description" DataValueField="ClusterId">
        </asp:DropDownList></td>
    </tr><tr>
        
    <td class="ItemLabel">
    <asp:RadioButton  ID="RblChapter" runat="server" Text="Chapter" GroupName="Category" />
    </td>
    <td style="width: 170px"><asp:DropDownList ID="DdlChapter" runat="server" DataSourceID="ChapterDataSource" DataTextField="ChapterCode" DataValueField="ChapterID" Width="170px" AppendDataBoundItems="True">
        <asp:ListItem>[Select Chapter]</asp:ListItem>
        </asp:DropDownList></td>
        
        
    </tr>    
        <tr>
            <td class="ItemLabel">
                <asp:Label ID="LblEmail" runat="server" Text="Email"></asp:Label>
            
            
            
            </td>
            <td style="width: 170px">
                <asp:DropDownList ID="DDlEmail" runat="server" Width="170px">
                    <asp:ListItem>[Select Email]</asp:ListItem>
                    <asp:ListItem>nsfcontest@gmail.com</asp:ListItem>
                </asp:DropDownList>
            </td>
        </tr>
    </table>
    <asp:ObjectDataSource ID="EventDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="EventsTableAdapters.GetEventTableAdapter"></asp:ObjectDataSource>
    <asp:SqlDataSource ID="RoleDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="usp_getrolebycategory" SelectCommandType="StoredProcedure">
        <SelectParameters>
            <asp:ControlParameter ControlID="DdlRoleCategory" DefaultValue="National" Name="Selection"
                PropertyName="SelectedValue" Type="String" />
        </SelectParameters>
    </asp:SqlDataSource>
    <asp:ObjectDataSource ID="ClusterDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="ClusterTableAdapters.ClusterTableAdapter"></asp:ObjectDataSource>
    <asp:SqlDataSource ID="ChapterDataSource" runat="server" ConnectionString="<%$ ConnectionStrings:NSFConnectionString %>"
        SelectCommand="usp_GetChapterAll" SelectCommandType="StoredProcedure"></asp:SqlDataSource>
    <asp:ObjectDataSource ID="ZoneDesDataSource" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
        TypeName="GetZoneDescriptionTableAdapters.ZoneTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_ZoneId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Original_ZoneId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DdlZone" Name="ZoneID" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="ZoneCode" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <asp:ObjectDataSource ID="ClusterDecDataSource" runat="server" DeleteMethod="Delete"
        InsertMethod="Insert" OldValuesParameterFormatString="original_{0}" SelectMethod="GetData"
        TypeName="GetClusterDescriptionTableAdapters.ClusterTableAdapter" UpdateMethod="Update">
        <DeleteParameters>
            <asp:Parameter Name="Original_ClusterId" Type="Int32" />
        </DeleteParameters>
        <UpdateParameters>
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
            <asp:Parameter Name="Original_ClusterId" Type="Int32" />
        </UpdateParameters>
        <SelectParameters>
            <asp:ControlParameter ControlID="DdlCluster" Name="ClusterID" PropertyName="SelectedValue"
                Type="Int32" />
        </SelectParameters>
        <InsertParameters>
            <asp:Parameter Name="ClusterCode" Type="String" />
            <asp:Parameter Name="Description" Type="String" />
        </InsertParameters>
    </asp:ObjectDataSource>
    <br />
    <br />
    <asp:ObjectDataSource ID="ZoneDataSource" runat="server" OldValuesParameterFormatString="original_{0}"
        SelectMethod="GetData" TypeName="ZoneTableAdapters.ZoneTableAdapter"></asp:ObjectDataSource>
</asp:Content>


 

 
 
 