<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdChapter.aspx.vb" Inherits="AddUpdChapter" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:HyperLink ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink><br />
    <table cellpadding="0" cellspacing="0" border="0" align="left" width="1004">
        <tr>
            <td align="center">
                <center>
                    <table cellpadding="4" cellspacing="0" border="0" width="600">
                        <tr>
                            <td align="center" colspan="5">
                                <h2>ADD/UPDATE CHAPTER</h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3"></td>
                            <td align="center">&nbsp;</td>
                            <td align="center">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="left">Zone</td>
                            <td></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlZone" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Selected="True" Value="0">Select Zone</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left">Status</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlStatus" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="A">Active</asp:ListItem>
                                    <asp:ListItem Value="I">Inactive</asp:ListItem>
                                    <asp:ListItem Selected="True">Select Status</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left">Cluster</td>
                            <td></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlCluster" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Selected="True" Value="0">Select Cluster</asp:ListItem>
                                </asp:DropDownList></td>
                            <td align="left">NIO</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlNIO" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>

                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left">Chapter Name</td>
                            <td></td>
                            <td align="left">
                                <asp:TextBox ID="txtChapName" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                            <td align="left">NIOP</td>
                            <td align="left">
                                <asp:DropDownList ID="ddlNIOP" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Selected="True" Value="0">Select</asp:ListItem>
                                    <asp:ListItem Value="5">5</asp:ListItem>
                                    <asp:ListItem Value="6">6</asp:ListItem>
                                    <asp:ListItem Value="7">7</asp:ListItem>
                                    <asp:ListItem Value="8">8</asp:ListItem>
                                    <asp:ListItem Value="9">9</asp:ListItem>
                                    <asp:ListItem Value="10">10</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                        </tr>
                        <tr>
                            <td align="left">State</td>
                            <td></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlState" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Selected="True" Value="0">Select State</asp:ListItem>
                                </asp:DropDownList>
                            </td>
                            <td align="left"><span id="spChapterFlag" runat="server">New Chapter Flag</span></td>
                            <td align="left">
                                <asp:DropDownList ID="ddlNewflag" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="Y">Yes</asp:ListItem>
                                    <asp:ListItem Value="N">No</asp:ListItem>
                                </asp:DropDownList></td>
                        </tr>
                        <tr>
                            <td align="left">City</td>
                            <td></td>
                            <td align="left">
                                <asp:TextBox ID="txtCity" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                            <td align="left">&nbsp;</td>
                            <td align="left">&nbsp;</td>
                        </tr>
                        <tr>
                            <td align="center" colspan="5">
                                <asp:Button ID="BtnAdd" runat="server" Text="Add" Width="60" />
                                <asp:Button ID="BtnCancel" runat="server" Text="Cancel" Width="60" />
                            </td>
                        </tr>
                    </table>
                    <asp:Panel runat="server" ID="panel3" Visible="False">
                        <center>
                            <asp:Label ID="Pnl3Msg" ForeColor="red" runat="server" Text="Select the record to Modify"></asp:Label>
                        </center>
                        <div style="clear: both; margin-bottom: 1px;"></div>
                        <div style="float: left;">
                            <asp:Button ID="BtnExportToExcel" runat="server" Text="Export to Excel" OnClick="BtnExportToExcel_Click" Style="margin-left: 45px;" /></div>
                        <div style="clear: both; margin-bottom: 1px;"></div>
                        <asp:GridView ID="GridView1" runat="server" DataKeyNames="ChapterID" AutoGenerateColumns="False" OnRowCommand="GridView1_RowCommand">
                            <Columns>
                                <asp:ButtonField ButtonType="Button" CommandName="Modify" Text="Modify" HeaderText="Modify" />
                                <asp:BoundField DataField="ChapterID" HeaderText="Chapter ID" />
                                <asp:BoundField DataField="ChapterCode" HeaderText="Chapter Code" />
                                <asp:BoundField DataField="Name" HeaderText="Chapter Name" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="State" HeaderText="State" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="City" HeaderText="City" ItemStyle-HorizontalAlign="Left" />
                                <asp:BoundField DataField="ClusterId" Visible="false" HeaderText="Cluster Id" />
                                <asp:BoundField DataField="ClusterCode" HeaderText="Cluster Code" ItemStyle-HorizontalAlign="Left" />

                                <asp:BoundField DataField="ZoneId" Visible="false" HeaderText="Zone Id" />
                                <asp:BoundField DataField="ZoneCode" HeaderText="ZoneCode" ItemStyle-HorizontalAlign="Left" />

                                <asp:BoundField DataField="Status" HeaderText="Status" />
                                <asp:BoundField DataField="NewChapterFlag" HeaderText="NewChapterFlag" />
                                <asp:BoundField DataField="WebFolderName" HeaderText="WebFolderName" />
                                <asp:BoundField DataField="NIO" HeaderText="NIO" />
                                <asp:BoundField DataField="NIOP" HeaderText="NIOP" />
                            </Columns>
                        </asp:GridView>
                    </asp:Panel>
                </center>
            </td>
        </tr>
    </table>
</asp:Content>
