<%@ Page Language="vb"  Inherits="VRegistration.status"  CodeFile="AFS_status.aspx.vb" CodeFileBaseClass="VRegistration.LinkPointAPI_cs.LinkPointTxn_Page" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN" >
<HTML>
	<HEAD>
		<title>status</title>
		<meta name="GENERATOR" Content="Microsoft Visual Studio 7.0">
		<meta name="CODE_LANGUAGE" Content="C#">
		<meta name="vs_defaultClientScript" content="JavaScript">
		<meta name="vs_targetSchema" content="http://schemas.microsoft.com/intellisense/ie5">
<% If ( fIE5 ) Then %>		
		<script language="javascript">
		var reqState=0;
		function showReqXML()
		{
		
		  if(reqState == 0)
		  {
		  document.all.xmlReqData.style.display = "";
		  document.all.xmlReq.innerText="Hide Request XML";
		  reqState = 1;
		  }
		  else
		  {
		  document.all.xmlReqData.style.display = 'none';
		  document.all.xmlReq.innerText="Show Request XML";
		  reqState = 0;
		  }
		}
		var resState=0;
		function showResXML()
		{
		
		  if(resState == 0)
		  {
		  document.all.xmlResData.style.display = "";
		  document.all.xmlRes.innerText="Hide Response XML";
		  resState = 1;
		  }
		  else
		  {
		  document.all.xmlResData.style.display = 'none';
		  document.all.xmlRes.innerText="Show Response XML";
		  resState = 0;
		  }
		}		
		</script>
<% End If %>		
	</HEAD>
	<body MS_POSITIONING="GridLayout">
		<p align="center"><b><font size="5">&nbsp;TRANSACTION RESULT</font></b><br>
		</p>
		<div align="center">
			<center>
				<table border="1" cellpadding="0" cellspacing="0">
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp;SSL API Version:</td>
						<td width="808">&nbsp;&nbsp;<%=LPTxn.getVersion()%></td>
					</tr>
					<% If(R_Time.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Time:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Time%></td>
					</tr>
					<% end if
					 if (R_Ref.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Reference:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Ref%></td>
					</tr>
					<% End If 
					 if (R_Approved.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Approval:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Approved%></td>
					</tr>
					<% End If  
					 if (R_Code.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Approval code:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Code%></td>
					</tr>
					<% End If  %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Error:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Error%></td>
					</tr>
					<%  if (R_OrderNum.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Order number:</td>
						<td width="808">&nbsp;&nbsp;<%=R_OrderNum%></td>
					</tr>
					<% End If
					  if (R_Message.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Message:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Message%></td>
					</tr>
					<% End If
					  if (R_AVS.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; AVS:</td>
						<td width="808">&nbsp;&nbsp;<%=R_AVS%></td>
					</tr>
					<% End If
					  if (R_Score.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Score:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Score%></td>
					</tr>
					<% End If
					  if (R_Tax.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Tax:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Tax%></td>
					</tr>
					<% End If
					  if (R_Shipping.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Shipping:</td>
						<td width="808">&nbsp;&nbsp;<%=R_Shipping%></td>
					</tr>
					<% End If
					  if (R_FraudCode.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; Fraud Code:</td>
						<td width="808">&nbsp;&nbsp;<%=R_FraudCode%></td>
					</tr>
					<% End If
					  if (R_ESD.Length > 0) Then %>
					<tr>
						<td width="279" bgcolor="#c0c0c0" style="WIDTH: 279px">&nbsp; ESD:</td>
						<td width="808">&nbsp;&nbsp;<%=R_ESD%></td>
					</tr>
					<% End If  %>
				</table>
			</center>
		</div>
		<p />
		<p />
		<% 
		if ( fIE5 ) then
		' do DHTML	
		%>
		<a id="xmlReq" href="javascript:showReqXML()">Show Request XML</a>
		<div id="xmlReqData" style="display: 'none'">
			<% 
			
			Response.Write("<textarea cols=60 readonly rows=20>" & order & "</textarea>") 
			%>
		</div>
		<br>
		<a id="xmlRes" href="javascript:showResXML()">Show Response XML</a>
		<div id="xmlResData" style="display: 'none'">
			<%
			Dim dbgXML As String
			dbgXML = (resp.Replace("><",">" & Chr(10) & "<")).Replace(">" & Chr(10) & "</","></")
			Response.Write("<textarea  cols=70 readonly rows=14 wrap=off >" & dbgXML & "</textarea>") 
		end if
		%>
		</div>
	</body>
</HTML>
