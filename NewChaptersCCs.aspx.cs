﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Data;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Drawing;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using NativeExcel;
using Microsoft.ApplicationBlocks.Data;



public partial class NewChaptersCCs : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

        if (!IsPostBack)
        {
            populateChapterCodes();
            populateChapterCoordinator();
        }

    }


    public void populateChapterCodes()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            cmdText = "select distinct C.ChapterCode,C.State,V.MemberID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone from Chapter C inner join volunteer V on (C.ChapterID=V.ChapterID) inner join IndSpouse IP on (V.MemberID=IP.AutoMemberID)  where C.CreateDate > DATEADD(month, -15, Convert(varchar, getdate(), 106)) order by C.State, C.ChapterCode ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    spnStatus.Visible = false;
                    grdChpaterCodes.DataSource = ds;
                    grdChpaterCodes.DataBind();
                }
                else
                {
                    spnStatus.Visible = true;
                }
            }
            else
            {
                spnStatus.Visible = true;
            }
        }
        catch
        {
        }

    }

    public void populateChapterCoordinator()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            cmdText = "Select V.ChapterCode, IP.FirstName, IP.LastName,IP.EMail, IP.CPhone, IP.HPhone from Volunteer V inner join Indspouse IP on (V.MemberID=IP.AutoMemberID) where RoleId=5 and V.CreateDate > DATEADD(month, -15, Convert(varchar, getdate(), 106)) order by IP.State, V.ChapterCode ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    spnChapCordinator.Visible = false;
                    grdChapterCoordinator.DataSource = ds;
                    grdChapterCoordinator.DataBind();
                }
                else
                {
                    spnChapCordinator.Visible = true;
                }
            }
            else
            {
                spnChapCordinator.Visible = true;
            }
        }
        catch
        {
        }

    }


    public void ExportExcel()
    {
        try
        {

            string cmdText = string.Empty;
            DataSet ds = null;
            cmdText = "select distinct C.ChapterCode,C.State,V.MemberID,IP.EMail,IP.FirstName,IP.LastName,IP.HPhone,IP.CPhone from Chapter C inner join volunteer V on (C.ChapterID=V.ChapterID) inner join IndSpouse IP on (V.MemberID=IP.AutoMemberID)  where C.CreateDate > DATEADD(month, -15, Convert(varchar, getdate(), 106)) order by C.State, C.ChapterCode ASC; Select V.ChapterCode, IP.FirstName, IP.LastName,IP.EMail, IP.CPhone, IP.HPhone from Volunteer V inner join Indspouse IP on (V.MemberID=IP.AutoMemberID) where RoleId=5 and V.CreateDate > DATEADD(month, -15, Convert(varchar, getdate(), 106)) order by IP.State, V.ChapterCode ASC";

            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    IWorkbook oWorkbooks = NativeExcel.Factory.CreateWorkbook();
                    IWorksheet oSheet = default(IWorksheet);
                    IWorksheet oSheet1 = default(IWorksheet);
                    oSheet = oWorkbooks.Worksheets.Add();
                    oSheet1 = oWorkbooks.Worksheets.Add();
                    oSheet.Name = "New Chapters";
                    oSheet1.Name = "New Chapter Coordinators";
                    oSheet.Range["A1:Q1"].MergeCells = true;
                    oSheet.Range["A1"].Value = "New Chapters";
                    oSheet.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet.Range["A1"].Font.Bold = true;
                    oSheet.Range["A1"].Font.Color = Color.Black;


                    oSheet.Range["A3"].Value = "Ser#";
                    oSheet.Range["A3"].Font.Bold = true;

                    oSheet.Range["B3"].Value = "Chapter Code";
                    oSheet.Range["B3"].Font.Bold = true;

                    oSheet.Range["C3"].Value = "First Name";
                    oSheet.Range["C3"].Font.Bold = true;

                    oSheet.Range["D3"].Value = "Last Name";
                    oSheet.Range["D3"].Font.Bold = true;

                    oSheet.Range["E3"].Value = "Email";
                    oSheet.Range["E3"].Font.Bold = true;

                    oSheet.Range["F3"].Value = "HPhone";
                    oSheet.Range["F3"].Font.Bold = true;

                    oSheet.Range["G3"].Value = "CPhone";
                    oSheet.Range["G3"].Font.Bold = true;


                    oSheet1.Range["A1:Q1"].MergeCells = true;
                    oSheet1.Range["A1"].Value = "New Chapter Coordinators";
                    oSheet1.Range["A1"].HorizontalAlignment = XlHAlign.xlHAlignCenter;
                    oSheet1.Range["A1"].Font.Bold = true;
                    oSheet1.Range["A1"].Font.Color = Color.Black;


                    oSheet1.Range["A3"].Value = "Ser#";
                    oSheet1.Range["A3"].Font.Bold = true;

                    oSheet1.Range["B3"].Value = "Chapter Code";
                    oSheet1.Range["B3"].Font.Bold = true;

                    oSheet1.Range["C3"].Value = "First Name";
                    oSheet1.Range["C3"].Font.Bold = true;

                    oSheet1.Range["D3"].Value = "Last Name";
                    oSheet1.Range["D3"].Font.Bold = true;

                    oSheet1.Range["E3"].Value = "Email";
                    oSheet1.Range["E3"].Font.Bold = true;

                    oSheet1.Range["F3"].Value = "HPhone";
                    oSheet1.Range["F3"].Font.Bold = true;

                    oSheet1.Range["G3"].Value = "CPhone";
                    oSheet1.Range["G3"].Font.Bold = true;

                    int iRowIndex = 4;
                    IRange CRange = default(IRange);
                    int i = 0;
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {

                        CRange = oSheet.Range["A" + iRowIndex.ToString()];
                        CRange.Value = i + 1;

                        CRange = oSheet.Range["B" + iRowIndex.ToString()];
                        CRange.Value = dr["ChapterCode"];

                        CRange = oSheet.Range["C" + iRowIndex.ToString()];
                        CRange.Value = dr["FirstName"];

                        CRange = oSheet.Range["D" + iRowIndex.ToString()];
                        CRange.Value = dr["LastName"];

                        CRange = oSheet.Range["E" + iRowIndex.ToString()];
                        CRange.Value = dr["Email"];

                        CRange = oSheet.Range["F" + iRowIndex.ToString()];
                        CRange.Value = dr["HPhone"];

                        CRange = oSheet.Range["G" + iRowIndex.ToString()];
                        CRange.Value = dr["CPhone"];

                      
                        iRowIndex = iRowIndex + 1;
                        i = i + 1;
                    }

                    int jRowIndex = 4;
                    IRange CJRange = default(IRange);
                    int j = 0;
                    foreach (DataRow dr in ds.Tables[1].Rows)
                    {

                        CJRange = oSheet1.Range["A" + jRowIndex.ToString()];
                        CJRange.Value = j + 1;

                        CJRange = oSheet1.Range["B" + jRowIndex.ToString()];
                        CJRange.Value = dr["ChapterCode"];

                        CJRange = oSheet1.Range["C" + jRowIndex.ToString()];
                        CJRange.Value = dr["FirstName"];

                        CJRange = oSheet1.Range["D" + jRowIndex.ToString()];
                        CJRange.Value = dr["LastName"];

                        CJRange = oSheet1.Range["E" + jRowIndex.ToString()];
                        CJRange.Value = dr["Email"];

                        CJRange = oSheet1.Range["F" + jRowIndex.ToString()];
                        CJRange.Value = dr["HPhone"];

                        CJRange = oSheet1.Range["G" + jRowIndex.ToString()];
                        CJRange.Value = dr["CPhone"];


                        jRowIndex = jRowIndex + 1;
                        j = j + 1;
                    }



                    DateTime dt = DateTime.Now;
                    string month = dt.ToString("MMM");
                    string day = dt.ToString("dd");
                    string year = dt.ToString("yyyy");
                    string monthDay = month + "" + day;
                    string FileName = "NewChaptersAndCCs";

                    string filename = "" + FileName + "_" + monthDay + "_" + year + ".xls";

                    Response.Clear();
                    Response.ContentType = "application/vnd.ms-excel";
                    Response.AddHeader("Content-Type", "application/vnd.ms-excel");
                    Response.AddHeader("Content-Disposition", "attachment;filename=" + filename);
                    oWorkbooks.SaveAs(Response.OutputStream);
                    Response.End();
                }
            }

        }
        catch
        {
        }
    }

    protected void btnExportExcel_Click(object sender, EventArgs e)
    {
        ExportExcel();
    }
}