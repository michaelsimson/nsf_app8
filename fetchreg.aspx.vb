Imports System
Imports System.Collections
Imports System.ComponentModel
Imports System.Data
Imports System.Drawing
Imports System.Web
Imports System.Web.SessionState
Imports System.Web.UI
Imports System.Web.UI.WebControls
Imports System.Web.UI.HtmlControls
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Imports Custom.Web.UI.WebControls

Partial Class fetchreg
    Inherits System.Web.UI.Page
    Shared prevPage As String = [String].Empty


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not IsPostBack Then
            prevPage = Request.UrlReferrer.ToString()

        End If

        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim prmArray(8) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@contestyear"
        prmArray(0).Value = Session("contestyear")
        prmArray(0).Direction = ParameterDirection.Input

        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ChildNumber"
        prmArray(1).Value = Request.QueryString("childnumber")
        prmArray(1).Direction = ParameterDirection.Input

        prmArray(2) = New SqlParameter
        prmArray(2).ParameterName = "@ParentId"
        prmArray(2).Value = Request.QueryString("parentid")
        prmArray(2).Direction = ParameterDirection.Input

       
        Dim ds As DataSet = Nothing

        Try

            ds = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "USP_Contest_Registration_Fetch", prmArray)
        Catch se As SqlException
            'lblmsg.Text = se.Message
            Return
        End Try

        If (ds.Tables(0).Rows.Count > 0) Then

            Rptfetch.DataSource = ds.Tables(0)
            Rptfetch.DataBind()

        End If
    End Sub

    Protected Sub Btnback_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Btnback.Click
        Response.Redirect(prevPage)
    End Sub
End Class
