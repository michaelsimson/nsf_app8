<%@ Application Language="VB" %>

<script runat="server">

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application startup
        ' Fires when the application is started
        Application("ConnectionString") = System.Configuration.ConfigurationManager.AppSettings("DBConnection")
        Application("ContestYear") = System.Configuration.ConfigurationManager.AppSettings("Contest_Year")
        Application("ContestType") = System.Configuration.ConfigurationManager.AppSettings("Contest_Finals")
        Application("NationalFinalsCity") = System.Configuration.ConfigurationManager.AppSettings("NationalFinalsCity")
        Application("FounderEmail") = System.Configuration.ConfigurationManager.AppSettings("FounderEMail")
        Application("LateRegistrationDate") = System.Configuration.ConfigurationManager.AppSettings("NationalFinalsLateDate")
        Application("LateFee") = System.Configuration.ConfigurationManager.AppSettings("NationalFinalsLateFee")
        Application("ContestEmail") = System.Configuration.ConfigurationManager.AppSettings("ContestEmail")
    End Sub
    
    Sub Application_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs on application shutdown
    End Sub
        
    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when an unhandled error occurs
        Dim prmArray(5) As System.Data.SqlClient.SqlParameter
        Dim conn As New System.Data.SqlClient.SqlConnection(Application("ConnectionString"))
        Dim cmd As New System.Data.SqlClient.SqlCommand
        Dim LastError As Exception = Server.GetLastError()
        Dim objExcept As Exception

        cmd.Connection = conn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_InsertAppExceptions"
        'conn.Open()

        Dim objErr As Exception = Server.GetLastError().GetBaseException
        Dim err As String = "Error in: " & Request.Url.ToString() & " Error Message: " & objErr.Message.ToString() & " Stack Trace: " & objErr.StackTrace.ToString()


        err.Replace(vbCrLf, "     ")

        If Len(err) > 4000 Then
            cmd.Parameters.AddWithValue("@ExceptionText", err.Substring(1, 4000))
        Else
            cmd.Parameters.AddWithValue("@ExceptionText", err)
        End If


        '*** Send EMail to the Contests@northsouth.org
        'Dim client As New System.Net.Mail.SmtpClient()
        'Dim mailObj As New System.Net.Mail.MailMessage
        'mailObj.From = New System.Net.Mail.MailAddress("contests@northsouth.org")
        'mailObj.To.Add(Application("ContestEmail"))
        'mailObj.CC.Add("chitturi@mail.org")
        'mailObj.Subject = "2006 Registration System Exception: "
        'mailObj.Body = err
     
        ''client.Host = "."
        ''client.Send(mailObj)


        'If Not Session("LoginEmail") Is Nothing Then
        '    cmd.Parameters.AddWithValue("@LoginID", Session("LoginEmail"))
        'Else
        '    cmd.Parameters.AddWithValue("@LoginID", 0)
        'End If

        'If Not Session("IndID") Is Nothing Then
        '    If Session("IndID") > 0 Then
        '        cmd.Parameters.AddWithValue("@IndId", Session("IndID"))
        '    Else
        '        cmd.Parameters.AddWithValue("@IndId", Session("ChapterID"))
        '    End If
        'Else
        '    cmd.Parameters.AddWithValue("@IndId", DbType.UInt32).Value = 0
        'End If

        'If Session("SpouseID") > 0 Then
        '    cmd.Parameters.AddWithValue("@SpouseID", Session("SpouseID"))
        'Else
        '    cmd.Parameters.AddWithValue("@SpouseID", 0)
        'End If

        'cmd.ExecuteNonQuery()

        conn.Close()

    End Sub

    Sub Session_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a new session is started
    End Sub

    Sub Session_End(ByVal sender As Object, ByVal e As EventArgs)
        ' Code that runs when a session ends. 
        ' Note: The Session_End event is raised only when the sessionstate mode
        ' is set to InProc in the Web.config file. If session mode is set to StateServer 
        ' or SQLServer, the event is not raised.
    End Sub
       
</script>