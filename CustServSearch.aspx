﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="CustServSearch.aspx.vb" Inherits="CustServSearch" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div style="text-align:left">
           <asp:LinkButton CssClass="btn_02" ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
</div>
<div>
          <table cellspacing="1" cellpadding="3"   align="center" border="0" >
            <tr bgcolor="#FFFFFF" >
                <td class="Heading" colspan="2" align="center" >Search Parent Records
</td>
            </tr>
            
          
            <tr>
                <td>&nbsp;</td><td>&nbsp;</td>
            </tr>
            
           
           
            <tr bgcolor="#FFFFFF" >
                <td >
                    E-Mail:</td>
                <td >
                    <asp:TextBox ID="tbEmail" runat="server" CssClass="inputBox"> </asp:TextBox>
                    </td>
                    </tr>
            <tr bgcolor="#FFFFFF" > 
                <td >
                    First Name</td>
                <td >
                    <asp:TextBox ID="tbFname" runat="server" CssClass="inputBox"></asp:TextBox>
                    </td>
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td >
                    Last Name:</td>
                <td >
                    <asp:TextBox ID="tbLname" runat="server" CssClass="inputBox"></asp:TextBox></td>
           </tr>
            <tr bgcolor="#FFFFFF" >
              <td >
                    State:</td>
                <td >
                    <asp:DropDownList ID="ddlState" runat="server" CssClass="inputBox">
                    </asp:DropDownList>
                 </td>
            </tr>
           
            <tr bgcolor="#FFFFFF" >
                <td >
                    NSF Chapter</td>
                <td >
                    <asp:DropDownList ID="ddlChapter" runat="server" CssClass="inputBox">
                    </asp:DropDownList></td>
               
            </tr>
            <tr bgcolor="#FFFFFF" >
                <td  align="center" colspan="2"> 
                    <asp:Button ID="btnSearch" runat="server" Text="Search" CssClass="FormButton" />
                </td>
            </tr>
        </table>
        <br />
         &nbsp; &nbsp;
        <asp:GridView DataKeyNames="AutomemberId" OnRowCommand="gvIndSpouse_RowCommand"  ID="gvIndSpouse"  runat="server" AllowPaging="True" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Double" Caption="Individual / Spouse" CssClass="GridStyle"  CaptionAlign="Top" PageSize="5"   BorderWidth="2px" CellPadding="3" EnableSortingAndPagingCallbacks="false">
            <Columns>                
                <asp:BoundField DataField="AutoMemberID" HeaderText="Member ID" />
                <asp:ButtonField Text="View Child"  headerText="View Child Info" ></asp:ButtonField>
<%--                <asp:HyperLinkField DataNavigateUrlFields="AutoMemberID" DataNavigateUrlFormatString="~/MainChild.aspx?Id={0}"  HeaderText="View Child Info" Text="View Child" />--%>
                <asp:BoundField DataField="FirstName" HeaderText="First Name" />
                <asp:BoundField DataField="MiddleInitial" HeaderText="Initial" />
                <asp:BoundField DataField="LastName" HeaderText="Last Name" />
                <asp:BoundField DataField="Address1" HeaderText="Address1" />
                <asp:BoundField DataField="State" HeaderText="State" />
                <asp:BoundField DataField="City" HeaderText="City" />
                <asp:BoundField DataField="Zip" HeaderText="Zip" />
                <asp:BoundField DataField="Email" HeaderText="Email" />
                <asp:BoundField DataField="HPhone" HeaderText="Home Phone" />
                <asp:BoundField DataField="CPhone" HeaderText="Cell Phone" />                
                <asp:BoundField DataField="Chapter" HeaderText="Chapter" />
            </Columns>
            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        </asp:GridView><br />
        <center>
        <asp:GridView DataKeyNames="ChildNumber"  ID="GVChild"  runat="server" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False" Caption="Child Info" BorderStyle="Double" CssClass="GridStyle" CaptionAlign="Top"   BorderWidth="2px" CellPadding="3" >
            <Columns>                
                <asp:BoundField DataField="ChildNumber" HeaderText="ChildNumber" />
                <asp:BoundField DataField="FIRST_NAME" HeaderText="First Name" />
                <asp:BoundField DataField="LAST_NAME" HeaderText="Last Name" />
                <asp:BoundField DataField="GENDER" HeaderText="GENDER" />
                <asp:BoundField DataField="DATE_OF_BIRTH" HeaderText="D.O.B" />
                <asp:BoundField DataField="GRADE" HeaderText="GRADE" />
                <asp:BoundField DataField="SchoolName" HeaderText="School" />
                 <asp:BoundField DataField="Memberid" HeaderText="Member Id" />
            </Columns>
            <HeaderStyle BorderStyle="Solid" BorderWidth="1px"  Font-Bold="True" ForeColor="White" />
            <FooterStyle BackColor="White" ForeColor="#000066" />
            <RowStyle ForeColor="#000066" />
            <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
        </asp:GridView>
        </center>
    </div>
    <asp:HiddenField ID="HSQL" runat="server" />
    <asp:Label ForeColor="Red" ID="lblMessage" runat="server" ></asp:Label>
</asp:Content>



 
 
 

