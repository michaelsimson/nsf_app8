
<!--#include file="scholarships_home_header.aspx"-->

<div class="title02">Scholarships 2010-11 </div>
<div align="justify">
<p class="txt01" align="justify">
Every time a child participates in an NSF bee or workshop, one other child in India benefits. What started off as one scholarship in 1989 is at 922 in 2011 and growing. Since 1989 NSF has granted scholarships to over 5000 very deserving children. The scholarships team worked very hard in 2010 to serve North India which was underrepresented and successfully started new chapters in (1) Chandigarh, Punjab and Haryana, (2) Guwahati, Assam, (3) Moradabad, UP, and (4) Patna, Bihar. Volunteers living in US (liaisons) motivated their friends and families in their hometowns and helped start new chapters. We hope their efforts will motivate other volunteers to come forward and start new chapters. 
</p>
<center>
<table class="txt01" width="365px" border="1" cellpadding="0" cellspacing="0">
<tr  bgcolor="#42c01e">
<td colspan="5" style=" font-family: Arial; color:#FFFFFF; font-size:11pt; font-weight:bold" align="center">
<b>2010-2011 NSF INDIA SCHOLARSHIPS SUMMARY</b>
</td>
</tr>
<tr>
<td width="10%">
</td>
<td width="47%" >
<b>&nbsp;Chapter</b>
</td>
<td align="center" width="17%" >
<b>Renewal</b>
</td>
<td align="center" width="13%" >
<b>Fresh</b>
</td>
<td align="center" width="13%" >
<b>Total</b>
</td>
</tr>
<tr  bgcolor="#eaf2dd">
<td align="center" >1</td>
<td align="left" >&nbsp;Bangalore</td>
<td align="center" >68</td>
<td align="center" >28</td>
<td align="center" >96</td>
</tr>
<tr>
<td align="center" >2</td>
<td  align="left" >&nbsp;Bhavnagar*</td>
<td  align="center" >0</td>
<td  align="center" >3</td>
<td  align="center" >3</td>
</tr>
<tr  bgcolor="#eaf2dd">
<td align="center" >3</td>
<td align="left" >&nbsp;Chandigarh*</td>
<td align="center" >0</td>
<td align="center" >6</td>
<td align="center" >6</td>
</tr>
<tr>
<td align="center" >4</td>
<td align="left" >&nbsp;Chennai</td>
<td align="center" >2</td>
<td align="center" >16</td>
<td align="center" >18</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center" >5</td>
<td align="left" >&nbsp;Guwahati*</td>
<td align="center" >0</td>
<td align="center" >3</td>
<td align="center" >3</td>
</tr>
<tr >
<td align="center" >6</td>
<td align="left" >&nbsp;Hyderabad</td>
<td align="center" >124</td>
<td align="center" >58</td>
<td align="center" >182</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center" >7</td>
<td align="left" >&nbsp;Jodhpur</td>
<td align="center" >61</td>
<td align="center" >26</td>
<td align="center" >87</td>
</tr>
<tr  >
<td align="center" >8</td>
<td align="left" >&nbsp;Kochi</td>
<td align="center" >12</td>
<td align="center" >1</td>
<td align="center" >13</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center">9</td>
<td align="left" >&nbsp;Kolkata</td>
<td align="center" >47</td>
<td align="center" >37</td>
<td align="center" >84</td>
</tr>
<tr >
<td align="center" >10</td>
<td align="left" >&nbsp;Madurai</td>
<td align="center" >38</td>
<td align="center" >30</td>
<td align="center" >68</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center" >11</td>
<td align="left" >&nbsp;Moradabad*</td>
<td align="center" >0</td>
<td align="center" >3</td>
<td align="center" >3</td>
</tr>
<tr >
<td align="center" >12</td>
<td align="left" >&nbsp;Nagercoil</td>
<td align="center" >7</td>
<td align="center" >7</td>
<td align="center" >14</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center" >13</td>
<td align="left" >&nbsp;Bhubaneshwar</td>
<td align="center" >112</td>
<td align="center" >132</td>
<td align="center" >244</td>
</tr>
<tr>
<td align="center" >14</td>
<td align="left" >&nbsp;Patna*</td>
<td align="center" >0</td>
<td align="center" >8</td>
<td align="center" >8</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center" >15</td>
<td align="left" >&nbsp;Pune</td>
<td align="center" >40</td>
<td align="center" >45</td>
<td align="center" >85</td>
</tr>
<tr >
<td align="center" >16</td>
<td align="left" >&nbsp;Patiala*</td>
<td align="center" >0</td>
<td align="center" >8</td>
<td align="center" >8</td>
</tr>
<tr bgcolor="#eaf2dd">
<td align="center"></td>
<td align="left" style=" padding-left:20px; font-family: Arial; color:#000000; font-size:10pt; font-weight:bold">
<b>TOTAL</b></td>
<td align="center"><b>511</b></td>
<td align="center"><b>411</b></td>
<td align="center"><b>922</b></td>
</tr>
<tr>
<td>
</td>
<td colspan="4">
&nbsp;* New Chapters Started in 2010
</td>
</tr>
<tr >
<td align="center" ></td>
<td align="left" >&nbsp;Designated Sch</td>
<td align="center" >63</td>
<td align="center" ></td>
<td align="center" >63</td>
</tr>
</table>
</center>
</div>
<!--#include file="scholarships_home_footer.aspx"-->
                        
