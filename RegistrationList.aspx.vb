'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/Volunteerlist.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page enables the Chapter Co-ordinators and the National 
'           Co-ordinators to view the Registerd Volunteers and tho edit 
'           any data pertains to a Registerd Volunteer.  
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetChapters
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data
Imports System.Data.SqlClient

Imports Microsoft.ApplicationBlocks.Data

Imports NorthSouth.BAL


Namespace VRegistration

Partial Class RegistrationList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Protected WithEvents Label1 As System.Web.UI.WebControls.Label


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("login.aspx")
        End If
        If Not Page.IsPostBack() Then

            '*** Populate Chapter DropDown
            Dim dsChapters As New DataSet
            Dim objChapters As New Chapter
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)
            If dsChapters.Tables.Count > 0 Then
                ddlNSFChapter.DataSource = dsChapters.Tables(0)
                ddlNSFChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterLocation").ToString
                ddlNSFChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlNSFChapter.DataBind()
            End If

            If Session("RoleGeneral") = True Then
                If Not Session("ChapterID") Is Nothing Then
                    ddlNSFChapter.Items.FindByValue(Session("ChapterID")).Selected = True
                    ddlNSFChapter.Enabled = False
                    ddlNSFChapter.Visible = True
                    LoadDataGrid(Session("ChapterID"))
                End If
            End If
            If Session("Admin") = True Then
                ddlNSFChapter.Items(1).Selected = True
                ddlNSFChapter.Enabled = True
                LoadDataGrid(ddlNSFChapter.SelectedItem.Value)
            End If
        End If
    End Sub

    Private Sub dgVolunteerList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgVolunteerList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                CType(e.Item.FindControl("hLinkVolunteer"), HyperLink).NavigateUrl = "Registration.aspx?VolunteerID=" & e.Item.DataItem("AutoMemberID").ToString & "&Action=ChapterFunction"
                CType(e.Item.FindControl("hLinkVolunteer"), HyperLink).Text = e.Item.DataItem("FirstName") & " " & e.Item.DataItem("LastName")
        End Select
    End Sub
    Private Sub dgVolunteerList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles dgVolunteerList.SelectedIndexChanged
        Session("VolunteerID") = dgVolunteerList.DataKeys(dgVolunteerList.SelectedIndex)
    End Sub
    Private Sub ddlNSFChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlNSFChapter.SelectedIndexChanged
        '*** Populate Volunteer DataGrid
        LoadDataGrid(ddlNSFChapter.SelectedValue)
    End Sub
    Private Sub LoadDataGrid(ByVal ChapterID As Integer)
        Dim dsIndSpouse As New DataSet
        Dim tblIndSpouse() As String = {"IndSpouse"}

            Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim prmArray(2) As SqlParameter

        prmArray(0) = New SqlParameter
        prmArray(0).ParameterName = "@ChapterID"
        prmArray(0).Value = ddlNSFChapter.SelectedItem.Value
        prmArray(0).Direction = ParameterDirection.Input


        prmArray(1) = New SqlParameter
        prmArray(1).ParameterName = "@ContestYear"
        prmArray(1).Value = Application("ContestYear")
        prmArray(1).Direction = ParameterDirection.Input


        '*** Populate Event Name List
        SqlHelper.FillDataset(conn, CommandType.StoredProcedure, "usp_GetRegisteredParents", dsIndSpouse, tblIndSpouse, prmArray)

        If dsIndSpouse.Tables.Count > 0 Then
            If ViewState("OrderBy") Is Nothing Then
                ViewState("OrderBy") = "AutoMemberID"
            End If

            dsIndSpouse.Tables(0).DefaultView.Sort = ViewState("OrderBy")
            dgVolunteerList.DataSource = dsIndSpouse.Tables(0)
            dgVolunteerList.DataBind()
        End If
    End Sub

    Private Sub dgVolunteerList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgVolunteerList.SortCommand
        If e.SortExpression = ViewState("OrderBy") Then
            ViewState("OrderBy") = e.SortExpression & " DESC"
        Else
            ViewState("OrderBy") = e.SortExpression
        End If

        If Not Session("ChapterID") Is Nothing Then
            LoadDataGrid(Session("ChapterID"))
        Else
            LoadDataGrid(ddlNSFChapter.SelectedValue)
        End If
    End Sub
End Class

End Namespace

