<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="ChapVolunteerRoles.aspx.cs" Inherits="ChapVolunteerRoles" Title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table style="border-left-color: black; border-bottom-color: black; border-top-style: double; border-top-color: black; border-right-style: double; border-left-style: double; border-right-color: black; border-bottom-style: double">
        <caption style="font-weight:bold">
            Select an option:</caption>
        <tr style="width:auto">
            <td style="width: 379px">
                <strong>Add/Update Existing Volunteers for your Chapter</strong></td>
            <td style="width: 220px">
                <asp:HyperLink ID="hlinkToVolunteer" runat="server" NavigateUrl="~/Admin/VolunteerEditByChap.aspx">Click here</asp:HyperLink></td>
        </tr>
        <tr>
            <td style="width: 379px">
                ----------------------------------------------------------------------------------------------</td>
            <td style="width: 220px">
                ------------------------------------------------------</td>
        </tr>
        <tr><td style="width: 379px">
                <strong>Select an Individual to add to Volunteer List </strong>
            </td>
            <td style="width: 220px">
                <asp:HyperLink ID="hlinkToSelectInd" runat="server" NavigateUrl="~/Admin/VolunteerNew.aspx">Click here</asp:HyperLink></td>
        </tr>
    </table>
    <asp:LinkButton ID="lnkBtn" runat="server" OnClick="lnkBtn_Click">Goback to higher menu</asp:LinkButton>
</asp:Content>


 

 
 
 