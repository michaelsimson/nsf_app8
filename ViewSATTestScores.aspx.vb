﻿Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports VRegistration

Partial Class ViewSATTestScores
    Inherits System.Web.UI.Page
    Dim User As Boolean

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        lblErrorMsg.Visible = False
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack() Then


            Try

                Dim year As Integer = 0
                year = Now.Year
                'If Now.Month <= 5 Then
                '    year = Now.Year - 1
                'Else
                '    year = Now.Year
                'End If
                ' year = Convert.ToInt32(DateTime.Now.Year)

                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year + 1) + "-" + Convert.ToString(year + 2).Substring(2, 2)), Convert.ToString(year + 1)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 1) + "-" + Convert.ToString(year).Substring(2, 2)), Convert.ToString(year - 1)))
                ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 2) + "-" + Convert.ToString(year - 1).Substring(2, 2)), Convert.ToString(year - 2)))

                ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()



                loadPhase(ddlSemester)
                loadPhase(ddlPSemester)

                'If Now.Month >= 3 Then
                '    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
                '    Dim iMaxYear As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "if not exists(select 1 from coachreg where approved='y' and eventyear=year(GetDate())) begin select  max(eventyear) from coachreg where  approved='y'  End Else select 0")
                '    If iMaxYear > 0 Then
                '        ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(iMaxYear)))
                '    End If
                'Else
                '    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
                'End If

                ''Options for Volunteer Name Selection
                If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    ddlPSemester.Visible = True
                    spnSemTitle.Visible = True
                    TrInsVol.Visible = False
                    TrInsParStd.Visible = True
                    lnkVolFunction.Visible = False
                    Session("User") = True
                    TrStudReport.Visible = False
                    lblNote.Visible = False
                    tdSemCoach.Visible = False
                    tdSemCoachDr.Visible = False
                    '   TrParent.Visible = True
                    TrCoach.Visible = False
                    tdWeekNo.Visible = True
                    tdWeekNoTitle.Visible = True
                    Btn_Submit.Visible = True
                    'btnGradeCard_Parent.Visible = True
                    If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
                        lnkParentPage.Visible = True
                        lnkstudentPage.Visible = False
                        Loadchild(Convert.ToInt32(Session("CustIndID")), 0)
                    ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                        lnkParentPage.Visible = False
                        lnkstudentPage.Visible = True
                        Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))

                    End If
                ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                    If (Session("RoleId").ToString() = "88") Then
                        lblCoach.Visible = False
                    Else
                        lblCoach.Visible = True
                    End If
                    Btn_Submit.Visible = False
                    BtnMissedAnswers.Enabled = True
                    BtnHWSubmission.Enabled = True
                    BtnCalcualteScores.Enabled = True
                    BtnViewSCores.Enabled = True
                    btnGradeCard_Parent.Enabled = True
                    tdWeekNo.Visible = True
                    tdWeekNoTitle.Visible = True
                    ddlPSemester.Visible = False
                    spnSemTitle.Visible = False
                    TrInsVol.Visible = True
                    TrInsParStd.Visible = False
                    lnkParentPage.Visible = False
                    lnkVolFunction.Visible = True
                    lnkstudentPage.Visible = False
                    Session("User") = False
                    TrStudReport.Visible = True
                    lblNote.Visible = True
                    'TrParent.Visible = False
                    TrCoach.Visible = True
                    tdSemCoach.Visible = True
                    tdSemCoachDr.Visible = True
                    'btnGradeCard_Parent.Visible = False

                    TdChild.Visible = False
                    Td_ddlChild.Visible = False

                    loadVolNames()
                    If (Request.QueryString("coachID") Is Nothing) Then
                    Else
                        Dim coachID As String = Request.QueryString("coachID").ToString()
                        Dim semester As String = Request.QueryString("Semester").ToString()
                        Dim SesionNo As String = Request.QueryString("SessionNo").ToString()
                        Dim ProductGroupid As String = Request.QueryString("pgId").ToString()
                        Dim ProductId As String = Request.QueryString("pId").ToString()
                        Dim Level As String = Request.QueryString("level").ToString()
                        ddlVolName.SelectedValue = coachID
                        LoadCoaches()
                        LoadProductGroup(False)
                        BindddlStudentID()
                        PopulateWeekNo()
                    End If



                    If ddlVolName.Items.Count = 1 Then
                        LoadCoaches()
                        LoadProductGroup(False)
                        BindddlStudentID()
                        PopulateWeekNo()
                    End If
                End If
            Catch ex As Exception
                'Response.Write(ex.ToString())
            End Try
        End If

    End Sub
    Public Sub loadVolNames()
        Dim TeamLead As String = ""
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(memberid) from dbo.[Volunteer] V where V.[RoleId]=" & Session("RoleID") & " and V.TeamLead='Y' and V.MemberID=" & Session("LoginID")) > 0 Then
            TeamLead = "Y"
        Else
            TeamLead = "N"
        End If

        If Session("RoleID") = 88 And TeamLead = "N" Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I inner join CoachReg CR On CR.CMemberID = I.AutoMemberID where CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.Semester='" + ddlSemester.SelectedValue + "' and CR.CMemberID=" & Session("LoginID") & "  order by I.FirstName,I.LastName")

            If ds.Tables(0).Rows.Count > 0 Then
                ddlVolName.DataSource = ds
                ddlVolName.DataBind()
                If (ds.Tables(0).Rows.Count = 1) Then
                    ddlVolName.Enabled = False
                Else
                    ddlVolName.Enabled = True
                End If

            Else
                lblScoreErr.Text = "Volunteer not present for Calendar Sign up for the selected year"
            End If
        ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89 Or (Session("RoleID") = 88 And TeamLead = "Y") Or Session("RoleID") = 96) Then 'Coach Admin can schedule for other Coaches. 

            ddlVolName.Visible = True
            ddlWeekNo.Visible = True
            '********* To display all Coaches ****************'
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CoachReg CR On CR.CMemberID = I.AutoMemberID Where  CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & " and CR.Semester='" + ddlSemester.SelectedValue + "' order by I.FirstName,I.LastName") ' V.RoleId in (88) order by I.LastName,I.FirstName")

            If ds.Tables(0).Rows.Count > 0 Then
                ddlVolName.DataSource = ds
                ddlVolName.DataBind()
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVolName.Items.Insert(0, New ListItem("Select Coach", -1))
                    ddlVolName.SelectedIndex = 0
                End If
            Else
                lblScoreErr.Text = "No Volunteers present for Calendar Sign up"
            End If

        Else
            ddlVolName.Visible = False
            ddlWeekNo.Visible = False
        End If
    End Sub

    Private Sub LoadCoaches()
        '**********To get Products assigned for Volunteers RoleIs =1,2,88,89****************'
        If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then 'All Products
            LoadProductGroup(False)
        ElseIf Session("RoleId").ToString() = "88" Or Session("RoleId").ToString() = "89" Then 'Team Lead='Y' gets Products From Volunteer Table
            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & ddlVolName.SelectedValue & " and RoleId in(88,89) and TeamLead='Y' and [National]='Y' and ProductId is not Null") > 1 Then
                'more than one 
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & ddlVolName.SelectedValue & " and RoleId in(88,89)" & " and ProductId is not Null ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup(False)
            Else 'TeamLead<>'Y' gets Products from CoachReg table
                'only one
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " Select ProductGroupID,ProductID from CoachReg where CMemberid=" & ddlVolName.SelectedValue & " and EventYear =" & ddlEventYear.SelectedValue & " and ProductId is not Null ") 'and RoleId=" & Session("RoleId") & "
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                'If ds.Tables(0).Rows.Count > 0 Then
                '    prd = ds.Tables(0).Rows(0)(1).ToString()
                '    Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                '    lblPrd.Text = prd
                '    lblPrdGrp.Text = Prdgrp
                'End If
                'LoadProductGroup(False)
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp
                LoadProductGroup(False)
            End If
        End If
    End Sub
    Private Sub LoadProductGroup(ByVal User As Boolean)
        Try
            Dim strSql As String
            If Session("User") = False Then
                strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN CoachReg CR ON CR.ProductGroupID = P.ProductGroupID AND CR.EventId = P.EventId where CR.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "")
                If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    strSql = strSql & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
                Else
                    strSql = strSql & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
                End If
                If ddlVolName.SelectedValue <> "-1" Then
                    strSql = strSql & " and CR.CMemberID=" & ddlVolName.SelectedValue & ""
                End If
                strSql = strSql & " and CR.Approved='Y'  order by P.ProductGroupID"
            ElseIf Session("User") = True Then
                Dim StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
                strSql = "SELECT Distinct P.[Name], P.ProductGroupID from dbo.[ProductGroup] P INNER JOIN dbo.[CoachReg] CR  ON P.ProductGroupid =CR.ProductGroupid "
                strSql = strSql & " Where CR.PMemberID =" & Session("CustIndID") & " and CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & "  and P.EventId =" & ddlEvent.SelectedValue
                If StudType = "Child" Then
                    strSql = strSql & " and CR.ChildNumber=" & ddlChild_Par.SelectedValue & " "
                ElseIf StudType = "Adult" Then
                    strSql = strSql & " and CR.AdultId=" & ddlChild_Par.SelectedValue & " "
                End If
                If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    strSql = strSql & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
                Else
                    strSql = strSql & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
                End If

            End If

            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblScoreErr.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, New ListItem("Select Product Group", -1))
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
                'LoadProductID()
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()

                'If (ddlProduct.SelectedValue <> "-1" And ddlLevel.SelectedValue <> "-1") Then
                '    PopulateWeekNo()
                'End If

            End If
        Catch ex As Exception
            lblScoreErr.Text = ex.ToString()
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ddlProduct.SelectedIndex = 0
                ddlProduct.Enabled = False
            Else
                Dim strSql As String
                'Volunteer
                If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                    strSql = "Select Distinct P.ProductID, P.Name from Product P INNER JOIN CoachReg CR ON CR.ProductID = P.ProductID AND CR.EventId = P.EventId  where CR.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & " AND P.ProductGroupID =" & ddlProductGroup.SelectedValue  'and P.Status='O'

                    If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                        strSql = strSql & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
                    Else
                        strSql = strSql & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
                    End If

                    If ddlVolName.SelectedValue <> "-1" Then
                        strSql = strSql & " AND CR.CMemberID=" & ddlVolName.SelectedValue & ""
                    End If
                    strSql = strSql & " and CR.Approved='Y' ORDER BY P.ProductID "
                ElseIf Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT" Then
                    Dim StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
                    strSql = "SELECT distinct P.[Name], P.ProductID from dbo.[Product] P INNER JOIN dbo.[CoachReg] CR ON P.ProductGroupid = CR.ProductGroupid and P.ProductID = CR.ProductID"
                    strSql = strSql & " Where CR.PMemberID =" & Session("CustIndID") & " and CR.Approved='Y' and CR.EventYear=" & ddlEventYear.SelectedValue & "  and P.EventId =" & ddlEvent.SelectedValue
                    strSql = strSql & " and P.ProductGroupId=" & ddlProductGroup.SelectedValue
                    If StudType = "Child" Then
                        strSql = strSql & " and CR.ChildNumber=" & ddlChild_Par.SelectedValue & " "
                    ElseIf StudType = "Adult" Then
                        strSql = strSql & " and CR.AdultId=" & ddlChild_Par.SelectedValue & " "
                    End If

                    If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                        strSql = strSql & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
                    Else
                        strSql = strSql & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
                    End If

                End If

                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlProduct.DataSource = drproductid
                ddlProduct.DataBind()
                If ddlProduct.Items.Count > 1 Then
                    ddlProduct.Items.Insert(0, New ListItem("Select Product", -1))
                    ddlProduct.Items(0).Selected = True
                    ddlProduct.Enabled = True
                ElseIf ddlProduct.Items.Count < 1 Then
                    ddlProduct.Enabled = False
                Else
                    ddlProduct.Enabled = False
                    LoadLevel(Session("User"))
                    'LoadSession(User)
                End If
            End If
        Catch ex As Exception
            lblScoreErr.Text = lblScoreErr.Text & "<br>" & ex.ToString
        End Try
    End Sub
    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False
            loadVolNames()
            ClearScoreGrids()
            ClearReportGrids()
            If ddlVolName.SelectedValue <> "-1" Then
                LoadCoaches()
                LoadProductGroup(False)
                BindddlStudentID()
            End If


            'If ddlVolName.Items.Count = 1 Then
            '    LoadCoaches()
            '    LoadProductGroup(False)
            '    BindddlStudentID()
            'End If
        End If



    End Sub

    Private Sub LoadLevel(ByVal User As Boolean) ', ByVal ProductGroupId As Integer)
        Try
            Dim StrSQL As String
            ddlLevel.Items.Clear()
            ddlLevel.Enabled = True
            If (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                Dim StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
                StrSQL = "SELECT DISTINCT Level FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and PMemberID = " & Session("CustIndId") & "  and  Approved ='Y' and Semester='" & ddlPSemester.SelectedValue & "'" 'and ProductGroupID=" & ddlProductGroup.SelectedValue
                If StudType = "Child" Then
                    StrSQL = StrSQL & " AND ChildNumber = " & ddlChild_Par.SelectedValue & ""
                ElseIf StudType = "Adult" Then
                    StrSQL = StrSQL & " AND AdultId = " & ddlChild_Par.SelectedValue & ""
                End If

            Else
                If ddlVolName.SelectedItem.Text.Contains("Select") Then
                    lblScoreErr.Text = " Please Select Coach"
                    Exit Sub
                End If
                StrSQL = "SELECT DISTINCT Level FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and CMemberID = " & ddlVolName.SelectedValue & " and  Approved ='Y' and Semester='" & ddlSemester.SelectedValue & "'" ' AND ChildNumber = " & ddlChild.SelectedValue & "  and ProductGroupID=" & ddlProductGroup.SelectedValue
            End If

            If Not ddlProductGroup.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductGroupID = " & ddlProductGroup.SelectedValue
            End If

            If Not ddlProduct.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductID = " & ddlProduct.SelectedValue
            End If

            'Response.Write(StrSQL)
            Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            If dsGrade.Tables(0).Rows.Count > 0 Then
                ddlLevel.DataSource = dsGrade
                ddlLevel.DataBind()
                ddlLevel.Enabled = True 'False
                If dsGrade.Tables(0).Rows.Count = 1 Then
                    ddlLevel.Enabled = False
                    LoadSession(Session("User"))
                End If
            End If
            If ddlLevel.Items.Count = 0 Then
                ddlLevel.SelectedValue = "-1"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadSession(ByVal User As Boolean)
        Try
            Dim StrSQL As String
            ddlSession.Items.Clear()
            ddlSession.Enabled = True

            If (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                Dim StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
                StrSQL = "SELECT DISTINCT SessionNo FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and PMemberID = " & Session("CustIndId") & " and  Approved ='Y' and Semester='" & ddlPSemester.SelectedValue & "'" 'and ProductGroupID=" & ddlProductGroup.SelectedValue
                If StudType = "Child" Then
                    StrSQL = StrSQL & " AND ChildNumber = " & ddlChild_Par.SelectedValue & ""
                ElseIf StudType = "Adult" Then
                    StrSQL = StrSQL & " AND AdultId = " & ddlChild_Par.SelectedValue & ""
                End If
            Else
                If ddlVolName.SelectedItem.Text.Contains("Select") Then
                    lblScoreErr.Text = " Please Select Coach"
                    Exit Sub
                Else
                    lblScoreErr.Text = ""
                    StrSQL = "SELECT DISTINCT SessionNo FROM [CoachReg] WHERE EventYear = " & ddlEventYear.SelectedValue & " and CMemberID = " & ddlVolName.SelectedValue & " and  Approved ='Y' and Semester='" & ddlSemester.SelectedValue & "'" ' " AND ChildNumber = " & ddlChild.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue
                End If
            End If
            If Not ddlProductGroup.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductGroupID = " & ddlProductGroup.SelectedValue
            End If

            If Not ddlProduct.SelectedItem.Text.Contains("Select") Then
                StrSQL = StrSQL & " AND ProductID = " & ddlProduct.SelectedValue
            End If

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlSession.DataSource = ds
                ddlSession.DataBind()
                ddlSession.Enabled = False
                If (ds.Tables(0).Rows.Count = 1) Then
                    PopulateWeekNo()
                End If
            End If
                If ddlSession.Items.Count = 0 Then
                ddlSession.SelectedValue = "-1"
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub Loadchild(ByVal ParentID As Integer, ByVal ChildNumber As Integer)
        Try
            lblScoreErr.Text = ""
            ''Dim strSql As String = "select Distinct C.First_name+' '+C.Last_name as ChildName, C.ChildNumber,C.Grade From Child C Inner Join ChildTestSummary CR on CR.MemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber where C.memberid=" ParentID & " and CR.EventYear=" & ddlEventYear.SelectedValue
            Dim strSql As String = "select Distinct case when CR.AdultId is null then C.[FIRST_NAME] + ' ' + C.[MIDDLE_INITIAL] + ' ' + C.[LAST_NAME] +'-'+'Child' else IP.FirstName +' '+ IP.LastName +'-'+'Adult' end AS ChildName, case when CR.AdultId is null then C.[ChildNumber] else CR.AdultId end as ChildNumber  from CoachReg CR  left Join [Child] C on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) where CR.Pmemberid=" & ParentID & " and CR.EventYear=" & ddlEventYear.SelectedValue & ""

            If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                strSql = strSql & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
            Else
                strSql = strSql & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
            End If

            If ChildNumber > 0 Then
                strSql = strSql & " and C.ChildNumber=" & ChildNumber
            End If
            'strSql = strSql & " and CR.Completed='Y' and CR.EventYear =Year(GetDATE())"
            strSql = strSql & " and CR.Approved='Y' and CR.EventYear =" & ddlEventYear.SelectedValue & " order by ChildName" 'Year(GetDATE())"

            Dim dsChild As DataSet
            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strSql)
            TdChild.Visible = True
            Td_ddlChild.Visible = True

            ddlChild_Par.DataSource = dsChild
            ddlChild_Par.DataBind()
            ddlChild_Par.Enabled = True


            If (ddlChild_Par.Items.Count < 1) Then
                ddlChild_Par.SelectedValue = "-1"
                lblScoreErr.Text = "Child(ren) has/have not yet taken the test."
                Btn_Submit.Enabled = False
                DGCoachPapers.DataSource = Nothing
                DGCoachPapers.DataBind()
            ElseIf (ddlChild_Par.Items.Count > 1) Then
                Btn_Submit.Enabled = True
                ddlChild_Par.Items.Insert(0, New ListItem("Select", "0"))
                ddlChild_Par.Items(0).Selected = True
                ddlChild_Par.Enabled = True
            Else
                Btn_Submit.Enabled = True
                ddlChild_Par.Enabled = False
                LoadProductGroup(True)
                If (ddlProductGroup.SelectedValue <> "-1") Then

                End If

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    Protected Sub BindddlStudentID()
        Try
            Dim ds As DataSet = New DataSet()
            Dim StrSQL As String = " SELECT distinct case when CR.AdultId is null then C.[FIRST_NAME] + ' ' + C.[MIDDLE_INITIAL] + ' ' + C.[LAST_NAME] +'-'+'Child' else IP.FirstName +' '+ IP.LastName +'-'+'Adult' end AS ChildName, case when CR.AdultId is null then C.[ChildNumber] else CR.AdultId end as ChildNumber  from CoachReg CR  left Join [Child] C on CR.PMemberID =C.MEMBERID and CR.ChildNumber =C.ChildNumber left join Indspouse IP on (IP.AutoMemberId=CR.AdultId) WHERE  CR.Approved='Y' and CR.EventYear =" & ddlEventYear.SelectedValue & ""

            If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                StrSQL = StrSQL & " and CR.Semester='" & ddlPSemester.SelectedValue & "'"
            Else
                StrSQL = StrSQL & " and CR.Semester='" & ddlSemester.SelectedValue & "'"
            End If

            If ddlVolName.SelectedValue <> "" Then
                StrSQL = StrSQL & " and CR.CMemberID=" & ddlVolName.SelectedValue & ""
            End If

            If ddlProductGroup.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.ProductGroupID =" & ddlProductGroup.SelectedValue
            End If
            If ddlProduct.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.ProductID =" & ddlProduct.SelectedValue
            End If
            If ddlLevel.SelectedItem.Text <> "-1" Then
                StrSQL = StrSQL & " and CR.Level ='" & ddlLevel.SelectedItem.Text & "'"
            End If
            If ddlSession.SelectedValue > 0 Then
                StrSQL = StrSQL & " and CR.SessionNo =" & ddlSession.SelectedValue
            End If

            StrSQL = StrSQL & " order by ChildName"

            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlChild.DataSource = ds
                ddlChild.DataBind()
                Dim StrCChildNumber As String = String.Empty
                For Each dr As DataRow In ds.Tables(0).Rows
                    StrCChildNumber = StrCChildNumber + dr("ChildNumber").ToString() + ","
                Next
                StrCChildNumber = StrCChildNumber.TrimEnd(",")
                hdnChildNumbers.Value = StrCChildNumber
            End If
            If ds.Tables(0).Rows.Count > 1 Then
                ddlChild.Items.Insert(0, New ListItem("Select Child", "-1"))
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ViewGrid()
        Try
            ClearScoreGrids()
            If lblScoreErr.Text <> "" Then
                DisableGrids()
                Exit Sub
            Else
                LoadCoachPapers()
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub DGCoachPapers_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)

        Dim CoachPaperID As Integer = Convert.ToInt32(e.Item.Cells(2).Text)
        Session("CoachPaperID") = CoachPaperID
        For i As Integer = 0 To DGCoachPapers.Items.Count - 1
            DGCoachPapers.Items(i).BackColor = Color.White
        Next
        If e.CommandName = "Select" And (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            e.Item.BackColor = Color.Gainsboro
            GetScores(CoachPaperID, True)

            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "SetButtonScroll();", True)
        Else
            e.Item.BackColor = Color.Gainsboro
            ClearReportGrids()
            ClearScoreGrids()
            BtnMissedAnswers.Enabled = True
            BtnHWSubmission.Enabled = True
            BtnViewSCores.Enabled = True
            BtnGradeCard.Enabled = True
            BtnCalcualteScores.Enabled = True
        End If
    End Sub
    Protected Sub DGCoachPapers_ItemDataBound(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)


    End Sub

    Protected Sub Dg_HwSubmit_ItemDataBound(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs)

        If (e.Item.ItemType = DataControlRowType.Header) Then
            e.Item.Cells(5).Text = "Student score <br> Out of " & hdnTotalSMarks.Value & ""
        End If
    End Sub
    Private Sub LoadCoachPapers()
        Try
            Dim sqlCommand As String = "usp_GetChildAnswerSheet"
            Dim param(10) As SqlParameter
            param(0) = New SqlParameter("@PaperType", ddlPaperType.SelectedValue)
            param(1) = New SqlParameter("@ProductGroupID", ddlProductGroup.SelectedValue)
            param(2) = New SqlParameter("@ProductID", ddlProduct.SelectedValue)
            param(3) = New SqlParameter("@PMemberId", IIf(Convert.ToInt32(Session("CustIndID")) > 0, Convert.ToInt32(Session("CustIndID")), DBNull.Value))
            param(4) = New SqlParameter("@Level", ddlLevel.SelectedItem.Text)
            param(5) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
            param(6) = New SqlParameter("@ChildId", IIf(Td_ddlChild.Visible = True, IIf(ddlChild_Par.SelectedValue > 0, ddlChild_Par.SelectedValue, DBNull.Value), IIf(ddlChild.SelectedValue > 0, ddlChild.SelectedValue, DBNull.Value)))
            param(7) = New SqlParameter("@MemberID", ddlVolName.SelectedValue)
            param(8) = New SqlParameter("@Semester", ddlSemester.SelectedValue)
            param(9) = New SqlParameter("@WeekId", ddlWeekNo.SelectedItem.Text)

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)

            If ds.Tables(0).Rows.Count > 0 Then
                lblCoachPapers.Visible = True
                DGCoachPapers.DataSource = ds
                DGCoachPapers.DataBind()
                If (ds.Tables(0).Rows.Count = 1 And ddlWeekNo.SelectedItem.Text <> "0") Then
                    Session("CoachPaperID") = ds.Tables(0).Rows(0)("CoachPaperId").ToString()
                    BtnMissedAnswers.Enabled = True
                    BtnHWSubmission.Enabled = True
                    BtnViewSCores.Enabled = True
                    BtnGradeCard.Enabled = True
                    BtnCalcualteScores.Enabled = True
                Else
                    BtnMissedAnswers.Enabled = False
                    BtnHWSubmission.Enabled = False
                    BtnViewSCores.Enabled = False
                    BtnGradeCard.Enabled = False
                    BtnCalcualteScores.Enabled = False
                End If

            Else
                lblCoachPapers.Visible = False
                DGCoachPapers.DataSource = Nothing
                DGCoachPapers.DataBind()
                lblScoreErr.Text = "Test Papers not yet released"
                Exit Sub
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub DisableGrids()
        GVAnswers.Visible = False
        GVTotalScore.Visible = False
        GVScaledScore.Visible = False
    End Sub
    Private Sub ClearScoreGrids()
        TrAnsExp.Visible = False
        lblScoreErr.Text = ""
        lblScores.Visible = False
        GVTotalScore.Visible = False
        GVTotalScore.DataSource = Nothing
        GVTotalScore.DataBind()

        GVAnswers.Visible = False
        GVAnswers.DataSource = Nothing
        GVAnswers.DataBind()
        btnGVAnsUpdate.Visible = False
        lblCScoreErr.Text = ""

        GVScaledScore.Visible = False
        GVScaledScore.DataSource = Nothing
        GVScaledScore.DataBind()
    End Sub
    Private Sub ClearReportGrids()
        lblMissedAns.Visible = False
        TrSort.Visible = False
        DGMissedAns.DataSource = Nothing
        DGMissedAns.DataBind()

        'Dg_HwSubmit.Visible = False
        lblHW_Sub.Visible = False
        Dg_HwSubmit.DataSource = Nothing
        Dg_HwSubmit.DataBind()

    End Sub
    Private Sub GetScores(ByVal CoachPaperID As Integer, ByVal ParentFlag As Boolean)

        Try
            lblPrdlevelWeek.Text = ""
            '  lblErrorMsg.Text = ""
            lblCPPrdlevelWeek.Text = ""
            Dim flag As Boolean = False
            Dim dsScale As DataSet
            lblScoreErr.Text = ""

            '******Child Test Answers for each Question******************************************************'
            Dim StrSQL As String
            Dim StudType = String.Empty
            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                StudType = ddlChild.SelectedItem.Text.Split("-")(1)
            Else
                StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
            End If
            If (StudType.Trim() = "Child") Then
                StrSQL = " Select Distinct Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,Ch.Answer,SA.CorrectAnswer,Ch.Score,Ch.CScore as CscorePar,Ch.CScore,IsNull(SA.Manual,'') as Manual From"
                StrSQL = StrSQL + " ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID and SA.SectionNumber =Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
                StrSQL = StrSQL + " Inner join ChildTestSummary CS on CS.CoachPaperId= Ch.CoachPaperID and CS.ChildNumber =Ch.ChildNumber  and CS.MemberID =Ch.MemberID"
                StrSQL = StrSQL + " WHERE Ch.ChildNumber=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperID & " and CS.Completed='Y'" 'and Ch.Score is not null"
            ElseIf StudType = "Adult" Then
                StrSQL = " Select Distinct Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,Ch.Answer,SA.CorrectAnswer,Ch.Score,Ch.CScore as CscorePar,Ch.CScore,IsNull(SA.Manual,'') as Manual From"
                StrSQL = StrSQL + " ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID and SA.SectionNumber =Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
                StrSQL = StrSQL + " Inner join ChildTestSummary CS on CS.CoachPaperId= Ch.CoachPaperID and CS.AdultId =Ch.AdultId  and CS.MemberID =Ch.MemberID"
                StrSQL = StrSQL + " WHERE Ch.AdultId=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperID & " and CS.Completed='Y'" 'and Ch.Score is not null"
            End If


            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL) '
            '"Select Distinct CT.CoachPaperId,CT.SectionNumber,CT.QuestionNumber,CT.Answer,CT.Score from ChildTestAnswers CT  WHERE CT.ChildNumber=" & ddlChild.SelectedValue & " and CT.EventYear=" & Now.Year & " and CT.CoachPaperID=" & CoachPaperID & " and CT.Score is not null")
            Dim ProductCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select distinct productcode from Product where ProductId=" & ddlProduct.SelectedValue & "")


            If ds.Tables(0).Rows.Count > 0 Then
                'BtnExport.Visible = True
                lblScores.Visible = True
                GVAnswers.Visible = True
                GVAnswers.DataSource = ds
                GVAnswers.DataBind()
                lblAnsPrdLevelWeek.Text = ProductCode & " - " & ddlLevel.SelectedValue & " - " & " Week#" & ddlWeekNo.SelectedItem.Text
                Dim i As Integer = 0
                Dim Section As Integer = 1
                For i = 0 To GVAnswers.Rows.Count - 1
                    Dim iSection As Integer = GVAnswers.Rows(i).Cells(1).Text

                    If (iSection Mod 2 = 0) Then
                        GVAnswers.Rows(i).BackColor = Color.Gainsboro
                    End If

                    Section = GVAnswers.Rows(i).Cells(1).Text
                Next

                For i = 0 To GVAnswers.Rows.Count - 1
                    Dim cscore As String = String.Empty
                    cscore = TryCast(DirectCast(GVAnswers.Rows(i).FindControl("txtCScore"), TextBox), TextBox).Text
                    If (cscore = "1") Then
                        TryCast(DirectCast(GVAnswers.Rows(i).FindControl("txtManual"), TextBox), TextBox).Text = cscore
                        ' GVAnswers.Rows(i).Cells(4).Text = cscore
                        GVAnswers.Rows(i).Cells(7).BackColor = Color.FromName("#99cc00")
                    End If
                    If (Session("entryToken").ToString().ToUpper() <> "VOLUNTEER") Then
                        TryCast(DirectCast(GVAnswers.Rows(i).FindControl("txtManual"), TextBox), TextBox).Enabled = False
                    End If


                Next


                If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then '"PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    btnGVAnsUpdate.Visible = True
                    'GVAnswers.Columns(6).Visible = False
                    ' GVAnswers.Columns(6).Visible = False
                Else
                    btnGVAnsUpdate.Visible = False
                    ' GVAnswers.Columns(7).Visible = False
                    ' GVAnswers.Columns(8).Visible = False
                End If
            Else
                lblScores.Visible = False
                GVAnswers.Visible = False
                GVAnswers.DataSource = Nothing
                GVAnswers.DataBind()
                lblScoreErr.Text = " Child has not yet completed the test."

                StrSQL = " Select Distinct CoachPaperId,SectionNumber,QuestionNumber,'' as Answer,CorrectAnswer,'' as Score,'' as CScore , '' as CscorePar,'' as CScore,IsNull(Manual,'') as Manual from TestAnswerKey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID=" & CoachPaperID & ""
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                If ds.Tables(0).Rows.Count > 0 Then

                Else
                    lblScoreErr.Text = " No answer key available for the selected coach paper."
                End If

            End If

            Try


                If (ds.Tables(0).Rows.Count <= 0 And (Session("entryToken").ToString().ToUpper() = "PARENT")) Then
                    StrSQL = " Select Distinct CoachPaperId,SectionNumber,QuestionNumber,'' as Answer,CorrectAnswer,'' as Score,'' as CScore , '' as CscorePar,'' as CScore,IsNull(Manual,'') as Manual from TestAnswerKey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID=" & CoachPaperID & ""
                    ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                    If ds.Tables(0).Rows.Count > 0 Then
                        lblErrorMsg.Visible = False
                        'BtnExport.Visible = True
                        lblScores.Visible = True
                        GVAnswers.Visible = True
                        GVAnswers.DataSource = ds
                        GVAnswers.DataBind()
                        If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then '"PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                            btnGVAnsUpdate.Visible = True
                            GVAnswers.Columns(6).Visible = False
                        Else
                            btnGVAnsUpdate.Visible = False
                            GVAnswers.Columns(7).Visible = False
                            GVAnswers.Columns(8).Visible = False
                        End If
                    Else
                        lblErrorMsg.Visible = True
                        lblScoreErr.Text = ""
                    End If

                End If
            Catch ex As Exception

            End Try
            '****************************Raw Scores from Child Test Summary************************************************************'
            Dim StrTot As String = "Select Distinct CT.CoachPaperID,CT.S1Score,CT.S2Score,CT.S3Score,CT.S4Score,CT.S5Score,CT.WSRawScore,CT.CRRawScore,CT.MathRawScore,CT.TotalRawScore from ChildTestSummary CT  WHERE CT.ChildNumber=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and CT.EventYear=" & ddlEventYear.SelectedValue & " and CT.CoachPaperID=" & CoachPaperID 'Now.Year
            If StudType = "Adult" Then
                StrTot = "Select Distinct CT.CoachPaperID,CT.S1Score,CT.S2Score,CT.S3Score,CT.S4Score,CT.S5Score,CT.WSRawScore,CT.CRRawScore,CT.MathRawScore,CT.TotalRawScore from ChildTestSummary CT  WHERE CT.Adultid=" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & " and CT.EventYear=" & ddlEventYear.SelectedValue & " and CT.CoachPaperID=" & CoachPaperID 'Now.Year
            End If
            Dim dsTotScore As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrTot)
            If dsTotScore.Tables(0).Rows.Count > 0 Then
                'BtnExport.Visible = True
                GVTotalScore.Visible = True
                GVTotalScore.DataSource = dsTotScore
                GVTotalScore.DataBind()
            Else
                GVTotalScore.Visible = False
                GVTotalScore.DataSource = Nothing
                GVTotalScore.DataBind()

                'GVAnswers.Visible = False
                'GVAnswers.DataSource = Nothing
                'GVAnswers.DataBind()

                lblScoreErr.Text = " Child has not yet completed the test."
                'lblscoreerr.Text = "No Child Scores present to show"
            End If

            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                If lblScoreErr.Text <> "" And SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CoachRelDates where CoachPaperID=" & CoachPaperID & " and GETDATE()> QDeadlineDate and AReleaseDate < GETDATE() and MemberID =" & ddlVolName.SelectedValue) > 0 Then
                    Session("CoachPaperID") = CoachPaperID
                    'lblScoreErr.Text = ""
                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from EnterAnsException where CoachPaperID =" & Session("CoachPaperID") & " and MemberID=" & ddlVolName.SelectedValue & " and Childnumber=" & ddlChild.SelectedValue) > 0 Then
                        lblScoreErr.Text = "Child has not submitted the answers."
                        TrAnsExp.Visible = False
                    Else
                        lblScoreErr.Text = ""
                        TrAnsExp.Visible = True
                    End If
                Else
                    TrAnsExp.Visible = False
                End If
            End If

            '*****************Scaled Scores From Child Test Summary*****************************'
            Dim Scaletext As String = String.Empty
            Scaletext = "SELECT CoachPaperID,WSScaledScore,CRScaledScore,MathScaledScore FROM ChildTestSummary WHERE ChildNumber =" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & "  AND EventYear=" & ddlEventYear.SelectedValue & " and TotalScaledScore is not null and CoachPaperID=" & CoachPaperID & ""
            If (StudType = "Adult") Then
                Scaletext = "SELECT CoachPaperID,WSScaledScore,CRScaledScore,MathScaledScore FROM ChildTestSummary WHERE Adultid =" & IIf(ParentFlag = True, ddlChild_Par.SelectedValue, ddlChild.SelectedValue) & "  AND EventYear=" & ddlEventYear.SelectedValue & " and TotalScaledScore is not null and CoachPaperID=" & CoachPaperID & ""
            End If
            dsScale = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Scaletext)
            If dsScale.Tables(0).Rows.Count > 0 Then
                GVScaledScore.Visible = True
                GVScaledScore.DataSource = dsScale
                GVScaledScore.DataBind()
            Else
                GVScaledScore.Visible = False
                GVScaledScore.DataSource = Nothing
                GVScaledScore.DataBind()
            End If

            'BtnExport.Visible = True
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            lblScoreErr.Text = ""
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False

            ClearScoreGrids()
            ClearReportGrids()

            If ddlProductGroup.SelectedIndex <> 0 Then
                LoadProductID()
                LoadLevel(Session("User"))
                LoadSession(Session("User"))
                'If ddlProductGroup.SelectedValue <> "-1" And ddlProduct.SelectedValue <> "-1" And ddlLevel.SelectedValue <> "-1" Then
                '    PopulateWeekNo()
                '    'LoadGrid()
                'End If
            End If
            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                BindddlStudentID()
            End If
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try


            lblScoreErr.Text = ""
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False

            ClearScoreGrids()
            ClearReportGrids()

            If ddlProduct.SelectedValue = "Select Product" Then
                lblScoreErr.Text = "Please select Valid Product"
            Else
                LoadLevel(Session("User"))
                'If ddlProductGroup.SelectedValue <> "-1" And ddlProduct.SelectedValue <> "-1" And ddlLevel.SelectedValue <> "-1" Then
                '    PopulateWeekNo()
                '    'LoadGrid()
                'End If

                'LoadSession(User)
                'LoadGrid()
            End If

            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" Then
                BindddlStudentID()
            End If
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub ddlSession_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        PopulateWeekNo()

    End Sub

    Protected Sub ddlChild_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Clear()
        ClearScoreGrids()
        ClearReportGrids()

        If Session("entryToken").ToString().ToUpper() = "PARENT" Then
            LoadProductGroup(Session("User"))
            If (ddlProductGroup.SelectedValue <> "0" And ddlProduct.SelectedValue <> "-1" And ddlLevel.SelectedValue <> "-1") Then
                PopulateWeekNo()
            End If
        End If
        'Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Grade From child where childNumber=" & ddlChild.SelectedValue)
        'If dsGrade.Tables(0).Rows.Count > 0 Then

        '    'hdnChGrade.Value = dsGrade.Tables(0).Rows(0)("Grade").ToString()
        'End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        DGCoachPapers.DataSource = Nothing
        DGCoachPapers.DataBind()
        lblCoachPapers.Visible = False

        ClearScoreGrids()
        ClearReportGrids()
        If ddlVolName.SelectedValue <> "-1" Then
            LoadCoaches()
            LoadProductGroup(False)
            BindddlStudentID()
            PopulateWeekNo()
        End If
    End Sub

    Protected Sub Btn_Submit_Click(ByVal sender As Object, ByVal e As EventArgs)
        ClearScoreGrids()
        ClearReportGrids()
        'modified by sims for populating child on click of submit button
        '   BindddlStudentID()
        Try
            If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" And ddlVolName.SelectedValue = "-1" Then 'And Session("User") = False Then
                lblScoreErr.Text = "Please Select Coach"
                Exit Sub
            ElseIf Session("entryToken").ToString().ToUpper() = "PARENT" And ddlChild_Par.SelectedItem.Value = "-1" Then '.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select Child"
                Exit Sub
            End If

            If ddlPaperType.SelectedValue = "-1" Then '.SelectedItem.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select PaperType"
            ElseIf ddlProductGroup.SelectedValue <= 0 Then
                lblScoreErr.Text = "Please select Product Group"
            ElseIf ddlProduct.SelectedValue <= 0 Then '.SelectedItem.Text.Contains("Select") Then
                lblScoreErr.Text = "Please select Product"
            ElseIf ddlWeekNo.Text = "0" Then
                lblScoreErr.Text = "Please select Week#"
            Else
                lblScoreErr.Text = ""
            End If
            If lblScoreErr.Text <> "" Then
                Exit Sub
            Else
                btnGradeCard_Parent.Enabled = True
                If (Session("entryToken").ToString().ToUpper() = "PARENT" Or Session("entryToken").ToString().ToUpper() = "STUDENT") Then
                    ' ViewGrid()
                    Table1.Visible = False
                    GetScores(Session("CoachPaperID"), True)
                Else
                    ViewGrid()
                    Table1.Visible = False
                    GetClassPerformance("CorrectCount")
                End If

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Function ValidateViewScores() As Integer

        Dim RetVal As Integer = 1
        If Session("entryToken").ToString().ToUpper() = "VOLUNTEER" And ddlVolName.SelectedValue = "-1" Then 'And Session("User") = False Then
            lblScoreErr.Text = "Please Select Coach"
            RetVal = -1
        ElseIf Session("entryToken").ToString().ToUpper() = "PARENT" And ddlChild_Par.SelectedItem.Value = "-1" Then '.Text.Contains("Select") Then
            lblScoreErr.Text = "Please select Child"
            RetVal = -1

        End If

        If ddlPaperType.SelectedValue = "-1" Then '.SelectedItem.Text.Contains("Select") Then
            lblScoreErr.Text = "Please select PaperType"
            RetVal = -1
        ElseIf ddlProductGroup.SelectedValue <= 0 Then
            lblScoreErr.Text = "Please select Product Group"
            RetVal = -1
        ElseIf ddlProduct.SelectedValue <= 0 Then '.SelectedItem.Text.Contains("Select") Then
            lblScoreErr.Text = "Please select Product"
            RetVal = -1
        ElseIf ddlWeekNo.Text = "0" Then
            lblScoreErr.Text = "Please select Week#"
            RetVal = -1
        Else
            lblScoreErr.Text = ""
        End If

        Return RetVal

    End Function

    Protected Sub BtnMissedAnswers_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblPrdlevelWeek.Text = ""
        lblAnsPrdLevelWeek.Text = ""
        If (ValidateViewScores() = 1) Then
            GetClassPerformance("CorrectCount")
        End If

    End Sub
    Protected Sub GetClassPerformance(ByVal Sort As String)
        Try
            ddlChild.Enabled = False
            ClearScoreGrids()
            ClearReportGrids()
            Dim StudType As String = String.Empty
            '  StudType = ddlChild.SelectedItem.Text.Split("-")(1)
            ' GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then
                Dim Cmdtext As String = String.Empty
                Cmdtext = "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and ((CR.ChildNumber =C.Childnumber) or (CR.AdultId=C.AdultId)) where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'"
                'If (StudType = "Adult") Then
                '    Cmdtext = "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.AdultId =C.AdultId where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'"
                'End If
                'Check if ChildTestAnswers has data--If child has completed the test
                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, Cmdtext)) = 0 Then
                    lblScoreErr.Text = "No student has taken the test."
                    Exit Sub
                End If



                Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1 and childnumber in (" & hdnChildNumbers.Value & ")) as CorrectCount,"
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1 and childnumber in (" & hdnChildNumbers.Value & ")) as IncorrectCount,"
                StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null and childnumber in (" & hdnChildNumbers.Value & ")) as TotalCount"
                StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
                StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
                StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
                '  Response.Write(StrSQL)


                StrSQL = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID  and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber asc"


                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                ds.Tables(0).Columns.Add("CorrectCount")
                ds.Tables(0).Columns.Add("InCorrectcount")
                ds.Tables(0).Columns.Add("TotalCount")

                Dim StrChilAnswers As String = String.Empty
                StrChilAnswers = "select * from childtestanswers where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperID=" & CoachPaperId & " and ((childnumber in (" & hdnChildNumbers.Value & "))or (AdultId in(" & hdnChildNumbers.Value & "))) order by QuestionNumber"
                Dim j As Integer = 0
                Dim dsAnswers As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrChilAnswers)
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        Dim correctCount As Integer = 0
                        Dim InCorrectCount As Integer = 0
                        Dim TotalCount As Integer = 0
                        For Each dr1 As DataRow In dsAnswers.Tables(0).Rows
                            If (dr("QuestionNumber").ToString() = dr1("QuestionNumber").ToString() And dr("SectionNumber").ToString() = dr1("SectionNumber").ToString()) Then

                                If (dr1("Score").ToString() = "1") Then
                                    correctCount = correctCount + 1
                                End If

                                If (dr1("Score").ToString() <> "1") Then
                                    InCorrectCount = InCorrectCount + 1
                                End If

                                If (dr1("Score").ToString() <> "") Then
                                    TotalCount = TotalCount + 1
                                End If
                                ds.Tables(0).Rows(j)("CorrectCount") = correctCount
                                ds.Tables(0).Rows(j)("InCorrectcount") = InCorrectCount
                                ds.Tables(0).Rows(j)("TotalCount") = TotalCount
                            End If
                        Next
                        j = j + 1
                    Next
                End If
                ds.AcceptChanges()
                'Dim cn As SqlConnection = New SqlConnection(System.Configuration.ConfigurationManager.AppSettings("DBConnection"))
                'cn.Open()
                'Dim cmd As SqlCommand = New SqlCommand(StrSQL, cn)

                'cmd.CommandTimeout = "600"
                'Dim da As SqlDataAdapter = New SqlDataAdapter(cmd)
                'Dim ds As DataSet = New DataSet()
                'da.Fill(ds)

                ds.Tables(0).DefaultView.Sort = "SectionNumber, " + Sort + " asc"
                Dim dt As DataTable = ds.Tables(0).DefaultView.ToTable()
                'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                Dim ProductCode As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " select distinct productcode from Product where ProductId=" & ddlProduct.SelectedValue & "")

                If ds.Tables(0).Rows.Count > 0 Then
                    DGMissedAns.DataSource = dt
                    DGMissedAns.DataBind()
                    lblCPPrdlevelWeek.Text = ProductCode & " - " & ddlLevel.SelectedItem.Text & " - Week#" & ddlWeekNo.SelectedItem.Text
                    lblMissedAns.Visible = True
                    ddlChild.Visible = True
                    TrSort.Visible = True
                    Dim i As Integer = 0
                    Dim Section As Integer = 1
                    For i = 0 To DGMissedAns.Items.Count
                        Dim iSection As Integer = DGMissedAns.Items(i).Cells(1).Text

                        If (iSection Mod 2 = 0) Then
                            DGMissedAns.Items(i).BackColor = Color.Gainsboro
                        End If

                        Section = DGMissedAns.Items(i).Cells(1).Text
                    Next
                Else
                    lblScoreErr.Text = "No student has taken the test."
                    TrSort.Visible = False
                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnHWSubmission_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHWSubmission.Click
        If (ValidateViewScores() = 1) Then
            GetHomeworkSubmissions("Yes")
        End If
    End Sub
    Protected Sub GetHomeworkSubmissions(ByVal IsHide As String)
        Try
            ddlChild.Enabled = False
            If (IsHide = "Yes") Then
                ClearScoreGrids()
                ClearReportGrids()

            End If
            Dim StudType As String = String.Empty
            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                StudType = ddlChild.SelectedItem.Text.Split("-")(1)
            Else
                StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
            End If
            lblCPPrdlevelWeek.Text = ""
            lblAnsPrdLevelWeek.Text = ""
            GetCoachPaperID()
            Dim CoachPaperId As Integer
            CoachPaperId = Session("CoachPaperID")
            'If hdnCoachPaperID.Value <> "" Then
            '    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            'End If

            If CoachPaperId > 0 Then
                Dim StrSQL As String = ""
                '"Select Distinct CT.EventYear ,CT.CoachPaperID ,C.ChildNumber,C.FIRST_NAME + '' + C.LAST_NAME as StudentName,CT.Completed from Child C Inner join ChildTestSummary CT on C.ChildNumber = Ct.ChildNumber and C.MEMBERID =CT.MemberID "
                'StrSQL = StrSQL & " where CT.EventYear =" & ddlEventYear.SelectedValue & " And CT.CoachPaperID = " & CoachPaperId
                Dim Level As String
                If ddlProductGroup.SelectedItem.Text = "SAT" Or ddlProductGroup.SelectedItem.Text = "Math" Then
                    Level = ddlLevel.SelectedItem.Text
                Else
                    Level = ""
                End If
                Dim Cmdtext As String = String.Empty
                If (StudType = "Child") Then
                    Cmdtext = "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'"
                ElseIf (StudType = "Adult") Then
                    Cmdtext = "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and (CR.AdultId =C.AdultId or CR.ChildNumber=C.ChildNumber) where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'"
                End If

                If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, Cmdtext)) = 0 Then
                    lblScoreErr.Text = "No child has taken the test."
                    Exit Sub
                End If
                If (StudType = "Child") Then
                    StrSQL = StrSQL & " Select Distinct CR.EventYear," & CoachPaperId & " as CoachPaperID ,C.ChildNumber,C.FIRST_NAME as FirstName, C.LAST_NAME as LastName,C.FIRST_NAME + ' ' + C.LAST_NAME as StudentName,CR.SessionNo,(Select Distinct Completed from ChildTestSummary where CoachPaperID =" & CoachPaperId & " and MemberID =CR.PMemberID and ChildNumber=CR.ChildNumber and EventYear =" & ddlEventYear.SelectedValue & ") as Completed,(select TotalRawScore from ChildTestSummary where ChildNumber=CR.ChildNumber and EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as Score, (select sum(cscore) from ChildTestAnswers where ChildNumber=CR.ChildNumber and EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as CScore,(select count(*) from testanswerkey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as MaxScore,CR.CMemberID, cr.productcode, cr.Level,cp.WeekId "
                    StrSQL = StrSQL & " from  CoachReg CR Inner join Child C on CR.ChildNumber = C.ChildNumber and CR.PMemberID =C.MEMBERID "
                    ' StrSQL = StrSQL & " Left join ChildTestSummary CT on CR.ChildNumber = CT.ChildNumber and CR.PMEMBERID =CT.MemberID and CR.EventYear =CT.EventYear"
                    StrSQL = StrSQL & " inner join COachPapers CP on(cp.COachPaperId=" & CoachPaperId & ") where CR.Approved='Y' and  CR.EventYear =" & ddlEventYear.SelectedValue & " and CR.CMemberID = " & ddlVolName.SelectedValue
                    StrSQL = StrSQL & " and CR.ProductID=" & ddlProduct.SelectedValue & " and CR.Level='" & ddlLevel.SelectedItem.Text & "' and CR.SessionNo=" & ddlSession.SelectedValue & " Order By Completed Desc,FirstName,LastName Asc "
                ElseIf (StudType = "Adult") Then
                    StrSQL = StrSQL & " Select Distinct CR.EventYear," & CoachPaperId & " as CoachPaperID ,CR.AdultId as ChildNumber,IP.FIRSTNAME as FirstName, IP.LASTNAME as LastName,IP.FIRSTNAME + ' ' + IP.LASTNAME as StudentName,CR.SessionNo,(Select Distinct Completed from ChildTestSummary where CoachPaperID =" & CoachPaperId & " and MemberID =CR.PMemberID and AdultId=CR.AdultId and EventYear =" & ddlEventYear.SelectedValue & ") as Completed,(select TotalRawScore from ChildTestSummary where AdultId=CR.AdultId and EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as Score, (select sum(cscore) from ChildTestAnswers where AdultId=CR.AdultId and EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as CScore,(select count(*) from testanswerkey where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & CoachPaperId & ") as MaxScore,CR.CMemberID, cr.productcode, cr.Level,cp.WeekId "
                    StrSQL = StrSQL & " from  CoachReg CR Inner join Indspouse IP on CR.AdultId = IP.AutoMemberId "
                    ' StrSQL = StrSQL & " Left join ChildTestSummary CT on CR.ChildNumber = CT.ChildNumber and CR.PMEMBERID =CT.MemberID and CR.EventYear =CT.EventYear"
                    StrSQL = StrSQL & " inner join COachPapers CP on(cp.COachPaperId=" & CoachPaperId & ") where CR.Approved='Y' and  CR.EventYear =" & ddlEventYear.SelectedValue & " and CR.CMemberID = " & ddlVolName.SelectedValue
                    StrSQL = StrSQL & " and CR.ProductID=" & ddlProduct.SelectedValue & " and CR.Level='" & ddlLevel.SelectedItem.Text & "' and CR.SessionNo=" & ddlSession.SelectedValue & " Order By Completed Desc,FirstName,LastName Asc "
                End If


                'Response.Write(StrSQL & "<br />")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)
                If ds.Tables(0).Rows.Count > 0 Then
                    hdnTotalSMarks.Value = ds.Tables(0).Rows(0)("MaxScore").ToString()
                    Dg_HwSubmit.DataSource = ds
                    Dg_HwSubmit.DataBind()
                    lblHW_Sub.Visible = True
                    Dg_HwSubmit.Columns(5).HeaderText = "student's score <br> Out of " & ds.Tables(0).Rows(0)("MaxScore").ToString() & ""
                    lblPrdlevelWeek.Text = ds.Tables(0).Rows(0)("ProductCode").ToString() & " - " & ds.Tables(0).Rows(0)("level").ToString() & " - Week#" & ds.Tables(0).Rows(0)("WeekId").ToString()

                    For i = 0 To Dg_HwSubmit.Items.Count - 1
                        Dim cscore As Integer = 0
                        If (Dg_HwSubmit.Items(i).Cells(6).Text <> "" And Dg_HwSubmit.Items(i).Cells(6).Text <> "&nbsp;") Then
                            cscore = Convert.ToInt32(Dg_HwSubmit.Items(i).Cells(6).Text)
                        End If

                        If (cscore > 0) Then
                            Dg_HwSubmit.Items(i).Cells(4).Text = "O"

                        End If

                    Next

                Else
                    lblScoreErr.Text = "No Data Present for the Coach"
                End If
            Else
                lblScoreErr.Text = "No Coach Papers present."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnViewSCores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnViewSCores.Click
        If (ValidateViewScores() = 1) Then


            ClearScoreGrids()
            ClearReportGrids()

            ddlChild.Enabled = True
            If ddlChild.SelectedValue = -1 Then
                lblScoreErr.Text = "Please Select Child Name"
                Exit Sub
            Else
                lblScoreErr.Text = ""
                GetScores(Session("CoachPaperID"), False)
            End If
        End If
    End Sub

    Protected Sub BtnGradeCard_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnGradeCard.Click
        If (ValidateViewScores() = 1) Then


            ClearScoreGrids()
            ClearReportGrids()
            If ddlChild.SelectedIndex = 0 Then
                lblScoreErr.Text = "Please Select Child Name."
            Else
                Dim StudType As String = ddlChild.SelectedItem.Text.Split("-")(1).Trim()
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "&StudType=" & StudType & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
            End If
        End If
    End Sub
    Private Sub GetCoachPaperID()
        Dim StrSQL As String = " SELECT DISTINCT C.[CoachPaperId] FROM  CoachRelDates C INNER JOIN CoachReg CR ON C.MemberID = CR.CMemberID and C.EventYear=CR.EventYear and C.ProductGroupId =C.ProductgroupId and C.ProductID =CR.ProductID and C.Level=CR.Level AND C.[Session]= CR.SessionNo "
        StrSQL = StrSQL + " WHERE C.ProductGroupID = ISNULL(" & ddlProductGroup.SelectedValue & ", C.ProductGroupCode) AND C.ProductID = ISNULL (" & ddlProduct.SelectedValue & ",CR.ProductID) AND C.[Level] = ISNULL ('" & ddlLevel.SelectedItem.Text & "',CR.Level)"
        StrSQL = StrSQL + " And C.Session=" & ddlSession.SelectedValue & " AND CR.CMemberID = " & ddlVolName.SelectedValue & "  AND C.EventYear =" & ddlEventYear.SelectedValue & " "

        hdnCoachPaperID.Value = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSQL)
    End Sub

    Protected Sub BtnCalcualteScores_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCalcualteScores.Click

        If (ValidateViewScores() = 1) Then


            lblScoreErr.Text = ""
            ' GetCoachPaperID()
            Dim sqlCommand As String = "usp_CalcSATStudentScores_Overall"
            Dim sqlCommand1 As String = "usp_CalcSATScaledTestScores_Overall"

            Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
            param(1) = New SqlParameter("@CoachPaperID", Session("CoachPaperID"))

            'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
            '******************Calcualtion Of Scores for Each Question******************************************'
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
            '********************Score Summary to ChildTestSummary *********************************************'
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand1, param)



            If (GVAnswers.Visible = True) Then
                GetScores(Session("CoachPaperID"), False)
            End If

            lblScoreErr.Text = "Scores Calculated Successfully."
        End If
    End Sub

    Protected Sub btnSort_Ques_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort_Ques.Click
        Try
            GetClassPerformance("Questionnumber")
            ddlChild.Enabled = False
            'ClearScoreGrids()
            'ClearReportGrids()

            'GetCoachPaperID()
            'Dim CoachPaperId As Integer
            'CoachPaperId = Session("CoachPaperID")
            ''If hdnCoachPaperID.Value <> "" Then
            ''    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            ''End If

            'If CoachPaperId > 0 Then

            '    'Check if ChildTestAnswers has data--If child has completed the test
            '    If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
            '        lblScoreErr.Text = "No child has taken the test."
            '        Exit Sub
            '    End If


            '    'Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,"
            '    'StrSQL = StrSQL & "(Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    'StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    'StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber and C.Score=1 and CR.CMemberID = " & ddlVolName.SelectedValue & ") as CorrectCount, "
            '    'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    'StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    'StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score!=1) as IncorrectCount, "
            '    'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    'StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    'StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score is not null) as TotalCount"
            '    'StrSQL = StrSQL & " From ChildTestAnswers Ch Inner Join CoachReg CR on CR.Childnumber = Ch.Childnumber and CR.PMemberID =Ch.MemberID INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
            '    'StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
            '    'StrSQL = StrSQL & " WHERE Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and CR.CMemberID =" & ddlVolName.SelectedValue & " and  Ch.Score is not null Order by Ch.SectionNumber,Ch.QuestionNumber asc "

            '    Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
            '    StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1  and childnumber in (" + hdnChildNumbers.Value + ")) as CorrectCount,"
            '    StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1 and childnumber in (" + hdnChildNumbers.Value + ")) as IncorrectCount,"
            '    StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null and childnumber in (" + hdnChildNumbers.Value + ")) as TotalCount"
            '    StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
            '    StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
            '    StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
            '    'Response.Write(StrSQL)

            '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            '    If ds.Tables(0).Rows.Count > 0 Then
            '        DGMissedAns.DataSource = ds
            '        DGMissedAns.DataBind()
            '        lblMissedAns.Visible = True
            '        TrSort.Visible = True
            '    Else
            '        lblScoreErr.Text = "No child has taken the test."
            '        TrSort.Visible = False
            '    End If
            'Else
            '    lblScoreErr.Text = "No Coach Papers present."
            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try


    End Sub

    Protected Sub btnSort_Missed_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSort_Missed.Click
        Try
            GetClassPerformance("InCorrectCount")

            ddlChild.Enabled = False
            'ClearScoreGrids()
            'ClearReportGrids()

            'GetCoachPaperID()
            'Dim CoachPaperId As Integer
            'CoachPaperId = Session("CoachPaperID")
            ''If hdnCoachPaperID.Value <> "" Then
            ''    CoachPaperId = Convert.ToInt32(hdnCoachPaperID.Value)
            ''End If

            'If CoachPaperId > 0 Then

            '    'Check if ChildTestAnswers has data--If child has completed the test
            '    If Convert.ToInt32(SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from ChildTestSummary C INNER JOIN CoachReg CR on CR.EventYear =C.EventYear and CR.PMemberID=C.MemberID and CR.ChildNumber =C.Childnumber where CR.CMemberID =" & ddlVolName.SelectedValue & " and C.EventYear=" & ddlEventYear.SelectedValue & " and C.CoachPaperID=" & CoachPaperId & " and C.Completed='Y'")) = 0 Then
            '        lblScoreErr.Text = "No child has taken the test."
            '        Exit Sub
            '    End If


            '    Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber,"
            '    StrSQL = StrSQL & "(Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber and C.Score=1 and CR.CMemberID = " & ddlVolName.SelectedValue & ") as CorrectCount, "
            '    StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score!=1) as IncorrectCount, "
            '    StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers C Inner Join CoachReg CR On CR.EventYear =C.EventYear and CR.Childnumber = C.ChildNumber "
            '    StrSQL = StrSQL & " and CR.PMemberID=C.MemberID Where C.EventYear =" & ddlEventYear.SelectedValue & " and C.CoachPaperID =" & CoachPaperId & " and C.QuestionNumber=Sa.QuestionNumber and "
            '    StrSQL = StrSQL & " C.SectionNumber =Sa.SectionNumber  and CR.CMemberID = " & ddlVolName.SelectedValue & " and C.Score is not null) as TotalCount"
            '    StrSQL = StrSQL & " From ChildTestAnswers Ch Inner Join CoachReg CR on CR.Childnumber = Ch.Childnumber and CR.PMemberID =Ch.MemberID INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
            '    StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber "
            '    StrSQL = StrSQL & " WHERE Ch.EventYear=" & ddlEventYear.SelectedValue & " and Ch.CoachPaperID=" & CoachPaperId & " and CR.CMemberID =" & ddlVolName.SelectedValue & " and  Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc "

            '    'Dim StrSQL As String = "Select Distinct Ch.Eventyear,Ch.CoachPaperId,Ch.SectionNumber,Ch.QuestionNumber," 'Ch.Answer,SA.CorrectAnswer,Ch.Score,"
            '    'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score=1) as CorrectCount,"
            '    'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score!=1) as IncorrectCount,"
            '    'StrSQL = StrSQL & " (Select COUNT(*) From ChildTestAnswers Where EventYear =" & ddlEventYear.SelectedValue & " and CoachPaperID =" & CoachPaperId & " and QuestionNumber=Sa.QuestionNumber and SectionNumber =Sa.SectionNumber and Score is not null) as TotalCount"
            '    'StrSQL = StrSQL & " From ChildTestAnswers Ch INNER JOIN TestAnswerKey SA ON SA.EventYear = Ch.EventYear and SA.CoachPaperID=ch.CoachPaperID "
            '    'StrSQL = StrSQL & " and SA.SectionNumber = Ch.SectionNumber and SA.QuestionNumber=Ch.QuestionNumber WHERE  Ch.EventYear=" & ddlEventYear.SelectedValue & ""
            '    'StrSQL = StrSQL & " and Ch.CoachPaperID=" & CoachPaperId & " and Ch.Score is not null Order by Ch.SectionNumber,CorrectCount asc"
            '    'Response.Write(StrSQL)
            '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSQL)

            '    If ds.Tables(0).Rows.Count > 0 Then
            '        DGMissedAns.DataSource = ds
            '        DGMissedAns.DataBind()
            '        lblMissedAns.Visible = True
            '        TrSort.Visible = True
            '    Else
            '        lblScoreErr.Text = "No child has taken the test."
            '        TrSort.Visible = False

            '    End If
            'Else
            '    lblScoreErr.Text = "No Coach Papers present."
            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub GVAnswers_RowDataBound(ByVal sender As Object, ByVal e As GridViewRowEventArgs)
        Try
            If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
                If e.Row.RowType = DataControlRowType.DataRow Then
                    If e.Row.Cells(7).Text = "Y" Then
                        e.Row.Cells(8).Enabled = True
                        e.Row.Cells(8).BackColor = Color.LemonChiffon
                    Else
                        e.Row.Cells(8).Enabled = False
                    End If
                    'e.Row.Cells(7).Visible = False
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Protected Sub GVAnswersUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim StrAnswers_Update As String = ""
            Dim txtManual As TextBox
            Dim LblScore As Label
            Dim lblCoachPaperId As Label
            Dim ErrFlag As Boolean = False
            Dim CoachPaperID As Integer
            Dim lblManualUpdate As Label
            Dim lblCScore As Label
            Dim StudType = ddlChild.SelectedItem.Text.Split("-")(1)

            For Each row As GridViewRow In GVAnswers.Rows
                txtManual = row.Cells(7).FindControl("txtManual")
                LblScore = row.Cells(7).FindControl("lblScore")
                lblCoachPaperId = row.Cells(7).FindControl("lblCoachPaperId")
                lblManualUpdate = row.Cells(7).FindControl("LblManualUpdate")
                lblCScore = row.Cells(7).FindControl("lblCScore")
                Dim score As String = LblScore.Text
                If lblManualUpdate.Text = "Y" And score <> "1" Then
                    If txtManual.Text = "" Then
                        StrAnswers_Update = StrAnswers_Update & "Update ChildTestAnswers set CScore=NULL "
                    Else
                        If Convert.ToDouble(txtManual.Text) >= -1 And Convert.ToDouble(txtManual.Text) <= 1 Then
                            'Changed from Cscore to score as per Mr.Praveen
                            StrAnswers_Update = StrAnswers_Update & "Update ChildTestAnswers set CScore=" & txtManual.Text
                        Else
                            ErrFlag = True
                        End If
                    End If
                    If (StudType = "Child") Then
                        StrAnswers_Update = StrAnswers_Update & " where EventYear =" & ddlEventYear.SelectedValue & " And ChildNumber = " & ddlChild.SelectedValue & " And CoachPaperID =" & lblCoachPaperId.Text & " And SectionNumber=" & row.Cells(1).Text & " And QuestionNumber =" & row.Cells(2).Text & ""
                        CoachPaperID = lblCoachPaperId.Text
                    ElseIf (StudType = "Adult") Then
                        StrAnswers_Update = StrAnswers_Update & " where EventYear =" & ddlEventYear.SelectedValue & " And AdultId = " & ddlChild.SelectedValue & " And CoachPaperID =" & lblCoachPaperId.Text & " And SectionNumber=" & row.Cells(1).Text & " And QuestionNumber =" & row.Cells(2).Text & ""
                        CoachPaperID = lblCoachPaperId.Text
                    End If

                    Session("CoachPaperID") = lblCoachPaperId.Text

                End If
            Next
            If ErrFlag = True Then
                lblCScoreErr.Text = "Score value must be between -1 And 1."
                Exit Sub
            Else
                If StrAnswers_Update <> "" Then
                    If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrAnswers_Update) > 0 Then
                        lblCScoreErr.Text = "Scores updated successfully."
                        Dim sqlCommand As String = "usp_CalcSATScaledTestScores"

                        Dim param(4) As SqlParameter
                        param(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
                        param(1) = New SqlParameter("@ChildNumber", ddlChild.SelectedValue)
                        param(2) = New SqlParameter("@CoachPaperID", Session("CoachPaperID"))
                        param(3) = New SqlParameter("@StudType", StudType)

                        '******************Calculation Of CScores for selected Child******************************************'
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.StoredProcedure, sqlCommand, param)
                        GetScores(CoachPaperID, False)
                        GetHomeworkSubmissions("No")

                    End If
                End If
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub

    Protected Sub ddlChild_Par_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        'Clear()
        ClearScoreGrids()
        ClearReportGrids()

        If Session("entryToken").ToString().ToUpper() = "PARENT" Then
            LoadProductGroup(Session("User"))
        End If
        'Dim dsGrade As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Grade From child where childNumber=" & ddlChild.SelectedValue)
        'If dsGrade.Tables(0).Rows.Count > 0 Then
        '    'hdnChGrade.Value = dsGrade.Tables(0).Rows(0)("Grade").ToString()
        'End If
    End Sub

    Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnYes.Click
        Try
            Dim StrInsertExp As String = ""
            StrInsertExp = StrInsertExp & " Insert into EnterAnsException (EventYear, MemberID, ChildNumber, CoachPaperID, ProductGroupID, ProductGroupCode, Semester, ProductID, ProductCode, Level, WeekID, NewDeadLine, CreateDate, CreatedBy)"
            StrInsertExp = StrInsertExp & " Select EventYear, " & ddlVolName.SelectedValue & "," & ddlChild.SelectedValue & ", CoachPaperID, ProductGroupID, ProductGroupCode, (Select Distinct Semester from CoachRelDates Where CoachPaperID =" & Session("CoachPaperID") & " And MemberID =" & ddlVolName.SelectedValue & "), ProductID, ProductCode, Level, WeekID, GETDATE()+7, GETDATE(), " & Session("LoginID") & " from CoachPapers where CoachPaperID=" & Session("CoachPaperID")

            'If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) from EnterAnsException where CoachPaperID =" & Session("CoachPaperID") & " And MemberID=" & ddlVolName.SelectedValue & "") > 0 Then
            '    lblScoreErr.Text = "Child has Not submitted the answers."
            '    TrAnsExp.Visible = False
            'Else
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrInsertExp) > 0 Then
                lblScoreErr.Text = "Exception Added for child " & ddlChild.SelectedItem.Text
                TrAnsExp.Visible = False
            End If
            'End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnNo.Click
        TrAnsExp.Visible = False
        lblScoreErr.Text = "Child has Not submitted the answers before the deadline."
    End Sub

    Protected Sub btnGradeCard_Parent_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnGradeCard_Parent.Click

        If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            If ddlChild_Par.Items.Count <= 0 Then
                lblScoreErr.Text = "Child(ren) has/have Not yet taken the test."
                Exit Sub
            ElseIf ddlChild_Par.SelectedValue = -1 Then
                lblScoreErr.Text = "Please Select Child Name"
                Exit Sub
            Else
                lblScoreErr.Text = ""
                Dim StudType As String = ddlChild.SelectedItem.Text.Split("-")(1).Trim()
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild_Par.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "&StudType=" & StudType & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
            End If
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            If ddlChild.Items.Count <= 0 Then
                lblScoreErr.Text = "Child(ren) has/have not yet taken the test."
                Exit Sub
            ElseIf ddlChild.SelectedValue = -1 Then
                lblScoreErr.Text = "Please Select Child Name"
                Exit Sub
            Else
                lblScoreErr.Text = ""
                Dim StudType As String = ddlChild.SelectedItem.Text.Split("-")(1).Trim()
                Response.Write("<script language='javascript'>window.open('GradeCard.aspx?ChildNumberID=" & ddlChild.SelectedValue & "&EventYear=" & ddlEventYear.SelectedValue & "&ProductGroup=" & ddlProductGroup.SelectedValue & "&Product=" & ddlProduct.SelectedValue & "&Level=" & ddlLevel.SelectedItem.Text & "&StudType=" & StudType & "','_blank','fullscreen=yes,left=150,top=0,toolbar=0,location=0,scrollbars=1');</script> ")
            End If

        End If

    End Sub

    Protected Sub ddlSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        'loadVolNames()
        If (Session("entryToken").ToString().ToUpper() = "PARENT") Or (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            DGCoachPapers.DataSource = Nothing
            DGCoachPapers.DataBind()
            lblCoachPapers.Visible = False
            loadVolNames()
            ClearScoreGrids()
            ClearReportGrids()
            If ddlVolName.SelectedValue <> "-1" Then
                LoadCoaches()
                LoadProductGroup(False)
                BindddlStudentID()
            End If


            'If ddlVolName.Items.Count = 1 Then
            '    LoadCoaches()
            '    LoadProductGroup(False)
            '    BindddlStudentID()
            'End If
        End If
    End Sub
    Private Sub loadPhase(ByVal ddlObject As DropDownList)
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2

            ddlObject.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlObject.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
        If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
            Dim cmdtext As String = " select max(Semester) from CoachReg where PMemberId=" & Session("LoginId").ToString() & " and EventYear=" & ddlEventYear.SelectedValue & ""
            Try
                Dim Semester As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdtext)
                ddlObject.SelectedValue = Semester
            Catch ex As Exception

            End Try
        ElseIf (Session("entryToken").ToString().ToUpper() = "VOLUNTEER" And Session("RoleId").ToString() = "88") Then
            Dim cmdtext As String = " select max(Semester) from CalSignup where MemberId=" & Session("LoginId").ToString() & " and EventYear=" & ddlEventYear.SelectedValue & ""
            Try
                Dim Semester As String = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdtext)
                ddlObject.SelectedValue = Semester
            Catch ex As Exception

            End Try
        End If
    End Sub

    Protected Sub ddlPSemester_SelectedIndexChanged(sender As Object, e As EventArgs)
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        ddlLevel.Items.Clear()
        ddlSession.Items.Clear()
        If (Session("entryToken").ToString().ToUpper() = "PARENT") Then
            lnkParentPage.Visible = True
            lnkstudentPage.Visible = False
            Loadchild(Convert.ToInt32(Session("CustIndID")), 0)
        ElseIf (Session("entryToken").ToString().ToUpper() = "STUDENT") Then
            lnkParentPage.Visible = False
            lnkstudentPage.Visible = True
            Loadchild(Convert.ToInt32(Session("CustIndID")), Convert.ToInt32(Session("StudentId")))

        End If
    End Sub
    Protected Sub PopulateWeekNo()
        lblScoreErr.Text = ""
        Dim CmdText As String = ""
        If (Session("entryToken").ToString().ToUpper() = "VOLUNTEER") Then
            CmdText = " select distinct CP.WeekId, cp.Coachpaperid from Coachreldates CR inner join CoachPapers CP on (CR.CoachpaperId=CP.CoachpaperId) where CR.eventyear=" & ddlEventYear.SelectedValue & " and CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " and CR.ProductId=" & ddlProduct.SelectedValue & " and CR.level='" & ddlLevel.SelectedValue & "' and CR.session=" & ddlSession.SelectedValue & " and CR.MemberId=" & ddlVolName.SelectedValue & " and CR.Semester='" & ddlSemester.SelectedValue & "' and CP.DocType='Q' order by WeekId ASC"
        Else
            Dim StudType = ddlChild_Par.SelectedItem.Text.Split("-")(1)
            CmdText = " select CP.WeekId,cp.Coachpaperid from Coachreldates CR inner join CoachPapers CP on (CR.CoachpaperId=CP.CoachpaperId) "
            If (StudType = "Child") Then
                CmdText = CmdText & " inner join CoachReg CH on (Ch.ChildNumber=" & ddlChild_Par.SelectedValue & " and CH.CmemberId=CR.MemberID and CH.productGroupID=CR.ProductGroupID and CH.ProductId=CR.productId and CH.Level=CR.Level)"
            ElseIf (StudType = "Adult") Then
                CmdText = CmdText & " inner join CoachReg CH on (Ch.AdultId=" & ddlChild_Par.SelectedValue & " and CH.CmemberId=CR.MemberID and CH.productGroupID=CR.ProductGroupID and CH.ProductId=CR.productId and CH.Level=CR.Level)"
            End If
            CmdText = CmdText & " where CR.eventyear=" & ddlEventYear.SelectedValue & " And CR.ProductGroupId=" & ddlProductGroup.SelectedValue & " And CR.ProductId=" & ddlProduct.SelectedValue & " And CR.level='" & ddlLevel.SelectedValue & "' and CR.session=" & ddlSession.SelectedValue & " and CR.Semester='" & ddlPSemester.SelectedValue & "' and CP.DocType='Q' order by WeekId ASC"
        End If




        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, CmdText)

        If ds.Tables(0).Rows.Count > 0 Then
            ddlWeekNo.DataSource = ds
            ddlWeekNo.DataBind()

            ddlWeekNo.Items.Insert(0, New ListItem("Select", "0"))
        Else
            ddlWeekNo.DataSource = ds
            ddlWeekNo.DataBind()
            lblScoreErr.Text = "Test Papers not yet released"
        End If
    End Sub

    Protected Sub Dg_HwSubmit_ItemCommand(source As Object, e As DataGridCommandEventArgs)
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        For i As Integer = 0 To Dg_HwSubmit.Items.Count - 1
            Dg_HwSubmit.Items(i).BackColor = Color.White
        Next
        If (cmdName = "StudentName") Then
            e.Item.BackColor = Color.Gainsboro
            Dim StudentId As String = CType(e.Item.FindControl("lblStudentId"), Label).Text
            Dim CoachPaperId As String = CType(e.Item.FindControl("lblCOachPaperId"), Label).Text

            ddlChild.SelectedValue = StudentId
            GetScores(CoachPaperId, False)
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "SetAnswerScroll();", True)
        End If
    End Sub

    Protected Sub ddlWeekNo_SelectedIndexChanged(sender As Object, e As EventArgs)
        Session("CoachPaperID") = ddlWeekNo.SelectedValue
        If (ddlWeekNo.SelectedValue <> "-1") Then
            ddlChild.Enabled = True
        End If

    End Sub

    Protected Sub BtnResetGrade_Click(sender As Object, e As EventArgs)
        If (ddlChild.SelectedValue = "-1") Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Please select child."
        ElseIf (ddlWeekNo.SelectedValue = "0") Then
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Please select week#."
        Else

            Dim StrCmd As String = " Select count(*) from ChildTestSummary where EventYear=" & ddlEventYear.SelectedValue & " and memberId=" & ddlVolName.SelectedValue & " and CoachPaperId=" & ddlWeekNo.SelectedValue & " and ChildNumber=" & ddlChild.SelectedValue & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrCmd)
            If ds.Tables(0).Rows.Count > 0 Then
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmReset();", True)
            Else
                lblErrorMsg.Visible = True
                lblErrorMsg.Text = "The child not yet completed the test."
            End If

        End If
    End Sub

    Protected Sub btnConfirmDelete_Click(sender As Object, e As EventArgs)
        Dim StrCmd As String = ""
        StrCmd = " Select count(*) from ChildTestSummary where EventYear=" & ddlEventYear.SelectedValue & " and memberId=" & ddlVolName.SelectedValue & " and CoachPaperId=" & ddlWeekNo.SelectedValue & " and ChildNumber=" & ddlChild.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrCmd)
        If ds.Tables(0).Rows.Count > 0 Then
            StrCmd = " update ChildtestSummary set Completed= null where EventYear=" & ddlEventYear.SelectedValue & " and CoachPaperId=" & ddlWeekNo.SelectedValue & " and ChildNumber=" & ddlChild.SelectedValue & ""
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, StrCmd)
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "Grade has been reset successfully."
        Else
            lblErrorMsg.Visible = True
            lblErrorMsg.Text = "The child not yet completed the test."
        End If
    End Sub
End Class

