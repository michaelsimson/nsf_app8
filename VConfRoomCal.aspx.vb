﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections

Imports System.Reflection



Partial Class VConfRoomCal
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Response.Redirect("~/maintest.aspx")
        End If
        If Page.IsPostBack = False Then
            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            'ddlYear.Items.Insert(0, Convert.ToString(year - 4))
            'ddlYear.Items.Insert(1, Convert.ToString(year - 3))
            'ddlYear.Items.Insert(2, Convert.ToString(year - 2))
            'ddlYear.Items.Insert(3, Convert.ToString(year - 1))
            'ddlYear.Items.Insert(4, Convert.ToString(year))

            ddlYear.Items.Add(New ListItem(Convert.ToString(year - 4) + "-" + Convert.ToString(year - 3), Convert.ToString(year - 4)))
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - 3) + "-" + Convert.ToString(year - 2), Convert.ToString(year - 3)))
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - 2) + "-" + Convert.ToString(year - 1), Convert.ToString(year - 2)))
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - 1) + "-" + Convert.ToString(year), Convert.ToString(year - 1)))
            ddlYear.Items.Add(New ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1), Convert.ToString(year)))
            'ddlYear.Items.Insert(1, Convert.ToString(year - 3))
            'ddlYear.Items.Insert(2, Convert.ToString(year - 2))
            'ddlYear.Items.Insert(3, Convert.ToString(year - 1))
            'ddlYear.Items.Insert(4, Convert.ToString(year))

            ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()

            ddlDay.SelectedIndex = ddlDay.Items.IndexOf(ddlDay.Items.FindByText(Today.DayOfWeek.ToString()))
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                hlnkMainMenu.Text = "Back to Parent Functions"
                hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
            ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                Response.Redirect("~/login.aspx?entry=v")
            ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then

                GetClassDetails()
                InitialiseTable()

                ddlDay_SelectedIndexChanged(ddlDay, New EventArgs)
                ' FillGrid()


            Else
                Response.Redirect("~/maintest.aspx")
            End If

        End If
    End Sub

    Private Sub InitialiseTable()
        Dim dt As New DataTable
        dt.Columns.Add("VRoom", GetType(Integer))

        dt.Columns.Add("8:00", GetType(String))
        dt.Columns.Add("9:00", GetType(String))
        dt.Columns.Add("10:00", GetType(String))
        dt.Columns.Add("11:00", GetType(String))
        dt.Columns.Add("12:00", GetType(String))
        dt.Columns.Add("13:00", GetType(String))
        dt.Columns.Add("14:00", GetType(String))
        dt.Columns.Add("15:00", GetType(String))
        dt.Columns.Add("16:00", GetType(String))
        dt.Columns.Add("17:00", GetType(String))

        dt.Columns.Add("18:00", GetType(String))
        dt.Columns.Add("19:00", GetType(String))
        dt.Columns.Add("20:00", GetType(String))
        dt.Columns.Add("21:00", GetType(String))
        dt.Columns.Add("22:00", GetType(String))
        dt.Columns.Add("23:00", GetType(String))
        dt.Columns.Add("0:00", GetType(String))
        Dim i As Integer = 1
        For i = 1 To 25
            dt.Rows.Add(i, "")
        Next
        ViewState("InitialTable") = dt

    End Sub

    Private Sub FillGrid()
        Try

            Dim dt As DataTable = ViewState("InitialTable")
            dt.Rows.Clear()
            Dim i As Integer = 1

            For i = 1 To 25
                dt.Rows.Add(i, "")
            Next

            ViewState("VRoomMaster") = dt
            GetDetailsByDay(ddlDay.SelectedValue, ddlYear.SelectedValue)
            Dim j As Integer

            If grdList.Columns.Count > 0 Then

                If ViewState("IsWeekEnd").ToString() = "True" Then

                    For j = 1 To 10

                        grdList.Columns(j).Visible = True
                    Next

                Else

                    For j = 1 To 10

                        grdList.Columns(j).Visible = False
                    Next
                End If
            End If



            grdList.DataSource = ViewState("VRoomMaster")
            grdList.DataBind()



        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub



    Private Sub GetDetailsByDay(sDay As String, iYear As Integer)

        Try

            Dim dt As DataTable = ViewState("ClassDetails")
            Dim dtMaster As DataTable = ViewState("VRoomMaster")
            Dim drResults() As DataRow = dt.Select("Day = '" & sDay & "' and EventYear =" & iYear & " and VRoom<>0")
            Dim inx As Integer

            For inx = 0 To drResults.GetUpperBound(0)

                Dim drs() As DataRow = dtMaster.Select("VRoom = " & drResults(inx)("Vroom"))
                If (drs.Length > 0) Then

                    drs(0)(1) = IIf(drResults(inx)("8:00") = 0, drs(0)(1), drResults(inx)("8:00") & drResults(inx)("ProductCode"))
                    drs(0)(2) = IIf(drResults(inx)("9:00") = 0, drs(0)(2), drResults(inx)("9:00") & drResults(inx)("ProductCode"))
                    drs(0)(3) = IIf(drResults(inx)("10:00") = 0, drs(0)(3), drResults(inx)("10:00") & drResults(inx)("ProductCode"))
                    drs(0)(4) = IIf(drResults(inx)("11:00") = 0, drs(0)(4), drResults(inx)("11:00") & drResults(inx)("ProductCode"))
                    drs(0)(5) = IIf(drResults(inx)("12:00") = 0, drs(0)(5), drResults(inx)("12:00") & drResults(inx)("ProductCode"))
                    drs(0)(6) = IIf(drResults(inx)("13:00") = 0, drs(0)(6), drResults(inx)("13:00") & drResults(inx)("ProductCode"))
                    drs(0)(7) = IIf(drResults(inx)("14:00") = 0, drs(0)(7), drResults(inx)("14:00") & drResults(inx)("ProductCode"))
                    drs(0)(8) = IIf(drResults(inx)("15:00") = 0, drs(0)(8), drResults(inx)("15:00") & drResults(inx)("ProductCode"))
                    drs(0)(9) = IIf(drResults(inx)("16:00") = 0, drs(0)(9), drResults(inx)("16:00") & drResults(inx)("ProductCode"))
                    drs(0)(10) = IIf(drResults(inx)("17:00") = 0, drs(0)(10), drResults(inx)("17:00") & drResults(inx)("ProductCode"))
                    drs(0)(11) = IIf(drResults(inx)("18:00") = 0, drs(0)(11), drResults(inx)("18:00") & drResults(inx)("ProductCode"))
                    drs(0)(12) = IIf(drResults(inx)("19:00") = 0, drs(0)(12), drResults(inx)("19:00") & drResults(inx)("ProductCode"))
                    drs(0)(13) = IIf(drResults(inx)("20:00") = 0, drs(0)(13), drResults(inx)("20:00") & drResults(inx)("ProductCode"))
                    drs(0)(14) = IIf(drResults(inx)("21:00") = 0, drs(0)(14), drResults(inx)("21:00") & drResults(inx)("ProductCode"))
                    drs(0)(15) = IIf(drResults(inx)("22:00") = 0, drs(0)(15), drResults(inx)("22:00") & drResults(inx)("ProductCode"))
                    drs(0)(16) = IIf(drResults(inx)("23:00") = 0, drs(0)(16), drResults(inx)("23:00") & drResults(inx)("ProductCode"))
                    drs(0)(17) = IIf(drResults(inx)("0:00") = 0, drs(0)(17), drResults(inx)("0:00") & drResults(inx)("ProductCode"))

                End If
            Next inx

            dtMaster.AcceptChanges()
            ViewState("VRoomMaster") = dtMaster
        Catch ex As Exception
            '  Response.Write("Err :" & ex.ToString())
        End Try
    End Sub


    Private Sub GetClassDetails()

        Try
            Dim cmdText As String = ""
            cmdText = "SELECT * FROM (Select B.ProductId as productid,Time,Day,B.ProductCode as ProductCode, Vroom, B.EventYear as EventYear  from CoachReg B Inner join calsignup A on B.CMemberId =A.MemberId where B.Approved='Y' and A.Accepted='Y' and A.EventYear=B.EventYear and B.ProductGroupCode= A.ProductGroupCode and B.ProductCode=A.ProductCode and B.Phase=A.Phase "
            cmdText = cmdText & " and B.Level=A.Level and B.Sessionno=A.Sessionno ) as s PIVOT( count(productid) FOR [Time] IN ([8:00],[9:00],[10:00],[11:00],[12:00],[13:00],[14:00],[15:00],[16:00],[17:00],[18:00],[19:00],[20:00],[21:00],[22:00],[23:00],[0:00]))AS VRoomCal where vroom is not null and vroom <> 0"

            Dim ds As DataSet
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            ViewState("ClassDetails") = ds.Tables(0)

        Catch ex As Exception

        End Try
    End Sub



    Protected Sub ddlDay_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlDay.SelectedIndexChanged

        'Add bool value when selecting saturday and sunday
        ViewState("IsWeekEnd") = IIf((ddlDay.SelectedValue = "Sunday" Or ddlDay.SelectedValue = "Saturday"), "True", "False")
        FillGrid()

    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        FillGrid()
        Dim attach As String = String.Empty
        attach = "attachment;filename=VConfRoomCalendar.xls"
        Response.ClearContent()
        Response.AddHeader("content-disposition", attach)
        Response.ContentType = "application/vnd.xls"

        Dim dt As DataTable = ViewState("VRoomMaster")
        Dim dr As DataRow
        Dim dc As DataColumn
        Dim i As Integer
        If dt.Rows.Count > 0 Then
            If ViewState("IsWeekEnd").ToString() = "False" Then
                For i = 1 To 10
                    dt.Columns.RemoveAt(1)
                Next
            End If
            Response.Write("<table border=1><tr style='height:35px'><td colspan=" & dt.Columns.Count & " style='color:green;font-weight:bold;text-align:center;vertical-align:middle;'> Virtual Conference Room Calendar</td></tr>")
            Response.Write("<tr><td style='font-weight:bold;text-align:right;'>Year</td><td>" & ddlYear.SelectedValue & " </td><td style='font-weight:bold;text-align:right;'>Day </td><td> " & ddlDay.SelectedValue & "</td></tr>")
            Response.Write("<tr>")
            For Each dc In dt.Columns
                Response.Write("<th>" + dc.ColumnName.ToString() + "</th>")
            Next
            Response.Write("</tr>")
            For Each dr In dt.Rows
                Response.Write("<tr><td>" & dr(0).ToString() & "</td>")
                For i = 1 To dt.Columns.Count - 1
                    Response.Write("<td>" & dr(i).ToString() & "</td>")
                Next
                Response.Write("</tr>")
            Next
        End If
        Response.Write("</table>")
        Response.Flush()
        HttpContext.Current.Response.Flush()
        HttpContext.Current.Response.SuppressContent = True
        HttpContext.Current.ApplicationInstance.CompleteRequest()

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged
        FillGrid()
    End Sub


    Protected Sub btnExportAll_Click(sender As Object, e As EventArgs) Handles btnExportAll.Click
        Dim ds As New DataSet
        Dim inxDay As Integer, inxRow As Integer, indxRemove As Integer

        Dim dt(7) As DataTable
        Dim strDay As String = ""

        For inxDay = 0 To 6
            strDay = ddlDay.Items(inxDay).Text.ToString()
            ViewState("VRoomMaster") = Nothing
            Dim dtInitial As DataTable = ViewState("InitialTable")
            dtInitial.Rows.Clear()

            For inxRow = 1 To 25
                dtInitial.Rows.Add(inxRow, "")
            Next
            ViewState("VRoomMaster") = dtInitial
            GetDetailsByDay(strDay, ddlYear.SelectedValue)

            dt(inxDay) = CType(ViewState("VRoomMaster"), DataTable).Copy()

            If strDay.ToLower <> "sunday" And strDay.ToLower <> "saturday" Then
                For indxRemove = 1 To 10
                    dt(inxDay).Columns.RemoveAt(1)
                Next
            End If
            dt(inxDay).TableName = strDay
            ds.Tables.Add(dt(inxDay))
        Next
        Try
            ExcelHelper.ToExcel(ds, "VConfRoomCalendarAll.xls", Page.Response)
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
End Class


