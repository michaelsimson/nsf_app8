﻿Imports System.Data
Imports System.Data.SqlClient
Imports System.Data.SqlDbType
Imports System.Text
Imports System.Web.Configuration
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Partial Class CustServSearch
    Inherits System.Web.UI.Page

    Dim sSQL, sSQLORG As String
    Dim dataAdapterInd, dataAdapterOrg As Data.SqlClient.SqlDataAdapter
    Dim dsInd As New Data.DataSet
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        GVChild.DataSource = Nothing
        GVChild.DataBind()
        Try
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
        Catch ex As Exception
            Response.Redirect("Maintest.aspx")
        End Try
        If Page.IsPostBack = False Then
            '*** Populate State DropDown
            Dim dsStates As DataSet
            Dim strSqlQuery As String
            strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster"
            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSqlQuery)
            If dsStates.Tables.Count > 0 Then
                ddlState.DataSource = dsStates.Tables(0)
                ddlState.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlState.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlState.DataBind()
                ddlState.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If

            '*** Populate Chapter DropDown
            '*** Populate Chapter Names List
            Dim objChapters As New Chapter
            Dim dsChapters As New DataSet
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            If dsChapters.Tables.Count > 0 Then
                ddlChapter.DataSource = dsChapters.Tables(0)
                ddlChapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlChapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlChapter.DataBind()
                ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            End If
            If Len(Session("LoginChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("LoginChapterID")
                ddlChapter.Enabled = False
            End If
            If Session("RoleID") = "5" Then
                ddlChapter.Items.Clear()
                Dim strSql As String
                strSql = "Select chapterid, chaptercode, state from chapter "
                strSql = strSql & " where clusterid in (Select clusterid from "
                strSql = strSql & " chapter where chapterid = " + Session("LoginChapterID") & ")"
                strSql = strSql & " order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))
                Dim drChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drChapters.Read())
                    ddlChapter.Items.Add(New ListItem(drChapters(1).ToString(), drChapters(0).ToString()))
                End While
                ddlChapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                ddlChapter.Enabled = True
            End If
        End If
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        Dim blnCheck As Boolean = False
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,CPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <> 'Yes'"
        Dim Email As String = tbEmail.Text
        Dim FirstName As String = tbFname.Text
        Dim LastName As String = tbLname.Text
        Dim state As String = ddlState.Text
        Dim NSFChapter As String = ddlChapter.SelectedItem.Text

        If (Len(Trim(FirstName)) > 0) Then
            sSQL = sSQL & " and FIRSTNAME like '%" & FirstName & "%'"
            blnCheck = True
        End If

        If (Len(Trim(LastName)) > 0) Then
            sSQL = sSQL & " and LASTNAME like '%" & LastName & "%'"
            blnCheck = True
        End If

        If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
            sSQL = sSQL & " and STATE like '%" & state & "%'"
            blnCheck = True
        End If

        If (Len(Trim(Email)) > 0) Then
            sSQL = sSQL & " and EMAIL like '%" & Email & "%'"
            blnCheck = True
        End If

        If Session("RoleID") = "5" Then
            Dim i As Integer
            NSFChapter = ddlChapter.SelectedItem.Text
            If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                blnCheck = True
            Else
                For i = 1 To ddlChapter.Items.Count - 1
                    If Len(NSFChapter) > 0 And NSFChapter <> "Select Chapter" Then
                        NSFChapter = NSFChapter & ",'" & ddlChapter.Items(i).Text & "'"
                    Else
                        NSFChapter = "'" & ddlChapter.Items(i).Text & "'"
                    End If
                Next
                sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
                blnCheck = True
            End If
        Else
            If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
                blnCheck = True
            End If
        End If
        If blnCheck = True Then
            HSQL.Value = sSQL
            Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
            dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
            Try
                dataAdapterInd.Fill(dsInd)
                gvIndSpouse.DataSource = dsInd.Tables(0)
                gvIndSpouse.DataBind()
                lblMessage.Text = ""
                If dsInd.Tables(0).Rows.Count = 0 Then
                    lblMessage.Text = "no records to Display"
                End If
            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try
        Else
            'No input
            lblMessage.Text = "please enter values"
        End If

    End Sub

    Protected Sub gvIndSpouse_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIndSpouse.PageIndexChanging
        gvIndSpouse.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(HSQL.Value, dataConnection)

        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvIndSpouse.DataSource = dsInd.Tables(0)
            gvIndSpouse.DataBind()
        Catch ex As Exception
            LblMessage.Text = ex.Message
        End Try
    End Sub
    Protected Sub gvIndSpouse_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs)
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = gvIndSpouse.Rows(index)
        ' IncurredExpence name Search
        Dim memberid As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select CASE when DonorType ='IND' THEN automemberid ELSE Relationship END from IndSpouse where automemberid = " & gvIndSpouse.DataKeys(index).Value & "")
        If memberid > 0 Then
            Loadchild(memberid)
        Else
            lblMessage.Text = "Sorry Not Found"
        End If
    End Sub

    Private Sub Loadchild(ByVal ParentID As Integer)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim strSql As String = "select FIRST_NAME,LAST_NAME ,GENDER ,DATE_OF_BIRTH ,GRADE,SchoolName,ChildNumber,Memberid  From Child where memberid=" & ParentID
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSql)
        If ds.Tables(0).Rows.Count > 0 Then
            GVChild.DataSource = ds
            GVChild.DataBind()
        Else
            lblMessage.Text = "No Child Found"
        End If
    End Sub
End Class
