Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Generic
Imports VRegistration

Partial Class PrepareSchedule
    Inherits System.Web.UI.Page
    Dim IsContinue As Boolean
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If


        If Not Page.IsPostBack Then

            Dim year As Integer = 0
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year + 1) + "-" + Convert.ToString(year + 2)), Convert.ToString(year + 1)))
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year) + "-" + Convert.ToString(year + 1), Convert.ToString(year)))
            ddlEventYear.Items.Add(New ListItem(Convert.ToString(year - 1) + "-" + Convert.ToString(year), Convert.ToString(year - 1)))
            'ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
            ''Options for Volunteer Name Selection
            loadPhase()
            'If (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches. 
            Dim ds_Vol As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID  " & IIf(Session("RoleID") = 89, " Where I.AutoMemberID=" & Session("LoginID"), "") & " order by I.FirstName")
            If ds_Vol.Tables(0).Rows.Count > 0 Then
                ddlVolName.DataSource = ds_Vol
                ddlVolName.DataBind()
                If ds_Vol.Tables(0).Rows.Count > 1 Then
                    ddlVolName.Items.Insert(0, "Select Volunteer")
                    ddlVolName.SelectedIndex = 0
                End If
            Else
                lblError.Text = "No Volunteers present for Calendar Sign up"
            End If

            'End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Then
                LoadProductGroup()
                LoadProductID()
            Else
                If Session("RoleId").ToString() = "89" Then
                    Dim ds As DataSet
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId is not Null") > 1 Then
                        'more than one 
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim i As Integer
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        For i = 0 To ds.Tables(0).Rows.Count - 1
                            If prd.Length = 0 Then
                                prd = ds.Tables(0).Rows(i)(1).ToString()
                            Else
                                prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                            End If

                            If Prdgrp.Length = 0 Then
                                Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                            Else
                                Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                            End If
                        Next
                        lblPrd.Text = prd
                        lblPrdGrp.Text = Prdgrp
                        LoadProductGroup()
                        LoadProductID()
                    Else
                        'only one
                        ds = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null ")
                        Dim prd As String = String.Empty
                        Dim Prdgrp As String = String.Empty
                        If ds.Tables(0).Rows.Count > 0 Then
                            prd = ds.Tables(0).Rows(0)(1).ToString()
                            Prdgrp = ds.Tables(0).Rows(0)(0).ToString()
                            lblPrd.Text = prd
                            lblPrdGrp.Text = Prdgrp
                        End If
                        LoadProductGroup()
                        LoadProductID()
                    End If
                End If
            End If
            LoadRole()
            LoadVolunteer()
            LoadEvent(ddlEvent)
            LoadWeekdays()
            LoadGrid(DGSchedule)
            lblError.Text = ""
            oDiv.Attributes.Add("onscroll", "setonScroll(this,'" + DGSchedule.ClientID + "');")
        End If
    End Sub
    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2
            ddlPhase.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        ddlPhase.SelectedValue = objCommon.GetDefaultSemester(ddlEventYear.SelectedValue)
    End Sub
    Public Sub LoadRole()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct RoleID,RoleCode From Volunteer Where RoleID in (" & Session("RoleID") & ")")
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRole.DataSource = ds
            ddlRole.DataBind()
        End If

    End Sub

    Public Sub LoadVolunteer()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.FirstName +''+I.LastName as Name,C.MemberID From CalSignUp C Inner Join IndSpouse I on I.AutoMemberID=C.MemberID " & IIf(Session("RoleID") = 89, " where C.MemberID=" & Session("LoginID"), ""))
        If ds.Tables(0).Rows.Count > 0 Then
            ddlVolName.DataSource = ds
            ddlVolName.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlVolName.Items.Insert(0, New ListItem("Select Event", "-1"))
            Else
                ddlVolName.Enabled = False
            End If
            ddlVolName.SelectedIndex = 0
        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub
        ' "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where EF.EventYear =" & ddlEventYear.SelectedValue & "  and E.EventId in(13,19)" '13- Coaching,19 -PreClub

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                ddlObject.Enabled = True
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
                LoadProductID()
            End If
            Try
                ddlObject.SelectedValue = "13"
                LoadProductGroup()
                LoadProductID()
            Catch ex As Exception

            End Try

        Else
            lblError.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Private Sub LoadWeekdays()
        For i As Integer = 1 To 7
            ddlWeekDay.Items.Add(WeekdayName(i))
        Next
        ddlWeekDay.SelectedIndex = ddlWeekDay.Items.IndexOf(ddlWeekDay.Items.FindByText("Saturday"))
    End Sub

    Private Sub LoadProductGroup()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " and EF.Semester='" & ddlPhase.SelectedValue & "' AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblError.Text = "No Product Group present for the selected Event."
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                If Session("RoleID").ToString() = "1" Or Session("RoleID").ToString() = "2" Or Session("RoleID").ToString() = "96" Then
                    ddlProductGroup.Items.Insert(1, "All")

                End If
                If Session("RoleID").ToString() = "1" Or Session("RoleID").ToString() = "2" Or Session("RoleID").ToString() = "96" Then
                    '  ddlProductGroup.Items(1).Selected = True
                    LoadProductID()
                Else
                    ddlProductGroup.Items(0).Selected = True
                End If
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
            End If

        Catch ex As Exception
            ' Response.Write(ex.ToString())
        End Try
    End Sub
    Private Sub LoadProductID()
        ddlProduct.Items.Clear()
        ' will load depending on Selected item in Productgroup
        Try
            'If ddlProductGroup.SelectedValue = "All" Then
            '    ddlProduct.Items.Insert(0, "All")

            'Else
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ' If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            ' ddlProduct.Enabled = False
            'Else
            Dim strSql As String
            strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " and EF.Semester='" & ddlPhase.SelectedValue & "' AND P.EventID=13"

            If (ddlProductGroup.SelectedValue <> "Select Product Group" And ddlProductGroup.SelectedValue <> "All") Then
                strSql = strSql & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & ""
            End If
            strSql = strSql & "  order by P.ProductID"
            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlProduct.DataSource = drproductid
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Items.Insert(0, "Select Product")
                If Session("RoleID").ToString() = "1" Or Session("RoleID").ToString() = "2" Or Session("RoleID").ToString() = "96" Then
                    ddlProduct.Items.Insert(1, "All")
                End If

                ddlProduct.Enabled = True
            ElseIf ddlProduct.Items.Count < 1 Then
                '  ddlProduct.Enabled = False
            Else
                LoadGrid(DGSchedule)
                '  ddlProduct.Enabled = False
            End If
            '  End If
            ' End If
            If Session("RoleID").ToString() = "1" Or Session("RoleID").ToString() = "2" Or Session("RoleID").ToString() = "96" Then
                ddlProduct.Items(0).Selected = True
            Else
                ddlProduct.Items(0).Selected = True
            End If
        Catch ex As Exception
            lblError.Text = lblError.Text & "<br>" & ex.ToString
        End Try
    End Sub

    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            'If ddlVolName.SelectedItem.Text = "" Then 'Session("RoleID") = "89" And
            '    lblError.Text = "Please select Volunteer"
            'Else
            If ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblError.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblError.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblError.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblError.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblError.Text = "Please select Product"
            Else
                lblError.Text = ""
                Dim cmdText As String
                If ddlProductGroup.SelectedValue = "All" And ddlProduct.SelectedValue = "All" Then
                    cmdText = "select count(*) from CalSignUp where Semester='" & ddlPhase.SelectedValue & "' and Eventyear=" & ddlEventYear.SelectedValue

                ElseIf ddlProduct.SelectedValue = "All" And ddlProductGroup.SelectedValue <> "All" Then
                    cmdText = "select count(*) from CalSignUp where Semester='" & ddlPhase.SelectedValue & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue
                Else
                    cmdText = "select count(*) from CalSignUp where Semester='" & ddlPhase.SelectedValue & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue
                End If

                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdText) > 0 Then '< 1 Then '" & IIf(Session("RoleID") = "89", " Memberid=" & ddlVolName.SelectedValue & " and ", "") & "
                    LoadGrid(DGSchedule)
                Else
                    DGSchedule.Visible = False
                    btnExport.Enabled = False
                    lblError.Text = "No record Exists."
                End If
            End If
            ViewState("IsContinue") = False
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub
    Public Function WeekDayNumber(ByVal weekName As String) As Integer
        Dim weekNames As New Dictionary(Of String, Integer)
        For i As Integer = 1 To 7
            weekNames.Add(WeekdayName(i), i)
        Next
        Return weekNames(weekName)
    End Function

    Public Sub LoadGrid(ByVal DGSchedule)
        Try
            Dim StrSQl As String = ""
            Dim ds As DataSet
            Dim cmdText As String
            If ddlProductGroup.SelectedValue = "All" And ddlProduct.SelectedValue = "All" Then
                cmdText = "Select Count(*) From CalSignUp Where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "'"

            ElseIf ddlProduct.SelectedValue = "All" And ddlProductGroup.SelectedValue <> "All" Then
                cmdText = "Select Count(*) From CalSignUp Where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "' And ProductGroupID = " & ddlProductGroup.SelectedValue & ""
            Else
                cmdText = "Select Count(*) From CalSignUp Where EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "' And ProductGroupID = " & ddlProductGroup.SelectedValue & " And ProductID = " & ddlProduct.SelectedValue & ""
            End If

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText) > 0 Then '" & IIf(Session("RoleID") = 89, " MemberID=" & Session("LoginID") & " and ", "") & "

                Dim ProductGroupId As Integer
                Dim ProductId As Integer
                If ddlProductGroup.SelectedValue = "All" Then
                    ProductGroupId = 0
                Else
                    ProductGroupId = ddlProductGroup.SelectedValue
                End If

                If ddlProduct.SelectedValue = "All" Then
                    ProductId = 0
                Else
                    ProductId = ddlProduct.SelectedValue
                End If

                Dim prmArray(12) As SqlParameter
                prmArray(0) = New SqlParameter("@EventYear", ddlEventYear.SelectedValue)
                prmArray(1) = New SqlParameter("@EventID", ddlEvent.SelectedValue)
                prmArray(2) = New SqlParameter("@Semester", ddlPhase.SelectedValue)
                prmArray(3) = New SqlParameter("@ProductGroupID", ProductGroupId)
                prmArray(4) = New SqlParameter("@ProductID", ProductId)
                'prmArray(5) = New SqlParameter("@MemberID", IIf(Session("RoleID") = 89, Session("LoginID"), "NULL"))

                Dim j As Integer = -1 '' Set to assign weekday names for Headers in the DataGris at appropriate Columns.
                Dim WeekDayVal As Integer = WeekDayNumber(ddlWeekDay.SelectedItem.Text)
                '***********Assigning Weekday parameters for the SP*************'
                Dim count As Integer = 0
                For i As Integer = 5 To 11
                    count = count + 1
                    j = j + 4
                    If WeekDayVal > 7 Then
                        WeekDayVal = WeekDayVal - 7
                    End If

                    prmArray(i) = New SqlParameter("@Day" & i - 4, WeekdayName(WeekDayVal))

                    'DGSchedule.Columns(j).HeaderText = WeekdayName(WeekDayVal).ToString.Substring(0, 3) ' "Sunday"

                    WeekDayVal = WeekDayVal + 1
                Next

                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_PrepSchCalSignup", prmArray)
                'Dim dt As DataTable = ds.Tables(0)

                If ds.Tables(0).Rows.Count > 0 Then
                    DGSchedule.DataSource = ds
                    DGSchedule.DataBind()
                    btnHelp.Visible = False
                    '************************Enable only the filled Cells in Grid****************************'
                    For Each row As DataGridItem In DGSchedule.Items
                        'For Each DataGridItem In DGSchedule.Items
                        Dim l As Integer = 2
                        For k As Integer = 0 To 6
                            If row.Cells(l).Text.ToString() = " " Then 'coltext.Length > 0 Then
                                DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).Enabled = False '.Text()
                            Else
                            End If
                            l = l + 4
                        Next
                    Next
                    '******************************************************************************************'
                    DGSchedule.Visible = True
                    btnExport.Enabled = True
                Else
                    lblError.Text = "No Data Exists for the selections made."
                End If

            Else
                lblError.Text = "No Data Exists."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Private Sub clear()
        lblError.Text = ""
        ddlEvent.SelectedIndex = 0
        ddlProductGroup.Items.Clear()
        ddlProductGroup.Enabled = False
        ddlProduct.Items.Clear()
        'ddlProduct.Enabled = False
        'ddlProductGroup.SelectedIndex = 0
        ddlPhase.SelectedIndex = 0
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        DGSchedule.Visible = False
        DGSchedule.Visible = False
        LoadEvent(ddlEvent)

    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblError.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
            LoadProductID()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        LoadProductGroup()
        LoadProductID()
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProductGroup.SelectedIndex <> 0 Then
            LoadProductID()
        End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblError.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblError.Text = "Please select Valid Product"
        Else
            ' LoadGrid(DGSchedule)
            If (ddlProduct.SelectedValue <> "All" And ddlProduct.SelectedValue <> "Select Product") Then
                Dim PgCode As String = ""
                Dim Cmdtext As String = " Select distinct productgroupid from Product where ProductId=" & ddlProduct.SelectedValue & ""
                PgCode = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, Cmdtext)
                ddlProductGroup.SelectedValue = PgCode
                LoadGrid(DGSchedule)
            ElseIf (ddlProduct.SelectedValue <> "All") Then
                ddlProductGroup.SelectedValue = "All"
                LoadGrid(DGSchedule)
            End If

        End If
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblError.Text = ""
    End Sub

    Protected Sub btnSaveApprovals_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSaveApprovals.Click
        Try
            Dim StrUpdate As String = ""
            'For i As Integer = 0 To DGSchedule.Items.Count - 1
            'For Each DataGridRow In DGSchedule.Items
            For Each row As DataGridItem In DGSchedule.Items
                'For Each DataGridItem In DGSchedule.Items
                Dim j As Integer = 3
                For k As Integer = 0 To 6
                    If row.Cells(j).Text <> " " Then 'coltext.Length > 0 Then
                        'Accepted = DirectCast(row.FindControl("txtAcc" & g), TextBox).Text().Trim()
                        StrUpdate = StrUpdate & " Update CalSignUp Set Accepted=" & IIf(DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Text.Trim() = "Sel", "NULL", "'" & DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Value.Trim() & "'") & ",ModifiedBy=" & Session("LoginId") & ",ModifiedDate=GETDATE() Where MemberID=" & row.Cells(1).Text & " and CONVERT(varchar(15),REPLICATE('0',2-LEN(cast(DATEPART(HOUR,CAST([TIME] AS TIME)) as nvarchar)))+cast(DATEPART(HOUR,CAST([TIME] AS TIME)) as nvarchar)+':'+REPLICATE('0',2-LEN(cast(DATEPART(Minute,CAST([TIME] AS TIME))as varchar)))+cast(DATEPART(Minute,CAST([TIME] AS TIME))as varchar),100)  ='" & row.Cells(j).Text & "' and Day ='" & DGSchedule.Columns(j).HeaderText & "' and EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & ";"
                    Else
                    End If
                    j = j + 4
                Next
            Next
            If SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, StrUpdate) > 0 Then
                lblError.Text = " Scheduled Successfully."
            End If
        Catch ex As Exception
            'Response.Write(ex.ToString())
        End Try

    End Sub
    'Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
    '    Dim Accepted As String = ""
    '    Dim dr As DataRow = CType(Cache("editRow"), DataRow)

    '    If (Not dr Is Nothing) Then
    '        If (Not dr.Item("Accepted") Is DBNull.Value) Then
    '            Accepted = dr.Item("Accepted")
    '        End If
    '        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
    '        ddlTemp = sender
    '        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
    '    Else
    '        Return
    '    End If
    'End Sub
    Protected Sub DGSchedule_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles DGSchedule.PageIndexChanged
        Try
            lblError.Text = ""
            If (ViewState("IsContinue") = False) Then
                pnlCofirm.Visible = True
                ViewState("currentPageNumber") = e.NewPageIndex

            ElseIf (ViewState("IsContinue") = True) Then
                DGSchedule.CurrentPageIndex = e.NewPageIndex
                DGSchedule.EditItemIndex = -1
                LoadGrid(DGSchedule)
                ViewState("IsContinue") = False
            End If
        Catch ex As Exception

        End Try

    End Sub
    'Protected Sub DGSchedule_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles DGSchedule.ItemCreated
    '    If e.Item.ItemType <> ListItemType.Header And e.Item.ItemType <> ListItemType.Footer Then
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    For i As Integer = 0 To 6
    '        '        If e.Item.Cells(i + 2).Text = "" Then '.FindControl("lblEditRegDeadLine")) Is Nothing Then
    '        '            CType(e.Item.Cells(5).FindControl("ddlAccept" & i + 1), DropDownList).Enabled = False
    '        '        End If
    '        '    Next
    '        'Next
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    Dim j As Integer = -2
    '        '    For i As Integer = 1 To 7
    '        '        j = j + 4
    '        '        If e.Item.Cells(j).Text = "" Then '.FindControl("lblEditRegDeadLine")) Is Nothing Then
    '        '            CType(e.Item.Cells(j + 3).FindControl("ddlAccept" & i), DropDownList).Enabled = False
    '        '            Response.Write(i & "***" & j & "$$$" & "ddlAccept" & i & "<br />")
    '        '        End If
    '        '        DGSchedule.Columns(j).HeaderText = WeekdayName(WeekDayVal) '.ToString.Substring(0, 3) ' "Sunday"
    '        '        i = i + 1
    '        '        WeekDayVal = WeekDayVal + 1
    '        '    Next
    '        'Next
    '        'For Each row As DataGridItem In DGSchedule.Items
    '        '    'For Each DataGridItem In DGSchedule.Items
    '        '    Dim j As Integer = 2
    '        '    For k As Integer = 0 To 6
    '        '        If row.Cells(j).Text = "" Then 'coltext.Length > 0 Then
    '        '            'Accepted = DirectCast(row.FindControl("txtAcc" & g), TextBox).Text().Trim()
    '        '            CType(e.Item.Cells(j + 2).FindControl("ddlAccept" & k + 1), DropDownList).Enabled = False
    '        '            row.Cells(j + 2)..Enabled = False
    '        '            ' StrUpdate = StrUpdate & " Update CalSignUp Set Accepted='" & DirectCast(row.FindControl("ddlAccept" & k + 1), DropDownList).SelectedItem.Value.Trim() & "' Where MemberID=" & row.Cells(0).Text & " and CONVERT(varchar(15),CAST(TIME AS TIME),100) ='" & row.Cells(j).Text & "' and Day ='" & DGSchedule.Columns(j).HeaderText & "' and EventYear=" & ddlEventYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Semester=" & ddlPhase.SelectedValue & " and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & ";"
    '        '        Else
    '        '        End If
    '        '        j = j + 4
    '        '    Next
    '        'Next
    '    End If
    'End Sub

    Protected Sub btnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnNo.Click
        Try
            ViewState("IsContinue") = True
            pnlCofirm.Visible = False
            If (Not ViewState("currentPageNumber") Is Nothing) Then
                DGSchedule.CurrentPageIndex = Convert.ToInt32(ViewState("currentPageNumber"))
                DGSchedule.EditItemIndex = -1
                LoadGrid(DGSchedule)
            End If

            ViewState("IsContinue") = False
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnYes.Click
        Try
            ' IsContinue = True
            'IsContinue = False
            ViewState("IsContinue") = False
            pnlCofirm.Visible = False
            btnSaveApprovals_Click(Nothing, Nothing)
            If (Not ViewState("currentPageNumber") Is Nothing) Then
                DGSchedule.CurrentPageIndex = Convert.ToInt32(ViewState("currentPageNumber"))
                DGSchedule.EditItemIndex = -1
                LoadGrid(DGSchedule)
            End If
            ViewState("IsContinue") = False
        Catch ex As Exception

        End Try
    End Sub
    Protected Sub btnExport_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnExport.Click
        Response.Clear()
        Response.AddHeader("content-disposition", "attachment;filename=Prepare_Schedule_" & ddlEventYear.SelectedItem.Value & "_" & ddlEvent.SelectedItem.Text & "_" & ddlProduct.SelectedItem.Text & ".xls")
        Response.Charset = ""
        ' If you want the option to open the Excel file without saving then comment out the line below
        'Response.Cache.SetCacheability(HttpCacheability.NoCache)
        Response.ContentType = "application/vnd.xls"
        Dim stringWrite As New System.IO.StringWriter()
        Dim htmlWrite As System.Web.UI.HtmlTextWriter = New HtmlTextWriter(stringWrite)
        Dim dgexport As New DataGrid
        Dim dt As DataTable = LoadTable()
        dgexport.DataSource = dt
        dgexport.DataBind()
        'LoadGrid(dgexport)
        dgexport.RenderControl(htmlWrite)
        Response.Write(stringWrite.ToString())
        Response.[End]()
    End Sub
    Function LoadTable() As DataTable
        Dim dt1 As DataTable = New DataTable()
        DGSchedule.AllowPaging = False
        DGSchedule.DataBind()
        LoadGrid(DGSchedule)
        DGSchedule.AllowPaging = False
        DGSchedule.DataBind()
        'With dt1.Columns
        dt1 = New DataTable()
        dt1.Columns.Add("Ser#", Type.GetType("System.String"))
        dt1.Columns.Add("CoachName", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(3).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S1", Type.GetType("System.String"))
        dt1.Columns.Add("P1", Type.GetType("System.String"))
        dt1.Columns.Add("A1", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(7).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S2", Type.GetType("System.String"))
        dt1.Columns.Add("P2", Type.GetType("System.String"))
        dt1.Columns.Add("A2", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(11).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S3", Type.GetType("System.String"))
        dt1.Columns.Add("P3", Type.GetType("System.String"))
        dt1.Columns.Add("A3", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(15).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S4", Type.GetType("System.String"))
        dt1.Columns.Add("P4", Type.GetType("System.String"))
        dt1.Columns.Add("A4", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(19).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S5", Type.GetType("System.String"))
        dt1.Columns.Add("P5", Type.GetType("System.String"))
        dt1.Columns.Add("A5", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(23).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S6", Type.GetType("System.String"))
        dt1.Columns.Add("P6", Type.GetType("System.String"))
        dt1.Columns.Add("A6", Type.GetType("System.String"))

        dt1.Columns.Add(DGSchedule.Columns(27).HeaderText, Type.GetType("System.String"))
        dt1.Columns.Add("S7", Type.GetType("System.String"))
        dt1.Columns.Add("P7", Type.GetType("System.String"))
        dt1.Columns.Add("A7", Type.GetType("System.String"))

        ' End With

        Dim row As DataGridItem
        Dim drow As DataRow
        Dim i As Integer = 0

        For Each row In DGSchedule.Items  '.Rows
            ' For i As Integer = 0 To DGSchedule.Items.Count - 1
            drow = dt1.NewRow()
            i += 1
            drow("Ser#") = i
            drow("CoachName") = row.Cells(2).Text 'row.Cells("chk").Value

            drow(DGSchedule.Columns(3).HeaderText) = row.Cells(3).Text
            drow("S1") = row.Cells(4).Text
            drow("P1") = row.Cells(5).Text
            Dim ddlAccept1 As DropDownList = TryCast(row.FindControl("ddlAccept1"), DropDownList)
            Dim Str1 As String = ddlAccept1.SelectedItem.Text

            If Str1 <> "Y" Then
                Str1 = String.Empty
            End If
            drow("A1") = Str1

            drow(DGSchedule.Columns(7).HeaderText) = row.Cells(7).Text
            drow("S2") = row.Cells(8).Text
            drow("P2") = row.Cells(9).Text
            Dim ddlAccept2 As DropDownList = TryCast(row.FindControl("ddlAccept2"), DropDownList)
            Dim Str2 As String = ddlAccept2.SelectedItem.Text
            If Str2 <> "Y" Then
                Str2 = String.Empty
            End If
            drow("A2") = Str2

            drow(DGSchedule.Columns(11).HeaderText) = row.Cells(11).Text
            drow("S3") = row.Cells(12).Text
            drow("P3") = row.Cells(13).Text
            Dim ddlAccept3 As DropDownList = TryCast(row.FindControl("ddlAccept3"), DropDownList)
            Dim Str3 As String = ddlAccept3.SelectedItem.Text
            If Str3 <> "Y" Then
                Str3 = String.Empty
            End If
            drow("A3") = Str3

            drow(DGSchedule.Columns(15).HeaderText) = row.Cells(15).Text
            drow("S4") = row.Cells(16).Text
            drow("P4") = row.Cells(17).Text
            Dim ddlAccept4 As DropDownList = TryCast(row.FindControl("ddlAccept4"), DropDownList)
            Dim Str4 As String = ddlAccept4.SelectedItem.Text
            If Str4 <> "Y" Then
                Str4 = String.Empty
            End If
            drow("A4") = Str4


            drow(DGSchedule.Columns(19).HeaderText) = row.Cells(19).Text
            drow("S5") = row.Cells(20).Text
            drow("P5") = row.Cells(21).Text
            Dim ddlAccept5 As DropDownList = TryCast(row.FindControl("ddlAccept5"), DropDownList)
            Dim Str5 As String = ddlAccept5.SelectedItem.Text
            If Str5 <> "Y" Then
                Str5 = String.Empty
            End If
            drow("A5") = Str5

            drow(DGSchedule.Columns(23).HeaderText) = row.Cells(23).Text
            drow("S6") = row.Cells(24).Text
            drow("P6") = row.Cells(25).Text
            Dim ddlAccept6 As DropDownList = TryCast(row.FindControl("ddlAccept7"), DropDownList)
            Dim Str6 As String = ddlAccept6.SelectedItem.Text
            If Str6 <> "Y" Then
                Str6 = String.Empty
            End If
            drow("A6") = Str6

            drow(DGSchedule.Columns(27).HeaderText) = row.Cells(27).Text
            drow("S7") = row.Cells(28).Text
            drow("P7") = row.Cells(29).Text
            Dim ddlAccept7 As DropDownList = TryCast(row.FindControl("ddlAccept7"), DropDownList)
            Dim Str7 As String = ddlAccept7.SelectedItem.Text
            If Str7 <> "Y" Then
                Str7 = String.Empty
            End If
            drow("A7") = Str7

            dt1.Rows.Add(drow)
        Next

        Return dt1

    End Function

    Protected Sub Page_SaveStateComplete(sender As Object, e As EventArgs) Handles Me.SaveStateComplete

    End Sub

    Protected Sub ddlWeekDay_SelectedIndexChanged(sender As Object, e As EventArgs)
        LoadGrid(DGSchedule)
    End Sub
End Class

