Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data

Partial Class DonorFunctions

    Inherits System.Web.UI.Page


    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select COUNT(eventID) from Event where EventId=5 and Status = 'O'") > 0 Then
            hlinkWalkathon.Enabled = True
        End If
    End Sub

    Protected Sub hlinkdonate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkdonate.Click
        Session("EventID") = 11
        Response.Redirect("donor_donate.aspx")
    End Sub

    Protected Sub lnkFundRaising_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lnkFundRaising.Click
        Session("EventID") = 9
        Dim eventid As Integer
        eventid = EventIDCheck.GetEventIDFromDB(9)
        If Session("EventID") = eventid Then
            Response.Redirect("FundRReg.aspx")
        End If
    End Sub

    Protected Sub hlinkWalkathon_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles hlinkWalkathon.Click
        Session("EventID") = 5
        Response.Redirect("Don_athon_selection.aspx?Ev=5")
    End Sub

    
End Class
