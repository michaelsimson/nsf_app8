﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRStatus.aspx.vb" Inherits="FundRStatus"  %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div align="left">
    <asp:HyperLink  CssClass ="btn_02" ID="hlnkMainPage" runat="server"></asp:HyperLink>
</div>
<div align="center" style="width :1000px">
<table border="0" cellpadding ="3"  cellspacing = "0" >
    <tr><td align="center">
    <h2>&nbsp;Fundraising Status </h2>
        <asp:Literal ID="ltrl1" runat="server"></asp:Literal>
        <br />
        <div style="float:right"><asp:literal ID="ltrDiscount" runat="server"></asp:literal></div></td> </tr> 
    <tr><td align="center" >
    <table   border="0" cellpadding ="3" cellspacing = "0" >
       <tr><td align="center" >
    <asp:datagrid id="dgCatalog" CellPadding="7" OnItemDataBound="dgCatalog_ItemDataBound" runat="server" CssClass="GridStyle"  AllowSorting="True"
										AutoGenerateColumns="False" BorderWidth="1px" 
          DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF" 
          BorderStyle="None" GridLines="Horizontal">
			<FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
			<SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
			<AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
			<ItemStyle  HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
			<HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7" ></HeaderStyle>
			<Columns>
	    <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblProductID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label> 											
        <asp:Label runat="server" ID="lblAmount" Visible="false"  CssClass="SmallFont"></asp:Label>
        <asp:Label id="lblFundRFeesID" Visible="false"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label> 											
        <asp:Label id="lblProductCode" runat="server" Visible="false"  CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
       <asp:Label id="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
        </ItemTemplate>
          <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
                <asp:Label runat="server" ID="lblSelfees" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
      </ItemTemplate>												
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
         <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
                    <asp:Label runat="server" ID="lblSelPaymentMode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentMode") %>'></asp:Label>

        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
		<asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
         <asp:Label runat="server" ID="lblQuantity" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Center"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
        </ItemTemplate>
         <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn  HeaderText="Check" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label   id="lblCheckAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label> 											
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn  HeaderText="Credit_Card" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblCreditCardAmt" Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn   HeaderText="In Kind" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false" >
        <ItemTemplate>
        <asp:Label id="lblInKindAmt"   Visible="false"  runat="server" CssClass="SmallFont"></asp:Label>       
        </ItemTemplate>												
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
        <ItemStyle HorizontalAlign="Right"  Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
        <asp:TemplateColumn HeaderText="Payment_Info" HeaderStyle-Wrap="false"  ItemStyle-Wrap="false">
        <ItemTemplate>
        <asp:Label id="lblPaymentDate" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentDate","{0:d}") %>'>		</asp:Label> &nbsp;&nbsp;
        <asp:Label id="lblPaymentMode"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label> 		
        <br /> 											
        <asp:Label id="lblPaymentReference"  runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>'>		</asp:Label> 									
        </ItemTemplate>
        <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle><ItemStyle Wrap="False"></ItemStyle>
        </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Event Date" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblEventDt" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "EventDate", "{0:d}")%>'>		</asp:Label>
                    </ItemTemplate>
                     <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Venue" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblChapterCode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Venue")%>'>		</asp:Label>
                    </ItemTemplate>
                     <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true" ></HeaderStyle>
                </asp:TemplateColumn>
        </Columns>
        <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
		</asp:datagrid>
		  
		</td></tr>
        <tr>
            <td>Adults attending: <b><asp:Label ID="lblAdult" runat="server"></asp:Label></b>&nbsp;&nbsp;&nbsp;Children attending: <b><asp:Label ID="lblChildren" runat="server"></asp:Label></b>
</td>
        </tr>

		<%--<tr><td align = "right" >
		<table cellpadding="2" cellspacing = "0" border ="0"><tr><td> Check </td><td>: <asp:Label ID="lblCheck" runat="server"></asp:Label></td>
		<td> CreditCard </td><td>: <asp:Label ID="lblCredit" runat="server"></asp:Label></td> 
		<td> In Kind </td><td>: <asp:Label ID="lblInKind" runat="server"></asp:Label></td> 
		<td> &nbsp;</td>
		 </tr></table>
		 	
		</td> </tr>--%>
		 
		 </table>
		</td></tr>		
		 </table>
	</div> 
</asp:Content>

