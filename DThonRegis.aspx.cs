﻿using System;
using System.Text;
using System.Collections;
using System.Configuration;
using System.Data;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.IO;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text.RegularExpressions;
using System.Net.Mail;

public partial class DThonRegis : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
       
       // Make sure the logged in

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
       
        if (Session["entryToken"].ToString() == "Parent")
        {
            hlinkParentRegistration.Visible = true;
        }
        else if (Session["entryToken"].ToString() == "Volunteer")
        {
            hlinkVolunteerRegistration.Visible = true;
        }
        else if (Session["entryToken"].ToString() == "Donor")
        {
            hlinkDonorFunctions.Visible = true;
        }

        if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor") 
        {
            Response.Redirect("~/login.aspx?entry=p");
        }

        if (!IsPostBack)
        {
            GetEvents(ddlEvent);
            ltlTitle.Text = "North South Foundation - Excel-a-Thon : Noble Cause Through Brilliant Minds!";

            if (Request.QueryString["Ev"] == null)
            {

            }
            else if (Request.QueryString["Ev"].ToString() == "18")
            {
                GetChapters(ddlChapter);
                ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue("18"));
                ddlRelationShip.SelectedIndex = ddlRelationShip.Items.IndexOf(ddlRelationShip.Items.FindByValue("Child"));
                ddlRelationShip.Enabled = false;
                ddlEvent.Enabled = false;
                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString (), CommandType.Text, "select COUNT(eventID) from Event where EventId=1 and Status = 'O'")) > 0)
                    ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue("1"));                    
                else
                    ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session["ChapterID"].ToString()));
                ddlChapter.Enabled = false;
                //trchapter.Visible = false;
                lblHeading.Text = ddlEvent.SelectedItem.Text;
                getChildren();
            }
            else if (Request.QueryString["Ev"].ToString() == "5") //walka a thon
            {
                if (Session["DonateAThonID"] == null)
                        Response.Redirect("Don_athon_selection.aspx?Ev=5");
                    lblPageCaption.Text = "Noble Cause Through Walk-a-thon";
                    txtHeader.Text = "Please sponsor me for the North South Foundation Walk-a-thon";
                    GetChapters(ddlChapter, "5");
                    ddlEvent.Enabled = false;
                    lblWM.Text = "Registration For";
                    lblShwMinimum.Text = "(Minimum $100)";
                    lblLbl1.Text = "1.Please select self, spouse or child.  For Child, select the specific child, if there is more than one.";
                    ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(Request.QueryString["Ev"].ToString()));
                    RValidAmt.ErrorMessage = "Minimum target should be $100";

                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select  Count(ChildNumber) from Child where Memberid=" + Session["CustIndID"] + "")) == 0)
                        ddlRelationShip.Items.FindByText("Child").Enabled = false;
                    if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "select  Count(AUTOMemberID) from INDSPOUSE where RelationShip = " + Session["CustIndID"] + "")) == 0)
                        ddlRelationShip.Items.FindByText("Spouse").Enabled = false;
            }
            else
            {
                ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(Request.QueryString["Ev"].ToString()));
            }
            if (Request.QueryString["ch"] != null)
            {
                ddlRelationShip.SelectedIndex = ddlRelationShip.Items.IndexOf(ddlRelationShip.Items.FindByText("Child"));
                getChildren();
                GetDetailsforChild(decode(Request.QueryString["ch"].ToString()));
                ddlChild.SelectedIndex = ddlChild.Items.IndexOf(ddlChild.Items.FindByValue(decode(Request.QueryString["ch"].ToString())));   
            }
            else if (Request.QueryString["sp"] != null)
            {
            ddlRelationShip.SelectedIndex = ddlRelationShip.Items.IndexOf(ddlRelationShip.Items.FindByText ("Spouse"));
            DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Top 1 AUTOMemberID as ChildNumber, FIRSTNAME + ' '+ LASTNAME as CNAME from INDSPOUSE where RelationShip = " + Session["CustIndID"] + "");
            ddlChild.DataSource = ds_Children;
            ddlChild.DataBind();
            GetDetailsforChild(decode(Request.QueryString["sp"].ToString()));
            }
            else if (Request.QueryString["slf"] != null)
            {
                ddlRelationShip.SelectedIndex = ddlRelationShip.Items.IndexOf(ddlRelationShip.Items.FindByText("Self"));
                DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Top 1 AUTOMemberID as ChildNumber, FIRSTNAME + ' '+ LASTNAME as CNAME from INDSPOUSE where AutoMemberID = " + Session["CustIndID"] + "");
                ddlChild.DataSource = ds_Children;
                ddlChild.DataBind();
                GetDetailsforChild(decode(Request.QueryString["slf"].ToString()));
            }
            else if (Request.QueryString["Ev"].ToString() == "5")
            {
                try
                {
                    ddlRelationShip.SelectedIndex = ddlRelationShip.Items.IndexOf(ddlRelationShip.Items.FindByText("Self"));
                    DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Top 1 AUTOMemberID as ChildNumber, FIRSTNAME + ' '+ LASTNAME as CNAME from INDSPOUSE where AutoMemberID = " + Session["CustIndID"] + "");
                    ddlChild.DataSource = ds_Children;
                    ddlChild.DataBind();
                    GetDetailsforChild(ddlChild.SelectedValue);
                }
                catch { }
            }
        }
             
    }

    private void GetDetailsforChild(string childnumber)
    {
        hdnWalkMarID.Value = string.Empty;
        if ((ddlEvent.SelectedValue == "18") && (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT count(WalkMarID) FROM WalkMarathon where ChapterID = " + ddlChapter.SelectedValue + " AND  EventID=" + ddlEvent.SelectedValue + " AND MemberID=" + Session["CustIndID"] + " AND WMemberID= " + childnumber + " AND Relationship='" + ddlRelationShip.SelectedValue  + "' and EventYear=Year(GETDATE())")) > 0))
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 * FROM WalkMarathon where ChapterID = " + ddlChapter.SelectedValue + " AND EventID=" + ddlEvent.SelectedValue + " AND MemberID=" + Session["CustIndID"] + " AND WMemberID= " + childnumber + " AND Relationship='" + ddlRelationShip.SelectedValue + "' and EventYear=Year(GETDATE())");
            if (ds.Tables[0].Rows.Count > 0)
            {
                TxtEMail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                txtHeader.Text = ds.Tables[0].Rows[0]["HeaderText"].ToString();
                txtCustom.Text = ds.Tables[0].Rows[0]["MemberContent"].ToString();
                txtAmt.Text = Math.Round(Convert.ToDecimal(ds.Tables[0].Rows[0]["TargetAmount"].ToString()), 0).ToString();
                hdnWalkMarID.Value = ds.Tables[0].Rows[0]["WalkMarID"].ToString();
                hdnImgName.Value = ds.Tables[0].Rows[0]["ImageFileName"].ToString();
                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf (ddlChapter.Items.FindByValue (ds.Tables[0].Rows[0]["ChapterID"].ToString()));
                //Response.Write(hdnWalkMarID.Value);
             }

        }
        else if ((ddlEvent.SelectedValue == "5") && (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT count(WalkMarID) FROM WalkMarathon where DonateAThonCalID = " + Session["DonateAThonID"] + " AND  EventID=" + ddlEvent.SelectedValue + " AND MemberID=" + Session["CustIndID"] + " AND WMemberID= " + childnumber + " AND Relationship='" + ddlRelationShip.SelectedValue + "' and EventYear=Year(GETDATE())")) > 0))
        {
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT TOP 1 * FROM WalkMarathon where DonateAThonCalID = " + Session["DonateAThonID"] + " AND EventID=" + ddlEvent.SelectedValue + " AND MemberID=" + Session["CustIndID"] + " AND WMemberID= " + childnumber + " AND Relationship='" + ddlRelationShip.SelectedValue + "' and EventYear=Year(GETDATE())");
            if (ds.Tables[0].Rows.Count > 0)
            {
                TxtEMail.Text = ds.Tables[0].Rows[0]["Email"].ToString();
                txtHeader.Text = ds.Tables[0].Rows[0]["HeaderText"].ToString();
                txtCustom.Text = ds.Tables[0].Rows[0]["MemberContent"].ToString();
                txtAmt.Text = Math.Round(Convert.ToDecimal(ds.Tables[0].Rows[0]["TargetAmount"].ToString()), 0).ToString();
                hdnWalkMarID.Value = ds.Tables[0].Rows[0]["WalkMarID"].ToString();
                hdnImgName.Value = ds.Tables[0].Rows[0]["ImageFileName"].ToString();
                ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(ds.Tables[0].Rows[0]["ChapterID"].ToString()));
                //Response.Write(hdnWalkMarID.Value);
            }

        }
        else if (ddlEvent.SelectedValue == "18")
        {
            TxtEMail.Text = string.Empty;
            txtHeader.Text = "Noble Cause Through Brilliant Minds!";
            txtCustom.Text = "<p align='justify' style='font-family:Calibri; font-size:14px;'> Hello! I am excited to inform you that I will be participating in regional contest organized by North South Foundation (NSF). Winners in the national round have been featured in the popular media and awarded scholarships.<br /><br />NSF was started over 22 years ago with the purpose of providing educational scholarships to needy children who display academic excellence. NSF funds these scholarships by raising donations in the US through spelling bees and direct donations. The NSF Scholarship program is designed to encourage excellence among the poor children who excel academically but need financial help to attend college. Each scholarship is $250 per student per year. NSF has distributed more than 5,000 scholarships to students who need financial support to pursue their quest for knowledge in engineering, medicine, polytechnic, science and other fields. The scholarship is an annual award and not a one-time payment. The student is eligible for the scholarship until graduation as long as he/she maintains high academic standards. The local chapters in various states of India invite applications from students, screen them and select the neediest students who eventually become NSF scholarship recipients. Scholarship amounts range from INR 5,000 to INR 12,000 per student per year.<br /><br />I have pledged to seek sponsorship to further the noble mission of this charity. My personal goal is to raise a minimum of $300 to provide scholarship for one student for one year. If you would please consider making a tax-deductible donation (Tax ID: 36-3659998) toward my goal of $300 it would be greatly appreciated. 100% of the money goes towards educational scholarships and any amount of money will help. Please help me make a difference in life of a deserving student who aspires to rise from poverty through education.<br /><br />If you have any questions about the contest, NSF or your donation, please feel free to contact our parents or visit the NSF website. Thank you for your kindness.</p>";
            txtAmt.Text = "00";
            hdnWalkMarID.Value = string.Empty;
            hdnImgName.Value = string.Empty;
        }
        else if (ddlEvent.SelectedValue == "5")
        {
            TxtEMail.Text = string.Empty;
            txtHeader.Text = "Please sponsor me for the North South Foundation Walk-a-thon";
            txtCustom.Text = "<p align='justify' style='font-family:Calibri; font-size:14px;'> Please join me in supporting a great cause:  to educate people who have the merit but no financial means.  North South Foundation provides scholarships each year to needy children in India based on academic excellence.  A scholarship of just $250 per student per year goes a long way to help a student pursue their quest for a degree in engineering, medicine, polytechnic, science and other fields.  The funds are sent to local chapters in India who invite applicants, screen them and select the brightest students who need a scholarship to pursue their dream of knowledge and a bright future.  Each scholarship recipient is published in the North South Foundation annual newsletter.  Your donation today is tax-deductible.<br><br>Please help me meet my goal of $Amt  today so that we can provide a bright future for a young student.</p>";
            txtAmt.Text = "00";
            hdnWalkMarID.Value = string.Empty;
            hdnImgName.Value = string.Empty;
        }
    }

    private void getChildren()
    {
        
        if ((Request.QueryString["Ev"] != null) &&  (Request.QueryString["Ev"].ToString() == "18"))
        {
            //select Distinct ChildNumber, FIRST_NAME + ' '+ LAST_NAME as CNAME from Child where MemberID = " + Session["CustIndID"] + " and ChildNumber in (Select ChildNumber from Contestant Where ParentID = " + Session["CustIndID"] + " and PaymentReference is not null and ContestYear = YEAR(GETDATE()) and EventId=1) order by 2
            DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Distinct ChildNumber, FIRST_NAME + ' '+ LAST_NAME as CNAME from Child where MemberID = " + Session["CustIndID"] + "   order by 2"); //in (Select ChildNumber from Contestant Where ParentID = " + Session["CustIndID"] + " and PaymentReference is not null and ContestYear = YEAR(GETDATE()) and EventId=1)
            ddlChild.DataSource = ds_Children;
            ddlChild.DataBind();
        }
        else
        {
            DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Distinct ChildNumber, FIRST_NAME + ' '+ LAST_NAME as CNAME from Child where MemberID = " + Session["CustIndID"] + "");
            ddlChild.DataSource = ds_Children;
            ddlChild.DataBind();
        }
        if ((ddlChild.Items.Count > 0) && (Request.QueryString["ch"] == null))
            GetDetailsforChild(ddlChild.SelectedValue);

    }
    private bool ValidatePhotoFile(int PhotoSize, string PhotoExtn)
    {
        bool bResult = false;
        if (PhotoSize < 500 || PhotoSize > 2048000)
        {
            lblMessage.Text = "Photo size doesn't fall within the acceptable range.(500 Kb - 2 MB)";
            bResult = false;
        }
        else
            bResult = true;
        if (PhotoExtn.ToLower() == "jpg" || PhotoExtn.ToLower() == "gif" || PhotoExtn.ToLower() == "png")

            bResult = true;
        else
        {
            lblMessage.Text = "Photo file name is not of acceptable type.(jpg or gif or png)";
            bResult = false;
        }
        return bResult;
    }
    private void InsertWalkMarathon()
    {
        String SQLstr;
        string WalkMarathonID;
        if (fu.HasFile)
        {
            if (ValidatePhotoFile(fu.FileBytes.Length, fu.FileName.Split(new char[] { '.' })[1].ToString()))
            {
                try
                {
                    string filename =  DateTime.Now.Year.ToString() + "_Walkathon_" + Session["CustIndID"] + "_" + ddlChild.SelectedValue  + "." + fu.FileName.Split(new char[] { '.' })[1].ToString();
                   fu.SaveAs(string.Concat(Server.MapPath("walkmarathon/"), filename));
                    hdnImgName.Value = filename;
                    lblMessage.Text = "File Uploaded: " + fu.FileName;
                    if (hdnWalkMarID.Value.Length == 0)
                    {
                        SQLstr = "INSERT INTO WalkMarathon(EventID,EventYear,ChapterID, TargetAmount, ImageFileName, MemberID,WMemberID,Email,RelationShip, HeaderText, MemberContent, CreateDate, CreatedBy";
                        if (Request.QueryString["Ev"].ToString() == "5")
                            SQLstr = SQLstr + ",DonateAThonCalID";
                        SQLstr = SQLstr + ") Values (" + ddlEvent.SelectedValue + ",Year(GetDate())," + ddlChapter.SelectedValue + "," + txtAmt.Text + ",'" + filename + "'," + Session["CustIndID"] + "," + ddlChild.SelectedValue.ToString() + ",'" + TxtEMail.Text + "','" + ddlRelationShip.SelectedItem.Text + "','" + txtHeader.Text.Replace("'", "''") + "','" + txtCustom.Text.Replace("'", "''").Replace("$Amt", txtAmt.Text) + "'";
                        SQLstr = SQLstr + ",Getdate()," + Session["CustIndID"];
                        if (Request.QueryString["Ev"].ToString() == "5")
                            SQLstr = SQLstr + "," + Session["DonateAThonID"];
                        SQLstr = SQLstr + "); Select Scope_Identity()";
                        //Response.Write(SQLstr.ToString()); 
                        WalkMarathonID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr).ToString() ;
                    }
                    else
                    {
                        WalkMarathonID = hdnWalkMarID.Value;
                        SQLstr = "Update WalkMarathon set EventID=" + ddlEvent.SelectedValue + ",ChapterID=" + ddlChapter.SelectedValue + ", TargetAmount=" + txtAmt.Text + ", ImageFileName='" + filename + "',Email='" + TxtEMail.Text + "', HeaderText='" + txtHeader.Text.Replace("'", "''") + "', MemberContent='" + txtCustom.Text.Replace("'", "''") + "', ModifyDate=GetDate(), ModifiedBy=" + Session["CustIndID"] + " where WalkMarID=" + WalkMarathonID;
                        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr);
                    }
                    String txt = EmailBodytxt(WalkMarathonID, "http://" + Request.Url.Host + Request.ApplicationPath + "/Don_athon_custom.aspx?NSFDONATE=" + encode(WalkMarathonID.ToString()));
                    if ((txtTSendEmails.Text.Length > 0) && (WalkMarathonID.Length > 0))
                    {
                        String[] strEmailAddressList;
                        String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                        Match EmailAddressMatch;
                        strEmailAddressList = txtTSendEmails.Text.Replace (',',';').Split(';');
                        foreach (object item in strEmailAddressList)
                                {
                                    EmailAddressMatch = Regex.Match(item.ToString ().Trim(), pattern);
                                    if (EmailAddressMatch.Success)
                                        sentemail(txtHeader.Text, txt, item.ToString().Trim(), ddlChild.SelectedItem.Text + "<" + TxtEMail.Text + ">");
                                }
                    }
                    if (hdnWalkMarID.Value.Length == 0)
                        sentemail("Thanks for Registering", txt, TxtEMail.Text, "nsfcontests@gmail.com");
                    Response.Redirect("Don_athon_custom.aspx?NSFDONATE=" + encode(WalkMarathonID.ToString()));
                }
                catch (Exception err)
                {
                    lblMessage.Text = err.Message;                    
                }
            }

        }
        else
        {
            if (hdnWalkMarID.Value.Length == 0)
            {
                SQLstr = "INSERT INTO WalkMarathon(EventID,EventYear, ChapterID, TargetAmount, ImageFileName, MemberID,WMemberID,Email,RelationShip, HeaderText, MemberContent, CreateDate, CreatedBy";
                if (Request.QueryString["Ev"].ToString() == "5")
                    SQLstr = SQLstr + ",DonateAThonCalID";
                SQLstr = SQLstr + ") Values (" + ddlEvent.SelectedValue + ",Year(GetDate())," + ddlChapter.SelectedValue + "," + txtAmt.Text + ",'testimg.jpg'," + Session["CustIndID"] + "," + ddlChild.SelectedValue.ToString() + ",'" + TxtEMail.Text + "','" + ddlRelationShip.SelectedItem.Text + "','" + txtHeader.Text.Replace("'", "''") + "','" + txtCustom.Text.Replace("'", "''").Replace("$Amt", txtAmt.Text) + "'";
                //SQLstr = SQLstr + ",Getdate()," + Session["CustIndID"] + "); Select Scope_Identity()";
                SQLstr = SQLstr + ",Getdate()," + Session["CustIndID"];
                if (Request.QueryString["Ev"].ToString() == "5")
                    SQLstr = SQLstr + "," + Session["DonateAThonID"];
                SQLstr = SQLstr + "); Select Scope_Identity()";
                //Response.Write(SQLstr.ToString()); 
                WalkMarathonID = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr).ToString();
            }
            else
            {
                WalkMarathonID = hdnWalkMarID.Value;
                SQLstr = "Update WalkMarathon set EventID=" + ddlEvent.SelectedValue + ",ChapterID=" + ddlChapter.SelectedValue + ", TargetAmount=" + txtAmt.Text + ",Email='" + TxtEMail.Text + "', HeaderText='" + txtHeader.Text.Replace("'", "''") + "', MemberContent='" + txtCustom.Text.Replace("'", "''") + "', ModifyDate=GetDate(), ModifiedBy=" + Session["CustIndID"] + " where WalkMarID=" + WalkMarathonID;
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, SQLstr);
            }
            String txt;
            if ((txtTSendEmails.Text.Length > 0) && (WalkMarathonID.Length > 0))
            {
                String[] strEmailAddressList;
                String pattern = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
                Match EmailAddressMatch;
                strEmailAddressList = txtTSendEmails.Text.Replace(',', ';').Split(';'); 
                //Response.Write(txt);
               String MId;
                foreach (object item in strEmailAddressList)
                {
                    EmailAddressMatch = Regex.Match(item.ToString().Trim(), pattern);
                    if (EmailAddressMatch.Success)
                    {
                        MId = SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, "Insert Into DonateAThonMailStatus(Email, CreateDate, WalkMarID, Status) Values ('" + item.ToString().Trim() + "',GetDate()," + WalkMarathonID + ",'N'); Select Scope_Identity()").ToString();
                        txt = EmailBodytxt(WalkMarathonID, "http://" + Request.Url.Host + Request.ApplicationPath + "/Don_athon_custom.aspx?NSFDONATE=" + encode(WalkMarathonID.ToString())+ "&MId="+encode(MId));
                        sentemail(txtHeader.Text, txt, item.ToString().Trim(), ddlChild.SelectedItem.Text + "<" + TxtEMail.Text + ">");
                    }
                }
            }
            txt = EmailBodytxt(WalkMarathonID, "http://" + Request.Url.Host + Request.ApplicationPath + "/Don_athon_custom.aspx?NSFDONATE=" + encode(WalkMarathonID.ToString()));
            if (hdnWalkMarID.Value.Length == 0)
                sentemail("Thanks for Registering", txt, TxtEMail.Text, "nsfcontests@gmail.com");
           Response.Redirect("Don_athon_custom.aspx?NSFDONATE=" + encode(WalkMarathonID.ToString()));
        }        
    }
    protected string encode(String inputText)
    {
        byte[] bytesToEncode = Encoding.UTF8.GetBytes(inputText);
        string encodedText = Convert.ToBase64String(bytesToEncode);
        return encodedText;
    }
     protected string decode(String inputText )
     {
         try
         {
             byte[] decodedBytes = Convert.FromBase64String(inputText);
             String decodedText = Encoding.UTF8.GetString(decodedBytes);
             return decodedText;
         }
         catch
         {
             Response.Redirect("userfunctions.aspx");
             return "";
         }
     }
    protected string EmailBodytxt(string Str,string lnk)
    {
        string emailtxt="";
        if (Request.QueryString["Ev"].ToString() == "5")
        {
            String[] srt = ddlChapter.SelectedItem.Text.Split('-');
            emailtxt = "<html><body>Dear Sponsor, <BR><BR>I am walking in this year's NSF " + ddlEvent.SelectedItem.Text + " on " + srt[1].ToString() + " benefiting scholarships to needy children in India to pursue higher education.  I have a set personal goal of raising enough money to provide at least one scholarship this year.<br><br>North South Foundation (NSF) provides scholarships each year to needy children in India based on academic excellence.  A scholarship of just $250 per student per year goes a long way to help a student pursue their quest for a degree in engineering, medicine, polytechnic, science and other fields.  The funds are sent to local chapters in India who invite applicants, screen them and select the brightest students who need a scholarship to pursue their dream of knowledge and a bright future. <br><br>You can help me by making a donation. Click the link below to visit my personal Webpage where you can make a secure online, tax deductible donation.  Donate as little or as much as you can.  Every penny counts. You may choose a dollar amount via the website and pay online via credit card. Or you may choose to register online to pledge a specific dollar amount and provide cash or a check made to 'North South Foundation' directly to me.<br><br>Please feel free to forward this message on to your friends and family who also might want to a make a difference. Thank you, in advance, for your support.<br><br>";
            emailtxt = emailtxt + "Follow this link to visit my personal web page and help me in my efforts to support North South Foundation:";
            emailtxt = emailtxt + "<a href='" + lnk + "' target='_blank'>" + lnk + "</a><br><br>Thank you,<br>" + ddlChild.SelectedItem.Text;
            emailtxt = emailtxt + "</html></body>";

        }
        else
        {
           
            emailtxt = " <html><body><form action='" + lnk + "' method='post'>     <table  cellpadding = '0' cellspacing = '0'  border='0' style='width:100%; text-align:center'>  <tr><td align='center'>  <table  border='1'  cellpadding = '0' cellspacing = '0' bordercolor='#189A2C' style='width:1000px; text-align:center'>  <tr><td>  <table  cellpadding = '0' cellspacing = '0'  border='0' style='width:1000px; text-align:center'>  <tr><td>   <table width='100%' border='0' cellspacing='0' cellpadding='0'>    <tr>      <td width='24%' valign='top' align='right'  >      <img src='http://northsouth.org/App9/images_new/img_01.jpg' alt='' width='100%' height='144' border='0' usemap='#Map' />        <map name='Map' id='Map'>          <area shape='circle' coords='105,81,61' href='http://www.northsouth.org/' />        </map></td>      <td width='76%' valign='top'><table width='100%' border='0' cellspacing='0' cellpadding='0'>        <tr>          <td><table width='100%' border='0' cellspacing='0' cellpadding='0'>     <tr>       <td width='51%'  background='http://northsouth.org/App9/images_new/topbg_1.jpg'><img src='http://northsouth.org/App9/images_new/img_02.jpg' alt='' width='399' height='109' /></td>       <td width='49%' valign='top' background='http://northsouth.org/App9/images_new/img_03.jpg'><table width='89%' border='0' cellspacing='0' cellpadding='0'>         <tr>           <td width='30%'>&nbsp;</td>           <td width='70%'>           <table width='95%' height='31' border='0' align='center' cellpadding='0' cellspacing='0'>        <tr>          <td valign='middle' style='padding-left:10px; padding-top:5px'>             </td>        </tr>           </table></td>         </tr>         ";
            emailtxt = emailtxt + " <tr><td colspan ='2' align='center' ><span style='font-family:Arial Rounded MT Bold; font-size:25pt; color:White'>   " + ddlEvent.SelectedItem.Text + " </span></td></tr>       </table></td>     </tr>          </table></td>        </tr>        <tr>          <td height='35' valign='middle' background='http://northsouth.org/App9/images_new/menubg.jpg'>     <table border = '0' cellpadding = '1' cellspacing = '0' width='100%'>  <tr><td style='text-align:center; width :10px' >  </td><td style='vertical-align:middle; text-align:left;'><span style='font-family:Arial Rounded MT Bold; font-size:14pt; color:White'>   " + txtHeader.Text + " </span>  </td><td style='text-align:center; width :10px' ></td></tr>   </table>     </td>        </tr>      </table></td>    </tr>         </table>  </td></tr>          <tr><td align='center' style='height :1px'></td></tr><tr><td align='center'>";
            emailtxt = emailtxt + "<br><br> If this page is not visible please <a href='" + lnk + "' target='_blank'> Click here</a> to view this page.</td></tr><tr><td align='center' valign='top'>       <table border = '0' cellpadding = '3' cellspacing = '0' width='100%'>  <tr>    <td style='vertical-align:middle; text-align:left;text-align :center' ><table border = '0' cellpadding = '3' cellspacing = '0' > <tr><td style='vertical-align:middle; text-align:Center;' colspan='2'> <img height='250' width='250' src='http://" + Request.Url.Host + Request.ApplicationPath + "/walkmarathon/";
            if (hdnImgName.Value.Length > 0)
                emailtxt = emailtxt + hdnImgName.Value;
            else
                emailtxt = emailtxt + "testimg.jpg";
            emailtxt = emailtxt + "' /></td></tr><tr><td style='vertical-align:middle; text-align:left;' colspan='2' ><br><br>      Fundraising target  :      $" + Math.Round(Convert.ToDecimal(txtAmt.Text), 2).ToString() + "   <br><br> </td>    </tr>       <tr>    <td style='vertical-align:middle; text-align:Center;' colspan='2'>    <input type='submit' value='Sponsor Now'  />         </td>    </tr> </table></td>   <td style='vertical-align:top; text-align:left; width:75%' class='style3' >     " + txtCustom.Text.Replace('"', '\'') + " <div align='left'>    Sincerely,&nbsp;&nbsp;&nbsp;&nbsp;<br />  " + ddlChild.SelectedItem.Text + "     </div>    </td>  <td style='font-family:Calibri; font-size:14px;' align='center'>       </td>  </tr>       </table>      </td> </tr>        ";
            emailtxt = emailtxt + "  <tr><td align='left' style='font-family:Calibri; font-size:14px;'>  <div style='width:80%; text-align:justify;font-family:Tahoma; font-size:12px; font-weight :normal;padding:5px'> <br><br>  <b>Note: (for Payment Please press 'Sponsor Me' button)</b><br />      1. Credit Card payment Needs Login so If you are NSF member you can use the existing user Email and Password<br /> 2. Cash and Check payment needs to register, you can enter the amount and Pay/Give the check to the Participant.<br /><br /><br />       </div>  </td> </tr>    <tr><td align='right'  bgcolor='#99CC33' style='height:25px; vertical-align:middle; color:#FFF' class='style2'>© North South Foundation. All worldwide rights reserved.&nbsp;&nbsp;&nbsp;     </td>    </tr>  </table></td> </tr> </table> </td> </tr> </table> </form>     </body>     </html>";
          }
        return emailtxt;
    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        // InsertWalkMarathon
        try
        {
           String pattern  = "^[a-zA-Z0-9][\\w\\.-\\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\\w\\.-]*[a-zA-Z0-9]\\.[a-zA-Z][a-zA-Z\\.]*[a-zA-Z]$";
           Match EmailAddressMatch  = Regex.Match(TxtEMail.Text, pattern);
             if (ddlEvent.SelectedValue == "-1")
                lblMessage.Text = "Please select Event";
            else if (ddlChapter.SelectedValue == "-1")
                lblMessage.Text = "Please select Chapter";
             else if ((TxtEMail.Text.Length < 3))
                 lblMessage.Text = "Enter Email";
             else if (!EmailAddressMatch.Success)
                 lblMessage.Text = "Email format is not correct";
            else if (txtAmt.Text.Length < 1)
                lblMessage.Text = "Please Enter Target Amount";
            else if ((Convert.ToDecimal(txtAmt.Text) < 250) && (Request.QueryString["Ev"].ToString() == "18"))
                lblMessage.Text = "Minimum target should be $250";
             else if ((Convert.ToDecimal(txtAmt.Text) < 100) && (Request.QueryString["Ev"].ToString() == "5"))
                 lblMessage.Text = "Minimum target should be $100";
            else if (Convert.ToDecimal(txtAmt.Text.ToString()) < 1)
                lblMessage.Text = "Please Enter  Target Amount";
            else if ((txtHeader.Text.Length < 5))
                lblMessage.Text = "Please Enter Subject";
            else if ((txtCustom.Text.Length < 10))
                 lblMessage.Text = "Please Enter some more body Text";
            else
             {
                 txtCustom.Text = txtCustom.Text.Replace("$300", "$" + Math.Round(Convert.ToDecimal(txtAmt.Text),2));
                 InsertWalkMarathon();
             }

        }
        catch (Exception ex)
        {
            lblMessage.Text = "Please check the Inputs";
           //Response.Write(ex.ToString()); 
        }
    }
    private void GetEvents(DropDownList ddlObject)
    {
        DataSet dsEvent = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Name,EventId  from Event where EventID in (1,2,3,5,9,14,15,18) Group by Name,EventId");
        ddlObject.DataSource = dsEvent;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "EventId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetChapters(DropDownList ddlObject)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetChapterAll");
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select Chapter", "-1"));
        ddlObject.SelectedIndex = 0;
    }

    private void GetChapters(DropDownList ddlObject,String Event)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "SELECT   C.ChapterID,C.ChapterCode + ' - '+ CONVERT(VARCHAR(11), D.EventDate, 107) as ChapterCode  FROM DonateAThonCal D Inner Join Chapter C On C.ClusterID=D.ClusterID WHERE  (D.EndDate >= GETDATE()) and D.DonateAThonID=" + Session["DonateAThonID"] + " and D.EventID=" + Event);
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        //ddlObject.Items.Insert(0, new ListItem("Select Chapter", "-1"));
        //ddlObject.SelectedIndex = 0;
    }

    private void sentemail(string sSubject, string sBody, string strMailTo, string strMailNw)
    {
        string sFrom = strMailNw;
       // string host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost");
        MailMessage mail = new MailMessage(sFrom, strMailTo, sSubject, sBody);

        SmtpClient client = new SmtpClient();
       // client.Host = host;
        mail.IsBodyHtml = true;
        try
        {
            client.Send(mail);
        }
        catch (Exception e)
        {
           
        }
    }
    protected void ddlChild_SelectedIndexChanged(object sender, EventArgs e)
    {
        //GetDetailsforChild(ddlChild.SelectedValue);
        Response.Redirect("DThonRegis.aspx?Ev=" + Request.QueryString["Ev"].ToString() + "&ch=" + encode(ddlChild.SelectedValue));  
    }
    protected void ddlRelationShip_SelectedIndexChanged(object sender, EventArgs e)
    {
        //select automemberid, Firstname +' '+ Lastname from IndSpouse where Automemberid=''
        //select automemberid, Firstname +' '+ Lastname from IndSpouse where RelationShip=''
        //Response.Write(ddlRelationShip.SelectedValue);
        if (ddlRelationShip.SelectedValue == "Child")
        {
            getChildren();
            Response.Redirect("DThonRegis.aspx?Ev=" + Request.QueryString["Ev"].ToString() + "&ch=" + encode(ddlChild.SelectedValue));
        }
        else if (ddlRelationShip.SelectedValue == "Self")
        {
            DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Top 1 AUTOMemberID as ChildNumber, FIRSTNAME + ' '+ LASTNAME as CNAME from INDSPOUSE where AUTOMemberID = " + Session["CustIndID"] + "");
            ddlChild.DataSource = ds_Children;
            ddlChild.DataBind();
            Response.Redirect("DThonRegis.aspx?Ev=" + Request.QueryString["Ev"].ToString() + "&slf=" + encode(ddlChild.SelectedValue));
        }
        else
        {
            DataSet ds_Children = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Top 1 AUTOMemberID as ChildNumber, FIRSTNAME + ' '+ LASTNAME as CNAME from INDSPOUSE where RelationShip = " + Session["CustIndID"] + "");
            ddlChild.DataSource = ds_Children;
            ddlChild.DataBind();
            Response.Redirect("DThonRegis.aspx?Ev=" + Request.QueryString["Ev"].ToString() + "&sp=" + encode(ddlChild.SelectedValue));
        }


    }
}
