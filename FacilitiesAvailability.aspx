﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FacilitiesAvailability.aspx.vb" Inherits="FacilitiesAvailability" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<div align="left">
        <asp:HyperLink  ID="hlnkMainPage" NavigateUrl="~/VolunteerFunctions.aspx"  CssClass="btn_02" runat="server">Back To Volunteer Functions</asp:HyperLink>
</div>
 <table id="MainTable" runat="server" align="left"><tr><td>
   <table id="FacilityTable" runat="server" align="center" class="title04">
         <tr><td> Facilities Availability  </td></tr>
  </table>
  <table id="SelTable" runat="server" border="1" align="center">
      <tr> 
            <td align="left" width="100px">  ContestYear </td>
                  <td> <asp:DropDownList ID="ddlYear" DataTextField="Year" DataValueField="Year"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 20px"></td>
             <td width="100px"> Event</td>
                  <td><asp:DropDownList ID="ddlEvent"  DataTextField="Name" DataValueField="EventId"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 20px"></td>
             <td width="100px">Chapter</td>
                  <td><asp:DropDownList ID="ddlChapter"  DataTextField="Chapter" DataValueField="ChapterId"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 20px"></td>
             <td width="100px">Purpose</td>
                  <td><asp:DropDownList ID="ddlPurpose"  DataTextField="Purpose" DataValueField="Purpose"  AutoPostBack="true" runat="server" width="150px"></asp:DropDownList></td> <td align="left" style="width: 20px"></td>
      </tr>
  </table><br/>
  </td>
  </tr><tr><td align="center" ><asp:Label ForeColor ="Red" ID="lblErrorMsg" runat="server" Visible="True" Text=""></asp:Label></td></tr>
  <tr><td>
  <table id="FaciMainTable" visible="true" border="1" align="center"><tr><td>
    <table id ="FaciSeltable" visible="true" align="center">
           <tr id="TrProductGroup" runat="server">
                   <td align="left" width="100px">ProductGroup </td><td>
                   <asp:DropDownList ID="ddlProductGroup" DataTextField="ProductGroup" DataValueField="ProductGroupId"  AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td> <td align="left" style="width: 77px"></td>
           </tr><tr id="TrPhase" runat="server">
                   <td id="TdPhase" runat="server" visible="true" width="100px"> Phase </td><td> 
                   <asp:DropDownList ID="ddlPhase" AutoPostBack ="true"  DataTextField="Phase" DataValueField="Phase" runat="server" Width="125px" visible="true"> </asp:DropDownList></td>
           </tr><tr id="TrDay" runat="server">
                   <td runat="server" visible="true" width="100px"> Day </td><td> 
                   <asp:DropDownList ID="ddlDay" AutoPostBack ="true"  DataTextField="Day" DataValueField="Day" runat="server" Width="125px" visible="true"> </asp:DropDownList></td>
           </tr><tr> 
                   <td width="100px"> BuildingID</td><td> 
                   <asp:DropDownList ID="ddlBldgID" AutoPostBack="true" runat="server" width="125px"></asp:DropDownList></td>
           </tr><tr> 
                    <td width="100px">BuildingName</td><td> 
                    <asp:DropDownList ID="ddlBldgName"  AutoPostBack="true" runat="server" width="125px"></asp:DropDownList> </td> 
           </tr><tr> 
                    <td width="100px">RoomNumber</td><td> 
                    <asp:DropDownList ID="ddlRoomNo"  AutoPostBack="true" runat="server" width="125px"></asp:DropDownList> </td> 
           </tr><tr> 
                    <td width="100px"> Floor</td><td> 
                    <asp:DropDownList ID="ddlFloor" runat="server" width="125px"></asp:DropDownList> </td> 
           </tr><tr> 
                    <td width="100px">Capacity</td><td> 
                    <asp:TextBox ID="txtCapacity" runat="server" width="120px"></asp:TextBox> </td> 
            </tr>
          </table>
       </td><td>  
    <table id="ContestsTable"><tr><td align="center" colspan="5" class="title04"> <asp:Label ID="lblHeading"  runat="server" width="125px" Text=""></asp:Label></td></tr> 
      <tr><td  colspan="2" align="center" class="title04">Available</td><td align="center" class="title04">Required</td><td align="center" class="title04">To be Rented</td></tr>
        <tr id="TrPodium" runat="server"> 
          <td align="left" width="100px">  Podium  </td>
            <td> <asp:TextBox ID="txtPodiumAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblPodiumReq"  runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtPodium" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrPodiumMic" runat="server"> 
          <td align="left" width="100px">  PodiumMic </td>
            <td> <asp:TextBox ID="txtPodiumMicAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblPodiumMicReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtPodiumMic" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrPodiumLaptop" runat="server"> 
          <td align="left" width="100px">  PodiumLaptop </td>
            <td> <asp:TextBox ID="txtPodiumLaptopAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblPodiumLaptopReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtPodiumLaptop" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
       <%-- <tr id="TrRoomGuide" runat="server">
          <td align="left" width="100px">  RoomGuides  </td>
            <td> <asp:TextBox ID="txtRoomGuideavl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblRoomGuideReq" runat="server" width="100px"></asp:Label ></td>
                <td> <asp:TextBox ID="txtRoomGuide" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>--%>
        <%--<tr id= "TrRoomMc" runat="server"> 
          <td align="left" width="100px">  RoomMc </td>
            <td> <asp:TextBox ID="txtRoomMcAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblRoomMcReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtRoomMc" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>--%>
       <%-- <tr id="TrProctor" runat="server"> 
          <td align="left" width="100px">  Proctor </td>
            <td> <asp:TextBox ID="txtProctorAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblProctorReq" runat="server" width="100px"> </asp:Label></td>
                <td> <asp:TextBox ID="txtProctor" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>--%>
        <tr id="TrProjector" runat="server"> 
          <td align="left" width="100px">  Projector  </td>
            <td><asp:TextBox ID="txtProjectorAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblProjectorReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtProjector" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrJudgesTable" runat="server"> 
          <td align="left" width="100px">  JudgesTable  </td>
            <td> <asp:TextBox ID="txtJTableAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblJTableReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtJTable" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrJudgeChairs" runat="server"> 
          <td align="left" width="100px">  JudgeChairs  </td>
            <td> <asp:TextBox ID="txtJChairAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblJChairReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtJChair" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrCJMic" runat="server"> 
          <td align="left" width="100px">  CJMic  </td>
            <td> <asp:TextBox ID="txtCJMicAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblCJMicReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtCJMic" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrChildMic" runat="server"> 
          <td align="left" width="100px">  ChildMic  </td>
            <td> <asp:TextBox ID="txtChMicAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblChMicReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtChMic" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrAudioMixer" runat="server"> 
          <td align="left" width="100px">  AudioMixer  </td>
            <td> <asp:TextBox ID="txtAMixerAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblAMixerReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtAMixer" DataTextField="Name" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrChildMicStand" runat="server"> 
          <td align="left" width="100px">  ChildMicStand  </td>
            <td> <asp:TextBox ID="txtChMicStandAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblChMicStandReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtChMicStand" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrDesktopMicStand" runat="server"> 
          <td align="left" width="100px"> DesktopMicStand  </td>
            <td> <asp:TextBox ID="txtDsMcStandAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblDsMcStandReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtDsMcStand" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrAudioWire" runat="server"> 
          <td align="left" width="100px">  AudioWires  </td>
            <td> <asp:TextBox ID="txtAWiresAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblAWiresReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtAWires" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrExtraMikes" runat="server"> 
          <td align="left" width="100px">  ExtraMics  </td>
            <td> <asp:TextBox ID="txtExtraMicAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblExtraMicReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtExtraMic" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrLaptop" runat="server">
          <td align="left" width="100px">  Laptop   </td>
            <td> <asp:TextBox ID="txtLaptopAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblLaptopReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtLaptop" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrTable" runat="server"> 
          <td align="left" width="100px"> Table  </td>
            <td> <asp:TextBox ID="txtTableAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblTableReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtTable" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrChair" runat="server"> <td align="left" width="100px">  Chairs  </td>
          <td> <asp:TextBox ID="txtChairAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
            <td> <asp:Label ID="lblChairReq" runat="server" width="100px"></asp:Label></td>
              <td> <asp:TextBox ID="txtChair" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                </tr>
        <%--<tr id="TrMic" runat="server"> 
          <td align="left" width="100px">  MicroPhone  </td>
            <td> <asp:TextBox ID="txtMicAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
              <td> <asp:Label ID="lblMicReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtMic" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>--%>
        <tr id="TrMicStand" runat="server"> 
          <td align="left" width="100px">  MicStand  </td>
            <td> <asp:TextBox ID="txtMicStandAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblMicStandReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtMicStand" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
        <tr id="TrInternetAccess" runat="server"> 
          <td align="left" width="100px">  InternetAccess  </td>
            <td> <asp:TextBox ID="txtIntAccessAvl" runat="server" width="100px" Enabled="false"></asp:TextBox></td> 
              <td> <asp:Label ID="lblIntAccessReq" runat="server" width="100px"></asp:Label></td>
                <td> <asp:TextBox ID="txtIntAccess" runat="server" width="100px" Enabled="false"></asp:TextBox></td>
                  </tr>
           </table >
     </td>
   </tr>
 </table>
 </td>
 </tr>
<tr><td>
 <table id="AddUpdateTable" runat="server" align="center"><tr><td></td></tr> 
   <tr><td>
      <asp:Button ID="BtnAddUpdate" OnClick="BtnAddUpdate_Click" runat="server" Text="Add/Update" />
         <asp:Button ID="BtnCancel"  OnClick="BtnCancel_Click" runat="server"  Text="Cancel" />
           </td></tr>
              <tr><td align="center" ><asp:Label ForeColor ="Red" ID="lblAddUpdate" runat="server" Visible="True" Text =""></asp:Label></td></tr>
 </table>
 </td>
 </tr>
 </table>  
 <asp:DataGrid ID="DGFacilities" runat="server" AutoGenerateColumns="False" CellPadding="4" BorderWidth="2px" Width="1200px" Visible="false">
          <FooterStyle BackColor="#CCCCCC" />
            <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left"/>
                 <ItemStyle BackColor="White" />
    <COLUMNS>
         <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Chapter"  HeaderText="Chapter" />
           <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ContestYear" HeaderText="ContestYear"/>
            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Purpose" HeaderText="Purpose"/>
             <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgID" HeaderText="BldgID"/>
               <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="BldgName" HeaderText="BldgName"/>
                 <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="RoomNo" HeaderText="RoomNumber"/>
                   <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Floor" HeaderText="Floor"/>
                     <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity"/>
         <%--   <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="ParentAct" HeaderText="ParentAct"/>
              <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Awards" HeaderText="Awards"/>
                <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="GradingRoom" HeaderText="GradingRoom"/>
                  <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="JudgeRoom" HeaderText="JudgeRoom"/>
                    <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Registration" HeaderText="Registration"/>
                      <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="Sponsors" HeaderText="Sponsors"/>
                        <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="InfoDesk" HeaderText="InfoDesk"/>
                          <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="RecDesk" HeaderText="RecDesk"/>
                            <asp:Boundcolumn  HeaderStyle-Font-Bold="true" DataField="NSFDesk" HeaderText="NSFDesk"/>--%>
            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Podium" HeaderText="Podium" />
              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="PodMic" HeaderText="PodMic"/>
                <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="PodLaptop" HeaderText="PodLT"/>
                  <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="OverHeadProj" HeaderText="Projector"/>
                    <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="JudTable" HeaderText="JTable" />
                      <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="JudChairs" HeaderText="JChair" />
                        <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="CJMic" HeaderText="CJMic"/>
                          <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChildMic" HeaderText="ChildMic"/>
                            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="AudMixer" HeaderText="AMixer"/>
                              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ChMicStand" HeaderText="ChMicStand" />
                                <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="DesktopMicStand" HeaderText="DeskMicStand"/>
                                  <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="AudioWires" HeaderText="AWires"/>
                                    <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="ExtraMikes" HeaderText="ExtraMic"/>
                                      <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Laptop" HeaderText="Laptop"/>
                                        <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Tables" HeaderText="Tables"/>
                                          <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="Chairs" HeaderText="Chairs"/>
                                            <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="MicStand" HeaderText="MicStand"/>
                                              <asp:Boundcolumn HeaderStyle-Font-Bold="true" DataField="IntAccess" HeaderText="IntAccess"/>
                                     
                                     
         </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>    
</asp:Content>

