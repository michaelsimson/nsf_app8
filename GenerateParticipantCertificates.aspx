<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="GenerateParticipantCertificates.aspx.vb" Inherits="GenerateParticipantCertificates" title="Untitled Page" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <table  width="100%" align="center">
        <tr bgcolor="#ffffff"  align="center" >
            <td align="left" >
            <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
            </td>
        </tr>
		<tr bgcolor="#ffffff"  align="center" >
		    <td class="Heading" colspan="2">Generate Certificate</td>
		</tr>
		<tr align="Center" runat="server">
		    <td><asp:Label ID="lblPage" Text="" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>&nbsp;</td>
		</tr>
		 <tr id="TrEventYear" runat="server" >
				        <td style="text-align:center">Event Year: <asp:DropDownList ID="ddlEventYear"  AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged"></asp:DropDownList></td>
				    </tr>
		<tr>
		    <td align="center">
			    <table align="center" width="100%">
				    <asp:Panel runat="server" ID="pnlData">
				 <%--   <tr>
				        <td style="text-align:right">Event Year:</td>
				        <td><asp:DropDownList ID="ddlEventYear"  AutoPostBack="true" runat="server" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged"></asp:DropDownList></td>
				    </tr>--%>
				    <tr bgcolor="#ffffff" >
				        <td style="text-align:right" >
				           Certificate Type: 
				        </td>
					    <td colspan="1" align="left"> &nbsp;&nbsp;
					        <asp:DropDownList OnSelectedIndexChanged="ddlCertificate_SelectedIndexChanged" AutoPostBack="true"   runat="server" ID="ddlCertificate">
					            <asp:ListItem Text="Participant" Value="Participant"></asp:ListItem>					            
					            <asp:ListItem Text="Volunteer-Fill" Value="Volunteer_Fill"></asp:ListItem>
					            <asp:ListItem Text="Volunteer-Blank" Value="Volunteer-Blank"></asp:ListItem>	
					            <asp:ListItem Text="Rank-Blank" Value="Rank_Blank"></asp:ListItem>
					            <asp:ListItem Text="Rank-Fill" Value="Rank_Fill"></asp:ListItem>
					        </asp:DropDownList>
					    </td>				    
					</tr>
				    <tr bgcolor="#ffffff" >
				        <td style="text-align:right" >
				           Select Contest Dates: 
				        </td>
					    <td colspan="1" align = "left"> &nbsp;&nbsp;
					       <asp:ListBox  SelectionMode="Multiple" runat="server" ID="lstContestDates" Enabled="true" AutoPostBack="true" OnSelectedIndexChanged="lstContestDates_SelectedIndexChanged">
					       </asp:ListBox>
					       <asp:Label ID="lblChapter" runat="Server"></asp:Label>
					       <asp:RequiredFieldValidator runat="server" ControlToValidate="lstContestDates" ID="rfvContestDates" ErrorMessage="Select Contest Date" Display="Dynamic" ></asp:RequiredFieldValidator>
                       </td>
                     </tr> 
                     <tr runat = "server" id="trrange" visible =false bgcolor="#ffffff" >
				         <td style="text-align:right" >
				           Select Range : 
				        </td>
					    <td align="left">	&nbsp;&nbsp;				       
                            <asp:DropDownList ID="ddlRange" runat="server">
                            </asp:DropDownList>
					       </td>					
				    </tr>
                      <asp:Panel runat="server" ID="pnlMessage" Visible="false">
				        <tr bgcolor="#ffffff" >
					        <td style="text-align:center" colspan="2">
					            <asp:Label Font-Bold="true" ForeColor="red" runat="server" ID="lblMessage" ></asp:Label>
					        </td>
					    </tr> 
				    </asp:Panel>
				    <tr>
				        <td  align="right" colspan="2" >
                            <asp:Button runat="server" ID="btnGenerate" Text="View" Height="25" CssClass="FormButton" Width="100"  />
                            <asp:Button runat="server" ID="btnExport" Text="Export Word" Height="25" CssClass="FormButton" Width="100"  />
                            <asp:Button runat="server" ID="btnExportPDF" Text="Export PDF" Height="25" CssClass="FormButton" Width="100"  />
                            &nbsp; &nbsp;
                            <asp:Label ID="lblNote" Font-Names="Calbri" Font-Size="11" runat="server" Text="In PDF, change Portrait to Landscape before printing" ForeColor="Black"></asp:Label>
                        </td> 
                    </tr>  
                    <tr><td  colspan="2" align="center"><asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr> 
                    <tr><td  colspan="2" align="center"><asp:Label ID="lblErrorBdg" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr> 
                 
                 <%--   <tr>
                        <td  align="center" colspan="2">
                           <font size="4" color=blue>Please select one row to pick names to go as signatures at the bottom of the certificate.</font> 
                        </td>
                    </tr>   --%>                
				    <tr >				       
				        <td style="text-align:center" colspan="2">
				           <asp:DataGrid runat="server" ID="dgSignatures"  AutoGenerateColumns="false">
                               <Columns>                                    
                                   <asp:ButtonColumn CommandName="Select" Text="Select"></asp:ButtonColumn>
                                   <asp:BoundColumn DataField="name" HeaderText="Name"  HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                    <asp:BoundColumn DataField="title" HeaderText="Left Title" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="firstName"  HeaderText="Left First Name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="lastName" HeaderText="Left Last Name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="lefttitle" HeaderText="Left Singature Title" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="title1" HeaderText="Right Title" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="firstName1" HeaderText="Right First Name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="lastName1" HeaderText="Right Last Name" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="righttitle" HeaderText="Right Signature Title" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="leftsignimage" HeaderText="Left Signature Image" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="rightsignimage" HeaderText="Right Signature Image" HeaderStyle-Font-Bold="true"></asp:BoundColumn>
                                   <asp:BoundColumn DataField="ProductCode" HeaderText="" Visible="false"></asp:BoundColumn>
                               </Columns>				                
                               <SelectedItemStyle BackColor="lightblue" Font-Bold="true" Font-Size="Medium"  />
                                <HeaderStyle Font-Bold ="true" />
				           </asp:DataGrid>
				         </td> 
				     </tr> 
				      
				    </asp:Panel>
				   
				</table>
			</td> 
			</tr> 
		</table>
</asp:Content>



 
 
 