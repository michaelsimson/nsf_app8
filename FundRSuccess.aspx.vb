﻿Imports System
Imports System.Web
Imports LinkPointTransaction
Imports System.Net.Mail
Imports System.Text
Imports System.IO
Imports System.Globalization
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports NorthSouth.BAL
Namespace VRegistration
    Partial Class FundRSuccess
        Inherits System.Web.UI.Page
        '    Private us As CultureInfo = New CultureInfo("en-US")
        Protected order As String
        Protected resp As String
        Protected fIE5 As Boolean
        Public SaleAmt As Decimal = 0
        Dim sbSaleItems As New StringBuilder

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load

            If (Not Session("LoggedIn") Is Nothing) Then
                If Session("LoggedIn") <> "True" Then
                    Dim entryToken As String = Nothing
                    If (Not Session("EntryToken") Is Nothing) Then
                        entryToken = Session("entryToken").ToString().Substring(0, 1)
                        Server.Transfer("login.aspx?entry=" + entryToken)
                    Else
                        Server.Transfer(System.Configuration.ConfigurationManager.AppSettings.Get("HomePageURL"))
                    End If
                ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Hyperlink1.Text = "Back to FundRaising Registration"
                    Hyperlink1.NavigateUrl = "FundRReg.aspx?id=1"
                End If
            End If
            Dim isTestMode As Boolean = False
            isTestMode = CBool(System.Configuration.ConfigurationManager.AppSettings.Get("TestMode").ToString)

            If Not Session("CustIndID") = Nothing Then
                Session("ParentID") = Session("CustIndID")
            End If

            ' Put user code to initialize the page here
            If Not IsPostBack Then

                Dim sb As New StringBuilder
                Dim bc As HttpBrowserCapabilities = Request.Browser
                Dim re As StreamReader
                'Dim reDAS As StreamReader
                Dim nDonationAmt As Decimal = 0
                Dim nTaxDeductibleAmount As Decimal = 0
                'Dim nregfee As Decimal = Session("RegFee")
                Dim nsaleamt As Decimal = Session("FundRAmt")
                'Dim nTaxDeductibleRegFee = CType(Session("RegFee"), Decimal) * (2 / 3)
                Dim emailBody As String = ""
                Dim screenConfirmText As String = ""
                Dim subMail As String
                subMail = "Confirmation received for NSF Fundraising "
                Dim strDonationMessage As String = ""
                Dim approved As String = ""
                Dim paymentReference As String = ""
                Dim strDonText As String = ""

                If (isTestMode = True) Then
                    subMail = "Test Email(no real credit card Transactions):" + "Confirmation received for NSF 2007 Contests"
                End If

                If (Not Session("R_APPROVED") Is Nothing) Then
                    approved = Session("R_APPROVED").ToString
                End If

                If (Not Session("PaymentReference") Is Nothing) Then
                    paymentReference = Session("PaymentReference").ToString
                End If

                If (approved = "APPROVED") Then
                    DisplayContests()
                    ' DisplayMealcharges()

                    fIE5 = ((bc.Browser = "IE") _
                                AndAlso (bc.MajorVersion > 4))
                    order = CType(Session("outXml"), String)
                    ' resp = CType(Session("resp"), String)  'commented on 1/15/07
                    ' ParseResponse(resp)                   'commented on 1/15/07

                    If (Not (Session("Donation")) Is Nothing) Then
                        nDonationAmt = CType(Session("Donation"), Decimal)
                    End If
                    '    Session("PaymentReference") = R_OrderNum

                    If (nDonationAmt > 0) Then
                        'strDonationMessage = "Thank you also for your generous donation of " & FormatCurrency(nDonationAmt) & ".<BR> This will help NSF’s goal of providing scholarships ($250 each) to 500 poor but meritorious students in India for the year 2006-2007."
                        strDonationMessage = "We also thank you for your generous donation of " & FormatCurrency(nDonationAmt) & ".This will help in reaching our goal of providing 1000 scholarships ($250 each) to those who excel among the poor go to college in India for the upcoming academic year."
                        strDonationMessage = strDonationMessage & "<p>Here are the purchase details of your current payment. Your tax-deductible contribution is: [TAXDEDUCTIBLEAMOUNT]</p> "
                        strDonText = "Please obtain a matching gift form from your (or your spouse’s) employer, fill out"
                        strDonText = strDonText & "the top portion and mail it to the NorthSouth Foundation at <?xml namespace='' ns='urn:schemas-microsoft-com:office:smarttags' prefix='st1' ?>"
                        strDonText = strDonText & "<st1:address w:st='on'><st1:Street w:st='on'>2 Marissa Ct</st1:Street>, <st1:City w:st='on'>Burr Ridge</st1:City>, <st1:State w:st='on'>IL</st1:State> "
                        strDonText = strDonText & "<st1:PostalCode w:st='on'>60527.</st1:PostalCode> </st1:address> By doing this, you double your contribution to the Foundation. </p> <p> Please retain this email for your records. This also serves as a receipt for your tax- deductible contribution."
                    Else
                        strDonText = "Please retain this page for your records."
                        strDonationMessage = "<p>Here are the Fundraising details of your current payment. </p> "
                    End If

                    'Your tax-deductible contribution is:  ( donation amount + 2/3 * AMNT)
                    nTaxDeductibleAmount = nDonationAmt '**(  + (nTaxDeductibleRegFee))

                    Dim eventType As String
                    If (Session("EventID") = 1) Then
                        eventType = "Finals"
                    ElseIf (Session("EventID") = 10) Then
                        eventType = "Shop"
                    ElseIf (Session("EventID") = 9) Then
                        eventType = "Fundraising"
                    Else
                        eventType = "Regional"
                    End If

                    Dim eventYear As String
                    eventYear = System.Configuration.ConfigurationManager.AppSettings.Get("Contest_Year")
                    '********  16072010 file name to be changed *********
                    re = File.OpenText(Server.MapPath("FundRConfirmingEmail.htm"))
                    emailBody = re.ReadToEnd
                    re.Close()
                    'emailBody = emailBody.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    emailBody = emailBody.Replace("[strDonText]", strDonText)
                    emailBody = emailBody.Replace("[PAYMENTREFERENCE]", paymentReference)
                    emailBody = emailBody.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    emailBody = emailBody.Replace("[DATAGRID]", sbSaleItems.ToString)
                    emailBody = emailBody.Replace("[DONATIONTEXT]", strDonationMessage)
                    emailBody = emailBody.Replace("[EVENTYEAR]", eventYear)
                    emailBody = emailBody.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nsaleamt))
                    emailBody = emailBody.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    ' SendDasMessage("Donation to North South Foundation", emailBody, CType(Session("LoginEmail"), String))

                    ''***Screen Population logic

                    re = File.OpenText(Server.MapPath("FundRConfirmingEmail.htm"))
                    screenConfirmText = re.ReadToEnd
                    re.Close()
                    'screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", R_OrderNum)
                    screenConfirmText = screenConfirmText.Replace("[strDonText]", strDonText)
                    screenConfirmText = screenConfirmText.Replace("[PAYMENTREFERENCE]", paymentReference)
                    screenConfirmText = screenConfirmText.Replace("[DONATIONAMOUNT]", FormatCurrency(nDonationAmt))
                    screenConfirmText = screenConfirmText.Replace("[DATAGRID]", sbSaleItems.ToString)
                    screenConfirmText = screenConfirmText.Replace("[DONATIONTEXT]", strDonationMessage)
                    screenConfirmText = screenConfirmText.Replace("[REGISTRATIONAMOUNT]", FormatCurrency(nsaleamt))
                    screenConfirmText = screenConfirmText.Replace("[TAXDEDUCTIBLEAMOUNT]", FormatCurrency(nTaxDeductibleAmount))
                    screenConfirmText = screenConfirmText.Replace("[EVENTYEAR]", eventYear)
                    screenConfirmText = screenConfirmText.Replace("[EVENTTYPE]", eventType)
                    lblDonationMessage.Text = screenConfirmText.ToString
                    screenConfirmText = Nothing

                    'Response.Write(sbContests.ToString())

                    '*************************************************
                    Dim pattern As String = "^[a-zA-Z0-9][\w\.-\.+]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
                    Dim EmailIDMatch As Match = Regex.Match(Session("FundEmail"), pattern)
                    If Not EmailIDMatch.Success Then
                        lblEmailStatus.Text = "There was an error in EMailID. Please print/save details of this page for your records."
                    Else
                        ShowAttendees()
                        If SendEmail(subMail, emailBody.ToString, CType(Session("FundEmail"), String)) Then
                            'If SendEmail(subMail, emailBody.ToString, CType("chitturi9@gmail.com", String)) Then
                            lblEmailStatus.Text = "An email was sent to you to confirm your registration."

                        Else
                            lblEmailStatus.Text = "There was an error sending email. Please print/save details of this page for your records."
                        End If
                    End If

                    ''*** DAS Message EMail Communication
                    'Uncomment the following section if DAS email  needs to be sent.
                    'Dim strDASMessage As String
                    'reDAS = File.OpenText(Server.MapPath("DASMessage.htm"))
                    'strDASMessage = reDAS.ReadToEnd
                    'subMail = "DAS Message from North South Foundation"
                    'If (isTestMode = True) Then
                    '    subMail = "Test Email:" + "DAS Message from North South Foundation"
                    'End If
                    'SendDasMessage("DAS Message from North South Foundation", strDASMessage, CType(Session("LoginEmail"), String))
                End If
            End If
        End Sub
        Private Sub ShowAttendees()
            Try
                Dim strSQL As String
                strSQL = "SELECT FF.FundRFeesID,FF.FundRCalID,CASE WHEN FR.FundRRegID IS NULL THEN 0 else FR.FundRRegID END as FundRRegID,  FR.Quantity ,FR.UnitFee as  FEE, FF.EventId, FF.EventCode, FF.ChapterId, FF.ChapterCode, FF.ProductGroupId, FF.ProductGroupCode, FF.ProductId, FF.ProductCode,P.Name as ProductName, FF.EventYear, "
                strSQL = strSQL & " FF.EventDate, FF.FeeFrom, FF.FeeTo, FF.TaxDeduction, FF.Exclusive, FF.ByContest, FF.Share, FR.TotalAmt,FR.PaymentMode,FR.PaymentDate,case when Fr.PaymentReference IS Null then 'Pending' else Fr.PaymentReference End as PaymentReference, convert(varchar(10),FR.EventDate,101) EventDate,(select Distinct(ORGANIZATION_NAME) From OrganizationInfo Org inner join FundRaisingCal FC on Org.automemberid=FC.VenueId where FC.FundRCalID=FR.FundRCalID) Venue "
                strSQL = strSQL & " FROM FundRFees FF Inner join Product P ON P.ProductID = FF.ProductID Inner Join FundRReg FR ON FF.FundRCalID = FR.FundRCalID and FF.ProductId = Fr.ProductId and FF.EventYear = FR.EventYear "
                strSQL = strSQL & " WHERE FR.MemberId = " & Session("CustIndID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "' and FF.ProductCode in ('Child','Adult')"
                strSQL = strSQL & " order by FR.PaymentDate,FF.ProductGroupID,FF.FeeFrom Desc"

                Dim dsAttendees As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
                Dim dtAttendees As DataTable = dsAttendees.Tables(0)
                lblAdult.Text = 0
                lblChildren.Text = 0
                Dim i As Integer, dr As DataRow
                For i = 0 To dtAttendees.Rows.Count - 1
                    dr = dtAttendees.Rows(i)
                    If dr("ProductCode").ToString.ToLower = "adult" Then
                        lblAdult.Text = dr("Quantity")
                    End If
                    If dr("ProductCode").ToString.ToLower = "child" Then
                        lblChildren.Text = dr("Quantity")
                    End If
                Next
            Catch ex As Exception

            End Try
        End Sub
        Protected Overrides Sub OnInit(ByVal e As EventArgs)
            '
            ' CODEGEN: This call is required by the ASP.NET Web Form Designer.
            '
            InitializeComponent()
            MyBase.OnInit(e)
        End Sub

        ' <summary>
        ' Required method for Designer support - do not modify
        ' the contents of this method with the code editor.
        ' </summary>
        Private Sub InitializeComponent()

        End Sub

        Private Function SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String) As Boolean
            '......................................
            ' sMailTo = "bindhu.rajalakshmi@capestart.com"
            '.>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@northsouth.org")
            'If emailvalidateion(sMailTo) = True Then
            '    email.To.Add(sMailTo)
            'End If
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'leave blank to use default SMTP server
            Dim client As New SmtpClient()
            ' Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            ' client.Host = host

            Dim ok As Boolean = True
            Try
                email.To.Add(sMailTo)
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
            Return ok
        End Function
        Private Sub SendDasMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
            'Build Email Message
            Dim email As New MailMessage
            email.From = New MailAddress("nsfcontests@northsouth.org")
            'If emailvalidateion(sMailTo) = True Then
            '    email.To.Add(sMailTo)
            'End If
            email.Subject = sSubject
            email.IsBodyHtml = True
            email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
            email.Body = sBody
            'TODO Need to be fixed to get the Attachments file
            'email.Attachments.Add(Server.MapPath("DASPledgeSheet2006.doc"))
            'leave blank to use default SMTP server
            Dim ok As Boolean = True
            Dim client As New SmtpClient()
            'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
            'client.Host = host

            Try
                email.To.Add(sMailTo)
                client.Send(email)
            Catch e As Exception
                lblMessage.Text = e.Message.ToString
                ok = False
            End Try
        End Sub
        Private Sub DisplayContests()
            Dim sb As New StringBuilder
            Dim rowcount As Int32 = 0
            Dim connContest As New SqlConnection(Application("ConnectionString"))
            Dim totalamt As Decimal = 0.0
            Dim totalTaxDeductamt As Decimal = 0.0
            Dim dsContestant As New DataSet
            Dim tblConestant() As String = {"Contestant"}
            'Session("PaymentReference")
            Dim SQLStr As String
            SQLStr = "select FR.Quantity,FR.TaxDeduction,FR.UnitFee,FR.TotalAmt,FR.PaymentDate,FR.PaymentMode,FR.PaymentNotes,FR.PaymentReference,P.Name as ProductName,Fr.TaxDeduction ,(FR.TotalAmt * FR.TaxDeduction )/100.0 as TaxDeductableAmount from FundRReg FR Inner Join Product P ON P.ProductId = FR.ProductId  where FR.MemberID = " & Session("CustIndID") & " AND FR.PaymentReference ='" & Session("PaymentReference") & "' AND FR.FundRCalID =" & Session("FundRCalID") & " AND FR.DonorType = '" & Session("FundDonorType").ToString().Trim & "'"
            'SQLStr = SQLStr & "from Coachreg CR Inner Join EventFees EF ON CR.EventID=EF.EventID and CR.EventYear = EF.eventYear and CR.ProductID = Ef.ProductID Inner Join Child Ch ON CR.ChildNumber=Ch.ChildNumber Inner Join Product P ON P.ProductId = CR.ProductID Inner Join coachcal C ON "
            'SQLStr = SQLStr & "CR.PaymentReference ='" & Session("PaymentReference") & "' AND CR.CMemberID = C.MemberID and CR.ProductID=C.ProductID and CR.[Level]=C.[Level]  Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID where CR.PMemberid=" & Session("CustIndID") & " and CR.EventYear=" & Now.Year
            SqlHelper.FillDataset(connContest, CommandType.Text, SQLStr, dsContestant, tblConestant)
            If dsContestant.Tables.Count > 0 Then
                If dsContestant.Tables(0).Rows.Count > 0 Then
                    sb.Append("<table border=1 width= 900px  cellspacing=0 cellpadding=0>")
                    sb.Append("<tr bgcolor=lightblue forecolor=white>")
                    sb.Append("<td align=Center  width=10%><b>Quantity</b></td>")
                    sb.Append("<td align=Center  width=15%>Name</td>")
                    sb.Append("<td align=Center  width=10%>Unit_Price</td>")
                    sb.Append("<td align=Center  width=10%>Amount</td>")
                    sb.Append("<td align=Center  width=30%>Payment_Info</td>")
                    sb.Append("<td align=Center  width=12%>Tax-Ded Percent</td>")
                    sb.Append("<td align=Center  width=13%>Tax-Ded Amount</td>")
                    sb.Append("</tr>")
                    Response.Write("<!-- I am here-->")
                    Response.Write("<!-- I am here2-->")
                    For rowcount = 0 To dsContestant.Tables(0).Rows.Count - 1
                        Response.Write("<!-- I am here3-->")
                        sb.Append("<tr>")
                        sb.Append("<td align=Center>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("Quantity").ToString() + "</td>")
                        sb.Append("<td align=Center>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("ProductName").ToString() + "</td>")
                        sb.Append("<td  align=Center>")
                        sb.Append(" $" & String.Format("{0:f2}", CType(dsContestant.Tables(0).Rows(rowcount).Item("UnitFee"), Decimal)) + "</td>")
                        sb.Append("<td  align=Center>")
                        sb.Append(" $" & String.Format("{0:f2}", CType(dsContestant.Tables(0).Rows(rowcount).Item("TotalAmt"), Decimal)) + "</td>")
                        sb.Append("<td align=Left>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentDate").ToString() + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentReference").ToString() + "<BR>")
                        sb.Append(dsContestant.Tables(0).Rows(rowcount).Item("PaymentMode").ToString() + "</td>")
                        sb.Append("<td align=Center>")
                        sb.Append("" & String.Format("{0:f2}", CType(dsContestant.Tables(0).Rows(rowcount).Item("TaxDeduction"), Decimal)) + "</td>")
                        sb.Append("<td align=Center>")
                        sb.Append("" & String.Format("{0:f2}", CType(dsContestant.Tables(0).Rows(rowcount).Item("TaxDeductableAmount"), Decimal)) + "</td></tr>")
                        totalamt = totalamt + CType(dsContestant.Tables(0).Rows(rowcount).Item("TotalAmt"), Decimal)
                        totalTaxDeductamt = totalTaxDeductamt + CType(dsContestant.Tables(0).Rows(rowcount).Item("TaxDeductableAmount"), Decimal)
                    Next
                    If Not Session("Discount") Is Nothing Then
                        totalamt = totalamt - Session("Discount")
                    End If
                    sb.Append("<tr><td align=right colspan=7><b> Total Amount Paid : " & String.Format("{0:c}", totalamt) & " &nbsp;&nbsp;&nbsp;")
                    If Not Session("Discount") Is Nothing Then
                        sb.Append("  &nbsp;&nbsp;&nbsp; Discount : " & String.Format("{0:c}", Session("Discount")))
                    End If
                    sb.Append(" &nbsp;&nbsp; Tax-Deductible Amount: " & String.Format("{0:c}", totalTaxDeductamt) & " &nbsp;&nbsp; </b> </td></tr>")
                    sb.Append("</table>")
                Else
                    sb.Append(" ")
                End If
            End If
            sbSaleItems.Append(sb.ToString)
            connContest = Nothing
        End Sub
        Private Function emailvalidateion(ByVal mailstring As String) As Boolean
            ''Added for testing 20-09-2013
            Dim Valid As Boolean
            Try
                Valid = Regex.IsMatch(mailstring, "\A(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?)\Z", RegexOptions.IgnoreCase)
            Catch ex As Exception

            End Try
            If Not Valid Then
                Return False
            Else
                Return True
            End If

        End Function
    End Class

End Namespace

