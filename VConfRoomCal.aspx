﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="VConfRoomCal.aspx.vb" Inherits="VConfRoomCal" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
    <div>
        <table id="tblLogin" border="0" cellpadding = "3" cellspacing = "0" width="900px" runat="server">
				<tr>
					<td></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<h2>Virtual Conference Room Calendar</h2>
					</td>
				</tr>
				 <tr>
			        <td colspan="2">
			            <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
			          </td>
			    </tr>
			    <tr>
			        <td colspan="2"> <center>
                        <table width="90%">

                            <tr>
                                <td colspan="6" style="text-align:right">
                                    
                                    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" />
                                    &nbsp;&nbsp;
                                    <asp:Button ID="btnExportAll" runat="server" Text="Export All Days" />
                                    <br />
 

                                </td>
                            </tr>
                            <tr>
                                <td style="width:25px;font-weight:bold" >Year</td>
                                <td style="width:25px">
                                    <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True">
                                    </asp:DropDownList>
                                </td>
                                <td style="width:25px"></td>
                                <td style="width:25px;font-weight:bold">Day</td>
                                <td style="width:25px">
                                    <asp:DropDownList ID="ddlDay" runat="server" AutoPostBack="True">
                                        <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                                        <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                        <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                        <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                        <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                        <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                        <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                    </asp:DropDownList>
                                </td>
                                <td></td>
                            </tr>
                            <tr>
                                <td style="width:25px">&nbsp;</td>
                                <td style="width:25px">
                                    &nbsp;</td>
                                <td style="width:25px">&nbsp;</td>
                                <td style="width:25px">&nbsp;</td>
                                <td style="width:25px">
                                    &nbsp;</td>
                                <td>&nbsp;</td>
                            </tr>
                            <tr>
                                <td colspan="6">
                                    <asp:GridView ID="grdList" runat="server" Width="100%" EnableModelValidation="True" AutoGenerateColumns="False">
                                        <Columns>
                                             
                                                
					<asp:BoundField DataField="VRoom" HeaderText="Ser#"></asp:BoundField>
					<asp:BoundField DataField="8:00" HeaderText="8:00"></asp:BoundField>
					<asp:BoundField DataField="9:00" HeaderText="9:00"></asp:BoundField>
					<asp:BoundField DataField="10:00" HeaderText="10:00"></asp:BoundField>
                                                                             
					<asp:BoundField DataField="11:00" HeaderText="11:00"></asp:BoundField>
					<asp:BoundField DataField="12:00" HeaderText="12:00"></asp:BoundField>
					<asp:BoundField DataField="13:00" HeaderText="13:00"></asp:BoundField>
					<asp:BoundField DataField="14:00" HeaderText="14:00"></asp:BoundField>
                                                                             
					<asp:BoundField DataField="15:00" HeaderText="15:00"></asp:BoundField>
					<asp:BoundField DataField="16:00" HeaderText="16:00"></asp:BoundField>
					<asp:BoundField DataField="17:00" HeaderText="17:00"></asp:BoundField>
					<asp:BoundField DataField="18:00" HeaderText="18:00"></asp:BoundField>
                                                                             
					<asp:BoundField DataField="19:00" HeaderText="19:00"></asp:BoundField>
					<asp:BoundField DataField="20:00" HeaderText="20:00" ></asp:BoundField>
					<asp:BoundField DataField="21:00" HeaderText="21:00"></asp:BoundField>
					<asp:BoundField DataField="22:00" HeaderText="22:00"></asp:BoundField>
                                                                             
					<asp:BoundField DataField="23:00" HeaderText="23:00"></asp:BoundField>
					<asp:BoundField DataField="0:00" HeaderText="0:00"></asp:BoundField> 
                                            </Columns>
                                        
                                 

                                    </asp:GridView>
                                </td>

                            </tr>
                        </table>

                        </center>
			        </td>
                     
			    </tr>
                <tr>
                    
                    <td colspan="2" align="center">
 

                        <br />   <br /> 
                    </td>
                </tr>
						</table>
					
		</div>
</asp:Content>