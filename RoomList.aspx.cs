﻿
using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using System.Diagnostics;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.Reflection;
using NativeExcel;

public partial class RoomList : System.Web.UI.Page
{

    protected System.Web.UI.WebControls.Button btnAddRow;
    protected System.Web.UI.WebControls.Button btnInsert;
    Boolean DateFlag = false;
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            GetContestYear(ddlContestYear);
            GetEvent(ddlEvent, "1,2");
            GetChapter(ddlChapter);

            // CheckEvent();
            int RoleID = Convert.ToInt32(Session["RoleID"]);
            try
            {
                //if ((RoleID == 3) || (RoleID == 5) || (RoleID == 52))
                if (Convert.ToInt32(Session["selChapterID"]) > 0)
                {

                    ddlChapter.SelectedValue = Session["selChapterID"].ToString();
                    ddlChapter.SelectedItem.Text = Session["selChapterName"].ToString();
                    ddlChapter.Enabled = false;
                    if (ddlChapter.SelectedValue == "1")
                    {
                        ddlEvent.SelectedValue = "1";
                    }
                    else
                    {
                        ddlEvent.SelectedValue = "2";
                    }
                    ddlEvent.Enabled = false;
                    //CheckEvent();
                    GetContestDates();
                    GetVendor(false);
                    GetBuildingID();
                }
                else
                {
                    ddlChapter.Enabled = true;
                    GetChapter(ddlChapter);
                }
                LoadTables();
            }
            catch(Exception ex) 
            {
                //Response.Write(ex.ToString());
            }
            //LoadGrid();
         }
    }

    public void LoadTables()
    {  
        DataTable DT = new DataTable();
        DT.Columns.Add("LabelName");
        DataRow DR;
        DR = DT.NewRow();

        DT.Columns.Add("Col1");
        DT.Columns.Add("Col2");
        DT.Columns.Add("Col3");
        DT.Columns.Add("Col4");
        DT.Columns.Add("Col5");
        DT.Columns.Add("Col6");
        DT.Columns.Add("Col7");
        DT.Columns.Add("Col8");
        DT.Columns.Add("Col9");
        DT.Columns.Add("Col10");
        DT.Columns.Add("Col11");
        DT.Columns.Add("Col12");
        DT.Columns.Add("Col13");
        DT.Columns.Add("Col14");
        DT.Columns.Add("Col15");

        DT.Rows.Add("Room Number");
        DT.Rows.Add("Floor");
        DT.Rows.Add("Capacity");
        DT.Rows.Add("Gallery");
        DT.Rows.Add("FlippersToWrite");
        DT.Rows.Add("Tables to write");
        DT.Rows.Add("Fixed Chairs");
        DT.Rows.Add("Podium");
        DT.Rows.Add("Podium Mic");
        DT.Rows.Add("Podium Laptop");
        DT.Rows.Add("Projector");
        DT.Rows.Add("Judge Table");
        DT.Rows.Add("Judge Chairs");
        DT.Rows.Add("Chief Judge Mic");
        DT.Rows.Add("Child Mic");
        DT.Rows.Add("Audio Mixer");
        DT.Rows.Add("Child Mic Stand");
        DT.Rows.Add("Desk Mic Stand");
        DT.Rows.Add("Audio Wires");
        DT.Rows.Add("Extra Mic");
        DT.Rows.Add("Internet Access");
        DT.Rows.Add("Laptop");
        DT.Rows.Add("Table");
        DT.Rows.Add("Chairs");
        DT.Rows.Add("MicStand");

        Session.Add("DT", DT);      
        DataGridRoom.DataSource = DT;
        DataGridRoom.DataBind();
        
    }
   
    private void GetContestYear(DropDownList ddlObject)
    {
        int[] year = new int[5];
        year[0] = DateTime.Now.Year;
        year[1] = DateTime.Now.AddYears(-1).Year;
        year[2] = DateTime.Now.AddYears(-2).Year;
        year[3] = DateTime.Now.AddYears(-3).Year;
        year[4] = DateTime.Now.AddYears(-4).Year;
        ddlObject.DataSource = year;
        ddlObject.DataBind();
        //ddlObject.Items.Insert(0, new ListItem("Select One", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void GetEvent(DropDownList ddlObject, String EvntIDs)
    {
        DataSet dsEvent = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select Name,EventId  from Event where EventID in (" + EvntIDs + ") Group by Name,EventId");
        ddlObject.DataSource = dsEvent;
        ddlObject.DataTextField = "Name";
        ddlObject.DataValueField = "EventId";
        ddlObject.DataBind();
        if (dsEvent.Tables[0].Rows.Count > 1)
        {
            ddlObject.Items.Insert(0, new ListItem("Select Event", "-1"));
            ddlObject.SelectedIndex = 0;
            ddlObject.Enabled = true;
        }
        else
            ddlObject.Enabled = false;
    }
    private void GetChapter(DropDownList ddlObject)
    {
        DataSet ds_Chapter = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetChapterAll");
        ddlObject.DataSource = ds_Chapter;
        ddlObject.DataTextField = "ChapterCode";
        ddlObject.DataValueField = "ChapterId";
        ddlObject.DataBind();
        ddlObject.Items.Insert(0, new ListItem("Select Chapter", "-1"));
        ddlObject.SelectedIndex = 0;
    }
    private void CheckEvent()
    {
        if (ddlEvent.SelectedValue == "1")
        {
            ddlChapter.SelectedValue = "1";
            ddlChapter.SelectedItem.Text = "Home";
            ddlChapter.Enabled = false;
            //TdContestDate.Visible = false;
           // ddlContestDate.Visible = false;
        }
        else
        {
            ddlChapter.Enabled = true;
           // TdContestDate.Visible = true;
           // ddlContestDate.Visible = true;
        }
        GetContestDates();
        GetVendor(false);
        GetBuildingID();
 
    }
    protected void DataGridRoom_ItemDataBound(object sender, DataGridItemEventArgs e)
    {
        DataTable dt, dt1, dt2;
        dt = new DataTable();

        dt.Columns.Add("ddlText", Type.GetType("System.String"));
        dt.Columns.Add("ddlValue", Type.GetType("System.Int32"));
        dt.Rows.Add("Select", 0);
        for (int l = 1; l <= 5; l++)
        {
            dt.Rows.Add(l, l);
        }

        dt1 = new DataTable();
        dt1.Columns.Add("ddlText", Type.GetType("System.String"));
        dt1.Columns.Add("ddlValue", Type.GetType("System.Int32"));
        for (int j = 0; j < 2; j++)
        {
             dt1.Rows.Add(j , j);
        }

        dt2 = new DataTable();
        dt2.Columns.Add("ddlText", Type.GetType("System.String"));
        dt2.Columns.Add("ddlValue", Type.GetType("System.String"));//Int32"));
        dt2.Rows.Add('N','N');//0);
        dt2.Rows.Add('Y','Y');//1);


        if (e.Item.ItemType == ListItemType.AlternatingItem || e.Item.ItemType == ListItemType.Item)
        {
            //string[] optionsCount = { "Select", "1", "2", "3", "4", "5" };
            //string[] optCount ={"0", "1"};
            //string[] optionsBool = { "Y", "N" };
            //Response.Write("List Item");
            Label LabelRow = new Label();
            LabelRow = (Label)e.Item.FindControl("LabelName");
                      
             if (LabelRow.Text == "Floor" || LabelRow.Text == "Podium" || LabelRow.Text == "Podium Mic" || LabelRow.Text == "Podium Laptop" || LabelRow.Text == "Projector" ||
                 LabelRow.Text == "Judge Table" || LabelRow.Text == "Judge Chairs" ||  LabelRow.Text == "Audio Wires" || LabelRow.Text == "Extra Mic" ||
                 LabelRow.Text == "Laptop" || LabelRow.Text == "MicStand")
            {
                   for (int i = 1; i <= 15; i++)
                    {
                        
                            DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                            list.DataSource = dt; //optionsCount;
                            list.DataBind();
                     }
            }
             else if (LabelRow.Text == "Chief Judge Mic" || LabelRow.Text == "Child Mic" || LabelRow.Text == "Audio Mixer" ||
                      LabelRow.Text == "Audio Mixer" || LabelRow.Text == "Child Mic Stand" || LabelRow.Text == "Desk Mic Stand" || LabelRow.Text == "Internet Access")
             {
                 for (int i = 1; i <= 15; i++)
                 {
                         DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                         list.DataSource = dt1;// optCount;
                         list.DataBind();
                 }
             }

            else if (LabelRow.Text == "Gallery" || LabelRow.Text == "FlippersToWrite" || LabelRow.Text == "Tables to write" || LabelRow.Text == "Fixed Chairs") 
            {
                for (int i = 1; i <= 15; i++)
                {
                        DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                        list.DataSource = dt2;// optionsBool;
                        list.DataBind();
                }
            }
           else if (LabelRow.Text == "Room Number" || LabelRow.Text == "Capacity" || LabelRow.Text == "Table" || LabelRow.Text == "Chairs" )
           {
               for (int i = 1; i <= 15; i++)
               {
                       DropDownList list = (DropDownList)e.Item.FindControl("Col" + i);
                       TextBox txtbx = (TextBox)e.Item.FindControl("TextCol" + i);
                       txtbx.Visible = true;
                       list.Visible = false;
                       txtbx.DataBind();
                }
            }
        }
    }
   
    protected void BtnAddUpdate_Click(object sender, EventArgs e)
    {
        String SQLInsertCmd = "";
        String SQLUpdateCmd = "";
        String SQLUpdateValue = "";
        String SQLVal = "";
        String SQLExec = "";
        String SQLUpdatExec = "";
        String SQLEvalStr = "";
        Boolean AddUpdateFlag = false;
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        LblError.Text = " ";
        LblAddUpdate.Text ="";

        if (ddlEvent.SelectedValue == "-1")
        {
            LblError.Text = " Please Select Event"; //LblError.Text +
            goto Execute;
        }
        else if (ddlChapter.SelectedValue == "-1")
        {
            LblError.Text = " Please Select Chapter";
            goto Execute;
        }
        else if(ddlContestDate.SelectedValue == "-1")
        {
             LblError.Text = "Select Contest Date";
             goto Execute;
        }
        //else if (TextVendor.Text == "")
        //{
           // LblError.Text = " Enter Vendor Name";
          //  goto Execute;
        //}
        else if (TextBldgID.Visible == true)
        {
            if (TextBldgID.Text == "")
            {
                LblError.Text = " Enter Building(Bldg) ID";
                goto Execute;
            }
            if (TextBldgName.Text == "")
            {
                LblError.Text = " Enter Building(Bldg) Name";
                goto Execute;
            }
        }
        else if (TextBldgName.Visible  == true)
        {
            if (TextBldgName.Text == "")
            {
                 LblError.Text = " Enter Building(Bldg) Name";
                 goto Execute;
            }
        }

        String RoomNumber;//= new String[16];
        RoomNumber = "";
        String CurrRoomNumber = "";
        for (int i = 1; i < DataGridRoom.Columns.Count; i++)
        {
           
              SQLInsertCmd = "Insert into RoomList (ContestYear,EventID,Event,ChapterID,Chapter,ContestDate,Venue,BldgID,BldgName,RoomNumber,Floor,Capacity,Gallery,Flipperstowrite,Tablestowrite,FixedChairs,Podium,PodMic,PodLT,Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,MicStand,CreatedBy,CreatedDate) Values ( " + ddlContestYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text + "'," + ddlChapter.SelectedValue + ",'" + ddlChapter.SelectedItem.Text + "','"+ ddlContestDate.SelectedItem.Text +"','" + TextVendor.Text + "','" + (TextBldgID.Visible == true ? TextBldgID.Text : ddlBldgID.SelectedItem.Text) + "','" + (TextBldgName.Visible == true ? TextBldgName.Text : ddlBldgName.SelectedItem.Text) + "',"; //BldgIDVal , BldgNameVal
              SQLUpdateCmd = "Update RoomList Set ContestYear=" + ddlContestYear.SelectedValue + ",EventID=" + ddlEvent.SelectedValue + ",Event='" + ddlEvent.SelectedItem.Text + "',ChapterID=" + ddlChapter.SelectedValue + ",Chapter='" + ddlChapter.SelectedItem.Text + "',ContestDate='" + ddlContestDate.SelectedItem.Text + "',Venue='" + TextVendor.Text + "',BldgID='" + (TextBldgID.Visible == true ? TextBldgID.Text : ddlBldgID.SelectedItem.Text) + "',BldgName='" + (TextBldgName.Visible == true ? TextBldgName.Text : ddlBldgName.SelectedItem.Text) + "',";
           
              SQLUpdateValue = "";
              SQLVal = "";
             
              foreach (DataGridItem dg in DataGridRoom.Items)
              {
                  Label label = (Label)(dg.Cells[0].Controls[1]);//.FindControl(("Room Number")));
                  String GridRowValue = "";
                  String textValue = "";
                  TextBox textVal = ((TextBox)(dg.FindControl(("TextCol") + i))) ;
                  String ddlValue = ((DropDownList)dg.Cells[i].Controls[1]).SelectedValue;
                  
                  textValue =  textVal.Text ;
                  
                  GridRowValue= "'" + textValue + ddlValue + "',";

                  SQLVal = SQLVal + GridRowValue ;//"'" + textValue + ddlValue + "'," ;// +","; // textval + dropdown + ",";//
           
                  if (label.Text == "Room Number")
                  {
                      if (textValue != "")
                      {
                          bool inRooms = (RoomNumber).Contains("'" + textValue + "'");
                          if (inRooms == true)
                          {
                              LblError.Text = "Room Number must be Unique";
                              goto Execute;
                          }
                          else
                          {
                              RoomNumber = RoomNumber + "'" + textValue + "',";//check + ","; // 
                          }
                      }
                      else if (textValue == "")
                      {
                          if (i == 1)
                          {
                              LblError.Text = "Enter Room Numbers ";
                          }
                         goto Execute;
                       }
                     SQLUpdateValue = SQLUpdateValue + "RoomNumber=" + GridRowValue;
                     SQLEvalStr = "Select Count(*) From RoomList Where ChapterID=" + ddlChapter.SelectedValue + " And ContestYear=" + ddlContestYear.SelectedValue + " And BldgID='" + (TextBldgID.Visible == true ? TextBldgID.Text : ddlBldgID.SelectedItem.Text) + "' And BldgName='" + (TextBldgName.Visible == true ? TextBldgName.Text : ddlBldgName.SelectedItem.Text) + "' AND RoomNumber='" + textValue + "'"; //  TextBldgID.Text
                     CurrRoomNumber = textValue;
                  }
                  else if (label.Text == "Floor")
                  { 
                        SQLUpdateValue = SQLUpdateValue + "Floor=" + GridRowValue; 
                  }
                  else if (label.Text == "Capacity")
                  {
                      SQLUpdateValue = SQLUpdateValue + "Capacity=" + GridRowValue; 
                  }
                  else if (label.Text =="Gallery")
                  {
                      SQLUpdateValue = SQLUpdateValue + "Gallery=" + GridRowValue; 
                  }
                  else if (label.Text =="FlippersToWrite")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Flipperstowrite="+ GridRowValue; 
                  }
                  else if (label.Text =="Tables to write")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Tablestowrite=" + GridRowValue;           
                  }
                  else if (label.Text =="Fixed Chairs")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "FixedChairs="+ GridRowValue;                
                  }
                  else if (label.Text =="Podium")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Podium=" + GridRowValue;       
                  }
                  else if (label.Text =="Podium Mic")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "PodMic=" + GridRowValue;        
                  }
                  else if (label.Text =="Podium Laptop")
                  {
                          SQLUpdateValue = SQLUpdateValue + "PodLT=" + GridRowValue;        
                  }
                  else if (label.Text =="Projector")
                  { 
                          SQLUpdateValue = SQLUpdateValue + "Projector=" + GridRowValue;        
                  }
                  else if (label.Text =="Judge Table")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "JTable=" + GridRowValue;       
                  }
                  else if (label.Text =="Judge Chairs")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "JChair=" + GridRowValue;       
                  }
                  else if (label.Text =="Chief Judge Mic")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "CJMic=" + GridRowValue;       
                  }
                  else if (label.Text =="Child Mic")
                  { 
                           SQLUpdateValue = SQLUpdateValue +"ChildMic=" + GridRowValue;      
                  }
                  else if (label.Text =="Audio Mixer")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "AMixer=" + GridRowValue;        
                  }
                  else if (label.Text =="Child Mic Stand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "ChMicStand=" + GridRowValue;       
                  }
                  else if (label.Text =="Desk Mic Stand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "DeskMicStand=" + GridRowValue;      
                  }
                  else if (label.Text =="Audio Wires")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "AWires=" + GridRowValue;     
                  }
                  else if (label.Text =="Extra Mic")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "ExtraMic=" + GridRowValue;   
                  }
                  else if (label.Text =="Internet Access")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "IntAccess="+ GridRowValue;      
                  }
                  else if (label.Text =="Laptop")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "Laptop="+ GridRowValue;      
                  }
                  else if (label.Text =="Table")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "Tables="+ GridRowValue;      
                  }
                  else if (label.Text =="Chairs")
                  { 
                           SQLUpdateValue = SQLUpdateValue + "Chair=" + GridRowValue;       
                  }
                  else if (label.Text =="MicStand")
                  { 
                            SQLUpdateValue = SQLUpdateValue + "MicStand=" + GridRowValue;      
                  }

            }
            try
            {

              if (Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, SQLEvalStr)) > 0)
                  SQLUpdatExec = SQLUpdatExec + SQLUpdateCmd + SQLUpdateValue + "ModifiedBy=" + Session["LoginID"] + ",ModifiedDate=GETDATE() Where ChapterID=" + ddlChapter.SelectedValue + " And ContestYear=" + ddlContestYear.SelectedValue + " And BldgID='" + (TextBldgID.Visible == true ? TextBldgID.Text : ddlBldgID.SelectedItem.Text) + "' AND RoomNumber='" + CurrRoomNumber + "'";
              else  
                  SQLExec = SQLExec + SQLInsertCmd + SQLVal + Session["LoginID"] + ",GETDATE())";
            }
            catch(Exception)
            {
            
            }
         }

      
   Execute:
          
        if (LblError.Text == " ")
        {
            try
            {
                conn.Open();
                if (SQLExec.Length > 20)
                {
                    SqlCommand cmdIns = new SqlCommand(SQLExec, conn);
                    cmdIns.ExecuteNonQuery();
                    AddUpdateFlag =true;
                }
                if (SQLUpdatExec.Length > 20)
                {
                    SqlCommand cmdIns = new SqlCommand(SQLUpdatExec, conn);
                    cmdIns.ExecuteNonQuery();
                    AddUpdateFlag = true;
                }

                if (AddUpdateFlag == true)
                {
                    LblAddUpdate.Text = "Added/Updated Successfully";
                    LoadGrid();
                }
                conn.Close(); 
            }
            catch (SqlException ex)
            {
                Console.WriteLine(ex.ToString());
            }
        }
        else
        {
            LblAddUpdate.Text = "No Records Added";
        }
   }
    protected void BtnUpdateCancel_Click(object sender, EventArgs e)
    {
        GetContestYear(ddlContestYear);
       // GetEvent(ddlEvent, "1,2");
       // GetChapter(ddlChapter);
       // CheckEvent();
        
        TextVendor.Text = "";
        TextBldgID.Text = "";
        TextBldgName.Text = "";
        LblAddUpdate.Text = "";
        LblError.Text = "";

        GetContestDates();
        GetBuildingID();
        GetVendor(false);
        DGRoomList.Visible = false;
        ClearDataGridRoom();
    }
  
    private void LoadGrid()
    {
            DGRoomList.Visible = true;
            String strSQL ="";
            
            strSQL = "SELECT Chapter, ContestYear, BldgName, BldgID, RoomNumber,Floor ,Capacity,Gallery,Flipperstowrite,Tablestowrite,FixedChairs,Podium,PodMic,PodLT,";
            strSQL =  strSQL+" Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,MicStand ";
            strSQL = strSQL + " FROM RoomList WHERE ContestYear=" + ddlContestYear.SelectedValue + " And EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + " and Venue='" + TextVendor.Text + "' And BldgID='" + (TextBldgID.Visible == true ? TextBldgID.Text : ddlBldgID.SelectedItem.Text) + "' And BldgName='" + (TextBldgName.Visible == true ? TextBldgName.Text : ddlBldgName.SelectedItem.Text) + "'"; //BldgIDVal +"' and BldgName='" + BldgNameVal + "'";
          
            try
            {
                DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strSQL);
            
            DGRoomList.DataSource = ds;
            DGRoomList.DataBind();

            if (ds.Tables[0].Rows.Count <1)
                LblRecError.Text = "No records available.";
            else
                LblRecError.Text = "";

            }
            catch (Exception ex)
            {
             //   Response.Write(ex.ToString());
            }
    }

    private void GetContestDates()
    {
        String StrSQL = "Select  DISTINCT CONVERT(VARCHAR(10), c.ContestDate, 101) as ContestDate,c.ContestDate as Date From Contest c Where c.Contest_Year =" + ddlContestYear.SelectedValue + " and EventID = " + ddlEvent.SelectedValue + " and c.NSFChapterID =" + ddlChapter.SelectedValue + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlContestDate.DataSource = ds;
            ddlContestDate.DataTextField = "ContestDate";
            ddlContestDate.DataValueField = "Date";
            ddlContestDate.DataBind();
        }
        if (ds.Tables[0].Rows.Count > 1)
        {
        ddlContestDate.Items.Insert(0, new ListItem("Select Contest Date", "-1"));
        ddlContestDate.SelectedIndex = 0;
        }
        else if (ds.Tables[0].Rows.Count == 1)
        {
            GetVendor(true);
            GetBuildingID();
        }
    }
    private void GetVendor(Boolean dateflag)
    {
        String StrSQL = "";
        DataSet ds = new DataSet();

        //if (dateflag == false) //ddlContestDate.Visible == false) //ddlEvent.SelectedValue == "1")
        //{
        //    StrSQL = "Select DISTINCT AutoMemberID,ORGANIZATION_NAME,ChapterID From OrganizationInfo o Inner Join Contest c ON  o.AutoMemberID  = c.VenueId Where c.Contest_Year =" + ddlContestYear.SelectedValue + " and c.NSFChapterID  = " + ddlChapter.SelectedValue + "";
        //}
        //else //if (ddlEvent.SelectedValue == "2")
        //{
        //    StrSQL = "Select DISTINCT AutoMemberID,ORGANIZATION_NAME,ChapterID From OrganizationInfo o Inner Join Contest c ON  o.AutoMemberID  = c.VenueId Where c.Contest_Year =" + ddlContestYear.SelectedValue + " and c.NSFChapterID  = " + ddlChapter.SelectedValue + "";
        //}
        StrSQL = "Select DISTINCT AutoMemberID,ORGANIZATION_NAME,ChapterID From OrganizationInfo o Inner Join Contest c ON  o.AutoMemberID  = c.VenueId Where c.Contest_Year =" + ddlContestYear.SelectedValue + " and c.NSFChapterID  = " + ddlChapter.SelectedValue;
        if (ddlContestDate.SelectedIndex != 0)
        {
            StrSQL = StrSQL + " and ContestDate='" + ddlContestDate.SelectedItem.Value + "'";
        }
        ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);

        if (ds.Tables[0].Rows.Count > 0)
        {
            TextVendor.Text = ds.Tables[0].Rows[0]["ORGANIZATION_NAME"].ToString();
        }
        else
        {
            TextVendor.Text = "";
        }

    }
    private void GetBuildingID()
    {
        try
        {
            String StrSQL = "Select Distinct BldgID,BldgName From RoomList Where ContestYear= " + ddlContestYear.SelectedValue + " and EventId=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + " and Venue='" + TextVendor.Text + "'";// and ContestDate='" + ddlContestDate.SelectedItem.Text + "'";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, StrSQL);
            if (ds.Tables[0].Rows.Count > 0)
            {
                ddlBldgID.DataSource = ds;
                ddlBldgID.DataTextField = "BldgID";
                ddlBldgID.DataValueField = "BldgID";
                ddlBldgID.DataBind();

                ddlBldgName.DataSource = ds;
                ddlBldgName.DataTextField = "BldgName";
                ddlBldgName.DataValueField = "BldgName";
                ddlBldgName.DataBind();

                ddlBldgID.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "-1"));
                ddlBldgName.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "-1"));
          
            }
            else
            {
                ClearDataGridRoom();
                DGRoomList.Visible = false;
 
                ddlBldgID.Items.Clear();
                ddlBldgName.Items.Clear();

                ddlBldgID.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "-1"));
                ddlBldgName.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "-1"));
     
                ddlBldgID.SelectedIndex = -1; //.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "0"));
                ddlBldgName.SelectedIndex = -1;//.Items.Insert(ds.Tables[0].Rows.Count, new ListItem("Other", "0"));
            }
            AddTextBox();

            //if (ds.Tables[0].Rows.Count == 1)
            //{
            //    LoadDataGridRoom(DataGridRoom);
            //    LoadGrid();
            //}
            //else 
                if (ds.Tables[0].Rows.Count > 0)
            {
                ddlBldgID.Items.Insert(0, new ListItem("Select Building ID", "-2"));
                ddlBldgName.Items.Insert(0, new ListItem("Select Building Name", "-2"));

                ddlBldgID.SelectedIndex = 0;// ds.Tables[0].Rows.Count + 1;
                ddlBldgName.SelectedIndex = 0;// ds.Tables[0].Rows.Count + 1;
            }
           
        }
        catch(Exception ex)
        {
           // Response.Write(ex.ToString());
        }
    }
    private void AddTextBox()
    {
        if (ddlBldgID.SelectedItem.Text == "Other")
        {
            TextBldgID.Visible = true;
        }
        else
        {
            TextBldgID.Visible = false;
        }
        if (ddlBldgName.SelectedItem.Text == "Other")
        {
            TextBldgName.Visible = true;
        }
        else
        {
            TextBldgName.Visible = false;
        }
    }

    protected void ddlContestYear_SelectedIndexChanged(object sender, EventArgs e)
    {
        // To clear Old Data
        //GetEvent(ddlEvent, "1,2");
        //GetChapter(ddlChapter);
        ddlContestDate.Items.Clear();
        TextVendor.Text = "";
        TextBldgID.Text = "";
        TextBldgName.Text = "";

        GetContestDates();
        GetVendor(false);
        GetBuildingID();
    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        // To clear Old Data
        ddlContestDate.Items.Clear();
        TextVendor.Text = "";
        TextBldgID.Text = "";
        TextBldgName.Text = "";
        //GetBuildingID();
        //Changes
        GetChapter(ddlChapter);
        CheckEvent();
        //LoadGrid();
    }
    protected void ddlChapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        ClearDataGridRoom();
        DGRoomList.Visible = false;
        TextBldgID.Text = "";
        TextBldgName.Text="";
       
       // LoadGrid();
        GetContestDates();
        GetVendor(false);
        GetBuildingID();
        //if (ddlChapter.SelectedValue == "1")
        //{
        //    ddlEvent.SelectedValue = "1";
        //    CheckEvent();
        //}
        //else
        //{
        //    GetBuildingID();
        //}
    }

    protected void ddlContestDate_SelectedIndexChanged(object sender, EventArgs e)
    {
       GetVendor(true);
       GetBuildingID();
    }
    protected void ddlBldgID_SelectedIndexChanged(object sender, EventArgs e)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(),CommandType.Text,"Select Distinct BldgName From RoomList Where ContestYear=" + ddlContestYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + "and Venue='" +  TextVendor.Text + "' and BldgId='" + ddlBldgID.SelectedItem.Text +"'");
        if (ds.Tables[0].Rows.Count > 0)
        {
            ddlBldgName.DataSource = ds;
            ddlBldgName.DataBind();
            ddlBldgName.Items.Insert(ds.Tables[0].Rows.Count, "Other");
        }
        AddTextBox();
        LoadDataGridRoom(DataGridRoom);
        LoadGrid();
   }
    protected void ddlBldgName_SelectedIndexChanged(object sender, EventArgs e)
    {
        AddTextBox();
        LoadDataGridRoom(DataGridRoom);
        LoadGrid();
    }
    private void LoadDataGridRoom(DataGrid DataGridRoom)
    {
        ClearDataGridRoom();
        String strSQL = "";

        strSQL = "SELECT Chapter, ContestYear, BldgName, BldgID, RoomNumber,Floor ,Capacity,Gallery,Flipperstowrite,Tablestowrite,FixedChairs,Podium,PodMic,PodLT,";
        strSQL = strSQL + " Projector,JTable,JChair,CJMic,ChildMic,AMixer,ChMicStand,DeskMicStand,AWires,ExtraMic,IntAccess,Laptop,Tables,Chair,MicStand ";
        strSQL = strSQL + " FROM RoomList WHERE ContestYear=" + ddlContestYear.SelectedValue + " And EventID=" + ddlEvent.SelectedValue + " and ChapterID=" + ddlChapter.SelectedValue + " and Venue='" + TextVendor.Text + "' and BldgID ='" + ddlBldgID.SelectedItem.Text + "' and BldgName='" + ddlBldgName.SelectedItem.Text + "'";

        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strSQL);
    
        foreach (DataGridItem dg in DataGridRoom.Items)
        {
            for (int m = 0; m < ds.Tables[0].Rows.Count; m++)
            {
                    Label label = (Label)(dg.Cells[0].Controls[1]);
                   
                    if (label.Text == "Room Number")
                    {
                        ((TextBox)(dg.FindControl(("TextCol") + (m+1)))).Text =  ds.Tables[0].Rows[m]["RoomNumber"].ToString();
                    }
                    if (label.Text == "Floor")
                    {
                         ((DropDownList)dg.Cells[m+1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["Floor"]);
                    }
                    if (label.Text == "Capacity")
                    {
                        ((TextBox)(dg.FindControl(("TextCol") + (m + 1)))).Text = ds.Tables[0].Rows[m]["Capacity"].ToString();
                    }
                    if (label.Text == "Gallery")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedValue = ds.Tables[0].Rows[m]["Gallery"].ToString();
                    }
                    if (label.Text == "FlippersToWrite")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedValue = ds.Tables[0].Rows[m]["Flipperstowrite"].ToString();
                    }
                    if (label.Text == "Tables to write")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedValue = ds.Tables[0].Rows[m]["Tablestowrite"].ToString();
                    }
                    if (label.Text == "Fixed Chairs")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedValue = ds.Tables[0].Rows[m]["FixedChairs"].ToString();
                    }
                    if (label.Text == "Podium")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["Podium"]);
                    }
                    if (label.Text == "Podium Mic")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["PodMic"]);
                    } 
                    if (label.Text == "Podium Laptop")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["PodLT"]);
                    } 
              
                    if (label.Text == "Projector")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["Projector"]);
                    } 
                    if (label.Text == "Judge Table")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["JTable"]);
                    }
                    if (label.Text == "Judge Chairs")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["JChair"]);
                    }
                    if (label.Text == "Chief Judge Mic")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["CJMic"]);
                    }
                    if (label.Text == "Child Mic")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["ChildMic"]);
                    }
                    if (label.Text == "Audio Mixer")
                    {
                       ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["AMixer"]);
                    }
                    if (label.Text == "Child Mic Stand")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["ChMicStand"]);
                    }
                    if (label.Text == "Desk Mic Stand")
                    {
                         ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["DeskMicStand"]);
                    }
                    if (label.Text == "Audio Wires")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["AWires"]);
                    }
                    if (label.Text == "Extra Mic") 
                    {
                         ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["ExtraMic"]);
                    }
                    if (label.Text == "Internet Access")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["IntAccess"]);
                    }
                    if (label.Text == "Laptop")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["Laptop"]);
                    }
                    if (label.Text == "Table")
                    {
                        ((TextBox)(dg.FindControl(("TextCol") + (m + 1)))).Text = ds.Tables[0].Rows[m]["Tables"].ToString();
                    }
                    if (label.Text == "Chairs")
                    {
                        ((TextBox)(dg.FindControl(("TextCol") + (m + 1)))).Text = ds.Tables[0].Rows[m]["Chair"].ToString();
                    }
                    if (label.Text == "MicStand")
                    {
                        ((DropDownList)dg.Cells[m + 1].Controls[1]).SelectedIndex = Convert.ToInt32(ds.Tables[0].Rows[m]["MicStand"]);
                    }
               }
          }
    }
    private void ClearDataGridRoom()
    {
        for (int i = 1; i < DataGridRoom.Columns.Count; i++)
        {
            foreach (DataGridItem dg in DataGridRoom.Items)
            {
                ((TextBox)(dg.FindControl(("TextCol") + i))).Text = "";
                ((DropDownList)dg.Cells[i].Controls[1]).ClearSelection();
            }
        }
    }
}
