'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/MainPage.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page is the Main entry poin to the Volunteer Registration 
'           sub system. This pages allows the user to choose various
'           options basing the role thy signed on.
'
'           Will utilize the following stored procedures
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Namespace VRegistration

Partial Class MainPage
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        Dim SecureUrl As String
        If Not Page.IsPostBack Then
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost") Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
        End If
    End Sub

End Class

End Namespace

