﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;

public partial class CoachClassCalNew : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Server.Transfer("maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {
                hdnLoginID.Value = Session["LoginID"].ToString();
                hdnRoleID.Value = Session["RoleID"].ToString(); ;
            }
        }
    }

    public class CoachClassCal
    {

        public string CoachClassCalID { get; set; }
        public string MemberId { get; set; }
        public string EventId { get; set; }
        public string EventYear { get; set; }
        public string EventCode { get; set; }
        public string ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductId { get; set; }
        public string Product { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public string SessionNo { get; set; }
        public string EndDate { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Time { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string SerNo { get; set; }
        public string WeekNo { get; set; }
        public string Status { get; set; }
        public string Substitute { get; set; }
        public string Makeup { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Reason { get; set; }
        public string ClassType { get; set; }
        public string SubstituteCoachName { get; set; }
        public string ClassStartDate { get; set; }
        public string ClassEndDate { get; set; }
        public string CoachpaperID { get; set; }
        public string HWDeadlineDate { get; set; }
        public string HWDueDate { get; set; }
        public string ARelDate { get; set; }
        public string SRelDate { get; set; }
        public string LoginID { get; set; }
        public string RetVal { get; set; }

    }

    public class ProductGroups
    {
        public string ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductId { get; set; }
        public string Product { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public string SessionNo { get; set; }
        public int MemberID { get; set; }
        public string Coachname { get; set; }
        public string Eventyear { get; set; }
    }


    [WebMethod]
    public static List<CoachClassCal> ListSchedule(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "  select CoachClassCalID,CL.[MemberId],CL.[EventId],CL.[EventYear] ,CL.[ProductGroupId] ,CL.[ProductGroup],CL.[ProductId] ,CL.[Product],CL.[Semester],CL.[Level],CL.[SessionNo] ,CL.[Date] ,CL.[Day] ,CL.[Time],CL.[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],CL.[CreateDate],CL.[CreatedBy],CL.[ModifyDate],CL.[ModifiedBy], Makeup, ClassType, IP.FirstName+' '+IP.lastname as name, CD.QReleaseDate, CD.QDeadLineDate, CD.AReleaseDate, CD.SReleaseDate from CoachClassCal CL left join Indspouse Ip on (Ip.AutoMemberID=Cl.Substitute) left join Coachpapers CP on (CP.WeekId=Cl.WeekNo and CP.Semester=CL.Semester and CP.Eventyear=Cl.Eventyear and CP.ProductgroupID=CL.ProductGroupID and CP.ProductID=CL.ProductID and CP.Level=CL.Level) left join CoachRelDates CD on (CD.CoachPaperID=CP.CoachpaperID and CD.memberID=Cl.MemberID and CD.Semester=CL.Semester and CD.Eventyear=Cl.Eventyear and CD.ProductgroupID=CL.ProductGroupID and CD.ProductID=CL.ProductID and CD.Level=CL.Level and CD.Session=CL.SessionNo) where CL.EventYear=" + CoachClassCal.EventYear + " and CL.Semester='" + CoachClassCal.Semester + "' and CL.EventID=" + CoachClassCal.EventId + " and CL.ProductGroupID=" + CoachClassCal.ProductGroupId + " and CL.ProductId=" + CoachClassCal.ProductId + " and CL.Level='" + CoachClassCal.Level + "' and CL.SessionNo=" + CoachClassCal.SessionNo + " and CL.MemberId=" + CoachClassCal.MemberId + "";

            if (Convert.ToInt32(CoachClassCal.CoachClassCalID) > 0)
            {
                cmdText += " and CoachClassCalID=" + CoachClassCal.CoachClassCalID + "";
            }

            cmdText += " order by WeekNo";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();

                        objCoachClassCal.CoachClassCalID = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.MemberId = dr["MemberId"].ToString();
                        objCoachClassCal.EventId = dr["EventId"].ToString();
                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
                        objCoachClassCal.Product = dr["Product"].ToString();
                        objCoachClassCal.Semester = dr["Semester"].ToString();
                        objCoachClassCal.Level = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = dr["Day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
                        objCoachClassCal.Status = dr["Status"].ToString();
                        objCoachClassCal.Reason = dr["Reason"].ToString();
                        objCoachClassCal.Substitute = dr["Substitute"].ToString();
                        objCoachClassCal.Makeup = dr["Makeup"].ToString();
                        objCoachClassCal.ClassType = dr["ClassType"].ToString();
                        objCoachClassCal.SubstituteCoachName = dr["name"].ToString();
                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "")
                        {
                            objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }

                        objListSurvey.Add(objCoachClassCal);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }





    [WebMethod]
    public static List<CoachClassCal> PostNewClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "insert into coachclasscal ([MemberId],[EventId],[EventYear] ,[ProductGroupId] ,[ProductGroup],[ProductId] ,[Product],[Semester],[Level],[SessionNo] ,[Date] ,[Day] ,[Time],[Duration],[SerNo],[WeekNo],[Status],[Substitute],[Reason],[CreateDate],[CreatedBy], Makeup, ClassType) values(" + objCoachClass.MemberId + "," + objCoachClass.EventId + "," + objCoachClass.EventYear + "," + objCoachClass.ProductGroupId + ",'" + objCoachClass.ProductGroup + "', " + objCoachClass.ProductId + ", '" + objCoachClass.Product + "', '" + objCoachClass.Semester + "', '" + objCoachClass.Level + "', " + objCoachClass.SessionNo + ", '" + objCoachClass.Date + "', '" + objCoachClass.Day + "','" + objCoachClass.Time + "', " + objCoachClass.Duration + ", 1, " + objCoachClass.WeekNo + ", '" + objCoachClass.Status + "', " + objCoachClass.Substitute + ", null, GetDate(), 4240, null, '" + objCoachClass.ClassType + "')";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "1";
            cmdText = "select ISNULL(CoachPaperID, 0) as CoachpaperID from CoachPapers where EventYear =" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and  ProductID=" + objCoachClass.ProductId + " and WeekId=" + objCoachClass.WeekNo + " and Level='" + objCoachClass.Level + "' and DocType='Q' and Semester='" + objCoachClass.Semester + "'";

            cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {

                    objCP.CoachpaperID = ds.Tables[0].Rows[0]["CoachpaperID"].ToString();


                }
            }
            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> VerifyRegularClassStatus(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "  select status from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and WeekNo=" + CoachClassCal.WeekNo + " and MemberId=" + CoachClassCal.MemberId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();


                        objCoachClassCal.Status = dr["Status"].ToString();

                        objListSurvey.Add(objCoachClassCal);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static int UpdateCoachClassSchedule(CoachClassCal objCoachClass)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Update CoachClassCal set Status='" + objCoachClass.Status + "' where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            retVal = 1;

        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return retVal;

    }

    [WebMethod]
    public static List<CoachClassCal> GetClassStartDateAndEndDate(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            DateTime dtClassDate = new DateTime();

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "  select distinct StartDate, EndDate, (Select distinct Day from CalSignup where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and Accepted='Y') as day,  (Select distinct Time from CalSignup where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "'  and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and Accepted='Y') as Time from CoachingDateCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);

            cmdText = "select distinct Date from CoachClasscal where Eventyear=" + CoachClassCal.EventYear + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo='" + CoachClassCal.SessionNo + "' and MemberID=" + CoachClassCal.MemberId + " and WeekNo=" + (Convert.ToInt32(CoachClassCal.WeekNo)) + " and ClassType='Regular'";

            string Date = string.Empty;
            string RegularClsDate = string.Empty;
            DateTime dtRegularClassDate = new DateTime();
            try
            {
                Date = SqlHelper.ExecuteScalar(System.Configuration.ConfigurationManager.AppSettings["DBConnection"], CommandType.Text,
 cmdText).ToString();
                RegularClsDate = Convert.ToDateTime(Date).ToString("MM/dd/yyyy");
                dtRegularClassDate = Convert.ToDateTime(RegularClsDate);
            }
            catch
            {
            }


            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();


                        objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.ClassEndDate = Convert.ToDateTime(dr["EndDate"].ToString()).ToString("MM/dd/yyyy");

                        string classDay = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("dddd");

                        if (RegularClsDate == "")
                        {
                            if (classDay == dr["day"].ToString())
                            {
                                dtClassDate = Convert.ToDateTime(dr["StartDate"].ToString());
                                objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).ToString("MM/dd/yyyy");
                                if (Convert.ToInt32(CoachClassCal.WeekNo) > 1)
                                {
                                    objCoachClassCal.ClassStartDate = Convert.ToDateTime(dr["StartDate"].ToString()).AddDays(Convert.ToInt32(CoachClassCal.WeekNo)).ToString("MM/dd/yyyy");

                                }
                            }
                            else
                            {
                                dtClassDate = Convert.ToDateTime(dr["StartDate"].ToString());
                                DateTime dtDate = new DateTime();
                                DateTime dtCoachDate = new DateTime();
                                for (int i = 1; i <= 7; i++)
                                {

                                    dtDate = dtClassDate.AddDays(i);
                                    if (Convert.ToDateTime(dtDate.ToShortDateString()).ToString("dddd") == dr["day"].ToString())
                                    {
                                        objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtDate.ToString()).ToString("MM/dd/yyyy");
                                        dtCoachDate = Convert.ToDateTime(dtDate.ToString());
                                    }
                                }
                                if (Convert.ToInt32(CoachClassCal.WeekNo) > 1)
                                {
                                    objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtCoachDate.ToString()).AddDays((Convert.ToInt32(CoachClassCal.WeekNo) - 1) * 7).ToString("MM/dd/yyyy");

                                }

                            }
                        }
                        else
                        {
                            objCoachClassCal.ClassStartDate = Convert.ToDateTime(dtRegularClassDate.ToString()).AddDays(7).ToString("MM/dd/yyyy");
                        }

                        objCoachClassCal.HWDueDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(5).ToString("MM/dd/yyyy");
                        objCoachClassCal.ARelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(7).ToString("MM/dd/yyyy");
                        objCoachClassCal.SRelDate = Convert.ToDateTime(objCoachClassCal.ClassStartDate).AddDays(-1).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = dr["day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        objListSurvey.Add(objCoachClassCal);


                    }




                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static List<CoachClassCal> GetWeekNo(CoachClassCal CoachClassCal)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {
            DateTime dtClassDate = new DateTime();

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = " select Isnull(Max(WeekNo),0) as WeekNo from CoachClassCal where EventYear=" + CoachClassCal.EventYear + " and Semester='" + CoachClassCal.Semester + "' and EventID=" + CoachClassCal.EventId + " and ProductGroupID=" + CoachClassCal.ProductGroupId + " and ProductId=" + CoachClassCal.ProductId + " and Level='" + CoachClassCal.Level + "' and SessionNo=" + CoachClassCal.SessionNo + " and (ClassType='Regular' or ClassType='Substitute' or ClassType='Makeup') and Status='On' and MemberId=" + CoachClassCal.MemberId + "";

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();


                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();

                        objListSurvey.Add(objCoachClassCal);


                    }


                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }

    [WebMethod]
    public static int InsertCoachRelDate(CoachClassCal objCoachClass)
    {

        int retVal = -1;

        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string sqlCommand = "usp_Insert_CoachRelDate";
            SqlParameter[] param = new SqlParameter[10];
            param[0] = new SqlParameter("@CoachPaperId", objCoachClass.CoachpaperID);
            param[1] = new SqlParameter("@Semester", objCoachClass.Semester);
            param[2] = new SqlParameter("@SessionNo", objCoachClass.SessionNo);
            param[3] = new SqlParameter("@QReleaseDate", objCoachClass.HWDeadlineDate);
            param[4] = new SqlParameter("@QDeadlineDate", objCoachClass.HWDueDate);
            param[5] = new SqlParameter("@AReleaseDate", objCoachClass.ARelDate);
            param[6] = new SqlParameter("@SReleaseDate", objCoachClass.SRelDate);
            param[7] = new SqlParameter("@CreateDate", System.DateTime.Now);
            param[8] = new SqlParameter("@CreatedBy", Int32.Parse(objCoachClass.LoginID));
            param[9] = new SqlParameter("@MemberId", Int32.Parse(objCoachClass.MemberId));

            SqlHelper.ExecuteDataset(cn, CommandType.StoredProcedure, sqlCommand, param);
            retVal = 1;
        }
        catch (Exception ex)
        {
            retVal = -1;
        }
        return retVal;

    }

    [WebMethod]
    public static List<ProductGroups> ListproductGroups(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select  distinct ProductGroupID, ProductGroupCode from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.ProductGroupId = dr["ProductGroupID"].ToString();
                        objProductGroup.ProductGroup = dr["ProductGroupCode"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }


    [WebMethod]
    public static List<ProductGroups> ListCoaches(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct CS.MemberID, IP.Firstname +' ' + IP.lastname as Name from CalSignup CS inner join Indspouse IP on (CS.memberID=IP.AutoMemberID) where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "' and Accepted='Y'";

            if (objProdGrp.MemberID > 0)
            {
                cmdText += " and CS.memberID=" + objProdGrp.MemberID + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.MemberID = Convert.ToInt32(dr["MemberID"].ToString());
                        objProductGroup.Coachname = dr["Name"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListLevel(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct Level from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + " and ProductID=" + objProdGrp.ProductId + " and ProductGroupID=" + objProdGrp.ProductGroupId + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.Level = dr["Level"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListProducts(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct productId, productCode from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.ProductId = dr["productId"].ToString();
                        objProductGroup.Product = dr["productCode"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<ProductGroups> ListSessionNo(ProductGroups objProdGrp)
    {

        int retVal = -1;
        List<ProductGroups> objListPrdgrp = new List<ProductGroups>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "select distinct SessionNo from CalSignup where Eventyear=" + objProdGrp.Eventyear + " and Semester='" + objProdGrp.Semester + "'  and memberID=" + objProdGrp.MemberID + "  and ProductGroupID=" + objProdGrp.ProductGroupId + " and ProductID=" + objProdGrp.ProductId + " and Level ='" + objProdGrp.Level + "' and Accepted='Y'";



            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        ProductGroups objProductGroup = new ProductGroups();

                        objProductGroup.SessionNo = dr["SessionNo"].ToString();

                        objListPrdgrp.Add(objProductGroup);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListPrdgrp;

    }

    [WebMethod]
    public static List<CoachClassCal> DeleteClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "Delete from CoachClassCal where CoachClassCalID=" + objCoachClass.CoachClassCalID + "";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            cmd = new SqlCommand(cmdText, cn);
            cmd.ExecuteNonQuery();
            CoachClassCal objCP = new CoachClassCal();
            retVal = 1;


            objListCP.Add(objCP);

        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            retVal = -1;
        }
        return objListCP;

    }

    [WebMethod]
    public static List<CoachClassCal> ValidatemakeupExtraClass(CoachClassCal objCoachClass)
    {

        int retVal = -1;
        List<CoachClassCal> objListCP = new List<CoachClassCal>();
        try
        {
            string RegulaClassDay = string.Empty;
            string RegularTime = string.Empty;

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string Cmdtext = string.Empty;
            Cmdtext = "select distinct Day, Time from CoachClasscal where Eventyear=" + objCoachClass.EventYear + " and ProductGroupID=" + objCoachClass.ProductGroupId + " and ProductId=" + objCoachClass.ProductId + " and Level='" + objCoachClass.Level + "' and SessionNo='" + objCoachClass.SessionNo + "' and MemberID=" + objCoachClass.MemberId + " and ClassType='Regular'";

            SqlCommand cmd = new SqlCommand(Cmdtext, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    RegulaClassDay = ds.Tables[0].Rows[0]["Day"].ToString();
                    RegularTime = Convert.ToDateTime(ds.Tables[0].Rows[0]["Time"].ToString()).ToString("HH:mm");
                }
            }

            CoachClassCal objCP = new CoachClassCal();
            string ToDay = DateTime.Now.ToShortDateString();
            DateTime dtDate = Convert.ToDateTime(ToDay);
            string ClassDate = Convert.ToDateTime(objCoachClass.Date).ToString("MM/dd/yyyy");

            DateTime dtCoachDate = Convert.ToDateTime(ClassDate);
            if (dtCoachDate > dtDate)
            {
                objCP.RetVal = "1";

                string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                {
                    objCP.RetVal = "-2";
                }
            }
            else if (dtCoachDate == dtDate)
            {
                ToDay = DateTime.Now.ToString("MM/dd/yyyy HH:mm");
                dtDate = Convert.ToDateTime(ToDay);
                ClassDate = objCoachClass.Date + " " + objCoachClass.Time;
                ClassDate = Convert.ToDateTime(ClassDate).ToString("MM/dd/yyyy HH:mm");
                dtCoachDate = Convert.ToDateTime(ClassDate);
                if (dtCoachDate > dtDate)
                {
                    objCP.RetVal = "1";
                }
                else
                {
                    objCP.RetVal = "-1";
                }

                string MakeupDay = Convert.ToDateTime(ClassDate).ToString("dddd");

                if (MakeupDay == RegulaClassDay && RegularTime == objCoachClass.Time)
                {
                    objCP.RetVal = "-2";
                }
            }
            else
            {
                objCP.RetVal = "-1";
            }

            objListCP.Add(objCP);
        }
        catch (Exception ex)
        {
            CoachClassCal objCP = new CoachClassCal();
            objCP.RetVal = "-1";
            objListCP.Add(objCP);
        }
        return objListCP;

    }



}