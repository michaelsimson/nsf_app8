﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;


public partial class CoachingStatusGUI : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {

        if (Session["LoginID"] == null)
        {
            Server.Transfer("maintest.aspx");
        }
        if (!IsPostBack)
        {
            if (Session["LoginID"] != null)
            {

            }
        }
    }
    public class CoachClassCal
    {

        public string CoachClassCalID { get; set; }
        public string MemberId { get; set; }
        public string EventId { get; set; }
        public string EventYear { get; set; }
        public string EventCode { get; set; }
        public string ProductGroupId { get; set; }
        public string ProductGroup { get; set; }
        public string ProductId { get; set; }
        public string Product { get; set; }
        public string Semester { get; set; }
        public string Level { get; set; }
        public string SessionNo { get; set; }
        public string EndDate { get; set; }
        public string Date { get; set; }
        public string Day { get; set; }
        public string Time { get; set; }
        public int Duration { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string HPhone { get; set; }
        public string CPhone { get; set; }
        public string SerNo { get; set; }
        public string WeekNo { get; set; }
        public string Status { get; set; }
        public string Substitute { get; set; }
        public string Makeup { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string Reason { get; set; }
        public string ClassType { get; set; }
        public string SubstituteCoachName { get; set; }
        public string ClassStartDate { get; set; }
        public string ClassEndDate { get; set; }
        public string CoachpaperID { get; set; }
        public string HWDeadlineDate { get; set; }
        public string HWDueDate { get; set; }
        public string ARelDate { get; set; }
        public string SRelDate { get; set; }
        public string LoginID { get; set; }
        public string RetVal { get; set; }
        public string CoachName { get; set; }
        public string ParentName { get; set; }
        public string ChildName { get; set; }

    }

    [WebMethod]
    public static List<CoachClassCal> ListSchedule(int CoachClassCalID)
    {

        int retVal = -1;
        List<CoachClassCal> objListSurvey = new List<CoachClassCal>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = string.Empty;
            cmdText = "   select CoachClassCalID,CL.[MemberId],CL.[EventId],CL.[EventYear] ,CL.[ProductGroupId] ,CL.[ProductGroup],CL.[ProductId] ,CL.[Product],CL.[Semester],CL.[Level],CL.[SessionNo] ,CL.[Date] ,CL.[Day] ,CL.[Time],CL.[Duration],[SerNo],[WeekNo],CL.[Status],[Substitute],[Reason],CL.[CreateDate],CL.[CreatedBy],CL.[ModifyDate],CL.[ModifiedBy], Makeup, ClassType, IP.FirstName+' '+IP.lastname as name, CD.QReleaseDate, CD.QDeadLineDate, CD.AReleaseDate, CD.SReleaseDate, C.First_Name +' '+C.Last_name as ChildName, IP1.FirstName+' '+IP1.lastname as CoachName, IP2.FirstName+' '+IP2.lastname as ParentName from CoachClassCal CL left join Indspouse Ip on (Ip.AutoMemberID=Cl.Substitute)  left join Coachpapers CP on (CP.WeekId=Cl.WeekNo and CP.Semester=CL.Semester and CP.Eventyear=Cl.Eventyear and CP.ProductgroupID=CL.ProductGroupID and CP.ProductID=CL.ProductID and CP.Level=CL.Level) left join CoachRelDates CD on (CD.CoachPaperID=CP.CoachpaperID and CD.memberID=Cl.MemberID and CD.Semester=CL.Semester and CD.Eventyear=Cl.Eventyear and CD.ProductgroupID=CL.ProductGroupID and CD.ProductID=CL.ProductID and CD.Level=CL.Level and CD.Session=CL.SessionNo) inner join coachReg CR on (CR.CmemberID=CL.memberID and CR.ProductGroupId=CL.ProductgroupId and CR.productId=CL.ProductId and CR.level=CL.Level and CR.SessionNo=CL.SessionNo and CR.Semester=Cl.Semester) left join Indspouse IP1 on (IP1.AutoMemberID=CR.CMemberID) left join Indspouse IP2 on (IP2.AutoMemberID=CR.pmemberID) inner join Child C on(C.ChildNumber=CR.ChildNumber) where Cl.Eventyear=2016 and Cl.Semester='Spring' and CR.ChildNumber='29123' and Cl.Status='On'";

            if (CoachClassCalID > 0)
            {
                cmdText += " and CoachClassCalID=" + CoachClassCalID + "";
            }

            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        CoachClassCal objCoachClassCal = new CoachClassCal();

                        objCoachClassCal.CoachClassCalID = dr["CoachClassCalID"].ToString();
                        objCoachClassCal.MemberId = dr["MemberId"].ToString();
                        objCoachClassCal.EventId = dr["EventId"].ToString();
                        objCoachClassCal.EventYear = dr["EventYear"].ToString();
                        objCoachClassCal.ProductGroupId = dr["ProductGroupId"].ToString();
                        objCoachClassCal.ProductGroup = dr["ProductGroup"].ToString();
                        objCoachClassCal.ProductId = dr["ProductId"].ToString();
                        objCoachClassCal.Product = dr["Product"].ToString();
                        objCoachClassCal.Semester = dr["Semester"].ToString();
                        objCoachClassCal.Level = dr["Level"].ToString();
                        objCoachClassCal.SessionNo = dr["SessionNo"].ToString();
                        objCoachClassCal.Date = Convert.ToDateTime(dr["Date"].ToString()).ToString("MM/dd/yyyy");
                        objCoachClassCal.Day = dr["Day"].ToString();
                        objCoachClassCal.Time = Convert.ToDateTime(dr["Time"].ToString()).ToString("HH:mm");
                        objCoachClassCal.Duration = Convert.ToInt32(dr["Duration"].ToString());
                        objCoachClassCal.WeekNo = dr["WeekNo"].ToString();
                        objCoachClassCal.Status = dr["Status"].ToString();
                        objCoachClassCal.Reason = dr["Reason"].ToString();
                        objCoachClassCal.Substitute = dr["Substitute"].ToString();
                        objCoachClassCal.Makeup = dr["Makeup"].ToString();
                        objCoachClassCal.ClassType = dr["ClassType"].ToString();
                        objCoachClassCal.SubstituteCoachName = dr["name"].ToString();

                        objCoachClassCal.CoachName = dr["CoachName"].ToString();
                        objCoachClassCal.ParentName = dr["ParentName"].ToString();
                        objCoachClassCal.ChildName = dr["ChildName"].ToString();

                        if (dr["QReleaseDate"] != null && dr["QReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.HWDeadlineDate = Convert.ToDateTime(dr["QReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["QDeadlineDate"] != null && dr["QDeadlineDate"].ToString() != "")
                        {
                            objCoachClassCal.HWDueDate = Convert.ToDateTime(dr["QDeadlineDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["SReleaseDate"] != null && dr["SReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.SRelDate = Convert.ToDateTime(dr["SReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }
                        if (dr["AReleaseDate"] != null && dr["AReleaseDate"].ToString() != "")
                        {
                            objCoachClassCal.ARelDate = Convert.ToDateTime(dr["AReleaseDate"].ToString()).ToString("MM/dd/yyyy");
                        }

                        objListSurvey.Add(objCoachClassCal);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;

    }
}