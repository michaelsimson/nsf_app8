﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VolAdminDashboard.aspx.cs" Inherits="VolAdminDashboard" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <!-- Bootstrap Core CSS -->
    <%-- <link href="css/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="css/vendor/metisMenu/metisMenu.min.css" rel="stylesheet">--%>

    <!-- DataTables CSS -->
    <link href="css/vendor/datatables-plugins/dataTables.bootstrap.css" rel="stylesheet">

    <!-- DataTables Responsive CSS -->
    <link href="css/vendor/datatables-responsive/dataTables.responsive.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="css/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

    <script src="css/vendor/jquery/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="css/vendor/bootstrap/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="css/vendor/metisMenu/metisMenu.min.js"></script>

    <!-- DataTables JavaScript -->
    <script src="css/vendor/datatables/js/jquery.dataTables.min.js"></script>
    <script src="css/vendor/datatables-plugins/dataTables.bootstrap.min.js"></script>
    <script src="css/vendor/datatables-responsive/dataTables.responsive.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="css/dist/js/sb-admin-2.js"></script>
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <link href="css/ezmodal.css" rel="stylesheet" />
    <script src="js/ezmodal.js"></script>
    <link href="css/Loader.css" rel="stylesheet" />

     <link href="css/jquery.toast.css" rel="stylesheet" />

    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker.min.css" />
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/css/datepicker3.min.css" />

    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.3.0/js/bootstrap-datepicker.min.js"></script>

    <script src="js/jquery.toast.js"></script>

    <script type="text/javascript">

        $(function (e) {
            
            listSchool();

        });

        function listSchool() {
            showLoader();


            $.ajax({
                type: "POST",
                url: "StudentDashboard.aspx/ListSchool",
                contentType: "application/json; charset=utf-8",
                dataType: "json",
                async: true,
                cache: false,
                success: function (data) {
                    alert(2);
                    //  hideLoader();
                    var tblHtml = "";
                    tblHtml += "<thead>";
                    tblHtml += "<tr>";

                    tblHtml += "<th>System Code</th>";
                    tblHtml += "<th>Region</th>";
                    tblHtml += "<th>State</th>";
                    tblHtml += "<th>School Id</th>";
                    tblHtml += "<th>School Name</th>";
                    tblHtml += "<th>City</th>";
                    tblHtml += "<th>District</th>";
                    tblHtml += "<th>Pincode</th>";
                    tblHtml += "<th>Email</th>";
                    tblHtml += "<th>Contact Name</th>";
                    tblHtml += "<th>Phone</th>";
                    tblHtml += "<th>Address 1</th>";
                    tblHtml += "<th>Address 2</th>";

                    tblHtml += "</tr>";
                    tblHtml += "</thead>";
                    tblHtml += "<tbody>";
                    $.each(data.d, function (index, value) {
                        tblHtml += "<tr>";

                        tblHtml += "<td>" + value.SystemCode + "</td>";
                        tblHtml += "<td>" + value.Region + "</td>";
                        tblHtml += "<td>" + value.StateCode + "</td>";
                        tblHtml += "<td>" + value.SchoolID + "</td>";
                        tblHtml += "<td>" + value.SchoolName + "</td>";
                        tblHtml += "<td>" + value.TownCity + "</td>";
                        tblHtml += "<td>" + value.District + "</td>";

                        tblHtml += "<td>" + value.PinCode + " </td>";
                        tblHtml += "<td>" + value.Email + " </td>";
                        tblHtml += "<td>" + value.ContactName + " </td>";
                        tblHtml += "<td>" + value.ContactPhone + " </td>";
                        tblHtml += "<td>" + value.Address1 + " </td>";
                        tblHtml += "<td>" + value.Address2 + " </td>";

                        tblHtml += "</tr>";
                    });
                    tblHtml += "</tbody>";
                    $('#tblSchool').html(tblHtml);
                    /**/ $('#tblSchool').DataTable({
                        responsive: true,
                        destroy: true
                    });

                    getAvailableSubjects();
                },
                failure: function (response) {
                    //  alert(response.d);
                }
            });
            hideLoader();
        }

        function showLoader() {
            $("#fountainTextG").css("display", "block");
            $("#overlay").css("display", "block");
        }

        function hideLoader() {
            $("#fountainTextG").css("display", "none");
            $("#overlay").css("display", "none");
        }
        function ValidateEmail(email) {
            var expr = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
            return expr.test(email);
        };

    </script>

    <div class="row" style="padding-top: 10px">
        <div class="col-lg-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <i class="fa fa-list fa-fw"></i>School Details
                  
                      
                </div>
                <!-- /.panel-heading -->
                <div class="panel-body">
                    <div id="divtblSchool">
                        <table id="tblSchool" style="width: 100%;" class="table table-striped table-bordered table-hover">
                        </table>
                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.panel-body -->
            </div>
            <!-- /.panel -->
        </div>
        <!-- /.col-lg-12 -->
    </div>

    <div id="fountainTextG" style="position: fixed; top: 30%; left: 40%; text-align: center; display: none;">
        <div class="sk-fading-circle">
            <div class="sk-circle1 sk-circle"></div>
            <div class="sk-circle2 sk-circle"></div>
            <div class="sk-circle3 sk-circle"></div>
            <div class="sk-circle4 sk-circle"></div>
            <div class="sk-circle5 sk-circle"></div>
            <div class="sk-circle6 sk-circle"></div>
            <div class="sk-circle7 sk-circle"></div>
            <div class="sk-circle8 sk-circle"></div>
            <div class="sk-circle9 sk-circle"></div>
            <div class="sk-circle10 sk-circle"></div>
            <div class="sk-circle11 sk-circle"></div>
            <div class="sk-circle12 sk-circle"></div>
        </div>
    </div>
    <div id="overlay"></div>

</asp:Content>
