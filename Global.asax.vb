Imports System.Web
Imports System.Web.SessionState
Imports System.Text
Imports System.IO
Imports System.Collections
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.Web.Mail

Public Class Global
    Inherits System.Web.HttpApplication

#Region " Component Designer Generated Code "

    Public Sub New()
        MyBase.New()

        'This call is required by the Component Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

    'Required by the Component Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Component Designer
    'It can be modified using the Component Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        components = New System.ComponentModel.Container()
    End Sub

#End Region

    Sub Application_Start(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when the application is started
        Application("ConnectionString") = ConfigurationSettings.AppSettings("DBConnection")
        Application("ContestYear") = ConfigurationSettings.AppSettings("Contest_Year")
        Application("ContestType") = ConfigurationSettings.AppSettings("Contest_Finals")
        Application("EventID") = ConfigurationSettings.AppSettings("EventID")
        Application("NationalFinalsCity") = ConfigurationSettings.AppSettings("NationalFinalsCity")
        Application("FounderEmail") = ConfigurationSettings.AppSettings("FounderEMail")
        Application("LateRegistrationDate") = ConfigurationSettings.AppSettings("NationalFinalsLateDate")
        Application("LateFee") = ConfigurationSettings.AppSettings("NationalFinalsLateFee")
        Application("ContestEmail") = ConfigurationSettings.AppSettings("ContestEmail")
    End Sub

    Sub Application_BeginRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires at the beginning of each request
    End Sub

    Sub Application_AuthenticateRequest(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires upon attempting to authenticate the use
    End Sub

    Sub Application_Error(ByVal sender As Object, ByVal e As EventArgs)
        ' Fires when an error occurs

        Dim prmArray(5) As SqlParameter
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        Dim LastError As Exception = Server.GetLastError()
        Dim objExcept As Exception

        cmd.Connection = conn
        cmd.CommandType = CommandType.StoredProcedure
        cmd.CommandText = "usp_InsertAppExceptions"
        conn.Open()

        Dim objErr As Exception = Server.GetLastError().GetBaseException
        Dim err As String = "Error in: " & Request.Url.ToString() & " Error Message: " & objErr.Message.ToString() & " Stack Trace: " & objErr.StackTrace.ToString()


        err.Replace(vbCrLf, "     ")

        If Len(err) > 4000 Then
            cmd.Parameters.Add("@ExceptionText", DbType.String).Value = err.Substring(1, 4000)
        Else
            cmd.Parameters.Add("@ExceptionText", DbType.String).Value = err
        End If


        '*** Send EMail to the Contests@northsouth.org
        Dim mailObj As New MailMessage
        mailObj.From = "contests@northsouth.org"
        mailObj.To = Application("ContestEmail")
        mailObj.Cc = "chitturi@mail.org"
        mailObj.Subject = "2006 Registration System Exception: "
        mailObj.Body = err

        SmtpMail.SmtpServer = ""
        'SmtpMail.Send(mailObj)


        If Not Session("LoginEmail") Is Nothing Then
            cmd.Parameters.Add("@LoginID", DbType.String).Value = Session("LoginEmail")
        Else
            cmd.Parameters.Add("@LoginID", DbType.String).Value = 0
        End If

        If Not Session("IndID") Is Nothing Then
            If Session("IndID") > 0 Then
                cmd.Parameters.Add("@IndId", DbType.UInt32).Value = Session("IndID")
            Else
                cmd.Parameters.Add("@IndId", DbType.UInt32).Value = Session("ChapterID")
            End If
        Else
            cmd.Parameters.Add("@IndId", DbType.UInt32).Value = 0
        End If

        If Session("SpouseID") > 0 Then
            cmd.Parameters.Add("@SpouseID", DbType.UInt32).Value = Session("SpouseID")
        Else
            cmd.Parameters.Add("@SpouseID", DbType.UInt32).Value = 0
        End If

        cmd.ExecuteNonQuery()

        conn.Close()

    End Sub
    Private Sub Global_BeginRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.BeginRequest
    End Sub
    Private Sub Global_EndRequest(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.EndRequest
    End Sub
End Class
