﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="BeeBookSchedule.aspx.cs" Inherits="BeeBook_BeeBookSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table id="tblBeeBookSchedule" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: auto;" class="tableclass">
        <tr>
            <td class="ContentSubTitle" valign="top" align="center" colspan="2">
                <center>
                    <h2>Bee Book Schedule</h2>
                </center>
            </td>
        </tr>
        <tr>
            <td>
                <table style="margin-left: 0px;">

                    <tr>
                        <td style="width: 150px"></td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Year </b></td>

                        <td align="left" nowrap="nowrap">
                            <asp:DropDownList ID="ddYear" runat="server" Width="150px"
                                AutoPostBack="True">
                                <asp:ListItem Value="0">Select year</asp:ListItem>
                                <asp:ListItem Value="-1">All</asp:ListItem>
                                <asp:ListItem Value="2014">2014</asp:ListItem>
                                <asp:ListItem Value="2015">2015</asp:ListItem>
                            </asp:DropDownList>

                        </td>

                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Event</b> </td>
                        <td style="width: 141px" align="left">
                            <asp:DropDownList ID="ddEvent" runat="server" Width="150px" AutoPostBack="True">
                                <asp:ListItem Value="0">Select Event</asp:ListItem>
                                <asp:ListItem Value="19">Prepclub</asp:ListItem>
                                <asp:ListItem Value="21">[National]</asp:ListItem>
                                <asp:ListItem Value="13">Coaching</asp:ListItem>
                                <asp:ListItem Value="3">Workshop</asp:ListItem>
                                <asp:ListItem Value="2">ChapterContests</asp:ListItem>
                                <asp:ListItem Value="1">Finals</asp:ListItem>
                                <asp:ListItem Value="-1">All</asp:ListItem>
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap" id="lbchapter" runat="server">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Chapter</b> </td>
                        <td style="width: 141px" runat="server" align="left" id="ddchapterdrop">
                            <asp:DropDownList ID="ddchapter" DataTextField="ChapterCode" DataValueField="ChapterID" runat="server" Width="150px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td align="left" nowrap="nowrap">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>Teams</b>
                        </td>
                        <td style="width: 141px" align="left">
                            <asp:DropDownList ID="ddTeams" DataTextField="TeamName" DataValueField="TeamID" runat="server" Width="155px" AutoPostBack="True">
                            </asp:DropDownList>
                        </td>
                        <td>
                            <asp:Button ID="btnSubmit" runat="server" Text="Submit" OnClick="btnSubmit_Click" />
                        </td>

                        <td>

                            <asp:Button ID="btnAddNew" runat="server" Text="AddNew" OnClick="btnAddNew_Click" />
                        </td>




                    </tr>

                </table>
                <br />
                <div style="text-align: center">
                    <asp:Label ID="lblMsg" runat="server"></asp:Label>
                </div>
            </td>
        </tr>
        <tr>
            <td align="center">
                <center>
                    <table runat="server" visible="false" cellpadding="4" cellspacing="0" border="0" width="50%" style="background-color: #ffffcc;" class="tableclass" id="tblAddUpdateTaskActivities">
                        <tr>
                            <td align="center" colspan="2">
                                <h2>ADD/UPDATE TASK AND ACTIVITIES</h2>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" colspan="3"></td>
                        </tr>
                        <tr class="ContentSubTitle">
                            <td align="right">Task</td>
                            <td align="left" style="width: 114px">
                                <asp:DropDownList ID="ddlTask" runat="server" Width="155px" Height="23px">
                                    <asp:ListItem Value="" Selected="True">Select</asp:ListItem>

                                </asp:DropDownList></td>
                            <td style="width: 69px">
                                <asp:Button ID="btnAddNewtask" runat="server" Text="Add New Task" OnClick="btnAddNewtask_Click" /></td>

                        </tr>
                        <tr class="ContentSubTitle" id="trAddNewTask" runat="server" style="display: none;">
                            <td align="right" style="height: 11px">Task</td>
                            <td align="left" style="height: 11px; width: 114px">
                                <asp:TextBox ID="txtTask" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                            <td align="left" style="height: 11px; width: 125px">
                                <asp:Button ID="btnAddtask" runat="server" Text="Add" OnClick="btnAddtask_Click" />

                                <asp:Button ID="btnTaskCancel" runat="server" Text="Cancel" OnClick="btnTaskCancel_Click" /></td>
                        </tr>
                        <tr class="ContentSubTitle">
                            <td align="right">Activities</td>
                            <td align="left" style="width: 114px">
                                <asp:TextBox ID="txtActivities" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>

                        <tr class="ContentSubTitle">
                            <td align="right">Start Date</td>
                            <td align="left" style="width: 114px">
                                <asp:TextBox ID="txtStartDate" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr class="ContentSubTitle">
                            <td align="right">Duration</td>
                            <td align="left" style="width: 114px">
                                <asp:TextBox ID="txtDuration" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr class="ContentSubTitle">
                            <td align="right">End Date</td>
                            <td align="left" style="width: 114px">
                                <asp:TextBox ID="txtEndDate" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>
                        <tr class="ContentSubTitle">
                            <td align="right">Year</td>
                            <td align="left" style="width: 114px">
                                <asp:TextBox ID="txtYear" runat="server" Width="150px" Height="18px"></asp:TextBox></td>
                        </tr>

                        <tr class="ContentSubTitle">
                            <td align="center"></td>
                            <td align="left" style="width: 114px">
                                <asp:Button ID="btnAddUpdate" runat="server" Text="Add" Width="60" OnClick="btnAddUpdate_Click" />
                                <asp:Button ID="btnCancel" runat="server" Text="Cancel" Width="60" OnClick="btnCancel_Click" />
                            </td>
                        </tr>

                        <tr>
                            <td align="center" colspan="2">
                                <asp:Label ID="lblError" runat="server" Text="" ForeColor="Red"></asp:Label></td>
                        </tr>
                    </table>
                </center>
                <br />
                <asp:GridView ID="grdBeeBookSchedule" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="100%" HeaderStyle-BackColor="#ffffcc">
                    <Columns>
                        <asp:TemplateField>
                            <ItemTemplate>
                                <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                                <div style="display: none;">
                                    <asp:Label ID="lblComponentID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ComponentID") %>'>'></asp:Label>
                                    <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'>'></asp:Label>
                                </div>
                            </ItemTemplate>
                        </asp:TemplateField>
                        <asp:BoundField DataField="ID" Visible="true" HeaderText="ID" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="Event" Visible="true" HeaderText="Event" HeaderStyle-Width="50px" />
                        <asp:BoundField DataField="Chapter" Visible="true" HeaderText="Chapter"></asp:BoundField>
                        <asp:BoundField DataField="Components" Visible="true" HeaderText="Task"></asp:BoundField>
                        <asp:BoundField DataField="SubComponents" Visible="true" HeaderText="Activities"></asp:BoundField>
                        <asp:BoundField DataField="Year" Visible="true" HeaderText="Year"></asp:BoundField>
                        <asp:BoundField DataField="Duration" Visible="true" HeaderText="Duration" ItemStyle-HorizontalAlign="Center"></asp:BoundField>
                        <asp:BoundField DataField="EndDate" Visible="true" HeaderText="End Date"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </td>
        </tr>
    </table>
</asp:Content>
