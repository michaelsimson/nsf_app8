Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports Microsoft.ApplicationBlocks.Data


Namespace VRegistration


Partial Class ChangePassword
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Put user code to initialize the page here
            Dim SecureUrl As String
            If Session("entryToken").ToString() = "Parent" Then
                hlinkParentRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Volunteer" Then
                hlinkVolunteerRegistration.Visible = True
            ElseIf Session("entryToken").ToString() = "Donor" Then
                hlinkDonorFunctions.Visible = True
            End If
            If (Not Request.IsSecureConnection And Not Request.Url.Host = "localhost") Then
                SecureUrl = "https://" & Request.Url.Host & Request.Url.PathAndQuery
                Response.Redirect(SecureUrl, True)
            End If
    End Sub

    Private Sub btnLogin_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles btnLogin.Click
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim UserId As String = Session("LoginEmail")
            Dim UpdateSQL As String = "UPDATE login_master SET user_pwd='" + txtPassword.Text + "' where user_email='" + txtEmail.Text + "' and user_pwd='" + txtOldPassword.Text + "'"
            
        Dim param(2) As SqlParameter
            param(0) = New SqlParameter("@Email", Trim(txtEmail.Text))
            param(1) = New SqlParameter("@Password", Trim(txtOldPassword.Text))
            Dim strRoles As String = SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, "getUserRole", param)
            If strRoles = "" Then
                txtErrMsg.Visible = True
            Else

                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, UpdateSQL)
                txtErrMsg.Visible = True
                txtEmail.Text = ""
                txtErrMsg.Text = "Your Password has been updated Successfully."
            End If
    End Sub
End Class

End Namespace

