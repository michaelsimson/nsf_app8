﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports NorthSouth.BAL
Partial Class CoachingSummary
    Inherits System.Web.UI.Page
    Public cnTemp As SqlConnection
    Dim ChildNumCount As Integer = 0
    Dim Year As Integer

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        cnTemp = New SqlConnection(Application("ConnectionString"))
        If Not Page.IsPostBack Then
            If Session("EventYear") Is Nothing Then
                Server.Transfer("login.aspx?entry=p")
            End If
            btnYear.Text = Session("CoachSeasonHeader")
            LoadData()
            hlbl.Text = "N"
        End If
    End Sub

    Private Sub LoadData()
        '***************************************************
        '***Get IndID and SpouseID for the givn Logon Person
        '***************************************************
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim StrIndSpouse As String = ""
        Dim intIndID As Integer = 0
        Dim dsIndSpouse As New DataSet

        StrIndSpouse = "Email='" & Session("LoginEmail") & "'"

        Dim objIndSpouse As New IndSpouse10
        objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsIndSpouse, StrIndSpouse)

        If dsIndSpouse.Tables.Count > 0 Then
            If dsIndSpouse.Tables(0).Rows.Count > 0 Then
                If dsIndSpouse.Tables(0).Rows.Count > 1 Then
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    ElseIf dsIndSpouse.Tables(0).Rows(1).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(1).Item("AutoMemberID")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName") &
                    " and " & dsIndSpouse.Tables(0).Rows(1).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(1).Item("LastName")

                Else
                    If dsIndSpouse.Tables(0).Rows(0).Item("DonorType") = "IND" Then
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    Else
                        intIndID = dsIndSpouse.Tables(0).Rows(0).Item("Relationship")
                    End If
                    lblParentName.Text = dsIndSpouse.Tables(0).Rows(0).Item("FirstName") & " " & dsIndSpouse.Tables(0).Rows(0).Item("LastName")
                End If
                Session("IndID") = intIndID
                Session("ParentFName") = dsIndSpouse.Tables(0).Rows(0).Item("FirstName")
                Session("ParentLName") = dsIndSpouse.Tables(0).Rows(0).Item("LastName")

                '**************************
                '*** Spouse Info Capturing
                '**************************
                Dim StrSpouse As String = ""
                Dim intSpouseID As Integer = 0
                Dim dsSpouse As New DataSet
                StrSpouse = "Relationship='" & Session("IndID") & "'"


                objIndSpouse.SearchIndSpouseWhere(Application("ConnectionString"), dsSpouse, StrSpouse)
                If dsSpouse.Tables.Count > 0 Then
                    If dsSpouse.Tables(0).Rows.Count > 0 Then
                        intSpouseID = dsSpouse.Tables(0).Rows(0).Item("AutoMemberID")
                    End If
                End If

                Session("SpouseID") = intSpouseID

                '********************************************************
                '*** Populate Parent Info on the Page
                '********************************************************
                Dim drIndSpouse As DataRow = dsIndSpouse.Tables(0).Rows(0)

                lblAddress1.Text = drIndSpouse.Item("Address1")
                lblAddress2.Text = drIndSpouse.Item("Address2")
                ' lblCity.Text = drIndSpouse.Item("City")
                lblStateZip.Text = drIndSpouse.Item("City") & ", " & drIndSpouse.Item("state") & " " & drIndSpouse.Item("zip")
                If drIndSpouse.Item("HPhone").ToString <> "" Then
                    lblHomePhone.Text = drIndSpouse.Item("HPhone") & "(Home)"
                Else
                    lblHomePhone.Text = "Home Phone Not Provided"
                End If
                If drIndSpouse.Item("CPhone").ToString <> "" Then
                    lblCellPhone.Text = drIndSpouse.Item("CPhone") & "(Cell)"
                Else
                    lblCellPhone.Text = "Cell Phone Not Provided"
                End If
                If drIndSpouse.Item("WPhone").ToString <> "" Then
                    lblWorkPhone.Text = drIndSpouse.Item("WPhone") & "(Work)"
                Else
                    lblWorkPhone.Text = "Work Phone Not Provided"
                End If
                lblEMail.Text = drIndSpouse.Item("EMail")

            End If
        End If

        Dim objChild As New Child
        Dim dsChild As New DataSet
        Dim prmArray(2) As SqlParameter
        prmArray(0) = New SqlParameter("@MemberID", Session("IndID"))
        prmArray(1) = New SqlParameter("@Year", Session("EventYear"))
        Session("HasAdultChild") = Nothing
        Session("HasAdult") = Nothing
        dsChild = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_GetParentChildInfo", prmArray)
        If dsChild.Tables.Count > 0 Then
            If dsChild.Tables(0).Select("MemberType in ('Ind','Spouse')").Count > 0 Then
                Session("HasAdult") = "Y"
                If dsChild.Tables(0).Select("MemberType ='Child'").Count > 0 Then
                    Session("HasAdultChild") = "Y"
                End If
            End If

            dgChildList.DataSource = dsChild.Tables(0)
            dgChildList.DataBind()
            Session("ChildCount") = dsChild.Tables(0).Select("MemberType='Child'").Count '.Rows.Count
            ViewState("ChildInfo") = dsChild
            If dsChild.Tables(0).Rows.Count = 0 Then
                btnRegister.Enabled = False
            End If
            Try


                Dim AdultId As String = String.Empty
                For i As Integer = 0 To dsChild.Tables(0).Rows.Count - 1

                    If (dsChild.Tables(0).Rows(i)("ChildNumber").ToString().IndexOf("A") > 0) Then
                        AdultId = AdultId & dsChild.Tables(0).Rows(i)("ChildNumber").ToString().Substring(0, dsChild.Tables(0).Rows(i)("ChildNumber").ToString().Length - 1) & ","
                    End If
                Next
                AdultId = AdultId.TrimEnd(",")
                Dim count As Integer = 0
                Dim CmdText As String = "select count(*) from ExCoaching where AdultId in (" & AdultId & ") and EventYear=" & Session("EventYear").ToString() & ""
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, CmdText)
                If (count > 0) Then
                    btnRegister.Enabled = True
                End If
            Catch ex As Exception

            End Try
        End If
    End Sub


    Protected Sub dgChildList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgChildList.ItemDataBound
        Dim SelCount As Integer = 0
        Select Case e.Item.ItemType
            Case ListItemType.Header
                If Not Session("HasAdult") Is Nothing And Session("EventID") = 13 Then
                    e.Item.Cells(0).Text = "Name of Parent"
                    lblHeader.Text = "Parent Detailed Information"
                    If Not Session("HasAdultChild") Is Nothing Then
                        e.Item.Cells(0).Text = "Name of Parent/Child"
                        lblHeader.Text = "Parent/Child Detailed Information"
                    End If
                End If
            Case ListItemType.Item, ListItemType.AlternatingItem
                Try

                    Dim SQLStr As String = ""
                    Dim SQLStr1 As String = ""
                    Dim dsInvitees As New DataSet
                    Dim tblEligible() As String = {"EligibleCoach"}
                    Dim memberType As String = e.Item.DataItem("MemberType")
                    'If memberType.ToString().ToLower().StartsWith("ind") Or memberType.ToString().ToLower().StartsWith("spouse") Then
                    '    e.Item.Cells(0).Text = "Name of Parent/Child"
                    '    lblHeader.Text = "Parent/Child Detailed Information"
                    'End If
                    SQLStr = "Select PRODUCTNAME,MAX(LATEREGDLDATE) LATEREGDLDATE, STATUS,PRODUCTID  from ( "
                    If memberType = "Child" Then
                        SQLStr = SQLStr & " Select Distinct P.Name as ProductName,EF.LateRegDLDate,P.Status,P.ProductID from Child Ch"
                        SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & " and Ef.EventYear >=" & Session("Year") & " and Ef.RegOpen='Y'"
                        SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
                        SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and EF.Semester= C.Semester"
                        SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                        SQLStr = SQLStr & " Left Join ChildCoachLevel CL ON CL.Level =C.Level " 'Updated on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                        SQLStr = SQLStr & " and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'" ' C.Level = CL.Level 
                        SQLStr = SQLStr & " where  ch.ChildNumber = " & e.Item.DataItem("ChildNumber") & " and (Ch.GRADE Between EF.GradeFrom and Ef.GradeTo  or  P.ProductGroupCode='UV')" ' Commented on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                        SQLStr = SQLStr & " AND ((EF.CoachSelCriteria IS NULL OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' AND CL.Eligibility IS NOT NULL ))" ' and P.Status='O'

                        SQLStr = SQLStr & " UNION Select distinct P.Name as ProductName,Ex.NewDeadLine LateRegDLDate,P.Status,P.ProductId from Child ch "
                        SQLStr = SQLStr & " Inner Join EventFees EF  ON  EF.EventID=" & Session("EventID") & " and Ef.EventYear >=" & Session("Year")
                        SQLStr = SQLStr & " Inner Join Product P ON P.ProductId = EF.ProductID  "
                        SQLStr = SQLStr & " Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and EF.Semester= C.Semester"
                        SQLStr = SQLStr & " Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID "
                        SQLStr = SQLStr & " INNER JOIN ExCoaching Ex On Ex.EventId=" & Session("EventID") & " and Ex.ProductID=P.ProductId and Ex.ProductGroupID=P.ProductGroupId and Ex.EventYear=" & Session("Year")
                        SQLStr = SQLStr & " and Ex.ChildNumber= ch.ChildNumber and Ex.CMemberId=I.AutoMemberId and GETDATE() < DateAdd(dd, 1,Ex.NewDeadLine) "
                        SQLStr = SQLStr & " Left Join ChildCoachLevel CL ON CL.Level =C.Level "
                        SQLStr = SQLStr & " and Ch.ChildNumber = CL.ChildNumber and C.ProductID = CL.ProductID and C.EventYear = CL.EventYear and CL.Eligibility='Y'"
                        SQLStr = SQLStr & " where  ch.ChildNumber = " & e.Item.DataItem("ChildNumber") & " and (Ch.GRADE Between EF.GradeFrom and Ef.GradeTo  or  P.ProductGroupCode='UV')" ' Commented on 09-09-2014 as Levels are added to UV-Product similar to SAT Product.
                        SQLStr = SQLStr & " AND ((EF.CoachSelCriteria IS NULL  OR EF.CoachSelCriteria='O') OR (EF.CoachSelCriteria='I' AND CL.Eligibility IS NOT NULL  ))" ' and P.Status='O'
                    Else
                        SQLStr = SQLStr & "Select Distinct P.Name as ProductName,EF.LateRegDLDate,P.Status,P.ProductID from Indspouse ad "
                        SQLStr = SQLStr & "Inner Join EventFees EF  ON  EF.EventID=13 and Ef.EventYear >=" & Session("Year") & " and EF.Adult='Y'" & " and EF.RegOpen='Y'"
                        SQLStr = SQLStr & "Inner Join Product P ON P.ProductId = EF.ProductID   "
                        SQLStr = SQLStr & "Inner Join CalSignUp C  ON P.ProductId = C.ProductID and EF.EventYear=C.EventYear and EF.Semester= C.Semester "
                        SQLStr = SQLStr & "Inner Join IndSpouse I ON C.MemberID=I.AutoMemberID   "
                        SQLStr = SQLStr & "where ad.Automemberid in (" & e.Item.DataItem("memberid") & ")"
                    End If
                    SQLStr = SQLStr & " ) PrdHis GROUP BY PRODUCTNAME,STATUS,PRODUCTID  Order by ProductId "
                    SqlHelper.FillDataset(Application("ConnectionString"), CommandType.Text, SQLStr, dsInvitees, tblEligible)

                    If dsInvitees.Tables(0).Rows.Count <= 0 And memberType = "Child" Then
                        ChildNumCount = ChildNumCount + 1
                        lblContestInfo.Text = "There is no coaching available to match your grade level."
                        lblContestInfo.Visible = True
                    Else
                        hlbl.Text = "Y"
                        lblContestInfo.Visible = False
                    End If
                    If hlbl.Text = "Y" Then
                        lblContestInfo.Visible = False
                    End If
                    Dim sbContest As New StringBuilder, strContestDesc As String
                    strContestDesc = ""
                    sbContest.Append("<ul>")
                    Dim sblevel As New StringBuilder
                    sblevel.Append("<ul>")
                    Dim i As Integer = 0
                    Dim conn As New SqlConnection(Application("ConnectionString"))
                    Dim hasRecord As Boolean = False
                    For Each dr As DataRow In dsInvitees.Tables(0).Rows
                        'If dr.Item("ChildNumber") = e.Item.DataItem("ChildNumber") Then
                        If strContestDesc <> dr.Item("ProductName") Then
                            If dr.Item("ProductName") = "Universal Values 201" Then
                                'If sbContest.ToString.Contains("Universal Values 101") = True Or sbContest.ToString.Contains("Universal Values 301") = True Then
                                '    Continue For
                                'End If
                                'Commented and updated on 11-09-2014  '******************UV201 was taken before: (Criteria to take UV301)*************************'
                                SQLStr1 = " Select Count(CoachRegID) as count from CoachReg where EventYear<" & Session("EventYear") & " and ProductCode = 'UV101' and Approved='Y' and childnumber =  " & e.Item.DataItem("ChildNumber") & ""

                                conn.Open()
                                Dim drContestCategory As SqlDataReader
                                drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr1)
                                If drContestCategory.Read() Then
                                    If drContestCategory("count") = 0 Then
                                        dr.Item("ProductName") = ""
                                    End If
                                End If
                                conn.Close()
                            ElseIf dr.Item("ProductName") = "Universal Values 301" Then
                                'If sbContest.ToString.Contains("Universal Values 101") = True Or sbContest.ToString.Contains("Universal Values 201") = True Then
                                '    Continue For
                                'End If
                                'Commented and updated on 11-09-2014  '******************UV101 was taken before: (Criteria to take UV201)*************************'
                                'SQLStr1 = SQLStr1 & "Select Count(*) as count from CoachReg CR1 where exists (select * from CoachReg CR2 where EventYear<" & Now.Year & " and CR1.childnumber = CR2.Childnumber and CR2.ProductCode = 'UV201' and Approved='Y') and CR1.childnumber =  " & e.Item.DataItem("ChildNumber") & " and CR1.ProductCode='UV301'"
                                SQLStr1 = " Select Count(CoachRegID) as count from CoachReg where EventYear<" & Session("EventYear") & " and ProductCode = 'UV201' and Approved='Y' and childnumber =  " & e.Item.DataItem("ChildNumber") & ""
                                'Response.Write(SQLStr1)
                                conn.Open()
                                Dim drContestCategory As SqlDataReader
                                drContestCategory = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, SQLStr1)
                                If drContestCategory.Read() Then
                                    If drContestCategory("count") = 0 Then
                                        dr.Item("ProductName") = ""
                                    End If
                                End If
                                conn.Close()
                            ElseIf dr.Item("ProductName") = "Universal Values 101" Then

                            End If
                            If dr.Item("ProductName") <> "" Then
                                sbContest.Append("<li>" & dr.Item("ProductName"))

                                If dr.Item("LateRegDLDate") < Now.Date() Then
                                    sbContest.Append(" - Deadline passed")
                                    SelCount = SelCount + 1
                                ElseIf dr.Item("Status") <> "O" And Now.Date() < dr.Item("LateRegDLDate") Then '  
                                    sbContest.Append(" -  Not Opened for Registration")
                                    SelCount = SelCount + 1
                                Else
                                    hasRecord = True
                                End If
                                sbContest.Append("</li>")
                            End If
                            strContestDesc = dr.Item("ProductName")
                        End If
                        ' End If

                        If dsInvitees.Tables(0).Rows.Count = SelCount And memberType = "Child" Then
                            ChildNumCount = ChildNumCount + 1
                        End If
                    Next

                    If ChildNumCount = Session("ChildCount") And Session("HasAdult") Is Nothing Then
                        btnRegister.Enabled = False
                    Else
                        btnRegister.Enabled = True
                    End If
                    If hasRecord = False Then
                        btnRegister.Enabled = False
                    End If



                    sbContest.Append("</ul>")
                    sblevel.Append("</ul>")
                    CType(e.Item.FindControl("lblEligibleContests"), Label).Text = sbContest.ToString
                    If CType(e.Item.FindControl("lblEligibleContests"), Label).Text = "" Then

                    End If
                    'CType(e.Item.FindControl("lbllevel"), Label).Text = sblevel.ToString
                Catch ex As Exception
                    lblContestInfo.Visible = True
                    lblContestInfo.Text = lblContestInfo.Text & "<br> " & ex.ToString()
                End Try

        End Select
    End Sub

    Protected Sub btnRegister_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnRegister.Click
        Response.Redirect("~/CoachingRegistration.aspx")
    End Sub
End Class
