Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Net
Imports System.Net.Mail
Imports NorthSouth.DAL
Imports System.Data.SqlClient
Partial Class CreateIndSpouse
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If Not Session("LoggedIn").ToString().Equals("true", StringComparison.CurrentCultureIgnoreCase) Then
            Server.Transfer("login.aspx")
        End If
        If Not IsPostBack Then
            Dim strSql As String
            Dim sBody As String
            'strSql = "SELECT * FROM INDDUPLICATE WHERE AutoMemberID=" & Request.QueryString("ID")
            'strSql = strSql & " AND DonorType IN('IND','SPOUSE') AND Status<>'Completed' ORDER BY DonorType"
            strSql = "SELECT * FROM INDDUPLICATE WHERE IndDupID=" & Request.QueryString("DupID")
            strSql = strSql & " AND DonorType IN('IND') AND Status<>'Completed' ORDER BY DonorType"
            'Dim drIndDuplicate As SqlDataReader
            'drIndDuplicate = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            Dim dsIndDuplicate As New DataSet
            dsIndDuplicate = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            Dim objIndividual As IndSpouse10
            Dim objSpouse As IndSpouse10
            Dim indID As Double
            Dim spouseID As Double
            Dim bIsIndividual As Boolean
            Dim bIsSpouse As Boolean
            bIsIndividual = False
            Dim iChapterID As Integer
            Dim strChapter As String
            bIsSpouse = False
            iChapterID = 0
            Dim strDonorType As String = ""
            Dim i As Integer
            Dim strEmail As String
            Dim strIndName As String
            Dim strSpouseName As String
            Dim strEmail1 As String
            Dim strEmail2 As String
            If dsIndDuplicate.Tables(0).Rows.Count > 0 Then
                For i = 0 To dsIndDuplicate.Tables(0).Rows.Count - 1
                    objIndividual = New IndSpouse10
                    strDonorType = dsIndDuplicate.Tables(0).Rows(i)("DonorType").ToString()
                    If strDonorType = "IND" And bIsIndividual = False Then
                        With objIndividual
                            .Title = dsIndDuplicate.Tables(0).Rows(i)("Title").ToString()
                            .FirstName = dsIndDuplicate.Tables(0).Rows(i)("FirstName").ToString()
                            .LastName = dsIndDuplicate.Tables(0).Rows(i)("LastName").ToString()
                            strIndName = StrConv(dsIndDuplicate.Tables(0).Rows(i)("FirstName").ToString(), VbStrConv.ProperCase)
                            .DonorType = "IND"
                            .CreateDate = Now
                            .MemberSince = Now
                            .Address1 = dsIndDuplicate.Tables(0).Rows(i)("Address1").ToString()
                            .Address2 = dsIndDuplicate.Tables(0).Rows(i)("Address2").ToString()
                            .City = dsIndDuplicate.Tables(0).Rows(i)("City").ToString()
                            .State = dsIndDuplicate.Tables(0).Rows(i)("State").ToString()
                            .Zip = dsIndDuplicate.Tables(0).Rows(i)("Zip").ToString()
                            If Len(dsIndDuplicate.Tables(0).Rows(i)("Email").ToString()) = 0 Then
                                .Email = ""
                            Else
                                .Email = dsIndDuplicate.Tables(0).Rows(i)("Email").ToString()
                            End If
                            strEmail = "'" & dsIndDuplicate.Tables(0).Rows(i)("Email").ToString() & "'"
                            strEmail1 = dsIndDuplicate.Tables(0).Rows(i)("Email").ToString()
                            .HPhone = dsIndDuplicate.Tables(0).Rows(i)("HPhone").ToString()
                            .CPhone = dsIndDuplicate.Tables(0).Rows(i)("CPhone").ToString()
                            .Country = dsIndDuplicate.Tables(0).Rows(i)("Country").ToString()
                            .Gender = dsIndDuplicate.Tables(0).Rows(i)("Gender").ToString()
                            .WPhone = dsIndDuplicate.Tables(0).Rows(i)("WPhone").ToString()
                            .WFax = dsIndDuplicate.Tables(0).Rows(i)("WFax").ToString()
                            .SecondaryEmail = dsIndDuplicate.Tables(0).Rows(i)("SecondaryEmail").ToString()
                            .Education = dsIndDuplicate.Tables(0).Rows(i)("Education").ToString()
                            .Career = dsIndDuplicate.Tables(0).Rows(i)("Career").ToString()
                            .Employer = dsIndDuplicate.Tables(0).Rows(i)("Employer").ToString()
                            .CountryOfOrigin = dsIndDuplicate.Tables(0).Rows(i)("CountryOfOrigin").ToString()
                            .StateOfOrigin = dsIndDuplicate.Tables(0).Rows(i)("StateOfOrigin").ToString()
                            .VolunteerFlag = dsIndDuplicate.Tables(0).Rows(i)("VolunteerFlag").ToString()
                            .ReferredBy = dsIndDuplicate.Tables(0).Rows(i)("ReferredBy").ToString()
                            .Chapter = dsIndDuplicate.Tables(0).Rows(i)("Chapter").ToString()
                            strChapter = dsIndDuplicate.Tables(0).Rows(i)("Chapter").ToString()
                            If Not IsDBNull(dsIndDuplicate.Tables(0).Rows(i)("ChapterID")) Then
                                .ChapterID = dsIndDuplicate.Tables(0).Rows(i)("ChapterID")
                                iChapterID = dsIndDuplicate.Tables(0).Rows(i)("ChapterID")
                            End If
                            .MaritalStatus = dsIndDuplicate.Tables(0).Rows(i)("MaritalStatus").ToString()
                            .Liaison = dsIndDuplicate.Tables(0).Rows(i)("Liaison").ToString()
                            .Sponsor = dsIndDuplicate.Tables(0).Rows(i)("Sponsor").ToString()
                            .VolunteerRole1 = Nothing
                            .VolunteerRole2 = Nothing
                            .VolunteerRole3 = Nothing
                            If .CreateDate = Nothing Then
                                .CreateDate = Now
                            End If
                            If .MemberSince = Nothing Then
                                .MemberSince = Now
                            End If
                            Dim sqlDateTime As SqlTypes.SqlDateTime
                            sqlDateTime = SqlTypes.SqlDateTime.Null

                            .ModifyDate = sqlDateTime
                            .CreatedBy = Session("LoginID")
                            .ModifiedBy = Nothing
                            .ModifyDate = Now
                            .PrimaryContact = "No"
                            Session("ParentLName") = .LastName
                            Session("ParentFName") = .FirstName
                            indID = objIndividual.AddIndSpouse(Application("ConnectionString"))
                            bIsIndividual = True
                            'Ferdine Silva Find to make serialization problem
                            'Session("PersonalProfile") = objIndividual
                        End With
                    End If
                Next
            End If
            strSql = "SELECT * FROM INDDUPLICATE WHERE DonorType IN('SPOUSE') "
            strSql = strSql & " AND Status<>'Completed' AND INDDUPID=" & CDbl(Request.QueryString("DupID")) + 1 & " ORDER BY DonorType "
            Dim dsSpouse As New DataSet
            dsSpouse = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            Dim j As Integer
            For j = 0 To dsSpouse.Tables(0).Rows.Count - 1
                If indID > 0 Then
                    objSpouse = New IndSpouse10
                    With objSpouse
                        .PrimaryIndSpouseID = indID
                        'set spouseid for individual.

                        .Relationship = indID
                        .Title = dsSpouse.Tables(0).Rows(j)("Title").ToString()
                        .FirstName = dsSpouse.Tables(0).Rows(j)("FirstName").ToString()
                        strSpouseName = StrConv(dsSpouse.Tables(0).Rows(j)("FirstName").ToString(), VbStrConv.ProperCase)
                        .LastName = dsSpouse.Tables(0).Rows(j)("LastName").ToString()
                        .DonorType = "SPOUSE"
                        .CreateDate = Now
                        .MemberSince = Now
                        .Address1 = dsSpouse.Tables(0).Rows(j)("Address1").ToString()
                        .Address2 = dsSpouse.Tables(0).Rows(j)("Address2").ToString()
                        .City = dsSpouse.Tables(0).Rows(j)("City").ToString()
                        .State = dsSpouse.Tables(0).Rows(j)("State").ToString()
                        .Zip = dsSpouse.Tables(0).Rows(j)("Zip").ToString()
                        If Len(dsSpouse.Tables(0).Rows(j)("Email").ToString()) = 0 Then
                            .Email = ""
                        Else
                            .Email = dsSpouse.Tables(0).Rows(j)("Email").ToString()
                        End If
                        If Len(strEmail) > 0 Then
                            strEmail = strEmail & ",'" & dsSpouse.Tables(0).Rows(j)("Email").ToString() & "'"
                        Else
                            strEmail = "'" & dsSpouse.Tables(0).Rows(j)("Email").ToString() & "'"
                        End If
                        strEmail2 = dsSpouse.Tables(0).Rows(j)("Email").ToString()
                        .HPhone = dsSpouse.Tables(0).Rows(j)("HPhone").ToString()
                        .CPhone = dsSpouse.Tables(0).Rows(j)("CPhone").ToString()
                        .Country = dsSpouse.Tables(0).Rows(j)("Country").ToString()
                        .Gender = dsSpouse.Tables(0).Rows(j)("Gender").ToString()
                        .WPhone = dsSpouse.Tables(0).Rows(j)("WPhone").ToString()
                        .WFax = dsSpouse.Tables(0).Rows(j)("WFax").ToString()
                        .SecondaryEmail = dsSpouse.Tables(0).Rows(j)("SecondaryEmail").ToString()
                        .Education = dsSpouse.Tables(0).Rows(j)("Education").ToString()
                        .Career = dsSpouse.Tables(0).Rows(j)("Career").ToString()
                        .Employer = dsSpouse.Tables(0).Rows(j)("Employer").ToString()
                        .CountryOfOrigin = dsSpouse.Tables(0).Rows(j)("CountryOfOrigin").ToString()
                        .StateOfOrigin = dsSpouse.Tables(0).Rows(j)("StateOfOrigin").ToString()
                        .VolunteerFlag = dsSpouse.Tables(0).Rows(j)("VolunteerFlag").ToString()
                        .ReferredBy = dsSpouse.Tables(0).Rows(j)("ReferredBy").ToString()
                        .ChapterID = iChapterID
                        .Chapter = strChapter
                        .MaritalStatus = dsSpouse.Tables(0).Rows(j)("MaritalStatus").ToString()
                        .Liaison = dsSpouse.Tables(0).Rows(j)("Liaison").ToString()
                        .Sponsor = dsSpouse.Tables(0).Rows(j)("Sponsor").ToString()
                        .VolunteerRole1 = Nothing
                        .VolunteerRole2 = Nothing
                        .VolunteerRole3 = Nothing
                        If .CreateDate = Nothing Then
                            .CreateDate = Now
                        End If
                        If .MemberSince = Nothing Then
                            .MemberSince = Now
                        End If
                        Dim sqlDateTime As SqlTypes.SqlDateTime
                        sqlDateTime = SqlTypes.SqlDateTime.Null

                        .ModifyDate = sqlDateTime
                        .CreatedBy = Session("LoginID")
                        .ModifiedBy = Nothing
                        .ModifyDate = Now
                        .PrimaryContact = "No"
                        Session("ParentLName") = .LastName
                        Session("ParentFName") = .FirstName
                        spouseID = objSpouse.AddIndSpouse(Application("ConnectionString"))
                        bIsSpouse = True
                    End With
                End If
            Next
            Dim strName As String
            If Len(strSpouseName) > 0 Then
                strName = strIndName & " and " & strSpouseName & ","
            Else
                strName = strIndName & ","
            End If
            strSql = "SELECT * FROM LOGIN_MASTER WHERE Upper(User_Email) IN(" & strEmail & ")"
            Dim drLoginEmail1 As SqlDataReader

            drLoginEmail1 = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, strSql)
            sBody = "Dear " & strName & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "We are sorry you had problems with our online registration.  We saved the data you " & vbCrLf
            sBody = sBody & "entered and created a new profile based on it.  By going to the home page at " & vbCrLf
            sBody = sBody & "www.northsouth.org and logging back in, you can retrieve your record and " & vbCrLf
            sBody = sBody & "update as appropriate.  The following are your log in ID and password:" & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & vbCrLf
            While drLoginEmail1.Read()
                sBody = sBody & "Login Email: " & drLoginEmail1("User_Email") & vbCrLf
                sBody = sBody & "Password: " & drLoginEmail1("User_Pwd") & vbCrLf
                sBody = sBody & vbCrLf
            End While
            If Len(strEmail1) > 0 And Len(strEmail2) > 0 Then
                sBody = sBody & "You can use either one." & vbCrLf
            End If
            sBody = sBody & "Please keep these in a safe place." & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "Thank you" & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "With regards," & vbCrLf
            sBody = sBody & vbCrLf
            sBody = sBody & "NSF Customer Service Team" & vbCrLf
            If Len(strEmail1) > 0 Then
                SendEmail("Registration Problem / NSF", sBody, strEmail1)
            End If
            If Len(strEmail2) > 0 Then
                SendEmail("Registration Problem / NSF", sBody, strEmail2)
            End If
            If spouseID > 0 Then
                'objIndividual = New IndSpouse10
                'objIndividual = Session("PersonalProfile")
                'objIndividual.PrimaryIndSpouseID = spouseID
                'objIndividual.UpdateIndSpouse(Application("ConnectionString"))
                strSql = "UPDATE INDSPOUSE SET PrimaryIndSpouseID=" & spouseID
                strSql = strSql & " WHERE AutomemberID=" & indID
                strSql = strSql & " AND DonorType='IND'"
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            End If
            'update Status of Indduplicate
            strSql = "UPDATE INDDUPLICATE SET Status='Completed',Remarks='Added records to IndSpouse Table',"
            strSql = strSql & "AddMemberID= " & indID & " WHERE AutoMemberID=" & Request.QueryString("ID")
            strSql = strSql & " AND DonorType IN ('IND') AND Status<>'Completed'"

            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

            strSql = "UPDATE INDDUPLICATE SET Status='Completed',Remarks='Added records to IndSpouse Table',"
            strSql = strSql & "AddMemberID= " & spouseID & " WHERE AutoMemberID=" & Request.QueryString("ID")
            strSql = strSql & " AND DonorType IN ('SPOUSE') AND Status<>'Completed'"
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)

        End If


        lblMessage.Text = "IndSpouse Record Creation Completed Successfully."
    End Sub
    Private Sub SendEmail(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@gmail.com")
        email.To.Add(sMailTo)
        email.Subject = sSubject
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody

        Dim client As New SmtpClient()
        Dim ok As Boolean = True

        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")

        'client.Host = host
        'client.Credentials = New NetworkCredential("nagakumar@objectinfo.com", "secureone")
        Try
            client.Send(email)
        Catch e As Exception
            ok = False
        End Try
    End Sub
End Class