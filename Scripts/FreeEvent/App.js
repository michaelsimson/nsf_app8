﻿var myApp = angular
    .module("myModule", ['ngRoute'])
    .config(function ($locationProvider) {
    $locationProvider.html5Mode(true);
});