﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class PersonalPref
    Inherits System.Web.UI.Page
    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=p")
        End If
        If Len(Trim("" & Session("LoginID"))) = 0 Then
            Server.Transfer("login.aspx?entry=P")
        End If
        If Len(Trim("" & Session("entryToken"))) = 0 Then
            Server.Transfer("login.aspx?entry=P")
        End If
        If Not IsPostBack Then
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                loadvalues(Session("CustIndId"))
            ElseIf Request.QueryString("id") = 1 Then
                loadvalues(Session("CustIndId"))
            ElseIf Request.QueryString("id") = 2 Then
                getcustindid()
                hlinkParentRegistration.Text = "Back to Volunteer Functions Page"
                hlinkParentRegistration.NavigateUrl = "Volunteerfunctions.aspx"
            ElseIf Request.QueryString("id") = 3 Then
                getcustindid()
                hlinkParentRegistration.Text = "Back to Donor Functions Page"
                hlinkParentRegistration.NavigateUrl = "DonorFunctions.aspx"
            ElseIf Request.QueryString("id") = 4 Then
                getcustindid()
                hlinkParentRegistration.Text = "Back to Walk-a-thon/Marathon Functions Page"
                hlinkParentRegistration.NavigateUrl = "WalkathonMarathonFunctions.aspx"
            ElseIf Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                tblPersPref.Visible = False
                pIndSearch.Visible = True
                hlinkParentRegistration.Text = "Back to Volunteer Functions Page"
                hlinkParentRegistration.NavigateUrl = "Volunteerfunctions.aspx"
            End If
        End If
    End Sub
    Private Sub getcustindid()
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType from Indspouse where automemberid=" & Session("loginID") & "")
        While reader.Read()
            If reader("DonorType").ToUpper() = "IND" Then
                lblCustIndID.Text = reader("Automemberid")
            ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                lblCustIndID.Text = reader("Relationship")
            End If
        End While
        reader.Close()
        loadvalues(lblCustIndID.Text)
    End Sub
    Private Sub loadvalues(ByVal CustindId As Integer)
        lblCustIndID.Text = CustindId
        Dim Readr As SqlDataReader
        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "select * from Chapter where status ='A' order by state,chaptercode")
        ddlChapterInd.DataSource = ds
        ddlChapterInd.DataBind()
        ddlChapterSp.DataSource = ds
        ddlChapterSp.DataBind()
        Readr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Sponsor,Liaison,FirstName + ' ' + LastName as name,ChapterID,NewsLetter,PubName from IndSpouse where AutoMemberID =" & CustindId & "")
        While Readr.Read()
            litInd.Text = Readr("name")
            LitInd1.Text = Readr("name")
            LitInd2.Text = Readr("name")
            litInd3.Text = Readr("name")
            If Not Readr("Sponsor").Equals(DBNull.Value) Then
                rbSponsorInd.SelectedValue = Readr("Sponsor")
            End If
            If Not Readr("PubName").Equals(DBNull.Value) Then
                txtPubname.Text = Readr("PubName")
            End If
            If Not Readr("Liaison").Equals(DBNull.Value) Then
                rbLiaisonInd.SelectedValue = Readr("Liaison")
            End If
            If Not Readr("Newsletter").Equals(DBNull.Value) Then
                ddlECommInd.SelectedIndex = ddlECommInd.Items.IndexOf(ddlECommInd.Items.FindByValue(Readr("Newsletter")))
            End If
            lblCustIndChapter.Text = Readr("ChapterID")
            ddlChapterInd.SelectedIndex = ddlChapterInd.Items.IndexOf(ddlChapterInd.Items.FindByValue(Readr("ChapterID")))
        End While
        Readr.Close()
        HlblSpouse.Text = "N"
        Readr = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Sponsor,Liaison,FirstName + ' ' + LastName as name,ChapterID,NewsLetter from IndSpouse where Relationship =" & CustindId & "")
        While Readr.Read()
            litIndSp.Text = Readr("name")
            LitSp1.Text = Readr("name")
            litSp2.Text = Readr("name")
            litSp3.Text = Readr("name")
            If Not Readr("Sponsor").Equals(DBNull.Value) Then
                rbSponsorIndSp.SelectedValue = Readr("Sponsor")
            End If
            If Not Readr("Liaison").Equals(DBNull.Value) Then
                rbLiaisonIndSp.SelectedValue = Readr("Liaison")
            End If
            If Not Readr("Newsletter").Equals(DBNull.Value) Then
                ddlECommSp.SelectedIndex = ddlECommSp.Items.IndexOf(ddlECommSp.Items.FindByValue(Readr("Newsletter")))
            End If
            HlblSpouse.Text = "Y"
            ddlChapterSp.SelectedIndex = ddlChapterSp.Items.IndexOf(ddlChapterSp.Items.FindByValue(Readr("ChapterID")))
        End While
        If HlblSpouse.Text = "Y" Then
            LitSp1.Visible = True
            ddlChapterSp.Visible = True
            ' BtnSpChapter.Visible = True
            lblSp1.Visible = True
            litSp2.Visible = True
            litSp3.Visible = True
            'btnECommSp.Visible = True
            ddlECommSp.Visible = True
            rbSponsorIndSp.Visible = True
            rbLiaisonIndSp.Visible = True
            litIndSp.Visible = True
        Else
            LitSp1.Visible = False
            ddlChapterSp.Visible = False
            'BtnSpChapter.Visible = False
            lblSp1.Visible = False
            litSp2.Visible = False
            litSp3.Visible = False
            'btnECommSp.Visible = False
            ddlECommSp.Visible = False
            rbSponsorIndSp.Visible = False
            rbLiaisonIndSp.Visible = False
            litIndSp.Visible = False
        End If
    End Sub
    Protected Sub lknChangeChapter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        divEComm.Visible = False
        DivChChapter.Visible = True
        divSponsor.Visible = False
        divvolunteer.Visible = False
        divPubName.Visible = False
    End Sub
    Protected Sub lbtnpubname_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        divEComm.Visible = False
        DivChChapter.Visible = False
        divSponsor.Visible = False
        divvolunteer.Visible = False
        divPubName.Visible = True
    End Sub

    Protected Sub BtnChChapter_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        If Not lblCustIndChapter.Text = ddlChapterInd.SelectedValue Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "Update IndSpouse set ChapterID=" & ddlChapterInd.SelectedValue & ",Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & " WHERE AutomemberID=" & lblCustIndID.Text & "")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "Insert into PersPref(MemberID, NewChapterID, OldChapterID, TypeofChange, CreateDate, CreatedBy) Values(" & lblCustIndID.Text & "," & ddlChapterInd.SelectedValue & "," & lblCustIndChapter.Text & ",'Chapter',GetDate()," & Session("LoginID") & ")")
        End If
        If HlblSpouse.Text = "Y" Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "Update IndSpouse set ChapterID=" & ddlChapterSp.SelectedValue & ",Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE RelationShip=" & lblCustIndID.Text & "")
        End If
        lblCustIndChapter.Text = ddlChapterInd.SelectedValue
        Session("CustIndChapterID") = lblCustIndChapter.Text
        Session("ChapterID") = Session("CustIndChapterID")
        DivChChapter.Visible = False
        lblErr.Text = "Chapter changed Successfully"
    End Sub

    Protected Sub lbtnEComm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        DivChChapter.Visible = False
        divSponsor.Visible = False
        divEComm.Visible = True
        divvolunteer.Visible = False
        divPubName.Visible = False
    End Sub

    Protected Sub btnEcomm_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        divEComm.Visible = False
    End Sub

    Protected Sub lbtnSponsor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        DivChChapter.Visible = False
        divEComm.Visible = False
        divvolunteer.Visible = False
        divSponsor.Visible = True
        divPubName.Visible = False
    End Sub

    Protected Sub btnAddSponsor_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        If rbSponsorInd.SelectedValue = "" Then
            lblErr.Text = "Please select Yes or No"
            Exit Sub
        End If
        If HlblSpouse.Text = "Y" Then
            If Not rbSponsorIndSp.SelectedValue = "" Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Sponsor='" & rbSponsorIndSp.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE relationship=" & lblCustIndID.Text & "")
            Else
                lblErr.Text = "Please select Yes or No"
                Exit Sub
            End If
        End If
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Sponsor='" & rbSponsorInd.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Automemberid=" & lblCustIndID.Text & "")
        divSponsor.Visible = False
    End Sub

    Protected Sub btnECommInd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnECommInd.Click
        If ddlECommInd.SelectedValue = 9 Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter=Null,Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Automemberid=" & lblCustIndID.Text & "")
        Else
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter='" & ddlECommInd.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Automemberid=" & lblCustIndID.Text & "")
        End If
        If HlblSpouse.Text = "Y" And ddlECommSp.SelectedValue = 9 Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter=Null,Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Relationship=" & lblCustIndID.Text & "")
        ElseIf HlblSpouse.Text = "Y" Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Newsletter='" & ddlECommSp.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Relationship=" & lblCustIndID.Text & "")
        End If
        divEComm.Visible = False
    End Sub

    Protected Sub lbtnVolunteer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        DivChChapter.Visible = False
        divEComm.Visible = False
        divSponsor.Visible = False
        divvolunteer.Visible = True
    End Sub

    Protected Sub btnAddVolunteer_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        If rbLiaisonInd.SelectedValue = "" Then
            lblErr.Text = "Please select Yes or No"
            Exit Sub
        End If
        If HlblSpouse.Text = "Y" Then
            If Not rbLiaisonIndSp.SelectedValue = "" Then
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Liaison='" & rbLiaisonIndSp.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE relationship=" & lblCustIndID.Text & "")
            Else
                lblErr.Text = "Please select Yes or No"
                Exit Sub
            End If
        End If
        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set Liaison='" & rbLiaisonInd.SelectedValue & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE Automemberid=" & lblCustIndID.Text & "")
        divSponsor.Visible = False
        divvolunteer.Visible = False
    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        lblCustIndID.Text = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append("  Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No member match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim reader As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select Automemberid,Relationship,DonorType from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & "")
        While reader.Read()
            If reader("DonorType").ToUpper() = "IND" Then
                lblCustIndID.Text = reader("Automemberid")
            ElseIf reader("DonorType").ToUpper() = "SPOUSE" Then
                lblCustIndID.Text = reader("Relationship")
            End If
        End While
        reader.Close()
        If Not lblCustIndID.Text = String.Empty Then
            rbLiaisonInd.SelectedIndex = False
            rbLiaisonIndSp.SelectedIndex = False
            rbSponsorInd.SelectedIndex = False
            rbSponsorIndSp.SelectedIndex = False
            loadvalues(lblCustIndID.Text)
            tblPersPref.Visible = True
            Panel4.Visible = False
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "No data found"
        End If

    End Sub

    Protected Sub btnAddPubName_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        lblErr.Text = ""
        If Not txtPubname.Text = "" Then
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE IndSpouse set PubName='" & txtPubname.Text & "',Modifydate=Getdate(),ModifiedBy=" & Session("loginID") & "  WHERE AutomemberId=" & lblCustIndID.Text & "")
        Else
            lblErr.Text = "Please Enter Publishing Name"
            Exit Sub
        End If
        divPubName.Visible = False
    End Sub

    Protected Sub lbtnUniqueEmailID_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnUniqueEmailID.Click
        Session("UserId") = SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "SELECT Email from IndSpouse Where AutomemberID=" & lblCustIndID.Text)
        Response.Redirect("MakeEmailIdsUnique.aspx")
    End Sub

    Protected Sub lbtnChangeEmail_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles lbtnChangeEmail.Click
        If Request.QueryString("CustServ") = 1 Then
            Response.Redirect("ChangeEmail.aspx?CustServ=1")
        Else
            Response.Redirect("ChangeEmail.aspx")
        End If
    End Sub
End Class
