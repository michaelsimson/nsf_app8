'**********************************************************************
'                         REVISION HISTORY                            '
'**********************************************************************
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
'   Name      : /VRegistration/EventsList.aspx  
'   Author    : Venkat Ambati
'   Contact   : tejasreeja@gmail.com
'
'	Desc:	This page allows the Chapter Coordinator to View the List 
'           Events and Provides alink to Edit a Perticular event. 
'
'           Will utilize the following stored procedures
'           1) NSF\usp_GetEventsList
'
'   Init	Date                       Description                    
'   ----	----------	----------------------------------------------- 
'   VA		08/13/2005	Initial Development
'''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''
Imports System.Data.SqlClient
Imports NorthSouth.BAL


Namespace VRegistration

Partial Class EventsList
    Inherits System.Web.UI.Page

#Region " Web Form Designer Generated Code "

    'This call is required by the Web Form Designer.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()

    End Sub
    Dim ddlTime As DropDownList


    Private Sub Page_Init(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Init
        'CODEGEN: This method call is required by the Web Form Designer
        'Do not modify it using the code editor.
        InitializeComponent()
    End Sub

#End Region

    Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
        'Put user code to initialize the page here
        If Session("LoggedIn") <> "LoggedIn" Then
            Server.Transfer("login.aspx")
        End If
        Dim NationalFinalsCity As String = Application("NationalFinalsCity")
        If Not Page.IsPostBack Then
            ddlEventYear.Items.FindByValue(Application("ContestYear")).Selected = True
            GetContestByYear(Application("ContestYear"))
            If Application("ContestType") = 1 Then
                Dim redirectURL As String
                If Request.ServerVariables("Server_Name") <> "localhost" Then
                    redirectURL = "/app6/UserFunctions.aspx"
                Else
                    redirectURL = "/VRegistration/UserFunctions.aspx"
                End If
                hlinkChapterFunctions.NavigateUrl = redirectURL
            End If
        End If
    End Sub

    Private Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEventYear.SelectedIndexChanged
        GetContestByYear(ddlEventYear.SelectedValue)
    End Sub
    Private Sub GetContestByYear(ByVal ContestYear As Integer)
            Dim contest As New NorthSouth.BAL.Contest
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim cmd As New SqlCommand
        cmd.Connection = conn
        conn.Open()
        cmd.CommandType = CommandType.StoredProcedure
            Select Case Application("ContestType").ToString
                Case "1"
                    cmd.CommandText = "usp_GetEventsList"
                    cmd.Parameters.AddWithValue("@ContestYear", DbType.String).Value = ddlEventYear.SelectedValue
                Case "2"
                    If Not Session("ChapterID") Is Nothing And Session("RoleGeneral") = True Then
                        cmd.CommandText = "usp_GetChapterEventsList"
                        cmd.Parameters.AddWithValue("@ContestYear", DbType.String).Value = ContestYear
                        cmd.Parameters.AddWithValue("@ChapterID", DbType.Int16).Value = Session("ChapterID")
                    Else
                        '*** Chapter ID Info Missing Exception 
                    End If
            End Select


        Dim da As New SqlDataAdapter
        Dim ds As New DataSet
        da.SelectCommand = cmd
        da.Fill(ds)
        If ds.Tables.Count > 0 Then

            If ViewState("OrderBy") Is Nothing Then
                ViewState("OrderBy") = "ContestID"
            End If
            ds.Tables(0).DefaultView.Sort = ViewState("OrderBy")
            dgContestList.DataSource = ds.Tables(0)
            dgContestList.DataBind()
        End If

    End Sub
    Private Sub dgContestList_ItemDataBound(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) Handles dgContestList.ItemDataBound
        Select Case e.Item.ItemType
            Case ListItemType.Item, ListItemType.AlternatingItem
                If Application("ContestType") = 1 Then
                    CType(e.Item.FindControl("lblContestCity"), Label).Text = Application("NationalFinalsCity")
                Else
                    CType(e.Item.FindControl("lblContestCity"), Label).Text = e.Item.DataItem("ChapterName")
                End If
                CType(e.Item.FindControl("hlEditEvent"), HyperLink).NavigateUrl = "EditContest.aspx?ContestID=" & e.Item.DataItem("ContestID").ToString
                CType(e.Item.FindControl("hlEditEvent"), HyperLink).Text = e.Item.DataItem("ContestDescription").ToString

                If Not IsDBNull(e.Item.DataItem("Building")) And (Not IsDBNull(e.Item.DataItem("Room"))) Then
                    CType(e.Item.FindControl("lblBuilding"), Label).Text = e.Item.DataItem("Building") & " / " & e.Item.DataItem("Room")
                End If

                If Not (IsDBNull(e.Item.DataItem("StartTime")) And IsDBNull(e.Item.DataItem("EndTime"))) Then
                    CType(e.Item.FindControl("lblEventTime"), Label).Text = GetDisplayTime(e.Item.DataItem("StartTime")) & " - " & GetDisplayTime(e.Item.DataItem("EndTime"))
                    'e.Item.BackColor = Color.Cyan
                End If

                If Not IsDBNull(e.Item.DataItem("CheckinTime")) And Not (e.Item.DataItem("CheckinTime")) Is String.Empty Then
                    CType(e.Item.FindControl("lblCheckInTime"), Label).Text = GetDisplayTime(e.Item.DataItem("CheckinTime"))
                End If
                If Not IsDBNull(e.Item.DataItem("ContestDate")) And IsDate(e.Item.DataItem("ContestDate")) Then
                    If Convert.ToDateTime(e.Item.DataItem("ContestDate")) = Convert.ToDateTime("1900/01/01") Then
                        CType(e.Item.FindControl("lblContestDate"), Label).Text = "TBD"
                    Else
                        CType(e.Item.FindControl("lblContestDate"), Label).Text = Convert.ToDateTime(e.Item.DataItem("ContestDate")).ToShortDateString
                    End If
                Else
                    CType(e.Item.FindControl("lblContestDate"), Label).Text = "TBD"

                End If
                If Not IsDBNull(e.Item.DataItem("RegistrationDeadline")) And IsDate(e.Item.DataItem("RegistrationDeadline")) Then
                    If Convert.ToDateTime(e.Item.DataItem("RegistrationDeadline")) = Convert.ToDateTime("1900/01/01") Then
                        CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text = "TBD"
                    Else
                        CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text = Convert.ToDateTime(e.Item.DataItem("RegistrationDeadline")).ToShortDateString
                    End If
                Else
                    CType(e.Item.FindControl("lblRegistrationDeadline"), Label).Text = "TBD"

                End If
        End Select
    End Sub

    Private Sub dgContestList_SortCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridSortCommandEventArgs) Handles dgContestList.SortCommand
        If e.SortExpression = ViewState("OrderBy") Then
            ViewState("OrderBy") = e.SortExpression & " DESC"
        Else
            ViewState("OrderBy") = e.SortExpression
        End If
    End Sub
        Private Function GetDisplayTime(ByVal strTime As String) As String
            Dim retValue As String = ""
            If strTime.ToString <> "" Then
                ddlTime = New DropDownList
                ddlTime.Items.Add(New ListItem("", ""))
                ddlTime.Items.Add(New ListItem("7:00 AM", "7:00"))
                ddlTime.Items.Add(New ListItem("7:30 AM", "7:30"))
                ddlTime.Items.Add(New ListItem("8:00 AM", "8:00"))
                ddlTime.Items.Add(New ListItem("8:30 AM", "8:30"))
                ddlTime.Items.Add(New ListItem("9:00 AM", "9:00"))
                ddlTime.Items.Add(New ListItem("9:30 AM", "9:30"))
                ddlTime.Items.Add(New ListItem("10:00 AM", "10:00"))
                ddlTime.Items.Add(New ListItem("10:30 AM", "10:30"))
                ddlTime.Items.Add(New ListItem("11:00 AM", "11:00"))
                ddlTime.Items.Add(New ListItem("11:30 AM", "11:30"))
                ddlTime.Items.Add(New ListItem("12:00 Noon", "12:00"))
                ddlTime.Items.Add(New ListItem("12:30 PM", "12:30"))
                ddlTime.Items.Add(New ListItem("1:00 PM", "13:00"))
                ddlTime.Items.Add(New ListItem("1:30 PM", "13:30"))
                ddlTime.Items.Add(New ListItem("2:00 PM", "14:00"))
                ddlTime.Items.Add(New ListItem("2:30 PM", "14:30"))
                ddlTime.Items.Add(New ListItem("3:00 PM", "15:00"))
                ddlTime.Items.Add(New ListItem("3:30 PM", "15:30"))
                ddlTime.Items.Add(New ListItem("4:00 PM", "16:00"))
                ddlTime.Items.Add(New ListItem("4:30 PM", "16:30"))
                ddlTime.Items.Add(New ListItem("5:00 PM", "17:00"))
                ddlTime.Items.Add(New ListItem("5:30 PM", "17:30"))
                ddlTime.Items.Add(New ListItem("6:00 PM", "18:00"))
                ddlTime.Items.Add(New ListItem("6:30 PM", "18:30"))
                ddlTime.Items.Add(New ListItem("7:00 PM", "19:00"))
                ddlTime.Items.Add(New ListItem("7:30 PM", "19:30"))
                ddlTime.Items.Add(New ListItem("8:00 PM", "20:00"))
                ddlTime.Items.Add(New ListItem("8:30 PM", "20:30"))
                ddlTime.Items.Add(New ListItem("9:00 PM", "21:00"))
                ddlTime.Items.Add(New ListItem("9:30 PM", "21:30"))
                ddlTime.Items.Add(New ListItem("10:00 PM", "22:00"))
                ddlTime.Items.Add(New ListItem("10:30 PM", "22:30"))
                ddlTime.Items.Add(New ListItem("11:00 PM", "23:00"))
                ddlTime.Items.Add(New ListItem("11:30 PM", "23:30"))
                Try
                    ddlTime.Items.FindByValue(strTime.Trim).Selected = True
                    retValue = ddlTime.SelectedItem.Text
                Catch
                    retValue = ""
                End Try
            End If
            Return retValue
        End Function
End Class

End Namespace

