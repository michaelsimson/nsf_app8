Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Text
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions

Partial Class Email
    Inherits System.Web.UI.Page
    Dim cnTemp As SqlConnection
    Dim intCtr As Integer
    Dim strInsertToValues As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        ''Debug()
        'Session("LoggedIn") = "True"
        'Session("LoginRole") = "ChapterC"
        'Session("RoleId") = 5
        'Session("LoginChapterID") = 48

        cnTemp = New SqlConnection(Application("ConnectionString"))
        'lblDebug.Text = Session("RoleId") & ", Test " & Session("LoginChapterID") & ", " & Session("LoginClusterID") & ", " & Session("LoginZoneID") & ", "
        'lblDebug.Visible = True

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            Exit Sub
        End If

        If Not Page.IsPostBack Then
            LoadlstAddress()
        End If

    End Sub

    Private Sub LoadlstAddress()
        Dim strSQL As String, dsData As New DataSet
        strSQL = ""
        'sandhya - temporary code . need to find a better fix for this issue
        If LCase(Mid(Trim(Session("LoginChapterID")), 1, 1)) = "s" Then
            Session("LoginChapterID") = "0"
        End If

        Try
            Select Case Session("RoleId")
                Case 5  'ChapterCoordinator
                    Select Case UCase(ddlTo.SelectedItem.Value)
                        Case "PP"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                     " Where ChapterID = " & Session("LoginChapterID") & ") or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID = " & Session("LoginChapterID") & ") and PrimaryContact = 'Y' ))" & _
                                     " Order By AutoMemberID "
                        Case "PB"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                     " Where ChapterID = " & Session("LoginChapterID") & ") or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID = " & Session("LoginChapterID") & ")))" & _
                                     " Order By AutoMemberID "
                        Case "VR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email<>'' " & _
                                     " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                     " Where ChapterID = " & Session("LoginChapterID") & "))" & _
                                     " Order By AutoMemberID "
                        Case "VUR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & Session("LoginChapterID") & " ) and email<>'' " & _
                                     " AND (VolunteerRole1 <> 0 OR VolunteerRole2 <> 0 OR VolunteerRole3 <> 0) " & _
                                     " Order By AutoMemberID "

                        Case "PAID"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email <> '' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is not null and " & _
                                     " ChapterID in (" & Session("LoginChapterID") & ") " & _
                                     " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is not null and " & _
                                     " ChapterID in (" & Session("LoginChapterID") & " ) " & _
                                     "))"

                        Case "PENDING"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email <> '' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is null and " & _
                                     " ChapterID in (" & Session("LoginChapterID") & ") " & _
                                     " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0 and  " & _
                                     " ChapterID in (" & Session("LoginChapterID") & " ) " & _
                                     "))"

                    End Select
                Case 4 'ClusterC
                    Dim strChapterSQL As String
                    strChapterSQL = " ( Select ChapterID from Chapter Where ClusterID = " & Session("LoginClusterID")
                    If Session("LoginZoneID") <> "" Then
                        strChapterSQL = strChapterSQL & " and ZoneID = " & Session("LoginZoneID")
                    End If
                    If Session("LoginChapterID") <> "" Then
                        strChapterSQL = strChapterSQL & " and ChapterID = " & Session("LoginChapterID")
                    End If
                    strChapterSQL = strChapterSQL & " ) "

                    Select Case UCase(ddlTo.SelectedItem.Value)
                        Case "PP"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                    " From IndSpouse where ChapterID IN " & strChapterSQL & _
                                    " and email <> '' " & _
                                    " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                    " Where ChapterID IN " & strChapterSQL & _
                                    " ) or (Relationship in (Select Distinct ParentID " & _
                                    " From Contestant Where ChapterID IN " & strChapterSQL & _
                                    " ) and PrimaryContact = 'Y' ))" & _
                                    " Order By AutoMemberID "
                        Case "PB"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN " & strChapterSQL & _
                                     " and email <> '' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                     " Where ChapterID IN " & strChapterSQL & _
                                     " ) or (Relationship in (Select Distinct ParentID " & _
                                     " From Contestant Where ChapterID IN " & strChapterSQL & _
                                     " )))" & _
                                     " Order By AutoMemberID "
                        Case "VR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email <> '' " & _
                                     " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                     " Where ChapterID IN " & strChapterSQL & _
                                     ")) Order By AutoMemberID "
                        Case "VUR"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where ChapterID IN (" & strChapterSQL & _
                                     " ) and email <> '' " & _
                                     " AND (VolunteerRole1 <> 0 OR VolunteerRole2 <> 0 OR VolunteerRole3 <> 0) " & _
                                     " Order By AutoMemberID "
                        Case "PAID"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email <> '' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is not null and " & _
                                     " ChapterID IN " & strChapterSQL & _
                                     " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is not null and " & _
                                     " ChapterID IN " & strChapterSQL & "))"

                        Case "PENDING"
                            strSQL = " SELECT  AutoMemberID, Email " & _
                                     " From IndSpouse where email <> '' " & _
                                     " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0 and " & _
                                     " ChapterID IN " & strChapterSQL & _
                                     " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                     " ContestYear = '" & Application("ContestYear") & "' " & _
                                     " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0 and " & _
                                     " ChapterID IN " & strChapterSQL & "))"

                    End Select

                Case 3, 2, 1 'ZonalC , NationalC, Admin
                    Dim strChapterSQL As String, strWhere As String
                    strWhere = ""
                    strChapterSQL = " ( Select ChapterID from Chapter "
                    'Where ClusterID = " & Session("LoginClusterID")
                    If Session("LoginClusterID") <> "" Then
                        If Session("LoginClusterID") <> 0 Then
                            strWhere = strWhere & " ClusterID = " & Session("LoginClusterID")
                        End If
                    End If
                    If Session("LoginZoneID") <> "" Then
                        If Session("LoginZoneID") <> 0 Then
                            If strWhere = "" Then
                                strWhere = " ZoneID = " & Session("LoginZoneID")
                            Else
                                strWhere = strWhere & " and ZoneID = " & Session("LoginZoneID")
                            End If
                        End If
                    End If
                    If Session("LoginChapterID") <> "" Then
                        If Session("LoginChapterID") <> 0 Then
                            If strWhere = "" Then
                                strWhere = " ChapterID = " & Session("LoginChapterID")
                            Else
                                strWhere = strWhere & " and ChapterID = " & Session("LoginChapterID")
                            End If
                        End If
                    End If
                    If strWhere = "" Then
                        strChapterSQL = strChapterSQL & " ) "
                    Else
                        strChapterSQL = strChapterSQL & " Where " & strWhere & " ) "
                    End If


                    If Session("LoginChapterID") = "" And Session("LoginZoneID") = "" And Session("LoginClusterID") = "" Then
                        'the above condition shows that logged in as an admin
                        Select Case UCase(ddlTo.SelectedItem.Value)
                            Case "PP"
                                strSQL = " SELECT  Top 100 AutoMemberID, Email " & _
                                         " From IndSpouse where email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant ) " & _
                                         " OR (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant ) and PrimaryContact = 'Y' ))" & _
                                         " Order By AutoMemberID "
                            Case "PB"
                                strSQL = " SELECT  Top 100 AutoMemberID, Email " & _
                                         " From IndSpouse where email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant )))" & _
                                         " Order By AutoMemberID "
                            Case "VR"
                                strSQL = " SELECT  TOP 100 AutoMemberID, Email " & _
                                         " From IndSpouse where email<>'' " & _
                                         " and (automemberid in (Select Distinct MemberID From Volunteer )) " & _
                                         " Order By AutoMemberID "
                            Case "VUR"
                                strSQL = " SELECT  Top 100 AutoMemberID, Email " & _
                                         " From IndSpouse where Email <> '' " & _
                                         " AND (VolunteerRole1 <> 0 OR VolunteerRole2 <> 0 OR VolunteerRole3 <> 0) " & _
                                         " Order By AutoMemberID "
                            Case "PAID"
                                strSQL = " SELECT  Top 100 AutoMemberID, Email " & _
                                         " From IndSpouse where Email <> '' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is not null " & _
                                         " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is not null))"
                            Case "PENDING"
                                strSQL = " SELECT  Top 100 AutoMemberID, Email " & _
                                         " From IndSpouse where Email <> '' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0))"
                        End Select

                        lblDebug.Text = "Only top 100 people are there. This is due to large number of records."
                    Else

                        Select Case UCase(ddlTo.SelectedItem.Value)
                            Case "PP"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                        " From IndSpouse where ChapterID IN " & strChapterSQL & _
                                        " and email<>'' " & _
                                        " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                        " Where ChapterID IN " & strChapterSQL & _
                                        " ) or (Relationship in (Select Distinct ParentID " & _
                                        " From Contestant Where ChapterID IN " & strChapterSQL & _
                                        " ) and PrimaryContact = 'Y' ))" & _
                                        " Order By AutoMemberID "
                            Case "PB"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN " & strChapterSQL & _
                                         " and email<>'' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant " & _
                                         " Where ChapterID IN " & strChapterSQL & _
                                         " ) or (Relationship in (Select Distinct ParentID " & _
                                         " From Contestant Where ChapterID IN " & strChapterSQL & _
                                         " )))" & _
                                         " Order By AutoMemberID "
                            Case "VR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where email<>'' " & _
                                         " and (automemberid in (Select Distinct MemberID From Volunteer  " & _
                                         " Where ChapterID IN " & strChapterSQL & _
                                         " )) Order By AutoMemberID "
                            Case "VUR"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where ChapterID IN (" & strChapterSQL & _
                                         " ) and email<>'' " & _
                                         " AND (VolunteerRole1 <> 0 OR VolunteerRole2 <> 0 OR VolunteerRole3 <> 0) " & _
                                         " Order By AutoMemberID "
                            Case "PAID"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where  Email <> '' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is not null and " & _
                                         " ChapterID IN " & strChapterSQL & _
                                         " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is not null and " & _
                                         " ChapterID IN " & strChapterSQL & "))"
                            Case "PENDING"
                                strSQL = " SELECT  AutoMemberID, Email " & _
                                         " From IndSpouse where  Email <> '' " & _
                                         " and (automemberid in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0 and " & _
                                         " ChapterID IN " & strChapterSQL & _
                                         " ) or Relationship in (Select Distinct ParentID From Contestant Where " & _
                                         " ContestYear = '" & Application("ContestYear") & "' " & _
                                         " and PaymentReference is null and ParentID <> 0 and ChildNumber <> 0 and " & _
                                         " ChapterID IN " & strChapterSQL & "))"

                        End Select
                    End If
            End Select

            lblDebug.Text = "To: " & ddlTo.SelectedItem.Value & ", " & Session("RoleId") & ", " & Session("LoginChapterID") & ", " & Session("LoginClusterID") & ", " & Session("LoginZoneID") & ", SQL =" & strSQL
            lblDebug.Visible = True
            If strSQL <> "" Then
                dsData = SqlHelper.ExecuteDataset(cnTemp, CommandType.Text, strSQL)
            Else
                Exit Sub
            End If
        Catch ex As Exception
            lblDebug.Text = "To: " & ddlTo.SelectedItem.Value & ", " & Session("RoleId") & ", " & Session("LoginChapterID") & ", " & Session("LoginClusterID") & ", " & Session("LoginZoneID") & ", SQL =" & strSQL
            lblDebug.Text = lblDebug.Text & "" & ex.Message
            Exit Sub
        End Try
        lblDebug.Visible = False

        lstAddress.Visible = False
        Dim intctr As Integer
        intctr = 0
        If Not dsData Is Nothing Then
            If dsData.Tables.Count > 0 Then
                If dsData.Tables(0).Rows.Count > 0 Then
                    lstAddress.DataSource = dsData
                    lstAddress.DataBind()
                    lstAddress.Visible = True
                    strInsertToValues = ""
                    For intctr = 0 To lstAddress.Items.Count - 1
                        lstAddress.Items(intctr).Selected = True
                    Next
                End If
            End If
        End If
    End Sub

    Protected Sub btnIns_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIns.Click
        lblEmailError.Text = ""
        txtTo.Text = ""
        For intCtr = 0 To lstAddress.Items.Count - 1
            If lstAddress.Items(intCtr).Selected = True Then
                txtTo.Text = txtTo.Text & lstAddress.Items(intCtr).Text & ", "
            End If
        Next
        If txtTo.Text <> "" Then
            txtTo.Text = Mid(txtTo.Text, 1, (Len(txtTo.Text) - 2))
        End If
    End Sub

    Private Function GetMailMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String, ByVal sMailFrom As String) As MailMessage
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim mm As MailMessage

        '(1) Create the MailMessage instance
        Try
            mm = New MailMessage(sMailFrom, sMailFrom)
        Catch
            Response.write("Email Address Having Problem is " & sMailTo & " From Address:" & sMailFrom)
        End Try


        '(2) Assign the MailMessage's properties
        mm.Subject = sSubject
        mm.Body = sBody
        mm.IsBodyHtml = True

        strEmailAddressList = Split(sMailTo, ",")
        For Each item In strEmailAddressList
            'check if email address is valid
            If EmailAddressCheck(item.ToString) Then
                mm.Bcc.Add(item.ToString)
            End If
        Next

        mm.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        If String.IsNullOrEmpty(AttachmentFile.FileName) OrElse AttachmentFile.PostedFile Is Nothing Then
            '  Throw New ApplicationException("Egad, a file wasn't uploaded... you should probably use more graceful error handling than this, though...")
        Else
            mm.Attachments.Add(New Attachment(AttachmentFile.PostedFile.InputStream, AttachmentFile.FileName))
        End If

        Return mm

    End Function

    Private Function SendEmail(ByVal sMailTo As String, ByVal mmMailMsg As MailMessage) As Boolean
        '' ''(1) Rename the mail to
        ' ''If mmMailMsg.To.Count > 0 Then mmMailMsg.To.RemoveAt(0)
        ' ''mmMailMsg.To.Add(sMailTo)

        '(3) Create the SmtpClient object
        Dim smtp As New SmtpClient

        '(4) Send the MailMessage (will use the Web.config settings)
        smtp.Host = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost") '"espresso.extremezone.com"

        Try
            smtp.Send(mmMailMsg)
        Catch 
            Return False
        End Try

        Return True

    End Function

    Protected Sub btnSend_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSend.Click
        Dim strEmailAddressList() As String
        Dim item As Object
        Dim sbEmailSuccess As New StringBuilder
        Dim sbEmailFailure As New StringBuilder
        Dim mm As MailMessage

        lblEmailError.Text = ""

        If txtFrom.Text = "" Then Exit Sub
        If txtTo.Text = "" Then Exit Sub
        If txtEmailBodtText.Text = "" Then Exit Sub

        If txtSubject.Text = "" Then
            txtSubject.Text = "Message from North South Foundation."
        End If


        mm = GetMailMessage(Replace(txtSubject.Text, "'", "''"), Replace(txtEmailBodtText.Text, "'", "''"), txtTo.Text, txtFrom.Text)
        If SendEmail(txtTo.Text, mm) = True Then
            lblEmailError.Text = "Email sent successfully."
            '    sbEmailSuccess.Append("Email Send To:" & item.ToString & "<BR>")
            'Else
            '    sbEmailFailure.Append("Email Send To:" & item.ToString & "<BR>")
        Else
            lblEmailError.Text = "Problem emailing."
        End If


        strEmailAddressList = Split(txtTo.Text, ",")
        ''Try
        ''    For Each item In strEmailAddressList
        ''        mm = GetMailMessage(Replace(txtSubject.Text, "'", "''"), Replace(txtEmailBodtText.Text, "'", "''"), item.ToString, txtFrom.Text)
        ''        If SendEmail(item.ToString, mm) = True Then
        ''            sbEmailSuccess.Append("Email Send To:" & item.ToString & "<BR>")
        ''        Else
        ''            sbEmailFailure.Append("Email Send To:" & item.ToString & "<BR>")
        ''        End If
        ''    Next
        ''Catch
        ''    Response.Write("Error Occured: <BR>")
        ''End Try
        lblEmailError.Visible = True


    End Sub

    Protected Sub ddlTo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlTo.SelectedIndexChanged
        lblEmailError.Text = ""
        LoadlstAddress()
    End Sub

    Function EmailAddressCheck(ByVal emailAddress As String) As Boolean
        Try
            Dim pattern As String = "^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$"
            Dim emailAddressMatch As Match = Regex.Match(emailAddress, pattern)
            If emailAddressMatch.Success Then
                EmailAddressCheck = True
            Else
                EmailAddressCheck = False
            End If
        Catch ex As Exception
            EmailAddressCheck = False
        End Try
    End Function

End Class


