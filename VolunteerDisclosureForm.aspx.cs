﻿using System;
using System.Collections;
using System.Data;
using System.Web.UI.WebControls;
using Microsoft.ApplicationBlocks.Data;
using System.Text;
using System.IO;
using System.Web;
using System.Net.Mail;
using System.Data.SqlClient;

public partial class Default4 : System.Web.UI.Page
{
    
    protected void Page_Load(object sender, EventArgs e)
    {
        try
        {

            //Session["loginID"] = "12345";
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            if (Convert.ToBoolean(Session["LoggedIn"]) != true && Session["LoggedIn"].ToString() != "LoggedIn")
                Response.Redirect("login.aspx?entry=v");
            string ConnectionString = "ConnectionString";
            string StrQrySearchName = " select distinct FirstName+' '+ LastName as name,chapterid  from  IndSpouse where AutoMemberId=" + Session["LoginID"] + "";
            DataSet dsNmae = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQrySearchName);
            string strName = dsNmae.Tables[0].Rows[0]["name"].ToString();

            VolNamelbl.Text = strName;
            if (!IsPostBack)
            {
                lbsuccess.Text = "";
                lberror.Text = "";

                fillValues();

            }

        }catch(Exception ex){}
       


    }
    protected void fillValues() {
        string ConnectionString = "ConnectionString";
        DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, "select * from VolDiscForm where MemberID=" + Session["LoginID"] + " ");
        try
        {
            if (dsCheck != null)
            {
                if (dsCheck.Tables[0].Rows.Count>0)
                {
                    QTextBoxEducation.Text = dsCheck.Tables[0].Rows[0]["Education"].ToString();
                    //ddEvent.SelectedValue = dsCheck.Tables[0].Rows[0]["EventId"].ToString();

                    QTextBoxWorkExp.Text = dsCheck.Tables[0].Rows[0]["WorkExp"].ToString();
                    QTextBoxVolWork.Text = dsCheck.Tables[0].Rows[0]["VolWork"].ToString();
                    string AgentBrokerStatus = dsCheck.Tables[0].Rows[0]["AgentBroker"].ToString();
                    if (AgentBrokerStatus.Equals("Y"))
                    {
                        RadioButton1.Checked = true;
                    }
                    else
                    {
                        RadioButton2.Checked = true;
                    }
                    QTextBoxOwnBusiness.Text = dsCheck.Tables[0].Rows[0]["OwnBusiness"].ToString();
                    QTextBoxConflictBus.Text = dsCheck.Tables[0].Rows[0]["PotConfBus"].ToString();
                    QTextBoxConflictNGO.Text = dsCheck.Tables[0].Rows[0]["PotConfNGO"].ToString();

                    QTextBoxVenue.Text = dsCheck.Tables[0].Rows[0]["VenueConf"].ToString();
                    QTextBoxConflictSolution.Text = dsCheck.Tables[0].Rows[0]["PotConfSol"].ToString();
                }
            }
        }
        catch (Exception ex) { }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        insertintoTable();
    }
    protected void insertintoTable()
    {
        try
        {
            if (QTextBoxEducation.Text == String.Empty || QTextBoxEducation.Text.Trim().Length == 0 || QTextBoxWorkExp.Text == String.Empty || QTextBoxVolWork.Text == String.Empty || QTextBoxOwnBusiness.Text == string.Empty || QTextBoxConflictBus.Text == String.Empty || QTextBoxConflictNGO.Text == String.Empty || QTextBoxVenue.Text == String.Empty || QTextBoxConflictSolution.Text==String.Empty)
            {
                lbsuccess.Text = "";
                lberror.Text = "All are mandatory fields.  None, Not applicable, etc. are acceptable.";
            }
            else
            {
                string ConnectionString = "ConnectionString";
                string loginid = Session["loginID"].ToString();
                string rbstatus = string.Empty;
                string StrQryInsert = "INSERT INTO VolDiscForm(MemberID, Education,WorkExp,VolWork,AgentBroker,OwnBusiness,PotConfBus,PotConfNGO,VenueConf,PotConfSol,CreateDate,CreatedBy) VALUES(" + loginid + ",@Edu,@WorkExp,@VolWork,";
             
                string StrQryUpdate = "Update VolDiscForm set Education=@Edu, WorkExp=@WorkExp , VolWork=@VolWork ";
                 
                if (RadioButton1.Checked == true)
                {
                    rbstatus = "Y";
                    StrQryInsert= StrQryInsert + "'" + rbstatus + "',";

                    StrQryUpdate = StrQryUpdate + ", AgentBroker='" + rbstatus+"'";
                                        
                }
                else
                {
                    StrQryInsert = StrQryInsert + " Null,";
                    StrQryUpdate = StrQryUpdate + ", AgentBroker=Null";
                }

                StrQryInsert = StrQryInsert + "@OwnBusiness,@ConflictBus,@ConflictNGO,@BoxVenue,@ConflictSolution,Getdate(),'" + loginid + "')";

                StrQryUpdate = StrQryUpdate + ", OwnBusiness=@OwnBusiness , PotConfBus=@ConflictBus , PotConfNGO=@ConflictNGO , VenueConf=@BoxVenue , PotConfSol=@ConflictSolution , ModifyDate=Getdate() , ModifiedBy=" + loginid + " where MemberID=" + loginid + "";
                   
               string CheckExists = "select count(*) from VolDiscForm where MemberID=" + loginid + "";

                if (Convert.ToInt32(SqlHelper.ExecuteScalar(Application["connectionstring"].ToString(), CommandType.Text, CheckExists)) > 0)
                {


                    SqlParameter[] parameters = { new SqlParameter("@Edu", SqlDbType.VarChar, 2000), new SqlParameter("@WorkExp", SqlDbType.VarChar, 2000), new SqlParameter("@VolWork", SqlDbType.VarChar, 2000), new SqlParameter("@OwnBusiness", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictBus", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictNGO", SqlDbType.VarChar, 2000), new SqlParameter("@BoxVenue", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictSolution", SqlDbType.VarChar, 2000) };
                    parameters[0].Value =QTextBoxEducation.Text;
                    parameters[1].Value= QTextBoxWorkExp.Text;
                    parameters[2].Value = QTextBoxVolWork.Text;
                    parameters[3].Value = QTextBoxOwnBusiness.Text;
                    parameters[4].Value = QTextBoxConflictBus.Text;
                    parameters[5].Value = QTextBoxConflictNGO.Text;
                    parameters[6].Value = QTextBoxVenue.Text;
                    parameters[7].Value = QTextBoxConflictSolution.Text;


                  
           
                  int i =  SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text,StrQryUpdate, parameters);
                  lberror.Text = "";
                  lbsuccess.Text = "Updated Successfully";

                }else 
                {
                   

                    SqlParameter[] parameters = { new SqlParameter("@Edu", SqlDbType.VarChar, 2000), new SqlParameter("@WorkExp", SqlDbType.VarChar, 2000), new SqlParameter("@VolWork", SqlDbType.VarChar, 2000), new SqlParameter("@OwnBusiness", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictBus", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictNGO", SqlDbType.VarChar, 2000), new SqlParameter("@BoxVenue", SqlDbType.VarChar, 2000), new SqlParameter("@ConflictSolution", SqlDbType.VarChar, 2000) };
                    parameters[0].Value = QTextBoxEducation.Text;
                    parameters[1].Value = QTextBoxWorkExp.Text;
                    parameters[2].Value = QTextBoxVolWork.Text;
                    parameters[3].Value = QTextBoxOwnBusiness.Text;
                    parameters[4].Value = QTextBoxConflictBus.Text;
                    parameters[5].Value = QTextBoxConflictNGO.Text;
                    parameters[6].Value = QTextBoxVenue.Text;
                    parameters[7].Value = QTextBoxConflictSolution.Text;
                    int i = SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, StrQryInsert, parameters);
                    lberror.Text = "";
                    lbsuccess.Text = "Added Successfully";
                }

   

            }
        }
        catch (Exception ex)
        {
            lbsuccess.Text = "";
           lberror.Text = "Server Error";
        }
    }
    protected void RadioButton1_CheckedChanged(object sender, EventArgs e)
    {

    }
    protected void RadioButton2_CheckedChanged(object sender, EventArgs e)
    {

    }
}