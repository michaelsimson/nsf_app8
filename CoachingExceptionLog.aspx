﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CoachingExceptionLog.aspx.cs" Inherits="CoachingExceptionLog" MasterPageFile="~/NSFMasterPage.master" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <style type="text/css">
            .grid {
                width: 500px;
            }

                .grid th {
                    background-color: Green;
                    color: #ffffff;
                }

                .grid tr:nth-child(even) {
                    background-color: #ffffff;
                }

                .grid tr:nth-child(odd) {
                    background-color: #cccccc;
                }

                .grid td {
                    padding-left: 10px;
                }
        </style>

        <script type="text/javascript">


            $(document).on("click", ".btnDownload", function (e) {
                var fileName = $(this).attr("attr-Name");

                document.getElementById("<%=hdnFileName.ClientID%>").value = fileName;
                document.getElementById("<%=btnDownloadConfirm.ClientID%>").click();
            });

        </script>
    </div>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <asp:Button ID="btnDownloadConfirm" Style="display: none;" runat="server" OnClick="btnDownloadConfirm_onClick" />
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Exception Log
                     <br />
        <br />
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div align="center">
        <asp:Literal ID="ltrExceptionList" runat="server"></asp:Literal>
    </div>

    <input type="hidden" id="hdnFileName" runat="server" value="ZoomMeeting" />

</asp:Content>
