<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ChangeEmail.aspx.vb" Inherits="VRegistration.ChangeEmail" title="Change Email" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    
    <div align="left">
        <asp:hyperlink id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" Visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>&nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" Visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink></div>
     <div style="text-align: center" align="left"> 
           
   <table border = "0" cellpadding = "0" cellspacing = "0" width = "1004px">
   <tr><td class="title02" vAlign="top" align="center">
						Change My Email Address
					</td>
				</tr>
   <tr><td align="center"> 
    <table id="tblEmail" runat="server" width="700px" cellpadding="3" cellspacing="0" border="0">
				
				
				<tr>
					<td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Current Email:</td>
					<td  align="left"><asp:textbox id="txtOldEmail" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px"></asp:textbox><asp:requiredfieldvalidator id="rfvOldEmail" runat="server" Display="Dynamic" ErrorMessage="Enter current Email"
							ControlToValidate="txtOldEmail"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" valign="top" noWrap align="right" style="width: 250px">Password:</td>
                   	<td  align="left"><asp:textbox id="txtPassword" runat="server" CssClass="smFont" TextMode="password" Width="150px"  Enabled ="false"></asp:textbox>
                           &nbsp;<asp:Label ID ="lblPwdError" runat="server" Text="" ForeColor="Green"></asp:Label>
					<asp:requiredfieldvalidator id="rfvPassword" runat="server" Display="Dynamic" ErrorMessage="Enter password"
							ControlToValidate="txtPassword" Enabled="false"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;New Email Address:
					</td>
					<td  align="left"><asp:textbox id="txtNewEmail" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px"></asp:textbox>
					<asp:requiredfieldvalidator id="Requiredfieldvalidator" runat="server" CssClass="SmallFont" ControlToValidate="txtNewEmail"
								Display="Dynamic" ErrorMessage="Enter new Email "></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 250px">&nbsp;Confirm New Email Address:
					</td>
					<td  align="left"><asp:textbox id="txtConfirm" runat="server" CssClass="smFont" TextMode="SingleLine"  Width="150px"></asp:textbox>
					<asp:requiredfieldvalidator id="rfvReTypeEmail" runat="server" CssClass="SmallFont" ControlToValidate="txtConfirm"
								Display="Dynamic" ErrorMessage="Retype new Email "></asp:requiredfieldvalidator>
							<br /></td>
				</tr>
				<tr>
				    <td></td>
					<td align="center" colSpan="1" style="HEIGHT: 26px">
                        &nbsp;&nbsp;<asp:button id="btnUpdate" runat="server" CssClass="FormButton" Text="Update"></asp:button>
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;
                        &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;
                    </td>
				</tr></table>
      <table border = "0" cellpadding = "0" cellspacing ="0"  runat="server" width="700px" align="center">
      <tr visible = "false" id="trEmailChangeNw" runat="server">
      <td align="center" >
      <asp:Label ID ="lblEmailChangeNew" runat="server" Text=""></asp:Label><br />
          <asp:TextBox ID="txtNewPassword" TextMode="Password" runat="server"></asp:TextBox> &nbsp;&nbsp;
          <asp:Button ID="btnUpdateNw" OnClick="btnUpdateNw_Click" runat="server" Text="Update" />

      </td> </tr> 
      <tr><td align="center" ><asp:Label ID="lblNewPwdError" runat="server" Text="" ForeColor ="Green"></asp:Label></td></tr>
      <tr><td align="center"><table id="tblLoginmaster" runat ="server" visible ="false">
      <tr><td class="ItemLabel" valign="top" noWrap align="right" Width="150px">UserName:</td>
					<td align="left"><asp:TextBox ID="txtNewUserName" runat="server" Text=""  Width="150px"></asp:TextBox>
					<asp:requiredfieldvalidator id="Requiredfieldvalidator1" runat="server" Display="Dynamic" ErrorMessage="Enter User Name"
							ControlToValidate="txtNewUserName"></asp:requiredfieldvalidator>
					</td></tr>
      <tr><td class="ItemLabel" valign="top" noWrap align="right" Width="150px">New Email:</td>
					<td align="left"><asp:TextBox ID="txtNewUserEmail" runat="server" Text=""  Width="150px"></asp:TextBox>
					<asp:requiredfieldvalidator id="Requiredfieldvalidator2" runat="server" Display="Dynamic" ErrorMessage="Enter User Email"
							ControlToValidate="txtNewUserName"></asp:requiredfieldvalidator>
				
					</td></tr>
      <tr><td class="ItemLabel" valign="top" noWrap align="right" Width="150px">New Password:</td>
					<td align="left"><asp:TextBox ID="txtNewUserPwd" TextMode="Password" runat="server" Text=""  Width="150px"></asp:TextBox>
					<asp:requiredfieldvalidator id="Requiredfieldvalidator3" runat="server" Display="Dynamic" ErrorMessage="Enter password"
							ControlToValidate="txtNewUserName"></asp:requiredfieldvalidator>
				
					</td></tr>
      <tr><td class="ItemLabel" valign="top" noWrap align="right" Width="150px">Confirm Password:</td>
					<td align="left"><asp:TextBox ID="txtNewUserCfrmPwd" TextMode="Password" runat="server" Text=""  Width="150px"></asp:TextBox>
					<asp:requiredfieldvalidator id="Requiredfieldvalidator4" runat="server" Display="Dynamic" ErrorMessage="Enter Confirm password"
							ControlToValidate="txtNewUserName"></asp:requiredfieldvalidator>
				
					</td></tr>
      <tr><td align="right"><asp:Button ID="btnCreate" runat="server" Text="Create" /></td>
      <td align="left"><asp:Button ID="BtnCancel" runat="server" Text="Cancel" /></td></tr>
      </table></td></tr>
      <tr><td colspan="2"><asp:Label ID="lblSuccess" runat="server" Text="" ForeColor="Red"></asp:Label></td></tr>
      
      <tr visible = "false" id="DisplaySuccessMsg" runat="server">
      <td align="center" >
      <br /><br />
      <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">
      Your record was updated successfully.  Please click  click <asp:LinkButton  class="btn_02" ID="hlinkBack" runat="server" >here</asp:LinkButton> to continue
      </p>
      </td></tr>
      <tr visible = "false" id="DisplayFailMsg" runat="server">
      <td align="center" >
      <br /><br />
     <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
      New Email already exists.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@northsouth.org" target="_blank" >nsfcontests@northsouth.org</a>.  Please click <a href="Http://northsouth.org">here</a> to continue.
      </p>
      </td></tr>
      <tr visible = "false" id="DisplayFailMsg5" runat="server">
      <td align="center" >
      <br /><br />
     <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
      New Email already exists in multiple places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@northsouth.org" target="_blank" >nsfcontests@northsouth.org</a>. Please click<a href="Http://northsouth.org">here</a> to continue.
      </p>
      </td></tr>
      <tr visible = "false" id="DisplayFailMsg6" runat="server">
      <td align="center" >
      <br /><br />
     <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">  
      Old email has duplicates in multiple places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@northsouth.org" target="_blank" >nsfcontests@northsouth.org</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
      </p>
      </td></tr>
      <tr visible = "false" id="DisplayFailMsg2" runat="server">
      <td align="center" >
      <br /><br />
     <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">  
      Old email has duplicates in multiple places.  Further new email exists already.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@northsouth.org" target="_blank" >nsfcontests@northsouth.org</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
      </p>
      </td></tr>
       <tr visible = "false" id="DisplayFailMsg3" runat="server">
      <td align="center" >
      <br /><br />
      <p style="text-align:justify;font-size:13px; font-family:Georgia, Times, serif; ">   
      Old email exists in more than two places.  A message will be sent to Customer Service team to resolve this issue.  If you don�t hear in two or three days, please send an email to <a href="mailto:nsfcontests@northsouth.org" target="_blank" >nsfcontests@northsouth.org</a>.   Please click <a href="Http://northsouth.org">here</a> to continue.
      </p>
      </td></tr>      
    <tr>
      <td align="center" >
      <br /><br />
      <asp:label id="txtErrMsg" runat="server" ForeColor="red" Visible="False"> Invalid Current Email.</asp:label>
      </td></tr>
      </table>	
      </td></tr></table>
      </div>	
</asp:Content>


 

 
 
 