<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true"
    CodeFile="demo_vocab_display_word.aspx.cs" Inherits="vocab_game_demo_vocab_display_word"
    Title="Vocabulary Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table style="margin-left:100px" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="80%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitDemo">Quit Demo</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
             <td>
                <asp:Label ID="lbl1" runat="server" Text = "1. DO NOT " Font-Bold="True" ></asp:Label> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <asp:Label ID="lbl2a" runat="server" Font-Bold="true" Text="2. "></asp:Label> Your results for this session will not be saved if you attempt <asp:Label ID="lbl2b" runat="server" Text = " less than 10 words." Font-Bold="True" ></asp:Label> <br />
                <asp:Label ID="lbl3a" runat="server" Font-Bold="true" Text="3. "></asp:Label> To <asp:Label ID="lbl3b" runat="server" Font-Bold="true" Text=" End a Game "></asp:Label> click <asp:Label ID="lbl3c" runat="server" Font-Bold="true" Text=" End Session "></asp:Label> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />
                <p>
                </p>
            </td>
        </tr>
    </table>
    <table style="margin-left:200px" border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="80%">
        <tr>
            <%--<td width="10%" align="center" valign="middle">
                <img src="images/bee_icon.gif" />
            </td>--%>
            <td style="width: 1011px">
                <asp:FormView ID="frmViewQA" runat="server" AllowPaging="True" OnDataBound="frmViewQA_DataBound" OnItemCommand="frmViewQA_ItemCommand">
                    <ItemTemplate>
                        <table>
                            <tr>
                                <td>
                                    The word is: 
                                        <asp:Label ID="lblWord" runat="server" Text='<%# Bind("Word") %>' Font-Bold="true"></asp:Label>.
                                    This word is a 
                                        <asp:Label ID="lblPartofSpeech" runat="server" Text='<%# Bind("[Parts of Speech]") %>' Font-Bold="true"></asp:Label>
                                        <asp:Label ID="lblAnswer" runat="server" Text='<%# Bind("Answer") %>' Visible="false"></asp:Label>
                                        <asp:Label ID="lblMeaning" runat="server" Text='<%# Bind("Meaning") %>' Visible="false"></asp:Label>
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOptionA" runat="server" GroupName="options" Text='<%# Bind("A") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOptionB" runat="server" GroupName="options" Text='<%# Bind("B") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOptionC" runat="server" GroupName="options" Text='<%# Bind("C") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOptionD" runat="server" GroupName="options" Text='<%# Bind("D") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:RadioButton ID="rbtnOptionE" runat="server" GroupName="options" Text='<%# Bind("E") %>' />
                                </td>
                            </tr>
                            <tr>
                                <td>
                                    <asp:Button ID="btnSubmitAnswer" runat="server" Text="Submit Answer" CommandName="SubmitAnswer" />
                                </td>
                            </tr>
                        </table>
                    </ItemTemplate>
                    <PagerSettings Visible="False" />
                </asp:FormView>
                <asp:Panel ID="panelWrongAnswer" runat="server" Visible="false" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
                        <tr>
                            <td>
                                The word was <asp:Label ID="lblWrongCorrectWord" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You answered <asp:Label ID="lblWrongGivenAnswer" runat="server" Font-Bold="true"></asp:Label>                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="lblsorry" Text="Sorry!" runat="server" Font-Bold="true" Font-Italic="true"></asp:Label>  
                                
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                The correct answer is 
                                    <asp:Label ID="lblWrongCorrectAnswer" runat="server" Font-Bold="true"></asp:Label>.
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                               <asp:Label ID="lblmean" Text=" Meaning:" runat="server" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblWrongMeaning" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You have attempted 
                                    <asp:Label ID="lblWrongAttemptCount" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="Label4" Text="word(s)" runat="server" Font-Bold="true"></asp:Label> in this session.
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="panelCorrectAnswer" runat="server" Visible="false" Width="100%">
                    <table border="0" cellpadding="0" cellspacing="3" bgcolor="#ffffff" width="100%">
                        <tr>
                            <td>
                                The word was 
                                    <asp:Label ID="lblCorrectCorrectWord" runat="server" Font-Bold="true"></asp:Label>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You answered 
                                    <asp:Label ID="lblCorrectGivenAnswer" runat="server" Font-Bold="true"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You answered the word <asp:Label ID="lblcorr" Text="correctly" runat="server" Font-Bold="true"></asp:Label> Good for you.
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <asp:Label ID="Labelmean" Text=" Meaning:" runat="server" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblCorrectMeaning" runat="server"></asp:Label>
                                <br />
                                <br />
                            </td>
                        </tr>
                        <tr>
                            <td>
                                You have attempted 
                                    <asp:Label ID="lblCorrectAttemptCount" runat="server" Font-Bold="true"></asp:Label>
                                    <asp:Label ID="Label1" Text="word(s)" runat="server" Font-Bold="true"></asp:Label> in this session.
                                <br />
                                <br />
                            </td>
                        </tr>
                    </table>
                </asp:Panel>
                <asp:Panel ID="panelControls" runat="server" Visible="false" Width="100%">
                    <asp:Button ID="btnNextWord" runat="server" Text="Next Word" OnClick="btnNextWord_Click" />
                    <asp:Button ID="btnEndSession" runat="server" Text="End Session" OnClick="btnEndSession_Click" />
                </asp:Panel>
                <asp:Panel ID="panelEndResult" runat="server" Visible="false" Width="100%">
                    You attempted <asp:Label ID="lblAttemptedWords" runat="server" Font-Bold="true"></asp:Label> 
                    <asp:Label ID="Label2" Text="word(s)" runat="server" Font-Bold="true"></asp:Label> out of which you answered<br />
                    <br />
                    <br />
                    <asp:Label ID="lblEndResultCorrect" runat="server" Font-Bold="true"></asp:Label>
                    <img src="images/correct.gif" />
                    <br />
                    <asp:Label ID="lblEndResultWrong" runat="server" Font-Bold="true"></asp:Label>
                    <img src="images/wrong.gif" />
                    <br />
                    <br />
                    <asp:Button ID="btnViewWordList" runat="server" Text="View Word List" OnClick="btnViewWordList_Click" />&nbsp;&nbsp;
                    <asp:Button ID="btnSelectAnotherWordCategory" runat="server" Text="Select Another Word Category"
                        OnClick="btnSelectAnotherWordCategory_Click" />
                </asp:Panel>
            </td>
        </tr>
    </table>
</asp:Content>
