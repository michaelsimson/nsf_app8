using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class vocab_game_demo_vocab_display_word : System.Web.UI.Page
{
    private static string strNextClicked;
    private static int answerflag;
    private static int endResultCorrect;
    private static int endResultWrong;

    private static int questionCount;

    public int QuestionCount
    {
        get { return questionCount; }
        set { questionCount = value; }
    }
    private int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }
    private static DataSet wordsDataSet;
    public DataSet dsDemoWords
    {
        get { return wordsDataSet; }
        set { wordsDataSet = value; }
    }

    private static int ChildNumber;
    private static string UsedID;
    private static int category;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Session["childNumber"] == null)
            {
                Response.Redirect("../Login.aspx");
            }
            else
            {
                ChildNumber = Convert.ToInt32(Session["childNumber"]);
                UsedID = Session["UserID"].ToString();
            }
            if (Request.QueryString["category"] != null)
            {
                category = int.Parse(Request.QueryString["category"].ToString());
                Level = category / 10;
                SubLevel = category % 10;

                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                dsDemoWords = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "vusp_GetVocabGameDetails", new SqlParameter("@Level", level ), new SqlParameter("@SubLevel", SubLevel));
              
                if (dsDemoWords.Tables[0].Rows.Count > 0)
                {
                    dsDemoWords.Tables[0].Columns.Add("Answered");
                    frmViewQA.DataSource = dsDemoWords;
                    frmViewQA.DataBind();
                }
                else
                {
                    frmViewQA.Visible = false;
                    panelWrongAnswer.Visible = false;
                    panelCorrectAnswer.Visible = true;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;
                    panelWarningLessWords.Visible = false;
                }
                QuestionCount = 1;
                strNextClicked = string.Empty;
                endResultCorrect = 0;
                endResultWrong = 0;
              
           }
                            SqlConnection conn1 = new SqlConnection(Application["ConnectionString"].ToString());
                            string strg = "select max(Session_ID) from Vocab_user_session_word where childNumber = '" + ChildNumber + "'";
                            DataSet ds2 = SqlHelper.ExecuteDataset(conn1, CommandType.Text, strg);
                            if (DBNull.Value.Equals(ds2.Tables[0].Rows[0][0]))
                            {
                                Session["sessionid"] = 1;
                            }
                            else
                            {
                                int sessionid = Convert.ToInt32(ds2.Tables[0].Rows[0][0]);
                                Session["sessionid"] = sessionid + 1;
                            }
        }
      
        if (strNextClicked == "NextClicked")
        {
            frmViewQA.DataSource = dsDemoWords;
            frmViewQA.DataBind();
            frmViewQA.PageIndex = QuestionCount ;
            strNextClicked = string.Empty;
        }
    }

    protected void btnViewWordList_Click(object sender, EventArgs e)
    {
        Session["WordListDataSet"] = dsDemoWords;
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language=JavaScript>");
        strScript.Append("window.open('VocabGame_ViewWordList.aspx', \"\",\"height=500,width=400,left=0,top=0,toolbar=no,menubar=no,scrollbars=yes\");");
        strScript.Append("</script>");
        RegisterClientScriptBlock("subscribescript", strScript.ToString());
    }

    protected void btnSelectAnotherWordCategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("VocabGame.aspx");
    }

    protected void btnEndSession_Click(object sender, EventArgs e)
    {
        if (QuestionCount < 10)
        {
            lblWarningLessWordsAttempt.Text = QuestionCount.ToString();
            panelWarningLessWords.Visible = true;
            frmViewQA.Visible = false;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = false;
        }
        else
        {
            if (QuestionCount >= 10)
            {
                InsertUserSession();
            }
            frmViewQA.Visible = false;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = true;
            lblAttemptedWords.Text = QuestionCount.ToString();
            lblEndResultCorrect.Text = endResultCorrect.ToString();
            lblEndResultWrong.Text = endResultWrong.ToString();
            panelWarningLessWords.Visible = false;
        }
    }

    protected void frmViewQA_DataBound(object sender, EventArgs e)
    {
        FormViewRow row = frmViewQA.Row;

        RadioButton rbtnOptionA = (RadioButton)row.FindControl("rbtnOptionA");
        RadioButton rbtnOptionB = (RadioButton)row.FindControl("rbtnOptionB");
        RadioButton rbtnOptionC = (RadioButton)row.FindControl("rbtnOptionC");
        RadioButton rbtnOptionD = (RadioButton)row.FindControl("rbtnOptionD");
        RadioButton rbtnOptionE = (RadioButton)row.FindControl("rbtnOptionE");

        if ((rbtnOptionA != null) && (rbtnOptionB != null) && (rbtnOptionC != null) && (rbtnOptionD != null) && (rbtnOptionE != null))
        {
            if (rbtnOptionA.Text == string.Empty)
            {
                rbtnOptionA.Visible = false;
            }

            if (rbtnOptionB.Text == string.Empty)
            {
                rbtnOptionB.Visible = false;
            }

            if (rbtnOptionC.Text == string.Empty)
            {
                rbtnOptionC.Visible = false;
            }

            if (rbtnOptionD.Text == string.Empty)
            {
                rbtnOptionD.Visible = false;
            }

            if (rbtnOptionE.Text == string.Empty)
            {
                rbtnOptionE.Visible = false;
            }
        }
    }

    protected void frmViewQA_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        //QuestionCount++;
        FormViewRow row = frmViewQA.Row;
        string strGivenAnswer = string.Empty;
        string strGivenAnswerOption = string.Empty;

        if (e.CommandName == "SubmitAnswer")
        {
            RadioButton rbtnOptionA = (RadioButton)row.FindControl("rbtnOptionA");
            RadioButton rbtnOptionB = (RadioButton)row.FindControl("rbtnOptionB");
            RadioButton rbtnOptionC = (RadioButton)row.FindControl("rbtnOptionC");
            RadioButton rbtnOptionD = (RadioButton)row.FindControl("rbtnOptionD");
            RadioButton rbtnOptionE = (RadioButton)row.FindControl("rbtnOptionE");

            Label lblWord = (Label)row.FindControl("lblWord");
            Label lblPartofSpeech = (Label)row.FindControl("lblPartofSpeech");
            Label lblAnswer = (Label)row.FindControl("lblAnswer");
            Label lblMeaning = (Label)row.FindControl("lblMeaning");
            Session["word"] = lblWord.Text;
            Session["speech"] = lblPartofSpeech.Text;

            if ((rbtnOptionA != null) && (rbtnOptionB != null) && (rbtnOptionC != null) && (rbtnOptionD != null) && (rbtnOptionE != null) && (lblWord != null) && (lblPartofSpeech != null) && (lblAnswer != null) && (lblMeaning != null))
            {
                if (rbtnOptionA.Checked == true)
                {
                    strGivenAnswer = rbtnOptionA.Text;
                    strGivenAnswerOption = "A";
                }
                else if (rbtnOptionB.Checked == true)
                {
                    strGivenAnswer = rbtnOptionB.Text;
                    strGivenAnswerOption = "B";
                }
                else if (rbtnOptionC.Checked == true)
                {
                    strGivenAnswer = rbtnOptionC.Text;
                    strGivenAnswerOption = "C";
                }
                else if (rbtnOptionD.Checked == true)
                {
                    strGivenAnswer = rbtnOptionD.Text;
                    strGivenAnswerOption = "D";
                }
                else if (rbtnOptionE.Checked == true)
                {
                    strGivenAnswer = rbtnOptionE.Text;
                    strGivenAnswerOption = "E";
                }

                frmViewQA.Visible = false;

                if (lblAnswer.Text.Trim().ToUpper() == strGivenAnswerOption)
                {
                    // Given answer is correct
                    dsDemoWords.Tables[0].Rows[QuestionCount - 1]["Answered"] = "Correct";
                    answerflag = 1 ;
                    endResultCorrect++;

                    panelWrongAnswer.Visible = false;
                    panelCorrectAnswer.Visible = true;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;
                    panelWarningLessWords.Visible = false;

                    lblCorrectCorrectWord.Text = lblWord.Text;
                    lblCorrectGivenAnswer.Text = strGivenAnswer;
                    lblCorrectMeaning.Text = lblMeaning.Text;
                    lblCorrectAttemptCount.Text = QuestionCount.ToString();
                }
                else
                { 
                    // Given answer is wrong
                    dsDemoWords.Tables[0].Rows[QuestionCount - 1]["Answered"] = "Wrong";
                    answerflag = 0 ;
                    endResultWrong++;

                    panelWrongAnswer.Visible = true;
                    panelCorrectAnswer.Visible = false;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;
                    panelWarningLessWords.Visible = false;

                    lblWrongCorrectWord.Text = lblWord.Text;
                    lblWrongGivenAnswer.Text = strGivenAnswer;
                    RadioButton rbtnOptionCorrect = (RadioButton)row.FindControl("rbtnOption" + lblAnswer.Text);
                    if (rbtnOptionCorrect != null)
                    {
                        lblWrongCorrectAnswer.Text = rbtnOptionCorrect.Text;
                    }
                    lblWrongMeaning.Text = lblMeaning.Text;
                    lblWrongAttemptCount.Text = QuestionCount.ToString();
                }
           
            string strCategory = string.Empty;
            if (category == 11)
            {
                strCategory = "Beginner / Easy";
            }
            else if (category == 12)
            {
                strCategory = "Beginner / Difficult";
            }
            else if (category == 21)
            {
                strCategory = "Intermediate / Easy";
            }
            else if (category == 22)
            {
                strCategory = "Intermediate / Difficult";
            }
            else if (category == 31)
            {
                strCategory = "Advanced / Easy";
            }
            else if (category == 32)
            {
                strCategory = "Advanced / Difficult";
            }
            else if (category == 41)
            {
                strCategory = "More Advanced / Easy";
            }
            else if (category == 42)
            {
                strCategory = "More Advanced / Difficult";
            }
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            string str = "SET QUOTED_IDENTIFIER OFF ";
            str +=  @"SELECT * FROM vocab_User_Session_Word WHERE word = """ + Session["word"] + @""" and childnumber='" + ChildNumber + "' and session_id = " + Session["sessionid"] + " and part_of_speech= '" + Session["speech"] + "'";
            str += " SET QUOTED_IDENTIFIER ON";
            DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str);
            if (ds.Tables[0].Rows.Count == 0)
            {
                string str1 = "SET QUOTED_IDENTIFIER OFF ";
                //str1 += @"insert into vocab_User_Session_Word(Childnumber,Session_id,Session_Date, Word,Word_category,Answer_flag,part_of_speech) values ('" + ChildNumber + "'," + Session["sessionid"] + ",'" + DateTime.Now + "','" + Session["word"].ToString() + "','" + strCategory + "','" + answerflag + "','" + Session["speech"] + "')";
                str1 += @"insert into vocab_User_Session_Word(Word, Childnumber,Session_id,Session_Date, Word_category,Answer_flag,part_of_speech) values (""" + Session["word"].ToString() + @""",'" + ChildNumber + "'," + Session["sessionid"] + ",'" + DateTime.Now + "','" + strCategory + "','" + answerflag + "','" + Session["speech"] + "')";
                str1 += " SET QUOTED_IDENTIFIER ON";
                SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str1);

                string str2 = "SET QUOTED_IDENTIFIER OFF ";
                str2 += @"SELECT * FROM vocab_User_Session_Word where word = """ + Session["word"] + @""" and childnumber = '" + ChildNumber + "'  order by session_id DESC";
                str2 += " SET QUOTED_IDENTIFIER ON";
                DataSet ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, str2);
                if (ds1.Tables[0].Rows.Count > 3)
                {
                    if ((Convert.ToBoolean(ds1.Tables[0].Rows[0][4]) == true ) && (Convert.ToBoolean(ds1.Tables[0].Rows[1][4]) == true ) && (Convert.ToBoolean(ds1.Tables[0].Rows[2][4]) == true ))
                    {
                        string str3 = "SET QUOTED_IDENTIFIER OFF ";
                        str3 += @"insert into vocab_User_easy_Word(Word,ChildNumber,Session_id,Session_Date,Word_category,part_of_speech) values (""" + Session["word"] + @""",'" + ChildNumber + "'," + Session["sessionid"] + ",'" + DateTime.Now + "','" + strCategory + "','" + Session["speech"] + "')";
                        str3 += " SET QUOTED_IDENTIFIER ON";
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str3);
                    }
                }
            }
            }
        }
    }

    private void GetNextWord(object sender, EventArgs e)
    {
        if (QuestionCount < (dsDemoWords.Tables[0].Rows.Count))
        {
            //QuestionCount++;
            strNextClicked = "NextClicked";

            frmViewQA.DataSource = dsDemoWords;
            frmViewQA.DataBind();
            frmViewQA.PageIndex = QuestionCount;
            QuestionCount++;

            frmViewQA.Visible = true;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = false;
            panelWarningLessWords.Visible = false;
            Page_Load(sender, e);
        }
        else
        {
            if (QuestionCount >= 10)
            {
                InsertUserSession();
            }
            frmViewQA.Visible = false;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = true;
            lblEndResultCorrect.Text = endResultCorrect.ToString();
            lblEndResultWrong.Text = endResultWrong.ToString();
            lblAttemptedWords.Text = QuestionCount.ToString();
            panelWarningLessWords.Visible = false;
        }
    }

    private void InsertUserSession()
    {
        string strCategory = string.Empty;
        if (category == 11)
        {
            strCategory = "Beginner / Easy";
        }
        else if (category == 12)
        {
            strCategory = "Beginner / Difficult";
        }
        else if (category == 21)
        {
            strCategory = "Intermediate / Easy";
        }
        else if (category == 22)
        {
            strCategory = "Intermediate / Difficult";
        }
        else if (category == 31)
        {
            strCategory = "Advanced / Easy";
        }
        else if (category == 32)
        {
            strCategory = "Advanced / Difficult";
        }
        else if (category == 41)
        {
            strCategory = "More Advanced / Easy";
        }
        else if (category == 42)
        {
            strCategory = "More Advanced / Difficult";
        }
        SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        SqlHelper.ExecuteNonQuery(conn, CommandType.StoredProcedure, "vusp_InsertVocabUserSessionMasterDetails", new SqlParameter("@sessiondate", DateTime.Now), new SqlParameter("@sessionid", Session["sessionid"]), new SqlParameter("@ChildNumber", ChildNumber), new SqlParameter("@WordCount", QuestionCount), new SqlParameter("@CorrectAnswers", endResultCorrect), new SqlParameter("@WordCategory", strCategory));
        }

    protected void btnNextWord_Click(object sender, EventArgs e)
    {
        GetNextWord(sender, e);
    }

    protected void btnWarningCancel_Click(object sender, EventArgs e)
    {
        GetNextWord(sender, e);
    }

    protected void btnWarningEndGame_Click(object sender, EventArgs e)
    {
        frmViewQA.Visible = false;
        panelControls.Visible = false;
        panelCorrectAnswer.Visible = false;
        panelWrongAnswer.Visible = false;
        panelEndResult.Visible = true;
        lblEndResultCorrect.Text = endResultCorrect.ToString();
        lblEndResultWrong.Text = endResultWrong.ToString();
        lblAttemptedWords.Text = QuestionCount.ToString();
        panelWarningLessWords.Visible = false;
    }

}
