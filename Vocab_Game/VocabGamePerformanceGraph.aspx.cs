using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Drawing;

public partial class vocab_game_VocabGamePerformanceGraph : System.Web.UI.Page
{

    private static int ChildNumber;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["childNumber"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
        else
        {
            ChildNumber = Convert.ToInt32(Session["childNumber"]);
        }

       
            DataSet dsPerformace = new DataSet();
            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
            dsPerformace = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "vusp_GetVocabUserSessionMasterDetails", new SqlParameter("@ChildNumber", ChildNumber ));
            if (!IsPostBack)
            {    
        if (dsPerformace.Tables[0].Rows.Count > 0)
            {
                rptgraph.DataSource = dsPerformace;
                rptgraph.DataBind();

                gvPerformanceGraph.DataSource = dsPerformace;
                gvPerformanceGraph.DataBind();

                rptgraph.Visible = true;
                gvPerformanceGraph.Visible = true;
                lblNoRecords.Visible = false;
            }

            else
            {
                rptgraph.Visible = false;
                gvPerformanceGraph.Visible = false;
                lblNoRecords.Visible = true;
            }
        }
    }

    protected void gvPerformanceGraph_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        Label lblPercentage = (Label)e.Row.FindControl("lblPercentage");
        if (lblPercentage != null)
        {
            lblPercentage.Width = Unit.Percentage(Double.Parse(lblPercentage.Text));
            lblPercentage.Text = lblPercentage.Text + " %";
        }
    }

}
