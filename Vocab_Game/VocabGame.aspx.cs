using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class vocab_game_demo_vocab_category_selection : System.Web.UI.Page
{
    private int level;
      public int Level
    {
        get { return level; }
        set { level = value; }
    }
    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }
    private static int ChildNumber;
    private static int category;

    protected void Page_Load(object sender, EventArgs e)
    {
        lblerr.Text = "";
        if (Session["childNumber"] == null)
        {
            Response.Redirect("../Login.aspx");
        }
        else
        {
            ChildNumber = Convert.ToInt32(Session["childNumber"]);
        }

        
    }

    protected void btnStartVocabularyGame_Click(object sender, EventArgs e)
    {
        if (radioBtnLstCategory.SelectedValue.ToString() != null)
        {
            category = Convert.ToInt32(radioBtnLstCategory.SelectedValue.ToString());
            Level = category / 10;
            SubLevel = category % 10;

            string strCategory = string.Empty;
            if (category == 11)
            {
                strCategory = "Beginner / Easy";
            }
            else if (category == 12)
            {
                strCategory = "Beginner / Difficult";
            }
            else if (category == 21)
            {
                strCategory = "Intermediate / Easy";
            }
            else if (category == 22)
            {
                strCategory = "Intermediate / Difficult";
            }
            else if (category == 31)
            {
                strCategory = "Advanced / Easy";
            }
            else if (category == 32)
            {
                strCategory = "Advanced / Difficult";
            }
            else if (category == 41)
            {
                strCategory = "More Advanced / Easy";
            }
            else if (category == 42)
            {
                strCategory = "More Advanced / Difficult";
            }

            SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());

            string strsql = "select count(word) from vocabwords_new where level = " + Level + " and [Sub-level] = " + SubLevel;
            DataSet ds1 = SqlHelper.ExecuteDataset(conn, CommandType.Text, strsql);
            if (Convert.ToInt32(ds1.Tables[0].Rows[0][0]) == 0)
            {
                lblerr.Text = "There are no words in " + strCategory + " category, please select another category";

            }
            else
            {
                string str = "select count(word) from vocabwords_new where level = " + Level + " and [Sub-level] = " + SubLevel + " and answer is not null and 'national' ='y' and word not in (select word from vocab_user_easy_word where childnumber = '" + ChildNumber + "' and word_category = '" + category + "')";
                DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str);

                if (Convert.ToInt32(ds.Tables[0].Rows[0][0]) <= 30)
                {
                    string str1 = "delete from vocab_user_easy_word where childnumber = '" + ChildNumber + "' and word_category = '" + category + "'";
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str1);
                    string str2 = "delete from vocab_User_Session_Word where childnumber = '" + ChildNumber + "' and word_category = '" + category + "'";
                    SqlHelper.ExecuteNonQuery(conn, CommandType.Text, str2);
                }
                if (Convert.ToInt32(ds.Tables[0].Rows.Count) == 0)
                {
                    lblerr.Text = "Please select another category as you have attempted all of the words from the " + strCategory + " category.";

                }

                else
                {
                    Response.Redirect("VocabGame_Display_Word.aspx?category=" + radioBtnLstCategory.SelectedValue.ToString());
                }

            }

        }
    }
}
