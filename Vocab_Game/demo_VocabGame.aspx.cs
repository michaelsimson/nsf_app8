using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class vocab_game_demo_vocab_category_selection : System.Web.UI.Page
{
    private int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }
    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }
    
    protected void Page_Load(object sender, EventArgs e)
    {

    }

    protected void btnStartVocabularyGame_Click(object sender, EventArgs e)
    {
        //if (radioBtnLstCategory.SelectedValue.ToString() != null)
        //{
        //    category = Convert.ToInt32(radioBtnLstCategory.SelectedValue.ToString());
        //    Level = category / 10;
        //    SubLevel = category % 10;

        //    SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
        //    string str = "select count(word) from vocabwords_new where level = " + Level + " and [Sub-level] = " + SubLevel;
        //    DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, str);
        //    if (ds.Tables[0].Rows[0][0] == 0)
        //    {
        //        lblerr.Text = "Please select another category as you have attempted all of the words from the chosen category.";

        //    }
        //}
        //else
        //{
            Response.Redirect("demo_vocab_display_word.aspx?category=" + radioBtnLstCategory.SelectedValue.ToString());
        //}
    }
}
