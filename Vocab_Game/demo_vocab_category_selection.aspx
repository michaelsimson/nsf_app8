<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true"
    CodeFile="demo_vocab_category_selection.aspx.cs" Inherits="vocab_game_demo_vocab_category_selection"
    Title="Vocabulary Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table style="margin-left:100px" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="80%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitDemo">Quit Demo</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <b>1. DO NOT</b> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <b>2.</b> Your results for this session will not be saved if you attempt <b>less than
                    10 words.</b>
                <br />
                <b>3.</b> To <b>End a Game</b> click <b>End Session</b> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />
                <p>
                </p>
            </td>
        </tr>
    </table>
    <table style="margin-left:200px" border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="80%">
        <tr>
            <td>
                <font class="black-2"><b>Please select a category to start your Vocabulary preparation.</b></font>
            </td>
        </tr>
        <tr><td><asp:Label ID="lblerr" runat="server" ForeColor="red"></asp:Label></td></tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="radioBtnLstCategory" runat="server" Height="104px" Width="172px"
                    Font-Bold="True">
                    <asp:ListItem Value="11" Selected="True">Beginner / Easy</asp:ListItem>
                                <asp:ListItem Value="12">Beginner / Difficult</asp:ListItem>
                                <asp:ListItem Value="21">Intermediate / Easy</asp:ListItem>
                                <asp:ListItem Value="22">Intermediate / Difficult</asp:ListItem>
                                <asp:ListItem Value="31">Advanced / Easy</asp:ListItem>
                                <asp:ListItem Value="32">Advanced / Difficult</asp:ListItem>
                                 <asp:ListItem Value="41">More Advanced / Easy</asp:ListItem>
                                <asp:ListItem Value="42">More Advanced / Difficult</asp:ListItem>
                </asp:RadioButtonList>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnStartVocabularyGame" runat="server" Text="Start Vocabulary Game" OnClick="btnStartVocabularyGame_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
