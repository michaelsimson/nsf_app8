<%@ Page Language="C#" AutoEventWireup="true" CodeFile="demo_vocab_ViewWordList.aspx.cs"
    Inherits="vocab_game_demo_vocab_ViewWordList" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vocabulary Preparation</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
                <tr>
                    <td align="left" width="50%">
                        <a href="javascript:window.print()">
                            <img src="images/print.gif" /></a>
                    </td>
                    <td align="right">
                        <a href="javascript:self.close()">Close Window</a>
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="center">
                        <asp:GridView ID="gvEndResult" runat="server" AutoGenerateColumns="False" CellPadding="4"
                            Width="80%" ForeColor="#333333" BorderColor="#CC9966" BorderStyle="None" OnDataBound="gvEndResult_DataBound">
                            <Columns>
                                <asp:BoundField HeaderText="Word" DataField="Word" />
                                <asp:BoundField HeaderText="Answered" DataField="Answered" />
                            </Columns>
                            <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                            <RowStyle BackColor="#EFF3FB" />
                            <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                            <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                            <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                            <EditRowStyle BackColor="#2461BF" />
                            <AlternatingRowStyle BackColor="White" />
                        </asp:GridView>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</body>
</html>
