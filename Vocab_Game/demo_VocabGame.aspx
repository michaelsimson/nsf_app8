<%@ Page Language="C#" MasterPageFile="~/NSFInnerMasterPage.master" AutoEventWireup="true"
    CodeFile="demo_VocabGame.aspx.cs" Inherits="vocab_game_demo_vocab_category_selection"
    Title="Vocabulary Preparation" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td>
                <a href="../MainTest.aspx" id="hrefQuitDemo">Quit Demo</a>
                <p>
                </p>
            </td>
        </tr>
        <tr>
            <td>
                <asp:Label ID="lbl1" runat="server" Text = "1. DO NOT " Font-Bold="True" ></asp:Label> use your browser's BACK and FORWARD buttons to navigate. You may
                get inaccurate results.
                <br />
                <asp:Label ID="lbl2a" runat="server" Font-Bold="true" Text="2. "></asp:Label> Your results for this session will not be saved if you attempt <asp:Label ID="lbl2b" runat="server" Text = " less than 10 words." Font-Bold="True" ></asp:Label> <br />
                <asp:Label ID="lbl3a" runat="server" Font-Bold="true" Text="3. "></asp:Label> To <asp:Label ID="lbl3b" runat="server" Font-Bold="true" Text=" End a Game "></asp:Label> click <asp:Label ID="lbl3c" runat="server" Font-Bold="true" Text=" End Session "></asp:Label> button, otherwise your results
                for this session will not be saved. This button will appear once you spell a word.
                <br />
                <p>
                </p>
            </td>
        </tr>
    </table>
    <table border="0" cellpadding="0" cellspacing="0" bgcolor="#ffffff" width="100%">
        <tr>
            <td>
               <asp:Label ID="lblhead" Text="Please select a category to start your Vocabulary preparation." runat="server" Font-Bold="true"></asp:Label>
            
            </td>
        </tr>
        <tr>
            <td>
                <asp:RadioButtonList ID="radioBtnLstCategory" runat="server" Height="104px" Width="172px"
                    Font-Bold="True">
                     <asp:ListItem Value="11" Selected="True">Beginner / Easy</asp:ListItem>
                                <asp:ListItem Value="12">Beginner / Difficult</asp:ListItem>
                                <asp:ListItem Value="21">Intermediate / Easy</asp:ListItem>
                                <asp:ListItem Value="22">Intermediate / Difficult</asp:ListItem>
                                <asp:ListItem Value="31">Advanced / Easy</asp:ListItem>
                                <asp:ListItem Value="32">Advanced / Difficult</asp:ListItem>
                </asp:RadioButtonList>
                <br />
            </td>
        </tr>
        <tr>
            <td>
                <asp:Button ID="btnStartVocabularyGame" runat="server" Text="Start Vocabulary Game" OnClick="btnStartVocabularyGame_Click" />
            </td>
        </tr>
    </table>
</asp:Content>
