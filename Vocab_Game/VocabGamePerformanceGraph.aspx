<%@ Page Language="C#" AutoEventWireup="true" CodeFile="VocabGamePerformanceGraph.aspx.cs"
    Inherits="vocab_game_VocabGamePerformanceGraph" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <title>Vocab Game Performance Graph</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
        <asp:Label Font-Bold="true" Font-Size="X-Large" ID="lblheader" runat="server" Text="Does Practice make You Perfect?"></asp:Label>
            &nbsp;<br /><br />
            <asp:Label Font-Bold="true" Font-Size="Large" ID="lblsubheader" runat="server" Text="Vocabulary Game Performance"></asp:Label>
            &nbsp;<br /><br />
            <asp:Label Font-Bold="true" Font-Size="medium" ID="Label1" runat="server" Text="Tabluar View"></asp:Label>
            
          <asp:GridView ID="gvPerformanceGraph" runat="server" Visible="false" AutoGenerateColumns="False"
                          CellPadding="4" Width="80%" ForeColor="#333333" BorderColor="#CC9966" BorderStyle="None" OnRowDataBound="gvPerformanceGraph_RowDataBound">
                <Columns>
                    <asp:BoundField HeaderText="Word Category" DataField="Word_Category" />
                    <%--<asp:BoundField HeaderText="Percentage (%)" DataField="Percentage" />--%>
                    <asp:TemplateField HeaderText="Percentage (%)">
                        <ItemTemplate>
                            <asp:Label ID="lblPercentage" style="text-align:center" runat="server" Text='<%# Bind("Percentage") %>' ForeColor="White" BackColor="#004080"></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                </Columns>
                <FooterStyle BackColor="#507CD1" ForeColor="White" Font-Bold="True" />
                <RowStyle BackColor="#EFF3FB" />
                <SelectedRowStyle BackColor="#D1DDF1" Font-Bold="True" ForeColor="#333333" />
                <PagerStyle BackColor="#2461BF" ForeColor="White" HorizontalAlign="Center" />
                <HeaderStyle BackColor="#507CD1" Font-Bold="True" ForeColor="White" />
                <EditRowStyle BackColor="#2461BF" />
                <AlternatingRowStyle BackColor="White" />
            </asp:GridView>
            <br />
            
            
            <asp:Label Font-Bold="true" Font-Size="Medium" ID="Label2" runat="server" Text="Cumulative Graphical View"></asp:Label>
            <br /><asp:Repeater ID="rptgraph" runat="server" Visible="false" >
            
            <ItemTemplate>
            <table border="1">
             <tr><td colspan="2">
             Category : <%#DataBinder.Eval(Container.DataItem, "Word_Category")%></td></tr>
      <tr>
     <td colspan="2"> Graphs showing the % success for the number of words </td></tr>
       <tr><td colspan="2">
       <%#DataBinder.Eval(Container.DataItem, "totalwords")%> 
       </td></tr>
       <tr><td><table width="100%"><tr>
       <td style="background-color:Blue ; height:10px ; width:<%#DataBinder.Eval(Container.DataItem,"Percentage")%>px"></td>
       <td>
       <%#DataBinder.Eval(Container.DataItem, "Percentage")%>% 
       </td></tr>
       </table>
       </td></tr>
       </table>
       <br />
       </ItemTemplate>
            </asp:Repeater>
           <br /><br />
            <asp:Label ID="lblNoRecords" runat="server" Text="No Record Found!" Visible="false"></asp:Label><br />
            <br />
            </div>
    </form>
</body>
</html>
