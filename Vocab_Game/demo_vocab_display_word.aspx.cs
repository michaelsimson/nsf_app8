using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;
using System.Data.SqlClient;
using Microsoft.ApplicationBlocks.Data;
using System.Text;

public partial class vocab_game_demo_vocab_display_word : System.Web.UI.Page
{
    private static string strNextClicked;

    private static int endResultCorrect;
    private static int endResultWrong;

    private static int questionCount;
    private int level;
    public int Level
    {
        get { return level; }
        set { level = value; }
    }

    private int subLevel;
    public int SubLevel
    {
        get { return subLevel; }
        set { subLevel = value; }
    }
    public int QuestionCount 
    {
        get { return questionCount; }
        set { questionCount = value; }
    }

    private static DataSet wordsDataSet;
    public DataSet dsDemoWords
    {
        get { return wordsDataSet; }
        set { wordsDataSet = value; }
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            if (Request.QueryString["category"] != null)
            {
                int category = int.Parse(Request.QueryString["category"].ToString());
                Level = category / 10;
                SubLevel = category % 10;

                SqlConnection conn = new SqlConnection(Application["ConnectionString"].ToString());
                dsDemoWords = SqlHelper.ExecuteDataset(conn, CommandType.StoredProcedure, "vusp_GetDemoVocabGameDetails", new SqlParameter("@Level", Level), new SqlParameter("@SubLevel", SubLevel));
                
                if (dsDemoWords.Tables[0].Rows.Count > 0)
                {
                    dsDemoWords.Tables[0].Columns.Add("Answered");
                    frmViewQA.DataSource = dsDemoWords;
                    frmViewQA.DataBind();
                }
                else
                {
                    frmViewQA.Visible = false;
                    panelWrongAnswer.Visible = false;
                    panelCorrectAnswer.Visible = true;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;
                }
                QuestionCount = 1;
                strNextClicked = string.Empty;
                endResultCorrect = 0;
                endResultWrong = 0;
            }
        }

        if (strNextClicked == "NextClicked")
        {
            frmViewQA.DataSource = dsDemoWords;
            frmViewQA.DataBind();
            frmViewQA.PageIndex = QuestionCount;
            strNextClicked = string.Empty;
        }
    }

    protected void btnViewWordList_Click(object sender, EventArgs e)
    {
        Session["WordListDataSet"] = dsDemoWords;
        StringBuilder strScript = new StringBuilder();
        strScript.Append("<script language=JavaScript>");
        strScript.Append("window.open('demo_vocab_ViewWordList.aspx', \"\",\"height=500,width=400,left=0,top=0,toolbar=no,menubar=no,scrollbars=yes\");");
        strScript.Append("</script>");
        RegisterClientScriptBlock("subscribescript", strScript.ToString());
    }

    protected void btnSelectAnotherWordCategory_Click(object sender, EventArgs e)
    {
        Response.Redirect("demo_vocab_category_selection.aspx");
    }

    protected void btnEndSession_Click(object sender, EventArgs e)
    {
        frmViewQA.Visible = false;
        panelControls.Visible = false;
        panelCorrectAnswer.Visible = false;
        panelWrongAnswer.Visible = false;
        panelEndResult.Visible = true;
        lblAttemptedWords.Text = (QuestionCount).ToString(); //+1
        lblEndResultCorrect.Text = endResultCorrect.ToString();
        lblEndResultWrong.Text = endResultWrong.ToString();
    }

    protected void frmViewQA_DataBound(object sender, EventArgs e)
    {
        FormViewRow row = frmViewQA.Row;

        RadioButton rbtnOptionA = (RadioButton)row.FindControl("rbtnOptionA");
        RadioButton rbtnOptionB = (RadioButton)row.FindControl("rbtnOptionB");
        RadioButton rbtnOptionC = (RadioButton)row.FindControl("rbtnOptionC");
        RadioButton rbtnOptionD = (RadioButton)row.FindControl("rbtnOptionD");
        RadioButton rbtnOptionE = (RadioButton)row.FindControl("rbtnOptionE");

        if ((rbtnOptionA != null) && (rbtnOptionB != null) && (rbtnOptionC != null) && (rbtnOptionD != null) && (rbtnOptionE != null))
        {
            if (rbtnOptionA.Text == string.Empty)
            {
                rbtnOptionA.Visible = false;
            }

            if (rbtnOptionB.Text == string.Empty)
            {
                rbtnOptionB.Visible = false;
            }

            if (rbtnOptionC.Text == string.Empty)
            {
                rbtnOptionC.Visible = false;
            }

            if (rbtnOptionD.Text == string.Empty)
            {
                rbtnOptionD.Visible = false;
            }

            if (rbtnOptionE.Text == string.Empty)
            {
                rbtnOptionE.Visible = false;
            }
        }
    }

    protected void frmViewQA_ItemCommand(object sender, FormViewCommandEventArgs e)
    {
        FormViewRow row = frmViewQA.Row;
        string strGivenAnswer = string.Empty;
        string strGivenAnswerOption = string.Empty;

        if (e.CommandName == "SubmitAnswer")
        {
            RadioButton rbtnOptionA = (RadioButton)row.FindControl("rbtnOptionA");
            RadioButton rbtnOptionB = (RadioButton)row.FindControl("rbtnOptionB");
            RadioButton rbtnOptionC = (RadioButton)row.FindControl("rbtnOptionC");
            RadioButton rbtnOptionD = (RadioButton)row.FindControl("rbtnOptionD");
            RadioButton rbtnOptionE = (RadioButton)row.FindControl("rbtnOptionE");

            Label lblWord = (Label)row.FindControl("lblWord");
            Label lblPartofSpeech = (Label)row.FindControl("lblPartofSpeech");
            Label lblAnswer = (Label)row.FindControl("lblAnswer");
            Label lblMeaning = (Label)row.FindControl("lblMeaning");

            if ((rbtnOptionA != null) && (rbtnOptionB != null) && (rbtnOptionC != null) && (rbtnOptionD != null) && (rbtnOptionE != null) && (lblWord != null) && (lblPartofSpeech != null) && (lblAnswer != null) && (lblMeaning != null))
            {
                if (rbtnOptionA.Checked == true)
                {
                    strGivenAnswer = rbtnOptionA.Text;
                    strGivenAnswerOption = "A";
                }
                else if (rbtnOptionB.Checked == true)
                {
                    strGivenAnswer = rbtnOptionB.Text;
                    strGivenAnswerOption = "B";
                }
                else if (rbtnOptionC.Checked == true)
                {
                    strGivenAnswer = rbtnOptionC.Text;
                    strGivenAnswerOption = "C";
                }
                else if (rbtnOptionD.Checked == true)
                {
                    strGivenAnswer = rbtnOptionD.Text;
                    strGivenAnswerOption = "D";
                }
                else if (rbtnOptionE.Checked == true)
                {
                    strGivenAnswer = rbtnOptionE.Text;
                    strGivenAnswerOption = "E";
                }

                frmViewQA.Visible = false;

                if (lblAnswer.Text.Trim().ToUpper() == strGivenAnswerOption)
                {
                    // Given answer is correct
                    dsDemoWords.Tables[0].Rows[QuestionCount-1 ]["Answered"] = "Correct";
                    endResultCorrect++;

                    panelWrongAnswer.Visible = false;
                    panelCorrectAnswer.Visible = true;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;

                    lblCorrectCorrectWord.Text = lblWord.Text;
                    lblCorrectGivenAnswer.Text = strGivenAnswer;
                    lblCorrectMeaning.Text = lblMeaning.Text;
                    lblCorrectAttemptCount.Text = (QuestionCount).ToString();//+1
                }
                else
                { 
                    // Given answer is wrong
                    dsDemoWords.Tables[0].Rows[QuestionCount-1 ]["Answered"] = "Wrong";
                    endResultWrong++;

                    panelWrongAnswer.Visible = true;
                    panelCorrectAnswer.Visible = false;
                    panelControls.Visible = true;
                    panelEndResult.Visible = false;

                    lblWrongCorrectWord.Text = lblWord.Text;
                    lblWrongGivenAnswer.Text = strGivenAnswer;
                    RadioButton rbtnOptionCorrect = (RadioButton)row.FindControl("rbtnOption" + lblAnswer.Text);
                    if (rbtnOptionCorrect != null)
                    {
                        lblWrongCorrectAnswer.Text = rbtnOptionCorrect.Text;
                    }
                    lblWrongMeaning.Text = lblMeaning.Text;
                    lblWrongAttemptCount.Text = (QuestionCount).ToString();//+1
                }
            }

        }
    }

    protected void btnNextWord_Click(object sender, EventArgs e)
    {
        if (QuestionCount < (dsDemoWords.Tables[0].Rows.Count ))
        {
            //QuestionCount++;
            strNextClicked = "NextClicked";

            frmViewQA.DataSource = dsDemoWords;
            frmViewQA.DataBind();
            frmViewQA.PageIndex = QuestionCount;
            QuestionCount++;

            frmViewQA.Visible = true;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = false;
            Page_Load(sender, e);
           }
        else
        {
            frmViewQA.Visible = false;
            panelControls.Visible = false;
            panelCorrectAnswer.Visible = false;
            panelWrongAnswer.Visible = false;
            panelEndResult.Visible = true;

            lblEndResultCorrect.Text = endResultCorrect.ToString();
            lblEndResultWrong.Text = endResultWrong.ToString();
            lblAttemptedWords.Text = (QuestionCount).ToString();//+1
        }
    }
}
