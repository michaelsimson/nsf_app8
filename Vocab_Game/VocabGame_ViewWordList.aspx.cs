using System;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Web.UI.WebControls.WebParts;
using System.Web.UI.HtmlControls;

public partial class vocab_game_demo_vocab_ViewWordList : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["WordListDataSet"] != null)
        {
            gvEndResult.DataSource = (DataSet)Session["WordListDataSet"];
            gvEndResult.DataBind();
        }
    }
    protected void gvEndResult_DataBound(object sender, EventArgs e)
    {
        DataSet dsviewlist = new DataSet();
        dsviewlist = (DataSet)Session["WordListDataSet"];
        for (int i = 0; i < dsviewlist.Tables[0].Rows.Count ; i++)
        {
            if (DBNull.Value.Equals(dsviewlist.Tables[0].Rows[i][11]))
            {
                gvEndResult.Rows[i].Visible = false;
            }
        }
    }
}
