﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="PrepareSchedule_old.aspx.vb" Inherits="PrepareSchedule" title="Prepare Schedule" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
  
    <script language="javascript" type="text/javascript">
            function PopupPicker(ctl) {
                      var PopupWindow = null;
                      settings = 'width=320,height=150,location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
                      PopupWindow = window.open('CalSignHelp.aspx?ID=' + ctl, 'CalSignUp Help', settings);
                      PopupWindow.focus();
                    }
	</script>
	<script  language="javascript" type="text/javascript">
	    function setonScroll(divObj, DgID) {
	        var datagrid = document.getElementById(DgID);
	        var HeaderCells = datagrid.getElementsByTagName('th');
	        var HeaderRow;
	        if (HeaderCells == null || HeaderCells.length == 0) {
	            var AllRows = datagrid.getElementsByTagName('tr');
	            HeaderRow = AllRows[0];
	        }
	        else {
	            HeaderRow = HeaderCells[0].parentNode;
	        }

	        var DivsTopPosition = parseInt(divObj.scrollTop);

	        if (DivsTopPosition > 0) {
	            HeaderRow.style.position = 'absolute';
	            HeaderRow.style.top = (parseInt(DivsTopPosition-2)).toString() + 'px';
	            HeaderRow.style.width = datagrid.style.width;
	            HeaderRow.style.zIndex = '1000';
	        }
	        else {
	            divObj.scrollTop = 0;
	            HeaderRow.style.position = 'relative';
	            HeaderRow.style.top = '0';
	            HeaderRow.style.bottom = '0';
	            HeaderRow.style.zIndex = '0';
	        }
	      
	    }
	
	</script>
  <%--  <div>--%>
        <table border="0" cellpadding ="3" cellspacing = "0" width ="980">
                <tr><td align="left">
                  <asp:hyperlink CssClass="btn_02" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>&nbsp;&nbsp;
                    </td></tr>
        </table>
        <table align="center"><tr><td align="center" style="font-size:16px; font-weight:bold ; font-family:Calibri">
                Prepare Schedule</td>
               </tr>
        </table>
        <table border="0" cellpadding="0" cellspacing="0" align="center">
            <tr>
               <td align="right"  class="btn_02" style="width: 25px">Admin: </td>
                    <td style="width: 25px"><asp:DropDownList ID="ddlRole"  DataTextField="RoleCode" DataValueField ="RoleID" runat="server"></asp:DropDownList></td><td></td>
               <td align="right" class="btn_02" style="width: 35px">EventYear: </td>
                    <td align="left" style="width: 20px"><asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px"  Width="120px"></asp:DropDownList></td><td style="width: 10px"></td>
               <td align="right" class="btn_02" style="width: 25px">Event: </td>
                    <td align="left" style="width: 133px"><asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged"  DataTextField="EventCode"  DataValueField="EventID" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td><td style="width: 10px"></td>
               <td align="right" class="btn_02" style="width: 25px">Phase: </td>
                    <td align="left" style="width: 133px">
                        <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged ="ddlPhase_SelectedIndexChanged"  Width="100px" Height ="20px" runat="server">
                              <asp:ListItem Value="1">One</asp:ListItem>
                                <asp:ListItem Value = "2">Two</asp:ListItem>
                                   <asp:ListItem Value = "3">Three</asp:ListItem>
                          </asp:DropDownList>&nbsp;</td><td style="width: 10px"></td>
               <td align="right" class="btn_02" style="width: 25px">ProductGroup: </td>
                    <td align="left" style="width: 133px"><asp:DropDownList ID="ddlProductGroup" DataTextField="Name" DataValueField="ProductGroupID"  OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="20px" Width="150px"></asp:DropDownList></td><td style="width: 10px"></td>
               <td align="right" class="btn_02" style="width: 25px">Product: </td>
                    <td align="left" style="width: 150px"><asp:DropDownList ID="ddlProduct" DataTextField="Name" DataValueField="ProductID" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false" runat="server" Height="20px" Width="150px"></asp:DropDownList></td><td style="width: 10px"></td>
                    <td><asp:DropDownList ID="ddlVolName"  DataTextField="Name" DataValueField="MemberID" runat="server"  AutoPostBack="True" Height="20px"  Width="150px" Visible="false"></asp:DropDownList></td>
            </tr>
            <tr>
                 <td><asp:Label ID ="lblPrd" runat="server" Visible="false"></asp:Label>
                     <asp:Label ID ="lblPrdGrp" runat="server" Visible="false"></asp:Label></td>
                 <td colspan="2" align="center"><asp:Button ID="btnSaveApprovals" runat="server" Text="SaveApprovals" /></td>
                 <td align="right"><asp:Label ID="lblSortBy" runat="server" Text="Sort By"></asp:Label></td><td><asp:DropDownList ID="ddlWeekDay" runat="server"></asp:DropDownList></td>
                 <td align="center" colspan="3"><asp:Button ID="btnSubmit" runat="server" Text="Submit" />&nbsp;</td>
                 <td align="center" colspan="4"><asp:Label ID="lblError" runat="server" ForeColor="Red"></asp:Label></td>
                 <td><asp:Button ID="btnExport" runat="server" Text="Export"  Enabled ="false" /></td>
            </tr>
        </table>
        <table width="100%">
            <tr class="noscroll"><td align="center"><br />
                <div id="oDiv" runat="server" style="position:relative;top: 0 -2px ;
                 left:0px; height:300px; width:100%; " ><%--overflow-y:auto--%>
                <asp:DataGrid ID="DGSchedule" runat="server" datakeyfield="ID" AutoGenerateColumns="False" AllowPaging="true" PageSize="50" PagerStyle-Position="Bottom"  PagerStyle-HorizontalAlign="Center" PagerStyle-Mode="numericpages" GridLines="Both" CellPadding="0"  Width="100%"  CellSpacing="1" BackColor="Navy" BorderWidth="3px" BorderStyle="Groove" BorderColor="Black" ForeColor="White" Font-Bold="True">
                    <FooterStyle ForeColor="Black" BackColor="Gainsboro" Height="30px"></FooterStyle><%--#CCCCCC"--%>
				    <SelectedItemStyle Font-Bold="True" ForeColor="White" BackColor="#008A8C" ></SelectedItemStyle>
				    <ItemStyle ForeColor="Black" BackColor="#EEEEEE"   Height="28px"></ItemStyle>
				    <HeaderStyle Font-Bold="True" ForeColor="White" BackColor="#a09f9f" Wrap="false" Height="30px"></HeaderStyle>
				    <PagerStyle HorizontalAlign="Right" ForeColor="Black" BackColor="#999999" Mode="NumericPages"></PagerStyle>
			        <EditItemStyle ForeColor="Black" BackColor="#EEEEEE" />
			      <%-- HeaderStyle-BackColor="Gainsboro"--%>
			       <Columns>
                         <asp:BoundColumn DataField="ID" ItemStyle-Width="50px" HeaderText="SignUpID" HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " readonly=true Visible="False" ></asp:BoundColumn>
                           <asp:BoundColumn DataField="CoachName" ItemStyle-Width="150px"  HeaderStyle-Width="150px" HeaderText="CoachName"></asp:BoundColumn>  
                             
                         <asp:BoundColumn DataField="Day1" ItemStyle-Width="50px" HeaderStyle-Width="50px" ItemStyle-BackColor="Gainsboro">
                         </asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                           <asp:BoundColumn DataField="S1" ItemStyle-Width="25px" HeaderStyle-Width="25px"  HeaderText="S" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                             <asp:BoundColumn DataField="P1"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"  HeaderText="P"  ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                               <asp:TemplateColumn HeaderText="A1" ItemStyle-Width="25px"  HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                <%--   <ItemTemplate><asp:TextBox ID="txtAccept1" runat="server" Width="25px"  Text='<%# Bind("A1") %>'></asp:TextBox>&nbsp; 
                                    </ItemTemplate>   --%>                 
                                  <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept1" runat="server"  SelectedValue="<%# Bind('A1') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                         <%--<HeaderStyle Width="180px" />
<ItemStyle Width="180px" />--%>
                                          </asp:TemplateColumn>
                           
                         <asp:BoundColumn DataField="Day2" ItemStyle-Width="50px"  HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                           <asp:BoundColumn DataField="S2" HeaderText="S"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="White"></asp:BoundColumn>
                             <asp:BoundColumn DataField="P2" HeaderText="P"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="White"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="A2"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="White">
                                    <%-- <ItemTemplate><asp:TextBox ID="txtAccept2" runat="server" Width="25px" Text='<%# Bind("A2") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                     </ItemTemplate> --%>
                                     
                                      <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept2" runat="server" SelectedValue="<%# Bind('A2') %>" >
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                                 
                             
                         <asp:BoundColumn DataField="Day3" ItemStyle-Width="50px"  HeaderStyle-Width="50px"  ItemStyle-BackColor="Gainsboro" ></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S3" HeaderText="S"  HeaderStyle-Width="25px"  ItemStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                              <asp:BoundColumn DataField="P3" HeaderText="P" ItemStyle-Width="25px"  HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="A3"   ItemStyle-Width="25px" HeaderStyle-Width="25px"   ItemStyle-BackColor="Gainsboro">
                                      <%--<ItemTemplate><asp:TextBox ID="txtAccept3" runat="server" Width="25px" Text='<%# Bind("A3") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                       </ItemTemplate>  --%>
                                       
                                       
                                        <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept3" runat="server" SelectedValue="<%# Bind('A3') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                                              
                                  
                             
                         <asp:BoundColumn DataField="Day4" ItemStyle-Width="50px"   HeaderStyle-Width="50px"    ItemStyle-BackColor="White"></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                           <asp:BoundColumn DataField="S4" HeaderText="S"   ItemStyle-Width="25px"   HeaderStyle-Width="25px"  ItemStyle-BackColor="White"></asp:BoundColumn>
                             <asp:BoundColumn DataField="P4" HeaderText="P"  ItemStyle-Width="25px"   HeaderStyle-Width="25px"   ItemStyle-BackColor="White"></asp:BoundColumn>
                               <asp:TemplateColumn HeaderText="A4"   ItemStyle-Width="25px"  HeaderStyle-Width="25px"     ItemStyle-BackColor="White" >
                                      <%--    <ItemTemplate><asp:TextBox ID="txtAccept4" runat="server" Width="25px" Text='<%# Bind("A4") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                            </ItemTemplate>  
                                            --%>
                                         <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept4" runat="server" SelectedValue="<%# Bind('A4') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                                               
                                   
                             
                         <asp:BoundColumn DataField="Day5" ItemStyle-Width="50px"   HeaderStyle-Width="50px"  ItemStyle-BackColor="Gainsboro" ></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                           <asp:BoundColumn DataField="S5" HeaderText="S"   ItemStyle-Width="25px"  HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                             <asp:BoundColumn DataField="P5" HeaderText="P"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                <asp:TemplateColumn HeaderText="A5"   ItemStyle-Width="25px"  HeaderStyle-Width="25px"  ItemStyle-BackColor="Gainsboro" >
                                <%--    <ItemTemplate><asp:TextBox ID="txtAccept5" runat="server" Width="25px" Text='<%# Bind("A5") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                      </ItemTemplate>  --%>                  
                                   <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept5" runat="server" SelectedValue="<%# Bind('A5') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                           
                                   
                            
                         <asp:BoundColumn DataField="Day6" ItemStyle-Width="50px"  HeaderStyle-Width="50px" ItemStyle-BackColor="White"></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                           <asp:BoundColumn DataField="S6" HeaderText="S"  ItemStyle-Width="25px"   HeaderStyle-Width="25px"  ItemStyle-BackColor="White"></asp:BoundColumn>
                             <asp:BoundColumn DataField="P6" HeaderText="P"  ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="White"></asp:BoundColumn>
                               <asp:TemplateColumn HeaderText="A6"   ItemStyle-Width="25px"   HeaderStyle-Width="25px"    ItemStyle-BackColor="White" >
                                  <%--<ItemTemplate><asp:TextBox ID="txtAccept6" runat="server" Width="25px" Text='<%# Bind("A6") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                   </ItemTemplate> --%>
                                    <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept6" runat="server"  SelectedValue="<%# Bind('A6') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                                                    
                                
                             
                         <asp:BoundColumn DataField="Day7"  ItemStyle-Width="50px"  HeaderStyle-Width="50px"  ItemStyle-BackColor="Gainsboro" ></asp:BoundColumn><%-- HeaderText='<%#DataBinder.Eval(Container, "DataItem.Day")%>'--%>
                            <asp:BoundColumn DataField="S7"   HeaderText="S"  ItemStyle-Width="25px"  HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                               <asp:BoundColumn DataField="P7"   HeaderText="P" ItemStyle-Width="25px"  HeaderStyle-Width="25px"   ItemStyle-BackColor="Gainsboro"></asp:BoundColumn>
                                 <asp:TemplateColumn HeaderText="A7"  ItemStyle-Width="25px"   HeaderStyle-Width="25px" ItemStyle-BackColor="Gainsboro">
                                    <%--<ItemTemplate><asp:TextBox ID="txtAccept7" runat="server" Width="25px" Text='<%# Bind("A7") %>'></asp:TextBox>&nbsp; <%--Text='<%#<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'- -%>
                                    </ItemTemplate>  --%>                  
                                    <ItemTemplate>
                                          <asp:DropDownList ID="ddlAccept7" runat="server" SelectedValue="<%# Bind('A7') %>">
                                                  <asp:ListItem Value="">Sel</asp:ListItem>
                                                  <asp:ListItem Value="Y">Y</asp:ListItem>
                                          </asp:DropDownList>
                                         </ItemTemplate>
                                          </asp:TemplateColumn>                   
                                 
                                   
                                 
                    </Columns> 
                    <AlternatingItemStyle BackColor="#a09f9f"  ForeColor="Gainsboro"/>
                </asp:DataGrid>
                </div>
                </td></tr>
          </table>
    <%--</div>--%>
<%--Gainsboro--%>
</asp:Content>
 <%--<EditItemTemplate>
         <asp:TextBox ID="txtA1" runat="server" Width="50px"  Text='<%# Bind("A1") %>'></asp:TextBox>
     </EditItemTemplate>--%>