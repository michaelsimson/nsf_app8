<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="PaymentTestPage.aspx.vb" Inherits="PaymentTestPage"  %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<table width="100%" >
				<tr>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center">
						<H1 align="center"><FONT face="Arial" color="#0000ff" size="3"><B>Welcome to North South 
									Foundation Website</B></FONT>&nbsp;<BR>
						</H1>
					</td>
				</tr>
				<tr>
					<td style="height: 150px">
						<P><FONT class=MidFont>Based on feedback, we have made enhancements to the user-interface this year. 
						Your continued feedback is important to make the interface even more user-friendly.  
						Send your comments to <A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com</U></A>&nbsp; 
						Please be as specific as possible so that we can be more effective in our response.</FONT>
											
						</P>
						<P><FONT  class=MidFont>We have added more options to your menu. 
						 You will also be able to register online for workshops when they are available in your area.</FONT>&nbsp;<BR>
						</P>
						<P><FONT  class=MidFont>You need your email and password to access the system. 
						The email must be the same one you used last time to enter this system. 
						If you forgot the email you used last time, please send us an email to 
						<A href="mailto:nsfcontests@gmail.com" target="_blank"><U>nsfcontests@gmail.com </U></A>
						giving your name, address, phone and other details. </FONT>&nbsp;
												
					</td>
				</tr>							
			</table>
			
			<table id="tblLogin" runat="server" style="width: 49%; height: 162px">
				<tr>
					<td style="width: 260px; height: 4px;" contenteditable="true"></td>
					<td class="ContentSubTitle" vAlign="top" noWrap align="center" style="width: 309px; height: 4px;" contenteditable="true">
						<h1>Sign On</h1>
					</td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 260px; height: 37px;">
                        E-mail</td>
					<td style="width: 309px; height: 37px;"><asp:textbox id="txtUserId" runat="server" MaxLength="50" width="300" CssClass="SmallFont"></asp:textbox><br>
							<asp:requiredfieldvalidator id="RequiredFieldValidator1" runat="server" Display="Dynamic" ErrorMessage="Enter Login Id."
							ControlToValidate="txtUserId"></asp:requiredfieldvalidator><asp:regularexpressionvalidator id="RegularExpressionValidator1" runat="server" CssClass="smFont" Display="Dynamic"
							ErrorMessage="Email Address should be a valid e-mail address " ControlToValidate="txtUserId" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator></td>
				</tr>
				<tr>
					<td class="ItemLabel" vAlign="top" noWrap align="right" style="width: 260px">Password
					</td>
					<td style="width: 309px"><asp:textbox id="txtPassword" runat="server" maxlength="30" width="150" CssClass="smFont" TextMode="Password"></asp:textbox><asp:requiredfieldvalidator id="RequiredFieldValidator2" runat="server" ErrorMessage="Enter Password." ControlToValidate="txtPassword"></asp:requiredfieldvalidator></td>
				</tr>
				<tr>
					<td style="width: 260px"></td>
					<td align="center" style="width: 309px"><asp:button id="btnLogin" runat="server" CssClass="FormButton" Text="Login"></asp:button></td>
				</tr>
				
				<tr>
					<td style="width: 260px"></td>
					<td class="smallfont">
				Click here if you
						<asp:hyperlink id="Hyperlink1" runat="server" NavigateUrl="Forgot.aspx">forgot your password?</asp:hyperlink>
				</td></tr>
				<tr>
					<td style="width: 260px"></td>
					<td class="smallfont">
				Click here if you
                        <asp:HyperLink ID="HyperLink2" runat="server" NavigateUrl="ChangeEmail.aspx">changed your email address.</asp:HyperLink>
				</td></tr>
				<tr>
					<td style="width: 260px"></td>
					<td class="smallfont">
					Click here if you are 
				<asp:HyperLink ID="HyperLink3" runat="server" NavigateUrl="createuser.aspx">new to NSF.</asp:HyperLink>
				</td></tr>           
			</table>
			
			
			
			<table id="tblErrorLogin" runat="server" visible=false>
				<tr>
					<td  class="ErrorFont">
						<p>Login Attempt failed. Invalid Email and/or password.  Please try again.
						</p>
					</td>
				</tr>
			</table>
			<table id="tblMissingRole" runat="server" visible=false>
				<tr>
					<td  class="ErrorFont">
						<p>Sorry, we don't yet have any assigned roles for you in our records.   Please consult your contact or if you don't have a contact, send an email to <a href="mailto:volunteer@northsouth.org" target=_blank>volunteer@northsouth.org</a> with all your details.
						</p>
					</td>
				</tr>
			</table>
			<table id="tblMissingProfile" runat="server">
				<tr>
					<td class="ErrorFont">
						<p>Your personal profile is not in our records.  Please press Continue to enter your personal profile
						</p>
						<asp:button id="btnContinue" runat="server" CssClass="FormButton" Text="Continue"></asp:button></td>
				</tr>
			</table>
			<table id="tblMissingToken" runat="server" visible=false>
				<tr>
					<td  class="ErrorFont">
						<p>Error: Missing entry token.
						</p>
					</td>
				</tr>
			</table>
			<!--
			<table id="tblLinks" runat="server" style="width: 659px; height: 1px">
				<tr>
					<td>
						<a id="homeLink" runat="server">  Home</a>
						
					</td>
				</tr>
				</table>-->
</asp:Content>


 

 
 
 