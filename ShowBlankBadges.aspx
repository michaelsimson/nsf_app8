<%@ Page Language="VB" AutoEventWireup="false" CodeFile="ShowBlankBadges.aspx.vb" Inherits="ShowBlankBadges" %>
<%@ Reference Page="~/generatebadges.aspx" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head runat="server">
    <title>Untitled Page</title>
     <script language="javascript" type="text/javascript">
        function printdoc()
        {
            document.getElementById('btnPrint').style.visibility="hidden";
            document.getElementById('hlnkMainMenu').style.visibility="hidden";            
            window.print();
            document.getElementById('btnPrint').style.visibility="visible";
            document.getElementById('hlnkMainMenu').style.visibility="visible";            
            return false;
        }
    </script>
    <style type="text/css"  >
        @page Section1
	        {
	            size:8.5in 11.0in;
	            margin:75.0pt .25in 0in .25in;
	            mso-header-margin:.5in;
	            mso-footer-margin:.5in;
	            mso-paper-source:4;
	         }
            div.Section1
	        {
	            page:Section1;
	        }
    </style>
</head>
<body>
    <form id="form1" runat="server">
        <asp:Panel runat="server" ID="pnlData">        
          <div class="Section1">          
               <table cellspacing="0"  style='border-collapse:collapse;'  cellpadding="0" width="100%"  align="center" border="0" >                                
                    <asp:PlaceHolder runat="server" ID="BadgeHolder"></asp:PlaceHolder>               
    		    </table> 	    		    
    	    </div>	    
		</asp:Panel>		
	  <asp:Panel runat="server" ID="pnlMessage">
		     <table cellspacing="0" style='border-collapse:collapse' cellpadding="0" width="100%"  align="left" border="0" >
            <tr >
                    <td class="Heading" colspan="4" style="height: 19px">
                        <asp:Label runat="server" ID="lblMessage" ></asp:Label>
                    </td>
             </tr>             
        </table>
		</asp:Panel>
	  <div style="page-break-before:always">
		      <asp:HyperLink runat="server" Text="Back to Main Menu" ID="hlnkMainMenu" NavigateUrl="~/volunteerfunctions.aspx" ></asp:HyperLink>&nbsp;&nbsp;&nbsp;
              <input type="button" runat="server"  id="btnPrint" class="FormButton" value="Print" onclick="return printdoc();" />       
        </div>
    </form>
</body>
</html>


 
 
 