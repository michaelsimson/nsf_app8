﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class IndiaLogin : System.Web.UI.Page
{
    string conn;

    protected void Page_Load(object sender, EventArgs e)
    {
        conn = Application["ConnectionString"].ToString();
        try
        {
            if (!Page.IsPostBack)
            {
                ResetSessionVariables();
                string entryToken = Request.QueryString["entry"];
                if (entryToken != null)
                {
                    if (entryToken.ToLower() == "s")
                        Session["entryToken"] = "School";
                    else if (entryToken.ToLower() == "t")
                        Session["entryToken"] = "Teacher";
                    else if (entryToken.ToLower() == "st")
                        Session["entryToken"] = "Student";
                    else if (entryToken.ToLower() == "v")
                        Session["entryToken"] = "Volunteer";
                    lblTitle.Text = Session["entryToken"].ToString();
                }
                else
                {
                    Response.Redirect("Maintest.aspx");
                }
            }
        }
        catch (Exception ex)
        {
            // Response.Write("error :" + ex.ToString());
        }
    }
    protected void btnLogin_Click(object sender, EventArgs e)
    {
        try
        {
            Session["UserID"] = txtUserID.Text;

            if (Session["entryToken"] == null)
            {
                Response.Redirect("Maintest.aspx");
            }
            string entryToken = Session["entryToken"].ToString();
            string strSQL = "";
            switch (entryToken.ToLower())
            {
                case "school":
                    strSQL = "Select count(*) from School where UserID='" + txtUserID.Text + "' and Pwd='" + txtPWD.Text + "'";
                    // Session["LoginID"] = txtUserID.Text;
                    break;
                case "teacher":
                    strSQL = "Select count(*) from Teacher where TeacherId='" + txtUserID.Text + "' and Password='" + txtPWD.Text + "'";
                    break;
                case "student":
                    strSQL = "Select count(*) from Student where StudentId='" + txtUserID.Text + "' and Pwd='" + txtPWD.Text + "'";
                    break;
                case "volunteer":
                    strSQL = "Select count(*) from Login_Master where user_email='" + txtUserID.Text + "' and User_pwd='" + txtPWD.Text + "' and user_role='Volunteer'";
                    break;
            }
            int iCount = Convert.ToInt32(SqlHelper.ExecuteScalar(conn, CommandType.Text, strSQL));
            if (iCount > 0)
            {
                Session["LoginID"] = txtUserID.Text;
                switch (entryToken.ToLower())
                {
                    case "school":
                        Session["SchoolId"] = txtUserID.Text;
                        strSQL = "Select * from School where SchoolId='" + txtUserID.Text + "' and Pwd='" + txtPWD.Text + "'";
                        DataSet dsS = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL);
                        Session["SystemId"] = dsS.Tables[0].Rows[0]["SystemId"];
                        Session["RegionId"] = dsS.Tables[0].Rows[0]["RegionID"];
                        Session["StateId"] = dsS.Tables[0].Rows[0]["StateID"];
                        strSQL = "update School set LastLogin=getdate() where SchoolID=" + Session["SchoolID"];
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSQL);
                        Response.Redirect("SchoolAdminDashboard.aspx");
                        break;
                    case "teacher":
                        strSQL = "Select * from Teacher where TeacherId='" + txtUserID.Text + "' and Password='" + txtPWD.Text + "'";
                        DataSet ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL);
                        Session["SystemId"] = ds.Tables[0].Rows[0]["SystemId"];
                        Session["RegionId"] = ds.Tables[0].Rows[0]["RegionID"];
                        Session["StateId"] = ds.Tables[0].Rows[0]["StateID"];
                        Session["SchoolId"] = ds.Tables[0].Rows[0]["SchoolId"];
                        Session["TeacherId"] = txtUserID.Text;
                        strSQL = "update Teacher set LastLogin=getdate() where TeacherId=" + Session["TeacherId"];
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSQL);
                        Response.Redirect("TeacherDashboard.aspx");
                        break;
                    case "student":
                        strSQL = "Select * from Student where StudentId='" + txtUserID.Text + "' and pwd='" + txtPWD.Text + "'";
                        DataSet dsStud = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL);

                        Session["SystemId"] = dsStud.Tables[0].Rows[0]["SystemId"];
                        Session["RegionId"] = dsStud.Tables[0].Rows[0]["RegionID"];
                        Session["StateId"] = dsStud.Tables[0].Rows[0]["StateID"];
                        Session["SchoolId"] = dsStud.Tables[0].Rows[0]["SchoolId"];
                        Session["StudentId"] = txtUserID.Text;
                        strSQL = "update Student set LastLogin=getdate() where StudentId=" + Session["StudentId"];
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSQL);
                        Response.Redirect("StudentDashboard.aspx");
                        break;
                    case "volunteer":
                        strSQL = "	select IP.AutoMemberID from Indspouse IP inner join Login_Master LM on (IP.Email=LM.User_email) where user_email='" + txtUserID.Text + "' and user_pwd='" + txtPWD.Text + "'";
                        DataSet dsVol = SqlHelper.ExecuteDataset(conn, CommandType.Text, strSQL);


                        Session["MemberId"] = dsVol.Tables[0].Rows[0]["AutoMemberID"].ToString();
                        strSQL = "update Indspouse set LastLogin=getdate() where AutoMemberId=" + Session["MemberId"];
                        SqlHelper.ExecuteNonQuery(conn, CommandType.Text, strSQL);
                        Response.Redirect("VolunteerFunctions.aspx");
                        break;
                }
            }
            else
            {
                lblErr.Text = "Invalid UserName and Password";
            }

        }
        catch (Exception ex) { }

    }

    private void ResetSessionVariables()
    {
        Session.Remove("LoggedIn");
        Session.Remove("LoginChapterID");
        Session.Remove("LoginEmail");
        Session.Remove("LoginEventID");
        Session.Remove("LoginID");
        Session.Remove("LoginRole");
        Session.Remove("SystemId");
        Session.Remove("RegionId");
        Session.Remove("SchoolId");
        Session.Remove("StateId");
        Session.Remove("TeacherId");
        Session.Remove("StudentId");
        Session.Remove("MemberId");

    }
}