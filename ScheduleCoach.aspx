﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ScheduleCoach.aspx.vb" Inherits="ScheduleCoach" title="ScheduleCoach" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
  
    <script language="javascript" type="text/javascript">
      function PopupPicker(ctl, w, h) {
          var PopupWindow = null;
          settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
          PopupWindow = window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl, 'DatePicker', settings);
          PopupWindow.focus();
      }
		</script>
    <div>
    <table border="0" cellpadding ="3" cellspacing = "0" width ="980">
    <tr>
  <td align="left"><asp:hyperlink CssClass="btn_02" id="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:hyperlink>
   &nbsp;&nbsp; <asp:hyperlink CssClass="btn_02" id="HlinkScheduleCoach"   runat="server" NavigateUrl="AssignCoach.aspx">Assign Coach</asp:hyperlink>
    </td> </tr>
        <tr>
    <td align="center">
   
   <table border="0" cellpadding="3" cellspacing="0">
   <tr><td align="center" colspan="6" style="font-size:16px; font-weight:bold ; font-family:Calibri">
       Scheduling of Coaches </td></tr>
    <tr><td align="left">Product Group</td><td align="left"><asp:DropDownList ID="ddlProductGroup" 
                                    DataTextField="Name" DataValueField="ProductGroupID" 
                                    OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged" 
                                    AutoPostBack="true"  runat="server" Height="24px" Width="150px"></asp:DropDownList></td>
       <td></td>
       <td align="left">Product</td>
       <td  align="left">
           <asp:DropDownList ID="ddlProduct" DataTextField="Name" 
               DataValueField="ProductID" AutoPostBack="true" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" Enabled="false" runat="server" Height="24px" 
               Width="130px">
           </asp:DropDownList>
       </td><td></td></tr>
   <tr><td align="left">Coach </td><td align="left"><asp:DropDownList ID="ddlCoachName" DataTextField="Name" 
                                    DataValueField="ID"  runat="server" Height="22px" Width="150px"></asp:DropDownList></td>
       <td></td>
       <td align="left">Event Year</td>
       <td align="left">
           <asp:DropDownList ID="ddlEventYear" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" AutoPostBack="true" runat="server" Height="24px" Width="130px">
           </asp:DropDownList>
       </td><td></td></tr>
      
      
       <tr><td align="left">Level</td><td align="left">
           <asp:DropDownList ID="ddlLevel" runat="server" Width="150" Height ="24">
               <asp:ListItem Value="0" Selected="True">Beginner</asp:ListItem>
               <asp:ListItem Value="1">Intermediate</asp:ListItem>
               <asp:ListItem Value="2">Advanced</asp:ListItem>
           </asp:DropDownList> </td>
       <td></td>
       <td align="left" > Phase</td>
       <td align="left">
                  <asp:DropDownList ID="ddlPhase" AutoPostBack="true" OnSelectedIndexChanged ="ddlPhase_SelectedIndexChanged" Width="130" Height ="24" runat="server">
               <asp:ListItem Selected="True" Value="1">One</asp:ListItem>
               <asp:ListItem Value = "2">Two</asp:ListItem>
           </asp:DropDownList> </td><td> </td></tr>
           </tr>
      
      
       <tr><td align="left">Session #</td><td align="left">
           <asp:DropDownList ID="ddlSession" runat="server" Width="150" Height ="24">
               <asp:ListItem Value="1" Selected="True">1</asp:ListItem>
               <asp:ListItem Value="2">2</asp:ListItem>
               <asp:ListItem Value="3">3</asp:ListItem>
               <asp:ListItem Value="4">4</asp:ListItem>
               <asp:ListItem Value="5">5</asp:ListItem>               
               <asp:ListItem Value="6">6</asp:ListItem>
               <asp:ListItem Value="7">7</asp:ListItem>
           </asp:DropDownList> </td>
       <td></td>
       <td align="left" > </td>
       <td align="left"></td> </tr> 
       <tr><td align="left"> Coaching Day(s)/Times </td><td colspan="2" align="left">
           <asp:TextBox ID="txtCoachingTime" TextMode="MultiLine" runat="server" Height="75px" Width="200px"></asp:TextBox>
        </td>
        <td align="left" colspan="3" style="font-size:14px; color:Red; vertical-align:middle; font-family:Calibri; font-weight:bold; margin-left:50px"> 
            * Please provide <b>EST</b> Timing</td>
      </tr>
      <tr><td align="left">Minimum Capacity </td><td align="left">
          <asp:DropDownList ID="ddlMinCap" runat="server" Height="24" Width="50"></asp:DropDownList> </td><td></td><td align="left">
          Maximum Capacity </td><td align="left">
          <asp:DropDownList ID="ddlCapacity" runat="server" Height="24" Width="50"></asp:DropDownList> </td>
       <td></td></tr>
       <tr><td align="left">Start Date  </td><td align="left"><asp:TextBox ID="txtStartDate"   runat="server" ></asp:TextBox> </td>
       <td align="left"> <a href="javascript:PopupPicker('txtStartDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td>
       <td align="left">End Date</td>
       <td><asp:TextBox ID="txtEndDate" runat="server"></asp:TextBox> </td>
       <td align="left"> <a href="javascript:PopupPicker('txtEndDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td>
       </tr>
       <tr><td align="center" colspan="6"> 
           <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label></td> </tr>
        <tr><td align="center" colspan="6">
            <asp:Button ID="btnSubmit" runat="server" Text="Submit" />
            <asp:Label ID="lblPrd" runat="server" Visible="false"></asp:Label>
             <asp:Label ID="lblPrdGrp" runat="server" Visible="false"></asp:Label>
            </td></tr>
        <tr><td align="center" colspan="6">
            <asp:Label ID="lblError" runat="server" ForeColor = "Red"></asp:Label></td></tr>
   </table>
   </td></tr>
   <tr><td align="center">
       <br />
       <asp:DataGrid ID="DGCoach" runat="server" 
           datakeyfield="CoachCalID" AutoGenerateColumns="False"  
					Height="14px" GridLines="Horizontal" CellPadding="2" BackColor="Navy" BorderWidth="3px" BorderStyle="Double"
					BorderColor="#336666" ForeColor="White" Font-Bold="True">
             <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>
					<HeaderStyle Font-Size="X-Small" ForeColor="White" Font-Names="Verdana" Font-Bold="True" ></HeaderStyle>
					<FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                <Columns>
                         <asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel" EditText="Edit" ></asp:EditCommandColumn> 
                         <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete" ></asp:buttoncolumn>
                        
                         <asp:BoundColumn DataField="CoachCalID" ItemStyle-Width="50px"   HeaderText="Coach ID"  HeaderStyle-ForeColor="White" HeaderStyle-Font-Bold="true " readonly=true Visible="true" ></asp:BoundColumn>
                         <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Volunteer Name">
                          <HEADERSTYLE Width="130px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >                                     
                                        <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Name")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>
 <asp:DropDownList id="ddlMember" runat="server" DataTextField="Name" DataValueField="MemberId" OnPreRender="Member_PreRender" OnSelectedIndexChanged="Member_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>   
                                      </EditItemTemplate>                                                                  
                         </asp:TemplateColumn > 
                         <asp:TemplateColumn HeaderText="Event Year">
                          <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                         <ItemTemplate >
                         <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                         </ItemTemplate>  
                            <EditItemTemplate>
                            <asp:DropDownList id="ddlEventYear" runat="server" DataTextField="EventYear" DataValueField="EventYear" 
                                        OnPreRender="EventYear_PreRender" AutoPostBack="false">
                            <asp:ListItem Value="2010" Selected="True">2010</asp:ListItem> 
                            <asp:ListItem Value="2011">2011</asp:ListItem>
                            <asp:ListItem Value="2012">2012</asp:ListItem>
                             <asp:ListItem Value="2013">2013</asp:ListItem>
                            </asp:DropDownList>                                       
                            </EditItemTemplate>                                 
                            </asp:TemplateColumn >
                             <asp:TemplateColumn HeaderText="Min Cap">
                              <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                              <ItemTemplate >
                                <asp:Label ID="lblMinCap" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MinCap")%>'></asp:Label>
                              </ItemTemplate>      
                              <EditItemTemplate>
                                     <asp:DropDownList id="ddlDGMinCap" runat="server"  OnPreRender="ddlDGMinCap_PreRender" OnSelectedIndexChanged="ddlDGMinCap_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>    
                                </EditItemTemplate>                                                               
                                </asp:TemplateColumn > 
                                 <asp:TemplateColumn HeaderText="Max Cap">
                                  <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblCapacity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MaxCap")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>                                  
                                        <asp:DropDownList id="ddlDGCapacity" runat="server"  OnPreRender="ddlDGCapacity_PreRender" OnSelectedIndexChanged="ddlDGCapacity_SelectedIndexChanged" AutoPostBack="true"></asp:DropDownList>    
                                </EditItemTemplate>                                                               
                                </asp:TemplateColumn > 
                                   
                                  <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Level" >
                                   <HEADERSTYLE Width="100px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblLevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                      </ItemTemplate>         
                                       <EditItemTemplate>
                                      <asp:DropDownList id="ddlLevel" OnPreRender="ddlLevel_PreRender" runat="server" AutoPostBack="false">
                                         <asp:ListItem Value="0" Selected="True">Beginner</asp:ListItem>
               <asp:ListItem Value="1">Intermediate</asp:ListItem>
               <asp:ListItem Value="2">Advanced</asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn >  
                                
                                 <asp:TemplateColumn  ItemStyle-HorizontalAlign="Left"  HeaderText="Session" >
                                   <HEADERSTYLE Width="100px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblSessionNo" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SessionNo")%>'></asp:Label>
                                      </ItemTemplate>         
                                       <EditItemTemplate>
                                      <asp:DropDownList id="ddlSessionNo" OnPreRender="ddlSessionNo_PreRender" runat="server" AutoPostBack="false">
                                         <asp:ListItem Value="1">1</asp:ListItem>
                                       <asp:ListItem Value="2">2</asp:ListItem>
                                       <asp:ListItem Value="3">3</asp:ListItem>
                                       <asp:ListItem Value="4">4</asp:ListItem>
                                       <asp:ListItem Value="5">5</asp:ListItem>               
                                       <asp:ListItem Value="6">6</asp:ListItem>
                                       <asp:ListItem Value="7">7</asp:ListItem>
                                        </asp:DropDownList>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn >  
                                
                                 <asp:TemplateColumn HeaderText="ProductGroupCode" Visible="false" >
                                      <ItemTemplate >
                                        <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                        <asp:HiddenField ID="hfProductGroupId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductGroupId")%>' />
                                      </ItemTemplate>  
                                       <%--<EditItemTemplate>
                                        <asp:DropDownList id="ddlProductGroup" runat="server" DataTextField="Name" DataValueField="ProductGroupId" Enabled="false"
                                       OnPreRender="ProductGroup_PreRender">  
                                      </asp:DropDownList> 
                                      </EditItemTemplate> --%>                                
                               </asp:TemplateColumn >
                                <asp:TemplateColumn HeaderText="Product">
                                 <HEADERSTYLE Width="80px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                        <asp:HiddenField ID="hfProductId" runat="server" Value='<%#DataBinder.Eval(Container, "DataItem.ProductId")%>' />
                                      </ItemTemplate>  
                                       <EditItemTemplate>
                                        <asp:DropDownList id="ddlProduct" runat="server" DataTextField="Name" DataValueField="ProductId" OnPreRender="Product_PreRender" OnSelectedIndexChanged="Product_SelectedIndexChanged" AutoPostBack="true">
                                       </asp:DropDownList> 
                                      </EditItemTemplate>                                 
                               </asp:TemplateColumn > 
                                <asp:TemplateColumn  ItemStyle-HorizontalAlign="left" HeaderText="CoachDayTime">
                                 <HEADERSTYLE  Width="400px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblCoachDayTime" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CoachDayTime")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>
                                      <asp:TextBox ID="txtCoachDayTime" TextMode="MultiLine" Height="75" Width="150" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.CoachDayTime")%>'></asp:TextBox>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn > 
                                 <asp:TemplateColumn HeaderText="StartDate">
                                  <HEADERSTYLE Width="80px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblStartDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartDate","{0:d}")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>
                                      <asp:TextBox ID="txtStartDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.StartDate","{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>                                                                  
                                </asp:TemplateColumn > 
                                 <asp:TemplateColumn HeaderText="EndDate">
                                  <HEADERSTYLE Width="80px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblEndDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndDate","{0:d}")%>'></asp:Label>
                                      </ItemTemplate>      
                                      <EditItemTemplate>
                                      <asp:TextBox ID="txtEndDate" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EndDate","{0:d}")%>'></asp:TextBox>
                                      </EditItemTemplate>  
                                      </asp:TemplateColumn> 
                                 <asp:TemplateColumn HeaderText="City">
                                  <HEADERSTYLE Width="80px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblCity" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.City")%>'></asp:Label>
                                      </ItemTemplate>
                                 </asp:TemplateColumn>  
                                  <asp:TemplateColumn HeaderText="State">
                                   <HEADERSTYLE Width="50px" Font-Bold="True" ForeColor="White"></HEADERSTYLE>
                                      <ItemTemplate >
                                        <asp:Label ID="lblState" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.State")%>'></asp:Label>
                                      </ItemTemplate>                                                                                          
                                </asp:TemplateColumn >  
                                 <asp:TemplateColumn HeaderText="Count" Visible="false" >
                                      <ItemTemplate >
                                        <asp:Label ID="lblCount" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Count")%>'></asp:Label>
                                         </ItemTemplate>                                                                        
                               </asp:TemplateColumn >                          
                </Columns> 
         
           	<PAGERSTYLE ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PAGERSTYLE>
             <AlternatingItemStyle BackColor="LightBlue" />         
       </asp:DataGrid>
   </td></tr>
   </table>
    
    
    </div>

</asp:Content>
