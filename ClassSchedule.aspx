﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="ClassSchedule.aspx.cs" Inherits="ClassSchedule" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <script>

        function ConfirmToUpdate(sender) {

            var btnlabel = sender.value;

            if (btnlabel.localeCompare("Close") == 0) {

                if (confirm("Do you really want to close?")) {

                    return true;
                }
                else {

                    return false;

                }
            }
            else if (btnlabel.localeCompare("Open") == 0) {
                if (confirm("Do you really want to Open?")) {

                    return true;
                }
                else {

                    return false;

                }
            }







        }






    </script>


    <table id="tblLogin" border="0" cellpadding="3" cellspacing="0" width="100%" runat="server" align="center" style="margin-left: 10px" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>
                <br />
                <br />
            </td>
        </tr>
        <tr>

            <td class="ContentSubTitle" valign="top" align="center">
                <h2>Class Calendar</h2>
            </td>
        </tr>

        <tr>
            <td align="center">

                <asp:RadioButtonList ID="rdoCalendar" runat="server" AutoPostBack="True" OnSelectedIndexChanged="rdoCalendar_SelectedIndexChanged">
                    <asp:ListItem Value="0">Close a Current Class</asp:ListItem>
                    <asp:ListItem Value="1">Open a Closed Class</asp:ListItem>
                    <asp:ListItem Value="2">Open a New Class</asp:ListItem>
                </asp:RadioButtonList>
            </td>

        </tr>
        <tr>
            <td colspan="2" align="center">

                <br />
                <asp:Label ID="lblClassSchedule" runat="server" CssClass="ContentSubTitle" Font-Size="Large"></asp:Label><br />
                <br />
                <table width="100%" style="margin-left: auto; margin-right: auto; font-weight: bold" runat="server" id="tblCoach" visible="false">

                    <tr class="ContentSubTitle" style="background-color: Honeydew;">
                        <td>Year</td>
                        <td>
                            <asp:DropDownList ID="ddlYear" runat="server" AutoPostBack="True" OnSelectedIndexChanged="ddlYear_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Event</td>
                        <td>
                            <asp:DropDownList ID="ddlEvent" runat="server">
                            </asp:DropDownList>
                        </td>
                        <td>Semester</td>
                        <td>
                            <asp:DropDownList ID="ddlPhase" runat="server" Width="50px" AutoPostBack="true" OnSelectedIndexChanged="ddlPhase_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>ProductGroup</td>
                        <td>
                            <asp:DropDownList ID="ddlProductGroup" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductGroupId" Width="125px" OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>
                        <td>Product</td>
                        <td>
                            <asp:DropDownList ID="ddlProduct" runat="server" AutoPostBack="True" DataTextField="Name" DataValueField="ProductId" Width="110px" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged">
                            </asp:DropDownList>
                        </td>

                        <td id="tdCoach" runat="server">CoachName</td>
                        <td>
                            <asp:DropDownList ID="ddlCoach" runat="server" DataTextField="CoachName" DataValueField="MemberId" Width="200px">
                            </asp:DropDownList>
                        </td>

                    </tr>


                    <tr>
                        <td colspan="12">
                            <center>

                                <asp:Button ID="btnSubmit" runat="server" Text="Submit" Style="text-align: center; height: 26px;" OnClick="btnSubmit_Click" />

                                <br />

                            </center>
                        </td>
                    </tr>

                    <tr class="ContentSubTitle">
                        <td colspan="12">
                            <asp:HiddenField ID="hyear" runat="server" />
                            <asp:HiddenField ID="hproductgroupname" runat="server" />
                            <asp:HiddenField ID="hproductname" runat="server" />
                            <asp:HiddenField ID="hphase" runat="server" />

                            <div align="center">
                                <asp:Label ID="lblTableName" runat="server" ForeColor="Black" Visible="true" Font-Bold="true"></asp:Label><br />
                                <asp:Label ID="lblErrTable" runat="server" ForeColor="Red" Visible="true" Font-Bold="true"></asp:Label>
                                <asp:Label ID="lblErr" runat="server" ForeColor="Red" Visible="true" Font-Bold="true"></asp:Label><br />
                                <asp:Label ID="lblsuccTable" runat="server" ForeColor="Blue" Visible="true" Font-Bold="true"></asp:Label>
                            </div>
                            <div align="center">
                                <asp:GridView ID="GridViewCoachClassTable" runat="server" EnableModelValidation="True" AutoGenerateColumns="False" AllowPaging="True" OnPageIndexChanging="GridViewCoachClassTable_PageIndexChanging" OnRowEditing="GridViewCoachClassTable_RowEditing" OnRowUpdating="GridViewCoachClassTable_RowUpdating" OnRowDataBound="GridViewCoachClassTable_RowDataBound">
                                    <Columns>
                                        <%--<asp:ButtonField CommandName="Cancel" HeaderText="Action" ShowHeader="True" Text="Close" ButtonType="Button" />--%>
                                        <asp:TemplateField HeaderText="Action">

                                            <ItemTemplate>
                                                <asp:HiddenField ID="SignupID" Value='<%# Bind("SignupID")%>' runat="server" />
                                                <%-- <asp:HiddenField ID="hlevel" Value='<%# Bind("Level")%>' runat="server" />
                                             <asp:HiddenField ID="hsessionno" Value='<%# Bind("SessionNo")%>' runat="server" />--%>
                                                <asp:HiddenField ID="MemberID" Value='<%# Bind("MemberID")%>' runat="server" />
                                                <asp:HiddenField ID="StartTime" Value='<%# Bind("Begin")%>' runat="server" />
                                                <asp:HiddenField ID="EndTime" Value='<%# Bind("End")%>' runat="server" />
                                                <asp:Button ID="btnAction" Text="" runat="server" OnClientClick="return ConfirmToUpdate(this)" CommandName="Update" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Year">
                                            <ItemTemplate>

                                                <asp:Label ID="Label1" runat="server" Text='<%#Eval("EventYear") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event">
                                            <ItemTemplate>
                                                <asp:Label ID="Label2" runat="server" Text='<%#Eval("EventCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MemberID">
                                            <ItemTemplate>

                                                <asp:Label ID="Label6" runat="server" Text='<%#Eval("MemberID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>

                                                <asp:Label ID="Label7" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product Group">
                                            <ItemTemplate>
                                                <asp:Label ID="Label3" runat="server" Text='<%#Eval("ProductGroupCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product">
                                            <ItemTemplate>
                                                <asp:Label ID="Label4" runat="server" Text='<%#Eval("ProductCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Semester">
                                            <ItemTemplate>
                                                <asp:Label ID="Label5" runat="server" Text='<%#Eval("Semester") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Level">
                                            <ItemTemplate>
                                                <asp:Label ID="lblLevel" runat="server" Text='<%#Eval("Level") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Session">
                                            <ItemTemplate>
                                                <asp:Label ID="lblSession" runat="server" Text='<%#Eval("SessionNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Accepted">
                                            <ItemTemplate>
                                                <asp:Label ID="Label8" runat="server" Text='<%#Eval("Accepted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Day">
                                            <ItemTemplate>
                                                <asp:Label ID="lblDays" runat="server" Visible="true" Text='<%#Eval("Days") %>'></asp:Label>
                                                <asp:Label ID="lblDay" runat="server" Visible="false" Text='<%#Eval("Day") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblTime" runat="server" Text='<%#Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lblsdate" runat="server" Text='<%#Eval("Begin") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Time">
                                            <ItemTemplate>
                                                <asp:Label ID="lbledate" runat="server" Text='<%#Eval("End") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VRoom">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvroom" runat="server" Text='<%#Eval("VRoom") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UserID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="Label12" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pwd" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="Label13" runat="server" Text='<%#Eval("Pwd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UpFlag">
                                            <ItemTemplate>
                                                <asp:Label ID="Label14" runat="server" Text='<%#Eval("UpFlag") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                                <br />
                                <div align="center">
                                    <asp:Label ID="lblTableName2" runat="server" ForeColor="Black" Visible="true" Font-Bold="true"></asp:Label>
                                </div>
                                <div align="center">
                                    <asp:Label ID="lblErrTable2" runat="server" ForeColor="Red" Visible="true" Font-Bold="true"></asp:Label>
                                </div>
                                <br />
                                <asp:GridView ID="GVScheduledClasses" runat="server" EnableModelValidation="True" AutoGenerateColumns="False">
                                    <Columns>
                                        <%--<asp:ButtonField CommandName="Cancel" HeaderText="Action" ShowHeader="True" Text="Close" ButtonType="Button" />--%>
                                        <asp:TemplateField HeaderText="Action">

                                            <ItemTemplate>
                                                <asp:HiddenField ID="HiddenField1" Value='<%# Bind("SignupID")%>' runat="server" />
                                                <%-- <asp:HiddenField ID="hlevel" Value='<%# Bind("Level")%>' runat="server" />
                                             <asp:HiddenField ID="hsessionno" Value='<%# Bind("SessionNo")%>' runat="server" />--%>
                                                <asp:HiddenField ID="HiddenField2" Value='<%# Bind("MemberID")%>' runat="server" />
                                                <asp:Button ID="Button1" Text="" runat="server" CommandName="Update" Visible="false" />
                                            </ItemTemplate>
                                        </asp:TemplateField>

                                        <asp:TemplateField HeaderText="Year">
                                            <ItemTemplate>

                                                <asp:Label ID="lblYear" runat="server" Text='<%#Eval("EventYear") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Event">
                                            <ItemTemplate>
                                                <asp:Label ID="Label16" runat="server" Text='<%#Eval("EventCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="MemberID">
                                            <ItemTemplate>

                                                <asp:Label ID="Label17" runat="server" Text='<%#Eval("MemberID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Name">
                                            <ItemTemplate>

                                                <asp:Label ID="Label18" runat="server" Text='<%#Eval("Name") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product Group">
                                            <ItemTemplate>
                                                <asp:Label ID="Label19" runat="server" Text='<%#Eval("ProductGroupCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Product">
                                            <ItemTemplate>
                                                <asp:Label ID="Label20" runat="server" Text='<%#Eval("ProductCode") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Semester">
                                            <ItemTemplate>
                                                <asp:Label ID="Label21" runat="server" Text='<%#Eval("Semester") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Level">
                                            <ItemTemplate>
                                                <asp:Label ID="Label22" runat="server" Text='<%#Eval("Level") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Session">
                                            <ItemTemplate>
                                                <asp:Label ID="Label23" runat="server" Text='<%#Eval("SessionNo") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Accepted">
                                            <ItemTemplate>
                                                <asp:Label ID="Label24" runat="server" Text='<%#Eval("Accepted") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Day">
                                            <ItemTemplate>
                                                <asp:Label ID="Label9" runat="server" Visible="true" Text='<%#Eval("Days") %>'></asp:Label>
                                                <asp:Label ID="Label25" runat="server" Visible="false" Text='<%#Eval("Day") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Time">
                                            <ItemTemplate>
                                                <asp:Label ID="sclblTime" runat="server" Text='<%#Eval("Time") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Start Time">
                                            <ItemTemplate>
                                                <asp:Label ID="Label26" runat="server" Text='<%#Eval("Begin") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="End Time">
                                            <ItemTemplate>
                                                <asp:Label ID="Label27" runat="server" Text='<%#Eval("End") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="VRoom">
                                            <ItemTemplate>
                                                <asp:Label ID="lblvroom" runat="server" Text='<%#Eval("VRoom") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UserID" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="Label28" runat="server" Text='<%#Eval("UserID") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="Pwd" Visible="false">
                                            <ItemTemplate>
                                                <asp:Label ID="Label29" runat="server" Text='<%#Eval("Pwd") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                        <asp:TemplateField HeaderText="UpFlag">
                                            <ItemTemplate>
                                                <asp:Label ID="Label30" runat="server" Text='<%#Eval("UpFlag") %>'></asp:Label>
                                            </ItemTemplate>
                                        </asp:TemplateField>
                                    </Columns>
                                    <PagerStyle HorizontalAlign="Center" />
                                </asp:GridView>
                            </div>
                        </td>

                    </tr>

                </table>
            </td>

        </tr>



    </table>
</asp:Content>
