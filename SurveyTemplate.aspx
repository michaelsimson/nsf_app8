﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="SurveyTemplate.aspx.cs" MasterPageFile="~/NSFMasterPage.master" Inherits="SurveyTemplate" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div style="text-align: left">
        <script src="Scripts/jquery-1.9.1.js"></script>

        <script language="javascript" type="text/javascript">
            function buildQuestHtml(updateFlag) {
                var strHtml = "";
                var rowCount = $("#txtRowCount").val();

                strHtml += "<div style='width:300px; margin-left:auto; margin-right:auto;'>";
                strHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                for (var i = 0; i < rowCount; i++) {
                    strHtml += "<div style='float:left; width:150px; text-align:left;'>Row Title</div>";

                    //var choiceFormat = document.getElementById('<%=hdnChoiceFormat%>').value;
                    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //if (choiceFormat == "text") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Drop down") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Radio") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Checkbox") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //}
                    strHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                }
                if (updateFlag == 0) {
                    strHtml += "<div><input type='button' id='btnQuestionRowSave' value='Save' /></div>";
                } else {
                    strHtml += "<div><input type='button' id='btnQuestionRowSave' value='Update' /></div>";
                }
                strHtml += "</div>";
                $("#dvQuestionRows").html(strHtml);
            }

            function buildQuestColumnHtml(updateFlag) {
                var strHtml = "";
                var rowCount = $("#txtColumnCount").val();

                strHtml += "<div style='width:300px; margin-left:auto; margin-right:auto;'>";
                strHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                for (var i = 0; i < rowCount; i++) {
                    strHtml += "<div style='float:left; width:150px; text-align:left;'>Column Title</div>";


                    strHtml += "<div style='float:left;'><input type='text' id='txtColumnTitle" + i + "' /></div>";
                    //if (choiceFormat == "text") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Drop down") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Radio") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //} else if (choiceFormat == "Checkbox") {
                    //    strHtml += "<div style='float:left;'><input type='text' id='txtRowTitle" + i + "' /></div>";
                    //}
                    strHtml += "<div style='clear:both; margin-bottom:20px;'></div>";
                }
                if (updateFlag == 0) {
                    strHtml += "<div><input type='button' id='btnSaveColumns' value='Save' /></div>";
                } else {
                    strHtml += "<div><input type='button' id='btnSaveColumns' value='Update' /></div>";
                }
                strHtml += "</div>";
                $("#dvQuestColumns").html(strHtml);
            }

            function postQuestionRows(surveyID, questionID, title, userID, mode, questRowID) {

                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/PostQuestionRows",
                    data: JSON.stringify({ objSurvey: { SurveyID: surveyID, QuestionID: questionID, QuestionRowTitle: title, CreatedBy: userID, Mode: mode, QuestionRowID: questRowID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        if (JSON.stringify(data.d) == 1) {
                            $("#spnErrMsg").text("Saved successfully");
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }
            function postQuestionColumns(surveyID, questionID, title, userID, mode, questColID) {
                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/PostQuestionColumns",
                    data: JSON.stringify({ objSurvey: { SurveyID: surveyID, QuestionID: questionID, QuestionColumnTitle: title, CreatedBy: userID, Mode: mode, QuestionColumnID: questColID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        if (JSON.stringify(data.d) == 1) {
                            $("#spnErrMsg").text("Saved successfully");
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function getQuestionRows() {
                var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/GetQuestionRows",
                    data: JSON.stringify({ objSurvey: { QuestionID: questionID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {
                            $("#txtRowCount").val(data.d.length);
                            buildQuestHtml(1);
                        }
                        var i = 0;
                        $.each(data.d, function (index, value) {



                            $("#txtRowTitle" + i + "").val(value.QuestionRowTitle);
                            $("#txtRowTitle" + i + "").attr("QuestRowID", value.QuestionRowID);
                            i = i + 1;
                        });

                        //
                        getQuestionColumns();
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function getQuestionColumns() {
                var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/GetQuestionColumns",
                    data: JSON.stringify({ objSurvey: { QuestionID: questionID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {

                        if (JSON.stringify(data.d.length) > 0) {
                            $("#txtColumnCount").val(data.d.length);
                            buildQuestColumnHtml(1);
                        }
                        var i = 0;

                        $.each(data.d, function (index, value) {

                            $("#txtColumnTitle" + i + "").attr("QuestColID", value.QuestionColumnID);
                            $("#txtColumnTitle" + i + "").val(value.QuestionColumnTitle);
                            i = i + 1;
                        });

                        //

                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            function DeleteQuestions() {
                var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/DeleteQuestions",
                    data: JSON.stringify({ objSurvey: { QuestionID: questionID } }),
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        alert(JSON.stringify(data.d));
                        if (JSON.stringify(data.d) == 1) {
                            $("#spnErrMsg").text("Deleted successfully");
                        }
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            $(document).on("keyup", "#txtRowCount", function (e) {

                buildQuestHtml(0);
            });
            $(document).on("keyup", "#txtColumnCount", function (e) {

                buildQuestColumnHtml(0);
            });

            $(document).on("click", "#btnQuestionRowSave", function (e) {

                var rowCount = $("#txtRowCount").val();
                var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                var userID = document.getElementById('<%=hdnUserID.ClientID%>').value;

                var mode = 1;
                if ($(this).val() == "Save") {
                    mode = 1;
                } else {
                    mode = 2;
                }
                for (var i = 0; i < rowCount; i++) {

                    var rowTitle = $("#txtRowTitle" + i + "").val();
                    var questRowID = $("#txtRowTitle" + i + "").attr("QuestRowID");
                    postQuestionRows(surveyID, questionID, rowTitle, userID, mode, questRowID);
                }

            });
            $(document).on("click", "#btnSaveColumns", function (e) {

                var rowCount = $("#txtColumnCount").val();
                var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                var userID = document.getElementById('<%=hdnUserID.ClientID%>').value;
                var mode = 1;
                if ($(this).val() == "Save") {
                    mode = 1;
                } else {
                    mode = 1;
                }
                for (var i = 0; i < rowCount; i++) {

                    var columnTitle = $("#txtColumnTitle" + i + "").val();
                    var questColID = $("#txtColumnTitle" + i + "").attr("QuestColID");
                    postQuestionColumns(surveyID, questionID, columnTitle, userID, mode, questColID);
                }

            });
            function confirmDeleteQns() {
                if (confirm("Are you sure want to delete this question?")) {
                    var surveyID = document.getElementById('<%=hdnSurveyID.ClientID%>').value;
                    var questionID = document.getElementById('<%=HdnQuestionID.ClientID%>').value;
                    DeleteQuestions();
                }
            }



            function LoadSurveysAPI() {

                $.ajax({
                    type: "POST",
                    url: "SurveyTemplate.aspx/testSurvey",
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    async: true,
                    cache: false,
                    success: function (data) {
                        alert(JSON.stringify(data));
                    },
                    failure: function (response) {
                        alert(response.d);
                    }
                });
            }

            $(function (e) {
                LoadSurveysAPI();
            });
        </script>
    </div>
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:LinkButton ID="LinkButton1" CssClass="btn_02" PostBackUrl="~/SurveyList.aspx" runat="server">Back to Survey List</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Survey Template
             <br />
        <br />
    </div>
    <br />
    <div align="center">
        <table align="center" style="width: 300px; background-color: #FFFFCC;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Survey Title</td>
                <td align="left" style="width: 140px;">
                    <asp:TextBox ID="txtSurveyTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Question Title</td>
                <td align="left" style="width: 140px;">
                    <asp:TextBox ID="txtQuestionTitle" runat="server"></asp:TextBox>
                </td>
            </tr>
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Question Type</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="DDlQuestionType" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="Single Text">Single Text</asp:ListItem>
                        <asp:ListItem Value="Multiple">Multiple</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold;">Choice Format</td>
                <td align="left" style="width: 140px;">
                    <asp:DropDownList ID="DDlChoiceFormat" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="Text">Text</asp:ListItem>
                        <asp:ListItem Value="Drop down">Drop down</asp:ListItem>
                        <asp:ListItem Value="Radio">Radio</asp:ListItem>
                        <asp:ListItem Value="Checkbox">Checkbox</asp:ListItem>
                    </asp:DropDownList>
                </td>
            </tr>
            <tr>

                <td colspan="2" align="center">
                    <asp:Button ID="BtnSave" runat="server" Text="Save" OnClick="BtnSave_Click" />
                </td>

            </tr>
        </table>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>

    <div align="center" runat="server" id="dvRow" visible="false">
        <b>Question Row</b>
        <table align="center" style="width: 300px;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold; width: 145px;">Row Count</td>
                <td align="left">
                    <input type="text" id="txtRowCount" style="float: left;" />
                </td>
            </tr>

        </table>
    </div>
    <div style="clear: both;"></div>
    <div align="center" id="dvQuestionRows">
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>

    <div align="center" runat="server" id="dvColumn" visible="false">
        <b>Question Column</b>
        <table align="center" style="width: 300px;">
            <tr>

                <td align="left" nowrap="nowrap" style="font-weight: bold; width: 140px;">Column Count</td>
                <td align="left">
                    <input type="text" id="txtColumnCount" style="float: left;" />
                </td>
            </tr>

        </table>
    </div>
    <div style="clear: both;"></div>
    <div align="center" id="dvQuestColumns">
    </div>

    <div align="center" id="dvMsg" runat="server" visible="false">
        <asp:Label ID="LblMsg" runat="server" ForeColor="Red">Saved Successfully</asp:Label>

    </div>
    <div align="center">
        <span id="spnErrMsg" style="color: red;"></span>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div align="center">
        <span style="font-weight: bold; color: black;">Table 1: Questions List</span>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdQuestionList" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdQuestionList_RowCommand">

            <Columns>

                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action" HeaderStyle-Width="400px">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" runat="server" Text="Select" CommandName="Select" />
                        <asp:Button ID="BtnDesignSurvey" runat="server" Text="Add Rows/Columns" CommandName="Add Rows/Columns" />
                        <asp:Button ID="BtnEditSurvey" runat="server" Text="Edit Rows/Columns" CommandName="Edit Rows/Columns" />
                        <asp:Button ID="BtnDelete" runat="server" Text="Delete" CommandName="DeleteQns" />

                        <div style="display: none;">
                            <%--<asp:Label runat="server" ID="lblYear" Text='<%#DataBinder.Eval(Container.DataItem,"Year") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblChapterID" Text='<%#DataBinder.Eval(Container.DataItem,"ChapterID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                            <asp:Label runat="server" ID="lblRespondantType" Text='<%#DataBinder.Eval(Container.DataItem,"RespondentType") %>'></asp:Label>--%>
                            <asp:Label runat="server" ID="lblQuestCount" Text='<%#DataBinder.Eval(Container.DataItem,"cnt") %>'></asp:Label>
                        </div>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="SurveyID" HeaderText="SurveyID"></asp:BoundField>
                <asp:BoundField DataField="QuestionID" HeaderText="QuestionID"></asp:BoundField>

                <asp:BoundField DataField="Title" HeaderText="Question Title"></asp:BoundField>
                <asp:BoundField DataField="QuestionType" HeaderText="Question Type"></asp:BoundField>
                <asp:BoundField DataField="ChoiceFormat" HeaderText="Choice Format"></asp:BoundField>
                <%-- <asp:BoundField DataField="RespondentType" HeaderText="Respondant Type"></asp:BoundField>
                <asp:BoundField DataField="ResponseCount" HeaderText="ResponseCount"></asp:BoundField>--%>
            </Columns>
        </asp:GridView>
    </div>
    <input type="hidden" id="hdnSurveyID" runat="server" value="0" />
    <input type="hidden" id="hdnSurveyText" runat="server" value="0" />
    <input type="hidden" id="hdnChoiceFormat" runat="server" value="0" />
    <input type="hidden" id="hdnUserID" runat="server" value="0" />
    <input type="hidden" id="HdnQuestionID" runat="server" value="0" />

</asp:Content>
