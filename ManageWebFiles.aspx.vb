Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO

Partial Class ManageWebFiles
    Inherits System.Web.UI.Page

    Dim volunteerid, roleid, memberid, eventyear, chapterId As Integer
    Dim ChapterPath As String = String.Empty
    Dim NationalFlag As String = String.Empty
    Dim SelectedPath As String = String.Empty
    Dim ParentPath As String = String.Empty



    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            ' Make sure that parent has logged in
            ' TODO: Enable the following code before going live
            If Session("LoginID") Is Nothing Then
                Response.Redirect("~/Maintest.aspx")
            End If

            If ((Session("entryToken") IsNot Nothing) AndAlso (Session("entryToken").ToString().ToLower() <> "volunteer")) Then
                ' TODO: Display error message on this page indicating that this page is only for Volunteer along with a link to login page
            End If

            'On the first page visit, populate the photo tree and select the root node
            PopulateClassVariables()

            If Not Page.IsPostBack Then
                Initialize()
            End If
        Catch ex As Exception
            lblError.Text = String.Format("Error: {0}", ex.Message)
        End Try

    End Sub
    Private Sub Initialize()
        LoadRootFolderList()
        btnDeleteFile.Attributes.Add("language", "javascript")
        btnDeleteFile.Attributes.Add("onClick", "return confirm('Do you want to delete the selected file or folder?');")

    End Sub
    Private Sub LoadRootFolderList()
        Dim diRoot As IO.DirectoryInfo

        ' TODO: Based on the role filter the root folder list here

        Dim accessibleFolder As String = GetAccessibleFolders()
        If (accessibleFolder <> String.Empty) Then
            diRoot = New IO.DirectoryInfo(accessibleFolder)
            RootFolderList.DataSource = diRoot.GetDirectories()
            RootFolderList.DataValueField = "FullName"
            RootFolderList.DataTextField = "Name"
            RootFolderList.AppendDataBoundItems = True
            RootFolderList.DataBind()
            Dim li As New ListItem(diRoot.Name, diRoot.FullName)
            RootFolderList.Items.Insert(1, li)
        End If

    End Sub
    Private Function GetAccessibleFolders() As String
        Dim FolderPath As String = String.Empty
        If ((roleid = 1 Or roleid = 2 Or roleid = 42) AndAlso (NationalFlag = "Y")) Then
            ' For developers give complete access
            FolderPath = Server.MapPath("~/../")
        ElseIf ((roleid = 43) AndAlso (NationalFlag = "Y")) Then
            ' For chapter co-ordinator give access to all chapter folders
            ' Todo: Determine where chapter folders are located
            FolderPath = Server.MapPath("~/../Chapters/")
        ElseIf ((roleid = 5 Or roleid = 43) AndAlso (ChapterPath <> String.Empty)) Then
            ' For chapter co-ordinator give access to all chapter folders
            ' Todo: Determine where chapter folders are located
            FolderPath = Server.MapPath(String.Concat("~/../Chapters/", ChapterPath, "/"))
        End If
        'lblError.Text = FolderPath
        Return FolderPath
    End Function
    Private Sub PopulateClassVariables()

        If Session("RoleId") IsNot Nothing Then
            roleid = Integer.Parse(Session("RoleId").ToString())
        Else
            roleid = -1
        End If

        ' Override the roleid value for testing purpose only! 
        If Request("RoleId") IsNot Nothing Then
            roleid = Integer.Parse(Request("RoleId").ToString())
        End If

        If Session("CustIndID") IsNot Nothing Then
            memberid = Integer.Parse(Session("CustIndID").ToString())
        Else
            memberid = -1
        End If

        If Session("volunteerid") IsNot Nothing Then
            volunteerid = Integer.Parse(Session("volunteerid").ToString())
        Else
            volunteerid = -1
        End If

        If Session("NationalFlag") IsNot Nothing Then
            NationalFlag = Session("NationalFlag").ToString()
        Else
            NationalFlag = String.Empty
        End If

        ' Override the NationalFlag value for testing purpose only! 
        If Request("NationalFlag") IsNot Nothing Then
            NationalFlag = Request("NationalFlag").ToString()
        End If

        If Session("LoginChapterID") IsNot Nothing Then
            chapterId = Integer.Parse(Session("LoginChapterID").ToString())
        Else
            chapterId = -1
        End If

        ' Override the ChapterID value for testing purpose only! 
        If Request("ChapterID") IsNot Nothing Then
            chapterId = Integer.Parse(Request("ChapterID").ToString())
        End If

        ChapterPath = GetChapterPath()
        ' Override the ChapterID value for testing purpose only! 
        If Request("ChapterPath") IsNot Nothing Then
            chapterId = Request("ChapterPath").ToString()
        End If

        eventyear = Now.Year

    End Sub
    Private Function GetChapterPath() As String
        Dim chapterPath As String = String.Empty
        chapterPath = "USA/CT_Hartford"
        Return chapterPath
    End Function
    Private Sub LoadArchiveTreeview()

        'Clear any treeview nodes...
        tvFolders.Nodes.Clear()
        Dim diArchive As IO.DirectoryInfo

        diArchive = New IO.DirectoryInfo(RootFolderList.SelectedValue)
        'Create root node for treeview...
        Dim tnRootNode As New TreeNode
        tnRootNode.Text = RootFolderList.SelectedItem.Text
        tnRootNode.Value = RootFolderList.SelectedValue
        tnRootNode.ShowCheckBox = False
        tnRootNode.Selected = True
        tnRootNode.SelectAction = TreeNodeSelectAction.SelectExpand

        tvFolders.Nodes.Add(tnRootNode)
        tvFolders.Nodes(0).Expanded = True

        'Load treeview nodes...
        LoadArchiveTreeviewNodes(diArchive, tvFolders.Nodes(0))

    End Sub

    Protected Sub LoadArchiveTreeviewNodes(ByVal diCurrentFolder As DirectoryInfo, ByVal tnCurrentParentNode As TreeNode)

        For Each fsiFSInfo As FileSystemInfo In diCurrentFolder.GetFileSystemInfos()
            If fsiFSInfo.Attributes And FileAttributes.Directory Then
                'Add node for subfolder...
                Dim tnCurrentChildNode As TreeNode = AddChildNode(tnCurrentParentNode, fsiFSInfo.Name, fsiFSInfo.FullName, True)

                'Recursively call this routine for each subfolder within current folder...
                Dim diSubfolder As New DirectoryInfo(fsiFSInfo.FullName)
                LoadArchiveTreeviewNodes(diSubfolder, tnCurrentChildNode)
            Else
                'Add link node for file...
                Dim tnCurrentChildNode As TreeNode = AddChildNode(tnCurrentParentNode, fsiFSInfo.Name, fsiFSInfo.FullName, False)
                'tnCurrentChildNode.NavigateUrl = fsiFSInfo.FullName
                'tnCurrentChildNode.Target = "_blank"
            End If
        Next

    End Sub
    Protected Sub EnableDisableControls()
        lblFileMessage.Text = tvFolders.SelectedNode.Value.Replace(Server.MapPath("~/../"), "")

        If System.IO.Directory.Exists(tvFolders.SelectedNode.Value) Then
            'User selected a folder
            btnDownloadFile.Visible = False
            lblDownloadInst.Visible = False
            'btnDownloadFile.Text = "Download Folder"
            btnDeleteFile.Enabled = IsFolderEmpty(tvFolders.SelectedNode.Value)
            btnDeleteFile.Text = "Delete Folder"
            lblDeleteInst.Text = "(To Delete the folder from the server, the folder must be empty.)"
            FileToUpload.Enabled = True
            btnUploadFile.Enabled = True
            lblUploadInst.Visible = True
            lblFolderName.Visible = True
            txtNewFolderName.Visible = True
            btnCreateNewFolder.Visible = True
        ElseIf System.IO.File.Exists(tvFolders.SelectedNode.Value) Then
            'User selected File
            btnDownloadFile.Visible = True
            btnDownloadFile.Text = "Download File"
            lblDownloadInst.Visible = True
            btnDeleteFile.Enabled = True
            btnDeleteFile.Text = "Delete File"
            lblDeleteInst.Text = "(Click this button to delete the selected file from the server.)"
            FileToUpload.Enabled = True
            btnUploadFile.Enabled = True
            lblUploadInst.Visible = True
            lblFolderName.Visible = False
            txtNewFolderName.Visible = False
            btnCreateNewFolder.Visible = False
        Else
            'no selection done yet
            btnDownloadFile.Enabled = False
            btnDownloadFile.Text = "Download"
            lblDownloadInst.Visible = False
            btnDeleteFile.Enabled = False
            btnDeleteFile.Text = "Delete"
            lblDeleteInst.Visible = False
            FileToUpload.Enabled = False
            btnUploadFile.Enabled = False
            lblUploadInst.Visible = False
            lblFolderName.Enabled = False
            txtNewFolderName.Enabled = False
            btnCreateNewFolder.Enabled = False
        End If

    End Sub
    Protected Function AddChildNode(ByVal tnParentNode As TreeNode, ByVal szNodeText As String, ByVal szNodeValue As String, ByVal isFolder As Boolean) As TreeNode
        Dim tnChildNode As New TreeNode

        'Set child nodes text/value...
        tnChildNode.Text = szNodeText
        tnChildNode.Value = szNodeValue
        tnChildNode.Expanded = False
        tnChildNode.SelectAction = TreeNodeSelectAction.SelectExpand
        If (Not isFolder) Then
            tnChildNode.ImageUrl = "~/Images/File.jpg"
        End If
        'If new folder is created or file is uploaded
        If (SelectedPath = szNodeValue) Or (ParentPath = szNodeValue) Then
            tnChildNode.Expand()
            tnChildNode.Select()
            tnParentNode.Expand()
        End If

        'Add child node to parent node...
        tnParentNode.ChildNodes.Add(tnChildNode)
        Return tnChildNode
    End Function

    Protected Sub tvArchive_SelectedNodeChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles tvFolders.SelectedNodeChanged
        EnableDisableControls()
    End Sub
    Private Sub UploadFile(ByVal FileUploadPath As String, ByVal objFileUpload As FileUpload)
        If objFileUpload.HasFile Then
            'lblMessage.Text = "Received " + objFileUpload.FileName + " Content Type " + objFileUpload.PostedFile.ContentType + " Length " + objFileUpload.PostedFile.ContentLength; 
            Try
                objFileUpload.SaveAs(FileUploadPath)
                lblMessage.Text = "File Upload Successful"
                SelectedPath = tvFolders.SelectedNode.Value
                If (tvFolders.SelectedNode.Parent IsNot Nothing) Then
                    ParentPath = tvFolders.SelectedNode.Parent.Value
                End If

                LoadArchiveTreeview()
                EnableDisableControls()
            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try

        Else
            lblMessage.Text = "No uploaded file"
        End If
    End Sub

    Private Sub DownloadSelectedFile(ByVal FilePath As String, ByVal DownloadFileName As String)
        Try

            Response.Clear()
            Response.AddHeader("Content-Disposition", "attachment; filename=" + DownloadFileName)
            Response.ContentType = "application/octet-stream"
            Response.WriteFile(FilePath)
            Response.Flush()
            Response.End()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub
    Private Sub CreateFolder(ByVal ParentFolder As String, ByVal FolderName As String)
        Dim NewFolderPath As String = String.Concat(ParentFolder, "/", FolderName)
        If System.IO.Directory.Exists(ParentFolder) AndAlso (Not System.IO.Directory.Exists(NewFolderPath)) Then
            System.IO.Directory.CreateDirectory(NewFolderPath)
            lblMessage.Text = "Folder Created Successfully"
            SelectedPath = tvFolders.SelectedNode.Value
            If (tvFolders.SelectedNode.Parent IsNot Nothing) Then
                ParentPath = tvFolders.SelectedNode.Parent.Value
            End If

            LoadArchiveTreeview()
            EnableDisableControls()
            txtNewFolderName.Text = ""
        Else
            lblMessage.Text = "Folder already exists"
        End If
    End Sub
    Private Sub DeleteFileFolder(ByVal FolderFileName As String)
        If System.IO.Directory.Exists(FolderFileName) Then
            Try
                System.IO.Directory.Delete(FolderFileName)
                lblMessage.Text = "Folder Deleted"
                lblFileMessage.Text = ""
            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try
        ElseIf System.IO.File.Exists(tvFolders.SelectedNode.Value) Then
            Try
                System.IO.File.Delete(FolderFileName)
                lblMessage.Text = "File Deleted"
                lblFileMessage.Text = ""
            Catch ex As Exception
                lblMessage.Text = ex.Message
            End Try
        End If
        SelectedPath = tvFolders.SelectedNode.Value
        If (tvFolders.SelectedNode.Parent IsNot Nothing) Then
            ParentPath = tvFolders.SelectedNode.Parent.Value
        End If

        LoadArchiveTreeview()
        EnableDisableControls()
    End Sub

    Protected Sub DownloadFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDownloadFile.Click
        WebPageLog_Insert(volunteerid, roleid, memberid, eventyear, "D", Path.GetDirectoryName(tvFolders.SelectedNode.Value), tvFolders.SelectedNode.Text)
        DownloadSelectedFile(tvFolders.SelectedNode.Value, tvFolders.SelectedNode.Text)
    End Sub

    Protected Sub UploadNewFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnUploadFile.Click
        If (btnCreateNewFolder.Visible) Then
            ' If btnCreateNewFolder.Enabled = true then the user has selected a folder and is trying to upload a new file
            If (FileToUpload.HasFile) AndAlso (Not System.IO.File.Exists(String.Concat(tvFolders.SelectedNode.Value, "/", FileToUpload.FileName))) Then
                Dim FilePath As String = String.Concat(tvFolders.SelectedNode.Value, "/", FileToUpload.FileName)
                Dim ParentFolder As String = Path.GetDirectoryName(tvFolders.SelectedNode.Value)
                Dim FolderName As String = tvFolders.SelectedNode.Text
                UploadFile(FilePath, FileToUpload)
                WebPageLog_Insert(volunteerid, roleid, memberid, eventyear, "U", ParentFolder, FolderName)
            Else
                lblMessage.Text = "File already exists or file to upload is not selected. If you are trying to replace existing file, please select the file name on the tree view and then upload this file."
            End If
        Else
            'If btnCreateNewFolder.Enabled = false then the user has selected a filte and is trying to replace the existing file
            If System.IO.File.Exists(tvFolders.SelectedNode.Value) AndAlso (tvFolders.SelectedNode.Text.ToLower = FileToUpload.FileName.ToLower) Then
                Dim FilePath As String = String.Concat(tvFolders.SelectedNode.Parent.Value, "/", FileToUpload.FileName)
                Dim ParentFolder As String = Path.GetDirectoryName(tvFolders.SelectedNode.Value)
                Dim FolderName As String = tvFolders.SelectedNode.Text
                UploadFile(FilePath, FileToUpload)
                WebPageLog_Insert(volunteerid, roleid, memberid, eventyear, "U", ParentFolder, FolderName)
            Else
                lblMessage.Text = "The name of the file you are trying to upload/replace doesn't match with the file name you have selected in the tree view. Please select the correct file name you want to replace before uploading the same."
            End If
        End If

    End Sub

    Protected Sub CreateNewFolder_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCreateNewFolder.Click
        If (txtNewFolderName.Text.Trim = String.Empty) Then
            lblMessage.Text = "Folder Name is a required field"
        Else
            Dim ParentFolder As String = Path.GetDirectoryName(tvFolders.SelectedNode.Value)
            Dim FolderName As String = tvFolders.SelectedNode.Text
            CreateFolder(tvFolders.SelectedNode.Value, txtNewFolderName.Text)
            WebPageLog_Insert(volunteerid, roleid, memberid, eventyear, "C", ParentFolder, FolderName)
        End If
    End Sub

    Protected Sub btnDeleteFile_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnDeleteFile.Click
        Dim ParentFolder As String = Path.GetDirectoryName(tvFolders.SelectedNode.Value)
        Dim FolderName As String = tvFolders.SelectedNode.Text
        DeleteFileFolder(tvFolders.SelectedNode.Value)
        WebPageLog_Insert(volunteerid, roleid, memberid, eventyear, "X", ParentFolder, FolderName)
    End Sub

    Private Function IsFolderEmpty(ByVal FolderPath As String) As Boolean

        Dim result As Boolean = True

        Dim folderInfo As New System.IO.DirectoryInfo(FolderPath)
        For Each fsiFSInfo As FileSystemInfo In folderInfo.GetFileSystemInfos
            result = False
            Exit For
        Next
        'If (result) Then
        '    lblMessage.Text = String.Concat(FolderPath, " is empty")
        'Else
        '    lblMessage.Text = String.Concat(FolderPath, " is not empty")
        'End If
        Return result
    End Function

    Private Sub WebPageLog_Insert( _
        ByVal VolunteerID As Integer, _
        ByVal RoleID As Integer, _
        ByVal MemberID As Integer, _
        ByVal EventYear As Integer, _
        ByVal UploadDownloadFlag As String, _
        ByVal Folder As String, _
        ByVal FileName As String)

        Dim conn As New SqlConnection(Application("ConnectionString").ToString())
        Dim sqlCommand As String = "WebPageLog_Insert"
        Dim param As SqlParameter() = New SqlParameter(6) {}
        param(0) = New SqlParameter("@VolunteerID", VolunteerID)
        param(1) = New SqlParameter("@RoleID", RoleID)
        param(2) = New SqlParameter("@MemberID", MemberID)
        param(3) = New SqlParameter("@EventYear", EventYear)
        param(4) = New SqlParameter("@UploadDownloadFlag", UploadDownloadFlag)
        param(5) = New SqlParameter("@Folder", Folder)
        param(6) = New SqlParameter("@FileName", FileName)
        SqlHelper.ExecuteScalar(conn, CommandType.StoredProcedure, sqlCommand, param)
    End Sub

    Protected Sub RootFolderList_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles RootFolderList.SelectedIndexChanged
        If (RootFolderList.SelectedValue <> String.Empty) Then
            trTreeView.Visible = True
            LoadArchiveTreeview()
            EnableDisableControls()
        Else
            trTreeView.Visible = False
        End If
    End Sub
End Class
