﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Partial Class assigncoach
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If

        If Not Page.IsPostBack Then
            If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Then
                ddlRole.Enabled = True
                TrPrdGrp.Visible = True
                TrPrd.Visible = True
            ElseIf Session("RoleId").ToString() = "89" Then
                ddlRole.SelectedIndex = ddlRole.Items.IndexOf(ddlRole.Items.FindByValue("88"))
                ddlRole.Enabled = False
                TrPrdGrp.Visible = False
                TrPrd.Visible = False
                btnassigncoach.Text = "Assign Coach"
            Else
                Server.Transfer("maintest.aspx")
            End If
            'loadvolunteer()
            ddlRole.SelectedValue = "88"
            If ddlRole.SelectedValue = "88" Then
                TrPrdGrp.Visible = False
                TrPrd.Visible = False
                btnassigncoach.Text = "Assign Coach"
            Else
                btnassigncoach.Text = "Assign Coach Admin"
                TrPrdGrp.Visible = True
                TrPrd.Visible = True
            End If
            'RbtnVolSignUp.Checked = True
            RbtnGeneralDb.Checked = True
            'FillVolSignUpGrid()
            'FillNotSignedUpVolunteer()
            'dvVolSignUp.Visible = True
            'dvNotSignedupVolunteer.Visible = True

            'dvVolunteer.Visible = False
            'Search.Enabled = False
            loadvolunteer()
            dvVolunteer.Visible = True
            dvVolSignUp.Visible = False
            dvNotSignedupVolunteer.Visible = False
            Search.Enabled = True
            txtParent.Text = ""
            pIndSearch.Visible = True

            LoadProductGroup()

        End If
    End Sub
    Private Sub loadvolunteer()
        ' lblerr.Text = String.Empty
        Dim cmdVolText As String = "select V.volunteerID,I.FirstName + ' ' + I.LastName as VName,I.AutoMemberID,I.City,I.State,I.Email,P.Name as ProductName,Products=STUFF((SELECT distinct ', ' + productcode FROM calsignup WHERE  memberid=I.AutoMemberId and eventyear<>2018  FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, ''),CASE WHEN cc.SignUpID IS NULL THEN 'True' Else 'False' END as Status from Volunteer V inner join IndSpouse I ON V.MemberID = I.AutoMemberID and V.RoleId=" & ddlRole.SelectedValue & " Left Join Product P On P.ProductID=V.ProductID Left JOIN CalSignUp CC ON  CC.MemberID = V.MemberId and GETDATE()< CC.Enddate and CC.EventYear >= Year(GETDATE()) order by I.LastName, I.FirstName ASC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdVolText)
        Dim dt As DataTable = ds.Tables(0)
        If ds.Tables(0).Rows.Count > 0 Then
            DGVolunteer.DataSource = dt
            If ddlRole.SelectedValue = "88" Then
                DGVolunteer.Columns(6).Visible = False
                btnassigncoach.Text = "Assign Coach"
            Else
                DGVolunteer.Columns(6).Visible = True
                btnassigncoach.Text = "Assign Coach Admin"
            End If
            DGVolunteer.DataBind()

        Else
            DGVolunteer.DataSource = Nothing
            DGVolunteer.DataBind()
            lblerr.Text = "No " & ddlRole.SelectedItem.Text & " found"
        End If

    End Sub
    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and Email like '%" + email + "%'")
            Else
                strSql.Append("  Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by lastname,firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "usp_SelectWhereIndspouse2", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
            For i As Integer = 0 To dt.Rows.Count - 1
                Dim memberId As String = dt.Rows(i)("automemberid").ToString()
                Dim Products As String = ""
                Dim cmdText As String = "select Products=STUFF((SELECT distinct ', ' + productcode FROM calsignup WHERE  memberid=" & memberId & " and eventyear<>" & DateTime.Now.Year & "  FOR XML PATH(''), TYPE).value('.[1]', 'nvarchar(max)'), 1, 2, '')"
                Dim dsProducts As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                If dsProducts.Tables.Count > 0 Then
                    If (dsProducts.Tables(0).Rows.Count > 0) Then
                        Products = dsProducts.Tables(0).Rows(0)("Products").ToString()
                    End If
                End If
                GridMemberDt.Rows(i).Cells(10).Text = Products
            Next
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub Search_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles Search.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs)
        pIndSearch.Visible = False
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        'MsgBox(SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql))
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtParent.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            HlblParentID.Text = GridMemberDt.DataKeys(index).Value
            ' pIndSearch.Visible = False
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnassigncoach_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnassigncoach.Click
        If Not HlblParentID.Text = "" Then
            If RbtnGeneralDb.Checked = True Then
                If ddlRole.SelectedValue = "88" Then
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=88 and memberid=" & HlblParentID.Text & "") < 1 Then
                        Dim strsql As String = "Insert into volunteer(MemberID, RoleId, RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national],TeamID) values ("
                        strsql = strsql & HlblParentID.Text & ",88,'Coach',Year(Getdate()),13,'Coaching',getdate()," & Session("LoginID") & ",'Y',7)"
                        Try
                            SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                            lblerr.Text = "Inserted Successfully"
                            HlblParentID.Text = ""
                            txtParent.Text = ""
                            loadvolunteer()
                        Catch ex As Exception
                            lblerr.Text = strsql
                            lblerr.Text = lblerr.Text & ex.ToString()
                        End Try
                    Else
                        lblerr.Text = " Role already exist for this volunteer"
                    End If
                Else
                    ''coach Admin
                    If ddlPrdGroup.Items(0).Selected = True And ddlPrdGroup.SelectedItem.Text = "Select Product Group" Then
                        lblerr.Text = " Please select Product Group"
                        Exit Sub
                    ElseIf ddlPrd.Items(0).Selected = True And ddlPrd.SelectedItem.Text = "Select Product" Then
                        lblerr.Text = " Please select Product"
                        Exit Sub
                    End If
                    Dim PrdgrpCode As String()
                    PrdgrpCode = ddlPrdGroup.SelectedValue.Split("-")
                    Dim PrdCode As String()
                    PrdCode = ddlPrd.SelectedValue.Split("-")
                    'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=89 and ProductGroupID=" & PrdgrpCode(0).ToString() & " and ProductID=" & PrdCode(0).ToString()) < 3 Then


                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=89 and MemberID=" & HlblParentID.Text & " and ProductGroupID=" & PrdgrpCode(0).ToString() & " and ProductID=" & PrdCode(0).ToString()) < 1 Then
                        Dim strsql As String = "Insert into volunteer(MemberID, RoleId,ProductID,ProductCode,ProductGroupID,ProductGroupCode,RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national],TeamID) values ("
                        strsql = strsql & HlblParentID.Text & ",89," & PrdCode(0).ToString() & ",'" & PrdCode(1).ToString() & "'," & PrdgrpCode(0).ToString() & ",'" & PrdgrpCode(1).ToString() & "','CoachAdmin',Year(Getdate()),13,'Coaching',getdate()," & Session("LoginID") & ",'Y',2)"
                        Try
                            SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                            lblerr.Text = "Inserted Successfully"
                            HlblParentID.Text = ""
                            txtParent.Text = ""
                            loadvolunteer()
                        Catch ex As Exception
                            lblerr.Text = strsql
                            lblerr.Text = lblerr.Text & ex.ToString()
                        End Try
                    Else
                        lblerr.Text = " Selected Coach Admin is already assigned for " & ddlPrd.SelectedItem.Text
                    End If
                    'Else
                    '    lblerr.Text = " Coach Admin is already assigned for " & ddlPrd.SelectedItem.Text
                    'End If
                End If
            Else
                'VolSignUp

                If ddlRole.SelectedValue = "88" Then
                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=88 and memberid=" & HlblParentID.Text & "") < 1 Then
                        Dim strsql As String = "Insert into volunteer(MemberID, RoleId,TeamID, RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national]) values ("
                        strsql = strsql & HlblParentID.Text & ",88,7,'Coach',Year(Getdate()),13,'Coaching',getdate()," & Session("LoginID") & ",'Y')"
                        Try
                            SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                            lblerr.Text = "Inserted Successfully"
                            HlblParentID.Text = ""
                            txtParent.Text = ""
                            loadvolunteer()
                            FillVolSignUpGrid()
                        Catch ex As Exception
                            lblerr.Text = strsql
                            lblerr.Text = lblerr.Text & ex.ToString()
                        End Try
                    Else
                        lblerr.Text = " Role already exist for this volunteer"
                    End If
                Else
                    ''coach Admin
                    If ddlPrdGroup.Items(0).Selected = True And ddlPrdGroup.SelectedItem.Text = "Select Product Group" Then
                        lblerr.Text = " Please select Product Group"
                        Exit Sub
                    ElseIf ddlPrd.Items(0).Selected = True And ddlPrd.SelectedItem.Text = "Select Product" Then
                        lblerr.Text = " Please select Product"
                        Exit Sub
                    End If
                    Dim PrdgrpCode As String()
                    PrdgrpCode = ddlPrdGroup.SelectedValue.Split("-")
                    Dim PrdCode As String()
                    PrdCode = ddlPrd.SelectedValue.Split("-")

                    If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(volunteerID) from volunteer where roleid=89 and MemberID=" & HlblParentID.Text & " and ProductGroupID=" & PrdgrpCode(0).ToString() & " and ProductID=" & PrdCode(0).ToString()) < 1 Then

                        Dim strsql As String = "Insert into volunteer(MemberID, RoleId,ProductID,ProductCode,ProductGroupID,ProductGroupCode,RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national],TeamID) values ("
                        strsql = strsql & HlblParentID.Text & ",89," & PrdCode(0).ToString() & ",'" & PrdCode(1).ToString() & "'," & PrdgrpCode(0).ToString() & ",'" & PrdgrpCode(1).ToString() & "','CoachAdmin',Year(Getdate()),13,'Coaching',getdate()," & Session("LoginID") & ",'Y',2)"
                        Try
                            SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, strsql)
                            lblerr.Text = "Inserted Successfully"
                            HlblParentID.Text = ""
                            txtParent.Text = ""
                            loadvolunteer()
                            FillVolSignUpGrid()
                        Catch ex As Exception
                            lblerr.Text = strsql
                            lblerr.Text = lblerr.Text & ex.ToString()
                        End Try
                    Else
                        lblerr.Text = " Coach Admin is already assigned for " & ddlPrd.SelectedItem.Text
                    End If

                End If
            End If
        Else
            lblerr.Text = "Please select The volunteer to be assinged using Search Button"
        End If
    End Sub

    Protected Sub DGVolunteer_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lbtn As LinkButton = CType(e.Item.FindControl("lbtnRemove"), LinkButton)
        Dim lbl As Label = CType(e.Item.FindControl("lblAutoMemberID"), Label)
        If (Not (lbtn) Is Nothing) Then
            Dim volunteerID As Integer = CInt(e.Item.Cells(2).Text)
            Dim cmdtext As String = String.Empty
            hdnVolunteerID.Value = volunteerID
            Dim AutoMemberID As String = lbl.Text
            Dim count As Integer = 0
            Try
                cmdtext = "select count(*) as CountSet from CoachReg where CMemberID = '" + AutoMemberID + "' and paymentreference is not null"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdtext)
                If ds.Tables.Count > 0 Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        count = ds.Tables(0).Rows(0)("CountSet").ToString()
                    End If
                End If
                If count = 0 Then
                    lbtn.Enabled = True
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "actVolunteerDelete();", True)
                Else
                    lbtn.Enabled = False
                End If


            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub ddlPrdGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        LoadProductID()
    End Sub

    Private Sub LoadProductGroup()
        'Dim strSql As String = "SELECT  Convert(varchar,ProductGroupID) + '-' + ProductGroupCode as code,Name from ProductGroup where EventId=13  order by ProductGroupID"
        Dim year As Integer = DateTime.Now.Year

        Dim strSql As String = "select Convert(varchar,ProductGroupID) + '-' + ProductGroupCode as code, Name from ProductGroup where EventId=13 and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventYear=" & year & ") order by ProductGroupID"
        Dim drproductgroup As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlPrdGroup.DataSource = drproductgroup
        ddlPrdGroup.DataBind()
        If ddlPrdGroup.Items.Count < 1 Then
            lblerr.Text = "No Product is opened. Please Contact admin and Get Product Opened in EventFees table"
        ElseIf ddlPrdGroup.Items.Count > 1 Then
            ddlPrdGroup.Items.Insert(0, "Select Product Group")
            ddlPrdGroup.Items(0).Selected = True
            ddlPrdGroup.Enabled = True
            LoadProductID()
        Else
            ddlPrdGroup.Enabled = False
            LoadProductID()
        End If
    End Sub
    Private Sub LoadProductID()
        Dim year As Integer = DateTime.Now.Year
        ' will load depending on Selected item in Productgroup
        Dim conn As New SqlConnection(Application("ConnectionString"))
        If ddlPrdGroup.Items(0).Selected = True And ddlPrdGroup.SelectedItem.Text = "Select Product Group" Then
            ddlPrd.Items.Clear()
            ddlPrd.Enabled = False
        Else
            Dim strSql As String
            Dim PrdgrpCpde As String()
            PrdgrpCpde = ddlPrdGroup.SelectedValue.Split("-")
            Try

                strSql = "Select Convert(varchar,ProductID) + '-' + ProductCode as code, Name from Product Where EventId=13 AND ProductGroupID =" & PrdgrpCpde(0).ToString() & " and ProductID in (select distinct(ProductId) from EventFees where EventYear=" & year & ") order by ProductID"
                Dim drproductid As SqlDataReader
                drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
                ddlPrd.DataSource = drproductid
                ddlPrd.DataBind()
                If ddlPrd.Items.Count > 1 Then
                    ddlPrd.Items.Insert(0, "Select Product")
                    ddlPrd.Items(0).Selected = True
                    ddlPrd.Enabled = True
                ElseIf ddlPrd.Items.Count < 1 Then
                    ddlPrd.Enabled = False
                Else
                    ddlPrd.Enabled = False
                    ''
                End If
            Catch ex As Exception
                lblerr.Text = strSql & ex.ToString
            End Try
        End If
    End Sub

    Protected Sub ddlRole_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        If ddlRole.SelectedValue = "88" Then
            TrPrdGrp.Visible = False
            TrPrd.Visible = False
            btnassigncoach.Text = "Assign Coach"
        Else
            btnassigncoach.Text = "Assign Coach Admin"
            TrPrdGrp.Visible = True
            TrPrd.Visible = True
            LoadProductGroup()
            'LoadProductID()
        End If
        loadvolunteer()
        If RbtnVolSignUp.Checked = True Then
            FillVolSignUpGrid()
        End If

    End Sub
    Private Sub FillVolSignUpGrid()
        ' lblerr.Text = ""
        Dim cmdText As String = String.Empty
        Dim RoleID As String = String.Empty
        Dim TeamID As String = String.Empty

        If ddlRole.SelectedValue = "88" Then
            RoleID = "88"
            TeamID = "7"
        ElseIf ddlRole.SelectedValue = "89" Then
            RoleID = "89"
            TeamID = "2"
        End If
        Dim CurrentYear As Integer = 0
        CurrentYear = DateTime.Now.Year
        Dim sCurYear As String
        sCurYear = CurrentYear.ToString()
        cmdText = "Select distinct Vs.MemberID,VS.TeamId,VS.TeamName,Vs.Year,VS.EventID,VS.ProductGroupID,VS.ProductID ,IP.Email,IP.FirstName,IP.LastName, IP.FirstName + ' '+IP.LastName as Name, IP.City,IP.State,IP.HPhone,IP.CPhone,PG.ProductGroupCode,P.ProductCode,E.EventCode,  VS.VolSignupId from VolSignUp VS inner join IndSpouse IP on (VS.MemberID = IP.AutoMemberID) left join ProductGroup PG on (VS.ProductGroupID=PG.ProductGroupID) left join Product P on (VS.ProductID=P.ProductID)      left join Event E on VS.EventId=E.EventId where VS.EventID=13 and VS.TeamId=" + TeamID + " and not exists (select * from volunteer V where VS.MemberID=V.MemberId and V.RoleId=" + RoleID + ") order by IP.LastName,IP.FirstName ASC"

        'cmdText = "Select distinct Vs.MemberID,VS.TeamId,VS.TeamName,Vs.Year,VS.EventID,VS.ProductGroupID,VS.ProductID ,IP.Email,IP.FirstName,IP.LastName, IP.City,IP.State,IP.HPhone,IP.CPhone,PG.ProductGroupCode,P.ProductCode,E.EventCode from VolSignUp VS inner join IndSpouse IP on (VS.MemberID = IP.AutoMemberID) left join ProductGroup PG on (VS.ProductGroupID=PG.ProductGroupID) left join Product P on (VS.ProductID=P.ProductID)      left join Event E on VS.EventId=E.EventId where VS.EventID=13 and VS.Year='" + sCurYear + "' and VS.TeamId=" + TeamID + " and not exists (select * from volunteer V where VS.MemberID=V.MemberId and V.RoleId=" + RoleID + " and V.EventYear='" + sCurYear + "') order by IP.LastName,IP.FirstName ASC"


        Dim ds As DataSet = New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
        If (ds.Tables().Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                grdVolunteerSignUp.DataSource = ds
                grdVolunteerSignUp.DataBind()
            Else
                grdVolunteerSignUp.DataSource = ds
                grdVolunteerSignUp.DataBind()
            End If
        Else
            grdVolunteerSignUp.DataSource = ds
            grdVolunteerSignUp.DataBind()
        End If
    End Sub

    Protected Sub RbtnGeneralDb_CheckedChanged(sender As Object, e As EventArgs) Handles RbtnGeneralDb.CheckedChanged
        loadvolunteer()
        dvVolunteer.Visible = True
        dvVolSignUp.Visible = False
        dvNotSignedupVolunteer.Visible = False
        Search.Enabled = True
        txtParent.Text = ""
        pIndSearch.Visible = True
    End Sub

    Protected Sub RbtnVolSignUp_CheckedChanged(sender As Object, e As EventArgs) Handles RbtnVolSignUp.CheckedChanged
        FillVolSignUpGrid()
        dvVolunteer.Visible = False
        dvVolSignUp.Visible = True
        dvNotSignedupVolunteer.Visible = True
        Search.Enabled = False
        txtParent.Text = ""
        pIndSearch.Visible = False
    End Sub

    Protected Sub grdVolunteerSignUp_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try

            If e.CommandName = "AddToVol" Then
                Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
                Dim RowIndex As Integer = gvRow.RowIndex

                lblerr.Text = ""


                Dim MemberID As String = String.Empty
                Dim ProductGroupID As String = String.Empty
                Dim ProductID As String = String.Empty
                Dim EventID As String = String.Empty
                Dim RoleID As String = String.Empty
                Dim TeamID As String = String.Empty
                Dim ProductGroupCode As String = String.Empty
                Dim ProductCode As String = String.Empty
                Dim EventCode As String = String.Empty
                Dim RoleCode As String = String.Empty
                Dim FirstName As String = String.Empty
                Dim LastName As String = String.Empty
                Dim Name As String = String.Empty
                If ddlRole.SelectedValue = "88" Then
                    RoleID = "88"
                    TeamID = "7"
                    RoleCode = "Coach"
                ElseIf ddlRole.SelectedValue = "89" Then
                    RoleID = "89"
                    TeamID = "2"
                    RoleCode = "CoachAdmin"
                End If

                MemberID = DirectCast(gvRow.FindControl("lblMemberID"), Label).Text
                ProductGroupID = DirectCast(gvRow.FindControl("lblProductGroupID"), Label).Text
                ProductID = DirectCast(gvRow.FindControl("lblProductID"), Label).Text
                EventCode = DirectCast(gvRow.FindControl("lblEventCode"), Label).Text
                EventID = DirectCast(gvRow.FindControl("lblEventID"), Label).Text

                ProductGroupCode = DirectCast(gvRow.FindControl("lblProductGroupCode"), Label).Text
                ProductCode = DirectCast(gvRow.FindControl("lblProductCode"), Label).Text
                FirstName = DirectCast(gvRow.FindControl("lblFirstName"), Label).Text
                LastName = DirectCast(gvRow.FindControl("lblLastName"), Label).Text
                Name = FirstName + " " + LastName
                txtParent.Text = Name
                Dim ProductVal As String = String.Empty
                Dim ProductGroupVal As String = String.Empty
                If ProductID <> "" Then
                    ProductVal = ProductID + " - " + ProductCode
                    ddlPrd.SelectedValue = ProductVal

                End If
                If ProductGroupID <> "" Then
                    ProductGroupVal = ProductGroupID + " - " + ProductGroupCode
                    ddlPrdGroup.SelectedValue = ProductGroupVal
                End If
                HlblParentID.Text = MemberID
                If (ProductID = "") Then
                    ProductID = "NULL"
                    ProductCode = "NULL"
                Else
                    ProductCode = "'" & ProductCode & "'"
                    ProductID = ProductID
                End If

                If (ProductGroupID = "") Then
                    ProductGroupID = "NULL"
                    ProductGroupCode = "NULL"
                Else
                    ProductGroupCode = "'" & ProductGroupCode & "'"
                    ProductGroupID = ProductGroupID
                End If


                Dim cmdText As String = String.Empty
                cmdText = "select count(*) as CountSet from Volunteer where MemberID=" + MemberID + " and RoleID=" + RoleID + " and EventID=" + EventID + ""
                Dim DupCount As Integer = 0
                Dim ds As DataSet = New DataSet()
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                If (ds.Tables().Count > 0) Then
                    If ds.Tables(0).Rows.Count > 0 Then
                        DupCount = Convert.ToInt32(ds.Tables(0).Rows(0)("CountSet").ToString())
                    End If
                End If
                If DupCount <= 0 Then

                    cmdText = "Insert into volunteer(MemberID, RoleId,TeamID, ProductID,ProductCode,ProductGroupID,ProductGroupCode,RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national]) values(" + MemberID + "," + RoleID + "," + TeamID + "," + ProductID + "," + ProductCode + "," + ProductGroupID + "," + ProductGroupCode + ",'CoachAdmin',Year(Getdate())," + EventID + ",'" + EventCode + "',GetDate()," & Session("LoginID") & ",'Y')"
                    SqlHelper.ExecuteNonQuery(Application("connectionstring"), CommandType.Text, cmdText)
                    lblerr.Text = "Inserted Successfully"

                Else
                    lblerr.Text = "Duplicate exists."
                End If
            ElseIf e.CommandName = "DeleteVol" Then
                Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = gvRow.RowIndex

                lblerr.Text = ""


                Dim VolSignupId As String = String.Empty

                VolSignupId = DirectCast(gvRow.FindControl("lblVolSignupId"), Label).Text
                hdnVolSignupId.Value = VolSignupId
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "DeleteConfirm();", True)


            End If

        Catch ex As Exception

        End Try
        FillVolSignUpGrid()
    End Sub
    Private Sub AssignVolToCoach()
        Dim cmdText As String = String.Empty
        cmdText = "Insert into volunteer(MemberID, RoleId, ProductID,ProductCode,ProductGroupID,ProductGroupCode,RoleCode, EventYear, EventId, EventCode,CreateDate, CreatedBy,[national]) values()"
        'Year(Getdate())
    End Sub
    Private Sub DeleteVolunteerSignup()
        Dim volunteerSignupIdID As String = hdnVolSignupId.Value
        If volunteerSignupIdID <> "0" Then

            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from VolSignup Where VolSignupId=" & volunteerSignupIdID & "")

                FillVolSignUpGrid()
                lblerr.Text = "Deleted successfully"
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub
    Private Sub DeleteVolunteer()
        Dim volunteerID As String = hdnVolunteerID.Value
        If volunteerID <> "0" Then


            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from Volunteer Where volunteerID=" & volunteerID & "")

                loadvolunteer()
                lblerr.Text = "Deleted successfully"
            Catch ex As Exception
                lblerr.Text = ex.Message
                lblerr.Text = (lblerr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        End If
    End Sub

    Protected Sub btnConfirmDelete_Click(sender As Object, e As EventArgs)
        DeleteVolunteer()
    End Sub
    Protected Sub btnDelete_Click(sender As Object, e As EventArgs)
        DeleteVolunteerSignup()
    End Sub
    Private Sub FillNotSignedUpVolunteer()
        lblerr.Text = ""
        Dim cmdText As String = String.Empty
        Dim RoleID As String = String.Empty
        Dim TeamID As String = String.Empty
        Dim CurrentYear As Integer = 0
        CurrentYear = DateTime.Now.Year
        Dim sCurYear As String
        sCurYear = CurrentYear.ToString()

        cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year='" + sCurYear + "' and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and not exists (select * from CalSignUp where EventYear='" + sCurYear + "' and b.AutoMemberID=MemberID ) order by B.LastName, B.FirstName ASC"



        Dim ds As DataSet = New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
        If (ds.Tables().Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                GrdNotSignedupVolunteer.DataSource = ds
                GrdNotSignedupVolunteer.DataBind()
            Else
                GrdNotSignedupVolunteer.DataSource = ds
                GrdNotSignedupVolunteer.DataBind()
            End If
        Else
            GrdNotSignedupVolunteer.DataSource = ds
            GrdNotSignedupVolunteer.DataBind()
        End If
    End Sub

    'Protected Sub GrdNotSignedupVolunteer_RowCommand(sender As Object, e As GridViewCommandEventArgs)
    '    Try

    '        If e.CommandName = "AddVol" Then
    '            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
    '            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")
    '            Dim RowIndex As Integer = gvRow.RowIndex

    '            lblerr.Text = ""


    '            Dim MemberID As String = String.Empty

    '            Dim EventID As String = String.Empty
    '            Dim RoleID As String = String.Empty
    '            Dim TeamID As String = String.Empty
    '            Dim ProductGroupCode As String = String.Empty
    '            Dim ProductCode As String = String.Empty
    '            Dim EventCode As String = String.Empty
    '            Dim RoleCode As String = String.Empty
    '            Dim FirstName As String = String.Empty
    '            Dim LastName As String = String.Empty
    '            Dim Name As String = String.Empty
    '            If ddlRole.SelectedValue = "88" Then
    '                RoleID = "88"
    '                TeamID = "7"
    '                RoleCode = "Coach"
    '            ElseIf ddlRole.SelectedValue = "89" Then
    '                RoleID = "89"
    '                TeamID = "2"
    '                RoleCode = "CoachAdmin"
    '            End If

    '            MemberID = DirectCast(gvRow.FindControl("lblMemberID"), Label).Text

    '            EventCode = "Coaching"
    '            EventID = "13"


    '            FirstName = DirectCast(gvRow.FindControl("lblFirstName"), Label).Text
    '            LastName = DirectCast(gvRow.FindControl("lblLastName"), Label).Text
    '            Name = FirstName + " " + LastName
    '            txtParent.Text = Name
    '            Dim ProductVal As String = String.Empty
    '            Dim ProductGroupVal As String = String.Empty

    '            HlblParentID.Text = MemberID

    '        End If

    '    Catch ex As Exception

    '    End Try

    'End Sub

    Public Sub ExportTable1ToExcel()
        Dim cmdVolText As String = "select V.volunteerID,I.FirstName + ' ' + I.LastName as VName,I.AutoMemberID,I.City,I.State,I.Email,P.Name as ProductName from Volunteer V inner join IndSpouse I ON V.MemberID = I.AutoMemberID and V.RoleId=" & ddlRole.SelectedValue & " Left Join Product P On P.ProductID=V.ProductID Left JOIN CalSignUp CC ON  CC.MemberID = V.MemberId and GETDATE()< CC.Enddate and CC.EventYear >= Year(GETDATE()) order by I.LastName, I.FirstName ASC"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdVolText)

        If ds.Tables(0).Rows.Count > 0 Then
            Dim dt As DateTime = DateTime.Now
            Dim month As String = dt.ToString("MMM")
            Dim day As String = dt.ToString("dd")
            Dim year As String = dt.ToString("yyyy")
            Dim monthDay As String = month & "" & day

            ds.Tables(0).Columns.Add("Ser#").SetOrdinal(0)
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                ds.Tables(0).Rows(i)("Ser#") = i + 1
            Next
            ds.Tables(0).TableName = "VolunteerswithAssignedRoles"
            Dim filename As String = "VolunteerswithAssignedRoles_" & monthDay & "_" & year & ".xls"

            ExcelHelper.ToExcel(ds, filename, Page.Response)
        End If
    End Sub

    Public Sub ExportTable2ToExcel()
        Dim cmdText As String = String.Empty
        Dim RoleID As String = String.Empty
        Dim TeamID As String = String.Empty

        If ddlRole.SelectedValue = "88" Then
            RoleID = "88"
            TeamID = "7"
        ElseIf ddlRole.SelectedValue = "89" Then
            RoleID = "89"
            TeamID = "2"
        End If
        Dim CurrentYear As Integer = 0
        CurrentYear = DateTime.Now.Year
        Dim sCurYear As String
        sCurYear = CurrentYear.ToString()
        cmdText = "Select distinct Vs.MemberID,VS.TeamId,VS.TeamName,Vs.Year,VS.EventID,VS.ProductGroupID,VS.ProductID ,IP.Email,IP.FirstName,IP.LastName, IP.City,IP.State,IP.HPhone,IP.CPhone,PG.ProductGroupCode,P.ProductCode,E.EventCode from VolSignUp VS inner join IndSpouse IP on (VS.MemberID = IP.AutoMemberID) left join ProductGroup PG on (VS.ProductGroupID=PG.ProductGroupID) left join Product P on (VS.ProductID=P.ProductID)      left join Event E on VS.EventId=E.EventId where VS.EventID=13 and VS.TeamId=" + TeamID + " and not exists (select * from volunteer V where VS.MemberID=V.MemberId and V.RoleId=" + RoleID + ") order by IP.LastName,IP.FirstName ASC"

        'cmdText = "Select distinct Vs.MemberID,VS.TeamId,VS.TeamName,Vs.Year,VS.EventID,VS.ProductGroupID,VS.ProductID ,IP.Email,IP.FirstName,IP.LastName, IP.City,IP.State,IP.HPhone,IP.CPhone,PG.ProductGroupCode,P.ProductCode,E.EventCode from VolSignUp VS inner join IndSpouse IP on (VS.MemberID = IP.AutoMemberID) left join ProductGroup PG on (VS.ProductGroupID=PG.ProductGroupID) left join Product P on (VS.ProductID=P.ProductID)      left join Event E on VS.EventId=E.EventId where VS.EventID=13 and VS.Year='" + sCurYear + "' and VS.TeamId=" + TeamID + " and not exists (select * from volunteer V where VS.MemberID=V.MemberId and V.RoleId=" + RoleID + " and V.EventYear='" + sCurYear + "') order by IP.LastName,IP.FirstName ASC"


        Dim ds As DataSet = New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
        If (ds.Tables().Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As DateTime = DateTime.Now
                Dim month As String = dt.ToString("MMM")
                Dim day As String = dt.ToString("dd")
                Dim year As String = dt.ToString("yyyy")
                Dim monthDay As String = month & "" & day
                ds.Tables(0).Columns.Add("Ser#").SetOrdinal(0)
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    ds.Tables(0).Rows(i)("Ser#") = i + 1
                Next

                ds.Tables(0).TableName = "VolunteersSignedUpforCoachingbutnotyetassignedacoachrole"
                Dim filename As String = "VolunteersSignedUpforCoachingbutnotyetassignedacoachrole_" & monthDay & "_" & year & ".xls"

                ExcelHelper.ToExcel(ds, filename, Page.Response)
            End If
        Else

        End If
    End Sub

    Public Sub ExportTable3ToExcel()
        lblerr.Text = ""
        Dim cmdText As String = String.Empty
        Dim RoleID As String = String.Empty
        Dim TeamID As String = String.Empty
        Dim CurrentYear As Integer = 0
        CurrentYear = DateTime.Now.Year
        Dim sCurYear As String
        sCurYear = CurrentYear.ToString()

        cmdText = "select B.AutoMemberID, B.FirstName, B.LastName, B.Gender, B.Email, B.HPhone, B.CPhone, B.City, B.State, B.Chapter from IndSpouse B where exists (select * from Volunteer where B.AutoMemberID=MemberID and RoleId=88) and exists (select * from volsignup where year='" + sCurYear + "' and EventId=13 and TeamId=7 and B.AutoMemberID=MemberID ) and not exists (select * from CalSignUp where EventYear='" + sCurYear + "' and b.AutoMemberID=MemberID ) order by B.LastName, B.FirstName ASC"



        Dim ds As DataSet = New DataSet()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
        If (ds.Tables().Count > 0) Then
            If ds.Tables(0).Rows.Count > 0 Then
                Dim dt As DateTime = DateTime.Now
                Dim month As String = dt.ToString("MMM")
                Dim day As String = dt.ToString("dd")
                Dim year As String = dt.ToString("yyyy")
                Dim monthDay As String = month & "" & day
                ds.Tables(0).Columns.Add("Ser#").SetOrdinal(0)
                For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                    ds.Tables(0).Rows(i)("Ser#") = i + 1
                Next

                ds.Tables(0).TableName = "CoachRoleassignedCalSignUpwasnotyetdone"
                Dim filename As String = "CoachRoleassignedCalSignUpwasnotyetdone_" & monthDay & "_" & year & ".xls"

                ExcelHelper.ToExcel(ds, filename, Page.Response)
            End If
        Else

        End If
    End Sub

    Protected Sub BtnExportTable3_Click(sender As Object, e As EventArgs)
        ExportTable3ToExcel()
    End Sub
    Protected Sub BtnExportTable2_Click(sender As Object, e As EventArgs)
        ExportTable2ToExcel()
    End Sub
    Protected Sub BtnExportTable1_Click(sender As Object, e As EventArgs)
        ExportTable1ToExcel()
    End Sub

End Class
