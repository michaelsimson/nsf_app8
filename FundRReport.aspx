﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FundRReport.aspx.vb" Inherits="FundRReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript">
        function add(productId, fundRID) {

            document.getElementById("<%=hdnProductID.ClientID%>").value = productId;
            document.getElementById("<%=hdnFundRID.ClientID%>").value = fundRID;
        
            
            document.getElementById('<%=btnExport.ClientID%>').click();

        }
    </script>
    <div align="left">
        <asp:HyperLink CssClass="btn_02" ID="hlnkMainPage" runat="server"></asp:HyperLink>
    </div>
    <asp:Button ID="btnExport" runat="server" Text="Export To Excel" Style="display: none;" OnClick="btnExport_Click" />
    <div align="center" style="width: 1000px">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="center">
                    <h4>&nbsp;Fundraising Status </h4>
                    <br />
                    <asp:Literal ID="ltrl1" runat="server"></asp:Literal>
                </td>
            </tr>
            <tr>

                <td align="center">

                    <asp:GridView Visible="false" DataKeyNames="FundRCalID" GridLines="Both" ID="gvEvents" runat="server" AllowPaging="False" AllowSorting="True" AutoGenerateColumns="False" BorderStyle="Solid" CssClass="GridStyle" CaptionAlign="Top" BackColor="White" BorderColor="#CCCCCC" BorderWidth="2px" CellPadding="5" EnableSortingAndPagingCallbacks="false">
                        <Columns>
                            <asp:ButtonField CommandName="Select" Text="Select Event" HeaderText="Select"></asp:ButtonField>
                            <asp:BoundField DataField="ChapterCode" HeaderText="Chapter" />
                            <asp:BoundField DataField="EventDate" HeaderText="EventDate" DataFormatString="{0:d}" />
                            <asp:BoundField DataField="Timings" HeaderText="Timings" />
                            <asp:BoundField DataField="VenueName" HeaderText="Venue" />
                            <asp:BoundField DataField="EventDescription" HeaderText="EventDescription" />
                        </Columns>
                        <HeaderStyle BorderStyle="Solid" BorderWidth="1px" Font-Bold="True" ForeColor="White" />
                        <FooterStyle BackColor="White" ForeColor="#000066" />
                        <RowStyle ForeColor="#000066" />
                        <SelectedRowStyle BackColor="#669999" Font-Bold="True" ForeColor="White" />
                        <PagerStyle BackColor="White" ForeColor="#000066" HorizontalAlign="Left" />
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <span style="font-weight: bold;" id="spnTable1" runat="server" visible="false">Table 1: Family of 4: Dinner/Awards/Entertainment/Workshops</span>
                    <div style="clear: both;"></div>
                    <table border="0" cellpadding="3" cellspacing="0">
                        <tr>
                            <td align="center">

                                <asp:DataGrid ID="dgCatalog" Width="1000px" Visible="false" CellPadding="7" OnItemDataBound="dgCatalog_ItemDataBound" runat="server" CssClass="GridStyle" AllowSorting="True"
                                    AutoGenerateColumns="False" BorderWidth="1px"
                                    DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF"
                                    BorderStyle="None" GridLines="Horizontal">
                                    <FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
                                    <SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
                                    <AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
                                    <ItemStyle HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
                                    <HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>
                                    <Columns>
                                        <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblProductID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label>
                                                <asp:Label runat="server" ID="lblAmount" Visible="false" CssClass="SmallFont"></asp:Label>
                                                <asp:Label ID="lblFundRFeesID" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label>
                                                <asp:Label ID="lblProductCode" runat="server" Visible="false" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
                                                <asp:Label ID="lblProductName" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSelfees" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblSelPaymentMode" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentMode") %>'></asp:Label>

                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblQuantity" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label runat="server" ID="lblStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Check" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCheckAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Credit_Card" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblCreditCardAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="In Kind" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblInKindAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>
                                        <asp:TemplateColumn HeaderText="Payment_Info" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                                            <ItemTemplate>
                                                <asp:Label ID="lblPaymentDate" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentDate","{0:d}") %>'>		</asp:Label>
                                                &nbsp;&nbsp;
        <asp:Label ID="lblPaymentMode" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label>
                                                <br />
                                                <asp:Label ID="lblPaymentReference" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>'>		</asp:Label>
                                            </ItemTemplate>
                                            <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                                            <ItemStyle Wrap="False"></ItemStyle>
                                        </asp:TemplateColumn>




                                    </Columns>
                                    <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
                                </asp:DataGrid>
                            </td>
                        </tr>

                        <%--<tr><td align = "right" >
		<table cellpadding="2" cellspacing = "0" border ="0"><tr><td> Check </td><td>: <asp:Label ID="lblCheck" runat="server"></asp:Label></td>
		<td> CreditCard </td><td>: <asp:Label ID="lblCredit" runat="server"></asp:Label></td> 
		<td> In Kind </td><td>: <asp:Label ID="lblInKind" runat="server"></asp:Label></td> 
		<td> &nbsp;</td>
		 </tr></table>
		 	
		</td> </tr>--%>
                    </table>
                </td>
            </tr>

        </table>

    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div align="center" id="dvContestChildA">
        <span style="font-weight: bold;" id="spnTable2" runat="server" visible="false">Table 2: Contest</span>
        <div style="clear: both;"></div>
        <asp:DataGrid ID="DataGridContestChildA" Width="1000px" OnItemDataBound="DataGridContestChildA_ItemDataBound" Visible="false" CellPadding="7" runat="server" CssClass="GridStyle" AllowSorting="True"
            AutoGenerateColumns="False" BorderWidth="1px"
            DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF"
            BorderStyle="None" GridLines="Horizontal">
            <FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
            <SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
            <AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
            <ItemStyle HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
            <HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="Label1" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label>
                        <asp:Label runat="server" ID="lblChildAAmount" Visible="false" CssClass="SmallFont"></asp:Label>
                        <asp:Label ID="Label3" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label>
                        <asp:Label ID="Label4" runat="server" Visible="false" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
                        <asp:Label ID="Label5" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label6" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label7" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentMode") %>'></asp:Label>

                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label8" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblChildAStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Check" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblChildACheckAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Credit_Card" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblChildACreditCardAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="In Kind" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblChildAInKindAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Payment_Info" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="Label13" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentDate","{0:d}") %>'>		</asp:Label>
                        &nbsp;&nbsp;
        <asp:Label ID="Label14" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label>
                        <br />
                        <asp:Label ID="Label15" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>'>		</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>

            </Columns>
            <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div align="center" id="dvContests">
        <span style="font-weight: bold;"></span>
        <div style="clear: both;"></div>
        <asp:DataGrid ID="DataGridContests" Width="1000px" OnItemDataBound="DataGridContests_ItemDataBound" Visible="false" CellPadding="7" runat="server" CssClass="GridStyle" AllowSorting="True"
            AutoGenerateColumns="False" BorderWidth="1px"
            DataKeyField="FundRFeesID" BackColor="White" BorderColor="#E7E7FF"
            BorderStyle="None" GridLines="Horizontal">
            <FooterStyle CssClass="GridFooter" BackColor="#B5C7DE" ForeColor="#4A3C8C"></FooterStyle>
            <SelectedItemStyle CssClass="SelectedRow" BackColor="#738A9C" Font-Bold="True" ForeColor="#F7F7F7"></SelectedItemStyle>
            <AlternatingItemStyle Wrap="False" BackColor="#F7F7F7"></AlternatingItemStyle>
            <ItemStyle HorizontalAlign="Left" Wrap="true" BackColor="#E7E7FF" ForeColor="#4A3C8C"></ItemStyle>
            <HeaderStyle Wrap="true" BackColor="#4A3C8C" Font-Bold="True" ForeColor="#F7F7F7"></HeaderStyle>
            <Columns>
                <asp:TemplateColumn HeaderText="Name" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="Label16" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductID") %>'>		</asp:Label>
                        <asp:Label runat="server" ID="lblContestAmount" Visible="false" CssClass="SmallFont"></asp:Label>
                        <asp:Label ID="Label18" Visible="false" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "FundRFeesID") %>'>		</asp:Label>
                        <asp:Label ID="Label19" runat="server" Visible="false" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductCode") %>'></asp:Label>
                        <asp:Label ID="Label20" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "ProductName") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn ItemStyle-HorizontalAlign="Right" HeaderText="Fee" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label21" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "Fee","{0:F}") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Pay_Method" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label22" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"PaymentMode") %>'></asp:Label>

                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Quantity" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="Label23" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"Quantity") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Status" Visible="false" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label runat="server" ID="lblContestStatus" CssClass="SmallFont" Text='<%#DataBinder.Eval(Container.DataItem,"FundRRegID") %>'></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Check" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblContestCheckAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Credit_Card" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblContestCreditCardAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="In Kind" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="lblContestInKindAmt" Visible="false" runat="server" CssClass="SmallFont"></asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Right" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Payment_Info" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:Label ID="Label28" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentDate","{0:d}") %>'>		</asp:Label>
                        &nbsp;&nbsp;
        <asp:Label ID="Label29" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentMode") %>'>		</asp:Label>
                        <br />
                        <asp:Label ID="Label30" runat="server" CssClass="SmallFont" Text='<%# DataBinder.Eval(Container.DataItem, "PaymentReference") %>'>		</asp:Label>
                    </ItemTemplate>
                    <HeaderStyle Wrap="False" ForeColor="White" Font-Bold="true"></HeaderStyle>
                    <ItemStyle Wrap="False"></ItemStyle>
                </asp:TemplateColumn>



            </Columns>
            <PagerStyle HorizontalAlign="Right" ForeColor="#4A3C8C" BackColor="#E7E7FF" Mode="NumericPages"></PagerStyle>
        </asp:DataGrid>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center">
        <asp:Literal ID="LtrBreakTablesByContest" runat="server"></asp:Literal>
    </div>
    <input type="hidden" id="hdnProductID" value="0" runat="server" />
    <input type="hidden" id="hdnFundRID" value="0" runat="server" />
   
</asp:Content>

