Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports System.Net.Mail
Imports System.Text.RegularExpressions
Imports NorthSouth.DAL
Imports Microsoft.ApplicationBlocks.Data

Partial Class ScheduleTechCoordinator

    Inherits System.Web.UI.Page
    'Private Const intContentYear As Integer = 2009
    Public contestId As Integer
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Try
            If (Session("LoggedIn") Is Nothing) Or (Session("LoggedIn") <> "True") Then
                If (Not Session("entryToken") Is Nothing) Then
                    Response.Redirect("Login.aspx?entry=" + Session("entryToken").ToString.Substring(1, 1))
                End If
            End If
            If (Session("selChapterID") Is Nothing) Then
                Response.Redirect("VolunteerFunctions.aspx")
            End If
            lblReportError.Text = String.Empty
            If Not Page.IsPostBack Then
                Dim nsfMaster As VRegistration.NSFMasterPage = Me.Master
                nsfMaster.addBackMenuItem("volunteerfunctions.aspx")
                lblChapter.Text = Session("selChapterName")
                Dim year As Integer = Convert.ToInt32(DateTime.Now.Year)
                ddlYear.Items.Insert(0, New ListItem(Convert.ToString(year - 1)))
                ddlYear.Items.Insert(1, New ListItem(Convert.ToString(year)))
                ddlYear.Items.Insert(2, New ListItem(Convert.ToString(year + 1)))
                ddlYear.SelectedIndex = 1
                LoadContests(True)
            End If
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Private Sub LoadContests(ByVal blnLoadGrid As Boolean)
        ' If blnLoadGrid Or IsNothing(Session("dsContests")) Then
        Dim sb As StringBuilder = New StringBuilder
        sb.Append("select a.ContestId, c.ChapterCode,a.Contestdate, a.productcode, b.name as ContestName, a.ExamRecID, a.LeftSignID, a.RightSignID, LeftTitle, RightTitle, a.Ph1Rooms ,a.Ph2Rooms")
        sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.ExamRecID) ExamReceiver")
        sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.LeftSignID) LeftSignature")
        sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.RightSignID) RightSignature")
        sb.Append(" from contest a inner join product b on b.productid=a.productid ")
        sb.Append(" inner join chapter c on a.NSFChapterID = c.ChapterID")
        sb.Append(" where(a.nsfchapterid = " & Session("selChapterID") & ")")
        sb.Append(" and a.contest_year = " & ddlYear.SelectedItem.Value)
        sb.Append(" order by contestdate, a.productid")
        Dim ds As New DataSet
        Dim conn As New SqlConnection(Application("ConnectionString"))
        ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
        'Session("dsContests") = ds
        'End If
        'grdContests.DataSource = Session("dsContests")
        grdContests.DataSource = ds
        grdContests.DataBind()
    End Sub

    Protected Sub grdContests_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdContests.CancelCommand
        Try
            grdContests.EditItemIndex = -1
            LoadContests(False)
            lblReportError.Text = String.Empty
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Private Sub showTimeOutMsg()
        Response.Redirect("timeoutError.aspx")
    End Sub
    Protected Sub grdContests_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdContests.EditCommand
        Try
            grdContests.EditItemIndex = CInt(e.Item.ItemIndex)
            Dim page As Integer = grdContests.CurrentPageIndex
            Dim pageSize As Integer = grdContests.PageSize

            Dim currentRowIndex As Integer

            ' Dim vDS As DataSet = CType(Session("dsContests"), DataSet)
            'Ferdine Included
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("select a.ContestId, c.ChapterCode,a.Contestdate, a.productcode, b.name as ContestName, a.ExamRecID, a.LeftSignID, a.RightSignID, LeftTitle, RightTitle , a.Ph1Rooms, a.Ph2Rooms")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.ExamRecID) ExamReceiver")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.LeftSignID) LeftSignature")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.RightSignID) RightSignature")
            sb.Append(" from contest a inner join product b on b.productid=a.productid ")
            sb.Append(" inner join chapter c on a.NSFChapterID = c.ChapterID")
            sb.Append(" where(a.nsfchapterid = " & Session("selChapterID") & ")")
            sb.Append(" and a.contest_year = " & ddlYear.SelectedItem.Value)
            sb.Append(" order by contestdate, a.productid")
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
            Dim vDS As DataSet = ds
            If (vDS Is Nothing) Then
                showTimeOutMsg()
                Return
            End If

            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If

            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            'Ferdine Added Instead of above line
            'MsgBox(grdContests.DataKeys(e.Item.ItemIndex))
            Session("editRowId") = grdContests.DataKeys(e.Item.ItemIndex)

            LoadContests(False)
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub

    Protected Sub grdContests_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles grdContests.PageIndexChanged
        grdContests.CurrentPageIndex = e.NewPageIndex
        grdContests.EditItemIndex = -1
        LoadContests(False)
    End Sub

    Protected Sub grdContests_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles grdContests.UpdateCommand
        Try
            Dim intExamReceiverID As Integer
            Dim intLeftSignID As Integer
            Dim intRightSignID As Integer
            Dim strLeftTitle As String = ""
            Dim strRightTitle As String = ""
            Dim intContestID As Integer
            Dim intPh1Rooms, intPh2Rooms As Integer
            intContestID = grdContests.DataKeys(e.Item.ItemIndex)
            getPh1SplitRooms(intContestID)
            intPh1Rooms = CType(e.Item.Cells(3).Controls(0).FindControl("ddlRooms1"), DropDownList).SelectedValue
            intPh2Rooms = CType(e.Item.Cells(3).Controls(0).FindControl("ddlRooms"), DropDownList).SelectedValue
            intExamReceiverID = CType(e.Item.Cells(5).Controls(0).FindControl("ddlExamReceiver"), DropDownList).SelectedValue
            intLeftSignID = CType(e.Item.Cells(6).Controls(0).FindControl("ddlLeftSignature"), DropDownList).SelectedValue
            If CType(e.Item.Cells(7).Controls(0).FindControl("ddlLeftTitle"), DropDownList).Visible Then
                strLeftTitle = CType(e.Item.Cells(7).Controls(0).FindControl("ddlLeftTitle"), DropDownList).SelectedValue
            End If
            intRightSignID = CType(e.Item.Cells(8).Controls(0).FindControl("ddlRightSignature"), DropDownList).SelectedValue
            If CType(e.Item.Cells(9).Controls(0).FindControl("ddlRightTitle"), DropDownList).Visible Then
                strRightTitle = CType(e.Item.Cells(9).Controls(0).FindControl("ddlRightTitle"), DropDownList).SelectedValue
            End If
            'validations
            If intExamReceiverID = 0 Then
                lblReportError.Text = "Please Select Tech Coordinator"
                Exit Sub
            End If
            If intLeftSignID = 0 Then
                lblReportError.Text = "Please Select Left Signature"
                Exit Sub
            End If

            If strLeftTitle.Trim = "" Then
                lblReportError.Text = "Please enter Left Title"
                Exit Sub
            End If

            If intRightSignID = 0 Then
                lblReportError.Text = "Please Select Right Signature"
                Exit Sub
            End If
            If intLeftSignID = intRightSignID Then
                lblReportError.Text = "Left Signature cannot be same as Right Signature"
                Exit Sub
            End If

            If strRightTitle.Trim = "" Then
                lblReportError.Text = "Please enter Right Title"
                Exit Sub
            End If

            'update to database
            Dim sb As StringBuilder = New StringBuilder
            sb.Append(" Update contest set ExamRecID=" & intExamReceiverID)
            sb.Append(" ,LeftSignID=" & intLeftSignID)
            sb.Append(" ,LeftTitle='" & strLeftTitle & "'")
            sb.Append(" ,RightSignID=" & intRightSignID)
            sb.Append(" ,RightTitle='" & strRightTitle & "'")
            sb.Append(" ,Ph1Rooms = " & intPh1Rooms)
            sb.Append(" ,Ph2Rooms = " & intPh2Rooms)
            sb.Append("  where contestid = " & intContestID)
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            SqlHelper.ExecuteScalar(conn, CommandType.Text, sb.ToString())

            grdContests.EditItemIndex = -1
            LoadContests(True)
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Public Sub ExamReceiver_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            Dim rowExamReceiverID As String = ""
            ddlTemp = sender
            'Dim dr As DataRow = CType(Session("editRow"), DataRow)
            Dim dr As DataRow
            dr = GetRow(Session("editRowId"))

            If (dr Is Nothing) Then
                Return
            End If

            Dim sb As StringBuilder = New StringBuilder
            sb.Append("Select distinct a.memberid, (isnull(firstname, '') +  ' ' + isnull(lastname,'')) fullname from Volunteer a, IndSpouse b where b.automemberid = a.memberid and roleid in (9) and ") 'Modified on 03-04-2013 to allow only TechCs (8, 5 )
            sb.Append(" a.Chapterid = " & Session("selChapterID"))
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
            If (Not dr.Item("ExamRecID") Is DBNull.Value) Then
                rowExamReceiverID = dr.Item("ExamRecID") & ""
            End If
            If ddlTemp.SelectedValue <> "" Then
                rowExamReceiverID = ddlTemp.SelectedValue
            End If
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowExamReceiverID))
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Public Sub LeftSign_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            Dim rowLeftSignID As String = ""
            ddlTemp = sender
            'Dim dr As DataRow = CType(Session("editRow"), DataRow)
            Dim dr As DataRow
            dr = GetRow(Session("editRowId"))

            If (dr Is Nothing) Then
                Return
            End If

            Dim sb As StringBuilder = New StringBuilder
            sb.Append("Select distinct a.memberid, (isnull(firstname, '') +  ' ' + isnull(lastname,'')) fullname from Volunteer a, IndSpouse b where b.automemberid = a.memberid and roleid in (8, 9, 5, 16) and ")
            sb.Append(" a.Chapterid = " & Session("selChapterID"))
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
            If (Not dr.Item("LeftSignID") Is DBNull.Value) Then
                rowLeftSignID = dr.Item("LeftSignID") & ""
            End If
            If ddlTemp.SelectedValue <> "" Then
                rowLeftSignID = ddlTemp.SelectedValue
            End If
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowLeftSignID))
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Public Sub RightSign_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            Dim rowRightSignID As String = ""
            ddlTemp = sender
            'Dim dr As DataRow = CType(Session("editRow"), DataRow)
            Dim dr As DataRow
            dr = GetRow(Session("editRowId"))

            If (dr Is Nothing) Then
                Return
            End If

            Dim sb As StringBuilder = New StringBuilder
            sb.Append("Select distinct a.memberid, (isnull(firstname, '') +  ' ' + isnull(lastname,'')) fullname from Volunteer a, IndSpouse b where b.automemberid = a.memberid and roleid in (8, 9, 5, 16) and ")
            sb.Append(" a.Chapterid = " & Session("selChapterID"))
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
            If (Not dr.Item("RightSignID") Is DBNull.Value) Then
                rowRightSignID = dr.Item("RightSignID") & ""
            End If
            If ddlTemp.SelectedValue <> "" Then
                rowRightSignID = ddlTemp.SelectedValue
            End If
            ddlTemp.DataSource = ds
            ddlTemp.DataBind()
            ddlTemp.Items.Insert(0, New ListItem("Please Select", "0"))
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRightSignID))
        Catch ex As Exception
            lblReportError.Text = ex.Message
        End Try
    End Sub
    Public Sub ddlLeftTitle_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        Dim rowLeftTitle As String = ""
        ddlTemp = sender
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow
        dr = GetRow(Session("editRowId"))
        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("LeftTitle") Is DBNull.Value) Then
            rowLeftTitle = dr.Item("LeftTitle") & ""
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowLeftTitle))
        End If
        ddlTemp.Visible = True
    End Sub
    Public Sub ddlRightTitle_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        Dim rowRightTitle As String = ""
        ddlTemp = sender
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow
        dr = GetRow(Session("editRowId"))

        If (dr Is Nothing) Then
            Return
        End If
        If (Not dr.Item("RightTitle") Is DBNull.Value) Then
            rowRightTitle = dr.Item("RightTitle") & ""
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRightTitle))
        End If
        ddlTemp.Visible = True
    End Sub
    Public Sub getPh1SplitRooms(ByVal intContestID)
        contestId = intContestID
    End Sub
    Public Sub ddlRooms_PreRender1(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        Dim rowRooms As String = ""
        ddlTemp = sender
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow
        dr = GetRow(Session("editRowId"))
        If (dr Is Nothing) Then
            Return
        End If

        Dim Ph1Split As String = (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Ph1Split from ContestCategory cc,Contest c Where c.ContestCategoryID=cc.ContestCategoryID and  c.contestId=" & Session("editRowId")).ToString)
        If Not Ph1Split = "" Then
            If (Not dr.Item("Ph1Rooms") Is DBNull.Value) Then
                rowRooms = dr.Item("Ph1Rooms") & ""
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRooms))
            End If
        Else
            ddlTemp.Enabled = False
        End If
        ddlTemp.Visible = True
    End Sub
    Public Sub ddlRooms_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        Dim rowRooms As String = ""
        ddlTemp = sender
        'Dim dr As DataRow = CType(Session("editRow"), DataRow)
        Dim dr As DataRow
        dr = GetRow(Session("editRowId"))

        If (dr Is Nothing) Then
            Return
        End If
        Dim Ph2Split As String = (SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, "Select Ph2Split from ContestCategory cc,Contest c Where c.ContestCategoryID=cc.ContestCategoryID and  c.contestId=" & Session("editRowId")).ToString)
        If Not Ph2Split = "" Then
            If (Not dr.Item("Ph2Rooms") Is DBNull.Value) Then
                rowRooms = dr.Item("Ph2Rooms") & ""
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowRooms))
            End If
        Else
            ddlTemp.Enabled = False
          End If
        ddlTemp.Visible = True
    End Sub
    Protected Function GetRow(ByVal ContestId As Integer) As DataRow
        Dim DataRw As DataRow
        Try
            Dim sb As StringBuilder = New StringBuilder
            sb.Append("select a.ContestId, c.ChapterCode,a.Contestdate, a.productcode, b.name as ContestName, a.ExamRecID, a.LeftSignID, a.RightSignID, LeftTitle, RightTitle, a.Ph1Rooms,a.Ph2Rooms")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.ExamRecID) ExamReceiver")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.LeftSignID) LeftSignature")
            sb.Append(" , (SELECT (isnull(firstname, '') +  ' ' + isnull(lastname,'')) from IndSpouse where Automemberid = a.RightSignID) RightSignature")
            sb.Append(" from contest a inner join product b on b.productid=a.productid ")
            sb.Append(" inner join chapter c on a.NSFChapterID = c.ChapterID")
            sb.Append(" where(a.ContestId = " & ContestId & ")")
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, sb.ToString())
            DataRw = ds.Tables(0).Rows(0)
            Return (DataRw)
        Catch ex As Exception
            lblReportError.Text = ex.Message
            Return (DataRw)
        End Try
    End Function

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        grdContests.CurrentPageIndex = 0
        grdContests.EditItemIndex = -1
        LoadContests(True)
    End Sub

    Protected Sub grdContests_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles grdContests.SelectedIndexChanged
    End Sub
End Class



