Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Net.Mail
Imports System.Net
Imports System.IO
Imports System.Xml
Imports NativeExcel
Imports System.Web.Script.Serialization
Imports Newtonsoft.Json.Linq
Imports VRegistration
Imports System.Web.Services

Partial Class CalendarSignup
    Inherits System.Web.UI.Page
    Public apiKey As String = "6sTMyAgRSpCmTSIJIWFP8w"
    Public apiSecret As String = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh"

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load

        'Session("LoggedIn") = "true"
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = "1" '"96"
        'Session("LoginID") = "4240" '"31453"
        lblTable1NR.Text = ""
        lblTable1ANR.Text = ""

        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        ' btnConfirm.Visible = False
        If Not Page.IsPostBack Then

            Try
                '' If (Session("RoleID").ToString() <> "88") Then
                fillYearFiletr()
                loadPhaseFilter()
                loadProductGroupFilter()
                loadProductFilter()
                loadLevelFilter()
                loadCoachFilter()
                '' End If
            Catch ex As Exception

            End Try

            Dim year As Integer = 0
            Dim roleID As String = String.Empty
            Try
                roleID = Session("RoleID").ToString()
                hdnRoleID.Value = roleID
            Catch ex As Exception
                roleID = 0
            End Try
            ddlEventYear.Items.Add(New ListItem("Select", "0"))
            year = Convert.ToInt32(DateTime.Now.Year)
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year + 1) + "-" + Convert.ToString(year + 2).Substring(2, 2)), Convert.ToString(year + 1)))
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 1) + "-" + Convert.ToString(year).Substring(2, 2)), Convert.ToString(year - 1)))
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 2) + "-" + Convert.ToString(year - 1).Substring(2, 2)), Convert.ToString(year - 2)))
            ddlEventYear.Items.Add(New ListItem((Convert.ToString(year - 3) + "-" + Convert.ToString(year - 2).Substring(2, 2)), Convert.ToString(year - 3)))



            'If Now.Month <= 6 Then
            '    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year - 1)))
            'Else
            '    ddlEventYear.SelectedIndex = ddlEventYear.Items.IndexOf(ddlEventYear.Items.FindByText(Convert.ToString(year)))
            'End If
            If (Session("RoleId").ToString() <> "88") Then
                ' ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
            Else
                Dim cmdYesrSem As String = String.Empty

                cmdYesrSem = "	 select max(EventYear) as EventYear, Semester from CalSignup where Accepted='Y' and EventId=13 and MemberId=" & Session("LoginId").ToString() & " and EventYear is not null group by Semester"
                Dim eventyear As String = ""
                Dim semester As String = ""
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdYesrSem)
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        eventyear = dr("EventYear").ToString()
                        semester = dr("Semester").ToString()
                    Next
                    ''  ddlEventYear.SelectedValue = eventyear
                    '' ddlPhase.SelectedValue = semester
                Else
                    '' ddlEventYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                End If

                Dim DefaultYear() As String = {"Jan", "Feb", "Mar", "Apr", "May", "Jun"}
                Dim CurMonth = DateTime.Now.ToString("MMM")
                If (DefaultYear.Contains(CurMonth)) Then
                    '' ddlEventYear.SelectedValue = DateTime.Now.AddYears(-1).ToString()
                Else
                    ''ddlEventYear.SelectedValue = DateTime.Now.Year.ToString()
                End If

                Dim IsProdOpenYear As String = "select Max(EventYear) from EventFees where  CalSignupOpen='Y'"
                Try
                    Dim OpenYear As Integer = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, IsProdOpenYear)
                    Dim OpenSemester As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select Max(Semester) from EventFees where CalSignupOpen='Y'")
                    ''  ddlEventYear.SelectedValue = OpenYear
                    '' ddlPhase.SelectedValue = OpenSemester.Trim()
                Catch ex As Exception

                End Try


            End If
            loadPhase()
            If Request.QueryString("Role") = "Coach" Then
                'Dim OpenSemester As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select Max(Semester) from EventFees where CalSignupOpen='Y'")

                ' ddlPhase.SelectedValue = OpenSemester.Trim()
                lblUniqueCount.Visible = False
                Dim StrSql As String = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
                Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, StrSql)
                If flag > 0 Then
                    btnConfirm.Visible = True
                End If
            End If

            ''Options for Volunteer Name Selection
            If Session("RoleID") = 88 Then 'Coach can schedule for his own Sessions.
                txtName.Visible = True
                btnSearch.Visible = False
                btnClear.Visible = False
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID from IndSpouse I inner join Volunteer V On V.MemberID = I.AutoMemberID where V.RoleId in (" & Session("RoleID") & ") and V.MemberID=" & Session("LoginID"))
                If ds.Tables(0).Rows.Count > 0 Then
                    txtName.Text = ds.Tables(0).Rows(0)("Name")
                    hdnMemberID.Value = ds.Tables(0).Rows(0)("MemberID")
                End If
            ElseIf (Session("RoleID") = 1 Or Session("RoleID") = 2 Or Session("RoleID") = 96 Or Session("RoleID") = 89) Then 'Coach Admin can schedule for other Coaches.
                EnableDG_NA(True)
                If Request.QueryString("Role") = "Coach" Then
                    txtName.Visible = False
                    btnSearch.Visible = False
                    btnClear.Visible = False
                Else


                    txtName.Visible = True
                    btnSearch.Visible = True
                    btnClear.Visible = True
                End If
                ddlVolName.Visible = True

                '********* To display all Coaches ****************'
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join Volunteer V On V.MemberID = I.AutoMemberID order by I.FirstName,I.LastName")

                'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.LastName,I.FirstName")
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlVolName.DataSource = ds
                    ddlVolName.DataBind()
                    If ds.Tables(0).Rows.Count > 0 Then
                        ddlVolName.Items.Insert(0, "Select Volunteer")
                        ddlVolName.SelectedIndex = 0
                    End If
                Else
                    lblerr.Text = "No Volunteers present for Calendar Sign up"
                End If

            Else
                EnableDG_NA(True)
                txtName.Visible = True
                ddlVolName.Visible = False
                btnSearch.Visible = True
                btnClear.Visible = True
            End If

            '**********To get Products assigned for Volunteers RoleIs =88,89****************'
            'If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Then
            'LoadProductGroup()
            'Else
            If Session("RoleId").ToString() = "89" Then 'Session("RoleId").ToString() = "88" Or
                'Commented By Bindhu on Aug 8_2016 To allow Coach admin to access all
                'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count (*) from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductGroupId is not Null") > 1 Then
                'more than one 
                Dim strCmd As String = " select ProductGroupID,ProductID from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductId is not Null "
                strCmd = strCmd & " UNION ALL select p.ProductGroupID, p.ProductID from product p inner join volunteer v on v.ProductGroupId =p.productgroupid  "
                strCmd = strCmd & " where Memberid = " & Session("LoginID") & " And RoleId = " & Session("RoleId") & " And v.ProductgroupId Is Not Null And v.ProductId Is null and v.ProductGroupId is not null"

                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, strCmd)
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp

            ElseIf Session("RoleId").ToString() = "88" Then
                'Ferdine changing this on Sep 2nd
                'If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & "  and ProductId is not Null") > 1 Then
                'more than one 
                ''Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID from CalSignUp where Memberid=" & Session("LoginID") & " and EventYear=" & Now.Year & " and ProductId is not Null ")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, " select ProductGroupID,ProductID FROM Product  WHERE EventID in (13,19) ")
                Dim i As Integer
                Dim prd As String = String.Empty
                Dim Prdgrp As String = String.Empty
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    If prd.Length = 0 Then
                        prd = ds.Tables(0).Rows(i)(1).ToString()
                    Else
                        prd = prd & "," & ds.Tables(0).Rows(i)(1).ToString()
                    End If

                    If Prdgrp.Length = 0 Then
                        Prdgrp = ds.Tables(0).Rows(i)(0).ToString()
                    Else
                        Prdgrp = Prdgrp & "," & ds.Tables(0).Rows(i)(0).ToString()
                    End If
                Next
                lblPrd.Text = prd
                lblPrdGrp.Text = Prdgrp

            End If

            LoadEvent(ddlEvent)
            LoadWeekdays(ddlWeekDays)

            LoadCapacity(ddlMaxCapacity)

            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim strlevel As String = String.Empty
            Dim coach As String = String.Empty
            'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            '    prdGroup = ddlProductGroup.SelectedValue
            'End If
            'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            '    prdID = ddlProduct.SelectedValue
            'End If
            'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
            '    level = ddlLevel.SelectedValue
            'End If
            'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            '    coach = ddlVolName.SelectedValue
            'End If

            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                strlevel = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If




            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
            ''  LoadMakeUpSessions()
            ''  LoadSubstituteSessions()
            ''  LoadExtraSessions()
            ''  LoadMakeupSubstituteSessions()
            '' LoadExtraSubstituteSessions()

            lblerr.Text = ""

            ''  fillGuestAttendeeGrid()
            ''  fillPractiseSession()

        End If
        hdnEventYear.Value = ddlEventYear.SelectedValue
    End Sub

    Private Sub EnableDG_NA(bFlag As Boolean)
        'added by bindhu
        btnExportTbl1a.Visible = bFlag
        DGCoachNA.Visible = bFlag
        spnTable1aTitle.Visible = bFlag
        lblErrTable1a.Visible = bFlag
    End Sub

    Private Sub FillCoaches(DG As DataGrid)
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    level = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DG)
    End Sub
    Private Sub loadPhase()
        ddlPhase.Items.Clear()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        ddlPhase.Items.Add(New ListItem("Select", "0"))
        For i As Integer = 0 To 2
            ddlPhase.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next
        '' Dim OpenSemester As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select Max(Semester) from EventFees where CalSignupOpen='Y' and EventYear=" & ddlEventYear.SelectedValue & "")
        '' ddlPhase.SelectedValue = OpenSemester
    End Sub
    Public Sub LoadEvent(ByVal ddlObject As DropDownList)
        Dim Strsql As String = "Select Distinct E.EventId,E.EventCode From Event E Inner Join EventFees EF on E.EventId=Ef.EventID  Where  E.EventId in(13,19)" '13- Coaching,19 -PreClub

        ''EF.EventYear =" & ddlEventYear.SelectedValue & "  and
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, Strsql)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()
            If ds.Tables(0).Rows.Count > 1 Then
                ddlObject.Items.Insert(0, New ListItem("Select Event", "-1"))
                '*********Added on 26-11-2013 to disable Prepbclub for the current year
                ddlObject.SelectedIndex = ddlObject.Items.IndexOf(ddlObject.Items.FindByText("Coaching"))
                ddlObject.Enabled = False
                LoadProductGroup()
                LoadProductID()
                '**************************************************************************'
            Else
                ddlObject.Enabled = False
                LoadProductGroup()
                LoadProductID()
            End If
        Else
            lblerr.Text = "No Events present for the selected year"
        End If
    End Sub

    Function getProductcode(ByVal ProductID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductCode from Product where ProductID=" & ProductID & "")
    End Function

    Function getProductGroupcode(ByVal ProductGroupID As Integer) As String
        Return SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select ProductGroupcode from ProductGroup where ProductGroupID=" & ProductGroupID & "")
    End Function

    Public Sub LoadDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "8:00 AM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub
    Public Sub LoadWeekDisplayTime(ByVal ddlObject As DropDownList, ByVal TimeFlag As Boolean)
        Dim dt As DataTable
        dt = New DataTable()
        Dim dr As DataRow
        Dim StartTime As DateTime = "6:00 PM " 'Now()
        dt.Columns.Add("ddlText", Type.GetType("System.String"))
        dt.Columns.Add("ddlValue", Type.GetType("System.String"))
        ' "8:00:00 AM " To "12:00:00 AM "
        While StartTime <= "11:59:00 PM "
            dr = dt.NewRow()

            dr("ddlText") = StartTime.ToString("h:mmtt")
            dr("ddlValue") = StartTime.ToString("h:mmtt")

            dt.Rows.Add(dr)
            StartTime = StartTime.AddHours(1) '.AddMinutes(60)
        End While

        ddlObject.DataSource = dt
        ddlObject.DataTextField = "ddlText"
        ddlObject.DataValueField = "ddlValue"
        ddlObject.DataBind()

        'ddlObject.Items.Insert(ddlObject.Items.Count, "12:00AM")
        ddlObject.Items.Add(New ListItem("12:00AM", "12:00AM"))
        ddlObject.Items.Insert(0, "Select time")
        If TimeFlag = False Then
            ddlObject.SelectedIndex = 0
        End If
    End Sub

    Private Sub LoadWeekdays(ByVal ddlObject As DropDownList)
        'For i As Integer = 0 To 6
        'items.Add(new ListItem("Item 2", "Value 2"));
        ddlObject.Items.Add(New ListItem("Select Day", "-1"))
        ddlObject.Items.Add(New ListItem("Monday", "0"))
        ddlObject.Items.Add(New ListItem("Tuesday", "1"))
        ddlObject.Items.Add(New ListItem("Wednesday", "2"))
        ddlObject.Items.Add(New ListItem("Thursday", "3"))
        ddlObject.Items.Add(New ListItem("Friday", "4"))
        ddlObject.Items.Add(New ListItem("Saturday", "5"))
        ddlObject.Items.Add(New ListItem("Sunday", "6"))

        'ddlObject.Items.Add(New ListItem(WeekdayName(i + 1), i))
        ' ddlWeekDays.Items.Add(WeekdayName(i))
        'Next
    End Sub

    Private Sub LoadCapacity(ByVal ddltemp As DropDownList)
        Dim li As ListItem
        Dim i As Integer
        For i = 0 To 30 Step 1 '10 To 100 Step 5
            li = New ListItem(i)
            ddltemp.Items.Add(li)
        Next
        ddltemp.SelectedIndex = ddltemp.Items.IndexOf(ddltemp.Items.FindByValue(20))
    End Sub

    Private Sub LoadProductGroup()
        Try
            'EF.EventYear=" & ddlEventYear.SelectedValue & " and EF.Semester='" & ddlPhase.SelectedValue & "' and
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where  EF.CalSignupOpen='Y' AND  P.EventId=" & ddlEvent.SelectedValue & IIf(lblPrdGrp.Text.Length > 0, " AND P.ProductGroupID IN (" & lblPrdGrp.Text & ")", "") & "  order by P.ProductGroupID"
            Dim drproductgroup As SqlDataReader
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)

            ddlProductGroup.DataSource = drproductgroup
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count < 1 Then
                lblerr.Text = "No Product Group present for the selected Event."
                ddlProduct.Items.Clear()
                ddlLevel.Items.Clear()
            ElseIf ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Items.Insert(0, "Select Product Group")
                ddlProductGroup.Items(0).Selected = True
                ddlProductGroup.Enabled = True
            Else
                ddlProductGroup.Enabled = False
                LoadProductID()
                LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Private Sub LoadProductID()
        ' will load depending on Selected item in Productgroup
        Try
            Dim conn As New SqlConnection(Application("ConnectionString"))
            ' If ddlProductGroup.Items(0).Selected = True And ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
            '    ddlProduct.Enabled = False
            ' Else

            'EF.EventYear = " & ddlEventYear.SelectedValue & " And EF.Semester ='" & ddlPhase.SelectedValue & "' and
            Dim strSql As String
            strSql = "Select distinct P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where  EF.CalSignupOpen='Y' AND P.EventID=" & ddlEvent.SelectedValue & IIf(lblPrd.Text.Length > 0, " AND P.ProductID IN (" & lblPrd.Text & ")", "") & "" 'and P.Status='O'
            If ddlProductGroup.SelectedItem.Text <> "Select Product Group" Then
                strSql = strSql & " and P.ProductGroupID =" & ddlProductGroup.SelectedValue & " "
                strSql = strSql & " order by P.ProductID"
            End If


            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlProduct.DataSource = drproductid
            ddlProduct.DataBind()
            If ddlProduct.Items.Count > 1 Then
                ddlProduct.Items.Insert(0, "Select Product")
                ddlProduct.Items(0).Selected = True
                ddlProduct.Enabled = True
            ElseIf ddlProduct.Items.Count < 1 Then
                ddlProduct.Enabled = False
            Else
                ddlProduct.Items(0).Selected = True
                ddlProduct_SelectedIndexChanged(ddlProduct, New EventArgs)
                ddlProduct.Enabled = False
                'LoadGrid(ddlAcceptedFilter.SelectedValue)
                'LoadMakeUpSessions()
                'LoadSubstituteSessions()
            End If
            '  End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Private Sub LoadLevel(ByVal ddlObject As DropDownList, ByVal ProductGroup As String, ByVal ProductID As String, ByVal EventID As String, ByVal EventYear As String)

        Try
            ddlObject.Items.Clear()
            ddlObject.Enabled = True

            Dim cmdText As String = String.Empty
            cmdText = "select ProdLevelID,LevelCode from ProdLevel where EventYear=" & EventYear & " and ProductGroupID=" & ProductGroup & " and ProductID=" & ProductID & " and EventID=" & EventID & ""
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim level As SqlDataReader
            level = SqlHelper.ExecuteReader(conn, CommandType.Text, cmdText)
            ddlObject.DataSource = level
            ddlObject.DataValueField = "LevelCode"
            ddlObject.DataTextField = "LevelCode"
            ddlObject.DataBind()
            Dim dt As DataTable = New DataTable()
            dt.Load(level)
            If (ddlObject.Items.Count > 1) Then
                ddlObject.Items.Insert(0, "Select Level")
            Else
                If (ddlObject.Items.Count = 0) Then
                    ddlObject.Items.Insert(0, "No Level found")
                End If

            End If
            Try


                Dim CmdAdult As String = "select count(*) from EventFees where Eventyear=" & EventYear & " and EventId=13 and ProductGroupId=" & ProductGroup & " and productId=" & ProductID & " and Adult='Y' and GradeFrom = 13 and GradeTo=13"
                Dim Count As Integer = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, CmdAdult)
                If (Count > 0) Then
                    ddlObject.SelectedValue = "Senior"
                    ddlObject.Enabled = False
                End If
            Catch ex As Exception

            End Try
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
        'If ProductGroup.Contains("SAT") Then ' ddlProductGroup.SelectedItem.Text.Contains("SAT") Then
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Junior", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Senior", 2))
        'ElseIf ProductGroup.Contains("Universal Values") Then
        '    ''ddlObject.Enabled = False
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Junior", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
        '    ddlObject.Items.Insert(3, New ListItem("Senior", 3))
        'ElseIf ProductGroup.Contains("Science") Then
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("One level only", 1))
        'Else
        '    ddlObject.Items.Insert(0, New ListItem("Select", 0))
        '    ddlObject.Items.Insert(1, New ListItem("Beginner", 1))
        '    ddlObject.Items.Insert(2, New ListItem("Intermediate", 2))
        '    ddlObject.Items.Insert(3, New ListItem("Advanced", 3))
        'End If
        ddlWeekDays.SelectedIndex = 0
    End Sub

    Function CheckVolunteer() As Boolean
        lblerr.Text = ""
        If ddlVolName.Visible = False And txtName.Visible = True And txtName.Text = "" Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = False And ddlVolName.Visible = True And ddlVolName.SelectedIndex = 0 Then
            lblerr.Text = "Please select Volunteer "
        ElseIf txtName.Visible = True And ddlVolName.Visible = True Then
            If ddlVolName.SelectedIndex = 0 And txtName.Text.Trim.ToString() = "" Then
                lblerr.Text = "Please select Volunteer "
            ElseIf ddlVolName.SelectedIndex > 0 And txtName.Text <> "" Then
                lblerr.Text = "Please select one of the options for Volunteer"
            Else
                lblerr.Text = ""
            End If
        End If

        If lblerr.Text = "" Then
            Return True
        Else
            Return False
        End If
    End Function
    Protected Sub btnSubmit_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSubmit.Click

        Try
            lblError.Text = ""
            If CheckVolunteer() = False Then
                lblerr.Text = lblerr.Text '"Please select Volunteer"
            ElseIf ddlEvent.SelectedItem.Text.Contains("Select") Then
                lblerr.Text = "Please select Event"
            ElseIf ddlProductGroup.Items.Count = 0 Then
                lblerr.Text = "No Product Group is present for selected Event."
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                lblerr.Text = "Please select Product Group"
            ElseIf ddlProduct.Items.Count = 0 Then
                lblerr.Text = "No Product is opened for " & ddlEventYear.SelectedValue & ". Please Contact admin and Get Product Opened "
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                lblerr.Text = "Please select Product"
                'ElseIf ddlLevel.Enabled = True And ddlLevel.SelectedItem.Text = "Select" Then
            ElseIf ddlLevel.SelectedItem.Text = "Select Level" Or ddlLevel.SelectedItem.Text = "No Level found" Then
                lblerr.Text = "Please select Level"
                'End If
            ElseIf ddlWeekDays.SelectedIndex = 0 Then
                lblerr.Text = "Please Select Day"
            ElseIf ddlDisplayTime.SelectedIndex = 0 Then
                lblerr.Text = "Please Enter Coaching time"
            ElseIf ddlMaxCapacity.SelectedValue < 2 Then
                lblerr.Text = "Please select Maximum Capacity"

            Else
                lblerr.Text = ""
                Dim strSQL As String
                Dim Level As String = ""
                If ddlLevel.Enabled = True Then
                    If ddlLevel.SelectedItem.Text = "Select Level" Or ddlLevel.SelectedItem.Text = "No Level found" Then
                        lblerr.Text = "Please select Level"
                        Exit Sub
                    End If
                    Level = ddlLevel.SelectedItem.Text
                End If

                If ddlSession.SelectedValue <> "" Then
                    Dim cmdtext As String = String.Empty

                    Dim pgID As String = ddlProductGroup.SelectedValue
                    Dim pID As String = ddlProduct.SelectedValue
                    Dim year As String = ddlEventYear.SelectedValue
                    Level = ddlLevel.SelectedValue
                    Dim sessionNo As String = ddlSession.SelectedValue

                    Dim iSessionNo As Integer = Convert.ToInt32(sessionNo) - 1

                    If sessionNo <> "1" Then
                        Dim volunteerId As String = String.Empty
                        If Session("RoleID").ToString() = "88" Then
                            volunteerId = Session("LoginId").ToString()
                        Else
                            volunteerId = ddlVolName.SelectedValue
                        End If

                        cmdtext = " select count(*) as sessioNoCount from CalSignup where EventYear=" & year & " and ProductGroupID=" & pgID & " and ProductID=" & pID & " and Level='" & Level & "' and SessionNo=" & iSessionNo & " and MemberID=" & volunteerId & ""
                        'Session("editRow") = dr
                        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdtext)
                        If ds.Tables(0).Rows.Count > 0 Then
                            If Convert.ToInt32(ds.Tables(0).Rows(0)("sessioNoCount").ToString()) = 0 Then
                                lblerr.Text = "Higher Session # was selected without a lower session # selected earlier."
                                ddlSession.SelectedValue = 1
                                Exit Sub
                            Else

                            End If
                        End If
                    End If
                End If

                Dim cmdYrValidation As String = String.Empty
                cmdYrValidation = "select max(EventYear) from EventFees where eventId=13 "
                Dim academicYr As Integer = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdYrValidation)
                If (academicYr = ddlEventYear.SelectedValue) Then

                Else

                    Dim IsProdOpen As String = "select count(*) from EventFees where Eventyear=" & ddlEventYear.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and Productid=" & ddlProduct.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' and CalSignupOpen='Y'"
                    Dim OpenCount As Integer = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, IsProdOpen)
                    If OpenCount <= 0 Then
                        Dim academicyear As Integer = academicYr + 1
                        Dim stracademicYr As String = academicYr & "-" & academicyear.ToString().Substring(2, 2)
                        lblerr.Text = "Please select the academic year " & stracademicYr & " for signup."
                        Exit Sub
                    End If

                End If
                Dim cmdTextSession As String = "select count(*) from CalSignup where eventyear=" & ddlEventYear.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' and ProductID=" & ddlProduct.SelectedValue & IIf(ddlLevel.SelectedValue <> "", " and Level ='" & ddlLevel.SelectedValue & "'", "") & " AND SessionNo=" & ddlSession.SelectedValue & " and Accepted='Y'"
                If (Session("RoleID").ToString() = "88") Then
                    cmdTextSession = cmdTextSession & " and Memberid = " & Session("LoginId").ToString() & ""
                Else
                    cmdTextSession = cmdTextSession & " and Memberid = " & ddlVolName.SelectedValue & ""

                End If
                If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "Select Count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Eventyear=" & ddlEventYear.SelectedValue & "and EventID =" & ddlEvent.SelectedValue & " and Day='" & ddlWeekDays.SelectedItem.Text & "' and Time='" & ddlDisplayTime.SelectedItem.Text & "' and Semester='" & ddlPhase.SelectedValue & "'") > 0 Then
                    lblerr.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."

                ElseIf (SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdTextSession) > 0) Then
                    lblerr.Text = "There is already an existing approved session " + ddlSession.SelectedValue + " on " + ddlWeekDays.SelectedItem.Text + " at " + ddlDisplayTime.SelectedValue + ". Please change the session#."
                    Exit Sub

                ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & " and Semester='" & ddlPhase.SelectedValue & "' and ProductID=" & ddlProduct.SelectedValue & " and Eventyear=" & ddlEventYear.SelectedValue & IIf(Level <> "", " and Level = '" & Level & "'", "") & " AND Semester='" & ddlPhase.SelectedValue & "' AND SessionNo=" & ddlSession.SelectedValue) < 3 Then '<3 Then

                    'strSQL = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Semester,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES ("
                    'strSQL = strSQL & IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue) & "," & ddlEventYear.SelectedValue & "," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "'," & ddlPhase.SelectedValue & "," & ddlProductGroup.SelectedValue & ",'" & getProductGroupcode(ddlProductGroup.SelectedValue) & "'," & ddlProduct.SelectedValue & ",'" & getProductcode(ddlProduct.SelectedValue) & "'," & IIf(Level <> "", "'" & Level & "'", "NULL") & "," & ddlSession.SelectedValue & ",'" & ddlWeekDays.SelectedItem.Text & "','" & ddlDisplayTime.SelectedValue & "'," & ddlPref.SelectedValue & "," & ddlMaxCapacity.SelectedValue & "," & Session("loginID") & ",Getdate())"
                    Dim strQuery As String = "INSERT INTO CalSignUp (MemberID, EventYear,EventID,EventCode, Semester,ProductGroupID, ProductGroupCode, ProductID, ProductCode, [Level],SessionNo, Day,Time,Preference,MaxCapacity, CreatedBy,CreateDate) VALUES (@MemberID,@EventYear,@EventID, @EventCode,@Semester,@ProductGroupID,@ProductGroupCode,@ProductID,@ProductCode,@Level,@SessionNo,@Day,@Time,@Preference,@MaxCapacity,@CreatedBy,@CreateDate)"

                    Dim cmd As New SqlCommand(strQuery)
                    cmd.Parameters.AddWithValue("@MemberID", IIf(txtName.Text <> "", hdnMemberID.Value, ddlVolName.SelectedValue))
                    cmd.Parameters.AddWithValue("@EventYear", ddlEventYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventID", ddlEvent.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventCode", ddlEvent.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@Semester", ddlPhase.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductGroupID", ddlProductGroup.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductGroupCode", getProductGroupcode(ddlProductGroup.SelectedValue))
                    cmd.Parameters.AddWithValue("@ProductID", ddlProduct.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductCode", getProductcode(ddlProduct.SelectedValue))
                    cmd.Parameters.AddWithValue("@Level", IIf(Level <> "", "" & Level & "", "NULL"))
                    cmd.Parameters.AddWithValue("@SessionNo", ddlSession.SelectedValue)
                    cmd.Parameters.AddWithValue("@Day", ddlWeekDays.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@Time", ddlDisplayTime.SelectedValue)
                    cmd.Parameters.AddWithValue("@Preference", ddlPref.SelectedValue)
                    cmd.Parameters.AddWithValue("@MaxCapacity", ddlMaxCapacity.SelectedValue)
                    cmd.Parameters.AddWithValue("@CreatedBy", Session("LoginId"))
                    cmd.Parameters.AddWithValue("@CreateDate", DateTime.Now.ToShortDateString())

                    Dim objNSF As NSFDBHelper = New NSFDBHelper()
                    If objNSF.InsertUpdateData(cmd) = True Then
                        'clear()
                        Dim prdGroup As String = String.Empty
                        Dim prdID As String = String.Empty
                        Dim strlevel As String = String.Empty
                        Dim coach As String = String.Empty
                        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
                        '    prdGroup = ddlProductGroup.SelectedValue
                        'End If
                        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
                        '    prdID = ddlProduct.SelectedValue
                        'End If
                        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
                        '    strlevel = ddlLevel.SelectedValue
                        'End If
                        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
                        '    coach = ddlVolName.SelectedValue
                        'End If

                        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                            prdGroup = ddlProductGroupFilter.SelectedValue
                        End If
                        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                            prdID = ddlProductFilter.SelectedValue
                        End If
                        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                            strlevel = ddlLevelFilter.SelectedValue
                        End If
                        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                            coach = ddlCoachFilter.SelectedValue
                        End If

                        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
                        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

                        lblerr.Text = "Inserted Successfully"
                    End If
                Else
                    lblerr.Text = "Coach with Same Product, Semester,Level and Session already found"
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub LoadGrid(ByVal Accepted As String, ByVal productGroup As String, ByVal product As String, ByVal level As String, ByVal coachname As String, DG As DataGrid)
        Try
            If spnTable1aTitle.Visible = False And DG.ID = "DGCoachNA" Then
                Exit Sub
            End If
            'ByVal productGroup As String, ByVal product As String, ByVal level As String
            Dim ds As DataSet
            Dim StrSql As String = ""

            If Session("RoleID") = "88" Then
                '' dvSearchSignups.Visible = False

                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, I.Email, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventyearFilter.SelectedValue & ") as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo and Semester=C.Semester) as NStudents, C.[Begin], C.[End], Vl.HostID, (select distinct productcode +',' as 'data()' from calSignup where  EventID=C.EventId  and MemberID=c.memberId and accepted='Y' and Eventyear not in (select max(EventYear) from EventFees where EventId=13 ) FOR XML PATH('')) as ExpInProducts FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on(Vl.Vroom=C.Vroom) where C.EventYear=" + ddlEventyearFilter.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + ""
                If DG.ID = "DGCoachNA" Then
                    'Added by Bindhu
                    StrSql = StrSql & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventyearFilter.SelectedValue & ") "
                End If
                If ddlAcceptedFilter.SelectedValue = "Y" Then
                    StrSql = StrSql & " and C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' "
                ElseIf ddlAcceptedFilter.SelectedValue = "N" Then
                    StrSql = StrSql & " and (C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' or C.Accepted is null) "
                End If

                If (ddlPhase.SelectedValue <> "0") Then
                    StrSql = StrSql & " and C.Semester='" & ddlPhase.SelectedValue & "' "
                End If
                StrSql = StrSql & "group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName, I.Email,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,C.[Begin], C.[End], Vl.HostID order by I.LastName, I.FirstName ,C.ProductGroupId, C.ProductId,C.Semester, C.[Level]  asc"
            Else
                dvSearchSignups.Visible = True
                Dim Semester As String = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
                If (ddlPhaseFilter.SelectedValue = "0") Then
                    Semester = ddlPhase.SelectedValue
                Else
                    Semester = ddlPhaseFilter.SelectedValue
                End If
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, I.Email, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventyearFilter.SelectedValue & ") as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo and Semester=C.Semester) as NStudents, C.[Begin], C.[End], Vl.HostID, (select distinct productcode  +',' as 'data()' from calSignup where  EventID=C.EventId  and MemberID=c.memberId and accepted='Y' and Eventyear not in (select max(EventYear) from EventFees where EventId=13 ) FOR XML PATH('')) as ExpInProducts  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on (Vl.Vroom=C.Vroom) where  C.EventYear=" & ddlEventyearFilter.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & productGroup & ")" & IIf(product <> "", " and C.ProductID in (" & product & ") ", ""), "")
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & ""
                If DG.ID = "DGCoachNA" Then
                    'Added by Bindhu
                    StrSql = StrSql & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventyearFilter.SelectedValue & " and Semester='" & Semester & "') "
                End If
                If level <> "Select Level" And level <> "" And level <> "No Level found" Then
                    StrSql = StrSql & " And C.Level = '" & level & "'"
                End If
                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(productGroup = "", "", " AND C.ProductGroupID=" & productGroup) & IIf(product <> "", " and C.ProductID=" & product, "")
                End If
                If Request.QueryString("Role") = "Coach" Then
                    StrSql = StrSql & IIf(coachname = "", "", " AND C.MemberID=" & coachname)
                End If
                If ddlProductGroupFilter.SelectedValue <> "Select" Then
                    StrSql = StrSql & "  and C.ProductGroupID='" & ddlProductGroupFilter.SelectedValue & "'"
                End If

                If (ddlProductFilter.SelectedValue <> "Select") Then
                    StrSql = StrSql & "  and C.ProductID='" & ddlProductFilter.SelectedValue & "'"
                End If

                If (ddlLevelFilter.SelectedValue <> "Select") Then
                    StrSql = StrSql & "  and C.Level='" & ddlLevelFilter.SelectedValue & "'"
                End If

                If (ddlPhaseFilter.SelectedValue <> "0") Then
                    StrSql = StrSql & "  and C.Semester='" & ddlPhaseFilter.SelectedValue & "'"
                End If

                If (ddlCoachFilter.SelectedValue <> "Select") Then
                    StrSql = StrSql & "  and C.memberID='" & ddlCoachFilter.SelectedValue & "'"
                End If

                If (ddlSessionFilter.SelectedValue <> "Select") Then
                    StrSql = StrSql & "  and C.SessionNo='" & ddlSessionFilter.SelectedValue & "'"
                End If

                If (ddlDayFilter.SelectedValue <> "0") Then
                    StrSql = StrSql & "  and C.Day='" & ddlDayFilter.SelectedValue & "'"
                End If

                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and C.MemberID=" & Request.QueryString("coachID") & ""
                End If



                If ddlAcceptedFilter.SelectedValue = "Y" Then
                    StrSql = StrSql & " and C.Accepted='" & ddlAcceptedFilter.SelectedValue & "'"
                ElseIf ddlAcceptedFilter.SelectedValue = "N" Then
                    StrSql = StrSql & " and (C.Accepted='" & ddlAcceptedFilter.SelectedValue & "' or C.Accepted is null)"
                End If

                StrSql = StrSql & " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,I.Email,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD, C.[Begin], C.[End], VL.hostID order by I.LastName, I.FirstName, ProductGroupID, ProductID,Semester, Level Asc"
            End If

            If ddlEvent.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                If DG.ID = "DGCoachNA" Then
                    Session("volDataSet_NA") = ds
                Else
                    Session("volDataSet") = ds
                End If
                If ds.Tables(0).Rows.Count > 0 Then
                    btnExport.Visible = True
                    btnConfirm.Visible = True

                    btnExportTbl1a.Visible = True
                    btnExportTbl1a.Visible = True
                    Dim cmdUniqueCoachCount As String = String.Empty
                    cmdUniqueCoachCount = "select COUNT(distinct(MemberID)) from CalSignUp where EventYear=" & ddlEventYear.SelectedValue & ""
                    Dim uniqueCount As Integer = 0
                    Try
                        uniqueCount = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdUniqueCoachCount)
                    Catch ex As Exception
                        uniqueCount = 0
                    End Try
                    If DG.ID <> "DGCoachNA" Then
                        lblUniqueCount.Text = "Unique Coaches:" & uniqueCount
                        lblerr.Text = ""
                        spnTableTitle.Visible = True
                    End If
                    DG.Visible = True
                    DG.DataSource = ds
                    DG.DataBind()

                    ViewState("dtbl") = ds.Tables(0)

                    If (ds.Tables(0).Rows.Count > 0) Then
                    Else
                        If DG.ID = "DGCoachNA" Then
                            lblTable1ANR.Text = "No record exists"
                        Else
                            ' lblErrTable1a.Text = "No record exists"
                        End If

                    End If
                    For i As Integer = 0 To DGCoach.Items.Count - 1
                        Dim HostUrl As LinkButton = Nothing
                        Dim joinButton As Button = Nothing
                        'HostUrl = CType(DGCoach.Items(i).FindControl("lnkMeetingURL"), LinkButton)
                        'joinButton = CType(DGCoach.Items(i).FindControl("btnJoin"), Button)
                        HostUrl = CType(DG.Items(i).FindControl("lnkMeetingURL"), LinkButton)
                        joinButton = CType(DG.Items(i).FindControl("btnJoin"), Button)
                        If (HostUrl.Text <> "") Then
                            joinButton.Visible = True
                        Else
                            joinButton.Visible = False
                        End If
                    Next
                Else
                    DG.Visible = False
                    btnConfirm.Visible = False

                    btnExportTbl1a.Visible = False
                    spnTableTitle.Visible = True
                    If Page.IsPostBack = True Then
                        ' lblerr.Text = "No schedule available"
                        If (DG.ID <> "DGCoachNA") Then
                            lblTable1NR.Text = "No record exists"
                            btnExport.Visible = False
                        Else
                            lblTable1ANR.Text = "No record exists"
                            btnExportTbl1a.Visible = False
                        End If

                    End If
                End If

            Else
                lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Private Function GetTimeFromString(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue).AddMinutes(addMinute)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function
    Private Function GetTimeFromStringSubtract(timeString As String, addMinute As Integer) As TimeSpan
        Dim dateWithTime As DateTime = (DateTime.MinValue)
        DateTime.TryParse(timeString, dateWithTime)
        Return dateWithTime.AddMinutes(addMinute).TimeOfDay
    End Function



#Region ">>>>>>>>Coach<<<<<<<<"

    Protected Sub DGCoach_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) 'Handles DGCoach.ItemCommand
        Dim cmdName As String = e.CommandName
        Dim s As String = e.CommandSource.ToString
        If (cmdName = "Delete") Then
            Dim SignUpID As Integer
            SignUpID = CInt(e.Item.Cells(3).Text)
            Dim rowsAffected As Integer
            Try
                rowsAffected = SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM CalSignUp WHERE SignUpID =" + CStr(SignUpID))
                If rowsAffected > 0 Then
                    lblError.Text = "Deleted Successfully."
                End If
            Catch se As SqlException
                lblError.Text = "Error: while deleting the record"
                Return
            End Try

            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim level As String = String.Empty
            Dim coach As String = String.Empty
            'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            '    prdGroup = ddlProductGroup.SelectedValue
            'End If
            'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            '    prdID = ddlProduct.SelectedValue
            'End If
            'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
            '    level = ddlLevel.SelectedValue
            'End If
            'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            '    coach = ddlVolName.SelectedValue
            'End If
            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                level = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If
            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, level, coach, source)

            DGCoach.EditItemIndex = -1
            'Added by Bindhu for Not accepted coaches
            DGCoachNA.EditItemIndex = -1

            FillCoaches(DGCoachNA)

            FillCoaches(DGCoach)

        ElseIf (cmdName = "SelectMeetingURL") Then
            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblSessionsKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Dim hostID As String = CType(e.Item.FindControl("lblStHostID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMeetingURL"), LinkButton)

            Dim coachName As String = CType(e.Item.FindControl("lblMemberID"), Label).Text
            Dim beginTime As String = CType(e.Item.FindControl("lblTime"), Label).Text
            Dim day As String = CType(e.Item.FindControl("lblDay"), Label).Text
            Dim prodcutID As String = CType(e.Item.FindControl("lblStProductID"), Label).Text
            Dim dtFromS As New DateTime()
            Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
            Dim mins As Double = 40.0
            If DateTime.TryParse(beginTime, dtFromS) Then
                Dim TS As TimeSpan = dtFromS - dtEnds
                mins = TS.TotalMinutes
            End If
            Dim today As String = DateTime.Now.DayOfWeek.ToString()

            Dim strLogmsg As String = String.Empty
            strLogmsg += DateTime.Now + "\n"
            strLogmsg += coachName + "\n"
            Dim strValidationStatus As String = String.Empty

            Dim iduration As Integer = 0
            Dim duration As String = String.Empty
            Dim cmdText As String = String.Empty
            cmdText = "select duration from EventFees where EventYear=" & ddlEventYear.SelectedValue & " and EventID=13 and ProductID=" & prodcutID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    iduration = Convert.ToDouble(ds.Tables(0).Rows(0)("Duration").ToString())
                    If iduration = 1 Then
                        duration = "-60"
                    ElseIf iduration = 1.5 Then
                        duration = "-90"
                    ElseIf iduration = 2 Then
                        duration = "-120"
                    ElseIf iduration = 2.5 Then
                        duration = "-150"
                    Else
                        duration = "-180"
                    End If
                End If
            End If

            If mins <= 30 AndAlso day = today Then
                Try
                    iduration = Convert.ToInt32(duration)
                Catch ex As Exception
                    CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                End Try
                If (mins < iduration) Then
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showmsg();", True)
                Else
                    hdnHostID.Value = hostID
                    Try
                        listLiveMeetings()
                    Catch ex As Exception

                    End Try
                    getmeetingIfo(SessionKey, hostID)
                    strValidationStatus = "Success"
                    strLogmsg += strValidationStatus + "\n"
                    writeToLogFile(strLogmsg)
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "JoinMeeting();", True)
                End If

            Else

                strValidationStatus = "Fails"
                strLogmsg += strValidationStatus + "\n"
                strLogmsg += DateTime.Now + "\n"
                writeToLogFile(strLogmsg)
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showmsg();", True)

            End If

        ElseIf (cmdName = "SelectMakeUpURL") Then

            Dim SessionKey As String = String.Empty
            Dim UserID As String = String.Empty
            Dim Pwd As String = String.Empty
            Dim HostUrl As LinkButton = Nothing
            SessionKey = CType(e.Item.FindControl("lblMakeupKey"), Label).Text
            UserID = CType(e.Item.FindControl("lblDGUserID"), Label).Text

            Pwd = CType(e.Item.FindControl("lblDGPWD"), Label).Text
            HostUrl = CType(e.Item.FindControl("lnkMakeUpMeetingURL"), LinkButton)
            GenerateURL(SessionKey, Pwd, UserID)

        End If
    End Sub

    Protected Sub DGCoach_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) ' Handles DGCoach.EditCommand
        Dim DG As DataGrid = CType(source, DataGrid)
        hdInAppr.Value = ""
        hdToAppvId.Value = ""
        HdnVRoom.Value = ""
        hdnSessionNo.Value = ""
        hdnBeginTime.Value = ""
        hdnDay.Value = ""
        DGCoach.EditItemIndex = -1
        DGCoachNA.EditItemIndex = -1
        If (DG.ID = "DGCoachNA") Then
            FillCoaches(DGCoach)
        Else
            FillCoaches(DGCoachNA)
        End If
        Cache("Source") = source
        source.EditItemIndex = CInt(e.Item.ItemIndex)
        'Dim strEventCode As String
        Dim vDS As DataSet = Nothing
        If DG.ID = "DGCoachNA" Then
            vDS = CType(Session("volDataSet_NA"), DataSet)
        Else
            vDS = CType(Session("volDataSet"), DataSet)
        End If
        Dim page As Integer = source.CurrentPageIndex
        Dim pageSize As Integer = source.PageSize
        Dim currentRowIndex As Integer
        lblerr.Text = ""
        lblError.Text = ""
        hdnItemIndex.Value = e.Item.ItemIndex
        If (page = 0) Then
            currentRowIndex = e.Item.ItemIndex
        Else
            currentRowIndex = (page * pageSize) + e.Item.ItemIndex
        End If

        Dim vDSCopy As DataSet = vDS.Copy
        Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
        'Session("editRow") = dr
        Cache("editRow") = dr
        hdnOldVroom.Value = DirectCast(e.Item.FindControl("lblDGVRoom"), Label).Text
        Dim signupID As String = CInt(e.Item.Cells(3).Text)
        Dim memberID As String = DirectCast(e.Item.FindControl("lblCoachID"), Label).Text
        Dim Semester As String = DirectCast(e.Item.FindControl("lblSemester"), Label).Text

        'Modify CoachClass
        Try
            ' Dim cmdtext As String = "select  SignupID from CalSignup where eventyear = " & ddlEventYear.SelectedValue & " and memberID=" & memberID & " order by ProductGroupId, ProductId, Semester , [Level] asc"
            'Added on Aug25,2017 By Bindhu to get index of the editable row as seen in the grid table.
            Dim cmdtext As String = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, I.Email, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo and Semester=C.Semester) as NStudents, C.[Begin], C.[End], Vl.HostID,  (select distinct productcode +',' as 'data()' from calSignup where  EventID=C.EventId  and MemberID=c.memberId and accepted='Y' and Eventyear not in (select max(EventYear) from EventFees where EventId=13 ) FOR XML PATH('')) as ExpInProducts FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on(Vl.Vroom=C.Vroom) where C.EventYear=" & ddlEventYear.SelectedValue & " AND C.EventID=13 and C.MemberID=" & memberID & " and C.Semester='" & Semester & "' "
            If source.ID = "DGCoachNA" Then
                'Added by Bindhu
                cmdtext = cmdtext & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventYear.SelectedValue & ") "
            End If
            cmdtext = cmdtext & " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName, I.Email,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,C.[Begin], C.[End], Vl.HostID order by I.LastName, I.FirstName, C.ProductGroupId, C.ProductId, C.Semester, C.[Level] Asc "

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdtext)
            Dim i As Integer = 0
            Dim editIndex As Integer = 0
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr1 As DataRow In ds.Tables(0).Rows
                        If signupID = dr1("SignupID").ToString() Then
                            editIndex = i
                        End If
                        i = i + 1
                    Next
                End If
            End If
            source.EditItemIndex = editIndex
            loadGridBasedOnmemberID(memberID, signupID, source, Semester)
        Catch ex As Exception
            source.EditItemIndex = CInt(e.Item.ItemIndex)

            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim level As String = String.Empty
            Dim coach As String = String.Empty
            'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            '    prdGroup = ddlProductGroup.SelectedValue
            'End If
            'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            '    prdID = ddlProduct.SelectedValue
            'End If
            'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
            '    level = ddlLevel.SelectedValue
            'End If
            'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            '    coach = ddlVolName.SelectedValue
            'End If
            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                level = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If
            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, level, coach, source)
        End Try
    End Sub

    Protected Sub DGCoach_ItemCreated(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.DataGridItemEventArgs) ' Handles DGCoach.ItemCreated
        If (e.Item.ItemType = ListItemType.Item Or e.Item.ItemType = ListItemType.AlternatingItem Or e.Item.ItemType = ListItemType.EditItem) Then
            Dim btn As LinkButton = Nothing
            Dim Edtbtn As LinkButton = Nothing
            btn = CType(e.Item.Cells(1).Controls(0), LinkButton)
            If Session("RoleID") = 88 Then
                Dim flag As Object = DataBinder.Eval(e.Item.DataItem, "Accepted")
                If Not flag Is DBNull.Value Then
                    Edtbtn = CType(e.Item.Cells(0).Controls(0), LinkButton)
                    Edtbtn.Enabled = False
                    btn.Enabled = False
                Else
                    btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
                End If
            Else
                btn.Attributes.Add("onclick", "return confirm('Are you sure you want to delete this record?');")
            End If
        End If
    End Sub

    Protected Sub DGCoach_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) 'Handles DGCoach.UpdateCommand

        Dim sqlStr As String
        Dim row As Integer = CInt(e.Item.ItemIndex)
        'read the values
        Dim SignUpID As Integer
        Dim MemberID As Integer
        Dim EventID As Integer
        Dim EventYear As Integer
        Dim Capacity As Integer
        Dim SessionNo As Integer
        Dim ProductID As Integer
        Dim Level As String
        Dim Day As String
        Dim Time As String
        Dim Preference As Integer
        Dim Accepted As String
        Dim Confirm As String
        Dim UserID As String
        Dim PwD As String
        Dim Vroom As Integer
        Dim hostID As String = String.Empty
        Dim sessionkey As String = String.Empty
        Dim semester As String = String.Empty
        Try
            SignUpID = CInt(e.Item.Cells(3).Text)
            'MemberID = CInt(CType(e.Item.FindControl("txtMemberID"), TextBox).Text)
            EventYear = ddlEventYear.SelectedValue
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                ProductID = dr.Item("productID")
                MemberID = dr.Item("MemberID")
                EventID = dr.Item("EventID")
            End If
            'And CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Enabled = True
            If CType(e.Item.FindControl("ddlDGLevel"), DropDownList).Items.Count > 0 Then
                Level = CStr(CType(e.Item.FindControl("ddlDGLevel"), DropDownList).SelectedItem.Text)
            Else
                Level = ""
            End If
            If (Level = "Select Level" Or Level = "No Level found") Then
                lblerr.Text = "Please select Level"
                Exit Sub
            End If
            SessionNo = CInt(CType(e.Item.FindControl("ddlDGSessionNo"), DropDownList).SelectedItem.Value)
            Capacity = CInt(CType(e.Item.FindControl("ddlDGMaxCapacity"), DropDownList).SelectedItem.Value)
            Time = CStr(CType(e.Item.FindControl("ddlDGTime"), DropDownList).SelectedItem.Text)
            Day = CStr(CType(e.Item.FindControl("ddlDGDay"), DropDownList).SelectedItem.Text) 'Value)
            hostID = CStr(CType(e.Item.FindControl("lblStrHostID"), Label).Text) 'Value)
            ' semester = CStr(CType(e.Item.FindControl("lblPhase"), Label).Text) 'Value) 
            semester = CStr(CType(e.Item.FindControl("ddlDGPhase"), DropDownList).SelectedValue) 'Value) 

            sessionkey = CStr(CType(e.Item.FindControl("lblSessionsKey"), Label).Text) 'Value)
            hdnSessionKey.Value = sessionkey

            Preference = CInt(CType(e.Item.FindControl("ddlDGPreferences"), DropDownList).SelectedItem.Value)
            If CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).Enabled = True Then
                If hdInAppr.Value <> "" Then
                    CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedIndex = 1
                End If
                Accepted = CStr(CType(e.Item.FindControl("ddlDGAccepted"), DropDownList).SelectedItem.Value)
                If Accepted = "" Then
                    Accepted = "NULL"
                End If
            Else
                Accepted = "NULL"
            End If

            If CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).Enabled = True Then
                If hdInAppr.Value <> "" Then
                    CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).SelectedIndex = 1
                End If
                Confirm = CStr(CType(e.Item.FindControl("ddlDGConfirm"), DropDownList).SelectedItem.Value)
                If Confirm = "" Then
                    Confirm = "NULL"
                End If
            Else
                Confirm = "NULL"
            End If
            UserID = CStr(CType(e.Item.FindControl("txtDGUserID"), TextBox).Text)
            PwD = CStr(CType(e.Item.FindControl("txtDGPWD"), TextBox).Text)
            Vroom = 0
            If CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).Items.Count > 0 Then
                If CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).SelectedItem.Text.ToLower <> "select" Then
                    Vroom = CInt(CType(e.Item.FindControl("ddlDGVRoom"), DropDownList).SelectedItem.Value)
                End If
            End If

            Dim cmdText As String = String.Empty
            Dim iduration As Decimal = 0
            cmdText = "select duration from EventFees where EventYear=" & EventYear & " and EventID=13 and ProductID=" & ProductID & ""
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    iduration = Convert.ToDecimal(ds.Tables(0).Rows(0)("Duration").ToString())
                End If
            End If

            Dim cmdVroomtext As String = " Select UserId, Pwd from VirtualRoomLookUp where Vroom=" & Vroom & ""
            Dim ZoomUserId As String = "NULL"
            Dim ZoomPwd As String = "NULL"
            If (Vroom > 0) Then
                Dim dsVroom As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdVroomtext)
                If dsVroom.Tables.Count > 0 Then
                    If dsVroom.Tables(0).Rows.Count > 0 Then
                        If (dsVroom.Tables(0).Rows(0)("UserId").ToString() <> "") Then
                            ZoomUserId = dsVroom.Tables(0).Rows(0)("UserId").ToString()
                            ZoomPwd = dsVroom.Tables(0).Rows(0)("Pwd").ToString()
                        End If
                    End If
                End If
            End If

            Dim dtCurrent As DateTime = DateTime.Now
            Dim strCurrentDate As String = DateTime.Now.ToShortDateString()

            strCurrentDate = strCurrentDate + " " + Time
            Dim dtStartTime As DateTime
            Dim dtEndTime As DateTime
            Dim strSTartTime As String = String.Empty
            Dim strEndTime As String = String.Empty
            dtStartTime = Convert.ToDateTime(strCurrentDate)
            dtStartTime = dtStartTime.AddMinutes(-30)
            dtEndTime = Convert.ToDateTime(strCurrentDate)
            dtEndTime = dtEndTime.AddHours(iduration)

            strSTartTime = dtStartTime.ToString("HH:mm:ss")
            strEndTime = dtEndTime.ToString("HH:mm:ss")


            If SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and Semester='" & ddlPhase.SelectedValue & "' and Eventyear=" & EventYear & " and EventID=" & EventID & " and Day='" & Day & "' and Time='" & Time & "' and  SignUpID not in (" & SignUpID & ")") > 0 Then
                lblError.Text = " Coach is already scheduled for this Period.Please modify the Day or Time."
            ElseIf SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select count(*) from CalSignUp where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Semester='" & semester & "' and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ")") < 3 Then

                sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & " ,Confirm=" & IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL") & " ,Preference=" & Preference & ",MaxCapacity =" & Capacity & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & ",[Begin]='" & strSTartTime & "', [End]='" & strEndTime & "', UserId=" & IIf(ZoomUserId <> "NULL", "'" & ZoomUserId & "'", "NULL") & ", Pwd=" & IIf(ZoomPwd <> "NULL", "'" & ZoomPwd & "'", "NULL") & " WHERE SignUpID=" & SignUpID
                If (Accepted <> "NULL") Then

                    Dim dsAppr As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring"), CommandType.Text, "select signupid,day,time from CalSignup where Memberid=" & MemberID & " and eventyear=" & EventYear & " and Semester='" & semester & "' and ProductID=" & ProductID & IIf(Level <> "", " and Level ='" & Level & "'", "") & " AND SessionNo=" & SessionNo & " and  SignUpID not in (" & SignUpID & ") and Accepted='Y'")
                    If dsAppr.Tables(0).Rows.Count > 0 Then
                        'session
                        If hdInAppr.Value = "" Then
                            hdInAppr.Value = dsAppr.Tables(0).Rows(0).Item("SignupId")
                            hdToAppvId.Value = SignUpID
                            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "ConfirmUpdate(" & SessionNo & ", '" & dsAppr.Tables(0).Rows(0).Item("Day").ToString() & "','" & dsAppr.Tables(0).Rows(0).Item("Time").ToString() & "','" & e.CommandSource.Id & "');", True)
                            Exit Sub
                        Else
                            sqlStr = "UPDATE CalSignUp SET Accepted=null,ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & " WHERE SignUpID=" & hdInAppr.Value
                            hdInAppr.Value = ""
                            hdToAppvId.Value = ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)
                        End If

                    End If
                End If
                Dim strQuery As String = "UPDATE CalSignUp SET MemberID = @MemberID,EventYear = @EventYEar,ProductID = @ProductID,ProductCode = @ProuctCode,[Level] =@Level ,SessionNo=@SessionNo,Day=@Day,Time = @Time,Accepted=@Accepted,Confirm=@Confirm,Preference=@Preference,MaxCapacity =@MaxCapacity,UserID=@UserID,PWD=@Pwd,Vroom=@Vroom,ModifiedDate = @ModifyDate,ModifiedBy = @LoginID,[Begin]=@BeginTime, [End]=@EndTime, UserId=@userId, Pwd=@Pwd WHERE SignUpID=@SignupID"

                Dim cmd As New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@MemberID", MemberID)
                cmd.Parameters.AddWithValue("@EventYEar", EventYear)
                cmd.Parameters.AddWithValue("@ProductID", ProductID)
                cmd.Parameters.AddWithValue("@ProuctCode", getProductcode(ProductID))
                cmd.Parameters.AddWithValue("@Level", IIf(Level <> "", "'" & Level & "'", "NULL"))
                cmd.Parameters.AddWithValue("@SessionNo", SessionNo)
                cmd.Parameters.AddWithValue("@Day", Day)
                cmd.Parameters.AddWithValue("@Time", Time)
                cmd.Parameters.AddWithValue("@Accepted", IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL"))
                cmd.Parameters.AddWithValue("@Confirm", IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL"))
                cmd.Parameters.AddWithValue("@Preference", Preference)
                cmd.Parameters.AddWithValue("@MaxCapacity", Capacity)
                cmd.Parameters.AddWithValue("@UserID", IIf(UserID = "", "NULL", "'" & UserID & "'"))
                cmd.Parameters.AddWithValue("@Pwd", IIf(PwD = "", "NULL", "'" & PwD & "'"))
                cmd.Parameters.AddWithValue("@Vroom", IIf(Vroom <= 0, "NULL", Vroom))
                cmd.Parameters.AddWithValue("@ModifyDate", DateTime.Now.ToShortDateString())
                cmd.Parameters.AddWithValue("@LoginID", Session("LoginID"))
                cmd.Parameters.AddWithValue("@BeginTime", strSTartTime)
                cmd.Parameters.AddWithValue("@EndTime", strEndTime)
                cmd.Parameters.AddWithValue("@userId", IIf(ZoomUserId <> "NULL", "'" & ZoomUserId & "'", "NULL"))
                cmd.Parameters.AddWithValue("@Pwd", IIf(ZoomPwd <> "NULL", "'" & ZoomPwd & "'", "NULL"))
                cmd.Parameters.AddWithValue("@SignupID", SignUpID)
                Dim objNSF As NSFDBHelper = New NSFDBHelper()
                ' SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)

                If objNSF.InsertUpdateData(cmd) = True Then
                    lblError.Text = "Updated Successfully"
                Else
                    sqlStr = "UPDATE CalSignUp SET MemberID = " & MemberID & ",EventYear = " & EventYear & ",ProductID = " & ProductID & ",ProductCode = '" & getProductcode(ProductID) & "',[Level] =" & IIf(Level <> "", "'" & Level & "'", "NULL") & ",SessionNo=" & SessionNo & ",Day='" & Day & "',Time = '" & Time & "',Accepted=" & IIf(Accepted <> "NULL", "'" & Accepted & "'", "NULL") & ",Confirm=" & IIf(Confirm <> "NULL", "'" & Confirm & "'", "NULL") & ",Preference=" & Preference & ",MaxCapacity =" & Capacity & ",Vroom=" & IIf(Vroom <= 0, "NULL", Vroom) & ",ModifiedDate = Getdate(),ModifiedBy = " & Session("loginID") & ",[Begin]='" & strSTartTime & "', [End]='" & strEndTime & "', UserId=" & IIf(ZoomUserId <> "NULL", "'" & ZoomUserId & "'", "NULL") & ", Pwd=" & IIf(ZoomPwd <> "NULL", "'" & ZoomPwd & "'", "NULL") & " WHERE SignUpID=" & SignUpID

                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, sqlStr)
                    lblError.Text = "Updated Successfully"

                    'Cancel Zoom Session

                    If (HdnVRoom.Value = hdnOldVroom.Value) Then
                    Else
                        Try
                            Dim cmdDateText As String = String.Empty
                            cmdDateText = "select StartDate from CoachingDateCal where EventYear=" & EventYear & " and productId=" & ProductID & " and Semester='" + semester + "'"
                            Dim startDate As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, cmdDateText)
                            If (startDate <> "") Then
                                Dim dtStartDate As DateTime = Convert.ToDateTime(startDate)
                                Dim dtCurrentDate As DateTime = Convert.ToDateTime(strCurrentDate)
                                If (dtStartDate > dtCurrentDate) Then
                                    DeleteMeeting(hdnSessionKey.Value, hostID)
                                    SwitchStudentAndCreateTrainingSession(Session("LoginID").ToString(), SignUpID)
                                End If
                            End If

                        Catch ex As Exception

                        End Try
                    End If
                End If
                DGCoach.EditItemIndex = -1
                DGCoachNA.EditItemIndex = -1
                Dim prdGroup As String = String.Empty
                Dim prdID As String = String.Empty
                Dim strlevel As String = String.Empty
                Dim coach As String = String.Empty
                'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
                '    prdGroup = ddlProductGroup.SelectedValue
                'End If
                'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
                '    prdID = ddlProduct.SelectedValue
                'End If
                'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
                '    strlevel = ddlLevel.SelectedValue
                'End If
                'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
                '    coach = ddlVolName.SelectedValue
                'End If
                If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                    prdGroup = ddlProductGroupFilter.SelectedValue
                End If
                If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                    prdID = ddlProductFilter.SelectedValue
                End If
                If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                    strlevel = ddlLevelFilter.SelectedValue
                End If
                If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                    coach = ddlCoachFilter.SelectedValue
                End If

                LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
                LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

            Else
                lblError.Text = "Coach with same product, Semester, Level and Session are already found"
            End If
        Catch ex As SqlException
            'ex.message
            'Response.Write(ex.ToString())
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            'Return
        End Try
    End Sub

    Protected Sub DGCoach_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) 'Handles DGCoach.CancelCommand

        Dim DG As DataGrid = source
        Dim ds As DataSet
        If DG.ID = "DGCoachNA" Then
            ds = CType(Session("volDataSet_NA"), DataSet)
        Else
            ds = CType(Session("volDataSet"), DataSet)
        End If

        lblerr.Text = ""
        lblError.Text = ""
        If (Not ds Is Nothing) Then

            Dim dsCopy As DataSet = ds.Copy
            Dim page As Integer = DGCoach.CurrentPageIndex
            Dim pageSize As Integer = DGCoach.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
            DG.EditItemIndex = -1
            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim strlevel As String = String.Empty
            Dim coach As String = String.Empty


            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                strlevel = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If

            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DG)

        Else
            Return
        End If
    End Sub


    Public Sub ddlDaytime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim DG As DataGrid = CType(Cache("Source"), DataGrid)

        If (Not dr Is Nothing) Then
            dr.Item("Day") = ddlTemp.SelectedItem.Text
            Dim day As String = ddlTemp.SelectedValue
            Dim startTime As String = dr.Item("Time").ToString()
            hdnDay.Value = day
            Dim cmdText As String = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            Dim Duration As Decimal
            If ds.Tables(0).Rows.Count > 0 Then
                Try
                    Duration = Convert.ToDecimal(ds.Tables(0).Rows(0)("Duration").ToString())
                Catch ex As Exception
                End Try

                If Duration = 1 Then
                    Duration = 60
                ElseIf Duration = 1.5 Then
                    Duration = 90
                ElseIf Duration = 2 Then
                    Duration = 120
                Else
                    Duration = 180
                End If
            End If
            Try
                Dim ddlVRoom As DropDownList = CType(DG.Items(hdnItemIndex.Value).FindControl("ddlDGVRoom"), DropDownList)
                checkAvailableVrooms(Duration, startTime, day, ddlVRoom, dr.Item("SignupID").ToString(), lblerr)
            Catch ex As Exception
                CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            End Try
            Cache("editRow") = dr


        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEventYear_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            ddlTemp.Items.Insert(0, Convert.ToString(Now.Year() + 1))
            ddlTemp.Items.Insert(1, Convert.ToString(Now.Year()))
            ddlTemp.Items.Insert(2, Convert.ToString(Now.Year() - 1))

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("EventYear")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If

    End Sub

    Public Sub ddlDGMember_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim strSql As String
        If Request.QueryString("Role") = "Coach" Then
            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I Inner Join CalSignUp C On C.MemberID = I.AutoMemberID order by I.FirstName"
        Else
            'strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I,Volunteer V where V.RoleId in (1,2,88,89) and v.MemberID = I.AutoMemberID order by I.FirstName"

            strSql = "select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.Firstname from IndSpouse I inner join CalSignup C on C.MemberId  = I.AutoMemberID  order by I.FirstName"

        End If

        Dim drcoach As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drcoach = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
        ddlTemp.DataSource = drcoach
        ddlTemp.DataBind()
        Dim rowMemberId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
            rowMemberId = dr.Item("MemberId")
        End If
        ddlTemp.SelectedValue = rowMemberId
        ' ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowMemberId))
        If Session("RoleID") = 88 Then 'Request.QueryString("Role") = "Coach"Then
            ddlTemp.Enabled = False
        End If
    End Sub

    Public Sub ddlDGMember_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            dr.Item("MemberID") = ddlTemp.SelectedValue
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGEvent_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim EventCode As String = ""
        Dim EventId As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("EventCode") Is DBNull.Value) Then
                EventCode = dr.Item("EventCode")
                EventId = dr.Item("EventId")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadEvent(ddlTemp)
            Dim rowEventID As Integer = 0
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(EventId))
        Else
            Return
        End If

    End Sub

    Public Sub ddlDGPhase_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Semester As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Semester") Is DBNull.Value) Then
                Semester = dr.Item("Semester")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Semester))
        End If
    End Sub

    Public Sub ddlDGProductGroup_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProductGroupID As Integer = 0
        Dim rowEventID As Integer = 0

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        Try
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If

            'Dim strSql As String = "SELECT  Distinct ProductGroupID, Name from ProductGroup where EventId=" & rowEventID & "  order by ProductGroupID"
            Dim strSql As String = String.Empty
            strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventYear.SelectedValue & " AND  P.EventId=" & rowEventID & "  order by P.ProductGroupID"
            Dim drproductGroupid As SqlDataReader
            drproductGroupid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductGroupid
            ddlTemp.DataBind()
            If (Not dr.Item("productGroupID") Is DBNull.Value) Then
                rowProductGroupID = dr.Item("productGroupID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductGroupID))
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub ddlDGProduct_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim conn As New SqlConnection(Application("ConnectionString"))
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim rowProdGroupId As Integer = 0
        Dim rowEventID As Integer = 0
        Dim rowSemester As String = String.Empty

        Try
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr.Item("ProductGroupId") Is DBNull.Value) Then
                rowProdGroupId = dr.Item("productGroupId")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                rowEventID = dr.Item("EventID")
            End If
            If (Not dr.Item("Semester") Is DBNull.Value) Then
                rowSemester = dr.Item("Semester")
            End If
            Dim strSql As String = String.Empty
            strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventYear.SelectedValue & " AND P.EventID=" & rowEventID & " and P.ProductGroupID =" & rowProdGroupId & "  and Semester='" & rowSemester & "' order by P.ProductID"

            Dim drproductid As SqlDataReader
            drproductid = SqlHelper.ExecuteReader(conn, CommandType.Text, strSql)
            ddlTemp.DataSource = drproductid
            ddlTemp.DataBind()
            Dim rowProductID As Integer = 0
            If (Not dr.Item("productID") Is DBNull.Value) Then
                rowProductID = dr.Item("productID")
            End If
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(rowProductID))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub ddlDGProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            dr.Item("productID") = ddlTemp.SelectedValue
            dr.Item("ProductCode") = getProductcode(ddlTemp.SelectedValue)
            Cache("editRow") = dr
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGLevel_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim level As String = ""
        Dim ProductGroupCode As String = ""
        Dim ProductGroupID As String
        Dim ProductID As String
        Dim EventID As String
        Dim EventYear As String
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Level") Is DBNull.Value) Then
                level = dr.Item("Level")
            End If
            If (Not dr.Item("ProductGroupCode") Is DBNull.Value) Then
                ProductGroupCode = dr.Item("ProductGroupCode")
                ProductGroupID = dr.Item("ProductGroupID")
            End If
            If (Not dr.Item("ProductID") Is DBNull.Value) Then
                ProductID = dr.Item("ProductID")
            End If
            If (Not dr.Item("EventID") Is DBNull.Value) Then
                EventID = dr.Item("EventID")
            End If
            If (Not dr.Item("EventYear") Is DBNull.Value) Then
                EventYear = dr.Item("EventYear")
            End If

            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            LoadLevel(ddlTemp, ProductGroupID, ProductID, EventID, EventYear)

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(level))

            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            ElseIf ProductGroupCode <> "UV" Then
                ddlTemp.Enabled = True
            End If

            Try

                Dim CmdAdult As String = "select count(*) from EventFees where Eventyear=" & EventYear & " and EventId=13 and ProductGroupId=" & ProductGroupID & " and productId=" & ProductID & " and Adult='Y' and GradeFrom = 13 and GradeTo=13"
                Dim Count As Integer = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, CmdAdult)
                If (Count > 0) Then
                    ddlTemp.SelectedValue = "Senior"
                    ddlTemp.Enabled = False
                End If
            Catch ex As Exception

            End Try

        Else
            Return
        End If
    End Sub

    Public Sub ddlDGSessionNo_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim SessionNo As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("SessionNo") Is DBNull.Value) Then
                SessionNo = dr.Item("SessionNo")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(SessionNo))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
            If hdnSessionNo.Value <> "" Then
                ddlTemp.SelectedValue = hdnSessionNo.Value
            End If
        Else
            Return
        End If
    End Sub
    Public Sub ddlDGDay_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)

        Dim Day As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If
            LoadWeekdays(ddlTemp)
        End If
        ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Day))
        If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
            ddlTemp.Enabled = False
        Else
            ddlTemp.Enabled = True
        End If
        If hdnDay.Value <> "" Then
            ddlTemp.SelectedValue = hdnDay.Value
        End If

    End Sub

    Public Sub ddlDGTime_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Day As String = ""

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            'LoadDisplayTime(ddlTemp, False)
            If (Not dr.Item("Day") Is DBNull.Value) Then
                Day = dr.Item("Day")
            End If

            If Day = "Saturday" Or Day = "Sunday" Then
                LoadDisplayTime(ddlTemp, False)
            Else
                LoadWeekDisplayTime(ddlTemp, False)
            End If

            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(dr.Item("Time")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If


            If hdnBeginTime.Value <> "" Then
                If ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(hdnBeginTime.Value)) > 0 Then
                    ddlTemp.SelectedValue = hdnBeginTime.Value
                End If
            End If
        End If
    End Sub

    Public Sub ddlDGAccepted_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Accepted As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        If (Not dr Is Nothing) Then
            If (Not dr.Item("Accepted") Is DBNull.Value) Then
                Accepted = dr.Item("Accepted")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Accepted))
            If Session("RoleID") = 88 Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub


    Public Sub ddlDGConfirm_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Confirm As String = ""
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Confirm") Is DBNull.Value) Then
                Confirm = dr.Item("Confirm")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Confirm))
            If Session("RoleID") = 88 Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGPreferences_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim Preference As Integer = 0
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)

        If (Not dr Is Nothing) Then
            If (Not dr.Item("Preference") Is DBNull.Value) Then
                Preference = dr.Item("Preference")
            End If
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Preference))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        Else
            Return
        End If
    End Sub

    Public Sub ddlDGMaxCapacity_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        If (Not dr Is Nothing) Then
            ddlTemp.Items.Clear()
            Dim li As ListItem
            Dim i As Integer
            For i = 0 To 35 Step 1 '10 To 100 Step 5
                li = New ListItem(i)
                ddlTemp.Items.Add(li)
            Next
            ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(dr.Item("MaxCapacity")))
            If Session("RoleID") = 88 And Not dr.Item("Accepted") Is DBNull.Value Then
                ddlTemp.Enabled = False
            Else
                ddlTemp.Enabled = True
            End If
        End If
    End Sub
    Protected Sub ddlDGVRoom_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            Dim cmdText As String = "select Vroom from VirtualRoomLookup"
            Dim ds As DataSet = New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                ddlTemp.DataValueField = "Vroom"
                ddlTemp.DataTextField = "Vroom"
                ddlTemp.DataSource = ds
                ddlTemp.DataBind()
            End If

            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            Dim Duration As Decimal
            Dim Day As String = String.Empty
            Day = dr.Item("Day").ToString()
            Dim strBeginTime As String = String.Empty
            Dim strEndTime As String = String.Empty
            strBeginTime = dr.Item("Time").ToString()
            If (hdnBeginTime.Value = "") Then
                strBeginTime = dr.Item("Time").ToString()
            Else
                strBeginTime = hdnBeginTime.Value
            End If
            strEndTime = dr.Item("End").ToString()

            cmdText = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0).Rows.Count > 0 Then
                Try
                    Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
                Catch ex As Exception

                End Try
                If Duration = 1 Then
                    Duration = 60
                ElseIf Duration = 1.5 Then
                    Duration = 90
                ElseIf Duration = 2 Then
                    Duration = 120
                Else
                    Duration = 180
                End If
            End If
            checkAvailableVrooms(Duration, strBeginTime, Day, ddlTemp, dr.Item("SignupID").ToString(), lblerr)
            If (HdnVRoom.Value <> "") Then
                Try
                    ddlTemp.SelectedValue = HdnVRoom.Value
                Catch ex As Exception
                End Try
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Protected Sub ddlDGVRoom_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Try
            Dim ddlTemp As System.Web.UI.WebControls.DropDownList
            ddlTemp = sender
            HdnVRoom.Value = ddlTemp.SelectedValue

            Dim cmd As String = "SELECT VROOM,UserId,Pwd FROM VirtualRoomLookUp where VRoom=" & ddlTemp.SelectedValue
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmd)

            If ds.Tables(0).Rows.Count > 0 Then
                HdnVroomUID.Value = ds.Tables(0).Rows(0)("UserId").ToString
                hdnVroomPwd.Value = ds.Tables(0).Rows(0)("Pwd").ToString
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub txtDGUserID_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        Dim Accepted As String = String.Empty
        Dim UserID As String = String.Empty
        Dim Pwa As String = String.Empty
        Accepted = dr.Item("Accepted").ToString()
        UserID = dr.Item("UserID").ToString()
        Pwa = dr.Item("Pwd").ToString()

        If (Accepted = "Y" And UserID = "" And Pwa = "") Then
            If (HdnVroomUID.Value <> "") Then
                txtTemp.Text = HdnVroomUID.Value
            Else
                txtTemp.Text = dr.Item("UserID").ToString()
            End If
        Else
            txtTemp.Text = dr.Item("UserID").ToString()
        End If

        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub

    Protected Sub txtDGPWD_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim txtTemp As System.Web.UI.WebControls.TextBox
        txtTemp = sender
        Dim Accepted As String = String.Empty
        Dim UserID As String = String.Empty
        Dim Pwa As String = String.Empty
        Accepted = dr.Item("Accepted").ToString()
        UserID = dr.Item("UserID").ToString()
        Pwa = dr.Item("Pwd").ToString()
        If (Accepted = "Y" And UserID = "" And Pwa = "") Then
            If (hdnVroomPwd.Value <> "") Then
                txtTemp.Text = hdnVroomPwd.Value
            Else
                txtTemp.Text = dr.Item("PWD").ToString()
            End If
        Else
            txtTemp.Text = dr.Item("PWD").ToString()
        End If
        If Session("RoleID") = 88 Then 'And Not dr.Item("Accepted") Is DBNull.Value
            txtTemp.Enabled = False
        Else
            txtTemp.Enabled = True
        End If
    End Sub


#End Region

    Private Sub clear()
        Try


            lblerr.Text = ""
            lblError.Text = ""
            ddlEvent.SelectedValue = ""
            ddlProductGroup.Items.Clear()
            ddlProductGroup.Enabled = False
            ddlProduct.Items.Clear()
            ddlProduct.Enabled = False
            'ddlProductGroup.SelectedIndex = 0
            ddlPhase.SelectedIndex = 0
            ddlSession.SelectedIndex = 0
            ddlWeekDays.SelectedIndex = 0
            ddlDisplayTime.SelectedIndex = 0

            If ddlLevel.Enabled = True Then
                ddlLevel.SelectedIndex = ddlLevel.Items.IndexOf(ddlLevel.Items.FindByValue(0))
            End If
            ddlMaxCapacity.SelectedIndex = ddlMaxCapacity.Items.IndexOf(ddlMaxCapacity.Items.FindByText("20"))
            ddlPref.SelectedIndex = 0
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlEventYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        ddlEvent.Items.Clear()
        ddlProductGroup.Items.Clear()
        DGCoach.Visible = False
        fillYearFiletr()

        ddlEventyearFilter.SelectedValue = ddlEventYear.SelectedValue
        loadProductGroupFilter()
        loadProductFilter()
        loadLevelFilter()
        loadCoachFilter()
        loadPhase()
        LoadEvent(ddlEvent)
        loadPhase()

        If (Session("RoleID").ToString() = "88") Then
            Dim OpenSemester As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select Max(Semester) from EventFees where CalSignupOpen='Y'")

            ' ddlPhase.SelectedValue = OpenSemester.Trim()
        End If
        'If ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex <> 0 Then
        'LoadProductGroup()
        'Else
        '   LoadProductGroup()
        'End If
        'If ddlVolName.SelectedIndex <> 0 Then
        '    LoadGrid()
        'End If

        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    strlevel = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

        '   LoadGrid(ddlAcceptedFilter.SelectedValue)
        LoadMakeUpSessions()
        LoadSubstituteSessions()
        fillGuestAttendeeGrid()
        LoadExtraSessions()
        LoadMakeupSubstituteSessions()
        LoadExtraSubstituteSessions()
    End Sub

    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        ddlProductGroup.Items.Clear()
        ddlProduct.Items.Clear()
        If ddlEvent.SelectedIndex <> 0 Then
            LoadProductGroup()
            LoadProductID()
        End If
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        LoadProductGroup()
        LoadProductID()
        If (Session("RoleID").ToString() = "88") Then

        End If

        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    strlevel = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
        LoadMakeUpSessions()
        LoadSubstituteSessions()
        LoadExtraSessions()
        LoadMakeupSubstituteSessions()
        LoadExtraSubstituteSessions()
        fillGuestAttendeeGrid()
        fillPractiseSession()

        'If ddlProduct.SelectedValue = "Select Product" Or ddlProduct.Items.Count = 0 Then
        '    lblerr.Text = "Please select Valid Product"
        'Else
        '    LoadGrid(ddlAcceptedFilter.SelectedValue)
        '    LoadMakeUpSessions()
        '    LoadSubstituteSessions()
        'End If
    End Sub

    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        '  If ddlProductGroup.SelectedIndex <> 0 Then
        LoadProductID()
        'LoadLevel(ddlLevel, ddlProductGroup.SelectedItem.Text)
        '' Commented by Ferdine
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    strlevel = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        ' hidden by Sims as per Praveen's comments
        ' LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        '  LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
        '  LoadGrid(ddlAcceptedFilter.SelectedValue)
        ' End If
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        lblerr.Text = ""
        lblError.Text = ""
        If ddlProduct.SelectedValue = "Select Product" Then
            lblerr.Text = "Please select Valid Product"
        Else
            Dim PgCode As String = ""
            Dim Cmdtext As String = " Select distinct productgroupid from Product where ProductId=" & ddlProduct.SelectedValue & ""
            PgCode = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, Cmdtext)
            ddlProductGroup.SelectedValue = PgCode
            ddlEventYear.SelectedValue = GetDefaultYear()
            ddlPhase.SelectedValue = GetDefaultSemester()
            '' Commented by Ferdine
            LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
            ' LoadGrid(ddlAcceptedFilter.SelectedValue)
        End If
        ' LoadLevel(ddlLevel, ddlProductGroup.SelectedValue, ddlProduct.SelectedValue, ddlEvent.SelectedValue, ddlEventYear.SelectedValue)
    End Sub

    Protected Sub ddlVolName_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlVolName.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""

        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    strlevel = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        ' LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        '  LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

        '        LoadGrid(ddlAcceptedFilter.SelectedValue)

        LoadMakeUpSessions()
        LoadSubstituteSessions()
        LoadExtraSessions()
        LoadMakeupSubstituteSessions()
        LoadExtraSubstituteSessions()
    End Sub

    Protected Sub btnSearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnSearch.Click
        pIndSearch.Visible = True
    End Sub

    Protected Sub btnSearch_onClick(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim firstName As String = String.Empty
        Dim lastName As String = String.Empty
        Dim email As String = String.Empty
        Dim strSql As New StringBuilder
        firstName = txtFirstName.Text
        lastName = txtLastName.Text
        email = txtEmail.Text
        Dim length As Integer = 0
        If firstName.Length > 0 Then
            strSql.Append("  I.firstName like '%" + firstName + "%'")
        End If
        If (lastName.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.lastName like '%" + lastName + "%'")
            Else
                strSql.Append("  I.lastName like '%" + lastName + "%'")
            End If
        End If
        If (email.Length > 0) Then
            length = strSql.ToString().Length
            If (length > 0) Then
                strSql.Append(" and I.Email like '%" + email + "%'")
            Else
                strSql.Append("  I.Email like '%" + email + "%'")
            End If
        End If
        If firstName.Length > 0 Or lastName.Length > 0 Or email.Length > 0 Then
            strSql.Append(" order by I.lastname,I.firstname")
            SearchMembers(strSql.ToString())
        Else
            lblIndSearch.Text = "Please Enter Email / First Name / Last Name / state"
            lblIndSearch.Visible = True
        End If

    End Sub

    Private Sub SearchMembers(ByVal sqlSt As String)
        Dim prmArray(1) As SqlParameter
        prmArray(0) = New SqlParameter("@wherecondition", sqlSt)
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.StoredProcedure, "[usp_SelectCalSignupVolunteers]", prmArray)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        GridMemberDt.DataSource = dt
        GridMemberDt.DataBind()
        If (Count > 0) Then
            Panel4.Visible = True
            lblIndSearch.Text = ""
            lblIndSearch.Visible = False
        Else
            lblIndSearch.Text = "No Volunteer match found"
            lblIndSearch.Visible = True
            Panel4.Visible = False
        End If
    End Sub

    Protected Sub GridMemberDt_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridMemberDt.RowCommand
        lblerr.Text = ""
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim row As GridViewRow = GridMemberDt.Rows(index)
        ' IncurredExpence name Search
        Dim strsql As String = "select COUNT(Automemberid) from  Indspouse Where Email=(select Email from Indspouse where automemberid=" & GridMemberDt.DataKeys(index).Value & ")"
        If SqlHelper.ExecuteScalar(Application("Connectionstring"), CommandType.Text, strsql) = 1 Then
            txtName.Text = row.Cells(1).Text + " " + row.Cells(2).Text
            hdnMemberID.Value = GridMemberDt.DataKeys(index).Value
            pIndSearch.Visible = False
            lblIndSearch.Visible = False
            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From CalSignup Where MemberID=" & hdnMemberID.Value & "") > 0 Then
                lblerr.Text = "The member already exists for Calendar Signup"
            End If
        Else
            lblIndSearch.Visible = True
            lblIndSearch.Text = "Email must be unique.  Please have the volunteer update the email to make it unique."
        End If
    End Sub

    Protected Sub btnIndClose_onclick(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnIndClose.Click
        pIndSearch.Visible = False
    End Sub

    Protected Sub btnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnCancel.Click
        clear()
    End Sub

    Protected Sub btnClear_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClear.Click
        txtName.Text = ""
        If ddlVolName.Items.Count > 0 Then
            ddlVolName.SelectedIndex = 0
        End If
    End Sub



    Protected Sub ddlWeekDays_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlWeekDays.SelectedIndexChanged
        If ddlWeekDays.SelectedValue = "5" Or ddlWeekDays.SelectedValue = "6" Then
            LoadDisplayTime(ddlDisplayTime, False)
        Else
            LoadWeekDisplayTime(ddlDisplayTime, False)
        End If
    End Sub

    Public Sub GetHostUrlMeeting(WebExID As String, Pwd As String)
        Dim strXMLServer As String = "https://northsouth.webex.com/WBXService/XMLService"
        Dim request As WebRequest = WebRequest.Create(strXMLServer)
        ' Set the Method property of the request to POST.
        request.Method = "POST"
        ' Set the ContentType property of the WebRequest.
        request.ContentType = "application/x-www-form-urlencoded"

        ' Create POST data and convert it to a byte array.
        Dim strXML As String = "<?xml version=""1.0"" encoding=""UTF-8""?>" & vbCr & vbLf
        strXML &= "<serv:message xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:serv=""http://www.webex.com/schemas/2002/06/service"" xsi:schemaLocation=""http://www.webex.com/schemas/2002/06/service http://www.webex.com/schemas/2002/06/service/service.xsd"">" & vbCr & vbLf
        strXML &= "<header>" & vbCr & vbLf
        strXML &= "<securityContext>" & vbCr & vbLf
        strXML &= "<webExID>" & WebExID & "</webExID>" & vbCr & vbLf
        strXML &= "<password>" & Pwd & "</password>" & vbCr & vbLf

        strXML &= "<siteName>northsouth</siteName>" & vbCr & vbLf
        strXML &= "<partnerID>mY7ck6lr82MeCSnQ2Mi6Ig</partnerID>" & vbCr & vbLf
        'strXML &= "<email>webex.nsf.adm@gmail.com</email>";
        strXML &= "</securityContext>" & vbCr & vbLf
        strXML &= "</header>" & vbCr & vbLf
        strXML &= "<body>" & vbCr & vbLf
        strXML &= "<bodyContent xsi:type=""java:com.webex.service.binding.meeting.GethosturlMeeting"">" & vbCr & vbLf
        'ep.GetAPIVersion    meeting.CreateMeeting
        strXML &= "<sessionKey>" & hdnTrainingSessionKey.Value & "</sessionKey>"

        strXML &= "</bodyContent>" & vbCr & vbLf
        strXML &= "</body>" & vbCr & vbLf
        strXML &= "</serv:message>" & vbCr & vbLf
        Dim byteArray As Byte() = Encoding.UTF8.GetBytes(strXML)

        ' Set the ContentLength property of the WebRequest.
        request.ContentLength = byteArray.Length

        ' Get the request stream.
        Dim dataStream As Stream = request.GetRequestStream()
        ' Write the data to the request stream.
        dataStream.Write(byteArray, 0, byteArray.Length)
        ' Close the Stream object.
        dataStream.Close()
        ' Get the response.
        Dim response As WebResponse = request.GetResponse()

        ' Get the stream containing content returned by the server.
        dataStream = response.GetResponseStream()
        Dim xmlReply As XmlDocument = Nothing
        If response.ContentType = "application/xml" OrElse response.ContentType = "text/xml;charset=UTF-8" Then

            xmlReply = New XmlDocument()

            xmlReply.Load(dataStream)
        End If
        Dim result As String = MeetingHostURLResponse(xmlReply)
        'lblMsg3.Text = result;

    End Sub

    Private Function MeetingHostURLResponse(xmlReply As XmlDocument) As String
        Dim sb As New StringBuilder()
        Try
            Dim manager As New XmlNamespaceManager(xmlReply.NameTable)
            manager.AddNamespace("serv", "http://www.webex.com/schemas/2002/06/service")
            manager.AddNamespace("meet", "http://www.webex.com/schemas/2002/06/service/meeting")
            manager.AddNamespace("com", "http://www.webex.com/schemas/2002/06/common")
            manager.AddNamespace("att", "http://www.webex.com/schemas/2002/06/service/attendee")

            Dim status As String = xmlReply.SelectSingleNode("/serv:message/serv:header/serv:response/serv:result", manager).InnerText

            If status = "SUCCESS" Then

                Dim meetingKey As String
                meetingKey = xmlReply.SelectSingleNode("/serv:message/serv:body/serv:bodyContent/meet:hostMeetingURL", manager).InnerXml
                Dim URL As String = String.Empty
                URL = meetingKey.Replace("&amp;", "&")
                hdnHostMeetingURL.Value = URL

            ElseIf status = "FAILURE" Then

            Else
                'lblMsg.ForeColor = System.Drawing.Color.Red;
                sb.Append("An Unknown error occurred. Please contact the Administrator.")
            End If
        Catch e As Exception
            sb.Append("Error: " & e.Message)
        End Try
        Return sb.ToString()
    End Function
    Public Sub GenerateURL(ByVal SessionKey As String, ByVal Pwd As String, ByVal UserID As String)
        hdnTrainingSessionKey.Value = SessionKey
        GetHostUrlMeeting(UserID, Pwd)
        Dim MeetinURL As String = hdnHostMeetingURL.Value
        If MeetinURL <> "" Then
            Dim URL = MeetinURL.Replace("&amp;", "&")
            hdnWebExMeetURL.Value = URL
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting();", True)
        End If

    End Sub

    Public Sub LoadMakeUpSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.VRoom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID)  where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.MemberID=" + Session("LoginID").ToString() + " VC.Semester='" & ddlPhase.SelectedValue & "'"
            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)"
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " and VC.Semester='" & ddlPhase.SelectedValue & "'"
                'AND VC.Semester=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex <> 0, " and VC.ProductGroupId=" & ddlProductGroup.SelectedValue, "") & " " & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " And VC.ProductID = " & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " And VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql &= " where CS.EventYear=" & ddlEventYear.SelectedValue & " And VC.SessionType='Scheduled Meeting'"
                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and CS.MemberID=" & Request.QueryString("coachID") & ""
                End If
                StrSql &= "order by IP.FirstName, IP.LastNAme ASC"
            End If
            'Response.Write(StrSql)
            '' If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)

            If ds.Tables(0).Rows.Count > 0 Then
                GrdMeeting.DataSource = ds
                GrdMeeting.DataBind()
                SpnMakeupTitle.Visible = False
                Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                Dim dateVal As String = String.Empty
                Dim i As Integer = 0
                Dim sessionKey As String = String.Empty

                For i = 0 To GrdMeeting.Rows.Count - 1
                    dateVal = GrdMeeting.Rows(i).Cells(13).Text
                    Dim dtDateVal As DateTime = New DateTime()
                    dtDateVal = Convert.ToDateTime(dateVal.ToString())
                    If (dtDateVal < dtTodayDate) Then
                        CType(GrdMeeting.Rows(i).FindControl("btnJoinMeeting"), Button).Enabled = False
                        Dim cmdUpdateText As String = String.Empty
                        sessionKey = GrdMeeting.Rows(i).Cells(10).Text
                        cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                    End If
                Next
            Else
                GrdMeeting.DataSource = ds
                GrdMeeting.DataBind()
                SpnMakeupTitle.Visible = True
            End If
            ''Else
            '   lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            ''End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub LoadSubstituteSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                'StrSql = "select distinct VC.SessionKey, VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,CS.SubstituteDate,CS.Vroom,cs.Time,Vl.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) inner join virtualroomlookup VL on (VL.Vroom=cs.Vroom) where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.SubstituteCoachID=" + Session("LoginID").ToString() + " and VC.SubstituteCoachID is not null"

                StrSql = " select CL.EventYear,E.EventCode,'coaching, US' as ChapterCode,IP.FirstName +' '+ IP.LastName as SubCoach,IP1.FirstName +' '+ IP1.LastName as Coach,CL.ProductID,CL.ProductGroupID,CL.EventID,VC.ChapterID,CS.ProductGroupCode,CS.ProductCode,CL.Date,CL.Time,CL.Semester,CL.Level,VC.SessionKey,VC.EndDate,VC.BeginTime,VC.EndTime,Cl.Duration,VC.TimeZone,VC.TimeZoneID,VC.Status,VC.MeetingUrl, CL.memberId, Cl.Substitute,VL.UserId,VL.PWD,CL.Day, IP.FirstName,IP.LastName,CL.Date as SubstituteDate,VL.Vroom,CS.Time, VL.HostID, Cl.SessionNo as Session,VC.MeetingPwd   from CoachClassCal CL inner join Indspouse IP on (CL.Substitute=IP.AutomemberId) inner join Indspouse IP1 on (Cl.MemberId=IP1.AutoMemberId) inner join Event E on (Cl.EventId=E.EventId) left join WebConfLog VC on (CL.memberID=VC.MemberId and VC.SessionType='Recurring Meeting' and Cl.Substitute is not null) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) left join VirtualRoomLookUp VL on (VL.Vroom=CS.Vroom) where CL.Substitute is not null and CL.Substitute <>'' and CL.Substitute <>0 and CL.EventYear='" + ddlEventYear.SelectedValue + "' and (Cl.ClassType='Regular' or Cl.ClassType='Holiday') and Cl.Semester='" + ddlPhase.SelectedValue + "' and CL.Substitute=" + Session("LoginID").ToString() + " and CL.Status='On' order by IP1.FirstName, IP1.Lastname ASC"

            Else
                'StrSql = "select distinct   VC.SessionKey,VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day, IP.FirstName,IP.LastName,CS.SubstituteDate,CS.Vroom,CS.Time, VL.HostID  from WebConfLog VC left join Event E on (E.EventId=VC.EventID) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on (VL.Vroom=CS.Vroom) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachID) "
                'StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & " AND VC.Semester='" & ddlPhase.SelectedValue & "'"
                'StrSql = StrSql & " where VC.SubstituteCoachID is not null and CS.EventYear=" & ddlEventYear.SelectedValue & ""

                'If Session("RoleId").ToString() <> "88" Then
                '    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "") & " "
                'End If

                'If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                '    StrSql = StrSql & " and (VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " or VC.SubstituteCoachID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " and VC.SubstituteCoachID is not null)"
                'End If

                'If (Request.QueryString("coachID") Is Nothing) Then
                'Else
                '    StrSql = StrSql & " and CS.MemberID=" & Request.QueryString("coachID") & ""
                'End If

                'StrSql &= " order by IP.FirstName, IP.LastName"

                StrSql = " select CL.EventYear,E.EventCode,'coaching, US' as ChapterCode,IP.FirstName +' '+ IP.LastName as SubCoach,IP1.FirstName +' '+ IP1.LastName as Coach,CL.ProductID,CL.ProductGroupID,CL.EventID,VC.ChapterID,CS.ProductGroupCode,CS.ProductCode,CL.Date,CL.Time,CL.Semester,CL.Level,VC.SessionKey,VC.EndDate,VC.BeginTime,VC.EndTime,Cl.Duration,VC.TimeZone,VC.TimeZoneID,VC.Status,VC.MeetingUrl, CL.memberId, Cl.Substitute,VL.UserId,VL.PWD,CL.Day, IP.FirstName,IP.LastName,CL.Date as SubstituteDate,VL.Vroom,CS.Time, VL.HostID, Cl.SessionNo as Session, VC.MeetingPwd   from CoachClassCal CL inner join Indspouse IP on (CL.Substitute=IP.AutomemberId) inner join Indspouse IP1 on (Cl.MemberId=IP1.AutoMemberId) inner join Event E on (Cl.EventId=E.EventId) left join WebConfLog VC on (CL.memberID=VC.MemberId and VC.SessionType='Recurring Meeting' and Cl.Substitute is not null and vc.ProductGroupId=cl.productGroupid and vc.Productid=cl.productid and vc.level=cl.level and vc.session=cl.Sessionno and vc.Semester=cl.Semester and vc.Eventyear=cl.Eventyear) inner join CalSignUp cs on (cs.meetingkey=VC.SessionKey) left join VirtualRoomLookUp VL on (VL.Vroom=CS.Vroom) where CL.Substitute is not null and CL.Substitute <>'' and CL.Substitute <>0 and CL.EventYear='" + ddlEventYear.SelectedValue + "' and (Cl.ClassType='Regular' or Cl.ClassType='Holiday') and Cl.Status='On' and Cl.Semester='" + ddlPhase.SelectedValue + "'"


                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND CL.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and CL.ProductID=" & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and (CL.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " or CL.Substitute=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & " and CL.Substitute is not null)"
                End If

                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and CL.MemberID=" & Request.QueryString("coachID") & ""
                End If

                StrSql &= " order by IP.FirstName, IP.LastName"

            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0
                    For i = 0 To GrdSubstituteSessions.Rows.Count - 1
                        dateVal = GrdSubstituteSessions.Rows(i).Cells(16).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal < dtTodayDate) Then
                            CType(GrdSubstituteSessions.Rows(i).FindControl("btnJoin"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = GrdSubstituteSessions.Rows(i).Cells(11).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    GrdSubstituteSessions.DataSource = ds
                    GrdSubstituteSessions.DataBind()
                    SpnSubstituteTitle.Visible = True
                End If
            Else
                '  lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub GrdMeeting_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "SelectMeetingURL" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                GrdMeeting.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                WebExID = GrdMeeting.Rows(selIndex).Cells(21).Text
                WebExPwd = GrdMeeting.Rows(selIndex).Cells(22).Text

                hdnSessionKey.Value = sessionKey
                Dim beginTime As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdMeeting.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                    Else
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)

                        Dim CmdText As String = String.Empty
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)

                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdSubstituteSessions_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdSubstituteSessions.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                Dim beginTime As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdSubstituteSessions.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                    Else
                        Dim CmdText As String = String.Empty
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception

                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub btnExport_Click(sender As Object, e As EventArgs) Handles btnExport.Click
        Try


            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and sessionno=c.sessionNo) as NStudents,I.EMAIL,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email,I.Hphone,I.CPhone,C.Confirm order by I.LastName, I.FirstName Asc"
            Else
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and sessionno=c.sessionNo) as NStudents,I.Email,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Semester='" & ddlPhase.SelectedValue & "'"

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql = StrSql & " group by C.Confirm,C.MeetingKey,C.MeetingPwd, C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email, I.Hphone,I.CPhone order by I.LastName, I.FirstName Asc"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds
            End If
            Dim dt As DataTable = ds.Tables(0)
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim FileName As String = "CalendarSignUp_" & Now.ToShortDateString & ".xls"

            oSheet.Range("A1:X1").MergeCells = True
            oSheet.Range("A1").Value = "Calendar SignUp"
            oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A1").Font.Bold = True

            oSheet.Range("A2").Value = "Ser#"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A2").Font.Bold = True

            oSheet.Range("B2").Value = "SignUp ID"
            oSheet.Range("B2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("b2").Font.Bold = True

            oSheet.Range("C2").Value = "Volunteer Name"
            oSheet.Range("C2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("C2").Font.Bold = True

            oSheet.Range("D2").Value = "Volunteer Email"
            oSheet.Range("D2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("D2").Font.Bold = True

            oSheet.Range("E2").Value = "Volunteer HPhone"
            oSheet.Range("E2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("E2").Font.Bold = True

            oSheet.Range("F2").Value = "Volunteer CPhone"
            oSheet.Range("F2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("F2").Font.Bold = True

            oSheet.Range("G2").Value = "ProductGroupCode"
            oSheet.Range("G2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("G2").Font.Bold = True

            oSheet.Range("H2").Value = "Product"
            oSheet.Range("H2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("H2").Font.Bold = True

            oSheet.Range("I2").Value = "Level"
            oSheet.Range("I2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("I2").Font.Bold = True

            oSheet.Range("J2").Value = "Session"
            oSheet.Range("J2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("J2").Font.Bold = True

            oSheet.Range("K2").Value = "Day"
            oSheet.Range("K2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("K2").Font.Bold = True

            oSheet.Range("L2").Value = "Time"
            oSheet.Range("L2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("L2").Font.Bold = True

            oSheet.Range("M2").Value = "Accepted"
            oSheet.Range("M2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("M2").Font.Bold = True

            oSheet.Range("N2").Value = "Confirm"
            oSheet.Range("N2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("N2").Font.Bold = True

            oSheet.Range("O2").Value = "Preferences"
            oSheet.Range("O2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("O2").Font.Bold = True

            oSheet.Range("P2").Value = "Max Cap"
            oSheet.Range("P2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("P2").Font.Bold = True

            oSheet.Range("Q2").Value = "#Of Students Approved"
            oSheet.Range("Q2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Q2").Font.Bold = True

            oSheet.Range("R2").Value = "VRoom"
            oSheet.Range("R2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("R2").Font.Bold = True

            oSheet.Range("S2").Value = "UserID"
            oSheet.Range("S2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("S2").Font.Bold = True

            oSheet.Range("T2").Value = "Password"
            oSheet.Range("T2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("T2").Font.Bold = True

            oSheet.Range("U2").Value = "Years"
            oSheet.Range("U2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("U2").Font.Bold = True

            oSheet.Range("V2").Value = "Sessions"
            oSheet.Range("V2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("V2").Font.Bold = True

            oSheet.Range("W2").Value = "SessionKey"
            oSheet.Range("W2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("W2").Font.Bold = True

            oSheet.Range("X2").Value = "Meeting Pwd"
            oSheet.Range("X2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("X2").Font.Bold = True

            oSheet.Range("Y2").Value = "Meeting URL"
            oSheet.Range("Y2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Y2").Font.Bold = True

            oSheet.Range("Z2").Value = "Event Year"
            oSheet.Range("Z2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Z2").Font.Bold = True

            oSheet.Range("AA2").Value = "EventCode"
            oSheet.Range("AA2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AA2").Font.Bold = True

            oSheet.Range("AB2").Value = "Semester"
            oSheet.Range("AB2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AB2").Font.Bold = True

            'dsPreYrFamilies Previous AND current year -Cur families
            Dim iRowIndex As Integer = 3, j As Integer
            Dim CRange As IRange
            For j = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(j)
                oSheet.Range("A" & Trim(Str(iRowIndex))).Value = j + 1
                oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("SignUpID")
                oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("Name")
                oSheet.Range("D" & Trim(Str(iRowIndex))).Value = dr("Email")
                oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("HPhone")
                oSheet.Range("F" & Trim(Str(iRowIndex))).Value = dr("CPhone")

                oSheet.Range("G" & Trim(Str(iRowIndex))).Value = dr("ProductGroupCode")
                oSheet.Range("H" & Trim(Str(iRowIndex))).Value = dr("ProductCode")
                oSheet.Range("I" & Trim(Str(iRowIndex))).Value = dr("Level")
                oSheet.Range("J" & Trim(Str(iRowIndex))).Value = dr("SessionNo")
                oSheet.Range("K" & Trim(Str(iRowIndex))).Value = dr("Day")
                oSheet.Range("L" & Trim(Str(iRowIndex))).Value = dr("Time")
                oSheet.Range("M" & Trim(Str(iRowIndex))).Value = dr("Accepted")
                oSheet.Range("N" & Trim(Str(iRowIndex))).Value = dr("Confirm")
                oSheet.Range("O" & Trim(Str(iRowIndex))).Value = dr("Preference")
                oSheet.Range("P" & Trim(Str(iRowIndex))).Value = dr("MaxCapacity")
                oSheet.Range("Q" & Trim(Str(iRowIndex))).Value = dr("NStudents")
                oSheet.Range("R" & Trim(Str(iRowIndex))).Value = dr("VRoom")

                oSheet.Range("S" & Trim(Str(iRowIndex))).Value = dr("UserID")
                oSheet.Range("T" & Trim(Str(iRowIndex))).Value = dr("PWD")
                oSheet.Range("U" & Trim(Str(iRowIndex))).Value = dr("Years")
                oSheet.Range("V" & Trim(Str(iRowIndex))).Value = dr("Sessions")

                oSheet.Range("W" & Trim(Str(iRowIndex))).Value = dr("MeetingKey")
                oSheet.Range("X" & Trim(Str(iRowIndex))).Value = dr("MeetingPwd")

                oSheet.Range("Y" & Trim(Str(iRowIndex))).Value = dr("HostJoinURL").ToString().Substring(0, Math.Min(20, dr("HostJoinURL").ToString().Length))

                oSheet.Range("Z" & Trim(Str(iRowIndex))).Value = dr("EventYear")
                oSheet.Range("AA" & Trim(Str(iRowIndex))).Value = dr("EventCode")
                oSheet.Range("AB" & Trim(Str(iRowIndex))).Value = dr("Semester")

                iRowIndex = iRowIndex + 1
            Next
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub btnExportTbl1a_Click(sender As Object, e As EventArgs) Handles btnExportTbl1a.Click
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents,I.EMAIL,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + Session("LoginID").ToString() + ""
                StrSql = StrSql & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventYear.SelectedValue & ") "
                StrSql = StrSql & " group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email,I.Hphone,I.CPhone,C.Confirm order by I.LastName, I.FirstName Asc"
            Else
                StrSql = "SELECT C.MeetingKey,C.MeetingPwd,isnull(C.HostJoinURL,'') HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear))  from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions, (select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y') as NStudents,I.Email,I.Hphone,I.CPhone,isnull(C.Confirm,'') Confirm  FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue & IIf(Session("RoleID") = 89, " and C.ProductGroupId in (" & lblPrdGrp.Text & ")" & IIf(ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "" And ddlProduct.SelectedValue <> "Select Product Group", " and C.ProductID in (" & lblPrd.Text & ") ", ""), "")

                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND C.EventID=" & ddlEvent.SelectedValue) & " AND C.Semester='" & ddlPhase.SelectedValue & "'"

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND C.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and C.ProductID=" & ddlProduct.SelectedValue, "")
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and C.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If

                StrSql = StrSql & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventYear.SelectedValue & ") "

                StrSql = StrSql & " group by C.Confirm,C.MeetingKey,C.MeetingPwd, C.HostJoinURL,C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Preference, C.VRoom, C.UserID, C.PWD,I.Email, I.Hphone,I.CPhone order by I.LastName, I.FirstName Asc"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                Session("volDataSet") = ds
            End If
            Dim dt As DataTable = ds.Tables(0)
            Dim oWorkbooks As IWorkbook = NativeExcel.Factory.CreateWorkbook()
            Dim oSheet As IWorksheet
            oSheet = oWorkbooks.Worksheets.Add()
            Dim FileName As String = "CalendarSignUp_NotAccepted" & Now.ToShortDateString & ".xls"

            oSheet.Range("A1:X1").MergeCells = True
            oSheet.Range("A1").Value = "Calendar SignUp"
            oSheet.Range("A1").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A1").Font.Bold = True

            oSheet.Range("A2").Value = "Ser#"
            oSheet.Range("A2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("A2").Font.Bold = True

            oSheet.Range("B2").Value = "SignUp ID"
            oSheet.Range("B2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("b2").Font.Bold = True

            oSheet.Range("C2").Value = "Volunteer Name"
            oSheet.Range("C2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("C2").Font.Bold = True

            oSheet.Range("D2").Value = "Volunteer Email"
            oSheet.Range("D2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("D2").Font.Bold = True

            oSheet.Range("E2").Value = "Volunteer HPhone"
            oSheet.Range("E2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("E2").Font.Bold = True

            oSheet.Range("F2").Value = "Volunteer CPhone"
            oSheet.Range("F2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("F2").Font.Bold = True

            oSheet.Range("G2").Value = "ProductGroupCode"
            oSheet.Range("G2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("G2").Font.Bold = True

            oSheet.Range("H2").Value = "Product"
            oSheet.Range("H2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("H2").Font.Bold = True

            oSheet.Range("I2").Value = "Level"
            oSheet.Range("I2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("I2").Font.Bold = True

            oSheet.Range("J2").Value = "Session"
            oSheet.Range("J2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("J2").Font.Bold = True

            oSheet.Range("K2").Value = "Day"
            oSheet.Range("K2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("K2").Font.Bold = True

            oSheet.Range("L2").Value = "Time"
            oSheet.Range("L2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("L2").Font.Bold = True

            oSheet.Range("M2").Value = "Accepted"
            oSheet.Range("M2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("M2").Font.Bold = True

            oSheet.Range("N2").Value = "Confirm"
            oSheet.Range("N2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("N2").Font.Bold = True

            oSheet.Range("O2").Value = "Preferences"
            oSheet.Range("O2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("O2").Font.Bold = True

            oSheet.Range("P2").Value = "Max Cap"
            oSheet.Range("P2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("P2").Font.Bold = True

            oSheet.Range("Q2").Value = "#Of Students Approved"
            oSheet.Range("Q2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Q2").Font.Bold = True

            oSheet.Range("R2").Value = "VRoom"
            oSheet.Range("R2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("R2").Font.Bold = True

            oSheet.Range("S2").Value = "UserID"
            oSheet.Range("S2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("S2").Font.Bold = True

            oSheet.Range("T2").Value = "Password"
            oSheet.Range("T2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("T2").Font.Bold = True

            oSheet.Range("U2").Value = "Years"
            oSheet.Range("U2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("U2").Font.Bold = True

            oSheet.Range("V2").Value = "Sessions"
            oSheet.Range("V2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("V2").Font.Bold = True

            oSheet.Range("W2").Value = "SessionKey"
            oSheet.Range("W2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("W2").Font.Bold = True

            oSheet.Range("X2").Value = "Meeting Pwd"
            oSheet.Range("X2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("X2").Font.Bold = True

            oSheet.Range("Y2").Value = "Meeting URL"
            oSheet.Range("Y2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Y2").Font.Bold = True

            oSheet.Range("Z2").Value = "Event Year"
            oSheet.Range("Z2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("Z2").Font.Bold = True

            oSheet.Range("AA2").Value = "EventCode"
            oSheet.Range("AA2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AA2").Font.Bold = True

            oSheet.Range("AB2").Value = "Semester"
            oSheet.Range("AB2").HorizontalAlignment = XlHAlign.xlHAlignCenter
            oSheet.Range("AB2").Font.Bold = True

            'dsPreYrFamilies Previous AND current year -Cur families
            Dim iRowIndex As Integer = 3, j As Integer
            Dim CRange As IRange
            For j = 0 To dt.Rows.Count - 1
                Dim dr As DataRow = dt.Rows(j)
                oSheet.Range("A" & Trim(Str(iRowIndex))).Value = j + 1
                oSheet.Range("B" & Trim(Str(iRowIndex))).Value = dr("SignUpID")
                oSheet.Range("C" & Trim(Str(iRowIndex))).Value = dr("Name")
                oSheet.Range("D" & Trim(Str(iRowIndex))).Value = dr("Email")
                oSheet.Range("E" & Trim(Str(iRowIndex))).Value = dr("HPhone")
                oSheet.Range("F" & Trim(Str(iRowIndex))).Value = dr("CPhone")

                oSheet.Range("G" & Trim(Str(iRowIndex))).Value = dr("ProductGroupCode")
                oSheet.Range("H" & Trim(Str(iRowIndex))).Value = dr("ProductCode")
                oSheet.Range("I" & Trim(Str(iRowIndex))).Value = dr("Level")
                oSheet.Range("J" & Trim(Str(iRowIndex))).Value = dr("SessionNo")
                oSheet.Range("K" & Trim(Str(iRowIndex))).Value = dr("Day")
                oSheet.Range("L" & Trim(Str(iRowIndex))).Value = dr("Time")
                oSheet.Range("M" & Trim(Str(iRowIndex))).Value = dr("Accepted")
                oSheet.Range("N" & Trim(Str(iRowIndex))).Value = dr("Confirm")
                oSheet.Range("O" & Trim(Str(iRowIndex))).Value = dr("Preference")
                oSheet.Range("P" & Trim(Str(iRowIndex))).Value = dr("MaxCapacity")
                oSheet.Range("Q" & Trim(Str(iRowIndex))).Value = dr("NStudents")
                oSheet.Range("R" & Trim(Str(iRowIndex))).Value = dr("VRoom")

                oSheet.Range("S" & Trim(Str(iRowIndex))).Value = dr("UserID")
                oSheet.Range("T" & Trim(Str(iRowIndex))).Value = dr("PWD")
                oSheet.Range("U" & Trim(Str(iRowIndex))).Value = dr("Years")
                oSheet.Range("V" & Trim(Str(iRowIndex))).Value = dr("Sessions")

                oSheet.Range("W" & Trim(Str(iRowIndex))).Value = dr("MeetingKey")
                oSheet.Range("X" & Trim(Str(iRowIndex))).Value = dr("MeetingPwd")

                oSheet.Range("Y" & Trim(Str(iRowIndex))).Value = dr("HostJoinURL").ToString().Substring(0, Math.Min(20, dr("HostJoinURL").ToString().Length))

                oSheet.Range("Z" & Trim(Str(iRowIndex))).Value = dr("EventYear")
                oSheet.Range("AA" & Trim(Str(iRowIndex))).Value = dr("EventCode")
                oSheet.Range("AB" & Trim(Str(iRowIndex))).Value = dr("Semester")

                iRowIndex = iRowIndex + 1
            Next
            Response.Clear()
            Response.ContentType = "application/vnd.ms-excel"
            Response.AddHeader("Content-Type", "application/vnd.ms-excel")
            Response.AddHeader("Content-Disposition", "attachment;filename=" & FileName)
            oWorkbooks.SaveAs(Response.OutputStream)
            Response.End()
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub


    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click

        Dim DG As DataGrid = CType(Cache("Source"), DataGrid)

        '  Me.DGCoach_UpdateCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))
        Me.DGCoach_UpdateCommand(DG, New DataGridCommandEventArgs(DG.Items(DG.EditItemIndex), DG, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))

    End Sub

    Protected Sub btnCancelGrid_Click(sender As Object, e As EventArgs) Handles btnCancelGrid.Click
        hdInAppr.Value = ""
        hdToAppvId.Value = ""
        '  DGCoach_CancelCommand(Me.DGCoach, New DataGridCommandEventArgs(Me.DGCoach.Items(Me.DGCoach.EditItemIndex), Me.DGCoach, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))
        Dim DG As DataGrid = CType(Cache("Source"), DataGrid)
        DGCoach_CancelCommand(DG, New DataGridCommandEventArgs(DG.Items(DG.EditItemIndex), DG, New System.Web.UI.WebControls.CommandEventArgs("Update", "")))

    End Sub

    Public Sub fillGuestAttendeeGrid()
        Try
            Dim cmdText As String = String.Empty
            Dim ds As New DataSet()
            If Session("RoleID").ToString() = "88" Then
                cmdText = "select GA.MemberID,GA.RegisteredID,GA.GuestAttendID,GA.CMemberID,GA.MemberID,CS.UserID,CS.Pwd,CS.MeetingKey,cs.ProductGroupID,cs.ProductID,cs.SessionNo, cs.Level, IP.FirstName +' '+ IP.LastName as Name, IP1.FirstName +' '+ IP1.LastName as CoachName, IP.Email as WebExEmail, cs.ProductGroupCode,cs.ProductCode,cs.Level, GA.MeetingURL, GA.RegisteredID,CS.Vroom,CS.Time, CS.Day from GuestAttendance GA inner join CalSignup CS on (GA.SessionKey=CS.MeetingKey) inner join IndSpouse IP on(IP.AutoMemberID=GA.MemberID) inner join IndSpouse IP1 on (IP1.AutoMemberID=GA.CMemberID) Where GA.MemberID=" & Session("LoginID").ToString() & " and GA.EventYear=" & ddlEventYear.SelectedValue & " and GA.Semester='" & ddlPhase.SelectedValue & "'"
            Else
                cmdText = "select GA.MemberID,GA.RegisteredID,GA.GuestAttendID,GA.CMemberID,GA.MemberID,CS.UserID,CS.Pwd,CS.MeetingKey,cs.ProductGroupID,cs.ProductID,cs.SessionNo, cs.Level, IP.FirstName +' '+ IP.LastName as Name, IP1.FirstName +' '+ IP1.LastName as CoachName, IP.Email as WebExEmail, cs.ProductGroupCode,cs.ProductCode,cs.Level, GA.MeetingURL, GA.RegisteredID,CS.Vroom,CS.Time, CS.Day from GuestAttendance GA inner join CalSignup CS on (GA.SessionKey=CS.MeetingKey) inner join IndSpouse IP on(IP.AutoMemberID=GA.MemberID) inner join IndSpouse IP1 on (IP1.AutoMemberID=GA.CMemberID) where GA.EventYear=" & ddlEventYear.SelectedValue & " and GA.Semester='" & ddlPhase.SelectedValue & "' "
                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    cmdText = cmdText & " and GA.MemberID=" & Request.QueryString("coachID") & ""
                End If

            End If
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    spnGuestAttTitle.Visible = False
                    GrdGuestAttendee.DataSource = ds
                    GrdGuestAttendee.DataBind()

                Else
                    GrdGuestAttendee.DataSource = ds
                    GrdGuestAttendee.DataBind()
                    spnGuestAttTitle.Visible = True

                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub fillPractiseSession()
        Dim cmdtext As String = ""
        Dim ds As New DataSet()
        spnTable1Title.Visible = True
        GrdMeeting.Visible = True
        cmdtext = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) where VC.EventYear=" & ddlEventYear.SelectedValue & " and VC.Semester='" & ddlPhase.SelectedValue & "' and "
        If Session("RoleID").ToString() = "88" Then
            cmdtext &= "VC.MemberID=" & Session("LoginID") & " and VC.SessionType='Practice'  order by VC.ProductGroupCode"
        Else
            cmdtext &= " VC.SessionType='Practice'  order by VC.ProductGroupCode"
        End If
        If (Request.QueryString("coachID") Is Nothing) Then
        Else
            cmdtext = cmdtext & " and VC.MemberID=" & Request.QueryString("coachID") & ""
        End If

        Try
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then

                    grdPractiseSession.DataSource = ds
                    grdPractiseSession.DataBind()
                    spnPractiseNoRecord.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0

                    For i = 0 To grdPractiseSession.Rows.Count - 1
                        dateVal = grdPractiseSession.Rows(i).Cells(15).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal < dtTodayDate) Then
                            CType(grdPractiseSession.Rows(i).FindControl("btnJoinMeeting"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = grdPractiseSession.Rows(i).Cells(12).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    grdPractiseSession.DataSource = ds
                    grdPractiseSession.DataBind()
                    spnPractiseNoRecord.Visible = True
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Protected Sub grdPractiseSession_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                grdPractiseSession.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty

                WebExID = grdPractiseSession.Rows(selIndex).Cells(21).Text
                WebExPwd = grdPractiseSession.Rows(selIndex).Cells(22).Text
                sessionKey = grdPractiseSession.Rows(selIndex).Cells(10).Text

                sessionKey = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblStSessionKey"), Label), Label).Text
                Dim hostID As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblSthostID"), Label), Label).Text

                hdnSessionKey.Value = sessionKey
                Dim beginTime As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(grdPractiseSession.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                    Else
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        Dim CmdText As String = String.Empty
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting()", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdGuestAttendee_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                Dim beginTime As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                Dim sessionKey As String = TryCast(DirectCast(GrdGuestAttendee.Rows(selIndex).FindControl("lblSessionKey"), Label), Label).Text
                hdnSessionKey.Value = sessionKey
                If mins <= 30 AndAlso day = today Then



                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinGuestMeeting()", True)
                Else
                    ' System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlLevel_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlLevel.SelectedIndexChanged
        lblerr.Text = ""
        lblError.Text = ""
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            prdGroup = ddlProductGroup.SelectedValue
        End If
        If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            prdID = ddlProduct.SelectedValue
        End If
        If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level" And ddlLevel.SelectedItem.Text <> "No Level found") Then
            strlevel = ddlLevel.SelectedValue
        End If
        If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            coach = ddlVolName.SelectedValue
        End If


        'hidden by Sims as per Praveen's comments

        '  LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

        'LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
        '  LoadGrid(ddlAcceptedFilter.SelectedValue)
    End Sub

    Protected Sub btnReset_Click(sender As Object, e As EventArgs) Handles btnReset.Click
        If Not Request.QueryString("Role") Is Nothing Then
            Server.Transfer("CalendarSignup.aspx?Role=" + Request.QueryString("Role"))
        Else
            Server.Transfer("CalendarSignup.aspx")
        End If

    End Sub

    Protected Sub btnConfirmToUpdate_Click(sender As Object, e As EventArgs) Handles btnConfirmToUpdate.Click
        Dim strSignUpIds As String = ""
        Dim chkRow As CheckBox
        Dim r As GridViewRow
        For Each r In gvConfirmList.Rows
            chkRow = r.Cells(0).FindControl("chkRow")
            If chkRow.Checked Then
                If strSignUpIds.Length > 0 Then
                    strSignUpIds = strSignUpIds & "," & TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                Else
                    strSignUpIds = TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                End If
            End If
        Next


        If strSignUpIds.Length > 0 Then
            Dim strSql As String = "update CalSignup set Confirm='Y' where SignUpId in ( " & strSignUpIds & ") and EventID=13 and EventYear=" & ddlEventYear.SelectedValue
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim strlevel As String = String.Empty
            Dim coach As String = String.Empty
            'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            '    prdGroup = ddlProductGroup.SelectedValue
            'End If
            'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            '    prdID = ddlProduct.SelectedValue
            'End If
            'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
            '    strlevel = ddlLevel.SelectedValue
            'End If
            'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            '    coach = ddlVolName.SelectedValue
            'End If

            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                strlevel = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If

            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
            ' LoadGrid(ddlAcceptedFilter.SelectedValue)

            strSql = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
            Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            If flag > 0 Then
                btnConfirm.Visible = True
            Else
                btnConfirm.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnNotConfirm_Click(sender As Object, e As EventArgs) Handles btnNotConfirm.Click
        Dim strSignUpIds As String = ""
        Dim chkRow As CheckBox
        Dim r As GridViewRow
        For Each r In gvConfirmList.Rows
            chkRow = r.Cells(0).FindControl("chkRow")
            If chkRow.Checked Then
                If strSignUpIds.Length > 0 Then
                    strSignUpIds = strSignUpIds & "," & TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                Else
                    strSignUpIds = TryCast(r.Cells(0).FindControl("lblSignUpId"), Label).Text
                End If
            End If
        Next
        If strSignUpIds.Length > 0 Then
            Dim strSql As String = "update CalSignup set Confirm='N' where SignUpId in ( " & strSignUpIds & ") and EventID=13 and EventYear=" & ddlEventYear.SelectedValue
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, strSql)
            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim strlevel As String = String.Empty
            Dim coach As String = String.Empty
            'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
            '    prdGroup = ddlProductGroup.SelectedValue
            'End If
            'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
            '    prdID = ddlProduct.SelectedValue
            'End If
            'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
            '    strlevel = ddlLevel.SelectedValue
            'End If
            'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
            '    coach = ddlVolName.SelectedValue
            'End If

            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                strlevel = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If


            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)


            strSql = " select count(*) from CalSignUp where MemberID=" & Session("LoginID") & " and Accepted='Y' and Confirm is null and EventYear=" & ddlEventYear.SelectedValue
            Dim flag As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, strSql)
            If flag > 0 Then
                btnConfirm.Visible = True
            Else
                btnConfirm.Visible = False
            End If
        End If
    End Sub

    Protected Sub btnConfirm_Click(sender As Object, e As EventArgs) Handles btnConfirm.Click
        Dim ds As DataSet
        Dim strSql As String = ""
        Try
            strSql = "SELECT  C.SignUpID,C.MemberID,C.EventYear,C.EventID, C.Semester,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Preference FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID where C.EventYear=" & ddlEventYear.SelectedValue
            strSql = strSql & " and C.MemberID=" & Session("LoginID") & " and C.Accepted='Y' and C.Confirm is null"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSql)
            If ds.Tables(0).Rows.Count > 0 Then
                gvConfirmList.DataSource = ds.Tables(0)
                gvConfirmList.DataBind()
                'Dim dr As DataRow = ds.Tables(0).Rows(0)
                'ViewState("SignupID") = dr("SignupId")
                'lblCProduct.Text = dr("ProductCode")
                'lblCLevel.Text = dr("Level")
                'lblCSession.Text = dr("SessionNo")
                'lblCDay.Text = dr("Day")
                'lblCTime.Text = dr("Time")
                System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "PopUpConfirmBox('show');", True)
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlAcceptedFilter_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlAcceptedFilter.SelectedIndexChanged

        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)

        'LoadGrid(ddlAcceptedFilter.SelectedValue)
    End Sub


    Protected Sub DGCoach_SortCommand(source As Object, e As DataGridSortCommandEventArgs)
        Dim strSQL As String
        Dim dt As DataTable

        dt = TryCast(ViewState("dtbl"), DataTable)

        If True Then
            Dim SortDir As String = String.Empty
            If dir = SortDirection.Ascending Then
                dir = SortDirection.Descending
                SortDir = "Desc"
            Else
                dir = SortDirection.Ascending
                SortDir = "Asc"
            End If
            Dim sortedView As New DataView(dt)
            sortedView.Sort = Convert.ToString(e.SortExpression + " ") & SortDir
            source.DataSource = sortedView
            source.DataBind()
        End If
    End Sub

    Protected Property dir() As SortDirection
        Get
            If ViewState("dirState") Is Nothing Then
                ViewState("dirState") = SortDirection.Ascending
            End If
            Return DirectCast(ViewState("dirState"), SortDirection)
        End Get
        Set(value As SortDirection)
            ViewState("dirState") = value
        End Set
    End Property

    Public Sub ddlDGSessionNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        lblError.Text = ""
        Try


            Dim dr As DataRow = CType(Cache("editRow"), DataRow)

            If (Not dr Is Nothing) Then

                Dim cmdText As String = String.Empty
                Dim pgID As String = dr.Item("productGroupID").ToString()
                Dim pID As String = dr.Item("productID").ToString()
                Dim year As String = dr.Item("EventYear").ToString()
                Dim Level As String = dr.Item("Level").ToString()
                Dim sessionNo As String = ddlTemp.SelectedValue

                Dim iSessionNo As Integer = Convert.ToInt32(sessionNo) - 1
                hdnSessionNo.Value = ddlTemp.SelectedValue
                If sessionNo <> "1" Then


                    cmdText = " select count(*) as sessioNoCount from CalSignup where EventYear=" & year & " and ProductGroupID=" & pgID & " and ProductID=" & pID & " and Level='" & Level & "' and SessionNo=" & iSessionNo & " and MemberID=" & dr.Item("MemberID").ToString() & ""
                    'Session("editRow") = dr
                    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)
                    If ds.Tables(0).Rows.Count > 0 Then
                        If Convert.ToInt32(ds.Tables(0).Rows(0)("sessioNoCount").ToString()) = 0 Then
                            lblError.Text = "Higher Session # was selected without a lower session # selected earlier."
                            ddlTemp.SelectedValue = 1
                            hdnSessionNo.Value = 1
                        Else

                        End If


                    End If
                End If
                Cache("editRow") = dr
            Else
                Return
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub


    Public Sub ddlDGDay_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender
        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim day As String = ddlTemp.SelectedValue
        Dim startTime As String = dr.Item("Time").ToString()
        hdnDay.Value = day
        Dim cmdText As String = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim Duration As Decimal
        If ds.Tables(0).Rows.Count > 0 Then
            Try
                Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
            Catch ex As Exception

            End Try

            If Duration = 1 Then
                Duration = 60
            ElseIf Duration = 1.5 Then
                Duration = 90
            ElseIf Duration = 2 Then
                Duration = 120
            Else
                Duration = 180
            End If
        End If
        Try
            Dim ddlVRoom As DropDownList = CType(DGCoach.Items(hdnItemIndex.Value).FindControl("ddlDGVRoom"), DropDownList)
            checkAvailableVrooms(Duration, startTime, day, ddlVRoom, dr.Item("SignupID").ToString(), lblerr)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Public Sub ddlDGTime_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
        Dim ddlTemp As System.Web.UI.WebControls.DropDownList
        ddlTemp = sender

        Dim dr As DataRow = CType(Cache("editRow"), DataRow)
        Dim day As String = dr.Item("Day").ToString()
        Dim startTime As String = ddlTemp.SelectedValue
        hdnBeginTime.Value = startTime
        Dim cmdText As String = "select Duration from EventFees where EventYear=" & dr.Item("EventYear").ToString() & " and ProductGroupID=" & dr.Item("ProductGroupID").ToString() & " and ProductID=" & dr.Item("ProductID").ToString() & " and EventID=13"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        Dim Duration As Decimal
        If ds.Tables(0).Rows.Count > 0 Then
            Try
                Duration = Convert.ToInt32(ds.Tables(0).Rows(0)("Duration").ToString())
            Catch ex As Exception
                Duration = 2
            End Try

            If Duration = 1 Then
                Duration = 60
            ElseIf Duration = 1.5 Then
                Duration = 90
            ElseIf Duration = 2 Then
                Duration = 120
            Else
                Duration = 180
            End If
        End If
        Try


            Dim ddlVRoom As DropDownList = CType(DGCoach.Items(hdnItemIndex.Value).FindControl("ddlDGVRoom"), DropDownList)
            checkAvailableVrooms(Duration, startTime, day, ddlVRoom, dr.Item("SignupID").ToString(), lblerr)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub checkAvailableVrooms(ByVal Duration As Integer, ByVal strTime As String, ByVal day As String, ddlObject As DropDownList, ByVal SignupID As String, lbl As Label)

        Dim cmdtext As String = String.Empty
        Dim ds As DataSet = New DataSet()
        Dim hostID As String = String.Empty

        Dim strStartTime As String = strTime
        Dim strStartDay As String = day
        Duration = Duration + 30
        Dim strEndTime As String = Convert.ToDateTime(strTime).ToString("HH:mm:ss")
        strStartTime = Convert.ToDateTime(strTime).AddMinutes(-30).ToString("HH:mm:ss")
        Dim dtPrStartTime As DateTime = Convert.ToDateTime(strStartTime)
        Dim dtPrEndTime As DateTime = New DateTime()
        Dim StrSHr As String = strStartTime.Substring(0, 2)
        Dim iShr = Convert.ToInt32(StrSHr)
        Dim StrEhr As String = Convert.ToDateTime(strEndTime).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2)
        Dim iEhr As Integer = Convert.ToInt32(StrEhr)
        If (iShr > 7 And iEhr < 8) Then
            dtPrEndTime = Convert.ToDateTime("23:59:00")
        Else
            dtPrEndTime = Convert.ToDateTime(strEndTime).AddMinutes(Duration)
        End If



        Dim dtStarttime As New DateTime()
        Dim dtEndTime As New DateTime()

        cmdtext = "select distinct CS.VRoom,VL.HostID,VL.UserID,VL.Pwd, CS.[Begin], CS.[End], CS.Day, CS.ProductGroupCode, cs.Time from CalSignup CS  inner join VirtualRoomLookup VL on(VL.VRoom=CS.VRoom) where CS.EventYear=" & ddlEventYear.SelectedValue & " and Accepted='Y' and Semester='" & ddlPhase.SelectedValue & "' and SignupId not in (" & SignupID & ") order by cs.Time"

        Try
            Dim vRooms As String = String.Empty

            Dim iDurationBasedOnPG As [Double] = 0
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        dtStarttime = Convert.ToDateTime(dr("Begin").ToString())
                        If dr("ProductGroupCode").ToString() = "COMP" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "GB" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "LSP" Then
                            iDurationBasedOnPG = 1
                        ElseIf dr("ProductGroupCode").ToString() = "MB" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "SAT" Then
                            iDurationBasedOnPG = 1.5
                        ElseIf dr("ProductGroupCode").ToString() = "SC" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "UV" Then
                            iDurationBasedOnPG = 1
                        End If
                        Dim StrSHr1 As String = Convert.ToDateTime(dr("Begin").ToString()).ToString("HH:mm:ss").Substring(0, 2)
                        Dim iShr1 = Convert.ToInt32(StrSHr1)
                        Dim StrEhr1 As String = Convert.ToDateTime(dr("Begin").ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2)
                        Dim iEhr1 As Integer = Convert.ToInt32(StrEhr1)
                        If (iShr1 > 7 And iEhr1 < 8) Then
                            dtEndTime = Convert.ToDateTime("23:59:00")
                        Else
                            dtEndTime = Convert.ToDateTime(dr("Begin").ToString()).AddHours(iDurationBasedOnPG)
                        End If

                        'dtEndTime = Convert.ToDateTime(dr("Begin").ToString()).AddHours(iDurationBasedOnPG)

                        Dim Meetingday As String = dr("Day").ToString()
                        Dim meetVroom As String = dr("Vroom").ToString()
                        If (dtStarttime >= dtPrStartTime Or dtStarttime <= dtPrStartTime) AndAlso dtStarttime <= dtPrEndTime AndAlso dtEndTime >= dtPrStartTime AndAlso (dtEndTime <= dtPrEndTime Or dtEndTime >= dtPrEndTime) AndAlso strStartDay = dr("Day").ToString() Then
                            vRooms += dr("VRoom").ToString() + ","
                        End If

                        'If (dtPrStartTime >= dtStarttime Or dtPrEndTime <= dtEndTime) And (strStartDay = dr("Day").ToString()) Then


                        'End If

                    Next
                End If
            End If

            cmdtext = "select distinct Vl.VRoom,VL.HostID,VL.UserID,VL.Pwd, VC.Day,VC.BeginTime,VC.EndTime, VC.StartDate, VC.ProductGroupCode from  WebConfLog VC  inner join VirtualRoomLookup VL on(VL.UserID=VC.UserID) where VC.EventYear=" & ddlEventYear.SelectedValue & " and VC.Semester='" & ddlPhase.SelectedValue & "' and (SessionType='Practice' or SessionType='Scheduled Meeting' or SessionType='Extra') and StartDate>getDate() order by VC.BeginTime"

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows
                        dtStarttime = Convert.ToDateTime(dr("BeginTime").ToString())
                        If dr("ProductGroupCode").ToString() = "COMP" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "GB" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "LSP" Then
                            iDurationBasedOnPG = 1
                        ElseIf dr("ProductGroupCode").ToString() = "MB" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "SAT" Then
                            iDurationBasedOnPG = 1.5
                        ElseIf dr("ProductGroupCode").ToString() = "SC" Then
                            iDurationBasedOnPG = 2
                        ElseIf dr("ProductGroupCode").ToString() = "UV" Then
                            iDurationBasedOnPG = 1
                        End If
                        Dim StrSHr1 As String = Convert.ToDateTime(dr("BeginTime").ToString()).ToString("HH:mm:ss").Substring(0, 2)
                        Dim iShr1 = Convert.ToInt32(StrSHr1)
                        Dim StrEhr1 As String = Convert.ToDateTime(dr("BeginTime").ToString()).AddMinutes(Duration).ToString("HH:mm:ss").Substring(0, 2)
                        Dim iEhr1 As Integer = Convert.ToInt32(StrEhr1)
                        If (iShr1 > 7 And iEhr1 < 8) Then
                            dtEndTime = Convert.ToDateTime("23:59:00")
                        Else
                            dtEndTime = Convert.ToDateTime(dr("BeginTime").ToString()).AddHours(iDurationBasedOnPG)
                        End If
                        ' dtEndTime = Convert.ToDateTime(dr("BeginTime").ToString()).AddHours(iDurationBasedOnPG)
                        ' dtEndTime = Convert.ToDateTime(dr("EndTime").ToString())
                        Dim Meetingday As String = dr("Day").ToString()
                        Dim meetVroom As String = dr("Vroom").ToString()
                        If (dtStarttime >= dtPrStartTime Or dtStarttime <= dtPrStartTime) AndAlso dtStarttime <= dtPrEndTime AndAlso dtEndTime >= dtPrStartTime AndAlso (dtEndTime <= dtPrEndTime Or dtEndTime >= dtPrEndTime) AndAlso strStartDay = dr("Day").ToString() Then
                            vRooms += dr("VRoom").ToString() + ","
                        End If
                    Next
                End If
            End If


            If vRooms <> "" Then
                vRooms = vRooms.TrimEnd(",")
            End If

            If vRooms <> "" Then
                cmdtext = "select HostID, USerID, PWD, Vroom from VirtualRoomLookUp where Vroom not in (" & vRooms & ")"
            Else
                cmdtext = "select HostID, USerID, PWD, Vroom from VirtualRoomLookUp"
            End If

            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    ddlObject.DataValueField = "Vroom"
                    ddlObject.DataTextField = "Vroom"
                    ddlObject.DataSource = ds
                    ddlObject.DataBind()
                Else
                    ddlObject.DataValueField = "Vroom"
                    ddlObject.DataTextField = "Vroom"
                    ddlObject.DataSource = ds
                    ddlObject.DataBind()
                    lbl.Text = "No Vroom is available at this time. Please choose different time."
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try

    End Sub

    Public Sub writeToLogFile(logMessage As String)
        Dim strPath As String '= Server.MapPath(strLogFile)
        Dim strLogMessage As String = ""
        Dim strLogFile As String = String.Empty
        Try
            strLogFile = "~\ZoomMeeting_" & DateTime.Now.ToShortDateString() & ".txt"
            strPath = Server.MapPath(strLogFile)

            Dim swLog As StreamWriter
            strLogMessage = String.Format("{0}: {1}", Date.Now, logMessage)
            If (Not File.Exists(strLogFile)) Then
                swLog = New StreamWriter(Server.MapPath(strLogFile))

            Else
                swLog = File.AppendText(strLogFile)

            End If
            swLog.WriteLine(strLogMessage)
            swLog.WriteLine()
            swLog.Close()
        Catch ex As Exception
        End Try
    End Sub

    Public Sub getmeetingIfo(sessionkey As String, HostID As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "5"
            URL = "https://api.zoom.us/v1/meeting/get"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += (Convert.ToString("&id=") & sessionkey) + ""
            urlParameter += (Convert.ToString("&host_id=") & HostID) + ""

            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try

    End Sub

    Public Sub makeZoomAPICall(urlParameters As String, URL As String, serviceType As String)
        Try


            Dim objRequest As HttpWebRequest = DirectCast(WebRequest.Create(URL), HttpWebRequest)
            objRequest.Method = "POST"
            objRequest.ContentLength = urlParameters.Length
            objRequest.ContentType = "application/x-www-form-urlencoded"

            ' post data is sent as a stream
            Dim myWriter As StreamWriter = Nothing
            myWriter = New StreamWriter(objRequest.GetRequestStream())
            myWriter.Write(urlParameters)
            myWriter.Close()

            ' returned values are returned as a stream, then read into a string
            Dim postResponse As String
            Dim objResponse As HttpWebResponse = DirectCast(objRequest.GetResponse(), HttpWebResponse)
            Using responseStream As New StreamReader(objResponse.GetResponseStream())
                postResponse = responseStream.ReadToEnd()

                responseStream.Close()
            End Using

            If serviceType = "5" Then

                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)


                hdnHostURL.Value = json("start_url").ToString()
                hdnHostMeetingURL.Value = json("start_url").ToString()

            ElseIf serviceType = "6" Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                Dim SessionKey As String = String.Empty

                For Each item As Object In json("meetings")
                    If (item("host_id").ToString() = hdnHostID.Value) Then

                        termianteMeeting(item("id").ToString(), hdnHostID.Value)

                    End If



                Next


            ElseIf serviceType = "7" Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                Dim SessionKey As String = String.Empty

            ElseIf serviceType = "3" Then

                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "delete from WebConfLog where SessionKey=" + hdnSessionKey.Value + "")

                Dim strQuery As String
                Dim cmd As SqlCommand
                strQuery = "update CalSignup set MeetingKey = null, meetingPwd=null, Status='Cancelled', HostjoinURL=null  where MeetingKey=@MeetingKey"
                cmd = New SqlCommand(strQuery)
                cmd.Parameters.AddWithValue("@MeetingKey", hdnSessionKey.Value)

                Dim objNSF As New NSFDBHelper()
                objNSF.InsertUpdateData(cmd)

            ElseIf (serviceType = "1") Then
                Dim json As Object = New JavaScriptSerializer().Deserialize(Of Object)(postResponse)

                hdnTrainingSessionKey.Value = json("id").ToString()
                hdnHostMeetingURL.Value = json("start_url").ToString()
                hdnMeetingURL.Value = json("join_url").ToString()
                hdnMeetingStatus.Value = "SUCCESS"

            End If

        Catch
        End Try
        'System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    End Sub

    Public Sub listLiveMeetings()
        Try
            Dim URL As String = String.Empty
            Dim service As String = "6"
            URL = "https://api.zoom.us/v1/meeting/live"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hdnHostID.Value + ""

            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub
    Public Sub termianteMeeting(ByVal sessionKey As String, ByVal hostID As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "7"
            URL = "https://api.zoom.us/v1/meeting/end"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""
            urlParameter += "&id=" + sessionKey + ""


            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub DeleteMeeting(ByVal sessionKey As String, ByVal hostID As String)
        Try
            Dim URL As String = String.Empty
            Dim service As String = "3"
            URL = "https://api.zoom.us/v1/meeting/delete"
            Dim urlParameter As String = String.Empty

            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""
            urlParameter += "&id=" + sessionKey + ""

            hdnSessionKey.Value = sessionKey
            makeZoomAPICall(urlParameter, URL, service)

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub


    Public Sub createZoomMeeting(title As String, hostID As String)
        Try


            Dim URL As String = String.Empty

            Dim service As String = "1"

            URL = "https://api.zoom.us/v1/meeting/create"

            Dim urlParameter As String = String.Empty


            urlParameter += "api_key=" + apiKey + ""
            urlParameter += "&api_secret=" + apiSecret + ""
            urlParameter += "&data_type=JSON"
            urlParameter += "&host_id=" + hostID + ""

            urlParameter += "&topic=" + title + ""
            urlParameter += "&type=3"
            urlParameter += "&option_jbh=true"
            urlParameter += "&option_host_video=false"
            urlParameter += "&option_audio=Both"


            makeZoomAPICall(urlParameter, URL, service)
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Change Coaching")
        End Try
    End Sub


    Public Sub SwitchStudentAndCreateTrainingSession(UserID As String, SignUpId As String)
        Try


            Dim cmdText As String = Nothing
            cmdText = "select IP.FirstName +' '+ IP.LastName as CoachName,CS.EventYear,CS.MemberID,CS.EventID,CS.SignupID,CS.MeetingKey,CS.EventCode,CS.Semester,CS.ProductGroupID,CS.ProductGroupCode,CS.ProductID,CS.ProductCode,CS.Level,CS.SessionNo,CS.Day,CS.Time,[Begin],[End],Cs.UserID,CS.PWD,CD.StartDate,CD.EndDate,CS.MaxCapacity,CD.ScheduleType,CS.Day, VL.HostID from CalsignUp CS inner join CoachingDateCal CD on (CS.ProductGroupID=CD.ProductGroupID and CS.ProductID=CD.ProductID and CS.EventID=CD.EventId and CD.EventYear='2016' and ScheduleType='term') inner join IndSpouse IP on (CS.MemberID=IP.AutoMemberID) inner join VirtualRoomLookUp Vl on(CS.VRoom=Vl.Vroom) where Accepted='Y' and SignUpId=" & SignUpId & ""

            Dim ds As New DataSet()
            ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

            If ds.Tables(0) IsNot Nothing Then
                If ds.Tables(0).Rows.Count > 0 Then
                    For Each dr As DataRow In ds.Tables(0).Rows

                        Dim hostID As String = String.Empty
                        Dim WebExID As String = dr("UserID").ToString()
                        Dim WebExPwd As String = dr("PWD").ToString()

                        hostID = dr("HostID").ToString()

                        Dim Capacity As Integer = Convert.ToInt32(dr("MaxCapacity").ToString())
                        Dim ScheduleType As String = dr("ScheduleType").ToString()

                        Dim year As String = dr("EventYear").ToString()
                        Dim eventID As String = dr("EventID").ToString()

                        Dim ProductGroupID As String = dr("ProductGroupID").ToString()
                        Dim ProductGroupCode As String = dr("ProductGroupCode").ToString()
                        Dim ProductID As String = dr("ProductID").ToString()
                        Dim ProductCode As String = dr("ProductCode").ToString()
                        Dim Semester As String = dr("Semester").ToString()
                        Dim Level As String = dr("Level").ToString()
                        Dim Sessionno As String = dr("SessionNo").ToString()
                        Dim CoachID As String = dr("MemberID").ToString()
                        Dim MeetingPwd As String = "training"

                        Dim Time As String = dr("Time").ToString()
                        Dim Day As String = dr("Day").ToString()
                        Dim STime As String = dr("Begin").ToString()
                        Dim ETime As String = dr("End").ToString()

                        Dim startDate As String = Convert.ToDateTime(dr("StartDate").ToString()).ToString("MM/dd/yyyy")
                        Dim EndDate As String = Convert.ToDateTime(dr("EndDate").ToString()).ToString("MM/dd/yyyy")
                        'string endTime = "01:00 AM";
                        'TimeSpan time1 = DateTime.Parse(endTime).Subtract(DateTime.Parse(Time));
                        'TimeSpan time2 = GetTimeFromString1(dr["End"].ToString());
                        'double hours = (time1 - time2).TotalHours;

                        Dim timeZoneID As String = "11"
                        Dim TimeZone As String = "EST/EDT � Eastern"
                        SignUpId = dr("SignupID").ToString()
                        Dim Mins As Double = 0.0
                        Dim dFrom As DateTime
                        Dim dTo As DateTime

                        Dim sDateFrom As String = STime
                        Dim sDateTo As String = ETime
                        If DateTime.TryParse(sDateFrom, dFrom) AndAlso DateTime.TryParse(sDateTo, dTo) Then
                            Dim TS As TimeSpan = dTo - dFrom

                            Mins = TS.TotalMinutes
                        End If
                        Dim durationHrs As String = Mins.ToString("0")
                        If durationHrs.IndexOf("-") > -1 Then
                            durationHrs = "188"
                        End If

                        If timeZoneID = "4" Then
                            TimeZone = "EST/EDT � Eastern"
                        ElseIf timeZoneID = "7" Then
                            TimeZone = "CST/CDT - Central"
                        ElseIf timeZoneID = "6" Then
                            TimeZone = "MST/MDT - Mountain"
                        End If

                        Dim CoachName As String = String.Empty

                        CoachName = dr("CoachName").ToString()
                        Dim meetingTitle As String = String.Empty
                        meetingTitle = CoachName & "-" & ProductCode & "-" & Level & "-" & Sessionno
                        If dr("MeetingKey").ToString() = "" Or dr("MeetingKey").ToString() = "0" Then
                            'CreateTrainingSession(WebExID, WebExPwd, ScheduleType, Capacity, startDate.Replace("-", "/"), Time, _
                            'Day, STime, ETime, EndDate.Replace("-", "/"), durationHrs, CoachName, ProductCode)
                            createZoomMeeting(meetingTitle, hostID)
                            If hdnMeetingStatus.Value = "SUCCESS" Then
                                ' GetHostUrlMeeting(WebExID, WebExPwd)
                                'WebExID = "michael.simson@capestart.com"
                                'WebExPwd = "mxh894"
                                Dim meetingURL As String = hdnHostMeetingURL.Value
                                cmdText = "usp_WebConfSessions '1','SetupTrainingSession'," & year & "," & eventID & ",112," & ProductGroupID & ",'" & ProductGroupCode & "'," & ProductID & ",'" & ProductCode & "','" & startDate & "','" & EndDate & "','" & STime & "','" & ETime & "'," & durationHrs & "," & timeZoneID & ",'" & TimeZone & "','" & SignUpId & "'," & CoachID & ",'" & Semester & "','" & Level & "','" & Sessionno & "','" & meetingURL & "','" & hdnTrainingSessionKey.Value & "','" & MeetingPwd & "','Active','" & Day & "','" & UserID & "','" & WebExID & "','" & WebExPwd & "', 'Recurring Meeting'"

                                Dim objDs As New DataSet()
                                objDs = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                                If objDs IsNot Nothing AndAlso objDs.Tables.Count > 0 Then
                                    If objDs.Tables(0).Rows.Count > 0 Then
                                        If Convert.ToInt32(objDs.Tables(0).Rows(0)("Retval").ToString()) > 0 Then

                                            Dim dsChild As New DataSet()
                                            Dim ChildText As String = "select C1.ChildNumber,CR.CoachRegID,C1.MemberID,C1.Gender,C1.FIRST_NAME +' '+ C1.LAST_NAME as Name,case when C1.OnlineClassEmail IS NULL then IP.Email else C1.OnlineClassEmail end as Email,IP.FirstName +' '+ IP.LastName as ParentName,C1.Grade,CR.AttendeeJoinURL,CR.CMemberID,IP.City,IP.Country,C1.OnlineClassEmail from child C1 inner join IndSpouse IP on(IP.AutoMemberID=C1.MemberID) inner join CoachReg CR on(CR.ChildNumber=C1.ChildNumber and CR.ProductGroupID='" & ProductGroupID & "' and CR.ProductID='" & ProductID & "' and CR.CMemberID=" & CoachID & " and CR.EventYear=" & ddlEventYear.SelectedValue & ") where C1.ChildNumber in(select ChildNumber from CoachReg where EventYear=" & ddlEventYear.SelectedValue & " and ProductGroupID='" & ProductGroupID & "' and ProductID='" & ProductID & "' and CMemberID=" & CoachID & " and Approved='Y' and Level = '" & Level & "' and SessionNo=" & Sessionno & ") and CR.Level='" & Level & "' and CR.SessionNo=" & Sessionno & ""
                                            dsChild = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, ChildText)
                                            Dim ChidName As String = String.Empty
                                            Dim Email As String = String.Empty
                                            Dim City As String = String.Empty
                                            Dim Country As String = String.Empty
                                            Dim ChildNumber As String = String.Empty
                                            Dim CoachRegID As String = String.Empty

                                            If dsChild IsNot Nothing AndAlso dsChild.Tables.Count > 0 Then
                                                If dsChild.Tables(0).Rows.Count > 0 Then
                                                    For Each drChild As DataRow In dsChild.Tables(0).Rows
                                                        ChidName = drChild("Name").ToString()
                                                        Email = drChild("Email").ToString()
                                                        City = drChild("City").ToString()
                                                        Country = drChild("Country").ToString()
                                                        ChildNumber = drChild("ChildNumber").ToString()
                                                        CoachRegID = drChild("CoachRegID").ToString()

                                                        ' RegisterMeetingAttendee(hdnTrainingSessionKey.Value, WebExID, WebExPwd, "", ChidName, City, Email, Country)
                                                        If hdnMeetingStatus.Value = "SUCCESS" Then

                                                            ' GetJoinMeetingURL(hdnTrainingSessionKey.Value, WebExID, WebExPwd, hdnMeetingAttendeeID.Value, ChidName, Email, MeetingPwd)
                                                            Dim CmdChildUpdateText As String = "update CoachReg set AttendeeJoinURL='" & hdnMeetingURL.Value & "', Status='Active',ModifyDate=GetDate(), ModifiedBy='" & UserID & "' where CoachRegID='" & CoachRegID & "'"

                                                            CmdChildUpdateText = "update CR set CR.CMemberID = C.MemberID, CR.Level = C.Level, CR.SessionNo=C.SessionNo,CR.AttendeeJoinURL='" & hdnMeetingURL.Value & "', CR.AttendeeID=null ,CR.ModifyDate=Getdate(),CR.ModifiedBy=" & Session("LoginID") & " from CoachReg CR, CalSignUp C where C.SignUpID =" & SignUpId & " and CR.CoachRegID = " & CoachRegID & ""

                                                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdChildUpdateText)
                                                        End If
                                                    Next
                                                End If
                                            End If
                                        Else

                                        End If
                                    End If
                                End If


                            Else
                            End If
                        Else

                        End If
                    Next

                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Change Coaching")
        End Try
    End Sub

    Protected Sub DGCoach_PageIndexChanged(source As Object, e As DataGridPageChangedEventArgs)
        Dim DG As DataGrid = source

        DG.CurrentPageIndex = e.NewPageIndex

        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroup.Items.Count > 0 And ddlProductGroup.SelectedValue <> "Select Product Group") Then
        '    prdGroup = ddlProductGroup.SelectedValue
        'End If
        'If (ddlProduct.Items.Count > 0 And ddlProduct.SelectedValue <> "Select Product") Then
        '    prdID = ddlProduct.SelectedValue
        'End If
        'If (ddlLevel.Items.Count > 0 And ddlLevel.SelectedValue <> "Select Level") Then
        '    strlevel = ddlLevel.SelectedValue
        'End If
        'If (ddlVolName.Items.Count > 0 And ddlVolName.SelectedValue <> "Select Volunteer") Then
        '    coach = ddlVolName.SelectedValue
        'End If
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DG)
        DG.CurrentPageIndex = e.NewPageIndex
    End Sub

    Public Sub fillYearFiletr()

        Dim Year As Integer = Convert.ToInt32(DateTime.Now.Year)

        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year + 1) + "-" + Convert.ToString(Year + 2).Substring(2, 2)), Convert.ToString(Year + 1)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year) + "-" + Convert.ToString(Year + 1).Substring(2, 2)), Convert.ToString(Year)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 1) + "-" + Convert.ToString(Year).Substring(2, 2)), Convert.ToString(Year - 1)))

        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 2) + "-" + Convert.ToString(Year - 1).Substring(2, 2)), Convert.ToString(Year - 2)))
        ddlEventyearFilter.Items.Add(New ListItem((Convert.ToString(Year - 3) + "-" + Convert.ToString(Year - 2).Substring(2, 2)), Convert.ToString(Year - 3)))


        ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(eventyear) from EventFees where EventId=13")
        If (Session("RoleId").ToString() = "88") Then
            ddlEventyearFilter.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select max(eventyear) from CalSignup where MemberId= " & Session("LoginId").ToString() & "")
        End If
    End Sub

    Public Sub loadProductGroupFilter()
        Try
            Dim strSql As String = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId where EF.EventYear=" & ddlEventyearFilter.SelectedValue & " AND  P.EventId=13  "

            If (ddlPhaseFilter.SelectedValue <> "0") Then
                strSql = strSql & " and Semester='" & ddlPhaseFilter.SelectedValue & "'"
            End If



            If (Session("RoleId").ToString() = "88") Then
                strSql = "SELECT  Distinct P.ProductGroupID, P.Name from ProductGroup P INNER JOIN EventFees EF ON EF.ProductGroupID = P.ProductGroupID AND EF.EventId = P.EventId inner join CalSignup Cs on (cs.ProductGroupId=P.ProductGroupId) where EF.EventYear=" & ddlEventyearFilter.SelectedValue & " AND  P.EventId=13 and cs.MemberId=" & Session("LoginId").ToString() & " "

                If (ddlPhaseFilter.SelectedValue <> "0") Then
                    strSql = strSql & " and Semester='" & ddlPhaseFilter.SelectedValue & "'"
                End If
            End If
            strSql = strSql & " order by P.ProductGroupID"
            Dim conn As New SqlConnection(Application("ConnectionString"))

            Dim drproductgroup As New DataSet()
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlProductGroupFilter.DataValueField = "ProductGroupID"
            ddlProductGroupFilter.DataTextField = "Name"

            ddlProductGroupFilter.DataSource = drproductgroup
            ddlProductGroupFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlProductGroupFilter.Items.Insert("0", "Select")
                ddlProductGroupFilter.Enabled = True
            Else
                ddlProductGroupFilter.Enabled = False
            End If



        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadProductFilter()
        Try
            Dim strSql As String = " Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId  where EF.EventYear=" & ddlEventyearFilter.SelectedValue & " AND P.EventID=13 "

            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and P.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If
            If (ddlPhaseFilter.SelectedValue <> "0") Then
                strSql = strSql & " and Semester='" & ddlPhaseFilter.SelectedValue & "'"
            End If

            If (Session("RoleId").ToString() = "88") Then
                strSql = "Select P.ProductID, P.Name from Product P INNER JOIN EventFees EF ON EF.ProductID = P.ProductID AND EF.EventId = P.EventId inner join CalSignup cs on (cs.Productid=P.ProductId)  where EF.EventYear=" & ddlEventyearFilter.SelectedValue & " AND P.EventID=13 and cs.Memberid=" & Session("LoginId").ToString() & ""
                If ddlProductGroupFilter.SelectedValue <> "Select" Then
                    strSql = strSql & " and P.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
                End If
                If (ddlPhaseFilter.SelectedValue <> "0") Then
                    strSql = strSql & " and Semester='" & ddlPhaseFilter.SelectedValue & "'"
                End If
            End If

            strSql = strSql & "order by P.ProductID"


            Dim conn As New SqlConnection(Application("ConnectionString"))


            Dim drproductgroup As New DataSet()
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlProductFilter.DataValueField = "ProductID"
            ddlProductFilter.DataTextField = "Name"

            ddlProductFilter.DataSource = drproductgroup
            ddlProductFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlProductFilter.Items.Insert(0, "Select")
                ddlProductFilter.Enabled = True
            Else
                ddlProductFilter.Enabled = False
            End If



        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadLevelFilter()
        Try
            Dim strSql As String = " select distinct LevelCode from ProdLevel where EventYear=" & ddlEventyearFilter.SelectedValue & "  and EventID=13 "
            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If

            If ddlProductFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and ProductID=" & ddlProductFilter.SelectedValue & ""
            End If

            If (Session("RoleId").ToString() = "88") Then
                strSql = "select distinct LevelCode from ProdLevel  P inner join CalSignup CS on (cs.Level=P.LevelCode) where P.EventYear=" & ddlEventyearFilter.SelectedValue & "  and P.EventID=13 and cs.memberId=" & Session("LoginId").ToString() & " "
                If ddlProductGroupFilter.SelectedValue <> "Select" Then
                    strSql = strSql & " and P.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
                End If
                If ddlProductFilter.SelectedValue <> "Select" Then
                    strSql = strSql & " and P.ProductID=" & ddlProductFilter.SelectedValue & ""
                End If
            End If
            Dim drproductgroup As New DataSet()

            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)
            ddlLevelFilter.DataValueField = "LevelCode"
            ddlLevelFilter.DataTextField = "LevelCode"
            ddlLevelFilter.DataSource = drproductgroup
            ddlLevelFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlLevelFilter.Items.Insert(0, "Select")
                ddlLevelFilter.Enabled = True
            Else
                ddlLevelFilter.Enabled = False
            End If


        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub loadCoachFilter()
        Try
            Dim strSql As String = " select Distinct I.Firstname + ' ' + I.Lastname as Name, I.AutoMemberId as MemberID,I.LastName,I.FirstName from IndSpouse I Inner Join CalSignup V On (V.MemberID = I.AutoMemberID and V.EventYear=" & ddlEventyearFilter.SelectedValue & ")  Where V.EventYear= " & ddlEventyearFilter.SelectedValue & "  "

            If ddlProductGroupFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and V.ProductGroupID=" & ddlProductGroupFilter.SelectedValue & ""
            End If
            If ddlProductFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and V.ProductID=" & ddlProductFilter.SelectedValue & ""
            End If
            If ddlLevelFilter.SelectedValue <> "Select" Then
                strSql = strSql & " and V.Level=" & ddlLevelFilter.SelectedValue & ""
            End If
            If ddlPhaseFilter.SelectedValue <> "0" Then
                strSql = strSql & " and V.Semester='" & ddlPhaseFilter.SelectedValue & "'"
            End If
            If (Session("RoleId").ToString() = "88") Then
                strSql = strSql & " and V.MemberId='" & Session("LoginId").ToString() & "'"
            End If

            strSql = strSql & " order by I.FirstName,I.LastName"

            Dim drproductgroup As New DataSet()
            Dim conn As New SqlConnection(Application("ConnectionString"))
            drproductgroup = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, strSql)

            ddlCoachFilter.DataValueField = "MemberID"
            ddlCoachFilter.DataTextField = "Name"

            ddlCoachFilter.DataSource = drproductgroup
            ddlCoachFilter.DataBind()

            If (drproductgroup.Tables(0).Rows.Count > 1) Then
                ddlCoachFilter.Items.Insert(0, "Select")
                ddlCoachFilter.Enabled = True
            Else
                ddlCoachFilter.Enabled = False
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub ddlProductGroupFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        loadProductFilter()
        loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
        '    prdGroup = ddlProductGroupFilter.SelectedValue
        'End If
        'If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
        '    prdID = ddlProductFilter.SelectedValue
        'End If
        'If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
        '    strlevel = ddlLevelFilter.SelectedValue
        'End If
        'If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
        '    coach = ddlCoachFilter.SelectedValue
        'End If
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

    End Sub

    Protected Sub ddlProductFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        loadLevelFilter()
        loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub

    Protected Sub btnSearchFilter_Click(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub

    Protected Sub btnClearFilter_Click(sender As Object, e As EventArgs)
        Try


            DGCoach.EditItemIndex = -1
            ddlProductGroupFilter.SelectedValue = "Select"
            ddlProductFilter.SelectedValue = "Select"
            ddlLevelFilter.SelectedValue = "Select"
            ddlCoachFilter.SelectedValue = "Select"
            ddlAcceptedFilter.SelectedValue = "B"
            ddlPhaseFilter.SelectedValue = "0"
            ddlSessionFilter.SelectedValue = "Select"
            ddlDayFilter.SelectedValue = "0"

            Dim prdGroup As String = String.Empty
            Dim prdID As String = String.Empty
            Dim strlevel As String = String.Empty
            Dim coach As String = String.Empty

            If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
                prdGroup = ddlProductGroupFilter.SelectedValue
            End If
            If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
                prdID = ddlProductFilter.SelectedValue
            End If
            If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
                strlevel = ddlLevelFilter.SelectedValue
            End If
            If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
                coach = ddlCoachFilter.SelectedValue
            End If

            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
            LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
        Catch ex As Exception

        End Try
    End Sub

    Protected Sub ddlLevelFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
        '    prdGroup = ddlProductGroupFilter.SelectedValue
        'End If
        'If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
        '    prdID = ddlProductFilter.SelectedValue
        'End If
        'If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
        '    strlevel = ddlLevelFilter.SelectedValue
        'End If
        'If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
        '    coach = ddlCoachFilter.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub

    Protected Sub ddlSessionFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        ' loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
        '    prdGroup = ddlProductGroupFilter.SelectedValue
        'End If
        'If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
        '    prdID = ddlProductFilter.SelectedValue
        'End If
        'If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
        '    strlevel = ddlLevelFilter.SelectedValue
        'End If
        'If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
        '    coach = ddlCoachFilter.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub
    Protected Sub ddlDayFilter_SelectedIndexChanged1(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        ' loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
        '    prdGroup = ddlProductGroupFilter.SelectedValue
        'End If
        'If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
        '    prdID = ddlProductFilter.SelectedValue
        'End If
        'If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
        '    strlevel = ddlLevelFilter.SelectedValue
        'End If
        'If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
        '    coach = ddlCoachFilter.SelectedValue
        'End If

        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub
    Protected Sub ddlPhaseFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        loadProductGroupFilter()
        loadProductFilter()
        loadLevelFilter()
        loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If



        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub

    Protected Sub ddlCoachFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If



        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub

    Protected Sub ddlAcceptedFilter_SelectedIndexChanged1(sender As Object, e As EventArgs)
        DGCoach.EditItemIndex = -1
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If


        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)

    End Sub

    Protected Sub loadGridBasedOnmemberID(MemberID As String, SignupID As String, DG As DataGrid, Semester As String)
        Dim cmdtext As String = String.Empty
        Dim editIndex
        cmdtext = "SELECT C.MeetingKey,C.MeetingPwd,C.HostJoinURL,C.MakeUpMeetKey,C.MakeUpURL,C.SignUpID,C.MemberID,I.FirstName +' '+ I.LastName as Name, I.Email, C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, P.ProductCode as ProductCode, C.[Level],C.SessionNo, C.ProductGroupID, PG.Name as ProductGroupCode, C.Day as Day , CONVERT(varchar(15),CAST(C.TIME AS TIME),100) as Time,C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,/*count(distinct(CS.EventYear))*/ (select count(distinct( CR.EventYear)) from CoachReg CR where CR.CMemberId=C.MemberId and CR.Approved='Y' and CR.EventYear < " & ddlEventYear.SelectedValue & ") as Years,count(CS.MemberID) as Sessions,(select count(*) from CoachReg where EventYear=C.EventYear and EventID=C.EventID and ProductGroupID=C.ProductGroupID and ProductID=C.ProductID and CMemberID=C.MemberID and Approved='Y' and Level=C.Level and C.Accepted='Y' and SessionNo=C.SessionNo and Semester=C.Semester) as NStudents, C.[Begin], C.[End], Vl.HostID, (select distinct productcode +',' as 'data()' from calSignup where  EventID=C.EventId  and MemberID=c.memberId and accepted='Y' and Eventyear not in (select max(EventYear) from EventFees where EventId=13 ) FOR XML PATH('')) as ExpInProducts FROM CalSignUp C INNER JOIN IndSpouse I ON C.MemberID=I.AutoMemberID left join CalSignup Cs on (I.AutoMemberID=CS.memberID and CS.Accepted='Y') INNER JOIN ProductGroup PG ON C.ProductGroupID=PG.ProductGroupId INNER JOIN Product P ON C.ProductID=P.ProductID left join VirtualRoomLookUp VL on(Vl.Vroom=C.Vroom) where C.EventYear=" + ddlEventYear.SelectedValue + " AND C.EventID=" + ddlEvent.SelectedValue + " and C.MemberID=" + MemberID + " and C.Semester='" & Semester & "'"
        If DG.ID = "DGCoachNA" Then
            'Added by Bindhu
            cmdtext = cmdtext & " and c.memberid not in ( select Distinct(MemberID) from CalSignUp where Accepted = 'Y' and eventyear=" & ddlEventYear.SelectedValue & " and Semester='" & Semester & "') "
        End If
        cmdtext = cmdtext & "  group by C.MeetingKey,C.MeetingPwd,C.HostJoinURL, C.MakeUpURL,C.MakeUpMeetKey,C.SignUpID,C.MemberID,I.FirstName,I.LastName, I.Email,C.EventYear,C.EventID, C.EventCode,C.Semester ,C.MaxCapacity, C.ProductID, C.[Level],C.SessionNo, C.ProductGroupID,P.ProductCode,C.[Level],C.SessionNo, C.ProductGroupID, PG.Name,C.Day,CONVERT(varchar(15),CAST(C.TIME AS TIME),100),C.Accepted,C.Confirm,C.Preference, C.VRoom, C.UserID, C.PWD,C.[Begin], C.[End], Vl.HostID order by I.LastName, I.FirstName, C.ProductGroupId, C.ProductId, C.Semester, C.[Level] Asc "
        Try
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdtext)
            If ds.Tables.Count > 0 Then
                If ds.Tables(0).Rows.Count > 0 Then
                    DG.DataSource = ds
                    DG.DataBind()

                    'Added by bindhu to reset the current grid-table set into dataset
                    If DG.ID = "DGCoachNA" Then
                        Session("volDataSet_NA") = ds
                    Else
                        Session("volDataSet") = ds
                    End If
                End If
            End If
        Catch ex As Exception
        End Try
    End Sub

    Public Sub LoadExtraSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.VRoom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join calsignup cs on (cs.ExtraSessionKey=VC.SessionKey and VC.MemberID=cs.MemberID)  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID)  where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.Semester='" & ddlPhase.SelectedValue & "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.MemberID=" + Session("LoginID").ToString() + ""
            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) inner join calsignup cs on (cs.ExtraSessionKey=VC.SessionKey and VC.MemberID=cs.MemberID)"
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & ""

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex <> 0, " and VC.ProductGroupId=" & ddlProductGroup.SelectedValue, "") & " " & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " And VC.ProductID = " & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql &= " where CS.EventYear=" & ddlEventYear.SelectedValue & " and VC.SessionType='Extra' and VC.Semester='" & ddlPhase.SelectedValue & "'"
                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and CS.MemberID=" & Request.QueryString("coachID") & ""
                End If
                StrSql &= "order by IP.FirstName, IP.LastNAme ASC"
            End If

            '  If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
            If ds.Tables(0).Rows.Count > 0 Then
                GrdExtraSesion.DataSource = ds
                GrdExtraSesion.DataBind()
                spnExtraSession.Visible = False
                Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                Dim dateVal As String = String.Empty
                Dim i As Integer = 0
                Dim sessionKey As String = String.Empty

                For i = 0 To GrdMeeting.Rows.Count - 1
                    dateVal = GrdMeeting.Rows(i).Cells(13).Text
                    Dim dtDateVal As DateTime = New DateTime()
                    dtDateVal = Convert.ToDateTime(dateVal.ToString())
                    If (dtDateVal < dtTodayDate) Then
                        CType(GrdMeeting.Rows(i).FindControl("btnJoinMeeting"), Button).Enabled = False
                        Dim cmdUpdateText As String = String.Empty
                        sessionKey = GrdMeeting.Rows(i).Cells(10).Text
                        cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                    End If
                Next
            Else
                GrdExtraSesion.DataSource = ds
                GrdExtraSesion.DataBind()
                spnExtraSession.Visible = True
            End If
            ''Else
            '   lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            '' End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdExtraSesion_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "SelectMeetingURL" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim selIndex As Integer = row.RowIndex
                GrdExtraSesion.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                Dim WebExID As String = String.Empty
                Dim WebExPwd As String = String.Empty
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdExtraSesion.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdExtraSesion.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                WebExID = GrdExtraSesion.Rows(selIndex).Cells(21).Text
                WebExPwd = GrdExtraSesion.Rows(selIndex).Cells(22).Text

                hdnSessionKey.Value = sessionKey
                Dim beginTime As String = TryCast(DirectCast(GrdExtraSesion.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdExtraSesion.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdExtraSesion.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If

                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds

                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert()", True)
                    Else
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception
                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)

                        Dim CmdText As String = String.Empty
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)

                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                End If
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub


    Protected Sub GrdMakeupSubstitute_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdMakeupSubstitute.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                Dim beginTime As String = TryCast(DirectCast(GrdMakeupSubstitute.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdMakeupSubstitute.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdMakeupSubstitute.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdMakeupSubstitute.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdMakeupSubstitute.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                    Else
                        Dim CmdText As String = String.Empty
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception

                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Protected Sub GrdExtraSubtitute_RowCommand(sender As Object, e As GridViewCommandEventArgs)
        Try
            Dim row As GridViewRow = Nothing
            If e.CommandName = "Join" Then
                row = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)

                Dim selIndex As Integer = row.RowIndex
                GrdExtraSubtitute.Rows(selIndex).BackColor = Color.FromName("#EAEAEA")
                'string MeetingURl = ((Label)GrdMeeting.Rows[selIndex].FindControl("LbkMeetingURL") as Label).Text;
                Dim beginTime As String = TryCast(DirectCast(GrdExtraSubtitute.Rows(selIndex).FindControl("lblBegTime"), Label), Label).Text
                Dim day As String = TryCast(DirectCast(GrdExtraSubtitute.Rows(selIndex).FindControl("lblMeetDay"), Label), Label).Text
                Dim dtFromS As New DateTime()
                Dim dtEnds As DateTime = DateTime.Now.AddMinutes(60)
                Dim sessionKey As String = String.Empty
                sessionKey = TryCast(DirectCast(GrdExtraSubtitute.Rows(selIndex).FindControl("lblStSessionkey"), Label), Label).Text

                Dim hostID As String = TryCast(DirectCast(GrdExtraSubtitute.Rows(selIndex).FindControl("lblStHostID"), Label), Label).Text
                Dim duration As String = "-" + TryCast(DirectCast(GrdExtraSubtitute.Rows(selIndex).FindControl("lblDuration"), Label), Label).Text
                Dim iduration As Integer = 0
                If (duration = "-") Then
                    iduration = 120
                Else
                    iduration = Convert.ToInt32(duration)
                End If
                Dim mins As Double = 40.0
                If DateTime.TryParse(beginTime, dtFromS) Then
                    Dim TS As TimeSpan = dtFromS - dtEnds
                    mins = TS.TotalMinutes
                End If
                Dim today As String = DateTime.Now.DayOfWeek.ToString()
                If mins <= 30 AndAlso day = today Then
                    If (mins < iduration) Then
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                    Else
                        Dim CmdText As String = String.Empty
                        hdnHostID.Value = hostID
                        Try
                            listLiveMeetings()
                        Catch ex As Exception

                            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
                        End Try

                        getmeetingIfo(sessionKey, hostID)
                        CmdText = "update WebConfLog set Status='InProgress' where SessionKey=" + sessionKey + ""
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, CmdText)
                        Dim meetingLink As String = hdnHostMeetingURL.Value
                        hdnHostMeetingURL.Value = meetingLink
                        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "JoinMeeting();", True)
                    End If

                Else
                    System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.[GetType](), "Msg", "showAlert();", True)
                End If
            End If

        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
        End Try
    End Sub

    Public Sub LoadMakeupSubstituteSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.VRoom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachId)  inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID)  where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.SubstituteCoachId=" + Session("LoginID").ToString() + " and VC.SubstituteCoachId is not null "
            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachId)  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) inner join calsignup cs on (cs.MakeUpMeetKey=VC.SessionKey and VC.MemberID=cs.MemberID)"
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & ""
                'AND VC.Semester=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql &= " where CS.EventYear=" & ddlEventYear.SelectedValue & " and VC.SessionType='Scheduled Meeting' and VC.Semester='" & ddlPhase.SelectedValue & "' and VC.SubstituteCoachId is not null "
                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and CS.MemberID=" & Request.QueryString("coachID") & ""
                End If
                StrSql &= " order by IP.FirstName, IP.LastNAme ASC"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    GrdMakeupSubstitute.DataSource = ds
                    GrdMakeupSubstitute.DataBind()

                    spnMakeupSubSTatus.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0
                    For i = 0 To GrdMakeupSubstitute.Rows.Count - 1
                        dateVal = GrdMakeupSubstitute.Rows(i).Cells(16).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal < dtTodayDate) Then
                            CType(GrdMakeupSubstitute.Rows(i).FindControl("btnJoin"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = GrdMakeupSubstitute.Rows(i).Cells(11).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    GrdMakeupSubstitute.DataSource = ds
                    GrdMakeupSubstitute.DataBind()
                    spnMakeupSubSTatus.Visible = True
                End If
            Else
                '  lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Public Sub LoadExtraSubstituteSessions()
        Try
            Dim ds As DataSet
            Dim StrSql As String = ""
            If Session("RoleID") = "88" Then
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.VRoom, VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachId) inner join calsignup cs on (VC.MemberID=cs.MemberID and VC.ProductGroupId=cs.ProductGroupid and VC.ProductId=cs.productId and VC.Level=cs.Level and VC.Session=cs.SessionNo and vc.Semester=cs.Semester and Vc.SessionType='Extra' and vc.SubstitutecoachId is not null and cs.Accepted='Y')  inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID)  where VC.EventYear='" + ddlEventYear.SelectedValue + "' and VC.EventID='" + ddlEvent.SelectedValue + "' and VC.SubstituteCoachId=" + Session("LoginID").ToString() + " and vc.SubstitutecoachId is not null"
            Else
                StrSql = "select VC.EventYear,E.EventCode,C.ChapterCode,VC.ProductGroupCode,VC.ProductCode,VC.SessionKey,VC.StartDate,VC.EndDate,VC.BeginTime,VC.EndTime,VC.Semester,VC.Level,VC.Session,VC.MeetingPwd,VC.Duration,VC.TimeZone,VC.TimeZoneID,IP.FirstName +' '+ IP.LastName as Coach,IP1.FirstName +' '+ IP1.LastName as SubCoach,VC.Status,VC.MeetingUrl,VC.ProductID,VC.ProductGroupID,VC.EventID,VC.ChapterID,VC.TimeZoneID,VC.MemberID,VC.UserId,VC.PWD,VC.Day,VL.Vroom,VL.HostID from WebConfLog VC left join Event E on (E.EventId=VC.EventID) left join Chapter C on(C.ChapterID=VC.ChapterID) left join IndSpouse IP on(IP.AutoMemberID=VC.MemberID) left join IndSpouse IP1 on(IP1.AutoMemberID=VC.SubstituteCoachId) inner join VirtualRoomLookUp VL on(VL.UserID=VC.UserID) inner join calsignup cs on (VC.MemberID=cs.MemberID and VC.ProductGroupId=cs.ProductGroupid and VC.ProductId=cs.productId and VC.Level=cs.Level and VC.Session=cs.SessionNo and vc.Semester=cs.Semester and Vc.SessionType='Extra' and vc.SubstitutecoachId is not null and cs.Accepted='Y')"
                StrSql = StrSql & IIf(ddlEvent.Items.Count > 1 And ddlEvent.SelectedIndex = 0, "", " AND VC.EventID=" & ddlEvent.SelectedValue) & ""
                'AND VC.Semester=" & ddlPhase.SelectedValue

                If Session("RoleId").ToString() <> "88" Then
                    StrSql = StrSql & IIf(ddlProductGroup.Items.Count > 1 And ddlProductGroup.SelectedIndex = 0, "", " AND VC.ProductGroupID=" & ddlProductGroup.SelectedValue) & IIf(ddlProduct.Items.Count > 1 And ddlProduct.SelectedIndex <> 0, " and VC.ProductID=" & ddlProduct.SelectedValue, "") & " "
                End If

                If Request.QueryString("Role") = "Coach" Then 'And Session("RoleID") = 88 Then 'Then '
                    StrSql = StrSql & " and VC.MemberID=" & IIf(ddlVolName.SelectedIndex > 0, ddlVolName.SelectedValue, Session("LoginID")) & ""
                End If
                StrSql &= " where CS.EventYear=" & ddlEventYear.SelectedValue & " and VC.SessionType='Extra' and VC.Semester='" & ddlPhase.SelectedValue & "' and vc.SubstitutecoachId is not null "
                If (Request.QueryString("coachID") Is Nothing) Then
                Else
                    StrSql = StrSql & " and CS.MemberID=" & Request.QueryString("coachID") & ""
                End If
                StrSql &= "order by IP.FirstName, IP.LastNAme ASC"
            End If
            'Response.Write(StrSql)
            If ddlEvent.Items.Count > 0 And ddlProductGroup.Items.Count > 0 Then
                ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSql)
                If ds.Tables(0).Rows.Count > 0 Then
                    GrdExtraSubtitute.DataSource = ds
                    GrdExtraSubtitute.DataBind()

                    spnExtraSubStatus.Visible = False
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    Dim dateVal As String = String.Empty
                    Dim i As Integer = 0
                    For i = 0 To GrdExtraSubtitute.Rows.Count - 1
                        dateVal = GrdExtraSubtitute.Rows(i).Cells(16).Text
                        Dim dtDateVal As DateTime = New DateTime()
                        dtDateVal = Convert.ToDateTime(dateVal.ToString())
                        If (dtDateVal < dtTodayDate) Then
                            CType(GrdExtraSubtitute.Rows(i).FindControl("btnJoin"), Button).Enabled = False
                            Dim cmdUpdateText As String = String.Empty
                            Dim sessionKey As String = GrdExtraSubtitute.Rows(i).Cells(11).Text
                            cmdUpdateText = "Update WebConfLog set [Status]='Closed' where SessionKey=" & sessionKey & ""
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, cmdUpdateText)
                        End If
                    Next
                Else
                    GrdExtraSubtitute.DataSource = ds
                    GrdExtraSubtitute.DataBind()

                    spnExtraSubStatus.Visible = True
                End If
            Else
                '  lblerr.Text = "No Event / ProductGroup Present for the selected Year."
            End If
        Catch ex As Exception
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Calendar Signup")
            'Response.Write(ex.ToString())
        End Try
    End Sub

    Protected Sub ddlEventyearFilter_SelectedIndexChanged(sender As Object, e As EventArgs)
        loadPhaseFilter()

        loadProductGroupFilter()
        loadProductFilter()
        loadLevelFilter()
        loadCoachFilter()
        Dim prdGroup As String = String.Empty
        Dim prdID As String = String.Empty
        Dim strlevel As String = String.Empty
        Dim coach As String = String.Empty
        'If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
        '    prdGroup = ddlProductGroupFilter.SelectedValue
        'End If
        'If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
        '    prdID = ddlProductFilter.SelectedValue
        'End If
        'If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
        '    strlevel = ddlLevelFilter.SelectedValue
        'End If
        'If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
        '    coach = ddlCoachFilter.SelectedValue
        'End If
        If (ddlProductGroupFilter.Items.Count > 0 And ddlProductGroupFilter.SelectedValue <> "Select") Then
            prdGroup = ddlProductGroupFilter.SelectedValue
        End If
        If (ddlProductFilter.Items.Count > 0 And ddlProductFilter.SelectedValue <> "Select") Then
            prdID = ddlProductFilter.SelectedValue
        End If
        If (ddlLevelFilter.Items.Count > 0 And ddlLevelFilter.SelectedValue <> "Select") Then
            strlevel = ddlLevelFilter.SelectedValue
        End If
        If (ddlCoachFilter.Items.Count > 0 And ddlCoachFilter.SelectedValue <> "Select") Then
            coach = ddlCoachFilter.SelectedValue
        End If

        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoach)
        LoadGrid(ddlAcceptedFilter.SelectedValue, prdGroup, prdID, strlevel, coach, DGCoachNA)
    End Sub
    'Private Sub loadPhaseFilter()

    '    ddlPhaseFilter.Items.Clear()
    '    Dim arrPhase As ArrayList = New ArrayList()
    '    Dim objCommon As Common = New Common()
    '    arrPhase = objCommon.GetSemesters()
    '    For i As Integer = 0 To 2
    '        ddlPhaseFilter.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
    '    Next
    '    ddlPhaseFilter.SelectedValue = objCommon.GetDefaultSemester(ddlEventyearFilter.SelectedValue)
    'End Sub
    Public Function GetDefaultYear() As String
        Dim EventYear As String = String.Empty
        Dim CmdText As String = "select Max(EventYear) from EventFees where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and CalSignupOpen='Y'"
        Try
            EventYear = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, CmdText).ToString()
        Catch ex As Exception

        End Try

        Return EventYear
    End Function

    Public Function GetDefaultSemester() As String
        Dim Semester As String = String.Empty
        Dim CmdText As String = "select Max(Semester) from EventFees where ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and CalSignupOpen='Y'"
        Try
            Semester = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, CmdText).ToString()
        Catch ex As Exception

        End Try
        Return Semester
    End Function
    Private Sub loadPhaseFilter()
        Try


            ddlPhaseFilter.Items.Clear()
            Dim arrPhase As ArrayList = New ArrayList()
            Dim objCommon As Common = New Common()
            arrPhase = objCommon.GetSemesters()
            ddlPhaseFilter.Items.Add(New ListItem("Select", "0"))
            For i As Integer = 0 To 2
                ddlPhaseFilter.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
            Next
            If (Session("RoleId").ToString() = "88") Then
                ddlPhaseFilter.Items.Clear()
                Dim CmdText As String = " Select distinct Semester from CalSignup where MemberId=" & Session("LoginId").ToString() & ""
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, CmdText)
                If (ds.Tables.Count > 0) Then
                    If (ds.Tables(0).Rows.Count > 0) Then

                        ddlPhaseFilter.DataValueField = "Semester"
                        ddlPhaseFilter.DataTextField = "Semester"
                        ddlPhaseFilter.DataSource = ds
                        ddlPhaseFilter.DataBind()
                        ddlPhaseFilter.Items.Add(New ListItem("Select", "0"))
                        ddlPhaseFilter.SelectedValue = "0"
                    End If
                End If
            End If
            '' Dim OpenSemester As String = SqlHelper.ExecuteScalar(Application("connectionstring"), CommandType.Text, "select Max(Semester) from EventFees where CalSignupOpen='Y' and EventYear=" & ddlEventYear.SelectedValue & "")
            '' ddlPhase.SelectedValue = OpenSemester
        Catch ex As Exception

        End Try
    End Sub
End Class
