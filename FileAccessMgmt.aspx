﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="FileAccessMgmt.aspx.vb" Inherits="FileAccessMgmt" title="File Access Management" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 
<div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
    <asp:LinkButton ID="LinkButton1" PostBackUrl="WebPageMgmtMain.aspx" runat="server">Back</asp:LinkButton>
</div>
<div runat ="server" id="divMulti" visible = "false" style ="width:800px; text-align : center ">
<table border="0px" width="100%" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align = "center" colspan="2" >Select Folder : 
    <asp:DropDownList ID="ddlselectFolder" AutoPostBack="true"  DataValueField="FolderListID" DataTextField="DisplayName" runat="server">
    </asp:DropDownList> <br/>
    <asp:Label ID="lblVErr" runat="server" ></asp:Label>
    </td> </tr> </table></div>
<div  runat ="server" id="divAll" visible = "true" style ="width:800px; text-align : center ">
<table border="0px" cellpadding = "5px" cellspacing = "0px" class ="SmallFont">
<tr><td align="center" width="800px">
<div align="center">
<table border="0" cellpadding ="3" cellspacing ="0"> 
 <tr><td align="center" colspan="3"><h5>Manage File Access Privileges</h5></td></tr>
<tr><td align="center">
<table runat = "server"  id="tblddls" border="0" cellpadding = "3" cellspacing = "0">
<tr><td align = "left" width="125px" > Level 1 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel1"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel1_SelectedIndexChanged" Width="150px" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 2 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel2"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel2_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 3 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel3"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel3_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 4 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel4"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel4_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 5 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel5"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel5_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 6 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel6"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel6_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 7 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel7"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel7_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 8 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel8"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel8_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 9 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel9"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel9_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 10 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel10"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel10_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 11 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel11"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel11_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>
<tr><td align = "left" > Level 12 Folders</td><td align="left"><asp:DropDownList ID="ddlLevel12"  runat="server" DataTextField="DisplayName" DataValueField = "value" OnSelectedIndexChanged="ddlLevel12_SelectedIndexChanged" Width="150px" Enabled="false" AutoPostBack="true"></asp:DropDownList></td></tr>


<tr><td align = "center" colspan="2"><asp:Label ID="lblError" ForeColor="Red"  runat="server"></asp:Label>&nbsp;</td></tr>


<tr><td align = "center" colspan="2" > 
    <asp:Button ID="btnContinue" runat="server" Text="Continue" OnClick ="btnContinue_Click"/>
</td></tr>


<tr id="trfilelist" runat = "server"><td align = "left" >List of Files </td><td align = "left" >
    <asp:DropDownList ID="ddlFiles"  DataTextField = "FileName" DataValueField = "FileListId" Width="250px" runat="server">
    </asp:DropDownList>
</td></tr>
<tr><td align = "left" >Role</td><td align = "left" >
    <asp:DropDownList ID="ddlRole" DataTextField="RoleCode" DataValueField="roleID" runat="server">
    </asp:DropDownList>
</td></tr>



</table> 
</td></tr>
<tr><td align="center"> 
    <asp:Label ID="lblURL" runat="server" ></asp:Label></td></tr>
    <tr id="trbuttons"  runat ="server"><td align = "center"  > 
    <asp:Button ID="BtnAddnew" runat="server" Text="Add New Access"  OnClick ="BtnAddnew_Click"/>&nbsp;&nbsp;
    &nbsp;&nbsp;
       <asp:Button ID="BtnClear" runat="server" Text="Clear" OnClick ="BtnClear_Click" />
<%--     <asp:Button ID="hBtn" runat="server" Text="test" OnClientClick ="CheckFile()"  />--%>
 
</td></tr>


<tr id="Tr1" runat="server" visible = "false"><td align = "center"  >
    <asp:Label ID="lblLevel" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblParentFID" runat="server" visible = "false"></asp:Label>
    <asp:Label ID="lblPath" runat="server" visible = "false"></asp:Label>
   
        
<%--<asp:Button ID="btnSave" runat="server" Text="Save" onclick="btnSave_Click" />
<asp:HiddenField ID="hdnIsOverwrite" runat="server" />--%>
    </td></tr>
</table></div>
</td> </tr>

<tr><td align="center">
    <asp:Label ID="lblTable11"  Visible = "false" runat="server" Text="Table 1: Restricted File Access" Font-Bold="true"></asp:Label>
<asp:DataGrid ID="DGVolunteer" runat="server" DataKeyField="RestFAID" 
        AutoGenerateColumns="False" OnItemCommand="DGVolunteer_ItemCommand" 
        CellPadding="4" 
        BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px" 
        BorderStyle="Solid" CellSpacing="2" ForeColor="Black" >
               <FooterStyle BackColor="#CCCCCC" />
               <SelectedItemStyle BackColor="LightBlue" Font-Bold="True" ForeColor="White" />
               <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left" 
                   Mode="NumericPages" />
               <ItemStyle BackColor="White" />
               <COLUMNS>
            <asp:buttoncolumn ItemStyle-ForeColor="Blue" ButtonType="LinkButton"  CommandName="Delete" Text="Delete"></asp:buttoncolumn>
			<asp:EditCommandColumn ItemStyle-ForeColor="Blue"  ButtonType="LinkButton" UpdateText="Update" CancelText="Cancel"  EditText="Edit"></asp:EditCommandColumn> 
            <asp:Boundcolumn  DataField="RestFAID"  HeaderText="RestFAID" Visible="false" />
            
<asp:TemplateColumn HeaderText="Folder URL">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblFolderURL" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FolderURL")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
               <asp:TemplateColumn HeaderText="Role">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblRoleCode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RoleCode")%>'></asp:Label>
                  </ItemTemplate>
                   <EditItemTemplate>
                  <asp:DropDownList id="ddlRole" OnPreRender="ddlRole_PreRender" DataTextField="RoleCode" DataValueField ="RoleID" runat="server" AutoPostBack="false">
                     
                   </asp:DropDownList>
                  </EditItemTemplate>                                               
                  </asp:TemplateColumn >
                   <asp:TemplateColumn HeaderText="File name">
              <HEADERSTYLE  Font-Bold="True"></HEADERSTYLE>
                  <ItemTemplate >
                    <asp:Label ID="lblFileName" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FileName")%>'></asp:Label>
                  </ItemTemplate></asp:TemplateColumn >
     
        </COLUMNS>           
               <HeaderStyle BackColor="White" />
    </asp:DataGrid>
</td></tr>
 </table> </div> 
<center>
    <asp:Label ID="lblNoAccess" runat="server"></asp:Label></center>
</asp:Content>





