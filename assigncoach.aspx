﻿<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="assigncoach.aspx.vb" Inherits="assigncoach" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">

    <link href="css/jquery.qtip.min.css" rel="stylesheet" />
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
    <script src="js/jquery.qtip.min.js"></script>

    <script type="text/javascript">
        function actVolunteerDelete() {
            if (confirm("Are you sure you want to delete?  This is permanent.")) {
                document.getElementById('<%= btnConfirmDelete.ClientID%>').click();
            }
        }

        function DeleteConfirm() {
            if (confirm("Are you sure you want to delete?")) {
                document.getElementById('<%= btnDelete.ClientID%>').click();
            }
        }

        $(function (e) {
            $(".lnkVolunteer").qtip({ // Grab some elements to apply the tooltip tos
                content: {

                    text: function (event, api) {
                        var type = $(this).attr("attr-Type");
                        var tblHtml = "";
                        return $.ajax({
                            url: 'SetupZoomSessions.aspx/GetPreviousClassBasedCoach', // URL to the local file
                            type: 'POST', // POST or GET
                            contentType: "application/json; charset=utf-8",
                            dataType: "json",
                            data: JSON.stringify({ MemberId: $(this).attr("attr-Memberid") }), // Data to pass along with your request
                            success: function (data) {


                                tblHtml += "<table style='border: 1px solid black; border-collapse: collapse; '>";
                                tblHtml += "<tr class='Header'>";

                                tblHtml += "<td class='Header'>Event Year</td>";
                                tblHtml += "<td class='Header'>Product</td>";
                                tblHtml += "<td class='Header'>Level</td>";
                                tblHtml += "<td class='Header'>No. Of Students</td>";
                                if (data.d.length) {
                                    tblHtml += "</tr>";
                                    $.each(data.d, function (index, value) {

                                        tblHtml += "<tr>";

                                        tblHtml += "<td class='tblBody'>" + value.EventYear + "</td>";
                                        
                                        tblHtml += "<td class='tblBody'>" + value.ProductName + "</td>";
                                        tblHtml += "<td class='tblBody'>" + value.Level + "</td>";
                                        tblHtml += "<td class='tblBody'>" + value.NoOfCountChildren + "</td>";

                                        tblHtml += "</tr>";
                                    });
                                } else {
                                    tblHtml = "</tr><td class='tblBody' align='center' colspan='4'><span style='color:red;'>No previous class exists.</span></td></tr>";
                                }
                                tblHtml += "</table>";


                            }
                        }).then(function (data) {
                            return tblHtml;
                        });
                    },


                    title: function (event, api) {
                        return '<span style="font-weight:bold; font-size:14px;">Previous Class Details</span>';
                    },
                    button: 'Close'
                },
                hide: {
                    event: false
                },
                style: {
                    classes: 'qtip-green qtip-shadow qTipWidth'

                },
                show: {
                    solo: true
                }

            })
        });
    </script>
    <style type="text/css">
        .qTipWidth {
            max-width: 700px !important;
        }

        .Header {
            border: 1px solid black;
            border-collapse: collapse;
            vertical-align: middle;
            color: black;
            font-weight: bold;
            padding: 3px 3px;
        }

        .tblBody {
            border: 1px solid black;
            border-collapse: collapse;
            padding: 3px 3px;
        }
    </style>
    <asp:Button ID="btnConfirmDelete" Style="display: none;" runat="server" OnClick="btnConfirmDelete_Click" />
    <asp:Button ID="btnDelete" Style="display: none;" runat="server" OnClick="btnDelete_Click" />
    <div align="left">
        <br />
        <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>

        &nbsp;&nbsp;<asp:HyperLink CssClass="btn_02" ID="HlinkScheduleCoach" runat="server" NavigateUrl="CalendarSignUp.aspx">Schedule Coach</asp:HyperLink>
    </div>
    <div align="center">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="center" colspan="5">
                    <h2>Assign Coach/Admin</h2>
                </td>
            </tr>
            <tr>
                <td align="left">Volunteer Role :</td>
                <td align="left">
                    <asp:DropDownList ID="ddlRole" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" Width="170" AutoPostBack="true" runat="server">
                        <asp:ListItem Selected="True" Value="89">Coach Admin</asp:ListItem>
                        <asp:ListItem Value="88">Coach</asp:ListItem>
                    </asp:DropDownList>
                </td>
                <td align="left">&nbsp;</td>
                <td>
                    <asp:RadioButton ID="RbtnGeneralDb" AutoPostBack="true" Text="General db" runat="server" GroupName="Col" /></td>
                <td>
                    <asp:RadioButton ID="RbtnVolSignUp" AutoPostBack="true" Text="Volunteer Signup" runat="server" GroupName="Col" />

                </td>
            </tr>

            <tr>
                <td align="left">Volunteer Name : </td>
                <td align="left">
                    <asp:TextBox ID="txtParent" Width="170px" runat="server" Enabled="false"></asp:TextBox></td>
                <td align="left">
                    <asp:Button ID="Search" runat="server" Text="Search" Visible="false " /></td>
            </tr>
            <tr runat="server" id="TrPrdGrp" visible="false">
                <td align="left">Product Group</td>
                <td align="left">
                    <asp:DropDownList ID="ddlPrdGroup" Width="170px" DataTextField="Name" DataValueField="code" OnSelectedIndexChanged="ddlPrdGroup_SelectedIndexChanged" AutoPostBack="true" runat="server"></asp:DropDownList></td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr runat="server" id="TrPrd" visible="false">
                <td align="left">Product </td>
                <td align="left">
                    <asp:DropDownList ID="ddlPrd" Enabled="false" DataTextField="Name" DataValueField="code" Width="170px" runat="server"></asp:DropDownList></td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Button ID="btnassigncoach" runat="server" Width="180px" Text="Assign Coach Admin" /></td>
            </tr>

            <tr>
                <td align="center" colspan="5">
                    <asp:Label ID="lblerr" ForeColor="Red" runat="server"></asp:Label></td>
            </tr>
        </table>

        <asp:Panel ID="pIndSearch" runat="server" Width="950px" Visible="False">
            <b>Search NSF member</b>
            <div align="center">
                <table border="1" runat="server" id="tblIndSearch" style="text-align: center" width="30%" visible="true" bgcolor="silver">
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;Last Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtLastName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;First Name:</td>
                        <td align="left">
                            <asp:TextBox ID="txtFirstName" runat="server"></asp:TextBox></td>
                    </tr>
                    <tr>
                        <td class="ItemLabel" valign="top" nowrap align="right">&nbsp;E-Mail:</td>
                        <td align="left">
                            <asp:TextBox ID="txtEmail" runat="server"></asp:TextBox></td>
                    </tr>
                    <%-- <tr>
            <td class="ItemLabel" vAlign="top" noWrap align="right">&nbsp;State:</td>
			<td align="left" >
                <asp:DropDownList ID="ddlState" runat="server">
                </asp:DropDownList></td>
    	</tr>--%>
                    <tr>
                        <td align="right">
                            <asp:Button ID="btnSearch" runat="server" OnClick="btnSearch_onClick" Text="Find" CausesValidation="False" />
                        </td>
                        <td align="left">
                            <asp:Button ID="btnIndClose" runat="server" Text="Close" OnClick="btnIndClose_onclick" CausesValidation="False" />
                        </td>
                    </tr>
                </table>
                <asp:Label ID="lblIndSearch" ForeColor="red" runat="server" Visible="false" Text="Select the Chapter from the DropDown"></asp:Label>
            </div>
            <br />
            <asp:Panel ID="Panel4" runat="server" Visible="False" HorizontalAlign="Center">
                <b>Search Result</b>
                <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridMemberDt" DataKeyNames="AutomemberId" AutoGenerateColumns="false" runat="server" OnRowCommand="GridMemberDt_RowCommand" RowStyle-CssClass="SmallFont">
                    <Columns>
                        <asp:ButtonField DataTextField="AutomemberId" HeaderText="Member Id"></asp:ButtonField>
                        <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                        <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                        <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                        <asp:BoundField DataField="HPhone" HeaderText="Home Phone"></asp:BoundField>
                        <asp:BoundField DataField="address1" HeaderText="Address"></asp:BoundField>
                        <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                        <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                        <asp:BoundField DataField="zip" HeaderText="Zip"></asp:BoundField>
                        <asp:BoundField DataField="chapterCode" HeaderText="Chapter"></asp:BoundField>
                        <asp:BoundField DataField="chapterCode" HeaderText="Products"></asp:BoundField>
                    </Columns>
                </asp:GridView>
            </asp:Panel>


        </asp:Panel>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" id="dvVolunteer" runat="server">
        <center>
            <div style="font-weight: bold;">Table 1: List of Volunteers with  Assigned Roles</div>
        </center>
        <div style="clear: both;"></div>
        <div style="float: left;">
            <asp:Button ID="BtnExportTable1" runat="server" Text="Export To Excel" OnClick="BtnExportTable1_Click" Style="margin-left: 10px;" />
        </div>
        <div style="clear: both;"></div>
        <asp:DataGrid ID="DGVolunteer" runat="server" DataKeyField="volunteerID"
            AutoGenerateColumns="False" OnItemCommand="DGVolunteer_ItemCommand"
            CellPadding="4"
            BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" CellSpacing="2" ForeColor="Black" Width="900px">
            <FooterStyle BackColor="#CCCCCC" />
            <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left"
                Mode="NumericPages" />
            <ItemStyle BackColor="White" />
            <Columns>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnRemove" runat="server" CausesValidation="false" CommandName="Delete" Text="Delete" Visible='<%#DataBinder.Eval(Container.DataItem, "Status")%>'></asp:LinkButton>
                        <div style="display: none">
                            <asp:Label ID="lblAutoMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn HeaderText="Ser#" HeaderStyle-Font-Bold="true">


                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.ItemIndex + 1%>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="volunteerID" HeaderText="Volunteer ID" Visible="false" />
                <asp:TemplateColumn HeaderText="Volunteer Name">

                    <ItemTemplate>

                        <asp:Label runat="server" ID="lnkVolunteer" attr-MemberId='<%#DataBinder.Eval(Container.DataItem, "AutoMemberID")%>' CssClass="lnkVolunteer" Text='<%# Bind("Vname")%>' Style="cursor: pointer; color: blue;"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn HeaderStyle-Font-Bold="true" Visible="false" DataField="Vname" HeaderText="Volunteer Name" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EMail" HeaderText="EMail" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="City" HeaderText="City" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="State" HeaderText="State" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductName" HeaderText="ProductName" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Products" HeaderText="Products" />

            </Columns>
            <HeaderStyle BackColor="White" />
        </asp:DataGrid><br />
    </div>
    <div align="center" id="dvVolSignUp" runat="server">
        <center>
            <div style="font-weight: bold;">Table 2: Volunteers who Signed Up for Coaching, but not yet assigned a coach role.</div>
        </center>
        <div style="clear: both;"></div>
        <div style="float: left;">
            <asp:Button ID="BtnExportTable2" runat="server" Text="Export To Excel" OnClick="BtnExportTable2_Click" />
        </div>
        <div style="clear: both;"></div>
        <asp:GridView HorizontalAlign="Center" Width="1200px" RowStyle-HorizontalAlign="Left" ID="grdVolunteerSignUp" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" ForeColor="Black" OnRowCommand="grdVolunteerSignUp_RowCommand">

            <Columns>
                <asp:TemplateField  ItemStyle-HorizontalAlign="Center" HeaderText="Coach">
                    <ItemTemplate>
                        <asp:Button ID="btnAddVolunteer" runat="server" Text="Add" CommandName="AddToVol" />
                         <asp:Button ID="BtnDelete" runat="server" Text="Delete" CommandName="DeleteVol" />

                        <div style="display: none;">

                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                            <asp:Label ID="lblEventID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                            <asp:Label ID="lblProductGroupID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                            <asp:Label ID="lblProductGroupCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
                            <asp:Label ID="lblProductID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                            <asp:Label ID="lblProductCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                            <asp:Label ID="lblEventCode" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>
                            <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Year") %>'></asp:Label>
                            <asp:Label ID="lblFirstName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FirstName")%>'></asp:Label>
                            <asp:Label ID="lblLastName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LastName") %>'></asp:Label>
                            <asp:Label ID="lblVolSignupId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "VolSignupId") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="MemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:TemplateField HeaderText="Volunteer Name">

                    <ItemTemplate>

                        <asp:Label runat="server" ID="lnkVol" attr-MemberId='<%#DataBinder.Eval(Container.DataItem, "MemberID")%>' CssClass="lnkVolunteer" Text='<%# Bind("Name")%>' Style="cursor: pointer; color: blue;"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>


                <asp:BoundField DataField="FirstName" Visible="false" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" Visible="false" HeaderText="Last Name"></asp:BoundField>
                <asp:BoundField DataField="EventID" HeaderText="EventID"></asp:BoundField>
                <asp:BoundField DataField="TeamName" HeaderText="Team"></asp:BoundField>
                <asp:BoundField DataField="ProductGroupCode" HeaderText="Prod Group Code"></asp:BoundField>
                <asp:BoundField DataField="ProductCode" HeaderText="Prod Code"></asp:BoundField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" id="dvNotSignedupVolunteer" runat="server">
        <center>
            <div style="font-weight: bold;">Table 3: Coach Role was assigned, but Calendar SignUp was not yet done.</div>
        </center>
        <div style="clear: both;"></div>
        <div style="float: left;">
            <asp:Button ID="BtnExportTable3" runat="server" Text="Export To Excel" OnClick="BtnExportTable3_Click" />
        </div>
        <div style="clear: both;"></div>
        <asp:GridView HorizontalAlign="Center" Width="1200px" RowStyle-HorizontalAlign="Left" ID="GrdNotSignedupVolunteer" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" ForeColor="Black">

            <Columns>
                <%--  <asp:TemplateField HeaderStyle-Width="55px" ItemStyle-HorizontalAlign="Center" HeaderText="Coach">
                    <ItemTemplate>
                        <asp:Button ID="btnAddVolunteer" runat="server" Text="Add" CommandName="AddVol" />

                        <div style="display: none;">

                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'></asp:Label>
                          
                            <asp:Label ID="lblFirstName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem, "FirstName")%>'></asp:Label>
                            <asp:Label ID="lblLastName" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"LastName") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>--%>

                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <asp:Label ID="HlblParentID" runat="server" Visible="false"></asp:Label>
    <input type="hidden" id="hdnVolunteerID" runat="server" value="0" />
     <input type="hidden" id="hdnVolSignupId" runat="server" value="0" />

</asp:Content>

