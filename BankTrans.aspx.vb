
Imports System
Imports System.Text
Imports System.IO
Imports System.Data
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Collections.Specialized
Imports System.Data.OleDb

Namespace VRegistration
    Partial Class BankTransactions
        Inherits System.Web.UI.Page

        Private Sub Page_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles MyBase.Load
            'Session("LoggedIn") = "true"
            'Session("LoginID") = "4240"
            'Session("entryToken") = "volunteer"
            'Session("RoleID") = 1
            'Session("loginChapterID") = 1

            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("maintest.aspx")
            End If
            'Session("LoginID") = 4240
            'Put user code to initialize the page here
            If Page.IsPostBack = False Then
                If Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    If (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "84") Or (Session("RoleId").ToString() = "85") Or (Session("RoleId").ToString() = "38") Then
                        'for Volunteer more than roleid 1,84,85   
                        Response.Write("<script language='javascript'>window.open('bankTransstatus.aspx','_blank','left=150,top=0,width=600,height=400,toolbar=0,location=0,scrollbars=1');</script> ")
                    Else
                        Response.Redirect("VolunteerFunctions.aspx")
                    End If
                    FillTransCategory()
                Else
                    Response.Redirect("Login.aspx")
                End If
            End If
        End Sub
        Sub FillTransCategory()

            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select distinct(TransCat) from BankTrans where TransCat is not null and BankId=" & ddlEditUpdateDB.SelectedValue & " order by TransCat")

            DdlTransCat.DataSource = ds.Tables(0)
            DdlTransCat.DataBind()
            DdlTransCat.Items.Insert(0, "Trans Category")
        End Sub

        Sub Button1_Click(ByVal sender As Object, ByVal e As EventArgs)
            Try

                Label1.Text = ""
                closepanel()
                If RbtnUploadFile.Checked = True Then
                    uploadfile()
                ElseIf RbtnVerifyTrans.Checked = True Then
                    verifyTrans()
                ElseIf RbtnEditUpdate.Checked = True Then
                    If ddlEditUpdate.SelectedValue > 0 And ddlEditUpdateColumn.SelectedItem.Text <> "Search Column" Then
                        Select Case ddlEditUpdate.SelectedValue
                            Case 1
                                Dim wherecntn As String
                                If ddlEditUpdateSel.SelectedItem.Text = "Null" Then
                                    wherecntn = " is Null "
                                ElseIf ddlEditUpdateColumn.SelectedItem.Text = "Bk_TransCat" Then
                                    wherecntn = " is not Null "
                                Else
                                    wherecntn = " = 'Y' "
                                End If
                                Session("StrSql") = "Select * from ChaseTemp where (" & ddlEditUpdateColumn.SelectedItem.Text & " " & wherecntn & ") order by Tdate Desc"
                                loadChaseGrid(True)
                            Case 2
                                Dim wherecntn As String
                                If ddlEditUpdateSel.SelectedItem.Text = "Null" Then
                                    wherecntn = " is Null "
                                ElseIf ddlEditUpdateColumn.SelectedItem.Text = "Bk_TransCat" Then
                                    wherecntn = " is not Null "
                                Else
                                    wherecntn = " = 'Y' "
                                End If
                                Session("StrSql") = "Select * from HBT1Temp where (" & ddlEditUpdateColumn.SelectedItem.Text & " " & wherecntn & ") order by Tdate Desc"
                                loadHBT1Grid(True)
                            Case 3
                                Dim wherecntn As String
                                If ddlEditUpdateSel.SelectedItem.Text = "Null" Then
                                    wherecntn = " is Null "
                                ElseIf ddlEditUpdateColumn.SelectedItem.Text = "Bk_TransCat" Then
                                    wherecntn = " is not Null "
                                Else
                                    wherecntn = " = 'Y' "
                                End If
                                Session("StrSql") = "Select * from HBT2Temp where (" & ddlEditUpdateColumn.SelectedItem.Text & " " & wherecntn & ") order by Tdate Desc"
                                loadHBT2Grid(True)
                        End Select
                        'ddlEditUpdate.SelectedIndex = ddlEditUpdate.Items.IndexOf(ddlEditUpdate.Items.FindByValue(0))
                    Else
                        Label1.Text = "Plese Select a Column to search Edit/Update"
                    End If
                ElseIf RbtnPostDB.Checked = True Then
                    postdata()
                ElseIf RbtnEditUpdateDB.Checked = True Then
                    Select Case ddlEditUpdateDB.SelectedValue
                        Case 1
                            Session("StrSql") = "Select TOP 2000 * from BankTrans where TransCat= '" & DdlTransCat.SelectedItem.Text & "' and BankId = 1 order by Transdate Desc"
                            LoadBankTrans(True)
                        Case 2
                            Session("StrSql") = "Select TOP 2000 * from BankTrans where TransCat= '" & DdlTransCat.SelectedItem.Text & "' and BankId = 2 order by Transdate Desc"
                            LoadBankTrans(True)
                        Case 3
                            Session("StrSql") = "Select TOP 2000 * from BankTrans where TransCat= '" & DdlTransCat.SelectedItem.Text & "' and BankId = 3 order by Transdate Desc"
                            LoadBankTrans(True)
                        Case Else
                            Label1.Text = "Please Select Transaction Account"
                    End Select
                Else
                    Label1.Text = "Please Choose the option"
                End If
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub uploadfile()
            'Moving data from CSV file to Temp Tables
            If FileUpLoad1.HasFile And ddlCSVUpload.SelectedValue > 0 Then
                Dim fileExt, filename As String
                Dim filecount As Integer = 0
                fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName)
                filename = Left(FileUpLoad1.FileName, 5)
                If (fileExt.ToLower = ".csv") Then
                    Try
                        Dim strfilename, FileType, FileNameSpec As String
                        Select Case ddlCSVUpload.SelectedValue
                            Case 1
                                FileType = "CHASE"
                                FileNameSpec = "JPMC_"
                            Case 2
                                FileType = "HBT1"
                                FileNameSpec = "HBT_S"
                            Case 3
                                FileType = "HBT2"
                                FileNameSpec = "HBT_O"
                        End Select
                        If filename.ToUpper = Left(FileNameSpec, 5).ToUpper Then
                            'strfilename = FileType & "file" & Now.Date.ToString("ddMMyyyy")
                            strfilename = FileUpLoad1.FileName
                            'FileCount is the count of file name in database
                            'filecount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from fileupload where fileType='" & FileType & "' AND DateDiff(d,createDate,GetDate())=0")
                            'If filecount <> 0 Then
                            '    strfilename = strfilename & filecount.ToString()
                            'End If
                            'strfilename = strfilename & ".csv"
                            FileUpLoad1.PostedFile.SaveAs(Server.MapPath("UploadFiles/" & strfilename))
                            Label1.Text = "Received " & strfilename & " Content Type " & FileUpLoad1.PostedFile.ContentType & " Length " & FileUpLoad1.PostedFile.ContentLength
                            Dim flag As Integer = 0
                            'Upload Files 
                            Dim dataCount As Integer = 0
                            Select Case ddlCSVUpload.SelectedValue
                                Case 1
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChaseTemp")
                                    If dataCount = 0 Then
                                        ImportChaseCSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                                Case 2
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT1Temp")
                                    If dataCount = 0 Then
                                        ImportHBT1CSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                                Case 3
                                    dataCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT2Temp")
                                    If dataCount = 0 Then
                                        ImportHBT2CSV_Database(Server.MapPath("UploadFiles/" & strfilename), strfilename)
                                    Else
                                        Label1.Text = "Sorry! Data exists in Temp file. It needs to be posted first"
                                    End If
                            End Select
                            ddlCSVUpload.SelectedIndex = ddlCSVUpload.Items.IndexOf(ddlCSVUpload.Items.FindByValue(0))
                        Else
                            Label1.Text = "File type selected Mismatch with Filename."
                        End If
                    Catch ex As Exception
                        Label1.Text = "ERROR: " & ex.Message.ToString()
                    End Try
                Else
                    Label1.Text = "Not a Csv file"
                End If

            Else
                Label1.Text = "No uploaded file / File Type Not Selected"
            End If
        End Sub

        Private Sub ImportChaseCSV_Database(ByVal TransFileWithPath As String, ByVal FileName As String)

            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            Try

                'open imported file as dataset
                Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
                Dim dsFile As DataSet = OpenFileConnection(TransFile, 0)

                Dim RowsImported As Integer = 0
                For Each TranRow As DataRow In dsFile.Tables(0).Rows
                    If TranRow(0).ToString.Trim <> "" Then
                        Dim TransType As String = TranRow(0).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim Tdate As String = TranRow(1).ToString.Replace("""", "").Replace(",", "").Replace("120000[0:GMT]", "").Trim
                        Dim Description As String = TranRow(2).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim Amount As String = TranRow(3).ToString.Replace("""", "").Replace(",", "").Trim
                        Dim Check_No As String = TranRow(4).ToString().Replace("""", "").Replace(",", "").Trim
                        'Dim MemberId As String = TranRow(4).ToString() '.Replace("""", "").Replace(",", "").Trim
                        'Dim DonorType As String = TranRow(5).ToString.Replace("""", "").Replace(",", "").Trim

                        'create insert statement
                        Dim SqlInsert As String = "INSERT INTO ChaseTemp(TransType, Tdate, Description,Amount, Bk_BankID,CreatedDate,CreatedBy,Bk_CheckNumber) VALUES(" ', MemberId, DonorType   
                        SqlInsert = SqlInsert & "'" & TransType & "','" & Tdate & "','" & Description & "'," & Amount & ",1,GetDate()," & Session("LoginID") & "," & IIf(Convert.ToInt32(Len(Check_No)) = 0, "NULL", Check_No) & ")" '& "," '& IIf(MemberId = "", "NULL", MemberId) & "," & IIf(DonorType = "", "NULL", "'" & DonorType & "'")
                        'Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    End If
                Next
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & FileName & "','CHASE',GetDate()," & Session("LoginID") & ")")
                'Commented for testing
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "delete from Chasetemp Where datediff(d,(Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 1 order by Transdate desc),Tdate)<=0")
                '//***********************************************************************************/

                'display number of rows imported
                'label1.Text = label1.Text & "<br> Rows Imported Successfully: " & RowsImported
                Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From ChaseTemp")
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BegRecdate, EndRecDate, TempRecRej, Postedby,CreatedDate) select '" & FileName & "'," & RowsImported & ", convert(varchar(10),min(tdate),101),convert(varchar(10),max(tdate),101)," & (RowsImported - count) & "," & Session("LoginID") & ",Getdate() from chaseTemp")
                Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & count
            Catch ex As Exception
                'Response.Write(ex.ToString())
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
            End Try
        End Sub

        Private Sub ImportHBT1CSV_Database(ByVal TransFileWithPath As String, ByVal FileName As String)
            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            'open imported file as dataset
            Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
            Dim dsFile As DataSet = OpenFileConnection(TransFile, 0)

            Dim RowsImported As Integer = 0
            For Each TranRow As DataRow In dsFile.Tables(0).Rows
                If TranRow(0).ToString.Trim <> "" And TranRow(0).ToString.Trim.Contains("Date") = False Then
                    Dim Tdate As String = TranRow(0).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim CheckNum As String = TranRow(1).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim Description As String = TranRow(2).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim WithdrawalAmount As String = TranRow(3).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim DepositAmount As String = TranRow(4).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim AdditionalInfo As String = TranRow(5).ToString.Replace("""", "").Replace(",", "").Trim
                    'Dim MemberId As String = TranRow(6).ToString() '.Replace("""", "").Replace(",", "").Trim
                    'Dim DonorType As String = TranRow(7).ToString.Replace("""", "").Replace(",", "").Trim

                    If Len(WithdrawalAmount) = 0 Then
                        WithdrawalAmount = 0
                    End If

                    If Len(CheckNum) = 0 Then
                        CheckNum = 0
                    End If

                    If Len(DepositAmount) = 0 Then
                        DepositAmount = 0
                    End If
                    If Len(AdditionalInfo) = 0 Then
                        AdditionalInfo = String.Empty
                    End If
                    If Len(Description) = 0 Then
                        Description = String.Empty
                    End If
                    Dim SqlInsert As String
                    'create insert statement
                    Try
                        SqlInsert = "INSERT INTO HBT1Temp(Tdate, CheckNum, Description,WithdrawalAmount, DepositAmount,AdditionalInfo, Bk_BankID,CreatedDate,CreatedBy) VALUES(" ',MemberId,DonorType
                        SqlInsert = SqlInsert & "'" & Tdate & "'," & CheckNum & ",'" & Description & "'," & WithdrawalAmount & "," & DepositAmount & ",'" & AdditionalInfo & "',2,GetDate()," & Session("LoginID") & ")" '& "," & IIf(MemberId = "", "NULL", MemberId) & "," & IIf(DonorType = "", "NULL", "'" & DonorType & "'")

                        'Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Label1.ForeColor = Drawing.Color.Red
                        Label1.Text = Label1.Text & "<br>" & SqlInsert
                        Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                    End Try
                End If
            Next
            'display number of rows imported
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & filename & "','HBT1',GetDate()," & Session("LoginID") & ")")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "delete from HBT1temp Where datediff(d,(Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 2 order by Transdate desc),Tdate)<=0")

            Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From HBT1Temp")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BegRecdate, EndRecDate, TempRecRej, Postedby,CreatedDate) select '" & FileName & "'," & RowsImported & ", convert(varchar(10),min(tdate),101),convert(varchar(10),max(tdate),101)," & (RowsImported - count) & "," & Session("LoginID") & ",Getdate() from HBT1Temp")
            Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & count
        End Sub

        Private Sub ImportHBT2CSV_Database(ByVal TransFileWithPath As String, ByVal FileName As String)
            Dim dbConn As SqlConnection
            Try
                'open NSF database connection
                dbConn = New SqlConnection(Application("ConnectionString"))
                dbConn.Open()
            Catch ex As Exception
                Label1.ForeColor = Drawing.Color.Red
                Label1.Text = "ERROR in Opening the database: " & ex.Message.ToString()
            End Try
            'open imported file as dataset
            Dim TransFile As FileInfo = New FileInfo(TransFileWithPath)
            Dim dsFile As DataSet = OpenFileConnection(TransFile, 0)

            Dim RowsImported As Integer = 0
            For Each TranRow As DataRow In dsFile.Tables(0).Rows
                If TranRow(0).ToString.Trim <> "" And TranRow(0).ToString.Trim.Contains("Date") = False Then
                    Dim Tdate As String = TranRow(0).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim CheckNum As String = TranRow(1).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim Description As String = TranRow(2).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim WithdrawalAmount As String = TranRow(3).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim DepositAmount As String = TranRow(4).ToString.Replace("""", "").Replace(",", "").Trim
                    Dim AdditionalInfo As String = TranRow(5).ToString.Replace("""", "").Replace(",", "").Trim
                    'Dim MemberId As String = TranRow(6).ToString() '.Replace("""", "").Replace(",", "").Trim
                    'Dim DonorType As String = TranRow(7).ToString.Replace("""", "").Replace(",", "").Trim

                    If Len(WithdrawalAmount) = 0 Then
                        WithdrawalAmount = 0
                    End If

                    If Len(CheckNum) = 0 Then
                        CheckNum = 0
                    End If

                    If Len(DepositAmount) = 0 Then
                        DepositAmount = 0
                    End If
                    If Len(AdditionalInfo) = 0 Then
                        AdditionalInfo = String.Empty
                    End If

                    If Len(Description) = 0 Then
                        Description = String.Empty
                    End If

                    'create insert statement
                    Try
                        Dim SqlInsert As String = "INSERT INTO HBT2Temp(Tdate, CheckNum, Description,WithdrawalAmount, DepositAmount,AdditionalInfo, Bk_BankID,CreatedDate,CreatedBy) VALUES(" ', MemberId, DonorType
                        SqlInsert = SqlInsert & "'" & Tdate & "'," & CheckNum & ",'" & Description & "'," & WithdrawalAmount & "," & DepositAmount & ",'" & AdditionalInfo & "',3,GetDate()," & Session("LoginID") & ")" '& "," & IIf(MemberId = "", "NULL", MemberId) & "," & IIf(DonorType = "", "NULL", "'" & DonorType & "'")
                        'Label1.Text = Label1.Text & "<br>" & SqlInsert
                        'create SQL command 
                        Dim cmd As SqlCommand = New SqlCommand(SqlInsert, dbConn)
                        RowsImported += cmd.ExecuteNonQuery()
                    Catch ex As Exception
                        Label1.ForeColor = Drawing.Color.Red
                        Label1.Text = Label1.Text & "<br> ERROR in ImportCSV_Database: " & ex.Message.ToString()
                    End Try
                End If
            Next
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "delete from HBT2temp Where datediff(d,(Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 3 order by Transdate desc),Tdate)<=0")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into fileUpload(FileName, FileType, CreateDate,CreatedBy ) values('" & FileName & "','HBT2',GetDate()," & Session("LoginID") & ")")

            Dim count As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From HBT2Temp")
            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BegRecdate, EndRecDate, TempRecRej, Postedby,CreatedDate) select '" & FileName & "'," & RowsImported & ", convert(varchar(10),min(tdate),101),convert(varchar(10),max(tdate),101)," & (RowsImported - count) & "," & Session("LoginID") & ",Getdate() from HBT2Temp")
            Label1.Text = Label1.Text & "<br> Rows Imported Successfully: " & count
        End Sub

        Private Function OpenFileConnection(ByVal TransFileTable As FileInfo, ByVal type As Integer) As DataSet
            Dim dsFile As New DataSet
            ' MsgBox(ddlCSVUpload.SelectedIndex)
            Try
                Dim ConnCSV As String
                If type = 1 Then
                    ConnCSV = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=No;FMT=Delimited';Data Source=" & TransFileTable.DirectoryName.ToString()
                Else
                    ConnCSV = "Provider=Microsoft.Jet.OLEDB.4.0;Extended Properties='text;HDR=Yes;FMT=Delimited';Data Source=" & TransFileTable.DirectoryName.ToString()
                End If
                Dim sqlSelect As String
                Dim objOleDBConn As OleDbConnection
                Dim objOleDBDa As OleDbDataAdapter
                objOleDBConn = New OleDbConnection(ConnCSV)
                objOleDBConn.Open()
                'sqlSelect = "select [Store ID],[Order #],[Date],[User ID],Type,[PayerAuth],[Invoice #],[PO #],[Trans ID],[Card/Route Number],[Exp Date],Approval,Amount from [" + TransFileTable.Name.ToString() + "]"
                sqlSelect = "select * from [" + TransFileTable.Name.ToString() + "]"
                objOleDBDa = New OleDbDataAdapter(sqlSelect, objOleDBConn)
                objOleDBDa.Fill(dsFile)
                objOleDBConn.Close()
            Catch ex As Exception
                Label1.Text = Label1.Text & "<br> ERROR in OpenFileConnection: " & ex.Message.ToString()
            End Try
            Return dsFile
        End Function
        Sub verifyTrans()
            Try
                Dim count As Integer
                Dim tempcount As Integer = 0
                Dim CTransLastDate, CTempFirstDate As Date
                Select Case ddlVerifyTrans.SelectedValue
                    Case 1
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from Chasetemp")
                        If tempcount > 0 Then
                            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from BankTrans where BankID = 1")
                            CTempFirstDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 CONVERT(VARCHAR(8), TDate, 1)  as CTempFirstDate from ChaseTemp order by TDate ")
                            If count > 0 Then
                                CTransLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 1 order by Transdate desc")
                                If CTempFirstDate <= CTransLastDate Then
                                    'We should not post transactions for dates
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update Chasetemp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_TransType='Y',Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_Amount=Amount from ChaseTemp a, ChaseTransType b where b.Token = a.TransType and (a.Parse_TransType is null or a.Parse_TransType<>'Y')")
                                ElseIf DateDiff(DateInterval.Day, CTransLastDate, CTempFirstDate) > 1 Then
                                    Session("CTransLastDate") = CTransLastDate
                                    TrMsg.Visible = True
                                    Literal1.Text = "The Last date in BankTrans table is " & CTransLastDate & ".  The First date in the Temp table is " & CTempFirstDate & ". So there is a gap in dates.<br> Was a CSV file missing or being skipped?"
                                    'Literal1.Text = "Was a CSV file missing or being skipped?"
                                Else
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update Chasetemp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_TransType='Y',Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_Amount=Amount from ChaseTemp a, ChaseTransType b where b.Token = a.TransType and (a.Parse_TransType is null or a.Parse_TransType<>'Y')")
                                    Session("CTransLastDate") = CTransLastDate
                                    VerifyDesc()
                                End If
                            Else
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update Chasetemp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_TransType='Y',Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_Amount=Amount from ChaseTemp a, ChaseTransType b where b.Token = a.TransType and (a.Parse_TransType is null or a.Parse_TransType<>'Y')")
                                VerifyDesc()
                            End If
                        Else
                            Label1.Text = "No record found in ChaseTemp"
                        End If
                    Case 2
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT1temp")
                        If tempcount > 0 Then
                            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from BankTrans where BankID = 2")
                            CTempFirstDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 CONVERT(VARCHAR(8), TDate, 1)  as CTempFirstDate from HBT1temp order by TDate ")
                            If count > 0 Then
                                CTransLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 2 order by Transdate desc")
                                If CTempFirstDate <= CTransLastDate Then
                                    'We should not post transactions for dates
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT1Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                ElseIf DateDiff(DateInterval.Day, CTransLastDate, CTempFirstDate) > 0 Then
                                    Session("CTransLastDate") = CTransLastDate
                                    TrMsg.Visible = True
                                    Literal1.Text = "The Last date in BankTrans table is " & CTransLastDate & ".  The First date in the Temp table is " & CTempFirstDate & ". So there is a gap in dates.<br> Was a CSV file missing or being skipped?"
                                    'Literal1.Text = "Was a CSV file missing or being skipped?"
                                Else
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT1Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                    Session("CTransLastDate") = CTransLastDate
                                    VerifyDesc()
                                End If
                            Else
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT1Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                VerifyDesc()
                            End If
                        Else
                            Label1.Text = "No record found in HBT1Temp"
                        End If
                    Case 3

                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from HBT2temp")
                        If tempcount > 0 Then
                            count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from BankTrans where BankID = 3")
                            CTempFirstDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 CONVERT(VARCHAR(8), TDate, 1)  as CTempFirstDate from HBT2temp order by TDate ")
                            If count > 0 Then
                                CTransLastDate = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select top 1 TransDate as CTransLastDate from BankTrans where BankID = 3 order by Transdate desc")
                                If CTempFirstDate <= CTransLastDate Then
                                    'We should not post transactions for dates
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT2Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                ElseIf DateDiff(DateInterval.Day, CTransLastDate, CTempFirstDate) > 0 Then
                                    Session("CTransLastDate") = CTransLastDate
                                    TrMsg.Visible = True
                                    Literal1.Text = "The Last date in BankTrans table is " & CTransLastDate & ".  The First date in the Temp table is " & CTempFirstDate & ". So there is a gap in dates.<br> Was a CSV file missing or being skipped?"
                                    'Literal1.Text = "Was a CSV file missing or being skipped?"
                                Else
                                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT2Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                    Session("CTransLastDate") = CTransLastDate
                                    VerifyDesc()
                                End If
                            Else
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT2Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                                VerifyDesc()
                            End If
                        Else
                            Label1.Text = "No record found in HBT2Temp"
                        End If
                    Case Else
                        Label1.ForeColor = Drawing.Color.Red
                        Label1.Text = "Please select an item from DropDown"
                End Select
            Catch ex As Exception
                'Response.Write(ex.ToString())
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub VerifyDesc()
            Try
                Dim unParsedTranscnt As Integer = 0
                If RbtnVerifyTrans.Checked = True Then
                    Select Case ddlVerifyTrans.SelectedValue
                        Case 1
                            unParsedTranscnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where (Parse_TransType is null OR Parse_TransType<>'Y')")
                            If unParsedTranscnt > 0 Then
                                Session("StrSql") = "Select * from ChaseTemp where (Parse_TransType is null OR Parse_TransType<>'Y') order by Tdate Desc"
                                loadChaseGrid(True)
                            Else
                                'Everything Parsed Successfully Move for Next Parsing
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE ChaseTemp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber = CASE b.ID WHEN  1 THEN RTRIM(LTRIM(substring(a.Description,CharIndex(b.Token,a.Description)+Len(b.Token),Len(a.Description))))  ELSE NUll END,Bk_DepositNumber = CASE b.ID WHEN  2 THEN RTRIM(LTRIM(substring(a.Description,CharIndex(b.Token,a.Description)+Len(b.Token),Len(a.Description)))) ELSE NUll END, Bk_TransCat = b.Bk_TransCat,Bk_VendCust = b.Bk_VendorCust,Bk_Reason = b.Bk_Reason,Parse_Desc='Y',MemberID=b.MemberID,DonorType=b.DonorType FROM ChaseTemp AS a INNER JOIN ChaseDesc AS b ON CHARINDEX(b.Token, a.Description) >0 AND a.Parse_TransType='Y' AND (a.Parse_Desc is NUll or a.Parse_Desc<>'Y')")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE ChaseTemp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",MemberID=b.MemberID,DonorType=b.DonorType   FROM ChaseTemp AS a INNER JOIN ChaseDesc AS b ON CHARINDEX(b.Token, a.Description) >0 and a.Bk_TransCat='Donation'")

                                Dim unParsedDesccnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where (Parse_Desc is null OR Parse_Desc<>'Y')")
                                If unParsedDesccnt > 0 Then
                                    Session("StrSql") = "Select * from ChaseTemp where (Parse_Desc is null OR Parse_Desc<>'Y') order by Tdate Desc"
                                    loadChaseGrid(True)
                                Else
                                    Dim unParsedTrnsCatCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where Bk_TransCat is Null")
                                    If unParsedTrnsCatCnt > 0 Then
                                        Session("StrSql") = "Select * from ChaseTemp where Bk_TransCat is Null order by Tdate Desc"
                                        loadChaseGrid(True)
                                    Else
                                        Label1.Text = "All Data Were Parsed Successfully"
                                    End If
                                End If
                            End If
                        Case 2
                            'Find HBT1Temp
                            unParsedTranscnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1Temp where (Parse_Desc is null or Parse_Desc<>'Y')")
                            If unParsedTranscnt > 0 Then
                                Session("StrSql") = "Select * from HBT1Temp where (Parse_Desc is null OR Parse_Desc<>'Y' ) order by Tdate Desc"
                                loadHBT1Grid(True)
                            Else
                                'Everything Parsed Successfully Move for Next Parsing
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT1temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", Bk_TransCat = b.Bk_TransCat,Bk_VendCust = b.Bk_VendorCust,Bk_Reason = b.Bk_Reason,Parse_Addinfo='Y',MemberID=b.MemberID,DonorType=b.DonorType FROM HBT1Temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0  AND (a.Parse_Addinfo is NUll OR a.Parse_Addinfo<> 'Y')  and  a.AdditionalInfo is not NUll and a.Bk_TransCat is Null ")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT1temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_VendCust = b.Bk_VendorCust,Bk_Reason = b.Bk_Reason,Parse_Addinfo='Y',MemberID=b.MemberID,DonorType=b.DonorType FROM HBT1Temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0  AND (a.Parse_Addinfo is NUll OR a.Parse_Addinfo<> 'Y')  and  a.AdditionalInfo is not NUll and a.Bk_TransCat is Not Null ")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT1temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",MemberID=b.MemberID,DonorType=b.DonorType FROM HBT1Temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0 and a.Bk_TransCat='Donation'")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT1temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_Addinfo='Y' Where (Parse_Addinfo is NUll OR Parse_Addinfo<> 'Y')  and (AdditionalInfo is NUll or AdditionalInfo='') and Bk_TransCat is Not Null")
                                Dim unParsedDesccnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*) FROM HBT1Temp where AdditionalInfo is not NUll and (Parse_Addinfo is NUll OR Parse_Addinfo<> 'Y') and AdditionalInfo<>''")
                                If unParsedDesccnt > 0 Then
                                    Session("StrSql") = "SELECT * FROM HBT1Temp where AdditionalInfo is not NUll and AdditionalInfo<>'' and (Parse_Addinfo is NUll OR Parse_Addinfo<> 'Y') order by Tdate Desc"
                                    loadHBT1Grid(True)
                                Else
                                    Dim unParsedTrnsCatCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1Temp where Bk_TransCat is Null")
                                    If unParsedTrnsCatCnt > 0 Then
                                        Session("StrSql") = "Select * from HBT1Temp where Bk_TransCat is Null order by Tdate Desc"
                                        loadHBT1Grid(True)
                                    Else
                                        Label1.Text = "All Data Were Parsed Successfully"
                                    End If
                                End If
                            End If
                        Case 3
                            'Find HBT2Temp
                            unParsedTranscnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT2Temp where (Parse_Desc is null or Parse_Desc<>'Y')")
                            If unParsedTranscnt > 0 Then
                                Session("StrSql") = "Select * from HBT2Temp where (Parse_Desc is null or Parse_Desc<>'Y') order by Tdate Desc"
                                loadHBT2Grid(True)
                            Else
                                'Everything Parsed Successfully Move for Next Parsing
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT2temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", Bk_TransCat = b.Bk_TransCat,Bk_VendCust = b.Bk_VendorCust,Bk_Reason = b.Bk_Reason,Parse_Addinfo='Y',MemberID=b.MemberID,DonorType=b.DonorType FROM HBT2Temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0  AND (a.Parse_Addinfo is NUll OR a.Parse_AddInfo <>'Y')  and  a.AdditionalInfo is not NUll and a.Bk_TransCat is Null")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT2temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ", Bk_VendCust = b.Bk_VendorCust,Bk_Reason = b.Bk_Reason,Parse_Addinfo='Y',MemberID=b.MemberID,DonorType=b.DonorType FROM HBT2Temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0  AND (a.Parse_Addinfo is NUll OR a.Parse_AddInfo <>'Y')  and  a.AdditionalInfo is not NUll and a.Bk_TransCat is Not Null")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT2temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",MemberID=b.MemberID,DonorType=b.DonorType FROM HBT2temp AS a INNER JOIN HBTAddinfo AS b ON CHARINDEX(b.Token, a.AdditionalInfo) >0 and a.Bk_TransCat='Donation'")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "UPDATE HBT2temp SET   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_Addinfo='Y' Where (Parse_Addinfo is NUll OR Parse_Addinfo<> 'Y')  and (AdditionalInfo is NUll or AdditionalInfo='') and Bk_TransCat is Not Null")
                                Dim unParsedDesccnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT count(*) FROM HBT2Temp where AdditionalInfo is not NUll and (Parse_Addinfo is NUll OR Parse_AddInfo <>'Y') and AdditionalInfo<>''")
                                If unParsedDesccnt > 0 Then
                                    Session("StrSql") = "SELECT * FROM HBT2Temp where AdditionalInfo is not NUll and AdditionalInfo<>'' and (Parse_Addinfo is NUll OR Parse_AddInfo <>'Y') order by Tdate Desc"
                                    loadHBT2Grid(True)
                                Else
                                    Dim unParsedTrnsCatCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT2Temp where Bk_TransCat is Null")
                                    If unParsedTrnsCatCnt > 0 Then
                                        Session("StrSql") = "Select * from HBT2Temp where Bk_TransCat is Null order by Tdate Desc"
                                        loadHBT1Grid(True)
                                    Else
                                        Label1.Text = "All Data Were Parsed Successfully"
                                    End If
                                End If
                            End If
                    End Select
                End If
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Public Sub loadChaseGrid(ByVal BlnReload As Boolean)
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (BlnReload = True) Then
                Session("ChaseDataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("ChaseDataSet") = ds
                    ChaseResults.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("ChaseDataSet") Is Nothing) Then
                    ds = CType(Session("ChaseDataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                PnlChaseTemp.Visible = False
                Label1.Text = "There is No Record to display"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                PnlChaseTemp.Visible = False
                Label1.Text = "There is No Record to display"
            Else
                lblChaseTemp.Text = "Records from ChaseTemp Table"
                Dim i As Integer = ds.Tables(0).Rows.Count
                ChaseResults.DataSource = ds.Tables(0)
                ChaseResults.DataBind()
                PnlChaseTemp.Visible = True
            End If
        End Sub
        Public Sub loadHBT1Grid(ByVal BlnReload As Boolean)
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (BlnReload = True) Then
                Session("HBT1DataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("HBT1DataSet") = ds
                    HBT1Results.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("HBT1DataSet") Is Nothing) Then
                    ds = CType(Session("HBT1DataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                pnlHBT1.Visible = False
                Label1.Text = "There is No Record to display"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                pnlHBT1.Visible = False
                Label1.Text = "There is No Record to display"
            Else
                lblHBT1Temp.Text = "Records from HBT1Temp Table"
                Dim i As Integer = ds.Tables(0).Rows.Count
                HBT1Results.DataSource = ds.Tables(0)
                HBT1Results.DataBind()
                pnlHBT1.Visible = True
            End If
        End Sub
        Public Sub loadHBT2Grid(ByVal BlnReload As Boolean)
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (BlnReload = True) Then
                Session("HBT2DataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("HBT2DataSet") = ds
                    HBT2Results.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("HBT2DataSet") Is Nothing) Then
                    ds = CType(Session("HBT2DataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                pnlHBT2.Visible = False
                Label1.Text = "There is No Record to display"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                pnlHBT2.Visible = False
                Label1.Text = "There is No Record to display"
            Else
                lblHBT2Temp.Text = "Records from HBT2Temp Table"
                Dim i As Integer = ds.Tables(0).Rows.Count
                HBT2Results.DataSource = ds.Tables(0)
                HBT2Results.DataBind()
                pnlHBT2.Visible = True
            End If
        End Sub

        Protected Sub ChaseResults_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.UpdateCommand
            Try
                ' TransType,Description, Parse_TransType, Parse_Desc, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber,Bk_VendCust, Bk_Reason
                Dim ChasetempId As Integer = CInt(e.Item.Cells(2).Text)
                Dim TransType As String = CType(e.Item.FindControl("txtTransType_1"), TextBox).Text
                Dim Description As String = CType(e.Item.FindControl("txtDescription_1"), TextBox).Text
                Dim Parse_TransType As String = CType(e.Item.FindControl("txtParse_TransType_1"), TextBox).Text
                Dim Parse_Desc As String = CType(e.Item.FindControl("txtParse_Desc_1"), TextBox).Text
                Dim Bk_TransType As String = CType(e.Item.FindControl("DdlBk_TransType_1"), DropDownList).Text
                Dim Bk_TransCat As String = IIf(CType(e.Item.FindControl("DdlBk_TransCat_1"), DropDownList).Text.Contains("Select TransCat"), "", CType(e.Item.FindControl("DdlBk_TransCat_1"), DropDownList).Text)
                Dim Bk_CheckNumber As String = CType(e.Item.FindControl("txtBk_CheckNumber_1"), TextBox).Text
                Dim Bk_DepositNumber As String = CType(e.Item.FindControl("txtBk_DepositNumber_1"), TextBox).Text
                Dim Bk_VendCust As String = CType(e.Item.FindControl("txtBk_VendCust_1"), TextBox).Text
                Dim Bk_Reason As String = CType(e.Item.FindControl("txtBk_Reason_1"), TextBox).Text
                Dim DonorType As String = CType(e.Item.FindControl("ddlBk_DonorType_1"), DropDownList).SelectedValue

                Dim Sql As String
                Sql = "update Chasetemp set"
                'TransType,Description, Parse_TransType, Parse_Desc, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber,Bk_VendCust, Bk_Reason
                If TransType = "" Then
                    Sql = Sql & " TransType=Null,"
                Else
                    Sql = Sql & " TransType='" & TransType & "',"
                End If
                If Description = "" Then
                    Sql = Sql & " Description=Null,"
                Else
                    Sql = Sql & " Description='" & Description & "',"
                End If
                If Parse_TransType = "" Then
                    Sql = Sql & " Parse_TransType=Null,"
                Else
                    Sql = Sql & " Parse_TransType='" & Parse_TransType & "',"
                End If
                If Parse_Desc = "" Then
                    Sql = Sql & " Parse_Desc=Null,"
                Else
                    Sql = Sql & " Parse_Desc='" & Parse_Desc & "',"
                End If
                If Bk_TransType = "" Then
                    Sql = Sql & " Bk_TransType=Null,"
                Else
                    Sql = Sql & " Bk_TransType='" & Bk_TransType & "',"
                End If

                If Bk_TransCat = "" Then
                    Sql = Sql & " Bk_TransCat=Null,"
                Else
                    Sql = Sql & " Bk_TransCat='" & Bk_TransCat & "',"
                End If

                If Bk_CheckNumber = "" Then
                    Sql = Sql & " Bk_CheckNumber=Null,"
                Else
                    Sql = Sql & " Bk_CheckNumber=" & Bk_CheckNumber & ","
                End If

                If Bk_DepositNumber = "" Then
                    Sql = Sql & " Bk_DepositNumber=Null,"
                Else
                    Sql = Sql & " Bk_DepositNumber=" & Bk_DepositNumber & ","
                End If
                If Bk_VendCust = "" Then
                    Sql = Sql & " Bk_VendCust=Null,"
                Else
                    Sql = Sql & " Bk_VendCust='" & Bk_VendCust & "',"
                End If

                If Bk_Reason = "" Then
                    Sql = Sql & " Bk_Reason=Null,"
                Else
                    Sql = Sql & " Bk_Reason='" & Bk_Reason & "',"
                End If

                If DonorType = "" Then
                    Sql = Sql & " DonorType=Null"
                Else
                    Sql = Sql & " DonorType='" & DonorType & "'"
                End If

                Sql = Sql & " where ChasetempId = " & ChasetempId

                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
                ChaseResults.EditItemIndex = -1
                loadChaseGrid(True)
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub ChaseResults_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.CancelCommand
            Dim ds As DataSet = CType(Session("ChaseDataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim currentRowIndex As Integer
                currentRowIndex = e.Item.ItemIndex
                'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                ChaseResults.EditItemIndex = -1
                loadChaseGrid(False)
            Else
                Return
            End If
        End Sub

        Protected Sub ChaseResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles ChaseResults.PageIndexChanged
            ChaseResults.CurrentPageIndex = e.NewPageIndex
            ChaseResults.EditItemIndex = -1
            loadChaseGrid(False)
        End Sub

        Protected Sub ChaseResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim ChaseTempId As Integer

                ChaseTempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM CHASETEMP WHERE CHASETEMPID =" + CStr(ChaseTempId))
                Catch se As SqlException
                    Return
                End Try
                loadChaseGrid(True)
                ChaseResults.EditItemIndex = -1
            End If
        End Sub
        
        Protected Sub ChaseResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles ChaseResults.EditCommand
            ChaseResults.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("ChaseDataSet"), DataSet)
            Dim page As Integer = ChaseResults.CurrentPageIndex
            Dim pageSize As Integer = ChaseResults.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            Cache("editRow") = dr
            loadChaseGrid(False)
        End Sub

        Protected Sub HBT1Results_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT1Results.UpdateCommand
            'Parse_Desc, Parse_AddInfo,  Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_VendCust, Bk_Reason
            Try
                Dim HBT1tempId As Integer = CInt(e.Item.Cells(2).Text)
                Dim Parse_Desc As String = CType(e.Item.FindControl("txtParse_Desc_2"), TextBox).Text
                Dim Parse_AddInfo As String = CType(e.Item.FindControl("txtParse_AddInfo_2"), TextBox).Text
                Dim Bk_TransType As String = CType(e.Item.FindControl("DdlBk_TransType_2"), DropDownList).Text
                Dim Bk_TransCat As String = IIf(CType(e.Item.FindControl("DdlBk_TransCat_2"), DropDownList).Text.Contains("Select TransCat"), "", CType(e.Item.FindControl("DdlBk_TransCat_2"), DropDownList).Text)
                Dim Bk_CheckNumber As String = CType(e.Item.FindControl("txtBk_CheckNumber_2"), TextBox).Text
                Dim Bk_DepositNumber As String = CType(e.Item.FindControl("txtBk_DepositNumber_2"), TextBox).Text
                Dim Bk_VendCust As String = CType(e.Item.FindControl("txtBk_VendCust_2"), TextBox).Text
                Dim Bk_Reason As String = CType(e.Item.FindControl("txtBk_Reason_2"), TextBox).Text
                Dim DonorType As String = CType(e.Item.FindControl("ddlBk_DonorType_2"), DropDownList).SelectedValue

                'Parse_Desc, Parse_AddInfo,  Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_VendCust, Bk_Reason
                Dim Sql As String
                Sql = "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ","
                If Parse_Desc = "" Then
                    Sql = Sql & " Parse_Desc=Null,"
                Else
                    Sql = Sql & " Parse_Desc='" & Parse_Desc & "',"
                End If
                If Parse_AddInfo = "" Then
                    Sql = Sql & " Parse_AddInfo=Null,"
                Else
                    Sql = Sql & " Parse_AddInfo='" & Parse_AddInfo & "',"
                End If

                If Bk_TransType = "" Then
                    Sql = Sql & " Bk_TransType=Null,"
                Else
                    Sql = Sql & " Bk_TransType='" & Bk_TransType & "',"
                End If

                If Bk_TransCat = "" Then
                    Sql = Sql & " Bk_TransCat=Null,"
                Else
                    Sql = Sql & " Bk_TransCat='" & Bk_TransCat & "',"
                End If

                If Bk_CheckNumber = "" Then
                    Sql = Sql & " Bk_CheckNumber=Null,"
                Else
                    Sql = Sql & " Bk_CheckNumber=" & Bk_CheckNumber & ","
                End If

                If Bk_DepositNumber = "" Then
                    Sql = Sql & " Bk_DepositNumber=Null,"
                Else
                    Sql = Sql & " Bk_DepositNumber=" & Bk_DepositNumber & ","
                End If
                If Bk_VendCust = "" Then
                    Sql = Sql & " Bk_VendCust=Null,"
                Else
                    Sql = Sql & " Bk_VendCust='" & Bk_VendCust & "',"
                End If

                If Bk_Reason = "" Then
                    Sql = Sql & " Bk_Reason=Null,"
                Else
                    Sql = Sql & " Bk_Reason='" & Bk_Reason & "',"
                End If

                If DonorType = "" Then
                    Sql = Sql & " DonorType=Null"
                Else
                    Sql = Sql & " DonorType='" & DonorType & "'"
                End If

                Sql = Sql & " where HBT1tempId = " & HBT1tempId
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
                HBT1Results.EditItemIndex = -1
                loadHBT1Grid(True)
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub HBT1Results_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT1Results.CancelCommand
            Dim ds As DataSet = CType(Session("HBT1DataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim page As Integer = HBT1Results.CurrentPageIndex
                Dim pageSize As Integer = HBT1Results.PageSize
                Dim currentRowIndex As Integer
                If (page = 0) Then
                    currentRowIndex = e.Item.ItemIndex
                Else
                    currentRowIndex = (page * pageSize) + e.Item.ItemIndex
                End If

                Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                HBT1Results.EditItemIndex = -1
                loadHBT1Grid(False)
            Else
                Return
            End If
        End Sub

        Protected Sub HBT1Results_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT1Results.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim HBT1TempId As Integer

                HBT1TempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM HBT1TEMP WHERE HBT1TEMPID =" + CStr(HBT1TempId))
                Catch se As SqlException
                    Return
                End Try
                loadHBT1Grid(True)
                HBT1Results.EditItemIndex = -1
            End If
        End Sub

        Protected Sub HBT1Results_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles HBT1Results.PageIndexChanged
            HBT1Results.CurrentPageIndex = e.NewPageIndex
            HBT1Results.EditItemIndex = -1
            loadHBT1Grid(False)
        End Sub

        Protected Sub HBT1Results_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT1Results.EditCommand
            HBT1Results.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("HBT1DataSet"), DataSet)
            Dim page As Integer = HBT1Results.CurrentPageIndex
            Dim pageSize As Integer = HBT1Results.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            Cache("editRow") = dr
            loadHBT1Grid(False)
        End Sub

        Protected Sub HBT2Results_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT2Results.UpdateCommand
            'Parse_Desc, Parse_AddInfo, Bk_BankID, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_VendCust, Bk_Reason

            Dim HBT2tempId As Integer = CInt(e.Item.Cells(2).Text)
            Dim Parse_Desc As String = CType(e.Item.FindControl("txtParse_Desc_3"), TextBox).Text
            Dim Parse_AddInfo As String = CType(e.Item.FindControl("txtParse_AddInfo_3"), TextBox).Text
            Dim Bk_TransType As String = CType(e.Item.FindControl("DdlBk_TransType_3"), DropDownList).Text
            Dim Bk_TransCat As String = IIf(CType(e.Item.FindControl("DdlBk_TransCat_3"), DropDownList).Text.Contains("Select TransCat"), "", CType(e.Item.FindControl("DdlBk_TransCat_3"), DropDownList).Text)
            Dim Bk_CheckNumber As String = CType(e.Item.FindControl("txtBk_CheckNumber_3"), TextBox).Text
            Dim Bk_DepositNumber As String = CType(e.Item.FindControl("txtBk_DepositNumber_3"), TextBox).Text
            Dim Bk_VendCust As String = CType(e.Item.FindControl("txtBk_VendCust_3"), TextBox).Text
            Dim Bk_Reason As String = CType(e.Item.FindControl("txtBk_Reason_3"), TextBox).Text
            Dim DonorType As String = CType(e.Item.FindControl("ddlBk_DonorType_3"), DropDownList).SelectedValue

            Dim Sql As String
            Sql = "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ","
            If Parse_Desc = "" Then
                Sql = Sql & " Parse_Desc=Null,"
            Else
                Sql = Sql & " Parse_Desc='" & Parse_Desc & "',"
            End If
            If Parse_AddInfo = "" Then
                Sql = Sql & " Parse_AddInfo=Null,"
            Else
                Sql = Sql & " Parse_AddInfo='" & Parse_AddInfo & "',"
            End If

            If Bk_TransType = "" Then
                Sql = Sql & " Bk_TransType=Null,"
            Else
                Sql = Sql & " Bk_TransType='" & Bk_TransType & "',"
            End If

            If Bk_TransCat = "" Then
                Sql = Sql & " Bk_TransCat=Null,"
            Else
                Sql = Sql & " Bk_TransCat='" & Bk_TransCat & "',"
            End If

            If Bk_CheckNumber = "" Then
                Sql = Sql & " Bk_CheckNumber=Null,"
            Else
                Sql = Sql & " Bk_CheckNumber=" & Bk_CheckNumber & ","
            End If

            If Bk_DepositNumber = "" Then
                Sql = Sql & " Bk_DepositNumber=Null,"
            Else
                Sql = Sql & " Bk_DepositNumber=" & Bk_DepositNumber & ","
            End If
            If Bk_VendCust = "" Then
                Sql = Sql & " Bk_VendCust=Null,"
            Else
                Sql = Sql & " Bk_VendCust='" & Bk_VendCust & "',"
            End If

            If Bk_Reason = "" Then
                Sql = Sql & " Bk_Reason=Null,"
            Else
                Sql = Sql & " Bk_Reason='" & Bk_Reason & "',"
            End If
            Sql = Sql & " where HBT2tempId = " & HBT2tempId

            If DonorType = "" Then
                Sql = Sql & " DonorType=Null"
            Else
                Sql = Sql & " DonorType='" & DonorType & "'"
            End If

            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
            Catch ex As Exception
                'MsgBox(ex.ToString)
            End Try
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set Parse_Desc='" & Parse_Desc & "',Parse_AddInfo='" & Parse_AddInfo & "',Bk_TransType='" & Bk_TransType & "',Bk_TransCat='" & Bk_TransCat & "',Bk_CheckNumber=" & Bk_CheckNumber & ",Bk_DepositNumber=" & Bk_DepositNumber & ",Bk_VendCust='" & Bk_VendCust & "',Bk_Reason='" & Bk_Reason & "' where HBT2tempId=" & HBT2tempId & "")
            HBT2Results.EditItemIndex = -1
            loadHBT2Grid(True)
        End Sub

        Protected Sub HBT2Results_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT2Results.CancelCommand
            Dim ds As DataSet = CType(Session("HBT2DataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim page As Integer = HBT2Results.CurrentPageIndex
                Dim pageSize As Integer = HBT2Results.PageSize
                Dim currentRowIndex As Integer
                If (page = 0) Then
                    currentRowIndex = e.Item.ItemIndex
                Else
                    currentRowIndex = (page * pageSize) + e.Item.ItemIndex
                End If

                'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                HBT2Results.EditItemIndex = -1
                loadHBT2Grid(False)
            Else
                Return
            End If
        End Sub

        Protected Sub HBT2Results_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT2Results.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim HBT2TempId As Integer

                HBT2TempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM HBT2TEMP WHERE HBT2TEMPID =" + CStr(HBT2TempId))
                Catch se As SqlException
                    Return
                End Try
                loadHBT2Grid(True)
                HBT2Results.EditItemIndex = -1
            End If
        End Sub

        Protected Sub HBT2Results_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles HBT2Results.PageIndexChanged
            HBT2Results.CurrentPageIndex = e.NewPageIndex
            HBT2Results.EditItemIndex = -1
            loadHBT2Grid(False)
        End Sub

        Protected Sub HBT2Results_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles HBT2Results.EditCommand
            HBT2Results.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("HBT2DataSet"), DataSet)
            Dim page As Integer = HBT2Results.CurrentPageIndex
            Dim pageSize As Integer = HBT2Results.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            'Session("editRow") = dr
            Cache("editRow") = dr
            loadHBT2Grid(False)
        End Sub

        Public Sub LoadBankTrans(ByVal BlnReload As Boolean)
            'Label1.Text = Session("StrSql")
            Dim ds As New DataSet
            Dim conn As New SqlConnection(Application("ConnectionString"))
            Dim cmd As New SqlCommand
            If (BlnReload = True) Then
                Session("BankTransDataSet") = Nothing
                Try
                    ds = SqlHelper.ExecuteDataset(conn, CommandType.Text, Session("StrSql"))
                    Session("BankTransDataSet") = ds
                    BankTransResults.CurrentPageIndex = 0
                Catch se As SqlException
                    'MsgBox(se.ToString())
                    'displayErrorMessage("There is an error while trying to execute the query.")
                    Return
                End Try
            Else
                If (Not Session("BankTransDataSet") Is Nothing) Then
                    ds = CType(Session("BankTransDataSet"), DataSet)
                End If
            End If

            If (ds Is Nothing Or ds.Tables.Count < 1) Then
                pnlBankTrans.Visible = False
                Label1.Text = "There is No Record to display"
            ElseIf (ds.Tables(0).Rows.Count < 1) Then    'two checks? a hack to handle empty tables with tables.count>0
                pnlBankTrans.Visible = False
                Label1.Text = "There is No Record to display"
            Else
                lblHBT2Temp.Text = "Records from HBT2Temp Table"
                Dim i As Integer = ds.Tables(0).Rows.Count
                BankTransResults.DataSource = ds.Tables(0)
                BankTransResults.DataBind()
                pnlBankTrans.Visible = True
            End If
        End Sub

        Protected Sub BankTransResults_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.UpdateCommand
            'TransType,TransCat,CheckNumber,DepositNumber,VendCust,Reason,DepSlipNumber,GLAccount
            Dim BankTransID As Integer = CInt(e.Item.Cells(2).Text)
            Dim TransType As String = CType(e.Item.FindControl("ddlTransType"), DropDownList).SelectedItem.Text
            '  Dim TransCat As String = CType(e.Item.FindControl("txtTransCat"), TextBox).Text
            Dim TransCat As String = CType(e.Item.FindControl("ddlTransCat"), DropDownList).SelectedItem.Text
            Dim CheckNumber As String = CType(e.Item.FindControl("txtCheckNumber"), TextBox).Text
            Dim DepositNumber As String = CType(e.Item.FindControl("txtDepositNumber"), TextBox).Text
            Dim VendCust As String = CType(e.Item.FindControl("txtVendCust"), TextBox).Text
            Dim Reason As String = CType(e.Item.FindControl("ddlReason"), DropDownList).SelectedItem.Value
            'Dim Reason As String = CType(e.Item.FindControl("txtReason"), TextBox).Text
            Dim DepSlipNumber As String = CType(e.Item.FindControl("txtDepSlipNumber"), TextBox).Text
            Dim GLAccount As String = CType(e.Item.FindControl("txtGLAccount"), TextBox).Text
            Dim RestType As String = CType(e.Item.FindControl("ddlRestType"), DropDownList).SelectedItem.Value
            Dim MemberID As String = CType(e.Item.FindControl("txtMemberID"), TextBox).Text
            Dim DonorType As String = CType(e.Item.FindControl("ddlDonorType_Bk"), DropDownList).SelectedItem.Value

            Dim Sql As String
            Sql = "update BankTrans set ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ","
            If TransType = "" Then
                Sql = Sql & " TransType=Null,"
            Else
                Sql = Sql & " TransType='" & TransType & "',"
            End If

            If TransCat = "" Then
                Sql = Sql & " TransCat=Null,"
            Else
                Sql = Sql & " TransCat='" & TransCat & "',"
            End If

            If CheckNumber = "" Then
                Sql = Sql & " CheckNumber=Null,"
            Else
                Sql = Sql & " CheckNumber=" & CheckNumber & ","
            End If

            If DepositNumber = "" Then
                Sql = Sql & " DepositNumber=Null,"
            Else
                Sql = Sql & " DepositNumber=" & DepositNumber & ","
            End If
            If VendCust = "" Then
                Sql = Sql & " VendCust=Null,"
            Else
                Sql = Sql & " VendCust='" & VendCust & "',"
            End If

            If Reason = "" Then
                Sql = Sql & " Reason=Null,"
            Else
                Sql = Sql & " Reason='" & Reason & "',"
            End If

            If DepSlipNumber = "" Then
                Sql = Sql & " DepSlipNumber=Null,"
            Else
                Sql = Sql & " DepSlipNumber=" & DepSlipNumber & ","
            End If
            If GLAccount = "" Then
                Sql = Sql & " GLAccount=Null,"
            Else
                Sql = Sql & " GLAccount=" & GLAccount & ","
            End If
            If RestType = "" Then
                Sql = Sql & " RestType=Null, "
            Else
                Sql = Sql & " RestType='" & RestType & "', "
            End If
            If MemberID = "" Then
                Sql = Sql & " MemberID=Null ,"
            Else
                Sql = Sql & " MemberID=" & MemberID & ", "
            End If
            If DonorType = "" Then
                Sql = Sql & " DonorType=Null"
            Else
                Sql = Sql & " DonorType='" & DonorType & "'"
            End If
            Sql = Sql & " where BankTransId = " & BankTransID

            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, Sql)
            Catch ex As Exception
                Label1.Text = ex.ToString()
            End Try
            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set Parse_Desc='" & Parse_Desc & "',Parse_AddInfo='" & Parse_AddInfo & "',Bk_TransType='" & Bk_TransType & "',Bk_TransCat='" & Bk_TransCat & "',Bk_CheckNumber=" & Bk_CheckNumber & ",Bk_DepositNumber=" & Bk_DepositNumber & ",Bk_VendCust='" & Bk_VendCust & "',Bk_Reason='" & Bk_Reason & "' where HBT2tempId=" & HBT2tempId & "")
            BankTransResults.EditItemIndex = -1
            LoadBankTrans(True)
        End Sub

        Protected Sub BankTransResults_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.CancelCommand
            Dim ds As DataSet = CType(Session("BankTransDataSet"), DataSet)
            If (Not ds Is Nothing) Then
                Dim dsCopy As DataSet = ds.Copy
                Dim page As Integer = BankTransResults.CurrentPageIndex
                Dim pageSize As Integer = BankTransResults.PageSize
                Dim currentRowIndex As Integer
                If (page = 0) Then
                    currentRowIndex = e.Item.ItemIndex
                Else
                    currentRowIndex = (page * pageSize) + e.Item.ItemIndex
                End If

                'Session("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                Cache("editRow") = dsCopy.Tables(0).Rows(currentRowIndex)  'save the original back in session
                BankTransResults.EditItemIndex = -1
                LoadBankTrans(False)
            Else
                Return
            End If
        End Sub

        Protected Sub BankTransResults_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.ItemCommand
            Dim cmdName As String = e.CommandName
            Dim s As String = e.CommandSource.ToString
            If (cmdName = "Delete") Then
                Dim HBT2TempId As Integer

                HBT2TempId = CInt(e.Item.Cells(2).Text)
                Try
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "DELETE FROM HBT2TEMP WHERE HBT2TEMPID =" + CStr(HBT2TempId))
                Catch se As SqlException
                    Return
                End Try
                LoadBankTrans(True)
                BankTransResults.EditItemIndex = -1
            End If
        End Sub

        Protected Sub BankTransResults_PageIndexChanged(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridPageChangedEventArgs) Handles BankTransResults.PageIndexChanged
            BankTransResults.CurrentPageIndex = e.NewPageIndex
            BankTransResults.EditItemIndex = -1
            LoadBankTrans(False)
        End Sub

        Protected Sub BankTransResults_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles BankTransResults.EditCommand
            BankTransResults.EditItemIndex = CInt(e.Item.ItemIndex)
            'Dim strEventCode As String
            Dim vDS As DataSet = CType(Session("BankTransDataSet"), DataSet)
            Dim page As Integer = BankTransResults.CurrentPageIndex
            Dim pageSize As Integer = BankTransResults.PageSize
            Dim currentRowIndex As Integer
            If (page = 0) Then
                currentRowIndex = e.Item.ItemIndex
            Else
                currentRowIndex = (page * pageSize) + e.Item.ItemIndex
            End If
            Dim vDSCopy As DataSet = vDS.Copy
            Dim dr As DataRow = vDSCopy.Tables(0).Rows(currentRowIndex) 'get the current row being edited
            Session("Reason") = CType(e.Item.Cells(11).FindControl("lblReason"), Label).Text
            Session("RestType") = CType(e.Item.Cells(16).FindControl("lblRestType"), Label).Text
            Session("DonorType") = CType(e.Item.Cells(18).FindControl("lblDonorType"), Label).Text
            'Session("editRow") = dr
            Cache("editRow") = dr
            LoadBankTrans(False)
        End Sub

        Sub postdata()
            Try
                Dim PTransTypeCnt, PDescCnt, PAddinfoCnt, count, tempcount, TransCatCount, Bk_TransCatCnt As Integer

                Dim filename As String
                'Moving data from temp table to Original Table
                Select Case ddlPostDB.SelectedValue
                    Case 1
                        ' Coding for ChaseTemmp
                        PTransTypeCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where Parse_TransType is null or Parse_TransType <>'Y'")
                        PDescCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from ChaseTemp where Parse_Desc is null or Parse_Desc <>'Y'")
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from Chasetemp")
                        TransCatCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from Chasetemp Where (MemberID is null Or DonorType is null) and Bk_Transcat='Donation'")
                        Bk_TransCatCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from Chasetemp Where Bk_Transcat is Null")

                        If tempcount > 0 Then
                            If PTransTypeCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (TransType)"
                            ElseIf PDescCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (Description)"
                            ElseIf TransCatCount > 0 Then
                                Label1.Text = "MemberID/Donortype missing in ChaseTemp table for Transcat 'Donation'.<br />Please Update table ChaseDesc."
                            ElseIf Bk_TransCatCnt > 0 Then
                                Label1.Text = "TransCat missing in ChaseTemp table.<br />Please Update table ChaseDesc."
                            Else
                                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans")
                                ExecuteQuery("insert into BankTrans(BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description, CreatedDate, CreatedBy,MemberID,DonorType) Select Bk_BankID, Bk_TransDate, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_Amount, Bk_VendCust, Bk_Reason,Description,GetDate()," & Session("LoginID") & ",MemberID,DonorType FROM ChaseTemp order by Tdate; Truncate Table  ChaseTemp")
                                filename = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 filename from fileupload where filetype like 'CHASE' order by fileID desc")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BeginRecID, BegRecdate, EndRecID, EndRecDate, TempRecRej, Postedby,CreatedDate) Select '" & filename & "'," & tempcount & "," & count + 1 & ", MIN(TransDate),MAX(BankTransID),MAX(TransDate),0," & Session("LoginID") & ",GetDate() from BankTrans where BANKID = 1 and banktransID>=" & count + 1 & "")
                                Label1.Text = "Records Inserted Successfully : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans") - count
                            End If
                        Else
                            Label1.Text = "No Record found to post."
                        End If
                    Case 2
                        ' Coding for HBT1Temmp
                        PDescCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1Temp where Parse_Desc is null or Parse_Desc <>'Y'")
                        PAddinfoCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1Temp where (Parse_AddInfo is null or Parse_AddInfo <> 'Y') and len(AdditionalInfo)>0 ")
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT1temp")
                        TransCatCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT1temp Where (MemberID is null Or DonorType is null) and Bk_Transcat='Donation'")
                        Bk_TransCatCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT1temp Where Bk_Transcat is Null")

                        If tempcount > 0 Then
                            If PDescCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (Description)"
                            ElseIf PAddinfoCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (Additional Info)"
                            ElseIf TransCatCount > 0 Then
                                Label1.Text = "MemberID/Donortype missing in HBT1temp table for Transcat 'Donation'.<br />Please Update table HBTAddInfo."
                            ElseIf Bk_TransCatCnt > 0 Then
                                Label1.Text = "TransCat missing in HBT1temp table.<br />Please Update table HBTAddInfo."
                            Else
                                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans")
                                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into BankTrans(BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description,AddInfo, CreatedDate, CreatedBy) Select Bk_BankID, Bk_TransDate, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_Amount, Bk_VendCust, Bk_Reason, Description, AdditionalInfo,GetDate()," & Session("LoginID") & " FROM HBT1Temp order by Tdate ; Truncate Table  HBT1Temp")
                                ExecuteQuery("insert into BankTrans(BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description,AddInfo, CreatedDate, CreatedBy,MemberID,DonorType) Select Bk_BankID, Bk_TransDate, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_Amount, Bk_VendCust, Bk_Reason, Description, AdditionalInfo,GetDate()," & Session("LoginID") & ",MemberID,DonorType FROM HBT1Temp order by Tdate ; Truncate Table  HBT1Temp")
                                filename = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 filename from fileupload where filetype like 'HBT1' order by fileID desc")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BeginRecID, BegRecdate, EndRecID, EndRecDate, TempRecRej, Postedby,CreatedDate) Select '" & filename & "'," & tempcount & "," & count + 1 & ", MIN(TransDate),MAX(BankTransID),MAX(TransDate),0," & Session("LoginID") & ",GetDate() from BankTrans where BANKID = 2 and banktransID>=" & count + 1 & "")
                                Label1.Text = "Records Inserted Successfully : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans") - count
                            End If
                        Else
                            Label1.Text = "No Record found to post."
                        End If
                    Case 3
                        ' Coding for HBT1Temmp
                        PDescCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT2Temp where Parse_Desc is null or Parse_Desc <>'Y'")
                        PAddinfoCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT2Temp where (Parse_AddInfo is null or Parse_AddInfo <> 'Y') and len(AdditionalInfo)>0 ")
                        tempcount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT2temp")
                        TransCatCount = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT2temp Where (MemberID is null Or DonorType is null) and Bk_Transcat='Donation'")
                        Bk_TransCatCnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Count(*) from HBT2temp Where Bk_Transcat is Null")
                        If tempcount > 0 Then
                            If PDescCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (Description)"
                            ElseIf PAddinfoCnt > 0 Then
                                Label1.Text = "One or more transactions were not parsed (Additional Info)"
                            ElseIf TransCatCount > 0 Then
                                Label1.Text = "MemberID/Donortype missing in HBT2temp table for Transcat 'Donation'.<br />Please Update table HBTAddInfo."
                            ElseIf Bk_TransCatCnt > 0 Then
                                Label1.Text = "TransCat missing in HBT2temp table.<br />Please Update table HBTAddInfo."
                            Else
                                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans")
                                'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "insert into BankTrans(BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description,AddInfo, CreatedDate, CreatedBy) Select Bk_BankID, Bk_TransDate, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_Amount, Bk_VendCust, Bk_Reason, Description, AdditionalInfo,GetDate()," & Session("LoginID") & " FROM HBT2Temp order by Tdate ; Truncate Table  HBT2Temp")
                                ExecuteQuery("insert into BankTrans(BankID, TransDate, TransType, TransCat, CheckNumber, DepositNumber, Amount, VendCust, Reason, Description,AddInfo, CreatedDate, CreatedBy,MemberID,DonorType) Select Bk_BankID, Bk_TransDate, Bk_TransType, Bk_TransCat, Bk_CheckNumber, Bk_DepositNumber, Bk_Amount, Bk_VendCust, Bk_Reason, Description, AdditionalInfo,GetDate()," & Session("LoginID") & ",MemberID,DonorType FROM HBT2Temp order by Tdate ; Truncate Table  HBT2Temp")
                                filename = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select Top 1 filename from fileupload where filetype like 'HBT2' order by fileID desc")
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Insert into BankTransLog(InputFileName, TempRecRead, BeginRecID, BegRecdate, EndRecID, EndRecDate, TempRecRej, Postedby,CreatedDate) Select '" & filename & "'," & tempcount & "," & count + 1 & ", MIN(TransDate),MAX(BankTransID),MAX(TransDate),0," & Session("LoginID") & ",GetDate() from BankTrans where BANKID = 3 and banktransID>=" & count + 1 & "")
                                Label1.Text = "Records Inserted Successfully : " & SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select MAX(BankTransID) from BankTrans") - count
                            End If
                        Else
                            Label1.Text = "No Record found to post."
                        End If
                    Case Else
                        Label1.Text = "Please select The type to be posted"
                End Select
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnYes_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                If RbtnVerifyTrans.Checked = True Then
                    Select Case ddlVerifyTrans.SelectedValue
                        Case 1
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update Chasetemp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Parse_TransType='Y',Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_Amount=Amount from ChaseTemp a, ChaseTransType b where b.Token = a.TransType and a.Parse_TransType is null ")
                            VerifyDesc()
                        Case 2
                            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType from HBT1Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and a.Parse_Desc is null")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT1temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT1Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                            VerifyDesc()
                        Case 3
                            'SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType from HBT2Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and a.Parse_Desc is null ")
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "update HBT2temp set   ModifiedDate=GetDate(), ModifiedBy=" & Session("LoginID") & ",Bk_CheckNumber= CASE CheckNum WHEN 0 THEN NULL ELSE CheckNum END,Bk_Amount = CASE  WithdrawalAmount WHEN 0 THEN DepositAmount ELSE WithdrawalAmount END ,Bk_TransDate=Tdate,Bk_TransType=b.Bk_TransType,Bk_TransCat=b.Bk_TransCat,Parse_Desc='Y' from HBT2Temp a, HBTDesc b where CHARINDEX(b.Token, a.Description) >0 and (a.Parse_Desc is null OR a.Parse_Desc <>'Y')")
                            VerifyDesc()
                    End Select
                End If
                TrMsg.Visible = False
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Sub closepanel()
            PnlChaseTemp.Visible = False
            pnlHBT1.Visible = False
            pnlHBT2.Visible = False
            'add/Modify Panel
            pnlHBTAddinfo.Visible = False
            pnlChaseDesc.Visible = False
            pnlHBTTransType.Visible = False
            pnlChaseTransType.Visible = False
            'pnlBankTrans.Visible = False
            If RbtnEditUpdate.Checked <> True Then
                ddlEditUpdate.SelectedIndex = ddlEditUpdate.Items.IndexOf(ddlEditUpdate.Items.FindByValue(0))
                ddlEditUpdateColumn.Enabled = False
                ddlEditUpdateSel.Enabled = False
            End If
            If RbtnEditUpdateDB.Checked <> True Then
                ddlEditUpdateDB.SelectedIndex = ddlEditUpdateDB.Items.IndexOf(ddlEditUpdateDB.Items.FindByValue(0))
                DdlTransCat.SelectedIndex = DdlTransCat.Items.IndexOf(DdlTransCat.Items.FindByValue(0))
                DdlTransCat.Enabled = False
            End If
            If RbtnVerifyTrans.Checked <> True Then
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
            End If
            If RbtnUploadFile.Checked <> True Then
                ddlCSVUpload.SelectedIndex = ddlCSVUpload.Items.IndexOf(ddlCSVUpload.Items.FindByValue(0))
            End If
            If RbtnPostDB.Checked <> True Then
                ddlPostDB.SelectedIndex = ddlPostDB.Items.IndexOf(ddlPostDB.Items.FindByValue(0))
            End If
            
        End Sub
        Protected Sub BtnNo_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                TrMsg.Visible = False
                Select Case ddlVerifyTrans.SelectedValue
                    Case 1
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table Chasetemp")
                        VerifyDesc()
                    Case 2
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table HBT1temp")
                    Case 3
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Truncate Table HBT2temp")
                End Select
                ddlVerifyTrans.SelectedIndex = ddlVerifyTrans.Items.IndexOf(ddlVerifyTrans.Items.FindByValue(0))
                Label1.Text = "Upload the missing csv file first"
                'ddlPostData.SelectedIndex = ddlPostData.Items.IndexOf(ddlPostData.Items.FindByValue(0))
            Catch ex As Exception
                Label1.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Protected Sub ddlEditUpdate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            closepanel()
            If ddlEditUpdate.SelectedValue = 1 Then
                ddlEditUpdateColumn.Items.Clear()
                ddlEditUpdateColumn.Items.Add("Search Column")
                ddlEditUpdateColumn.Items.Add("Parse_TransType")
                ddlEditUpdateColumn.Items.Add("Parse_Desc")
                ddlEditUpdateColumn.Items.Add("Posted")
                ddlEditUpdateColumn.Items.Add("Bk_TransCat")
                ddlEditUpdateColumn.Enabled = True
                ddlEditUpdateSel.Enabled = True
            ElseIf ddlEditUpdate.SelectedValue >= 2 Then
                ddlEditUpdateColumn.Items.Clear()
                ddlEditUpdateColumn.Items.Add("Search Column")
                ddlEditUpdateColumn.Items.Add("Parse_Desc")
                ddlEditUpdateColumn.Items.Add("Parse_AddInfo")
                ddlEditUpdateColumn.Items.Add("Posted")
                ddlEditUpdateColumn.Items.Add("Bk_TransCat")
                ddlEditUpdateColumn.Enabled = True
                ddlEditUpdateSel.Enabled = True
            Else
                ddlEditUpdateColumn.Enabled = False
                ddlEditUpdateSel.Enabled = False
                Label1.Text = "Please Select valid Account"
            End If
        End Sub

        Private Sub GetChaseDesc()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM ChaseDesc")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridChaseDesc.DataSource = dt
            GridChaseDesc.DataBind()
            pnlChaseDesc.Visible = True
        End Sub
        Protected Sub GridChaseDesc_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridChaseDesc.RowCommand
            BtnAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridChaseDesc.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM ChaseDesc where Id=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtToken.Text = dsRecords.Tables(0).Rows(0)("Token").ToString()
            txtBk_TransCat.Text = dsRecords.Tables(0).Rows(0)("Bk_TransCat").ToString()
            txtBk_VendorCust.Text = dsRecords.Tables(0).Rows(0)("Bk_VendorCust").ToString()
            txtBk_Reason.Text = dsRecords.Tables(0).Rows(0)("Bk_Reason").ToString()
            txtMembID1.Text = dsRecords.Tables(0).Rows(0)("MemberID").ToString()
            ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByText(IIf(dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString() Is DBNull.Value, "SELECT", dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString())))
            ddlDonorType_1.SelectedIndex = ddlDonorType_1.Items.IndexOf(ddlDonorType_1.Items.FindByText(IIf(dsRecords.Tables(0).Rows(0)("DonorType").ToString() Is DBNull.Value, "SELECT", dsRecords.Tables(0).Rows(0)("DonorType").ToString())))
        End Sub
        Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
            Try
                Dim Strsql As String
                If BtnAdd.Text = "Add" Then
                    If txtToken.Text.Length > 2 And (txtBk_TransCat.Text.Length > 2 Or txtBk_VendorCust.Text.Length > 2) Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from ChaseDesc where Token  LIke '" & txtToken.Text & "%'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Dim token, Bk_TransCat, Bk_VendorCust, Bk_Reason, Bk_DonorType, MemberID, DonorType As String
                            If txtToken.Text.Length > 1 Then
                                token = "'" & txtToken.Text & "'"
                            Else
                                token = "null"
                            End If
                            If txtBk_TransCat.Text.Length > 1 Then
                                Bk_TransCat = "'" & txtBk_TransCat.Text & "'"
                            Else
                                Bk_TransCat = "Null"
                            End If
                            If txtBk_VendorCust.Text.Length > 1 Then
                                Bk_VendorCust = "'" & txtBk_VendorCust.Text & "'"
                            Else
                                Bk_VendorCust = "Null"
                            End If
                            If txtBk_Reason.Text.Length > 1 Then
                                Bk_Reason = "'" & txtBk_Reason.Text & "'"
                            Else
                                Bk_Reason = "Null"
                            End If
                            If ddlDonorType.SelectedIndex <> "0" Then
                                Bk_DonorType = "'" & ddlDonorType.SelectedItem.Text & "'"
                            Else
                                Bk_DonorType = "NULL"
                            End If
                            If txtBk_TransCat.Text = "Donation" Then
                                If txtMembID1.Text.Length = 0 Then
                                    lblChaseDesc.Text = "MemberID field is mandatory."
                                    Exit Sub
                                ElseIf ddlDonorType_1.SelectedIndex = "0" Then
                                    lblChaseDesc.Text = "DonorType field is mandatory."
                                    Exit Sub
                                ElseIf txtMembID1.Text.Length > 0 And ddlDonorType_1.SelectedIndex > 0 Then
                                    If ddlDonorType_1.SelectedValue = "IND" Then
                                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from INDSpouse where  AutoMemberID=" & txtMembID1.Text) <= 0 Then
                                            lblChaseDesc.Text = "No IndSpouse record present for the given MemberID "
                                            Exit Sub
                                        End If
                                    Else
                                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from OrganizationInfo where  AutoMemberID=" & txtMembID1.Text) <= 0 Then
                                            lblChaseDesc.Text = "No Organization record present for the given MemberID "
                                            Exit Sub
                                        End If
                                    End If
                                End If
                            End If
                            If txtMembID1.Text.Length > 0 Then
                                MemberID = txtMembID1.Text
                            Else
                                MemberID = "Null"
                            End If
                            If ddlDonorType_1.SelectedIndex <> "0" Then
                                DonorType = "'" & ddlDonorType_1.SelectedItem.Text & "'"
                            Else
                                DonorType = "NULL"
                            End If
                            Strsql = "Insert Into ChaseDesc(Token, Bk_TransCat, Bk_VendorCust, Bk_Reason,Bk_DonorType,MemberID,DonorType) Values ("
                            Strsql = Strsql & token & "," & Bk_TransCat & "," & Bk_VendorCust & "," & Bk_Reason & "," & Bk_DonorType & "," & MemberID & "," & DonorType & ")"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetChaseDesc()
                            lblChaseDesc.Text = "Record Added Successfully"
                            clear()
                        Else
                            lblChaseDesc.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblChaseDesc.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnAdd.Text = "Update" Then
                    If txtToken.Text.Length > 2 And (txtBk_TransCat.Text.Length > 2 Or txtBk_VendorCust.Text.Length > 2) Then
                        Dim Bk_TransCat, Bk_VendorCust, Bk_Reason, Bk_DonorType, MemberID, DonorType As String

                        If txtBk_TransCat.Text.Length > 1 Then
                            Bk_TransCat = "'" & txtBk_TransCat.Text & "'"
                        Else
                            Bk_TransCat = "Null"
                        End If
                        If txtBk_VendorCust.Text.Length > 1 Then
                            Bk_VendorCust = "'" & txtBk_VendorCust.Text & "'"
                        Else
                            Bk_VendorCust = "Null"
                        End If
                        If txtBk_Reason.Text.Length > 1 Then
                            Bk_Reason = "'" & txtBk_Reason.Text & "'"
                        Else
                            Bk_Reason = "Null"
                        End If
                        If ddlDonorType.SelectedIndex <> "0" Then
                            Bk_DonorType = "'" & ddlDonorType.SelectedItem.Text & "'"
                        Else
                            Bk_DonorType = "NULL"
                        End If
                        If txtMembID1.Text.Length > 0 Then
                            MemberID = txtMembID1.Text
                        Else
                            MemberID = "Null"
                        End If
                        If ddlDonorType_1.SelectedIndex <> "0" Then
                            DonorType = "'" & ddlDonorType_1.SelectedItem.Text & "'"
                        Else
                            DonorType = "NULL"
                        End If
                        If txtBk_TransCat.Text = "Donation" Then
                            If txtMembID1.Text.Length = 0 Then
                                lblChaseDesc.Text = "MemberID field is mandatory."
                                Exit Sub
                            ElseIf ddlDonorType_1.SelectedIndex = "0" Then
                                lblChaseDesc.Text = "DonorType field is mandatory."
                                Exit Sub
                            ElseIf txtMembID1.Text.Length > 0 And ddlDonorType_1.SelectedIndex > 0 Then
                                If ddlDonorType_1.SelectedValue = "IND" Then
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from INDSpouse where  AutoMemberID=" & txtMembID1.Text) <= 0 Then
                                        lblChaseDesc.Text = "No IndSpouse record present for the given MemberID "
                                        Exit Sub
                                    End If
                                Else
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from OrganizationInfo where  AutoMemberID=" & txtMembID1.Text) <= 0 Then
                                        lblChaseDesc.Text = "No Organization record present for the given MemberID "
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If
                        Strsql = "Update ChaseDesc Set Token='" & txtToken.Text & "',Bk_TransCat=" & Bk_TransCat & ",Bk_VendorCust=" & Bk_VendorCust & ",Bk_Reason=" & Bk_Reason & ",Bk_DonorType=" & Bk_DonorType & ",MemberID =" & MemberID & ", DonorType=" & DonorType & " where ID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetChaseDesc()
                        lblChaseDesc.Text = "Record Updated Successfully"
                        clear()
                        BtnAdd.Text = "Add"
                    Else
                        lblChaseDesc.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                lblChaseDesc.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
            clear()
        End Sub
        Sub clear()
            txtToken.Text = ""
            txtBk_TransCat.Text = ""
            txtBk_VendorCust.Text = ""
            txtBk_Reason.Text = ""
            txtMembID1.Text = ""
            ddlDonorType.SelectedIndex = 0
            ddlDonorType_1.SelectedIndex = 0
            Session("ID") = Nothing
            BtnAdd.Text = "Add"
        End Sub

        Protected Sub btnChase_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChase.Click
            GetChaseDesc()
            pnlHBTAddinfo.Visible = False
            pnlChaseTransType.Visible = False
            pnlHBTTransType.Visible = False
        End Sub

        Protected Sub btnClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnClose.Click
            clear()
            pnlChaseDesc.Visible = False
        End Sub

        Private Sub GetChaseTransType()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM ChaseTransType")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridChaseTransType.DataSource = dt
            GridChaseTransType.DataBind()
            pnlChaseTransType.Visible = True
        End Sub
        Protected Sub GridChaseTransType_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridChaseTransType.RowCommand
            BtnTransAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridChaseTransType.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetTransSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetTransSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM ChaseTransType where Id=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtTransToken.Text = dsRecords.Tables(0).Rows(0)("Token").ToString()
            txtTransType.Text = dsRecords.Tables(0).Rows(0)("Bk_Transtype").ToString()
            '  ddlDonorType.SelectedIndex = ddlDonorType.Items.IndexOf(ddlDonorType.Items.FindByText(IIf(dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString() Is DBNull.Value, "SELECT", dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString())))
        End Sub
        Protected Sub BtnTransAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTransAdd.Click
            Try
                Dim Strsql As String
                If BtnTransAdd.Text = "Add" Then
                    If txtTransToken.Text.Length > 2 And txtTransType.Text.Length > 2 Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from ChaseTransType where Token  LIke '" & txtTransToken.Text & "%'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Dim token, Bk_TransType As String
                            If txtTransToken.Text.Length > 1 Then
                                token = "'" & txtTransToken.Text & "'"
                            Else
                                token = "null"
                            End If
                            If txtTransType.Text.Length > 1 Then
                                Bk_TransType = "'" & txtTransType.Text & "'"
                            Else
                                Bk_TransType = "Null"
                            End If
                            Strsql = "Insert Into ChaseTransType(Token, Bk_TransType) Values ("
                            Strsql = Strsql & token & "," & Bk_TransType & ")"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetChaseTransType()
                            lblChaseTransType.Text = "Record Added Successfully"
                            Transclear()
                        Else
                            lblChaseTransType.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblChaseTransType.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnTransAdd.Text = "Update" Then
                    If txtTransToken.Text.Length > 2 And txtTransType.Text.Length > 2 Then
                        Strsql = "Update ChaseTransType Set Token='" & txtTransToken.Text & "',Bk_TransType='" & txtTransType.Text & "' where ID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetChaseTransType()
                        lblChaseTransType.Text = "Record Updated Successfully"
                        Transclear()
                        BtnTransAdd.Text = "Add"
                    Else
                        lblChaseTransType.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                lblChaseTransType.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnTransCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnTransCancel.Click
            Transclear()
        End Sub
        Sub Transclear()
            txtTransToken.Text = ""
            txtTransType.Text = ""
            Session("ID") = Nothing
            BtnTransAdd.Text = "Add"
        End Sub

        Protected Sub btnChaseTrans_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnChaseTrans.Click
            GetChaseTransType()
            pnlHBTAddinfo.Visible = False
            pnlChaseDesc.Visible = False
            pnlHBTTransType.Visible = False
        End Sub

        Protected Sub BtnTransClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnTransClose.Click
            Transclear()
            pnlChaseTransType.Visible = False
        End Sub
        Private Sub GetHBTAddinfo()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM HBTAddinfo")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridHBTAddinfo.DataSource = dt
            GridHBTAddinfo.DataBind()
            pnlHBTAddinfo.Visible = True
        End Sub
        Protected Sub GridHBTAddinfo_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridHBTAddinfo.RowCommand
            BtnHBTAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridHBTAddinfo.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetHBTSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetHBTSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM HBTAddinfo where Id=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtHBTToken.Text = dsRecords.Tables(0).Rows(0)("Token").ToString()
            txtHBTBk_TransCat.Text = dsRecords.Tables(0).Rows(0)("Bk_TransCat").ToString()
            txtHBTBk_VendorCust.Text = dsRecords.Tables(0).Rows(0)("Bk_VendorCust").ToString()
            txtHBTBk_Reason.Text = dsRecords.Tables(0).Rows(0)("Bk_Reason").ToString()
            ddlDonorType_HBT.SelectedIndex = ddlDonorType_HBT.Items.IndexOf(ddlDonorType_HBT.Items.FindByText(IIf(dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString() Is DBNull.Value, "SELECT", dsRecords.Tables(0).Rows(0)("Bk_DonorType").ToString())))
            txtMembID2.Text = dsRecords.Tables(0).Rows(0)("MemberID").ToString()
            ddlDonorType_2.SelectedIndex = ddlDonorType_2.Items.IndexOf(ddlDonorType_2.Items.FindByText(IIf(dsRecords.Tables(0).Rows(0)("DonorType").ToString() Is DBNull.Value, "SELECT", dsRecords.Tables(0).Rows(0)("DonorType").ToString())))

        End Sub
        Protected Sub BtnHBTAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTAdd.Click
            Try
                Dim Strsql As String
                If BtnHBTAdd.Text = "Add" Then
                    If txtHBTToken.Text.Length > 2 And (txtHBTBk_TransCat.Text.Length > 2 Or txtHBTBk_VendorCust.Text.Length > 2) Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from HBTAddinfo where Token  LIke '" & txtHBTToken.Text & "%'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Dim token, Bk_TransCat, Bk_VendorCust, Bk_Reason, Bk_DonorType, MemberID, DonorType As String
                            If txtHBTToken.Text.Length > 1 Then
                                token = "'" & txtHBTToken.Text & "'"
                            Else
                                token = "null"
                            End If
                            If txtHBTBk_TransCat.Text.Length > 1 Then
                                Bk_TransCat = "'" & txtHBTBk_TransCat.Text & "'"
                            Else
                                Bk_TransCat = "Null"
                            End If
                            If txtHBTBk_VendorCust.Text.Length > 1 Then
                                Bk_VendorCust = "'" & txtHBTBk_VendorCust.Text & "'"
                            Else
                                Bk_VendorCust = "Null"
                            End If
                            If txtHBTBk_Reason.Text.Length > 1 Then
                                Bk_Reason = "'" & txtHBTBk_Reason.Text & "'"
                            Else
                                Bk_Reason = "Null"
                            End If
                            If ddlDonorType_HBT.SelectedIndex > 0 Then
                                Bk_DonorType = "'" & ddlDonorType_HBT.SelectedItem.Text & "'"
                            Else
                                Bk_DonorType = "NULL"
                            End If
                            If txtHBTBk_TransCat.Text = "Donation" Then
                                If txtMembID2.Text.Length = 0 Then
                                    lblHBTAddinfo.Text = "MemberID field is mandatory."
                                    Exit Sub
                                ElseIf ddlDonorType_2.SelectedIndex = "0" Then
                                    lblHBTAddinfo.Text = "DonorType field is mandatory."
                                    Exit Sub
                                ElseIf txtMembID2.Text.Length > 0 And ddlDonorType_2.SelectedIndex > 0 Then
                                    If ddlDonorType_2.SelectedValue = "IND" Then
                                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from INDSpouse where  AutoMemberID=" & txtMembID2.Text) <= 0 Then
                                            lblHBTAddinfo.Text = "No IndSpouse record present for the given MemberID "
                                            Exit Sub
                                        End If
                                    Else
                                        If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from OrganizationInfo where  AutoMemberID=" & txtMembID2.Text) <= 0 Then
                                            lblHBTAddinfo.Text = "No Organization record present for the given MemberID "
                                            Exit Sub
                                        End If
                                    End If
                                End If
                            End If

                            If txtMembID2.Text.Length > 0 Then
                                MemberID = txtMembID2.Text
                            Else
                                MemberID = "Null"
                            End If
                            If ddlDonorType_2.SelectedIndex > 0 Then
                                DonorType = "'" & ddlDonorType_2.SelectedItem.Text & "'"
                            Else
                                DonorType = "NULL"
                            End If

                            Strsql = "Insert Into HBTAddinfo(Token, Bk_TransCat, Bk_VendorCust, Bk_Reason,Bk_DonorType,MemberID,DonorType) Values ("
                            Strsql = Strsql & token & "," & Bk_TransCat & "," & Bk_VendorCust & "," & Bk_Reason & "," & Bk_DonorType & "," & MemberID & "," & DonorType & ")"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetHBTAddinfo()
                            lblHBTAddinfo.Text = "Record Added Successfully"
                            HBTclear()
                        Else
                            lblHBTAddinfo.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblHBTAddinfo.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnHBTAdd.Text = "Update" Then
                    If txtHBTToken.Text.Length > 2 And (txtHBTBk_TransCat.Text.Length > 2 Or txtHBTBk_VendorCust.Text.Length > 2) Then
                        Dim Bk_TransCat, Bk_VendorCust, Bk_Reason, Bk_DonorType, MemberID, DonorType As String
                        If txtHBTBk_TransCat.Text.Length > 1 Then
                            Bk_TransCat = "'" & txtHBTBk_TransCat.Text & "'"
                        Else
                            Bk_TransCat = "Null"
                        End If
                        If txtHBTBk_VendorCust.Text.Length > 1 Then
                            Bk_VendorCust = "'" & txtHBTBk_VendorCust.Text & "'"
                        Else
                            Bk_VendorCust = "Null"
                        End If
                        If txtHBTBk_Reason.Text.Length > 1 Then
                            Bk_Reason = "'" & txtHBTBk_Reason.Text & "'"
                        Else
                            Bk_Reason = "Null"
                        End If
                        If ddlDonorType_HBT.SelectedIndex > 0 Then
                            Bk_DonorType = "'" & ddlDonorType_HBT.SelectedItem.Text & "'"
                        Else
                            Bk_DonorType = "NULL"
                        End If
                        If txtHBTBk_TransCat.Text = "Donation" Then
                            If txtMembID2.Text.Length = 0 Then
                                lblHBTAddinfo.Text = "MemberID field is mandatory."
                                Exit Sub
                            ElseIf ddlDonorType_2.SelectedIndex = "0" Then
                                lblHBTAddinfo.Text = "DonorType field is mandatory."
                                Exit Sub
                            ElseIf txtMembID2.Text.Length > 0 And ddlDonorType_2.SelectedIndex > 0 Then
                                If ddlDonorType_2.SelectedValue = "IND" Then
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from INDSpouse where  AutoMemberID=" & txtMembID2.Text) <= 0 Then
                                        lblHBTAddinfo.Text = "No IndSpouse record present for the given MemberID "
                                        Exit Sub
                                    End If
                                Else
                                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, " Select Count(*) from OrganizationInfo where  AutoMemberID=" & txtMembID2.Text) <= 0 Then
                                        lblHBTAddinfo.Text = "No Organization record present for the given MemberID "
                                        Exit Sub
                                    End If
                                End If
                            End If
                        End If

                        If txtMembID2.Text.Length > 0 Then
                            MemberID = txtMembID2.Text
                        Else
                            MemberID = "Null"
                        End If
                        If ddlDonorType_2.SelectedIndex > 0 Then
                            DonorType = "'" & ddlDonorType_2.SelectedItem.Text & "'"
                        Else
                            DonorType = "NULL"
                        End If

                        Strsql = "Update HBTAddinfo Set Token='" & txtHBTToken.Text & "',Bk_TransCat=" & Bk_TransCat & ",Bk_VendorCust=" & Bk_VendorCust & ",Bk_Reason=" & Bk_Reason & ",Bk_DonorType=" & Bk_DonorType & ",MemberID=" & MemberID & ", DonorType=" & DonorType & "  where ID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetHBTAddinfo()
                        lblHBTAddinfo.Text = "Record Updated Successfully"
                        HBTclear()
                        BtnHBTAdd.Text = "Add"
                    Else
                        lblHBTAddinfo.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                lblHBTAddinfo.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub

        Protected Sub BtnHBTCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTCancel.Click
            HBTclear()
        End Sub
        Sub HBTclear()
            txtHBTToken.Text = ""
            txtHBTBk_TransCat.Text = ""
            txtHBTBk_VendorCust.Text = ""
            txtHBTBk_Reason.Text = ""
            txtMembID2.Text = ""
            ddlDonorType_HBT.SelectedIndex = 0
            ddlDonorType_2.SelectedIndex = 0
            Session("ID") = Nothing
            BtnHBTAdd.Text = "Add"
        End Sub

        Protected Sub BtnHBTAddinfo_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTAddinfo.Click
            GetHBTAddinfo()
            pnlChaseDesc.Visible = False
            pnlChaseTransType.Visible = False
            pnlHBTTransType.Visible = False
        End Sub

        Protected Sub BtnHBTClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTClose.Click
            HBTclear()
            pnlHBTAddinfo.Visible = False
        End Sub

        Private Sub GetHBTTransType()
            Dim dsRecords As New DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM HBTDesc Order By ID")
            Dim dt As DataTable = dsRecords.Tables(0)
            Dim dv As DataView = New DataView(dt)
            GridHBTTransType.DataSource = dt
            GridHBTTransType.DataBind()
            pnlHBTTransType.Visible = True
        End Sub
        Protected Sub GridHBTTransType_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridHBTTransType.RowCommand
            BtnHBTTransAdd.Text = "Update"
            Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
            Dim TransID As Integer
            TransID = Val(GridHBTTransType.DataKeys(index).Value)
            If (TransID) And TransID > 0 Then
                Session("ID") = TransID
                GetHBTTransSelectedRecord(TransID, e.CommandName.ToString())
            End If
        End Sub
        Private Sub GetHBTTransSelectedRecord(ByVal transID As Integer, ByVal status As String)
            Dim strsql As String = "SELECT * FROM HBTDesc where Id=" & transID
            Dim dsRecords As DataSet
            dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
            txtHBTTransToken.Text = dsRecords.Tables(0).Rows(0)("Token").ToString()
            txtHBTTransType.Text = dsRecords.Tables(0).Rows(0)("Bk_Transtype").ToString()
            txtHBTTransCat.Text = dsRecords.Tables(0).Rows(0)("Bk_TransCat").ToString()
        End Sub
        Protected Sub BtnHBTTransAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTTransAdd.Click
            Try
                Dim Strsql As String
                If BtnHBTTransAdd.Text = "Add" Then
                    If txtHBTTransToken.Text.Length > 2 And (txtHBTTransType.Text.Length > 2 Or txtHBTTransCat.Text.Length > 2) Then
                        Dim cnt As Integer = 0
                        Strsql = "Select count(*) from HBTDesc where Token  Like '" & txtHBTTransToken.Text & "'"
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Dim token, Bk_TransType, Bk_Transcat As String
                            If txtHBTTransToken.Text.Length > 1 Then
                                token = "'" & txtHBTTransToken.Text & "'"
                            Else
                                token = "null"
                            End If
                            If txtHBTTransType.Text.Length > 1 Then
                                Bk_TransType = "'" & txtHBTTransType.Text & "'"
                            Else
                                Bk_TransType = "Null"
                            End If
                            If txtHBTTransCat.Text.Length > 1 Then
                                Bk_Transcat = "'" & txtHBTTransCat.Text & "'"
                            Else
                                Bk_Transcat = "Null"
                            End If
                            Strsql = "Insert Into HBTDesc(Token, Bk_TransType,Bk_TransCat,PrimaryID) Values ("
                            Strsql = Strsql & token & "," & Bk_TransType & "," & Bk_Transcat & ",555)"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            Dim reader1 As SqlDataReader = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "ALTER TABLE HBTDesc DROP CONSTRAINT pk_PrimaryID;update HBTDesc set PrimaryID = 0;Select * from HBTDesc order by len(Token) DESC")
                            Dim PKid As Integer = 1
                            While reader1.Read()
                                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "Update HBTDesc SET PrimaryID=" & PKid & " where id=" & reader1("ID") & "")
                                PKid = PKid + 1
                            End While
                            reader1.Close()
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "ALTER TABLE HBTDesc ADD CONSTRAINT pk_PrimaryID PRIMARY KEY (PrimaryID)")
                            GetHBTTransType()
                            lblHBTTransType.Text = "Record Added Successfully"
                            TransHBTclear()
                        Else
                            lblHBTTransType.Text = "The Token Already already Exist"
                        End If
                    Else
                        lblHBTTransType.Text = "Please Enter Data in All boxes"
                    End If
                ElseIf BtnHBTTransAdd.Text = "Update" Then
                    If txtHBTTransToken.Text.Length > 2 And (txtHBTTransType.Text.Length > 2 Or txtHBTTransType.Text.Length > 2) Then
                        Dim Bk_TransType, Bk_Transcat As String
                        If txtHBTTransType.Text.Length > 1 Then
                            Bk_TransType = "'" & txtHBTTransType.Text & "'"
                        Else
                            Bk_TransType = "Null"
                        End If
                        If txtHBTTransCat.Text.Length > 1 Then
                            Bk_Transcat = "'" & txtHBTTransCat.Text & "'"
                        Else
                            Bk_Transcat = "Null"
                        End If
                        Strsql = "Update HBTDesc Set Token='" & txtHBTTransToken.Text & "',Bk_TransType=" & Bk_TransType & ",Bk_TransCat=" & Bk_Transcat & " where ID=" & Session("ID")
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetHBTTransType()
                        lblHBTTransType.Text = "Record Updated Successfully"
                        TransHBTclear()
                        BtnHBTTransAdd.Text = "Add"
                    Else
                        lblHBTTransType.Text = "Please Enter Data in All boxes"
                    End If
                End If
            Catch ex As Exception
                SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, "update HBTDesc set primaryid=(select count(*)  from  hbtdesc) where primaryid=555")
                lblHBTTransType.Text = "Error in SQL Execution :" & ex.ToString()
            End Try
        End Sub
        Protected Sub BtnHBTTransCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTTransCancel.Click
            TransHBTclear()
        End Sub
        Sub TransHBTclear()
            txtHBTTransToken.Text = ""
            txtHBTTransType.Text = ""
            txtHBTTransCat.Text = ""
            Session("ID") = Nothing
            BtnHBTTransAdd.Text = "Add"
        End Sub

        Protected Sub BtnHBTTransType_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnHBTTransType.Click
            GetHBTTransType()
            pnlHBTAddinfo.Visible = False
            pnlChaseDesc.Visible = False
            pnlChaseTransType.Visible = False
        End Sub

        Protected Sub BtnHBTTransClose_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnHBTTransClose.Click
            TransHBTclear()
            pnlHBTTransType.Visible = False
        End Sub
        Sub ExecuteQuery(ByVal StrSQL As String)
            Dim conn As New SqlConnection
            conn.ConnectionString = Application("ConnectionString")
            Dim cmd As SqlCommand = New SqlCommand(StrSQL, conn)
            conn.Open()
            cmd.CommandTimeout = 600
            cmd.CommandType = CommandType.Text
            cmd.ExecuteNonQuery()
            conn.Close()
        End Sub
       
        Protected Sub ddlEditUpdateDB_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs)
            closepanel()
            FillTransCategory()
            DdlTransCat.Enabled = True
        End Sub
        Public Sub ChaseBk_TransType_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transType from ChaseTransType Where Bk_transType is NOt NULL order by Bk_transType Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transType"), dsRecords("Bk_transType")))
                End While
                'ddl.Items.Insert(0, "Select TransType")
            End If
        End Sub
        Public Sub ChaseBk_TransType_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransType As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransType") Is DBNull.Value) Then
                    Bk_TransType = dr.Item("Bk_TransType")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransType))
            Else
                Return
            End If
        End Sub
        Public Sub ChaseBk_TransCat_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransCat As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransCat") Is DBNull.Value) Then
                    Bk_TransCat = dr.Item("Bk_TransCat")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransCat))
            Else
                Return
            End If
        End Sub
        Public Sub ChaseBk_TransCat_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transCat from ChaseDesc Where Bk_transCat is Not NULL order by Bk_transCat Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transCat"), dsRecords("Bk_transCat")))
                End While
                ddl.Items.Insert(0, "Select TransCat")
            End If
        End Sub
        Public Sub HBT1Bk_TransType_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transType from HBTDesc Where Bk_transType is NOt NULL order by Bk_transType Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transType"), dsRecords("Bk_transType")))
                End While
                'ddl.Items.Insert(0, "Select TransType")
            End If
        End Sub
        Public Sub HBT1Bk_TransType_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransType As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransType") Is DBNull.Value) Then
                    Bk_TransType = dr.Item("Bk_TransType")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransType))
            Else
                Return
            End If
        End Sub
        Public Sub HBT1Bk_TransCat_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransCat As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransCat") Is DBNull.Value) Then
                    Bk_TransCat = dr.Item("Bk_TransCat")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransCat))
            Else
                Return
            End If
        End Sub
        Public Sub HBT1Bk_TransCat_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transCat from HBTDesc Where Bk_transCat is Not NULL order by Bk_transCat Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transCat"), dsRecords("Bk_transCat")))
                End While
                ddl.Items.Insert(0, "Select TransCat")

            End If
        End Sub
        Public Sub HBT2Bk_TransType_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transType from HBTDesc Where Bk_transType is NOt NULL order by Bk_transType Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transType"), dsRecords("Bk_transType")))
                End While
                'ddl.Items.Insert(0, "Select TransType")
            End If
        End Sub
        Public Sub HBT2Bk_TransType_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransType As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransType") Is DBNull.Value) Then
                    Bk_TransType = dr.Item("Bk_TransType")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransType))
            Else
                Return
            End If
        End Sub
        Public Sub HBT2Bk_TransCat_PreRender(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim Bk_TransCat As String
            Dim dr As DataRow = CType(Cache("editRow"), DataRow)
            If (Not dr Is Nothing) Then
                If (Not dr.Item("Bk_TransCat") Is DBNull.Value) Then
                    Bk_TransCat = dr.Item("Bk_TransCat")
                End If
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByText(Bk_TransCat))
            Else
                Return
            End If
        End Sub
        Public Sub HBT2Bk_TransCat_OnInit(ByVal sender As Object, ByVal e As System.EventArgs)
            Dim ddl As DropDownList
            ddl = sender
            ddl.Items.Clear()
            Dim dsRecords As SqlDataReader
            Dim i As Integer = 0
            If ddl.Items.Count = 0 Then
                dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "select Distinct Bk_transCat from HBTDesc Where Bk_transCat is Not NULL order by Bk_transCat Desc")
                While dsRecords.Read
                    ddl.Items.Insert(++i, New ListItem(dsRecords("Bk_transCat"), dsRecords("Bk_transCat")))
                End While
                ddl.Items.Insert(0, "Select TransCat")
            End If
        End Sub
        Protected Sub lbtnVolunteerFunctions_Click(ByVal sender As Object, ByVal e As System.EventArgs)
            Session.Remove("StrSQL")
            Session.Remove("HBT1DataSet")
            Session.Remove("HBT2DataSet")
            Session.Remove("ChaseDataSet")
            Session.Remove("CTransLastDate")
            Session.Remove("ID")
            Cache.Remove("editRow")
            Response.Redirect("~/VolunteerFunctions.aspx")
        End Sub
        Public Sub SetDropDown_Reason(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("Reason")))
            Catch ex As Exception

            End Try
        End Sub
        Public Sub SetDropDown_RestType(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("RestType")))
            Catch ex As Exception
            End Try
        End Sub
        Public Sub SetDropDownBk_DonorType(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("DonorType")))
            Catch ex As Exception
            End Try
        End Sub
        Public Sub SetDropDown_DonorType(ByVal sender As Object, ByVal e As System.EventArgs)
            Try
                Dim ddlTemp As System.Web.UI.WebControls.DropDownList
                ddlTemp = sender
                ddlTemp.SelectedIndex = ddlTemp.Items.IndexOf(ddlTemp.Items.FindByValue(Session("DonorType")))
            Catch ex As Exception
            End Try
        End Sub
        Protected Sub ddlTransType_PreRender(sender As Object, e As EventArgs)
            Try
                Dim dr As DataRow = Cache("editRow")
                Dim BankId As Integer = dr("BankId")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select distinct(TransType) from BankTrans where BankID = " & BankId)
                Dim ddl As DropDownList = sender
                ddl.DataSource = ds.Tables(0)
                ddl.DataBind()
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(dr("TransType")))
            Catch ex As Exception
            End Try
        End Sub
        Protected Sub ddlTransCat_PreRender(sender As Object, e As EventArgs)
            Try
                Dim dr As DataRow = Cache("editRow")
                Dim BankId As Integer = dr("BankId")
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select distinct(TransCat) from BankTrans where BankID = " & BankId)
                Dim ddl As DropDownList = sender
                ddl.DataSource = ds.Tables(0)
                ddl.DataBind()
                ddl.SelectedIndex = ddl.Items.IndexOf(ddl.Items.FindByValue(dr("TransCat")))
            Catch ex As Exception
            End Try
        End Sub
    End Class
End Namespace