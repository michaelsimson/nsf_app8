<%@ Page Language="C#" %>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<script runat="server">
	protected void Page_Load(object sender, EventArgs e)
	{
		string filePath = (string) Context.Items["DOWNLOAD_FILE_PATH"];
		if (filePath == null || filePath.Length <= 0){
			filePath = (string) Session["DOWNLOAD_FILE_PATH"];
			Session["DOWNLOAD_FILE_PATH"] = "";
		}
		if (filePath != null && filePath.Length > 0)
		{
			System.IO.FileInfo file = new System.IO.FileInfo(filePath);
			if (file.Exists)
			{
				Response.Clear();
				//Response.AddHeader("Content-Disposition", "attachment; filename=" + file.Name);
                Response.AddHeader("Content-Disposition", "attachment; filename=\"" + file.Name + "\"");
				Response.AddHeader("Content-Length", file.Length.ToString());				
				Response.ContentType = "application/octet-stream";
				Response.WriteFile(file.FullName);
				Response.End(); //if file does not exist

			}
			else
			{
				Response.Write("Requested Files Does not exist: " + filePath);
			}
		}
		else
		{
			Response.Write("Nothing to download");
		}
	}
</script>