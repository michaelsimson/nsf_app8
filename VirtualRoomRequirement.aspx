﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VirtualRoomRequirement.aspx.cs" Inherits="VirtualRoomRequirement" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <script type="text/javascript">

        function confirmtosave() {
            try {

                if (confirm("Do you really want to override the existing results in the server?")) {
                    // alert(document.getElementById('<%= hiddenbtn.ClientID %>'));
                    document.getElementById('<%= hiddenbtn.ClientID %>').click();

                }
                else {

                }
            } catch (ex) { }
        }
        function confirmtosavedatenull() {
            try {

                if (confirm("The Minimum of Start Date is Empty! Do you really want to override the existing results in the server?")) {
                    // alert(document.getElementById('<%= hiddenbtn.ClientID %>'));
                    document.getElementById('<%= hiddenbtn.ClientID %>').click();

                }
                else {

                }
            } catch (ex) { }
        }

    </script>

    <div>
        <table border="0" cellpadding="3" cellspacing="0" width="980">
            <tr>
                <td align="left">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>
                    &nbsp;&nbsp;&nbsp;<asp:LinkButton CssClass="btn_02" ID="LBmain" runat="server" OnClick="LinkButton1_Click">Back to Main Page</asp:LinkButton>

                    &nbsp;&nbsp;
                </td>
            </tr>
        </table>

        <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);">
            <strong>Virtual Room Requirement

            </strong>
        </div>
        <br />

        <table width="100%">
            <tr>
                <td width="10%"></td>
                <td align="right"><b>Year</b></td>
                <td>
                    <asp:DropDownList ID="DDyear" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="right"><b>Semester</b></td>
                <td>
                    <asp:DropDownList ID="ddlSemester" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="right"><b>Pre-Class Preparation</b></td>
                <td>
                    <asp:DropDownList ID="DDpre" runat="server" OnSelectedIndexChanged="DDpre_SelectedIndexChanged">
                    </asp:DropDownList>
                </td>
                <td align="right"><b>Post-Class Coverage</b></td>
                <td>
                    <asp:DropDownList ID="DDpost" runat="server"
                        OnSelectedIndexChanged="DDpost_SelectedIndexChanged">
                    </asp:DropDownList></td>
                <td width="20%"></td>
            </tr>
            <tr>

                <td></td>
                <td align="center" colspan="7">
                    <br />
                    <asp:Button ID="disresults" runat="server" Text="Display Results from Server" OnClick="disresults_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp
                        <asp:Button ID="Btnsubmit" runat="server" Text="Make a New Run"
                            OnClick="Virtual_Click" />
                    <br />
                </td>
                <td></td>
                <td></td>

            </tr>
            <tr>

                <td align="center" colspan="8">
                    <asp:Label ID="lblalert" Visible="false" runat="server" ForeColor="Red"
                        Text=""></asp:Label></td>

            </tr>



        </table>


        <div runat="server" id="aftersubmit" visible="false">

            <table>
                <tr>
                    <td align="left" style="height: 30px">
                        <asp:Button ID="saveresultsbtn" runat="server" Text="Save New Results" OnClick="saveresultsbtn_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        <asp:Button ID="excel" runat="server" Text="Export To Excel"
                            OnClick="excel_Click" />
                        <asp:Button ID="hiddenbtn" runat="server" Text="Button" OnClick="hiddenbtn_Click" Style="display: none;" />
                    </td>

                </tr>
                <tr>
                    <td align="center">
                        <asp:Label ID="lblsuccess" Visible="False" runat="server" ForeColor="Blue" Font-Bold="True"></asp:Label>
                        <br />
                    </td>

                </tr>
                <tr>
                    <td>
                        <asp:HiddenField ID="hYear" runat="server" />
                        <asp:DataGrid ID="gridview_VR" runat="server"
                            AutoGenerateColumns="False" Height="14px" CellPadding="2"
                            BorderWidth="3px" BorderStyle="Double"
                            BorderColor="#336666" ForeColor="White" Font-Bold="True" Font-Italic="False"
                            Font-Overline="False" Font-Strikeout="False"
                            Font-Underline="False" CssClass="gv_vr_rep">



                            <ItemStyle Font-Size="X-Small" Font-Names="Verdana" BackColor="White"></ItemStyle>

                            <FooterStyle ForeColor="#333333" BackColor="White"></FooterStyle>
                            <Columns>

                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="S.No">
                                    <HeaderStyle Width="130px" Font-Bold="True" ForeColor="White"></HeaderStyle>

                                    <ItemTemplate><%#Container.DataSetIndex+1 %></ItemTemplate>



                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="SignUpID ">

                                    <ItemTemplate>
                                        <%--<asp:HiddenField ID="EventYear" Value='<%# Bind("EventYear")%>' runat="server" />--%>
                                        <asp:Label ID="lblsign" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.SignUpID")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <%--          <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Rownum ">
                      <HEADERSTYLE Width="130px" Font-Bold="True" ForeColor="White"></HEADERSTYLE> 
                        <ItemTemplate>
                                 <asp:Label ID="lblsign" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>
                                    </ItemTemplate>                                                         

                      <ItemStyle HorizontalAlign="Left"></ItemStyle>
                      </asp:TemplateColumn >--%>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Member ID">

                                    <ItemTemplate>
                                        <asp:Label ID="lblmember" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.MemberID")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="First Name">

                                    <ItemTemplate>
                                        <asp:Label ID="lblfirstname" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.FirstName")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Last Name">

                                    <ItemTemplate>
                                        <asp:Label ID="lbllastname" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.LastName")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Event Year ">

                                    <ItemTemplate>
                                        <asp:Label ID="lblEventYear" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.EventYear")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="ProductGroup Code ">


                                    <ItemTemplate>
                                        <asp:Label ID="lblpgcode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductGroupCode")%>'></asp:Label>
                                    </ItemTemplate>



                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Product Code">

                                    <ItemTemplate>
                                        <asp:Label ID="lblpcode" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.ProductCode")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Level">

                                    <ItemTemplate>
                                        <asp:Label ID="lbllevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Level")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                 <asp:TemplateColumn HeaderText="Semester">

                                    <ItemTemplate>
                                        <asp:Label ID="lbllevel" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Semester")%>'></asp:Label>
                                    </ItemTemplate>
                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Accepted">

                                    <ItemTemplate>
                                        <asp:Label ID="lblaccept" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Accepted")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="D">

                                    <ItemTemplate>
                                        <asp:Label ID="lblD" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.D")%>'></asp:Label>
                                    </ItemTemplate>

                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Day">

                                    <ItemTemplate>
                                        <asp:Label ID="lblday" runat="server" Visible="true" Text='<%#DataBinder.Eval(Container, "DataItem.Day").ToString().Substring(0,3)%>'></asp:Label>
                                        <%--<asp:Label ID="Label2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Day").ToString().Substring(0,2)%>'></asp:Label>--%>
                                    </ItemTemplate>


                                </asp:TemplateColumn>

                                <asp:TemplateColumn HeaderText="Time">

                                    <ItemTemplate>
                                        <asp:Label ID="lbltime" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.Time")%>'></asp:Label>
                                        <asp:Label ID="Label1" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Times")%>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Duration" Visible="true">

                                    <ItemTemplate>
                                        <asp:Label ID="lblduration" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Duration")%>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="Begin Time" Visible="true">

                                    <ItemTemplate>
                                        <asp:Label ID="lblbegin" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.Begin")%>'></asp:Label>
                                        <asp:Label ID="Label3" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Begins")%>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>
                                <asp:TemplateColumn HeaderText="End Time">

                                    <ItemTemplate>
                                        <asp:Label ID="lblEnd" runat="server" Visible="false" Text='<%#DataBinder.Eval(Container, "DataItem.End")%>'></asp:Label>
                                        <asp:Label ID="Label4" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Ends")%>'></asp:Label>
                                    </ItemTemplate>


                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Cycle">

                                    <ItemTemplate>
                                        <asp:Label ID="lblcycle" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Cycle")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Virtual Room">

                                    <ItemTemplate>
                                        <asp:Label ID="lblvir" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.VRoom")%>'></asp:Label>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="UserID">

                                    <ItemTemplate>
                                        <asp:Label ID="Label42" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.UserId")%>'></asp:Label>
                                        <%-- <asp:Label ID="Label1" runat="server" Text='<%#"northsouth.account0" +DataBinder.Eval(Container, "DataItem.VRoom")+"@live.com"%>'></asp:Label>
                                        --%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Password">

                                    <ItemTemplate>
                                        <asp:Label ID="Labe2" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Password")%>'></asp:Label>
                                        <%--<asp:Label ID="Lblpwd" runat="server" Text='<%#"nsfaccount0" +DataBinder.Eval(Container, "DataItem.VRoom")%>'></asp:Label>--%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Previous">

                                    <ItemTemplate>
                                        <%--       <asp:Label ID="Labe2" runat="server" Text='<%#processpwd(Eval("Previous"))%>'></asp:Label>--%>
                                        <asp:Label ID="lblprev" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.Previous")%>'></asp:Label>
                                        <%-- <asp:Label ID="lblrow" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>--%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                                <asp:TemplateColumn ItemStyle-HorizontalAlign="Left" HeaderText="Next">

                                    <ItemTemplate>
                                        <%--<asp:Label ID="Labe2" runat="server" Text='<%#processpwd(Eval("next"))%>'></asp:Label>--%>
                                        <asp:Label ID="lblnext" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.next")%>'></asp:Label>
                                        <%-- <asp:Label ID="lblrow" runat="server" Text='<%#DataBinder.Eval(Container, "DataItem.RowNum")%>'></asp:Label>--%>
                                    </ItemTemplate>

                                    <ItemStyle HorizontalAlign="Left"></ItemStyle>
                                </asp:TemplateColumn>
                            </Columns>
                            <PagerStyle ForeColor="#8C4510" HorizontalAlign="Center" Mode="NumericPages"></PagerStyle>

                        </asp:DataGrid>

                    </td>

                </tr>

            </table>
        </div>
        <div align="center">
            <asp:Label ID="lblNoRecords" runat="server" ForeColor="Red"></asp:Label>
        </div>





    </div>
    <br />


</asp:Content>

