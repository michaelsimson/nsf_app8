﻿<%@ Page Language="VB" AutoEventWireup="false" MasterPageFile="~/NSFMasterPage.master" CodeFile="volAssessmentForm.aspx.vb" Inherits="volAssessmentForm" %>


<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">


    <script language="javascript">
        window.history.forward(1);
    </script>
    <script language="javascript" type="text/javascript">
        function PopupPicker(ctl, w, h) {
            var PopupWindow = null;
            settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('DatePicker.aspx?Ctl=' + ctl, 'DatePicker', settings);
            PopupWindow.focus();
        }
    </script>
    <script type="text/javascript" language="javascript">
        function ConfirmOnDelete() {
            if (confirm("Are you sure to delete?") == true)
                return true;
            else
                return false;
        }
    </script>

    <div align="left">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:HyperLink ID="hlinkParentRegistration" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:HyperLink>
    </div>

    <div style="width: 900px" align="center">
        <h4>Self Assessment Exam Answers Sheet</h4>
    </div>
    <div style="width: 900px" align="center" id="divid1" runat="server">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="center" colspan="2"></td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
                <td align="center">&nbsp;</td>
            </tr>
            <tr>
                <td align="left"></td>
                <td align="left"></td>
                <td align="left">&nbsp;</td>
                <td align="left">&nbsp;</td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="left">Product Group</td>
                <td align="left">
                    <asp:DropDownList ID="ddlProductGroup"
                        DataTextField="Name" DataValueField="ProductGroupID"
                        OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                        AutoPostBack="true" runat="server" Height="24px" Width="150px">
                    </asp:DropDownList></td>
                <td align="left">&nbsp;</td>
                <td align="left">Product </td>
                <td align="left">
                    <asp:DropDownList Width="150px" Enabled="false" ID="ddlProduct" DataTextField="ProductName" OnSelectedIndexChanged="ddlProduct_SelectedIndexChanged" DataValueField="ProductID" AutoPostBack="true" runat="server"></asp:DropDownList></td>
            </tr>
            <tr>
                <td align="center" colspan="5">
                    <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Create New" />
                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnResult" runat="server" Text="Get Results" OnClick="btnResult_Click" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <asp:Button ID="btnExport" OnClick="btnExport_Click" Visible="false" runat="server" Text="Export Results" />
                </td>
            </tr>
            <tr>
                <td align="center" colspan="5">
                    <asp:Label ID="lblerr" runat="server" ForeColor="Red"></asp:Label>
                    <asp:Label ID="lblAssessExamID" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblEditResult" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblProductID" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblCount" ForeColor="White" runat="server"></asp:Label>
                    <asp:Label ID="lblPrd" ForeColor="White" runat="server" Visible="false"></asp:Label>
                    <asp:Label ID="lblPrdGrp" ForeColor="White" runat="server" Visible="false"></asp:Label>

                </td>
            </tr>
        </table>
        <asp:Label ID="lblHeader" CssClass="SmallFont" runat="server"></asp:Label>
        <asp:GridView HorizontalAlign="center" RowStyle-HorizontalAlign="center" ID="GdAssessExam" HeaderStyle-BackColor="#ffffcc" DataKeyNames="AssessExamID" AutoGenerateColumns="false" runat="server" OnRowCommand="GdAssessExam_RowCommand" RowStyle-CssClass="SmallFont">
            <Columns>
                <asp:ButtonField DataTextField="AssessExamID" HeaderText="AssessExamID"></asp:ButtonField>
                <asp:BoundField DataField="StartDate" HeaderText="StartDate" DataFormatString="{0:d}"></asp:BoundField>
                <asp:BoundField DataField="EndDate" HeaderText="EndDate" DataFormatString="{0:d}"></asp:BoundField>
                <asp:BoundField DataField="GradeFrom" HeaderText="GradeFrom"></asp:BoundField>
                <asp:BoundField DataField="GradeTo" HeaderText="GradeTo"></asp:BoundField>
                <asp:BoundField DataField="Name" HeaderText="ProductName"></asp:BoundField>
                <asp:BoundField DataField="Total" HeaderText="No. of Contestants"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>
    <div align="center" id="DivID2" runat="server" style="width: 900px" visible="false">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="center" colspan="3">Please provide answers to the following questions<br />
                    <br />
                </td>
            </tr>


            <tr>
                <td align="left">Grade From </td>
                <td align="left">
                    <asp:DropDownList ID="ddlGradeFrom" Width="60px" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="left">Grade To </td>
                <td align="left">
                    <asp:DropDownList ID="ddlGradeTo" Width="60px" runat="server">
                    </asp:DropDownList>
                </td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="left">Start Date</td>
                <td align="left">
                    <asp:TextBox ID="txtStartDate" Width="100px" runat="server"></asp:TextBox><a href="javascript:PopupPicker('txtStartDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="left">End Date</td>
                <td align="left">
                    <asp:TextBox ID="txtEndDate" Width="100px" runat="server"></asp:TextBox><a href="javascript:PopupPicker('txtEndDate', 200, 200);"><img src="images/calendar.gif" border="0"></a></td>
                <td align="left">&nbsp;</td>
            </tr>
            <tr>
                <td align="left"><b>Q1</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ1" runat="server"></asp:TextBox></td>
                <td align="left" style="width: 100px">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtQ1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q2</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ2" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtQ2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q3</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ3" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtQ3" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q4</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ4" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtQ4" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q5</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ5" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtQ5" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q6</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ6" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtQ6" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q7</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ7" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtQ7" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q8</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ8" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtQ8" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q9</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ9" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtQ9" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="left"><b>Q10</b> </td>
                <td align="left">
                    <asp:TextBox ID="txtQ10" runat="server"></asp:TextBox></td>
                <td align="left">
                    <asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtQ10" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td>
            </tr>
            <tr>
                <td align="center" colspan="3">
                    <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick="btnSubmit_Click" />
                    &nbsp;&nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btncancel" runat="server" Text="Cancel" EnableClientScript="False" CausesValidation="False" OnClick="btncancel_Click" /></td>
            </tr>
        </table>
    </div>
    <div align="center" id="divResult" runat="server" style="width: 900px" visible="false">
        <table border="0" cellpadding="3" cellspacing="0">
            <tr>
                <td align="center">
                    <h4>Results</h4>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>Over All : </b>
                    <br />
                    <center>
                        <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridOverAll" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" HeaderStyle-BackColor="#ffffcc">
                            <Columns>
                                <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                                <asp:BoundField DataField="Participants" HeaderText="Participants"></asp:BoundField>
                                <asp:BoundField DataField="total" HeaderText="Average Score" DataFormatString="{0:N1}"></asp:BoundField>
                            </Columns>
                        </asp:GridView>
                    </center>
                </td>
            </tr>
            <tr>
                <td align="left">
                    <b>By Question :</b><br />
                    <asp:GridView HorizontalAlign="Left" RowStyle-HorizontalAlign="Left" ID="GridByQ" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont" HeaderStyle-BackColor="#FFFFCC">
                        <Columns>
                            <asp:BoundField DataField="Grade" HeaderText="Grade"></asp:BoundField>
                            <asp:BoundField DataField="Q1" HeaderText="Q1 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q2" HeaderText="Q2 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q3" HeaderText="Q3 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q4" HeaderText="Q4 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q5" HeaderText="Q5 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q6" HeaderText="Q6 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q7" HeaderText="Q7 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q8" HeaderText="Q8 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q9" HeaderText="Q9 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Q10" HeaderText="Q10 Avg Score" DataFormatString="{0:N0}"></asp:BoundField>
                            <asp:BoundField DataField="Participants" HeaderText="Participants"></asp:BoundField>
                        </Columns>
                    </asp:GridView>
                </td>
            </tr>
            <tr>
                <td align="center">
                    <asp:Button ID="Btnclose" runat="server" Text="Close" OnClick="Btnclose_Click" /></td>
            </tr>
        </table>
    </div>
</asp:Content>
