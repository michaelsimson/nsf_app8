﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
using SurveyMonkey;
using System.Web.Script.Serialization;
using System.IO;


public partial class SurveyResponse : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        // ListAllSurvey();
    }
    public static string responseHtml = string.Empty;
    public static string reponseExcelFileName = string.Empty;
    public void ListAllSurvey()
    {
        dvSurveyList.Visible = false;
        string CmdText = string.Empty;
        DataSet ds = new DataSet();
        CmdText = "select SL.SurveyID,SL.Year,SL.EventID,SL.ChapterID,SL.ProductGroupID,SL.RespondentType,SL.Title,ResponseCount,E.Name as EventName,C.ChapterCode,P.Name from SurveyList SL inner join Event E on(SL.EventID=E.EventID) inner join ProductGroup P on(SL.ProductGroupID=P.ProductGroupID)inner join Chapter C on (SL.ChapterID=C.ChapterID)";
        try
        {
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);
            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdSurveyTemlate.DataSource = ds;
                    GrdSurveyTemlate.DataBind();
                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void GrdSurveyTemlate_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        try
        {
            if (e.CommandName == "Select")
            {
                //dvRespondContent.Visible = true;
            }
        }
        catch
        {
        }
    }


    public class SurveyMaster
    {
        public string SurveyID { get; set; }
        public string SurveyTitle { get; set; }
        public string QuestionID { get; set; }
        public string QuestionTitle { get; set; }
        public string QuestionRowID { get; set; }
        public string QuestionRowTitle { get; set; }
        public string QuestionColumnID { get; set; }
        public string QuestionColumnTitle { get; set; }
        public string CretaedDate { get; set; }
        public string CreatedBy { get; set; }
        public string ModifiedDate { get; set; }
        public string ModifiedBy { get; set; }
        public string ChoiceFormat { get; set; }
        public int ColCount { get; set; }
        public int RowCount { get; set; }
        public int Mode { get; set; }
        public List<SurveyRows> lstSurveyRows { get; set; }
        public List<SurveyColumns> lstSurveyCols { get; set; }
    }

    public class SurveyRows
    {
        public int? QuestionID { get; set; }
        public int? QuestRowID { get; set; }
        public string RowTitle { get; set; }
    }

    public class SurveyColumns
    {
        public int? QuestionID { get; set; }
        public int? QuestColID { get; set; }
        public string ColumnTitle { get; set; }
    }

    [WebMethod]
    public static List<SurveyMaster> GetSurveyLayOut(SurveyMaster objSurvey)
    {
        int retVal = -1;
        List<SurveyMaster> objListSurvey = new List<SurveyMaster>();
        try
        {

            SqlConnection cn = new SqlConnection(System.Configuration.ConfigurationManager.AppSettings["DBConnection"]);
            cn.Open();

            string cmdText = "select QuestionID,title,ChoiceFormat, (select COUNT(*) from SurveyQuestionColumns where QuestionID=SQ.QuestionID) as cnt, (select COUNT(*) from QuestionRows where QuestionID=SQ.QuestionID) as Rowcnt from SurveyQuestions SQ ; select * from QuestionRows ; select * from SurveyQuestionColumns";
            SqlCommand cmd = new SqlCommand(cmdText, cn);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataSet ds = new DataSet();
            da.Fill(ds);
            if (null != ds)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        SurveyMaster sur = new SurveyMaster();
                        sur.QuestionID = dr["QuestionID"].ToString();
                        sur.QuestionRowTitle = dr["title"].ToString();
                        sur.ChoiceFormat = dr["ChoiceFormat"].ToString();
                        sur.QuestionTitle = dr["title"].ToString();
                        sur.ColCount = Convert.ToInt32(dr["cnt"].ToString());
                        sur.RowCount = Convert.ToInt32(dr["Rowcnt"].ToString());

                        List<SurveyRows> lstSurveyRows = new List<SurveyRows>();
                        List<SurveyColumns> lstSurveyCols = new List<SurveyColumns>();
                        foreach (DataRow dr1 in ds.Tables[1].Rows)
                        {
                            SurveyRows objSurveyRows = new SurveyRows();
                            objSurveyRows.QuestionID = Convert.ToInt32(dr1["QuestionID"].ToString());
                            objSurveyRows.QuestRowID = Convert.ToInt32(dr1["QuestRowID"].ToString());
                            objSurveyRows.RowTitle = dr1["Title"].ToString();
                            lstSurveyRows.Add(objSurveyRows);
                        }

                        foreach (DataRow dr2 in ds.Tables[2].Rows)
                        {
                            SurveyColumns objSurveyCols = new SurveyColumns();
                            objSurveyCols.QuestionID = Convert.ToInt32(dr2["QuestionID"].ToString());
                            objSurveyCols.QuestColID = Convert.ToInt32(dr2["QuestColID"].ToString());
                            objSurveyCols.ColumnTitle = dr2["Title"].ToString();
                            lstSurveyCols.Add(objSurveyCols);
                        }

                        sur.lstSurveyCols = lstSurveyCols;
                        sur.lstSurveyRows = lstSurveyRows;
                        objListSurvey.Add(sur);
                    }
                }
            }


        }
        catch (Exception ex)
        {
        }
        return objListSurvey;
    }

    [WebMethod]
    public static List<Respondent> GetRespondantList(long SurveyID)
    {

        string apiKey = "cqrfdehmyse424xpqy4njg3e";
        string token = "ZdmKP6aY5gXVreKYNNZ8-3e62ZZNthJIakgvPVC4EsktsRYlw9mFGnRb8fDf8kHk1BUOmbI7a-zCbWoOmHhuhRIVZ4UOFYYuAANJ.BIv7VL8Qj3XqsVVKfbCfr8El3sS";
       // string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
        var sm = new SurveyMonkeyApi(apiKey, token);
        var settings = new GetSurveyListSettings
        {
            //Title = "2015 NSF Finals - Parent SurveyMaster",
            StartDate = Convert.ToDateTime("01/01/2015")
        };


        List<Respondent> Respondant = new List<Respondent>();
        Respondant = sm.GetRespondentList(SurveyID);

        return Respondant;

    }

    [WebMethod]
    public static List<Response> GetResponseFromSM(long SurveyID, long RespondantID)
    {

        string apiKey = "cqrfdehmyse424xpqy4njg3e";
        string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
        var sm = new SurveyMonkeyApi(apiKey, token);

        List<long> respondent = new List<long>();
        respondent.Add(RespondantID);

        List<Response> Respondanse = new List<Response>();


        Respondanse = sm.GetResponses(SurveyID, respondent);
        return Respondanse;

    }

    [WebMethod]
    public static List<Survey> ListSurveyDetails(long SurveyID)
    {

        string apiKey = "cqrfdehmyse424xpqy4njg3e";
        string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
        var sm = new SurveyMonkeyApi(apiKey, token);
        var settings = new GetSurveyListSettings
        {
            //Title = "2015 NSF Finals - Parent SurveyMaster",
            StartDate = Convert.ToDateTime("01/01/2015")
        };

        Survey sur = sm.GetSurveyDetails(SurveyID);
        List<Survey> surveys = new List<Survey>();
        surveys.Add(sur);

        return surveys;

    }

    //[WebMethod]
    //public static List<Response> GetResponseExcel(long SurveyID, string RespondantID)
    //{

    //    string apiKey = "cqrfdehmyse424xpqy4njg3e";
    //    string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
    //    var sm = new SurveyMonkeyApi(apiKey, token);

    //    List<long> respondent = new List<long>();
    //    for (int i = 0; i < RespondantID.Split(',').Length; i++)
    //    {
    //        long resID;
    //        resID = Convert.ToInt64(RespondantID.Split(',')[i].ToString());
    //        respondent.Add(resID);
    //    }
    //    List<Response> Respondanse = new List<Response>();


    //    Respondanse = sm.GetResponses(SurveyID, respondent);
    //    return Respondanse;

    //}

    [WebMethod]
    public static List<Response> GetResponseExcel(long SurveyID, ArrayList RespondantID)
    {

        string apiKey = "cqrfdehmyse424xpqy4njg3e";
        string token = "paTBz61kMlD0mPWrsaHR3921EuYbHlBvqU0GDZQ.5nahBvB1GbdZpbh2WnOzXY4S-sChcHmcNe9jkWOZEXI1nL0-aEGKf9DsYQT0HdoSlLw=";
        var sm = new SurveyMonkeyApi(apiKey, token);

        List<long> respondent = new List<long>();
        for (int i = 0; i < RespondantID.Count; i++)
        {
            long resID;
            resID = Convert.ToInt64(RespondantID[i].ToString());
            respondent.Add(resID);
        }
        List<Response> Respondanse = new List<Response>();

        JavaScriptSerializer serializer = new JavaScriptSerializer();

        serializer.MaxJsonLength = Int32.MaxValue;



        StringWriter stringWrite = new StringWriter();
        HtmlTextWriter htmlWrite = new HtmlTextWriter(stringWrite);

        Respondanse = sm.GetResponses(SurveyID, respondent);
        return Respondanse;

    }
    [WebMethod]
    public static void GetResponseHTML(string ResponseHTML, string FileName)
    {
        responseHtml = "";
        reponseExcelFileName = "";
        responseHtml = ResponseHTML;
        reponseExcelFileName = FileName;
    }

    public void ExportToExcel()
    {
        string reposneHTml = responseHtml;
        Response.ContentType = "application/force-download";
        Response.AddHeader("content-disposition", "attachment; filename=" + reponseExcelFileName + ".xls");
        Response.Write("<html xmlns:x=\"urn:schemas-microsoft-com:office:excel\">");
        Response.Write("<head>");
        Response.Write("<META http-equiv=\"Content-Type\" content=\"text/html; charset=utf-     8\">");
        Response.Write("<!--[if gte mso 9]><xml>");
        Response.Write("<x:ExcelWorkbook>");
        Response.Write("<x:ExcelWorksheets>");
        Response.Write("<x:ExcelWorksheet>");
        Response.Write("<x:Name>Response Data</x:Name>");
        Response.Write("<x:WorksheetOptions>");
        Response.Write("<x:Print>");
        Response.Write("<x:ValidPrinterInfo/>");
        Response.Write("</x:Print>");
        Response.Write("</x:WorksheetOptions>");
        Response.Write("</x:ExcelWorksheet>");
        Response.Write("</x:ExcelWorksheets>");
        Response.Write("</x:ExcelWorkbook>");
        Response.Write("</xml>");
        Response.Write("<![endif]--> ");
        StringWriter tw = new StringWriter();
        HtmlTextWriter hw = new HtmlTextWriter(tw);

        Response.Write(reposneHTml.ToString());

        Response.Write("</head>");
        Response.End();
    }
    protected void btnMeetingCancelConfirm_onClick(object sender, EventArgs e)
    {
        ExportToExcel();
    }
}