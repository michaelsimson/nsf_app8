﻿Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Globalization
Imports System.Collections
Imports VRegistration


Partial Class CoachingDateCal
    Inherits System.Web.UI.Page

    Dim cmdText As String

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        'Session("EntryToken") = "VOLUNTEER"
        'Session("RoleId") = 1 '89
        'Session("LoginID") = 4240 '22214       
        'Session("LoggedIn") = "true"

        Try
            If Session("LoginID") Is Nothing Then
                Response.Redirect("~/maintest.aspx")
            End If

            If Page.IsPostBack = False Then

                LoadYear()
                FillEvent()
                loadPhase()


                If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                    hlnkMainMenu.Text = "Back to Parent Functions"
                    hlnkMainMenu.NavigateUrl = "~/UserFunctions.aspx"
                ElseIf Not Session("EntryToken").ToString.ToUpper() = "VOLUNTEER" Then
                    Response.Redirect("~/login.aspx?entry=v")
                ElseIf (Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2") Or (Session("RoleId").ToString() = "96") Or (Session("RoleId").ToString() = "89") Then


                    FillProductGroup()
                    FillProduct(ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlYear.SelectedValue, ddlProduct, ddlProductGroup)
                    BindCoachingDateCal()
                Else
                    Response.Redirect("~/maintest.aspx")
                End If
                ' maintain values in calendar control when postback happend
            Else


                Dim icnt As Integer = grdCoachBySchedule.Rows.Count
                If icnt <> 0 Then
                    Dim j As Integer
                    For j = 0 To 5
                        Dim btn As Button
                        btn = grdCoachBySchedule.Rows(j).Cells(0).FindControl("btnAdd")
                        If btn.Visible = True Then
                            ' To maintain viewstate of calendar controls while postback
                            Dim ctrl As Label = DirectCast(grdCoachBySchedule.Rows(j).Cells(2).FindControl("lblStartDate"), Label)
                            Dim txt As String = DirectCast(div1.FindControl("hfSD" & j), HiddenField).Value
                            If txt.Length > 0 Then
                                ctrl.Text = txt
                            End If

                            ctrl = DirectCast(grdCoachBySchedule.Rows(j).Cells(2).FindControl("lblEndDate"), Label)
                            txt = DirectCast(div1.FindControl("hfED" & j), HiddenField).Value
                            If txt.Length > 0 Then
                                ctrl.Text = txt
                            End If

                        End If
                    Next
                End If
                ' while editing fill calendar 
                If divUpdate.Visible = True Then
                    If hfUpdateStartDate.Value <> "" Then
                        lblUpdStartDate.Text = hfUpdateStartDate.Value
                    End If
                    If hfUpdateEndDate.Value <> "" Then
                        lblUpdEndDate.Text = hfUpdateEndDate.Value
                    End If
                End If

            End If
        Catch ex As Exception
            '            Response.Write(ex.ToString)
        End Try
    End Sub

    Private Sub LoadYear()
        Dim year As Integer = 0
        year = Convert.ToInt32(DateTime.Now.Year)

        ddlYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        ddlUpdYear.Items.Add(New ListItem((Convert.ToString(year) + "-" + Convert.ToString(year + 1).Substring(2, 2)), Convert.ToString(year)))

        For i As Integer = 0 To 4
            ddlYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))

            ddlUpdYear.Items.Add(New ListItem(Convert.ToString(year - (i + 1)) + "-" + Convert.ToString(year - (i)).Substring(2, 2), Convert.ToString(year - (i + 1))))
        Next
        ddlYear.SelectedValue = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, "select MAX(eventyear) from EventFees where EventId=13").ToString()
    End Sub
    ' Add events in dropdown for adding and updating
    Private Sub FillEvent()

        ddlEvent.Items.Add(New ListItem("Coaching", "13"))
        ddlUpdEvent.Items.Add(New ListItem("Coaching", "13"))

    End Sub

    ' Get product group details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProductGroup()

        Try

            Dim dsProductGrp As DataSet
            Dim Year As Integer
            Dim CurrentYear As String = String.Empty
            Year = ddlYear.SelectedValue
            CurrentYear = ddlYear.SelectedValue
            If Session("RoleId") = 89 Then
                cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and  ProductGroupID in( select distinct(ProductGroupId) from EventFees where EventId=" & ddlEvent.SelectedValue & " and EventYear=" + ddlYear.SelectedValue + " and Semester='" & ddlPhase.SelectedValue & "')  Order by ProductGroupId"
            Else
                cmdText = "select ProductGroupId,ProductGroupCode from ProductGroup where EventID=" + ddlEvent.SelectedItem.Value + " and ProductGroupID in (select distinct(ProductGroupId) from EventFees where EventId=" + ddlEvent.SelectedItem.Value + " and EventYear=" + ddlYear.SelectedValue + " and Semester='" & ddlPhase.SelectedValue & "')"
                ' cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where ProductGroupID in( select distinct(ProductGroupId) from EventFees where EventId=" & ddlEvent.SelectedValue & " and EventYear=" + CurrentYear + ") Order by ProductGroupId"

                'cmdText = " select distinct ProductGroupID,ProductGroupCode from volunteer where eventid =" & ddlEvent.SelectedValue & " and ProductId is not Null Order by ProductGroupId"
            End If

            ' "SELECT distinct A.[ProductGroupId], A.[ProductGroupCode] AS ProductGroupCode  FROM dbo.[Product] A Where A.EventID in(13) Order by A.ProductGroupCode")
            dsProductGrp = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ViewState("PrdGroup") = dsProductGrp.Tables(0)

            ddlProductGroup.DataSource = dsProductGrp.Tables(0)
            ddlProductGroup.DataBind()

            If ddlProductGroup.Items.Count > 1 Then
                ddlProductGroup.Enabled = True
                ddlProductGroup.Items.Insert(0, "Select Product Group")
            Else
                ddlProductGroup.SelectedIndex = 0
                ddlProductGroup.Enabled = False

            End If

            ddlUpdProductGroup.DataSource = dsProductGrp.Tables(0)
            ddlUpdProductGroup.DataBind()

            If ddlUpdProductGroup.Items.Count > 1 Then
                ddlUpdProductGroup.Enabled = True
                ddlUpdProductGroup.Items.Insert(0, "Select Product Group")
            Else
                ddlUpdProductGroup.SelectedIndex = 0
                ddlUpdProductGroup.Enabled = False

            End If

            ddlProductGroup_SelectedIndexChanged(ddlProductGroup, New EventArgs)

        Catch ex As Exception

        End Try
    End Sub

    ' Get product details from volunteer table
    ' for roleid=89 select by condition
    ' for roleid=1,2 and 96 select all product
    Private Sub FillProduct(EventID As String, ProductGroup As String, Year As String, ddlObject As DropDownList, ddlProductGroupObject As DropDownList)
        '  Dim Year As Integer
        Dim CurrentYear As String = String.Empty
        Year = ddlYear.SelectedValue
        CurrentYear = ddlYear.SelectedValue


        If Session("RoleId") = 89 Then
            cmdText = " select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & "  and ProductId in (select distinct(ProductId) from EventFees where  EventYear=" & Year & " and EventID=" & EventID & " and Semester='" & ddlPhase.SelectedValue & ""

            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and ProductGroupId=" & ProductGroup & " "
            End If
            cmdText = cmdText & " ) "

            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and ProductGroupId=" & ProductGroup & " "
            End If


            ' Added by Bindhu on Aug26_2016 to allow coach admin to have access with all products of assigned productgroup
            cmdText = cmdText & " union all select distinct p.productid, p.ProductCode from product p inner join Volunteer v on v.ProductGroupId =p.productgroupid where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId")
            cmdText = cmdText & "  and P.ProductId in (select distinct(ProductId) from EventFees where  EventYear=" & Year & " and EventID=" + EventID + " and Semester='" & ddlPhase.SelectedValue & "  "

            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and ProductGroupId=" & ProductGroup & " "
            End If
            cmdText = cmdText & " ) "

            cmdText = cmdText & " and v.ProductId is null  and v.ProductGroupId is not null  "

            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and P.ProductGroupId=" & ProductGroup & " "
            End If

        Else
            cmdText = "select ProductId,ProductCode from Product where ProductId in (select distinct(ProductId) from EventFees where  EventYear=" & Year & " and EventID=" & EventID & " and Semester='" & ddlPhase.SelectedValue & "'"
            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and ProductGroupId=" & ProductGroup & " "
            End If
            cmdText = cmdText & " ) "
            If ddlProductGroupObject.SelectedValue <> "Select Product Group" Then
                cmdText = cmdText & " and ProductGroupId=" & ProductGroup & " "
            End If
            cmdText = cmdText & " Order by ProductId"

            ' cmdText = " select distinct ProductID,ProductCode from volunteer where eventid =" & ddlEvent.SelectedValue & " and ProductGroupId =" & ddlProductGroup.SelectedValue & " and ProductId in (select distinct(ProductId) from EventFees where ProductGroupId=" + ddlProductGroup.SelectedItem.Value + " and EventYear=" + CurrentYear + " and EventID=" + ddlEvent.SelectedValue + ") Order by ProductId"
        End If
        '"SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlProductGroup.SelectedValue & " Order by A.ProductCode"
        Dim dsProduct As DataSet
        dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
        ddlObject.DataSource = dsProduct.Tables(0)
        ddlObject.DataBind()
        If ddlObject.Items.Count > 1 Then
            ddlObject.Enabled = True
            ddlObject.Items.Insert(0, "Select Product")
        Else
            ddlObject.SelectedIndex = 0
            ddlObject.Enabled = False

        End If

    End Sub

    'Fill product for each selected product group
    Protected Sub ddlProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlProductGroup.SelectedIndexChanged
        Try
            ddlProduct.Items.Clear()
            FillProduct(ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlYear.SelectedValue, ddlProduct, ddlProductGroup)

        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub

    'Master Table to fill Grid for insert
    Private Sub FillCoachDateCal()

        Dim dt As New DataTable
        dt.Columns.Add("Id", GetType(Integer))
        dt.Columns.Add("ScheduleType", GetType(String))
        dt.Columns.Add("StartDate", GetType(String))
        dt.Columns.Add("EndDate", GetType(String))
        dt.Columns.Add("Nature", GetType(String))
        dt.Columns.Add("Message", GetType(String))

        dt.Rows.Add(1, "Term", "", "", "", "")
        dt.Rows.Add(2, "Diwali", "", "", "", "")
        dt.Rows.Add(3, "Halloween", "", "", "", "")
        dt.Rows.Add(4, "Thanksgiving", "", "", "", "")
        dt.Rows.Add(5, "Christmas", "", "", "", "")
        dt.Rows.Add(6, "New Year", "", "", "", "")
        dt.Rows.Add(7, "Spring Break", "", "", "", "")



        ViewState("MasterTable") = dt
        grdCoachBySchedule.DataSource = dt
        grdCoachBySchedule.DataBind()

    End Sub

    ' Bind all columns of CoachingDateCal with gridview
    Private Sub BindCoachingDateCal()

        If Session("RoleId") = 89 Then
            '  cmdText = "select * from CoachingDateCal where MemberId= " & Session("LoginID") & " and eventyear=" & ddlYear.SelectedValue & " order by CDateCalId"
            cmdText = "select * from CoachingDateCal where eventyear=" & ddlYear.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' "
            If ddlProductGroup.SelectedValue <> "Select Product Group" Then
                cmdText &= " and ProductGroupID=" & ddlProductGroup.SelectedValue & ""
            End If
            If ddlProduct.SelectedValue <> "" Then
                cmdText &= " and ProductID=" & ddlProduct.SelectedValue & ""
            End If
            cmdText &= "order by CDateCalId"

        Else
            cmdText = "select * from CoachingDateCal where eventyear=" & ddlYear.SelectedValue & " and Semester='" & ddlPhase.SelectedValue & "' "
            If ddlProductGroup.SelectedValue <> "Select Product Group" Then
                cmdText &= " and ProductGroupID=" & ddlProductGroup.SelectedValue & ""
            End If
            If ddlProduct.SelectedValue <> "" Then
                cmdText &= " and ProductID=" & ddlProduct.SelectedValue & ""
            End If
            cmdText &= "order by CDateCalId"
        End If

        Dim ds As DataSet
        ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)
        grdCoachCalAll.DataSource = ds.Tables(0)
        grdCoachCalAll.DataBind()

    End Sub


    'Handle insert command
    Protected Sub grdCoachBySchedule_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCoachBySchedule.RowCommand

        Try

            If e.CommandName.ToString() = "Add" Then

                Dim strSType As String, strStartDate As String, strEndDate As String, strNature As String, strMsg As String
                Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
                Dim RowIndex As Integer = gvRow.RowIndex
                strSType = DirectCast(gvRow.Cells(1).FindControl("lblScheduleType"), Label).Text

                If (strSType.ToString.ToLower <> "term") Then
                    'chk 
                    If grdCoachBySchedule.Rows(0).FindControl("btnAdd").Visible = True Then
                        ShowMessage("Term is mandatory before saving other schedule", True)
                        grdCoachBySchedule.Rows(0).FindControl("btnAdd").Focus()
                        grdCoachBySchedule.Rows(0).BackColor = ColorTranslator.FromHtml("#EAEAEA")
                        Exit Sub
                    End If
                End If
                grdCoachBySchedule.Rows(0).BackColor = Nothing
                strStartDate = DirectCast(gvRow.Cells(2).FindControl("lblStartDate"), Label).Text
                strEndDate = DirectCast(gvRow.Cells(3).FindControl("lblEndDate"), Label).Text
                Dim ddlNat As DropDownList = DirectCast(gvRow.Cells(4).FindControl("ddlNature"), DropDownList)
                Dim txtMsg As TextBox = DirectCast(gvRow.Cells(5).FindControl("txtMessage"), TextBox)
                strNature = ddlNat.SelectedValue
                strMsg = txtMsg.Text

                If ddlYear.SelectedValue = String.Empty Then
                    ShowMessage("Select Year", True)
                ElseIf ddlProductGroup.SelectedValue = String.Empty Or ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                    ShowMessage("Select Product Group", True)
                ElseIf ddlProduct.SelectedValue = String.Empty Or ddlProduct.SelectedItem.Text = "Select Product" Then
                    ShowMessage("Select Product", True)
                ElseIf ddlPhase.SelectedItem.Text = "Select Semester" Then
                    ShowMessage("Select Semester", True)
                ElseIf strStartDate.ToString() = "__/__/____" Or strStartDate.ToString() = "" Then
                    ShowMessage("Select Start Date", True)
                ElseIf strEndDate.ToString() = "__/__/____" Or strEndDate.ToString() = "" Then
                    ShowMessage("Select End Date", True)

                Else
                    Dim dtStart As DateTime = DateTime.ParseExact(strStartDate, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                    Dim dtEnd As DateTime = DateTime.ParseExact(strEndDate, "MM/dd/yyyy", CultureInfo.InvariantCulture)
                    Dim dtTodayDate As DateTime = DateTime.Now.ToShortDateString()
                    If (dtStart < dtTodayDate) Then

                        ShowMessage("Start Date should be greater than Today Date", True)
                        Exit Sub
                    ElseIf dtEnd < dtStart Then
                        ShowMessage("End Date should be greater than Start Date", True)
                        Exit Sub
                    End If

                    Dim cmdDuplicateText As String
                    cmdDuplicateText = "SELECT count(*) from CoachingDateCal WHERE EventYear =" & ddlYear.SelectedValue & " and EventId=" & ddlUpdEvent.SelectedValue & " and EventCode='" & ddlEvent.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & "  and ProductId=" & ddlProduct.SelectedValue & "  and Semester='" & ddlPhase.SelectedValue & "' and  ScheduleType= '" & strSType & "' "

                    Dim iCount As Integer = 0
                    iCount = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdDuplicateText)

                    Dim strQuery As String = "insert into CoachingDateCal ( EventYear, EventId, EventCode,ProductGroupId, ProductGroupCode, ProductId, ProductCode, Semester,ScheduleType, StartDate, EndDate, Nature, Message,CreatedDate, CreatedBy) values(@EventYear,@EventId,@EventCode,@ProductGroupId,@ProductGroupCode,@ProductId, @ProductCode, @Semester,@ScheduleType,@StartDate,@EndDate,@Nature,@Message,@CreatedDate,@CreatedBy)"

                    Dim cmd As New SqlCommand(strQuery)
                    cmd.Parameters.AddWithValue("@EventYear", ddlYear.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventId", ddlEvent.SelectedValue)
                    cmd.Parameters.AddWithValue("@EventCode", ddlEvent.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@ProductGroupId", ddlProductGroup.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductGroupCode", ddlProductGroup.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@ProductId", ddlProduct.SelectedValue)
                    cmd.Parameters.AddWithValue("@ProductCode", ddlProduct.SelectedItem.Text)
                    cmd.Parameters.AddWithValue("@Semester", ddlPhase.SelectedValue)
                    cmd.Parameters.AddWithValue("@ScheduleType", strSType)
                    cmd.Parameters.AddWithValue("@StartDate", strStartDate)
                    cmd.Parameters.AddWithValue("@EndDate", strEndDate)
                    cmd.Parameters.AddWithValue("@Nature", strNature)
                    cmd.Parameters.AddWithValue("@Message", strMsg)
                    cmd.Parameters.AddWithValue("@CreatedDate", DateTime.Now.ToShortDateString())
                    cmd.Parameters.AddWithValue("@CreatedBy", Session("LoginId"))


                    'Dim cmdText As String
                    'cmdText = "insert into CoachingDateCal ( EventYear, EventId, EventCode,ProductGroupId, ProductGroupCode, ProductId, ProductCode, Semester, "
                    'cmdText = cmdText & " ScheduleType, StartDate, EndDate, Nature, Message, "
                    'cmdText = cmdText & " CreatedDate, CreatedBy) values "
                    'cmdText = cmdText & "( " & ddlYear.SelectedValue & " , " & ddlEvent.SelectedValue & ", '" & ddlEvent.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "', " & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "' , " & ddlPhase.SelectedValue
                    'cmdText = cmdText & " , '" & strSType & "', '" & strStartDate & "','" & strEndDate & "','" & strNature & "','" & strMsg & "',"
                    'cmdText = cmdText & " GETDATE() , " & Session("LoginId") & ")"
                    If iCount = 0 Then

                        Dim objNSF As NSFDBHelper = New NSFDBHelper()
                        objNSF.InsertUpdateData(cmd)
                        ' SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                        FillCoachingDateCalByCondition()
                        Reset()
                        ShowMessage("Inserted Successfully", False)
                        BindCoachingDateCal()
                    Else

                        ShowMessage("Duplicate exists", True)

                    End If
                End If
            End If

        Catch ex As Exception
            '  Response.Write(ex.ToString)
        End Try

    End Sub

    'Error or Success message function
    Private Sub ShowMessage(msg As String, bIsError As Boolean)

        lblMsg.Text = msg
        lblMsg.ForeColor = Color.Blue
        If bIsError = True Then
            lblMsg.ForeColor = Color.Red
        End If

    End Sub

    ' Row databound to assign calendar javascript function for imagebutton
    Protected Sub grdCoachBySchedule_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCoachBySchedule.RowDataBound
        Try

            If e.Row.RowType = DataControlRowType.DataRow Then

                Dim lblDate As Control = e.Row.FindControl("lblStartDate")
                Dim imgBtn As ImageButton = e.Row.FindControl("imgStartDate")
                Dim iRowIndex As Integer = e.Row.RowIndex
                imgBtn.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblDate.ClientID & "','" & div1.FindControl("hfSD" & iRowIndex).ClientID & "'); return false;")

                lblDate = e.Row.FindControl("lblEndDate")
                imgBtn = e.Row.FindControl("imgEndDate")
                imgBtn.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblDate.ClientID & "','" & div1.FindControl("hfED" & iRowIndex).ClientID & "'); return false;")

            End If

        Catch ex As Exception
            'Response.Write("Error :" & ex.ToString())
        End Try
    End Sub



    'Get Details from db depends on the selected condition
    ' and assign data in gridview also hide controls of already inserted row
    Private Sub FillCoachingDateCalByCondition()

        Try

            If ddlProductGroup.SelectedValue <> "Select Product Group" And ddlProduct.SelectedValue <> "Select Product" And ddlPhase.SelectedValue <> "Select Semester" Then

                cmdText = "select * from CoachingDateCal WHERE EventYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Semester ='" & ddlPhase.SelectedValue & "'"

                Dim ds As DataSet
                ds = SqlHelper.ExecuteDataset(Application("connectionstring").ToString(), CommandType.Text, cmdText)

                If ds.Tables(0).Rows.Count > 0 Then

                    ViewState("tblCoachingDateCal") = ds.Tables(0)

                    Dim i As Integer, gvRow As GridViewRow, lblSD As Label, lblED As Label, lblScheduleType As Label, ddlNature As DropDownList, txtMsg As TextBox

                    For i = 0 To grdCoachBySchedule.Rows.Count - 1

                        gvRow = grdCoachBySchedule.Rows(i)

                        lblScheduleType = gvRow.FindControl("lblScheduleType")
                        Dim dt As DataTable = ViewState("tblCoachingDateCal")
                        Dim drs() As DataRow = dt.Select("ScheduleType ='" & lblScheduleType.Text & "'")

                        If drs.Length > 0 Then

                            Try
                                lblSD = gvRow.FindControl("lblStartDate")
                                lblED = gvRow.FindControl("lblEndDate")
                                ddlNature = gvRow.FindControl("ddlNature")
                                txtMsg = gvRow.FindControl("txtMessage")

                                lblSD.Text = String.Format("{0:MM/dd/yyyy}", drs(0)("StartDate"))

                                lblED.Text = String.Format("{0:MM/dd/yyyy}", drs(0)("EndDate"))
                                ddlNature.SelectedValue = drs(0)("Nature")
                                txtMsg.Text = drs(0)("Message")

                                gvRow.FindControl("btnAdd").Visible = False
                                DirectCast(gvRow.FindControl("imgStartDate"), ImageButton).Visible = False
                                DirectCast(gvRow.FindControl("imgEndDate"), ImageButton).Visible = False
                                ddlNature.Enabled = False
                                txtMsg.Enabled = False

                            Catch ex As Exception
                                '  Response.Write("Err :" & ex.ToString())
                            End Try
                        End If
                    Next

                End If
            End If
        Catch ex As Exception
            '   Response.Write("error:" & ex.ToString())
        End Try

    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlYear.SelectedIndexChanged

        grdCoachBySchedule.Visible = False
        ' FillCoachDateCal()
        FillProductGroup()
        FillProduct(ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlYear.SelectedValue, ddlProduct, ddlProductGroup)
        ddlProductGroup_SelectedIndexChanged(ddlProductGroup, e)
    End Sub





    Protected Sub grdCoachCalAll_RowCommand(sender As Object, e As GridViewCommandEventArgs) Handles grdCoachCalAll.RowCommand

        If e.CommandName = "Modify" Then



            lblMsg.Text = ""
            divUpdate.Visible = True

            Dim gvRow As GridViewRow = DirectCast(DirectCast(e.CommandSource, Button).NamingContainer, GridViewRow)
            gvRow.BackColor = ColorTranslator.FromHtml("#EAEAEA")


            Dim RowIndex As Integer = gvRow.RowIndex

            Dim cDateCalId As Integer = gvRow.Cells(1).Text
            ddlUpdYear.SelectedValue = gvRow.Cells(2).Text
            ddlUpdEvent.SelectedValue = DirectCast(gvRow.FindControl("lblEventId"), Label).Text
            Dim productGroupId As Integer = DirectCast(gvRow.FindControl("lblProductGroupId"), Label).Text
            Dim productId As Integer = DirectCast(gvRow.FindControl("lblProductId"), Label).Text

            Dim Semester As String = gvRow.Cells(6).Text
            ddlUpdPhase.SelectedValue = Semester
            Dim strSType As String = DirectCast(gvRow.FindControl("lblScheduleType"), Label).Text
            h_UpdScheduleType.Value = strSType
            ddlUpdScheduleType.SelectedValue = strSType
            Dim strStartDate, strEndDate, strNature, strMsg As String

            strStartDate = DirectCast(gvRow.FindControl("lblStartDate"), Label).Text
            strEndDate = DirectCast(gvRow.FindControl("lblEndDate"), Label).Text
            strNature = DirectCast(gvRow.FindControl("lblNature"), Label).Text
            strMsg = DirectCast(gvRow.FindControl("lblMessage"), Label).Text

            ddlUpdProductGroup.SelectedIndex = ddlUpdProductGroup.Items.IndexOf(ddlUpdProductGroup.Items.FindByValue(productGroupId))

            '  ddlUpdProductGroup_SelectedIndexChanged(ddlUpdProductGroup, New EventArgs)

            FillProduct(ddlUpdEvent.SelectedValue, ddlUpdProductGroup.SelectedValue, ddlUpdYear.SelectedValue, ddlUpdProduct, ddlUpdProductGroup)

            ddlUpdProduct.SelectedIndex = ddlUpdProduct.Items.IndexOf(ddlUpdProduct.Items.FindByValue(productId))

            lblCDateCalId.Text = cDateCalId.ToString


            lblUpdStartDate.Text = String.Format("{0:MM/dd/yyyy}", strStartDate)
            lblUpdEndDate.Text = String.Format("{0:MM/dd/yyyy}", strEndDate)

            hfUpdateStartDate.Value = String.Format("{0:MM/dd/yyyy}", strStartDate)
            hfUpdateEndDate.Value = String.Format("{0:MM/dd/yyyy}", strEndDate)

            ddlUpdNature.SelectedValue = strNature
            txtUpdMessage.Text = strMsg

            imgUpdStartDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblUpdStartDate.ClientID & "','" & hfUpdateStartDate.ClientID & "'); return false;")

            imgUpdEndDate.Attributes.Add("onclick", "javascript:showCalendarControl('" & lblUpdEndDate.ClientID & "','" & hfUpdateEndDate.ClientID & "'); return false;")

        End If
    End Sub

    Protected Sub ddlUpdProductGroup_SelectedIndexChanged(sender As Object, e As EventArgs) Handles ddlUpdProductGroup.SelectedIndexChanged
        Dim dsProduct As DataSet
        If ddlUpdProductGroup.SelectedValue <> "Select Product Group" Then

            If Session("RoleId") = 89 Then
                cmdText = " select distinct ProductID,ProductCode from volunteer where Memberid=" & Session("LoginID") & " and RoleId=" & Session("RoleId") & " and ProductGroupId=" & ddlUpdProductGroup.SelectedValue & " and ProductId is not Null  Order by ProductId"
            Else
                cmdText = " select distinct ProductID,ProductCode from volunteer where eventid =13 and ProductGroupId =" & ddlUpdProductGroup.SelectedValue & " and ProductId is not Null Order by ProductId"
            End If
            '"SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlProductGroup.SelectedValue & " Order by A.ProductCode"
            '  Dim dsProduct As DataSet
            dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, cmdText)
            ddlUpdProduct.DataSource = dsProduct.Tables(0)
            ddlUpdProduct.DataBind()
            If ddlUpdProduct.Items.Count > 1 Then
                ddlUpdProduct.Enabled = True
                ddlUpdProduct.Items.Insert(0, "Select Product")
            Else
                ddlUpdProduct.SelectedIndex = 0
                ddlUpdProduct.Enabled = False

            End If
        End If

        'dsProduct = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT distinct A.[ProductId] as ProductId, A.[ProductCode] AS ProductCode FROM dbo.[Product] A Where A.EventID in(13) and A.ProductGroupId= " & ddlUpdProductGroup.SelectedValue & " Order by A.[ProductId]")


        'ddlUpdProduct.DataSource = dsProduct.Tables(0)
        'ddlUpdProduct.DataBind()

        'If ddlUpdProduct.Items.Count > 1 Then
        '    ddlUpdProduct.Enabled = True
        '    ddlUpdProduct.Items.Insert(0, "Select Product")
        'Else
        '    ddlUpdProduct.SelectedIndex = 0
        '    ddlUpdProduct.Enabled = False

        'End If

    End Sub


    Protected Sub btnUpdate_Click(sender As Object, e As EventArgs) Handles btnUpdate.Click
        Try
            ' Validate duplicate entry
            Dim cmdText As String
            cmdText = "SELECT count(*) from CoachingDateCal WHERE EventYear =" & ddlUpdYear.SelectedValue & " and EventId=" & ddlUpdEvent.SelectedValue & " and EventCode='" & ddlUpdEvent.SelectedItem.Text & "' and ProductGroupId=" & ddlUpdProductGroup.SelectedValue & " and ProductGroupCode='" & ddlUpdProductGroup.SelectedItem.Text & "' and ProductId=" & ddlUpdProduct.SelectedValue & " and ProductCode='" & ddlUpdProduct.SelectedItem.Text & "' and Semester='" & ddlUpdPhase.SelectedValue & "' and ScheduleType='" & ddlUpdScheduleType.SelectedItem.Text & "' "
            '  cmdText = cmdText & " and ScheduleType='" & ddlUpdScheduleType.SelectedValue & "' and memberid =" & Session("LoginId") & " and cDateCalId <>" & lblCDateCalId.Text

            cmdText = cmdText & " and ScheduleType='" & ddlUpdScheduleType.SelectedValue & "' and cDateCalId <>" & lblCDateCalId.Text

            If ddlUpdProductGroup.SelectedValue.ToString() = "" Or ddlUpdProductGroup.SelectedItem.Text = "Select Product Group" Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlUpdProduct.SelectedValue.ToString = String.Empty Or ddlUpdProduct.SelectedItem.Text = "Select Product" Then
                ShowMessage("Select Product", True)
            ElseIf lblUpdStartDate.Text.ToString = String.Empty Or lblUpdStartDate.Text.ToString() = "__/__/____" Then
                ShowMessage("Pick Start Date", True)
            ElseIf lblUpdEndDate.Text.ToString = String.Empty Or lblUpdEndDate.Text.ToString() = "__/__/____" Then
                ShowMessage("Pick End Date", True)
            ElseIf SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) > 0 Then
                ShowMessage("Duplicate Entry ", True)
            Else
                Dim dtStart As DateTime = Convert.ToDateTime(lblUpdStartDate.Text).ToString("MM/dd/yyyy")
                Dim dtEnd As DateTime = Convert.ToDateTime(lblUpdEndDate.Text).ToString("MM/dd/yyyy")

                If dtEnd < dtStart Then
                    ShowMessage("End Date should be greater than Start Date", True)
                    Exit Sub
                End If
                'Response.Write("hidden :" + h_UpdScheduleType.Value.ToString.ToLower())
                If h_UpdScheduleType.Value.ToString.ToLower() = "term" And ddlUpdScheduleType.SelectedValue.ToString.ToLower() = "term" Then



                ElseIf h_UpdScheduleType.Value.ToString.ToLower() = "term" And ddlUpdScheduleType.SelectedValue.ToString().ToLower() <> "term" Then

                    cmdText = "SELECT count(*) from CoachingDateCal WHERE EventYear =" & ddlUpdYear.SelectedValue & " and EventId=" & ddlUpdEvent.SelectedValue & " and EventCode='" & ddlUpdEvent.SelectedItem.Text & "' and ProductGroupId=" & ddlUpdProductGroup.SelectedValue & " and ProductGroupCode='" & ddlUpdProductGroup.SelectedItem.Text & "' and ProductId=" & ddlUpdProduct.SelectedValue & " and ProductCode='" & ddlUpdProduct.SelectedItem.Text & "' and Semester='" & ddlUpdPhase.SelectedValue & "'"
                    cmdText = cmdText & " and ScheduleType='Term' and memberid =" & Session("LoginId") & " and cDateCalId <>" & lblCDateCalId.Text

                    If SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, cmdText) = 0 Then

                        ShowMessage("Term is mandatory before saving other schedule", True)
                        Exit Sub

                    End If


                End If


                cmdText = "Update CoachingDateCal SET EventYear =" & ddlUpdYear.SelectedValue & ",EventId=" & ddlUpdEvent.SelectedValue & ", EventCode='" & ddlUpdEvent.SelectedItem.Text & "',ProductGroupId=" & ddlUpdProductGroup.SelectedValue & ", ProductGroupCode='" & ddlUpdProductGroup.SelectedItem.Text & "', ProductId=" & ddlUpdProduct.SelectedValue & ", ProductCode='" & ddlUpdProduct.SelectedItem.Text & "', Semester='" & ddlUpdPhase.SelectedValue & "', "
                cmdText = cmdText & " ScheduleType='" & ddlUpdScheduleType.SelectedValue & "', StartDate='" & lblUpdStartDate.Text & "', EndDate='" & lblUpdEndDate.Text & "', Nature='" & ddlUpdNature.SelectedValue & "', Message='" & txtUpdMessage.Text & "', "
                cmdText = cmdText & " ModifiedBy=" & Session("LoginId") & ", ModifiedDate=GETDATE() WHERE cDateCalId= " & lblCDateCalId.Text
                SqlHelper.ExecuteNonQuery(Application("connectionstring").ToString(), CommandType.Text, cmdText)
                Reset()
                BindCoachingDateCal()
                ShowMessage("Updated Successfully", False)

            End If

        Catch ex As Exception
            '  Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub btnCancel_Click(sender As Object, e As EventArgs) Handles btnCancel.Click
        Reset()
    End Sub

    Private Sub Reset()
        divUpdate.Visible = False
        lblMsg.Text = ""


    End Sub


    Protected Sub grdCoachCalAll_RowDataBound(sender As Object, e As GridViewRowEventArgs) Handles grdCoachCalAll.RowDataBound
        If e.Row.RowType = DataControlRowType.DataRow Then
            e.Row.BackColor = Nothing

        End If
    End Sub

    Protected Sub btnContinue_Click(sender As Object, e As EventArgs) Handles btnContinue.Click
        loadCoachDate()
    End Sub
    Public Sub loadCoachDate()
        Try
            Reset()
            grdCoachBySchedule.Visible = False

            If ddlYear.SelectedValue = String.Empty Then
                ShowMessage("Select Year", True)
            ElseIf ddlProductGroup.Items.Count = 0 Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProductGroup.SelectedItem.Text = "Select Product Group" Then
                ShowMessage("Select Product Group", True)
            ElseIf ddlProduct.Items.Count = 0 Then
                ShowMessage("Select Product", True)
            ElseIf ddlProduct.SelectedItem.Text = "Select Product" Then
                ShowMessage("Select Product", True)
            ElseIf ddlPhase.SelectedItem.Text = "Select Semester" Then
                ShowMessage("Select Semester", True)
            Else
                BindCoachingDateCal()
                grdCoachBySchedule.Visible = True

                FillCoachDateCal()
                FillCoachingDateCalByCondition()

            End If

        Catch ex As Exception
            '  Response.Write("Err :" & ex.ToString)
        End Try
    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(sender As Object, e As EventArgs)
        FillProductGroup()
        FillProduct(ddlEvent.SelectedValue, ddlProductGroup.SelectedValue, ddlYear.SelectedValue, ddlProduct, ddlProductGroup)
    End Sub
    Private Sub loadPhase()
        Dim arrPhase As ArrayList = New ArrayList()
        Dim objCommon As Common = New Common()
        arrPhase = objCommon.GetSemesters()
        For i As Integer = 0 To 2
            ddlPhase.Items.Add(New ListItem(arrPhase(i).ToString(), arrPhase(i).ToString()))
        Next

        ddlPhase.SelectedValue = objCommon.GetDefaultSemester(ddlYear.SelectedValue)
    End Sub

    Protected Sub ddlProduct_SelectedIndexChanged(sender As Object, e As EventArgs)
        Try


            Dim PgCode As String = ""
            Dim Cmdtext As String = " Select distinct productgroupid from Product where ProductId=" & ddlProduct.SelectedValue & ""
            PgCode = SqlHelper.ExecuteScalar(Application("connectionstring").ToString(), CommandType.Text, Cmdtext)
            ddlProductGroup.SelectedValue = PgCode
            loadCoachDate()
        Catch ex As Exception

        End Try
    End Sub
End Class
