﻿<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="VolunteerDisclosureForm.aspx.cs" Inherits="Default4" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
           <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>

           <span style="margin-left:400px;">Volunteer Name:	<b>
<asp:Label ID="VolNamelbl" runat="server" align="right" Text="Label"></asp:Label></b></span>

    <div>
       
        <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
            runat="server">

            <strong>Volunteer Disclosure Form

            </strong>
            
        </div>
        <br />
        <div align="center">
        <asp:Label ID="lberror" Font-Bold="true" style="color:red;" runat="server"  Text=""></asp:Label>
       <asp:Label ID="lbsuccess" Font-Bold="true" style="color:green;" runat="server"  Text=""></asp:Label>
        </div>
        <br />
        <br />
        <div style="font-size: 14px; font-weight: bold; font-family: Calibri; ">
        <div>
            <asp:Label ID="Label1" runat="server" Text="Q1: Education - Please list the schools you attended and any degrees you received along with graduation years."></asp:Label>
            <br />
           &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxEducation" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label2" runat="server" Text="Q2: Work Experience - Please list the employers with name, period worked and one sentence on the nature of work."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxWorkExp" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label3" runat="server" Text="Q3: Volunteer Work - Please list the charities with name, period served and the nature of work in each."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxVolWork" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label4" runat="server" Text="Q4: Agent/Broker: Are you or a family member an agent or broker in insurance, real estate, investment or any other businesses?"></asp:Label>
            <br />
             <div style="text-align: left">
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:RadioButton ID="RadioButton1" GroupName="agent" name="agent" Text="Yes" runat="server" OnCheckedChanged="RadioButton1_CheckedChanged" />
             &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                 <asp:RadioButton ID="RadioButton2" GroupName="agent" name="agent" Text="No" runat="server" OnCheckedChanged="RadioButton2_CheckedChanged" />
                 </div>
        </div>
         <div>
            <asp:Label ID="Label5" runat="server" Text="Q5: Own Business Interest:  If you or a family member own a business either in full or in part, please list each along with a brief discription."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxOwnBusiness" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label6" runat="server" Text="Q6: Potential Conflict - Business: If you or a family member have a business interest in education including tutoring, please explain the nature of business, customers and their demographics."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxConflictBus" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label7" runat="server" Text="Q7: Potential Conflict - NGO: If you or a family member have an interest in one or more NGOs in education, please explain the nature of business, customers and their demographics."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxConflictNGO" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
         <div>
            <asp:Label ID="Label8" runat="server" Text="Q8: Venue - Do you have a business or personal relationship with the venue for contests or workshops?  Please explain…."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxVenue" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
          <div>
            <asp:Label ID="Label9" runat="server" Text="Q9: Potential Conflict - Solution: If there is a potential confict, explain how you plan to avoid the conflict of interest or even the appearance of a conflict to a NSF stakeholder."></asp:Label>
            <br />
            &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:TextBox ID="QTextBoxConflictSolution" Width="800" Height="100" TextMode="MultiLine" runat="server"></asp:TextBox>
        </div>
        <div align="center">
            <asp:Button ID="Button1" runat="server" Text="Submit" OnClick="Button1_Click" />

        </div>
            </div>
    </div>

</asp:Content>
