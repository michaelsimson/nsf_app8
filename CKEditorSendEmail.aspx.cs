﻿using Microsoft.ApplicationBlocks.Data;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Net.Mail;
using System.Text;
using System.Text.RegularExpressions;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Windows.Forms;

public partial class CKEditorSendEmail : System.Web.UI.Page
{
    Boolean validEmailornot;
    String fileExt = "";
    string ServerPath = "";
    protected void Page_Load(object sender, EventArgs e)
    {

        string ddChapterstr = string.Empty;

        ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter where state='OH' Order by State, ChapterCode";

        DataSet dschapterselected = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, ddChapterstr);
        if (dschapterselected.Tables[0].Rows.Count > 0)
        {
            ddlChapter.Enabled = true;
            ddlChapter.DataSource = dschapterselected;
            ddlChapter.DataTextField = "chaptercode";
            ddlChapter.DataValueField = "ChapterID";
            ddlChapter.DataBind();
            if (dschapterselected.Tables[0].Rows.Count == 1)
            {
                ddlChapter.SelectedIndex = 0;
                ddlChapter.Enabled = false;
            }
        }
     }
    protected void ChooseFileUpload_Click(object sender, EventArgs e)
    {
        ServerPath = Server.MapPath("~/UploadFiles/");

        String FileName = FileUpLoad1.FileName;       //txtEmailBody.Text = "<b>Hai</b>";
        
        fileExt = System.IO.Path.GetExtension(FileUpLoad1.FileName);
        FileUpLoad1.PostedFile.SaveAs(ServerPath+"tempEmail" + fileExt);

        string content = "";
        string line = "";
        StreamReader reader = new StreamReader(ServerPath + "tempEmail" + fileExt);

        while ((line = reader.ReadLine()) != null)
        {

            content += line;
          
        }

        txtEmailBody.Text = content;
       
    }
    protected void SendEmail(string sSubject, string sBody,string sMailFrom, string sMailTo)
    {
        lblError.Text = "";
        string MailStatus;
        string sFrom;
        sFrom = sMailFrom;
        //sFrom = "nsfcontests@gmail.com";
        MailMessage mail = new MailMessage(sFrom, sMailTo, sSubject, sBody);
        mail.CC.Add(ccemaillist.Text);
        SmtpClient client = new SmtpClient();
        mail.IsBodyHtml = true;
        Boolean ok;
        ok = true;
        try
        {
          
            client.Send(mail);
            MailStatus = "Sent";

            lblError.Visible = true;
            lblError.ForeColor = Color.Blue;
            lblError.Text = "Mail sended successfully";
           // System.IO.File.Delete(ServerPath + "tempEmail" + fileExt);
        }
        catch (Exception ex)
        {
            lblError.Visible = true;
            lblError.ForeColor = Color.Red;
            lblError.Text = "Error: Mail not sended ";
        }
    }
    protected void Button1_Click(object sender, EventArgs e)
    {
        if (tbMailFrom.Text.ToString().Equals(""))
        {
            lblError.Text = "Enter From Email Address";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;

        }
        else if (tbMailTo.Text.ToString().Equals(""))
        {
            lblError.Text = "Enter To Email Address";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;
        }
        else if (tbEmailSubject.Text.ToString().Equals(""))
        {
            lblError.Text = "Enter Subject of the Email";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;
        }
         else
        {

            lblError.Text = "";
            validEmailornot = ValidateEmail(tbMailFrom.Text);
            if(validEmailornot==true)
            SendEmail(tbEmailSubject.Text, txtEmailBody.Text, tbMailFrom.Text, tbMailTo.Text);
       
        }
    }
    private Boolean ValidateEmail(string emailstring)
    {
        string email = emailstring;
        Regex regex = new Regex(@"^([\w\.\-]+)@([\w\-]+)((\.(\w){2,3})+)$");
        Match match = regex.Match(email);
        if (match.Success)
        {
            lblError.Text = "";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;
            return true;
        }
        else
        {
            lblError.Text = email + " is Invalid Email Address";
            lblError.ForeColor = Color.Red;
            lblError.Visible = true;
            return false;
        }
    
    }
    protected void loadTomaillist_Click(object sender, EventArgs e)
    {

        tbMailTo.Text = "";
        string chapterID=ddlChapter.SelectedValue;

        string strqry = "";
        strqry = "select email from indspouse where chapterid=" + chapterID + " and len(email)>0";
        DataSet tomaillist = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strqry);

        StringBuilder sbEmailList = new StringBuilder();
        if (tomaillist.Tables[0].Rows.Count > 0)
        {
            int ctr = 0;
            for (ctr = 0; ctr <= tomaillist.Tables[0].Rows.Count - 1; ctr++)
            {
                sbEmailList.Append(tomaillist.Tables[0].Rows[ctr]["email"].ToString());
                if (ctr <= tomaillist.Tables[0].Rows.Count - 2)
                {
                    sbEmailList.Append(",");
                }
            }
            Session["emaillist"] = sbEmailList.ToString();
            tbMailTo.Text=sbEmailList.ToString();
            tbMailTo.Enabled=false;

        }
    }

    protected void cleartolist_Click(object sender, EventArgs e)
    {
        tbMailTo.Text = "";
        tbMailTo.Enabled = true;
    }
}