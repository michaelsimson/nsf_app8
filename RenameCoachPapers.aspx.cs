﻿using Microsoft.ApplicationBlocks.Data;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Globalization;
using System.IO;
using System.Net;
using System.Net.Mail;
using System.Security.AccessControl;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;
public partial class RenameCoachPapers :  System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        RenameFile();
    }
    public void RenameFile()
    {
        string Cmdtext = "select TestFileName, coachpaperId from CoachPapers where Sections =0";
        string path = Server.MapPath("~");

        DataSet ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, Cmdtext);
        if (null != ds && ds.Tables.Count > 0)
        {
            if (ds.Tables[0].Rows.Count > 0)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    string exFile = dr["TestFileName"].ToString();
                    string fileName = dr["TestFileName"].ToString();
                    string extension = fileName.Split('.')[1];
                    string DocType = fileName.Split('_')[9];
                    string section = fileName.Substring(0, fileName.LastIndexOf('_'));
                    string fileName1 = section.Substring(0, section.LastIndexOf('_'));
                    section = section.Substring(section.LastIndexOf('_'), 5);
                    section = section.Replace('0', '1');
                    fileName = fileName1 + "" + section + "_" + DocType;

                    string Fromfile = path + "\\CoachPapers8\\" + exFile + "";
                    string Tofile = path + "\\CoachPapers8\\" + fileName + "";
                    File.Move(Fromfile, Tofile);

                    Cmdtext = " Update CoachPapers set Section = 1, testFileName ='" + fileName + "' where CoachPaperId=" + dr["coachpaperId"].ToString() + "";

                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
                }
            }
        }

    }
}