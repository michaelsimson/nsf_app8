<%@ Page Language="vb" AutoEventWireup="false"  title="Credit Card Charges Reconciliation Help"  %><%--Inherits="CCReconcileHelp" CodeFile="CCReconcileHelp.aspx.vb"--%>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">
<HTML>
	<HEAD>
		<title>CCReconcile Help</title>
		<LINK href="Styles.css" rel="stylesheet">

	</HEAD>
	<body  leftmargin="0" rightmargin="0" topmargin="0" bottommargin="0" marginwidth="0" marginheight="0">
		<form id="Form1" method="post" runat="server">
		<table runat="server" width="100%">
		    <tr><td runat="server" class="ItemLabelCenter" style=" font-size:medium;">Matched Status Flag is set in NFG_Transactions as follows:</td></tr>
		    <tr><td></td></tr>
		    <tr runat="server" id="TrPhase"><td class="MidFont"><b>Y:</b> When Sum of Fees in component tables equals to the Fee in NFG_Transactions table.</td></tr>
		    <tr runat="server" id="TrSession"><td class="MidFont"><b>N:</b> When Sum of Fees in component tables does not equal to the Fee in NFG_Transactions .</td></tr>
		    <tr runat="server" id="TrPref"><td class="MidFont"><b>Null:</b> When records don�t exist in the component table but exist in NFG_Transactions table.</td></tr>
		    		    <tr runat="server" id="Tr1"><td class="MidFont"><b>O:</b> For all events[1,4,5,6,9,10,11,12,13,14,18,19,20] other than regional contests and workshops.</td></tr>
		    <tr runat="server" id="Tr2"><td class="MidFont"><b>R:</b> For all Refund Transaction Records for all Events.</td></tr>
		    <tr runat="server" id="Tr3"><td class="MidFont"> </td></tr>
		    <tr runat="server" id="Tr4"><td class="MidFont"><b>Note:</b> Y, N, Null only apply to regional contests and workshops and R applies to all Events.</td></tr>
		    <tr runat="server" id="Tr5"><td class="MidFont"><b>Component Tables:</b> Contestant [EventID =2] , DuplicateContestantReg[EventID=2],Registration[EventID= 3].</td></tr>

		</table>
		</form>
	</body>
</HTML>

 

 
 
 