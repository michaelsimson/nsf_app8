﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;

public partial class TeamPlanning_TeamPlanning : System.Web.UI.Page
{
    string RoleID = "";

    protected void Page_Load(object sender, EventArgs e)
    {
        

        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        else
        {
            RoleID = Session["RoleID"].ToString();
            if (!IsPostBack)
            {

                //BindBeeBookSchedule();

                if (Session["LoginTPChapterID"].ToString() != "0")
                {
                    Events();
                    Yearscount();
                    ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                    ChapterDrop(ddchapter);
                    if (Session["LoginTPChapterID"].ToString() == "1")
                    {
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddEvent.SelectedValue = "1";
                        ddEvent.Enabled = false;
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                        Team();
                    }
                    else if (Session["LoginTPChapterID"].ToString() == "109")
                    {
                        ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                        ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                        ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                    }
                    else if (Session["LoginTPChapterID"].ToString() != "1" && Session["LoginTPChapterID"].ToString() != "109")
                    {
                        ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                        ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                        ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));

                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        ddchapter.SelectedValue = Session["LoginTPChapterID"].ToString();
                        ddchapter.Enabled = false;
                    }


                }
                else
                {
                    if (Convert.ToInt32(RoleID) <= 5)
                    {
                        Events();
                        Yearscount();
                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                        ChapterDrop(ddchapter);

                        ddchapter.Enabled = true;
                        ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
                        ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
                        ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
                        ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
                        ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
                        ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
                        ddEvent.Items.Insert(0, new ListItem("Finals", "1"));
                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        Yearscount();
                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);
                        ChapterDrop(ddchapter);
                    }
                    else
                    {
                        Yearscount();
                        string MemberID = Session["LoginID"].ToString();
                        string cmdText = "";
                        DataSet ds = new DataSet();
                        cmdText = "select distinct E.EventID,E.EventCode from Volunteer V inner join Event E on (V.EventID=E.EventID) where MemberID=" + MemberID + " and RoleID=" + Convert.ToInt32(RoleID) + " order by E.EventID Asc";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddEvent.DataSource = ds;
                        ddEvent.DataTextField = "EventCode";
                        ddEvent.DataValueField = "EventID";
                        ddEvent.DataBind();

                        ddEvent.Items.Insert(0, new ListItem("Select Event", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddEvent.Enabled = true;
                            }
                            else
                            {
                                ddEvent.Enabled = false;
                                ddEvent.SelectedIndex = 1;
                            }
                        }

                        cmdText = "select distinct E.ChapterID,E.ChapterCode from Volunteer V inner join Chapter E on (V.ChapterID=E.ChapterID) where MemberID=" + MemberID + " and RoleID=" + Convert.ToInt32(RoleID) + " order by E.ChapterID Asc";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddchapter.DataSource = ds;
                        ddchapter.DataTextField = "ChapterCode";
                        ddchapter.DataValueField = "ChapterID";
                        ddchapter.DataBind();

                        ddchapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddchapter.Enabled = true;
                            }
                            else
                            {
                                ddchapter.Enabled = false;
                                ddchapter.SelectedIndex = 1;
                            }
                        }

                        cmdText = "select E.TeamID,E.TeamName from Role V inner join VolTeamMatrix E on (V.TeamID=E.TeamID) where V.RoleID=" + RoleID + "";
                        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        ddTeams.DataSource = ds;
                        ddTeams.DataTextField = "TeamName";
                        ddTeams.DataValueField = "TeamID";
                        ddTeams.DataBind();

                        ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
                        if (ds.Tables[0] != null)
                        {
                            if (ds.Tables[0].Rows.Count > 1)
                            {
                                ddTeams.Enabled = true;
                            }
                            else
                            {
                                ddTeams.Enabled = false;
                                ddTeams.SelectedIndex = 1;
                                if (Convert.ToInt32(RoleID) == 8)
                                {
                                    ddTeams.Enabled = true;
                                    ddTeams.SelectedIndex = 0;
                                }
                                
                            }
                        }


                        ddYear.SelectedValue = Convert.ToString(DateTime.Now.Year);

                    }
                }





            }
        }

        lblMsg.Text = "";
        lblActivityMsg.Text = "";
        lblErrMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";

    }
    string ConnectionString = "ConnectionString";
    bool isDateParse = false;
    int index = -1;
    int activityIndex = -1;
    string VolText = "";
    protected void Events()
    {
        try
        {
            //ddEvent.Items.Insert(0, new ListItem("Prepclub", "19"));
            //ddEvent.Items.Insert(0, new ListItem("OnlineWorkshop", "20"));
            //ddEvent.Items.Insert(0, new ListItem("[National]", "21"));
            //ddEvent.Items.Insert(0, new ListItem("Coaching", "13"));
            //ddEvent.Items.Insert(0, new ListItem("Workshop", "3"));
            //ddEvent.Items.Insert(0, new ListItem("ChapterContests", "2"));
            //ddEvent.Items.Insert(0, new ListItem("Finals", "1"));


        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void Yearscount()
    {
        try
        {
            ddYear.Items.Clear();
            int MaxYear = DateTime.Now.Year;
            ArrayList list = new ArrayList();

            for (int i = MaxYear; i <= DateTime.Now.Year + 1; i++)
            {
                list.Add(new ListItem(i.ToString(), i.ToString()));
            }
            ddYear.DataSource = list;
            ddYear.DataTextField = "Text";
            ddYear.DataValueField = "Value";
            ddYear.DataBind();

            ddYear.Items.Insert(0, new ListItem("Select Year", "0"));
        }
        catch
        {
            //  SessionExp();
        }
    }
    protected void ChapterDrop(DropDownList ddChapter)
    {

        //if (((ddEvent.SelectedValue == "2") || (ddEvent.SelectedValue == "19") || (ddEvent.SelectedValue == "3")))
        //{
        string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        try
        {
            DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
            if (dschapter.Tables[0].Rows.Count > 0)
            {
                ddChapter.Enabled = true;
                ddChapter.DataSource = dschapter;
                ddChapter.DataTextField = "chaptercode";
                ddChapter.DataValueField = "ChapterID";
                ddChapter.DataBind();

                ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        //}
        //else if (ddEvent.SelectedValue == "1")
        //{
        //    string ddChapterstr = "select distinct ChapterID,chaptercode,[state] from chapter order by [State],ChapterCode";
        //    try
        //    {
        //        DataSet dschapter = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, ddChapterstr);
        //        if (dschapter.Tables[0].Rows.Count > 0)
        //        {
        //            ddChapter.Enabled = true;
        //            ddChapter.DataSource = dschapter;
        //            ddChapter.DataTextField = "chaptercode";
        //            ddChapter.DataValueField = "ChapterID";
        //            ddChapter.DataBind();

        //            ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
        //        }
        //        ddchapter.SelectedValue = "1";
        //        ddChapter.Enabled = false;
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message);
        //    }
        //}
        //else
        //{
        //    ddchapter.Items.Clear();
        //    ddChapter.Items.Insert(0, new ListItem("Select Chapter", "0"));
        //    ddChapter.Enabled = false;
        //}



    }
    protected void Team()
    {
        string StrQryCheck = "";

        if (ddEvent.SelectedValue != "0")
        {
            string eventText = ddEvent.SelectedItem.Text;
            if (ddEvent.SelectedItem.Text == "Chapter")
            {
                eventText = "ChapterContests";
            }
            StrQryCheck = "select * from  VolTeamMatrix where " + eventText + "='Y'";
        }
        else
        {
            StrQryCheck = "select * from  VolTeamMatrix";
        }
        try
        {
            DataSet dsCheck = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, StrQryCheck);
            if (dsCheck.Tables[0].Rows.Count > 0)
            {

                ddTeams.DataSource = dsCheck;
                ddTeams.DataTextField = "TeamName";
                ddTeams.DataValueField = "TeamId";
                ddTeams.DataBind();

                ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
            }
            else
            {
                ddTeams.DataSource = dsCheck;
                ddTeams.DataTextField = "TeamName";
                ddTeams.DataValueField = "TeamId";
                ddTeams.DataBind();

                ddTeams.Items.Insert(0, new ListItem("Select Team", "0"));
            }
        }
        catch (Exception ex)
        {
            //throw new Exception(ex.Message);
        }

    }
    protected void ddEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedItem.Text != "Select Event")
        {
            Team();
        }
        //ChapterDrop(ddchapter);
        try
        {

            if (ddEvent.SelectedValue == "1")
            {
                ddchapter.SelectedValue = "1";
                ddchapter.Enabled = false;
            }
            else
            {
                ddchapter.SelectedValue = "0";
                ddchapter.Enabled = true;
            }
        }
        catch (Exception ex)
        {
        }


    }
    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (validateSearch() == "1")
        {
            lblErrMsg.Text = "";
            dvActivityGroup.Style.Add("display", "block");
            BindBeeBookSchedule();

            lblErrMsg.Text = "";
            lblMsg.Text = "";

            lblTeamActivityGridMsg.Text = "";
            tblActivityGroup.Style.Add("display", "none");
        }
        else
        {
            lblActivityMsg.Text = "";
            lblMsg.Text = "";
            lblActivityMsg.Text = "";
            lblTeamActivityGridMsg.Text = "";
            validateSearch();
        }
        tblTeamActivity.Style.Add("display", "none");
        grdteamActivity.DataSource = null;
        grdteamActivity.DataBind();
        dvaddNewActivity.Style.Add("display", "none");
        dvCalculateEndDate.Style.Add("display", "none");

        dvTeamActivity.Style.Add("display", "none");

        dvDeleteActivity.Style.Add("display", "none");
        dvActivityGroupDelete.Style.Add("display", "none");
        pnlActivitySearch.Visible = false;
        pIndSearch.Visible = false;

        dvCloseActivitytable.Visible = false;
    }
    protected void btnAddNew_Click(object sender, EventArgs e)
    {
        dvDeleteActivity.Style.Add("display", "none");
        dvActivityGroupDelete.Style.Add("display", "none");
        tblActivityGroup.Style.Add("display", "table");
        tblTeamActivity.Style.Add("display", "none");
        txtActivityGroup.Text = "";
        ddlUnitOfTime.SelectedValue = "0";
        btnSaveGroup.Text = "Add";
        lblErrMsg.Text = "";
        lblMsg.Text = "";
        lblActivityMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        txtVolunteerInCharge.Text = "";
        txtVendor.Text = "";
        txtFinishedDate.Text = "";
        ddlStatus.SelectedValue = "0";
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
        pnlActivitySearch.Visible = false;
        pIndSearch.Visible = false;
        Panel2.Visible = false;
        Panel4.Visible = false;
        hdnDonorType.Value = "";
        hdnVendorID.Value = "";
        hdnMemberID.Value = "";
    }
    protected void btnCancelGroup_Click(object sender, EventArgs e)
    {
        tblActivityGroup.Style.Add("display", "none");
        txtActivityGroup.Text = "";
        ddlUnitOfTime.SelectedValue = "0";

        lblErrMsg.Text = "";
        lblMsg.Text = "";
        lblActivityMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        dvDeleteActivity.Style.Add("display", "none");
        dvActivityGroupDelete.Style.Add("display", "none");
        pnlActivitySearch.Visible = false;
        pIndSearch.Visible = false;
        txtVolunteerInCharge.Text = "";
        txtVendor.Text = "";
        txtFinishedDate.Text = "";
        ddlStatus.SelectedValue = "0";
        Panel2.Visible = false;
        Panel4.Visible = false;

        hdnDonorType.Value = "";
        hdnVendorID.Value = "";
        hdnMemberID.Value = "";
    }
    protected void btnCancelActivity_Click(object sender, EventArgs e)
    {
        tblTeamActivity.Style.Add("display", "none");

        txtGroupActivity.Text = "";
        txtDuration.Text = "";
        txtEndDate.Text = "";
        txtActivity.Text = "";
        lblErrMsg.Text = "";
        lblMsg.Text = "";
        lblActivityMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        txtActivityFinishDate.Text = "";
        txtActivityVendor.Text = "";
        txtActivityVolInCharge.Text = "";
        ddlActivityStatus.SelectedValue = "0";
        dvDeleteActivity.Style.Add("display", "none");
        dvActivityGroupDelete.Style.Add("display", "none");
        pnlActivitySearch.Visible = false;
        pIndSearch.Visible = false;

        Panel2.Visible = false;
        Panel4.Visible = false;

        hdnDonorType.Value = "";
        hdnVendorID.Value = "";
        hdnMemberID.Value = "";
    }
    protected void btnAddNewActivity_Click(object sender, EventArgs e)
    {
        dvDeleteActivity.Style.Add("display", "none");
        tblTeamActivity.Style.Add("display", "table");
        tblActivityGroup.Style.Add("display", "none");
        btnSaveActivity.Text = "Add";
        txtGroupActivity.Text = hdnActivityGroup.Value;
        txtDuration.Text = "";
        txtEndDate.Text = "";
        txtActivity.Text = "";
        lblErrMsg.Text = "";
        lblMsg.Text = "";
        lblActivityMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        txtActivityFinishDate.Text = "";
        txtActivityVendor.Text = "";
        txtActivityVolInCharge.Text = "";
        ddlActivityStatus.SelectedValue = "0";

        hdnDonorType.Value = "";
        hdnVendorID.Value = "";
        hdnMemberID.Value = "";
        //hdnFinalDate.Value = "";
        //hdnEndDate.Value = "";
        DataSet ds = new DataSet();
        string Date = "";
        string cmdtext = "select max(EndDate) as EndDate from TeamActivity where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    if (ds.Tables[0].Rows[0]["EndDate"].ToString() == "")
                    {
                        ancDatePicker.Visible = true;
                        isDateParse = true;
                        txtEndDate.Enabled = true;
                    }
                    else
                    {
                        //For Server
                        string date = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("MM-dd-yyyy");
                        //string date = Convert.ToDateTime(ds.Tables[0].Rows[0]["EndDate"].ToString()).ToString("dd-MM-yyyy");
                        hdnFinalDate.Value = date;
                        ancDatePicker.Visible = false;
                        isDateParse = false;
                        txtEndDate.Enabled = false;
                    }
                }
                else
                {
                    ancDatePicker.Visible = true;
                    isDateParse = true;
                }

            }
            if (grdteamActivity.Rows.Count > 1)
            {

                txtEndDate.Enabled = false;
            }
            else
            {
                if (hdnEndDate.Value != "")
                {
                    txtEndDate.Enabled = false;
                }
                else
                {
                    txtEndDate.Enabled = true;
                }
            }
            int rowCount = grdteamActivity.Rows.Count;
            if (rowCount > 1)
            {
                dvCalculateEndDate.Style.Add("display", "block;");
                dvaddNewActivity.Style.Add("margin-bottom", "-24px;");
            }
            else
            {
                dvaddNewActivity.Style.Add("margin-bottom", "-14px;");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
        pnlActivitySearch.Visible = false;
        pIndSearch.Visible = false;

        Panel2.Visible = false;
        Panel4.Visible = false;
    }

    protected void btnSaveGroup_Click(object sender, EventArgs e)
    {
        if (validateActivityGroup() == "1")
        {

            lblMsg.Text = "";
            string groupName = txtActivityGroup.Text;
            string unitOfTime = ddlUnitOfTime.SelectedValue;
            string userID = Session["LoginId"].ToString();
            int eventID = Convert.ToInt32(ddEvent.SelectedValue);
            string eventName = ddEvent.SelectedItem.Text;
            int chapterID = Convert.ToInt32(ddchapter.SelectedValue);
            string chapterName = ddchapter.SelectedItem.Text;
            int teamID = Convert.ToInt32(ddTeams.SelectedValue);

            string teamName = ddTeams.SelectedItem.Text;
            string year = ddYear.SelectedValue;
            string beginDate = txtBeginDate.Text;
            string endDate = txtGroupEndDate.Text;
            string cmdtext = "";
            string msg = "";
            string cmdDupCheck = "";
            if (txtVolunteerInCharge.Text == "")
            {
                hdnMemberID.Value = "";
            }
            if (txtVendor.Text == "")
            {
                hdnVendorID.Value = "";
            }
            string volunteerId = (hdnMemberID.Value == "" ? "0" : hdnMemberID.Value);
            string volId = volunteerId;
            if (volunteerId == "0")
            {
                volId = "null";
            }

            string vendorId = (hdnVendorID.Value == "" ? "0" : hdnVendorID.Value);
            string venId = vendorId;
            if (vendorId == "0")
            {
                venId = "null";
            }
            string volunteername = txtVolunteerInCharge.Text;
            string vendorname = txtVendor.Text;
            string donorType = hdnDonorType.Value;

            string status = (ddlStatus.SelectedValue == "0" ? "" : ddlStatus.SelectedValue);
            if (status == "")
            {
                status = "null";
            }
            else
            {
                status = "'" + status + "'";
            }

            if (donorType == "")
            {
                donorType = "null";
            }
            else
            {
                donorType = "'" + donorType + "'";
            }
            string finishDate = txtFinishedDate.Text;
            if (btnSaveGroup.Text == "Add")
            {
                msg = "Activity Group added successfully";
                cmdDupCheck = "select count(*) as count from TeamSettings where ActivityGroup='" + groupName + "' and Year='" + year + "' and EventID=" + eventID + " and ChapterID=" + chapterID + " and TeamID=" + teamID + "";
                if (endDate == "")
                {
                    if (finishDate != "")
                    {
                        cmdtext = "Insert into TeamSettings ([Year],EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,UnitOfTime,[Order],BegDate,CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values('" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + groupName + "',(select ISNUll(max(TeamSettingsID),0)+1 from TeamSettings),'" + unitOfTime + "',(select ISNUll(max([Order]),0)+1 from TeamSettings where Year='" + year + "'),'" + beginDate + "',GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",'" + txtFinishedDate.Text + "')";
                    }
                    else
                    {
                        cmdtext = "Insert into TeamSettings ([Year],EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,UnitOfTime,[Order],BegDate,CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values('" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + groupName + "',(select ISNUll(max(TeamSettingsID),0)+1 from TeamSettings),'" + unitOfTime + "',(select ISNUll(max([Order]),0)+1 from TeamSettings where Year='" + year + "'),'" + beginDate + "',GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",null)";
                    }
                }
                else
                {
                    if (finishDate != "")
                    {
                        cmdtext = "Insert into TeamSettings ([Year],EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,UnitOfTime,[Order],BegDate,EndDate,CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values('" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + groupName + "',(select ISNUll(max(TeamSettingsID),0)+1 from TeamSettings),'" + unitOfTime + "',(select ISNUll(max([Order]),0)+1 from TeamSettings where Year='" + year + "'),'" + beginDate + "','" + endDate + "',GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",'" + txtFinishedDate.Text + "')";
                    }
                    else
                    {
                        cmdtext = "Insert into TeamSettings ([Year],EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,UnitOfTime,[Order],BegDate,EndDate,CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values('" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + groupName + "',(select ISNUll(max(TeamSettingsID),0)+1 from TeamSettings),'" + unitOfTime + "',(select ISNUll(max([Order]),0)+1 from TeamSettings where Year='" + year + "'),'" + beginDate + "','" + endDate + "',GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",null)";
                    }
                }
            }
            else
            {
                msg = "Activity Group updated successfully";
                cmdDupCheck = "select count(*) as count from TeamSettings where TeamSettingsID<>" + hdnTeamSettingsID.Value + " and ActivityGroup='" + groupName + "' and Year='" + year + "' and EventID=" + eventID + " and ChapterID=" + chapterID + " and TeamID=" + teamID + "";
                if (endDate == "")
                {
                    if (finishDate != "")
                    {
                        cmdtext = "Update TeamSettings set [Year]='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + groupName + "',UnitOfTime='" + unitOfTime + "',BegDate='" + beginDate + "',EndDate=NULL,ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate='" + txtFinishedDate.Text + "' where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
                    }
                    else
                    {
                        cmdtext = "Update TeamSettings set [Year]='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + groupName + "',UnitOfTime='" + unitOfTime + "',BegDate='" + beginDate + "',EndDate=NULL,ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate=null where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
                    }

                }
                else
                {
                    if (finishDate != "")
                    {
                        cmdtext = "Update TeamSettings set [Year]='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + groupName + "',UnitOfTime='" + unitOfTime + "',BegDate='" + beginDate + "',EndDate='" + endDate + "',ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate='" + txtFinishedDate.Text + "' where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
                    }
                    else
                    {
                        cmdtext = "Update TeamSettings set [Year]='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + groupName + "',UnitOfTime='" + unitOfTime + "',BegDate='" + beginDate + "',EndDate='" + endDate + "',ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate=null where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
                    }
                }
            }
            DataSet ds = new DataSet();
            try
            {
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdDupCheck);
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["count"].ToString()) > 0)
                {
                    lblMsg.Text = "Duplicate Record Exists!";
                    lblMsg.ForeColor = Color.Red;

                }
                else
                {
                    SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
                    lblMsg.Text = msg;
                    lblMsg.ForeColor = Color.Blue;
                    txtActivityGroup.Text = "";
                    txtBeginDate.Text = "";
                    txtGroupEndDate.Text = "";
                    ddlUnitOfTime.SelectedValue = "0";
                    BindBeeBookSchedule();

                    tblActivityGroup.Style.Add("display", "none");
                    dvDeleteActivity.Style.Add("display", "none");
                    dvActivityGroupDelete.Style.Add("display", "none");
                    pnlActivitySearch.Visible = false;
                    pIndSearch.Visible = false;
                    Panel2.Visible = false;
                    Panel4.Visible = false;
                    txtVolunteerInCharge.Text = "";
                    txtVendor.Text = "";
                    txtFinishedDate.Text = "";
                    ddlStatus.SelectedValue = "0";
                    hdnDonorType.Value = "";
                    hdnVendorID.Value = "";
                    hdnMemberID.Value = "";
                    //BindTeamActivity(hdnTeamSettingsID.Value);
                }
            }
            catch (Exception ex)
            {
                hdnDonorType.Value = "";
                hdnVendorID.Value = "";
                hdnMemberID.Value = "";
                throw new Exception(ex.Message);
            }
        }
        else
        {
            validateActivityGroup();
        }

    }
    protected void BindBeeBookSchedule()
    {
        Session["TeamPlanSettings"] = string.Empty;
        string cmdtext = "select [Order],TeamSettingsID,ActivityGroup,UnitOfTime,EventID,ChapterID,TeamID,Year from TeamSettings where EventID=" + ddEvent.SelectedValue + " and ChapterID=" + ddchapter.SelectedValue + " and TeamID=" + ddTeams.SelectedValue + " and Year='" + ddYear.SelectedValue + "'";
        if (ddYear.SelectedValue == "-1")
        {
            cmdtext = "select [Order],TeamSettingsID,ActivityGroup,UnitOfTime,BegDate,EndDate,TS.EventID,TS.ChapterID,TeamID,Year,(IP.FirstName+' '+IP.LastName) as Name, case when TS.VDonorType='OWN' then OI.ORGANIZATION_NAME  when TS.VDonorType='IND' then (IP1.FirstName+' '+IP1.LastName) when TS.VDonorType='SPOUSE' then (IP1.FirstName+' '+IP1.LastName) else '' end as VendorName,TS.MemberID,TS.VendorID,TS.Status,TS.FinishDate,TS.VDonorType from TeamSettings TS left join IndSpouse IP on(TS.MemberId=IP.AutoMemberId)left join IndSpouse IP1 on (TS.VendorID=IP1.AutoMemberId) left join OrganizationInfo OI on (TS.VendorID=OI.AutoMemberID) where TS.EventID=" + ddEvent.SelectedValue + " and TS.ChapterID=" + ddchapter.SelectedValue + " and TS.TeamID=" + ddTeams.SelectedValue + " order by TS.[Order] Asc";
        }
        else
        {
            cmdtext = "select [Order],TeamSettingsID,ActivityGroup,UnitOfTime,BegDate,EndDate,TS.EventID,TS.ChapterID,TeamID,Year,(IP.FirstName+' '+IP.LastName) as Name, case when TS.VDonorType='OWN' then OI.ORGANIZATION_NAME  when TS.VDonorType='IND' then (IP1.FirstName+' '+IP1.LastName) when TS.VDonorType='SPOUSE' then (IP1.FirstName+' '+IP1.LastName) else '' end as VendorName,TS.MemberID,TS.VendorID,TS.Status,TS.FinishDate,TS.VDonorType from TeamSettings TS left join IndSpouse IP on(TS.MemberId=IP.AutoMemberId)left join IndSpouse IP1 on (TS.VendorID=IP1.AutoMemberId) left join OrganizationInfo OI on (TS.VendorID=OI.AutoMemberID) where TS.EventID=" + ddEvent.SelectedValue + " and TS.ChapterID=" + ddchapter.SelectedValue + " and TS.TeamID=" + ddTeams.SelectedValue + " and TS.Year='" + ddYear.SelectedValue + "' order by TS.[Order] Asc";
        }
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                lblActivityMsg.Text = "";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    grdTeamSettings.DataSource = ds.Tables[0];
                    grdTeamSettings.DataBind();
                    lblActivityMsg.Text = "";
                    DataTable dt = new DataTable();
                    dt = ds.Tables[0];
                    Session["TeamPlanSettings"] = dt;
                    // grdTeamSettings.Rows[0].BackColor = Color.FromName("#EAEAEA");
                }
                else
                {
                    grdTeamSettings.DataSource = null;
                    grdTeamSettings.DataBind();

                    lblActivityMsg.Text = "No record found";
                    lblActivityMsg.ForeColor = Color.Red;
                }
            }
            else
            {
                grdTeamSettings.DataSource = null;
                grdTeamSettings.DataBind();
                lblActivityMsg.Text = "No record found";
                lblActivityMsg.ForeColor = Color.Red;
            }
            string cmdRowOrerUp = "select max([Order]) as MaxOrder from TeamSettings";
            string cmdRowDown = "select min([Order]) as MinOrder from TeamSettings";
            DataSet rowUpDs = new DataSet();
            int rowOrderUp = 1;
            int rowOrderDown = 0;
            rowUpDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdRowOrerUp);
            if (rowUpDs.Tables[0] != null)
            {
                if (rowUpDs.Tables[0].Rows.Count > 0)
                {
                    if (rowUpDs.Tables[0].Rows[0]["MaxOrder"] != null && rowUpDs.Tables[0].Rows[0]["MaxOrder"].ToString() != "")
                    {
                        rowOrderUp = Convert.ToInt32(rowUpDs.Tables[0].Rows[0]["MaxOrder"].ToString());
                    }
                }
            }
            rowUpDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdRowDown);
            if (rowUpDs.Tables[0] != null)
            {
                if (rowUpDs.Tables[0].Rows.Count > 0)
                {
                    if (rowUpDs.Tables[0].Rows[0]["MinOrder"] != null && rowUpDs.Tables[0].Rows[0]["MinOrder"].ToString() != "")
                    {
                        rowOrderDown = Convert.ToInt32(rowUpDs.Tables[0].Rows[0]["MinOrder"].ToString());
                    }
                }
            }
            int rowCount = grdTeamSettings.Rows.Count;
            if (rowCount > 0)
            {
                btnExportToExcel.Visible = true;
                if (rowCount == 1)
                {
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).Enabled = false;
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).ToolTip = "";
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowDown") as ImageButton).Enabled = false;
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowDown") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowDown") as ImageButton).ToolTip = "";
                }
                else
                {
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).Enabled = false;
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdTeamSettings.Rows[0].FindControl("imgRowUp") as ImageButton).ToolTip = "";
                    ((ImageButton)grdTeamSettings.Rows[rowCount - 1].FindControl("imgRowDown") as ImageButton).Enabled = false;
                    ((ImageButton)grdTeamSettings.Rows[rowCount - 1].FindControl("imgRowDown") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdTeamSettings.Rows[rowCount - 1].FindControl("imgRowDown") as ImageButton).ToolTip = "";
                }
            }
            if (index != -1)
            {
                grdTeamSettings.Rows[index].BackColor = Color.FromName("#EAEAEA");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grdTeamSettings_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblActivityMsg.Text = "";
        lblMsg.Text = "";
        lblErrMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";

        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Up" || e.CommandName == "Down")
            {
                row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            }
            else
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            }

            pIndSearch.Visible = false;
            pnlActivitySearch.Visible = false;

            int selIndex = row.RowIndex;
            index = row.RowIndex;
            grdTeamSettings.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
            index = row.RowIndex;
            string teamID = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblTeamID") as Label).Text;
            string eventID = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblEventID") as Label).Text;
            string chapterID = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblChapterID") as Label).Text;
            string year = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblYear") as Label).Text;
            string order = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblRowOrder") as Label).Text;


            string teamSettingsID = grdTeamSettings.Rows[selIndex].Cells[4].Text;
            string activityGroup = grdTeamSettings.Rows[selIndex].Cells[5].Text;
            string unitOfTime = grdTeamSettings.Rows[selIndex].Cells[6].Text;
            string beginDate = grdTeamSettings.Rows[selIndex].Cells[7].Text;
            string endDate = grdTeamSettings.Rows[selIndex].Cells[8].Text;

            string finalBegDate = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblGroupStartDate") as Label).Text;
            string finalEndDate = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblGroupEndDate") as Label).Text;

            hdnMemberID.Value = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblMemberID1") as Label).Text;
            hdnVendorID.Value = ((Label)grdTeamSettings.Rows[selIndex].FindControl("lblVendorID1") as Label).Text;

            hdnGroupDuration.Value = unitOfTime;

            hdnTeamID.Value = teamID;
            hdnEventID.Value = eventID;
            hdnChapterID.Value = chapterID;
            hdnTeamSettingsID.Value = teamSettingsID;
            hdnYear.Value = year;
            ddEvent.SelectedValue = eventID;
            ddchapter.SelectedValue = chapterID;

            ddTeams.SelectedValue = teamID;
            hdnActivityGroup.Value = activityGroup;
            if (e.CommandName == "Modify")
            {
                Team();
                tblActivityGroup.Style.Add("display", "table");
                tblTeamActivity.Style.Add("display", "none");
                dvaddNewActivity.Style.Add("display", "none");
                dvCalculateEndDate.Style.Add("display", "none");
                dvTeamActivity.Style.Add("display", "none");
                grdteamActivity.DataSource = null;
                grdteamActivity.DataBind();

                txtVolunteerInCharge.Text = (grdTeamSettings.Rows[selIndex].Cells[9].Text).Replace("&nbsp;", "");
                txtVendor.Text = (grdTeamSettings.Rows[selIndex].Cells[10].Text).Replace("&nbsp;", "");
                ddlStatus.SelectedValue = (grdTeamSettings.Rows[selIndex].Cells[11].Text).Replace("&nbsp;", "0");
                hdnDonorType.Value = (((Label)grdTeamSettings.Rows[selIndex].FindControl("lblDonorType") as Label).Text).Replace("&nbsp;", "");
                if (txtVolunteerInCharge.Text != "")
                {
                    btnVolunteerDelete.Visible = true;
                }
                else
                {
                    btnVolunteerDelete.Visible = false;
                }

                if (txtVendor.Text != "")
                {
                    btnVendorDelete.Visible = true;
                }
                else
                {
                    btnVendorDelete.Visible = false;
                }

                if ((grdTeamSettings.Rows[selIndex].Cells[11].Text).Replace("&nbsp;", "") == "")
                {
                    ddlStatus.SelectedValue = "0";
                }
                txtFinishedDate.Text = (grdTeamSettings.Rows[selIndex].Cells[12].Text).Replace("&nbsp;", "");

                txtActivityGroup.Text = activityGroup;
                txtBeginDate.Text = (beginDate == "&nbsp;" ? "" : beginDate);
                txtGroupEndDate.Text = (endDate == "&nbsp;" ? "" : endDate);
                ddlUnitOfTime.SelectedValue = unitOfTime;
                btnSaveGroup.Text = "Update";
                lblMsg.Text = "";
                lblActivityMsg.Text = "";
                tblTeamActivity.Style.Add("display", "none");
                hdnBeginDate.Value = finalBegDate;
                hdnEndDate.Value = finalEndDate;
                if (finalEndDate == "")
                {
                    hdnFinalDate.Value = finalBegDate;
                }
                else
                {
                    hdnFinalDate.Value = finalEndDate;
                }

                dvActivityGroupDelete.Style.Add("display", "block");
                dvDeleteActivity.Style.Add("display", "none");
                dvCloseActivitytable.Visible = false;
            }
            else if (e.CommandName == "Select")
            {
                Team();
                dvaddNewActivity.Style.Add("display", "block");
                //dvCalculateEndDate.Style.Add("display", "block");

                tblActivityGroup.Style.Add("display", "none");
                txtGroupActivity.Text = activityGroup;
                BindTeamActivity(teamSettingsID);
                lblMsg.Text = "";
                lblActivityMsg.Text = "";
                dvTeamActivity.Style.Add("display", "block");
                tblTeamActivity.Style.Add("display", "none");
                hdnBeginDate.Value = finalBegDate;
                hdnEndDate.Value = (endDate == "&nbsp;" ? "" : endDate);
                if (finalEndDate == "")
                {
                    hdnFinalDate.Value = finalBegDate;
                }
                else
                {
                    hdnFinalDate.Value = finalEndDate;
                }
                int rowCount = grdteamActivity.Rows.Count;
                if (rowCount > 0)
                {
                    dvCloseActivitytable.Visible = true;
                }
                if (rowCount > 1)
                {
                    dvCalculateEndDate.Style.Add("display", "block;");
                    dvaddNewActivity.Style.Add("margin-bottom", "-24px;");
                }
                else
                {
                    dvaddNewActivity.Style.Add("margin-bottom", "-14px;");
                }
                dvActivityGroupDelete.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
            }
            else if (e.CommandName == "Up")
            {
                tblActivityGroup.Style.Add("display", "none");
                dvaddNewActivity.Style.Add("display", "block");
                // dvCalculateEndDate.Style.Add("display", "block");

                //dvCalculateEndDate.Style.Add("display", "block");
                dvTeamActivity.Style.Add("display", "block");
                grdteamActivity.DataSource = null;
                grdteamActivity.DataBind();
                int rowIndex = selIndex;
                int previousRowIndex = selIndex - 1;

                string rowOrderText = ((Label)grdTeamSettings.Rows[rowIndex].FindControl("lblRowOrder") as Label).Text;
                string previousRowOrderText = ((Label)grdTeamSettings.Rows[previousRowIndex].FindControl("lblRowOrder") as Label).Text;

                string teamSettingsGroupID = grdTeamSettings.Rows[rowIndex].Cells[4].Text;
                string previousRowTeamSettingsGroupID = grdTeamSettings.Rows[previousRowIndex].Cells[4].Text;

                index = index - 1;
                string cmdOrder = "update TeamSettings set [Order]=" + previousRowOrderText + " where TeamSettingsID=" + teamSettingsGroupID + "; update TeamSettings set [Order]=" + rowOrderText + " where TeamSettingsID=" + previousRowTeamSettingsGroupID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdOrder);
                BindBeeBookSchedule();
                BindTeamActivity(teamSettingsID);
                //BindTeamActivity(previousRowTeamSettingsGroupID.ToString());
                dvActivityGroupDelete.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
            }
            else if (e.CommandName == "Down")
            {
                tblActivityGroup.Style.Add("display", "none");
                dvaddNewActivity.Style.Add("display", "block");

                //dvCalculateEndDate.Style.Add("display", "block");

                dvTeamActivity.Style.Add("display", "block");
                grdteamActivity.DataSource = null;
                grdteamActivity.DataBind();
                int rowIndex = selIndex;
                int nextRowIndex = selIndex + 1;

                string rowOrderText = ((Label)grdTeamSettings.Rows[rowIndex].FindControl("lblRowOrder") as Label).Text;
                string nextRowOrderText = ((Label)grdTeamSettings.Rows[nextRowIndex].FindControl("lblRowOrder") as Label).Text;

                string teamSettingsGroupID = grdTeamSettings.Rows[rowIndex].Cells[4].Text;
                string nextRowTeamSettingsGroupID = grdTeamSettings.Rows[nextRowIndex].Cells[4].Text;

                index = index + 1;
                string cmdOrder = "update TeamSettings set [Order]=" + nextRowOrderText + " where TeamSettingsID=" + teamSettingsGroupID + "; update TeamSettings set [Order]=" + rowOrderText + " where TeamSettingsID=" + nextRowTeamSettingsGroupID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdOrder);
                BindBeeBookSchedule();
                BindTeamActivity(teamSettingsID);
                dvActivityGroupDelete.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
                //BindTeamActivity(nextRowTeamSettingsGroupID.ToString());
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void grdteamActivity_RowCommand(object sender, GridViewCommandEventArgs e)
    {
        lblActivityMsg.Text = "";
        lblMsg.Text = "";
        lblErrMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        try
        {
            GridViewRow row = null;
            if (e.CommandName == "Up" || e.CommandName == "Down")
            {
                row = (GridViewRow)(((ImageButton)e.CommandSource).NamingContainer);
            }
            else
            {
                row = (GridViewRow)(((Button)e.CommandSource).NamingContainer);
            }

            int selIndex = row.RowIndex;
            activityIndex = row.RowIndex;
            grdteamActivity.Rows[selIndex].BackColor = Color.FromName("#EAEAEA");
            hdnTeamActID.Value = grdteamActivity.Rows[selIndex].Cells[4].Text;
            string activityGroup = grdteamActivity.Rows[selIndex].Cells[6].Text;
            string duration = grdteamActivity.Rows[selIndex].Cells[7].Text;
            string endDate = grdteamActivity.Rows[selIndex].Cells[8].Text;
            string activity = grdteamActivity.Rows[selIndex].Cells[5].Text;

            hdnMemberID.Value = ((Label)grdteamActivity.Rows[selIndex].FindControl("lblMemberID") as Label).Text;
            hdnVendorID.Value = ((Label)grdteamActivity.Rows[selIndex].FindControl("lblVendorID") as Label).Text;

            if (e.CommandName == "Modify")
            {
                btnSaveActivity.Text = "Update";
                tblTeamActivity.Style.Add("display", "table");
                txtGroupActivity.Text = activityGroup;
                txtDuration.Text = duration;
                txtEndDate.Text = endDate;
                txtActivity.Text = activity;
                lblMsg.Text = "";
                lblActivityMsg.Text = "";

                txtActivityVolInCharge.Text = (grdteamActivity.Rows[selIndex].Cells[9].Text).Replace("&nbsp;", "");
                txtActivityVendor.Text = (grdteamActivity.Rows[selIndex].Cells[10].Text).Replace("&nbsp;", "");
                ddlActivityStatus.SelectedValue = (grdteamActivity.Rows[selIndex].Cells[11].Text).Replace("&nbsp;", "0");
                hdnDonorType.Value = (((Label)grdteamActivity.Rows[selIndex].FindControl("lblDonorType1") as Label).Text).Replace("&nbsp;", "");
                if ((grdteamActivity.Rows[selIndex].Cells[11].Text).Replace("&nbsp;", "") == "")
                {
                    ddlActivityStatus.SelectedValue = "0";
                }

                txtActivityFinishDate.Text = (grdteamActivity.Rows[selIndex].Cells[12].Text).Replace("&nbsp;", "");

                if (txtActivityVolInCharge.Text != "")
                {
                    btnActVolunteerDelete.Visible = true;
                }
                else
                {
                    btnActVolunteerDelete.Visible = false;
                }
                if (txtActivityVendor.Text != "")
                {
                    btnActVendorDelete.Visible = true;
                }
                else
                {
                    btnActVendorDelete.Visible = false;
                }

                if (grdteamActivity.Rows.Count > 1)
                {
                    txtEndDate.Enabled = false;
                }
                else
                {
                    txtEndDate.Enabled = true;
                }
                if (hdnBeginDate.Value != "" && hdnEndDate.Value == "")
                {
                    if (grdteamActivity.Rows.Count > 1)
                    {
                        if (selIndex > 0)
                        {
                            endDate = ((Label)grdteamActivity.Rows[selIndex - 1].FindControl("lblEndDate") as Label).Text;
                        }
                        else
                        {
                            endDate = hdnBeginDate.Value;
                        }
                    }
                    else
                    {
                        endDate = hdnBeginDate.Value;
                    }
                    hdnFinalDate.Value = endDate;
                }
                dvDeleteActivity.Style.Add("display", "block");
            }
            else if (e.CommandName == "Up")
            {
                tblTeamActivity.Style.Add("display", "none");
                int rowIndex = selIndex;
                int previousRowIndex = selIndex - 1;

                string rowOrderText = ((Label)grdteamActivity.Rows[rowIndex].FindControl("lblActivityRowOrder") as Label).Text;
                string previousRowOrderText = ((Label)grdteamActivity.Rows[previousRowIndex].FindControl("lblActivityRowOrder") as Label).Text;

                string teamSettingsGroupID = grdteamActivity.Rows[rowIndex].Cells[4].Text;
                string previousRowTeamSettingsGroupID = grdteamActivity.Rows[previousRowIndex].Cells[4].Text;

                activityIndex = activityIndex - 1;
                string cmdOrder = "update TeamActivity set [Order]=" + previousRowOrderText + " where TeamActID=" + teamSettingsGroupID + "; update TeamActivity set [Order]=" + rowOrderText + " where TeamActID=" + previousRowTeamSettingsGroupID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdOrder);
                BindTeamActivity(hdnTeamSettingsID.Value);
                dvDeleteActivity.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
            }
            else if (e.CommandName == "Down")
            {
                tblTeamActivity.Style.Add("display", "none");
                int rowIndex = selIndex;
                int nextRowIndex = selIndex + 1;

                string rowOrderText = ((Label)grdteamActivity.Rows[rowIndex].FindControl("lblActivityRowOrder") as Label).Text;
                string nextRowOrderText = ((Label)grdteamActivity.Rows[nextRowIndex].FindControl("lblActivityRowOrder") as Label).Text;

                string teamSettingsGroupID = grdteamActivity.Rows[rowIndex].Cells[4].Text;
                string nextRowTeamSettingsGroupID = grdteamActivity.Rows[nextRowIndex].Cells[4].Text;


                activityIndex = activityIndex + 1;
                string cmdOrder = "update TeamActivity set [Order]=" + nextRowOrderText + " where TeamActID=" + teamSettingsGroupID + "; update TeamActivity set [Order]=" + rowOrderText + " where TeamActID=" + nextRowTeamSettingsGroupID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdOrder);
                BindTeamActivity(hdnTeamSettingsID.Value);
                dvDeleteActivity.Style.Add("display", "none");
                dvDeleteActivity.Style.Add("display", "none");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void BindTeamActivity(string teamSettingsID)
    {
        string cmdtext = "select TA.TeamActID,TA.Activity,TS.ActivityGroup,TA.Duration,TA.EndDate,TA.Year,TA.TeamSettingsID,TA.EventID,TA.ChapterID,TA.TeamID,TA.[Order],TA.MemberID,TA.VendorID,TA.VDonorType,TA.Status,TA.FinishDate,(IP.FirstName+' '+IP.LastName) as Name, case when TA.VDonorType='OWN' then OI.ORGANIZATION_NAME  when TA.VDonorType='IND' then (IP1.FirstName+' '+IP1.LastName) when TA.VDonorType='SPOUSE' then (IP1.FirstName+' '+IP1.LastName) else '' end as VendorName from TeamActivity TA inner join TeamSettings TS on (TA.TeamSettingsID=TS.TeamSettingsID)left join IndSpouse IP on(TA.MemberId=IP.AutoMemberId)left join IndSpouse IP1 on (TA.VendorID=IP1.AutoMemberId) left join OrganizationInfo OI on (TA.VendorID=OI.AutoMemberID)  where TA.TeamSettingsID=" + teamSettingsID + " order by [Order] ASC";
        DataSet ds = new DataSet();
        try
        {
            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            if (ds.Tables[0] != null)
            {
                lblTeamActivityGridMsg.Text = "";
                if (ds.Tables[0].Rows.Count > 0)
                {
                    lblTeamActivityGridMsg.Text = "";
                    grdteamActivity.DataSource = ds.Tables[0];
                    grdteamActivity.DataBind();
                    dvCloseActivitytable.Visible = true;
                    dvCalculateEndDate.Style.Add("display", "block");
                }
                else
                {
                    lblTeamActivityGridMsg.Text = "No record found";

                    lblTeamActivityGridMsg.ForeColor = Color.Red;
                    grdteamActivity.DataSource = null;
                    grdteamActivity.DataBind();
                    dvCloseActivitytable.Visible = false;
                    dvCalculateEndDate.Style.Add("display", "none");
                }
            }
            else
            {
                lblTeamActivityGridMsg.Text = "No record found";
                lblTeamActivityGridMsg.ForeColor = Color.Red;
                grdteamActivity.DataSource = null;
                grdteamActivity.DataBind();
            }
            string cmdRowOrerUp = "select max([Order]) as MaxOrder from TeamActivity";
            string cmdRowDown = "select min([Order]) as MinOrder from TeamActivity";
            DataSet rowUpDs = new DataSet();
            int rowOrderUp = 1;
            int rowOrderDown = 0;
            rowUpDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdRowOrerUp);
            if (rowUpDs.Tables[0] != null)
            {
                if (rowUpDs.Tables[0].Rows.Count > 0)
                {
                    if (rowUpDs.Tables[0].Rows[0]["MaxOrder"] != null && rowUpDs.Tables[0].Rows[0]["MaxOrder"].ToString() != "")
                    {
                        rowOrderUp = Convert.ToInt32(rowUpDs.Tables[0].Rows[0]["MaxOrder"].ToString());
                    }
                }
            }
            rowUpDs = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdRowDown);
            if (rowUpDs.Tables[0] != null)
            {
                if (rowUpDs.Tables[0].Rows.Count > 0)
                {
                    if (rowUpDs.Tables[0].Rows[0]["MinOrder"] != null && rowUpDs.Tables[0].Rows[0]["MinOrder"].ToString() != "")
                    {
                        rowOrderDown = Convert.ToInt32(rowUpDs.Tables[0].Rows[0]["MinOrder"].ToString());
                    }
                }
            }
            int rowCount = grdteamActivity.Rows.Count;
            if (rowCount > 0)
            {

                if (rowCount == 1)
                {
                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).Enabled = false;
                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowDown") as ImageButton).Enabled = false;

                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowDown") as ImageButton).Style.Add("cursor", "default");

                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).ToolTip = "";
                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowDown") as ImageButton).ToolTip = "";
                }
                else
                {
                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).Enabled = false;
                    ((ImageButton)grdteamActivity.Rows[rowCount - 1].FindControl("imgActivityRowDown") as ImageButton).Enabled = false;

                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).Style.Add("cursor", "default");
                    ((ImageButton)grdteamActivity.Rows[rowCount - 1].FindControl("imgActivityRowDown") as ImageButton).Style.Add("cursor", "default");

                    ((ImageButton)grdteamActivity.Rows[0].FindControl("imgActivityRowUp") as ImageButton).ToolTip = "";
                    ((ImageButton)grdteamActivity.Rows[rowCount - 1].FindControl("imgActivityRowDown") as ImageButton).ToolTip = "";

                }
            }
            if (activityIndex != -1)
            {
                grdteamActivity.Rows[activityIndex].BackColor = Color.FromName("#EAEAEA");
            }
            if (rowCount > 1)
            {
                dvCalculateEndDate.Style.Add("display", "block;");
                dvaddNewActivity.Style.Add("margin-bottom", "-24px;");
            }
            else
            {
                dvaddNewActivity.Style.Add("margin-bottom", "-14px;");
            }
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }

    protected void btnSaveActivity_Click(object sender, EventArgs e)
    {
        try
        {
            if (validateActivity() == "1")
            {
                lblActivityMsg.Text = "";
                string activityGroupName = txtGroupActivity.Text;
                string teamSettingsID = hdnTeamSettingsID.Value;
                string year = hdnYear.Value;
                string eventID = ddEvent.SelectedValue;
                string chapterID = ddchapter.SelectedValue;
                string teamID = ddTeams.SelectedValue;
                string teamName = ddTeams.SelectedItem.Text;
                string activityGroupID = hdnTeamSettingsID.Value;
                string activityGroup = txtGroupActivity.Text;
                string activity = txtActivity.Text;
                string duration = txtDuration.Text;
                string endDate = txtEndDate.Text;
                string userID = Session["LoginId"].ToString();
                string teamActID = hdnTeamActID.Value;
                string cmdtext = "";
                string msg = "";
                string iDate = endDate;
                string finalEndDate = "";
                string cmdDupCheck = "";
                if (txtActivityVolInCharge.Text == "")
                {
                    hdnMemberID.Value = "";
                }
                if (txtActivityVendor.Text == "")
                {
                    hdnVendorID.Value = "";
                }
                string volunteerId = (hdnMemberID.Value == "" ? "0" : hdnMemberID.Value);
                string vendorId = (hdnVendorID.Value == "" ? "0" : hdnVendorID.Value);
                string volunteername = txtVolunteerInCharge.Text;
                string vendorname = txtVendor.Text;
                string donorType = hdnDonorType.Value;
                string status = (ddlActivityStatus.SelectedValue == "0" ? "" : ddlActivityStatus.SelectedValue);
                if (status == "")
                {
                    status = "null";
                }
                else
                {
                    status = "'" + status + "'";
                }

                if (donorType == "")
                {
                    donorType = "null";
                }
                else
                {
                    donorType = "'" + donorType + "'";
                }

                string volId = volunteerId;
                if (volunteerId == "0")
                {
                    volId = "null";
                }


                string venId = vendorId;
                if (vendorId == "0")
                {
                    venId = "null";
                }

                string finishDate = txtActivityFinishDate.Text;
                if (btnSaveActivity.Text == "Add")
                {
                    //DateTime oDate = DateTime.Parse(iDate);
                    //if (isDateParse == true)
                    //{
                    //    finalEndDate = oDate.Month + "-" + oDate.Day + "-" + oDate.Year;
                    //}
                    if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
                    {
                        finalEndDate = hdnEndDate.Value;

                    }
                    else
                    {
                        finalEndDate = calculateEndDate();
                    }

                    msg = "Activity added successfully";
                    cmdDupCheck = "select count(*) as count from TeamActivity where Activity='" + activity + "' and ActivityGroup='" + activityGroup + "' and ActivityGroupID=" + teamSettingsID + " and Year='" + year + "' and EventID=" + eventID + " and ChapterID=" + chapterID + " and TeamID=" + teamID + "";
                    if (finishDate != "")
                    {
                        cmdtext = "Insert into TeamActivity (TeamSettingsID,Year,EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,Activity,ActivityID,Duration,EndDate,[Order],CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values(" + teamSettingsID + ",'" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + activityGroupName + "'," + teamSettingsID + ",'" + activity + "',(select ISNUll(max(TeamActID),0)+1 from TeamActivity)," + duration + ",'" + finalEndDate + "',(select ISNULL(max([Order]),0)+1 from TeamActivity where TeamSettingsID=" + teamSettingsID + "),GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",'" + txtFinishedDate.Text + "')";
                    }
                    else
                    {
                        cmdtext = "Insert into TeamActivity (TeamSettingsID,Year,EventID,ChapterID,TeamID,TeamName,ActivityGroup,ActivityGroupID,Activity,ActivityID,Duration,EndDate,[Order],CreatedDate,CretaedBy,MemberID,VendorID,VDonorType,Status,FinishDate) values(" + teamSettingsID + ",'" + year + "'," + eventID + "," + chapterID + "," + teamID + ",'" + teamName + "','" + activityGroupName + "'," + teamSettingsID + ",'" + activity + "',(select ISNUll(max(TeamActID),0)+1 from TeamActivity)," + duration + ",'" + finalEndDate + "',(select ISNULL(max([Order]),0)+1 from TeamActivity where TeamSettingsID=" + teamSettingsID + "),GETDATE()," + userID + "," + volId + "," + venId + "," + donorType + "," + status + ",null)";
                    }
                }
                else
                {

                    finalEndDate = calculateEndDate();
                    msg = "Activity updated successfully";
                    cmdDupCheck = "select count(*) as count from TeamActivity where TeamActID<>" + teamActID + " and  Activity='" + activity + "' and ActivityGroup='" + activityGroup + "' and ActivityGroupID=" + teamSettingsID + " and Year='" + year + "' and EventID=" + eventID + " and ChapterID=" + chapterID + " and TeamID=" + teamID + "";
                    if (finishDate != "")
                    {
                        cmdtext = "Update TeamActivity set TeamSettingsID=" + teamSettingsID + ",Year='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + activityGroup + "',ActivityGroupID=" + activityGroupID + ",Activity='" + activity + "',ActivityID=" + teamActID + ",Duration='" + duration + "',EndDate='" + finalEndDate + "',ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate='" + txtFinishedDate.Text + "' where TeamActID=" + teamActID + " and TeamSettingsID=" + teamSettingsID + "";
                    }
                    else
                    {
                        cmdtext = "Update TeamActivity set TeamSettingsID=" + teamSettingsID + ",Year='" + year + "',EventID=" + eventID + ",ChapterID=" + chapterID + ",TeamID=" + teamID + ",TeamName='" + teamName + "',ActivityGroup='" + activityGroup + "',ActivityGroupID=" + activityGroupID + ",Activity='" + activity + "',ActivityID=" + teamActID + ",Duration='" + duration + "',EndDate='" + finalEndDate + "',ModifieDate=GETDATE(),ModifiedBy=" + userID + ",MemberID=" + volId + ",VendorID=" + venId + ",VDonorType=" + donorType + ",Status=" + status + ",FinishDate=null where TeamActID=" + teamActID + " and TeamSettingsID=" + teamSettingsID + "";
                    }
                }
                DataSet ds = new DataSet();
                ds = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, cmdDupCheck);
                if (Convert.ToInt32(ds.Tables[0].Rows[0]["count"].ToString()) > 0)
                {
                    lblActivityMsg.Text = "Duplicate Record Exists!";
                    lblActivityMsg.ForeColor = Color.Red;

                }
                else
                {

                    SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);

                    lblActivityMsg.Text = msg;
                    lblActivityMsg.ForeColor = Color.Blue;
                    txtGroupActivity.Text = "";
                    txtDuration.Text = "";
                    txtEndDate.Text = "";
                    txtActivity.Text = "";
                    txtBeginDate.Text = "";
                    txtActivityFinishDate.Text = "";
                    txtActivityVendor.Text = "";
                    txtActivityVolInCharge.Text = "";
                    ddlActivityStatus.SelectedValue = "0";

                    tblTeamActivity.Style.Add("display", "none");
                    dvDeleteActivity.Style.Add("display", "none");
                    BindTeamActivity(teamSettingsID);
                    pnlActivitySearch.Visible = false;
                    if (btnSaveActivity.Text == "Add")
                    {
                        if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
                        {
                            calculateEndDateScenario1();
                        }
                        lblActivityMsg.Text = msg;
                        lblActivityMsg.ForeColor = Color.Blue;
                    }
                    if (btnSaveActivity.Text == "Update")
                    {
                        if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
                        {
                            recalcluateEndDateScenario1();
                        }
                        lblActivityMsg.Text = msg;
                        lblActivityMsg.ForeColor = Color.Blue;
                        dvDeleteActivity.Style.Add("display", "none");
                        pnlActivitySearch.Visible = false;
                    }
                    hdnDonorType.Value = "";
                    hdnVendorID.Value = "";
                    hdnMemberID.Value = "";
                }

            }
            else
            {
                validateActivity();
            }
        }
        catch (Exception ex)
        {
            hdnDonorType.Value = "";
            hdnVendorID.Value = "";
            hdnMemberID.Value = "";
            throw new Exception(ex.Message);
        }
    }
    public string validateSearch()
    {
        string retVal = "1";
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
        if (ddYear.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select year";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select event";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select chapter";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddTeams.SelectedValue == "0")
        {
            lblErrMsg.Text = "Please select team";
            lblErrMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        return retVal;
    }
    public string validateActivityGroup()
    {
        string retVal = "1";
        if (ddYear.SelectedValue == "0" || ddYear.SelectedValue == "-1")
        {

            lblMsg.Text = "Please select year";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddEvent.SelectedValue == "0")
        {
            lblMsg.Text = "Please select event";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddchapter.SelectedValue == "0")
        {
            lblMsg.Text = "Please select chapter";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddTeams.SelectedValue == "0")
        {
            lblMsg.Text = "Please select team";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (txtActivityGroup.Text == "")
        {
            lblMsg.Text = "Please fill activity group";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (ddlUnitOfTime.SelectedValue == "0")
        {
            lblMsg.Text = "Please select unit of time";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (txtBeginDate.Text == "")
        {
            lblMsg.Text = "Please fill begin date";
            lblMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        //else if (txtVolunteerInCharge.Text == "")
        //{
        //    lblMsg.Text = "Please fill Volunteer in charge";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}
        //else if (txtVendor.Text == "")
        //{
        //    lblMsg.Text = "Please fill Vendor";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}

        //else if (ddlStatus.SelectedItem.Text == "Select")
        //{
        //    lblMsg.Text = "Please select status";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}
        //if (ddlStatus.SelectedValue == "4")
        //{
        //    if (txtFinishedDate.Text == "")
        //    {
        //        lblMsg.Text = "Please fill Finish Date";
        //        lblMsg.ForeColor = Color.Red;
        //        retVal = "-1";
        //    }
        //}
        return retVal;
    }
    public string validateActivity()
    {
        string retVal = "1";
        string date = hdnEndDate.Value;
        if (date != "")
        {
            txtEndDate.Text = date;
        }

        if (txtGroupActivity.Text == "")
        {

            lblActivityMsg.Text = "Please fill activity group";
            lblActivityMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        else if (txtDuration.Text == "")
        {

            lblActivityMsg.Text = "Please fill duration";
            lblActivityMsg.ForeColor = Color.Red;
            retVal = "-1";
        }

        else if (txtActivity.Text == "")
        {
            lblActivityMsg.Text = "Please fill activity";
            lblActivityMsg.ForeColor = Color.Red;
            retVal = "-1";
        }
        //else if (txtActivityVolInCharge.Text == "")
        //{
        //    lblMsg.Text = "Please fill Volunteer in charge";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}
        //else if (txtActivityVendor.Text == "")
        //{
        //    lblMsg.Text = "Please fill Vendor";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}

        //else if (ddlActivityStatus.SelectedItem.Text == "Select")
        //{
        //    lblMsg.Text = "Please fill Vendor";
        //    lblMsg.ForeColor = Color.Red;
        //    retVal = "-1";
        //}
        //if (ddlActivityStatus.SelectedValue == "4")
        //{
        //    if (txtActivityFinishDate.Text == "")
        //    {
        //        lblMsg.Text = "Please fill Finish Date";
        //        lblMsg.ForeColor = Color.Red;
        //        retVal = "-1";
        //    }
        //}
        return retVal;

    }
    protected void ddTeams_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
    }
    protected void ddchapter_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddEvent.SelectedValue == "1")
        {
            ddchapter.SelectedValue = "1";
        }
    }

    public string calculateEndDate()
    {
        string endDate = "";
        string beginDate = "";
        beginDate = hdnFinalDate.Value;
        endDate = hdnEndDate.Value;
        int duration = Convert.ToInt32(txtDuration.Text);
        string result = "";


        DateTime dtEndDate = Convert.ToDateTime(beginDate);
        result = dtEndDate.ToString("MM-dd-yyyy");
        if (hdnGroupDuration.Value == "Day")
        {
            result = Convert.ToString(dtEndDate.AddDays(duration));
        }
        else if (hdnGroupDuration.Value == "Month")
        {
            result = Convert.ToString(dtEndDate.AddMonths(duration));
        }
        else if (hdnGroupDuration.Value == "Week")
        {
            result = Convert.ToString(dtEndDate.AddDays(duration * 7));
        }
        else if (hdnGroupDuration.Value == "Hour")
        {
            result = Convert.ToString(dtEndDate.AddHours(duration));
        }
        else if (hdnGroupDuration.Value == "15 Minutes")
        {
            result = Convert.ToString(dtEndDate.AddMinutes(duration * 15));
        }
        DateTime dtResult = Convert.ToDateTime(result);
        result = dtResult.ToString("MM-dd-yyyy");

        if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
        {

        }
        return result;
    }
    public string calculateEndDateS2()
    {
        int duration = Convert.ToInt32(txtDuration.Text);
        string result = "";
        string endDate = "";
        endDate = hdnFinalDate.Value;
        DateTime dtEndDate = Convert.ToDateTime(endDate);
        result = dtEndDate.ToString("MM-dd-yyyy");
        string startDateAct = "";
        string endDateAct = "";
        if (grdteamActivity.Rows.Count > 0)
        {
            for (int i = 0; i < grdteamActivity.Rows.Count; i++)
            {

            }
            if (hdnGroupDuration.Value == "Day")
            {
                result = Convert.ToString(dtEndDate.AddDays(-duration));
            }
            else if (hdnGroupDuration.Value == "Month")
            {
                result = Convert.ToString(dtEndDate.AddMonths(-duration));
            }
            else if (hdnGroupDuration.Value == "Week")
            {
                result = Convert.ToString(dtEndDate.AddDays(-(duration * 7)));
            }
            else if (hdnGroupDuration.Value == "Hour")
            {
                result = Convert.ToString(dtEndDate.AddHours(-duration));
            }
            else if (hdnGroupDuration.Value == "15 Minutes")
            {
                result = Convert.ToString(dtEndDate.AddMinutes(-(duration * 15)));
            }
            DateTime dtResult = Convert.ToDateTime(result);
            result = dtResult.ToString("MM-dd-yyyy");
        }
        else
        {
            result = result.ToString();
        }
        return result;
    }

    public void calculateEndDateScenario1()
    {
        int i;
        int count = 0;
        int duration = 0;
        string endDate = "";
        DateTime dtEndDate;
        string result = "";
        string cmdText = "";
        string activityID = "";
        string activityGroupID = "";
        int length = grdteamActivity.Rows.Count;
        if (length > 0)
        {
            for (i = length; i <= length; i--)
            {
                if (i == 0)
                {
                    break;
                }
                else
                {
                    if (count > 0)
                    {
                        activityID = grdteamActivity.Rows[i - 1].Cells[4].Text;
                        activityGroupID = ((Label)grdteamActivity.Rows[i - 1].FindControl("lblTeamSettingsID") as Label).Text;
                        dtEndDate = Convert.ToDateTime(endDate);
                        result = dtEndDate.ToString("MM-dd-yyyy");

                        if (hdnGroupDuration.Value == "Day")
                        {
                            result = Convert.ToString(dtEndDate.AddDays(-duration));
                        }
                        else if (hdnGroupDuration.Value == "Month")
                        {
                            result = Convert.ToString(dtEndDate.AddMonths(-duration));
                        }
                        else if (hdnGroupDuration.Value == "Week")
                        {
                            result = Convert.ToString(dtEndDate.AddDays(-(duration * 7)));
                        }
                        else if (hdnGroupDuration.Value == "Hour")
                        {
                            result = Convert.ToString(dtEndDate.AddHours(-duration));
                        }
                        else if (hdnGroupDuration.Value == "15 Minutes")
                        {
                            result = Convert.ToString(dtEndDate.AddMinutes(-(duration * 15)));
                        }
                        DateTime dtResult = Convert.ToDateTime(result);
                        result = dtResult.ToString("MM-dd-yyyy");
                        cmdText = "update TeamActivity set EndDate='" + result + "' where TeamSettingsID=" + activityGroupID + " and TeamActID=" + activityID + "";
                        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        BindTeamActivity(hdnTeamSettingsID.Value);


                    }

                    count++;

                    duration = Convert.ToInt32(grdteamActivity.Rows[i - 1].Cells[7].Text);

                    endDate = ((Label)grdteamActivity.Rows[i - 1].FindControl("lblEndDate") as Label).Text;
                }

            }
        }
        //BindTeamActivity(hdnTeamSettingsID.Value);
    }

    public void recalcluateEndDateScenario1()
    {
        lblActivityMsg.Text = "";
        lblMsg.Text = "";
        lblErrMsg.Text = "";
        lblTeamActivityGridMsg.Text = "";
        int i;
        int count = 0;
        int duration = 0;
        string endDate = "";
        DateTime dtEndDate;
        string result = "";
        string cmdText = "";
        string activityID = "";
        string activityGroupID = "";
        int length = grdteamActivity.Rows.Count;
        if (length > 0)
        {
            for (i = length; i <= length; i--)
            {
                if (i == 0)
                {
                    break;
                }
                else
                {
                    if (count > 0)
                    {

                        activityID = grdteamActivity.Rows[i - 1].Cells[4].Text;
                        activityGroupID = ((Label)grdteamActivity.Rows[i - 1].FindControl("lblTeamSettingsID") as Label).Text;
                        dtEndDate = Convert.ToDateTime(endDate);
                        result = dtEndDate.ToString("MM-dd-yyyy");

                        if (hdnGroupDuration.Value == "Day")
                        {
                            result = Convert.ToString(dtEndDate.AddDays(-duration));
                        }
                        else if (hdnGroupDuration.Value == "Month")
                        {
                            result = Convert.ToString(dtEndDate.AddMonths(-duration));
                        }
                        else if (hdnGroupDuration.Value == "Week")
                        {
                            result = Convert.ToString(dtEndDate.AddDays(-(duration * 7)));
                        }
                        else if (hdnGroupDuration.Value == "Hour")
                        {
                            result = Convert.ToString(dtEndDate.AddHours(-duration));
                        }
                        else if (hdnGroupDuration.Value == "15 Minutes")
                        {
                            result = Convert.ToString(dtEndDate.AddMinutes(-(duration * 15)));
                        }
                        DateTime dtResult = Convert.ToDateTime(result);
                        result = dtResult.ToString("MM-dd-yyyy");
                        cmdText = "update TeamActivity set EndDate='" + result + "' where TeamSettingsID=" + activityGroupID + " and TeamActID=" + activityID + "";
                        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        BindTeamActivity(hdnTeamSettingsID.Value);
                    }
                    else
                    {
                        endDate = hdnEndDate.Value;
                        activityID = grdteamActivity.Rows[i - 1].Cells[4].Text;
                        activityGroupID = ((Label)grdteamActivity.Rows[i - 1].FindControl("lblTeamSettingsID") as Label).Text;
                        //dtEndDate = Convert.ToDateTime(endDate);
                        //result = dtEndDate.ToString("MM-dd-yyyy");
                        cmdText = "update TeamActivity set EndDate='" + endDate + "' where TeamSettingsID=" + activityGroupID + " and TeamActID=" + activityID + "";
                        SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                        BindTeamActivity(hdnTeamSettingsID.Value);
                    }
                    count++;
                    duration = Convert.ToInt32(grdteamActivity.Rows[i - 1].Cells[7].Text);
                    endDate = ((Label)grdteamActivity.Rows[i - 1].FindControl("lblEndDate") as Label).Text;
                }

            }
        }
    }
    protected void btnReCalculateEndDate_Click(object sender, EventArgs e)
    {
        if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
        {
            recalcluateEndDateScenario1();
        }
        else
        {
            recalculateEndDateScenario2();
        }
    }
    public void recalculateEndDateScenario2()
    {
        int i;
        int count = 0;
        int duration = 0;
        string endDate = "";
        DateTime dtEndDate;
        string result = "";
        string cmdText = "";
        string activityID = "";
        string activityGroupID = "";
        int length = grdteamActivity.Rows.Count;
        for (i = 0; i < length; i++)
        {
            if (count > 0)
            {

                activityID = grdteamActivity.Rows[i].Cells[4].Text;
                activityGroupID = ((Label)grdteamActivity.Rows[i].FindControl("lblTeamSettingsID") as Label).Text;
                dtEndDate = Convert.ToDateTime(endDate);
                duration = Convert.ToInt32(grdteamActivity.Rows[i].Cells[7].Text);
                result = dtEndDate.ToString("MM-dd-yyyy");
                if (hdnGroupDuration.Value == "Day")
                {
                    result = Convert.ToString(dtEndDate.AddDays(duration));
                }
                else if (hdnGroupDuration.Value == "Month")
                {
                    result = Convert.ToString(dtEndDate.AddMonths(duration));
                }
                else if (hdnGroupDuration.Value == "Week")
                {
                    result = Convert.ToString(dtEndDate.AddDays(duration * 7));
                }
                else if (hdnGroupDuration.Value == "Hour")
                {
                    result = Convert.ToString(dtEndDate.AddHours(duration));
                }
                else if (hdnGroupDuration.Value == "15 Minutes")
                {
                    result = Convert.ToString(dtEndDate.AddMinutes(duration * 15));
                }
                DateTime dtResult = Convert.ToDateTime(result);
                result = dtResult.ToString("MM-dd-yyyy");
                cmdText = "update TeamActivity set EndDate='" + result + "' where TeamSettingsID=" + activityGroupID + " and TeamActID=" + activityID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                BindTeamActivity(hdnTeamSettingsID.Value);
            }
            else
            {
                endDate = hdnBeginDate.Value;
                activityID = grdteamActivity.Rows[i].Cells[4].Text;
                activityGroupID = ((Label)grdteamActivity.Rows[i].FindControl("lblTeamSettingsID") as Label).Text;
                dtEndDate = Convert.ToDateTime(endDate);
                duration = Convert.ToInt32(grdteamActivity.Rows[i].Cells[7].Text);
                result = dtEndDate.ToString("MM-dd-yyyy");
                if (hdnGroupDuration.Value == "Day")
                {
                    result = Convert.ToString(dtEndDate.AddDays(duration));
                }
                else if (hdnGroupDuration.Value == "Month")
                {
                    result = Convert.ToString(dtEndDate.AddMonths(duration));
                }
                else if (hdnGroupDuration.Value == "Week")
                {
                    result = Convert.ToString(dtEndDate.AddDays(duration * 7));
                }
                else if (hdnGroupDuration.Value == "Hour")
                {
                    result = Convert.ToString(dtEndDate.AddHours(duration));
                }
                else if (hdnGroupDuration.Value == "15 Minutes")
                {
                    result = Convert.ToString(dtEndDate.AddMinutes(duration * 15));
                }
                DateTime dtResult = Convert.ToDateTime(result);
                result = dtResult.ToString("MM-dd-yyyy");
                cmdText = "update TeamActivity set EndDate='" + result + "' where TeamSettingsID=" + activityGroupID + " and TeamActID=" + activityID + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                BindTeamActivity(hdnTeamSettingsID.Value);
            }
            count++;

            endDate = ((Label)grdteamActivity.Rows[i].FindControl("lblEndDate") as Label).Text;
        }
    }

    private void ExportToExcel()
    {
        if (Session["TeamPlanSettings"].ToString() != "")
        {

            DataTable dtTeamPlanSettings = (DataTable)Session["TeamPlanSettings"];
            Response.Clear();
            //Microsoft.Office.Interop.Excel.Workbook workbook = new Microsoft.Office.Interop.Excel.Workbook();
            //workbook.Worksheets.Add("Sample of New Excel");

            Response.AppendHeader("content-disposition", "attachment;filename=TeamPlanningSettings.xls");
            Response.ContentType = "application/ms-excel";
            Response.Write("<table border='1'><tr>");
            foreach (DataColumn dr in dtTeamPlanSettings.Columns)
            {
                Response.Write("<th>" + dr.ColumnName + "</th>");
            }
            Response.Write("</tr>");
            Response.Write(System.Environment.NewLine);


            foreach (DataRow dr in dtTeamPlanSettings.Rows)
            {
                Response.Write("<tr>");
                for (int i = 0; i < dtTeamPlanSettings.Columns.Count; i++)
                {
                    Response.Write("<td>" + dr[i].ToString() + "</td>");
                }

                Response.Write("</tr>");

            }
            Response.Write("</table>");

            Response.Flush();
            Response.SuppressContent = true;
            HttpContext.Current.Response.Flush();
            HttpContext.Current.Response.SuppressContent = true;
            HttpContext.Current.ApplicationInstance.CompleteRequest();


        }
    }


    protected void btnExportToExcel_Click(object sender, EventArgs e)
    {
        ExportExcelAll();

    }

    public void ExportExcelAll()
    {
        DataSet ds = new DataSet();
        string cmdtext = "";
        if (grdTeamSettings.Rows.Count > 0)
        {
            cmdtext = "select Year,[Order],TeamSettingsID,ActivityGroup, ActivityGroupID as 'Act Group ID',UnitOfTime,BegDate,EndDate,(IP.FirstName+' '+IP.LastName) as 'Volunteer', case when TS.VDonorType='OWN' then OI.ORGANIZATION_NAME  when TS.VDonorType='IND' then (IP1.FirstName+' '+IP1.LastName) when TS.VDonorType='SPOUSE' then (IP1.FirstName+' '+IP1.LastName) else '' end as 'Vendor',TS.Status,TS.FinishDate from TeamSettings TS left join IndSpouse IP on(TS.MemberId=IP.AutoMemberId)left join IndSpouse IP1 on (TS.VendorID=IP1.AutoMemberId) left join OrganizationInfo OI on (TS.VendorID=OI.AutoMemberID) where TS.EventID=" + ddEvent.SelectedValue + " and TS.ChapterID=" + ddchapter.SelectedValue + " and TS.TeamID=" + ddTeams.SelectedValue + " and TS.Year='" + ddYear.SelectedValue + "' order by TS.[Order] Asc;";
        }
        if (grdTeamSettings.Rows.Count > 0)
        {
            cmdtext += " select TA.Year,TA.[Order],TA.TeamActID,TA.Activity,TA.ActivityID,TS.ActivityGroup,TS.ActivityGroupID as 'Act Group ID',TA.Duration,TA.EndDate,(IP.FirstName+' '+IP.LastName) as 'Volunteer', case when TA.VDonorType='OWN' then OI.ORGANIZATION_NAME  when TA.VDonorType='IND' then (IP1.FirstName+' '+IP1.LastName) when TA.VDonorType='SPOUSE' then (IP1.FirstName+' '+IP1.LastName) else '' end as 'Vendor',TA.Status,TA.FinishDate from TeamActivity TA inner join TeamSettings TS on (TA.TeamSettingsID=TS.TeamSettingsID)left join IndSpouse IP on(TA.MemberId=IP.AutoMemberId)left join IndSpouse IP1 on (TA.VendorID=IP1.AutoMemberId) left join OrganizationInfo OI on (TA.VendorID=OI.AutoMemberID)  where TA.EventID=" + ddEvent.SelectedValue + " and TA.ChapterID=" + ddchapter.SelectedValue + " and TA.TeamID=" + ddTeams.SelectedValue + " and TA.Year='" + ddYear.SelectedValue + "' order by TA.TeamSettingsID,TA.[Order] ASC";


        }
        try
        {

            ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdtext);
            ds.Tables[0].TableName = "Activity Group";
            if (ds.Tables[1] != null)
            {
                ds.Tables[1].TableName = "Activity";
            }
            DateTime dt = DateTime.Now;
            string month = dt.ToString("MMM");
            string day = dt.ToString("dd");
            string year = dt.ToString("yyyy");
            string monthDay = month + "" + day;
            string teamName = (ddTeams.SelectedItem.Text).Replace(" ", "");
            string filename = "TeamPlanningSettings_" + teamName + "_" + monthDay + "_" + year + ".xls";
            ExcelHelper.ToExcel(ds, filename, Page.Response);

        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnActivityGroupDelete_Click(object sender, EventArgs e)
    {
        ActivityGroupDelete();
    }
    public void ActivityGroupDelete()
    {
        int count = 0;
        string cmdText = "select count(*) as CountSet from TeamActivity where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
        DataSet ds = new DataSet();
        ds = SqlHelper.ExecuteDataset(Application[ConnectionString].ToString(), CommandType.Text, cmdText);

        if (ds.Tables[0] != null)
        {
            if (ds.Tables[0].Rows[0]["CountSet"] != null && ds.Tables[0].Rows[0]["CountSet"].ToString() != "")
            {
                count = Convert.ToInt32(ds.Tables[0].Rows[0]["CountSet"]);
            }
        }
        if (count > 0)
        {
            System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmMsgToDelete();", true);
        }
        else
        {
            if (hdnTeamSettingsID.Value != "")
            {
                cmdText = "Delete from TeamSettings where teamSettingsID=" + hdnTeamSettingsID.Value + "";
                SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
                BindBeeBookSchedule();
                reArrangeActivityGroupOrder();
                lblMsg.Text = "Activity Group deleted successfully";
                lblMsg.ForeColor = Color.Blue;
                tblActivityGroup.Style.Add("display", "none");
                dvActivityGroupDelete.Style.Add("display", "none");
            }
            else
            {
                lblMsg.Text = "Please select Activity Group";
                lblMsg.ForeColor = Color.Red;
            }
        }
    }
    protected void btnConfirmDelete_Click(object sender, EventArgs e)
    {
        string cmdText = "Delete from TeamSettings where TeamSettingsID=" + hdnTeamSettingsID.Value + "; Delete from TeamActivity where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
        if (hdnTeamSettingsID.Value != "")
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            BindBeeBookSchedule();
            grdteamActivity.DataSource = null;
            grdteamActivity.DataBind();
            dvaddNewActivity.Style.Add("display", "none");
            dvCalculateEndDate.Style.Add("display", "none");
            dvTeamActivity.Style.Add("display", "none");
            tblTeamActivity.Style.Add("display", "none");
            reArrangeActivityGroupOrder();
            lblMsg.Text = "Activity Group deleted successfully";
            lblMsg.ForeColor = Color.Blue;
            tblActivityGroup.Style.Add("display", "none");
            dvDeleteActivity.Style.Add("display", "none");
            dvActivityGroupDelete.Style.Add("display", "none");
        }
        else
        {
            lblMsg.Text = "Please select Activity Group";
            lblMsg.ForeColor = Color.Red;
        }
    }
    protected void btnDeleteActivity_Click(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "confirmActivityMshDelete();", true);
    }
    protected void btnConfirmActivityDelete_Click(object sender, EventArgs e)
    {
        ActivityDelete();
    }

    public void ActivityDelete()
    {
        string cmdText = "";
        cmdText = "Delete from TeamActivity where TeamActID=" + hdnTeamActID.Value + "";
        if (hdnTeamActID.Value != "")
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblMsg.Text = "Activity deleted successfully";
            dvActivityGroupDelete.Style.Add("display", "none");
            lblMsg.ForeColor = Color.Blue;
            BindTeamActivity(hdnTeamSettingsID.Value);
            reArrangeOrder();

            tblTeamActivity.Style.Add("display", "none");
            dvDeleteActivity.Style.Add("display", "none");
            if (hdnBeginDate.Value != "" && hdnEndDate.Value != "")
            {
                recalcluateEndDateScenario1();
            }
            else
            {
                recalculateEndDateScenario2();
            }
        }
        else
        {
            lblMsg.Text = "Please select Activity";
            lblMsg.ForeColor = Color.Red;
        }
    }
    public void reArrangeOrder()
    {
        int count = 0;
        string cmdText = "";
        for (int i = 0; i < grdteamActivity.Rows.Count; i++)
        {
            count++;
            string teamActId = grdteamActivity.Rows[i].Cells[4].Text;
            cmdText = "Update TeamActivity set [Order]=" + count + " where TeamActID=" + teamActId + " and TeamSettingsID=" + hdnTeamSettingsID.Value + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        }
        BindTeamActivity(hdnTeamSettingsID.Value);
    }

    public void reArrangeActivityGroupOrder()
    {
        int count = 0;
        string cmdText = "";
        for (int i = 0; i < grdTeamSettings.Rows.Count; i++)
        {
            count++;
            string teamSettingsID = grdTeamSettings.Rows[i].Cells[4].Text;
            cmdText = "Update TeamSettings set [Order]=" + count + " where TeamSettingsID=" + teamSettingsID + "";
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
        }
        BindBeeBookSchedule();
    }
    protected void btnCloseActivityTabel_Click(object sender, EventArgs e)
    {

        grdteamActivity.DataSource = null;
        grdteamActivity.DataBind();
        dvTeamActivity.Style.Add("display", "none");
        dvCalculateEndDate.Style.Add("display", "none");
        dvaddNewActivity.Style.Add("display", "none");
        dvDeleteActivity.Style.Add("display", "none");
        dvDeleteActivity.Style.Add("display", "none");
        dvCloseActivitytable.Visible = false;
        tblTeamActivity.Style.Add("display", "none");
        pnlActivitySearch.Visible = false;
    }
    protected void ddlDonorType_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlDonorType.SelectedValue == "Organization")
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtOrgName.Text = String.Empty;
            txtLastName.Enabled = false;
            txtFirstName.Enabled = false;
            txtOrgName.Enabled = true;
        }
        else
        {
            txtLastName.Text = String.Empty;
            txtFirstName.Text = String.Empty;
            txtLastName.Enabled = true;
            txtFirstName.Enabled = true;
            txtOrgName.Enabled = false;
            txtOrgName.Text = String.Empty;
        }
    }
    protected void btnSearch_onClick(object sender, EventArgs e)
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstName.Text;
        lastName = txtLastName.Text;
        state = ddlState.SelectedItem.Value.ToString();
        email = txtEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = "";
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");
        }

        if (lastName.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
                //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append("  I.lastName like '%" + lastName + "%'");
                // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");
            else
                strSql.Append("  I.Email like '%" + email + "%'");
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
                strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");

        }
        if (firstName.Length > 0 || orgname.Length > 0 || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorType.SelectedValue == "INDSPOUSE")
                SearchMembers(strSql.ToString());
            else
                SearchOrganization(strSqlOrg.ToString());
        }
        else
        {

            lblMsg.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";

            return;
        }
    }
    protected void btnIndClose_onclick(object sender, EventArgs e)
    {
        pIndSearch.Visible = false;
        Panel2.Visible = false;
        Panel4.Visible = false;
        //if ((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
        //{
        //    panel3.Visible = true;
        //}
    }
    private void SearchMembers(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }
    private void SearchOrganization(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        GridMemberDt.DataSource = dt;
        GridMemberDt.DataBind();
        if (Count > 0)
        {
            Panel4.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel4.Visible = false;
        }

    }
    protected void btnVolunteerFind_onClick(object sender, EventArgs e)
    {
        hdnMemberText.Value = "Volunteer";
        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = false;
        Panel2.Visible = false;
        Panel4.Visible = false;
    }
    protected void btnVendorFind_onClick(object sender, EventArgs e)
    {
        hdnMemberText.Value = "Vendor";
        GetStates();
        pIndSearch.Visible = true;
        ddlDonorType.SelectedValue = "INDSPOUSE";
        ddlDonorType.Enabled = true;
        Panel2.Visible = false;
        Panel4.Visible = false;
    }
    private void GetStates()
    {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlState.DataSource = dsStates.Tables[0];
        ddlState.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlState.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlState.DataBind();
        ddlState.Items.Insert(0, new ListItem("Select State", String.Empty));
        ddlState.SelectedIndex = 0;
    }
    protected void GridMemberDt_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int index = int.Parse(e.CommandArgument.ToString());
        GridViewRow row = GridMemberDt.Rows[index];


        hdnDonorType.Value = "";
        if (hdnMemberText.Value == "Volunteer")
        {
            int IncurMemberID = (int)GridMemberDt.DataKeys[index].Value;
            hdnMemberID.Value = IncurMemberID.ToString();
            txtVolunteerInCharge.Text = (GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtVolunteerInCharge.Text = (txtVolunteerInCharge.Text == "&nbsp;" ? " " : txtVolunteerInCharge.Text);

            hdnMemberName.Value = GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text;

        }
        else
        {
            int IncurMemberID = (int)GridMemberDt.DataKeys[index].Value;
            txtVendor.Text = (GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtVendor.Text = (txtVendor.Text == "&nbsp;" ? " " : txtVendor.Text);
            hdnDonorType.Value = GridMemberDt.Rows[index].Cells[4].Text;
            hdnVendorID.Value = IncurMemberID.ToString();
            hdnMemberName.Value = GridMemberDt.Rows[index].Cells[1].Text + " " + GridMemberDt.Rows[index].Cells[2].Text;

        }
        pIndSearch.Visible = false;
    }

    protected void ddlDonorActivity_SelectedIndexChanged(object sender, EventArgs e)
    {

        if (ddlDonorType.SelectedValue == "Organization")
        {
            txtLastnameActivity.Text = String.Empty;
            txtFirstnameActivity.Text = String.Empty;
            txtOrganizationName.Text = String.Empty;
            txtLastnameActivity.Enabled = false;
            txtFirstnameActivity.Enabled = false;
            txtOrganizationName.Enabled = true;
        }
        else
        {
            txtLastnameActivity.Text = String.Empty;
            txtFirstnameActivity.Text = String.Empty;
            txtLastnameActivity.Enabled = true;
            txtFirstnameActivity.Enabled = true;
            txtOrganizationName.Enabled = false;
            txtOrganizationName.Text = String.Empty;
        }
    }
    protected void btnActivitVolSearch_onClick(object sender, EventArgs e)
    {
        string firstName = string.Empty;
        string lastName = string.Empty;
        string state = string.Empty;
        string email = string.Empty;
        string orgname = string.Empty;
        StringBuilder strSql = new StringBuilder();
        StringBuilder strSqlOrg = new StringBuilder();
        firstName = txtFirstnameActivity.Text;
        lastName = txtLastnameActivity.Text;
        state = ddlSate.SelectedItem.Value.ToString();
        email = txtActivityEmail.Text;
        orgname = txtOrgName.Text;
        //if (state.Length < 1)
        //{
        //    lblIndSearch.Text = "Please Select State";
        //    lblIndSearch.Visible = true;
        //    return;
        //}
        //else
        //{
        //    lblIndSearch.Text = "";
        //    lblIndSearch.Visible = false;
        //}

        if (orgname.Length > 0)
        {
            strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + orgname + "%'");
        }

        if (firstName.Length > 0)
        {
            strSql.Append("  I.firstName like '%" + firstName + "%'");
        }

        if (lastName.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.lastName like '%" + lastName + "%'");
                //strSqlOrg.Append(" AND O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
            else
            {
                strSql.Append("  I.lastName like '%" + lastName + "%'");
                // strSqlOrg.Append(" O.ORGANIZATION_NAME like '%" + lastName + "%'");
            }
        }

        if (state.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
            {
                strSql.Append(" and I.State like '%" + state + "%'");
            }
            else
            {
                strSql.Append("  I.State like '%" + state + "%'");
            }
        }

        if (email.Length > 0)
        {
            int length = strSql.ToString().Length;
            if (length > 0)
                strSql.Append(" and I.Email like '%" + email + "%'");
            else
                strSql.Append("  I.Email like '%" + email + "%'");
        }

        if (state.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.State like '%" + state + "%'");
            else
                strSqlOrg.Append(" O.State like '%" + state + "%'");
        }

        if (email.Length > 0)
        {
            int length = strSqlOrg.ToString().Length;
            if (length > 0)
                strSqlOrg.Append(" and O.EMAIL like '%" + email + "%'");
            else
                strSqlOrg.Append(" O.EMAIL like '%" + email + "%'");

        }
        if (firstName.Length > 0 || orgname.Length > 0 || lastName.Length > 0 || email.Length > 0 || state.Length > 0)
        {
            strSql.Append(" order by I.lastname,I.firstname");
            if (ddlDonorActivity.SelectedValue == "INDSPOUSE")
                SearchMembersActivity(strSql.ToString());
            else
                SearchOrganizationActivity(strSqlOrg.ToString());
        }
        else
        {

            lblMsg.Text = "Please Enter Email/ Organization Name / First Name / Last Name / state";

            return;
        }
    }
    protected void btnActivityVolClose_onclick(object sender, EventArgs e)
    {
        pnlActivitySearch.Visible = false;
        Panel2.Visible = false;
        Panel4.Visible = false;
        //if ((ddlChapters.SelectedIndex != 0) && (ddlYear.SelectedIndex != 0))
        //{
        //    panel3.Visible = true;
        //}
    }
    private void SearchMembersActivity(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, " select 1, I.AutoMemberID,I.Firstname, I.lastname, I.email, I.Hphone,I.ADDRESS1,I.CITY,I.state,I.Zip,Ch.ChapterCode,I.DonorType   from IndSpouse  I left Join Chapter Ch On I.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        grdVolTeamActivity.DataSource = dt;
        grdVolTeamActivity.DataBind();
        if (Count > 0)
        {
            Panel2.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel2.Visible = false;
        }

    }
    private void SearchOrganizationActivity(string sqlSt)
    {
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, "select 0, O.AutoMemberID,O.ORGANIZATION_NAME as Firstname, '' as lastname, O.email, O.PHONE as Hphone,O.ADDRESS1,O.CITY,O.state,O.Zip,Ch.ChapterCode,'OWN' as DonorType   from OrganizationInfo  O left Join Chapter Ch On O.ChapterID = Ch.ChapterID WHERE " + sqlSt);
        DataTable dt = ds.Tables[0];
        int Count = dt.Rows.Count;
        DataView dv = new DataView(dt);
        grdVolTeamActivity.DataSource = dt;
        grdVolTeamActivity.DataBind();
        if (Count > 0)
        {
            Panel2.Visible = true;
            lblIndSearch.Text = "";
            lblIndSearch.Visible = false;
        }
        else
        {
            lblIndSearch.Text = "No match found";
            lblIndSearch.Visible = true;
            Panel2.Visible = false;
        }

    }
    protected void btnActivityVolFind_onClick(object sender, EventArgs e)
    {
        hdnMemberText.Value = "Volunteer";
        GetStatesActtivity();
        pnlActivitySearch.Visible = true;
        ddlDonorActivity.SelectedValue = "INDSPOUSE";
        ddlDonorActivity.Enabled = false;
        Panel2.Visible = false;
        Panel4.Visible = false;
    }
    protected void btnActivityVendorFind_onClick(object sender, EventArgs e)
    {
        hdnMemberText.Value = "Vendor";
        GetStatesActtivity();
        pnlActivitySearch.Visible = true;
        ddlDonorActivity.SelectedValue = "INDSPOUSE";
        ddlDonorActivity.Enabled = true;
        Panel2.Visible = false;
        Panel4.Visible = false;
    }
    private void GetStatesActtivity()
    {
        DataSet dsStates;
        dsStates = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.StoredProcedure, "usp_GetStates");
        ddlSate.DataSource = dsStates.Tables[0];
        ddlSate.DataTextField = dsStates.Tables[0].Columns["Name"].ToString();
        ddlSate.DataValueField = dsStates.Tables[0].Columns["StateCode"].ToString();
        ddlSate.DataBind();
        ddlSate.Items.Insert(0, new ListItem("Select State", String.Empty));
        ddlSate.SelectedIndex = 0;
    }
    protected void grdVolTeamActivity_RowCommand(object sender, GridViewCommandEventArgs e)
    {

        int index = int.Parse(e.CommandArgument.ToString());
        GridViewRow row = grdVolTeamActivity.Rows[index];


        hdnDonorType.Value = "";
        if (hdnMemberText.Value == "Volunteer")
        {
            int IncurMemberID = (int)grdVolTeamActivity.DataKeys[index].Value;
            hdnMemberID.Value = IncurMemberID.ToString();
            txtActivityVolInCharge.Text = (grdVolTeamActivity.Rows[index].Cells[1].Text + " " + grdVolTeamActivity.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtActivityVolInCharge.Text = (txtActivityVolInCharge.Text == "&nbsp;" ? " " : txtActivityVolInCharge.Text);

            hdnMemberName.Value = grdVolTeamActivity.Rows[index].Cells[1].Text + " " + grdVolTeamActivity.Rows[index].Cells[2].Text;

        }
        else
        {
            int IncurMemberID = (int)grdVolTeamActivity.DataKeys[index].Value;
            txtActivityVendor.Text = (grdVolTeamActivity.Rows[index].Cells[1].Text + " " + grdVolTeamActivity.Rows[index].Cells[2].Text).Replace("&amp;", " ").Replace("&nbsp;", " ");
            txtActivityVendor.Text = (txtActivityVendor.Text == "&nbsp;" ? " " : txtActivityVendor.Text);
            hdnDonorType.Value = grdVolTeamActivity.Rows[index].Cells[4].Text;
            hdnVendorID.Value = IncurMemberID.ToString();
            hdnMemberName.Value = grdVolTeamActivity.Rows[index].Cells[1].Text + " " + grdVolTeamActivity.Rows[index].Cells[2].Text;

        }
        pIndSearch.Visible = false;
        pnlActivitySearch.Visible = false;
    }


    protected void btnVolunteerDelete_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "volunteerDelete();", true);
    }

    protected void btnVendorDelete_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "vendorDelete();", true);
    }

    protected void btnActVolunteerDelete_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "actVolunteerDelete();", true);
    }

    protected void btnActVendorDelete_onClick(object sender, EventArgs e)
    {
        System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "actVendorDelete();", true);

    }

    protected void btnVolunteerConfirm_Click(object sender, EventArgs e)
    {
        string cmdText = "";
        cmdText = "update TeamSettings set MemberID=null where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
        try
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblMsg.Text = "Volunteer in Charge deleted successfully";
            lblMsg.ForeColor = Color.Blue;

            txtVolunteerInCharge.Text = "";
            btnVolunteerDelete.Visible = false;
            BindBeeBookSchedule();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnVendorConfirm_Click(object sender, EventArgs e)
    {
        string cmdText = "";
        cmdText = "update TeamSettings set VendorID=null,vDonorType='' where TeamSettingsID=" + hdnTeamSettingsID.Value + "";
        try
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblMsg.Text = "Vendor deleted successfully";
            lblMsg.ForeColor = Color.Blue;
            btnVendorDelete.Visible = false;

            txtVendor.Text = "";
            BindBeeBookSchedule();
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnActVolunteerConfirm_Click(object sender, EventArgs e)
    {
        string cmdText = "";
        cmdText = "update TeamActivity set MemberID=null where TeamActID=" + hdnTeamActID.Value + "";
        try
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);
            lblActivityMsg.Text = "Volunteer in Charge deleted successfully";
            lblActivityMsg.ForeColor = Color.Blue;
            txtActivityVolInCharge.Text = "";
            btnActVolunteerDelete.Visible = false;
            BindTeamActivity(hdnTeamSettingsID.Value);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }
    }
    protected void btnActVendorConfirm_Click(object sender, EventArgs e)
    {
        string cmdText = "";
        cmdText = "update TeamActivity set VendorID=null,vDonorType='' where TeamActID=" + hdnTeamActID.Value + "";
        try
        {
            SqlHelper.ExecuteNonQuery(Application[ConnectionString].ToString(), CommandType.Text, cmdText);

            lblActivityMsg.Text = "Vendor deleted successfully";
            lblActivityMsg.ForeColor = Color.Blue;
            btnActVendorDelete.Visible = false;
            txtActivityVendor.Text = "";
            BindTeamActivity(hdnTeamSettingsID.Value);
        }
        catch (Exception ex)
        {
            throw new Exception(ex.Message);
        }

    }
    protected void ddlActivityStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlActivityStatus.SelectedValue == "Finished")
        {

            txtActivityFinishDate.Enabled = true;
        }
        else
        {
            txtActivityFinishDate.Enabled = false;
        }

    }
    protected void ddlStatus_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlStatus.SelectedValue == "Finished")
        {
            txtFinishedDate.Enabled = true;
        }
        else
        {
            txtFinishedDate.Enabled = false;
        }

    }
}