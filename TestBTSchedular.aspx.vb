﻿Imports Braintree
Imports Microsoft.ApplicationBlocks.Data
Imports System.IO
Imports System.Net.Mail
Imports System.Globalization

Partial Class TestBTSchedular
    Inherits System.Web.UI.Page

    Dim strLogFile As String = System.Configuration.ConfigurationManager.AppSettings("logFilePath").ToString()
    Dim gateway As BraintreeGateway
    Public Function writeToLogFile(logMessage As String) As String
        Dim strPath As String = Server.MapPath(strLogFile)
        Try
            Dim strLogMessage As String = String.Empty
            Dim swLog As StreamWriter
            strLogMessage = String.Format("{0}: {1}", DateTime.Now, logMessage)
            If (Not File.Exists(strLogFile)) Then
                swLog = New StreamWriter(Server.MapPath(strLogFile))
            Else
                swLog = File.AppendText(strLogFile)
            End If
            swLog.WriteLine(strLogMessage)
            swLog.WriteLine()
            swLog.Close()
        Catch ex As Exception
            Return ""
        End Try
        Return strPath
    End Function

    'Public Sub Import(ByVal Rows As Generic.List(Of Generic.List(Of String)))
    '    For Each Row As Generic.List(Of String) In Rows
    '        ImportRow(Row)
    '    Next
    'End Sub
    Public Function UpdateChargeDetails() As String

        Dim strLogMessage As String, cmdText As String, strStartDate As String
        strStartDate = "03/24/2015"
        strLogMessage = "================= Log Details- " & Now & "===================\n"
        Try
            Dim BT_PaymentRef As String, BT_TransId As String, BT_RefundRef As String, BT_ETG_Response As String
            Dim BT_Token As String, BT_Brand As String, ccF, ccM, ccL As String, MS_TransDate As String, BT_DisbDate As String
            Dim EMail, CardHName As String, TransType, CreatedBy As String, CreateDate As String, BT_CustomerId As String
            Dim MS_Amount As Decimal
            ' Brain Tree Gateway
            Dim BT As NameValueCollection = CType(ConfigurationManager.GetSection("BT/crd"), NameValueCollection)
            gateway = New BraintreeGateway(Braintree.Environment.PRODUCTION, BT("MerchantId"), BT("PublicKey"), BT("PrivateKey"))
            'gateway.SettlementBatchSummary.Generate(


            'Dim Result = gateway.SettlementBatchSummary.Generate(New Date(2015, 3, 24))

            'If Result.IsSuccess() Then
            '    Dim userinfos As Generic.List(Of String) = Result.Target.Records
            '    Response.Write(userinfos.Item(0))

            'End If

            ' Dim gateway As BraintreeGateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
            Dim ccReq As CreditCardRequest = New CreditCardRequest()
            Dim ccAddrReq As CreditCardAddressRequest = New CreditCardAddressRequest()
            Dim tReq As TransactionRequest = New TransactionRequest()
            Dim search As New TransactionSearchRequest()
            search.CreatedAt.Between(Date.ParseExact(strStartDate, "MM/dd/yyyy", CultureInfo.InvariantCulture), DateTime.Now.AddDays(-1))
            Dim coll As ResourceCollection(Of Transaction) = gateway.Transaction.Search(search)
            Dim t As Transaction
            Dim isValid As Boolean = False
            For Each t In coll
                isValid = True
                MS_Amount = Nothing
                MS_TransDate = ""
                ccF = ""
                ccM = ""
                ccL = ""
                BT_Brand = ""
                BT_ETG_Response = ""
                BT_PaymentRef = ""
                BT_TransId = ""
                BT_CustomerId = ""
                BT_RefundRef = ""
                BT_Token = ""
                EMail = ""
                CardHName = ""
                BT_RefundRef = ""
                TransType = ""
                CreateDate = ""
                BT_DisbDate = ""
                CreatedBy = ""

                BT_TransId = t.Id
                BT_CustomerId = t.Customer.Id
                BT_PaymentRef = t.Id + "-" + t.Customer.Id
                If Not t.RefundedTransactionId Is Nothing Then
                    BT_RefundRef = t.RefundedTransactionId + "-" + t.Customer.Id
                End If
                BT_Brand = t.CreditCard.CardType.ToString
                BT_Token = t.CreditCard.Token
                Try
                    MS_Amount = Convert.ToDecimal(t.Amount)
                    ccF = Left(t.CreditCard.MaskedNumber.ToString, 4)
                    ccM = Right(Left(t.CreditCard.MaskedNumber.ToString, 6), 2)
                    ccL = Right(t.CreditCard.MaskedNumber.ToString, 4)
                    If t.Customer.Email Is Nothing Then
                        EMail = ""
                    Else
                        EMail = t.Customer.Email.ToString
                    End If
                    If Not t.CreditCard.CardholderName Is Nothing Then
                        CardHName = t.CreditCard.CardholderName.ToString
                    Else
                        CardHName = ""
                    End If
                    MS_TransDate = t.CreatedAt
                    BT_ETG_Response = t.ProcessorAuthorizationCode
                    If Not t.DisbursementDetails Is Nothing Then
                        BT_DisbDate = t.DisbursementDetails.DisbursementDate.ToString
                    End If
                Catch ex As Exception
                    strLogMessage = strLogMessage & (" \n\r 0. Error on Brain Tree \n\r \r" & ex.ToString)
                End Try
                cmdText = "select count(*) from MSChargeTemp where PaymentReference='" & BT_PaymentRef & "'"
                strLogMessage = strLogMessage & "\n 1. DUPLICATE CHECKING \n \r\r Payment Reference :" & BT_PaymentRef
                Dim iCnt As Integer = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, cmdText)
                cmdText = "select ETG_Response,TransType, CreateDate,CreatedBy from CCSubmitLog where paymentreference='" + BT_PaymentRef + "'"
                Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmdText)

                If ds.Tables(0).Rows.Count > 0 Then
                    TransType = ds.Tables(0).Rows(0)("TransType").ToString
                    CreateDate = ds.Tables(0).Rows(0)("CreateDate").ToString
                    CreatedBy = ds.Tables(0).Rows(0)("CreatedBy").ToString
                End If
                If iCnt > 0 Then
                    cmdText = "Update MSChargeTemp Set MS_TransDate=CONVERT(DATETIME,'" & MS_TransDate & "',103),DisbDate=CONVERT(DATETIME,"
                    If BT_DisbDate = "" Then
                        cmdText = cmdText & "NULL"
                    Else
                        cmdText = cmdText & "'" & BT_DisbDate & "'"
                    End If
                    cmdText = cmdText & ",103),ID='" & BT_TransId & "',CustomerId='" & BT_CustomerId & "',RefundReference='" & BT_RefundRef & "', Brand='" & BT_Brand & "' , Token='" & BT_Token & "',TransType='" & TransType & "',Email='" & EMail & "', CHName='" & CardHName & "',MS_CN1='" & ccF & "', MS_CN2='" & ccM & "', MS_CN3='" & ccL & "',CreateDate=CONVERT(DATETIME," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",103) , CreatedBy=" & IIf(CreatedBy = "", "NULL", CreatedBy) & ", ModifiedDate=getdate(), MS_Amount=" & MS_Amount & ", AuthCode=" & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & " where PaymentReference = '" & BT_PaymentRef & "'"
                    strLogMessage = strLogMessage & ("\n 2. UPDATE \n\r PaymentReference : " & BT_PaymentRef)
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                Else
                    'cmdText = "insert into MSCharge(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode) values (CONVERT(DATETIME,'" & MS_TransDate & "',103) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",103),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & TransType & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',Convert(Datetime," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",103)," & IIf(CreatedBy = "", "NULL", CreatedBy) & ", " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ") "
                    cmdText = "insert into MSChargeTemp(MS_TransDate,DisbDate,ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName, MS_CN1, MS_CN2, MS_CN3,CreateDate,CreatedBy,AuthCode) values (CONVERT(DATETIME,'" & MS_TransDate & "',101) ,CONVERT(DATETIME," & IIf(BT_DisbDate = "", "NULL", "'" & BT_DisbDate & "'") & ",101),'" & BT_TransId & "','" & BT_CustomerId & "','" & BT_PaymentRef & "', '" & BT_RefundRef & "'," & MS_Amount & ", '" & BT_Brand & "' , '" & BT_Token & "','" & TransType & "','" & EMail & "','" & CardHName & "','" & ccF & "', '" & ccM & "', '" & ccL & "',Convert(Datetime," & IIf(CreateDate = "", "NULL", "'" & CreateDate & "'") & ",101)," & IIf(CreatedBy = "", "NULL", CreatedBy) & ", " & IIf(BT_ETG_Response = "", "NULL", "'" & BT_ETG_Response & "'") & ") "
                    strLogMessage = strLogMessage & (" \n 3. INSERT \n\r PaymentReference: " & BT_PaymentRef)
                    SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, cmdText)
                End If
            Next
            strLogMessage = strLogMessage & "\n 4. Records exists in Brain Tree : " & isValid
        Catch ex1 As Exception
            strLogMessage = strLogMessage & (" \n 5. Error on Brain Tree Web Service \n\r\r" & ex1.ToString)
        End Try
        Return writeToLogFile(strLogMessage)

    End Function

    Protected Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        gateway = New BraintreeGateway(Braintree.Environment.SANDBOX, "bgfws8vsfvrp293b", "k6jpvm6fh7rpkjjy", "3c415cc5b8720eab9ba78f3df340eea8")
        Dim wh As WebhookNotification = gateway.WebhookNotification.Parse(Request.Params("bt_signature"), Request.Params("bt_payload"))
        Dim msg As String = String.Format("[Webhook Received {0}] | Kind: {1} | Subscription: {2}", wh.Timestamp.Value, wh.Kind, wh.Subscription.Id)
        Response.Write(msg)

        'strLogPath = UpdateChargeDetails()
        'Dim str As String
        'If strLogPath <> "" Then
        '    str = "Success"
        'Else
        '    str = "Failed"
        'End If
        'SendMessage("Brain Tree Schedular", "Disbursement Details", "")
        'Try
        '    Dim cmd As String = "select convert(varchar(10),MS_TransDate,101) MS_TransDate,convert(varchar(10),DisbDate,101) DisbDate, ID,CustomerId,PaymentReference,RefundReference,MS_Amount, Brand, Token,TransType,Email,CHName from MSChargeTemp where MS_TransDate between convert(datetime,'03/24/2015',101) and (getdate()-1) order by MS_TransDate"
        '    Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, cmd)
        '    gvBTDetails.DataSource = ds.Tables(0)
        '    gvBTDetails.DataBind()
        'Catch ex As Exception
        '    '    Response.Write("Err :" & ex.ToString)
        'End Try
    End Sub
    Dim strLogPath As String
    Private Sub SendMessage(ByVal sSubject As String, ByVal sBody As String, ByVal sMailTo As String)
        'Build Email Message
        Dim email As New MailMessage
        email.From = New MailAddress("nsfcontests@northsouth.org")
        email.To.Add("bindhu.rajalakshmi@capestart.com")
        email.Subject = sSubject
        email.IsBodyHtml = True
        email.DeliveryNotificationOptions = DeliveryNotificationOptions.OnFailure
        email.Body = sBody
        'TODO Need to be fixed to get the Attachments file
        Dim oAttch As Net.Mail.Attachment = New Net.Mail.Attachment(strLogPath)
        email.Attachments.Add(oAttch)

        'leave blank to use default SMTP server
        Dim ok As Boolean = True
        Dim client As New SmtpClient()
        'Dim host As String = System.Configuration.ConfigurationManager.AppSettings.Get("SMTPHost")
        'client.Host = host
        Try
            client.Send(email)
        Catch e As Exception
            'lblMessage.Text = e.Message.ToString
            ok = False
        End Try
    End Sub

    Protected Sub GridView1_SelectedIndexChanged(sender As Object, e As EventArgs) Handles gvBTDetails.SelectedIndexChanged

    End Sub
End Class
