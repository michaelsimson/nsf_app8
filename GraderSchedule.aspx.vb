﻿
Imports System.Reflection
Imports NativeExcel
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Partial Class GraderSchedule
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("Maintest.aspx")
        End If
        If Not IsPostBack() Then
            GetContestYear(ddlYear)
            GetEvent()
            GetChapter(ddlChapter)
            GetPurpose()
            If Convert.ToInt32(Session("selChapterID")) > 0 Then
                ddlChapter.SelectedValue = Session("selChapterID")
                ddlChapter.SelectedItem.Text = Session("selChapterName").ToString()
                ddlChapter.Enabled = False
                If (ddlChapter.SelectedValue = "1") Then
                    ddlEvent.SelectedValue = "1"
                Else
                    ddlEvent.SelectedValue = "2"
                End If
                ddlEvent.Enabled = False
                'GetProductGroup(ddlProductGroup)
                GetDate()
                ' LoadBldgID()
            End If
        End If
    End Sub
    Private Sub GetContestYear(ByVal ddlYear As DropDownList)
        ddlYear.Items.Insert(0, Convert.ToInt32(DateTime.Now.Year + 1))
        ddlYear.Items.Insert(1, Convert.ToInt32(DateTime.Now.Year))
        ddlYear.Items.Insert(2, Convert.ToInt32(DateTime.Now.Year - 1))
        ddlYear.Items(1).Selected = True
    End Sub
    Private Sub GetEvent()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct EventId,Name From Event Where EventId in(1,2)")
        ddlEvent.DataSource = ds
        ddlEvent.DataBind()
        ddlEvent.Items.Insert(0, New ListItem("Select Event", "-1"))
        ddlEvent.SelectedIndex = 0
    End Sub
    Private Sub GetPurpose()
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct(RTRIM(Purpose)) as Purpose from ContestResourceList Where Purpose Is Not Null") '"Select Distinct(Purpose) from ContestResourceList C Where ContestResourceListID=(Select ContestResourceListID from ContestResourceList R Where C.ContestResourceListID= R.ContestResourceListID) And Purpose is Not Null")
        ddlPurpose.DataSource = ds
        ddlPurpose.DataBind()
        ddlPurpose.Items.Insert(0, "Select Purpose")
        ddlPurpose.SelectedIndex = 3
    End Sub

    Private Sub GetChapter(ByVal ddlChapter As DropDownList)
        Dim ds_Chapter As DataSet
        ddlChapter.Enabled = True
        ds_Chapter = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.StoredProcedure, "usp_GetChapterAll")
        ddlChapter.DataSource = ds_Chapter
        ddlChapter.DataBind()
        ddlChapter.Items.Insert(0, New ListItem("Select Chapter", "-1"))
        ddlChapter.SelectedIndex = 0
    End Sub
    Private Sub GetProductGroup(ByVal ddlObject As DropDownList)
        Dim StrSQL As String = "Select Distinct ProductGroupId,ProductGroupCode from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedValue & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlObject.DataSource = ds
            ddlObject.DataBind()

            ddlObject.Items.Insert(0, "Select ProductGroup")
            ddlObject.SelectedIndex = 0
        Else
            lblErr.Text = "No ProductGroups assigned"
        End If
       
    End Sub
    Private Sub GetDate()
        Dim StrDateSQL As String = ""
        lblErr.Text = ""
        StrDateSQL = "Select Distinct convert(VarChar(10), ContestDate, 101) as Date,ContestDate  From Contest Where Contest_Year=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and nsfChapterID=" & ddlChapter.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrDateSQL)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlDate.DataSource = ds
            ddlDate.DataBind()

            ddlDate.Items.Insert(0, "Select ContestDate")
            ddlDate.SelectedIndex = 0
        Else
            lblErr.Text = "No Contest Dates Present for the Selections "
        End If
      
    End Sub
    Protected Sub ddlEvent_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlEvent.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedValue = 1 Then
            ddlChapter.SelectedValue = 1
            ddlChapter.Enabled = False
            'GetProductGroup(ddlProductGroup)
            'GetDate()
            ddlPurpose.SelectedIndex = 3
            ClearDropDowns()
            LoadRolesVol()
            'LoadBldgID()
        Else
            GetChapter(ddlChapter)
        End If
    End Sub

    Protected Sub ddlChapter_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlChapter.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        If ddlChapter.SelectedValue = 1 Then
            ddlEvent.SelectedValue = 1
        End If
        'GetProductGroup(ddlProductGroup)
        GetDate()
        ddlPurpose.SelectedIndex = 3
        ClearDropDowns()
        LoadRolesVol()
        'LoadBldgID()
    End Sub
    Protected Sub ddlPurpose_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPurpose.SelectedIndexChanged
        ClearDropDowns()
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event/Chapter"
            Exit Sub
        End If
        LoadRoomSchData()
    End Sub
    Private Sub LoadRoomSchData()

        Dim SQLRoomsch As String = "Select * from Roomschedule Where ContestYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterId=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, SQLRoomsch)
        Try
            GetDate()
            'GetProductGroup(ddlProductGroup) '
            'LoadBldgID()
            lblAddUPdate.Text = ""

        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub
    Protected Sub ddlProductGroup_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProductGroup.SelectedIndexChanged

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        LoadProduct(ddlProductGroup, ddlProduct)
        ' LoadPhase()

    End Sub
    Protected Sub ddlProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        LoadPhase()
    End Sub
    Private Sub LoadProduct(ByVal ddlObject As DropDownList, ByVal ddlProductObj As DropDownList)

        Dim StrSQL As String = "Select Distinct ProductCode,ProductId from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlObject.SelectedValue & " order by ProductId" 'and BldgId='" & ddlBldgID.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlProductObj.DataSource = ds
        'ddlProductObj.DataTextField = "ProductCode"
        'ddlProductObj.DataValueField = "ProductId"
        ddlProductObj.DataBind()

        ddlProductObj.Items.Insert(0, New ListItem("Select Product ", "0"))
        ddlProductObj.SelectedIndex = 0

    End Sub

    Private Sub LoadPhase()

        Dim StrSQL As String = "Select Distinct Phase from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, StrSQL)
        ddlPhase.DataSource = ds
        'ddlPhase.DataTextField = "Phase"
        'ddlPhase.DataValueField = "Phase"
        ddlPhase.DataBind()

        ddlPhase.Items.Insert(0, New ListItem("Select Phase ", "0"))
        ddlPhase.SelectedIndex = 0

        LoadDGRoomSchedule()

    End Sub

    Protected Sub ddlBldgID_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlBldgID.SelectedIndexChanged
        TrCopy.Visible = False
        ddlRoomNo.Enabled = True
        ddlRoomNo.Items.Clear()
        LoadRoomNumber(ddlBldgID.SelectedValue)
        ClearRoles()
        LoadRolesVol()
        'LoadRoleData()
    End Sub
    Private Sub LoadRoomNumber(ByVal ddlBldgIdVal As String)

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,RoomNumber from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgIdVal & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)

        ddlSeqNo.DataSource = ds
        ddlSeqNo.DataBind()

        ddlSeqNo.Items.Insert(0, "Select SeqNo")
        ddlSeqNo.SelectedIndex = 0

        ddlRoomNo.DataSource = ds
        ddlRoomNo.DataBind()

        ddlRoomNo.Items.Insert(0, "Select RoomNumber")
        ddlRoomNo.SelectedIndex = 0

        'GetProductGroup()
        'GetDate()
    End Sub

    Protected Sub ddlSeqNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlSeqNo.SelectedIndexChanged

        lblAddUPdate.Text = ""
        'lblErr.Text = ""
        TrCopy.Visible = False

        Dim StrSeqNoSQl As String = "Select Distinct RoomNumber,StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and SeqNo='" & ddlSeqNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrSeqNoSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber")))
            ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
            ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
        Else
            'ddlRoomNo.Enabled = False
            ddlRoomNo.SelectedIndex = 0
            ddlStartTime.SelectedIndex = 0
            ddlEndTime.SelectedIndex = 0
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
        End If
        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Protected Sub ddlRoomNo_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlRoomNo.SelectedIndexChanged

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False

        Dim StrRoomNoSQl As String = "Select Distinct SeqNo,StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo")))
            ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
            ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))
        Else
            ddlSeqNo.SelectedIndex = 0
            ddlStartTime.SelectedIndex = 0
            ddlEndTime.SelectedIndex = 0
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule for the Selected Contest date"
        End If
        ClearRoles()
        LoadRolesVol()
        LoadRoleData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadBldgID()
        ddlRoomNo.Enabled = True

        Dim StrBldgSQlEval As String = ""
        Dim StrBldgSQl As String = ""
        lblErr.Text = ""
        StrBldgSQl = "Select Distinct BldgID from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)

        ' ddlRoomNo.Items.Clear()
        ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrBldgSQl)
        If ds.Tables(0).Rows.Count > 0 Then
            ddlBldgID.DataSource = ds
            ddlBldgID.DataTextField = "BldgID"
            ddlBldgID.DataValueField = "BldgID"
            ddlBldgID.DataBind()

            ddlBldgID.Items.Insert(0, "Select BldgID")
            ddlBldgID.SelectedIndex = 0

        Else
            lblErr.Text = "No buildings assigned"
        End If
    End Sub
    Private Sub CheckRoomReqData()
        'Dim RoomGStart As Integer = 0
        lblErr.Text = ""
        Dim RoomGCount As Integer = 12
        For i As Integer = 0 To RoomGCount - 1
            TableGrader.Rows(i).Visible = True
        Next

        Dim SQLRoomReq As String = "Select IsNull(Grader,0) as Grader From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & ""
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
        If ds.Tables(0).Rows.Count > 0 Then
            For j As Integer = ds.Tables(0).Rows(0)("Grader") To RoomGCount - 1
                TableGrader.Rows(j).Visible = False
            Next
            If ds.Tables(0).Rows(0)("Grader") = 0 Then
                lblErr.Text = "Graders were not Specified for Phase " & ddlPhase.SelectedValue & " in Room Requirement"
            End If
        Else
            lblErr.Text = "Room Requirement Data was not added for the Contest"
        End If

    End Sub

    Protected Sub ddlPhase_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlPhase.SelectedIndexChanged
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        TrCopy.Visible = False
        If ddlEvent.SelectedIndex = 0 Or ddlChapter.SelectedIndex = 0 Or ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please do all the Selections"
            Exit Sub
        End If
        LoadBldgID()
        LoadTimeData()
        CheckRoomReqData()
        LoadDGRoomSchedule()
    End Sub
    Private Sub LoadTimeData()
        Dim SQLContestTime As String = "Select Distinct StartTime,EndTime From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "" ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLContestTime)

        ddlStartTime.DataSource = ds
        ddlStartTime.DataBind()

        ddlEndTime.DataSource = ds
        ddlEndTime.DataBind()
    End Sub

    Private Sub LoadRoleData()
        Dim SQLGraderTeam As String = ""
        Dim ds As DataSet
        Try
            SQLGraderTeam = "Select * From GraderSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
            ds = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLGraderTeam)

            If ds.Tables(0).Rows.Count > 0 Then
                ddlGrader_1.SelectedIndex = ddlGrader_1.Items.IndexOf(ddlGrader_1.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_1") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_1"))))
                ddlGrader_2.SelectedIndex = ddlGrader_2.Items.IndexOf(ddlGrader_2.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_2") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_2"))))
                ddlGrader_3.SelectedIndex = ddlGrader_3.Items.IndexOf(ddlGrader_3.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_3") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_3"))))
                ddlGrader_4.SelectedIndex = ddlGrader_4.Items.IndexOf(ddlGrader_4.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_4") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_4"))))
                ddlGrader_5.SelectedIndex = ddlGrader_5.Items.IndexOf(ddlGrader_5.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_5") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_5"))))
                ddlGrader_6.SelectedIndex = ddlGrader_6.Items.IndexOf(ddlGrader_6.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_6") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_6"))))
                ddlGrader_7.SelectedIndex = ddlGrader_7.Items.IndexOf(ddlGrader_7.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_7") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_7"))))
                ddlGrader_8.SelectedIndex = ddlGrader_8.Items.IndexOf(ddlGrader_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_8") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_8"))))
                ddlGrader_9.SelectedIndex = ddlGrader_9.Items.IndexOf(ddlGrader_9.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_9") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_9"))))
                ddlGrader_10.SelectedIndex = ddlGrader_10.Items.IndexOf(ddlGrader_10.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_10") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_10"))))
                ddlGrader_11.SelectedIndex = ddlGrader_11.Items.IndexOf(ddlGrader_11.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_11") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_11"))))
                ddlGrader_12.SelectedIndex = ddlGrader_12.Items.IndexOf(ddlGrader_12.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_12") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_12"))))

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub


    Private Sub LoadRolesVol()

        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and RoleID=21)"
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and RoleID=21)"
        End If

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)

        ddlGrader_1.DataSource = ds
        ddlGrader_1.DataBind()
        ddlGrader_1.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_2.DataSource = ds
        ddlGrader_2.DataBind()
        ddlGrader_2.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_3.DataSource = ds
        ddlGrader_3.DataBind()
        ddlGrader_3.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_4.DataSource = ds
        ddlGrader_4.DataBind()
        ddlGrader_4.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_5.DataSource = ds
        ddlGrader_5.DataBind()
        ddlGrader_5.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_6.DataSource = ds
        ddlGrader_6.DataBind()
        ddlGrader_6.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_7.DataSource = ds
        ddlGrader_7.DataBind()
        ddlGrader_7.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_8.DataSource = ds
        ddlGrader_8.DataBind()
        ddlGrader_8.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_9.DataSource = ds
        ddlGrader_9.DataBind()
        ddlGrader_9.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_10.DataSource = ds
        ddlGrader_10.DataBind()
        ddlGrader_10.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_11.DataSource = ds
        ddlGrader_11.DataBind()
        ddlGrader_11.Items.Insert(0, New ListItem("Select", 0))

        ddlGrader_12.DataSource = ds
        ddlGrader_12.DataBind()
        ddlGrader_12.Items.Insert(0, New ListItem("Select", 0))

        'LoadRoleNames(ddlGrader_1, "RoleID = 21")

    End Sub
    Private Sub LoadRoleNames(ByVal ddlObject As DropDownList, ByVal Role As String)

        Dim SQLRoleStr As String = ""
        If ddlEvent.SelectedValue = 1 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Finals='Y' and " & Role & " )"
        ElseIf ddlEvent.SelectedValue = 2 Then
            SQLRoleStr = "Select AutoMemberID,( Title + '.' +FirstName +' '+ LastName ) as Name  From IndSpouse Where AutoMemberID in(Select MemberID From Volunteer Where EventYear= " & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and ChapterID > 1 and ChapterID=" & ddlChapter.SelectedValue & " and " & Role & " )"
        End If
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoleStr)
        ddlObject.DataSource = ds
        ddlObject.DataBind()
        ddlObject.Items.Insert(0, New ListItem("Select", 0))

    End Sub
    Private Sub ClearRoles()
        ddlGrader_1.Items.Clear()
        ddlGrader_2.Items.Clear()
        ddlGrader_3.Items.Clear()
        ddlGrader_4.Items.Clear()
        ddlGrader_5.Items.Clear()
        ddlGrader_6.Items.Clear()
        ddlGrader_7.Items.Clear()
        ddlGrader_8.Items.Clear()
        ddlGrader_9.Items.Clear()
        ddlGrader_10.Items.Clear()
        ddlGrader_11.Items.Clear()
        ddlGrader_12.Items.Clear()
        'LoadRolesVol()
        ' LoadRoleData()
    End Sub
    Protected Sub btnAddUpdate_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnAddUpdate.Click

        Dim SQLGraderInsert As String = ""
        Dim SQLGraderUpdate As String = ""
        Dim SQLGraderEval As String = ""
        Dim SQLGraderExec As String = ""

        lblAddUPdate.Text = ""
        lblErr.Text = ""
        If ddlEvent.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Event"
            Exit Sub
        ElseIf ddlChapter.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Chapter"
            Exit Sub
        ElseIf ddlPurpose.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Purpose"
            Exit Sub
        ElseIf ddlDate.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Date"
            Exit Sub
        ElseIf ddlProductGroup.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select ProductGroup"
            Exit Sub
        ElseIf ddlPhase.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select Phase"
            Exit Sub
            'ElseIf ddlStartTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select StartTime"
            '    Exit Sub
            'ElseIf ddlEndTime.SelectedItem.Text = "Select Start time" Then
            '    lblAddUPdate.Text = "Please Select EndTime"
            '    Exit Sub
        ElseIf ddlBldgID.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select BldgID"
            Exit Sub
        ElseIf ddlRoomNo.SelectedIndex = 0 Then
            lblAddUPdate.Text = "Please Select RoomNo"
            Exit Sub
        End If
        Try
            SQLGraderInsert = "Insert into GraderSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,StartTime,EndTime,"
            SQLGraderInsert = SQLGraderInsert & "GradMemberID_1,GradMemberID_2,GradMemberID_3,GradMemberID_4,GradMemberID_5,GradMemberID_6,GradMemberID_7,GradMemberID_8,GradMemberID_9,GradMemberID_10,GradMemberID_11,GradMemberID_12,CreatedBy,CreatedDate)"
            SQLGraderInsert = SQLGraderInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ddlBldgID.SelectedItem.Text & "'," & ddlSeqNo.SelectedValue & ",'" & ddlRoomNo.SelectedItem.Text & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlProductGroup.SelectedValue & ",'" & ddlProductGroup.SelectedItem.Text & "'," & ddlProduct.SelectedValue & ",'" & ddlProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ",'" & ddlStartTime.SelectedItem.Text & "','" & ddlEndTime.SelectedItem.Text & "',"
            SQLGraderInsert = SQLGraderInsert & IIf(ddlGrader_1.SelectedValue = 0, "NULL", ddlGrader_1.SelectedValue) & "," & IIf(ddlGrader_2.SelectedValue = 0, "NULL", ddlGrader_2.SelectedValue) & "," & IIf(ddlGrader_3.SelectedValue = 0, "NULL", ddlGrader_3.SelectedValue) & "," & IIf(ddlGrader_4.SelectedValue = 0, "NULL", ddlGrader_4.SelectedValue) & ","
            SQLGraderInsert = SQLGraderInsert & IIf(ddlGrader_5.SelectedValue = 0, "NULL", ddlGrader_5.SelectedValue) & "," & IIf(ddlGrader_6.SelectedValue = 0, "NULL", ddlGrader_6.SelectedValue) & "," & IIf(ddlGrader_7.SelectedValue = 0, "NULL", ddlGrader_7.SelectedValue) & "," & IIf(ddlGrader_8.SelectedValue = 0, "NULL", ddlGrader_8.SelectedValue) & "," & IIf(ddlGrader_9.SelectedValue = 0, "NULL", ddlGrader_9.SelectedValue) & "," & IIf(ddlGrader_10.SelectedValue = 0, "NULL", ddlGrader_10.SelectedValue) & "," & IIf(ddlGrader_11.SelectedValue = 0, "NULL", ddlGrader_11.SelectedValue) & "," & IIf(ddlGrader_12.SelectedValue = 0, "NULL", ddlGrader_12.SelectedValue) & "," & Session("LoginID") & ",GetDate() )"

            SQLGraderUpdate = "Update GraderSchedule Set Date='" & ddlDate.SelectedItem.Text & "' ,StartTime='" & ddlStartTime.SelectedItem.Text & "',EndTime='" & ddlEndTime.SelectedItem.Text & "',GradMemberID_1 = " & IIf(ddlGrader_1.SelectedValue = 0, "NULL", ddlGrader_1.SelectedValue) & ",GradMemberID_2=" & IIf(ddlGrader_2.SelectedValue = 0, "NULL", ddlGrader_2.SelectedValue) & ",GradMemberID_3=" & IIf(ddlGrader_3.SelectedValue = 0, "NULL", ddlGrader_3.SelectedValue) & ",GradMemberID_4=" & IIf(ddlGrader_4.SelectedValue = 0, "NULL", ddlGrader_4.SelectedValue) & ","
            SQLGraderUpdate = SQLGraderUpdate & " GradMemberID_5=" & IIf(ddlGrader_5.SelectedValue = 0, "NULL", ddlGrader_5.SelectedValue) & ",GradMemberID_6=" & IIf(ddlGrader_6.SelectedValue = 0, "NULL", ddlGrader_6.SelectedValue) & ",GradMemberID_7=" & IIf(ddlGrader_7.SelectedValue = 0, "NULL", ddlGrader_7.SelectedValue) & ",GradMemberID_8=" & IIf(ddlGrader_8.SelectedValue = 0, "NULL", ddlGrader_8.SelectedValue) & ",GradMemberID_9=" & IIf(ddlGrader_9.SelectedValue = 0, "NULL", ddlGrader_9.SelectedValue) & ",GradMemberID_10=" & IIf(ddlGrader_10.SelectedValue = 0, "NULL", ddlGrader_10.SelectedValue) & ",GradMemberID_11=" & IIf(ddlGrader_11.SelectedValue = 0, "NULL", ddlGrader_11.SelectedValue) & ",GradMemberID_12=" & IIf(ddlGrader_12.SelectedValue = 0, "NULL", ddlGrader_12.SelectedValue) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
            SQLGraderUpdate = SQLGraderUpdate & "Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and SeqNo =" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text.Trim() & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            SQLGraderEval = "Select Count(*) From GraderSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and BldgID='" & ddlBldgID.SelectedItem.Text.Trim() & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Value & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

            If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLGraderEval) = 0 Then
                'SQLGraderExec = SQLGraderInsert
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLGraderInsert)
                lblAddUPdate.Text = "Added Successfully"
            Else
                'SQLGraderExec = SQLGraderUpdate
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLGraderUpdate)
                lblAddUPdate.Text = "Updated Successfully"
            End If
            LoadDGRoomSchedule()
            ' btnAddUpdate.Text = "Add"
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        TrCopy.Visible = False
        lblAddUPdate.Text = ""
        lblErr.Text = ""
        ' ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ddlPurpose.SelectedIndex = 3
        ddlDate.SelectedIndex = 0
        ClearDropDowns()
        ClearRoles()
    End Sub
    Private Sub ClearDropDowns()
        lblAddUPdate.Text = ""
        DGRoomSchedule.Visible = False
        ddlProductGroup.Items.Clear() '.SelectedIndex = 0 '
        ddlProduct.Items.Clear()
        ddlPhase.Items.Clear()
        'ddlDate.SelectedIndex = 0 '.Items.Clear()
        ddlBldgID.Items.Clear()
        ddlSeqNo.Items.Clear()
        ddlRoomNo.Items.Clear()
        ddlStartTime.Items.Clear()
        ddlEndTime.Items.Clear()
        Dim RoomGCount As Integer = 12
        For i As Integer = 0 To RoomGCount - 1
            TableGrader.Rows(i).Visible = True
        Next
    End Sub

    Protected Sub ddlYear_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlYear.SelectedIndexChanged
        'ddlEvent.SelectedIndex = 0
        'ddlChapter.SelectedIndex = 0
        ClearDropDowns()
        ClearRoles()
        GetDate()
        'GetProductGroup(ddlProductGroup)
    End Sub
    Protected Sub BtnCopy_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopy.Click
        TrCopy.Visible = True
        GetProductGroup(ddlToPG)
    End Sub

    Protected Sub ddlToPG_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToPG.SelectedIndexChanged
        LoadProduct(ddlToPG, ddlToProduct)
    End Sub

    Protected Sub ddlToProduct_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlToProduct.SelectedIndexChanged
        lblAddUPdate.Text = ""
        Dim StrRoomNoSQl As String = "Select Distinct StartTime,EndTime from RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and Date='" & ddlDate.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "" ' and BldgId='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim dsTime As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomNoSQl)
        If dsTime.Tables(0).Rows.Count > 0 Then
            ddlStartCpTime.DataSource = dsTime
            ddlStartCpTime.DataBind()
            ddlEndCpTime.DataSource = dsTime
            ddlEndCpTime.DataBind()
        Else
            lblAddUPdate.Text = "Contest not yet Scheduled in RoomSchedule to copy data"
        End If
    End Sub
    Private Sub LoadDGRoomSchedule()
        DGRoomSchedule.Visible = True
        Dim dgItem As DataGridItem
        Dim lblCapcty As Label
        Dim lbtnRemoveCont As LinkButton
        Dim lbtnEditCont As LinkButton
        Dim lblGradr_1 As Label
        Dim lblGradr_2 As Label
        Dim lblGradr_3 As Label
        Dim lblGradr_4 As Label
        Dim lblGradr_5 As Label
        Dim lblGradr_6 As Label
        Dim lblGradr_7 As Label
        Dim lblGradr_8 As Label
        Dim lblGradr_9 As Label
        Dim lblGradr_10 As Label
        Dim lblGradr_11 As Label
        Dim lblGradr_12 As Label

        Dim lbl_GradSchID As Label
        Dim i As Integer = 0
        Dim StrRoomSch As String = "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrRoomSch)

        DGRoomSchedule.DataSource = ds
        DGRoomSchedule.DataBind()
        Dim ds1, ds2 As DataSet
        Dim GradStart As Integer
        Dim GradEnd As Integer

        Try
            For Each dgItem In DGRoomSchedule.Items
                GradStart = 17
                GradEnd = 28

                lblCapcty = dgItem.FindControl("lblCapacity")
                ds1 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select Distinct Capacity from RoomList Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'")
                lblCapcty.Text = ds1.Tables(0).Rows(0)("Capacity")

                lbtnRemoveCont = dgItem.FindControl("lbtnRemove")
                lbtnEditCont = dgItem.FindControl("lbtnEdit")

                lblGradr_1 = dgItem.FindControl("lblGrader_1")
                lblGradr_2 = dgItem.FindControl("lblGrader_2")
                lblGradr_3 = dgItem.FindControl("lblGrader_3")
                lblGradr_4 = dgItem.FindControl("lblGrader_4")
                lblGradr_5 = dgItem.FindControl("lblGrader_5")
                lblGradr_6 = dgItem.FindControl("lblGrader_6")
                lblGradr_7 = dgItem.FindControl("lblGrader_7")
                lblGradr_8 = dgItem.FindControl("lblGrader_8")
                lblGradr_9 = dgItem.FindControl("lblGrader_9")
                lblGradr_10 = dgItem.FindControl("lblGrader_10")
                lblGradr_11 = dgItem.FindControl("lblGrader_11")
                lblGradr_12 = dgItem.FindControl("lblGrader_12")

                lbl_GradSchID = dgItem.FindControl("lblGradSchID")

                For j As Integer = GradStart To GradEnd
                    DGRoomSchedule.Columns(j).Visible = True   'Grader
                Next
                If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From GraderSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") > 0 Then
                    ds2 = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From GraderSchedule Where ContestYear=" & ddlYear.SelectedValue & " and EventId=" & ddlEvent.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlProductGroup.SelectedValue & " and ProductId=" & ddlProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & " and BldgId='" & ds.Tables(0).Rows(i)("BldgId") & "' and RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "'") ' " & IIf(ddlBldgID.SelectedIndex = 0, "", "  ''''' ") & IIf(ddlRoomNo.SelectedIndex <= 0, "", "

                    If ds2.Tables(0).Rows.Count > 0 Then
                        ' For j As Integer = 0 To ds2.Tables(0).Rows.Count - 1
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_1") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_1")), lblGradr_1)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_2") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_2")), lblGradr_2)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_3") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_3")), lblGradr_3)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_4") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_4")), lblGradr_4)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_5") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_5")), lblGradr_5)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_6") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_6")), lblGradr_6)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_7") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_7")), lblGradr_7)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_8") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_8")), lblGradr_8)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_9") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_9")), lblGradr_9)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_10") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_10")), lblGradr_10)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_11") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_11")), lblGradr_11)
                        DisplayName(IIf(ds2.Tables(0).Rows(0)("GradMemberID_12") Is DBNull.Value, 0, ds2.Tables(0).Rows(0)("GradMemberID_12")), lblGradr_12)

                        lbl_GradSchID.Text = ds2.Tables(0).Rows(0)("GradSchID")

                    Else
                        lblGradr_1.Text = ""
                        lblGradr_2.Text = ""
                        lblGradr_3.Text = ""
                        lblGradr_4.Text = ""
                        lblGradr_5.Text = ""
                        lblGradr_6.Text = ""
                        lblGradr_7.Text = ""
                        lblGradr_8.Text = ""
                        lblGradr_9.Text = ""
                        lblGradr_10.Text = ""
                        lblGradr_11.Text = ""
                        lblGradr_12.Text = ""
                        lbl_GradSchID.Text = ""
                    End If
                Else
                    lblGradr_1.Text = ""
                    lblGradr_2.Text = ""
                    lblGradr_3.Text = ""
                    lblGradr_4.Text = ""
                    lblGradr_5.Text = ""
                    lblGradr_6.Text = ""
                    lblGradr_7.Text = ""
                    lblGradr_8.Text = ""
                    lblGradr_9.Text = ""
                    lblGradr_10.Text = ""
                    lblGradr_11.Text = ""
                    lblGradr_12.Text = ""
                    lbl_GradSchID.Text = ""
                End If

                Dim SQLRoomReq As String = "Select IsNull(Grader,0) as Grader From RoomRequirement Where EventYear=" & ddlYear.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text.Trim() & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and Phase =" & ddlPhase.SelectedValue & " "
                Dim dsRoomReq As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, SQLRoomReq)
                If dsRoomReq.Tables(0).Rows.Count > 0 Then
                    GradStart = GradStart + dsRoomReq.Tables(0).Rows(0)("Grader")
                    For k As Integer = GradStart To GradEnd
                        DGRoomSchedule.Columns(k).Visible = False
                    Next
                End If


                If (lbl_GradSchID.Text <> "") Then
                    lbtnRemoveCont.Enabled = True
                    lbtnEditCont.Enabled = True
                Else
                    lbtnRemoveCont.Enabled = False
                    lbtnEditCont.Enabled = False
                End If
                i = i + 1
            Next
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub
    Private Sub DisplayName(ByVal MemberID As Integer, ByVal lblRole As Label)
        If MemberID <> 0 Then
            Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select (Title + '.' +FirstName +''+ LastName ) as Name  From IndSpouse Where AutoMemberID =" & MemberID)
            lblRole.Text = ds.Tables(0).Rows(0)("Name")
        End If
    End Sub

    Protected Sub DGRoomSchedule_ItemCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs)
        Dim lblGradSchID As Label
        lblGradSchID = e.Item.FindControl("lblGradSchID")
        Dim GradSchID As Integer = lblGradSchID.Text ' CInt(e.Item.Cells(25).Text)

        If e.CommandName = "Delete" Then
            Try
                SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, "Delete from GraderSchedule Where GradSchID=" & GradSchID & "")
                LoadDGRoomSchedule()
            Catch ex As Exception
                lblErr.Text = ex.Message
                lblErr.Text = (lblErr.Text + "<BR>Delete failed. Please try again.")
                Return
            End Try
        ElseIf e.CommandName = "Edit" Then
            'lblRoomSchID.Text = ContTeamID.ToString()
            LoadForUpdate(GradSchID)
        End If
    End Sub
    Private Sub LoadForUpdate(ByVal GradSchID As Integer)

        'btnAddUpdate.Text = "Update"
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "select * from GraderSchedule Where GradSchID=" & GradSchID & "")
        'Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select * From RoomSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo")  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'"
        Dim BldgID As String = ""
        Try
            If ds.Tables(0).Rows.Count > 0 Then

                ddlStartTime.SelectedIndex = ddlStartTime.Items.IndexOf(ddlStartTime.Items.FindByValue(ds.Tables(0).Rows(0)("StartTime")))
                ddlEndTime.SelectedIndex = ddlEndTime.Items.IndexOf(ddlEndTime.Items.FindByValue(ds.Tables(0).Rows(0)("EndTime")))

                ddlBldgID.SelectedIndex = ddlBldgID.Items.IndexOf(ddlBldgID.Items.FindByValue(ds.Tables(0).Rows(0)("BldgID")))
                ddlRoomNo.Items.Clear()
                LoadRoomNumber(ds.Tables(0).Rows(0)("BldgID").ToString.Trim)
                ddlSeqNo.SelectedIndex = ddlSeqNo.Items.IndexOf(ddlSeqNo.Items.FindByValue(ds.Tables(0).Rows(0)("SeqNo").ToString.Trim()))
                ddlRoomNo.SelectedIndex = ddlRoomNo.Items.IndexOf(ddlRoomNo.Items.FindByValue(ds.Tables(0).Rows(0)("RoomNumber").ToString))

                ddlGrader_1.SelectedIndex = ddlGrader_1.Items.IndexOf(ddlGrader_1.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_1") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_1"))))
                ddlGrader_2.SelectedIndex = ddlGrader_2.Items.IndexOf(ddlGrader_2.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_2") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_2"))))
                ddlGrader_3.SelectedIndex = ddlGrader_3.Items.IndexOf(ddlGrader_3.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_3") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_3"))))
                ddlGrader_4.SelectedIndex = ddlGrader_4.Items.IndexOf(ddlGrader_4.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_4") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_4"))))
                ddlGrader_5.SelectedIndex = ddlGrader_5.Items.IndexOf(ddlGrader_5.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_5") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_5"))))
                ddlGrader_6.SelectedIndex = ddlGrader_6.Items.IndexOf(ddlGrader_6.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_6") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_6"))))
                ddlGrader_7.SelectedIndex = ddlGrader_7.Items.IndexOf(ddlGrader_7.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_7") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_7"))))
                ddlGrader_8.SelectedIndex = ddlGrader_8.Items.IndexOf(ddlGrader_8.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_8") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_8"))))
                ddlGrader_9.SelectedIndex = ddlGrader_9.Items.IndexOf(ddlGrader_9.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_9") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_9"))))
                ddlGrader_10.SelectedIndex = ddlGrader_10.Items.IndexOf(ddlGrader_10.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_10") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_10"))))
                ddlGrader_11.SelectedIndex = ddlGrader_11.Items.IndexOf(ddlGrader_11.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_11") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_11"))))
                ddlGrader_12.SelectedIndex = ddlGrader_12.Items.IndexOf(ddlGrader_12.Items.FindByValue(IIf(ds.Tables(0).Rows(0)("GradMemberID_12") Is DBNull.Value, 0, ds.Tables(0).Rows(0)("GradMemberID_12"))))

            End If
        Catch ex As Exception
            'Response.Write(ex.ToString)
        End Try
    End Sub

    Protected Sub BtnCopySchedule_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCopySchedule.Click

        Dim SQLContTeamCpInsert As String = ""
        Dim SQLContTeamCpUpdate As String = ""
        Dim SQLContTeamCpEval As String = ""
        Dim SQLContTeamCpExec As String = ""
        Dim StrFromContest As String = ""
        Dim GradSchID As String = ""
        Dim i, count As Integer
        lblAddUPdate.Text = ""
        lblErr.Text = ""

        If ddlToPG.SelectedIndex = 0 Then
            lblErr.Text = "Please Select Copy Contest"
            Exit Sub
        End If

        StrFromContest = "Select * From GraderSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlProductGroup.SelectedValue & " and ProductID=" & ddlProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & " Order by SeqNo"  '   " and BldgID='" & ddlBldgID.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & " and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, StrFromContest)

        Try
            If ds.Tables(0).Rows.Count > 0 Then
                For i = 0 To ds.Tables(0).Rows.Count - 1
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "Insert into GraderSchedule(ContestYear,ChapterID,Chapter,EventID,Event,BldgID,SeqNo,RoomNumber,Purpose,Date,ProductGroupId,ProductGroupCode,ProductId,ProductCode,Phase,StartTime,EndTime,"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & "GradMemberID_1,GradMemberID_2,GradMemberID_3,GradMemberID_4,GradMemberID_5,GradMemberID_6,GradMemberID_7,GradMemberID_8,GradMemberID_9,GradMemberID_10,GradMemberID_11,GradMemberID_12,CreatedBy,CreatedDate)"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & " Values( " & ddlYear.SelectedValue & "," & ddlChapter.SelectedValue & ",'" & ddlChapter.SelectedItem.Text & "'," & ddlEvent.SelectedValue & ",'" & ddlEvent.SelectedItem.Text & "','" & ds.Tables(0).Rows(i)("BldgID") & "','" & ds.Tables(0).Rows(i)("SeqNo") & "','" & ds.Tables(0).Rows(i)("RoomNumber") & "','" & ddlPurpose.SelectedItem.Text & "','" & ddlDate.SelectedItem.Text & "'," & ddlToPG.SelectedValue & ",'" & ddlToPG.SelectedItem.Text & "'," & ddlToProduct.SelectedValue & ",'" & ddlToProduct.SelectedItem.Text & "'," & ddlPhase.SelectedValue & ",'" & ddlStartCpTime.SelectedItem.Text & "','" & ddlEndCpTime.SelectedItem.Text & "',"
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("GradMemberID_1") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_1")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_2") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_2")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_3") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_3")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_4") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_4")) & ","
                    SQLContTeamCpInsert = SQLContTeamCpInsert & IIf(ds.Tables(0).Rows(i)("GradMemberID_5") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_5")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_6") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_6")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_7") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_7")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_8") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_8")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_9") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_9")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_10") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_10")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_11") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_11")) & "," & IIf(ds.Tables(0).Rows(i)("GradMemberID_12") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID_12")) & "," & Session("LoginID") & ",GetDate() )"

                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & "Update ContestTeamSchedule Set Date='" & ddlDate.SelectedItem.Text & "', BldgID='" & ds.Tables(0).Rows(i)("BldgID") & "',SeqNo=" & ds.Tables(0).Rows(i)("SeqNo") & ", RoomNumber='" & ds.Tables(0).Rows(i)("RoomNumber") & "',StartTime='" & ddlStartCpTime.SelectedValue & "',EndTime='" & ddlEndCpTime.SelectedValue & "',RMcMemberID = " & IIf(ds.Tables(0).Rows(i)("RMcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("RMcMemberID")) & ",PronMemberID=" & IIf(ds.Tables(0).Rows(i)("PronMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("PronMemberID")) & ",CJMemberID=" & IIf(ds.Tables(0).Rows(i)("CJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("CJMemberID")) & ",AJMemberID=" & IIf(ds.Tables(0).Rows(i)("AJMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("AJMemberID")) & ","
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " LTJudMemberID=" & IIf(ds.Tables(0).Rows(i)("LTJudMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("LTJudMemberID")) & ",DictHMemberID=" & IIf(ds.Tables(0).Rows(i)("DictHMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("DictHMemberID")) & ",GradMemberID=" & IIf(ds.Tables(0).Rows(i)("GradMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("GradMemberID")) & ",ProcMemberID=" & IIf(ds.Tables(0).Rows(i)("ProcMemberID") Is DBNull.Value, "NULL", ds.Tables(0).Rows(i)("ProcMemberID")) & ",ModifiedBy=" & Session("LoginID") & ",ModifiedDate=GetDate()"
                    'SQLContTeamCpUpdate = SQLContTeamCpUpdate & " Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""

                Next
                SQLContTeamCpEval = "Select Count(*) From GraderSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & ""      'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'
                count = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") 'Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                If i <= count Then 'SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select Count(*) From RoomSchedule Where ContestYear =" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupId=" & ddlToPG.SelectedValue & " and ProductId=" & ddlToProduct.SelectedValue & " and Phase= " & ddlPhase.SelectedValue & "") Then     'and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'

                    If SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, SQLContTeamCpEval) > 0 Then
                        Dim ds3 As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, "Select GradSchID From GraderSchedule Where ContestYear=" & ddlYear.SelectedValue & " and ChapterID=" & ddlChapter.SelectedValue & " and EventID=" & ddlEvent.SelectedValue & " and Purpose='" & ddlPurpose.SelectedItem.Text & "' and ProductGroupID=" & ddlToPG.SelectedValue & " and ProductID=" & ddlToProduct.SelectedValue & " and Phase=" & ddlPhase.SelectedValue & "") ' and BldgID='" & ddlBldgID.SelectedItem.Text & "' and RoomNumber='" & ddlRoomNo.SelectedItem.Text & "'" ' and SeqNo=" & ddlSeqNo.SelectedValue & ")

                        For j As Integer = 0 To ds3.Tables(0).Rows.Count - 1
                            GradSchID = GradSchID & ds3.Tables(0).Rows(j)("GradSchID") & ","
                        Next
                        GradSchID = GradSchID.Trim(",")
                        SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Delete From GraderSchedule Where GradSchID in(" & GradSchID & ")")

                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Updated)"
                    Else
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString"), CommandType.Text, SQLContTeamCpInsert)
                        lblAddUPdate.Text = "Copied Successfully(Added)"
                    End If
                ElseIf i > count Then
                    lblErr.Text = "RoomSchedule data  has fewer records for the Contest"
                Else
                    lblErr.Text = "RoomSchedule data is not yet added for the Contest"
                End If
            End If
            LoadDGRoomSchedule()
            ' btnAddUpdate.Text = "Add"
        Catch ex As Exception
            ' Response.Write(ex.ToString)
        End Try
    End Sub


    Protected Sub ddlDate_SelectedIndexChanged(ByVal sender As Object, ByVal e As System.EventArgs) Handles ddlDate.SelectedIndexChanged
        ClearDropDowns()
        GetProductGroup(ddlProductGroup)
    End Sub
End Class
