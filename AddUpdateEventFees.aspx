<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="AddUpdateEventFees.aspx.vb" Inherits="AddUpdateEventFees" Title="AddUpdate Event Fees" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">


    <div style="text-align: left">
        &nbsp;
           <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
        &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp;  
    <script language="javascript" type="text/javascript">
        function ValidateSemester(s, p, semester) {
            if (confirm("Do you really want to upgrade the semester as " + s + " for the product '" + p + "' before closing " + semester + " classes? ")) {
                document.getElementById('<%= btnUpdate.ClientID%>').click();
            }

        }
        function UpgradeConfirmation(s) {
            if (confirm("Do you really want to upgrade the semester as " + s + " for all products?")) {
                document.getElementById('<%= btnUpgrade.ClientID%>').click();
            }

        }
        function PopupPicker(ctl, w, h) {
            var PopupWindow = null;
            settings = 'width=' + w + ',height=' + h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
            PopupWindow = window.open('DatePicker.aspx?Ctl=_ctl0_Content_main_' + ctl, 'DatePicker', settings);
            PopupWindow.focus();
        }
        function WarningCalSignup(msg) {
            if (confirm(msg)) {
                document.getElementById('<%= BtnCloseCalSignup.ClientID%>').click();
            }

        }
        function WarningReg(msg) {
            if (confirm(msg)) {
                document.getElementById('<%= BtnCloseReg.ClientID%>').click();
            }

        }
        function WarningCalSignupAndStudentReg(msg) {
            if (confirm(msg)) {
                document.getElementById('<%= BtnCLoseCalSignupAndReg.ClientID%>').click();
            }

        }

    </script>
    </div>
    <div style="display: none">
        <asp:Button ID="btnUpgrade" runat="server" Text="Upgrade" /><asp:Button ID="btnUpdate" runat="server" Text="Update" />
        <asp:Button ID="BtnCloseCalSignup" runat="server" Text="Upgrade" OnClick="BtnCloseCalSignup_Click" Style="display: none;" />
        <asp:Button ID="BtnCloseReg" runat="server" Text="Upgrade" OnClick="BtnCloseReg_Click" Style="display: none;" />
        <asp:Button ID="BtnCLoseCalSignupAndReg" runat="server" Text="Upgrade" OnClick="BtnCLoseCalSignupAndReg_Click" Style="display: none;" />
        <asp:HiddenField ID="hfSemName" runat="server" />
    </div>
    <table cellpadding="2" cellspacing="0" border="0" width="1200px" aligin="center">
        <tr>
            <td>
                <table cellpadding="2" cellspacing="0" border="0" width="1200px" aligin="center">
                    <tr>
                        <td align="center"></td>
                    </tr>
                    <tr>
                        <td align="center">
                            <table cellpadding="5" cellspacing="0" border="0" align="center">
                                <tr>
                                    <td align="left">Event</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEvent" AutoPostBack="true" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" DataTextField="Name" DataValueField="EventID" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">Event Year</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlEventYear" AutoPostBack="true" Width="75px" OnSelectedIndexChanged="ddlEventYear_SelectedIndexChanged" runat="server" Height="24px">
                                        </asp:DropDownList>
                                    </td>
                                    <td align="left">Proposed Year</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPropYear" runat="server" Height="24px" Width="75px">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="center" colspan="6">
                                        <asp:Button ID="btnAddNew" runat="server" Text="Add New" />
                                        &nbsp;
        <asp:Button ID="btnRelpicateAll" runat="server" Text="Replicate All" />
                                        &nbsp;
        <asp:Button ID="btnRelpicateSel" runat="server" Text="Relpicate Selected" /></td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                    <tr>
                        <td align="center"></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <table runat="server" align="center" width="800px">
                    <tr>
                        <td>
                            <table id="traddUpdate" runat="server" visible="false" align="left">

                                <tr>
                                    <td align="left">Product Group</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlProductGroup"
                                            DataTextField="Name" DataValueField="ProductGroupID"
                                            OnSelectedIndexChanged="ddlProductGroup_SelectedIndexChanged"
                                            AutoPostBack="true" Enabled="false" runat="server" Height="24px" Width="150px">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">Product</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlProduct" DataTextField="Name"
                                            DataValueField="ProductID" Height="24px" Width="150px" Enabled="false" runat="server">
                                        </asp:DropDownList></td>
                                </tr>
                                <tr>
                                    <td align="left">Grade From</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGradeFrom" runat="server">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>11</asp:ListItem>
                                            <asp:ListItem>12</asp:ListItem>
                                            <asp:ListItem>13</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Grade To</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGradeTo" runat="server">
                                            <asp:ListItem>0</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                            <asp:ListItem>11</asp:ListItem>
                                            <asp:ListItem>12</asp:ListItem>
                                            <asp:ListItem>13</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Selection Criteria</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlGradeTest" runat="server" Style="margin-left: 0px">
                                            <asp:ListItem Value="O" Selected="True">Grade Based</asp:ListItem>
                                            <asp:ListItem Value="I">Test Based</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="TrDlineDate" runat="server">
                                    <td align="left">DeadLine Date  </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDeadLineDate" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><a href="javascript:PopupPicker('txtDeadLineDate', 200, 200);">
                                        <img src="images/calendar.gif" border="0"></a></td>
                                </tr>
                                <tr id="TrLateDlineDate" runat="server">
                                    <td align="left">DeadLine Date with Late fee </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtLateRegDLineDate" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><a href="javascript:PopupPicker('txtLateRegDLineDate', 200, 200);">
                                        <img src="images/calendar.gif" border="0"></a></td>
                                </tr>
                                <tr id="TrChangeCoachDL" runat="server">
                                    <td align="left">Change Coach DeadLine Date </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtChangeCoachDL" runat="server"></asp:TextBox>
                                    </td>
                                    <td align="left"><a href="javascript:PopupPicker('txtChangeCoachDL', 200, 200);">
                                        <img src="images/calendar.gif" border="0"></a></td>
                                </tr>
                                <tr>
                                    <td align="left">Product Level Pricing</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlPricing" runat="server" Style="margin-left: 0px">
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Calendar Signup Open</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlCalSignupOpen" runat="server" Style="margin-left: 0px">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Semester</td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlSemester" runat="server">
                                            <asp:ListItem>Fall</asp:ListItem>
                                            <asp:ListItem>Spring</asp:ListItem>
                                            <asp:ListItem>Summer</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">HwDueDateNo</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtHwDueDateNo" runat="server"></asp:TextBox></td>
                                </tr>
                            </table>
                        </td>
                        <td>
                            <table id="traddUpdate1" runat="server" visible="false" align="left">
                                <tr>
                                    <td align="left">Reg Fee</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtRegFee" runat="server" Text="0.00"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">Late Fee</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtlateFee" Text="0.00" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">TaxDedRegional</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtTax" Text="0.00" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr>
                                    <td align="left">Discount Amt</td>
                                    <td align="left">
                                        <asp:TextBox ID="txtDiscount" Text="0.00" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="TrCapacity" runat="server" visible="false">
                                    <td align="left">Capacity </td>
                                    <td align="left">
                                        <asp:TextBox ID="txtCapacity" Text="0" runat="server"></asp:TextBox></td>
                                </tr>
                                <tr id="TrDuration" runat="server" visible="false">
                                    <td align="left">Duration </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlDuration" runat="server">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="0.5">0.5</asp:ListItem>
                                            <asp:ListItem Value="1">1.0</asp:ListItem>
                                            <asp:ListItem Value="1.5">1.5</asp:ListItem>
                                            <asp:ListItem Value="2">2.0</asp:ListItem>
                                            <asp:ListItem Value="2.5">2.5</asp:ListItem>
                                            <asp:ListItem Value="3">3.0</asp:ListItem>
                                            <asp:ListItem Value="3.5">3.5</asp:ListItem>
                                            <asp:ListItem Value="4">4.0</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="TrFrequency" runat="server" visible="false">
                                    <td align="left">Frequency </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlFrequency" runat="server">
                                            <asp:ListItem Value="">Select</asp:ListItem>
                                            <asp:ListItem Value="Daily">Daily</asp:ListItem>
                                            <asp:ListItem Value="Weekly">Weekly</asp:ListItem>
                                            <asp:ListItem Value="Bi-Weekly">Bi-Weekly</asp:ListItem>
                                            <asp:ListItem Value="Monthly">Monthly</asp:ListItem>
                                            <asp:ListItem Value="Once">Once</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="Tr1" runat="server">
                                    <td align="left">Number of Classes
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddClasses" runat="server">
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr id="Tr2" runat="server">
                                    <td align="left">Number of Licenses
                                    </td>
                                    <td align="left">
                                        <asp:DropDownList ID="ddlLicenses" runat="server">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem>1</asp:ListItem>
                                            <asp:ListItem>2</asp:ListItem>
                                            <asp:ListItem>3</asp:ListItem>
                                            <asp:ListItem>4</asp:ListItem>
                                            <asp:ListItem>5</asp:ListItem>
                                            <asp:ListItem>6</asp:ListItem>
                                            <asp:ListItem>7</asp:ListItem>
                                            <asp:ListItem>8</asp:ListItem>
                                            <asp:ListItem>9</asp:ListItem>
                                            <asp:ListItem>10</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Allow Adult</td>
                                    <td align="left">
                                        <asp:CheckBox ID="chkAdult" runat="server" Enabled="false" />
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">Reg Open</td>
                                    <td align="left">
                                        <asp:DropDownList ID="DDLRegOpen" runat="server" Style="margin-left: 0px">
                                            <asp:ListItem Value="0">Select</asp:ListItem>
                                            <asp:ListItem Value="Y">Yes</asp:ListItem>
                                            <asp:ListItem Value="N">No</asp:ListItem>
                                        </asp:DropDownList>
                                    </td>
                                </tr>
                                <tr>
                                    <td align="left">AnsRelDateNo</td>
                                    <td align="left">
                                        <asp:TextBox ID="TxtAnsRelDateNo" runat="server"></asp:TextBox></td>
                                </tr>

                            </table>
                        </td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>
            <td>
                <div runat="server" id="divAddUpdate" visible="false" style="width: 225px; margin: 0 auto;">
                    <asp:Button ID="btnAdd" runat="server" Text="Add" Style="height: 26px" />
                    &nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnCancel" runat="server" OnClick="btnCancel_Click" Text="Clear" />
                </div>
                <div style="width: 500px; margin: 0 auto;">
                    <asp:Label ID="lblErr" ForeColor="Red" runat="server"></asp:Label>&nbsp;<asp:Label Visible="false" ID="lblEventFeesID"
                        runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblError" ForeColor="Red" runat="server"></asp:Label>
                    <br />
                    <asp:Label ID="lblNotemsg" ForeColor="Green" Visible="false" runat="server">Note :*** You cannot update Event and Event year in any record.</asp:Label>
                    <br />
                </div>
            </td>
        </tr>
        <tr>
            <td></td>
        </tr>
    </table>
    <div style="width: 1250px; overflow-y: scroll;">
        <div style="float: right">
            <asp:Button ID="btnConfirmUpgrade" runat="server" Text="Upgrade Semester" Visible="false" ToolTip="It will update the semester of all the products of the selected event based on current month" />
        </div>
        <asp:DataGrid ID="DGEventFees" runat="server" DataKeyField="EventFeesID"
            AutoGenerateColumns="False" OnItemCommand="DGEventFees_ItemCommand" CellPadding="4"
            BackColor="#CCCCCC" BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" CellSpacing="2" ForeColor="Black" Width="1200px">
            <FooterStyle BackColor="#CCCCCC" />
            <SelectedItemStyle BackColor="#000099" Font-Bold="True" ForeColor="White" />
            <PagerStyle BackColor="#CCCCCC" ForeColor="Black" HorizontalAlign="Left"
                Mode="NumericPages" />
            <ItemStyle BackColor="White" />
            <Columns>
                <%--0--%>
                <asp:TemplateColumn HeaderText="Select" HeaderStyle-Wrap="false" ItemStyle-Wrap="false">
                    <ItemTemplate>
                        <asp:CheckBox ID="chkSelect" runat="server" />
                    </ItemTemplate>
                    <HeaderStyle ForeColor="White" Font-Bold="true" Wrap="False"></HeaderStyle>
                    <ItemStyle HorizontalAlign="Center" Wrap="False"></ItemStyle>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnRemove" runat="server" CommandName="Delete" Text="Delete"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:TemplateColumn>
                    <ItemTemplate>
                        <asp:LinkButton ID="lbtnEdit" runat="server" CommandName="Edit" Text="Edit"></asp:LinkButton>
                    </ItemTemplate>
                </asp:TemplateColumn>
                <asp:BoundColumn DataField="EventFeesID" HeaderText="EventFeesID" Visible="false" />
                <%--3--%>
                <%-- <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventName" HeaderText="EventName" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="EventYear" HeaderText="EventYear" />--%>
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductGroupCode" HeaderText="Product Group" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductCode" HeaderText="Product" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="GradeFrom" HeaderText="Grade From" ItemStyle-Width="30px" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="GradeTo" HeaderText="Grade To" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="RegFee" HeaderText="RegFee" DataFormatString="{0:c}" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="LateFee" HeaderText="LateFee" DataFormatString="{0:c}" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="TaxDedRegional" HeaderText="TxDed Rate" DataFormatString="{0:F}" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DiscountAmt" HeaderText="Discount" DataFormatString="{0:F}" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ProductLevelPricing" HeaderText="ProdLevel Price" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Semester" HeaderText="Semester" />
                <%--13 --15--%>
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CoachSelCriteria" HeaderText="CoachSel Criteria" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="DeadlineDate" HeaderText="Dead Line" Visible="false" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="LateRegDLDate" HeaderText="Late Registration" Visible="false" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Classes" HeaderText="Classes" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Licenses" HeaderText="Licenses" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="ChangeCoachDL" HeaderText="Change Coach" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Capacity" HeaderText="Capacity" Visible="false" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Duration" HeaderText="Duration" Visible="false" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Frequency" HeaderText="Frequency" Visible="false" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="Adult" HeaderText="Adult" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="CalSignupOpen" HeaderText="Calendar Signup" />
                <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="RegOpen" HeaderText="Reg Open" />
                 <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="HwDueDateNo" HeaderText="HWDueDateNo" />
                 <asp:BoundColumn HeaderStyle-Font-Bold="true" DataField="AnsRelDateNo" HeaderText="AnsRelDateNo" />
            </Columns>
            <HeaderStyle BackColor="White" />
        </asp:DataGrid>
    </div>
    <asp:HiddenField ID="HdnSemester" runat="server" Value="" />
    <asp:HiddenField ID="hdnEventYear" runat="server" Value="" />
</asp:Content>

