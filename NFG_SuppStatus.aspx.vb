Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class NFG_SuppStatus
    Inherits System.Web.UI.Page

    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0>"
        'Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "SELECT Distinct FromDate,ToDate From NFG_Supp order by Todate Desc;")
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("Connectionstring"), CommandType.Text, "SELECT Distinct FromDate,ToDate From NFG_Supp order by FromDate,Todate Asc;")
        If ds.Tables(0).Rows.Count > 0 Then
            TableStr = TableStr & "<tr><td align=Center>From Date</td><td align=Center>ToDate</td></tr>"
            For i As Integer = 0 To ds.Tables(0).Rows.Count - 1
                TableStr = TableStr & "<tr><td align=Center>" & ds.Tables(0).Rows(i)("FromDate") & "</td><td align=Center>" & ds.Tables(0).Rows(i)("ToDate") & "</td></tr>"
            Next
        Else
            TableStr = TableStr & "<tr><td align=Center>No Records Added</td></tr>"
        End If
        TableStr = TableStr & "</Table><br><br>"
        Literal1.Text = TableStr.ToString
    End Sub
End Class
