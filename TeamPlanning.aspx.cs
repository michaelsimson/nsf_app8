﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class TeamPlanning_TeamPlanning : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }

    }
    protected void ddlTeamPlanning_SelectedIndexChanged(object sender, EventArgs e)
    {
        if (ddlTeamPlanning.SelectedItem.Text == "Team Plan Settings")
        {
            Response.Redirect("TeamPlanningSettings.aspx");
        }
        else if (ddlTeamPlanning.SelectedItem.Text == "Team Schedule")
        {
            Response.Redirect("TeamSchedule.aspx");
        }
       
    }
}