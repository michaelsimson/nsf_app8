<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="ChildAssessmentForm.aspx.vb" Inherits="ChildAssessmentForm" title="Child Assessment Form" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
 <div align="left">
        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<asp:hyperlink id="hlinkParentRegistration" runat="server" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink></div>

<div style="width :900px" align="center">
<h4>Self Assessment Exam</h4>
</div>
<div style="width :900px" align="center" id = "divid1" runat="server" >
<table border="0" cellpadding = "3" cellspacing = "0">
<tr><td  align="center" colspan="2" > </td></tr>
<tr><td  align="left"></td><td align = "left" ></td></tr>
<tr><td  align="left">Child</td><td align = "left" ><asp:DropDownList ID="ddlChild" AutoPostBack="true" OnSelectedIndexChanged="ddlChild_SelectedIndexChanged" DataTextField="Name" DataValueField ="ChildNumber" Width="150px" runat="server"></asp:DropDownList></td></tr>
<tr><td  align="left">Exam For</td><td align = "left" ><asp:DropDownList  Width="150px" Enabled="false" ID="ddlProduct" DataTextField="ProductName" DataValueField="ProductID" runat="server"></asp:DropDownList></td></tr>
<tr><td  align="center" colspan = "2"> 
    <asp:Button ID="btnContinue" runat="server" OnClick="btnContinue_Click" Text="Continue" />&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btnAnswer" runat="server" Width="90px" OnClick="btnAnswer_Click" Text="Answer Key" /></td></tr>
    <tr><td  align="center" colspan="2" > 
        <asp:Label ID="lblerr" runat="server" ForeColor = "Red" ></asp:Label>
        <asp:Label ID="lblAssessExRespID" runat="server" ForeColor="White"  ></asp:Label>
        </td></tr>
</table>
</div>
  <center>  <asp:Label ID="lblError" runat="server" ForeColor = "Red" ></asp:Label></center>
<div align="center" id="DivID2" runat="server"  style="width :900px" visible="false">
<table border="0" cellpadding = "3" cellspacing = "0">
<tr><td align = "center" colspan = "3" >Please provide answers to the following questions<br /><br /></td>
   </tr>
<tr><td  align="left"><b> Q1</b> </td><td align = "left"><asp:TextBox ID="txtQ1" runat="server"></asp:TextBox></td><td align = "left" style = "width :100px">
<asp:RequiredFieldValidator ID="RequiredFieldValidator1" ControlToValidate="txtQ1" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q2</b> </td><td align = "left"><asp:TextBox ID="txtQ2" runat="server"></asp:TextBox></td><td align = "left">
    <asp:RequiredFieldValidator ID="RequiredFieldValidator2" ControlToValidate="txtQ2" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q3</b> </td><td align = "left"><asp:TextBox ID="txtQ3" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator3" ControlToValidate="txtQ3" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q4</b> </td><td align = "left"><asp:TextBox ID="txtQ4" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator4" ControlToValidate="txtQ4" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q5</b> </td><td align = "left"><asp:TextBox ID="txtQ5" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator5" ControlToValidate="txtQ5" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q6</b> </td><td align = "left"><asp:TextBox ID="txtQ6" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator6" ControlToValidate="txtQ6" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q7</b> </td><td align = "left"><asp:TextBox ID="txtQ7" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator7" ControlToValidate="txtQ7" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q8</b> </td><td align = "left"><asp:TextBox ID="txtQ8" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator8" ControlToValidate="txtQ8" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q9</b> </td><td align = "left"><asp:TextBox ID="txtQ9" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator9" ControlToValidate="txtQ9" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="left"><b> Q10</b> </td><td align = "left"><asp:TextBox ID="txtQ10" runat="server"></asp:TextBox></td><td align = "left">
<asp:RequiredFieldValidator ID="RequiredFieldValidator10" ControlToValidate="txtQ10" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator></td></tr>
<tr><td  align="center" colspan = "3"> <asp:Button ID="btnSubmit" runat="server" Text="Save" OnClick ="btnSubmit_Click" /> 
    &nbsp;&nbsp;&nbsp;&nbsp; 
    <asp:Button ID="btncancel" runat="server" Text="Cancel" EnableClientScript="False" CausesValidation="False" OnClick ="btncancel_Click" /></td>
    </tr>
</table> 
</div>
<div runat = "server" visible = "false" id="DivID3">
<center><h3>Result</h3></center>
 <asp:GridView  BorderWidth="2px" HorizontalAlign="Center" RowStyle-HorizontalAlign="Center"  
        ID="GridResult" GridLines ="Both" AutoGenerateColumns = "False" runat="server"        
        CellPadding="4" CellSpacing="1" ForeColor="#333333" >

<RowStyle HorizontalAlign="Center" BackColor="#FFFBD6" ForeColor="#333333"></RowStyle>

         <Columns>
                                <asp:BoundField DataField="Heading" HeaderStyle-ForeColor="Wheat"  headerText="" ></asp:BoundField>
                                <asp:BoundField DataField="ProductCode"  HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="Wheat" headerText="Exam" ></asp:BoundField>
                                <asp:BoundField DataField="Q1" HeaderStyle-ForeColor="Wheat" headerText="Q1" ></asp:BoundField>
                                <asp:BoundField DataField="Q2" HeaderStyle-ForeColor="Wheat" headerText="Q2" ></asp:BoundField>
                                <asp:BoundField DataField="Q3" HeaderStyle-ForeColor="Wheat" headerText="Q3" ></asp:BoundField>
                                <asp:BoundField DataField="Q4" HeaderStyle-ForeColor="Wheat" headerText="Q4" ></asp:BoundField>
                                <asp:BoundField DataField="Q5" HeaderStyle-ForeColor="Wheat" headerText="Q5" ></asp:BoundField>
                                <asp:BoundField DataField="Q6" HeaderStyle-ForeColor="Wheat" headerText="Q6" ></asp:BoundField>
                                <asp:BoundField DataField="Q7" HeaderStyle-ForeColor="Wheat" headerText="Q7" ></asp:BoundField>
                                <asp:BoundField DataField="Q8" HeaderStyle-ForeColor="Wheat" headerText="Q8" ></asp:BoundField>
                                <asp:BoundField DataField="Q9" HeaderStyle-ForeColor="Wheat" headerText="Q9" ></asp:BoundField>
                                <asp:BoundField DataField="Q10" HeaderStyle-ForeColor="Wheat" headerText="Q10" ></asp:BoundField>
                               </Columns> 
        
         <FooterStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
         <PagerStyle BackColor="#FFCC66" ForeColor="#333333" HorizontalAlign="Center" />
         <SelectedRowStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
         <HeaderStyle BackColor="#990000" Font-Bold="True" ForeColor="White" />
         <AlternatingRowStyle BackColor="White" />
        
    </asp:GridView>
</div>
</asp:Content>

