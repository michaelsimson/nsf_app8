<%@ Page Language="VB" Inherits="testing"  AutoEventWireup="false" CodeFile="testing.aspx.vb"  %>
<%@ Register
    Assembly="AjaxControlToolkit"
    Namespace="AjaxControlToolkit"
    TagPrefix="ajaxToolkit" %>
<%@ Register assembly="CrystalDecisions.Web, Version=10.5.3700.0, Culture=neutral, PublicKeyToken=692fbea5521e1304" namespace="CrystalDecisions.Web" tagprefix="CR" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">

<html xmlns="http://www.w3.org/1999/xhtml" >
<head id="Head1" runat="server">
    <title>Bank Transaction Reports</title>
    <script language="javascript" type="text/javascript">
			function PopupPicker(ctl,w,h)
			{
				var PopupWindow=null;
				settings='width='+ w + ',height='+ h + ',location=no,directories=no,menubar=no,toolbar=no,status=no,scrollbars=no,resizable=no,dependent=no';
				PopupWindow=window.open('DatePicker.aspx?Ctl=' + ctl,'DatePicker',settings);
				PopupWindow.focus();
}

//<%--function CleanWord(html) {

//    html = html.replace(/<o:p>\s*<\/o:p>/g, '');
//    html = html.replace(/<o:p>[\s\S]*?<\/o:p>/g, '');

//    // Remove mso-xxx styles.
//    html = html.replace(/\s*mso-[^:]+:[^;"]+;?/gi, '');

//    // Remove margin styles.
//    html = html.replace(/\s*MARGIN: 0cm 0cm 0pt\s*;/gi, '');
//    html = html.replace(/\s*MARGIN: 0cm 0cm 0pt\s*"/gi, "\"");

//    html = html.replace(/\s*TEXT-INDENT: 0cm\s*;/gi, '');
//    html = html.replace(/\s*TEXT-INDENT: 0cm\s*"/gi, "\"");

//    //	html = html.replace( /\s*TEXT-ALIGN: [^\s;]+;?"/gi, "\"" ) ;

//    html = html.replace(/\s*PAGE-BREAK-BEFORE: [^\s;]+;?"/gi, "\"");

//    html = html.replace(/\s*FONT-VARIANT: [^\s;]+;?"/gi, "\"");

//    html = html.replace(/\s*tab-stops:[^;"]*;?/gi, '');
//    html = html.replace(/\s*tab-stops:[^"]*/gi, '');


//    html = html.replace(/<a .*?href=(["'])mailto:(.+?)\1.*?><span .*?>(.+?)<\/span><\/a>/gi, '$3');
//    html = html.replace(/<a .*?href=(["'])mailto:(.+?)\1.*?>(.+?)<\/a>/gi, '$3');

//    // Remove FONT face attributes.
//    if (bIgnoreFont) {
//        html = html.replace(/\s*face="[^"]*"/gi, '');
//        html = html.replace(/\s*face=[^ >]*/gi, '');

//        html = html.replace(/\s*FONT-FAMILY:[^;"]*;?/gi, '');
//    }

//    // Remove Class attributes
//    html = html.replace(/<(\w[^>]*) class=([^ |>]*)([^>]*)/gi, "<$1$3");

//    // Remove styles.
//    if (bRemoveStyles)
//        html = html.replace(/<(\w[^>]*) style="([^\"]*)"([^>]*)/gi, "<$1$3");

//    // Remove style, meta and link tags
//    html = html.replace(/<STYLE[^>]*>[\s\S]*?<\/STYLE[^>]*>/gi, '');
//    html = html.replace(/<(?:META|LINK)[^>]*>\s*/gi, '');

//    // Remove empty styles.
//    html = html.replace(/\s*style="\s*"/gi, '');

//    html = html.replace(/<SPAN\s*[^>]*>\s*&nbsp;\s*<\/SPAN>/gi, '');

//    html = html.replace(/<SPAN\s*[^>]*><\/SPAN>/gi, '');

//    // Remove Lang attributes
//    html = html.replace(/<(\w[^>]*) lang=([^ |>]*)([^>]*)/gi, "<$1$3");

//    html = html.replace(/<SPAN\s*>([\s\S]*?)<\/SPAN>/gi, '$1');

//    html = html.replace(/<FONT\s*>([\s\S]*?)<\/FONT>/gi, '$1');

//    // Remove XML elements and declarations
//    html = html.replace(/<\\?\?xml[^>]*>/gi, '');

//    // Remove w: tags with contents.
//    html = html.replace(/<w:[^>]*>[\s\S]*?<\/w:[^>]*>/gi, '');

//    // Remove Tags with XML namespace declarations: <o:p><\/o:p>
//    html = html.replace(/<\/?\w+:[^>]*>/gi, '');

//    // Remove comments [SF BUG-1481861].
//    html = html.replace(/<\!--[\s\S]*?-->/g, '');

//    html = html.replace(/<(U|I|STRIKE)>&nbsp;<\/\1>/g, '&nbsp;');

//    html = html.replace(/<H\d>\s*<\/H\d>/gi, '');

//    // Remove "display:none" tags.
//    html = html.replace(/<(\w+)[^>]*\sstyle="[^"]*DISPLAY\s?:\s?none[\s\S]*?<\/\1>/ig, '');

//    // Remove language tags
//    html = html.replace(/<(\w[^>]*) language=([^ |>]*)([^>]*)/gi, "<$1$3");

//    // Remove onmouseover and onmouseout events (from MS Word comments effect)
//    html = html.replace(/<(\w[^>]*) onmouseover="([^\"]*)"([^>]*)/gi, "<$1$3");
//    html = html.replace(/<(\w[^>]*) onmouseout="([^\"]*)"([^>]*)/gi, "<$1$3");

//    // Remove empty tags (three times, just to be sure).
//    // This also removes any empty anchor
//    html = html.replace(/<([^\s>]+)(\s[^>]*)?>\s*<\/\1>/g, '');
//    html = html.replace(/<([^\s>]+)(\s[^>]*)?>\s*<\/\1>/g, '');
//    html = html.replace(/<([^\s>]+)(\s[^>]*)?>\s*<\/\1>/g, '');
//    html = html.replace(/(?:\r\n|\n|\r)/g, ' ');

//    //	return '<html><head><title></title></head><body>' + html + '</body></html>' ;

//    return html;

//}
--%>
</script>
</head>
<body>
    <form id="form1" runat="server">

<table border="0" runat="server" visible="false" >
<tr><td>
    <asp:TextBox ID="txtCore" runat="server" TextMode="MultiLine" Height="500px" Width="250px"></asp:TextBox></td><td>
        <asp:TextBox ID="txtResult" runat="server"  TextMode="MultiLine"  Height="500px" Width="250px"></asp:TextBox></td></tr>
<tr><td colspan ="2" align="center">
    <asp:Button ID="Button1" runat="server" Text="Button"  /></td></tr>
</table>
    <CR:CrystalReportViewer ID="CrystalReportViewer1" runat="server" 
        AutoDataBind="True" Height="1057px" ReportSourceID="CrystalReportSource1" 
        Width="869px" />
    <CR:CrystalReportSource ID="CrystalReportSource1" runat="server">
        <Report FileName="CrystalReport.rpt">
        </Report>
    </CR:CrystalReportSource>
</form> 
</body> 
</html> 

