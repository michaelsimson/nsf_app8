Imports System
Imports System.Data
Imports System.Data.SqlClient
Imports System.Diagnostics
Imports System.IO
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.DAL
Imports System.Collections
Partial Class BankTransStatus
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        Dim Begindate, EndDate, beginRecID, EndRecid As String
        Dim Cnt As Integer
        Dim tableExist As String = ""
        Dim reader As SqlDataReader
        Dim TableStr As String = "<br><Table border =1 cellpadding =3 cellspacing =0><tr><td align=Center>Table Name</td><td align=Center>Table Exists</td><td align=Center>Beg Rec#</td><td align=Center>Beg Date</td><td align=Center>End Rec#</td><td align=Center>End Date</td></tr>"
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "select count(*) from ChaseTemp")
        If Cnt > 0 Then
            Cnt = 0
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'JPMC_%' and BeginRecID is NULL  order by BankTranslogID Desc")
            While reader.Read()
                Begindate = reader("BeginDate")
                EndDate = reader("EndDate")
            End While
            TableStr = TableStr & "<tr><td align=Center>CHASETemp</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>CHASETemp</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'JPMC_%' and BeginRecID>0 and BeginRecID is not NULL  order by BankTranslogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>BankTrans - Chase</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT1temp")
        If Cnt > 0 Then
            Cnt = 0
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'HBT_S%' and BeginRecID is NULL  order by BankTranslogID Desc")
            While reader.Read()
                Begindate = reader("BeginDate")
                EndDate = reader("EndDate")
            End While
            TableStr = TableStr & "<tr><td align=Center>HBT1Temp</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>HBT1Temp</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'HBT_S%' and BeginRecID>0 and BeginRecID is not NULL  order by BankTranslogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>BankTrans - HBT1</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        Cnt = SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, "Select count(*) from HBT2Temp")
        If Cnt > 0 Then
            Cnt = 0
            reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'HBT_O%' and BeginRecID is NULL  order by BankTranslogID Desc")
            While reader.Read()
                Begindate = reader("BeginDate")
                EndDate = reader("EndDate")
            End While
            TableStr = TableStr & "<tr><td align=Center>HBT2Temp</td><td align=Center> Y </td><td align=Center>  </td><td align=Center>" & Begindate & " </td><td align=Center> </td><td align=Center> " & EndDate & "</td></tr>"
            reader.Close()
        Else
            TableStr = TableStr & "<tr><td align=Center>HBT2Temp</td><td align=Center> N </td><td align=Center>  </td><td align=Center> </td><td align=Center> </td><td align=Center></td></tr>"
        End If
        beginRecID = ""
        Begindate = ""
        EndRecid = ""
        EndDate = ""
        reader = SqlHelper.ExecuteReader(Application("ConnectionString"), CommandType.Text, "select TOP 1 BeginRecID, ISnull(CONVERT(VARCHAR(10), BegRecDate, 101),'') as BeginDate, EndRecID, ISNULL(CONVERT(VARCHAR(10),EndRecDate,101),'') as EndDate from BankTranslog where inputfilename like 'HBT_O%' and BeginRecID>0 and BeginRecID is not NULL  order by BankTranslogID Desc")
        While reader.Read()
            beginRecID = reader("BeginRecID")
            Begindate = reader("BeginDate")
            EndRecid = reader("EndRecID")
            EndDate = reader("EndDate")
        End While
        TableStr = TableStr & "<tr><td align=Center>BankTrans - HBT2</td><td align=Center>" & tableExist & "</td><td align=Center> " & beginRecID & "</td><td align=Center>" & Begindate & "</td><td align=Center>" & EndRecid & "</td><td align=Center> " & EndDate & "</td></tr>"
        reader.Close()
        TableStr = TableStr & "</Table><br><br>"
        Literal1.Text = TableStr.ToString
    End Sub

End Class
