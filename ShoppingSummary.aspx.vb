﻿Imports System
Imports System.Web
Imports System.IO
Imports System.Data.SqlClient
Imports Microsoft.ApplicationBlocks.Data
Imports System.Configuration
Imports System.Globalization
Partial Class ShoppingSummary
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("maintest.aspx")
        End If
        If Not IsPostBack Then
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                hlinkChapterFunctions.Text = "Back to Parent Functions"
                hlinkChapterFunctions.NavigateUrl = "UserFunctions.aspx"
            Else
                Hyperlink3.NavigateUrl = "ShoppingCatalog.aspx?id=2"
            End If
            lblCustIndID.Text = Session("CustIndID")
            loadgrid()
            LoadChapter(ddlChapter)
            ddlEvent.SelectedIndex = ddlEvent.Items.IndexOf(ddlEvent.Items.FindByValue(Session("SEventID")))
            ddlChapter.SelectedIndex = ddlChapter.Items.IndexOf(ddlChapter.Items.FindByValue(Session("SChapterID")))
        End If
    End Sub

    Private Sub LoadChapter(ByVal ddlst As DropDownList)
        Dim drStates As SqlDataReader
        Dim conn As New SqlConnection(Application("ConnectionString"))
        drStates = SqlHelper.ExecuteReader(conn, CommandType.Text, "select chapterID,chaptercode from chapter where Status='A' order by State,Chaptercode")
        While drStates.Read()
            ddlst.Items.Add(New ListItem(drStates(1).ToString(), drStates(0).ToString()))
        End While
    End Sub
    Private Sub loadgrid()
        Dim strSQL As String = "SELECT C.CatID, C.Category, C.ShortName, C.Description, C.UnitPrice, S.SaleTranID,S.Quantity ,S.Amount FROM Catalog C INNER JOIN SaleTran S ON C.CatID = S.CatID AND S.Memberid=" & lblCustIndID.Text & " WHERE S.PaymentReference is Null"
        Dim ds As DataSet = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSQL)
        Dim dt As DataTable = ds.Tables(0)
        Dim Count As Integer = dt.Rows.Count
        Dim dv As DataView = New DataView(dt)
        dgCatalog.DataSource = dt
        dgCatalog.DataBind()
        If (Count < 1) Then
            If Session("EntryToken").ToString.ToUpper() = "PARENT" Then
                Response.Redirect("ShoppingCatalog.aspx?id=1")
            Else
                Response.Redirect("ShoppingCatalog.aspx?id=2")
            End If
            lblTotal.Text = ""
        Else
            lblerr.Text = ""
            Session("SaleItems") = "(" & Session("SEventID") & ")(" & Session("SChapterID") & ")(" & Session("CustIndid") & ") : "
            Dim i As Integer
            For i = 0 To dt.Rows.Count - 1
                If i > 0 Then
                    Session("SaleItems") = Session("SaleItems") & ", "
                End If
                Session("SaleItems") = Session("SaleItems") & "(" & dt.Rows(i)("CatID") & ")(" & dt.Rows(i)("Quantity") & ")(" & String.Format("{0:f2}", CType(dt.Rows(i)("UnitPrice"), Decimal)) & ")"
            Next
            Session("SaleAmt") = CalcAmount()
            lblTotal.Text = "Total  :  $" & Session("SaleAmt")
        End If
    End Sub

    Function CalcAmount() As Decimal
        Dim totamount As Decimal = 0.0
        Dim item As DataGridItem
        For Each item In dgCatalog.Items
            Dim lblamount As Label = CType(item.FindControl("lblAmount"), Label)
            totamount = totamount + Decimal.Parse(lblamount.Text.Replace("$", ""))
        Next
        Return totamount
    End Function

    Protected Sub btnPay_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Response.Redirect("~/TermsAndConditions.aspx")
    End Sub
    Protected Sub dgCatalog_EditCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCatalog.EditCommand
        dgCatalog.EditItemIndex = CInt(e.Item.ItemIndex)
        loadgrid()
    End Sub

    Protected Sub dgCatalog_CancelCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCatalog.CancelCommand
       dgCatalog.EditItemIndex = -1
        loadgrid()
    End Sub

    Protected Sub dgCatalog_UpdateCommand(ByVal source As Object, ByVal e As System.Web.UI.WebControls.DataGridCommandEventArgs) Handles dgCatalog.UpdateCommand
        Dim SaletranID As Integer
        Dim Quantity As Integer
        Dim UnitPrice As Decimal
        Dim Amount As Decimal
        Dim sqlStr As String
        Dim txtQty As String
        Try
            SaletranID = CInt(CType(e.Item.FindControl("lblSaleTranID"), Label).Text)
            txtQty = CType(e.Item.FindControl("txtQuantity"), TextBox).Text
            UnitPrice = CDec(CType(e.Item.FindControl("lblUnitPrice"), Label).Text)
            If checkquantity(txtQty) Then
                lblerr.Text = "Error in Quantity"
                Exit Sub
            End If
            Quantity = CInt(CType(e.Item.FindControl("txtQuantity"), TextBox).Text)
           If Quantity > 0 Then
                Amount = CDec(Quantity) * UnitPrice
                sqlStr = "UPDATE SaleTran SET Quantity = " & Quantity & ",Amount = " & Amount & " WHERE SaleTranID=" & SaletranID
                SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
                lblerr.Text = "Updated Successfully"
            Else
                sqlStr = "Delete From SaleTran WHERE SaleTranID=" & SaletranID
                SqlHelper.ExecuteScalar(Application("ConnectionString"), CommandType.Text, sqlStr)
            End If
        Catch ex As SqlException
            'ex.message
            lblerr.Text = sqlStr & "<br> Error:updating the record" + ex.ToString
            Return
        End Try
        dgCatalog.EditItemIndex = -1
        loadgrid()
    End Sub

    Function checkquantity(ByVal Qty As String) As Boolean
        Dim flag As Boolean = False
        If IsNumeric(Qty) = False Then
            Return True
            Exit Function
        ElseIf Decimal.Parse(Qty) < 0 Then
            Return True
            Exit Function
        End If
        Return flag
    End Function
End Class
