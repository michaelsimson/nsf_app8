﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using Newtonsoft.Json;
using System.Drawing;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using System.Data.SqlClient;
using System.Web.Services;
using System.Net.Mail;
using System.Text.RegularExpressions;
using System.Collections;
using VRegistration;
using System.Net;
using System.IO;
public partial class SendEmail_HR : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        lblErrMsg.Text = "";
        if (Session["LoginID"] == null)
        {
            Response.Redirect("~/Maintest.aspx");
        }
        if (!IsPostBack)
        {
            Session["EmailSource"] = "Email_HR";
            PopulateDefaultRoles();
            LoadRoles();
        }
    }

    public void PopulateDefaultRoles()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select RoleId, Name from Role where Roleid in (1,2,3,4,5,6,7,8,9,10,11,12,13,30,35,36,71,80,81,82,83,84,85,89,90,93,94,96,97,100)";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdRoles.DataSource = ds;
                    GrdRoles.DataBind();
                    BtnSendEmail.Visible = true;
                    lblNoRecord.Visible = false;
                    chkSelectAll.Checked = true;
                    CreateTemporaryTable();
                }
                else
                {
                    BtnSendEmail.Visible = false;
                    lblNoRecord.Visible = true;
                }
            }
        }
        catch (Exception ex)
        {
        }


    }
    public void CreateTemporaryTable()
    {
        string cmdText = " drop table tmp_EmailHR";
        cmdText += " Create table tmp_EmailHR (RoleId int, Name varchar(max))";
        // cmdText += " insert into tmp_EmailHR(RoleId, Name) select RoleId, Name from Role where Roleid in (1,2,3,4,5,6,7,8,9,10,11,12,13,30,35,36,71,80,81,82,83,84,85,89,90,93,94,96,97,100) ";
        SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

    }
    public void LoadRoles()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select RoleId, Name from Role where Roleid not in (1,2,3,4,5,6,7,8,9,10,11,12,13,30,35,36,71,80,81,82,83,84,85,89,90,93,94,96,97,100)";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    ddlRole.DataValueField = "RoleId";
                    ddlRole.DataTextField = "Name";
                    ddlRole.DataSource = ds;
                    ddlRole.DataBind();
                    ddlRole.Items.Insert(0, new ListItem("Select", "-1"));
                }
                else
                {

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void ddlRole_SelectedIndexChanged(object sender, EventArgs e)
    {

    }
    protected void BtnAddNew_Click(object sender, EventArgs e)
    {
        string Cmdtext = string.Empty;
        if (ValidateRole() > 0)
        {
            Cmdtext = " select count(*) from tmp_EmailHR where RoleId=" + ddlRole.SelectedValue + " ";
            int count = 0;
            try
            {
                count = Convert.ToInt32(SqlHelper.ExecuteScalar(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext));
            }
            catch
            {
                count = 0;
            }
            if (count == 0)
            {
                Cmdtext = " insert into tmp_EmailHR(RoleId, Name) values(" + ddlRole.SelectedValue + ", '" + ddlRole.SelectedItem.Text + "') ";
                SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);

                RefreshRoles();
                lblErrMsg.Text = "Role added successfully.";
                lblErrMsg.ForeColor = Color.Green;
                ddlRole.SelectedValue = "-1";
            }
            else
            {
                lblErrMsg.Text = "Role exists already in the list. Please select different role.";
                lblErrMsg.ForeColor = Color.Red;
            }
        }
    }

    public int ValidateRole()
    {
        int Retval = 1;
        if (ddlRole.SelectedValue == "-1")
        {
            lblErrMsg.Text = "Please select Role.";
            lblErrMsg.ForeColor = Color.Red;
            Retval = -1;
        }
        return Retval;
    }

    public void RefreshRoles()
    {
        try
        {
            string cmdText = string.Empty;
            cmdText = "select RoleId, Name from tmp_EmailHR";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    GrdAdditionalRole.DataSource = ds;
                    GrdAdditionalRole.DataBind();
                    chkAddAllRole.Visible = true;
                    //  CreateTemporaryTable();
                }
                else
                {

                }
            }
        }
        catch (Exception ex)
        {
        }
    }
    protected void BtnSendEmail_Click(object sender, EventArgs e)
    {
        string CmdText = "";
        string EmailIds = "";
        string RoleIds = "";
        for (int i = 0; i < GrdRoles.Rows.Count; i++)
        {
            if (((CheckBox)GrdRoles.Rows[i].FindControl("chkRole") as CheckBox).Checked == true)
            {
                RoleIds += ((Label)GrdRoles.Rows[i].FindControl("lblRoleId") as Label).Text + ",";
            }

        }
        for (int i = 0; i < GrdAdditionalRole.Rows.Count; i++)
        {
            if (((CheckBox)GrdAdditionalRole.Rows[i].FindControl("chkRole") as CheckBox).Checked == true)
            {
                RoleIds += ((Label)GrdAdditionalRole.Rows[i].FindControl("lblRoleId") as Label).Text + ",";
            }
        }
        if (RoleIds != "")
        {
            RoleIds = RoleIds.TrimEnd(',');

            CmdText = "Select email from IndSpouse A where email is not null and exists (select * from Volunteer where A.AutoMemberID = memberid and Roleid in (" + RoleIds + ")) and (NewsLetter is null or NewsLetter<>3)";
            DataSet ds = new DataSet();
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, CmdText);

            if (null != ds && ds.Tables.Count > 0)
            {
                if (ds.Tables[0].Rows.Count > 0)
                {
                    foreach (DataRow dr in ds.Tables[0].Rows)
                    {
                        EmailIds += dr["Email"].ToString() + ",";
                    }
                }
            }
            if (EmailIds != "")
            {
                EmailIds = EmailIds.TrimEnd(',');
                Session["emaillist"] = EmailIds;
                Response.Redirect("emaillist.aspx");
            }
            else
            {
                lblErrMsg.ForeColor = Color.Red;
                lblErrMsg.Text = "No email exists";
            }
        }
        else
        {
            lblErrMsg.Text = "Please select the roles from the Table 1: Roles";
            lblErrMsg.ForeColor = Color.Red;
        }
    }
    protected void chkSelectAll_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < GrdRoles.Rows.Count; i++)
        {
            if (chkSelectAll.Checked == true)
            {
                ((CheckBox)GrdRoles.Rows[i].FindControl("chkRole") as CheckBox).Checked = true;
            }
            else
            {
                ((CheckBox)GrdRoles.Rows[i].FindControl("chkRole") as CheckBox).Checked = false;
            }
        }
    }
    protected void chkAddAllRole_CheckedChanged(object sender, EventArgs e)
    {
        for (int i = 0; i < GrdAdditionalRole.Rows.Count; i++)
        {
            if (chkAddAllRole.Checked == true)
            {
                ((CheckBox)GrdAdditionalRole.Rows[i].FindControl("chkRole") as CheckBox).Checked = true;
            }
            else
            {
                ((CheckBox)GrdAdditionalRole.Rows[i].FindControl("chkRole") as CheckBox).Checked = false;
            }
        }
    }
}