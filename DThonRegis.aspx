﻿<%@ Page Language="C#" AutoEventWireup="true" MaintainScrollPositionOnPostback="true" CodeFile="DThonRegis.aspx.cs" ValidateRequest ="false"  Inherits="DThonRegis" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head id="Head1" runat="server">
    <title>
        <asp:Literal ID="ltlTitle" runat="server"></asp:Literal>
        </title>
        <link href="css/style.css" rel="stylesheet" type="text/css" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<style type="text/css">
<!--
body {
	margin-left: 0px;
	margin-top: 0px;
	margin-right: 0px;
	margin-bottom: 0px;
	text-align:center;
	width:100%;
	background-color:#FFF;
}
.style1 {
	color: #FFFFFF;
	font-family: "Arial Rounded MT Bold";
	font-size: 14px;
}
    .style2
    {
        height: 288px;
    }
-->
</style>
</head>
<body>
    <form id="form1" runat="server">
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:100%; text-align:center">
    <tr><td align="center">
    <table  border="1"  cellpadding = "0" cellspacing = "0" bordercolor="#189A2C" style="width:1000px; text-align:center">
    <tr><td>
    <table  cellpadding = "0" cellspacing = "0"  border="0" style="width:100%; text-align:center">
    <tr><td> 
    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                    <tr>
                      <td width="24%" valign="top" align="right"  >
                      <img src="images_new/img_01.jpg" alt="" width="100%" height="144" border="0" usemap="#Map" />
                        <map name="Map" id="Map">
                          <area shape="circle" coords="105,81,61" href="http://www.northsouth.org/" />
                        </map></td>
                      <td width="76%" valign="top"><table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                          <td><table width="100%" border="0" cellspacing="0" cellpadding="0">
                            <tr>
                              <td width="51%"  background="images_new/topbg_1.jpg"><img src="images_new/img_02.jpg" alt="" width="399" height="109" /></td>
                              <td width="49%" valign="top" background="images_new/img_03.jpg"><table width="89%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                  <td width="30%">&nbsp;</td>
                                  <td width="70%">
                                  <table width="95%" height="31" border="0" align="center" cellpadding="0" cellspacing="0">
                                      <tr>
                                        <td valign="middle" style="padding-left:10px; padding-top:5px">
                                        <asp:Menu ID="NavLinks" CssClass="Nav_menu" runat="server" Orientation="Horizontal">
                                        <Items>                    
                                       
                                        </Items>     
                                        </asp:Menu>
                                        </td>
                                      </tr>
                                  </table></td>
                                </tr>
                                <tr><td colspan ="2" align="center" ><br />
                                    <asp:Label Font-Names="Arial Rounded MT Bold" Font-Size="25pt" ForeColor="White" ID="lblHeading" runat="server" ></asp:Label></td></tr>
                              </table></td>
                            </tr>
                          </table></td>
                        </tr>
                        <tr>
                          <td height="35" valign="middle" background="images_new/menubg.jpg">
<table border = "0" cellpadding = "3" cellspacing = "0" width="100%">
    <tr><td style="text-align:center; width :10px" >  </td><td style="vertical-align:middle; text-align:left;" class="style1">
        <asp:Label ID="lblPageCaption" runat="server" Font-Names="Arial Rounded MT Bold" Font-Size="14pt" ForeColor="White" Text="Noble Cause Through Brilliant Minds!" ></asp:Label>
     
        <asp:Literal ID="Literal2" runat="server"></asp:Literal> </td><td style="text-align:center; width :10px" ></td></tr> 
    </table>
</td>
                        </tr>
                      </table></td>
                    </tr>
                  </table>
    </td></tr>
   
      
    <tr><td height="5px" align="left">
         &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkParentRegistration" runat="server" Visible="false" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkVolunteerRegistration" visible="false" runat="server" NavigateUrl="~/VolunteerFunctions.aspx">Back to Volunteer Functions Page</asp:hyperlink>
        &nbsp;<asp:hyperlink CssClass="btn_02" id="hlinkDonorFunctions" visible="false" runat="server" NavigateUrl="~/DonorFunctions.aspx">Back to Donor Functions Page</asp:hyperlink>
        

     </td> </tr> 
    <tr><td align="center" >
       <table border="0" width="900px" cellpadding="0" cellspacing="0" align="center">
            <tr>
            <td width="6px" height="6px" background="images/tl.gif"></td>
            <td  height="6px" background="images/t.gif"></td>
            <td width="11px" height="6px" background="images/tr.gif"></td>
            </tr>

            <tr>
            <td  background="images/l.gif"></td>
            <td align="center" style="font-family:Tahoma; font-size:12px; font-weight :bold ">
              <table width="90%" cellpadding="3px" cellspacing="0px" border="0px">
              <tr><td align="right" colspan ="2" ><div style="width:70%; text-align:justify;font-family:Tahoma; font-size:12px; font-weight :normal ">
            <b>Note: Personalize your page and share it with friends and family</b><br />
                
                  <asp:Label ID="lblLbl1" runat="server" Text="1.Please select the child, if there is more than one."></asp:Label><br />
                2. Add a photo, write a personal message, set your fundraising target, and more...<br />
                3. Share your fundraising page with friends and family and ask them to make donations.<br />


                </div> </td></tr>
              
              <tr><td align="left"> Select Event</td><td align="left">: 
                  <asp:DropDownList ID="ddlEvent" runat="server"></asp:DropDownList>
              <span style="color :Red; font-family:Arial; vertical-align:top">&nbsp;*</span></td></tr>
              <tr><td align="left"> Relationship </td><td align="left">: 
                  <asp:DropDownList ID="ddlRelationShip" AutoPostBack="true" runat="server" 
                      onselectedindexchanged="ddlRelationShip_SelectedIndexChanged">
                      <asp:ListItem Selected="True">Self</asp:ListItem>
                      <asp:ListItem>Spouse</asp:ListItem>
                      <asp:ListItem>Child</asp:ListItem>
                  </asp:DropDownList>
              <span style="color :Red; font-family:Arial; vertical-align:top">&nbsp;*</span></td></tr>
              <tr id="trchild" ><td align="left"> 
                  <asp:Label ID="lblWM" runat="server" Text="Select Child"></asp:Label> </td><td align="left">: 
                         <asp:DropDownList ID="ddlChild" DataTextField="CName" 
                      DataValueField ="ChildNumber"  AutoPostBack="true" runat="server" 
                      onselectedindexchanged="ddlChild_SelectedIndexChanged">
                  </asp:DropDownList>
              <span style="color :Red; font-family:Arial; vertical-align:top">&nbsp;*</span></td></tr>
                <tr><td align="left">Child/Contact Email</td><td align="left">
                : <asp:TextBox ID="TxtEMail"  Width="200px"  runat="server"></asp:TextBox> 
                <span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;*</span>
                 <asp:RequiredFieldValidator ControlToValidate="TxtEMail" ID="rfvEmail" runat="server" ErrorMessage="Enter Mail"></asp:RequiredFieldValidator><br />
                     <asp:regularexpressionvalidator id="revPrimaryEmailInd" runat="server" ControlToValidate="TxtEMail" Display="Dynamic"
										ErrorMessage="Enter Valid EMail" ValidationExpression="\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*"></asp:regularexpressionvalidator>
                
               </td> </tr> 
               <tr runat="server" id="trchapter" ><td align="left">Chapter</td><td align="left">: 
                   <asp:DropDownList ID="ddlChapter" runat="server"></asp:DropDownList><span style="color :Red; font-family:Arial; vertical-align:top">
                   &nbsp;*</span>
               </td></tr>
                <tr><td align="left"> Target Amount</td><td valign="bottom" align="left">: 
                    <asp:TextBox ID="txtAmt" runat="server" Text="00" Width="50px"></asp:TextBox>
                    <asp:RangeValidator ID="RValidAmt" runat="server" 
                        ErrorMessage="Minimum target should be $250" ControlToValidate="txtAmt" 
                        Display="Dynamic" MaximumValue="999999" MinimumValue="250" Type="Currency"></asp:RangeValidator>
                    <asp:Label ID="lblShwMinimum" runat="server" Text="(Minimum $250)"></asp:Label></td></tr>
                 <tr><td align="left"> Your photo</td><td align="left">: 
                            <asp:FileUpload ID="fu" runat="server" /></td></tr>
                      <tr><td align="left"> Send Mails to  </td><td align="left" valign="middle">: 
                           <asp:TextBox ID="txtTSendEmails" Width="500px" TextMode="MultiLine" runat="server"></asp:TextBox><br />
                          Note: You can add any number of emails with semicolon (;) in between.
                   </td></tr>  
                  <tr ><td align="left"> Subject </td><td align="left">: 
                      <asp:TextBox ID="txtHeader" Text="Noble Cause Through Brilliant Minds!" runat="server" Width="500px"></asp:TextBox>
                      <asp:RequiredFieldValidator ID="RFVHeader" ControlToValidate="txtHeader" runat="server" ErrorMessage="*"></asp:RequiredFieldValidator>
                      </td></tr>
                   <tr ><td align="left" class="style2"> Email/Page Content</td><td align="left" 
                           class="style2">
 <FTB:FreeTextBox id="txtCustom"  runat="Server" Width="650px" Height="300px" Focus="false" Text="<p align='justify' style='font-family:Calibri; font-size:14px;'> Hello! I am excited to inform you that I have cleared the regional contest and made it to the National Finals organized by North South Foundation (NSF). Winners in the national round have been featured in the popular media and awarded scholarships.<br /><br />NSF was started over 22 years ago with the purpose of providing educational scholarships to needy children who display academic excellence. NSF funds these scholarships by raising donations in the US through spelling bees and direct donations. The NSF Scholarship program is designed to encourage excellence among the poor children who excel academically but need financial help to attend college. Each scholarship is $250 per student per year. NSF has distributed more than 5,000 scholarships to students who need financial support to pursue their quest for knowledge in engineering, medicine, polytechnic, science and other fields. The scholarship is an annual award and not a one-time payment. The student is eligible for the scholarship until graduation as long as he/she maintains high academic standards. The local chapters in various states of India invite applications from students, screen them and select the neediest students who eventually become NSF scholarship recipients. Scholarship amounts range from INR 5,000 to INR 12,000 per student per year.<br /><br />I have pledged to seek sponsorship to further the noble mission of this charity. My personal goal is to raise a minimum of $300 to provide scholarship for one student for one year. If you would please consider making a tax-deductible donation (Tax ID: 36-3659998) toward my goal of $300 it would be greatly appreciated. 100% of the money goes towards educational scholarships and any amount of money will help. Please help me make a difference in life of a deserving student who aspires to rise from poverty through education.<br /><br />If you have any questions about the contest, NSF or your donation, please feel free to contact our parents or visit the NSF website. Thank you for your kindness.</p>" SupportFolder="aspnet_client/FreeTextBox/" />

                                                                <asp:RequiredFieldValidator ControlToValidate="txtCustom" ID="RFVCustom" runat="server" 
                                                                    ErrorMessage="*"></asp:RequiredFieldValidator>

                           </td></tr>
                           
                         
                   <tr><td align="center" colspan="2"> 
                       <asp:Button ID="btnSubmit" runat="server" Text="Submit" 
                           onclick="btnSubmit_Click" />
                       <asp:HiddenField ID="hdnWalkMarID" runat="server" />
                       <asp:HiddenField ID="hdnImgName" runat="server" />
                       <!-- Histats.com START (hidden counter)--> <script type="text/javascript">document.write(unescape("%3Cscript src=%27http://s10.histats.com/js15.js%27 type=%27text/javascript%27%3E%3C/script%3E"));</script> <a href="http://www.histats.com" target="_blank" title="best tracker" ><script type="text/javascript" > try {Histats.start(1,1896397,4,0,0,0,""); Histats.track_hits();} catch(err){}; </script></a> <noscript><a href="http://www.histats.com" target="_blank"><img src="http://sstatic1.histats.com/0.gif?1896397&101" alt="best tracker" border="0"></a></noscript> <!-- Histats.com END --> 
                       </td></tr>
                   <tr><td align="center" colspan="2"> 
                       <asp:Label ID="lblMessage" runat="server" Font-Size="Small" ForeColor="Red"></asp:Label>
                       </td></tr>
              </table>
            <td background="images/r.gif"></td>
            </tr>

            <tr>
            <td width="6px" height="12px" background="images/bl.gif"></td>
            <td height="tpx" background="images/b.gif"></td>
            <td width="11px" height="12px" background="images/br.gif"></td>
            </tr>
            </table>
            
    </td></tr>
    </table>
    </td></tr></table> 
    </td></tr></table>
    
   
    </form>
</body>
</html>
