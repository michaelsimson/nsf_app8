Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports NorthSouth.DAL
Imports System.Data
Imports System.Data.SqlClient
Partial Class search_sponcer
    Inherits System.Web.UI.Page
    'Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
    '    If (GridView1.Rows.Count > 0) Then
    '        Label1.Visible = True
    '    End If
    '    If (GridView2.Rows.Count > 0) Then
    '        Label2.Visible = True
    '    End If
    'End Sub
    Dim dataAdapterInd As SqlDataAdapter
    Dim dataAdapterOrg As SqlDataAdapter
    Dim dsInd As New DataSet
    Dim dsOrg As New DataSet
    Dim sSQL As String
    Dim sSQLORG As String
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=" & Session("entryToken"))
        End If
        If Not IsPostBack Then

            Dim dsStates As DataSet
            Dim strSqlQuery As String
            strSqlQuery = "SELECT Distinct StateCode,Name FROM StateMaster"
            dsStates = SqlHelper.ExecuteDataset(Application("ConnectionString"), CommandType.Text, strSqlQuery)
            If dsStates.Tables.Count > 0 Then
                ddlstate.DataSource = dsStates.Tables(0)
                ddlstate.DataTextField = dsStates.Tables(0).Columns("Name").ToString
                ddlstate.DataValueField = dsStates.Tables(0).Columns("StateCode").ToString
                ddlstate.DataBind()
                ddlstate.Items.Insert(0, New ListItem("Select State", String.Empty))
            End If

            '*** Populate Chapter DropDown
            '*** Populate Chapter Names List
            Dim objChapters As New Chapter
            Dim dsChapters As New DataSet
            objChapters.GetAllChapters(Application("ConnectionString"), dsChapters)

            If dsChapters.Tables.Count > 0 Then
                ddlchapter.DataSource = dsChapters.Tables(0)
                ddlchapter.DataTextField = dsChapters.Tables(0).Columns("ChapterCode").ToString
                ddlchapter.DataValueField = dsChapters.Tables(0).Columns("ChapterID").ToString
                ddlchapter.DataBind()
                ddlchapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
            End If
            If Not Session("sSQL") Is Nothing Then
                If Session("sSQL").ToString().Trim.Length > 0 Then
                    sSQL = Session("sSQL")
                    sSQLORG = Session("sSQLORG")
                    loadrecords()
                End If
            End If
            Session("NavPath") = "Sponsor"
            'Dim dataConnection As New SqlConnection(Application("ConnectionString"))
            'If Len(sSQL) > 0 Then
            '    Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
            '    dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
            '    dataAdapterInd.Fill(dsInd)
            '    gvIndSpouse.DataSource = dsInd.Tables(0)
            '    gvIndSpouse.DataBind()
            'End If
            'If Len(sSQLORG) > 0 Then
            '    Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQLORG, dataConnection)
            '    dataAdapterOrg = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
            '    dataAdapterOrg.Fill(dsOrg)
            '    gvOrg.DataSource = dsOrg.Tables(0)
            '    gvOrg.DataBind()
            'End If

            'gvIndSpouse.EnableSortingAndPagingCallbacks = True
            'gvOrg.EnableSortingAndPagingCallbacks = True

            If Len(Session("LoginChapterID")) > 0 Then
                ddlchapter.SelectedValue = Session("LoginChapterID")
                ddlchapter.Enabled = False
            End If
            If Session("RoleID") = "4" Or Session("RoleID") = "5" Then
                ddlchapter.Items.Clear()
                Dim strSql As String
                strSql = "Select chapterid, chaptercode, state from chapter where "
                If Session("RoleID") = "4" Then
                    strSql = strSql & " clusterid in (Select clusterid from chapter where chapterid = " + Session("LoginChapterID") & ")"
                ElseIf Session("RoleID") = "5" Then
                    strSql = strSql & " chapterid = " + Session("LoginChapterID")
                End If
               
                strSql = strSql & " order by state, chaptercode"
                Dim con As New SqlConnection(Application("ConnectionString"))
                Dim i As Integer = 0
                Dim drNSFChapters As SqlDataReader = SqlHelper.ExecuteReader(con, CommandType.Text, strSql)
                While (drNSFChapters.Read())
                    i = i + 1
                    ddlchapter.Items.Add(New ListItem(drNSFChapters(1).ToString(), drNSFChapters(0).ToString()))
                End While
                If i > 1 Then
                    ddlchapter.Items.Insert(0, New ListItem("Select Chapter", String.Empty))
                    ddlchapter.Enabled = True
                End If
            End If
        End If
    End Sub
    Protected Sub btnsearch_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles btnsearch.Click
        Session.Remove("sSQLORG")
        Session.Remove("sSQL")
        sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,City,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <> 'Yes'"
        sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes'"
       
        Dim blnCheck As Boolean = False
        Dim Email As String = txtemail.Text
        Dim FirstName As String = txtfirstname.Text
        Dim LastName As String = txtlastname.Text
        Dim Street As String = txtstreet.Text
        Dim City As String = txtcity.Text
        Dim state As String = ddlstate.Text
        Dim ZipCode As String = txtzip.Text
        Dim NSFChapter As String = ddlchapter.SelectedValue

        lblMessage.Text = " "
       
        If (Len(Trim(FirstName)) > 0) Then
            sSQL = sSQL & " and FIRSTNAME like '%" & FirstName & "%'"
            sSQLORG = sSQLORG & " and ORGANIZATION_NAME like '%" & FirstName & "%'"
            blnCheck = True
        End If

        If (Len(Trim(LastName)) > 0) Then
            sSQL = sSQL & " and LASTNAME like '%" & LastName & "%'"
            sSQLORG = sSQLORG & " and ORGANIZATION_NAME like '%" & LastName & "%'"
            blnCheck = True
        End If

        If (Len(Trim(Street)) > 0) Then
            sSQL = sSQL & " and ADDRESS1 like '%" & Street & "%'"
            sSQLORG = sSQLORG & " and ADDRESS1 like '%" & Street & "%'"
            blnCheck = True
        End If

        If (Len(Trim(City)) > 0) Then
            sSQL = sSQL & " and CITY like '%" & City & "%'"
            sSQLORG = sSQLORG & " and CITY like '%" & City & "%'"
            blnCheck = True
        End If

        If (Len(Trim(state)) > 0 And Trim(state) <> "Other") Then
            sSQL = sSQL & " and STATE like '%" & state & "%'"
            sSQLORG = sSQLORG & " and STATE like '%" & state & "%'"
            blnCheck = True
        End If


        If (Len(Trim(ZipCode)) > 0) Then
            sSQL = sSQL & " and ZIP like '%" & ZipCode & "%'"
            sSQLORG = sSQLORG & " and ZIP like '%" & ZipCode & "%'"
            blnCheck = True
        End If

        If (Len(Trim(Email)) > 0) Then
            sSQL = sSQL & " and EMAIL like '%" & Email & "%'"
            sSQLORG = sSQLORG & " and EMAIL like '%" & Email & "%'"
            blnCheck = True
        End If
        'If Session("RoleID") = "5" Then
        '    Dim i As Integer
        '    NSFChapter = String.Empty
        '    For i = 1 To ddlchapter.Items.Count - 1
        '        If Len(NSFChapter) > 0 Then
        '            NSFChapter = NSFChapter & ",'" & ddlchapter.Items(i).Text & "'"
        '        Else
        '            NSFChapter = "'" & ddlchapter.Items(i).Text & "'"
        '        End If
        '    Next
        '    sSQL = sSQL & " and CHAPTER IN (" & NSFChapter & ")"
        '    sSQLORG = sSQLORG & " and NSF_CHAPTER IN(" & NSFChapter & ") "
        '    blnCheck = True
        'Else
        '    If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "[Select Chapter]") Then
        '        sSQL = sSQL & " and CHAPTER like '%" & NSFChapter & "%'"
        '        sSQLORG = sSQLORG & " and NSF_CHAPTER like '%" & NSFChapter & "%'"
        '        blnCheck = True
        '    End If
        'End If
        If Session("RoleID") = "5" Or Session("RoleID") = "4" Then
            Dim i As Integer
            NSFChapter = ddlchapter.SelectedValue

            If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                sSQL = sSQL & " and CHAPTERID = " & NSFChapter & ""
                sSQLORG = sSQLORG & " and CHAPTERID =" & NSFChapter & ""
                blnCheck = True
            Else
                For i = 1 To ddlchapter.Items.Count - 1
                    If Len(NSFChapter) > 0 And NSFChapter <> "Select Chapter" Then
                        NSFChapter = NSFChapter & ",'" & ddlchapter.Items(i).Value & "'"
                    Else
                        NSFChapter = "'" & ddlchapter.Items(i).Value & "'"
                    End If
                Next
                sSQL = sSQL & " and CHAPTERID IN (" & NSFChapter & ")"
                sSQLORG = sSQLORG & " and CHAPTERID IN(" & NSFChapter & ") "
                blnCheck = True
            End If
        Else
            If (Len(Trim(NSFChapter)) > 0 And NSFChapter <> "Select Chapter") Then
                sSQL = sSQL & " and CHAPTERID = " & NSFChapter & ""
                sSQLORG = sSQLORG & " and CHAPTERID = " & NSFChapter & ""
                blnCheck = True
            End If
        End If
        If (blnCheck = False) Then
            If Session("RoleID") = "5" Or Session("RoleID") = "4" Then
                sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <>'Yes' AND ChapterID IN(" & NSFChapter & ") ORDER BY LastName,FirstName ASC"
                sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes' AND ChapterId in(" & NSFChapter & ")  ORDER BY ORGANIZATION_NAME"
            Else
                sSQL = "Select MemberID,AutoMemberID,DONORTYPE,FirstName,MiddleInitial,LastName,Address1,State,Zip,EMail,HPhone,LiasonPerson,ReferredBy,Chapter from IndSpouse WHERE DeletedFlag <>'Yes' ORDER BY LastName,FirstName ASC"
                sSQLORG = "Select * from OrganizationInfo WHERE DeletedFlag <>'Yes' ORDER BY ORGANIZATION_NAME"
            End If
        Else
            sSQLORG = sSQLORG & " ORDER BY ORGANIZATION_NAME "
        End If

        Session("sSQL") = sSQL
        Session("sSQLORG") = sSQLORG
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvIndSpouse.DataSource = dsInd.Tables(0)
            gvIndSpouse.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try


        Dim dataCommandOrg As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQLORG, dataConnection)
        dataAdapterOrg = New Data.SqlClient.SqlDataAdapter(dataCommandOrg)
        Try
            dataAdapterOrg.Fill(dsOrg)
            gvOrg.DataSource = dsOrg.Tables(0)
            gvOrg.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        If (dsInd.Tables(0).Rows.Count < 1) And (dsOrg.Tables(0).Rows.Count < 1) Then
            lblMessage.Text = "No matching records found!"
        End If
    End Sub


    Protected Sub gvIndSpouse_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvIndSpouse.PageIndexChanging
        gvIndSpouse.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(Session("sSQL"), dataConnection)
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvIndSpouse.DataSource = dsInd.Tables(0)
            gvIndSpouse.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Protected Sub gvOrg_PageIndexChanging(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewPageEventArgs) Handles gvOrg.PageIndexChanging
        gvOrg.PageIndex = e.NewPageIndex
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(Session("sSQLORG"), dataConnection)
        dataAdapterOrg = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterOrg.Fill(dsOrg)
            gvOrg.DataSource = dsOrg.Tables(0)
            gvOrg.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
    End Sub

    Private Sub loadrecords()
        Dim dataConnection As New SqlConnection(Application("ConnectionString"))
        Dim dataCommandInd As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQL, dataConnection)
        dataAdapterInd = New Data.SqlClient.SqlDataAdapter(dataCommandInd)
        Try
            dataAdapterInd.Fill(dsInd)
            gvIndSpouse.DataSource = dsInd.Tables(0)
            gvIndSpouse.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try


        Dim dataCommandOrg As Data.SqlClient.SqlCommand = New SqlClient.SqlCommand(sSQLORG, dataConnection)
        dataAdapterOrg = New Data.SqlClient.SqlDataAdapter(dataCommandOrg)
        Try
            dataAdapterOrg.Fill(dsOrg)
            gvOrg.DataSource = dsOrg.Tables(0)
            gvOrg.DataBind()
        Catch ex As Exception
            lblMessage.Text = ex.Message
        End Try
        If (dsInd.Tables(0).Rows.Count < 1) And (dsOrg.Tables(0).Rows.Count < 1) Then
            lblMessage.Text = "No matching records found!"
        End If
    End Sub
  
    Protected Sub hplback_Click(ByVal sender As Object, ByVal e As System.EventArgs)
        Session.Remove("sSQLORG")
        Session.Remove("sSQL")
        Response.Redirect("VolunteerFunctions.aspx")
    End Sub
End Class

