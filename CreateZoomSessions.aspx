﻿<%@ Page Language="C#" AutoEventWireup="true" CodeFile="CreateZoomSessions.aspx.cs" Inherits="CreateZoomSessionOwkshop" MasterPageFile="~/NSFMasterPage.master" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div>
        <style type="text/css">
            .web_dialog_overlay {
                position: fixed;
                top: 0;
                right: 0;
                bottom: 0;
                left: 0;
                height: 100%;
                width: 100%;
                margin: 0;
                padding: 0;
                background: #000000;
                opacity: .15;
                filter: alpha(opacity=15);
                -moz-opacity: .15;
                z-index: 101;
                display: none;
            }

            .web_dialog {
                display: none;
                position: fixed;
                width: 520px;
                height: 200px;
                top: 55%;
                left: 50%;
                margin-left: -190px;
                margin-top: -100px;
                background-color: #ffffff;
                border: 2px solid #336699;
                padding: 0px;
                z-index: 102;
                font-family: Verdana;
                font-size: 10pt;
            }

            .web_dialog_title {
                border-bottom: solid 2px #336699;
                background-color: #336699;
                padding: 4px;
                color: White;
                font-weight: bold;
            }

                .web_dialog_title a {
                    color: White;
                    text-decoration: none;
                }

            .align_right {
                text-align: right;
            }
        </style>



        <link href="css/jquery.qtip.min.css" rel="stylesheet" />
        <link href="css/ezmodal.css" rel="stylesheet" />
        <script type="text/javascript" src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
        <script src="js/jquery.qtip.min.js"></script>

        <script src="js/ezmodal.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />

        <script src="js/jquery.toast.js"></script>
        <link href="css/jquery.toast.css" rel="stylesheet" />

        <script src="Scripts/jquery-1.9.1.js"></script>
        <script type="text/javascript" src="js/jquery-1.7.2.min.js"></script>
        <link type="text/css" href="css/pepper-grinder/jquery-ui.css" rel="stylesheet" />
        <script type="text/javascript" src="js/jquery-ui-1.8.19.custom.min.js"></script>

        <script type="text/javascript">
            function JoinMeeting() {

                var url = document.getElementById("<%=hdnHostURL.ClientID%>").value;

                window.open(url, '_blank');

            }


            function showmsg() {
                alert("You can only start/join the meeting up to 15 minutes before start time.");
            }
            $(function (e) {
                var pickerOpts = {
                    dateFormat: $.datepicker.regional['fr']
                };
                $.datepicker.setDefaults({ showOn: 'both', buttonImageOnly: true, buttonImage: 'images/calendar-green.gif', buttonText: 'Select Date' });

                $("#<%=ddlworkshopDate.ClientID %>").datepicker({ dateFormat: 'mm-dd-yy' });
                $("#<%=TxtEndDate.ClientID %>").datepicker({ dateFormat: 'mm-dd-yy' });
            });

            function ConfirmDelete() {
                if (confirm("Are you sure want to delete the sessions?")) {
                    document.getElementById("<%=BtnDeleteMeeting.ClientID%>").click();
                }
            }
            function popUp() {


            }
            $(document).on("click", ".btnInviteAttendee", function (e) {

                var joinURl = $(this).attr("attr-joinurl");
                var sessionKey = $(this).attr("attr-SessionKey");
                var hostId = $(this).attr("attr-HostId");
                var fromEmail = document.getElementById("<%=hdnLoginEmail.ClientID%>").value;
                var toEmail = document.getElementById("<%=hdnLoginOrgEmail.ClientID%>").value;
                $(".spnMemberTitle").text("Send Email");
                $(".dvSendEMailContent").show();
                $(".btnSenEmail").show();
                $(".txFrom").val(fromEmail);
                $(".txtTo").val("");
                $(".txtCC").val(toEmail);
                $(".txtSubject").val("");
                $(".txtMailBody").val("");
                $(".txtMailBody").val(joinURl);
                $(".spnErrMsg").text("");
                var jsonData = JSON.stringify({ SessionKey: sessionKey, HostId: hostId });

                $.ajax({
                    type: "POST",
                    url: "ZoomSessions.aspx/GetMeetingInfo",
                    data: jsonData,
                    contentType: "application/json; charset=utf-8",
                    dataType: "json",
                    success: function (data) {
                        var password = "";
                        var topic = "";
                        var timeZone = "";
                        var joinURl = "";
                        var date = "";
                        var time = "";
                        var coachName = "";
                        $.each(data.d, function (index, value) {
                            password = value.Password;
                            topic = value.Topic;
                            joinURl = value.JoinURL;
                            timeZone = value.TimeZone;
                            date = value.Date;
                            time = value.Time;
                            coachName = value.CoachName;
                        });
                        var content = "";
                        content = "Hi there, "
                        content += "\n\n";
                        content += "" + coachName + " is inviting you to a scheduled Zoom meeting";
                        content += "\n\n"
                        content += "Topic: " + topic + "";
                        content += "\n"
                        content += "Time: " + date + " " + time + " " + timeZone + "(US and Canada)";
                        content += "\n\n"
                        content += "Join from PC, Mac, Linux, iOS or Android: " + joinURl + "";
                        content += "\n"
                        content += "Password: " + password + "";
                        content += "\n\n"
                        content += "Or iPhone one-tap :";
                        content += "\n"
                        content += "US: +16465588656,,550101276# or +16699006833,,550101276# ";
                        content += "\n"
                        content += "Or Telephone:";
                        content += "\n"
                        content += "Dial(for higher quality, dial a number based on your current location): ";
                        content += "\n"
                        content += "US: +1 646 558 8656 or +1 669 900 6833";
                        content += "\n"
                        content += "Meeting ID: " + sessionKey + "";
                        content += "\n"
                        content += "International numbers available: https://zoom.us/u/bg1A7g1o";

                        $(".txtMailBody").val(content);
                        //$(".txtMailBody").val(replace($(".txtMailBody").val(), "<br>", "&#10;"));
                        $(".btnPopUP").trigger("click");
                    }
                });

            });

            function senEmailToCoachAndparents() {

                $("#fountainTextG").css("display", "block");
                $("#overlay").css("display", "block");
                var from = $(".txFrom").val();
                var to = $(".txtTo").val();

                var cc = $(".txtCC").val();
                var subject = $(".txtSubject").val();

                var body = $(".txtMailBody").val().replace(/\n/gi, '<br />');

                var jsonData = JSON.stringify({ FromEmail: from, ToEmail: to, Subject: subject, Body: body, CC: cc });

                var url = "SetupZoomSessions.aspx/SendEmailsCoachesAndParents";

                if (validateEmaul() == 1) {
                    $.ajax({
                        type: "POST",
                        url: url,
                        contentType: "application/json; charset=utf-8",
                        dataType: "json",
                        data: jsonData,
                        async: true,
                        cache: false,
                        success: function (data) {

                            var retVal = data.d;
                            if (retVal == 1) {
                                $(".spnErrMsg").text("Email has been sent successfully");
                                $("#fountainTextG").css("display", "none");
                                $("#overlay").css("display", "none");
                                $().toastmessage('showToast', {
                                    text: 'Emails sent successfully',
                                    sticky: true,
                                    position: 'top-right',
                                    type: 'success',
                                    close: function () { console.log("toast is closed ..."); }
                                });
                            }

                        },

                        failure: function (response) {
                            $("#fountainTextG").css("display", "none");
                            $("#overlay").css("display", "none");
                        }
                    });
                }
            }

            $(document).on("click", ".btnSenEmail", function (e) {

                senEmailToCoachAndparents();
            });

            function validateEmaul() {
                var retVal = 1;

                var from = $(".txFrom").val();
                var to = $(".txtTo").val();
                var cc = $(".txtCC").val();
                var subject = $(".txtSubject").val();
                var body = $(".txtMailBody").val();

                if (from == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter From email");
                } else if (to == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter To email");
                } else if (subject == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter Subject");
                } else if (body == "") {
                    retVal = -1;
                    $(".spnErrMsg").text("Please enter Content");
                }
                return retVal;
            }

            function DeleteMeeting() {
                if (confirm("Are you sure want to delete the attendee?")) {
                    document.getElementById("<%=BtnDelete.ClientID%>").click();
                }
            }

            function DeleteCoHost() {
                if (confirm("Are you sure want to delete the Co-Host?")) {
                    document.getElementById("<%=BtnDeletCoHost.ClientID%>").click();
                }
            }



        </script>
    </div>
    <asp:Button ID="BtnDeleteMeeting" runat="server" Text="Delete" OnClick="BtnDeleteMeeting_Click" Style="display: none;" />
    <asp:Button ID="BtnDelete" runat="server" Text="Delete" OnClick="BtnDelete_Click" Style="display: none;" />
    <asp:Button ID="BtnDeletCoHost" runat="server" Text="Delete" OnClick="BtnDeletCoHost_Click" Style="display: none;" />
    <table id="tblLogin" border="0" cellpadding="0" cellspacing="0" width="100%" style="border: none;" runat="server" align="center" class="tableclass">
        <tr>
            <td>
                <asp:HyperLink runat="server" ID="hlnkMainMenu" CssClass="btn_02" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx"></asp:HyperLink>

            </td>
        </tr>
        <tr>

            <td>
                <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
                    runat="server">
                    <strong>Zoom Sessions</strong>
                </div>
            </td>
        </tr>

    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvManipulateZoom" style="width: 1250px; position: relative; left: 110px;">
        <div id="dvFrstRow" style="padding: 10px;">
            <div style="width: 200px; float: left; padding: 10px; display: none;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Year</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlYear" Style="width: 110px;" runat="server" AutoPostBack="true">
                        <asp:ListItem Value="2017">2017</asp:ListItem>
                        <asp:ListItem Value="2018" Selected="True">2018</asp:ListItem>
                        <asp:ListItem Value="2019">2019</asp:ListItem>
                        <asp:ListItem Value="2020">2020</asp:ListItem>
                        <asp:ListItem Value="2021">2021</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 200px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Host</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlVolunteer" Style="width: 120px;" OnSelectedIndexChanged="ddlVolunteer_SelectedIndexChanged" AutoPostBack="true" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div style="width: 200px; float: left; padding: 10px;" id="dvRole" runat="server">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Role</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlRole" Style="width: 110px;" OnSelectedIndexChanged="ddlRole_SelectedIndexChanged" AutoPostBack="true" runat="server">
                    </asp:DropDownList>
                </div>
            </div>


            <div style="width: 200px; float: left; padding: 10px;" id="dvEvent" runat="server">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Event</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlEvent" OnSelectedIndexChanged="ddlEvent_SelectedIndexChanged" Style="width: 110px;" runat="server">
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 200px; float: left; padding: 10px;" id="dvTeam" runat="server">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Team</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlteam" Style="width: 110px;" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                    </asp:DropDownList>

                </div>
            </div>

            <%-- </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvSecondRow" style="padding: 10px;">--%>

            <div style="width: 200px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Co-Host</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlCoHost" Style="width: 110px;" runat="server">
                    </asp:DropDownList>
                </div>
            </div>

            <div style="width: 200px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Start Date</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:TextBox ID="ddlworkshopDate" Style="width: 95px;" runat="server">
                       
                    </asp:TextBox>

                </div>
            </div>
            <div style="width: 200px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Time (EST)</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlTime" Style="width: 110px;" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="08:00">8:00 AM</asp:ListItem>
                        <asp:ListItem Value="09:00">9:00 AM</asp:ListItem>
                        <asp:ListItem Value="10:00">10:00 AM</asp:ListItem>
                        <asp:ListItem Value="11:00">11:00 AM</asp:ListItem>
                        <asp:ListItem Value="12:00">12:00 PM</asp:ListItem>
                        <asp:ListItem Value="13:00">01:00 PM</asp:ListItem>
                        <asp:ListItem Value="14:00">02:00 PM</asp:ListItem>
                        <asp:ListItem Value="15:00">03:00 PM</asp:ListItem>
                        <asp:ListItem Value="16:00">04:00 PM</asp:ListItem>
                        <asp:ListItem Value="17:00">05:00 PM</asp:ListItem>
                        <asp:ListItem Value="18:00">06:00 PM</asp:ListItem>
                        <asp:ListItem Value="19:00">07:00 PM</asp:ListItem>
                        <asp:ListItem Value="20:00">08:00 PM</asp:ListItem>
                        <asp:ListItem Value="21:00">09:00 PM</asp:ListItem>
                        <asp:ListItem Value="22:00">10:00 PM</asp:ListItem>
                        <asp:ListItem Value="23:00">11:00 PM</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>

            <div style="width: 200px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Duration (Hrs)</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="ddlDuration" Style="width: 110px;" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="60">1.0</asp:ListItem>
                        <asp:ListItem Value="90">1.5</asp:ListItem>
                        <asp:ListItem Value="120">2.0</asp:ListItem>
                        <asp:ListItem Value="150">2.5</asp:ListItem>
                        <asp:ListItem Value="180">3.0</asp:ListItem>
                        <asp:ListItem Value="210">3.5</asp:ListItem>
                        <asp:ListItem Value="240">4.0</asp:ListItem>
                        <asp:ListItem Value="270">4.5</asp:ListItem>
                        <asp:ListItem Value="300">5</asp:ListItem>
                        <asp:ListItem Value="330">5.5</asp:ListItem>
                        <asp:ListItem Value="360">6.0</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 350px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Meeting Type</span>
                </div>
                <div style="width: 250px; float: left;">
                    <asp:RadioButton ID="RbtnRecurring" runat="server" Text="Recurrence" GroupName="Type" OnCheckedChanged="RbtnRecurring_CheckedChanged" AutoPostBack="true" Font-Bold="true" />
                    <asp:RadioButton ID="RbtnSingle" Checked="true" runat="server" Text="Single" GroupName="Type" OnCheckedChanged="RbtnRecurring_CheckedChanged" AutoPostBack="true" Font-Bold="true" />
                    <%-- <asp:DropDownList ID="ddlMeetingType" Style="width: 110px;" runat="server" OnSelectedIndexChanged="ddlMeetingType_SelectedIndexChanged" AutoPostBack="true">
                        <asp:ListItem Value="3">Recurring</asp:ListItem>
                        <asp:ListItem Value="2" Selected="True">Single</asp:ListItem>
                    </asp:DropDownList>--%>
                </div>
            </div>


            <%--  </div>--%>
            <%-- <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvThirdRow" style="padding: 10px;">--%>


            <div style="width: 200px; float: left; padding: 10px;" runat="server" id="dvEndDate" visible="false">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">End Date</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:TextBox ID="TxtEndDate" Style="width: 96px;" runat="server">
                       
                    </asp:TextBox>
                </div>
            </div>

            <div style="width: 200px; float: left; padding: 10px;" runat="server" id="dvRecurringType" visible="false">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Recurring Type</span>
                </div>
                <div style="width: 120px; float: left;">
                    <asp:DropDownList ID="DDLRecurrinType" Style="width: 120px;" runat="server">
                        <asp:ListItem Value="0">Select</asp:ListItem>
                        <asp:ListItem Value="1">Daily</asp:ListItem>
                        <asp:ListItem Value="2">Weekly</asp:ListItem>
                        <asp:ListItem Value="3">Monthly</asp:ListItem>
                    </asp:DropDownList>
                </div>
            </div>


            <div style="width: 400px; float: left; padding: 10px;">
                <div style="width: 70px; float: left;">
                    <span style="font-weight: bold;">Topic</span>
                </div>
                <div style="width: 320px; float: left;">
                    <asp:TextBox ID="txtTopic" Style="width: 300px;" runat="server"></asp:TextBox>
                </div>
            </div>

            <div style="width: 250px; float: left; display: none; padding: 10px;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Start Type</span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlStartType" Style="width: 140px;" runat="server">

                        <asp:ListItem Value="Video">Video</asp:ListItem>
                        <asp:ListItem Value="Scree_Share" Selected="True">Scree_Share</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>
            <div style="width: 250px; float: left; display: none; padding: 10px;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Join Before Host</span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlJoinBH" Style="width: 140px;" runat="server">

                        <asp:ListItem Value="true">True</asp:ListItem>
                        <asp:ListItem Value="false">False</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>



            <%-- </div>--%>

            <%-- <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvForthRow" style="padding: 10px; display: none;">--%>

            <div style="width: 250px; float: left; display: none; padding: 10px;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Audio</span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlAudio" Style="width: 140px;" runat="server">
                        <asp:ListItem Value="both">Both</asp:ListItem>
                        <asp:ListItem Value="Voip">Voip</asp:ListItem>
                        <asp:ListItem Value="telephony">Telephony</asp:ListItem>


                    </asp:DropDownList>
                </div>
            </div>

            <div style="width: 250px; float: left; display: none; padding: 10px;">
                <div style="width: 90px; float: left;">
                    <span style="font-weight: bold;">Host Video</span>
                </div>
                <div style="width: 150px; float: left;">
                    <asp:DropDownList ID="ddlHostVideo" Style="width: 140px;" runat="server">

                        <asp:ListItem Value="true">True</asp:ListItem>
                        <asp:ListItem Value="false" Selected="True">False</asp:ListItem>

                    </asp:DropDownList>
                </div>
            </div>



        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <div style="width: 250px;">
            <div style="float: left;">
                <asp:Button ID="btnCreateMeeting" runat="server" Text="Create Session" OnClick="btnCreateMeeting_Click1" />
            </div>
            <div style="float: left; margin-left: 10px;">
                <asp:Button ID="btnClear" runat="server" Text="Reset" OnClick="btnClear_Click1" />
            </div>
        </div>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:Label ID="lblErr" runat="server"></asp:Label>
    </center>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvAttendee" runat="server" style="width: 390px; margin-left: auto; background-color: #FFFFCC; height: 25px; padding: 10px; margin-right: auto;" visible="false">
        <div style="width: 250px; float: left;">
            <div style="width: 90px; float: left;">
                <span style="font-weight: bold;" runat="server" id="spnAttendee">Attendee</span>
            </div>
            <div style="width: 150px; float: left;">
                <asp:DropDownList ID="ddlAttendee" Style="width: 140px;" runat="server">
                </asp:DropDownList>
            </div>
        </div>
        <div style="float: left; margin-left: 10px;">
            <asp:Button ID="btnAddAttendee" runat="server" Text="Add" OnClick="btnAddAttendee_Click" />
        </div>
        <div style="float: left; margin-left: 10px;">
            <asp:Button ID="btnCancel" runat="server" Text="Cancel" OnClick="btnCancel_Click" />
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <center>
        <asp:Label ID="lblTitle" runat="server" Font-Bold="true">Table 1: Zoom Sessions</asp:Label>
    </center>
    <div style="width: 1250px; margin: auto;">

        <center>
            <asp:Label ID="lblNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
        </center>

        <asp:GridView ID="gvOnlineWkShopZoom" OnRowCommand="gvOnlineWkShopZoom_RowCommand" runat="server" Width="100%" AutoGenerateColumns="False">
            <Columns>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button ID="btnModify" runat="server" Text="Modify" CommandName="Modify" />
                        <asp:Button ID="btnDelete" runat="server" Text="Delete" CommandName="DeleteSession" />
                        <div style="display: none;">
                            <asp:Label ID="lblWebConfId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"ConfId") %>'></asp:Label>
                            <asp:Label ID="lblHostId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            <asp:Label ID="lblSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="Year" Visible="false" HeaderText="Year" />
                <asp:BoundField DataField="Name" HeaderText="Host Name" />
                <asp:BoundField DataField="RoleCode" HeaderText="Role" />
                <asp:BoundField DataField="teamName" HeaderText="team" />
                <asp:BoundField DataField="EventCode" HeaderText="Event" />
                <asp:BoundField DataField="Date" HeaderText="Meeting Date" DataFormatString="{0:MM-dd-yyyy}" />
                <asp:BoundField DataField="Time" HeaderText="Time" />
                <asp:BoundField DataField="Duration" HeaderText="Duration (Hrs)" />
                <asp:BoundField DataField="SessionType" HeaderText="SessionType" />
                <asp:BoundField DataField="RecurringType" HeaderText="Recurring Type" />
                <asp:BoundField DataField="EndDate" HeaderText="End Date" DataFormatString="{0:MM-dd-yyyy}" />
                <asp:BoundField DataField="Vroom" HeaderText="Vroom" />
                <asp:BoundField DataField="SessionKey" HeaderText="SessionKey" />
                <%--  <asp:BoundField DataField="WebinarLink" HeaderText="WebinarLink" />--%>
                <asp:TemplateField HeaderText="Host URL">
                    <ItemTemplate>

                        <div style="display: none;">

                            <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                            <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                            <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                            <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </div>
                        <asp:Button ID="btnJoinMeeting" runat="server" Text="Join Meeting" CommandName="Join" />


                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField HeaderText="Attendees">
                    <ItemTemplate>

                        <div style="display: none;">

                            <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>
                            <asp:Label ID="lblHostURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostURL") %>'></asp:Label>
                            <asp:Label ID="lblCMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"MemberId") %>'></asp:Label>
                            <asp:Label ID="lblAttEventId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"EventId") %>'></asp:Label>
                            <asp:Label ID="lblAttDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                            <asp:Label ID="lblAttTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                            <asp:Label ID="lblAttSessionKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>'></asp:Label>
                            <asp:Label ID="lblDuration" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Duration") %>'></asp:Label>
                            <asp:Label ID="lblHostKey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostId") %>'></asp:Label>
                        </div>
                        <input type="button" class="btnInviteAttendee" value="Invite" attr-joinurl='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>' attr-sessionkey='<%#DataBinder.Eval(Container.DataItem,"SessionKey") %>' attr-hostid='<%#DataBinder.Eval(Container.DataItem,"HostId") %>' />
                        <asp:Button ID="btnAdd" runat="server" Text="Add" CommandName="AddAttendee" />
                        <asp:Button ID="btnView" runat="server" Text="View" CommandName="ViewAttendee" />
                        <asp:Button ID="BtnAddHost" runat="server" Text="Add Co-Host" CommandName="AddHost" />
                        <%--  <asp:Button ID="" runat="server" attr-JoinURl=CssClass="BtnInviteAttendee" Text="invite Attendees" CommandName="invite" />--%>
                    </ItemTemplate>
                </asp:TemplateField>
                <%--  <asp:TemplateField HeaderText="Attendees">
                    <ItemTemplate>

                        <div style="display: none;">

                            <asp:Label ID="lblJoinURL" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"JoinURL") %>'></asp:Label>

                    
                        </div>

                    </ItemTemplate>
                </asp:TemplateField>--%>
            </Columns>

        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 20px;"></div>
    <div id="dvAttendeeSection" runat="server" visible="false">
        <center>
            <center>
                <asp:Label ID="Label1" runat="server" Font-Bold="true">Table 2: Zoom Attendees</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="dvHostNameS" runat="server" visible="false" style="position: relative; left: 220px;">
                <div style="float: left;">
                    <asp:Label ID="lblHostName" runat="server" Style="font-weight: bold;"></asp:Label>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <asp:Label ID="lblHostEmail" runat="server" Style="font-weight: bold;"></asp:Label>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblAttendeeNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <div id="dvAttendeeGrid" runat="server" visible="false">
                <asp:GridView ID="gvAttendee" runat="server" Width="65%" OnRowCommand="gvAttendee_RowCommand" AutoGenerateColumns="False" EnableModelValidation="True">
                    <Columns>
                        <asp:TemplateField>
                            <HeaderTemplate>
                                Ser#
                            </HeaderTemplate>
                            <ItemTemplate>
                                <asp:Label ID="lblSRNO" runat="server"
                                    Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                            </ItemTemplate>
                        </asp:TemplateField>

                        <asp:TemplateField HeaderText="Action">
                            <ItemTemplate>

                                <div style="display: none;">


                                    <asp:Label ID="lblSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                                    <asp:Label ID="lblAttendeeId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AttendeeId") %>'></asp:Label>

                                </div>
                                <asp:Button ID="btnDeleteAttendee" runat="server" Text="Delete" CommandName="DeleteAttendee" />


                            </ItemTemplate>
                        </asp:TemplateField>


                        <asp:BoundField DataField="Year" HeaderText="Year" Visible="false" />
                        <asp:BoundField DataField="Name" HeaderText="Attendee Name" />
                        <asp:BoundField DataField="Email" HeaderText="Attendee Email" />
                        <asp:BoundField DataField="HostName" Visible="false" HeaderText="Host Name" />
                        <asp:BoundField DataField="HostEmail" Visible="false" HeaderText="Host Email" />
                        <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                        <asp:BoundField DataField="Time" HeaderText="Time" />
                        <asp:BoundField DataField="Duration" HeaderText="Duration (Hrs" />
                        <asp:BoundField DataField="SessionType" HeaderText="SessionType" />

                        <asp:TemplateField HeaderText="Join URL">
                            <ItemTemplate>

                                <%--    <div style="display: none;">

                                <asp:Label ID="lblStHostID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"HostID") %>'></asp:Label>
                                <asp:Label ID="lblStSessionkey" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Sessionkey") %>'></asp:Label>
                                <asp:Label ID="lblDate" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Date") %>'></asp:Label>
                                <asp:Label ID="lblTime" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                            </div>--%>
                                <asp:Button ID="btnJoinMeeting" runat="server" Text="Join" CommandName="Join" />


                            </ItemTemplate>
                        </asp:TemplateField>


                    </Columns>

                </asp:GridView>
            </div>
        </center>
    </div>
    <div style="clear: both; margin-bottom: 20px;"></div>
    <div id="dvCoHost" runat="server" visible="false">
        <center>
            <center>
                <asp:Label ID="Label2" runat="server" Font-Bold="true">Table 3: Zoom Co-Host</asp:Label>
            </center>
            <div style="clear: both; margin-bottom: 5px;"></div>
            <center>
                <asp:Label ID="lblCoHostNoRecord" runat="server" Text="No record exists." Visible="false" ForeColor="Red"></asp:Label>
            </center>
            <div id="dvHostName" runat="server" visible="false" style="position: relative; left: 250px;">
                <div style="float: left;">
                    <asp:Label ID="lblHostN" runat="server" Style="font-weight: bold;"></asp:Label>
                </div>
                <div style="float: left; margin-left: 20px;">
                    <asp:Label ID="lblHostE" runat="server" Style="font-weight: bold;"></asp:Label>
                </div>
            </div>
            <div style="clear: both; margin-bottom: 5px;"></div>

            <asp:GridView ID="GrdCoHost" runat="server" Width="60%" AutoGenerateColumns="False" EnableModelValidation="True" OnRowCommand="GrdCoHost_RowCommand">
                <Columns>
                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Action">
                        <ItemTemplate>

                            <div style="display: none;">

                                <asp:Label ID="lblCoHostId" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"CoHostId") %>'></asp:Label>

                            </div>
                            <asp:Button ID="btnDeleteCoHost" runat="server" Text="Delete" CommandName="DeleteHost" />


                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:BoundField DataField="Year" HeaderText="Year" Visible="false" />
                    <asp:BoundField DataField="Name" HeaderText="Co-Host Name" />
                    <asp:BoundField DataField="Email" HeaderText="Co-Host Email" />

                    <asp:BoundField DataField="Date" HeaderText="Date" DataFormatString="{0:MM-dd-yyyy}" />
                    <asp:BoundField DataField="Time" HeaderText="Time" />
                    <asp:BoundField DataField="Duration" HeaderText="Duration (Hrs" />
                    <asp:BoundField DataField="SessionType" HeaderText="SessionType" />

                </Columns>

            </asp:GridView>

        </center>
    </div>

    <button type="button" class="btnPopUP" ezmodal-target="#demo" style="display: none;">Open</button>

    <div id="demo" class="ezmodal">

        <div class="ezmodal-container">
            <div class="ezmodal-header">
                <div class="ezmodal-close" data-dismiss="ezmodal">x</div>

                <span class="spnMemberTitle">Send Email</span>
            </div>

            <div class="ezmodal-content">

                <div class="dvSendEMailContent" style="display: none;">
                    <div align="center"><span class="spnErrMsg" style="color: red;"></span></div>
                    <div style="clear: both; margin-bottom: 5px;"></div>
                    <div>
                        <div class="from">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">From </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txFrom" style="width: 400px;" />
                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="To">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">To </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <input type="text" class="txtTo" style="width: 400px;" />
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="CC">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">CC </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtCC" style="width: 600px; height: 18px;"></textarea>

                            </div>
                        </div>
                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="subjext">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Subject </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="float: left; margin-left: 5px;">
                                <textarea class="txtSubject" style="width: 600px; height: 18px;"></textarea>
                            </div>
                        </div>

                        <div style="clear: both; margin-bottom: 10px;"></div>
                        <div class="body">
                            <div style="float: left; width: 150px;">
                                <span style="font-weight: bold;">Body </span>
                            </div>
                            <div style="float: left; width: 10px;">
                                <span style="font-weight: bold;">:</span>
                            </div>
                            <div style="clear: both; margin-bottom: 5px;"></div>
                            <div>
                                <asp:TextBox ID="txtPP" runat="server" Style="display: none;"></asp:TextBox>
                                <textarea class="txtMailBody" style="width: 800px; height: 150px;"></textarea>

                            </div>
                        </div>

                    </div>
                </div>


            </div>



            <div class="ezmodal-footer">
                <button id="Button1" type="button" class="btnSenEmail" style="display: none;">Send Email</button>
                <button type="button" class="btn" data-dismiss="ezmodal">Close</button>

            </div>

        </div>

    </div>

    <asp:HiddenField ID="hdnHostId" runat="server" Value="" />
    <asp:HiddenField ID="hdnHostURL" runat="server" Value="" />
    <asp:HiddenField ID="hdnSessionKey" runat="server" Value="" />

    <asp:HiddenField ID="hdnVroom" runat="server" Value="" />
    <asp:HiddenField ID="hdnConfId" runat="server" Value="" />
    <asp:HiddenField ID="hdnLoginEmail" runat="server" Value="" />
    <asp:HiddenField ID="hdnLoginOrgEmail" runat="server" Value="" />

    <asp:HiddenField ID="hdnEventId" runat="server" Value="" />
    <asp:HiddenField ID="hdnMeetingKey" runat="server" Value="" />
    <asp:HiddenField ID="hdnJoinURl" runat="server" Value="" />
    <asp:HiddenField ID="hdnDate" runat="server" Value="" />
    <asp:HiddenField ID="hdnTime" runat="server" Value="" />
    <asp:HiddenField ID="hdnMemberId" runat="server" Value="" />
    <asp:HiddenField ID="hdnAttendeeId" runat="server" Value="" />
    <asp:HiddenField ID="hdnCoHostId" runat="server" Value="" />
    <asp:HiddenField ID="hdnDuration" runat="server" Value="" />
    <asp:HiddenField ID="hdnWebConfId" runat="server" Value="" />

</asp:Content>
