<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="RegionalSummary.aspx.vb" Inherits="Parents_RegionalSummary" title="Regional Summary" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">
<div align="left">&nbsp;&nbsp;&nbsp;
<asp:hyperlink id="hlinkParentRegistration" runat="server" CssClass="btn_02" NavigateUrl="~/UserFunctions.aspx">Back to Parent Functions Page</asp:hyperlink>
</div>
			<table width="100%" class="tableclass">
				<tr>
					<td class="title02" align="center" colspan="2">Regional Summary
					</td>
				</tr>
				<tr>
					<td class="title04">Parent Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px">
						<table>
							<tr>
								<td><asp:label id="lblParentName" Runat="server" CssClass="txt01_strong"></asp:label> -  
								<asp:label ID="lblChapter" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress1" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblAddress2" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCity" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblHomePhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblWorkPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblCellPhone" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
							<tr>
								<td><asp:label id="lblEMail" Runat="server" CssClass="SmallFont"></asp:label></td>
							</tr>
						</table>
					</td>
				</tr>
				<tr><td></td></tr>
				<tr><td></td></tr>
				<tr id="trChild1" runat="server">
					<td class="SmallFont">Child/Children Detailed Information
					</td>
				</tr>
				<tr>
					<td style="width: 819px"><asp:datagrid id="dgChildList" runat="server" CssClass="GridStyle" DataKeyField="ChildNumber"
							CellPadding="4" BorderWidth="1px" OnItemDataBound="dgChildList_ItemDataBound" AutoGenerateColumns="False" AllowSorting="True" Width="100%">
							<FooterStyle CssClass="GridFooter"></FooterStyle>
							<SelectedItemStyle CssClass="SelectedRow"></SelectedItemStyle>
							<AlternatingItemStyle CssClass="GridAltItem" Wrap="False"></AlternatingItemStyle>
							<ItemStyle CssClass="GridItem" Wrap="False"></ItemStyle>
							<HeaderStyle CssClass="GridHeader" Wrap="False"></HeaderStyle>
							<Columns>
								<asp:TemplateColumn HeaderText="Name of Child" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor="#990000" SortExpression="FIRST_NAME">
									<ItemTemplate>
										<asp:Label id="label20" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.FIRST_NAME") %>' CssClass="SmallFont">
										</asp:Label>&nbsp;
										<asp:Label id="Label1" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.LAST_NAME") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="School Name" HeaderStyle-Font-Bold="true"  HeaderStyle-ForeColor="#990000" SortExpression="SchoolName">
									<ItemTemplate>
										<asp:Label id="Label5" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.SchoolName") %>' CssClass="SmallFont">
										</asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="Grade" HeaderStyle-Font-Bold="true"  SortExpression="Grade" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="Label2" runat="server" Text='<%# DataBinder.Eval(Container, "DataItem.GRADE") %>' CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>
								<asp:TemplateColumn HeaderText="" HeaderStyle-Font-Bold="true" SortExpression="Grade" HeaderStyle-Font-Underline="false" HeaderStyle-ForeColor="#990000">
									<ItemTemplate>
										<asp:Label id="lblEligibleContests" runat="server" CssClass="SmallFont"></asp:Label>
									</ItemTemplate>
								</asp:TemplateColumn>								
							</Columns>
						</asp:datagrid></td>
				</tr>
								<tr><td class="SmallFont">2/3rd of the Registration Fee is tax-deductible.
					</td>
				</tr>
				<tr id="TblContestInfo" visible = "false" runat ="server">
					<td  align="center" colspan="2">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<p  style ="text-align :justify; font-family:Calibri; font-size:14px; color :Red">There should be only one chapter coordinator (CC) for each center.  Here this rule was violated.  Please contact your CC to correct the situation. Once this is corrected, you will be able to register.</p></td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:button id="btnRegister" runat="server" CssClass="FormButtonCenter" Text="Click here to Register" ></asp:button>
                        <asp:HiddenField ID="hdnFlag" runat="server" Value="false" />
                    </td>
				</tr>
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestInfo" runat="server" CssClass="SmallFont" ForeColor="Red" Visible="False" ></asp:Label></td>
				</tr>
				
				<tr>
					<td class="ItemCenter" align="center" colspan="2"><asp:Label ID="lblContestDateInfo" runat="server" CssClass="SmallFont" ForeColor="Red"  ></asp:Label></td>
				</tr>
			</table>			
</asp:Content>



 
 
 