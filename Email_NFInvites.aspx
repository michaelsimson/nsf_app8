﻿<%@ Page Language="VB" MasterPageFile="~/MasterPage.master" AutoEventWireup="false" CodeFile="Email_NFInvites.aspx.vb" Inherits="Email_NFInvites" title="Send Emails application to Invites and Finals Registrants" %>
<%@ Register TagPrefix="FTB" Namespace="FreeTextBoxControls" Assembly="FreeTextBox" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

    <table style="width: 680px">
<tr><td style="width: 680px;">
<asp:HyperLink ID="hybback" runat="server" Text="Back to Volunteer Functions" NavigateUrl="~/VolunteerFunctions.aspx" Width="284px" ></asp:HyperLink>
</td></tr></table><table><tr><td align="center" style="width: 542px">
<asp:Label ID="lblerr" runat="server" ForeColor="Red" Text="You are not allowed to use this application." Visible="False" Width="471px"></asp:Label>
  </td></tr></table>
  <center>
<table id="tabletarget" runat="server" width="700px"  border="0" cellpadding="3" 
          cellspacing="0">
     
    <tr><td colspan ="4" align="center"> <H3>Send Emails to Invites and Finals Registrants</H3>
 </td></tr>
     <tr><td style="width:30px; height: 13px; text-align:left"></td>
        <td style="width:57px; height: 13px; text-align:left"></td>
        <td style="height: 13px; width: 84px; text-align:left"><asp:Label ID="lblregistrationtype" runat="server" Text="Category:" Width="140px" Font-Bold="true"></asp:Label> 
        </td><td style="height: 13px; text-align:left"><asp:DropDownList ID="drpregistrationtype" runat="server" >
                        <asp:ListItem Value="1" Selected="True" Text="All Invites"></asp:ListItem>
                        <asp:ListItem Value="2" Text="Paid Registrants"></asp:ListItem>
                        <asp:ListItem Value="3" Text="Pending"></asp:ListItem>
                        <asp:ListItem Value="4" Text="Invites less Paid"></asp:ListItem>
                        <asp:ListItem Value="5" Text="Invites less Paid less Pending"></asp:ListItem>
                        <asp:ListItem Value="6" Text="Photos Not yet sent"></asp:ListItem>
                  </asp:DropDownList></td>
                  </tr>
                
                 <tr style="height:25px"><td style="width: 30px"></td><td style="width: 57px"></td><td style="width: 84px"><asp:Label ID="lblproductgroup" Text="Product Group:" Width="140px" runat="server" Font-Bold="true"></asp:Label>
                 </td><td style="width: 332px; text-align:left"><asp:ListBox id="lstProductGroup" DataValueField="ProductGroupID" DataTextField="Name" SelectionMode="Multiple"  Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductGroup_SelectedIndexChanged" >
                                               </asp:ListBox>    <br /><br />
                 </td> </tr>
                 
                 <tr style="height:25px"><td style="width: 30px"></td><td style="width: 57px"></td><td style="width: 84px; text-align:left"><asp:Label ID="lblProductid" Text="Product:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
                  </td><td style="width: 332px; text-align:left"><asp:ListBox id="lstProductid" Enabled="false" DataValueField="ProductCode" DataTextField="Name" SelectionMode="Multiple" Width="150px" Height="75px" runat="server" AutoPostBack="true" OnSelectedIndexChanged="lstProductid_SelectedIndexChanged">
                                                </asp:ListBox>  <br /><br /></td></tr>
                  <tr style="height:25px; text-align:left"><td style="width: 30px"></td><td style="width: 57px"></td><td style="width: 84px; text-align:left"><asp:Label ID="lblyear" Text="Event Year:" runat="server" Width="140px" Font-Bold="true"></asp:Label>
                 </td><td style="width: 332px; text-align:left">
                 <asp:ListBox id="lstyear" SelectionMode="Multiple" Width="140px" Height="75px" runat="server" OnSelectedIndexChanged="lstyear_SelectedIndexChanged" AutoPostBack="true">
                 </asp:ListBox>
                  </td></tr>
                 <tr style="height:25px"><td style="width: 30px" colspan="4"></td>
                     </tr>
                 <tr><td style="height: 24px; width: 30px;"></td><td style="height: 24px; width: 57px;"></td><td style="height: 24px; text-align:center" colspan="2">
                 <asp:Button ID="btnselectmail" runat="server" Text="Continue" />
                 </td></tr>
  </table>
</center>
   </asp:Content>


