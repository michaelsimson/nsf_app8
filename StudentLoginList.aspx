<%@ Page Language="C#" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="true" CodeFile="StudentLoginList.aspx.cs" Inherits="StudentLoginList" Title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <div align="center">
        <table border="0" cellpadding="3" cellspacing="0" width="980">
            <tr>
                <td align="left">
                    <asp:HyperLink CssClass="btn_02" ID="hlinkChapterFunctions" runat="server" NavigateUrl="VolunteerFunctions.aspx">Back to Volunteer Functions</asp:HyperLink>
                    &nbsp;&nbsp;&nbsp;<asp:LinkButton CssClass="btn_02" ID="lblmain" runat="server" OnClick="LinkButton1_Click">Back to Main Page</asp:LinkButton></td>
            </tr>
        </table>
        <asp:Panel ID="Panel1" Visible="false" runat="server">
            <table>
            </table>
        </asp:Panel>
        <asp:Panel runat="server" ID="Pnldefault">
            <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);">
                Student Login List
        
            <br />
                <br />



                <br />

            </div>
        </asp:Panel>
        <asp:Panel runat="server" ID="PnlCoachtitle">
            <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);">
                Student Login List By Coach
        
            <br />
                <br />



                <br />

            </div>
        </asp:Panel>

        <asp:Panel runat="server" ID="PnlStudenttitle">
            <div align="center" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);">
                Student Login List By Student
        
            <br />
                <br />



                <br />

            </div>
        </asp:Panel>


        <asp:Panel runat="server" ID="pnlradio">
            <asp:RadioButton ID="coachid" runat="server" Text="By Coach"
                Checked="false" name="radio" GroupName="Option" AutoPostBack="true"
                OnCheckedChanged="coachid_CheckedChanged1" />

            &nbsp;<asp:RadioButton ID="studentid" runat="server" Text="By Student" Checked="false"
                name="radio" GroupName="Option" AutoPostBack="true"
                OnCheckedChanged="studentid_CheckedChanged" />
        </asp:Panel>
        <div align="center" style="margin-top: 20px;">

            <asp:Panel ID="pnldisp" runat="server" CssClass="ItemLabel">

                <asp:Label ID="Label4" runat="server" Text="Product Group" Width="70px"></asp:Label>
                &nbsp<asp:DropDownList ID="ddproductgroup" runat="server"
                    OnSelectedIndexChanged="ddproductgroup_SelectedIndexChanged1"
                    AutoPostBack="True" Width="125px">
                </asp:DropDownList>&nbsp
    <asp:Label ID="Label5" runat="server" Text="Product" Width="70px"></asp:Label>
                &nbsp<asp:DropDownList ID="ddproduct" runat="server" AutoPostBack="True" Width="125px" OnSelectedIndexChanged="ddproduct_SelectedIndexChanged">
                </asp:DropDownList>&nbsp
    <asp:Label ID="Label6" runat="server" Text="Level" Width="70px"></asp:Label>
                &nbsp<asp:DropDownList ID="ddlevel" runat="server"
                    AutoPostBack="True" Width="125px"
                    OnSelectedIndexChanged="ddlevel_SelectedIndexChanged">
                </asp:DropDownList>&nbsp
    <asp:Label ID="lblstudent" runat="server" Text="Student Name" Width="70px"></asp:Label>
                <asp:Label ID="lblcoach" runat="server" Text="Coach Name"></asp:Label>
                &nbsp<asp:DropDownList ID="ddname" runat="server" Width="150px" AutoPostBack="True"
                    OnSelectedIndexChanged="ddname_SelectedIndexChanged">
                </asp:DropDownList>&nbsp
       <asp:Button ID="Button1" runat="server"
           Text="Display" OnClick="Button1_Click" />
            </asp:Panel>
        </div>


        <asp:Label ID="lblmessage" runat="server" Text="Label" Visible="false"></asp:Label>





    </div>
    <div align="center"
        style="font-size: 26px; font-weight: bold; font-family: Calibri" dir="ltr">
    </div>

    <asp:Label ID="Lbldisp" runat="server" Text="No Record Found" ForeColor="Red"></asp:Label>

    <br />
    <br />


    <asp:GridView ID="GridDisplay"
        Style="width: 90%; margin: 0 auto;"
        AllowPaging="True" PageSize="50" runat="server"
        OnPageIndexChanging="GridDisplay_PageIndexChanging">
    </asp:GridView>

    </div>
    
       
</asp:Content>


