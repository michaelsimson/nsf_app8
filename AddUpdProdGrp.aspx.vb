Imports System.Data.SqlClient
Imports System.Data
Imports Microsoft.ApplicationBlocks.Data
Imports NorthSouth.BAL
Imports System.Globalization
Partial Class AddUpdProdGrp
    Inherits System.Web.UI.Page
    Protected Sub Page_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Load
        If LCase(Session("LoggedIn")) <> "true" Then
            Server.Transfer("login.aspx?entry=v")
        End If
        If Not IsPostBack Then
            If LCase(Session("LoggedIn")) <> "true" Then
                Server.Transfer("login.aspx?entry=" & Session("entryToken"))
            End If
            If (Not (Session("RoleId") = Nothing) And ((Session("RoleId").ToString() = "1") Or (Session("RoleId").ToString() = "2"))) Then
            Else
                BtnAdd.Visible = False
                GridView1.Columns(1).Visible = False
            End If
            GetRecords()
            GetYear()
        End If
    End Sub
    Private Sub GetRecords()
        Dim dsRecords As New DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM ProductGroup")
        Dim dt As DataTable = dsRecords.Tables(0)
        Dim dv As DataView = New DataView(dt)
        GridView1.DataSource = dt
        GridView1.DataBind()
        GridView1.Columns(1).Visible = True
        panel3.Visible = True
        If (dt.Rows.Count = 0) Then
            Pnl3Msg.Text = "No Records to Display"
        End If
    End Sub

    Protected Sub GridView1_RowCommand(ByVal sender As Object, ByVal e As System.Web.UI.WebControls.GridViewCommandEventArgs) Handles GridView1.RowCommand
        BtnAdd.Text = "Update"
        Dim index As Integer = Integer.Parse(e.CommandArgument.ToString())
        Dim TransID As Integer
        TransID = Val(GridView1.DataKeys(index).Value)
        If (TransID) And TransID > 0 Then
            Session("EventID") = TransID
            GetSelectedRecord(TransID, e.CommandName.ToString())
        End If
    End Sub
    Private Sub GetSelectedRecord(ByVal transID As Integer, ByVal status As String)
        Dim strsql As String = "Select * from ProductGroup where ProductGroupId=" & transID
        Dim dsRecords As DataSet
        dsRecords = SqlHelper.ExecuteDataset(Application("ConnectionString").ToString(), CommandType.Text, strsql)
        txtGrpCode.Text = dsRecords.Tables(0).Rows(0)("ProductGroupCode").ToString()
        txtGrpName.Text = dsRecords.Tables(0).Rows(0)("Name").ToString()
        txtGrpCode.Enabled = False
        ddlEvent.ClearSelection()

        Dim i As Integer
        For i = 0 To ddlEvent.Items.Count - 1
            If (ddlEvent.Items(i).Text = dsRecords.Tables(0).Rows(0)("EventCode").ToString()) Then
                ddlEvent.SelectedIndex = i
                Exit For
            End If
        Next
    End Sub

    Protected Sub BtnAdd_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnAdd.Click
        Dim Strsql As String
        If BtnAdd.Text = "Add" Then
            If txtGrpCode.Text.Length > 0 Then
                If txtGrpName.Text.Length > 0 Then
                    If Not ddlEvent.Text = "Select Event" Then
                        Dim cnt As Integer = 0
                        Strsql = "Select Count(ProductGroupCode) from ProductGroup where ProductGroupCode='" & txtGrpCode.Text & "' AND EventId=" & ddlEvent.SelectedItem.Value
                        cnt = SqlHelper.ExecuteScalar(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        If cnt = 0 Then
                            Strsql = "Insert Into ProductGroup(ProductGroupCode,Name, EventId, EventCode,  CreateDate, CreatedBy) Values ('"
                            Strsql = Strsql & txtGrpCode.Text & "','" & txtGrpName.Text & "'," & ddlEvent.SelectedItem.Value & ",'"
                            Strsql = Strsql & ddlEvent.SelectedItem.Text & "','" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "','" & Session("LoginID") & "')"
                            SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                            GetRecords()
                            clear()
                            Pnl3Msg.Text = "Record Added Successfully"
                        Else
                            Pnl3Msg.Text = "The Group Code with Selected Event already Exist"
                        End If
                    Else
                        Pnl3Msg.Text = "Please Select Event"
                    End If
                    Else
                        Pnl3Msg.Text = "Please Enter Product Group Name"
                    End If

            Else
                Pnl3Msg.Text = "Please Enter Product Group Code"
            End If
        ElseIf BtnAdd.Text = "Update" Then
            If txtGrpCode.Text.Length > 0 Then
                If txtGrpName.Text.Length > 0 Then
                    If Not ddlEvent.Text = "Select Event" Then
                        Strsql = "Update ProductGroup Set EventId=" & ddlEvent.SelectedItem.Value & ", EventCode='" & ddlEvent.SelectedItem.Text & "', ModifyDate='" & Convert.ToDateTime(Now).ToString("MM/dd/yyyy") & "', ModifiedBy='" & Session("LoginID") & "', ProductGroupCode='" & txtGrpCode.Text & "',Name = '" & txtGrpName.Text & "'  Where ProductGroupID=" & Session("EventID")
                        'MsgBox(Strsql)
                        SqlHelper.ExecuteNonQuery(Application("ConnectionString").ToString(), CommandType.Text, Strsql)
                        GetRecords()
                        clear()
                        Pnl3Msg.Text = "Record Updated Successfully"
                        BtnAdd.Text = "Add"
                    Else
                        Pnl3Msg.Text = "Please Select Valid Event"
                    End If
                Else
                    Pnl3Msg.Text = "Please Enter Product Group Name"
                End If

            Else
                Pnl3Msg.Text = "Please Enter Product Group Code"
            End If
        End If
    End Sub
    Private Sub clear()
        txtGrpCode.Text = ""
        txtGrpName.Text = ""
        ddlEvent.ClearSelection()
        ddlEvent.Items.FindByText("Select Event").Selected = True
        BtnAdd.Text = "Add"
        txtGrpCode.Enabled = True
        
    End Sub
    Private Sub GetYear()
        Dim dsRecords As SqlDataReader
        Dim i As Integer = 0
        dsRecords = SqlHelper.ExecuteReader(Application("ConnectionString").ToString(), CommandType.Text, "SELECT * FROM Event order by EventId Desc")
        While dsRecords.Read
            ddlEvent.Items.Insert(++i, New ListItem(dsRecords("EventCode"), dsRecords("EventId")))
        End While

        
    End Sub

    Protected Sub BtnCancel_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles BtnCancel.Click
        clear()
    End Sub
End Class
