﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" Inherits="CoachingRegisCount" CodeFile="CoachingRegisCount.aspx.cs" %>
<asp:Content ID="Content2" ContentPlaceHolderID="Content_main" Runat="Server">
    <div style="text-align:left">
   
        <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
   &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; &nbsp;&nbsp;&nbsp; 
</div>
<div runat="server" align="center">
    <table><tr><td style="width:80px"></td><td>
    <table border = "1" cellpadding = "0" cellspacing = "0" align="center" width="400px">
    <tr><td>
    <table border = "0" cellpadding = "3" cellspacing = "0" align="center" width="400px" class ="style2">
        <tr><td align="Center" colspan ="2" class="title02">Coaching Registration Count</td></tr>
   <tr><td align="left" >Product Group Name </td><td align="left">
          <asp:ListBox ID="lstProductGroup" SelectionMode="Multiple" runat="server" Width="150px" 
                        onselectedindexchanged="lstProductGroup_SelectedIndexChanged" AutoPostBack="true"></asp:ListBox>
      </td></tr>
       <tr><td align="left" >Product Name </td><td align="left">
           <asp:ListBox ID="lstProduct" SelectionMode="Multiple" runat="server" Width="150px"></asp:ListBox>
       </td></tr>
        <tr><td align="left" >Event Year</td><td align="left">
            <asp:ListBox ID="lstYear" SelectionMode="Multiple" runat="server" Width="150px"></asp:ListBox>
        </td></tr>
        <tr>
         <td align="left">Phase</td>        
         <td align="left"> <asp:DropDownList Width="130px" ID="ddlPhase" runat="server">
             <asp:ListItem Value="1">One</asp:ListItem>
             <asp:ListItem Value="2">Two</asp:ListItem>
         
             </asp:DropDownList>
        </td>
         </tr>
        <tr><td align="center" colspan="2">
            <asp:Button ID="btnGetCount" runat="server" Text="Get Count" 
                onclick="btnGetCount_Click" />
            </td></tr>
        <tr><td align="center" colspan="2">
            <asp:Label ID="lblerr" runat="server" ></asp:Label>
            </td></tr>
    </table>
    </td></tr>
    </table> </td><td>
    <table><tr><td>
    Sort :  <asp:DropDownList Width="180px" ID="ddSorting" runat="server" 
            AutoPostBack="True" OnSelectedIndexChanged="ddSorting_SelectedIndexChanged">
             <asp:ListItem Value="LastName,FirstName">By Last Name,First Name</asp:ListItem>
             <asp:ListItem Value="DayTime">By Day/Time</asp:ListItem>
             <asp:ListItem Value="Paid desc">By Number paid</asp:ListItem>
           <asp:ListItem Value="Approved desc">By Approved</asp:ListItem>
             </asp:DropDownList></td></tr></table></td></tr></table></div>
             
    <br />
    <div>
    <asp:DataGrid ID="grdCoaching" HorizontalAlign="Center" BorderWidth="1px"     runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" >
            	<FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000" 
                        Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
            <asp:BoundColumn ItemStyle-HorizontalAlign ="center" HeaderText="S.No." HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="SNo" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="FirstName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="FirstName" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="LastName" DataField="LastName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="ProductCode" DataField="ProductCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Phase" DataField="phase" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Session" DataField="SessionNo" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Day" DataField="day" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                  <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Time" DataField="time" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />

                <asp:BoundColumn ItemStyle-HorizontalAlign ="Center" HeaderText="Capacity" DataField="Capacity" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Center" HeaderText="Paid" DataField="paid" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Center" HeaderText="Pending" DataField="Pending" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"   />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Center" HeaderText="Approved" DataField="Approved" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Right" HeaderText="paidAmount" DataField="PaidAmt" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White" DataFormatString="{0:c}" />
                
            </Columns>
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
         </asp:DataGrid>
    </div>
    <br />
     <table border="0" cellpadding = "2" cellspacing = "0" style="text-align :left" >
            
        </table>
        <div align="center" id="HeadText"  visible="false"
        runat="server" >
   <b>Coaches with no Registrations</b>

       
    </div>
            <div>
    <asp:DataGrid ID="DataGridNoreg" HorizontalAlign="Center" BorderWidth="1px"     runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" >
            	<FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000" 
                        Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="center" HeaderText="S.No." HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="SNo" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="FirstName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="FirstName" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="LastName" DataField="LastName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="ProductCode" DataField="ProductCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Phase" DataField="phase" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Session" DataField="SessionNo" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Day" DataField="day" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                  <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Time" DataField="time" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />

                
            </Columns>
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
         </asp:DataGrid>
    </div>
      <div align="center" id="Div2"  
        runat="server" >
     <asp:Label ID="lblNoreg" runat="server" Text="" ></asp:Label>
     </div>
    <br />

       <div align="center" id="Div1"  visible="false" 
        runat="server" >
   <b>Coaches Signed Up, but not yet Accepted
</b>

       
    </div>
            <div>
    <asp:DataGrid ID="DGSigned" HorizontalAlign="Center" BorderWidth="1px"     runat="server" AutoGenerateColumns="False" 
                    CellPadding="4" ForeColor="#333333" GridLines="None" >
            	<FooterStyle Wrap="False" BackColor="#990000" Font-Bold="True" ForeColor="White"></FooterStyle>
				<HeaderStyle Wrap="False" HorizontalAlign="Center" BackColor="#990000" 
                        Font-Bold="True" ForeColor="White"></HeaderStyle>
                    <AlternatingItemStyle BackColor="White" />
                    <ItemStyle BackColor="#FFFBD6" ForeColor="#333333" />
            <Columns>
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="center" HeaderText="S.No." HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="SNo" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="FirstName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  DataField="FirstName" />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="LastName" DataField="LastName" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="ProductCode" DataField="ProductCode" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Level" DataField="Level" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Phase" DataField="phase" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Session" DataField="SessionNo" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                 <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Day" DataField="day" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />
                  <asp:BoundColumn ItemStyle-HorizontalAlign ="Left" HeaderText="Time" DataField="time" HeaderStyle-Font-Bold="true" HeaderStyle-ForeColor ="White"  />

                
            </Columns>
                    <SelectedItemStyle BackColor="#FFCC66" Font-Bold="True" ForeColor="Navy" />
            <PagerStyle Wrap="False"></PagerStyle>
         </asp:DataGrid>
    </div>
     <div align="center" id="Div3"  
        runat="server" >
     <asp:Label ID="lbcalsignup" runat="server" Text="" ></asp:Label>
     </div>
	</asp:Content>		
