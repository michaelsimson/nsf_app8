﻿using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System;
using System.Collections.Generic;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Data;
using System.Drawing;
using Microsoft.ApplicationBlocks.Data;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Collections;
using System.Globalization;
using System.Data.SqlTypes;
using Excel = Microsoft.Office.Interop.Excel;
using System.Reflection;
using System.Text;
using System.Web.Services;
using System.Data.Sql;
using System.Data.SqlClient;
//using SurveyMonkey;
using System.IO;
using System.Net.Mail;
using System.Text.RegularExpressions;
using NativeExcel;
using System.Threading;
using System.Net;
using System.Web.Script.Serialization;
using Newtonsoft.Json.Linq;
using Newtonsoft.Json;


public partial class TestSurveyDesign : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        if (!IsPostBack)
        {
            Tab1.CssClass = "Clicked";
           // MainView.ActiveViewIndex = 0;
        }
    }

    protected void Tab1_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Clicked";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Initial";
       // MainView.ActiveViewIndex = 0;
    }

    protected void Tab2_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Clicked";
        Tab3.CssClass = "Initial";
      //  MainView.ActiveViewIndex = 1;
    }

    protected void Tab3_Click(object sender, EventArgs e)
    {
        Tab1.CssClass = "Initial";
        Tab2.CssClass = "Initial";
        Tab3.CssClass = "Clicked";
       // MainView.ActiveViewIndex = 2;
    }
    public string apiKey = "6sTMyAgRSpCmTSIJIWFP8w";
    public string apiSecret = "86uc2sNH94tt0vDI4x3sOeKMYcXLXJISb4Fh";
    public void SwapReleaseDates()
    {
        string Cmdtext = string.Empty;
        Cmdtext = "select * from Coachreldates where eventyear=2017 and SReleasedate>AReleasedate";
        DataSet ds = new DataSet();
        try
        {
            string CoachRelId = "";
            string ARelDate = "";
            String SRelDate = "";
            ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, Cmdtext);
            if (ds.Tables[0] != null)
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {
                    CoachRelId = dr["CoachRelId"].ToString();
                    ARelDate = Convert.ToDateTime(dr["AReleasedate"].ToString()).ToShortDateString();
                    SRelDate = Convert.ToDateTime(dr["SReleasedate"].ToString()).ToShortDateString();

                    string UdpateText = " Update CoachRelDates set SReleasedate='" + ARelDate + "', AReleasedate='" + SRelDate + "' where CoachRelId=" + CoachRelId + "";
                    SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, UdpateText);
                }
            }
        }
        catch
        {

        }
    }

    public void listMeetings()
    {
        try
        {
            string cmdtext = "select * from CalSignup Cs inner join VirtualRoomLookUp vl on (cs.Vroom=vl.vroom) where cs.eventyear=2017   and cs.accepted='Y' and cs.Day='Sunday'";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, cmdtext);
            if ((ds.Tables[0].Rows.Count > 0))
            {
                foreach (DataRow dr in ds.Tables[0].Rows)
                {

                    string URL = string.Empty;
                    string service = "8";
                    URL = "https://api.zoom.us/v1/meeting/get";
                    string urlParameter = string.Empty;

                    urlParameter += "api_key=" + apiKey + "";
                    urlParameter += "&api_secret=" + apiSecret + "";
                    urlParameter += "&data_type=JSON";
                    urlParameter += "&id=" + dr["Meetingkey"].ToString() + "";
                    urlParameter += "&host_id=" + dr["Hostid"].ToString() + "";
                    urlParameter += "&page_size=300";
                    makeZoomAPICall(urlParameter, URL, service);
                }
                //  lblText.Text = hdnSessionKey.Value;
            }

        }

        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
    }

    public void makeZoomAPICall(string urlParameters, string URL, string serviceType)
    {
        try
        {


            HttpWebRequest objRequest = (HttpWebRequest)WebRequest.Create(URL);
            objRequest.Method = "POST";
            objRequest.ContentLength = urlParameters.Length;
            objRequest.ContentType = "application/x-www-form-urlencoded";

            // post data is sent as a stream
            StreamWriter myWriter = null;
            myWriter = new StreamWriter(objRequest.GetRequestStream());
            myWriter.Write(urlParameters);
            myWriter.Close();

            // returned values are returned as a stream, then read into a string
            string postResponse;
            HttpWebResponse objResponse = (HttpWebResponse)objRequest.GetResponse();
            using (StreamReader responseStream = new StreamReader(objResponse.GetResponseStream()))
            {
                postResponse = responseStream.ReadToEnd();

                responseStream.Close();


                var parser = new JavaScriptSerializer();

                dynamic obj = parser.Deserialize<dynamic>(postResponse);



                JObject json = (JObject)JsonConvert.DeserializeObject(postResponse);


                var serializer = new JavaScriptSerializer();
                hdnSessionKey.Value = hdnSessionKey.Value + "," + json["id"].ToString();

            }
            if (serviceType == "1")
            {


            }


        }
        catch (Exception ex)
        {
            CoachingExceptionLog.createExceptionLog("", "Exception", ex.Message, "Setup Zoom Sessions");
        }
        //System.Web.UI.ScriptManager.RegisterStartupScript(Page, Page.GetType(), "Msg", "StartMeeting(" + postResponse + ");", true);
    }
}