﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/NSFMasterPage.master" CodeFile="RecCoachesReport.aspx.cs" Inherits="RecCoachesReport" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" runat="Server">
    <asp:LinkButton ID="lbtnVolunteerFunctions" CssClass="btn_02" PostBackUrl="~/VolunteerFunctions.aspx" runat="server">Back to Volunteer Functions</asp:LinkButton>
    <div align="center" id="Iddonation" style="font-size: 26px; font-weight: bold; font-family: Calibri; color: rgb(73, 177, 23);"
        runat="server">
        Recruiting Coaches Report
             <br />
        <br />
    </div>
    <br />

    <table align="center" style="width: 700px;">
        <tr>

            <td align="left" nowrap="nowrap" style="font-weight: bold;">Year&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left" style="width: 140px;">
                <asp:DropDownList ID="ddlYear" runat="server" Width="100px">
                    <%--   <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="2015">2015</asp:ListItem>
                    <asp:ListItem Value="2014">2014</asp:ListItem>
                    <asp:ListItem Value="2013">2013</asp:ListItem>
                    <asp:ListItem Value="2012">2012</asp:ListItem>
                    <asp:ListItem Value="2011">2011</asp:ListItem>--%>
                </asp:DropDownList>
            </td>
            <td align="left" nowrap="nowrap" style="font-weight: bold;">Type Of Coaches&nbsp;&nbsp;&nbsp;&nbsp;</td>
            <td align="left">
                <asp:DropDownList ID="ddlCoachType" runat="server" OnSelectedIndexChanged="ddlCoachType_SelectedIndexChanged" AutoPostBack="true">
                    <asp:ListItem Value="0">Select</asp:ListItem>
                    <asp:ListItem Value="New Coaches">New Coaches</asp:ListItem>
                    <asp:ListItem Value="Returning Coaches">Returning Coaches</asp:ListItem>
                    <asp:ListItem Value="Multiple Sessions/Products"> Multiple Sessions/Products</asp:ListItem>
                    <asp:ListItem Value="Approve Calendar Signups">Approve Calendar Signups</asp:ListItem>
                    <asp:ListItem Value="Approved Multiple Sessions">Approved Multiple Sessions</asp:ListItem>
                </asp:DropDownList>
            </td>
            <td runat="server" visible="false">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <asp:Button ID="btnsubmit" runat="server"
                    Text="Submit" OnClick="btnsubmit_Click" /></td>
            <td align="center">
                <asp:Button ID="BtnExpo"
                    runat="server" Visible="false"
                    Text="Export to Excel" OnClick="BtnExpo_Click" /></td>


        </tr>

    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center">
        <tr>

            <td>
                <asp:Label ID="lblerr" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

            </td>
        </tr>
    </table>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div align="center" id="dvCoachingNew" runat="server" visible="false">

        <center>
            <div style="font-weight: bold;">Table 1: Was not Assigned Coach Role among those who did Volunteer SignUp</div>
        </center>
        <center>
            <span id="spnNotAssignedCoah" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <span id="spnNotAssignedCoach" visible="false" runat="server" style="font-weight: bold;"></span>
        </div>
        <div style="clear: both;">
        </div>

        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdNotAssignedCoach" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdNotAssignedCoach_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>

            </Columns>
        </asp:GridView>


    </div>
    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div id="dvAssignedCoach" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 2: Was Assigned Coach, but did not Signup Calendar</div>
        </center>
        <center>
            <span id="spnAssignedCoach" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <span id="spnAssignedCoachCount" visible="false" runat="server" style="font-weight: bold;"></span>
        </div>
        <div style="clear: both;">
        </div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdAssignedCoach" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdAssignedCoach_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div id="dvSignedUpCalendar" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 3: Signed up Calendar</div>
        </center>
        <center>
            <span id="spnSignedUpCal" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <span id="spnSignedupCalCount" visible="false" runat="server" style="font-weight: bold;"></span>
        </div>
        <div style="clear: both;">
        </div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdSignedUpCalendar" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdSignedUpCalendar_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div id="DvReturningSignedUpCal" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 1: Signed up Calendar</div>
        </center>
        <center>
            <span id="spnReturningSignedUpCal" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <span id="spnSignedUpCount" visible="false" runat="server" style="font-weight: bold;"></span>
        </div>
        <div style="clear: both;">
        </div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdReturningSignedUpCal" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdReturningSignedUpCal_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>
            </Columns>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>
    <div id="dvReturningNotSignedupCal" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 2: Did not sign up Calendar</div>
        </center>

        <center>
            <span id="spnReturningNotSignedupCal" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both;">
        </div>
        <div style="float: left;">
            <span id="spnNoCalSignup" visible="false" runat="server" style="font-weight: bold;"></span>
        </div>
        <div style="clear: both;">
        </div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdReturningNotSignedupCal" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdReturningNotSignedupCal_PageIndexChanging">

            <Columns>


                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 10px;">
    </div>

    <div id="dvMemberInfo" runat="server" visible="false">
        <center>
            <span id="spnMemberTitle" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table 2: Calendar Signups for Acceptance</span>
        </center>
        <center>

            <asp:GridView ID="GrdCoachSignUp" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-right: auto; margin-top: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdCoachSignUp_RowCommand" OnRowEditing="GrdCoachSignUp_RowEditing" OnRowDataBound="GrdCoachSignUp_RowDataBound" OnRowCancelingEdit="GrdCoachSignUp_RowCancelingEdit" OnRowUpdating="GrdCoachSignUp_RowUpdating">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" Visible="false"></asp:LinkButton>
                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" Visible="false"></asp:LinkButton>
                            <div style="display: none;">
                                <asp:Label runat="server" ID="lblHdnDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Phase") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnLevel" Text='<%#DataBinder.Eval(Container.DataItem,"SLevel") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPreference" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnUserID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblhdnAutoMemberID" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblmeetingKey" Text='<%#DataBinder.Eval(Container.DataItem,"MeetingKey") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblhostId" Text='<%#DataBinder.Eval(Container.DataItem,"HostId") %>'></asp:Label>
                               <asp:Label runat="server" ID="lblCoachName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SignUp ID" SortExpression="SignUpID">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSignUpID" Text='<%#DataBinder.Eval(Container.DataItem,"SignUpID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Volunteer Name" SortExpression="Name">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVolunteerName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVolunteerList" runat="server" Style="width: 100px;">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Year" SortExpression="EventYear">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlEventYear" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Code" SortExpression="EventCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventCode" Text='<%#DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phase" SortExpression="Phase">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Phase") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ProductGroup" SortExpression="ProductGroupCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="ProductCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlProduct" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level" SortExpression="Level">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblLevel" Text='<%#DataBinder.Eval(Container.DataItem,"SLevel") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlLevel" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Session" SortExpression="SessionNo">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlSessionNo" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day" SortExpression="Day">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlDay" runat="server">
                                <asp:ListItem Value="-1">Select Day</asp:ListItem>
                                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time" SortExpression="Time">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlTime" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted" SortExpression="Accepted">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlAccepted" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="Y">Y</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Preference" SortExpression="Preference">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPreferences" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlPreference" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="2">3</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Cap" SortExpression="MaxCapacity">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlMaxCap" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VRoom" SortExpression="VRoom">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVRoom" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UserID" SortExpression="UserID" Visible="false">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblUID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUserID" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Password" SortExpression="PWD" Visible="false">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPWD" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Years" SortExpression="Years">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblYears" Text='<%#DataBinder.Eval(Container.DataItem,"Years") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sessions" SortExpression="Sessions">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSessions" Text='<%#DataBinder.Eval(Container.DataItem,"Sessions") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </center>

        <div style="clear: both; margin-bottom: 20px;"></div>
        <div id="dvColorStatus" runat="server" visible="false">
            <div id="dvGreen" style="float: left;">
                <div style="width: 30px; height: 20px; background-color: #58d68d; float: left;"></div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Accepted Sessions</div>
            </div>
        </div>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div>
        <center>
            <asp:Button ID="btnColseTable2" runat="server"
                Text="Close Table2" Visible="false" OnClick="btnColseTable2_Click" />

        </center>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvCoachNotAccepted" runat="server" visible="false">

        <center>
            <div style="font-weight: bold;">Table 1A: Coach List - Nothing Accepted</div>
        </center>
        <center>
            <span id="Span2" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="gvNothingAccepted" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="gvNothingAccepted_PageIndexChanging" OnRowCommand="gvNothingAccepted_RowCommand">

            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>
                        <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none;">
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>
            </Columns>
        </asp:GridView>
        <br />
        <center>
            <div style="font-weight: bold;">Table 1: Coach List - Mixed: One or more accepted & one or more not accepted</div>
        </center>
        <center>
            <span id="Span1" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdCoachListNotYetAccepted" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdCoachListNotYetAccepted_PageIndexChanging" OnRowCommand="GrdCoachListNotYetAccepted_RowCommand">

            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none;">
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvMultiplePrdsAndSessionsCoaches" runat="server" visible="false">

        <center>
            <div style="font-weight: bold;">Table 1: Coaches with Multiple Products and/or Sessions</div>
        </center>
        <center>
            <span id="spnMultipleCoachSession" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <div id="dvMultipleSessionColorCode" runat="server" visible="false" style="float: right;">
            <div id="Div2" style="float: left;">
                <div style="width: 30px; height: 20px; background-color: #58d68d; float: left;"></div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Completely Accepted Products</div>
            </div>
            <div id="Div1" style="float: left; margin-left: 10px;">
                <div style="width: 30px; height: 20px; background-color: white; float: left; border: 1px solid black;">
                    <div style="color: #58d68d; font-size: 12px; font-weight: bold; height: 10px; margin-left: 2px; margin-top: 0; width: 15px;">
                        Text
                    </div>
                </div>
                <div style="float: left; font-weight: bold; margin-left: 10px;">Partially Accepted Products</div>
            </div>
        </div>
        <div style="clear: both; margin-bottom: 10px;"></div>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="grdMultiplePrdsAndSessionsCoaches" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="grdMultiplePrdsAndSessionsCoaches_PageIndexChanging" OnRowCommand="grdMultiplePrdsAndSessionsCoaches_RowCommand">

            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none;">
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>


    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvApprovedTwoSessions" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 1:  Approved Two Sessions</div>
        </center>
        <center>
            <span id="spnTableAppTwo" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdApprovedTwoSessions" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdApprovedTwoSessions_PageIndexChanging" OnRowCommand="GrdApprovedTwoSessions_RowCommand">

            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none;">
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <table align="center">
        <tr>

            <td>
                <asp:Label ID="LblSuccess" runat="server" ForeColor="Red" Font-Bold="true"></asp:Label>

            </td>
        </tr>
    </table>

    <div style="clear: both; margin-bottom: 10px;"></div>

    <div id="dvCalSignUp4AppThreeSess" runat="server" visible="false">
        <center>
            <span id="spnCalSignUp4AppThreeSess" style="font-family: Trebuchet MS; font-weight: bold;" runat="server">Table 4:  Calendar Signups for Approved Three or more Sessions</span>
        </center>
        <center>

            <asp:GridView ID="GrdCalSignUp4AppThreeSess" runat="server" AutoGenerateColumns="false" EnableViewState="true" Width="1200px" Style="margin-left: auto; margin-right: auto; margin-top: 10px;" HeaderStyle-BackColor="#ffffcc" OnRowCommand="GrdCoachSignUp_RowCommand" OnRowEditing="GrdCalSignUp4AppThreeSess_RowEditing" OnRowDataBound="GrdCalSignUp4AppThreeSess_RowDataBound" OnRowCancelingEdit="GrdCalSignUp4AppThreeSess_RowCancelingEdit" OnRowUpdating="GrdCalSignUp4AppThreeSess_RowUpdating">
                <Columns>
                    <asp:TemplateField>
                        <ItemTemplate>
                            <asp:LinkButton ID="lblEdit" runat="server" Text="Edit" CommandName="Edit"></asp:LinkButton>
                            <asp:LinkButton ID="lnkUpdate" runat="server" Text="Update" CommandName="Update" Visible="false"></asp:LinkButton>
                            <asp:LinkButton ID="lnkCancel" runat="server" Text="Cancel" CommandName="Cancel" Visible="false"></asp:LinkButton>
                            <div style="display: none;">
                                <asp:Label runat="server" ID="lblHdnDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPhase" Text='<%#DataBinder.Eval(Container.DataItem,"PhaseEx") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblEventID" Text='<%#DataBinder.Eval(Container.DataItem,"EventID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductGroupID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblProductID" Text='<%#DataBinder.Eval(Container.DataItem,"ProductID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPreference" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnUserID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblHdnPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                                <asp:Label runat="server" ID="lblhdnAutoMemberID" Text='<%#DataBinder.Eval(Container.DataItem,"MemberID") %>'></asp:Label>
                            </div>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField>
                        <HeaderTemplate>
                            Ser#
                        </HeaderTemplate>
                        <ItemTemplate>
                            <asp:Label ID="lblSRNO" runat="server"
                                Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="SignUp ID" SortExpression="SignUpID">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSignUpID" Text='<%#DataBinder.Eval(Container.DataItem,"SignUpID") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Volunteer Name" SortExpression="Name">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVolunteerName" Text='<%#DataBinder.Eval(Container.DataItem,"Name") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVolunteerList" runat="server" Style="width: 100px;">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Year" SortExpression="EventYear">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventYear" Text='<%#DataBinder.Eval(Container.DataItem,"EventYear") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlEventYear" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Event Code" SortExpression="EventCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblEventCode" Text='<%#DataBinder.Eval(Container.DataItem,"EventCode") %>'></asp:Label>
                        </ItemTemplate>

                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Phase" SortExpression="Phase">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPhase" Text='<%#DataBinder.Eval(Container.DataItem,"Phase") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="ProductGroup" SortExpression="ProductGroupCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPgCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductGroupCode") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Product" SortExpression="ProductCode">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblProductCode" Text='<%#DataBinder.Eval(Container.DataItem,"ProductCode") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlProduct" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Level" SortExpression="Level">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblLevel" Text='<%#DataBinder.Eval(Container.DataItem,"Level") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlLevel" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Session" SortExpression="SessionNo">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSession" Text='<%#DataBinder.Eval(Container.DataItem,"SessionNo") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlSessionNo" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="3">3</asp:ListItem>
                                <asp:ListItem Value="4">4</asp:ListItem>
                                <asp:ListItem Value="5">5</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Day" SortExpression="Day">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblDay" Text='<%#DataBinder.Eval(Container.DataItem,"Day") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlDay" runat="server">
                                <asp:ListItem Value="-1">Select Day</asp:ListItem>
                                <asp:ListItem Value="Monday">Monday</asp:ListItem>
                                <asp:ListItem Value="Tuesday">Tuesday</asp:ListItem>
                                <asp:ListItem Value="Wednesday">Wednesday</asp:ListItem>
                                <asp:ListItem Value="Thursday">Thursday</asp:ListItem>
                                <asp:ListItem Value="Friday">Friday</asp:ListItem>
                                <asp:ListItem Value="Saturday">Saturday</asp:ListItem>
                                <asp:ListItem Value="Sunday">Sunday</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Time" SortExpression="Time">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblTime" Text='<%#DataBinder.Eval(Container.DataItem,"Time") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlTime" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Accepted" SortExpression="Accepted">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblAccepted" Text='<%#DataBinder.Eval(Container.DataItem,"Accepted") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlAccepted" runat="server">
                                <asp:ListItem Value="">Select</asp:ListItem>
                                <asp:ListItem Value="Y">Y</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Preference" SortExpression="Preference">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPreferences" Text='<%#DataBinder.Eval(Container.DataItem,"Preference") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlPreference" runat="server">
                                <asp:ListItem Value="1">1</asp:ListItem>
                                <asp:ListItem Value="2">2</asp:ListItem>
                                <asp:ListItem Value="2">3</asp:ListItem>
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Max Cap" SortExpression="MaxCapacity">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblMaxCap" Text='<%#DataBinder.Eval(Container.DataItem,"MaxCapacity") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlMaxCap" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="VRoom" SortExpression="VRoom">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblVRoom" Text='<%#DataBinder.Eval(Container.DataItem,"VRoom") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>

                            <asp:DropDownList ID="ddlVRoom" runat="server">
                            </asp:DropDownList>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="UserID" SortExpression="UserID" Visible="false">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblUID" Text='<%#DataBinder.Eval(Container.DataItem,"UserID") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtUserID" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Password" SortExpression="PWD">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblPWD" Text='<%#DataBinder.Eval(Container.DataItem,"PWD") %>'></asp:Label>
                        </ItemTemplate>
                        <EditItemTemplate>
                            <asp:TextBox ID="txtPWD" runat="server" Width="50px"></asp:TextBox>
                        </EditItemTemplate>
                    </asp:TemplateField>

                    <asp:TemplateField HeaderText="Years" SortExpression="Years">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblYears" Text='<%#DataBinder.Eval(Container.DataItem,"Years") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>
                    <asp:TemplateField HeaderText="Sessions" SortExpression="Sessions">

                        <ItemTemplate>
                            <asp:Label runat="server" ID="lblSessions" Text='<%#DataBinder.Eval(Container.DataItem,"Sessions") %>'></asp:Label>
                        </ItemTemplate>
                    </asp:TemplateField>

                </Columns>
            </asp:GridView>

        </center>
    </div>
    <div style="clear: both; margin-bottom: 10px;"></div>
    <div>
        <center>
            <asp:Button ID="BtnCloaseTable4" runat="server"
                Text="Close Table 4" Visible="false" OnClick="btnColseTable4_Click" />

        </center>
    </div>

    <div style="clear: both; margin-bottom: 10px;"></div>
    <div id="dvApprovedThreeSessions" runat="server" visible="false">
        <center>
            <div style="font-weight: bold;">Table 2:  Approved Three or More Sessions</div>
        </center>
        <center>
            <span id="spnTblAppThreeSess" visible="false" runat="server" style="color: red; font-weight: bold;">No Record Exist</span>
        </center>
        <asp:GridView HorizontalAlign="Center" Width="1100px" RowStyle-HorizontalAlign="Left" ID="GrdApprovedThreeSessions" AutoGenerateColumns="false" runat="server" RowStyle-CssClass="SmallFont"
            BorderColor="#999999" BorderWidth="3px"
            BorderStyle="Solid" HeaderStyle-BackColor="#ffffcc" AllowPaging="true" PageSize="50" OnPageIndexChanging="GrdApprovedThreeSessions_PageIndexChanging" OnRowCommand="GrdApprovedThreeSessions_RowCommand">

            <Columns>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Action">
                    <ItemTemplate>

                        <asp:Button ID="btnSelectGroup" Width="60" runat="server" Text="Select" CommandName="Select" />
                        <div style="display: none;">
                            <asp:Label ID="lblMemberID" runat="server" Text='<%#DataBinder.Eval(Container.DataItem,"AutoMemberID") %>'>'></asp:Label>
                        </div>
                    </ItemTemplate>

                </asp:TemplateField>
                <asp:TemplateField>
                    <HeaderTemplate>
                        Ser#
                    </HeaderTemplate>
                    <ItemTemplate>
                        <asp:Label ID="lblSRNO" runat="server"
                            Text='<%#Container.DataItemIndex+1 %>'></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>

                <asp:BoundField DataField="AutoMemberID" HeaderText="Member Id"></asp:BoundField>
                <asp:BoundField DataField="FirstName" HeaderText="FirstName"></asp:BoundField>
                <asp:BoundField DataField="LastName" HeaderText="Last Name"></asp:BoundField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="EventID">
                    <ItemTemplate>
                        <asp:Label ID="lblEvent" runat="server" Text="13"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:TemplateField ItemStyle-HorizontalAlign="Center" HeaderText="Team">
                    <ItemTemplate>
                        <asp:Label ID="lblTeam" runat="server" Text="Coaching"></asp:Label>
                    </ItemTemplate>
                </asp:TemplateField>
                <asp:BoundField DataField="email" HeaderText="E-Mail"></asp:BoundField>
                <asp:BoundField DataField="HPhone" HeaderText="H Phone"></asp:BoundField>
                <asp:BoundField DataField="CPhone" HeaderText="C Phone"></asp:BoundField>
                <asp:BoundField DataField="city" HeaderText="City"></asp:BoundField>
                <asp:BoundField DataField="state" HeaderText="State"></asp:BoundField>
                <asp:BoundField DataField="Products" HeaderText="Products"></asp:BoundField>

            </Columns>
        </asp:GridView>
    </div>

    <input type="hidden" id="hdnMemberID" value="0" runat="server" />
    <input type="hidden" id="hdnFromVal" value="0" runat="server" />
    <input type="hidden" id="hdnSessionKey" value="" runat="server" />
    <input type="hidden" id="hdnHostURL" value="" runat="server" />

    <input type="hidden" id="hdnJoinMeetingUrl" value="" runat="server" />
    <input type="hidden" id="hdnSignupId" value="" runat="server" />
</asp:Content>
