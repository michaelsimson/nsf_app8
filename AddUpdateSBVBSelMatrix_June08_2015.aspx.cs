﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using Microsoft.ApplicationBlocks.Data;
using System.Collections.Generic;
using System.Collections;
using System.Web.UI.WebControls;
using System.Web;
using System.Drawing;
using System.Web.UI;
using System.Web.UI.HtmlControls;

public partial class AddUpdateSBVBSelMatrix : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
           try
        {
        if (!IsPostBack)
        {
            //Session["LoginID"] = 4240;
            //Session["RoleId"] = 1;
            if (Session["LoginID"] == null)
            {
                Response.Redirect("~/Maintest.aspx");
            }

            //if (Session["ChapterID"] == null && Session["entryToken"].ToString() != "Volunteer" && Session["entryToken"].ToString() != "Donor")
            //{
            //    Response.Redirect("~/login.aspx?entry=p");
            //}
            if (Convert.ToInt32(Session["RoleId"]) != 1 && Convert.ToInt32(Session["RoleId"]) != 97)
            {
                Response.Redirect("~/VolunteerFunctions.aspx");
            }
     
            int MaxYear = DateTime.Now.Year;
            //ArrayList list = new ArrayList();

            for (int i = MaxYear; i >= (DateTime.Now.Year-3); i--)
            {
                ddlYear.Items.Add(new ListItem(i.ToString(), i.ToString()));
            }


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
          
           
        }


       
        }
           catch (Exception ex) {
               //Response.Write("Err :" + ex.ToString());
           }
    }
   



    protected void btnSubmit_Click(object sender, EventArgs e)
    {
        if (ddlEvent.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Event";
            lblErr.Visible = true;
        }
        else if (ddlProductGroup.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product Group";
            lblErr.Visible = true;
        }
        else if (ddlProduct.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Product";
            lblErr.Visible = true;
        }
        else if (ddlPhase.SelectedIndex == 0)
        {
            lblErr.ForeColor = Color.Red;
            lblErr.Text = "Select Phase";
            lblErr.Visible = true;
        }
        else
        {
            displayGridView();
            
        }

       
    }
    protected void displayGridView()
    {

        if (ddlPhase.SelectedIndex == 1)
        {

            string checkexistsqry = "select SBVBSelMatrixID,RoundType,RoundFrom,RoundTo,PubUnpub,Level,SubLevel,WordCount1 as Words1,WordCount2 as Words2,WordCount3 as Words3,WordCount4 as Words4 from SBVBSelMatrix where Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode='"+ddlProduct.SelectedValue+"' and Phase=" + ddlPhase.SelectedValue + "";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
            DataTable dt;
            if (ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("SBVBSelMatrixID");
                dt.Columns.Add("RoundType");
                dt.Columns.Add("RoundFrom");
                dt.Columns.Add("RoundTo");
                dt.Columns.Add("PubUnpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("SubLevel");
                dt.Columns.Add("Words1");
                dt.Columns.Add("Words2");
                dt.Columns.Add("Words3");
                dt.Columns.Add("Words4");
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                if (!((ddlEvent.SelectedValue.Equals("1")) && (ddlProduct.SelectedValue.Equals("IVB"))))
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                }

                if(ddlEvent.SelectedValue.Equals("2") && ((ddlProduct.SelectedValue.Equals("SSB")||(ddlProduct.SelectedValue.Equals("JSB")))))
                {
                 dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                }
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Tie Breaker";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);

                if (ddlEvent.SelectedValue.Equals("1") && ddlProductGroup.SelectedValue.Equals("SB") && (ddlProduct.SelectedValue.Equals("JSB") || ddlProduct.SelectedValue.Equals("SSB")))
                {
                    dt.Rows.RemoveAt(1);
                }
            }
            gvVocabSelMatrix.DataSource = dt;
            gvVocabSelMatrix.DataBind();
            for(int xx=0;xx<gvVocabSelMatrix.Rows.Count;xx++)
            {
            gvVocabSelMatrix.Rows[xx].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            }
            //gvVocabSelMatrix.Rows[1].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            //gvVocabSelMatrix.Rows[2].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            //if (!((ddlEvent.SelectedValue.Equals("1")) && (ddlProduct.SelectedValue.Equals("IVB"))))
            //{
            //    gvVocabSelMatrix.Rows[3].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
            //}
            gvVocabSelMatrix.Visible = true;
            btnUpdate.Visible = true;
            lblErr.Visible = false;

           
           HtmlTableCell tcw2 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw2");
           tcw2.Visible = false;
           HtmlTableCell tcw3 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw3");
           tcw3.Visible = false;
           HtmlTableCell tcw4 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw4");
           tcw4.Visible = false;

        }
        else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
        {

            string checkexistsqry = "select SBVBSelMatrixID,RoundType,RoundFrom,RoundTo,PubUnpub,Level,SubLevel,WordCount1  as Words1,WordCount2 as Words2,WordCount3 as Words3,WordCount4 as Words4 from SBVBSelMatrix where Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode='"+ddlProduct.SelectedValue+"' and Phase=" + ddlPhase.SelectedValue + "";
            DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
            DataTable dt;
            if (ds.Tables[0].Rows.Count > 0)
            {
                dt = ds.Tables[0];
            }
            else
            {
                dt = new DataTable();
                DataRow dr;
                dt.Columns.Add("SBVBSelMatrixID");
                dt.Columns.Add("RoundType");
                //dt.Columns.Add("RoundCount");

                dt.Columns.Add("RoundFrom");
                dt.Columns.Add("RoundTo");

                dt.Columns.Add("PubUnpub");
                dt.Columns.Add("Level");
                dt.Columns.Add("SubLevel");
                dt.Columns.Add("Words1");
                dt.Columns.Add("Words2");
                dt.Columns.Add("Words3");
                dt.Columns.Add("Words4");
                dr = dt.NewRow();
                dr["SBVBSelMatrixID"] = "";
                dr["RoundType"] = "Regular";
                dr["PubUnpub"] = "P";
                dr["Level"] = "1";
                dr["SubLevel"] = "1";
                dr["Words1"] = "";
                dr["Words2"] = "";
                dr["Words3"] = "";
                dr["Words4"] = "";
                dt.Rows.Add(dr);
                if (ddlPhase.SelectedIndex != 3)
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                   
                }
                if (ddlEvent.SelectedValue.Equals("1") && ((ddlProduct.SelectedValue.Equals("SSB"))) && ddlPhase.SelectedValue.Equals("3"))
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                }
                if (ddlEvent.SelectedIndex == 1 && ddlPhase.SelectedIndex==2)
                {
                    dr = dt.NewRow();
                    dr["SBVBSelMatrixID"] = "";
                    dr["RoundType"] = "Regular";
                    dr["PubUnpub"] = "P";
                    dr["Level"] = "1";
                    dr["SubLevel"] = "1";
                    dr["Words1"] = "";
                    dr["Words2"] = "";
                    dr["Words3"] = "";
                    dr["Words4"] = "";
                    dt.Rows.Add(dr);
                   
                }
            }

            gvVocabSelMatrix.DataSource = dt;
            gvVocabSelMatrix.DataBind();
            gvVocabSelMatrix.Rows[0].Style.Add(HtmlTextWriterStyle.FontWeight, "Bold");
           

            gvVocabSelMatrix.Visible = true;
            btnUpdate.Visible = true;
            lblErr.Visible = false;
            
            if (ddlEvent.SelectedIndex==1)
            {
                HtmlTableCell tcw2 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw2");
                tcw2.Visible = false;
                HtmlTableCell tcw3 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw3");
                tcw3.Visible = false;
                HtmlTableCell tcw4 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw4");
                tcw4.Visible = false;
            }
        }
   
    }

    protected void ddlProductGroup_SelectedIndexChanged(object sender, EventArgs e)
    {


        fillProduct();
       
    }
    protected void fillProduct() { 
    
   
        //ddlProductGroup
        string ddlproductqry;


        ddlproductqry = "select Name,ProductId,ProductCode from Product where EventID=" + ddlEvent.SelectedValue + " and ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and Status='O'";
            
            DataSet dsstate = SqlHelper.ExecuteDataset(Application["connectionstring"].ToString(), CommandType.Text, ddlproductqry);
            
            ddlProduct.DataSource = dsstate;
            ddlProduct.DataTextField = "Name";
            ddlProduct.DataValueField = "ProductCode";
            ddlProduct.DataBind();


            ddlProduct.Items.Insert(0, new ListItem("Select Product", "-1"));
        
    
    }
    protected void btnUpdate_Click(object sender, EventArgs e)
    {
        int k;
        string roundtype;
        string strRC;
        string strRCfrom;
        string strRCto;
        string PubUnPub;
        string Level;
        string SubLevel;
        string wc1;
        string wc2;
        string wc3;
        string wc4;
        string updateqry="";
        string insertqry="";
        string strSBVBSelMatrixID;
        string errorText="";
        int RoundCount=0;
        int sumwc=0;
        int sumrc = 0;
        string checkexistsqry = "select * from SBVBSelMatrix where Year="+ddlYear.SelectedValue+" and Eventid=" + ddlEvent.SelectedValue + " and productGroupCode='" + ddlProductGroup.SelectedValue + "' and productCode='"+ddlProduct.SelectedValue+"' and Phase=" + ddlPhase.SelectedValue + "";
        DataSet ds = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, checkexistsqry);
        if (ds.Tables[0].Rows.Count > 0)
        {
            if (ddlPhase.SelectedIndex == 1)
            {
               
                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    HiddenField hfSBVBSelMatrixID = (HiddenField)gvVocabSelMatrix.Rows[i].FindControl("SBVBSelMatrixID");
                    strSBVBSelMatrixID = hfSBVBSelMatrixID.Value;
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();

                    Label lblroundtype = (Label)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    roundtype = lblroundtype.Text;
                    strRC = "";
                    TextBox tbRoundFrom = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundFrom");
                    strRCfrom = tbRoundFrom.Text;
                    TextBox tbRoundto = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundTo");
                    strRCto = tbRoundto.Text;
                    if (strRC.Equals(""))
                        strRC = "null";
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    wc1 = tbwc1.Text;
                    if (wc1.Equals(""))
                        wc1 = "0";
                    sumwc = sumwc + Convert.ToInt32(wc1);
                    
                    updateqry = updateqry + " update SBVBSelMatrix set PubUnpub='" + PubUnPub + "',Level=" + Level + ",SubLevel=" + SubLevel + ",WordCount1=" + wc1 + ",ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where SBVBSelMatrixID=" + strSBVBSelMatrixID + "";

                }
                //updating into SQL Table
                string strfindWCcontest = "select Questions from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int WCContestSQL = 0;
                DataSet dsWCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindWCcontest);
                if (dsWCContest.Tables[0].Rows.Count > 0)
                {
                    WCContestSQL = Convert.ToInt32(dsWCContest.Tables[0].Rows[0]["Questions"].ToString());
                    if (WCContestSQL == sumwc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Updated Successfully!";
                            lblErr.Visible = true;
                          
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Word1 Column should be equal to "+WCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of Questions is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
               
            }
            else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
            {
                errorText="";
                //getting roundcount from sql table
                string strfindRCcontest = "select Rounds from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int RCContestSQL = 0;
                DataSet dsRCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindRCcontest);
                RCContestSQL = Convert.ToInt32(dsRCContest.Tables[0].Rows[0]["Rounds"].ToString());

                for (int i = 0; i < ds.Tables[0].Rows.Count; i++)
                {
                    HiddenField hfSBVBSelMatrixID = (HiddenField)gvVocabSelMatrix.Rows[i].FindControl("SBVBSelMatrixID");
                    strSBVBSelMatrixID = hfSBVBSelMatrixID.Value;
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    Label lblroundtype = (Label)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    roundtype = lblroundtype.Text;

                    strRC = "";

                    TextBox tbRoundFrom = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundFrom");
                    strRCfrom = tbRoundFrom.Text;
                    TextBox tbRoundTo = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundTo");
                    strRCto = tbRoundTo.Text;
                    
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    //TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    //wc1 = tbwc1.Text;
                    //if (wc1.Equals(""))
                    if (strRCfrom.Equals("") || strRCto.Equals(""))
                    {
                        wc1 ="null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else if (Convert.ToInt32(strRCto) < Convert.ToInt32(strRCfrom))
                    {
                        errorText = "Value in To should be >=  value in From column";
                        wc1 = "null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else
                    {
                        int cs = 0;
                        RoundCount = Convert.ToInt32(strRCto) - Convert.ToInt32(strRCfrom) + 1;
                        if (ddlEvent.SelectedIndex == 1)
                        {
                            
                                cs = 20;
                            
                           
                            double resultValue = ((Convert.ToDouble(cs) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                            wc1=resultValue+"";
                            wc2 = "null";
                            wc3 = "null";
                            wc4 = "null";
                        }
                        else
                        {
                            
                                cs = 5;

                                
                                double resultValue1 = ((Convert.ToDouble(cs) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc1 = resultValue1 + "";
                                double resultValue2 = ((Convert.ToDouble(15) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc2 = resultValue2 + "";
                                double resultValue3 = ((Convert.ToDouble(25) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc3 = resultValue3 + "";
                                double resultValue4 = ((Convert.ToDouble(35) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc4 = resultValue4 + "";

                           }
                        
                    }
                    if (strRC.Equals(""))
                        strRC = "0";
                    sumrc = sumrc + RoundCount;

                    updateqry = updateqry + " update SBVBSelMatrix set RoundFrom=" + strRCfrom + ",RoundTo="+strRCto+", PubUnpub='" + PubUnPub + "',Level=" + Level + ",SubLevel=" + SubLevel + ",WordCount1=" + wc1 + ",WordCount2=" + wc2 + ",WordCount3=" + wc3 + ",WordCount4=" + wc4 + ",ModifiedDate=getDate(),ModifiedBy=" + Session["LoginID"] + " where SBVBSelMatrixID=" + strSBVBSelMatrixID + "";

                }
                //updating into SQL Table

                if (errorText.Length > 0)
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = errorText;
                    lblErr.Visible = true;
                }
                else
                {
               
                if (dsRCContest.Tables[0].Rows.Count > 0)
                {
                    
                    if (RCContestSQL == sumrc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, updateqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Updated Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Round Count Column should be equal to "+RCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of rounds is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }

            }

                if (ddlEvent.SelectedIndex == 1)
                {
                    HtmlTableCell tcw2 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw2");
                    tcw2.Visible = false;
                    HtmlTableCell tcw3 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw3");
                    tcw3.Visible = false;
                    HtmlTableCell tcw4 = (HtmlTableCell)gvVocabSelMatrix.HeaderRow.FindControl("thw4");
                    tcw4.Visible = false;
                }
            }
  
        }
        else
        {
            if (ddlPhase.SelectedIndex == 1)
            {
               
                for (int i = 0; i < gvVocabSelMatrix.Rows.Count; i++)
                {
    
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    Label lblroundtype = (Label)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    roundtype = lblroundtype.Text;

                    strRC = "";
                    
                    TextBox tbRoundFrom = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundFrom");
                    strRCfrom = tbRoundFrom.Text;
                    TextBox tbRoundto = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundTo");
                    strRCto = tbRoundto.Text;
                    if (strRC.Equals(""))
                        strRC = "null";
                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    wc1 = tbwc1.Text;
                    if (wc1.Equals(""))
                        wc1 = "0";
                    sumwc = sumwc + Convert.ToInt32(wc1);

                    insertqry = insertqry + " insert into SBVBSelMatrix(Year,EventID,Event,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnpub,Phase,RoundType,[Level],SubLevel,WordCount1,CreatedDate,CreatedBy) values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text + "',( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and EventID=" + ddlEvent.SelectedValue + "),'" + ddlProductGroup.SelectedValue + "',( select ProductID from product where ProductCode='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),'" + ddlProduct.SelectedValue + "','" + PubUnPub + "'," + ddlPhase.SelectedValue + ",'" + roundtype + "'," + Level + "," + SubLevel + "," + wc1 + ",getDate()," + Session["LoginID"] + ")";

                }
                //inserting into SQL Table
                string strfindWCcontest = "select Questions from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int WCContestSQL = 0;
                DataSet dsWCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindWCcontest);
                if (dsWCContest.Tables[0].Rows.Count > 0)
                {
                    WCContestSQL = Convert.ToInt32(dsWCContest.Tables[0].Rows[0]["Questions"].ToString());
                    if (WCContestSQL == sumwc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, insertqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Added Successfully!";
                            lblErr.Visible = true;
                          
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Word1 Column should be equal to "+WCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "#of Questions is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }
            }
            else if (ddlPhase.SelectedIndex == 2 || ddlPhase.SelectedIndex==3)
            {
               
                //getting round cound from sql Table
                string strfindRCcontest = "select Rounds from ContestSettings where ContestYear=" + ddlYear.SelectedValue + " and EventID=" + ddlEvent.SelectedValue + " and ProductGroup='" + ddlProductGroup.SelectedItem.Text + "' and Product='" + ddlProduct.SelectedItem.Text + "' and Phase=" + ddlPhase.SelectedValue + "";
                int RCContestSQL = 0;
                DataSet dsRCContest = SqlHelper.ExecuteDataset(Application["ConnectionString"].ToString(), CommandType.Text, strfindRCcontest);
                RCContestSQL = Convert.ToInt32(dsRCContest.Tables[0].Rows[0]["Rounds"].ToString());
                for (int i = 0; i < gvVocabSelMatrix.Rows.Count; i++)
                {
   
                    //roundtype = gvVocabSelMatrix.Rows[i].Cells[0].Text.Trim();
                    Label lblroundtype = (Label)gvVocabSelMatrix.Rows[i].FindControl("RoundType");
                    roundtype = lblroundtype.Text;

                    strRC = "";

                    TextBox tbRoundFrom = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundFrom");
                    strRCfrom = tbRoundFrom.Text;
                    TextBox tbRoundTo = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("RoundTo");
                    strRCto = tbRoundTo.Text;

                    DropDownList ddlPubUnPub = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlPubUnpub");
                    PubUnPub = ddlPubUnPub.SelectedItem.Text;
                    DropDownList ddlLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlLevel");
                    Level = ddlLevel.SelectedItem.Text;
                    DropDownList ddlSubLevel = (DropDownList)gvVocabSelMatrix.Rows[i].FindControl("ddlSublevel");
                    SubLevel = ddlSubLevel.SelectedItem.Text;
                    //TextBox tbwc1 = (TextBox)gvVocabSelMatrix.Rows[i].FindControl("Words1");
                    //wc1 = tbwc1.Text;
                    //if (wc1.Equals(""))
                    if (strRCfrom.Equals("") || strRCto.Equals(""))
                    {
                        wc1 = "null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else if (Convert.ToInt32(strRCto) < Convert.ToInt32(strRCfrom))
                    {
                        errorText = "Value in To should be >=  value in From column";
                        wc1 = "null";
                        wc2 = "null";
                        wc3 = "null";
                        wc4 = "null";
                    }
                    else
                    {
                        int cs = 0;
                        RoundCount = Convert.ToInt32(strRCto) - Convert.ToInt32(strRCfrom) + 1;
                        if (ddlEvent.SelectedIndex == 1)
                        {
                           
                                cs = 20;
                           
                           
                            double resultValue = ((Convert.ToDouble(cs) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                            wc1 = resultValue + "";
                            wc2 = "null";
                            wc3 = "null";
                            wc4 = "null";
                        }
                        else
                        {
                            
                                cs = 5;
                                double resultValue1 = ((Convert.ToDouble(cs) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc1 = resultValue1 + "";
                                double resultValue2 = ((Convert.ToDouble(15) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc2 = resultValue2 + "";
                                double resultValue3 = ((Convert.ToDouble(25) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc3 = resultValue3 + "";
                                double resultValue4 = ((Convert.ToDouble(35) + 2) * (Convert.ToDouble(RoundCount) + (Convert.ToDouble(RoundCount) / Convert.ToDouble(RCContestSQL))));
                                wc4 = resultValue4 + "";
                            
                        }


                       
                    }
                    if (strRC.Equals(""))
                        strRC = "0";
                        sumrc = sumrc + RoundCount;
                    insertqry = insertqry + " insert into SBVBSelMatrix(Year,EventID,Event,ProductGroupID,ProductGroupCode,ProductID,ProductCode,PubUnpub,Phase,RoundType,RoundFrom,RoundTo,[Level],SubLevel,WordCount1,WordCount2,WordCount3,WordCount4,CreatedDate,CreatedBy) values(" + ddlYear.SelectedValue + "," + ddlEvent.SelectedValue + ",'" + ddlEvent.SelectedItem.Text + "',( select ProductGroupID from productgroup where ProductGroupCode='" + ddlProductGroup.SelectedValue + "' and EventID=" + ddlEvent.SelectedValue + "),'" + ddlProductGroup.SelectedValue + "',( select ProductID from product where ProductCode='" + ddlProduct.SelectedValue + "' and EventId=" + ddlEvent.SelectedValue + "),'" + ddlProduct.SelectedValue + "','" + PubUnPub + "'," + ddlPhase.SelectedValue + ",'" + roundtype + "'," + strRCfrom + ","+strRCto+"," + Level + "," + SubLevel + "," + wc1 + "," + wc2 + "," + wc3 + "," + wc4 + ",getDate()," + Session["LoginID"] + ")";

                }
                //inserting into SQL Table
                if (errorText.Length > 0)
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = errorText;
                    lblErr.Visible = true;
                }
                else
                {
                if (dsRCContest.Tables[0].Rows.Count > 0)
                {
                    RCContestSQL = Convert.ToInt32(dsRCContest.Tables[0].Rows[0]["Rounds"].ToString());
                    if (RCContestSQL == sumrc)
                    {
                        //update for Phase:I
                        k = SqlHelper.ExecuteNonQuery(Application["ConnectionString"].ToString(), CommandType.Text, insertqry);
                        if (k > 0)
                        {
                            displayGridView();
                            lblErr.ForeColor = Color.Blue;
                            lblErr.Text = "Added Successfully!";
                            lblErr.Visible = true;
                            
                        }
                    }
                    else
                    {
                        // print an error message

                        lblErr.ForeColor = Color.Red;
                        lblErr.Text = " Sum of Round Count Column should be equal to "+RCContestSQL;
                        lblErr.Visible = true;
                    }
                }
                else
                {
                    lblErr.ForeColor = Color.Red;
                    lblErr.Text = "“#of rounds is not available in the ContestSettings table";
                    lblErr.Visible = true;
                }

            }
            }
         
                   }
     
        

    }
    protected void gvVocabSelMatrix_RowDataBound(object sender, GridViewRowEventArgs e)
    {
        if (e.Row.RowType == DataControlRowType.DataRow)
        {
            if (ddlPhase.SelectedIndex == 1)
            {
              
                    
                    HtmlTableCell tcw2 = (HtmlTableCell)(e.Row.FindControl("tdWords2"));
                    tcw2.Visible = false;
                    HtmlTableCell tcw3 = (HtmlTableCell)(e.Row.FindControl("tdWords3"));
                    tcw3.Visible = false;
                    HtmlTableCell tcw4 = (HtmlTableCell)(e.Row.FindControl("tdWords4"));
                    tcw4.Visible = false;

                    TextBox tbRoundFrom = (TextBox)e.Row.FindControl("RoundFrom");
                    tbRoundFrom.Enabled = false;
                    TextBox tbRoundto = (TextBox)e.Row.FindControl("RoundTo");
                    tbRoundto.Enabled = false;

               
            }
            else if (ddlPhase.SelectedIndex == 2)
            {

                TextBox tbWC1 = (e.Row.FindControl("Words1") as TextBox);
                tbWC1.Enabled = false;
                TextBox tbWC2 = (e.Row.FindControl("Words2") as TextBox);
                tbWC2.Enabled = false;
                TextBox tbWC3 = (e.Row.FindControl("Words3") as TextBox);
                tbWC3.Enabled = false;
                TextBox tbWC4 = (e.Row.FindControl("Words4") as TextBox);
                tbWC4.Enabled = false;
                if (ddlEvent.SelectedIndex == 1)
                {
                    HtmlTableCell tcw2 = (HtmlTableCell)(e.Row.FindControl("tdWords2"));
                    tcw2.Visible = false;
                    HtmlTableCell tcw3 = (HtmlTableCell)(e.Row.FindControl("tdWords3"));
                    tcw3.Visible = false;
                    HtmlTableCell tcw4 = (HtmlTableCell)(e.Row.FindControl("tdWords4"));
                    tcw4.Visible = false;
                }
            }
            else if (ddlPhase.SelectedIndex == 3)
            {

                TextBox tbWC1 = (e.Row.FindControl("Words1") as TextBox);
                tbWC1.Enabled = false;
                TextBox tbWC2 = (e.Row.FindControl("Words2") as TextBox);
                tbWC2.Enabled = false;
                TextBox tbWC3 = (e.Row.FindControl("Words3") as TextBox);
                tbWC3.Enabled = false;
                TextBox tbWC4 = (e.Row.FindControl("Words4") as TextBox);
                tbWC4.Enabled = false;

                HtmlTableCell tcw2 = (HtmlTableCell)(e.Row.FindControl("tdWords2"));
                tcw2.Visible = false;
                HtmlTableCell tcw3 = (HtmlTableCell)(e.Row.FindControl("tdWords3"));
                tcw3.Visible = false;
                HtmlTableCell tcw4 = (HtmlTableCell)(e.Row.FindControl("tdWords4"));
                tcw4.Visible = false;

               

            }
            
        }
    }
    protected void ddlEvent_SelectedIndexChanged(object sender, EventArgs e)
    {
        ddlPhase.Items.Clear();
        if (ddlEvent.SelectedValue.Equals("1"))
        {
            ddlPhase.Items.Add(new ListItem("Select Phase"));
            ddlPhase.Items.Add(new ListItem("Phase I","1"));
            ddlPhase.Items.Add(new ListItem("Phase II", "2"));
            ddlPhase.Items.Add(new ListItem("Phase III", "3"));
          
        }
        else if(ddlEvent.SelectedValue.Equals("2"))
        {
            ddlPhase.Items.Add(new ListItem("Select Phase"));
            ddlPhase.Items.Add(new ListItem("Phase I", "1"));
            ddlPhase.Items.Add(new ListItem("Phase II", "2"));
            
        }
    }
}