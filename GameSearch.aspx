<%@ Page Language="VB" MasterPageFile="~/NSFMasterPage.master" AutoEventWireup="false" CodeFile="GameSearch.aspx.vb" Inherits="GameSearch" title="Untitled Page" %>

<asp:Content ID="Content1" ContentPlaceHolderID="Content_main" Runat="Server">

<script type="text/javascript" language="javascript">
function hide_table(var1)
{
document.getElementById(var1).style.display = "none";
}
function show_searchtable(var1)
{
document.getElementById(var1).style.display = "block";
}
</script>


<div>
          <table cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
            <tr >
                <td colspan="4" align="center">
                <asp:Label ID="lblsearch" runat="server" Text="Search for a Game Registration " Font-Size="Large" ForeColor="DarkBlue"></asp:Label>
                 </td>
            </tr >
            <%--<tr >
                <td colspan="4" style="height: 22px"></td>
            </tr> --%>
            <tr >
                <td colspan="4"><asp:LinkButton ID="lnkmain" runat="server" CausesValidation="false"></asp:LinkButton></td>
            </tr>
            <tr>
            </tr>
            </table>
            <table id="tblVolselection" runat="server" cellspacing="1" cellpadding="3" width="90%"  align="center" border="0" >
            <tr><td style="color:Red; width: 40%;"><asp:Label id="lblerror" runat="server"></asp:Label></td>
            <td><asp:DropDownList ID="drpvolselection" runat="server">
            <asp:ListItem Text="Select One" Value="0" Selected="True"></asp:ListItem>
            <asp:ListItem Text="Search for Parent" Value="1"></asp:ListItem>
            <asp:ListItem Text="Game Requests" Value="2"></asp:ListItem>
            </asp:DropDownList>
                <asp:Label ID="lblvolselerror" runat="server" ForeColor="red"></asp:Label></td>
            </tr>
             <tr><td colspan="3" align="center">
             <asp:Button ID="btnsubmit" runat="server" Text="Submit" />
             </td>
             </tr>
           </table>
            <table id="tblsearch" runat="server" cellspacing="1" cellpadding="3" width="90%"  align="center" border="0">
            <tr bgcolor="#ffffff" >
                <td style="height: 30px; width: 168px;"  >
                    Child Name:</td>
                <td style="height: 30px;" >
                    <asp:TextBox ID="txtname" runat="server" CssClass="inputBox"> </asp:TextBox>
                    </td>
                
            </tr>
            <tr bgcolor="#ffffff" >
                <td style="width: 168px; height: 30px;" >
                    Principal Parent Name:</td>
                <td style="height: 30px" >
                    <asp:TextBox ID="txtpname" runat="server" CssClass="inputBox"></asp:TextBox></td>
                  </tr>
                  <tr bgcolor="#ffffff" >
                <td style="width: 168px" >
                    Spouse Name:</td>
                <td >
                    <asp:TextBox ID="txtsname" runat="server" CssClass="inputBox"></asp:TextBox></td>
                  </tr>
            <tr bgcolor="#ffffff" >
                <td style="width: 168px" >
                    E-Mail: </td>
                <td >
                    <asp:TextBox ID="txtemail" runat="server" CssClass="inputBox"></asp:TextBox>
                     </td>
            </tr>
            <tr bgcolor="#ffffff" >
                <td style="width: 168px; height: 26px;" >
                    Phone Number contains:</td>
                <td style="height: 26px" >
                    <asp:TextBox ID="txtphone" runat="server" CssClass="inputBox"></asp:TextBox>
                    <asp:Label ID="lblphone" runat="server" Text="(xxx-xxx-xxxx)"></asp:Label></td></tr>
               
            <tr bgcolor="#ffffff" >
                <td  colspan="4"> 
                    <asp:Button ID="btnSearch" runat="server" Text="Search"  />
                  </td>
            </tr>
        </table>
        
             
        <table id="table1" cellspacing="1" cellpadding="3" width="90%"  align="center" border="0">
     <tr bgcolor="#ffffff" >
        <td colspan="4" class="Heading"><asp:Label ID="lblsearchR" Text="Search Results" runat="server" Visible="false"></asp:Label></td>
     </tr>
     <tr><td><asp:Label ID="lblnodata" runat="server" ForeColor="red"></asp:Label></td></tr>
     </table>
    <asp:GridView ID="grd1" runat="server" AutoGenerateColumns="false" HeaderStyle-BackColor="#CCCC99" HeaderStyle-Font-Bold="true"  HeaderStyle-Font-Size="XX-Small" HeaderStyle-ForeColor="black" >
     <Columns>
     <asp:TemplateField  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
     <asp:LinkButton ID="lnk1" runat="server" Text="Select" CommandName="Select"></asp:LinkButton>
     </ItemTemplate>
     </asp:TemplateField>
     
     <asp:TemplateField HeaderText="Chapter"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblC" runat="server" Text='<%#Eval("Chapter")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
      <asp:TemplateField HeaderText="Child Name"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblCN" runat="server" Text='<%#Eval("Child Name")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
      <asp:TemplateField HeaderText="Child Gender"  HeaderStyle-ForeColor="Black">
      <ItemTemplate>
              <asp:Label ID="lblCG" runat="server" Text='<%#Eval("CGender")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
      
      <asp:TemplateField HeaderText="Phone"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblP" runat="server" Text='<%#Eval("HPhone")%>' Width="100px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
            
            <asp:TemplateField HeaderText="Cell Phone"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblCp" runat="server" Text='<%#Eval("Cphone")%>' Width="100px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
             
             <asp:TemplateField HeaderText="Parent Name"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblPN" runat="server" Text='<%#Eval("Parent Name")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
             <asp:TemplateField HeaderText="Parent Gender" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblPG" runat="server" Text='<%#Eval("PGender")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
       <asp:TemplateField HeaderText="Marital Status" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblMS" runat="server" Text='<%#Eval("MStatus")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
            <asp:TemplateField HeaderText="EMail"  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblEm" runat="server" Text='<%#Eval("E-Mail")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
            <asp:TemplateField HeaderText="Child Number" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblChildnumber" runat="server" Text='<%#Eval("childnumber")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Auto MemberID" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblAMI" runat="server" Text='<%#Eval("automemberid")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter ID" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblCID" runat="server" Text='<%#Eval("chapterid")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Date Of Birth" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblDOB" runat="server" Text='<%#Eval("date_of_birth","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
     <asp:TemplateField HeaderText="School Name" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblSN" runat="server" Text='<%#Eval("schoolname")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
     <asp:TemplateField HeaderText="Address" HeaderStyle-ForeColor="Black" >
     <ItemTemplate>
              <asp:Label ID="lblAdd" runat="server" Text='<%#Eval("address")%>' Width="200px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
     <asp:TemplateField HeaderText="City" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblCity" runat="server" Text='<%#Eval("City")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="Zip" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblZip" runat="server" Text='<%#Eval("zip")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           <asp:TemplateField HeaderText="State" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblState" runat="server" Text='<%#Eval("State")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Spouse Name" HeaderStyle-ForeColor="Black">
     <ItemTemplate>
              <asp:Label ID="lblSpN" runat="server" Text='<%#Eval("SpouseName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
     
     </Columns>
     
</asp:GridView>




<%---- ********************Gridview for Volunteer ************ ------%> 


<asp:GridView ID="Grd2" runat="server" AutoGenerateColumns="false" AllowPaging="true" PageSize="20" >
<Columns>
<asp:TemplateField  HeaderStyle-ForeColor="Black">
     <ItemTemplate>
     <asp:LinkButton ID="lnk1" runat="server" Text="Select" CommandName="Select"></asp:LinkButton>
     </ItemTemplate>
     </asp:TemplateField>
    <asp:TemplateField HeaderText="Game ID">
    <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small"  ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblgmid" runat="server" Text='<%#Eval("GameID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Child Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="200px" Height="10px" />
     <ItemTemplate>
              <asp:Label ID="lblcname" runat="server" Text='<%#Eval("ChildName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
    
     <asp:TemplateField HeaderText="Game Name" ItemStyle-Width="400px" ItemStyle-Height="10px" >
     <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblgname" runat="server" Text='<%#Eval("name")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
        
        <asp:TemplateField HeaderText="Create Date" ItemStyle-HorizontalAlign="Center">
        <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblcdate" runat="server" Text='<%#Eval("CreateDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
             
          <asp:TemplateField HeaderText="Child Login ID" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black"/>
     <ItemTemplate>
              <asp:Label ID="lblcid" runat="server" Text='<%#Eval("childLoginID")%>'></asp:Label>
           </ItemTemplate>
          </asp:TemplateField>
          
          <asp:TemplateField HeaderText="Child Password" >
          <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="150px" />
             <ItemTemplate>
              <asp:Label ID="lblcpd" runat="server" Text='<%#Eval("ChildPWD")%>'></asp:Label>
           </ItemTemplate>
          </asp:TemplateField>

        <asp:TemplateField HeaderText="Approved" >
<HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black"  Width="200px" />
     <ItemTemplate>
              <asp:Label ID="lblapp" runat="server" Text='<%#Eval("Approved")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Start Date" ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="250px" />
     <ItemTemplate>
              <asp:Label ID="lblsd" runat="server" Text='<%#Eval("StartDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
          </asp:TemplateField>
           
           <asp:TemplateField HeaderText="End Date"  ItemStyle-HorizontalAlign="Center" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="150px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lbled" runat="server" Text='<%#Eval("EndDate","{0:MM/dd/yyyy}")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Parent Name" ItemStyle-Width="600px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" Height="10px" />
     <ItemTemplate>
              <asp:Label ID="lblpname" runat="server" Text='<%#Eval("ParentName")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Active" ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblac" runat="server" Text='<%#Eval("Active")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Phone" ItemStyle-Width="500px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblhp" runat="server" Text='<%#Eval("hphone")%>' Height="10px"></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Email" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" Width="200px" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblem" runat="server" Text='<%#Eval("email")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter ID" ItemStyle-Width="100px" ItemStyle-Height="10px" ItemStyle-HorizontalAlign="Center">
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblci" runat="server" Text='<%#Eval("ChapterID")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Chapter" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="10px" />
     <ItemTemplate>
              <asp:Label ID="lblc" runat="server" Text='<%#Eval("Chapter")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="City" ItemStyle-Width="400px" ItemStyle-Height="10px" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" Width="10px" />
     <ItemTemplate>
              <asp:Label ID="lblcity" runat="server" Text='<%#Eval("City")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="State" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblstate" runat="server" Text='<%#Eval("State")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="ChildNumber" Visible="false" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblCnumber" runat="server" Text='<%#Eval("ChildNumber")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="Automemberid"  Visible="false" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblAMemberID" runat="server" Text='<%#Eval("automemberid")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           <asp:TemplateField HeaderText="EventID"  Visible="false" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblEventID" runat="server" Text='<%#Eval("eventid")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           
           <asp:TemplateField HeaderText="ProductId"  Visible="false" >
           <HeaderStyle BackColor="#CCCC99" Font-Bold="True" Font-Size="XX-Small" ForeColor="Black" />
     <ItemTemplate>
              <asp:Label ID="lblproductID" runat="server" Text='<%#Eval("productid")%>'></asp:Label>
           </ItemTemplate>
           </asp:TemplateField>
           
           
           
           
           
           
</Columns>
</asp:GridView>
       
    </div>

</asp:Content>

